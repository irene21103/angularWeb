package com.ebao.ls.uw.ds;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.Resource;
import javax.transaction.UserTransaction;

import org.apache.commons.collections.MapUtils;
import org.apache.log4j.Logger;

import com.ebao.foundation.app.sys.sysmgmt.ds.SysMgmtDSLocator;
import com.ebao.foundation.app.sys.sysmgmt.ds.UserVO;
import com.ebao.foundation.module.db.DBean;
import com.ebao.ls.chl.service.ChannelOrgService;
import com.ebao.ls.chl.vo.ChannelOrgVO;
import com.ebao.ls.foundation.service.GenericServiceImpl;
import com.ebao.ls.foundation.util.BeanConvertor;
import com.ebao.ls.leave.service.UserLeaveManagerService;
import com.ebao.ls.pa.nb.util.NBUtils;
import com.ebao.ls.pty.vo.DeptVO;
import com.ebao.ls.pty.vo.EmployeeVO;
import com.ebao.ls.pub.CodeCst;
import com.ebao.ls.pub.leave.data.bo.DataentryVerifConf;
import com.ebao.ls.pub.leave.data.bo.UserAreaConf;
import com.ebao.ls.pub.leave.data.bo.UserLeaveMange;
import com.ebao.ls.pub.leave.data.bo.VerifactionManagerConf;
import com.ebao.ls.pub.leave.vo.DataentryVerifConfVO;
import com.ebao.ls.pub.leave.vo.UserAreaConfVO;
import com.ebao.ls.pub.leave.vo.UserLeaveMangeVO;
import com.ebao.ls.pub.leave.vo.VerifactionManagerConfVO;
import com.ebao.ls.uw.ctrl.fileUpload.UserAreaDownUpLoadAction;
import com.ebao.ls.uw.data.TDataentryVerifConfDao;
import com.ebao.ls.uw.data.TDateStandardConfDao;
import com.ebao.ls.uw.data.TUserAreaConfDao;
import com.ebao.ls.uw.data.TUserLeaveMangeDao;
import com.ebao.ls.uw.data.TVerifactionManagerConfDao;
import com.ebao.ls.uw.ds.vo.LeaveAuthEdit;
import com.ebao.ls.uw.util.Utils;
import com.ebao.pub.framework.ExceptionFactory;
import com.ebao.pub.sys.sysmgmt.usermgr.UserDS;
import com.ebao.pub.util.BeanUtils;
import com.ebao.pub.util.Trans;
import com.ebao.pub.util.TransUtils;

/**
 * <p>Title: Module Information Andy GOD</p>
 * <p>Description: Function Description Only vegetable</p>
 * <p>Copyright: Copyright (c) 2015</p>
 * <p>Company: TGL Co., Ltd.</p>
 * <p>Create Time: Aug 11, 2015</p> 
 * @author 
 * <p>Update Time: Aug 11, 2015</p>
 * <p>Updater: Victor Chang</p>
 * <p>Update Comments: </p>
 */
public class UserLeaveManagerDSImpl extends GenericServiceImpl<UserLeaveMangeVO, UserLeaveMange, TUserLeaveMangeDao> implements UserLeaveManagerService{

	private static Logger logger = Logger.getLogger(UserLeaveManagerDSImpl.class);  
	
  @Resource(name = TUserAreaConfDao.BEAN_DEFAULT)
  private TUserAreaConfDao userAreaConfDao;
  
   
  @Resource(name = TVerifactionManagerConfDao.BEAN_DEFAULT)
  private TVerifactionManagerConfDao verifactionManagerConfDao;
  
  @Resource(name = TDataentryVerifConfDao.BEAN_DEFAULT)
  private TDataentryVerifConfDao dataentryVerifConfDao;
  
  @Resource(name = TDateStandardConfDao.BEAN_DEFAULT)
  private TDateStandardConfDao dateStandardConfDao;
  
  @Resource(name = TUserLeaveMangeDao.BEAN_DEFAULT)
  private TUserLeaveMangeDao userLeaveMangeDao;
 
  
	//PCR_41834 核保轄區設置作業調整及保留核保決定資料記錄
	@Resource(name = ChannelOrgService.BEAN_DEFAULT) 
	private ChannelOrgService channelOrgService;
	//END of PCR_41834  by Alex Chang 2020.12.14
  
  @Override
  protected UserLeaveMangeVO newEntityVO() {
    return new UserLeaveMangeVO();
  }
  
  /**
   * <p>Description : 核保作業查詢休假管理及核保轄區設置主管資料_以listId與empId為條件</p>
   * @param leaveAuthListId
   * @param empId
   * @param uwSourceType(區分新契約/保全)
   * @return
   */
	@Override
	public LeaveAuthEdit findCSOrUNBAuthLeave(Long leaveAuthListId, Long empId, String uwSourceType) {
		LeaveAuthEdit retVO = new LeaveAuthEdit();

		if (Utils.isCsUw(uwSourceType)) {
			//保全：type_id=2(上級核保主管)；category_id=4(POS)
			retVO = userLeaveMangeDao.findLeaveAuthByListIdEmpId(leaveAuthListId, empId,
					CodeCst.VERIFACTION_MANAGER_TYPE_2);
		} else {
			//新契約:type=1(覆核主管)
			retVO = userLeaveMangeDao.findLeaveAuthByListIdEmpId(leaveAuthListId, empId,
					CodeCst.VERIFACTION_MANAGER_TYPE_1);
		}
		return retVO;
	}

	/**
	 * <p>Description : 休假管理及核保轄區設置資料_以listId與empId為條件</p>
	 * <p>Created By : simon.huang</p>
	 * <p>Create Time : Feb 16, 2017</p>
	 * @param leaveAuthListId
	 * @param empId
	 * @param managerType
	 * @return
	 */
	@Override
	public LeaveAuthEdit findAuthLeave(Long leaveAuthListId, Long empId, int managerType) {
		LeaveAuthEdit retVO = new LeaveAuthEdit();

		retVO = userLeaveMangeDao.findLeaveAuthByListIdEmpId(leaveAuthListId, empId,
				managerType);

		return retVO;
	}
	  
  /**
   * <p>Description : 新契約－核保作業覆核主管查詢</p>
   * <p>Created By : simon.huang</p>
   * <p>Create Time : May 19, 2016</p>
   * @return
   */
  @Override
  public List<VerifactionManagerConfVO> findVerifactionManager(Long empId){
      List<VerifactionManagerConf> verifactionManagerConfs = new ArrayList<VerifactionManagerConf>();
      List<VerifactionManagerConfVO> verifactionManagerConfVOs = new ArrayList<VerifactionManagerConfVO>();
      verifactionManagerConfs = verifactionManagerConfDao.findVerifactionManagerConfByEmpId(empId);
      verifactionManagerConfVOs=(List<VerifactionManagerConfVO>)BeanUtils.copyCollection(VerifactionManagerConfVO.class, verifactionManagerConfs);
      return verifactionManagerConfVOs;
  }
  
  /**
   * <p>Description : 查詢部門/分公司或科別資料_以deptId與dept_grade為條件</p>
   * <p>Created By : Victor Chang</p>
   * <p>Create Time : Aug 14, 2015</p>
   * @param deptId
   * @param level
   * @return
   */
  @Override
  public List<DeptVO> findDeptByLevel(String deptId, String level){
      return null;
  }
  
  /**
   * <p>Description : 查詢員工資料BY DeptId</p>
   * <p>Created By : Victor Chang</p>
   * <p>Create Time : Aug 14, 2015</p>
   * @param deptId
   * @return
   */
  @Override
  public List<EmployeeVO> findEmpByDeptId(String deptId){
      return null;
  }
  
  /**
   * <p>Description : 查詢休假及核保轄區資料BY EmpId</p>
   * <p>Created By : Victor Chang</p>
   * <p>Create Time : Aug 14, 2015</p>
   * @param empId
   * @return
   */
  @Override
  public LeaveAuthEdit findDeptByEmpId(String empId){
      return userLeaveMangeDao.findDeptByEmpId(empId);
  }
  
  /**
   * <p>Description : 儲存/修改休假</p>
   * <p>Created By : Victor Chang</p>
   * <p>Create Time : Aug 14, 2015</p>
   * @param newVO
   * @throws Exception
   */
  @Override
   public void saveUserLeaveData(UserLeaveMangeVO newVO) throws Exception{
      
      UserLeaveMange leaveUwAuth = null;
      if(newVO.getListId() != null && newVO.getListId() > 0){
        leaveUwAuth = userLeaveMangeDao.load(newVO.getListId());
      }else{
        leaveUwAuth = new UserLeaveMange();
      }
      BeanConvertor.convertBean(leaveUwAuth, newVO, true);

      userLeaveMangeDao.save(leaveUwAuth);
       
   }
 

   /**
    * <p>Description : 儲存/修改核保轄區資料</p>
    * <p>Created By : Victor Chang</p>
    * <p>Create Time : Aug 14, 2015</p>
    * @param userAreaConfList
    * @param empId
    * @throws Exception
    */
   @Override
    public void saveUserAreaData(List<UserAreaConfVO> userAreaConfList,Long empId) throws Exception{
       //Long empId = userAreaConfList.get(0).getEmpId();
       List<UserAreaConf> uwAreaList = userAreaConfDao.findUserAreaConfByEmpId(empId);
       Map<Long ,UserAreaConf> uwAreaMap = new HashMap<Long ,UserAreaConf>();
       
       if(uwAreaList != null && uwAreaList.size() > 0){
         for(UserAreaConf uwArea : uwAreaList){
           uwAreaMap.put(uwArea.getListId(), uwArea);
         }
       }
       
       if(userAreaConfList != null && userAreaConfList.size() > 0){
           UserAreaConf oldUwArea = null;
         for(UserAreaConfVO newUwAreaVO : userAreaConfList){
           oldUwArea = null;
           boolean isInsert = true;
           if(newUwAreaVO.getListId() != null && newUwAreaVO.getListId() > 0){
             /* 更新 */
             oldUwArea = (UserAreaConf)MapUtils.getObject(uwAreaMap, newUwAreaVO.getListId(), null);
             if(oldUwArea != null){
               BeanConvertor.convertBean(oldUwArea, newUwAreaVO, true);
               userAreaConfDao.save(oldUwArea);
               isInsert = false;
             }
             uwAreaMap.remove(newUwAreaVO.getListId());
           }
           if(isInsert){
             /* 新增 */
             oldUwArea = new UserAreaConf();
             BeanConvertor.convertBean(oldUwArea, newUwAreaVO);
             oldUwArea.setListId(null);
             userAreaConfDao.save(oldUwArea);
           }
         }
       }
       /* 刪除剩下的舊資料 */
       if(uwAreaMap.size() > 0){
         for(Entry<Long, UserAreaConf> entry : uwAreaMap.entrySet()){
             userAreaConfDao.remove(entry.getValue());
         }
       }
    } 
    
    /**
     * <p>Description : 儲存/修改 預收覆核人員設置</p>
     * <p>Created By : Victor Chang</p>
     * <p>Create Time : Aug 14, 2015</p>
     * @param dataentryVerif
     * @throws Exception
     */
    @Override
     public void saveDataentryVerifData(DataentryVerifConfVO dataentryVerif) throws Exception{
            DataentryVerifConf bo = new DataentryVerifConf();
            if(dataentryVerif.getListId() !=null && dataentryVerif.getListId()>0){
                bo = dataentryVerifConfDao.load(dataentryVerif.getListId());
            }
            bo.copyFromVO(dataentryVerif, true, false);
           
            UserTransaction ut = null;
            try {
                ut = Trans.getUserTransaction();
                ut.begin();
                dataentryVerifConfDao.saveorUpdate(bo);
                ut.commit();
            } catch (Exception e) {
                TransUtils.rollback(ut);
                throw ExceptionFactory.parse(e);
            }
       
        
  
       
     } 

     /**
      * <p>Description : 儲存/修改  覆核主管設定</p>
      * <p>Created By : Victor Chang</p>
      * <p>Create Time : Aug 14, 2015</p>
      * @param verifactionManagerList
      * @param empId
      * @throws Exception
      */
     @Override
      public void saveVerifactionManagerData(List<VerifactionManagerConfVO> verifactionManagerList ,Long empId) throws Exception{
//         if(verifactionManagerList.size()>0){
//         Long empId = verifactionManagerList.get(0).getEmpId();
         List<VerifactionManagerConf> uwAreaList = verifactionManagerConfDao.findVerifactionManagerConfByEmpId(empId);
         Map<Long ,VerifactionManagerConf> uwAreaMap = new HashMap<Long ,VerifactionManagerConf>();
         
         if(uwAreaList != null && uwAreaList.size() > 0){
           for(VerifactionManagerConf uwArea : uwAreaList){
             uwAreaMap.put(uwArea.getListId(), uwArea);
           }
         }
         
         if(verifactionManagerList != null && verifactionManagerList.size() > 0){
             VerifactionManagerConf oldUwArea = null;
           for(VerifactionManagerConfVO newUwAreaVO : verifactionManagerList){
             oldUwArea = null;
             boolean isInsert = true;
             if(newUwAreaVO.getListId() != null && newUwAreaVO.getListId() > 0){
               /* 更新 */
               oldUwArea = (VerifactionManagerConf)MapUtils.getObject(uwAreaMap, newUwAreaVO.getListId(), null);
               if(oldUwArea != null){
                 BeanConvertor.convertBean(oldUwArea, newUwAreaVO, true);
                 verifactionManagerConfDao.save(oldUwArea);
                 isInsert = false;
               }
               uwAreaMap.remove(newUwAreaVO.getListId());
             }
             if(isInsert){
               /* 新增 */
               oldUwArea = new VerifactionManagerConf();
               BeanConvertor.convertBean(oldUwArea, newUwAreaVO);
               oldUwArea.setListId(null);
               verifactionManagerConfDao.save(oldUwArea);
             }
           }
         }
         /* 刪除剩下的舊資料 */
         if(uwAreaMap.size() > 0){
           for(Entry<Long, VerifactionManagerConf> entry : uwAreaMap.entrySet()){
               verifactionManagerConfDao.remove(entry.getValue());
           }
         }
      }
//     }
  /**
   * <p>Description : 人員查詢</p>
   * <p>Created By : Victor Chang</p>
   * <p>Create Time : Aug 17, 2015</p>
   * @param deptId
   * @param sectionDeptId
   * @param empId
   * @param empCode
   * @param isEditRole
   * @param loginDeptId
   * @return
   * @throws Exception
   */
   @Override
  public List<LeaveAuthEdit> searchLeaveDataByEmp(Long deptId, Long sectionDeptId, 
          Long empId, String empCode, boolean isEditRole, Long loginDeptId) throws Exception{
       
       return userLeaveMangeDao.searchLeaveDataByEmp(deptId, sectionDeptId, empId, empCode, isEditRole, loginDeptId);
       
          }
  
  /**
   * <p>Description : 轄區查詢</p>
   * <p>Created By : Victor Chang</p>
   * <p>Create Time : Aug 17, 2015</p>
   * @param channelId
   * @param communicateId
   * @param isEditRole
   * @param loginDeptId
   * @return
   * @throws Exception
   */
  @Override
  public List<LeaveAuthEdit> searchLeaveDataByArea(Long channelId, 
          Long communicateId, boolean isEditRole, Long loginDeptId) throws Exception{
      return userLeaveMangeDao.searchLeaveDataByArea(channelId, communicateId, isEditRole, loginDeptId);
  }
  
  /**
   * <p>Description : 以empId取得轄區資訊</p>
   * <p>Created By : Victor Chang</p>
   * <p>Create Time : Aug 11, 2015</p>
   * @param empId
   * @return
   */
  @Override
  public List<UserAreaConfVO>findAreaByEmpId(Long empId){
    List<UserAreaConf> userAreaConfs = new ArrayList<UserAreaConf>();
    List<UserAreaConfVO> userAreaConfVOs = new ArrayList<UserAreaConfVO>();
    userAreaConfs = userAreaConfDao.findUserAreaConfByEmpId(empId);
    if(userAreaConfs != null){
        userAreaConfVOs=(List<UserAreaConfVO>)BeanUtils.copyCollection(UserAreaConfVO.class, userAreaConfs);
    }
    return userAreaConfVOs ;

  }
  
  @Override
  public DataentryVerifConfVO findDataentryVerifConfByEmpId(Long empId){
      
    DataentryVerifConf dataentryVerifConf = new DataentryVerifConf();
    dataentryVerifConf = dataentryVerifConfDao.findDataentryVerifConfByEmpId(empId) ;
    DataentryVerifConfVO vo = new DataentryVerifConfVO();
    if(dataentryVerifConf != null){
        dataentryVerifConf.copyToVO(vo, false);
        vo.setUpdatedBy(dataentryVerifConf.getUpdatedBy());
        vo.setUpdateTime(dataentryVerifConf.getUpdateTime());
      
    }
    return vo;
   
  }
  
  @Override
  public List<Map<String, String>> findVerifManagerType(Integer categoryId){
      List<Map<String, String>> verifManagerTypes = verifactionManagerConfDao.findVerifManagerType(categoryId);
      return verifManagerTypes;
  }

  @Override
  public List<Map<String, String>> findCompanyOrgan(){
      List<Map<String, String>> companyOrgan = verifactionManagerConfDao.findCompanyOrgan();
      return companyOrgan;
  }
  
  
  public List<DeptVO> findDeptByOrgId(Long orgId){
      
      List<DeptVO>  vos = verifactionManagerConfDao.findDeptByOrgId(orgId);
      return vos;
  }
  
  /**
   * <p>Description : 取得chhelOrgId下核保人員，過濾已離職的員工。</p>
   * <p>Created By : Sunny Wu</p>
   * <p>Create Time : Oct 2, 2016</p>
   * <p>Updated By : Yvon Sun</p>
   * <p>Updated Time : May 9, 2019</p>
   * @param channelId
   * @return
   */
  @Override
  public List<Long> findUwEmpByChannelId(Long channelId) {
	  List<Long> emps = new ArrayList<Long>();
	  List<UserAreaConf> userAreaList = userAreaConfDao.findUserAreaConfByChannelId(channelId);
	  if ( userAreaList != null) {
		  for(UserAreaConf conf: userAreaList) {
              UserVO userVO = SysMgmtDSLocator.getUserDS().getUserByUserID(conf.getEmpId());
              if (!userVO.getDisable()) {
                  emps.add(conf.getEmpId());
              }
		  }
	  }
	  return emps;
  }
  
  @Override
  public String getSampPersonIndi(Long empId) {
	   DataentryVerifConf verifConf = dataentryVerifConfDao.findDataentryVerifConfByEmpId(empId);
	  if (verifConf != null ) {
		  return verifConf.getSamePersonIndi();
	  }
	  return null;
  }
  
  /**
  * <p>Description : 刪除休假</p>
  * <p>Created By : Victor Chang</p>
  * <p>Create Time : Nov 24, 2016</p>
  * @param newVO
  * @throws Exception
  */
  @Override
  public void delUserLeaveData(UserLeaveMangeVO newVO) throws Exception{
      UserLeaveMange bo = new UserLeaveMange();
      if(newVO.getListId() !=null && newVO.getListId()>0){
          bo = userLeaveMangeDao.load(newVO.getListId());
      }
      bo.copyFromVO(newVO, true, false);
     
      UserTransaction ut = null;
      try {
          ut = Trans.getUserTransaction();
          ut.begin();
          userLeaveMangeDao.remove(bo);
          ut.commit();
      } catch (Exception e) {
          TransUtils.rollback(ut);
          throw ExceptionFactory.parse(e);
      } 
      
  }

	/**
	 * <p>Description : 取得所有轄區設置資訊</p>
	 * <p>PCR_41834 核保轄區設置作業調整及保留核保決定資料記錄 </p>
	 * <p>Created By : Alex Chang</p>
	 * <p>Create Time : 2020.12.14</p>
	 * @param empId
	 * @return
	 */
//	@Override
//	public List<UserAreaConfVO> getAllUserAreaConf(){
//		List<UserAreaConf> userAreaConfs = new ArrayList<UserAreaConf>();
//		List<UserAreaConfVO> userAreaConfVOs = new ArrayList<UserAreaConfVO>();
//		userAreaConfs = userAreaConfDao.getAllUserAreaConf();
//		if(userAreaConfs != null){
//			userAreaConfVOs=(List<UserAreaConfVO>)BeanUtils.copyCollection(UserAreaConfVO.class, userAreaConfs);
//		}
//		return userAreaConfVOs ;
//	
//	}
  
	/**
	 * <p>Description : 儲存轄區設置資料</p>
	 * <p>PCR_41834 核保轄區設置作業調整及保留核保決定資料記錄 </p>
	 * <p>Created By : Alex Chang</p>
	 * <p>Create Time : 2020.12.14</p>
	 * @param empId
	 * @return
	 */
	@Override
	public HashMap<String, String> saveUserAreaConfs(HashMap<String, String[]> insertDataMap) {
        UserTransaction ut = null;
 		long channelId= 0l, channelType= 0l, deptId= 0l, empId= 0l, organId= 0l;
 		String rowNumStr_Xls= null;
 		HashMap<String, String> rtnErrMap= new HashMap<String, String>();
		SimpleDateFormat sdfYYYYMMDD_HMS=new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date startTime_OneRow= new Date();
 		try {
 			ut = Trans.getUserTransaction();
 			ut.begin();
         	
         	for(String tmpKey: insertDataMap.keySet() ) {
         		boolean is1stRow= (rowNumStr_Xls== null? true: false); 
         		if (is1stRow) {	//1st Row?
         			startTime_OneRow= new Date();
	        		NBUtils.logger(this.getClass(), "saveUserAreaConfs [[Save one row]] begin");
         		}
         		rowNumStr_Xls= tmpKey;
    			String[] tmpAry= insertDataMap.get(tmpKey);
    	 		channelId	= (tmpAry[0]== null? 0l: Long.parseLong(tmpAry[0]));
    	 		channelType	= (tmpAry[1]== null? 0l: Long.parseLong(tmpAry[1]));
    	 		deptId		= (tmpAry[2]== null? 0l: Long.parseLong(tmpAry[2]));
    	 		empId		= (tmpAry[3]== null? 0l: Long.parseLong(tmpAry[3]));
    	 		organId		= (tmpAry[4]== null? 0l: Long.parseLong(tmpAry[4]));

	        	UserAreaConfVO voUserAreaConf =new UserAreaConfVO();
	        	voUserAreaConf.setListId(null);
	        	voUserAreaConf.setEmpId(empId);
	        	voUserAreaConf.setDeptId(deptId);
	        	voUserAreaConf.setOrgId(organId);
	        	voUserAreaConf.setChannelType(channelType);
	        	voUserAreaConf.setChannelOrgId(channelId);
	        	
	        	UserAreaConf userAreaConfObj= new UserAreaConf();

	        	BeanConvertor.convertBean(userAreaConfObj, voUserAreaConf, true);
	            userAreaConfDao.save(userAreaConfObj);
	            
         		if (is1stRow) //1st Row?
	        		NBUtils.logger(this.getClass(), "saveUserAreaConfs [[Save one row]] end"+ "\r\n"
	        								+ "startTime ["+ sdfYYYYMMDD_HMS.format( startTime_OneRow)+ "]");
         	}

         	ut.commit();
 		} catch (Exception e) {
	 		logger.warn("Error in Excel Line No.:["+ rowNumStr_Xls+ "]");
	 		rtnErrMap.put(rowNumStr_Xls, e.getMessage() );
	 		
	 		TransUtils.rollback(ut);
	 		throw ExceptionFactory.parse(e);
 		}
		 finally{
 			 if (ut!= null) {ut= null;}
// 			// 關閉所有連線
//             DBean.closeAll(null, ps, db);
 		 }
 		return rtnErrMap;
    }                    

	/**
	 * <p>Description : 儲存轄區設置資料</p>
	 * <p>PCR_41834 核保轄區設置作業調整及保留核保決定資料記錄 </p>
	 * <p>Created By : Alex Chang</p>
	 * <p>Create Time : 2020.12.14</p>
	 * @param empId
	 * @return
	 */
	@Override
 	public void saveUserAreaConfs(Long empId, Long channelId) {
        UserTransaction ut = null;
         try {
        	UserVO userVO = UserDS.getUserById(empId );
        	String organIdStr= userVO.getOrganId();
        	String deptIdStr= userVO.getDeptId();

        	long organId= Long.parseLong(organIdStr);
        	long deptId= Long.parseLong(deptIdStr);

        	ChannelOrgVO channelOrgVO = channelOrgService.getChannelOrg(channelId );
        	long channelType= channelOrgVO.getChannelType();

        	UserAreaConfVO vo =new UserAreaConfVO();
        	vo.setListId(null);
        	vo.setEmpId(empId);
        	vo.setDeptId(deptId);
        	vo.setOrgId(organId);
        	vo.setChannelType(channelType);
        	vo.setChannelOrgId(channelId);
        	ut = Trans.getUserTransaction();
        	ut.begin();
//        	List<UserAreaConfVO> userAreaLst= new ArrayList<UserAreaConfVO>();
//        	userAreaLst.add(vo);
        	UserAreaConf oldUwArea = new UserAreaConf();
            BeanConvertor.convertBean(oldUwArea, vo, true);
            userAreaConfDao.save(oldUwArea);
            
        	ut.commit();
         } catch (Exception e) {
             TransUtils.rollback(ut);
             throw ExceptionFactory.parse(e);
         }                    
 		 finally{
 			 if (ut!= null) {ut= null;}
// 			// 關閉所有連線
//             DBean.closeAll(null, ps, db);
 		 }
		
	}

	/**
	 * <p>Description : 清除轄區設置所有資料</p>
	 * <p>PCR_41834 核保轄區設置作業調整及保留核保決定資料記錄 </p>
	 * <p>Created By : Alex Chang</p>
	 * <p>Create Time : 2020.12.14</p>
	 * @return
	 */
	@Override
 	public void removeUserAreaConf() {
        UserTransaction ut = null;
		DBean db = new DBean();
		PreparedStatement ps = null;
		Statement stmt = null;
		ResultSet rs = null;
 		try {
			db.connect();
			Connection con = db.getConnection();
 			ut = Trans.getUserTransaction();
 			ut.begin();
// 			userAreaConfDao.queryByExample(example)
 			String sqlTmp= "Delete From T_USER_AREA_CONF ";
 			stmt= con.createStatement();
			int rtnExecCode= stmt.executeUpdate(sqlTmp);
			
    		logger.debug("############### Delete Row quantity["+ rtnExecCode+ "]\r\n"
					//+ "["+ ""+ "]"+ "\r\n"
					+ "");
        	ut.commit();
         } catch (Exception e) {
             TransUtils.rollback(ut);
             throw ExceptionFactory.parse(e);
         }
 		 finally{
 			 if (ut!= null) {ut= null;}
 			// 關閉所有連線
             DBean.closeAll(null, ps, db);
 		 }
	}                    

}
