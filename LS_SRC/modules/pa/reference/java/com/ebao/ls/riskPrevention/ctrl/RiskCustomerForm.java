package com.ebao.ls.riskPrevention.ctrl;

import com.ebao.ls.riskPrevention.vo.RiskCustomerLogVO;
import com.ebao.ls.riskPrevention.vo.RiskCustomerVO;
import com.ebao.pub.web.pager.PagerFormImpl;

public class RiskCustomerForm extends PagerFormImpl {

    private static final long serialVersionUID = -744673924865248218L;
    
    public final static String RETURN_MSG = "T_STRING_RESOURCE__STR_ID";
    
    public final static String RISK_CUSTOMER_LOG_ID = "T_RISK_CUSTOMER__LOG_ID";
    
    private RiskCustomerVO vo = new RiskCustomerVO();

    private RiskCustomerLogVO log = new RiskCustomerLogVO();
    
    private String policyId;

    private String riskNationalScore;

    private String certiCode;

    private String logListId;

    private boolean auth = false;

    private boolean error = false;

    private String mainPOPup;

    private String detailPOPup;
    
    
    private Integer riskManual;//調整風險
    
    public RiskCustomerVO getVo() {
        return vo;
    }

    public void setVo(RiskCustomerVO vo) {
        this.vo = vo;
    }

    public RiskCustomerLogVO getLog() {
        return log;
    }

    public void setLog(RiskCustomerLogVO log) {
        this.log = log;
    }

    public boolean isAuth() {
        return auth;
    }

    public void setAuth(boolean auth) {
        this.auth = auth;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getRiskNationalScore() {
        return riskNationalScore;
    }

    public void setRiskNationalScore(String riskNationalScore) {
        this.riskNationalScore = riskNationalScore;
    }

    public String getCertiCode() {
        return certiCode;
    }

    public void setCertiCode(String certiCode) {
        this.certiCode = certiCode;
    }

    public String getLogListId() {
        return logListId;
    }

    public void setLogListId(String logListId) {
        this.logListId = logListId;
    }

    public String getMainPOPup() {
        return mainPOPup;
    }

    public void setMainPOPup(String mainPOPup) {
        this.mainPOPup = mainPOPup;
    }

    public String getDetailPOPup() {
        return detailPOPup;
    }

    public void setDetailPOPup(String detailPOPup) {
        this.detailPOPup = detailPOPup;
    }

    public String getPolicyId() {
        return policyId;
    }

    public void setPolicyId(String policyId) {
        this.policyId = policyId;
    }

    public Integer getRiskManual() {
        return riskManual;
    }

    public void setRiskManual(Integer riskManual) {
        this.riskManual = riskManual;
    }

}
