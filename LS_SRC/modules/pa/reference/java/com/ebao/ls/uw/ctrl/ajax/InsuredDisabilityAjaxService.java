package com.ebao.ls.uw.ctrl.ajax;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.transaction.UserTransaction;

import org.apache.commons.collections.MapUtils;

import com.ebao.foundation.module.util.i18n.StringResource;
import com.ebao.ls.cs.commonflow.data.TPolicyChangeDelegate;
import com.ebao.ls.pa.nb.bs.DisabilityCancelDetailService;
import com.ebao.ls.pa.nb.data.DisabilityCancelDetailDao;
import com.ebao.ls.pa.pub.bs.DisabilityCancelListService;
import com.ebao.ls.pa.pub.bs.InsuredDisabilityIcfService;
import com.ebao.ls.pa.pub.bs.InsuredDisabilityListService;
import com.ebao.ls.pa.pub.bs.InsuredService;
import com.ebao.ls.pub.ajax.AjaxService;
import com.ebao.ls.uw.ctrl.underwriting.UwPolicySubmitActionHelper;
import com.ebao.ls.uw.data.bo.UwInsuredDisability;
import com.ebao.ls.uw.data.bo.UwInsuredDisabilityIcf;
import com.ebao.ls.uw.ds.UwInsuredDisabilityIcfService;
import com.ebao.ls.uw.ds.UwInsuredDisabilityService;
import com.ebao.ls.uw.ds.UwPolicyService;
import com.ebao.ls.uw.ds.UwProcessService;
import com.ebao.ls.uw.ds.constant.Cst;
import com.ebao.ls.uw.vo.UwLifeInsuredVO;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.util.StringUtils;
import com.ebao.pub.util.Trans;
import com.ebao.pub.util.TransUtils;
import com.ebao.pub.wincony.ui.CodeTableUtils;

public class InsuredDisabilityAjaxService implements
                AjaxService<Map<String, String>, Map<String, Object>> {
    @Resource(name = UwInsuredDisabilityService.BEAN_DEFAULT)
    private UwInsuredDisabilityService uwInsuredDisbilityDS;
    
    @Resource(name = UwPolicyService.BEAN_DEFAULT)
    private UwPolicyService uwPolicyDS;

	@Resource(name = DisabilityCancelDetailService.BEAN_DEFAULT)
	private DisabilityCancelDetailService disabilityCancelDetailService;

    @Resource(name = UwPolicySubmitActionHelper.BEAN_DEFAULT)
    protected UwPolicySubmitActionHelper uwHelper;
    
	@Resource(name = DisabilityCancelDetailDao.BEAN_DEFAULT)
	protected DisabilityCancelDetailDao disabilityCancelDetailDao;
	 
    @Resource(name = UwInsuredDisabilityIcfService.BEAN_DEFAULT)
    private UwInsuredDisabilityIcfService uwInsuredDisabilityIcfService;
    
    @Resource(name = InsuredDisabilityIcfService.BEAN_DEFAULT)
	private InsuredDisabilityIcfService insuredDisabilityIcfService;

	@Resource(name = InsuredService.BEAN_DEFAULT)
	private InsuredService insuredService;

	@Resource(name = InsuredDisabilityListService.BEAN_DEFAULT)
	private InsuredDisabilityListService insuredDisabilityListService;

    @Resource(name = UwInsuredDisabilityService.BEAN_DEFAULT)
    private UwInsuredDisabilityService uwInsuredDisabilityService;
    
    @Resource(name = DisabilityCancelListService.BEAN_DEFAULT)
	private DisabilityCancelListService disabilityCancelListService;
    
	@Resource(name = UwProcessService.BEAN_DEFAULT)
	private UwProcessService uwProcessService;
	
    
    @Override
    public Map<String, Object> execute(Map<String, String> in) {
        
    	String type = in.get("type");
		String sourceType = in.get("sourceType");
		Long changeId = -1l;
		Long policyChgId = -1l;
		
		if ("UwCs".equals(sourceType)){
			changeId = in.get("changeId2")!=null?Long.parseLong(in.get("changeId2")):null;
			policyChgId = TPolicyChangeDelegate.findByMasterChgIdOrderByOrderId(changeId).get(0).getPolicyChgId();
		}
		
        UserTransaction trans = null;
        Map<String, Object> returnMap = new HashMap<String, Object>();
        try {
        	
        	
        	Long underwriteId = in.get("underwriteId")!=null?Long.parseLong(in.get("underwriteId")):null;
            Long insuredId = in.get("insuredId")!=null?Long.parseLong(in.get("insuredId")):null;
            String disabilityCategory = in.get("disabilityCategory")!=null?in.get("disabilityCategory"):null;
            String disabilityClass = in.get("disabilityClass")!=null?in.get("disabilityClass"):null;
            
            if ("load".equals(type)) {
                UwLifeInsuredVO uwLifeInsuredVO = uwPolicyDS.findUwLifeInsuredByListId(underwriteId, insuredId);
                returnMap.put("insuredDisabilityType", uwInsuredDisbilityDS.getDisabilityTypes(uwLifeInsuredVO.getUwListId()));
                returnMap.put("uwLifeInsured", uwLifeInsuredVO);
                returnMap.put("success", "1");
                
            } else if ("reload".equals(type)) {
                returnMap.put("success", "1");
                returnMap.put("insuredList", getInusredList(underwriteId));
                
            } else if ("save".equals(type)) {
                UwLifeInsuredVO uwLifeInsuredVO = uwPolicyDS.findUwLifeInsuredByListId(underwriteId, insuredId);
				Long policyId = uwLifeInsuredVO.getPolicyId();
				
            	//身心障礙類別不為空才檢核，檢核所有身心類別是否都有對應ICF資料
    			if(in.get("disabilityTypes") != null){
    				List<UwInsuredDisability> uwDisList = uwInsuredDisbilityDS.findByUwInsuredId(uwLifeInsuredVO.getUwListId());
    				String[] disabilityTypes = in.get("disabilityTypes").split(",");
    				StringBuffer buffer = new StringBuffer();
    				for(int i=0;i < disabilityTypes.length; i++){
    					if(!isExistsICF(disabilityTypes[i], uwDisList)){
    						buffer.append(disabilityTypes[i]).append("、");
    					}
    				}
    				if (buffer.length() > 0){
    					String errmsg = StringResource.getStringData("MSG_1267237", AppContext.getCurrentUser().getLangId());
    					Map<String, String> map = new HashMap<String, String>();
    					map.put("disabilityTypes", buffer.substring(0, buffer.length() - 1));
    					errmsg = com.ebao.ls.cs.pub.util.StringUtils.formatMapStr(errmsg, map);
    		            returnMap.put("success", "0");
    		            returnMap.put("error", errmsg);
    		            return returnMap;
    				}
    			}
    			
    			trans = Trans.getUserTransaction();
                trans.begin();
    			
                // 判斷前端傳入的身心障礙類別是否比資料庫ICF資料還少，是則代表頁面取消該身心類別勾選，需刪除ICF相關table資訊
                String[] deleteIcfs = getDeleteIcfs(underwriteId,policyId,insuredId,in.get("disabilityTypes").split(","),disabilityClass,changeId,policyChgId);
                if (deleteIcfs!=null && deleteIcfs.length>0) {
                	//刪除核保ICF編碼
            		uwInsuredDisabilityIcfService.deleteUwIcf(underwriteId, insuredId, deleteIcfs, changeId, policyChgId);
            		//同步核保被保人相關欄位資料
            		uwProcessService.syncUwInsured(underwriteId, insuredId, disabilityClass);
				}
                
                if ("UwCs".equals(sourceType)){
            		insuredService.syncInsured(changeId, policyChgId, insuredId, disabilityClass);
                	disabilityCancelListService.resetUwDisabilityCancelListAndIcf(underwriteId, insuredId, "POS", "1");
                }
                
                uwLifeInsuredVO = uwPolicyDS.findUwLifeInsuredByListId(underwriteId, insuredId);
                uwPolicyDS.updateUwLifeInsured(uwLifeInsuredVO);
                
                trans.commit();
                returnMap.put("success", "1");
                returnMap.put("insuredList", getInusredList(underwriteId));
                
            } else if ("delete".equals(type)) {
            	
                List<String> insuredListIdList = Arrays.asList(in.get("insuredListIds").split(","));
                List<UwLifeInsuredVO> insuredVOs = new ArrayList<UwLifeInsuredVO>();
                for (String id : insuredListIdList) {
                    insuredVOs.add(uwPolicyDS.findUwLifeInsuredByListId(underwriteId, Long.parseLong(id)));
                }
                trans = Trans.getUserTransaction();
                trans.begin();
                
                for (UwLifeInsuredVO uwLife : insuredVOs) {
                	String[] deleteIcfs = getDeleteIcfs(uwLife,changeId,policyChgId);
                	uwInsuredDisabilityIcfService.deleteUwIcf(underwriteId, uwLife.getListId(), deleteIcfs, changeId, policyChgId);
                	
                	//同步核保被保人相關欄位資料
            		uwProcessService.syncUwInsured(underwriteId, uwLife.getListId(), disabilityClass);
            		
            		if ("UwCs".equals(sourceType)){
            			insuredService.syncInsured(changeId, policyChgId, uwLife.getListId(), disabilityClass);
                    	disabilityCancelListService.resetUwDisabilityCancelListAndIcf(underwriteId, uwLife.getListId(), "POS", "1");
                    }
            		
            		uwLife = uwPolicyDS.findUwLifeInsuredByListId(underwriteId, uwLife.getListId());
                    uwPolicyDS.updateUwLifeInsured(uwLife);
                }

                trans.commit();
                returnMap.put("success", "1");
                returnMap.put("insuredList", getInusredList(underwriteId));
            } else if("change".equals(type)){
            	String typeId = MapUtils.getString(in, "typeId", null);
            	String disabilityCategoryDesc = "";
            	
            	if(!StringUtils.isNullOrEmpty(typeId)){
            		
            		if (Cst.DISABILITY_TYPE_MULTIPLE.equals(typeId)) {
            			String disabilityType = CodeTableUtils.getDesc("T_DISABILITY_CATEGORY", typeId);
	            		disabilityCategoryDesc  = typeId + disabilityType;
            			disabilityCategory  = typeId;
            			
            		} else {
            			String[][] disabCategory = CodeTableUtils.getCodeData("V_DISABILITY_CATEGORY", "DISABILITY_TYPE = '"+typeId+"'", "", AppContext.getCurrentUser().getLangId());
	            		if(disabCategory != null && disabCategory.length > 0 && disabCategory[0].length > 0){
	            			disabilityCategoryDesc  = disabCategory [0][0] + disabCategory [0][2];
	            			disabilityCategory = disabCategory [0][0];
	            		}
	            	}
            	}
            	returnMap.put("success", "1");
            	returnMap.put("disabilityType", typeId);
            	returnMap.put("disabilityCategory", disabilityCategory);
                returnMap.put("disabilityCategoryDesc", disabilityCategoryDesc);
            }
        } catch (Exception e) {
            if (trans != null) {
                TransUtils.rollback(trans);
            }
            e.printStackTrace();
            returnMap.put("success", "0");
            returnMap.put("error", String.valueOf(e));

        }

        return returnMap;
    }

	private Collection getInusredList(Long underwriteId) {
		Collection insuredLists = uwPolicyDS.findUwLifeInsuredEntitis(underwriteId);
		Collection result = new ArrayList();
		for (UwLifeInsuredVO insured : (List<UwLifeInsuredVO>) insuredLists) {
			Map<String,Object> displayInsuredDisability = uwHelper.getDisplayInsuredDisability(insured);
			if(displayInsuredDisability != null) {
				result.add(displayInsuredDisability);
			}
		}
		return result;
	}

	/**
	 * 是否存在ICF編碼
	 * @param disabilityCode
	 * @param insuredDisabilityLists
	 * @return
	 */
	private boolean isExistsICF(String disabilityCode, List<UwInsuredDisability> uwDisList) {
		boolean exists = false;
		for (UwInsuredDisability uwDis : uwDisList) {
			if (uwDis.getDisabilityType().equals(disabilityCode)) {
				if(uwDis.getUwInsuredDisabilityIcfs().size() > 0){
					exists = true;
				}			
			}
		}
		return exists;
	}
	
	private String[] getDeleteIcfs(Long underwriteId, Long policyId,Long insuredListId ,String[] DisabilityTypes, String disabilityClass, Long changeId, Long policyChgId){
		UwLifeInsuredVO uwLifeInsuredVO = uwPolicyDS.findUwLifeInsuredByListId(underwriteId, insuredListId);
		List<UwInsuredDisability> uwDisList = uwInsuredDisbilityDS.findByUwInsuredId(uwLifeInsuredVO.getUwListId());
        List<String> icfs = new ArrayList<String>();
		for (UwInsuredDisability uwDis : uwDisList) {
			String disabilityCode = uwDis.getDisabilityType();
			if(!Arrays.stream(DisabilityTypes).anyMatch(s -> s.equals(disabilityCode))){
				for(UwInsuredDisabilityIcf icf:uwDis.getUwInsuredDisabilityIcfs()){
					icfs.add(String.valueOf(icf.getUwIcfListId()));
				}	
			}
		}
		return icfs.toArray(new String[icfs.size()]);
	}
	
	private String[] getDeleteIcfs(UwLifeInsuredVO uwLife, Long changeId, Long policyChgId){
		List<String> icfList = new ArrayList<String>();
		List<UwInsuredDisability> uwDisList = uwInsuredDisabilityService.findByUwInsuredId(uwLife.getUwListId());
		for (UwInsuredDisability uwDis : uwDisList) {
			List<UwInsuredDisabilityIcf> uwIcfList = uwDis.getUwInsuredDisabilityIcfs();
			for (UwInsuredDisabilityIcf uwIcf : uwIcfList) {
				icfList.add(String.valueOf(uwIcf.getUwIcfListId()));
			}
		}
		return icfList.toArray(new String[icfList.size()]);
	}
}
