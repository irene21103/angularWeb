package com.ebao.ls.liaRoc.batch;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.scheduling.annotation.Scheduled;

import com.ebao.ls.pa.nb.batch.NBSpringCorntabTrigger;

public class LiaRocUpload107BatchTrigger extends NBSpringCorntabTrigger {

	private Logger logger = Logger.getLogger(getClass());

	@Resource(name = LiaRocUpload107Batch.BEAN_DEFAULT)
	private LiaRocUpload107Batch liaRocUpload107Batch;

	@Override
	@Scheduled(cron = "0 0 12 * * *") // 12:00 every day.
	protected void execute() {

		try {
			logger.info("公會107通報排程");
			//submit(1333021L);		
			if (super.isBatch()) {
				liaRocUpload107Batch.mainProcess();
			}
		} catch (Exception e) {
			logger.error(e);
		}
	}
}
