package com.ebao.ls.riskPrevention.ctrl;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.UserTransaction;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.foundation.commons.exceptions.ExceptionUtil;
import com.ebao.foundation.module.db.Trans;
import com.ebao.ls.pty.ds.DeptService;
import com.ebao.ls.pub.CodeCst;
import com.ebao.ls.pub.auth.AuthItemCst;
import com.ebao.ls.riskPrevention.bs.RiskCustomerLogService;
import com.ebao.ls.riskPrevention.bs.RiskCustomerService;
import com.ebao.ls.riskPrevention.bs.impl.RiskCustomerAuthServiceImpl;
import com.ebao.ls.riskPrevention.ci.RiskCustomerCI;
import com.ebao.ls.riskPrevention.vo.RiskCustomerLogVO;
import com.ebao.ls.riskPrevention.vo.RiskCustomerVO;
import com.ebao.ls.uw.ci.UwChangeMsgCI;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.framework.GenericAction;
import com.ebao.pub.i18n.lang.StringResource;
import com.ebao.pub.util.BeanUtils;
import com.ebao.pub.util.StringUtils;
import com.tgl.tools.ListTool;

public class RiskCustomerModifyAction extends GenericAction {

    @Resource(name = RiskCustomerService.BEAN_DEFAULT)
    private RiskCustomerService riskCustomerService;

    @Resource(name = RiskCustomerLogService.BEAN_DEFAULT)
    private RiskCustomerLogService riskCustomerLogService;

    @Resource(name = RiskCustomerCI.BEAN_DEFAULT)
    RiskCustomerCI riskCustomerCI;

    @Resource(name = DeptService.BEAN_DEFAULT)
    DeptService deptService;

    @Resource(name = UwChangeMsgCI.BEAN_DEFAULT)
    UwChangeMsgCI uwChangeMsgCI;

    @Resource(name = RiskCustomerAuthServiceImpl.BEAN_DEFAULT)
    RiskCustomerAuthServiceImpl pageAuthService;
    
    private static final String QUERY_NEWEST_LIST_ID = "1";
    private static final String QUERY_NEWEST_UPDATE_TIME = "2";
    private static final String QUERY_BY_LIST_ID = "3";

    @Override
    public ActionForward process(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {

        // Modify and New should have Authentication
        boolean auth = pageAuthService.hasAccessRight(AuthItemCst.AUTH_ITEM_PA_RISK_CUSTOMER);

        RiskCustomerForm cForm = (RiskCustomerForm) form;
        request.setAttribute("currentUser",
                        deptService.getDeptById(AppContext.getCurrentUser().getDeptId()).getDeptName() +
                                        " - " + AppContext.getCurrentUser().getUserName());
        request.setAttribute("currentTime", AppContext.getCurrentUserLocalTime());
        request.setAttribute("auth", auth); // readOnly
        request.setAttribute("isNew", false);
        request.setAttribute("authError", false);
        request.setAttribute("certiCodeNotExist", false);
        request.setAttribute("dataErrorExist", false);

        if (CodeCst.YES_NO__YES.equals(cForm.getMainPOPup())) {
            request.setAttribute("mainPOPup", CodeCst.YES_NO__YES);
            request.setAttribute("detailPOPup", CodeCst.YES_NO__YES);
        } else if (CodeCst.YES_NO__YES.equals(cForm.getDetailPOPup())){
            request.setAttribute("detailPOPup", CodeCst.YES_NO__YES);
        } else {
            request.setAttribute("mainPOPup", CodeCst.YES_NO__YES.equals(request.getAttribute("mainPOPup")) ? CodeCst.YES_NO__YES : CodeCst.YES_NO__NO);
            request.setAttribute("detailPOPup", CodeCst.YES_NO__YES.equals(request.getAttribute("detailPOPup")) ? CodeCst.YES_NO__YES : CodeCst.YES_NO__NO);
        }

        // Modify Function need CertiCode
        if (StringUtils.isNullOrEmpty(cForm.getCertiCode())) {
            request.setAttribute("certiCodeNotExist", true);
            return mapping.findForward("success");
        }

        switch (cForm.getAction()) {
        // From other Modules Pop-ups //
            case LOAD: // For the Modify / readOnly Function in Pop-ups
                request.setAttribute("detailPOPup", CodeCst.YES_NO__YES);
                doSearch(cForm, request, QUERY_NEWEST_LIST_ID);
                break;

//            case UPDATE: // For the New / Update Function in Pop-ups
//                request.setAttribute("detailPOPup", CodeCst.YES_NO__YES);
            case NEW:
                if (auth) {
                    try {
                        RiskCustomerVO newVO = riskCustomerCI.newCustomerDetail(cForm.getCertiCode());
                        if (newVO == null) {
                            request.setAttribute("certiCodeNotExist", true);
                            return mapping.findForward("success");
                        } else {
                            RiskCustomerLogVO logVO = new RiskCustomerLogVO();
                            logVO = (RiskCustomerLogVO) BeanUtils.getBean(RiskCustomerLogVO.class, newVO);
                            cForm.setLog(logVO);
                            cForm.setVo(newVO);
                            request.setAttribute("riskNationalScore", CodeCst.YES_NO__YES.equals(cForm.getLog().getRiskNational()) ? "50" : "0");
                            request.setAttribute("voList", ListTool.toList(cForm.getVo()));
                            request.setAttribute("isNew", true);
                            return mapping.findForward("success");
                        }
                    } catch (Exception e) {
                        request.setAttribute("dataErrorExist", true);
                        return mapping.findForward("success");
                    }
                } else {
                    request.setAttribute("authError", true);
                    return mapping.findForward("success");
                }

            // From Menu //
            case EDIT: // For Modify Function
                if (!auth) {
                    request.setAttribute("authError", true);
                    return mapping.findForward("success");
                }
                doSearch(cForm, request, QUERY_NEWEST_LIST_ID);
                RiskCustomerVO newVO = riskCustomerCI.newCustomerDetail(cForm.getCertiCode(), cForm.getLog().getRiskFamous(), cForm.getLog().getRiskManual());
                cForm.setVo(newVO);
                cForm.setLog((RiskCustomerLogVO) BeanUtils.getBean(RiskCustomerLogVO.class, newVO));
                request.setAttribute("riskNationalScore", CodeCst.YES_NO__YES.equals(cForm.getLog().getRiskNational()) ? "50" : "0");
                request.setAttribute("voList", ListTool.toList(cForm.getVo()));
                request.setAttribute(RiskCustomerForm.RISK_CUSTOMER_LOG_ID, cForm.getLog().getListId());
                break;

            case SEARCH: // Just ReadOnly for the selected Option
                request.setAttribute("auth", Boolean.FALSE);
                doSearch(cForm, request, QUERY_BY_LIST_ID); // search by listId
                return mapping.findForward("success");

                // For all save //
            case SAVE: // do Modify Save
                if (auth) {
                    // FieldNumber有Bug
                    cForm.getVo().setRiskManual(Integer.valueOf(request.getParameterValues("riskManual")[0]));
                    UserTransaction ut = Trans.getUserTransaction();
                    RiskCustomerLogVO newLog = null;
                    try {
                        ut.begin();
                        Boolean pass  = hasModifyValue(cForm.getVo());
                        if(pass){
                            newLog = saveToDB(cForm.getVo());
                            ut.commit();
                        }else{
                            request.setAttribute(RiskCustomerForm.RETURN_MSG, StringResource.getStringData("MSG_1262284", CodeCst.LANGUAGE__CHINESE_TRAD));
                            ut.rollback();
                        }
                    } catch (Exception e) {
                        ut.rollback();
                        throw ExceptionUtil.parse(e);
                    }
                    if (!StringUtils.isNullOrEmpty(cForm.getPolicyId()) && newLog != null) {
                        // 核保訊息通知有人工修改紀錄
                        uwChangeMsgCI.prcUwChangeRiskMsg(Long.parseLong(cForm.getPolicyId()), newLog.getCertiCode(), newLog.getInsertTime());
                    }
                } else {
                    request.setAttribute("authError", true);
                    return mapping.findForward("success");
                }
                doSearch(cForm, request, QUERY_NEWEST_LIST_ID);
                break;
            default:
                break;
        }

        // SEARCH AREA
        return mapping.findForward("success");
    }

    private void doSearch(RiskCustomerForm cForm, HttpServletRequest request, String searchMode) throws Exception{
        if (cForm.getCertiCode() == null) {
            request.setAttribute("certiCodeNotExist", true);
        } else {
            if (QUERY_BY_LIST_ID.equals(searchMode)) {
                if (!StringUtils.isNullOrEmpty(cForm.getLogListId())) {
                    cForm.setLog(riskCustomerLogService.queryByListID(Long.parseLong(cForm.getLogListId())));
                } else {
                    request.setAttribute("dataErrorExist", true);
                }
            } else if (QUERY_NEWEST_LIST_ID.equals(searchMode)) {
                cForm.setLog(riskCustomerLogService.pageQuery(cForm.getCertiCode()));
            } else if (QUERY_NEWEST_UPDATE_TIME.equals(searchMode)) {
                cForm.setLog(riskCustomerLogService.queryNewestDateByCertiCode(cForm.getCertiCode()));
            }
            // query 資料 to VO
            if (cForm.getLog() != null) {
                cForm.setVo((RiskCustomerVO) BeanUtils.getBean(RiskCustomerVO.class, cForm.getLog()));
                request.setAttribute("riskNationalScore", CodeCst.YES_NO__YES.equals(cForm.getLog().getRiskNational()) ? "50" : "0");
                request.setAttribute("voList", ListTool.toList(cForm.getVo()));
            }
        }
    }

    private RiskCustomerLogVO saveToDB(RiskCustomerVO vo) {
        RiskCustomerLogVO newLog = new RiskCustomerLogVO();
        newLog = (RiskCustomerLogVO) BeanUtils.getBean(RiskCustomerLogVO.class, vo);

        newLog.setInsertBy(AppContext.getCurrentUser().getUserId());
        newLog.setUpdateBy(newLog.getInsertBy());
        newLog.setInsertTime(AppContext.getCurrentUserLocalTime());
        newLog.setUpdateTime(newLog.getInsertTime());
        newLog.setInsertTimestamp(newLog.getInsertTime());
        newLog.setUpdateTimestamp(newLog.getInsertTime());

        newLog.setUpdatedDeptID(AppContext.getCurrentUser().getDeptId());
        newLog.setUpdatedDeptName(deptService.getDeptById(newLog.getUpdatedDeptID()).getDeptName());

        riskCustomerService.saveOrUpdate(vo);
        newLog = riskCustomerLogService.save(newLog);

        return newLog;
    }
    
    private Boolean hasModifyValue(RiskCustomerVO vo){
        if(StringUtils.isEmpty(vo.getCertiCode()))
            throw new IllegalArgumentException("CertiCode is NULL");
        RiskCustomerLogVO logVO = riskCustomerLogService.queryNewestDateByCertiCode(vo.getCertiCode());
        /* 新資料 */
        if(logVO == null || logVO.getListId() == null)
            return Boolean.TRUE;
        /* 有修改紀錄 */
        if(logVO.getRiskNational() != null && !logVO.getRiskNational().equals(vo.getRiskNational()))//國家是否高風險, 地區風險
            return Boolean.TRUE;
        if(logVO.getRiskNational() == null && vo.getRiskNational() != null)//國家是否高風險, 地區風險
            return Boolean.TRUE;
        if(logVO.getRiskFamous() != null && !logVO.getRiskFamous().equals(vo.getRiskFamous()))//高知名度
            return Boolean.TRUE;
        if(logVO.getRiskFamous() == null && vo.getRiskFamous() != null)//高知名度
            return Boolean.TRUE;
        if( logVO.getRiskOIU() != null && vo.getRiskOIU() != null && (logVO.getRiskOIU().intValue() != vo.getRiskOIU().intValue())) //交易管道
            return Boolean.TRUE;
        if(logVO.getRiskProduct() != null && vo.getRiskProduct() != null && (logVO.getRiskProduct().intValue() != vo.getRiskProduct().intValue()))//產品風險
            return Boolean.TRUE;
        if(logVO.getRiskManual() != null && vo.getRiskManual() != null && (logVO.getRiskManual().intValue() != vo.getRiskManual().intValue())) //調整風險
            return Boolean.TRUE;
        
        return Boolean.FALSE;
    }
    
}