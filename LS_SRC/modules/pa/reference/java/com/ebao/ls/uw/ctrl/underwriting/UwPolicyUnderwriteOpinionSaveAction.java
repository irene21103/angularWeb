package com.ebao.ls.uw.ctrl.underwriting;

import java.util.Collection;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.UserTransaction;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.ls.pa.nb.bs.ProposalProcessService;
import com.ebao.ls.pa.pub.ci.CoverageCI;
import com.ebao.ls.pa.pub.ci.PolicyCI;
import com.ebao.ls.pa.pub.service.PolicyService;
import com.ebao.ls.pa.pub.vo.CoverageVO;
import com.ebao.ls.pa.pub.vo.PolicyVO;
import com.ebao.ls.uw.ctrl.ReqParamNameConstants;
import com.ebao.ls.uw.ctrl.UwGenericAction;
import com.ebao.ls.uw.ds.UwProcessService;
import com.ebao.ls.uw.ds.UwRiApplyService;
import com.ebao.ls.uw.ds.constant.UwSourceTypeConstants;
import com.ebao.ls.uw.ds.sp.CompleteUWProcessSp;
import com.ebao.ls.uw.util.Utils;
import com.ebao.ls.uw.vo.UwCommentVO;
import com.ebao.ls.uw.vo.UwPolicyVO;
import com.ebao.pub.framework.ExceptionFactory;
import com.ebao.pub.framework.GenericException;
import com.ebao.pub.util.Log;
import com.ebao.pub.util.Trans;
import com.ebao.pub.util.TransUtils;

/**
 * <p>
 * Title: GEL-UW
 * </p>
 * <p>
 * Description: Save selected proposal's status and returns to uw sharing pool
 * </p>
 * <p>
 * Copyright: Copyright (c) 2016
 * </p>
 * <p>
 * Company: ebaoTech Corporation
 * </p>
 * 
 * @author ChiaHui Su
 * @version 1.0
 * @since 2016-02-19
 */
public class UwPolicyUnderwriteOpinionSaveAction extends UwGenericAction {
  public static final String BEAN_DEFAULT = "/uw/PolicyUnderwriteOpinionSave";

  /**
   * Main method of the action which making following changes to the system: 1.
   * save current policy status; 2. exit underwriting policy UI and return to
   * sharing pool UI.
   * 
   * @param mapping
   * @param form
   * @param request
   * @param response
   * @return
   * @throws GenericException
   */
  @Override
  public ActionForward uwProcess(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
                      throws GenericException {
      try {
          Long underwriteId = Long.valueOf(request.getParameter("underwriteId"));
          // get the original underwriting policy
          UwPolicyVO uwPolicyVOFromDB = getUwPolicyDS().findUwPolicy(underwriteId);
          
          // 取得畫面上核保綜合意見欄結果
          List<UwCommentVO> optionList = helper
                          .getCommentLists(request);
          if (Log.isDebugEnabled(this.getClass())) {
              Log.info(UwPolicyUnderwriteOpinionSaveAction.class,
                              "vo is " + uwPolicyVOFromDB.toString());
          }

          UserTransaction userTransaction = Trans.getUserTransaction();
          try {
              userTransaction.begin();
              // 儲存畫面上核保綜合意見欄結果
              helper.saveUWComment(uwPolicyVOFromDB.getUnderwriteId(),
                              optionList);
              //getUwPolicyDS().updateUwPolicy(uwPolicyVOFromDB, true);
              
              userTransaction.commit();
              
              request.setAttribute("submitSuccess", true);
          } catch (Exception e) {
              TransUtils.rollback(userTransaction);
              throw ExceptionFactory.parse(e);
          }

          return mapping.findForward("display");
      } catch (Exception e) {
          throw ExceptionFactory.parse(e);
      }
  }
  
  @Resource(name = UwPolicySubmitActionHelper.BEAN_DEFAULT)
  private UwPolicySubmitActionHelper helper;
  
}

