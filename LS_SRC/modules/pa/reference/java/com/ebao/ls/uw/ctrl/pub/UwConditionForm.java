package com.ebao.ls.uw.ctrl.pub;

import com.ebao.pub.framework.GenericForm;

/**
 * 
 * <p>Title:Underwriting</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: eBaoTech Corporation</p>
 * @author Walter Huang
 * @since  Jun 27, 2005
 * @version 1.0
 */
public class UwConditionForm extends GenericForm {

  private java.lang.Long underwriteId = null;

  private java.lang.String conditionCode = "";

  private java.lang.Long policyId = null;

  private java.lang.Long conditionId = null;

  private java.lang.Long underwriterId = null;

  private java.lang.String descLang1 = "";

  private java.lang.String descLang2 = "";

  private java.lang.Long uwListId = null;

  /**
   * returns the value of the underwriteId
   *
   * @return the underwriteId
   */
  public java.lang.Long getUnderwriteId() {
    return underwriteId;
  }

  /**
   * sets the value of the underwriteId
   *
   * @param underwriteId the underwriteId
   */
  public void setUnderwriteId(java.lang.Long underwriteId) {
    this.underwriteId = underwriteId;
  }

  /**
   * returns the value of the conditionCode
   *
   * @return the conditionCode
   */
  public java.lang.String getConditionCode() {
    return conditionCode;
  }

  /**
   * sets the value of the conditionCode
   *
   * @param conditionCode the conditionCode
   */
  public void setConditionCode(java.lang.String conditionCode) {
    this.conditionCode = conditionCode;
  }

  /**
   * returns the value of the policyId
   *
   * @return the policyId
   */
  public java.lang.Long getPolicyId() {
    return policyId;
  }

  /**
   * sets the value of the policyId
   *
   * @param policyId the policyId
   */
  public void setPolicyId(java.lang.Long policyId) {
    this.policyId = policyId;
  }

  /**
   * returns the value of the conditionId
   *
   * @return the conditionId
   */
  public java.lang.Long getConditionId() {
    return conditionId;
  }

  /**
   * sets the value of the conditionId
   *
   * @param conditionId the conditionId
   */
  public void setConditionId(java.lang.Long conditionId) {
    this.conditionId = conditionId;
  }

  /**
   * returns the value of the underwriterId
   *
   * @return the underwriterId
   */
  public java.lang.Long getUnderwriterId() {
    return underwriterId;
  }

  /**
   * sets the value of the underwriterId
   *
   * @param underwriterId the underwriterId
   */
  public void setUnderwriterId(java.lang.Long underwriterId) {
    this.underwriterId = underwriterId;
  }

  /**
   * returns the value of the descLang1
   *
   * @return the descLang1
   */
  public java.lang.String getDescLang1() {
    return descLang1;
  }

  /**
   * sets the value of the descLang1
   *
   * @param descLang1 the descLang1
   */
  public void setDescLang1(java.lang.String descLang1) {
    this.descLang1 = descLang1;
  }

  /**
   * returns the value of the descLang2
   *
   * @return the descLang2
   */
  public java.lang.String getDescLang2() {
    return descLang2;
  }

  /**
   * sets the value of the descLang2
   *
   * @param descLang2 the descLang2
   */
  public void setDescLang2(java.lang.String descLang2) {
    this.descLang2 = descLang2;
  }

  /**
   * returns the value of the uwListId
   *
   * @return the uwListId
   */
  public java.lang.Long getUwListId() {
    return uwListId;
  }

  /**
   * sets the value of the uwListId
   *
   * @param uwListId the uwListId
   */
  public void setUwListId(java.lang.Long uwListId) {
    this.uwListId = uwListId;
  }

  /**
   * returns the value of the UwCondition
   *
   * @return the UwConditionForm
   */
  public UwConditionForm getValue() {
    UwConditionForm value = new UwConditionForm();
    value.setUnderwriteId(underwriteId);
    value.setConditionCode(conditionCode);
    value.setPolicyId(policyId);
    value.setConditionId(conditionId);
    value.setUnderwriterId(underwriterId);
    value.setDescLang1(descLang1);
    value.setDescLang2(descLang2);
    value.setUwListId(uwListId);
    return value;
  }

  /**
   * populates with the values of a UwConditionForm
   * @param value
   */
  public void populateWithValue(UwConditionForm value) {
    underwriteId = value.getUnderwriteId();
    conditionCode = value.getConditionCode();
    policyId = value.getPolicyId();
    conditionId = value.getConditionId();
    underwriterId = value.getUnderwriterId();
    descLang1 = value.getDescLang1();
    descLang2 = value.getDescLang2();
    uwListId = value.getUwListId();
  }
}
