package com.ebao.ls.uw.ctrl.extraloading;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import com.ebao.ls.pa.pub.ci.CalculatorCI;
import com.ebao.ls.pa.pub.ref.api.prd.ProductService;
import com.ebao.ls.pa.pub.service.PolicyService;
import com.ebao.ls.uw.ctrl.ActionUtil;
import com.ebao.ls.uw.ds.UwPolicyService;
import com.ebao.ls.uw.ds.sp.QueryUwDataSp;
import com.ebao.ls.uw.vo.UwExtraLoadingVO;
import com.ebao.ls.uw.vo.UwProductVO;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.framework.AppException;
import com.ebao.pub.framework.GenericException;
import com.ebao.pub.framework.MultiWarning;
import com.ebao.pub.framework.internal.spring.DSProxy;
import com.ebao.pub.util.DateUtils;

/**
 * added by hendry.xu to solve the same record in extra loading .
 * 
 * @author hendry.xu
 * @param warning
 * @param preInsuredId
 * @param request
 * @param isNewbiz
 */
public class ExtraLoadingActionHelper {
	
  public static final String BEAN_DEFAULT = "extraLoadingActionHelper";	
	
  /**
   * ERORR: EM表找不到
   */
  public static final long APP_CAL_RATE = 20520120017L;

  public void processSameRecordWarning(HttpServletRequest request,
      MultiWarning warning, Long preInsuredId, boolean isNewbiz)
      throws Exception {
    List extraLoading = ActionUtil.getExtraLoadings(request, isNewbiz,
        preInsuredId);
    UwExtraLoadingVO preVO;
    UwExtraLoadingVO currentVO;
    for (int i = 0; i < extraLoading.size() - 1; i++) {
      preVO = (UwExtraLoadingVO) extraLoading.get(i);
      for (int j = i + 1; j < extraLoading.size(); j++) {
        currentVO = (UwExtraLoadingVO) extraLoading.get(j);
        if (currentVO.getExtraArith().equals(preVO.getExtraArith())
            && currentVO.getExtraType().equals(preVO.getExtraType())
            && currentVO.getEndDate().compareTo(preVO.getEndDate()) == 0
            && preVO.getStartDate().compareTo(currentVO.getStartDate()) == 0) {
          warning.setContinuable(false);
          warning.addWarning("ERR_20510020021"); //發現完全一樣的額外登錄
          break;
        }
      }
    }
  }

  /**
   * init the product vo
   * 
   * @param req
   * @return
   * @throws Exception
   */
  public UwProductVO initProductVO(HttpServletRequest req,
      Long underwriteId, Long itemId) throws Exception {
    UwPolicyService uwPolicyDS = (UwPolicyService) DSProxy
        .newInstance(UwPolicyService.class);
    UwProductVO uwProductVO = uwPolicyDS.findUwProduct(underwriteId, itemId);
    // set duration limit
    req.setAttribute("durationScope", QueryUwDataSp.getPremTerm(uwProductVO,policyService.getActiveVersionDate(uwProductVO)));
    // get the special product extraLoading ariths
    String[] ariths = getExtraAriths(Long.valueOf(uwProductVO.getProductId()
        .intValue()),policyService.getActiveVersionDate(uwProductVO));
    String arithsFlag = "";
    if (ariths.length == 0) {
      arithsFlag = "true";
    }
    String sqlAriths = "ADD_ARITH in (";
    for (int i = 0; i < ariths.length - 1; i++) {
      sqlAriths = sqlAriths + "'" + ariths[i] + "'" + ",";
    }
    if (!"true".equals(arithsFlag)) {
      sqlAriths = sqlAriths + "'" + ariths[ariths.length - 1] + "'" + ")";
    } else {
      sqlAriths = sqlAriths + "99)";
    }
    req.setAttribute("sqlAriths", sqlAriths);
    req.setAttribute("arithsFlag", arithsFlag);
    return uwProductVO;
  }

  /**
   * get the extraLoading arichs for specisal product
   * 
   * @param productId
   * @return
   * @throws GenericException
   */
  protected String[] getExtraAriths(Long productId, Date versionDate)
      throws GenericException {
    ProductService productService = (ProductService) com.ebao.pub.framework.internal.spring.DSProxy
        .newInstanceByBeanName("paProductService");
    List<String> extraAriths = productService.getProduct(productId, versionDate)
        .getExtraTypes();
    return (String[]) extraAriths.toArray(new String[extraAriths.size()]);
  }

  /**
   * calculate extra premium
   * 
   * @param productVO
   * @param gender
   * @param i
   * @throws GenericException
   */
  public void doCalExRate(UwProductVO productVO, String gender, int i,
      String strExtraArith, Long insuredId, boolean clearFlag)
      throws GenericException {
    try {
      BigDecimal emValue = new BigDecimal(productVO.getUwExtraLoadings().get(i)
          .getEmValue().intValue());
      Date startDate = productVO.getUwExtraLoadings().get(i).getStartDate();
      // calculate the extra rate
      BigDecimal rate;
      rate = CalculateEmRateForTimes(productVO, strExtraArith, insuredId,
          startDate, emValue, gender);
      if (rate != null) {
        productVO.getUwExtraLoadings().get(i).setExtraPara(rate.setScale(2));
      }
      if (clearFlag) {
        productVO.getUwExtraLoadings().get(i).setExtraPrem(null);
      }
    } catch (Exception e) {
      throw new AppException(APP_CAL_RATE);
    }
  }

  /**
   * just to calculate all types of emRate,not including % of annual premium
   * 
   * @param productVO
   * @param strExtraArith
   * @param insuredId
   * @param startDate
   * @param emValue
   * @param gender
   * @return
   * @throws GenericException
   */
  private BigDecimal CalculateEmRateForTimes(UwProductVO productVO,
      String strExtraArith, Long insuredId, Date startDate, BigDecimal emValue,
      String gender) throws GenericException {
    BigDecimal rate;
    CalculatorCI calculatorCI = (CalculatorCI) com.ebao.pub.framework.internal.spring.DSProxy
        .newInstanceByBeanName("paCalculatorCI");
    rate = calculatorCI.getExtraRate(productVO.getItemId(), emValue, startDate,
        insuredId, strExtraArith);
    return rate;
  }
  
  @Resource(name = PolicyService.BEAN_DEFAULT)
  PolicyService policyService;
}

