package com.ebao.ls.beneOwner.ctrl;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.foundation.core.logging.Log;
import com.ebao.ls.pty.ci.CustomerCI;
import com.ebao.ls.pty.ds.vo.CompanySearchConditionVO;
import com.ebao.ls.pty.ds.vo.PersonSearchConditionVO;
import com.ebao.ls.pub.CodeCst;
import com.ebao.pub.framework.GenericAction;

public class BeneOwnerCheckPartyAction extends GenericAction {

	private final Log log = Log.getLogger(BeneOwnerCheckPartyAction.class);

	@Resource(name = CustomerCI.BEAN_DEFAULT)
	private CustomerCI customerCI;

	@Override
	public ActionForward process(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		BeneOwnerCheckPartyForm boForm = (BeneOwnerCheckPartyForm) form;

		String beneOwnerCode = request.getParameter("beneOwnerCode");
		String isCompany = request.getParameter("isCompany");
		if (CodeCst.YES_NO__YES.equals(isCompany)) {
			CompanySearchConditionVO conditionVo = new CompanySearchConditionVO();
			conditionVo.setRegisterCode(beneOwnerCode);
			boForm.setCompanyCustomerVOList(customerCI.getCompanyList(conditionVo));
		} else {
			PersonSearchConditionVO conditions = new PersonSearchConditionVO();
			conditions.setCertiCode(beneOwnerCode);
			boForm.setCustomerVOList(customerCI.getPersonList(conditions));
		}
		return mapping.findForward("success");
	}
}
