package com.ebao.ls.callout.batch.impl;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;

import com.ebao.foundation.commons.exceptions.ExceptionUtil;
import com.ebao.foundation.commons.exceptions.GenericException;
import com.ebao.foundation.core.logging.Log;
import com.ebao.foundation.module.para.Para;
import com.ebao.ls.batch.file.AbstractFileUploadBatchJob;
import com.ebao.ls.batch.file.filehelper.FileGenericHelper;
import com.ebao.ls.callout.batch.CalloutConstants;
import com.ebao.ls.callout.batch.CalloutDataExportService;
import com.ebao.ls.callout.batch.CalloutHelp;
import com.ebao.ls.callout.batch.CalloutXMLVO;
import com.ebao.ls.pub.cst.YesNo;
import com.ebao.ls.ws.vo.cmn.cmn170.CalloutBatchType1List;
import com.ebao.ls.ws.vo.cmn.cmn170.CalloutBatchType2List;
import com.ebao.ls.ws.vo.cmn.cmn170.CalloutBatchType3List;
import com.ebao.ls.ws.vo.cmn.cmn170.CalloutBatchType4List;
import com.ebao.ls.ws.vo.cmn.cmn170.CalloutBatchType5List;
import com.ebao.ls.ws.vo.cmn.cmn170.CalloutBatchType6List;
import com.ebao.pub.batch.help.BatchHelp;
import com.ebao.pub.batch.type.LogLevel;
import com.ebao.pub.batch.util.BatchLogUtils;
import com.ebao.pub.util.DateUtils;
import com.ebao.pub.util.EnvUtils;

public class CalloutDataExportServiceImpl  extends AbstractFileUploadBatchJob implements CalloutDataExportService{
    
    private Log log = Log.getLogger(CalloutDataExportServiceImpl.class);
    
    private static final String SERVICE_KEY = "INT1010";

    /** 系統別 */
    private static final String SYSTEM_CODE = "CMN";

    /** 服務名稱 */
    private static final String SERVICE_NAME = "calloutBatchEVoiceService";

    /** 控制檔檔名 */
    private static final String CONTROL_FILE_NAME = "calloutBatchEVoiceService";
    
    private java.util.Map<Long, CalloutXMLVO> fieldMap = new java.util.HashMap<Long, CalloutXMLVO>();
    
    public CalloutDataExportServiceImpl(){
        fieldMap.put(CalloutConstants.ZeroEightZeroOne , instanceCalloutBatchType6("080-1"));
        fieldMap.put(CalloutConstants.ZeroEightZeroTwo, instanceCalloutBatchType6("080-2"));
        fieldMap.put(CalloutConstants.ZeroEightZeroThree, instanceCalloutBatchType2("080-3"));
        fieldMap.put(CalloutConstants.ZeroEightZeroFour, instanceCalloutBatchType2("080-4"));
        fieldMap.put(CalloutConstants.ZeroEightZeroFive, instanceCalloutBatchType2("080-5"));
        fieldMap.put(CalloutConstants.ZeroEightZeroSixOne, instanceCalloutBatchType2("080-6-1"));
        fieldMap.put(CalloutConstants.ZeroEightZeroSixTwo, instanceCalloutBatchType6("080-6-2"));
        fieldMap.put(CalloutConstants.ZeroEightZeroSeven, instanceCalloutBatchType3("080-7"));
        fieldMap.put(CalloutConstants.ZeroEightZeroEight, instanceCalloutBatchType3("080-8"));
        fieldMap.put(CalloutConstants.ZeroEightZeroNine, instanceCalloutBatchType4("080-9"));
        fieldMap.put(CalloutConstants.ZeroEightZeroTen, instanceCalloutBatchType4("080-10"));
        fieldMap.put(CalloutConstants.ZeroEightZeroX, instanceCalloutBatchType1("080-X"));
        fieldMap.put(CalloutConstants.POSOne, instanceCalloutBatchType5Both("POS-1"));
        fieldMap.put(CalloutConstants.POSTwo, instanceCalloutBatchType5Both("POS-2"));
        fieldMap.put(CalloutConstants.POSThree, instanceCalloutBatchType5Insured("POS-3"));
        fieldMap.put(CalloutConstants.POSFour, instanceCalloutBatchType5Insured("POS-4"));
        fieldMap.put(CalloutConstants.POSFive, instanceCalloutBatchType5Insured("POS-5"));
    }
    
    private void syso(Object msg){
        log.info("[SYSOUT][BR-CMN-50353-1][電訪任務下載檔案]" + msg);
        BatchLogUtils.addLog(LogLevel.INFO, null, "[SYSOUT][BR-CMN-50353-1][電訪任務下載檔案]" + msg);
    }
    
    private void syso(String layout, Object... msg){
        syso(String.format(layout, msg));
    }
    
    @Override
    protected String getRootPath() {
        //String rootPath = EnvUtils.getSendToEVOICESharePath();
		//374909 e-Approval ITR-2001139_因應新客服系統上線，修改匯出檔案路徑
        String rootPath = EnvUtils.getSendToECPSharePath();
        //rootPath = "E:\\";
        return rootPath;
    }
    
    @Override
    protected String getServiceKey() {
        return SERVICE_KEY;
    }
    
    @Override
    public void feedMap(List<Map<String, Object>> mapList, Long typeId,  java.util.Date processDate) throws GenericException {

        try {
            init();
        } catch (Exception ex) {
            throw com.ebao.foundation.commons.exceptions.ExceptionUtil.parse(ex);
        }
        
        /* map to XML */
        java.util.List<java.util.Map<String,Object>> sampleList = new java.util.LinkedList<Map<String,Object>>();
        for(Map<String, Object> map : mapList){
            if(map.containsKey("IS_CALLOUT") && YesNo.YES_NO__YES.equals(MapUtils.getString(map, "IS_CALLOUT"))){
                sampleList.add(map);
            }
        }
        syso("[抽樣輸出筆數]:=" + sampleList.size());
        if(CollectionUtils.isEmpty(sampleList))
            return;
        final String xml = fieldMap.get(typeId).parse(sampleList);
        
        /* 輸出檔案名稱 */
        final String fileName = Para.getParaValue(typeId);
        final String newFileName = String.format(fileName, DateUtils.date2String(processDate, "yyyyMMdd"));
        final Long runId = BatchHelp.getRunId();

        try {
            write(xml, newFileName);
            syso(CONTROL_FILE_NAME + "_" + newFileName + " Ctrl Uploaded!");
        } catch (IOException e) {
            BatchLogUtils.addLog(runId, LogLevel.ERROR, null, xml);
            throw ExceptionUtil.parse(e);
        } catch (SQLException e) {
            BatchLogUtils.addLog(runId, LogLevel.ERROR, null, xml);
            throw ExceptionUtil.parse(e);
        } catch (ClassNotFoundException e) {
            BatchLogUtils.addLog(runId, LogLevel.ERROR, null, xml);
            throw ExceptionUtil.parse(e);
        }
            

    }
    
    private void write(String xml, String fileName) throws IOException, SQLException, ClassNotFoundException{
        /* 生成資料檔數據 */
        deleteTargetFile(fileName);
        List<String> lines = new ArrayList<String>();
        lines.add(xml);
        doUpload(fileName, lines, FileGenericHelper.ENCODING_UTF_8);
        lines.clear();
        /* 控制檔 */
        String fileType =  fileName.replace(".xml", "");
        doUploadControl(SYSTEM_CODE, SERVICE_NAME, CONTROL_FILE_NAME + "_" + fileType, FileGenericHelper.ENCODING_UTF_8, false);
    }
    
    private CalloutXMLVO instanceCalloutBatchType1(final String docketNum){
        return new CalloutXMLVO() {         
            @Override
            public String parse(java.util.List<Map<String, Object>> mapList) throws GenericException{
                CalloutBatchType1List output = CalloutHelp.convertToBatch1(docketNum, mapList);
                return com.tgl.tools.gen.JaxbTool.toXml(output);
            }
        };
    }
    
    private CalloutXMLVO instanceCalloutBatchType2(final String docketNum){
        return new CalloutXMLVO() {         
            @Override
            public String parse(java.util.List<Map<String, Object>> mapList) throws GenericException{
                CalloutBatchType2List output = CalloutHelp.convertToBatch2(docketNum, mapList);
                return com.tgl.tools.gen.JaxbTool.toXml(output);
            }
        };
    }
    
    private CalloutXMLVO instanceCalloutBatchType3(final String docketNum){
        return new CalloutXMLVO() {         
            @Override
            public String parse(java.util.List<Map<String, Object>> mapList) throws GenericException{
                CalloutBatchType3List output = CalloutHelp.convertToBatch3(docketNum, mapList);
                return com.tgl.tools.gen.JaxbTool.toXml(output);
            }
        };
    }
    
    private CalloutXMLVO instanceCalloutBatchType4(final String docketNum){
        return new CalloutXMLVO() {         
            @Override
            public String parse(java.util.List<Map<String, Object>> mapList) throws GenericException{
                CalloutBatchType4List output = CalloutHelp.convertToBatch4(docketNum, mapList);
                return com.tgl.tools.gen.JaxbTool.toXml(output);
            }
        };
    }
    
    private CalloutXMLVO instanceCalloutBatchType5Both(final String docketNum){
        return new CalloutXMLVO() {         
            @Override
            public String parse(java.util.List<Map<String, Object>> mapList)  throws GenericException{
                CalloutBatchType5List output = CalloutHelp.convertToBatchBoth(docketNum, mapList);
                return com.tgl.tools.gen.JaxbTool.toXml(output);
            }
        };
    }
    
    private CalloutXMLVO instanceCalloutBatchType5Insured(final String docketNum){
        return new CalloutXMLVO() {         
            @Override
            public String parse(java.util.List<Map<String, Object>> mapList)  throws GenericException{
                CalloutBatchType5List output = CalloutHelp.convertToBatchInsured(docketNum, mapList);
                return com.tgl.tools.gen.JaxbTool.toXml(output);
            }
        };
    }
    
    private CalloutXMLVO instanceCalloutBatchType6(final String docketNum){
        return new CalloutXMLVO() {         
            @Override
            public String parse(java.util.List<Map<String, Object>> mapList) throws GenericException{
                CalloutBatchType6List output = CalloutHelp.convertToBatch6(docketNum, mapList);
                return com.tgl.tools.gen.JaxbTool.toXml(output);
            }
        };
    }

    @Override
    protected boolean isClear() {
        return Boolean.FALSE;
    }
}
