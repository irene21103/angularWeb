package com.ebao.ls.crs.company;

import com.ebao.ls.crs.vo.CRSControlManVO;
import com.ebao.ls.foundation.service.GenericService;

public interface CRSControlManService extends GenericService<CRSControlManVO>{
	public final static String BEAN_DEFAULT = "crsControlManService";
	
	public abstract java.util.List<CRSControlManVO> findByCertiCode(String certiCode);
}
