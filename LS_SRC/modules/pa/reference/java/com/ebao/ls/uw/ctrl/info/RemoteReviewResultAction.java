package com.ebao.ls.uw.ctrl.info;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.foundation.core.logging.Log;
import com.ebao.ls.pa.nb.bs.RemoteReviewService;
import com.ebao.ls.pa.nb.ctrl.helper.RemoteHelper;
import com.ebao.ls.pa.pub.bs.PolicyService;
import com.ebao.ls.pa.pub.vo.PolicyProposalInfoVO;
import com.ebao.ls.pa.pub.vo.PolicyVO;
import com.ebao.ls.pa.pub.vo.RemoteReviewVO;
import com.ebao.ls.pub.CodeCst;
import com.ebao.pub.framework.GenericAction;

public class RemoteReviewResultAction extends GenericAction {
	private String forward = "display";

	private final Log log = Log.getLogger(RemoteReviewResultAction.class);

	@Resource(name = PolicyService.BEAN_DEFAULT)
	private PolicyService policyService;

	@Resource(name = RemoteReviewService.BEAN_DEFAULT)
	private RemoteReviewService remoteReviewService;

	@Resource(name = RemoteHelper.BEAN_DEFAULT)
	private RemoteHelper remoteHelper;

	@Override
	public ActionForward process(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String policyId = request.getParameter("policyId").toString();

		PolicyVO policy = policyService.load(Long.valueOf(policyId));
		PolicyProposalInfoVO proposalVO = policy.getProposalInfo();
		String remoteIndi = proposalVO.getRemoteIndi();
		String remoteCallout = proposalVO.getRemoteCallout();

		if (remoteHelper.isRemoteCalloutVideo(remoteIndi, remoteCallout)) {
			List<RemoteReviewVO> remoteReviewList = remoteReviewService.findByPolicyId(Long.valueOf(policyId));

			request.setAttribute("remoteReviewList", remoteReviewList);
			request.setAttribute("isRemoteCalloutVideo", CodeCst.YES_NO__YES);
		} else {
			request.setAttribute("isRemoteCalloutVideo", CodeCst.YES_NO__NO);
		}

		return mapping.findForward(forward);
	}

}
