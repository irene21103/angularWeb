package com.ebao.ls.uw.ctrl.lien;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ebao.ls.escape.helper.EscapeHelper;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.ls.uw.ctrl.UwGenericAction;
import com.ebao.ls.uw.ds.sp.CompleteUWProcessSp;
import com.ebao.ls.uw.util.Utils;
import com.ebao.ls.uw.vo.UwLienVO;
import com.ebao.ls.uw.vo.UwPolicyVO;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.framework.ExceptionFactory;
import com.ebao.pub.framework.GenericException;
import com.ebao.pub.util.DateUtils;
import com.ebao.pub.util.Log;

/**
 * <p>
 * Title:Underwriting
 * </p>
 * <p>
 * Description:
 * </p>
 * <p>
 * Copyright: Copyright (c) 2004
 * </p>
 * <p>
 * Company: eBaoTech Corporation
 * </p>
 * 
 * @author mingchun.shi
 * @since Jun 27, 2005
 * @version 1.0
 */
public class LienAction extends UwGenericAction {
  // flag of submit customer service's lien records
  private static final String SUBMITCSLIEN = "submit";
  // flag of submit new business's lien records
  private static final String SUBMITNBLIEN = "nb";
public static final String BEAN_DEFAULT = "/uw/uwLien";

  /**
   * Edit lien information into the corresponding proposal.
   */
  @Override
  public ActionForward uwProcess(ActionMapping mapping, ActionForm form,
      HttpServletRequest req, HttpServletResponse res) throws GenericException {
    try {
      UwLienForms formBean = (UwLienForms) form;
      String chargePeriod = req.getParameter("chargePeriod");
      // from the page lien.jsp getting the data of commenceDate
      formBean.setCommencementDate(DateUtils.toDate(req
          .getParameter("commenceDate")));
      // set the reduceAmout value
      if (formBean.getReducedAmount() != null) {
        calulateLienAmout(formBean, chargePeriod);
      }
      // from the page lien.jsp getting the data of underwriteId,itemId
      String underwriteId = req.getParameter("underwriteId");
      if (underwriteId != null) {
        formBean.setUnderwriteId(Long.valueOf(underwriteId));
      }
      String itemId = req.getParameter("itemId");
      if (itemId != null) {
        formBean.setItemId(Long.valueOf(itemId));
      }
      // from the page lien.jsp getting the data of flag to decide which act
      // to perform
      String action = req.getParameter("flag");
      java.util.Date maxDate;
      if (formBean.getConverYear() != null) {
        maxDate = DateUtils.addYear(formBean.getCommencementDate(), formBean
            .getConverYear().intValue());
      } else {
        maxDate = DateUtils.addYear(formBean.getCommencementDate(), 500);
      }
      // if (formBean != null) {
      // perform the flow to add the lien
      if ("true".equals(req.getParameter("lienDel"))) {
        delLien(formBean.getUnderwriteId(), formBean.getItemId());
      }
      // perform the flow to submit customer service lien
      else if (SUBMITCSLIEN.equalsIgnoreCase(action)) {
        if (formBean.getListId() != null) {
          //
          //
          for (int i = 0; i < formBean.getEmValue().length; i++) {
            UwLienVO reduceVO = new UwLienVO();
            if (formBean.getIsOn()[i].equals("Y")
                && formBean.getEmValue()[i] != null) {
              setCSVO(formBean, reduceVO, i, chargePeriod, req);
              // if (i < formBean.getEmValue().length - 1)]
              // 
            }
            if (formBean.getListId() != null
                && formBean.getListId()[i].trim().length() > 0) {
              if (formBean.getIsOn()[i].equals("Y")) {
                getUwPolicyDS().updateUwLien(reduceVO);
              } else {
                reduceVO.setUwListId(Long.valueOf(formBean.getListId()[i]));
                getUwPolicyDS().removeUwLien(reduceVO);
              }
            } else {
              if (formBean.getIsOn()[i].equals("Y")) {
                getUwPolicyDS().createUwLien(reduceVO);
              }
            }
          }
        }
      }
      // perform the flow to submit new business lien
      else if (SUBMITNBLIEN.equalsIgnoreCase(action)) {
        UwLienVO reduceVO = new UwLienVO();
        // set reduceVO
        setNBVO(formBean, reduceVO, req, chargePeriod);
        // if uwListId is null ,creating lien information
        if (reduceVO.getUwListId() == null) {
          getUwPolicyDS().createUwLien(reduceVO);
        }
        // if uwListId isnot null,updating lien information
        else {
          getUwPolicyDS().updateUwLien(reduceVO);
        }
      }
      // }
      // when cs-uw,synchronize records
      UwPolicyVO uwPolicyVO = getUwPolicyDS().findUwPolicy(
          formBean.getUnderwriteId());
      if (Utils.isCsUw(uwPolicyVO.getUwSourceType())) {
        CompleteUWProcessSp.synUWInfo4CS(uwPolicyVO.getPolicyId(), uwPolicyVO
            .getUnderwriteId(), uwPolicyVO.getChangeId(), Integer.valueOf(4));
      }
      formBean.setListId(null);
      // get the lien max date
      com.ebao.ls.encode.encoder.Encode.encodeForHTML(req).setAttribute("maxDate", maxDate);
      // set underwriteid
      req.setAttribute("underwriteId", formBean.getUnderwriteId());
      // set itemId
      req.setAttribute("itemId", new Long[]{formBean.getItemId()});
    } catch (Exception e) {
      throw ExceptionFactory.parse(e);
    }
    // ActionForward the page MakeDecision.jsp
    return mapping.findForward("product");
  }

  /**
   * set the customer service reduceVO
   * 
   * @param formBean UwLienForms
   * @param reduceVO UwLienVO
   * @param i int
   * @throws Exception
   */
  private void setCSVO(UwLienForms formBean, UwLienVO reduceVO, int i,
      String chargePeriod, HttpServletRequest req) throws Exception {
    // set underwriteId
    reduceVO.setUnderwriteId(formBean.getUnderwriteId());
    // set itemId
    reduceVO.setItemId(formBean.getItemId());
    // set emValue
    reduceVO.setEmValue(formBean.getEmValue()[i]);
    // set reduceAmount
    reduceVO.setReducedAmount(new java.math.BigDecimal((formBean
        .getReducedAmount()[i]).doubleValue()));
    // set lien type
    reduceVO.setLienType(formBean.getLienType()[i]);
    // set list id
    String listId[] = req.getParameterValues("policyListId");
    if (listId != null && listId.length > i && listId[i] != null
        && listId[i].trim().length() > 0) {
      reduceVO.setListId(Long.valueOf(listId[i]));
    }
    // set reduceRate
    if (formBean.getReduceRate()[i] != null) {
      if (!chargePeriod.equals("1"))
        reduceVO.setReduceRate(new java.math.BigDecimal((formBean
            .getReduceRate()[i]).doubleValue() / 100));
      else
        reduceVO.setReduceRate(new java.math.BigDecimal((formBean
            .getReduceRate()[i]).doubleValue()));
    } else
      reduceVO.setReduceRate(new BigDecimal(0));
    // modified end
    // set lien start date and end date
    // if the reduceStart date value exist
    if (formBean.getReduceStart()[i].trim().length() > 0) {
      reduceVO.setReduceStart(DateUtils.toDate(formBean.getReduceStart()[i]));
    } else {
      // if the record is first lien information
      if (formBean.getReduceStart().length == 1) {
        reduceVO.setReduceStart(AppContext.getCurrentUserLocalTime());
      } else {
        if (i - 1 >= 0) {
          reduceVO.setReduceStart(DateUtils
              .toDate(formBean.getReduceEnd()[i - 1]));
        } else {
          reduceVO.setReduceStart(AppContext.getCurrentUserLocalTime());
        }
      }
    }
    // if the reduceEnd date value exist
    if (formBean.getReduceEnd()[i].trim().length() > 0) {
      reduceVO.setReduceEnd(DateUtils.toDate(formBean.getReduceEnd()[i]));
    } else {
      // if (formBean.getReduceEnd().length == 1) {
      // reduceVO.setReduceEnd(DateUtils.addYear(AppContext.getCurrentUserLocalTime(),
      // formBean.getLienYear()[i].intValue()));
      // } else {
      // reduceVO.setReduceEnd(DateUtils.addYear(DateUtils.toDate(formBean.getReduceEnd()[i
      // - 1]), formBean.getLienYear()[i]
      // .intValue()));
      // }
      Log.info(LienAction.class, "reduceVO.getReduceStart()"
          + reduceVO.getReduceStart());
      // modify by hendry.xu to solve the problem of end more than one day.
      reduceVO.setReduceEnd(DateUtils.addDay(DateUtils.addYear(reduceVO
          .getReduceStart(), formBean.getLienYear()[i].intValue()), -1));
    }
    Log.info(LienAction.class, "reduceVO.getReduceEnd()"
        + reduceVO.getReduceEnd());
    // set uwListId
    if (formBean.getListId() != null
        && formBean.getListId()[i].trim().length() > 0) {
      reduceVO.setUwListId(Long.valueOf(formBean.getListId()[i]));
    }
  }

  /**
   * set new business undate reduceVO
   * 
   * @param formBean UwLienForms
   * @param reduceVO UwLienVO
   * @param req HttpServletRequest
   * @throws Exception
   */
  private void setNBVO(UwLienForms formBean, UwLienVO reduceVO,
      HttpServletRequest req, String chargePeriod) throws Exception {
    // set underwriteID
    reduceVO.setUnderwriteId(formBean.getUnderwriteId());
    // set emValue
    reduceVO.setEmValue(formBean.getEmValue()[0]);
    // set itemId
    reduceVO.setItemId(formBean.getItemId());
    // set lien type
    reduceVO.setLienType(formBean.getLienType()[0]);
    // set reducedAmount
    if (formBean.getReducedAmount()[0] != null) {
      reduceVO.setReducedAmount(new java.math.BigDecimal(formBean
          .getReducedAmount()[0].doubleValue()));
    }
    // set lien strat and end date
    if (formBean.getCommencementDate() == null) {
      reduceVO.setReduceStart(com.ebao.pub.framework.AppContext
          .getCurrentUserLocalTime());
      reduceVO.setReduceEnd(com.ebao.pub.framework.AppContext
          .getCurrentUserLocalTime());
    } else {
      reduceVO.setReduceStart(formBean.getCommencementDate());
      // modify by hendry.xu to fix the end day more one day.
      reduceVO.setReduceEnd(DateUtils.addDay(DateUtils.addYear(formBean
          .getCommencementDate(), formBean.getLienYear()[0].intValue()), -1));
    }
    // set uwlistId
    String s = req.getParameter("uwlistId");
    if (s != null && s.trim().length() > 0) {
      reduceVO.setUwListId(Long.valueOf(s.trim()));
    }
    if (!chargePeriod.equals("1") && formBean.getReduceRate() != null) {
      reduceVO.setReduceRate(new java.math.BigDecimal(
          formBean.getReduceRate()[0].doubleValue() / 100));
    } else {
      reduceVO.setReduceRate(new java.math.BigDecimal(
          formBean.getReduceRate()[0].doubleValue()));
    }
  }

  /**
   * set the reduceAmout value
   * 
   * @param form
   */
  private void calulateLienAmout(UwLienForms form, String chargePeriod) {
    Double[] value = new Double[form.getReduceRate().length];
    // Fix Unchecked Input for Loop Condition
    Integer loopTotal = form.getReduceRate().length;
    if (loopTotal >= Integer.MAX_VALUE) {
      loopTotal = Integer.MAX_VALUE;
    }
    for (int i = 0; i < loopTotal; i++) {
      if (form.getReduceRate()[i].doubleValue() > 1
          || !chargePeriod.equals("1")
          && form.getReduceRate()[i].doubleValue() == 1) {
        value[i] = new Double(form.getReduceRate()[i].doubleValue() / 100
            * form.getAmount().doubleValue());
      } else {
        value[i] = new Double(form.getReduceRate()[i].doubleValue()
            * form.getAmount().doubleValue());
      }
    }
    form.setReducedAmount(value);
  }

  /**
   * del the lien information
   * 
   * @param underwriteId
   * @param itemId
   * @throws Exception
   */
  private void delLien(Long underwriteId, Long itemId) throws Exception {
    if (!getUwPolicyDS().findUwLienEntitis(underwriteId, itemId).isEmpty()) {
      Collection<UwLienVO> lienc = getUwPolicyDS().findUwLienEntitis(
          underwriteId, itemId);
      Iterator<UwLienVO> it = lienc.iterator();
      while (it.hasNext()) {
        UwLienVO vo = it.next();
        getUwPolicyDS().removeUwLien(vo);
      }
    }
  }
}