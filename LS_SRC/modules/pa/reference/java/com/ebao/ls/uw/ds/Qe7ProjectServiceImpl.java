package com.ebao.ls.uw.ds;

import java.util.List;

import com.ebao.foundation.commons.exceptions.GenericException;
import com.ebao.ls.foundation.service.GenericServiceImpl;
import com.ebao.ls.pa.nb.data.bo.Qe7Project;
import com.ebao.ls.pa.nb.vo.Qe7ProjectVO;
import com.ebao.ls.uw.data.Qe7ProjectDao;

public class Qe7ProjectServiceImpl extends GenericServiceImpl<Qe7ProjectVO, Qe7Project, Qe7ProjectDao> implements Qe7ProjectService {

	@Override
	protected Qe7ProjectVO newEntityVO() {
		return new Qe7ProjectVO();
	}
	
	@Override
	public void uploadList(List<Qe7ProjectVO> voList) throws GenericException {
		for (Qe7ProjectVO vo:voList) {
			Qe7Project bo = new Qe7Project();
			bo.copyFromVO(vo, true, true);
			super.dao.save(bo);
		}
	}

	@Override
	public void removeAll() throws GenericException {
		super.dao.deleteAllRecord();
	}

}
