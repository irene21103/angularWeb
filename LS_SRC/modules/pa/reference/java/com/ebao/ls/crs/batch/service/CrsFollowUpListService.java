package com.ebao.ls.crs.batch.service;

import java.util.Date;

import com.ebao.foundation.commons.exceptions.GenericException;
import com.ebao.ls.fatca.data.bo.FatcaDueDiligenceList;

public interface CrsFollowUpListService {
	public final static String BEAN_DEFAULT = "crsFllowUpListService";        
    
    public void sendCrsLetter(FatcaDueDiligenceList data, Long reportId, Date processDate, String targetSurveyType) throws GenericException;
    
    public void sendCrsLetterPH(FatcaDueDiligenceList data, Date processDate) throws GenericException;
    
    public FatcaDueDiligenceList parseMap(java.util.Map<String, Object> map, String surveyType) throws GenericException;
    
    public FatcaDueDiligenceList parseMapPH(java.util.Map<String, Object> map, String surveyType) throws GenericException;
    
}
