package com.ebao.ls.uw.ctrl.ri;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.Factory;
import org.apache.commons.collections.ListUtils;

import com.ebao.ls.uw.ctrl.letter.UNBLetterForm;

/**
 * Title: Policy Management <br>
 * Description: Detail Reg action form <br>
 * Copyright: Copyright (c) 2004 <br>
 * Company: eBaoTech Corporation <br>
 * Create Time: 2008/11/06 <br>
 * 
 * @author lucky.xie
 * @version 1.0
 */
public class UwRiApplyInsuredForm  extends UNBLetterForm {

	private static final long serialVersionUID = -3017001816871244849L;

	/**
	 * @return Returns the trusteePartyType.
	 */

	
	/* 參數說明:  1. 臨分   2. 諮詢 */
	private String facCate;
	
	private Long listId;
	
	@SuppressWarnings("unchecked")
	private List<UwCoverageReinsureForm> coverageReinsurerList = ListUtils.lazyList(
            new ArrayList<UwCoverageReinsureForm>(), new Factory() {
                @Override
                public UwCoverageReinsureForm create() {
                    return new UwCoverageReinsureForm();
                }
            });;
	
	private String[] medInfo;
	private String medStatus;
	private String medStatusDesc;
	private String medInfoIndi;
	private String medHistory;
	private String medQuestion;
	private String medDisease;
	private String medOther;
	private String uwDecision;
	private String uwStandard;
	private String uwSecondary;
	private String uwDelay;
	private String uwRefuse;
	private String uwReason;
	private String uwDescr;
	
	private Long insuredId;
	private String isRi;

	public String[] getMedInfo() {
		return medInfo;
	}

	public void setMedInfo(String[] medInfo) {
		this.medInfo = medInfo;
	}

	public String getUwDecision() {
		return uwDecision;
	}

	public void setUwDecision(String uwDecision) {
		this.uwDecision = uwDecision;
	}

	public String getMedStatus() {
		return medStatus;
	}

	public void setMedStatus(String medStatus) {
		this.medStatus = medStatus;
	}

	public String getMedStatusDesc() {
		return medStatusDesc;
	}

	public void setMedStatusDesc(String medStatusDesc) {
		this.medStatusDesc = medStatusDesc;
	}

	public String getMedInfoIndi() {
		return medInfoIndi;
	}

	public void setMedInfoIndi(String medInfoIndi) {
		this.medInfoIndi = medInfoIndi;
	}

	public String getMedHistory() {
		return medHistory;
	}

	public void setMedHistory(String medHistory) {
		this.medHistory = medHistory;
	}

	public String getMedQuestion() {
		return medQuestion;
	}

	public void setMedQuestion(String medQuestion) {
		this.medQuestion = medQuestion;
	}

	public String getMedDisease() {
		return medDisease;
	}

	public void setMedDisease(String medDisease) {
		this.medDisease = medDisease;
	}

	public String getMedOther() {
		return medOther;
	}

	public void setMedOther(String medOther) {
		this.medOther = medOther;
	}

	public String getUwStandard() {
		return uwStandard;
	}

	public void setUwStandard(String uwStandard) {
		this.uwStandard = uwStandard;
	}

	public String getUwSecondary() {
		return uwSecondary;
	}

	public void setUwSecondary(String uwSecondary) {
		this.uwSecondary = uwSecondary;
	}

	public String getUwDelay() {
		return uwDelay;
	}

	public void setUwDelay(String uwDelay) {
		this.uwDelay = uwDelay;
	}

	public String getUwRefuse() {
		return uwRefuse;
	}

	public void setUwRefuse(String uwRefuse) {
		this.uwRefuse = uwRefuse;
	}

	public String getUwReason() {
		return uwReason;
	}

	public void setUwReason(String uwReason) {
		this.uwReason = uwReason;
	}

	public String getUwDescr() {
		return uwDescr;
	}

	public void setUwDescr(String uwDescr) {
		this.uwDescr = uwDescr;
	}

	public Long getInsuredId() {
		return insuredId;
	}

	public void setInsuredId(Long insuredId) {
		this.insuredId = insuredId;
	}

	public String getIsRi() {
		return isRi;
	}

	public void setIsRi(String isRi) {
		this.isRi = isRi;
	}
	
	public Long getListId() {
		return listId;
	}

	public void setListId(Long listId) {
		this.listId = listId;
	}

	public List<UwCoverageReinsureForm> getCoverageReinsurerList() {
		return coverageReinsurerList;
	}

	public void setCoverageReinsurerList(List<UwCoverageReinsureForm> coverageReinsurerList) {
		this.coverageReinsurerList = coverageReinsurerList;
	}

	public String getFacCate() {
		return facCate;
	}

	public void setFacCate(String facCate) {
		this.facCate = facCate;
	}

}
