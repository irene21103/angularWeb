package com.ebao.ls.uw.ctrl.underwriting;

import com.ebao.foundation.module.db.hibernate.HibernateSession3;
import com.ebao.foundation.module.web.action.ExceptionInfoUtils;
import com.ebao.ls.arap.pub.ci.CashCI;
import com.ebao.ls.arap.pub.ci.PremCI;
import com.ebao.ls.arap.vo.SealRecordEddaVO;
import com.ebao.ls.chl.service.AgentService;
import com.ebao.ls.cs.commonflow.ds.AlterationItemService;
import com.ebao.ls.cs.commonflow.ds.ApplicationService;
import com.ebao.ls.cs.commonflow.ds.task.CsTaskTransService;
import com.ebao.ls.cs.commonflow.vo.AlterationItemVO;
import com.ebao.ls.cs.commonflow.vo.ApplicationVO;
import com.ebao.ls.cs.logger.ApplicationLogger;
import com.ebao.ls.escape.helper.EscapeHelper;
import com.ebao.ls.fnd.bs.FundService;
import com.ebao.ls.fnd.ci.ChargeDeductionCI;
import com.ebao.ls.pa.nb.bs.DisabilityCancelDetailService;
import com.ebao.ls.pa.nb.bs.ProposalProcessService;
import com.ebao.ls.pa.nb.ci.ValidatorService;
import com.ebao.ls.pa.nb.ctrl.helper.RemoteHelper;
import com.ebao.ls.pa.nb.util.NBUtils;
import com.ebao.ls.pa.nb.vo.DisabilityCancelDetailVO;
import com.ebao.ls.pa.pub.bs.BeneficiaryService;
import com.ebao.ls.pa.pub.bs.ContractBeneFundService;
import com.ebao.ls.pa.pub.bs.InsuredService;
import com.ebao.ls.pa.pub.bs.InvestCashAccountService;
import com.ebao.ls.pa.pub.bs.NbPolicySpecialRuleService;
import com.ebao.ls.pa.pub.bs.NbTaskService;
import com.ebao.ls.pa.pub.bs.PolicyBankAuthService;
import com.ebao.ls.pa.pub.bs.PolicyHolderService;
import com.ebao.ls.pa.pub.bs.PolicyRoleHelper;
import com.ebao.ls.pa.pub.bs.SharedCashAccountService;
import com.ebao.ls.pa.pub.bs.TargetMaturityFundService;
import com.ebao.ls.pa.pub.bs.coverage.CoverageService;
import com.ebao.ls.pa.pub.ci.CoverageCI;
import com.ebao.ls.pa.pub.ci.PolicyCI;
import com.ebao.ls.pa.pub.data.bo.Edda;
import com.ebao.ls.pa.pub.data.org.EddaDao;
import com.ebao.ls.pa.pub.model.PolicyInfo;
import com.ebao.ls.pa.pub.ref.api.prd.ProductService;
import com.ebao.ls.pa.pub.service.PolicyService;
import com.ebao.ls.pa.pub.vo.BeneficiaryVO;
import com.ebao.ls.pa.pub.vo.ContractBeneFundVO;
import com.ebao.ls.pa.pub.vo.CoverageAgentVO;
import com.ebao.ls.pa.pub.vo.CoverageInsuredVO;
import com.ebao.ls.pa.pub.vo.CoveragePremium;
import com.ebao.ls.pa.pub.vo.CoverageVO;
import com.ebao.ls.pa.pub.vo.InsuredVO;
import com.ebao.ls.pa.pub.vo.InvestCashAccountVO;
import com.ebao.ls.pa.pub.vo.NbRiResultVO;
import com.ebao.ls.pa.pub.vo.PayPlanExtendVO;
import com.ebao.ls.pa.pub.vo.PayPlanVO;
import com.ebao.ls.pa.pub.vo.PayerAccountVO;
import com.ebao.ls.pa.pub.vo.PolicyBankAuthVO;
import com.ebao.ls.pa.pub.vo.PolicyHolderVO;
import com.ebao.ls.pa.pub.vo.PolicyVO;
import com.ebao.ls.pa.pub.vo.PremInvestRateVO;
import com.ebao.ls.pa.pub.vo.SharedCashAccountVO;
import com.ebao.ls.pa.pub.vo.TargetMaturityFundVO;
import com.ebao.ls.product.model.query.output.LifeProduct;
import com.ebao.ls.product.model.query.output.ProdBizCategory;
import com.ebao.ls.product.model.query.output.ProdBizCategorySub;
import com.ebao.ls.product.service.LifeProductCategoryService;
import com.ebao.ls.product.service.LifeProductService;
import com.ebao.ls.pty.ci.BankCI;
import com.ebao.ls.pty.ci.LIACI;
import com.ebao.ls.pty.data.bo.BankAccount;
import com.ebao.ls.pty.ds.BankAccountService;
import com.ebao.ls.pty.vo.BankAccountVO;
import com.ebao.ls.pty.vo.bank.BankOrgVO;
import com.ebao.ls.pub.CodeCst;
import com.ebao.ls.pub.util.BizUtils;
import com.ebao.ls.qry.ds.QryBaseVO;
import com.ebao.ls.qry.ds.policy.FinancialInfoNewVO;
import com.ebao.ls.qry.ds.policy.PolicyDS;
import com.ebao.ls.riskPrevention.bs.RiskCustomerService;
import com.ebao.ls.sc.vo.AgentChlVO;
import com.ebao.ls.uw.ctrl.ActionUtil;
import com.ebao.ls.uw.ctrl.ReqParamNameConstants;
import com.ebao.ls.uw.ctrl.ajax.UwExtraPremHelper;
import com.ebao.ls.uw.ctrl.decision.UwProductDecisionViewForm;
import com.ebao.ls.uw.ctrl.pub.UwConditionForm;
import com.ebao.ls.uw.ctrl.pub.UwEndorsementForm;
import com.ebao.ls.uw.ctrl.pub.UwExclusionForm;
import com.ebao.ls.uw.ctrl.pub.UwPolicyForm;
import com.ebao.ls.uw.data.TUwInsuredListDelegate;
import com.ebao.ls.uw.ds.UwCommentService;
import com.ebao.ls.uw.ds.UwInsuredDisabilityService;
import com.ebao.ls.uw.ds.UwPolicyService;
import com.ebao.ls.uw.ds.UwProcessService;
import com.ebao.ls.uw.ds.UwRiApplyService;
import com.ebao.ls.uw.ds.constant.Cst;
import com.ebao.ls.uw.ds.constant.UwDecisionConstants;
import com.ebao.ls.uw.ds.constant.UwExceptionConstants;
import com.ebao.ls.uw.ds.constant.UwSourceTypeConstants;
import com.ebao.ls.uw.ds.constant.UwStatusConstants;
import com.ebao.ls.uw.ds.sp.CompleteUWProcessSp;
import com.ebao.ls.uw.util.Utils;
import com.ebao.ls.uw.vo.UwCommentOptionVO;
import com.ebao.ls.uw.vo.UwCommentVO;
import com.ebao.ls.uw.vo.UwConditionVO;
import com.ebao.ls.uw.vo.UwEndorsementVO;
import com.ebao.ls.uw.vo.UwExclusionVO;
import com.ebao.ls.uw.vo.UwExtraLoadingVO;
import com.ebao.ls.uw.vo.UwLienVO;
import com.ebao.ls.uw.vo.UwLifeInsuredVO;
import com.ebao.ls.uw.vo.UwPolicyVO;
import com.ebao.ls.uw.vo.UwProductVO;
import com.ebao.pub.annotation.DataModeChgModifyPoint;
import com.ebao.pub.annotation.PrdAPIUpdate;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.framework.AppException;
import com.ebao.pub.framework.ExceptionFactory;
import com.ebao.pub.framework.GenericException;
import com.ebao.pub.framework.MultiWarning;
import com.ebao.pub.framework.SysException;
import com.ebao.pub.framework.internal.spring.DSProxy;
import com.ebao.pub.i18n.lang.CodeTable;
import com.ebao.pub.util.BeanUtils;
import com.ebao.pub.util.DateUtils;
import com.ebao.pub.util.StringUtils;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.transaction.SystemException;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.log4j.Logger;
import org.hibernate.LockMode;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.jfree.util.Log;

/**
 * <p>
 * Title:Underwriting Description: Copyright: Copyright (c) 2004 Company:
 * eBaoTech Corporation
 * 
 * @author david.shan
 * @since Time:Jun 26, 2005
 * @version 1.0
 */
public class UwPolicySubmitActionHelper {
	public static final String BEAN_DEFAULT = "uwPolicySubmitHelper";
        
        private Logger logger = Logger.getLogger(UwPolicySubmitActionHelper.class);
	
	/**
	 * Retrieve indicator from mixture
	 * 
	 * @param indicatorMixture Mixture
	 * @return Indicator
	 * @author jason.luo
	 * @since 11.22.2004
	 * @version 1.0
	 */
	String getIndicator(String indicatorMixture) {
		if (indicatorMixture.length() == indicatorMixture.indexOf(",") + 2) {
			return String.valueOf(indicatorMixture.charAt(indicatorMixture
					.indexOf(",") + 1));
		} else {
			// default value
			return "Y";
		}
	}

	/**
	 * Retrieve uwListId from mixture
	 * 
	 * @param indicatorMixture Mixture
	 * @return uwListId
	 * @author jason.luo
	 * @since 11.22.2004
	 * @version 1.0
	 */
	public String getUwListId(String indicatorMixture) {
		return indicatorMixture.substring(0, indicatorMixture.indexOf(","));
	}

	/**
	 * Retrieves underwriting comments from request and sets it into UwPolicyVO
	 * obj.
	 * 
	 * @param request HttpServeltRequest
	 * @param vo UwPolicyVO
	 */
	public void setUnderwritingComments(HttpServletRequest request, UwPolicyVO vo) {
	}

	/**
	 * Retrieve commencement date from request and sets it into the UwPolicyVO and
	 * make the corresponding reviewDate synchronizing if commencement date has
	 * been changed.
	 * 
	 * @param request
	 * @param uwPolicyVO
	 * @throws GenericException
	 * @deprecated TGL無使用
	 */
	@Deprecated
	public void setCommencementDate(HttpServletRequest request,
			UwPolicyVO uwPolicyVO) throws GenericException {
		Date currDate = AppContext.getCurrentUserLocalTime();
		if (ActionUtil.compareDate(currDate, uwPolicyVO.getValidateDate())) {
			uwPolicyVO.setBackdatingIndi("Y");
		} else {
			uwPolicyVO.setBackdatingIndi("N");
			uwPolicyVO.setValidateDate(currDate);
		}
		CompleteUWProcessSp.updateCommencedate(uwPolicyVO.getUnderwriteId(),
				uwPolicyVO.getValidateDate(), uwPolicyVO.getActualValidate(), "");
		if (UwSourceTypeConstants.NEW_BIZ.equals(uwPolicyVO.getUwSourceType())) {
			UwPolicyService uwPolicyDS = DSProxy.newInstance(UwPolicyService.class);
			Collection products = uwPolicyDS.findUwProductEntitis(uwPolicyVO
					.getUnderwriteId());
			Iterator productsIter = products.iterator();
			while (productsIter.hasNext()) {
				UwProductVO product = (UwProductVO) productsIter.next();
				product.setValidateDate(uwPolicyVO.getValidateDate());
				uwPolicyDS.updateUwProduct(product);
				shiftLienDate(product, uwPolicyVO.getUwSourceType());
				shiftLoadingDate(product, uwPolicyVO.getUwSourceType());
			}
		}
	}

	/**
	 * Shift lien date when NB UW
	 * 
	 * @param product UwProductVO
	 * @throws GenericException Application Exception
	 * @author jason.luo
	 * @since 11.20.2004
	 * @version 1.0
	 */
	public void shiftLienDate(UwProductVO product, String uwSourceType)
			throws GenericException {
		if (UwSourceTypeConstants.NEW_BIZ.equals(uwSourceType)) {
			UwPolicyService uwPolicyDS = DSProxy.newInstance(UwPolicyService.class);
			Collection liens = uwPolicyDS.findUwLienEntitis(
					product.getUnderwriteId(), product.getItemId());
			Iterator liensIter = liens.iterator();
			// get minimal start date
			Date minLienStartDate = getMinLienStartDate(liens);
			if (null != minLienStartDate) {
				double lienInterval = DateUtils.getDayAmount(product.getValidateDate(),
						minLienStartDate);
				while (liensIter.hasNext()) {
					UwLienVO lien = (UwLienVO) liensIter.next();
					lien.setReduceStart(DateUtils.addDay(lien.getReduceStart(),
							(int) lienInterval));
					lien.setReduceEnd(DateUtils.addDay(lien.getReduceEnd(),
							(int) lienInterval));
					uwPolicyDS.updateUwLien(lien);
				}
			}
		}
	}

	/**
	 * Shift extra loading date when NB UW
	 * 
	 * @param product UwProductVO
	 * @throws GenericException Application Exception
	 * @author jason.luo
	 * @since 11.20.2004
	 * @version 1.0
	 */
	void shiftLoadingDate(UwProductVO product, String uwSourceType)
			throws GenericException {
		UwPolicyService uwPolicyDS = DSProxy.newInstance(UwPolicyService.class);
		Collection loadings = uwPolicyDS.findUwExtraLoadingEntitis(
				product.getUnderwriteId(), product.getItemId());
		if (UwSourceTypeConstants.NEW_BIZ.equals(uwSourceType)) {
			Iterator loadingsIter = loadings.iterator();
			Date minLoadingStartDate = getMinLoadingStartDate(loadings);
			if (null != minLoadingStartDate) {
				double loadingInterval = DateUtils.getDayAmount(minLoadingStartDate,
						product.getValidateDate());
				while (loadingsIter.hasNext()) {
					UwExtraLoadingVO loading = (UwExtraLoadingVO) loadingsIter.next();
					loading.setStartDate((DateUtils.addDay(loading.getStartDate(),
							(int) loadingInterval)));
					loading.setEndDate((DateUtils.addDay(loading.getEndDate(),
							(int) loadingInterval)));
					uwPolicyDS.updateUwExtraLoading(loading);
				}
			}
		}
	}

	/**
	 * Get minimal extra loading start date
	 * 
	 * @param loadings Extra loading
	 * @return Minimal extra loading start date
	 * @author jason.luo
	 * @since 11.20.2004
	 * @version 1.0
	 */
	private Date getMinLoadingStartDate(Collection loadings) {
		Iterator iter = loadings.iterator();
		Date date = null;
		while (iter.hasNext()) {
			UwExtraLoadingVO loading = (UwExtraLoadingVO) iter.next();
			if (null == date) {
				date = loading.getStartDate();
				continue;
			}
			if (loading.getStartDate().before(date)) {
				date = loading.getStartDate();
			}
		}
		return date;
	}

	/**
	 * Get minimal lien start date
	 * 
	 * @param liens Liens
	 * @return Minimal lien start date
	 * @author jason.luo
	 * @since 11.20.2004
	 * @version 1.0
	 */
	private Date getMinLienStartDate(Collection liens) {
		Iterator iter = liens.iterator();
		Date date = null;
		while (iter.hasNext()) {
			UwLienVO lien = (UwLienVO) iter.next();
			if (null == date) {
				date = lien.getReduceStart();
				continue;
			}
			if (lien.getReduceStart().before(date)) {
				date = lien.getReduceStart();
			}
		}
		return date;
	}

	/**
	 * Update uw condition codes
	 * 
	 * @param request HttpServletRequest
	 * @param uwvo UwPolicyVO
	 * @throws GenericException Application Exception
	 * @author jason.luo
	 * @since 10.12.2004
	 */
	public void storeConditionCodes(HttpServletRequest request,
			UwPolicyVO uwPolicyVO) throws Exception {
		Long underwriteId = uwPolicyVO.getUnderwriteId();
		UwPolicyService uwPolicyDS = DSProxy.newInstance(UwPolicyService.class);
		// delete previous codes
		Collection codes = uwPolicyDS.findUwConditionEntitis(underwriteId);
		if (null == codes) {
			codes = new ArrayList();
		}
		Iterator iter = codes.iterator();
		while (iter.hasNext()) {
			uwPolicyDS.removeUwCondition((UwConditionVO) iter.next());
		}
		// add new codes
		String[] conditionCodes = request.getParameterValues("conditionCode");
		String[] lang1 = request.getParameterValues("conditionCodeDescLang1");
		String[] lang2 = request.getParameterValues("conditionCodeDescLang2");
		String[] conditionId = request.getParameterValues("conditionId");
		if (null != conditionCodes) {
			for (int i = 0; i < conditionCodes.length; i++) {
				if (!StringUtils.isNullOrEmpty(conditionCodes[i])) {
					UwConditionVO uwConditionVO = (UwConditionVO) BeanUtils.getBean(
							UwConditionVO.class, uwPolicyVO);
					uwConditionVO.setConditionCode(conditionCodes[i]);
					if (!"F".equals(conditionCodes[i])) {
						uwConditionVO.setDescLang1(null);
						uwConditionVO.setDescLang2(null);
					} else {
						if (!StringUtils.isNullOrEmpty(lang1[i])) {
							uwConditionVO.setDescLang1(lang1[i]);
						}
						if (lang2 != null && lang2.length > i
								&& !StringUtils.isNullOrEmpty(lang2[i])) {
							uwConditionVO.setDescLang2(lang2[i]);
						}
					}
					if (conditionId != null && i < conditionId.length
							&& !StringUtils.isNullOrEmpty(conditionId[i])) {
						uwConditionVO.setConditionId(Long.valueOf(conditionId[i]));
					}
					uwPolicyDS.createUwCondition(uwConditionVO);
				}
				// set condition code list
				request.setAttribute(
						UwPolicyMainAction.CONDITIONCODE_LIST,
						BeanUtils.copyCollection(UwConditionForm.class,
								uwPolicyDS.findUwConditionEntitis(underwriteId)));
			}// end codeMixins for loop
		}
	}

	/**
	 * Check product decision rules
	 * 
	 * @param products Products
	 * @param warning MultiWarning
	 */
	public void checkProductDecisionRule(Collection products, MultiWarning warning)
			throws GenericException {
		Iterator iter = products.iterator();
		List<CoverageVO> nbuVOs = null;
		while (iter.hasNext()) {
			UwProductVO product = (UwProductVO) iter.next();
			if (null == nbuVOs) {
				nbuVOs = coverageCI.findByPolicyId(product.getPolicyId());
			}
			if (isProductDecisionMade(product)) {
				int decision = product.getDecisionId().intValue();
				if (UwDecisionConstants.STANDARD_CASE_UNDERWRITTEN == decision) {
					// lien validation
					checkLienRule(warning, product);
					// loading validation
					// extraLoadingRule(warning, product);
					// restrict coverage validation
					checkRCRule(warning, nbuVOs, product);
				}
			}
		}
	}

	/**
	 * Check restrict coverage rule
	 * 
	 * @param warning
	 * @param nbuVOs
	 * @param product
	 */
	private void checkRCRule(MultiWarning warning, List<CoverageVO> nbuVOs,
			UwProductVO product) {
		// restrict coverage validation
		if (null != nbuVOs) {
			for (CoverageVO vo : nbuVOs) {
				if (vo.getItemId().equals(product.getItemId())) {
					if (null != product.getReducedAmount()
							&& product.getReducedAmount().compareTo(product.getAmount()) < 0) {
						warning.setContinuable(false);
						warning.addWarning("ERR_"
								+ UwExceptionConstants.APP_STAND_DEC_SUB_STAND_TERM_VIOLATION,
								"-RC-RSA-" + String.valueOf(product.getProductId()));
					}
				}
			}
		}
	}

	/**
	 * Check lien rule
	 * 
	 * @param warning
	 * @param product
	 * @throws GenericException
	 */
	private void checkLienRule(MultiWarning warning, UwProductVO product)
			throws GenericException {
		Collection liens = uwPolicyDS.findUwLienEntitis(product.getUnderwriteId(),
				product.getItemId());
		if (null != liens && 0 < liens.size()) {
			warning.setContinuable(false);
			warning.addWarning("ERR_"
					+ UwExceptionConstants.APP_STAND_DEC_SUB_STAND_TERM_VIOLATION, "-L-"
					+ String.valueOf(product.getProductId()));
		}
	}

	private boolean isDecisionValidate(Integer id) {
		if (null == id) {
			return false;
		} else {
			if (id.intValue() == UwDecisionConstants.CONDITIONAL_UNDERWRITTEN
					|| id.intValue() == UwDecisionConstants.STANDARD_CASE_UNDERWRITTEN) {
				return true;
			} else {
				return false;
			}
		}
	}

	public void productValidate(UwProductVO invalidProduct, Collection products,
			HashMap map, MultiWarning warning) {
		Iterator iter = products.iterator();
		while (iter.hasNext()) {
			UwProductVO upvo = (UwProductVO) iter.next();
			if (null != upvo.getMasterId()
					&& upvo.getMasterId().longValue() == invalidProduct.getItemId()
							.longValue()) {
				if (isDecisionValidate(upvo.getDecisionId())) {
					if (!map.containsKey(upvo)) {
						map.put(upvo, upvo);
						warning.setContinuable(false);
						warning.addWarning("ERR_"
								+ UwExceptionConstants.APP_PLAN_INVALIDATE,
								String.valueOf(upvo.getProductId()));
						UwProductVO newVO = new UwProductVO();
						newVO.setItemId(upvo.getItemId());
						newVO.setDecisionId(upvo.getDecisionId());
						newVO.setMasterId(upvo.getMasterId());
						productValidate(newVO, products, map, warning);
					}
				} else {
					if (isProductDecisionMade(upvo)) {
						productValidate(upvo, products, map, warning);
					}
				}
			}
		}
	}

	private boolean isProductDecisionMade(UwProductVO vo) {
		if (null == vo.getDecisionId()) {
			return false;
		}
		return true;
	}

	boolean isProductInvalidate(UwProductVO vo) {
		if (!isProductDecisionMade(vo)) {
			return false;
		} else {
			int decisionId = vo.getDecisionId().intValue();
			if (decisionId == UwDecisionConstants.POSTPONE
					|| decisionId == UwDecisionConstants.DECLINATION) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Determines whether the product applicable for prefer life ind checking *
	 * 
	 * @param product Product
	 * @author jason.luo
	 * @since 12.07.2004
	 * @version 1.0
	 */
	public boolean isApplicableForPerferLifeCheck(UwProductVO product) {
		if (null == product.getDecisionId()) {
			return false;
		} else {
			if (UwDecisionConstants.POSTPONE == product.getDecisionId().intValue()
					|| UwDecisionConstants.DECLINATION == product.getDecisionId()
							.intValue()) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Update uw endorsement codes
	 * 
	 * @param request HttpServletRequest
	 * @param uwvo UwPolicyVO
	 * @throws GenericException Application Exception
	 * @author jason.luo
	 * @since 10.12.2004
	 */
	@Deprecated
	public void storeEndorsementCodes(HttpServletRequest request,
			UwPolicyVO uwPolicyVO) throws Exception {
		
		//return by simon.huang 舊程式使用核保主頁面的批註資料寫入現行已移至批註頁面寫入
		if (true) {
			return;
		}
		// delete previous codes
		Long underwriteId = uwPolicyVO.getUnderwriteId();
		uwPolicyDS.removeUwEndorsementByUnderwriteId(underwriteId);
		// add new codes
		String[] endorsementCodes = request.getParameterValues("endorsementCode");
		String[] descLang1s = request.getParameterValues("endorsementLang1Des");
		String[] descLang2s = request.getParameterValues("endorsementLang2Des");
		String[] effectDate = request.getParameterValues("endorsementStartDate");
		String[] expiryDate = request.getParameterValues("endorsementEndDate");
		String[] endoId = request.getParameterValues("endorsementId");
		// get master product's itemId
		CoverageVO masterProductVO = coverageCI.getMainBenefitByPolicyId(uwPolicyVO
				.getPolicyId());
		if (null != endorsementCodes) {
			for (int i = 0; i < endorsementCodes.length; i++) {
				if (!StringUtils.isNullOrEmpty(endorsementCodes[i])) {
					UwEndorsementVO uwEndorsementVO = (UwEndorsementVO) BeanUtils
							.getBean(UwEndorsementVO.class, uwPolicyVO);
					uwEndorsementVO.setEndoCode(endorsementCodes[i]);
					// when endorsement code equals to 81,82,then set the
					// value,otherwise erase it
					if ("81".equals(endorsementCodes[i])
							|| "82".equals(endorsementCodes[i])) {
						if (!StringUtils.isNullOrEmpty(descLang1s[i])) {
							uwEndorsementVO.setDescLang1(descLang1s[i]);
						}
						if (descLang2s != null && i < descLang2s.length
								&& !StringUtils.isNullOrEmpty(descLang2s[i])) {
							uwEndorsementVO.setDescLang2(descLang2s[i]);
						}
					} else {
						uwEndorsementVO.setDescLang1(null);
						uwEndorsementVO.setDescLang2(null);
					}
					if (effectDate != null && i < effectDate.length
							&& !StringUtils.isNullOrEmpty(effectDate[i])) {
						uwEndorsementVO.setEffectDate(DateUtils.toDate(effectDate[i]));
					}
					if (expiryDate != null && i < expiryDate.length
							&& !StringUtils.isNullOrEmpty(expiryDate[i])) {
						uwEndorsementVO.setExpiryDate(DateUtils.toDate(expiryDate[i]));
					}
					uwEndorsementVO.setItemId(masterProductVO.getItemId());
					if (endoId != null && i < endoId.length
							&& !StringUtils.isNullOrEmpty(endoId[i])) {
						uwEndorsementVO.setEndorsementId(Long.valueOf(endoId[i]));
					}
					// for NB source
					if (CodeCst.UW_SOURCE_TYPE__NB.equals(uwPolicyVO.getUwSourceType())
							|| !StringUtils.isNullOrEmpty(endoId[i])) {
						uwEndorsementVO.setEndorsementVersion(Long
								.valueOf(CodeCst.ENDORSEMENT_VERSION__NB));
					} else {
						uwEndorsementVO.setEndorsementVersion(Long
								.valueOf(CodeCst.ENDORSEMENT_VERSION__CS));
					}
					// create the code
					uwPolicyDS.createUwEndorsement(uwEndorsementVO);
				}
			}// end codeMixins for loop
				// set endorsement code list
			request.setAttribute(
					UwPolicyMainAction.ENDORSEMENTCODE_LIST,
					BeanUtils.copyCollection(UwEndorsementForm.class,
							uwPolicyDS.findUwEndorsementEntitis(underwriteId)));
		}
	}

	/**
	 * store exclusion code
	 * @Deprecated by simon.huang on 2015-07-09 改由核保 保項批註管理 新/刪/修 (此舊有程式保留供參考用)
	 * @param request
	 * @param uwPolicyVO
	 * @throws Exception
	 */
	@Deprecated
	public void storeExclusionCodes(HttpServletRequest request,
			UwPolicyVO uwPolicyVO) throws Exception {

		//return by simon.huang on 2015-07-09
		if (true) {
			return;
		}
		// delete previous codes
		uwPolicyDS.removeUwExclusionsByUnderwriteId(uwPolicyVO.getUnderwriteId());
		// add new codes
		String[] exclusionCodes = request.getParameterValues("exclusionCode");
		String[] descLang1s = request.getParameterValues("exclusionCodeDescLang1");
		String[] descLang2s = request.getParameterValues("exclusionCodeDescLang2");
		String[] effectDate = request.getParameterValues("exclusionEffectDate");
		// Should use the parameter ->exclusionExpiryDate, changed to fix defect
		// GEL00039814. ning.qi 2008-02-27
		String[] expiryDate = request.getParameterValues("exclusionExpiryDate");
		String[] reviewPeriod = request.getParameterValues("reviewNum");
		String[] exclusionId = request.getParameterValues("exclusionId");
		String[] productId = request.getParameterValues("productId");
		String[] insuredId = request.getParameterValues("insuredId");
		if (null != exclusionCodes) {
			for (int i = 0; i < exclusionCodes.length; i++) {
				if (!StringUtils.isNullOrEmpty(exclusionCodes[i])) {
					UwExclusionVO uwExclusionVO = (UwExclusionVO) BeanUtils.getBean(
							UwExclusionVO.class, uwPolicyVO);
					uwExclusionVO.setExclusionCode(exclusionCodes[i]);
					if ("81".equals(exclusionCodes[i]) || "82".equals(exclusionCodes[i])
							|| "81X".equals(exclusionCodes[i])
							|| "FX".equals(exclusionCodes[i])) {
						if (!StringUtils.isNullOrEmpty(descLang1s[i])) {
							uwExclusionVO.setDescLang1(descLang1s[i]);
						}
						if (descLang2s != null && i < descLang2s.length
								&& !StringUtils.isNullOrEmpty(descLang2s[i])) {
							uwExclusionVO.setDescLang2(descLang2s[i]);
						}
					} else {
						uwExclusionVO.setDescLang1(null);
						uwExclusionVO.setDescLang2(null);
					}
					if (effectDate != null && i < effectDate.length
							&& !StringUtils.isNullOrEmpty(effectDate[i])) {
						uwExclusionVO.setEffectDate(DateUtils.toDate(effectDate[i]));
					} else {
						uwExclusionVO.setEffectDate(uwPolicyVO.getValidateDate());
					}
					if (expiryDate != null && i < expiryDate.length
							&& !StringUtils.isNullOrEmpty(expiryDate[i])) {
						uwExclusionVO.setExpiryDate(DateUtils.toDate(expiryDate[i]));
					}
					// review date should be effective date + review period
					uwExclusionVO.setReviewDate(Utils.getReviewDate(
							uwExclusionVO.getEffectDate(), reviewPeriod[i]));
					if (exclusionId != null && i < exclusionId.length
							&& !StringUtils.isNullOrEmpty(exclusionId[i])) {
						uwExclusionVO.setExclusionId(Long.valueOf(exclusionId[i]));
					}
					if (productId != null && i < productId.length
							&& !StringUtils.isNullOrEmpty(productId[i])) {
						uwExclusionVO.setProductId(Long.valueOf(productId[i]));
					}
					if (insuredId != null && i < insuredId.length
							&& !StringUtils.isNullOrEmpty(insuredId[i])) {
						uwExclusionVO.setInsuredId(Long.valueOf(insuredId[i]));
					}
					// create the code
					uwPolicyDS.createUwExclusion(uwExclusionVO);
				}
				// set exclusion code list
				request
						.setAttribute(UwPolicyMainAction.EXCLUSIONCODE_LIST, BeanUtils
								.copyCollection(UwExclusionForm.class, uwPolicyDS
										.findUwExclusionEntitis(uwPolicyVO.getUnderwriteId())));
			}// end codeMixins for loop
		}
	}

	/**
	 * @param request
	 * @param uwPolicyVO
	 * @param policyId
	 * @throws NamingException
	 * @throws SystemException
	 * @throws GenericException
	 */
	public void storePolicyInfo(HttpServletRequest request,
			UwPolicyVO uwPolicyVO, Collection products, Collection lifeInsureds)
			throws GenericException {
		// store some user to avoid being cast
		try {
			/*
			 * to ensure the condition code,exclusion code and endorsement code will
			 * be saved even when any error occurs.
			 */
			storeConditionCodes(request, uwPolicyVO);
			
			storeEndorsementCodes(request, uwPolicyVO);
			storeExclusionCodes(request, uwPolicyVO);
			storeUwPolicyValue(request, uwPolicyVO, products, lifeInsureds);
		} catch (Exception e) {
			throw ExceptionFactory.parse(e);
		}
	}

	/**
	 * @param request
	 * @param underwriteId
	 * @param uwPolicyVO
	 * @param products
	 * @param lifeInsureds
	 */
	public void storeUwPolicyValue(HttpServletRequest request,
			UwPolicyVO uwPolicyVO, Collection products, Collection lifeInsureds)
			throws Exception {
		// set the lock time to null
		uwPolicyVO.setLockTime(null);
		// update commence date
		Date validateDate = uwPolicyVO.getValidateDate();
		Long underwriteId = uwPolicyVO.getUnderwriteId();
		getUwPolicyDS().updateUwPolicy(uwPolicyVO, true);
		// update policy info
		String uwSourceType = uwPolicyVO.getUwSourceType();
		if (UwSourceTypeConstants.NEW_BIZ.equals(uwSourceType)) {
			Iterator productsIter = products.iterator();
			while (productsIter.hasNext()) {
				UwProductVO product = (UwProductVO) productsIter.next();
				product.setValidateDate(validateDate);
				product.setActualValidate(validateDate);
				product.setRiskCommenceDate(validateDate);
				shiftLienDate(product, uwPolicyVO.getUwSourceType());
				shiftLoadingDate(product, uwPolicyVO.getUwSourceType());
			}
		}
		/* set uw status as replied when policy is claimed */
		if (Utils.isClaimUw(uwSourceType)) {
			uwPolicyVO.setUwStatus(CodeCst.UW_STATUS__REPLIED);
		}
		// get standlife indicator & medicial billing indicator
		String[] uwDecisionMixins = request
				.getParameterValues(ReqParamNameConstants.BENEFIT_STAND_LIFE_IND_MIXIN);
		String[] uwMedicalIndiMixins = request
				.getParameterValues(ReqParamNameConstants.BENEFIT_UW_MEDICAL_MIXIN);
		if (null != uwDecisionMixins) {
			// retrieve benefit at here just for performance reason
			Iterator insuredIter = lifeInsureds.iterator();
			while (insuredIter.hasNext()) {
				UwLifeInsuredVO insuredVO = (UwLifeInsuredVO) insuredIter.next();
				// for standard life indicator
				for (int i = 0; i < uwDecisionMixins.length; i++) {
					if (String.valueOf(insuredVO.getUwListId()).equals(
							getUwListId(uwDecisionMixins[i]))) {
						String standLife = getIndicator(uwDecisionMixins[i]);
						insuredVO.setStandLife(standLife);
						Iterator productIter = products.iterator();
						while (productIter.hasNext()) {
							UwProductVO product = (UwProductVO) productIter.next();
							Long insuredId = insuredVO.getInsuredId();
							String oldStandLifeIndi = product.getStandLife1();
							// if stand life indicator has changed,store it in
							// lia
							Boolean actionCodeFlag = null;
							if ("N".equals(oldStandLifeIndi) && "Y".equals(standLife)) {
								actionCodeFlag = Boolean.valueOf(true);
							} else if ("Y".equals(oldStandLifeIndi) && "N".equals(standLife)) {
								actionCodeFlag = Boolean.valueOf(false);
							}
							if (actionCodeFlag != null) {
								liaCI.updateActionCodebyPartyAndPolicy(insuredId,
										uwPolicyVO.getPolicyCode(), actionCodeFlag);
							}
							if (insuredId.equals(product.getInsured1())) {
								product.setStandLife1(standLife);
							}
							// insured2 can be nullable
							else if (insuredId.equals(product.getInsured2())) {
								product.setStandLife2(standLife);
							}
						}// end of benefits loop
						break;// record found
					}
				}// end of decision mixture loop
					// for medical billing indicator
				if (null != uwMedicalIndiMixins) {
					for (int j = 0; j < uwMedicalIndiMixins.length; j++) {
						if (String.valueOf(insuredVO.getUwListId()).equals(
								getUwListId(uwMedicalIndiMixins[j]))) {
							String medicalExamIndi = getIndicator(uwMedicalIndiMixins[j]);
							insuredVO.setMedicalExamIndi(medicalExamIndi);
							Iterator productIter = products.iterator();
							while (productIter.hasNext()) {
								UwProductVO product = (UwProductVO) productIter.next();
								Long insuredId = insuredVO.getInsuredId();
								if (insuredId.equals(product.getInsured1())) {
									product.setMedicalFlag1(medicalExamIndi);
								} else if (insuredId.equals(product.getInsured2())) {
									product.setMedicalFlag2(medicalExamIndi);
								}
							}// end of benefits loop
							break;// record found
						}
					}
				}
				/* to store product info */
				Iterator iter = products.iterator();
				UwProductVO uwProductVO = null;
				while (iter.hasNext()) {
					uwProductVO = new UwProductVO();
					BeanUtils.copyProperties(uwProductVO, iter.next());
					uwPolicyDS.updateUwProduct(uwProductVO);
				}
				uwPolicyDS.updateUwLifeInsured(insuredVO);
				Collection insuredLists = uwPolicyDS
						.findUwLifeInsuredEntitis(underwriteId);
				request.setAttribute(UwPolicyMainAction.LIFE_ASSURED_LIST,
						getInsuredListForms(uwPolicyVO, insuredLists));
			}
		}
	}

	/**
	 * Integration with party module, translates uwLifeInsuredVO into
	 * UwInsuredListDecisionViewForm with the supporting utility( CustomerVO) of
	 * party module
	 * 
	 * @param uwPolicyVO Collection
	 * @return Collection UwInsuredListDecisionViewForm collection
	 * @throws GenericException Application Exception
	 */
	public Collection getInsuredListForms(UwPolicyVO uwPolicyVO,
			Collection insuredLists) throws GenericException {

		Long underwriteId = uwPolicyVO.getUnderwriteId();
		// get isured information
		Iterator iter = insuredLists.iterator();
		Collection result = new ArrayList();
		while (iter.hasNext()) {
			UwLifeInsuredVO uwInsuredVO = (UwLifeInsuredVO) iter.next();
			UwInsuredListDecisionViewForm uwInsuredListForm = new UwInsuredListDecisionViewForm();
			try {
				BeanUtils.copyProperties(uwInsuredListForm, uwInsuredVO);
				// 2015-06-24 by sunny get Data by T_INSURED_LIST
				// CustomerVO customer =
				// customerCI.getPerson(insured.getInsuredId());
				InsuredVO insuredVO = insuredService.load(uwInsuredVO.getListId());
				if (insuredVO != null) {
					// set life insured's gender
					uwInsuredListForm.setGenderName(insuredVO.getGender());
					//國籍
					uwInsuredListForm.setNationality(insuredVO.getNationality());
					uwInsuredListForm.setJobClass(insuredVO.getJobClass());
					uwInsuredListForm.setOccupCate(insuredVO.getOccupCate());
					uwInsuredListForm.setJobClassPt(insuredVO.getJobClassPt());
					uwInsuredListForm.setJobCateIdPt(insuredVO.getJobCateIdPt());
					// IR 450287
					if (null == insuredVO.getJobClassFt() && null != insuredVO.getJobClass())
						uwInsuredListForm.setJobClassFt(insuredVO.getJobClass());
					else
						uwInsuredListForm.setJobClassFt(insuredVO.getJobClassFt());

					if (null == insuredVO.getJobCateIdFt() && null != insuredVO.getOccupCate())
						uwInsuredListForm.setJobCateIdFt(insuredVO.getOccupCate());
					else
						uwInsuredListForm.setJobCateIdFt(insuredVO.getJobCateIdFt());

					// set party id type
					if (null != insuredVO.getCertiType()) {
						uwInsuredListForm.setPartyIdType(String
								.valueOf(insuredVO.getCertiType()));
					}
					// set party id no
					uwInsuredListForm.setPartyIdNO(insuredVO.getCertiCode());
					// set life insured's name
					uwInsuredListForm.setLifeAssuredName(insuredVO.getName());
					uwInsuredListForm.setLifeAssuredRomanName(insuredVO.getRomanName());
					//是否標準體
					uwInsuredListForm.setStdLifeIndi(uwInsuredVO.getStandLife());
					// set entry age
					List<CoverageInsuredVO> list = insuredService
							.findCoverageInsuredListByInsuredId(insuredVO.getListId());
					if (list != null && list.size() > 0) {
						uwInsuredListForm.setEntryAge(list.get(0).getEntryAge());
					} else {
						/* 被保險人無設定險種，改由計算取得保險年齡。 */
						if (!NBUtils.isEmptyOrDummyDate(insuredVO.getBirthDate())
								&& !NBUtils.isEmptyOrDummyDate(uwPolicyVO.getApplyDate())) {
							Long entryAge = BizUtils.getAge(CodeCst.AGE_METHOD__ANBD,
									insuredVO.getBirthDate(),
									uwPolicyVO.getApplyDate());
							if (entryAge != null) {
								uwInsuredListForm.setEntryAge(entryAge.intValue());
							}
						}
					}
					//BMI
					if (insuredVO.getBmi() != null) {
						uwInsuredListForm.setBmi(new BigDecimal(insuredVO.getBmi()));
					}
					//是否體檢
					uwInsuredListForm.setMedicalExamIndi(uwInsuredVO.getMedicalExamIndi());
					//是否調閱影像
					uwInsuredListForm.setImageIndi(uwInsuredVO.getImageIndi());
					//身心障礙
					String disabilityType = uwInsuredVO.getDisabilityType();
					if("000".equals(disabilityType)){
						uwInsuredListForm.setDisabilityIndi("N");
					} else if(!StringUtils.isNullOrEmpty(disabilityType)){
						uwInsuredListForm.setDisabilityIndi("Y");
					} else{
						uwInsuredListForm.setDisabilityIndi("");
					}
					//內勤人員生調
					uwInsuredListForm.setHoLifeSurveyIndi(uwInsuredVO.getHoLifeSurveyIndi());

					//PCR_391319 新式公會-新增被保險人受監護宣告  20201030 by Simon
					uwInsuredListForm.setGuardianAncmnt(uwInsuredVO.getGuardianAncmnt());
					
					result.add(uwInsuredListForm);

				} else {
					// remove不存在被保險人
					uwInsuredListDao.remove(uwInsuredVO.getUwListId());
				}

			} catch (GenericException ge) {
				throw ExceptionFactory.parse(new SysException(
						UwExceptionConstants.SYS_CUSTOMER_RETRIEVE_FAILURE, ge));
			} catch (Exception e) {
				throw ExceptionFactory.parse(e);
			}

		}
		return result;
	}// end of method getInsuredListDecisionViewForms

	/**
	 * get proposal holder list from UwPolicy
	 * 
	 * @param PolicyHolder holder
	 * @return Collection
	 * @throws GenericException Application Exception
	 */
	public Collection getProposalList(PolicyHolderVO holder, UwPolicyVO uwPolicyVO)
			throws GenericException {
		Collection result = new ArrayList();
		HashMap map = new HashMap();
		// 2015-06-25 by sunny get Data by T_POLICY_HOLDER(not T_CUSTOMER)
		map.put("proposalName", holder.getName());
		map.put("proposalRomanName", holder.getRomanName());
		map.put("partyIdType", String.valueOf(holder.getCertiType()));
		map.put("certiCode", holder.getCertiCode());
		map.put("listId", holder.getListId());
		map.put("entryAge", holder.getApplicantAge());
		map.put("gender", holder.getGender());
		map.put("nationality", holder.getNationality());
		map.put("occupClass", holder.getOccupClass());
		map.put("occupCate", holder.getOccupCate());
		// TODO get VIP flag from cs
		map.put("vip", "N");

		// IR 450287
		if (null == holder.getJobCateIdFt() && null != holder.getOccupCate())
			map.put("jobCateIdFt", holder.getOccupCate());
		else
			map.put("jobCateIdFt", holder.getJobCateIdFt());

		if (null == holder.getJobClassFt() && null != holder.getOccupClass())
			map.put("jobCateClassFt", holder.getOccupClass());
		else
			map.put("jobCateClassFt", holder.getJobClassFt());

		map.put("jobCateIdPt", holder.getJobCateIdPt());
		map.put("jobCateClassPt", holder.getJobClassPt());
		
		//PCR_378267 保留核保決定當下之風險等級，後續其他單位或AML系統名單異動時不可覆蓋。
		String riskLevel = this.getRiskLevel(uwPolicyVO, holder.getCertiCode());
		map.put("riskLevel", riskLevel);
		result.add(map);
		return result;
	}// end of method getInsuredListDecisionViewForms
	
	/**
	 * <p>Description : 取得風險評級。</p>
	 * <p>Created By : Vince.Cheng</p>
	 * <p>Create Time : Aug 31, 2020</p>
	 * @param policyId 
	 * @return
	 */
	public String getRiskLevel(UwPolicyVO uwPolicyVO, String certiCode) {
		String riskLevel = uwPolicyVO.getUwRiskLevel();
		
		/*
		 * 如果新契約核保決定時有寫入uwPolicy.UwRiskLevel欄位，則使用新契約的值；
		 * 如果沒有值，則取aml log的資料。
		*/
		if (org.apache.commons.lang3.StringUtils.isBlank(riskLevel)) {
			riskLevel = riskCustomerService.queryAMLRiskLevelByCertiCode(certiCode);
		}
		
		return riskLevel;
	}

	/**
	 * get nominee list from UwPolicy
	 * 
	 * @param Long policyId
	 * @return Collection
	 * @throws GenericException Application Exception
	 */
	public Collection<HashMap<String, Object>> getNomineeList(
			List<BeneficiaryVO> beneficiaryList) throws GenericException {
		Collection<HashMap<String, Object>> result = new ArrayList<HashMap<String, Object>>();
		// get the nominee list by policyId
		// from t_benefit_insured
		// 2015-06-25 change by sunny get Data by T_CONTRACT_BENE(not
		// T_CUSTOMER)
		Iterator<BeneficiaryVO> iter = beneficiaryList.iterator();
		while (iter.hasNext()) {

			BeneficiaryVO bo = iter.next();

			if (bo.isInputBene()) {
				HashMap<String, Object> map = new HashMap<String, Object>();
				map.put("nomineeName", bo.getName());
				map.put("nomineeRomanName", bo.getRomanName());
				map.put("partyIdType", bo.getCertiType());
				// set party id no
				map.put("certiCode", bo.getCertiCode());
				// set nominee listId
				map.put("listId", bo.getListId());
				map.put("beneType", bo.getBeneType());
				map.put("designation", bo.getDesignation());
				map.put("shareOrder", bo.getShareOrder());
				if(bo.getShareRate() != null){
					map.put("shareRate", (bo.getShareRate().multiply(new BigDecimal(Double.valueOf("100")))));	
				}else{
					map.put("shareRate", null);
				}
				map.put("avgIndi", bo.getAvgIndi());
				if (bo.getInsuredId() != null) {
					InsuredVO insuredvo = insuredService.load(bo.getInsuredId());
					if (insuredvo != null) {
						map.put("lifeAssuredName", insuredvo.getName());
					}
				}
				result.add(map);
			}
		}
		return result;
	}// end of method getInsuredListDecisionViewForms

	public Collection<HashMap<String, Object>> getDisabilityList(
			Collection insuredLists, Collection insuredListForms) {
		HashMap<Long, String> nameMap = new HashMap<Long, String>();
		int size = insuredListForms.size();
		Object[] insuredListFormsArray = insuredListForms.toArray();
		for (int i = 0; i < size; i++) {
			UwInsuredListDecisionViewForm insuredListDecisionViewForm = (UwInsuredListDecisionViewForm) (insuredListFormsArray[i]);
			nameMap.put(insuredListDecisionViewForm.getListId(),
					insuredListDecisionViewForm.getLifeAssuredName());
		}
		Iterator iter = insuredLists.iterator();
		Collection result = new ArrayList();
		while (iter.hasNext()) {
			UwLifeInsuredVO insured = (UwLifeInsuredVO) iter.next();
			Map<String,Object> displayInsuredDisability = getDisplayInsuredDisability(insured);
			if(displayInsuredDisability != null) {
				result.add(displayInsuredDisability);
			}
			
		}
		return result;
	}

	public Map<String, Object> getDisplayInsuredDisability(UwLifeInsuredVO insured) {

		String disabilityType = insured.getDisabilityType();
		if (disabilityType != null && !"000".equals(disabilityType)) {
			HashMap<String, Object> map = new HashMap<String, Object>();
			List<String> types = uwInsuredDisbilityDS.getDisabilityTypes(insured.getUwListId());
			StringBuffer sb = new StringBuffer();
			boolean isFirst = true;
			for (String type : types) {
				if (isFirst) {
					isFirst = false;
				} else {
					sb.append(",");
				}
				sb.append(type + "-" + CodeTable.getCodeDesc("T_DISABILITY_TYPE", type, AppContext.getCurrentUser().getLangId()));
			}
			InsuredVO insuredVO = insuredService.load(insured.getListId());
			map.put("disabilityType", sb.toString());
			map.put("disabilityTypes", types);
			map.put("disabilityCategory", insured.getDisabilityCategory());
			map.put("disabilityClass", insured.getDisabilityClass());
			map.put("listId", insured.getListId());
			map.put("insuredName", insuredVO.getName());
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("policyId", insured.getPolicyId());
			params.put("insuredID", insured.getListId());
			params.put("sourceType", "UNB");

			List<DisabilityCancelDetailVO> disabilityCancelDetailList = disabilityCancelDetailService.find(params);
			Map<String, DisabilityCancelDetailVO> itemIdMap = NBUtils.toMap(disabilityCancelDetailList, "itemID");
			DisabilityCancelDetailVO disabilityCancelDetailVO = null;
			boolean isAllInput = true; //判斷存在保項若有其它一個未輸入,頁面就不顯示已輸入的末承保原因
			//找出存在的保項
			List<CoverageVO> coverageList = coverageService.findByPolicyId(insured.getPolicyId());
			for (CoverageVO coverageVO : coverageList) {
				Long insuredId = coverageVO.getLifeInsured1().getInsuredId();
				//只比對相同保險人的險種
				if(insuredId.equals(insured.getListId())) {
					String itemID = coverageVO.getItemId().toString();
					DisabilityCancelDetailVO item = itemIdMap.get(itemID);
					if(item == null && coverageVO.isWaiver()) {
						item = itemIdMap.get(""); //相容舊邏輯，豁免險展開前的舊資料
					}
					if (item == null) {
						//有保項未輸入
						isAllInput = false;
					}
					if (disabilityCancelDetailVO == null && item != null) {
						//抓取保項有輸入的未承保原因項目
						disabilityCancelDetailVO = item;
					}
					
				}
			}
			if (isAllInput == false) {
				disabilityCancelDetailVO = null;
			}

			if (disabilityCancelDetailVO != null) {
				//未承保原因類別1
				String reasonType1 = disabilityCancelDetailVO.getCancelReasonType1();
				map.put("cancelReasonType1", reasonType1);
				map.put("cancelReasonType1Desc", CodeTable.getCodeDesc("T_DISABILITY_CANCEL_TYPE1", reasonType1));
				//未承保原因類別2
				String reasonType2 = disabilityCancelDetailVO.getCancelReasonType2();
				map.put("cancelReasonType2", reasonType2);
				map.put("cancelReasonType2Desc", CodeTable.getCodeDesc("T_DISABILITY_CANCEL_TYPE2", reasonType2));
				//未承保原因項目
				map.put("cancelReason", disabilityCancelDetailVO.getCancelReason());
				//未承保原因項目新制身心障礙類別
				if (!StringUtils.isNullOrEmpty(disabilityCancelDetailVO.getDisabilityCategoryNewRule())) {
					map.put("cancelReason", disabilityCancelDetailVO.getDisabilityCategoryNewRule());
				}
			} else {
				//未承保原因類別1
				map.put("cancelReasonType1", "");
				map.put("cancelReasonType1Desc", "");
				//未承保原因類別2
				map.put("cancelReasonType2", "");
				map.put("cancelReasonType2Desc", "");
				//未承保原因項目
				map.put("cancelReason", "");
			}
			return map;
		}
		return null;
	}

	
	/**
	 * <p>Description : 取得核保綜合意見欄選項及結果</p>
	 * <p>Created By : Sunny Wu</p>
	 * <p>Create Time : Jun 29, 2015</p>
	 * @param underwriterId
	 * @return
	 */
	public Collection getCommentOptionList(
			Long underwriteId, Long commentVersionId) {
		Collection<UwCommentOptionVO> list = uwCommentService
				.getCommentOptionListByVersionId(commentVersionId);
		Collection<UwCommentVO> resultList = uwCommentService
				.getCommentResultByUnderwriteId(underwriteId);
		Map<Long, String> resultMap = new HashMap<Long, String>();
		for (UwCommentVO result : resultList) {
			resultMap.put(result.getOptionId(), result.getResultCode());
		}
		int index = 1;
		List<Map<String, Object>> returnList = new ArrayList<Map<String, Object>>();
		for (UwCommentOptionVO option : list) {

			option.setOptionText(index + "." + option.getOptionText());
			Map map = BeanUtils.getProperties(option);
			String resultCode = resultMap.get(option.getListId());
			if (resultCode != null) {
				map.put("resultCode", resultCode);
			}
			returnList.add(map);
			index++;
		}
		return returnList;
	}

	/**
	 * <p>Description : 取得再保公司列表</p>
	 * <p>Created By : Sunny Wu</p>
	 * <p>Create Time : Jun 29, 2015</p>
	 * @param underwriterId
	 * @return
	 */
	public Collection<HashMap<String, Object>> getRiCompList(
			Long underwriterId) {
		Collection result = new ArrayList();
		HashMap<String, Object> map = new HashMap<String, Object>();
		result.add(map);
		return result;
	}

	/**
	 * get scope of ma or ma factor
	 * 
	 * @param productVO
	 * @param req
	 * @throws GenericException
	 */
	public BigDecimal[] getFactorScope(UwProductVO productVO, Integer factorType,
			Collection insureds) throws GenericException {
		// get sa factor scope
		BigDecimal[] decimal;
		if (factorType.intValue() == 8) {
			factorType = Integer.valueOf(1);
		} else {
			factorType = Integer.valueOf(2);
		}
		String strStandLife = "";
		Iterator iter = insureds.iterator();
		while (iter.hasNext()) {
			UwLifeInsuredVO ulivo = (UwLifeInsuredVO) iter.next();
			if (ulivo.getInsuredId().longValue() == productVO.getInsured1()
					.longValue()) {
				strStandLife = ulivo.getStandLife();
				break;
			}
		}
		decimal = chargeDeductionCI.getFactorBound(
				Long.valueOf(productVO.getProductId().intValue()),
				String.valueOf(productVO.getPayMode()), strStandLife, factorType,
				productVO.getPolicyId());
		return decimal;
	}

	/**
	 * @param uwPolicyVO
	 * @param products
	 * @throws GenericException
	 */
	public void updatePolicyAndProduct(UwPolicyVO uwPolicyVO, Collection products)
			throws GenericException {
		uwPolicyDS.updateUwPolicy(uwPolicyVO, true);
		Iterator iter = products.iterator();
		while (iter.hasNext()) {
			UwProductVO product = (UwProductVO) iter.next();
			product.setRiskCommenceDate(uwPolicyVO.getActualValidate());
			uwPolicyDS.updateUwProduct(product);
		}
	}

	/**
	 * @param request
	 * @param uwPolicyVO
	 * @param products
	 * @throws Exception
	 * @deprecated 易保於理賠核保作業中使用,TGL理賠核保不由此進入,因此已無使用
	 */
	@DataModeChgModifyPoint("remove set method for Mix Conditions,Mix Exclusion, Mix Exclude ,no used")
	@PrdAPIUpdate
	@Deprecated
	public void setUwPolicyValueFromPage(HttpServletRequest request,
			UwPolicyVO uwPolicyVO, Collection products) throws GenericException {
		// set commencement date
		String strRenewValidateDate = request
				.getParameter(ReqParamNameConstants.POLICY_COMMENCEMENT_DATE_STRING);
		
		if (strRenewValidateDate != null && !"".equals(strRenewValidateDate.trim())) {
			Date renewValidateDate = toDate(strRenewValidateDate);
			if (renewValidateDate != null) {
				//續保件生效日與保單生效日不同才作設定
				if (!DateUtils.date2String(renewValidateDate).equals(
						DateUtils.date2String(uwPolicyVO.getValidateDate()))) {
					uwPolicyVO.setRenewValidateDate(renewValidateDate);
				}
			}
		}
		// set underwriting comment
		if (null != request
				.getParameter(ReqParamNameConstants.POLICY_REVERSAL_REASON)) {
			uwPolicyVO.setReversalReason(request
					.getParameter(ReqParamNameConstants.POLICY_REVERSAL_REASON));
		}
		if (null != request.getParameter(ReqParamNameConstants.POLICY_GIB_STATUS)
				&& request.getParameter(ReqParamNameConstants.POLICY_GIB_STATUS).trim()
						.length() > 0) {
			uwPolicyVO.setGibStatus(Integer.valueOf(request
					.getParameter(ReqParamNameConstants.POLICY_GIB_STATUS)));
		}
		if (null != request
				.getParameter(ReqParamNameConstants.POLICY_SPECIAP_COMM_INDI)) {
			uwPolicyVO.setSpecialCommIndi(request
					.getParameter(ReqParamNameConstants.POLICY_SPECIAP_COMM_INDI));
		}
		/*
		 * String[] conditions = request
		 * .getParameterValues(ReqParamNameConstants.POLICY_CONDITION_MIXIN);
		 * uwPolicyVO.setConditionCodeMixins(conditions); // add new codes String[]
		 * codeMixins = request
		 * .getParameterValues(ReqParamNameConstants.POLICY_ENDORSEMENT_MIXIN);
		 * uwPolicyVO.setEndorsCodeMixins(codeMixins); String[] exclusion = request
		 * .getParameterValues(ReqParamNameConstants.POLICY_EXCLUSION_MIXIN);
		 * uwPolicyVO.setExclusionCodeMixins(exclusion);
		 */
		// set underwriting comment
		if (null != request
				.getParameter(ReqParamNameConstants.POLICY_UNDERWRITING_COMMENT)) {
			uwPolicyVO.setUwNotes(request
					.getParameter(ReqParamNameConstants.POLICY_UNDERWRITING_COMMENT));
		}
		// set risk commencement date
		String riskCommendate = request
				.getParameter(ReqParamNameConstants.POLICY_RISK_COMMENCEMENT_DATE_STRING);
		if (riskCommendate != null
				&& riskCommendate.trim().length() > 0
				&& ActionUtil.compareDate(uwPolicyVO.getValidateDate(),
						toDate(riskCommendate))
				&& "Y".equals(request.getParameter("backdatingIndi"))) {
			Iterator productsIter = products.iterator();
			while (productsIter.hasNext()) {
				UwProductVO upvo = (UwProductVO) productsIter.next();
				if (null == upvo.getMasterId()) {
					if (this.getProductService()
							.getProductByVersionId(Long.valueOf(upvo.getProductId().intValue()), 
									upvo.getProductVersionId())
							.isBackdateIndi()) {
						throw new AppException(
								UwExceptionConstants.APP_COMPARE_RISK_AND_COMM);
					}
				}
			}
		}
		if (riskCommendate != null && !("").equals(riskCommendate.trim())) {
			uwPolicyVO.setActualValidate(toDate(riskCommendate));
			String riskBackDatingIndi = request
					.getParameter("riskBackdatingIndicator");
			uwPolicyVO.setRiskBackdatingIndi(riskBackDatingIndi);
		}
		String discountType = request
				.getParameter(ReqParamNameConstants.POLICY_DISCOUNT_TYPE);
		if (null == discountType || "".equals(discountType.trim())) {
			discountType = "0";
		}
		uwPolicyVO.setDiscountType(discountType);
		Iterator productsIter = products.iterator();
		while (productsIter.hasNext()) {
			UwProductVO upvo = (UwProductVO) productsIter.next();
			upvo.setDiscountType(discountType);
		}
		// set manul escalate information
		setManulEsc(request, uwPolicyVO);
		// update pending reason
		String pendingReason = request
				.getParameter(ReqParamNameConstants.POLICY_PENDING_REASON);
		if (pendingReason != null && !"".equals(pendingReason.trim())) {
			uwPolicyVO.setUwPending(Long.valueOf(pendingReason));
		} else {
			uwPolicyVO.setUwPending(null);
		}
		// set Generate LCA IND,Update LCA IND and Consent Given IND
		uwPolicyVO.setLcaDateIndi(request
				.getParameter(ReqParamNameConstants.POLICY_UPDATE_LCA_DATE_INDI));
		// set generate LCA indicator
		uwPolicyVO.setGenerateLcaIndi(request
				.getParameter(ReqParamNameConstants.POLICY_GENERATE_LCA_INDI));
		// set consent given indicator
		uwPolicyVO.setConsentGivenIndi(request
				.getParameter(ReqParamNameConstants.POLICY_CONSENT_GIVEN_INDI));
		// set spec comm indi
		uwPolicyVO.setSpecialCommIndi(request.getParameter("specialCommIndi"));
		uwProcessDS.setLCAIndsAndConsentGivenInd(uwPolicyVO);

		//uwPolicyVO.setUnderwriterId(ActionUtil.getUnderwriterId()); //任務初始已指定初始核保員,不需重新指定
		uwPolicyVO.setLastUnderwriterId(ActionUtil.getUnderwriterId());
		Date validateDate = uwPolicyVO.getValidateDate();
		Date currDate = AppContext.getCurrentUserLocalTime();
		if (ActionUtil.compareDate(currDate, validateDate)) {
			uwPolicyVO.setBackdatingIndi("Y");
		} else {
			uwPolicyVO.setBackdatingIndi("N");
			uwPolicyVO.setValidateDate(currDate);
		}
		// set APILP's info
		if (request.getParameter("apilpDesc") != null
				&& request.getParameter("apilpDesc").trim().length() != 0) {
			uwPolicyVO.setApilpDesc(request.getParameter("apilpDesc"));
		}
		if (request.getParameter("apilpMaximumLimit") != null
				&& request.getParameter("apilpMaximumLimit").trim().length() != 0) {
			uwPolicyVO.setApilpMaximumLimit(new BigDecimal(request
					.getParameter("apilpMaximumLimit")));
		}
		// 20130906, appeal reason is the appeal reason desc in current version.
		if (!StringUtils.isNullOrEmpty(request.getParameter("appealReason"))) {
			uwPolicyVO.setAppealReasonDesc(request.getParameter("appealReason"));
		}
		// 20130906, end
	}

	/**
	 * add by hanzhong.yan for cq:GEL00029240 refactor the method "public void
	 * setUwPolicyValueFromPage(HttpServletRequest request,UwPolicyVO uwPolicyVO,
	 * Collection products)" put value from page into UwPolicyVO object
	 * 
	 * @param request
	 * @param underwriteId
	 * @param uwPolicyVO
	 * @param isNewbiz
	 * @deprecated 原易保方法,TGL改使用 {@link #drawValueFromPage_TGL(HttpServletRequest, UwPolicyVO, Collection)}
	 */
	@DataModeChgModifyPoint("remove set method for Mix Conditions,Mix Exclusion, Mix Exclude ,no used")
	@Deprecated
	public void drawValueFromPage(HttpServletRequest request,
			UwPolicyVO uwPolicyVO, Collection products) throws GenericException {
		// set commencement date
		String strRenewValidateDate = request
				.getParameter(ReqParamNameConstants.POLICY_COMMENCEMENT_DATE_STRING);
		if (strRenewValidateDate != null && !"".equals(strRenewValidateDate.trim())) {
			Date renewValidateDate = toDate(strRenewValidateDate);
			if (renewValidateDate != null) {
				//續保件生效日與保單生效日不同才作設定
				if (!DateUtils.date2String(renewValidateDate).equals(
						DateUtils.date2String(uwPolicyVO.getValidateDate()))) {
					uwPolicyVO.setRenewValidateDate(renewValidateDate);
				} 
			}
		}

		if (null != request
				.getParameter(ReqParamNameConstants.POLICY_REVERSAL_REASON)) {
			uwPolicyVO.setReversalReason(request
					.getParameter(ReqParamNameConstants.POLICY_REVERSAL_REASON));
		}
		if (null != request
				.getParameter(ReqParamNameConstants.POLICY_GIB_STATUS)
				&& request.getParameter(ReqParamNameConstants.POLICY_GIB_STATUS).trim()
						.length() > 0) {
			uwPolicyVO.setGibStatus(Integer.valueOf(request
					.getParameter(ReqParamNameConstants.POLICY_GIB_STATUS)));
		}
		if (null != request
				.getParameter(ReqParamNameConstants.POLICY_SPECIAP_COMM_INDI)) {
			uwPolicyVO.setSpecialCommIndi(request
					.getParameter(ReqParamNameConstants.POLICY_SPECIAP_COMM_INDI));
		}
		/*
		 * String[] conditions = request
		 * .getParameterValues(ReqParamNameConstants.POLICY_CONDITION_MIXIN);
		 * uwPolicyVO.setConditionCodeMixins(conditions); // add new codes String[]
		 * codeMixins = request
		 * .getParameterValues(ReqParamNameConstants.POLICY_ENDORSEMENT_MIXIN);
		 * uwPolicyVO.setEndorsCodeMixins(codeMixins); String[] exclusion = request
		 * .getParameterValues(ReqParamNameConstants.POLICY_EXCLUSION_MIXIN);
		 * uwPolicyVO.setExclusionCodeMixins(exclusion);
		 */
		// set underwriting comment
		if (null != request
				.getParameter(ReqParamNameConstants.POLICY_UNDERWRITING_COMMENT)) {
			uwPolicyVO.setUwNotes(request
					.getParameter(ReqParamNameConstants.POLICY_UNDERWRITING_COMMENT));
		}
		// set risk commencement date
		String riskCommendate = request
				.getParameter(ReqParamNameConstants.POLICY_RISK_COMMENCEMENT_DATE_STRING);
		if (riskCommendate != null && !("").equals(riskCommendate.trim())) {
			uwPolicyVO.setActualValidate(toDate(riskCommendate));
			String riskBackDatingIndi = request
					.getParameter("riskBackdatingIndicator");
			uwPolicyVO.setRiskBackdatingIndi(riskBackDatingIndi);
		}
		String discountType = request
				.getParameter(ReqParamNameConstants.POLICY_DISCOUNT_TYPE);
		if (null == discountType || "".equals(discountType.trim())) {
			discountType = "0";
		}
		uwPolicyVO.setDiscountType(discountType);
		Iterator productsIter = products.iterator();
		while (productsIter.hasNext()) {
			UwProductVO upvo = (UwProductVO) productsIter.next();
			upvo.setDiscountType(discountType);
		}
		// set manul escalate information
		setManulEsc(request, uwPolicyVO);
		// update pending reason
		String pendingReason = request
				.getParameter(ReqParamNameConstants.POLICY_PENDING_REASON);
		if (pendingReason != null && !"".equals(pendingReason.trim())) {
			uwPolicyVO.setUwPending(Long.valueOf(pendingReason));
		} else {
			uwPolicyVO.setUwPending(null);
		}
		// set Generate LCA IND,Update LCA IND and Consent Given IND
		uwPolicyVO.setLcaDateIndi(request
				.getParameter(ReqParamNameConstants.POLICY_UPDATE_LCA_DATE_INDI));
		// set generate LCA indicator
		uwPolicyVO.setGenerateLcaIndi(request
				.getParameter(ReqParamNameConstants.POLICY_GENERATE_LCA_INDI));
		// set consent given indicator
		uwPolicyVO.setConsentGivenIndi(request
				.getParameter(ReqParamNameConstants.POLICY_CONSENT_GIVEN_INDI));
		// set spec comm indi
		uwPolicyVO.setSpecialCommIndi(request.getParameter("specialCommIndi"));
		// UwProcessDSDelegate.setLCAIndsAndConsentGivenInd(uwPolicyVO);
		if ("Y".equals(uwPolicyVO.getLcaDateIndi())) {
			uwPolicyVO.setLcaDate(AppContext.getCurrentUserLocalTime());
		}
		//uwPolicyVO.setUnderwriterId(ActionUtil.getUnderwriterId()); //任務初始已指定初始核保員,不需重新指定
		uwPolicyVO.setLastUnderwriterId(ActionUtil.getUnderwriterId());

		Date validateDate = uwPolicyVO.getValidateDate();
		Date currDate = AppContext.getCurrentUserLocalTime();
		if (ActionUtil.compareDate(currDate, validateDate)) {
			uwPolicyVO.setBackdatingIndi("Y");
		} else {
			uwPolicyVO.setBackdatingIndi("N");
			// uwPolicyVO.setValidateDate(currDate);
		}
		// set APILP's info
		if (request.getParameter("apilpDesc") != null
				&& request.getParameter("apilpDesc").trim().length() != 0) {
			uwPolicyVO.setApilpDesc(request.getParameter("apilpDesc"));
		}
		if (request.getParameter("apilpMaximumLimit") != null
				&& request.getParameter("apilpMaximumLimit").trim()
						.length() != 0) {
			uwPolicyVO.setApilpMaximumLimit(new BigDecimal(request
					.getParameter("apilpMaximumLimit")));
		}
	}

    /**
     * <p>Description : 是否透過eDDA核印(isNextPay=true 續期)</p>
     * <p>Created By : jeremy.zhu</p>
     * <p>Create Time : Mar 27, 2019</p>
     * @param policyId
     * @param isNextPay(isNextPay=true 續期)
     * @return
     */
    private String isEddaBankAuth(Long policyId, boolean isNextPay) {
        Map<String, PolicyBankAuthVO> bankAuthMap = policyBankAuthService.findMapByPolicyId(policyId);
        PolicyBankAuthVO bankAuthVO;
        if (isNextPay == false) {
            // 先取首期
            bankAuthVO = bankAuthMap.get(com.ebao.ls.pa.nb.util.Cst.SOURCETYPE_INIT_AUTH);
            if (bankAuthVO == null) {
                // 若首期空值，取首期同續期
                bankAuthVO = bankAuthMap.get(com.ebao.ls.pa.nb.util.Cst.SOURCETYPE_NEXT_EQUALS_INIT);
            }
        } else {
            // 取續期
            bankAuthVO = bankAuthMap.get(com.ebao.ls.pa.nb.util.Cst.SOURCETYPE_NEXT_AUTH);
        }

        if (bankAuthVO != null && bankAuthVO.getEddaPoolId() != null) {
            return CodeCst.YES_NO__YES;
        }
        return CodeCst.YES_NO__NO;
    }

    /**
     * <p>Description : 是否為eDDA約定書已檢附(isNextPay=true 續期)</p>
     * <p>Created By : jeremy.zhu</p>
     * <p>Create Time : Mar 27, 2019</p>
     * @param policyVO
     * @param isNextPay(isNextPay=true 續期)
     * @return
     */
    private String isBankPay(PolicyVO policyVO, boolean isNextPay) {
        CoverageVO coverage = policyVO.gitMasterCoverage();
        if (coverage != null) {
            CoveragePremium premiumVO = isNextPay ? coverage.getNextPremium() : coverage.getCurrentPremium();
            if (premiumVO != null && premiumVO.getPaymentMethod() != null) {
                int payMode = premiumVO.getPaymentMethod();
                if (CodeCst.PAY_MODE__GIRO == payMode) {
                    return CodeCst.YES_NO__YES;
                }
            }
        }
        return CodeCst.YES_NO__NO;
    }

    private PolicyVO loadBfInforcePolicy(Long policyId) {
        Map parmMap = new HashMap();
        parmMap.put("policyId", policyId);
        parmMap.put("serviceId", CodeCst.SERVICE__IN_FORCE_POLICY);
        Map orderMap = new HashMap();
        orderMap.put("policyChgId", "desc");
        List<AlterationItemVO> alterationItemVOs = alterationItemService.find(parmMap, orderMap);
        PolicyVO policy = null;

        if (alterationItemVOs != null) {
            Long policyChgId = alterationItemVOs.get(0).getPolicyChgId();
            policy = paPolicyService.findBfByPolicyIdAndPolicyChgId(policyId, policyChgId);
        }
        return policy;
    }

    /**
     * initialize comment, draw these value from workflow
     *
     * @param uwPolicyForm
     * @param uwPolicyVO
     */
    private void initializeComment(UwPolicyForm uwPolicyForm, UwPolicyVO uwPolicyVO) {
        Long policyId = uwPolicyVO.getPolicyId();

        List<Map<String, String>> commentsList = qryPolicyDS.qryCommentsData(policyId, false);

        if (CollectionUtils.isEmpty(commentsList)) {
            commentsList = qryPolicyDS.qryCommentsData(policyId, true);
        }

        for (Map<String, String> map : commentsList) {
            String name_ = map.get("NAME_");
            String comments = map.get("COMMENTS");
            if (ProposalProcessService.TASK_DATAENTRY.equals(name_)) {
                uwPolicyForm.setDetailRegComment(comments);
            } else if (ProposalProcessService.TASK_VERI.equals(name_)) {
                uwPolicyForm.setProofingComment(comments);
            }
        }

        // 影像備註
        uwPolicyForm.setImageRemakeComment(uwPolicyDS.getImageRemakeByPolicyId(policyId));

        // 電訪異常/回覆說明
        uwPolicyForm.setCalloutTransComment(uwPolicyDS.getCalloutMemoByPolicyId(policyId));

        // 覆核/主管核保意見
        uwPolicyForm.setUwComment(uwPolicyVO.getUwNotes());

        // 陳核備註說明
        uwPolicyForm.setProposeDesc(uwPolicyVO.getProposeDesc());

        // 核保特別說明
        uwPolicyForm.setCheckNote(uwPolicyVO.getCheckNote());

        // 核保人員核保意見
        uwPolicyForm.setReportNotes(uwPolicyVO.getReportNotes());

        //PCR364900-新契約補全建檔 
        //補全特別說明
        String complementTaskMemo = nbTaskServiceCI.getComplementTaskMemo(policyId);
        uwPolicyForm.setComplementTaskMemo(complementTaskMemo);
        
        //補全特別說明
        String complementTaskReviewMemo = nbTaskServiceCI.getComplementTaskReviewMemo(policyId);
        uwPolicyForm.setComplementTaskReviewMemo(complementTaskReviewMemo);

    }

    /**
     * Integration with party and other modules, translates UwProductVO into
     * UwProductDecisionViewForm with the supporting utility( CustomerVO) of party
     * module
     * 
     * @param productVOs Collection UwProductVO collection
     * @return Collection UwProductDecisionViewForm collection
     * @throws GenericException Application Exception
     */
    private Collection getUwProductDecisionViewForms(Collection productVOs) {
        // similar decision lies in method getInsuredListDecisionViewForms
        Collection result = new ArrayList();
        Iterator iter = productVOs.iterator();
        while (iter.hasNext()) {
            UwProductDecisionViewForm uwProductDecisionViewForm = new UwProductDecisionViewForm();
            UwProductVO uwProductVO = (UwProductVO) iter.next();
            BeanUtils.copyProperties(uwProductDecisionViewForm, uwProductVO);
            // 取得被保人姓名
            CoverageVO coverageVO = coverageService.load(uwProductVO.getItemId());
            List<String> lifeAssuredNames = new ArrayList<String>();
            // IR:130568
            if (coverageVO != null && CollectionUtils.isNotEmpty(coverageVO.getInsureds())) {
                List<CoverageInsuredVO> voList = coverageVO.getInsureds();
                for (CoverageInsuredVO vo : voList) {
                    if (vo != null) {
                        lifeAssuredNames.add(policyService.getInsuredByInsuredId(vo.getInsuredId()).getName());
                    }
                }
            }

            String benefitName = CodeTable.getCodeDesc("V_NB_PRODUCT_NAME_BY_VERSION", uwProductVO.getProductVersionId().toString());
            uwProductDecisionViewForm.setBenefitName(benefitName);

            uwProductDecisionViewForm.setLifeAssuredName(org.apache.commons.lang3.StringUtils.join(lifeAssuredNames, ","));

            if (uwProductDecisionViewForm.getPolicyFeeAn() != null) {
                uwProductDecisionViewForm.setTotalPremAn(uwProductDecisionViewForm
                        .getStdPremAn().add(uwProductDecisionViewForm.getPolicyFeeAn()));
            }
            if (uwProductDecisionViewForm.getExtraPremAn() != null) {
                uwProductDecisionViewForm.setTotalPremAn(uwProductDecisionViewForm
                        .getTotalPremAn().add(uwProductDecisionViewForm.getExtraPremAn()));
            }
            try {
                if (Integer.valueOf(CodeCst.PAY_MODE__INVEST_ACCOUNT_OFFER).equals(uwProductVO.getPayMode())
                        || Integer.valueOf(CodeCst.PAY_MODE__INVEST_ACC_BID).equals(uwProductVO.getPayMode())) {
                    uwProductDecisionViewForm.setTotalPremAn(null);
                }
            } catch (Exception ex) {
                throw ExceptionFactory.parse(ex);
            }

            String bigAmountDiscnt = coverageService.getLargeDiscountRate(uwProductVO.getPolicyId(), uwProductVO.getItemId());
            uwProductDecisionViewForm.setBigAmountDiscnt(bigAmountDiscnt);

            result.add(uwProductDecisionViewForm);
        }
        return result;
    }

    /**
     * 計算首期保費
     * <p>Description : </p>
     * <p>Created By : Sunny Wu</p>
     * <p>Create Time : Jun 25, 2015</p>
     * @param uwPolicyForm
     * @param uwProductDecisionViewForms
     * @param sourceType
     * @param policyVO
     */
    private void setPrem(UwPolicyForm uwPolicyForm, Collection uwProductDecisionViewForms, String sourceType, PolicyVO policyVO) {
        BigDecimal totalStdPrem = new BigDecimal("0");
        BigDecimal totalDiscntedPrem = new BigDecimal("0");
        for (Iterator iter = uwProductDecisionViewForms.iterator(); iter.hasNext();) {
            // artf137256 modify by simon.huang on 2015-07-28
            UwProductDecisionViewForm uwProductForm = (UwProductDecisionViewForm) iter.next();
            Long itemId = uwProductForm.getItemId();
            Long productId = new Long(uwProductForm.getProductId());

            //IR-292481 [PROD]核保主頁面標準保費未含XWB保費，參考加費頁面處理方式，修改標準保費為預設取T_CONTRACT_PRODUCT.STD_PREM_AF值，非豁免險才取TWPK1_100000000004263公式的值覆蓋掉STD_PREM_AF值
            BigDecimal stdPrem = BigDecimal.ZERO;
            for (CoverageVO coverageVO : policyVO.gitSortCoverageList()) {
                if (itemId.compareTo(coverageVO.getItemId()) == 0) {
                    stdPrem = coverageVO.getCurrentPremium().getStdPremAf();//期繳保費(無高保額折讓)

                    if (!coverageVO.isWaiver()) {// 非豁免
                        stdPrem = coverageService.getStdPremWithoutAnyDiscnt(itemId, productId);
                    }

                    break;
                }
            }

            totalStdPrem = totalStdPrem.add(stdPrem);
            totalDiscntedPrem = totalDiscntedPrem.add(stdPrem);
        }
        uwPolicyForm.setTotalStdPrem(totalStdPrem);
        uwPolicyForm.setTotalDiscntedPrem(totalDiscntedPrem);
        // 2016-10-5 Kate RTC#41578 當來源是保全核保，則首期表定保費
        // 要改取承保當時所產生的核心表資料紀錄檔
        if (Utils.isCsUw(sourceType)) {
            QryBaseVO qryBaseVO = new QryBaseVO();
            qryBaseVO.setPolicyId(uwPolicyForm.getPolicyId());
            FinancialInfoNewVO financialInfoNewVO = qryPolicyDS.qryFinancialInfo_new(qryBaseVO);

            uwPolicyForm.setTotalStdPrem(financialInfoNewVO.getFirst_install_prem());
        }
    }

    /**
     * 建構UwPolicyForm
     *
     * @param isIframe
     * @param uwPolicyForm
     * @param uwPolicyVO
     */
    public void constructUwPolicyForm(String isIframe, UwPolicyForm uwPolicyForm, UwPolicyVO uwPolicyVO) {
        if (uwPolicyForm != null && uwPolicyVO != null) {
            Long currentUserId = AppContext.getCurrentUser().getUserId();
            String sourceType = uwPolicyVO.getUwSourceType();
            Long policyId = uwPolicyVO.getPolicyId();
            Long underwriteId = uwPolicyVO.getUnderwriteId();
            try {
                PolicyVO policyVO;
                if (UwSourceTypeConstants.NEW_BIZ.equals(sourceType)) {
                    ApplicationLogger.addLoggerData("policyService.getProposalStatusByPolicyCode() begin");
                    int proposalStatus = policyService.getProposalStatusByPolicyCode(uwPolicyVO.getPolicyCode());
                    ApplicationLogger.addLoggerData("policyService.getProposalStatusByPolicyCode() end");
                    if (proposalStatus == CodeCst.PROPOSAL_STATUS__IN_FORCE) {
                        //保要書狀態為已生效，查生效前的保單資料
                        policyVO = loadBfInforcePolicy(policyId);
                    } else {
                        ApplicationLogger.addLoggerData("policyService.loadPolicyByPolicyId() begin");
                        policyVO = policyService.loadPolicyByPolicyId(policyId);
                        ApplicationLogger.addLoggerData("policyService.loadPolicyByPolicyId() end");
                    }
                } else {
                    //保全查T表資料
                    ApplicationLogger.addLoggerData("policyService.loadPolicyByPolicyId() begin");
                    policyVO = policyService.loadPolicyByPolicyId(policyId);
                    ApplicationLogger.addLoggerData("policyService.loadPolicyByPolicyId() end");
                }

                // 2016-11-08 Kate Hsiao 當來源是保全核保時，須判斷登入人員是否為此保全受理的操作人員
                boolean isCsTransOperatorId = false;
                if (Utils.isCsUw(sourceType)) {
                    if (StringUtils.isNullOrEmpty(isIframe) || "false".equals(isIframe)) {
                        Long csTransId = csTaskTransService.getTheTaskOperatorUser(uwPolicyVO.getChangeId());
                        if (ObjectUtils.equals(currentUserId, csTransId)) {
                            isCsTransOperatorId = true;
                        }
                    }
                }

                BeanUtils.copyProperties(uwPolicyForm, uwPolicyVO);

                ApplicationLogger.addLoggerData("連結保單 policyService.load() begin");
                PolicyInfo policyInfo = policyService.load(policyId);
                if (policyInfo != null && policyInfo.getFormerPolicy() != null) {
                    /* 連結保單 */
                    String formerPolicyCode = paPolicyService.getPolicyCodeByPolicyId(policyInfo.getFormerPolicy());
                    uwPolicyForm.setFormerPolicyCode(formerPolicyCode);
                }
                if (uwPolicyForm.getRenewValidateDate() == null) {
                    uwPolicyForm.setRenewValidateDate(uwPolicyForm.getValidateDate());
                }
                uwPolicyForm.setIsIframe(isIframe);
                uwPolicyForm.setCsTransOperatorId(isCsTransOperatorId);
                uwPolicyForm.setHolderCertiCode(policyVO.getPolicyHolder().getCertiCode());
                ApplicationLogger.addLoggerData("連結保單 end");

                // 通路
                uwPolicyForm.setSalesChannel(String.valueOf(policyVO.getChannelType()));

                // eddA約定書檢附
                String eddaBankPay = CodeCst.YES_NO__NO; //eDDA約定書
                String nextEddaBankPay = CodeCst.YES_NO__NO; //eDDA約定書
                String eddaBankAuth = CodeCst.YES_NO__NO; //透過eDDA核印
                String nextEddaBankAuth = CodeCst.YES_NO__NO; //透過eDDA核印
                String isBankPay = isBankPay(policyVO, false);	//首期付款是否為銀行轉帳
                String isNextBankPay = isBankPay(policyVO, true);//續期付款是否為銀行轉帳
                //因下面要判斷首/續期繳費方式是否金融通構,將eddaIndi由isBankPay()中移出至外面判斷
                boolean eddaIndi = eddaDao.getEddaIndi(policyVO.getPolicyId());
                if (!CodeCst.YES_NO__YES.equals(isBankPay) && !CodeCst.YES_NO__YES.equals(isNextBankPay)) {
                    //2019/05/14 IR-317911 皆非金融機構，需增加EDDA  match檢核
                    if (eddaIndi) {
                        //因為無法用首/續期付款方式判斷，因此首/續期的eDDA約定書均顯示有檢附
                        eddaBankPay = CodeCst.YES_NO__YES; //eDDA約定書=有檢附
                        nextEddaBankPay = CodeCst.YES_NO__YES; //eDDA約定書=有檢附
                        //再依是否match作首/續期透過eDDA核印顯示是
                        List<Edda> eddaList = eddaDao.findByProperty("policyId", policyVO.getPolicyId());
                        if (!eddaList.isEmpty()) {
                            Edda edda = eddaList.get(0);
                            boolean isEddaMatch = false;
                            if (!NBUtils.isEmptyOrDummyCertiCode(edda.getCertiCode())) {
                                SealRecordEddaVO sealRecordEddaVO = premCI.checkEddaPool(policyVO.getPolicyNumber(), edda.getCertiCode());
                                if (sealRecordEddaVO.getListId() != null) {
                                    isEddaMatch = true;
                                }
                            }
                            if (isEddaMatch) {
                                eddaBankAuth = CodeCst.YES_NO__YES;
                                nextEddaBankAuth = CodeCst.YES_NO__YES;
                            }
                        }
                    }
                } else {
                    if (eddaIndi) {
                        //依勾選電子化約定書+付款方式「金融機構=Y」顯示「eDDA約定書」=有檢附
                        eddaBankPay = isBankPay; //eDDA約定書
                        nextEddaBankPay = isNextBankPay; //eDDA約定書			
                    }
                    //依核印檔是否有match eDDA，顯示「透過eDDA核印」=是
                    eddaBankAuth = isEddaBankAuth(policyId, false);
                    nextEddaBankAuth = isEddaBankAuth(policyId, true);
                }
                uwPolicyForm.setEddaBankPay(eddaBankPay); //eDDA約定書
                uwPolicyForm.setNextEddaBankPay(nextEddaBankPay); //eDDA約定書	
                uwPolicyForm.setEddaBankAuth(eddaBankAuth); //透過eDDA核印
                uwPolicyForm.setNextEddaBankAuth(nextEddaBankAuth); //透過eDDA核印

                CoverageVO coverageVO = policyVO.gitMasterCoverage();
                List<CoverageAgentVO> coverageAgents = coverageVO.gitSortCoverageAgents();
                if (CollectionUtils.isNotEmpty(coverageAgents)) {
                    CoverageAgentVO coverageAgent = coverageVO.getCoverageAgents().get(0);
                    uwPolicyForm.setAgentOrgan(coverageAgent.getChannelCode());
                }
                uwPolicyForm.setUserId(currentUserId);
                // 繳別
                uwPolicyForm.setInitialType(coverageVO.getCurrentPremium().getPaymentFreq());
                // 首期續期繳費方式
                List<PayerAccountVO> payerAccounts = policyVO.getPayerAccounts();

                //PCR-383857 取得付款授權書資料 2020/12/22 Add by Kathy
                Map<String, PolicyBankAuthVO> bankAuthMap = policyBankAuthService.findMapByPolicyId(policyId);
                PolicyBankAuthVO bankAuthVO;
                if (CollectionUtils.isNotEmpty(payerAccounts)) {
                    PayerAccountVO payeraccountVo = payerAccounts.get(0);
                    int payMode = payeraccountVo.getPaymentMethod();
                    uwPolicyForm.setPayMode(String.valueOf(payMode));
                    // 繳費方式=銀行轉帳/信用卡顯示核印/驗證狀態
                    if (payMode == CodeCst.PAY_MODE__GIRO || payMode == CodeCst.PAY_MODE__CREDIT_CARD) {
                        uwPolicyForm.setApprovalStatus(payeraccountVo.getApprovalStatus());
                        //首期繳費方式 = 信用卡 && 狀態 = 失敗 && 付款授權書CARD_POOL = 'Y'，設定approvalStatus = 'C2'(送驗證前失敗)
                        if (payMode == CodeCst.PAY_MODE__CREDIT_CARD
                                && CodeCst.APPROVAL_STATUS__FAILED.equals(payeraccountVo.getApprovalStatus())) {
                            // 先取首期
                            bankAuthVO = bankAuthMap.get(com.ebao.ls.pa.nb.util.Cst.SOURCETYPE_INIT_AUTH);
                            if (bankAuthVO == null) {
                                // 若首期空值，取首期同續期
                                bankAuthVO = bankAuthMap.get(com.ebao.ls.pa.nb.util.Cst.SOURCETYPE_NEXT_EQUALS_INIT);
                            }
                            if (CodeCst.YES_NO__YES.equals(bankAuthVO.getCardPool())) {
                                uwPolicyForm.setApprovalStatus("C2");
                            }
                        }
                    }
                    // 判斷是否有續期繳款資料
                    if (!policyVO.gitMasterCoverage().isSinglePay()) {
                        int payNext = payeraccountVo.getPaymentMethodNext();
                        uwPolicyForm.setPayNext(String.valueOf(payNext));
                        if (payNext == CodeCst.PAY_MODE__GIRO || payNext == CodeCst.PAY_MODE__CREDIT_CARD) {
                            uwPolicyForm.setNextApprovalStatus(payeraccountVo.getNextApprovalStatus());
                        }
                        //續期繳費方式 = 信用卡 && 狀態 = 失敗 && 付款授權書CARD_POOL = 'Y'，設定approvalStatus = 'C2'(送驗證前失敗)
                        if (payNext == CodeCst.PAY_MODE__CREDIT_CARD
                                && CodeCst.APPROVAL_STATUS__FAILED.equals(payeraccountVo.getNextApprovalStatus())) {
                            // 取續期
                            bankAuthVO = bankAuthMap.get(com.ebao.ls.pa.nb.util.Cst.SOURCETYPE_NEXT_AUTH);
                            if (CodeCst.YES_NO__YES.equals(bankAuthVO.getCardPool())) {
                                uwPolicyForm.setNextApprovalStatus("C2");
                            }
                        }
                    }
                }

                // 當來源是保全核保，則首期繳費方式與核印狀態、續期繳費方式與核印狀態
                // 要改取承保當時所產生的核心表資料紀錄檔
                if (Utils.isCsUw(sourceType)) {
                    QryBaseVO qryBaseVO = new QryBaseVO();
                    qryBaseVO.setPolicyId(policyId);

                    ApplicationLogger.addLoggerData("qryPolicyDS.qryFinancialInfo_new() begin");
                    FinancialInfoNewVO financialInfoNewVO = qryPolicyDS.qryFinancialInfo_new(qryBaseVO);
                    ApplicationLogger.addLoggerData("qryPolicyDS.qryFinancialInfo_new() end");
                    uwPolicyForm.setPayMode(financialInfoNewVO.getFirst_pay_mode());//
                    uwPolicyForm.setApprovalStatus(financialInfoNewVO.getFirst_seal_approval_status());
                    uwPolicyForm.setPayNext(financialInfoNewVO.getRenewal_pay_mode());
                    uwPolicyForm.setNextApprovalStatus(financialInfoNewVO.getRenewal_seal_approval_status());

                    // 2017-05-17 Kate 新增客戶申請日 
                    ApplicationLogger.addLoggerData("csApplicationService.getApplication(" + uwPolicyVO.getChangeId() + ") begin");
                    ApplicationVO applicationVO = applicationService.getApplication(uwPolicyVO.getChangeId());
                    ApplicationLogger.addLoggerData("csApplicationService.getApplication(" + uwPolicyVO.getChangeId() + ") end");
                    uwPolicyForm.setCustApplyTime(applicationVO.getCustAppTime());
                }

                // 集彚編號
                uwPolicyForm.setPayTogetherCode(policyVO.getPayTogetherCode());

                // 集彙/繳費折扣%
                uwPolicyForm.setDiscntRate(paPolicyCI.getDiscntRateByPolicyId(policyId));

                // 一般懸帳
                uwPolicyForm.setSuspense(cashCI.getSuspenseByIdPremPurpose(policyVO.getPolicyId(), CodeCst.PREM_PURPOSE__GENERAL));

                // 幣別
                uwPolicyForm.setMoneyId(String.valueOf(policyVO.getCurrency()));

                // 暫不列印
                String pendingIssueType = uwPolicyVO.getPendingIssueType();
                if (Cst.PENDING_ISSUE_TYPE_NOT_MAIL.equals(pendingIssueType)
                        || Cst.PENDING_ISSUE_TYPE_NOT_PRINT.equals(pendingIssueType)) {
                    uwPolicyForm.setPendingIssueStatus("Y");
                } else {
                    uwPolicyForm.setPendingIssueStatus("N");
                }

                // 批次簽署
                uwPolicyForm.setBatchSignRange(policyVO.getProposalInfo().getBatchSignRange());
                // 由保經代公司對高齡、保費來源為解約/貸款/保單借款辦理電訪等作業
                uwPolicyForm.setElderCalloutFlag(paPolicyService.isBrBdElderCalloutFlag(policyVO));
                
                String remoteIndi = policyVO.getProposalInfo().getRemoteIndi();
                String remoteCallout = policyVO.getProposalInfo().getRemoteCallout();
                List<String> certiCodeList = paRemoteHelper.findRemotePolicyCertiCodeList(policyId);
                uwPolicyForm.setRemoteIndi(paRemoteHelper.converterRemoteIndi(remoteIndi));//遠距投保
				uwPolicyForm.setRemoteOverseas(paRemoteHelper.getDisplayRemoteOverseas(policyId, remoteIndi)); // 境外遠距投保
                uwPolicyForm.setRemoteCallout(paRemoteHelper.getDisplayRemoteCallout(remoteIndi, remoteCallout));//錄音錄影覆核抽檢件
                uwPolicyForm.setRemoteCheck(paRemoteHelper.getDisplayRemoteCheck(policyId, remoteIndi, certiCodeList));//身分驗證結果

                ApplicationLogger.addLoggerData("initializeComment() begin");
                // set comments information 2009-12-2
                initializeComment(uwPolicyForm, uwPolicyVO);
                ApplicationLogger.addLoggerData("initializeComment() end");

                Collection benefits = uwPolicyDS.findUwProductEntitis(underwriteId);
                Collection<UwProductDecisionViewForm> uwProductDecisionViewForms = getUwProductDecisionViewForms(benefits);
                // 首期表定保費
                ApplicationLogger.addLoggerData("setPrem() begin");
                setPrem(uwPolicyForm, uwProductDecisionViewForms, sourceType, policyVO);
                ApplicationLogger.addLoggerData("setPrem() end");

                // 首期應繳保費
                uwPolicyForm.setInstallPrem(policyVO.getInstallPrem());

                // artf189164 : [Coding] [ATN-COR-UNB-BSD-007-CR023_SR002] 核保呈核-程式修改及測試
                // add by simon.huang on 2016/03/20
                ApplicationLogger.addLoggerData("uwTransferHelper.defineUwTransferRole() begin");
                uwTransferHelper.defineUwTransferRole(uwPolicyForm, uwPolicyVO, isIframe);
                ApplicationLogger.addLoggerData("uwTransferHelper.defineUwTransferRole() end");
                ApplicationLogger.addLoggerData("uwTransferHelper.getDisplayUwComment() begin");
                uwTransferHelper.getDisplayUwComment(uwPolicyForm, underwriteId);
                ApplicationLogger.addLoggerData("uwTransferHelper.getDisplayUwComment() end");
                ApplicationLogger.addLoggerData("uwTransferHelper.getLastUnderwriterInfo() begin");
                uwTransferHelper.getLastUnderwriterInfo(uwPolicyForm, policyId);
                ApplicationLogger.addLoggerData("uwTransferHelper.getLastUnderwriterInfo() end");

                ApplicationLogger.addLoggerData("lifeProductService.getProduct(" + coverageVO.getProductId() + ") begin");
                LifeProduct lifeProd = lifeProductService.getProduct(coverageVO.getProductId().longValue());
                ApplicationLogger.addLoggerData("lifeProductService.getProduct(" + coverageVO.getProductId() + ") end");
                //IR_395054 修正：核保勾選溢繳退費「否」，綜合查詢頁面未顯示「否」
                uwPolicyForm.setIlpRefundCustIndi(policyVO.getIlpRefundCustIndi());
                if (uwPolicyForm.getIlpRefundCustIndi() == null
                        && "1".equals(lifeProd.getProduct().getOverPaymentRule())) {
                    uwPolicyForm.setIlpRefundCustIndi(CodeCst.YES_NO__YES);
                }

                // get agent_code by agent_id
                if (uwPolicyVO.getServiceAgent() != null) {
                    AgentChlVO agentInfo = agentService.findAgentByAgentId(uwPolicyVO.getServiceAgent());
                    uwPolicyForm.setAgentCode(agentInfo.getAgentCode());
                }

                // set appearl reason desc
                if (uwPolicyForm.getAppealReason() != null
                        && !uwPolicyForm.getAppealReason().equals("0")) {
                    uwPolicyForm.setAppealReasonDesc(CodeTable.getCodeDesc("T_APPEAL_REASON", uwPolicyForm.getAppealReason()));
                }

                // 要保書填寫日大於等於2022.10.01 且 屬於「高齡投保件(核保高齡關懷提問)」或「高齡投保件(高齡投保評估量表)」時，按鈕才須打開可輸入
                boolean matchElderCareStartDate = paNbPolicySpecialRuleService.isMatchElderCareStartDate(policyId);
                boolean hasElderQuestionCoverage = paValidatorService.hasElderQuestionCoverage(policyVO);
                boolean hasElderScaleCoverage = paValidatorService.hasElderScaleCoverage(policyVO);
                boolean hasElderCareQuestionRole = paValidatorService.hasElderCareQuestionRole(policyVO);
                boolean hasElderCareScaleRole = paValidatorService.hasElderCareScaleRole(policyVO);
                if (matchElderCareStartDate
                        && ((hasElderQuestionCoverage && hasElderCareQuestionRole)
                        || (hasElderScaleCoverage && hasElderCareScaleRole))) {
                    uwPolicyForm.setUnderwritingElderCareQuestion(true);
                }
            } catch (Exception e) {
                String message = ExceptionInfoUtils.getExceptionMsg(e);
                int len = message.length();
                if (len > 0) {
                    ApplicationLogger.addLoggerData("[ERROR]Exception => " + message.substring(0, (len > 2000 ? 2000 : len)));
                } else {
                    ApplicationLogger.addLoggerData("[ERROR]Exception => " + e);
                }
                try {
                    ApplicationLogger.flush();
                } catch (Exception e1) {
                    logger.warn(e.getMessage());
                }
                throw ExceptionFactory.parse(e);
            }
        }
    }

	/**
	 * <p>Description : 設定核保頁面資料，寫入DB</p>
	 * <p>Created By : simon.huang</p>
	 * <p>Create Time : Apr 26, 2016</p>
	 * @param request
	 * @param uwPolicyVOFromDB
	 * @param products
	 * @throws GenericException
	 */
	public void drawValueFromPage_TGL(HttpServletRequest request,
			UwPolicyVO uwPolicyVOFromDB, Collection products)
			throws GenericException {
		
		Long userId = AppContext.getCurrentUser().getUserId();
		Long underwriteId = uwPolicyVOFromDB.getUnderwriteId();
		//需依角色設定欄位
		UwPolicyForm uwPolicyForm = new UwPolicyForm();
		uwTransferHelper.defineUwTransferRole(uwPolicyForm, uwPolicyVOFromDB, "false");
		
		boolean isUnderwriter = uwPolicyForm.getIsUnderwriter();		//核保員
		boolean isManualEscaUser = uwPolicyForm.getIsEscaUser();		//人工陳核回覆人員
		boolean isUwManager = uwPolicyForm.getIsManager();				//核保簽核主管
		boolean isExtraPremManager = uwPolicyForm.getIsExtraPremManager();		//加費簽核主管
		
		/* 核保員 */
		if (isUnderwriter) {

			/* set 保單生效日 核保主頁面 保單生效日 */
			String strRenewValidateDate = request
					.getParameter(ReqParamNameConstants.POLICY_COMMENCEMENT_DATE_STRING);
			if (strRenewValidateDate != null && !"".equals(strRenewValidateDate.trim())) {
				Date renewValidateDate = toDate(strRenewValidateDate);
				if (renewValidateDate != null) {
					//續保件生效日與保單生效日不同才作設定
					if (!DateUtils.date2String(renewValidateDate).equals(
							DateUtils.date2String(uwPolicyVOFromDB.getValidateDate()))) {
						uwPolicyVOFromDB.setRenewValidateDate(renewValidateDate);
					}
				}
			}
			/* set 保單核保決定 */
			String policyDecision = request.getParameter("policyDecision");
			uwPolicyVOFromDB.setPolicyDecision(policyDecision);
			/* 覆核/主管核保意見 (核保員不存)*/
			//String comment = request.getParameter("uwComment");
			//uwPolicyVOFromDB.setUwNotes(comment);
			//tring uwSourceType = uwPolicyVOFromDB.getUwSourceType();
			
			/* 空方法
			setUnderwritingComments(request, uwPolicyVOFromDB);
			*/
			/* 核保特別說明 */
			String checkNote = request.getParameter("checkNote");
			uwPolicyVOFromDB.setCheckNote(checkNote);

			/* 主管陳核 說明/人工陳核標誌/陳核主管userid */
			setManulEsc(request, uwPolicyVOFromDB);

			/* 是否回溯 */
			uwPolicyVOFromDB.setBackdatingIndi("N");
			/* 险种是否回溯 */
			uwPolicyVOFromDB.setRiskBackdatingIndi("N");

			/* set 調影像否 */
			//String imageIndi = request.getParameter("imageIndi");
			//if (imageIndi != null
			//                && imageIndi.trim()
			//                                .length() != 0) {
			//    uwPolicyVOFromDB.setImageIndi(imageIndi);
			//}
			/* 清空uw pending原因 */
			uwPolicyVOFromDB.setUwPending(null);
			/* TODO 電訪回覆件確認 set 未承保原因 */
			String cancelDesc = request.getParameter("cancelDesc");
			uwPolicyVOFromDB.setCancelDesc(cancelDesc);

			/* set 高保額退費 */
			String hiPremIndi = request.getParameter("hiPremIndi");
			uwPolicyVOFromDB.setHiPremIndi(hiPremIndi);

			/* set 投資型保費溢繳保費直接退費 */
			String ilpRefundCustIndi = request.getParameter("ilpRefundCustIndi");
			uwPolicyVOFromDB.setIlpRefundCustIndi(ilpRefundCustIndi);
			/* set 標準體承保 */
			String reportStandLife = request.getParameter("reportStandLife");
			uwPolicyVOFromDB.setReportStandLife(reportStandLife);
			/* set 暫不列印 */
			String pendingIssueStatus = request.getParameter("pendingIssueStatus");
			if (!StringUtils.isNullOrEmpty(pendingIssueStatus)) {
				if (CodeCst.YES_NO__YES.equals(pendingIssueStatus)) {
					//暫不列印
					uwPolicyVOFromDB.setPendingIssueType("1");
				} else {
					/* 無 */
					uwPolicyVOFromDB.setPendingIssueType("0");
				}
			}
			/* set 暫不列印原因  */
			String pendingIssueCause = request.getParameter("pendingIssueCause");
			uwPolicyVOFromDB.setPendingIssueCause(pendingIssueCause);
			/* set 暫不列印原因-其他說明 */
			String pendingIssueDesc = request.getParameter("pendingIssueDesc");
			if (Cst.PENDING_ISSUE_CAUSE_OTHER.equals(pendingIssueCause)) {
				if (!StringUtils.isNullOrEmpty(pendingIssueDesc)) {
					uwPolicyVOFromDB.setPendingIssueDesc(pendingIssueDesc);
				}
			} else {
				uwPolicyVOFromDB.setPendingIssueDesc(null);
			}
			
			/* set 核保人員核保意見 */
			String reportNotes = request.getParameter("reportNotes");
			uwPolicyVOFromDB.setReportNotes(reportNotes);
			/* 洗錢資恐高風險客戶核保審查意見*/
			String riskReportNotes = request.getParameter("riskReportNotes");
			uwPolicyVOFromDB.setRiskReportNotes(riskReportNotes);
                    // 法人實質受益人辨識評估結果
                    String legalBeneEvalNotes = request.getParameter("legalBeneEvalNotes");
                    uwPolicyVOFromDB.setLegalBeneEvalNotes(legalBeneEvalNotes);

			// 2015-09-02 change by peter temp save 產生再保申請
			String[] riListId = request.getParameterValues("riListId");
			String[] riCompanyId = request.getParameterValues("riCompanyIds");
			String[] reinsuredResult = request.getParameterValues("reinsuredResult");
			String[] reinsuredDate = request.getParameterValues("reinsuredDate");
			if (riCompanyId != null && riCompanyId.length > 0) {
				for (int i = 0; i < riListId.length; i++) {
//					uwRiApplyService.setRiCompanyInfo(riListId[i], reinsuredResult[i], reinsuredDate[i]);	
				}
			}

			// 2016-02-23 ChiaHui Su 取得畫面上被保險人資料修改結果 start
			List<UwLifeInsuredVO> insuredList = getInsuredList(request, uwPolicyVOFromDB.getUwSourceType()); //PCR-524169,POS need sourceType
			
			for (UwLifeInsuredVO vo : insuredList) {
				//同步核保作業被保險人UW表至被保險人T表
				uwPolicyDS.updateUwLifeInsured(vo);
			}
			// 2016-02-23 ChiaHui Su 取得畫面上被保險人資料修改結果 end

			//生效日會依繳費日調整，存檔時需同步核加費檔的startDate/endDate

			Collection<UwExtraLoadingVO> uwExtraLoadingList = uwPolicyDS.findUwExtraLoadingEntitis(underwriteId);
			Collection<UwProductVO> uwProducts = uwPolicyDS.findUwProductEntitis(underwriteId);
			
			Map<String, List<UwExtraLoadingVO>> itemIdBindUwExtraLoading = NBUtils.toMapList(uwExtraLoadingList, "itemId");
			Map<String, UwProductVO> itemIdBindUwProduct = NBUtils.toMap(uwProducts, "itemId");

			for (String itemId : itemIdBindUwExtraLoading.keySet()) {
				UwProductVO uwProductVO = itemIdBindUwProduct.get(itemId);

				for (UwExtraLoadingVO uwExtraLoadingVO : itemIdBindUwExtraLoading.get(itemId)) {
					UwExtraPremHelper.syncExtraPremStartDate(uwProductVO, uwExtraLoadingVO);
					uwPolicyDS.updateUwExtraLoading(uwExtraLoadingVO);
				}
			}

			/* 個案融通件不適用全球人壽保險金額增加批註條款*/
			String insurabilityExCase = request.getParameter("insurabilityExCase");
			uwPolicyVOFromDB.setInsurabilityExCase(insurabilityExCase);

			/**更新再保會辦結果**/
	        // 再保儲存
	        java.util.List<NbRiResultVO> resultVOList = this.getRiResultList(request);
	        this.uwRiApplyService.saveOrUpdateNbRiResultList(resultVOList);

		} else if (isManualEscaUser) {
			
			/* 覆核/主管核保意見(陳核回覆) */
			String comment = request.getParameter("uwComment");
			uwPolicyVOFromDB.setUwNotes(comment);
			
			if (uwPolicyVOFromDB.getUwEscaUser() == null) {
				/* 未設定人工陳核人員，預設抓取核保員的休假豁區的核保簽核主管  */
				Long uwManagerUserId = uwTransferHelper.getTransferUser(userId, uwPolicyVOFromDB.getUwSourceType());
				if (uwManagerUserId != null) {
					uwPolicyVOFromDB.setUwEscaUser(uwManagerUserId);
				}
			}
		} else if (isUwManager || isExtraPremManager) {
			//isManager

			/* 主管陳核 說明/人工陳核標誌/陳核主管userid */
			setManulEsc(request, uwPolicyVOFromDB);

			/* 覆核/主管核保意見(陳核回覆) */
			String comment = request.getParameter("uwComment");
			uwPolicyVOFromDB.setUwNotes(comment);
			
			/* 陳核說明 (人工陳核) */
			String proposeDesc = request.getParameter("proposeDesc");
			uwPolicyVOFromDB.setProposeDesc(proposeDesc);

			/* set 主管簽核意見 */
			String uwCommentIndi = request.getParameter("uwCommentIndi");
			uwPolicyVOFromDB.setUwCommentIndi(uwCommentIndi);
		}

	}
	
	private java.util.List<NbRiResultVO> getRiResultList(HttpServletRequest request){
	      java.util.List<NbRiResultVO> resultVOList = new java.util.LinkedList<NbRiResultVO>();
	      String[] riResultArray = request.getParameterValues("riResultListId");
	      String[] resultStatusArray = request.getParameterValues("resultStatus");
	      String[] reinsuredDateArray = request.getParameterValues("reinsuredDate");
	      java.util.List<String>  riResultListIdList = java.util.Arrays.asList(riResultArray == null ? new String[0] : riResultArray);
	      java.util.List<String>  resultStatusList = java.util.Arrays.asList(resultStatusArray == null ? new String[0] : resultStatusArray);
	      java.util.List<String> reInsuredDateList = java.util.Arrays.asList(reinsuredDateArray == null ? new String[0] : reinsuredDateArray);
	      int maxSize = 0;
	      if(CollectionUtils.isNotEmpty(riResultListIdList)) {
	          maxSize = riResultListIdList.size();
	      }
	      if(CollectionUtils.isNotEmpty(resultStatusList)) {
	          maxSize = resultStatusList.size();
	      }
	      if(CollectionUtils.isNotEmpty(reInsuredDateList)) {
	          maxSize = reInsuredDateList.size();
	      }
	      
	      for(int i = 0 ; i < maxSize ; i++) {
	          NbRiResultVO column = new NbRiResultVO();
	          if( riResultListIdList.size() > i) {
	              String listId = riResultListIdList.get(i);
	              if(listId != null)
	                  column.setListId(Long.parseLong(listId));
	          }
	          
	          if( resultStatusList.size() > i) {
	              String resultStatus = resultStatusList.get(i);
	              if(org.apache.commons.lang3.StringUtils.isNotEmpty(resultStatus))
	                  column.setResultStatus(resultStatus);
	          }
	          
	          if( reInsuredDateList.size() > i) {
	                String reInsuredDateText = reInsuredDateList.get(i);
	                java.util.Date reInsuredDate = null;
	                try {
	                    reInsuredDate = com.ebao.ls.ws.util.DateUtils.parse(reInsuredDateText, "dd/MM/yyyy");
	                } catch (ParseException e) {
	                    Log.info("reInsuredDateText : =" + reInsuredDateText);
	                }
	                if (reInsuredDate != null) {
	                    column.setReinsuredDate(reInsuredDate);
	                }
	          }
	          resultVOList.add(column);
	      }
	      return resultVOList;
	  } 

	/**
	 * add by hanzhong.yan for cq:GEL00029240 refactor the method "public void
	 * setUwPolicyValueFromPage(HttpServletRequest request,UwPolicyVO uwPolicyVO,
	 * Collection products)" check some restrict field of the page
	 * 
	 * @param request
	 * @param uwPolicyVO
	 * @param products
	 * @throws GenericException
	 */
	@PrdAPIUpdate
	public void checkPageInfo(HttpServletRequest request, UwPolicyVO uwPolicyVO,
			Collection products) throws GenericException {
		// set risk commencement date
		String riskCommendate = request
				.getParameter(ReqParamNameConstants.POLICY_RISK_COMMENCEMENT_DATE_STRING);
		if (riskCommendate != null
				&& riskCommendate.trim().length() > 0
				&& ActionUtil.compareDate(uwPolicyVO.getValidateDate(),
						toDate(riskCommendate))
				&& "Y".equals(request.getParameter("backdatingIndi"))) {
			Iterator productsIter = products.iterator();
			while (productsIter.hasNext()) {
				UwProductVO upvo = (UwProductVO) productsIter.next();
				if (null == upvo.getMasterId()) {
					if (this.getProductService()
							.getProductByVersionId(Long.valueOf(upvo.getProductId().intValue()), 
									upvo.getProductVersionId())
							.isBackdateIndi()) {
						throw new AppException(
								UwExceptionConstants.APP_COMPARE_RISK_AND_COMM);
					}
				}
			}
		}
		// the method below is used for check "when policy's Generate LCA Indicator
		// is 'Y',then the LCA Date Indicator must be 'Y',too."
		uwProcessDS.setLCAIndsAndConsentGivenInd(uwPolicyVO);
	}

	/**
	 * to wrap exception to GenericException
	 * 
	 * @param strDate
	 * @return
	 * @throws GenericException
	 */
	private Date toDate(String strDate) throws GenericException {
		try {
			return DateUtils.toDate(strDate);
		} catch (Exception e) {
			throw ExceptionFactory.parse(e);
		}
	}

	/**
	 * @param request
	 * @param uwPolicyVO
	 * @throws NumberFormatException
	 */
	private void setManulEsc(HttpServletRequest request, UwPolicyVO uwPolicyVO)
			throws NumberFormatException {

		/* 陳核說明 (人工陳核) */
		String proposeDesc = request.getParameter("proposeDesc");
		uwPolicyVO.setProposeDesc(proposeDesc);
		
		String uwEscaUser = request.getParameter("uwEscaUser");
		// only when uwEscaUser has value,then update the field
		if (uwEscaUser != null && uwEscaUser.trim().length() > 0) {
			uwPolicyVO.setUwEscaUser(Long.valueOf(uwEscaUser));
		} else {
			uwPolicyVO.setUwEscaUser(null);
		}
		// else
		// uwPolicyVO.setUwEscaUser(null);
		String manuEscIndi = request.getParameter("manuEscIndi");
		if (manuEscIndi == null || manuEscIndi.trim().length() == 0) {
			manuEscIndi = "N";
		}
		uwPolicyVO.setManuEscIndi(manuEscIndi);


	}

	/**
	 * @param underwriteId
	 */
	@SuppressWarnings("unchecked")
	protected void changeUnderwritingDecision(Long underwriteId) {
		List<UwExclusionVO> uwExclusionList = (List<UwExclusionVO>) uwPolicyDS
				.findUwExclusionEntitis(underwriteId);
		List<UwProductVO> uwProductList = (List<UwProductVO>) uwPolicyDS
				.findUwProductEntitis(underwriteId);
		boolean updateFlag = false;
		for (UwProductVO uwProductVO : uwProductList) {
			updateFlag = false;
			for (UwExclusionVO uwExclusionVO : uwExclusionList) {
				if (uwProductVO.getPolicyId().equals(uwExclusionVO.getPolicyId())
						&& new Long(uwProductVO.getProductId()).equals(uwExclusionVO
								.getProductId())
						&& uwProductVO.getDecisionId() != null
						&& uwProductVO.getDecisionId().intValue() == CodeCst.PRODUCT_DECISION__ACCEPTED) {
					uwProductVO.setDecisionId(CodeCst.PRODUCT_DECISION__CONDITION);
					updateFlag = true;
				}
			}
			if (updateFlag) {
				uwPolicyDS.updateUwProduct(uwProductVO);
			}
		}
	}

	/**
	 * <p>Description : 保項核保決定要由保單核保結論判斷</p>
	 * <p>Created By : Sunny Wu</p>
	 * <p>Create Time : Jul 9, 2015</p>
	 * @param policyDescision
	 * @param underwriteId
	 */
	protected void changeUnderwritingDecision(String policyDescision, Long underwriteId) {
		List<UwProductVO> uwProductList = (List<UwProductVO>) uwPolicyDS
				.findUwProductEntitis(underwriteId);

		int decisionId = CodeCst.PRODUCT_DECISION__OTHER;

		if (Cst.POLICY_DECISION_ACCEPT.equals(policyDescision)) {
			// 若有加費或批註則為有條件接受
			List<UwExclusionVO> uwExclusionList = (List<UwExclusionVO>) uwPolicyDS
					.findUwExclusionEntitis(underwriteId);
			List<Long> conditionItems = new ArrayList<Long>();
			for (UwExclusionVO uwExclusionVO : uwExclusionList) {
				conditionItems.add(uwExclusionVO.getItemId());
			}
			List<UwExtraLoadingVO> uwExtraLoadingList = (List<UwExtraLoadingVO>) uwPolicyDS
					.findUwExtraLoadingEntitis(underwriteId);
			for (UwExtraLoadingVO uwExtraLoadingVO : uwExtraLoadingList) {
				conditionItems.add(uwExtraLoadingVO.getItemId());
			}

			for (UwProductVO uwProductVO : uwProductList) {
				if (conditionItems.contains(uwProductVO.getItemId())) {
					decisionId = CodeCst.PRODUCT_DECISION__CONDITION;
				} else {
					decisionId = CodeCst.PRODUCT_DECISION__ACCEPTED;
				}
				//狀態更新
				uwProductVO.setUwStatus(UwStatusConstants.FINISHED);
				uwProductVO.setDecisionId(decisionId);
				uwPolicyDS.updateUwProduct(uwProductVO);
			}
		} else {

			if (Cst.POLICY_DECISION_MISSING_DOC.equals(policyDescision)) {
				decisionId = CodeCst.PRODUCT_DECISION__MISSING_DOC;
			} else if (Cst.POLICY_DECISION_POSTPONED.equals(policyDescision)) {
				decisionId = CodeCst.PRODUCT_DECISION__POSTPONED;
			} else if (Cst.POLICY_DECISION_DECLINED.equals(policyDescision)) {
				decisionId = CodeCst.PRODUCT_DECISION__DECLINED;
			} else if (Cst.POLICY_DECISION_WITHDRAW.equals(policyDescision)) {
				decisionId = CodeCst.PRODUCT_DECISION__WITHDRAW;
			} else if (Cst.POLICY_DECISION_REJECT_EXTRA_PREM
					.equals(policyDescision)) {
				decisionId = CodeCst.PRODUCT_DECISION__REJECT_EXTRA_PREM;
			} else if (Cst.POLICY_DECISION_REJECT_EXCLUSION
					.equals(policyDescision)) {
				decisionId = CodeCst.PRODUCT_DECISION__REJECT_EXCLUSION;
			} else if (Cst.POLICY_DECISION_NOT_PAY.equals(policyDescision)) {
				decisionId = CodeCst.PRODUCT_DECISION__NOT_PAY;
			} else if (Cst.POLICY_DECISION_NOT_MEDICAL_EXAM
					.equals(policyDescision)) {
				decisionId = CodeCst.PRODUCT_DECISION__NOT_MEDICAL_EXAM;
			} else if (Cst.POLICY_DECISION_OTHER.equals(policyDescision)) {
				decisionId = CodeCst.PRODUCT_DECISION__OTHER;
			}
			//狀態更新
			for (UwProductVO uwProductVO : uwProductList) {
				uwProductVO.setUwStatus(UwStatusConstants.FINISHED);
				uwProductVO.setDecisionId(decisionId);
				uwPolicyDS.updateUwProduct(uwProductVO);
			}
		}
	}

	/**
	 * <p>Description : 取得畫面上核保檢核綜合意見欄結果</p>
	 * <p>Created By : Sunny Wu</p>
	 * <p>Create Time : Jul 2, 2015</p>
	 * @param request
	 * @return
	 */
	public List<UwCommentVO> getCommentLists(
			HttpServletRequest request) {
		String[] listIds = com.ebao.ls.encode.encoder.Encode.encodeForHTML(request).getParameterValues("optionListId");

		List<UwCommentVO> list = new ArrayList<UwCommentVO>();
		for (String listId : listIds) {
			UwCommentVO vo = new UwCommentVO();
			vo.setOptionId(Long.parseLong(listId));
			String result = com.ebao.ls.encode.encoder.Encode.encodeForHTML(request).getParameter("option_" + listId);
			if (result != null) {
				vo.setResultCode(result);
			}

			list.add(vo);
		}

		return list;
	}

	/**
	 * <p>Description : 儲存畫面上核保綜合意見欄結果</p>
	 * <p>Created By : Sunny Wu</p>
	 * <p>Create Time : Jul 2, 2015</p>
	 * @param underwriteId
	 * @param optionList
	 */
	public void saveUWComment(Long underwriteId, List<UwCommentVO> optionList) {
		uwCommentService.saveResults(underwriteId, optionList);
	}

	/**
	 * <p>Description : 核保決定通過後，新增資料至T_BANK_ACCOUNT</p>
	 * <p>更新 T_PAYER_ACCOUNT.ACCOUNT_ID,APPPROVAL_STATUS,NEXT_ACCOUNT_ID,NEXT_APPROVAL_STATUS</p>
	 * <p>Created By : simon.huang</p>
	 * <p>Create Time : Jul 30, 2015</p>
	 * @param policyId
	 */
	public void saveBankAccountOnAcceptUnderwrite(Long policyId) {

		List<PolicyBankAuthVO> bankAuthVoList = policyBankAuthService.findByPolicyId(policyId);
		
		for (PolicyBankAuthVO bankAuth : bankAuthVoList) {
			
			//僅允許信用卡及銀行轉帳可新增bank_account, 因bankCode,bankAccount為not null able
			if (!CodeCst.ACCOUNT_TYPE__CREDIT_CARD.equals(bankAuth.getAccountType()) &&
					!CodeCst.ACCOUNT_TYPE__GIRO.equals(bankAuth.getAccountType())) {
				continue;
			}
			
			if(CodeCst.APPROVAL_STATUS__DELETED.equals(bankAuth.getApprovalStatus())) {
				continue;
			}
			
			//僅允許信用卡及銀行轉帳可新增bank_account, 因bankCode,bankAccount為not null able
			if (StringUtils.isNullOrEmpty(bankAuth.getBankCode()) 
					|| StringUtils.isNullOrEmpty(bankAuth.getBankAccount())) {
				continue;
			}

			if(bankAuth.getAccountId() != null && 
							!CodeCst.APPROVAL_STATUS__IN_PROCESS.equals(bankAuth.getApprovalStatus())) {
				policyBankAuthService.updatePayerAccountByBankAuth(policyId, bankAuth, false ,true);
			}
		}
	}

	/**
	 * <p>Description : 核保通過後增加回寫受益人的BANK ACCOUNT</p>
	 * <p>Created By : Alex Cheng</p>
	 * <p>Create Time : Nov 26, 2015</p>
	 * @param beneficiaryVO
	 * @param applyDate
	 */
	public void saveOrUpdateAccount(BeneficiaryVO beneficiaryVO, Date applyDate) {
		// set t_bank_account
		BankAccountVO bankAccountVO = null;
		if (beneficiaryVO.getBankAccount() != null
				&& beneficiaryVO.getBankCode() != null
				&& beneficiaryVO.getParty() != null) {
			
			String bankCode = beneficiaryVO.getBankCode();
			if(!StringUtils.isNullOrEmpty(beneficiaryVO.getBranchCode())) {
				bankCode = beneficiaryVO.getBranchCode();
			}
			
			bankAccountVO = getBankAccountbyAccountNoAndBankCodeAndPartyId(
					beneficiaryVO.getBankAccount(),bankCode , beneficiaryVO.getPartyId());
			if (bankAccountVO != null) {
				beneficiaryVO.setAccountId(bankAccountVO.getAccountId());
			}
		}

		if (!StringUtils.isNullOrEmpty(beneficiaryVO.getBankCode())
				&& !StringUtils.isNullOrEmpty(beneficiaryVO.getBankAccount())) {
			if (bankAccountVO == null) {
				bankAccountVO = new BankAccountVO();
				bankAccountVO.setPartyId(beneficiaryVO.getPartyId());
			}
			// Set Bank Account from fronted form ;
			this.setBentficiaryBankAccount(bankAccountVO, beneficiaryVO, applyDate);

			if (bankAccountVO.getAccountId() == null) {
				// create
				Long accountId = bankAccountService
						.createAccount(bankAccountVO);
				beneficiaryVO.setAccountId(accountId);
			} else {
				// update
				//bankAccountService.updateAccount(bankAccountVO);
			}
		} else {
			// clear t_bank_account reference
			beneficiaryVO.setAccountId(null);
		}
	}
	
	/**
	 * <p>Description : 第7年度增值回饋分享金帳戶資料回寫BankAccount</p>
	 * <p>Created By : simon.huang</p>
	 * <p>Create Time : May 25, 2017</p>
	 * @param policyId
	 * @throws Exception 
	 */
	public void saveFeedbackBonus7BankAccount(Long policyId) {

		CoverageVO coverageVO = coverageCI.getMainBenefitByPolicyId(policyId);

		PayPlanVO feedbackBonusVO = coverageVO.getPayPlanByType(CodeCst.PAY_PLAN_TYPE__FEEDBACK_BONUS);
		if(feedbackBonusVO != null && coverageVO.getPayPlanExtend() != null){
			
			PayPlanExtendVO payPlanExtendVO = coverageVO.getPayPlanExtend();
			InsuredVO insuredVO = coverageVO.getLifeInsured1().getInsured();
			Long partyId = insuredVO.getPartyId();
			// set t_bank_account
			
	
			BankAccountVO bankAccountVO = null;
			if (payPlanExtendVO.getBankAccount() != null
					&& payPlanExtendVO.getBankCode() != null) {
				
				
				String bankCode = payPlanExtendVO.getBankCode() ;
				/* 2018/05/23 by Simon
				 * IR-244353-付款帳號只有3碼導致無法匯款:調整一律儲存7碼至BankCode欄位
				if(bankCode.length() > 3){
					//因db僅有bankCode所以有可能是儲存branchCode的資料,所以只取3碼
					bankCode = bankCode.substring(0,3);
				} 
				*/
				HibernateSession3.currentSession().flush();
				NBUtils.logger(getClass(), "getBankAccountbyAccountNoAndBankCodeAndPartyId("
								+ payPlanExtendVO.getBankAccount()+", \n" 
								+ bankCode+", \n" +  
								+ partyId+", \n");
								
				bankAccountVO = getBankAccountbyAccountNoAndBankCodeAndPartyId(
						payPlanExtendVO.getBankAccount(), 
						bankCode, 
						partyId);
				
				NBUtils.logger(getClass(), "getBankAccountbyAccountNoAndBankCodeAndPartyId()"+
								(bankAccountVO == null?"null" : bankAccountVO.getAccountId()));
				if (bankAccountVO != null) {
					payPlanExtendVO.setAccountId(bankAccountVO.getAccountId());
				} 
			}

			if (!StringUtils.isNullOrEmpty(payPlanExtendVO.getBankCode())
					&& !StringUtils.isNullOrEmpty(payPlanExtendVO.getBankAccount())) {
			
				if (bankAccountVO == null) {
					bankAccountVO = new BankAccountVO();
					bankAccountVO.setPartyId(partyId);
				}
				String bankCode = payPlanExtendVO.getBankCode();
				bankAccountVO.setBankCode(bankCode);
				bankAccountVO.setBranchCode(null);
				/* 2018/05/23 by Simon
				 * IR-244353-付款帳號只有3碼導致無法匯款:調整一律儲存7碼至BankCode欄位
				if(bankCode.length() > 3){
					//因db僅有bankCode所以有可能是儲存branchCode的資料,所以只取3碼
					bankAccountVO.setBankCode(bankCode.substring(0,3));
				} else if(bankCode.length() == 3){
					//因db僅有bankCode所以有可能是儲存bankCode的資料,所以清空branchCode
					bankAccountVO.setBranchCode(null);
				}
				*/
				bankAccountVO.setBankAccount(payPlanExtendVO.getBankAccount());
				bankAccountVO.setAccountStatus(CodeCst.ACCOUNT_STATUS__VALID); // valid
				bankAccountVO.setAccountType(CodeCst.ACCOUNT_TYPE__GIRO); // Direct Debit value 2
				bankAccountVO.setSwiftNumber("");
				bankAccountVO.setApplyDate(coverageVO.getApplyDate());
				boolean isForeign = paPolicyService.isForeignCurrency(policyId);
				if (isForeign) {
					if (!StringUtils.isNullOrEmpty(bankAccountVO.getBankCode())) {
						String mainBankCode = bankAccountVO.getBankCode() ;
						if(mainBankCode.length() > 3){
							//bankOrg bankCode只有3碼
							mainBankCode = mainBankCode.substring(0,3);
						}
						BankOrgVO bankOrgVO = bankCI.getValidBankOrgbyBankCode(mainBankCode);
						if (bankOrgVO != null) {
							bankAccountVO.setSwiftNumber(bankOrgVO.getSwiftCode());
						}
					}
				} 
				// 姓名
				bankAccountVO.setAccoName(insuredVO.getName());
				if (bankAccountVO.getAccountId() == null) {
					// create
					NBUtils.logger(getClass(), "bankAccountService.createAccount()");
					Long accountId = bankAccountService.createAccount(bankAccountVO);
					payPlanExtendVO.setAccountId(accountId);
					HibernateSession3.currentSession().flush();
				} else {
					// update
					NBUtils.logger(getClass(), "bankAccountService.updateAccount()");
					//bankAccountService.updateAccount(bankAccountVO);
					//HibernateSession3.currentSession().flush();
				}
			} else {
				// clear t_bank_account reference
				payPlanExtendVO.setAccountId(null);
			}
			
			coverageService.saveWithoutCalc(coverageVO);
		}
	}

	/**
	 * <p>Description : 先取得現金收益分配帳戶(T_INVEST_CASH_ACCOUNT)ACCOUNT_ID</p>
	 * <p>Created By : Kathy Yeh</p>
	 * <p>Create Time : Jul 28, 2019</p>
	 * @param policyId, applyDate
	 * @throws Exception 
	 */
 	public void saveBeneType8byInvCashAccount(Long policyId,Date applyDate) {

        List<BeneficiaryVO> beneList = beneficiaryService.findByPolicyIdAndBeneType(policyId,
        		CodeCst.BENEFICIARY_TYPE_08);
        InvestCashAccountVO investCashAccountVO = new InvestCashAccountVO();	
		investCashAccountVO = investCashAccountService.findByPolicyId(policyId);

		//收益分配方式批註申請書中各標的若有採現金給付，以批註申請書提供之要保人帳戶為主給付
        if ((beneList != null && beneList.size() > 0 ) &&
        	(investCashAccountVO != null) &&
        	(!StringUtils.isNullOrEmpty(investCashAccountVO.getBankAccount())) &&
        	(!StringUtils.isNullOrEmpty(investCashAccountVO.getBranchCode())))	
        {
            BeneficiaryVO beneVO = (BeneficiaryVO) beneList.get(0);
            beneVO.setBankCode(investCashAccountVO.getBankCode()); /* 銀行代碼  */
            beneVO.setBranchCode(investCashAccountVO.getBranchCode()); /* 分行代碼  */
            beneVO.setBankAccount(investCashAccountVO.getBankAccount()); /* 帳號  */
            beneVO.setPayMode(CodeCst.BENE_PAY_MODE_2);  /* 匯款  */
            this.saveOrUpdateAccount(beneVO, applyDate);
			beneficiaryService.saveOrUpdate(beneVO);
            investCashAccountVO.setAccountId(beneVO.getAccountId());
            investCashAccountService.saveOrUpdate(investCashAccountVO);
        }
	}

	/**
	 * <p>Description : BC-373 PCR-332528 RTC-336493 投資型定期買回暨投資標的運用期屆滿處理方式，
	 * 新增預設受益人 & T_CONTRACT_BENE_FUND資料</p>
	 * <p>Created By : Yvon Sun</p>
	 * <p>Create Time : Aug 16, 2019</p>
	 *
	 * @param policyId 保單號碼
	 * @return 投資型定期買回暨投資標的運用期屆滿處理方式
	 */
	public void saveTargetMaturityFundBeneficiaryRelatedData(Long policyId) {
		PolicyVO policyVO = paPolicyService.load(policyId);
		List<CoverageVO> coverageVOs = policyVO.getCoverages();
		for (CoverageVO coverageVO : coverageVOs) {
			if (categoryService.isProductTypeS1(coverageVO.getProductId().longValue())) { //目標到期債商品
				List<PremInvestRateVO> investRateVOs = coverageVO.getPremInvestRates();
				TargetMaturityFundVO targetMaturityFundVO = targetMaturityFundService.findByPolicyId(policyId);
				for (PremInvestRateVO investRate : investRateVOs) {
					if (fundService.isLadderWithdrawFund(investRate.getFundCode())) { //商品有定期買回約定
						List<BeneficiaryVO> beneVoList = beneficiaryService.findByPolicyIdAndBeneType(policyId, CodeCst.BENEFICIARY_TYPE_19);
						BeneficiaryVO beneVO = beneVoList == null || beneVoList.size() == 0 ? new BeneficiaryVO() : beneVoList.get(0);
						List<ContractBeneFundVO> beneFundVoList = beneVO.getListId() == null ? null : beneFundService.findBeneFundByBeneListID(beneVO.getListId());
						ContractBeneFundVO beneFundVO = new ContractBeneFundVO();
						if (beneFundVoList != null) {
							for (ContractBeneFundVO tempVO : beneFundVoList) {
								if (tempVO.getFundCode().equals(investRate.getFundCode())) {
									beneFundVO = tempVO;
									break;
								}
							}
						}
						String mMF = fundService.getMMFByFundCode(investRate.getFundCode());
						beneVO.setBeneType(CodeCst.BENEFICIARY_TYPE_19); //定期買回，一定要先setBeneType，否則做sync的時候會存檔，會發生違反not null exception
						beneVO.setItemId(coverageVO.getItemId()); //有設定item id才能在綜合查詢顯示險種代碼
						beneVO = beneficiaryService.syncByPolicyHolder(policyVO, beneVO, policyVO.getPolicyHolder());
						beneFundVO.setPolicyId(policyId);
						beneFundVO.setFundCode(investRate.getFundCode());
						beneFundVO.setTransToFundCode(mMF);
						if (CodeCst.REDEEM_TYPE__REMITTANCE.equals(targetMaturityFundVO.getRedemptionRegularly())) { //1-以現金匯款方式給付
							PolicyHolderVO policyHolder = policyVO.getPolicyHolder();
							SharedCashAccountVO targetMaturityCashAccount = sharedCashAccountService.findByPageIdAndPolicyId(CodeCst.CASH_ACCOUNT_PAGE__TARGET_MATURITY_FUND, policyId);
							Integer currency = policyVO.getCurrency();
							if (currency == CodeCst.MONEY__NTD) { //台幣
								beneVO.setPayMode(CodeCst.BENE_PAY_MODE_2);
							} else { //外幣
								beneVO.setPayMode(CodeCst.BENE_PAY_MODE_4);
							}
							if (targetMaturityCashAccount.getBankCode() != null
									&& targetMaturityCashAccount.getBranchCode() != null
									&& targetMaturityCashAccount.getBankAccount() != null
									&& targetMaturityCashAccount.getAccName() != null) { // 投資型定期買回暨投資標的運用期屆滿處理方式有完整帳戶資料，使用此資料填入受益人
								beneVO.setBankCode(targetMaturityCashAccount.getBankCode());
								beneVO.setBranchCode(targetMaturityCashAccount.getBranchCode());
								beneVO.setBankAccount(targetMaturityCashAccount.getBankAccount());
								beneVO.setAccName(targetMaturityCashAccount.getAccName());
							} else { // 本頁面區塊無輸入帳戶資料，使用要保人帳戶資料填入受益人
								beneVO.setBankCode(policyHolder.getBankCode());
								beneVO.setBranchCode(policyHolder.getBranchCode());
								beneVO.setBankAccount(policyHolder.getOiuBankAccount());
								beneVO.setAccName(policyHolder.getAccEnglishName());
							}
							beneFundVO.setShareRate(BigDecimal.ZERO);
						} else { //本欄空值 或 2-投入投資標的計價幣別貨幣帳戶
							beneVO.setPayMode(CodeCst.BENE_PAY_MODE_8);
							beneFundVO.setShareRate(BigDecimal.ONE);
						}
						beneVO = beneficiaryService.saveOrUpdate(beneVO);
						beneFundVO.setBeneListId(beneVO.getListId());
						beneFundService.saveOrUpdate(beneFundVO);
					}

					if (fundService.getFundFullTermDateByCode(investRate.getFundCode()) != null) { //投資標的基金的滿期日有值表示有運用期屆滿處理
						List<BeneficiaryVO> beneVoList;
						BeneficiaryVO beneVO;
						Map<ProdBizCategory, ProdBizCategorySub> prodBizCategory = categoryService.findProdBizCategories(coverageVO.getProductId().longValue());
						ProdBizCategorySub prodBizCategorySub = prodBizCategory.get(ProdBizCategory.MAIN_02);
						if (prodBizCategorySub == ProdBizCategorySub.SUB_02_TAN
								|| prodBizCategorySub == ProdBizCategorySub.SUB_02_SAN
								|| prodBizCategorySub == ProdBizCategorySub.SUB_02_VA
								|| prodBizCategorySub == ProdBizCategorySub.SUB_02_SNA) { //年金商品
							beneVoList = beneficiaryService.findByPolicyIdAndBeneType(policyId, CodeCst.BENEFICIARY_TYPE_21);
							beneVO = beneVoList == null || beneVoList.size() == 0 ? new BeneficiaryVO() : beneVoList.get(0);
							beneVO.setBeneType(CodeCst.BENEFICIARY_TYPE_21); //年金投資標的運用期屆滿，一定要先setBeneType，否則做sync的時候會存檔，會發生違反not null exception
							beneVO = beneficiaryService.syncByInsured(policyVO, beneVO, policyVO.gitMasterInsured());
						} else { //非年金商品
							beneVoList = beneficiaryService.findByPolicyIdAndBeneType(policyId, CodeCst.BENEFICIARY_TYPE_20);
							beneVO = beneVoList == null || beneVoList.size() == 0 ? new BeneficiaryVO() : beneVoList.get(0);
							beneVO.setBeneType(CodeCst.BENEFICIARY_TYPE_20); //投資標的運用期屆滿，一定要先setBeneType，否則做sync的時候會存檔，會發生違反not null exception
							beneVO = beneficiaryService.syncByPolicyHolder(policyVO, beneVO, policyVO.getPolicyHolder());
						}
						beneVO.setItemId(coverageVO.getItemId()); //有設定item id才能在綜合查詢顯示險種代碼
						List<ContractBeneFundVO> beneFundVoList = beneVO.getListId() == null ? null : beneFundService.findBeneFundByBeneListID(beneVO.getListId());
						ContractBeneFundVO beneFundVO = new ContractBeneFundVO();
						String mMF = fundService.getMMFByFundCode(investRate.getFundCode());
						if (beneFundVoList != null) {
							for (ContractBeneFundVO tempVO : beneFundVoList) {
								if (tempVO.getFundCode().equals(investRate.getFundCode())) {
									beneFundVO = tempVO;
									break;
								}
							}
						}
						beneFundVO.setPolicyId(policyId);
						beneFundVO.setFundCode(investRate.getFundCode());
						beneFundVO.setTransToFundCode(mMF);
						if (CodeCst.REDEEM_TYPE__REMITTANCE.equals(targetMaturityFundVO.getEndOfThePeriod())) { //1-以現金匯款方式給付
							PolicyHolderVO policyHolder = policyVO.getPolicyHolder();
							SharedCashAccountVO targetMaturityCashAccount = sharedCashAccountService.findByPageIdAndPolicyId(CodeCst.CASH_ACCOUNT_PAGE__TARGET_MATURITY_FUND, policyId);
							if (policyVO.getCurrency() == CodeCst.MONEY__NTD) { //台幣
								beneVO.setPayMode(CodeCst.BENE_PAY_MODE_2);
							} else { //外幣
								beneVO.setPayMode(CodeCst.BENE_PAY_MODE_4);
							}
							if (targetMaturityCashAccount.getBankCode() != null
									&& targetMaturityCashAccount.getBranchCode() != null
									&& targetMaturityCashAccount.getBankAccount() != null
									&& targetMaturityCashAccount.getAccName() != null) { // 投資型定期買回暨投資標的運用期屆滿處理方式有完整帳戶資料，使用此資料填入受益人
								beneVO.setBankCode(targetMaturityCashAccount.getBankCode());
								beneVO.setBranchCode(targetMaturityCashAccount.getBranchCode());
								beneVO.setBankAccount(targetMaturityCashAccount.getBankAccount());
								beneVO.setAccName(targetMaturityCashAccount.getAccName());
							} else { // 本頁面區塊無輸入帳戶資料，使用要保人帳戶資料填入受益人
								beneVO.setBankCode(policyHolder.getBankCode());
								beneVO.setBranchCode(policyHolder.getBranchCode());
								beneVO.setBankAccount(policyHolder.getOiuBankAccount());
								beneVO.setAccName(policyHolder.getAccEnglishName());
							}
							beneFundVO.setShareRate(BigDecimal.ZERO);
						} else { //本欄空值 或 2-投入投資標的計價幣別貨幣帳戶
							beneVO.setPayMode(CodeCst.BENE_PAY_MODE_8);
							beneFundVO.setShareRate(BigDecimal.ONE);
						}
						beneVO = beneficiaryService.saveOrUpdate(beneVO);
						beneFundVO.setBeneListId(beneVO.getListId());
						beneFundService.saveOrUpdate(beneFundVO);
					}
				}
				break;
			}
		}
	}
	
	/**
	 * <p>Description : setBentficiaryBankAccount </p>
	 * <p>Created By : Alex Cheng</p>
	 * <p>Create Time : Nov 26, 2015</p>
	 * @param inputVO
	 * @param beneficiaryVO
	 * @param applyDate
	 */
	private void setBentficiaryBankAccount(BankAccountVO inputVO,
			BeneficiaryVO beneficiaryVO, Date applyDate) {

		/* 2018/05/23 by Simon
		 * IR-244353-付款帳號只有3碼導致無法匯款:調整一律儲存7碼至BankCode欄位
		 */
		if(StringUtils.isNullOrEmpty(beneficiaryVO.getBranchCode())) {
			inputVO.setBankCode(beneficiaryVO.getBankCode());	
		}else {
			inputVO.setBankCode(beneficiaryVO.getBranchCode());	
		}
		inputVO.setBranchCode(null);
		inputVO.setBankAccount(beneficiaryVO.getBankAccount());
		inputVO.setAccountStatus(CodeCst.ACCOUNT_STATUS__VALID); // valid
		inputVO.setAccountType(CodeCst.ACCOUNT_TYPE__GIRO); // Direct Debit value 2
		inputVO.setSwiftNumber("");
		boolean isForeign = paPolicyService.isForeignCurrency(beneficiaryVO.getPolicyId());
		/* 若是外幣保單 (受益人)畫面輸入的英文姓名 */
		if (isForeign) {
			if(StringUtils.isNullOrEmpty(beneficiaryVO.getAccEnglishName())) {
				if(!StringUtils.isNullOrEmpty(beneficiaryVO.getName())) {
					inputVO.setAccoName(beneficiaryVO.getName());	
				}
			} else {
				inputVO.setAccoName(beneficiaryVO.getAccEnglishName());	
			}
			
			if (!StringUtils.isNullOrEmpty(beneficiaryVO.getBankCode())) {
				String mainBankCode = beneficiaryVO.getBankCode() ;
				if(mainBankCode.length() > 3){
					//bankOrg bankCode只有3碼
					mainBankCode = mainBankCode.substring(0,3);
				}
				BankOrgVO bankOrgVO = bankCI.getValidBankOrgbyBankCode(mainBankCode);
				if (bankOrgVO != null) {
					inputVO.setSwiftNumber(bankOrgVO.getSwiftCode());
				}
			}
		} else {
			// 否則 (受益人)畫面輸入的姓名
			if(!StringUtils.isNullOrEmpty(beneficiaryVO.getName())) {
				inputVO.setAccoName(beneficiaryVO.getName());	
			}
		}
 
		if (!StringUtils.isNullOrEmpty(beneficiaryVO.getCertiCode())) {
			inputVO.setCertiCode(beneficiaryVO.getCertiCode());
			inputVO.setCertiType(beneficiaryVO.getCertiType());
			
		}
		if (applyDate == null) {
			// 若T_CONTRACT_MASTER.APPLY_DATE無值先放系統日
			applyDate = AppContext.getCurrentUserLocalTime();
		}
		inputVO.setApplyDate(applyDate);
	}

	public void updateInsuredListDisability(Long underwriteId, boolean isNewbiz) {
		Collection<UwLifeInsuredVO> uwInsuredList = uwPolicyDS
				.findUwLifeInsuredEntitis(underwriteId);
		for (UwLifeInsuredVO uwLifeInsured : uwInsuredList) {
			uwProcessDS.updateInsuredListDisability(uwLifeInsured, isNewbiz);
		}
	}

	/**
	 * <p>Description : 取得畫面上被保險人修改資料</p>
	 * <p>Created By : ChiaHui Su</p>
	 * <p>Create Time : Feb 23, 2016</p>
	 * @param request
	 * @return
	 */
	public List<UwLifeInsuredVO> getInsuredList(
			HttpServletRequest request, String sourceType) {
		String[] lifeInsuredIds = request.getParameterValues("lifeInsuredId");
		String[] medicalExamIndis = request.getParameterValues("medicalExamIndi");
		String[] imageIndis = request.getParameterValues("imageIndi");
		String[] disabilityIndis = request.getParameterValues("disabilityIndi");
		String[] hoLifeSurveyIndis = request.getParameterValues("hoLifeSurveyIndi");
		String[] guardianAncmnts = request.getParameterValues("guardianAncmnt");
		
		String isGuardianAncmnt = EscapeHelper.escapeHtml(request .getParameter("isGuardianAncmnt"));
	
		List<UwLifeInsuredVO> list = new ArrayList<UwLifeInsuredVO>();
		if (lifeInsuredIds != null) {
			for (int i = 0; i < medicalExamIndis.length; i++) {
				String[] info = lifeInsuredIds[i].split(",");
				UwLifeInsuredVO vo =
						uwPolicyDS.findUwLifeInsuredByListId(
								Long.valueOf(info[0]),
								Long.valueOf(info[1]));
				vo.setMedicalExamIndi(medicalExamIndis[i]); //是否體檢
				vo.setImageIndi(imageIndis[i]); //是否調閱影像 
				vo.setDisabilityIndi(disabilityIndis[i]); //身心障礙
				if (disabilityIndis[i] != null && disabilityIndis[i].equals("N")) {
					vo.setDisabilityCategory("00");
					vo.setDisabilityClass("0");
					vo.setDisabilityType("000");
				}
				vo.setHoLifeSurveyIndi(hoLifeSurveyIndis[i]);//內勤人員生調
				
				//PCR_391319 新式公會-新增被保險人受監護宣告  20201030 by Simon
				if(guardianAncmnts != null) {
					vo.setGuardianAncmnt(guardianAncmnts[i]);	
				} else {
					//若保單層受監護宣告為否或空白,所有被保險人Default=否,不可修改
					if(CodeCst.YES_NO__YES.equals(isGuardianAncmnt) == false) {						 
						//PCR-524169,保全核保，不參考保單層值 -start   
						if (Utils.isCsUw(sourceType) ) { //POS邏輯																
							  if (vo.getGuardianAncmnt() ==null) {
								  vo.setGuardianAncmnt(CodeCst.YES_NO__NO);
							  }							
						} else {	//UNB-原邏輯
							vo.setGuardianAncmnt(CodeCst.YES_NO__NO);
						}
	                   //PCR-524169,保全核保，不參考保單層值 -end
					}
				}
				list.add(vo);
			}
		}
		return list;
	}
	
	//RTC-240914-[PROD]核保通過存檔時系統產生資訊提示訊息 by simon( 2018/04/20 )
	//因前面處理policy_bank_auth 會寫入資料，已在bankAccountService.getBankAccountbyAccountNoAndBankCodeAndPartyId()
	//執行flush()但production還是會有查不到此session寫入的資料,改用PolicyBankAuth來比對
	private BankAccountVO getBankAccountbyAccountNoAndBankCodeAndPartyId(String accountNumber, 
					String bankCode, Long partyId){

		Session session = HibernateSession3.currentSession();

		StringBuffer buf = new StringBuffer();
		buf.append("select * from T_BANK_ACCOUNT ");
		buf.append(" where 1=1 ");
		buf.append("   and BANK_CODE = ?");
		buf.append("   and BANK_ACCOUNT = ?");
		buf.append("   and PARTY_ID = ?");

		SQLQuery query =  session.createSQLQuery(buf.toString());
		query.setParameter(0, bankCode);
		query.setParameter(1, accountNumber);
		query.setParameter(2, partyId);
		query.addEntity("BankAccount",BankAccount.class, LockMode.READ);
		BankAccount bankAccount =  (BankAccount) query.uniqueResult();
		if(bankAccount != null)
		{
			BankAccountVO bankAccountVO = new BankAccountVO();
			bankAccount.copyToVO(bankAccountVO, false);
			return bankAccountVO;
		}
		return null;
	}

	public LIACI getLiaCI() {
		return liaCI;
	}

	public void setLiaCI(LIACI liaCI) {
		this.liaCI = liaCI;
	}

	@Resource(name = LIACI.BEAN_DEFAULT)
	private LIACI liaCI;

	@Resource(name = ChargeDeductionCI.BEAN_DEFAULT)
	private ChargeDeductionCI chargeDeductionCI;

	public ChargeDeductionCI getChargeDeductionCI() {
		return chargeDeductionCI;
	}

	public void setChargeDeductionCI(ChargeDeductionCI chargeDeductionCI) {
		this.chargeDeductionCI = chargeDeductionCI;
	}

	@Resource(name = UwProcessService.BEAN_DEFAULT)
	private UwProcessService uwProcessDS;

	@Resource(name = UwInsuredDisabilityService.BEAN_DEFAULT)
	private UwInsuredDisabilityService uwInsuredDisbilityDS;

	public UwProcessService getUwProcessDS() {
		return uwProcessDS;
	}

	public void setUwProcessDS(UwProcessService uwProcessDS) {
		this.uwProcessDS = uwProcessDS;
	}

	@Resource(name = UwPolicyService.BEAN_DEFAULT)
	private UwPolicyService uwPolicyDS;

	public UwPolicyService getUwPolicyDS() {
		return uwPolicyDS;
	}

	public void setUwPolicyDS(UwPolicyService uwPolicyDS) {
		this.uwPolicyDS = uwPolicyDS;
	}

	@Resource(name = CoverageCI.BEAN_DEFAULT)
	private CoverageCI coverageCI;

	/**
	 * @return the coverageCI
	 */
	public CoverageCI getCoverageCI() {
		return coverageCI;
	}

	/**
	 * @param coverageCI the coverageCI to set
	 */
	public void setCoverageCI(CoverageCI coverageCI) {
		this.coverageCI = coverageCI;
	}

    @Resource(name = ProductService.BEAN_DEFAULT)
    private ProductService productService;

    @Resource(name = InsuredService.BEAN_DEFAULT)
    private InsuredService insuredService;

    public ProductService getProductService() {
        return productService;
    }

    public void setProductService(ProductService productService) {
        this.productService = productService;
    }

    @Resource(name = AgentService.BEAN_DEFAULT)
    private AgentService agentService;

    @Resource(name = LifeProductService.BEAN_DEFAULT)
    private LifeProductService lifeProductService;

    @Resource(name = AlterationItemService.BEAN_DEFAULT)
    private AlterationItemService alterationItemService;

    // PCR364900-新契約補全建檔 
    @Resource(name = NbTaskService.BEAN_DEFAULT)
    private NbTaskService nbTaskServiceCI;
    
    @Resource(name = RemoteHelper.BEAN_DEFAULT)
    protected RemoteHelper paRemoteHelper;

    @Resource(name = CashCI.BEAN_DEFAULT)
    private CashCI cashCI;

    @Resource(name = PolicyCI.BEAN_DEFAULT)
    protected PolicyCI paPolicyCI;

    @Resource(name = ApplicationService.BEAN_DEFAULT)
    private ApplicationService applicationService;

    @Resource(name = PolicyDS.BEAN_DEFAULT)
    private PolicyDS qryPolicyDS;

    @Resource(name = CsTaskTransService.BEAN_DEFAULT)
    private CsTaskTransService csTaskTransService;

    @Resource(name = PremCI.BEAN_DEFAULT)
    protected PremCI premCI;

    @Resource(name = EddaDao.BEAN_DEFAULT)
    private EddaDao<Edda> eddaDao;

    @Resource(name = TUwInsuredListDelegate.BEAN_DEFAULT)
    private TUwInsuredListDelegate uwInsuredListDao;

    @Resource(name = BankCI.BEAN_DEFAULT)
    private BankCI bankCI;

    @Resource(name = UwTransferHelper.BEAN_DEFAULT)
    private UwTransferHelper uwTransferHelper;

    @Resource(name = UwRiApplyService.BEAN_DEFAULT)
    private UwRiApplyService uwRiApplyService;

    @Resource(name = PolicyRoleHelper.BEAN_DEFAULT)
    protected PolicyRoleHelper policyRoleHelper;

    @Resource(name = DisabilityCancelDetailService.BEAN_DEFAULT)
    private DisabilityCancelDetailService disabilityCancelDetailService;

    @Resource(name = ValidatorService.BEAN_DEFAULT)
    ValidatorService paValidatorService;

    @Resource(name = NbPolicySpecialRuleService.BEAN_DEFAULT)
    NbPolicySpecialRuleService paNbPolicySpecialRuleService;

    @Resource(name = PolicyService.BEAN_DEFAULT)
    PolicyService policyService;

    @Resource(name = UwCommentService.BEAN_DEFAULT)
    private UwCommentService uwCommentService;

    @Resource(name = PolicyBankAuthService.BEAN_DEFAULT)
    PolicyBankAuthService policyBankAuthService;

    @Resource(name = PolicyHolderService.BEAN_DEFAULT)
    protected PolicyHolderService policyHolderService;

    @Resource(name = BankAccountService.BEAN_DEFAULT)
    private BankAccountService bankAccountService;

    @Resource(name = RiskCustomerService.BEAN_DEFAULT)
    RiskCustomerService riskCustomerService;

    @Resource(name = CoverageService.BEAN_DEFAULT)
    protected CoverageService coverageService;

    //PCR-307485 VLO 取得受益人帳戶(beneType = 8) 
    //2019/07/28 Add by Kathy
    @Resource(name = BeneficiaryService.BEAN_DEFAULT)
    private BeneficiaryService beneficiaryService;

    //PCR-307485 VLO 取得收益給付方式帳戶資訊
    //2019/07/28 Add by Kathy
    @Resource(name = InvestCashAccountService.BEAN_DEFAULT)
    private InvestCashAccountService investCashAccountService;

    //BC-373 PCR-332528 RTC-336493 判斷商品是否為目標到期債基金 2019/08/26 Add by Yvon Sun
    @Resource(name = LifeProductCategoryService.BEAN_DEFAULT)
    private LifeProductCategoryService categoryService;

    //BC-373 PCR-332528 RTC-336493 儲存受益人投資相關資料 2019/08/26 Add by Yvon Sun
    @Resource(name = ContractBeneFundService.BEAN_DEFAULT)
    private ContractBeneFundService beneFundService;

    //BC-373 PCR-332528 RTC-336493 判斷投資標的基金是否有定期買回約定或滿期日 2019/08/26 Add by Yvon Sun
    @Resource(name = FundService.BEAN_DEFAULT)
    private FundService fundService;

    //BC-373 PCR-332528 RTC-336493 投資型定期買回暨投資標的運用期屆滿處理方式資料 2019/08/26 Add by Yvon Sun
    @Resource(name = TargetMaturityFundService.BEAN_DEFAULT)
    private TargetMaturityFundService targetMaturityFundService;

    //BC-373 PCR-332528 RTC-336493 用來同步要、被保人資料到受益人的保單資料 2019/08/26 Add by Yvon Sun
    @Resource(name = com.ebao.ls.pa.pub.bs.PolicyService.BEAN_DEFAULT)
    private com.ebao.ls.pa.pub.bs.PolicyService paPolicyService;

    //BC-373 PCR-332528 RTC-336493 新契約共用的金融帳戶資訊 2019/08/26 Add by Yvon Sun
    @Resource(name = SharedCashAccountService.BEAN_DEFAULT)
    private SharedCashAccountService sharedCashAccountService;
}
