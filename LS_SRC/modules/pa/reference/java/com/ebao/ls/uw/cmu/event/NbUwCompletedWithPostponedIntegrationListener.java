package com.ebao.ls.uw.cmu.event;

import com.ebao.foundation.commons.annotation.TriggerPoint;
import com.ebao.ls.cmu.event.NbUwCompletedWithPostponed;
import com.ebao.pub.event.AbstractIntegrationListener;
import com.ebao.pub.integration.IntegrationMessage;

public class NbUwCompletedWithPostponedIntegrationListener
    extends
      AbstractIntegrationListener<NbUwCompletedWithPostponed> {
  final static String code = "uw.nbuwCompleted.postponed";

  @Override
  @TriggerPoint(component = "Underwriting", description = "During manual underwriting, if the underwriting decision is 'Postponed' and has been submitted successfully,  the event is triggered.<br/>Menu navigation: New business > Worklist > Underwriting.<br/>", event = "direct:event."
      + code, id = "com.ebao.tp.ls." + code)
  protected String getEventCode(NbUwCompletedWithPostponed event,
      IntegrationMessage<Object> in) {
    return "event." + code;
  }
}
