package com.ebao.ls.uw.ctrl;

/*
 * <p>Title: GEL-UW</p>
 * <p>Description: UW module request parameter name constant</p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: eBaoTech Corporation.</p>
 * @author jason.luo
 * @version 1.0
 * @since 08.23.2004
 */

/**
 * UW module request parameter name constant
 */
public class ReqParamNameConstants {

  /**
   * Request parameter name of underwrite id
   */
  //underwriteId
  public static final String POLICY_UNDERWRITEID = "underwriteId";

  /**
   * Request parameter name of benefit item id
   */
  //itemId
  public static final String BENEFIT_ITEM_ID = "itemId";

  /**
   * Request parameter name of underwriteId
   */
  //policyId
  public static final String POLICY_ID = "policyId";

  /**
   * Request parameter name of the proposalNO
   *   used at UnderwritingHistoryQuery
   */
  //applyCode
  public static final String PROPOSALNO = "proposalNO";

  /**
   * Request parameter name of the policyCode
   */
  //policyCode
  public static final String POLICYCODE = "policyCode";

  /**
   * Request parameter name of the uwAppTypeCode
   *   used at UnderwritingHistoryQuery
   */
  public static final String UW_APP_TYPE_CODE = "uwAppTypeCode";

  /**
   * Request parameter name of the displayStatus
   *   used at UnderwritingHistoryQuery
   */
  //uwStatus[validated uw history - 4, all uw history - others]
  public static final String HISTORY_DISPLAY_STATUS = "displayStatus";

  /**
   * Request parameter name of the commencement date
   */
  //validateDate
  public static final String POLICY_COMMENCEMENT_DATE_STRING = "renewValidateDate";

  /**
   * Request parameter name of the underwriting comment
   */
  //uwNotes
  public static final String POLICY_UNDERWRITING_COMMENT = "uwComment";
  public static final String POLICY_REVERSAL_REASON = "reversalReason";
  public static final String POLICY_SPECIAP_COMM_INDI = "specialCommIndi";
  public static final String POLICY_GIB_STATUS = "gibStatus";
  /**
   * Request parameter name of the risk commencement date
   */
  //actualValidate
  public static final String POLICY_RISK_COMMENCEMENT_DATE_STRING = "riskCommencementDate";

  /**
   * Request parameter name of the discount type
   */
  //dntType - currently all the discount has been recorded at benefit level
  public static final String POLICY_DISCOUNT_TYPE = "dsntType";

  /**
   * Request parameter name of the standard life indicator mixin
   *   currently presented using the following format
   *   T_UW_INSURED_LIST.UW_LIST_ID,T_UW_INSURED_LIST.STAND_LIFE
   */
  public static final String BENEFIT_STAND_LIFE_IND_MIXIN = "standLifeIndiMixin";

  /**
   * Request parameter name of the uwdecision mixin
   *   currently presented using the following format
   *   T_UW_INSURED_LIST.UW_LIST_ID,T_UW_INSURED_LIST.MEDICAL_EXAM_INDI 
   */
  public static final String BENEFIT_UW_MEDICAL_MIXIN = "uwMedicalIndiMixin";

  /**
   * Request parameter name of the pending reason
   */
  //uwReason
  public static final String POLICY_PENDING_REASON = "pendingReason";

  /**
   * Request parameter name of the generate lca indicator
   */
  //generateLcaIndi
  public static final String POLICY_GENERATE_LCA_INDI = "generateLcaIndi";

  /**
   * Request parameter name of the generate lca indicator
   */
  //lcaDateIndi
  public static final String POLICY_UPDATE_LCA_DATE_INDI = "lcaDateIndi";

  /**
   * Request parameter name of the consent given indicator
   */
  //consentGivenIndi
  public static final String POLICY_CONSENT_GIVEN_INDI = "consentGivenIndi";

  /*
   * All of the endorsement condition and exclusion related parameter had been
   * uncomment at 10.12.2004
   * because all of the things mentioned above had been revised to 
   * committed on one go for system performance reason. 
   */
  /**
   * Request parameter name of endorsement code
   */
  //public static final String POLICY_ADD_ENDORSEMENTCODE = "endorsementCodeName";
  /**
   * Request parameter name of endorsementDescLang1
   */
  //public static final String POLICY_ENDORSEMENT_DESC_LAGN1 = "endorsementCodeDescLang1";
  /**
   * Request parameter name of endorsementDescLang2
   */
  //public static final String POLICY_ENDORSEMENT_DESC_LAGN2 = "endorsementCodeDescLang2";
  /**
   * Request parameter name of endorsementCodeId(used at delete endorsementCode)
   */
  //public static final String POLICY_ENDORSEMENT_CODEIDS = "endorsementCodeId";
  /**
   * Request parameter name of condition code
   */
  //public static final String POLICY_ADD_CONDITIONCODE = "conditionCodeName";
  /**
   * Request parameter name of conditionDescLang1
   */
  //public static final String POLICY_CONDITION_DESC_LAGN1 = "conditionCodeDescLang1";
  /**
   * Request parameter name of conditionDescLang2
   */
  //public static final String POLICY_CONDITION_DESC_LAGN2 = "conditionCodeDescLang2";
  /**
   * Request parameter name of conditionCodeIds(used at delete conditionCode)
   */
  //public static final String POLICY_CONDITION_CODEIDS = "conditionCodeId";
  /**
   * Request parameter of exclusion code
   */
  //public static final String POLICY_ADD_EXCLUSIONCODE  = "exclusionCodeName";
  /**
   * Request parameter of exclusion code
   */
  public static final String POLICY_ADD_REVIEW_PERIOD = "reviewPeriod";

  /**
   * Request parameter of exclusionDescLang1
   */
  //public static final String POLICY_EXCLUSION_DESC_LAGN1 = "exclusionCodeDescLang1";
  /**
   * Request parameter of exclusionDescLang2
   */
  //public static final String POLICY_EXCLUSION_DESC_LAGN2 = "exclusionCodeDescLang2";
  /**
   * Request parameter name of exclusionCodeIds(used at delete exclusionCode)
   */
  //public static final String POLICY_EXCLUSION_CODEIDS = "exclusionCodeId";
  /**
   * Request parameter name of exclusionCodeMixin
   * Format:~~endorsementCode(mandatory)~~English Text(optional)~~Bahasa Text(optional)
   */
  public static final String POLICY_ENDORSEMENT_MIXIN = "endorsementCodeMixinHidden";

  /**
   * Request parameter name of exclusionCodeMixin
   * Format:~~conditionCode(mandatory)~~English Text(optional)~~Bahasa Text(optional)
   */
  public static final String POLICY_CONDITION_MIXIN = "conditionCodeMixinHidden";

  /**
   * Request parameter name of exclusionCodeMixin
   * Format:~~exclusionCode(mandatory)~~ReviewPeriod(optional)~~English Text(optional)~~Bahasa Text(optional)
   */
  public static final String POLICY_EXCLUSION_MIXIN = "exclusionCodeMixinHidden";

  /**
   * Default Constructor
   */
  //prevents initialization
  private ReqParamNameConstants() {

  }

}
