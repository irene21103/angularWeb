package com.ebao.ls.uw.ds;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.hibernate.criterion.Restrictions;
import org.hibernate.util.StringHelper;

import com.ebao.ls.cmu.service.DocumentService;
import com.ebao.ls.foundation.service.GenericServiceImpl;
import com.ebao.ls.pa.nb.bs.rule.ProposalRuleResultService;
import com.ebao.ls.pa.nb.vo.ProposalRuleResultVO;
import com.ebao.ls.pa.pub.data.bo.UwSickFormItem;
import com.ebao.ls.pa.pub.data.bo.UwSickFormLetter;
import com.ebao.ls.pa.pub.vo.UwSickFormItemVO;
import com.ebao.ls.pa.pub.vo.UwSickFormLetterVO;
import com.ebao.ls.pub.CodeCst;
import com.ebao.ls.pub.cst.ProposalRuleSourceType;
import com.ebao.ls.pub.cst.ProposalRuleStatus;
import com.ebao.ls.uw.data.UwSickFormItemDelegate;
import com.ebao.ls.uw.data.UwSickFormLetterDelegate;
import com.ebao.pub.framework.RTException;
import com.ebao.pub.util.DBean;

/**
 * <p>Title: 核保頁面_疾病問卷照會列表</p>
 * <p>Description: 核保頁面_疾病問卷照會列表</p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company: TGL Co., Ltd.</p>
 * <p>Create Time: Apr 20, 2016</p> 
 * @author 
 * <p>Update Time: Apr 20, 2016</p>
 * <p>Updater: ChiaHui Su</p>
 * <p>Update Comments: </p>
 */
public class UwSickFormLetterServiceImpl extends GenericServiceImpl<UwSickFormLetterVO, UwSickFormLetter, UwSickFormLetterDelegate>

		implements UwSickFormLetterService {

	@Resource(name = UwSickFormLetterDelegate.BEAN_DEFAULT)
	private UwSickFormLetterDelegate uwSickFormLetterDao;

	@Resource(name = UwSickFormItemDelegate.BEAN_DEFAULT)
	private UwSickFormItemDelegate uwSickFormItemDao;

	@Override
	protected UwSickFormLetterVO newEntityVO() {
		return new UwSickFormLetterVO();
	}

	@Override
	public List<UwSickFormLetterVO> findByPolicyInsured(Long underwriteId, Long insuredId, String status) {

		List<UwSickFormLetter> uwSickFormLetters = uwSickFormLetterDao.findByPolicyInsured(
				underwriteId, insuredId, status);
		List<UwSickFormLetterVO> vos = new ArrayList<UwSickFormLetterVO>();
		for (UwSickFormLetter letter : uwSickFormLetters) {
			UwSickFormLetterVO vo = new UwSickFormLetterVO();
			letter.copyToVO(vo, false);
			vos.add(vo);
		}
		return vos;
	}

	@Override
	public void delete(UwSickFormLetterVO vo) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("sickFormLetterId", vo.getListId());
		List<UwSickFormItem> items = uwSickFormItemDao.find(params);
		for (UwSickFormItem item : items) {
			uwSickFormItemDao.remove(item);
		}
		uwSickFormLetterDao.remove(uwSickFormLetterDao.load(vo.getListId()));

	}

	@Resource(name = DocumentService.BEAN_DEFAULT)
	private DocumentService documentService;

	@Resource(name = UwSickFormItemService.BEAN_DEFAULT)
	private UwSickFormItemService itemService;

	@Resource(name = ProposalRuleResultService.BEAN_DEFAULT)
	protected ProposalRuleResultService proposalRuleResultService;

	@Override
	/**
	 * 刪除Letter
	 */
	public void drop(UwSickFormLetterVO letterVO) {
		if (letterVO == null || letterVO.getListId() == null) {
			return;
		}
		Map<String, UwSickFormItemVO> map = itemService.findByLetterId(letterVO.getListId());
		for (String item : map.keySet()) {
			UwSickFormItemVO vo = map.get(item);
			if (vo.getMsgId() != null) {
				/**若有核保訊息需要系統關閉**/
				ProposalRuleResultVO msgVO = proposalRuleResultService.load(vo.getMsgId());
				msgVO.setRuleStatus(CodeCst.PROPOSAL_RULE_STATUS__SYSTEMCLOSED);
				proposalRuleResultService.save(msgVO);
			}
			itemService.remove(vo.getListId());
		}
		this.remove(letterVO.getListId());
	}

	@Override
	/**
	 * 更新疾病Letter的疾病項目
	 */
	public void update(UwSickFormLetterVO letterVO, List<String> sickFormList) {
		Long letterId = letterVO.getListId();
		Map<String, UwSickFormItemVO> map = itemService.findByLetterId(letterId);
		for (String item : sickFormList) {
			if (map.containsKey(item)) {
				map.remove(item);
			} else {
				UwSickFormItemVO vo = new UwSickFormItemVO();
				vo.setSickFormLetterId(letterId);
				vo.setSickFormItem(item);
				itemService.save(vo);
			}
		}
		if (!map.isEmpty()) {
			for (String key : map.keySet()) {
				itemService.remove(map.get(key).getListId());
			}
		}

	}

	@Override
	/**
	 * 新增疾病Letter
	 */
	public void addNewLetter(Long underwriteId, Long insuredId,
			List<String> sickFormList, String status) {
		UwSickFormLetterVO letter = new UwSickFormLetterVO();
		letter.setUnderwriteId(underwriteId);
		letter.setInsuredId(insuredId);
		letter.setStatus(status);
		letter = save(letter);
		for (String item : sickFormList) {
			UwSickFormItemVO vo = new UwSickFormItemVO();
			vo.setSickFormLetterId(letter.getListId());
			vo.setSickFormItem(item);
			itemService.save(vo);
		}

	}

	@Override
	/**
	 * 更新規則校驗產生的疾病問卷項目
	 */
	public void updateDefaultSickForm(Long underwriteId, Long insuredId,
			List<String> newSickFormList) {
		List<UwSickFormLetter> letterVOs = uwSickFormLetterDao.findByPolicyInsured(underwriteId, null, "0");
		/**比對原本的項目**/
		if (letterVOs != null && !letterVOs.isEmpty()) {
			UwSickFormLetterVO letterVO = new UwSickFormLetterVO();
			letterVOs.get(0).copyToVO(letterVO, false);
			update(letterVO, newSickFormList);
		} else {
			/**原本沒有預設的項目需新增letter**/
			addNewLetter(underwriteId, insuredId, newSickFormList, "0");
		}

	}

	@Override
	public Map<Long, UwSickFormLetterVO> getDefaultSickFormLetterMap(
			Long underwriteId) {
		List<UwSickFormLetter> letterVOs = uwSickFormLetterDao.findByPolicyInsured(underwriteId, null, "0");
		Map<Long, UwSickFormLetterVO> map = new HashMap<Long, UwSickFormLetterVO>();
		for (UwSickFormLetter letter : letterVOs) {
			UwSickFormLetterVO vo = new UwSickFormLetterVO();
			letter.copyToVO(vo, false);
			map.put(letter.getInsuredId(), vo);
		}
		return map;
	}

	@Override
	/**
	 * 取得最新一筆的疾病問卷(非規則校驗產生的項目設定,status <> 0)
	 */
	public UwSickFormLetterVO getLetterVO(Long underwriteId, Long insuredId) {
		List<UwSickFormLetter> letterVOs = uwSickFormLetterDao.findByPolicyInsured(underwriteId, insuredId, null);
		for (UwSickFormLetter letter : letterVOs) {
			if (!"0".equals(letter.getStatus())) {
				UwSickFormLetterVO vo = new UwSickFormLetterVO();
				letter.copyToVO(vo, false);
				return vo;
			}
		}
		return null;
	}

	@Override
	/**
	 * 取得underwriteId 下所有被保人, 最新一筆的疾病問卷(非規則校驗產生的項目設定,status <> 0)
	 */
	public Map<Long, UwSickFormLetterVO> getLetterMap(Long underwriteId) {
		List<UwSickFormLetter> letterVOs = uwSickFormLetterDao.findByPolicyInsured(underwriteId, null, null);
		Map<Long, UwSickFormLetterVO> map = new HashMap<Long, UwSickFormLetterVO>();
		for (UwSickFormLetter letter : letterVOs) {
			if (!"0".equals(letter.getStatus())) {
				/**只取最新飛預設的哪一筆**/
				if (map.get(letter.getInsuredId()) == null) {
					UwSickFormLetterVO vo = new UwSickFormLetterVO();
					letter.copyToVO(vo, false);
					map.put(letter.getInsuredId(), vo);
				}
			}
		}
		return map;
	}

	@Override
	public void updateDocumentId(List<Long> letterIdList, Long documentId) {

		if (letterIdList.size() > 0) {

			DBean db = null;
			PreparedStatement pst = null;
			ResultSet rs = null;
			StringBuffer sb = new StringBuffer();

			String params = StringHelper.repeat("?" + ", ", letterIdList.size() - 1)
					+ "?";

			sb.append(" update t_uw_sick_form_letter set document_Id = ?");
			sb.append(" 	where  list_id in (" + params + ")");
			try {
				db = new DBean();
				db.connect();
				pst = db.getConnection().prepareStatement(sb.toString());

				int index = 1;

				pst.setLong(index++, documentId);

				for (int i = 0; i < letterIdList.size(); i++) {
					pst.setLong(index++, letterIdList.get(i));
				}
				pst.execute();

			} catch (SQLException e) {
				throw new RTException(e);
			} catch (ClassNotFoundException e) {
				throw new RTException(e);
			} finally {
				DBean.closeAll(rs, pst, db);
			}
		}
	}

	@Override
	public void removeByInsured(Long policyId, Long insuredId) {
		
		List<UwSickFormLetter> letterList = uwSickFormLetterDao.find(
				Restrictions.eq("insuredId", insuredId));
		
		for (UwSickFormLetter letter : letterList) {
			//RTC-193908-關閉未發放且待處理中的問卷核保訊息
			List<ProposalRuleResultVO> ruleResultList = proposalRuleResultService.find(
					Restrictions.eq("policyId", policyId),
					Restrictions.eq("sourceType", ProposalRuleSourceType.PROPOSAL_RULE_SOURCE_TYPE__SICK_FORM),
					Restrictions.eq("sourceId", letter.getListId()),
					Restrictions.eq("ruleStatus", ProposalRuleStatus.PROPOSAL_RULE_STATUS__PENDING));

			for(ProposalRuleResultVO ruleResult : ruleResultList){
				ruleResult.setRuleStatus(ProposalRuleStatus.PROPOSAL_RULE_STATUS__SYSTEMCLOSED);
			}
			
			proposalRuleResultService.saveRuleResults(ruleResultList);
			//END RTC-193908

			uwSickFormItemDao.deleteItem(letter.getListId());
			uwSickFormLetterDao.remove(letter);
		}
	}

}
