package com.ebao.ls.uw.ctrl.letter;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.UserTransaction;

import org.apache.commons.lang3.StringUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ebao.foundation.common.context.AppUserContext;
import com.ebao.foundation.module.web.action.ExceptionInfoUtils;
import com.ebao.ls.chl.service.AgentService;
import com.ebao.ls.cmu.bs.document.DocumentManagementService;
import com.ebao.ls.cmu.bs.template.TemplateService;
import com.ebao.ls.cmu.pub.model.DocumentInput.DocumentPropsKey;
import com.ebao.ls.cmu.service.DocumentService;
import com.ebao.ls.letter.common.CommonLetterService;
import com.ebao.ls.notification.extension.letter.nb.FmtUnb0781ExtensionVO;
import com.ebao.ls.notification.extension.letter.nb.index.FmtUnb0781IndexVO;
import com.ebao.ls.notification.message.WSItemString;
import com.ebao.ls.notification.message.WSLetterUnit;
import com.ebao.ls.pa.nb.bs.PolicyMedicalService;
import com.ebao.ls.pa.nb.bs.ProposalProcessService;
import com.ebao.ls.pa.nb.bs.rule.ProposalRuleMsgService;
import com.ebao.ls.pa.nb.bs.rule.ProposalRuleResultService;
import com.ebao.ls.pa.nb.ctrl.letter.NbLetterHelper;
import com.ebao.ls.pa.nb.ctrl.letter.NbNotificationHelper;
import com.ebao.ls.pa.pub.bs.InsuredService;
import com.ebao.ls.pa.pub.bs.PolicyMedicalCheckService;
import com.ebao.ls.pa.pub.bs.PolicyService;
import com.ebao.ls.pa.pub.ci.PolicyCI;
import com.ebao.ls.pa.pub.vo.CoverageVO;
import com.ebao.ls.pa.pub.vo.InsuredVO;
import com.ebao.ls.pa.pub.vo.PolicyVO;
import com.ebao.ls.pa.pub.vo.UwMedicalLetterVO;
import com.ebao.ls.pub.CodeCst;
import com.ebao.ls.uw.ctrl.pub.UwActionHelper;
import com.ebao.ls.uw.ds.UwMedicalLetterService;
import com.ebao.pub.event.EventService;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.framework.GenericAction;
import com.ebao.pub.framework.GenericException;
import com.ebao.pub.i18n.lang.CodeTable;
import com.ebao.pub.i18n.lang.StringResource;
import com.ebao.pub.util.Trans;
import com.ebao.pub.util.TransUtils;
import com.hazelcast.util.StringUtil;

public class MaintainMedicalLetterAction extends GenericAction {
	public static final String BEAN_DEFAULT = "/pa/maintainMedicalLetter";

	//public static String init = "init";

	//public static String updateLetterStatus = "updateLetterStatus";

	public static String previewNewLetter = "previewNew";

	//public static String previewArchive = "previewArchive";

	//public static String printNewLetter = "print";

	//public static String batchPrintNewLetter = "batchPrint";

	public static String saveStatus = "saveStatus";

	public static String batch = "batch";

	public static String onLine = "onLine";
	
	protected final static String BATCH_PRINT__SUCCESS = "MSG_1257540";

	protected final static String ONLINE_PRINT__SUCCESS = "MSG_1257541";
	
	protected final static String PRINT__ERROR = "MSG_1257542";
	
	protected final static long TEMPLATE_ID = 20032L;

	private static final Logger log = LoggerFactory.getLogger(MaintainMedicalLetterAction.class);
	
	@Resource(name = PolicyMedicalCheckService.BEAN_DEFAULT)
	protected PolicyMedicalCheckService medicalCheckDS;

	@Override
	public ActionForward process(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		MedicalExamForm medicalForm = (MedicalExamForm) form;
		String subAction = medicalForm.getSubAction();
		
		
		ActionForward forward = mapping.findForward("success");
		
		Long policyId = medicalForm.getPolicyId();
		
		
		if (previewNewLetter.equals(subAction)) {
			FmtUnb0781ExtensionVO extensionVO = getExtensionVO(medicalForm);
		
			FmtUnb0781IndexVO index = new FmtUnb0781IndexVO(); 
			nbNotificationHelper.initIndexVO(index, extensionVO, TEMPLATE_ID);
			
			WSLetterUnit unit = new WSLetterUnit(extensionVO, index);
			//通路寄送規則,採Email寄送
			NbLetterHelper.unbSendingMethod(unit);
			
			/**信函預覽**/
			try {
				commonLetterService.previewLetterSetupPDFHeader(request, response,TEMPLATE_ID, unit);
			} catch (Exception e) {
				com.ebao.pub.util.Log.error(this.getClass(), e.getMessage());
				request.setAttribute("error","create Pdf error");
				return mapping.findForward("previewError");
			}
		} else if (saveStatus.equals(subAction) || onLine.equals(subAction) || batch.equals(subAction)){
				Map<DocumentPropsKey, Object> props = new HashMap<DocumentPropsKey, Object>();
				props.put(DocumentPropsKey.insuredCertiCode, insuredService.load(medicalForm.getInsuredId()).getCertiCode());
				props.put(DocumentPropsKey.insuredName, medicalForm.getInsuredName());
				FmtUnb0781ExtensionVO extensionVO = getExtensionVO(medicalForm);
				
				FmtUnb0781IndexVO index = new FmtUnb0781IndexVO(); 
				nbNotificationHelper.initIndexVO(index, extensionVO, TEMPLATE_ID);

				WSLetterUnit unit = new WSLetterUnit(extensionVO, index);
				//通路寄送規則,採Email寄送
				NbLetterHelper.unbSendingMethod(unit);
				
				String policyCode = extensionVO.getPolicyCode();
				UserTransaction trans = null;
				try {
					trans = Trans.getUserTransaction();
					trans.begin();
					if ( uwMedicalLetterService.STATUS_DEFAULT.equals(medicalForm.getStatus())) {
						/**若原本為預設的體檢letter則重新產生letter資料另外發送, listId要清空**/
						medicalForm.setmListId(null);
					}
					/**儲存畫面資料*/
					if(saveStatus.equals(subAction)){ 
						/**儲存**/
						medicalForm.setStatus(uwMedicalLetterService.STATUS_SAVE);
						Long listId = saveUwMedicalLetterVO(medicalForm);
						
					} else if(onLine.equals(subAction)){/**online發送**/
						medicalForm.setStatus(uwMedicalLetterService.STATUS_SEND);
						Long listId = saveUwMedicalLetterVO(medicalForm);
						
						//2017/12/11設定medicalItemListId給催辦照會判斷狀態用
						extensionVO.setMedicalItemListId(getMedicalItemListId(listId));

						if ( medicalForm.getDocumentId() == null) {
							Long documentId = commonLetterService
									.sendLetter(policyId, TEMPLATE_ID,
											unit, policyId, policyCode, 
											props);
							/**新增核保問題  RTC 37972 刪除
							Long pListId =  saveProposalRuleResultVO(medicalForm, request);*/
							uwMedicalLetterService.updateUwMedicalLetterMsgAndDocument(listId, null, documentId);
						} else {
							Long documentId = medicalForm.getDocumentId();
							commonLetterService.updateLetterContent( documentId , unit);
							/**IR208721 儲存後再按online發送會有問題, 因為原本只把B改為O, 並沒有規檔, 修改為A, 讓下游歸檔, 發送方式一致性Online列印也改為Adhot發送**/
				    		commonLetterService.sendGroupLetter(documentId,null);
						    //if(success == false){
				    		//	 throw new GenericException(ErrorConstant.DOC_ONLINE_ISSUE_ERROR);
				    		//}
						}
						
						medicalForm.setRetMsg(ONLINE_PRINT__SUCCESS);
						
						
					} else if(batch.equals(subAction)){  /**提交batch列印**/
						medicalForm.setStatus(uwMedicalLetterService.STATUS_SEND);
						Long listId = saveUwMedicalLetterVO(medicalForm);
						
						//2017/12/11設定medicalItemListId給催辦照會判斷狀態用
						extensionVO.setMedicalItemListId(getMedicalItemListId(listId));
						
						if ( medicalForm.getDocumentId() == null) {
							
							Long documentId = commonLetterService.sendBatchLetter(policyId, TEMPLATE_ID, unit, policyId, policyCode, props);
							/**新增核保問題  RTC 37972 刪除
							Long pListId =  saveProposalRuleResultVO(medicalForm, request);*/
							uwMedicalLetterService.updateUwMedicalLetterMsgAndDocument(listId, null, documentId);
						} else {
							commonLetterService.updateLetterContent( medicalForm.getDocumentId() , unit);
						}
						medicalForm.setRetMsg(BATCH_PRINT__SUCCESS);

						
					}
					trans.commit();
				} catch (Exception ee) {
					TransUtils.rollback(trans);
					medicalForm.setRetMsg(PRINT__ERROR);
					log.error(ExceptionInfoUtils.getExceptionMsg(ee));
				}
		}
		// refresh page
		if (forward != null) {
			fillForm(request, medicalForm);
		}
		initMedicalPage(request,policyId);
		return forward;
	}
	
	private List<String> getMedicalItemListId(Long listId) {
		List<String> medicalItemListId = new ArrayList<String>();
		
		List<Map<String,String>> itemList = uwMedicalLetterService.findMedicalItem(listId);
		for(Map<String,String> item : itemList) {
			medicalItemListId.add(item.get("listId"));
		}
		
		return medicalItemListId;
	}

	private void initMedicalPage(HttpServletRequest request,Long policyId) {
		List<Map<String, String>> physicalExamObjList = policyMedicalService
				.getPhysicalExamObjects(policyId);

		request.setAttribute("physicalExamObjList", physicalExamObjList);
		request.setAttribute("bizId", policyId);
		request.setAttribute("templateId", TEMPLATE_ID);
		request.setAttribute("underwriteId",Long.parseLong(com.ebao.ls.encode.encoder.Encode.encodeForHTML(request).getParameter("underwriteId")));
		
		PolicyVO policyVO = policyService.load(policyId);
		
		/* 改用 policyVO.gitSortInsuredList();
		List<InsuredVO> insuredList = insuredService.findByPolicyId(policyId);
		Collections.sort(insuredList, new Comparator<InsuredVO>() {
			@Override
			public int compare(InsuredVO m1, InsuredVO m2) {
				return new CompareToBuilder()
				.append("0".equals(m1.getInsuredCategory())?"9":m1.getInsuredCategory(),
						"0".equals(m2.getInsuredCategory())?"9":m2.getInsuredCategory())
				.append(m1.getNbInsuredCategory(),m2.getNbInsuredCategory())
				.toComparison();
			}
		});*/
		
		
		request.setAttribute("insuredList", policyVO.gitSortInsuredList());
		
		/* 核保作業中有被保險人列表的功能增加判斷是否有險種，無險種disabled被保險人checkbox不可勾選 */
		UwActionHelper.countInsuredCoverage(request, policyVO);
		
		List<Map<String, String>> documentList = documentManagementService
				.findDocumentList(null,
						Long.valueOf(policyId),
						TEMPLATE_ID);

		request.setAttribute("documentList", documentList);

	}

	private void fillForm(HttpServletRequest request,
					MedicalExamForm medicalForm) throws Exception {
		request.setAttribute("bizId", medicalForm.getPolicyId());
		request.setAttribute("mListId", medicalForm.getmListId());
		request.setAttribute("templateId", TEMPLATE_ID);
		
	}



	//體檢照會通知單 
	private FmtUnb0781ExtensionVO getExtensionVO(MedicalExamForm form) {
		Long policyId =  form.getPolicyId();
		PolicyVO policyVO = policyService.load(policyId);
		FmtUnb0781ExtensionVO extensionVO = new FmtUnb0781ExtensionVO();
		Date noticeDate = AppContext.getCurrentUserLocalTime();
		//BC353 PCR-263273 有固定回覆日時催辦日改用催辦到期日(SpecialReplyDueDate) 2018/11/13 add by Kathy
    	Date specialReplyDueDate = null;
        Map<String, Object> specialDaysMap =  new HashMap<String, Object>();
        specialDaysMap = documentService.getDocSpecialReplyDueDate(policyId, TEMPLATE_ID, noticeDate);  
        if (specialDaysMap != null) {
        	specialReplyDueDate = (Date) specialDaysMap.get("specialReplyDueDate");
        }

		if (specialReplyDueDate  != null) {
			extensionVO.setRemindStartDate(specialReplyDueDate);
		}else{
			//催辦日(預設同回覆到期日)
			Date remindStartDate = commonLetterService.getRemaindStartDate(TEMPLATE_ID, noticeDate);
			extensionVO.setRemindStartDate(remindStartDate);
		}

		nbNotificationHelper.initNBLetterExtensionVO(policyId, extensionVO);
		nbNotificationHelper.setLetterContractorAtUW(extensionVO,form.getPolicyId());
		extensionVO.setNoticeDate(noticeDate);
		extensionVO.setTemplateId(TEMPLATE_ID);
		//extensionVO.setRemindStartDate(remindStartDate);
		//extensionVO.setBarcode1(CodeCst.CARD_CODE_UNBN016);
		extensionVO.setBarcode1(CodeCst.BARCODE1_FMT_UNB_0781);
		extensionVO.setBarcode2(policyVO.getPolicyNumber());
		extensionVO.setMedicalReasons(getMedicalReasons(form));//取得體檢照會原因
		extensionVO.setMedicalItems(getMedicalItems(form));  //體檢項目
		extensionVO.setHospital(getHospital(form));
		extensionVO.setCertiCode(getCertiCode( policyVO ,form.getInsuredId()));
		extensionVO.setInsuredId(String.valueOf(form.getInsuredId()));
		extensionVO.setInsuredName(form.getInsuredName());
		String notice = form.getNotice();
		if ( StringUtils.isNotBlank(notice)) {
			extensionVO.setNotice(notice);
		} else {
			extensionVO.setNotice(null);
		}
		extensionVO.setHotline(nbNotificationHelper.getHotline());
		extensionVO.setEntryAge(getEntryAge( policyVO ,form.getInsuredId()));

		return extensionVO;

	}



	private long saveUwMedicalLetterVO(MedicalExamForm medicalForm)
					throws GenericException {

		UwMedicalLetterVO vo = new UwMedicalLetterVO();

		vo.setListId(medicalForm.getmListId());
		vo.setDocumentId(medicalForm.getDocumentId());
		vo.setHospitalTxt(medicalForm.getHospitalTxt());
		vo.setInsuredId(medicalForm.getInsuredId());
		vo.setIsContraHospital(medicalForm.getIsContraHospital());
		vo.setIsHospitalTxt(medicalForm.getIsHospitalTxt());
		vo.setIsPaySelf(medicalForm.getIsPaySelf());
		vo.setMsgId(medicalForm.getMsgId());
		vo.setNotice(medicalForm.getNotice());
		vo.setOtherReason(medicalForm.getOtherReason());
		vo.setStatus(medicalForm.getStatus());
		vo.setUnderwriteId(medicalForm.getUnderwriteId());

		String[] reasonCodes = medicalForm.getReasonCode();
		String[] medicalItems = medicalForm.getMedicalItem();
		String[] newItemNames = medicalForm.getNewItemName();
		String[] specMenos = medicalForm.getNewItemComment();

		return uwMedicalLetterService.updateUwMedicalLetter(vo, reasonCodes, medicalItems, newItemNames, specMenos);

	}

	/**
	 * to find PolicyVO By policyId
	 * 
	 * @param policyId
	 */
	private PolicyVO findPolicyVO(Long policyId) {
		PolicyVO policyVO = policyService.load(policyId);
		return policyVO;
	}

	
	
	/**
	 * <p>Description : 取得體檢照會原因</p>
	 * <p>Created By : Victor Chang</p>
	 * <p>Create Time : Oct 23, 2016</p>
	 * @param medicalForm
	 * @return
	 */
	
	private List<WSItemString> getMedicalReasons(MedicalExamForm medicalForm) {
	
		String[] list = medicalForm.getReasonCode();
	
		List<WSItemString> item = new ArrayList<WSItemString>();
		int prefix = 1;
		if(list != null){
			for(String each:list){
				item.add(new WSItemString(null,CodeTable.getCodeDesc( "T_MEDICAL_REASON", each,AppContext.getCurrentUser().getLangId())));
				// item.add(new WSItemString(String.valueOf(prefix),CodeTable.getCodeDesc( "T_MEDICAL_REASON", each,AppContext.getCurrentUser().getLangId())));
				prefix++;
			}
		}
		if(StringUtils.isNotEmpty(medicalForm.getOtherReason())){
			item.add(new WSItemString(null,medicalForm.getOtherReason()));

		    // item.add(new WSItemString(String.valueOf(prefix),medicalForm.getOtherReason()));
		}
		return item;
	
	}

	
	/**
	* <p>Description : 取得體檢項目</p>
	* <p>Created By : Victor Chang</p>
	* <p>Create Time : Oct 23, 2016</p>
	* @param medicalForm
	* @return
	*/
	private List<WSItemString> getMedicalItems(MedicalExamForm medicalForm) {
		String[] list =  medicalForm.getMedicalItem();
		String[] listNew = medicalForm.getNewItemName();
		String[] listNewComment = medicalForm.getNewItemComment();

	    List<WSItemString> item = new ArrayList<WSItemString>();
	    
	    if(list != null){
		    item.addAll(this.uwMedicalLetterService.getLetterText(list));
		}
	    int prefix = item.size() + 1;
		if(listNew != null){
			int size = listNew.length;
			for(int i = 0; i<size ;i++){
				String newItem = listNew[i];
				String newItemComment = listNewComment[i];
				
				if(StringUtils.isNotEmpty(newItem)){
					//item.add(new WSItemString(null, each));
					//RTC189147-調整半形()為全形（）
					item.add(new WSItemString(String.valueOf(prefix), 
							newItem + (StringUtil.isNullOrEmpty(newItemComment.trim())?""
									:"（"+org.apache.commons.lang.StringEscapeUtils.unescapeHtml(newItemComment)+"）")));
					prefix++;
				}
			}
		}
		return item;

	} 
	/**
	* <p>Description : 取得體檢項目</p>
	* <p>Created By : Victor Chang</p>
	* <p>Create Time : Oct 23, 2016</p>
	* @param medicalForm
	* @return
	*/
	private String getHospital(MedicalExamForm medicalForm) {
		String item ="";
		if(CodeCst.YES_NO__YES.equals(medicalForm.getIsContraHospital())){  
			item = StringResource.getStringData("MSG_1251991",AppUserContext.getCurrentUser().getLangId())+"、" ;
		}
		if(CodeCst.YES_NO__YES.equals(medicalForm.getIsPaySelf())){  
			item =item+ StringResource.getStringData("MSG_1251741",AppUserContext.getCurrentUser().getLangId())+"、" ;
		}
		if(StringUtils.isNotEmpty(medicalForm.getHospitalTxt())){  
			item =item + StringResource.getStringData("MSG_1256975",AppUserContext.getCurrentUser().getLangId())+medicalForm.getHospitalTxt()+StringResource.getStringData("MSG_242922",AppUserContext.getCurrentUser().getLangId()) ;
		}
		if ( item.endsWith("、")) {
			item = item.substring(0, item.length()-1);
		}
	
		return item;
	
	}

	//點選被保險人身分證字號; 
	private static String getCertiCode(PolicyVO policyVO ,Long insuredId) {
		InsuredVO insuredVO = policyVO.gitInsured(insuredId);
		if(insuredVO == null){
			return " ";
		}else{
			return insuredVO.getCertiCode();
		}
	}
	
	// 點選被保險投保年齡; 
	private static int getEntryAge(PolicyVO policyVO ,Long insuredId) {
		
		if(policyVO !=null &&  insuredId !=null ){
			List<CoverageVO> cvo = policyVO.gitCoveragesByInsured(insuredId);
			
			return cvo.get(0).getInsureds().get(0).getEntryAge();	   
		}else{
			return 0;
		}
	
	}
	@Resource(name = PolicyService.BEAN_DEFAULT)
	private PolicyService policyService;

	@Resource(name = CommonLetterService.BEAN_DEFAULT)
	protected CommonLetterService commonLetterService;

	@Resource(name = ProposalProcessService.BEAN_DEFAULT)
	private ProposalProcessService proposalProcessService;

	@Resource(name = NbLetterHelper.BEAN_DEFAULT)
	private NbLetterHelper nbLetterHelper;

	@Resource(name = ProposalRuleResultService.BEAN_DEFAULT)
	private ProposalRuleResultService paProposalRuleResultService;

	@Resource(name = EventService.BEAN_DEFAULT)
	private EventService eventService;

	@Resource(name = PolicyCI.BEAN_DEFAULT)
	private PolicyCI policyCI;

	@Resource(name = ProposalRuleMsgService.BEAN_DEFAULT)
	private ProposalRuleMsgService paProposalRuleMsgService;
	
	@Resource(name = PolicyMedicalService.BEAN_DEFAULT)
	private PolicyMedicalService policyMedicalService;
	@Resource(name = DocumentManagementService.BEAN_DEFAULT)
	private DocumentManagementService documentManagementService;
	@Resource(name = InsuredService.BEAN_DEFAULT)
	private InsuredService insuredService;
	
	@Resource(name = NbNotificationHelper.BEAN_DEFAULT)
	private NbNotificationHelper nbNotification;
	@Resource(name = UwMedicalLetterService.BEAN_DEFAULT)
	private UwMedicalLetterService uwMedicalLetterService;
	
	@Resource(name = NbNotificationHelper.BEAN_DEFAULT)
	private NbNotificationHelper nbNotificationHelper;
	
	@Resource(name = AgentService.BEAN_DEFAULT)
	private AgentService agentService;
	
	@Resource(name=TemplateService.BEAN_DEFAULT)
	protected TemplateService templateService;

    //BC353 PCR-263273 有固定回覆日時催辦日改用催辦到期日(SpecialReplyDueDate) 2018/11/13 add by Kathy
    @Resource(name=DocumentService.BEAN_DEFAULT)
	protected DocumentService documentService;

}
