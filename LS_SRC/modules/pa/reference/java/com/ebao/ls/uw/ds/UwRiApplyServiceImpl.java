package com.ebao.ls.uw.ds;

import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.hibernate.SQLQuery;
import org.hibernate.criterion.Restrictions;

import com.ebao.foundation.common.lang.StringUtils;
import com.ebao.foundation.commons.exceptions.GenericException;
import com.ebao.foundation.core.logging.Log;
import com.ebao.foundation.module.db.hibernate.HibernateSession3;
import com.ebao.foundation.module.util.beanutils.BeanUtils;
import com.ebao.ls.foundation.service.GenericServiceImpl;
import com.ebao.ls.image.data.bo.Image;
import com.ebao.ls.image.vo.BillCardVO;
import com.ebao.ls.pa.pub.bs.coverage.CoverageService;
import com.ebao.ls.pa.pub.data.bo.NbReinsurerRequests;
import com.ebao.ls.pa.pub.data.bo.NbReinsurerSpec;
import com.ebao.ls.pa.pub.data.bo.NbRiResult;
import com.ebao.ls.pa.pub.data.bo.UwRiFacExtend;
import com.ebao.ls.pa.pub.ref.api.prd.ProductService;
import com.ebao.ls.pa.pub.ref.vo.prd.ProductVO;
import com.ebao.ls.pa.pub.vo.CoverageInsuredVO;
import com.ebao.ls.pa.pub.vo.CoverageVO;
import com.ebao.ls.pa.pub.vo.NbReinsurerRequestsVO;
import com.ebao.ls.pa.pub.vo.NbReinsurerSpecVO;
import com.ebao.ls.pa.pub.vo.NbRiResultVO;
import com.ebao.ls.pa.pub.vo.PolicyVO;
import com.ebao.ls.pa.pub.vo.UwRiFacExtendVO;
import com.ebao.ls.pub.CodeCst;
import com.ebao.ls.pub.cst.Billcard;
import com.ebao.ls.pub.cst.YesNo;
import com.ebao.ls.uw.data.ReinsurerDelegate;
import com.ebao.ls.uw.data.ReinsurerRequestsDelegate;
import com.ebao.ls.uw.data.ReinsurerSpecDelegate;
import com.ebao.ls.uw.data.RiResultDelegate;
import com.ebao.ls.uw.data.TUwRiFacDelegate;
import com.ebao.ls.uw.data.TUwRiFacExtendDelegate;
import com.ebao.ls.uw.data.bo.UwRiFac;
import com.ebao.ls.uw.vo.UwRiFacVO;
import com.ebao.pub.framework.ExceptionFactory;
import com.ebao.pub.i18n.lang.CodeTable;
import com.ebao.pub.util.DBean;

public class UwRiApplyServiceImpl extends GenericServiceImpl<UwRiFacExtendVO, UwRiFacExtend, TUwRiFacExtendDelegate> implements UwRiApplyService{

    @Resource(name = TUwRiFacExtendDelegate.BEAN_DEFAULT)
    private TUwRiFacExtendDelegate tuwRiFacExtendDelegate;
    
    @Resource(name = ReinsurerSpecDelegate.BEAN_DEFAULT)
    private ReinsurerSpecDelegate reinsurerSpecDelegate;
    
    @Resource(name = ReinsurerRequestsDelegate.BEAN_DEFAULT)
    private ReinsurerRequestsDelegate reinsurerRequestsDelegate;
    
    @Resource(name = RiResultDelegate.BEAN_DEFAULT)
    private RiResultDelegate riResultDelegate;
    
    @Resource(name = CoverageService.BEAN_DEFAULT)
    private CoverageService coverageService;
    
    @Resource(name = ReinsurerDelegate.BEAN_DEFAULT)
	protected ReinsurerDelegate reinsurerDelegate;

	@Resource(name = TUwRiFacDelegate.BEAN_DEFAULT)
	private TUwRiFacDelegate uwRiFacDao;
    
    private final static Log log = Log.getLogger(UwRiApplyServiceImpl.class);
    
	@Override
	public List<Integer> getBillCardByPolicyId(Long policyId) {
		List<Integer> result = new ArrayList<Integer>();
		List<Image> list =dao.getBillCardIdByPolicyId(policyId);
		
		for(Image image : list){
			Integer TypeId = image.getImageTypeId();
			result.add(TypeId);
		};
		return result;
	}

	@Override
	public BillCardVO getBillCardByImageTypeId(Long typeId) {
		List<Billcard> list =dao.getBillCardByImageTypeId(typeId);
		BillCardVO billCardVO = new BillCardVO();
		Billcard billCard = list.get(0);
		try {
			com.ebao.foundation.module.util.beanutils.BeanUtils.copyProperties(billCardVO, billCard);
		} catch (Exception e) {
		}
		
		return billCardVO;
	}

	@Override
	protected UwRiFacExtendVO newEntityVO() {
		return new UwRiFacExtendVO();
	}


	@Override
	public Long getRiCompByCompCode(String riCompany) {
		Long result = dao.getRiCompByCompCode(riCompany);
		return result;
	}


	@Override
	public List<UwRiFacExtendVO> getRiInfoByUnderwriteId(Long underwriteId) {
		List<UwRiFacExtendVO> uwRiFacExtendVO = new ArrayList<UwRiFacExtendVO>();
			uwRiFacExtendVO = dao.getRiInfoByUnderwriteId(underwriteId);
		return uwRiFacExtendVO;
	}
	/**
	 * <p>Description : load 變更再保公司資料的共用表格 t_nb_reinsurer_spec : </p>
	 * <p>Created By : Peter.Liu</p>
	 * <p>Create Time : Apr 12, 2016</p>
	 * @param nbReinsurerSpecVO
	 */
	@Override
	public NbReinsurerSpecVO loadReinsurerSpec(Long reinsurerId) {
		NbReinsurerSpec reinsurerCompany = reinsurerSpecDelegate.load(reinsurerId);
		
		NbReinsurerSpecVO nbReinsurerSpecVO = new NbReinsurerSpecVO();
		if( reinsurerCompany != null) {
			try {
			  reinsurerCompany.copyToVO(nbReinsurerSpecVO, true);
			} catch (Exception e) {
			}
		} else {
			nbReinsurerSpecVO.setReinsurerId(reinsurerId);
		}
		return nbReinsurerSpecVO;
	}
	
	@Override
	public NbReinsurerSpecVO loadReinsurerSpecByCode(String code) {
		Long reinsurerId = this.reinsurerDelegate.getRiCompByCompCode(code);
		return this.loadReinsurerSpec(reinsurerId);
	}
	/**
	 * <p>Description : save or update 變更再保公司資料的共用表格 t_nb_reinsurer_spec</p>
	 * <p>Created By : Peter.Liu</p>
	 * <p>Create Time : Apr 12, 2016</p>
	 * @param nbReinsurerSpecVO
	 */
	@Override
	public void reinsurerSpecSaveOrUpdate(NbReinsurerSpecVO nbReinsurerSpecVO) {
		List<NbReinsurerSpec> reinsurerCompany = reinsurerSpecDelegate.findByCriteria(Restrictions.eq("reinsurerId", nbReinsurerSpecVO.getReinsurerId()));
		if(reinsurerCompany.size() == 0){
			NbReinsurerSpec nbReinsurerSpec = new NbReinsurerSpec();
			try {
				BeanUtils.copyProperties(nbReinsurerSpec, nbReinsurerSpecVO);
			} catch (Exception e) {
			}
			
			reinsurerSpecDelegate.saveorUpdate(nbReinsurerSpec);
		}else{
			
			for(NbReinsurerSpec nbReinsurerSpec : reinsurerCompany){
				try {
					BeanUtils.copyProperties(nbReinsurerSpec, nbReinsurerSpecVO);
				} catch (Exception e) {
				}
				
				reinsurerSpecDelegate.saveorUpdate(nbReinsurerSpec);
			}
			
		}
		}
	/**
	 * <p>Description : save or update 變更再保公司資料的共用表格 t_nb_reinsurer_Requests</p>
	 * <p>Created By : Peter.Liu</p>
	 * <p>Create Time : Apr 12, 2016</p>
	 * @param nbReinsurerRequestsVO
	 */
	@Override
	public void reinsurerRequestsSaveOrUpdate(NbReinsurerRequestsVO nbReinsurerRequestsVO) {
		List<NbReinsurerRequests> reinsurerList = reinsurerRequestsDelegate.
						findByCriteria(
										Restrictions.eq("reinsurerId", nbReinsurerRequestsVO.getReinsurerId()),
                    Restrictions.eq("extendId", nbReinsurerRequestsVO.getExtendId()),
                    Restrictions.eq("riFacID", nbReinsurerRequestsVO.getRiFacID())
										);
		if(reinsurerList.size() == 0){
			NbReinsurerRequests nbReinsurerRequests = new NbReinsurerRequests();
			nbReinsurerRequests.copyFromVO(nbReinsurerRequestsVO, true, true);
			reinsurerRequestsDelegate.save(nbReinsurerRequests);
		}else{
			
			for(NbReinsurerRequests nbReinsurerRequests : reinsurerList){
				try {
					BeanUtils.copyProperties(nbReinsurerRequests, nbReinsurerRequestsVO);
				} catch (Exception e) {
				}
				reinsurerRequestsDelegate.saveorUpdate(nbReinsurerRequests);
			}
		}
		
	}
	
	@Override
	public UwRiFacExtendVO getRiApplyExtendByUnderwriteId(Long underwriteId) {
		return dao.getRiApplyExtendByUnderwriteId(underwriteId);
	}
	
	@Override
	public List<Map<String, Object>> getPolicyRiApplyInfoByExtendId(Long extendId) {
		return dao.getPolicyRiApplyInfoByExtendId(extendId);
	}
	
	@Override
	public List<Map<String, Object>> getRIRequest(Long underwriteId) {
		return dao.getRIRequest(underwriteId);
	}
	
	@Override
	public List<String> getExtendMedInfoByExtendId(Long extendId) {
		return dao.getExtendMedInfoByExtendId(extendId);
	}
	
	@Override
	public void updateNbReinsureRequest(Long listId, String status, Date date) {
		/**update by sunny**/
		//String sql = "update T_NB_REINSURER_REQUESTS SET RESULT_STATUS = ? , REINSURED_DATE = ? where list_id  = ? ";
		String sql = "update T_NB_RI_RESULT SET RESULT_STATUS = ? , REINSURED_DATE = ? where list_id  = ? ";
	    Connection con = null;
	    PreparedStatement ps = null;
	    ResultSet rs = null;
	    DBean db = new DBean();
	    try {
	      db.connect();
	      con = db.getConnection();
	      ps = con.prepareStatement(sql);
	      ps.setString(1, status);
	      if ( date == null) {
	    	  ps.setNull(2, Types.DATE);
	      } else {
	    	  ps.setDate(2, new java.sql.Date(date.getTime()));
	      }
	      ps.setLong(3, listId);
	      
	      ps.execute();
	    } catch (Exception e) {
	      throw ExceptionFactory.parse(e);
	    } finally {
	      DBean.closeAll(rs, ps, db);
	    }
	}

	@Override
	public void insertResult(NbRiResultVO riVO) throws Exception {
		/**發送多次再保申請不再重新寫檔**/
		NbRiResult riResult = new NbRiResult();
		riResult.copyFromVO(riVO, true, true);
//		List<NbRiResult> results = this.riResultDelegate.queryByExample(riResult);
//		if ( results == null || results.isEmpty()) {
//			riResultDelegate.save(riResult);
//		}
		riResultDelegate.save(riResult);
	}

	@Override
	public List<NbRiResultVO> queryNbRiResultList(PolicyVO policyVO) {
		List<NbRiResultVO> resultVOList = new java.util.LinkedList<NbRiResultVO>();
		if (policyVO == null)
			return resultVOList;
		
		List<NbRiResult> nbRIList = this.riResultDelegate.queryRiResultListByPolicyId(policyVO.getPolicyId());
		if(CollectionUtils.isEmpty(nbRIList)) {
			return resultVOList;
		}
		
		for(NbRiResult result:  nbRIList) {
			NbRiResultVO vo = new NbRiResultVO();
			result.copyToVO(vo, Boolean.FALSE);
			CoverageVO coverageVO = coverageService.load(new Long(result.getItemId()));
			if(coverageVO == null ) {
				log.info(String.format("[queryNbRiResultList][NO_DATA_FOUND] policy_code := %s, item_id := %d ", policyVO.getPolicyNumber(), result.getItemId()));
				continue;
			}
			/* 判斷豁免險 */
			String isWaver = YesNo.YES_NO__YES.equals(coverageVO.getWaiverExt().getWaiver()) ? YesNo.YES_NO__YES : YesNo.YES_NO__NO;
			vo.setIsWaver(isWaver);
			
			/* 主被保險人訊息 */
			vo.setInsuredName(getMainInsuredName(coverageVO));
			
			/* 明細資料 */
			if(result.getRequest() != null && result.getRequest().getListId() != null) {
				Long requestId = result.getRequest().getListId();
				NbReinsurerRequestsVO requestVO = getReinsurerRequests(requestId);
				
				vo.setRequest(requestVO);
			}
			
			/* 險種代號 */
			String internalId = CodeTable.getCodeDesc("V_PRODUCT_LIFE_1", String.valueOf(coverageVO.getProductId()));
			vo.setInternalId(internalId);
			resultVOList.add(vo);
		}
		return resultVOList;
	}
	
	private NbReinsurerRequestsVO getReinsurerRequests(Long requestId) {
		NbReinsurerRequestsVO vo = null;
		NbReinsurerRequests request = this.reinsurerRequestsDelegate.load(new Long(requestId));
		if(request == null) {
			return vo;
		}
		NbReinsurerRequestsVO requestVO = new NbReinsurerRequestsVO();
		request.copyToVO(requestVO, Boolean.FALSE);
		return vo;
	}
	
	private String getMainInsuredName(CoverageVO vo) {
		String insuredName = "";
		List<CoverageInsuredVO> insuredVOList = vo.getInsureds();
		if (CollectionUtils.isEmpty(insuredVOList))
			return insuredName;
		
		CoverageInsuredVO mainInsuredVO = null;
		for(CoverageInsuredVO insuredVO : insuredVOList) {
			if(insuredVO.getOrderId()  == null) {
				mainInsuredVO = insuredVO;
				break;
			}
			if(insuredVO.getOrderId() != null && insuredVO.getOrderId().intValue() == 1) {
				mainInsuredVO = insuredVO;
				break;
			}
		}
		if(mainInsuredVO != null) {
			insuredName = mainInsuredVO.getInsured().getName();
		}
		return insuredName;
	}
	// 2017/12/19 Add : YCsu
	// [artf410579](Internal Issue 189705)[UAT2][eBao]保號：0052751760，主頁面再保資料有誤，請修正。
	/** 畫面上(UI : UwMainFixPage.jsp)無法再以for loop判斷控制DataTable:column是否該顯示該ROW資料，因此分成2個Method*/
	@Override
	public List<Map<String, Object>> getRIResult(Long policyId, PolicyVO policyVO) {
		List<NbRiResult> results = this.riResultDelegate.findByProperty("policyId", policyId);
		List<Map<String, Object>> returnResults = new ArrayList<Map<String, Object>>();
		CoverageVO coverageVO1 = new CoverageVO();
		NbRiResult nbRiResult1 = new NbRiResult();
		// int index1 = 0;
		for(NbRiResult result:results) {

        	CoverageVO coverageVO = coverageService.load((Long)result.getItemId());
        	if (CodeCst.YES_NO__NO.equals(coverageVO.getWaiverExt().getWaiver())) {
            	Map map = new HashMap();
    			try {
    				map = org.apache.commons.beanutils.BeanUtils.describe(result);
    			} catch (IllegalAccessException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			} catch (InvocationTargetException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			} catch (NoSuchMethodException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}
    			map.put("isWaiver1", "N"); // is Waiver : No
    			map.put("isWaiver2", ""); // is Waiver : No
    			if(coverageVO.getInsureds()!=null 
    					&& coverageVO.getInsureds().size() > 0){
    				if(coverageVO.getInsureds().get(0).getInsured()!=null
    						&& !StringUtils.isNullOrEmpty(coverageVO.getInsureds().get(0).getInsured().getName())) {
    					map.put("name", coverageVO.getInsureds().get(0).getInsured().getName());
    				}else {
    					map.put("name", "");
    				}
    			}else {
    				map.put("name", "");
    			}
    			
            	map.put("internalId", CodeTable.getCodeDesc("V_PRODUCT_LIFE_1", String.valueOf(coverageVO.getProductId())));

    			returnResults.add(map);        		
        	}else {
        		// 2018/02/23 Modify : YCSu
        		// [artf410579](Internal Issue 222977)保單01105001060已做再保諮詢後-保項無法刪除
        		//if(index1 == 0) {
        			coverageVO1 = coverageVO;
        			nbRiResult1 = result;
            		// index1=index1+1;
        			
        			/* 在Proposal中取得是否有豁免險商品ID */
        			/** 該ROW資料 */
        			Long waiver = policyVO.getProposalInfo().getWaiverId();
        			if (waiver != null && waiver > 0) {
        				Map<String, Object> coverage = getDisplayWaiver(new Long(waiver), coverageVO1, nbRiResult1);
        				returnResults.add(coverage);
        			}        			
            	//}
        	}
		}
		
		// 2017/12/26 Modify : YCsu
//		if(results.size()>0) { 
//			/* 在Proposal中取得是否有豁免險商品ID */
//			/** 該ROW資料 */
//			Long waiver = policyVO.getProposalInfo().getWaiverId();
//			if (waiver != null && waiver > 0) {
//				Map<String, Object> coverage = getDisplayWaiver(new Long(waiver), coverageVO1, nbRiResult1);
//				returnResults.add(coverage);
//			}			
//		}

		return returnResults;
	}	

	// 2017/12/19 Modify : YCsu
	// [artf410579](Internal Issue 189705)[UAT2][eBao]保號：0052751760，主頁面再保資料有誤，請修正。	
	@Override
	public List<Map<String, Object>> getRIResultAll(Long policyId, PolicyVO policyVO) {
		List<NbRiResult> results = this.riResultDelegate.findByProperty("policyId", policyId);
		List<Map<String, Object>> returnResults = new ArrayList<Map<String, Object>>();
		for(NbRiResult result:results) {

        	CoverageVO coverageVO = coverageService.load((Long)result.getItemId());
        	if (CodeCst.YES_NO__NO.equals(coverageVO.getWaiverExt().getWaiver())) {
            	Map map = new HashMap();
				map.put("listIdAll", result.getListId());
    			map.put("isWaiver1All", "N"); // is Waiver : No (非豁免件)
    			map.put("isWaiver2All", "");    				
				map.put("resultStatusAll", result.getResultStatus());
				map.put("reinsuredDateAll", result.getReinsuredDate());
    			returnResults.add(map);        		
        	}else {
            	Map map = new HashMap();
				map.put("listIdAll", result.getListId());
    			map.put("isWaiver1All", "");
    			map.put("isWaiver2All", "Y"); // is Waiver : Yes (豁免件)    				
				map.put("resultStatusAll", result.getResultStatus());
				map.put("reinsuredDateAll", result.getReinsuredDate());

    			returnResults.add(map);    
        	}       	
		}
		
		return returnResults;
	}
	
	/**
	 * <p>Description : 預收輸入險種列表 -豁免險種</p>
	 * <p>Created By : YCsu</p>
	 * <p>Create Time : Dec 19, 2017</p>
	 * @param Long waiverId
	 * @param CoverageVO coverageVO1
	 * @param NbRiResult nbRiResult1
	 * @return
	 */
	public Map<String, Object> getDisplayWaiver(Long waiverId, CoverageVO coverageVO1, NbRiResult nbRiResult1) {

		ProductVO waiveInfoVO = productService.getProduct(waiverId);
		Map<String, Object> coverage = new HashMap<String, Object>();
		coverage.put("isWaiver1", "");
		coverage.put("isWaiver2", "Y"); //is waiver : Yes (豁免件)
		coverage.put("listId", nbRiResult1.getListId());
		coverage.put("itemId", waiveInfoVO.getProductId());
		coverage.put("masterId", "");
		coverage.put("productId", waiveInfoVO.getProductId());
		coverage.put("productVersionId", "");
		coverage.put("versionTypeId", "");
		coverage.put("insuredCategory", "");
		coverage.put("insuredName", "");
		if(coverageVO1.getInsureds()!=null 
				&& coverageVO1.getInsureds().size() > 0)
		{
			if(coverageVO1.getInsureds().get(0).getInsured()!=null
					&& !StringUtils.isNullOrEmpty(coverageVO1.getInsureds().get(0).getInsured().getName())) {
				coverage.put("name", coverageVO1.getInsureds().get(0).getInsured().getName());
			}else {
				coverage.put("name", ""); 
			}
		}else {
			coverage.put("name", "");
		}
		coverage.put("internalId", CodeTable.getCodeDesc("V_PRODUCT_LIFE_1", String.valueOf(waiveInfoVO.getProductId())));
		coverage.put("reinsurerId", nbRiResult1.getReinsurerId());
		coverage.put("reinsuredDate", nbRiResult1.getReinsuredDate());
		coverage.put("amount", "");
		coverage.put("unit", "");
		coverage.put("benefitLevel", "");
		coverage.put("productName", waiveInfoVO.getProductName());
		coverage.put("waiver", CodeCst.YES_NO__YES);
		coverage.put("insuredId", "");
		coverage.put("internalId", waiveInfoVO.getInternalId());
		coverage.put("proposalCategory", "999");
		coverage.put("isAnnuityProd", CodeCst.YES_NO__NO);
		coverage.put("isFormerIdProd", CodeCst.YES_NO__NO);
		coverage.put("reviewPeriodIndi", CodeCst.YES_NO__NO);
		coverage.put("isATIT", CodeCst.YES_NO__NO);
		coverage.put("isRegularInsurance", CodeCst.YES_NO__NO);
		coverage.put("autoExpPreventOpt", "");//自動防失效選擇(自動墊繳)
		//////////////////////////////////////////
		coverage.put("singlePayMode", "");
		coverage.put("singleNextPayMode", CodeCst.YES_NO__NO);
		coverage.put("isDummy", CodeCst.YES_NO__NO);
		coverage.put("isFatca", CodeCst.YES_NO__NO);
		coverage.put("isUwManual", CodeCst.YES_NO__NO);
		coverage.put("isIlpProd", CodeCst.YES_NO__NO);

		return coverage;
	}
	
	@Override
	public List<NbRiResultVO> findNBRiResultByInsured(Long insuredId){
		List<NbRiResultVO> resultList = new ArrayList<NbRiResultVO>();
		List<NbRiResult> results = this.riResultDelegate.findByProperty("insuredId", insuredId);
		for(NbRiResult result : results){
			NbRiResultVO vo = new NbRiResultVO();
			result.copyToVO(vo, true);
			resultList.add(vo);
		}
		return resultList;
	}

	@Override
	public List<NbRiResultVO> findNBRiResultByItem(Long itemId){
		List<NbRiResultVO> resultList = new ArrayList<NbRiResultVO>();
		List<NbRiResult> results = this.riResultDelegate.findByProperty("itemId", itemId);
		for(NbRiResult result : results){
			NbRiResultVO vo = new NbRiResultVO();
			result.copyToVO(vo, true);
			resultList.add(vo);
		}
		return resultList;
	}
	
	/**
	 * 由RMS訊息取得林分公司清單
	 */
	@Override
	public Map<Long, List<Long>> loadRiCoverageCompanyMap(
			List<Map<String, Object>> riMsg) {
		
		Map<Long, List<Long>> riCoverageCompanyMap = new HashMap<Long, List<Long>>();
		for(Map riMap:riMsg) {
		  if (riMap.get("result") == "1") {
	      Long itemId = (Long)riMap.get("itemId");
	      CoverageVO vo = coverageService.load(itemId);
	      List<Long> ids = reinsurerDelegate.getRiCompanyCode(vo);
	      riCoverageCompanyMap.put(itemId, ids);
		  }
		}
		
		
		return riCoverageCompanyMap;
	}
	
	/**
	 * 
	 * @param requestId
	 * @param extendId
	
	@Override
	public void setRequestExtend(Long requestId, Long extendId) {
		UwRiRequestExtend entity = new UwRiRequestExtend();
		entity.setExtendId(extendId);
		entity.setRequestId(requestId);
		requestExtendDelegate.save(entity);
	}
	 */
	/**
	 * 取得保單下備保人最新的再保資訊內容
	 */
	@Override
	public List<UwRiFacExtendVO> getDefaultRiFacExtendVOByPolicyId(Long policyId) {
		List<UwRiFacExtendVO>  extendVOs = this.tuwRiFacExtendDelegate.getDefaultRiFacExtendVOByPolicyId(policyId);
		return extendVOs;
	}
	/**
	 * 取得保單下最新的資訊保單, 再保公司ID
	 */
	@Override
	public List<Long> getDefaultReinsureId(Long policyId) {
		// TODO Auto-generated method stub
		return null;
	}
	
/*
 public List<UwRiFacExtendVO> getRiInfoByUnderwriteId(Long underwriteId) {
		DBean db = new DBean();
		PreparedStatement ps = null;
		ResultSet rs = null;
		UwRiFacExtendVO uwRiFacExtendVO = new UwRiFacExtendVO();
		List<UwRiFacExtendVO> resultList = new ArrayList<UwRiFacExtendVO>();
		Integer underId = NumericUtils.parseInteger(underwriteId);
		List<UwRiFacExtend> boList = super.findByCriteria(
                        Restrictions.eq("underwritingId", underId ));
		for(UwRiFacExtend bo : boList){
			 try {
				BeanUtils.copyProperties(uwRiFacExtendVO, boList);
			} catch (Exception e) {
			}
			 resultList.add(uwRiFacExtendVO);
		}
                       
        return resultList;
        
	}
 */
	@Resource(name = ProductService.BEAN_DEFAULT)
	protected ProductService productService;
	
	/**
	 * <p>Description : 變更被保險人同步移除再保資料</p>
	 * <p>Created By : Simon.Huang</p>
	 * <p>Create Time : 2018年3月26日</p>
	 * @param insuredId
	 */
	@Override
	public void removeNBRiResultByInsuredId(Long insuredId) {
		try {
			SQLQuery sqlQuery = HibernateSession3.currentSession().createSQLQuery("delete T_NB_RI_RESULT where insured_id = ?");
			sqlQuery.setLong(0, insuredId);
			sqlQuery.executeUpdate();
		} catch (Exception ex) {
			throw ExceptionFactory.parse(ex);
		} 
	}
	
	@Override
	public void saveOrUpdateNbRiResultList(List<NbRiResultVO> riResultVOList) throws GenericException {
		if(CollectionUtils.isEmpty(riResultVOList)) {
			return;
		}
		for(NbRiResultVO vo : riResultVOList) {
			NbRiResult data =  riResultDelegate.load(new Long(vo.getListId()));
			data.setResultStatus(vo.getResultStatus()); // 再保會辦結果
			data.setReinsuredDate(vo.getReinsuredDate());//再保回覆日
			riResultDelegate.saveorUpdate(data);
		}
	}
	
	@Override
	public List<UwRiFacExtendVO> getTemporaryExtend(Long policyId, String certiCode) {
		List<UwRiFacExtendVO>  extendVOs = tuwRiFacExtendDelegate.getTemporaryExtendVOByPolicyIdAndCertiCode(policyId, certiCode);
		return extendVOs;
	}
	
	@Override
	public List<UwRiFacExtendVO> getSendExtend(Long policyId, String certiCode) {
	    List<UwRiFacExtendVO>  extendVOs = tuwRiFacExtendDelegate.getSendExtendVOByPolicyIdAndCertiCode(policyId, certiCode);
        return extendVOs;
	}
	
	@Override
	public List<UwRiFacExtendVO> getSendFacExtend(Long policyId) {
        List<UwRiFacExtendVO>  extendVOs = tuwRiFacExtendDelegate.getSendFaxExtendVOByPolicyId(policyId);
        return extendVOs;
	}

	
	@Override
	public UwRiFacVO getRiFacVO(Long uwListId) {
	    List<UwRiFac> items = uwRiFacDao.findByListId(uwListId);

	    if(items == null || items.size() < 1) {
	      return null;
	    }
	    
	    UwRiFacVO itemVO = new UwRiFacVO();
	    items.get(0).copyToVO(itemVO, true);
	    return itemVO;
	}
	
	@Override
	public UwRiFacVO getRiFacVO(Long extendId, Long itemId) {
	    UwRiFac item = uwRiFacDao.findByItemIdAndExtendId(extendId, itemId);

	    UwRiFacVO itemVO = new UwRiFacVO();
	    item.copyToVO(itemVO, true);
	    
	    return itemVO;
	    
	}
	
	@Override
	public List<NbReinsurerRequestsVO> getNbRequests(Long riFacId) {
		
		List<NbReinsurerRequestsVO> requestsVO = new ArrayList<NbReinsurerRequestsVO>();
		
		List<NbReinsurerRequests> requests = reinsurerRequestsDelegate.findByCriteria(Restrictions.eq("riFacId", riFacId));
		
		if (requests != null && requests.size() > 0) {
			
			for(NbReinsurerRequests request : requests) {
				
				NbReinsurerRequestsVO requestVO = new NbReinsurerRequestsVO();
				request.copyToVO(requestVO, true);
				requestsVO.add(requestVO);
				
			}
			
		}
		
		return requestsVO;
		
	}
	
	public void removeNbRequestByExtendId(Long extendId) {
		List<NbReinsurerRequests> boList = reinsurerRequestsDelegate.findByProperty("extendId", extendId);
		for(NbReinsurerRequests bo : boList) {
			reinsurerRequestsDelegate.remove(bo);
		}
	}
	
	public void removeUwRiFacByExtendId(Long extendId) {
	  uwRiFacDao.deleteRiFac(extendId);
	}
  
  public List<NbReinsurerRequestsVO> loadNbRequestsByExtendId(Long extendId) {
    
    List<NbReinsurerRequestsVO> voList = new ArrayList<NbReinsurerRequestsVO>();
    
    List<NbReinsurerRequests> boList = reinsurerRequestsDelegate.findByProperty("extendId", extendId);
    for(NbReinsurerRequests bo : boList) {
      NbReinsurerRequestsVO vo = new NbReinsurerRequestsVO();
      bo.copyToVO(vo, true);
      voList.add(vo);
    }
    
    return voList;
  }
  
  public List<NbReinsurerRequestsVO> loadNbRequests(Long reinsureId, Long riFacId) {
    
    List<NbReinsurerRequestsVO> voList = new ArrayList<NbReinsurerRequestsVO>();
    
    List<NbReinsurerRequests> boList = reinsurerRequestsDelegate.findByCriteria(
        Restrictions.eq("reinsurerId", reinsureId),
        Restrictions.eq("riFacID", riFacId));
    
    for(NbReinsurerRequests bo : boList) {
      NbReinsurerRequestsVO vo = new NbReinsurerRequestsVO();
      bo.copyToVO(vo, true);
      voList.add(vo);
    }
    
    return voList;
  }
	
	
	public UwRiFacVO saveOrUpdateUwRiFacVO(UwRiFacVO itemVO) {
		
		UwRiFac bo = uwRiFacDao.findByItemIdAndExtendId(itemVO.getExtendId(), itemVO.getItemId());
		bo.copyFromVO(itemVO, true, true);
		bo = uwRiFacDao.saveOrUpdateRiFac(bo);
		bo.copyToVO(itemVO, true);
		
		return itemVO;
		
	}
/*
 public List<UwRiFacExtendVO> getRiInfoByUnderwriteId(Long underwriteId) {
		DBean db = new DBean();
		PreparedStatement ps = null;
		ResultSet rs = null;
		UwRiFacExtendVO uwRiFacExtendVO = new UwRiFacExtendVO();
		List<UwRiFacExtendVO> resultList = new ArrayList<UwRiFacExtendVO>();
		Integer underId = NumericUtils.parseInteger(underwriteId);
		List<UwRiFacExtend> boList = super.findByCriteria(
                        Restrictions.eq("underwritingId", underId ));
		for(UwRiFacExtend bo : boList){
			 try {
				BeanUtils.copyProperties(uwRiFacExtendVO, boList);
			} catch (Exception e) {
			}
			 resultList.add(uwRiFacExtendVO);
		}
                       
        return resultList;
        
	}
 */
}
