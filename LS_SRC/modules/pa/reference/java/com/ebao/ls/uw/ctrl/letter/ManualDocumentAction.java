package com.ebao.ls.uw.ctrl.letter;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ebao.foundation.module.util.beanutils.BeanUtils;
import com.ebao.foundation.module.util.i18n.CodeTable;
import com.ebao.foundation.module.web.action.ExceptionInfoUtils;
import com.ebao.ls.arap.pub.StringUtils;
import com.ebao.ls.cmu.pub.document.DocumentFormFile;
import com.ebao.ls.cmu.pub.vo.DocumentVO;
import com.ebao.ls.cs.common.report.CSReportService;
import com.ebao.ls.escape.helper.EscapeHelper;
import com.ebao.ls.notification.extension.letter.cs.PosTitleVO;
import com.ebao.ls.notification.extension.letter.nb.NbLetterExtensionVO;
import com.ebao.ls.notification.message.WSLetterUnit;
import com.ebao.ls.pa.nb.data.bo.DocumentImage;
import com.ebao.ls.pty.ci.DeptCI;
import com.ebao.ls.pub.CodeCst;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.framework.ExceptionFactory;
import com.ebao.pub.framework.GenericException;
import com.ebao.pub.i18n.lang.StringResource;
import com.ebao.pub.util.json.DateUtils;
import com.ebao.ls.pa.nb.util.NBUtils;

public class ManualDocumentAction extends UNBLetterAction {

	private static final String QUERYINFO = "queryInfo";
    protected final static String BATCH_PRINT__SUCCESS = "MSG_1257540";

    protected final static String ONLINE_PRINT__SUCCESS = "MSG_1257541";

    @Resource(name = DeptCI.BEAN_DEFAULT)
    private DeptCI deptCI;
    
    @Resource(name = CSReportService.BEAN_DEFAULT)
	private CSReportService cSReportService;

    private static final Logger log = LoggerFactory.getLogger(ManualDocumentAction.class);
    /**
     * Action進入點
     */
    @Override
    public ActionForward process(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws GenericException {
          
        UNBLetterForm unbLetterForm = (UNBLetterForm) form;
        String actionType = unbLetterForm.getActionType();
        try {    
        	
            List<DocumentFormFile> uploadFileList = super.nbLetterHelper.getUploadFileList(request, unbLetterForm);

            UNBLetterForm beanBak = new UNBLetterForm();
            BeanUtils.copyProperties(beanBak, unbLetterForm);
            
            String[] applyImageList = unbLetterForm.getApplyImageList();
            String channelId = unbLetterForm.getChannelIdText();
            String agentId = unbLetterForm.getAgentIdText();
            String documentContent = unbLetterForm.getDocumentContent();
            
            // PLC 393544 保費新增欄位
            String receCetiDate = beanBak.getReceCetiDate();
            String donePatchDate = beanBak.getDonePatchDate();
            String cetiReason1 = beanBak.getCetiReason1();
			String cetiReason2 = beanBak.getCetiReason2();
			String cetiReason3 = beanBak.getCetiReason3();
			String cetiReason4 = beanBak.getCetiReason4();
			String cetiReason5 = beanBak.getCetiReason5();
			String cetiReason6 = beanBak.getCetiReason6();
			String cetiReason7 = beanBak.getCetiReason7();
			String cetiReason8 = beanBak.getCetiReason8();
            if (SAVE.equals(actionType) || PERVIEW.equals(actionType) || ONLINE_PRINT.equals(actionType)) {
            	try {
            		// PLC 393544 非保費清空
            		if (unbLetterForm.getMainCategory() != 14) {
            			unbLetterForm.setChannelIdDesc(null);
            			unbLetterForm.setAgentIdDesc(null);
            		}
	                saveNewLetter(request, response, unbLetterForm, uploadFileList);
	                if (unbLetterForm.getDocumentNo() != null) {
	                    if (SAVE.equals(actionType)) {
	                        unbLetterForm.setRetMsg(BATCH_PRINT__SUCCESS);
	                    } else if (ONLINE_PRINT.equals(actionType)) {
	                        unbLetterForm.setRetMsg(ONLINE_PRINT__SUCCESS);
	                    }
	                }
            	} catch(Exception ex) {
            		log.error(ExceptionInfoUtils.getExceptionMsg(ex));
            		request.setAttribute("errorMsg",StringResource.getStringData("MSG_1262276", CodeCst.LANGUAGE__CHINESE_TRAD)+":"+ex.getMessage());
            	}
                unbLetterForm.setApplyImageList(unbLetterForm.getApplyImageList());
                EscapeHelper.escapeHtml(request).setAttribute("templateId",request.getParameter("templateId"));
				EscapeHelper.escapeHtml(request).setAttribute("applyImageList", unbLetterForm.getApplyImageList());
                
            }  else if ( ADD.equals(actionType) ) {
                Long templatedId = unbLetterForm.getTemplateId();
                String policyNo = unbLetterForm.getPolicyNo();
                unbLetterForm = new UNBLetterForm();
                if (request.getParameter("forWordMode") != null ) {
                	unbLetterForm.setTemplateId(templatedId);
                	unbLetterForm.setPolicyNo(policyNo);
                	unbLetterForm.setForWordMode(request.getParameter("forWordMode") );
                }
                
                uploadFileList.clear();
                
                
			} else if (QUERYINFO.equals(actionType)) {
				// 參考BillStatusQuery, 只有外勤
				Map<String, String> sharedOIU = cSReportService.initShared(unbLetterForm.getPolicyId());
				PosTitleVO ptVO = cSReportService.getPosTitle(sharedOIU, new Date());
            	
				channelId = ptVO.getChannelCode();
				agentId = ptVO.getServiceAgentId();
            } else if ( unbLetterForm.getDocumentId() > 0L) {
            	/**修改照會=>由照會查詢畫面進入**/
            	Long documentId = unbLetterForm.getDocumentId();
            	DocumentVO documentVO = super.documentService.load(documentId);
            	unbLetterForm.setTemplateId(documentVO.getTemplateId());
            	unbLetterForm.setPolicyNo(documentVO.getPolicyCode());
            	unbLetterForm.setPolicyId(documentVO.getPolicyId());
            	unbLetterForm.setDocumentNo(documentVO.getDocumentNo());
            	
            	List<String> contents = super.commonLetterService.getManualDocumentContent(documentId);

            	documentContent = "";
            	boolean isFirst =true;
            	for(String str:contents) {
            		if ( isFirst) {
            			isFirst = false;
            		} else {
            			documentContent += "\r\n";
            		}
            		documentContent += str;
            	}
            	
				Map<String, Object> unit = super.documentService.getClobContent(unbLetterForm.getDocumentNo());
				if (unit != null) {
					Map<String, Object> wsLetterUnit = (Map<String, Object>) unit.get("WSLetterUnit");
					if (wsLetterUnit.get("agentNoteMergedInfo") != null) {
						Map<String, Object> agentNoteMergedInfo = (Map<String, Object>) wsLetterUnit.get("agentNoteMergedInfo");
						Map<String, Object> noteTo = (Map<String, Object>) agentNoteMergedInfo.get("noteTo");
						agentId = StringUtils.O2S(noteTo.get("sendTo1Id"));
					}
					if (wsLetterUnit.get("extension") != null) {
						Map<String, Object> extension = (Map<String, Object>) wsLetterUnit.get("extension");
						channelId = StringUtils.O2S(extension.get("senderUnitCode"));
						receCetiDate = parseDate(StringUtils.O2S(extension.get("receCetiDate")));
						donePatchDate = parseDate(StringUtils.O2S(extension.get("donePatchDate")));
						cetiReason1 = StringUtils.O2S(extension.get("cetiReason1"));
						cetiReason2 = StringUtils.O2S(extension.get("cetiReason2"));
						cetiReason3 = StringUtils.O2S(extension.get("cetiReason3"));
						cetiReason4 = StringUtils.O2S(extension.get("cetiReason4"));
						cetiReason5 = StringUtils.O2S(extension.get("cetiReason5"));
						cetiReason6 = StringUtils.O2S(extension.get("cetiReason6"));
						cetiReason7 = StringUtils.O2S(extension.get("cetiReason7"));
						cetiReason8 = StringUtils.O2S(extension.get("cetiReason8"));
						if (extension.get("documentContent") != null && StringUtils.isBlank(documentContent)) {
							documentContent = StringUtils.O2S(extension.get("documentContent"));
						}
					}
				}
            	
            	//組已上傳檔影像檔清單
            	List<DocumentImage> documentImageList = super.commonLetterService.getDocumentImage(Long.toString(documentId));
            	Set<String> applyImageStringList = new HashSet<String>();
            	if( uploadFileList.size() == 0 &&  documentImageList.size() > 0) {
            		for( DocumentImage documentImage : documentImageList) {
                		if( documentImage.getImageIds() == null) {
                			
                			DocumentFormFile documentFormFile = new DocumentFormFile();
                			documentFormFile.setArchivePath(documentImage.getArchivePath());
                			documentFormFile.setFileName(documentImage.getFileName());
                			documentFormFile.setImageName(documentImage.getImageName());
                			
                			uploadFileList.add(documentFormFile);
                		}else {
                			applyImageStringList.add(documentImage.getImageIds());
                		}
            			
            		}
            		
            		if(applyImageList == null) {
            			applyImageList = new String[applyImageStringList.size()];
            			int i = 0;
            			for( String applyImageIds : applyImageStringList) {
                			applyImageList[i++]=applyImageIds;
            			}
            		}
            	}
            	
            	unbLetterForm.setActionType(SAVE);
            	unbLetterForm.setApplyImageList(applyImageList);
            	
            } else if (unbLetterForm.getTemplateId() == 20030) {
            	/**帶入簽收單照會相關內容**/
				String signResult = request.getParameter("signResult");
            	String holderSignResult = request.getParameter("holderSignResult");
            	String legalSignResult = request.getParameter("legalSignResult");
            	StringBuffer sb = new StringBuffer();
            	
            	if ( holderSignResult != null) {
            		sb.append(StringResource.getStringData("MSG_1254406", request)).append("=");
            		sb.append(CodeTable.getCodeDesc("T_POLICY_SIGN_RESULT", holderSignResult, request));            		
            	}
            	if ( signResult != null) {
            		if (sb.length() > 0) sb.append(";");
            		sb.append(StringResource.getStringData("MSG_1254405", request)).append("=");
            		sb.append(CodeTable.getCodeDesc("T_POLICY_SIGN_RESULT", signResult, request));            		
            	}
            	if ( legalSignResult != null) {
            		if (sb.length() > 0) sb.append(";");
            		sb.append(StringResource.getStringData("MSG_1253998", request)).append("=");
            		sb.append(CodeTable.getCodeDesc("T_POLICY_SIGN_RESULT", legalSignResult, request));            		
            	}
            	documentContent = sb.toString();
            	
            } else if (unbLetterForm.getTemplateId() == 20034) {
            	/**帶入承保後照會相關內容**/
            	documentContent = NBUtils.getTWMsg("MSG_1267023");
            }

        	unbLetterForm.setChannelIdText(channelId);
        	unbLetterForm.setAgentIdText(agentId);
            unbLetterForm.setUploadFileList(uploadFileList);
        	unbLetterForm.setDocumentContent(documentContent);
        	
            unbLetterForm.setChannelIdDesc(beanBak.getChannelIdDesc());
			unbLetterForm.setAgentIdDesc(beanBak.getAgentIdDesc());
			unbLetterForm.setReceCetiDate(receCetiDate);
			unbLetterForm.setDonePatchDate(donePatchDate);
			unbLetterForm.setCetiReason1(cetiReason1);
			unbLetterForm.setCetiReason2(cetiReason2);
			unbLetterForm.setCetiReason3(cetiReason3);
			unbLetterForm.setCetiReason4(cetiReason4);
			unbLetterForm.setCetiReason5(cetiReason5);
			unbLetterForm.setCetiReason6(cetiReason6);
			unbLetterForm.setCetiReason7(cetiReason7);
			unbLetterForm.setCetiReason8(cetiReason8);
			
			EscapeHelper.escapeHtml(request).setAttribute("uploadFileList",uploadFileList);
            EscapeHelper.escapeHtml(request).setAttribute(com.ebao.pub.Cst.ACTION_FORM, unbLetterForm);
			EscapeHelper.escapeHtml(request).setAttribute("forWordMode", request.getParameter("forWordMode"));
            Integer categoryIn =deptCI.getBizCategoryByDeptId(AppContext.getCurrentUser().getDeptId());
			EscapeHelper.escapeHtml(request).setAttribute("categoryIn", categoryIn);
			if(PERVIEW.equals(actionType) == false){
				return mapping.findForward("success");
			}else {
				if(request.getAttribute("errorMsg") != null) {
					request.setAttribute("error", request.getAttribute("errorMsg"));
	        		return mapping.findForward("previewError");
				} else {
					return mapping.findForward(FORWARD_WINDOW_CLOSE);
				}
			}
        } catch (Exception e) {
        	if ( PERVIEW.equals(actionType)) {
        		com.ebao.pub.util.Log.error(this.getClass(), e.getMessage());
        		request.setAttribute("error","create Pdf error");
        		return mapping.findForward("previewError");
        	}
            throw ExceptionFactory.parse(e);
        }
    } 
    
    @Override
    protected NbLetterExtensionVO getExtension(ActionForm actionForm) {
        
        UNBLetterForm unbLetterForm = (UNBLetterForm) actionForm;
        Long key =new Long(unbLetterForm.getTemplateId());
        
        NbLetterExtensionVO extension =  new NbLetterExtensionVO();
        
        extension.setPolicyCode(unbLetterForm.getPolicyNo());
        //extension.setDocumentContent();
        
        return extension;
    }
    
    private String parseDate(String org) {
    	try {
        	String result = org.replace("年", "/").replace("月", "/").replace("日", "");
        	if (result.length() == 9) {
        		result = (Integer.valueOf(result.split("/")[0]) + 1911) + result.substring(result.indexOf("/"), result.length());
        	}
    		return DateUtils.format(DateUtils.getDateByFormat(result, "yyyy/MM/dd"), "yyyy-MM-dd");
    	} catch (Exception e) {
    		return "";
    	}
    }
    

    
}
