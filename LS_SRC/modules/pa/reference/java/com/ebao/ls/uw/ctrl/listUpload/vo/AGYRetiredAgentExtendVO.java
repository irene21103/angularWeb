package com.ebao.ls.uw.ctrl.listUpload.vo;

import com.ebao.ls.pa.nb.vo.AGYRetiredAgentVO;

public class AGYRetiredAgentExtendVO extends AGYRetiredAgentVO {

	/**
	 * <code>serialVersionUID</code> 的註解
	 */
	private static final long serialVersionUID = 312613112484068512L;
	
	private Long serialNumber;


    private String errorMsg;

    public Long getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(Long serialNumber) {
		this.serialNumber = serialNumber;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

}
