package com.ebao.ls.riskPrevention.ctrl;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.UserTransaction;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.foundation.module.db.Trans;
import com.ebao.ls.pty.ds.DeptService;
import com.ebao.ls.pub.auth.AuthItemCst;
import com.ebao.ls.riskPrevention.bs.NationalityCodeLogService;
import com.ebao.ls.riskPrevention.bs.NationalityCodeService;
import com.ebao.ls.riskPrevention.bs.impl.NationalityCodeAuthServiceImpl;
import com.ebao.ls.riskPrevention.vo.NationalityCodeLogVO;
import com.ebao.ls.riskPrevention.vo.NationalityCodeVO;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.framework.GenericAction;
import com.ebao.pub.util.BeanUtils;
import com.ebao.pub.util.TransUtils;

public class NationalityCodeAction extends GenericAction {
	
	@Resource(name = NationalityCodeService.BEAN_DEFAULT)
	NationalityCodeService nationalityCodeService;
	
	@Resource(name = NationalityCodeLogService.BEAN_DEFAULT)
	NationalityCodeLogService nationalityCodeLogService;
	
	@Resource(name = DeptService.BEAN_DEFAULT)
	DeptService deptService;
	
	@Resource(name = NationalityCodeAuthServiceImpl.BEAN_DEFAULT)
	NationalityCodeAuthServiceImpl pageAuthService;
	
	@Override
	public ActionForward process(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		boolean auth = pageAuthService.hasAccessRight(AuthItemCst.AUTH_ITEM_PA_NATIONALITY_CODE);
		NationalityCodeForm cForm = (NationalityCodeForm)form;
		request.setAttribute("currentUser", AppContext.getCurrentUser().getUserName());
		request.setAttribute("currentTime", AppContext.getCurrentUserLocalTime());
		request.setAttribute("auth", auth);
		UserTransaction ut =  Trans.getUserTransaction();
		switch(cForm.getAction()){
				
			case SEARCH:
				NationalityCodeLogVO vo = new NationalityCodeLogVO();
				if("".equals(cForm.getSearchVO().getNationalCode())){
					vo.setNationalRisk(cForm.getSearchVO().getNationalRisk());
					request.setAttribute("list", nationalityCodeLogService.riskQuery(vo, cForm.getPager()));
				} else {
					vo.setNationalCode(cForm.getSearchVO().getNationalCode());
					request.setAttribute("list", nationalityCodeLogService.nationalQuery(vo, cForm.getPager(), false));
				}
				break;
				
			case SAVE:
				if(auth){
					try{
						ut.begin();
						if(cForm.getVo().getNationalCode()==null || cForm.getVo().getNationalName()==null || cForm.getVo()==null)
							break;
						NationalityCodeVO newVO = cForm.getVo();
						nationalityCodeService.update(newVO, cForm.getPager());
						NationalityCodeLogVO newLog = new NationalityCodeLogVO();
						BeanUtils.copyProperties(newLog, newVO);
						newLog.setUpdatedDeptID(AppContext.getCurrentUser().getDeptId());
						newLog.setUpdatedDeptName(deptService.getDeptById(AppContext.getCurrentUser().getDeptId()).getDeptName());
						newLog.setInsertBy(AppContext.getCurrentUser().getUserId());
						newLog.setUpdateBy(AppContext.getCurrentUser().getUserId());
						newLog.setInsertTime(AppContext.getCurrentUserLocalTime());
						newLog.setInsertTimestamp(newLog.getInsertTime());
						newLog.setUpdateTime(newLog.getInsertTime());
						newLog.setUpdateTimestamp(newLog.getInsertTime());
						nationalityCodeLogService.save(newLog);
						ut.commit();
						request.setAttribute("list", nationalityCodeLogService.nationalQuery(newLog, cForm.getPager(), false));
					}catch(Exception e){
						TransUtils.rollback(ut);
						throw e;
					}
				} else {
					request.setAttribute("authError", true);
				}
				break;
			
			default:
				break;
		}
		return mapping.findForward("success");
	}
}
