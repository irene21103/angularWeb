package com.ebao.ls.crs.service;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.util.WebUtils;

import com.ebao.ls.crs.constant.CRSCodeCst;
import com.ebao.ls.pub.ajax.AjaxService;

import net.sf.json.JSONObject;

public class CRSPartyAjaxService  implements AjaxService<HashMap<String,String>, JSONObject>{
	
	public final String BEAN_DEFAULT = "crsPartyAjaxService";
	
	@Override
	public JSONObject execute(HashMap<String, String> inputMap) {
		String execute = MapUtils.getString(inputMap, "execute");
		if(CRSCodeCst.WEB_ACTION_TYPE__CLEAN_SESSION.equals(execute)) {
			doCleanSession();
		}
		return null;
	}
	
	private void doCleanSession() {
		ServletRequestAttributes attr = (ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
		Object loginActionType = WebUtils.getSessionAttribute(attr.getRequest(), CRSCodeCst.WEB_ACTION_TYPE__LOGIN_ACTION_TYPE);
		System.out.println("loginActionType := " + loginActionType);
	}

	
}
