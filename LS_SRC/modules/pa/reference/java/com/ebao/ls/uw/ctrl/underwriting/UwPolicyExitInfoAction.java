package com.ebao.ls.uw.ctrl.underwriting;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.UserTransaction;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.foundation.module.db.hibernate.HibernateSession3;
import com.ebao.ls.escape.helper.EscapeHelper;
import com.ebao.ls.liaRoc.LiarocDownloadLogCst;
import com.ebao.ls.liaRoc.ds.LiarocDownloadLogHelper;
import com.ebao.ls.pa.nb.bs.ProposalProcessService;
import com.ebao.ls.pa.nb.bs.ProposalService;
import com.ebao.ls.pa.nb.bs.rule.ProposalRuleService;
import com.ebao.ls.pa.nb.ctrl.detailreg.DetailRegAllInOneForm;
import com.ebao.ls.pa.nb.ctrl.detailreg.DetailRegForm;
import com.ebao.ls.pa.nb.ctrl.detailreg.DetailRegHelper;
import com.ebao.ls.pa.nb.util.NBUtils;
import com.ebao.ls.pa.pub.bs.AgentNotifyEncoService;
import com.ebao.ls.pa.pub.bs.AgentNotifyPurposeService;
import com.ebao.ls.pa.pub.bs.AgentNotifyService;
import com.ebao.ls.pa.pub.bs.IlpRiskService;
import com.ebao.ls.pa.pub.bs.MailToService;
import com.ebao.ls.pa.pub.bs.PolicyBankAuthService;
import com.ebao.ls.pa.pub.bs.PolicyConversionService;
import com.ebao.ls.pa.pub.bs.PolicyMiscFeeService;
import com.ebao.ls.pa.pub.bs.PolicyService;
import com.ebao.ls.pa.pub.bs.ReviewService;
import com.ebao.ls.pa.pub.vo.PolicyVO;
import com.ebao.ls.pa.pub.vo.VOGenerator;
import com.ebao.ls.pub.CodeCst;
import com.ebao.ls.uw.ds.UwPolicyService;
import com.ebao.ls.uw.ds.UwProcessService;
import com.ebao.pub.event.EventService;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.framework.ExceptionFactory;
import com.ebao.pub.framework.GenericAction;
import com.ebao.pub.util.DateUtils;
import com.ebao.pub.util.StringUtils;
import com.ebao.pub.util.Trans;
import com.ebao.pub.util.TransUtils;
import com.ebao.pub.workflow.jbpm.JbpmContextUtil;

/**
 * <p>
 * Title:Underwriting
 * </p>
 * <p>
 * Description:
 * </p>
 * <p>
 * Copyright: Copyright (c) 2004
 * </p>
 * <p>
 * Company: eBaoTech Corporation
 * </p>
 * 
 * @author sunny
 * @since 2015-05-22
 * @version 1.0
 */
public class UwPolicyExitInfoAction extends GenericAction {
    public static final String BEAN_DEFAULT = "/uw/uwPolicyExitInfoAction";

    @Resource(name = UwPolicyService.BEAN_DEFAULT)
    private UwPolicyService uwPolicyDS;

    @Resource(name = UwProcessService.BEAN_DEFAULT)
    private UwProcessService uwProcessDS;

    @Resource(name = EventService.BEAN_DEFAULT)
    private EventService eventService;

    @Override
    public ActionForward process(ActionMapping mapping, ActionForm form,
                    HttpServletRequest request, HttpServletResponse response)
                    throws Exception {
        UserTransaction ut = null;
        DetailRegAllInOneForm detailRegForm = (DetailRegAllInOneForm) form;
        ActionForward forword = null;
        Long policyId = Long.valueOf(detailRegForm.getPolicyId());
        try {
    
            this.beforeCopyFormData(detailRegForm);
            org.hibernate.Session s = com.ebao.foundation.module.db.hibernate.HibernateSession3
					.currentSession();
        
            PolicyVO policyVO = policyService.load(policyId);
            ut = Trans.getUserTransaction();
            ut.begin();

            proposalService.saveProposalVer2(policyVO);
            detailRegHelper.saveWaiverCoverage(policyId);
            
            policyVO = policyService.load(policyVO.getPolicyId());
			proposalService.synchronizeToParty(policyVO);
			
            s.flush();
			detailRegHelper.setVerificationDefaultValue(policyVO, null, null, null, null);
            policyService.save(policyVO);
        	
            s.flush();
            proposalService.doSubmitDetailRegistration(policyVO);
            
            ut.commit();
        } catch (Exception e) {
            TransUtils.rollback(ut);
            throw ExceptionFactory.parse(e);
        }
        
        boolean isSuccess = false;
        try {
        	HibernateSession3.detachSession();
        	ut = Trans.getUserTransaction();
        	ut.begin();
        	LiarocDownloadLogHelper.initNB(LiarocDownloadLogCst.NB_UW_MODIFY_PAGE_EXIT_CHECK_RULE, policyId);
        	//核保中修改，重新執行OPQ欄規則校驗
            boolean isEcOffline = policyService.isEcOffline(policyId);
			if (isEcOffline) {
				proposalService.checkProposalRules(policyId, ProposalRuleService.ROLESSET_VERIFICATION_EC_OFFLINE);
			} else {
				proposalService.checkProposalRules(policyId, ProposalRuleService.ROLESSET_UW_MODIFIY);
			}

        	HibernateSession3.currentSession().flush();

        	ut.commit();
        	
        	isSuccess = true;
        } catch (Exception e) {
        	TransUtils.rollback(ut);
        	throw ExceptionFactory.parse(e);
        } finally {
			LiarocDownloadLogHelper.flush();
		}

        try {
            ut = Trans.getUserTransaction();
            ut.begin(); 

            if(isSuccess) {
				//增加核保流程控管機制
				NBUtils.logger(this.getClass(), "policyDS.updatePolicyOPQCheckIndi() begin");
				policyService.updatePolicyOPQCheckIndi(policyId, CodeCst.OPQ_CHECK_FINISH);	
				NBUtils.logger(this.getClass(), "policyDS.updatePolicyOPQCheckIndi() end");
            }
            
            String underwriteId = com.ebao.ls.encode.encoder.Encode.encodeForHTML(request).getParameter("underwriteId");
            Long userId = AppContext.getCurrentUser().getUserId();
            
            uwProcessDS.updateNBInfo(Long.valueOf(underwriteId), userId);
            EscapeHelper.escapeHtml(request).setAttribute("underwriteId", underwriteId);
            ut.commit();
        } catch (Exception e) {
            TransUtils.rollback(ut);
            throw ExceptionFactory.parse(e);
        }
        // update to control NBU process with workflow engine,by robert.xu
        // on
        // 2008.4.17
        // add to control NBU process with workflow engine,by robert.xu on
        // 2008.4.17
        // WfHelper.processManagerForSaveOrCancel(policyId);
        // WfUtils.maintainVariable(policyId, null, null);
       
        try {
        	HibernateSession3.detachSession();
        	//若有系統關閉已發放核保訊息，可能會會更新照會單狀態，會執行一次workflow變數更新
        	//執行detachSession會造成jbpm底層session抓到舊的,先close讓底層重抓
        	JbpmContextUtil.closeJbpmContext();
        	//END
        	ut = Trans.getUserTransaction();
        	ut.begin(); 
        	proposalProcessService.maintainFlowVariable(policyId);
        	ut.commit();
        } catch (Exception e) {
        	TransUtils.rollback(ut);
        	throw ExceptionFactory.parse(e);
        }
        // add end
           
        request.setAttribute("policyId", policyId);
        EscapeHelper.escapeHtml(request).setAttribute("originType",EscapeHelper.escapeHtml(request.getParameter("originType")));
        forword = mapping.findForward("success");
        // update end
        return forword;
    }

    protected void beforeCopyFormData(DetailRegForm form) {
        if (!StringUtils.isNullOrEmpty(form.getInitialPrem())) {
            form.setInitialPrem(StringUtils.replace(form.getInitialPrem(), ",",
                            ""));
        }
        if (!StringUtils.isNullOrEmpty(form.getPremAmount())) {
            form.setPremAmount(StringUtils.replace(form.getPremAmount(), ",",
                            ""));
        }
        if (!StringUtils.isNullOrEmpty(form.getMiscFee())) {
            form.setMiscFee(StringUtils.replace(form.getMiscFee(), ",", ""));
        }
        if (!"99".equals(form.getUwReason())) {
            form.setUwReasonDesc(null);
        }
        // if the commencement date is null, set it to system time
        // and set backdating indicator to "N"
        if (StringUtils.isNullOrEmpty(form.getValidateDate())) {
            String sysDate = DateUtils.date2String(AppContext
                            .getCurrentUserLocalTime());
            form.setValidateDate(sysDate);
            form.setBackdatingIndi("N");
        }
    }

    public int findIndexOfArray(String key, String[] array) {
        if (StringUtils.isNullOrEmpty(key)) {
            return -1;
        }
        if (array == null) {
            return -1;
        }
        for (int i = 0; i < array.length; i++) {
            if (key.equals(array[i])) {
                return i;
            }
        }
        return -1;
    }

    @Resource(name = PolicyService.BEAN_DEFAULT)
    protected PolicyService policyService;

    @Resource(name = ProposalService.BEAN_DEFAULT)
    protected ProposalService proposalService;

    @Resource(name = PolicyMiscFeeService.BEAN_DEFAULT)
    protected PolicyMiscFeeService policyMiscFeeService;

    @Resource(name = PolicyConversionService.BEAN_DEFAULT)
    protected PolicyConversionService policyConversionService;

    @Resource(name = VOGenerator.BEAN_DEFAULT)
    protected VOGenerator voGenerator;

    @Resource(name = DetailRegHelper.BEAN_DEFAULT)
    protected DetailRegHelper detailRegHelper;

    @Resource(name = ProposalProcessService.BEAN_DEFAULT)
    protected ProposalProcessService proposalProcessService;

    @Resource(name = AgentNotifyService.BEAN_DEFAULT)
    private AgentNotifyService agentNotifyService;

    @Resource(name = AgentNotifyEncoService.BEAN_DEFAULT)
    private AgentNotifyEncoService agentNotifyEncoService;

    @Resource(name = AgentNotifyPurposeService.BEAN_DEFAULT)
    private AgentNotifyPurposeService agentNotifyPurposeService;

    @Resource(name = PolicyBankAuthService.BEAN_DEFAULT)
    private PolicyBankAuthService policyBankAuthService;

    @Resource(name = IlpRiskService.BEAN_DEFAULT)
    private IlpRiskService riskService;

    @Resource(name = MailToService.BEAN_DEFAULT)
    private MailToService mailToService;

    @Resource(name = ReviewService.BEAN_DEFAULT)
    private ReviewService reviewService;

}