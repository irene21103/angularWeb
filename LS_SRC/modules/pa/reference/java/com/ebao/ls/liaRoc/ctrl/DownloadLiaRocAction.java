package com.ebao.ls.liaRoc.ctrl;

import java.util.LinkedHashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.UserTransaction;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.foundation.module.util.i18n.StringResource;
import com.ebao.ls.liaRoc.ci.LiaRocDownloadWSCI;
import com.ebao.ls.pa.pty.service.CertiCodeCI;
import com.ebao.ls.pa.pub.service.PolicyService;
import com.ebao.ls.pa.pub.vo.CoverageVO;
import com.ebao.ls.pa.pub.vo.PolicyVO;
import com.ebao.ls.rstf.ci.liaroc20.Liaroc20Cst;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.framework.GenericAction;
import com.ebao.pub.util.Trans;
import com.ebao.pub.util.TransUtils;

/**
 * <h1>公會資料下載</h1><p>
 * @since 2016/03/19<p>
 * @author Amy Hung
 * <p>Update Time: </p>
 * <p>Updater: </p>
 * <p>Update Comments: </p>
*/
public class DownloadLiaRocAction extends GenericAction {

    @Resource(name = LiaRocDownloadWSCI.BEAN_DEFAULT)
    private LiaRocDownloadWSCI liaRocDownloadWSCI;

    @Resource(name = CertiCodeCI.BEAN_DEFAULT)
    private CertiCodeCI certiCodeCI;

    @Resource(name = PolicyService.BEAN_DEFAULT)
    private PolicyService policyService;

    @Override
    public ActionForward process(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                    HttpServletResponse response) throws Exception {

        DownloadLiaRocForm oForm = (DownloadLiaRocForm) form;

        if (StringUtils.equals("download", oForm.getActionType())) {
            download(oForm, request);
        } else {

            // 從理賠或是保全過來，傳入保單號碼自動帶入所有的被保人ID
            if (request.getParameter("policyId") != null) {
                try {
                    Long policyId = Long.valueOf(request.getParameter("policyId").toString());
                    generateRowByPolicyId(policyId, oForm);
                } catch (Exception e) {
                    // do nothing.
                }
            }

            if (oForm.getDownloadIds().size() == 0) {
                oForm.getDownloadIds().add("");
            }

        }

        return mapping.findForward("download");

    }

    /**
     * <p>Description : 下載公會資料</p>
     * <p>Created By : Amy Hung</p>
     * <p>Create Time : Mar 21, 2016</p>
     * @param form
     * @param request
     */
    private void download(DownloadLiaRocForm form, HttpServletRequest request) {

        boolean isPass = true;
        
        String ssString =AppContext.getCurrentUser()
                        .getLangId();

        // @check 輸入的身份證號／統一編(證)號
        /*for (String id : form.getDownloadIds()) {
            CertiCodeCheckResultVO chkVO = certiCodeCI.checkCertiCode(id);
            if (!chkVO.isValidated()) {
                isPass = false;
                form.getErrorIds().add(id);
            }
        }*/
        // end.

        if (!isPass) {
            // 顯示錯誤的資料提示 user
            String msg = StringResource.getStringData("MSG_1257675", AppContext.getCurrentUser().getLangId());
            msg = msg + StringUtils.join(form.getErrorIds().toArray(), ", ");
            request.setAttribute("saveMsg", msg);
        } else {

            // @呼叫服務平台下載公會資料
            UserTransaction trans = null;
            try {

                trans = Trans.getUserTransaction();
                trans.begin();

                Set<String> idsSet = new LinkedHashSet<String>(form.getDownloadIds());

                isPass = liaRocDownloadWSCI.send(Liaroc20Cst.MN_CMN.getCode(), idsSet);

                trans.commit();

            } catch (Exception e) {
                TransUtils.rollback(trans);
                isPass = false;
                Logger.getLogger(DownloadLiaRocAction.class.getName())
                                .log(Level.INFO, "呼叫公會下載資料失敗, Msg:" + e.getMessage());
            }
            // end.

            if (isPass) {
                form.getDownloadIds().clear();
                form.getDownloadIds().add("");
                request.setAttribute("saveMsg",
                                StringResource.getStringData("MSG_1257673", AppContext.getCurrentUser().getLangId()));
            } else {
                request.setAttribute("saveMsg",
                                StringResource.getStringData("MSG_1257674", AppContext.getCurrentUser().getLangId()));
            }

        }

    }

    /**
     * <p>Description : 從理賠或是保全過來，傳入保單號碼自動帶入所有的被保人ID</p>
     * <p>Created By : Amy Hung</p>
     * <p>Create Time : Mar 22, 2016</p>
     * @param policyId
     */
    public void generateRowByPolicyId(Long policyId, DownloadLiaRocForm form) {

        PolicyVO policyVO = policyService.loadPolicyByPolicyId(policyId);

        if (policyVO == null)
            return;

        for (CoverageVO coverage : policyVO.getCoverages()) {
            if (coverage.getLifeInsured1() != null) {
                String certiCode = coverage.getLifeInsured1().getInsured().getCertiCode();
                if (form.getDownloadIds().indexOf(certiCode) == -1) {
                    form.getDownloadIds().add(certiCode);
                }
            }
        }

    }

}
