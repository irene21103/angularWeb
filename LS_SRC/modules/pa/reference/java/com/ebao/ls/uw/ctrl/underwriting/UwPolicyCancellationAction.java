package com.ebao.ls.uw.ctrl.underwriting;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.ls.cs.ci.CSCI;
import com.ebao.ls.pub.CodeCst;
import com.ebao.ls.uw.ctrl.UwGenericAction;
import com.ebao.ls.uw.ds.UwProcessService;
import com.ebao.ls.uw.ds.constant.UwSourceTypeConstants;
import com.ebao.ls.uw.util.Utils;
import com.ebao.ls.uw.vo.UwPolicyVO;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.framework.ExceptionFactory;
import com.ebao.pub.framework.GenericException;

/**
 * <p>
 * Title:Underwriting
 * </p>
 * <p>
 * Description:
 * </p>
 * <p>
 * Copyright: Copyright (c) 2004
 * </p>
 * <p>
 * Company: eBaoTech Corporation
 * </p>
 * 
 * @author jason.luo
 * @since Time:Jun 26, 2005
 * @version 1.0
 */
public class UwPolicyCancellationAction extends UwGenericAction {
  /**
   * Cancel UwProposal underwriting
   * 
   * @param mapping The ActionMapping used to select this instance
   * @param form The optional ActionForm bean for this request (if any)
   * @param request HttpRequest
   * @param response HttpResponse
   * @return ActionForward ActionForward
   * @throws java.lang.Exception Application Exception
   */
  @Override
  public ActionForward uwProcess(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws GenericException {
    String NEXT_PAGE = "sharingPoolMainPage";
    Long policyId = Long.valueOf(request.getParameter("policyId"));
    try {
      Long underwriteId = Long.valueOf(com.ebao.ls.encode.encoder.Encode.encodeForHTML(request).getParameter("underwriteId"));
      Long underwriterId = Long
          .valueOf(AppContext.getCurrentUser().getUserId());
      // trans = Trans.getUserTransaction();
      // trans.begin();
			UwPolicyVO vo = getUwPolicyDS().findUwPolicy(underwriteId);
			if (!UwSourceTypeConstants.NEW_BIZ.equals(vo.getUwSourceType())) {
				uwProcessDS.quashUnderwriting(underwriteId, underwriterId, "");
			}
      if (Utils.isCsUw(vo.getUwSourceType())) {
        csCI.updateApplicationInfoByUW(vo.getChangeId(),
            Integer.valueOf(CodeCst.CS_APP_STATUS__PEND_UW), null);
      }
      // add to control NBU process with workflow engine,by robert.xu on
      // 2008.4.17
      if (UwSourceTypeConstants.NEW_BIZ.equals(vo.getUwSourceType())) {
        // WfHelper.processManagerForSaveOrCancel4UW(policyId);
                // 2015-06-29 mark by Sunny
                // ProposalProcessService service = DSProxy
                // .newInstance(ProposalProcessService.class);
                // service.releaseTask(policyId);
        NEXT_PAGE = "nbWorkflowPage";
      }
      // add end
      // trans.commit();
    } catch (Exception e) {
      throw ExceptionFactory.parse(e);
    }
    return mapping.findForward(NEXT_PAGE);
  }

  @Resource(name = UwProcessService.BEAN_DEFAULT)
  private UwProcessService uwProcessDS;

  public void setUwProcessDS(UwProcessService uwProcessDS) {
    this.uwProcessDS = uwProcessDS;
  }

  @Resource(name = CSCI.BEAN_DEFAULT)
  private CSCI csCI;

  public CSCI getCsCI() {
    return csCI;
  }

  public void setCsCI(CSCI csCI) {
    this.csCI = csCI;
  }
}
