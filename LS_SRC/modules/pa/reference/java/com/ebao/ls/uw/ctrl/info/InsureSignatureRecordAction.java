package com.ebao.ls.uw.ctrl.info;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.ls.image.ci.ImageCI;
import com.ebao.ls.pa.pub.vo.InsuredVO;
import com.ebao.ls.pa.pub.vo.PolicyHolderVO;
import com.ebao.ls.uw.ds.UwInsureHistoryService;
import com.ebao.ls.uw.ds.vo.UwInsureClientVO;
import com.ebao.pub.framework.GenericAction;
import com.ebao.pub.util.StringUtils;

/**
 * <p>Title: 查詢投保簽名檔歷史資料ACTION</p>
 * <p>Description: Function Description Only vegetable</p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company: TGL Co., Ltd.</p>
 * <p>Create Time: Mar 27, 2016</p> 
 * @author 
 * <p>Update Time: Mar 71, 2016</p>
 * <p>Updater: Victor Chang</p>
 * <p>Update Comments: </p>
 */
public class InsureSignatureRecordAction extends GenericAction {

  @Resource(name = UwInsureHistoryService.BEAN_DEFAULT)
  private UwInsureHistoryService uwInsureHistoryService;

  @Resource(name = ImageCI.BEAN_DEFAULT)
  private ImageCI imageCI;
  
  @Override
  public ActionForward process(ActionMapping mapping, ActionForm form,
          HttpServletRequest request, HttpServletResponse response)
          throws Exception {

    UwHistoryInsureForm uwHistoryInsureForm = (UwHistoryInsureForm) form;
    String policyId = request.getParameter("policyId").toString();
    String forward = "display";
    
    if(!StringUtils.isNullOrEmpty(policyId)){
      uwHistoryInsureForm.setPolicyId(new Long(policyId));
      UwInsureClientVO vo = new UwInsureClientVO();

      
      List<String> certiCodeMap = new ArrayList<String>();
      for (InsuredVO o : uwInsureHistoryService.findInsuredById(policyId)) {
          if(o != null && 
                  !StringUtils.isNullOrEmpty(o.getCertiCode()) && 
                  !StringUtils.isNullOrEmpty(o.getCertiType()) ){
            boolean flag = true;
            for(UwInsureClientVO uwInsureClientVO : uwHistoryInsureForm.getInsureClientList()){
              if(uwInsureClientVO.getCertiCode().equals(o.getCertiCode())){
                flag = false;
              }
              if(!flag) break;
            }
            if(flag){
              UwInsureClientVO tmpVo = new UwInsureClientVO();
              tmpVo.setCertiCode(o.getCertiCode());
              tmpVo.setCertiType(o.getCertiType());
              tmpVo.setName(o.getName());
              uwHistoryInsureForm.getInsureClientList().add(tmpVo);
              
              certiCodeMap.add(o.getCertiCode());
            }
          }
        }
      PolicyHolderVO policyHolderVO = uwInsureHistoryService.findPolicyHolderById(policyId);
      if(policyHolderVO != null && 
              !StringUtils.isNullOrEmpty(policyHolderVO.getCertiCode()) && 
              !StringUtils.isNullOrEmpty(policyHolderVO.getCertiType()) ){
        vo.setCertiCode(policyHolderVO.getCertiCode());
        vo.setCertiType(policyHolderVO.getCertiType());
        vo.setName(policyHolderVO.getName());
        
        if(certiCodeMap.contains(policyHolderVO.getCertiCode()) == false) {
        	uwHistoryInsureForm.getInsureClientList().add(vo);	
        }
      }
      
      if(uwHistoryInsureForm.getInsureClientList().size() > 0){
        uwInsureHistoryService.findSignatureHistoryByClient(uwHistoryInsureForm.getInsureClientList());
      }
    }
    return mapping.findForward(forward);
  }

}
