package com.ebao.ls.callout.batch.task;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.ApplicationContext;

import com.ebao.foundation.common.context.AppUserContext;
import com.ebao.foundation.module.util.i18n.CodeTable;
import com.ebao.ls.callout.bs.CalloutTransService;
import com.ebao.ls.cs.commonflow.data.bo.AlterationItem;
import com.ebao.ls.cs.commonflow.ds.ApplicationService;
import com.ebao.ls.cs.util.POSUtils;
import com.ebao.ls.pub.CodeCst;
import com.ebao.ls.pub.cst.CalloutTargetType;
import com.ebao.ls.ws.vo.cmn.cmn170.roleInfo.CalloutTargetInfo;
import com.ebao.ls.ws.vo.cmn.cmn170.roleInfo.HolderInfo;
import com.ebao.ls.ws.vo.cmn.cmn170.roleInfo.InsuredInfoBasic;
import com.ebao.ls.ws.vo.cmn.cmn170.roleInfo.InsuredInfoPartlyExtend;
import com.ebao.ls.ws.vo.cmn.cmn170.roleInfo.InsuredInfoTotallyExtend;
import com.ebao.ls.ws.vo.cmn.cmn170.roleInfo.LegalRepInfo;
import com.ebao.ls.ws.vo.cmn.cmn170.type.BatchType1;
import com.ebao.ls.ws.vo.cmn.cmn170.type.BatchType2;
import com.ebao.ls.ws.vo.cmn.cmn170.type.BatchType3;
import com.ebao.ls.ws.vo.cmn.cmn170.type.BatchType4;
import com.ebao.ls.ws.vo.cmn.cmn170.type.BatchType5;
import com.ebao.ls.ws.vo.cmn.cmn170.type.ChangeItem;
import com.ebao.pub.util.json.DateUtils;

public class CalloutUtils {
	
	public static HolderInfo mapToHolder(Map<String, Object> map) {
		HolderInfo holder = new HolderInfo();
        holder.setName(MapUtils.getString(map, "PH_NAME", "").trim());
        holder.setCertiCode(MapUtils.getString(map, "PH_CERTI_CODE", "").trim());
        holder.setGender(MapUtils.getString(map, "PH_GENDER", "").trim());
        holder.setContactNum1(MapUtils.getString(map, "PH_OFFICE_TEL", "").trim());
        holder.setContactNum2(MapUtils.getString(map, "PH_HOME_TEL", "").trim());
        holder.setContactNum3(MapUtils.getString(map, "PH_PHONE1", "").trim());
        holder.setContactNum4(MapUtils.getString(map, "PH_PHONE2", "").trim());
        java.util.Date birthDate = (java.util.Date) map.get("ph_birthday");
        if(birthDate != null){
            holder.setBirthday(DateUtils.convertToMinguoDate(birthDate));
        }
        holder.setHomeAddr(MapUtils.getString(map, "PH_HOME_ADDR", "").trim());
		return holder;
	}

	public static CalloutTargetInfo mapToCalloutTarge(Map<String, Object> map , boolean islegalResp) {
		CalloutTargetInfo calloutTarget = new CalloutTargetInfo();
//		要保人之法定代理人1有值→顯示「要保人之法定代理人」
//		要保人之法定代理人1無值→顯示「要保人」
		String lrCertiCode = MapUtils.getString(map, "LR_CERTI_CODE", "");
		String lrName = MapUtils.getString(map, "LR_NAME", "");
		String ctBirthdayStr = "";
		
		if (StringUtils.isNotEmpty(lrCertiCode) && StringUtils.isNotEmpty(lrName) && islegalResp){
			calloutTarget.setName(MapUtils.getString(map, "LR_NAME", ""));
			calloutTarget.setRomanName(MapUtils.getString(map, "LR_ROMAN_NAME", ""));
			calloutTarget.setCertiCode(MapUtils.getString(map, "LR_CERTI_CODE", ""));
	        calloutTarget.setGender(MapUtils.getString(map, "LR_GENDER", ""));
	        calloutTarget.setContactNum1(MapUtils.getString(map, "LR_OFFICE_TEL", ""));
	        calloutTarget.setContactNum2(MapUtils.getString(map, "LR_HOME_TEL", ""));
	        calloutTarget.setContactNum3(MapUtils.getString(map, "LR_PHONE1", ""));
	        calloutTarget.setContactNum4(MapUtils.getString(map, "LR_PHONE2", ""));
	        Date ctBirthday = (Date)map.get("LR_BIRTHDAY");
	        if(ctBirthday != null){
	        	ctBirthdayStr = DateUtils.convertToMinguoDate(ctBirthday);
	        }
		}else{
			//要保人
			calloutTarget.setName(MapUtils.getString(map, "PH_NAME", ""));
			calloutTarget.setRomanName(MapUtils.getString(map, "PH_ROMAN_NAME", ""));
	        calloutTarget.setCertiCode(MapUtils.getString(map, "PH_CERTI_CODE", ""));
	        calloutTarget.setGender(MapUtils.getString(map, "PH_GENDER", ""));
	        calloutTarget.setContactNum1(MapUtils.getString(map, "PH_OFFICE_TEL", ""));
	        calloutTarget.setContactNum2(MapUtils.getString(map, "PH_HOME_TEL", ""));
	        calloutTarget.setContactNum3(MapUtils.getString(map, "PH_PHONE1", ""));
	        calloutTarget.setContactNum4(MapUtils.getString(map, "PH_PHONE2", ""));
	        Date ctBirthday = (Date)map.get("PH_BIRTHDAY"); 
	        if(ctBirthday != null){
	        	ctBirthdayStr = DateUtils.convertToMinguoDate(ctBirthday);
	        }
		}
		calloutTarget.setBirthday(ctBirthdayStr);
        return calloutTarget;
	}
		
	public static LegalRepInfo mapToLegalRep(Map<String, Object> map) {
		LegalRepInfo legalRep = new LegalRepInfo();
		legalRep.setName(MapUtils.getString(map, "LR_NAME", ""));
		legalRep.setCertiCode(MapUtils.getString(map, "LR_CERTI_CODE", ""));
		legalRep.setRelWIthInsured(CodeTable.getCodeDesc("T_NB_PAYER_TO_PH", (MapUtils.getString(map, "REL_TO_INS", "")), "311"));
		legalRep.setRelWIthHolder(CodeTable.getCodeDesc("T_NB_PAYER_TO_PH", (MapUtils.getString(map, "REL_TO_PH", "")), "311"));
	    Date lrBirthday = (Date)map.get("LR_BIRTHDAY");
	    if(lrBirthday != null){
	       	legalRep.setBirthday(DateUtils.convertToMinguoDate(lrBirthday));
	    }
		
		return legalRep;
	}
	
	public static InsuredInfoBasic mapToInsuredBasic(Map<String, Object> map) {
		InsuredInfoBasic insured = new InsuredInfoBasic();
        insured.setName(MapUtils.getString(map, "I_NAME", "").trim());
        insured.setCertiCode(MapUtils.getString(map, "I_CERTI_CODE", "").trim());
        insured.setInsuredYear(MapUtils.getString(map, "I_AGE", "").trim());
		return insured;
	}

	public static InsuredInfoPartlyExtend mapToInsuredPartly(Map<String, Object> map) {
		InsuredInfoPartlyExtend insured = new InsuredInfoPartlyExtend();
		insured.setName(MapUtils.getString(map, "I_NAME", "").trim());
		insured.setCertiCode(MapUtils.getString(map, "I_CERTI_CODE", "").trim());
		insured.setContactNum1(MapUtils.getString(map, "I_OFFICE_TEL", "").trim());
		java.util.Date birthDate = (java.util.Date) map.get("I_BIRTH_DATE");
		if(birthDate != null){
		    insured.setBirthday(DateUtils.convertToMinguoDate(birthDate));
		}
		return insured;
	}
	

	public static InsuredInfoTotallyExtend mapToInsuredTotally(Map<String, Object> map) {
		InsuredInfoTotallyExtend insured = new InsuredInfoTotallyExtend();
		insured.setName(MapUtils.getString(map, "I_NAME", "").trim());
		insured.setCertiCode(MapUtils.getString(map, "I_CERTI_CODE", "").trim());
		insured.setContactNum1(MapUtils.getString(map, "I_OFFICE_TEL", "").trim());
		insured.setContactNum2(MapUtils.getString(map, "I_HOME_TEL", "").trim());
		insured.setContactNum3(MapUtils.getString(map, "I_PHONE1", "").trim());
		insured.setContactNum4(MapUtils.getString(map, "I_PHONE2", "").trim());
		java.util.Date birthDate = (java.util.Date) map.get("I_BIRTH_DATE");
        if(birthDate != null){
            insured.setBirthday(DateUtils.convertToMinguoDate(birthDate));
        }
		return insured;
	}
	
	
	public static BatchType1 mapToBatch1(Map<String, Object> map) {
		BatchType1 data = new BatchType1();
        data.setCalloutNum(MapUtils.getString(map, "CALLOUT_NUM", "").trim());
        data.setCalloutTarget(MapUtils.getString(map, "PH_NAME", "").trim());
        data.setPolicyCode(MapUtils.getString(map, "POLICY_CODE", "").trim());
        data.setHolderInfo(mapToHolder(map));
        data.setLiabilityStatus(MapUtils.getString(map, "STATE", "").trim());
        data.setApplyDate(MapUtils.getString(map, "APPLY_DATE", "").trim());
        data.setValidateDate(MapUtils.getString(map, "VALIDATE_DATE", "").trim());
        data.setIssueDate(MapUtils.getString(map, "ISSUE_DATE","").trim());
        data.setDispatchDate(MapUtils.getString(map, "DISPATCH_DATE", "").trim());
        data.setAcknowledgeDate(MapUtils.getString(map, "REPLY_DATE", "").trim());
        data.setMailToType(MapUtils.getString(map, "SEND_TYPE", "").trim());
        data.setInsuredInfo(mapToInsuredBasic(map));
        //data.setExchangeRate(getDecimal(map, "EXCHANGE_RATE", new BigDecimal(0)));
        data.setCurrency(MapUtils.getString(map, "MONEY_NAME", "").trim());
        //data.setEachPrem(getDecimal(map, "EACH_PREM", new BigDecimal(0)));
        //data.setCustomizedPrem(getDecimal(map, "CUSTOMIZED_PREM", new BigDecimal(0)));
        data.setBizParent(MapUtils.getString(map, "BIG_PRODCATE", "").trim());
        data.setBizCate(MapUtils.getString(map, "SMALL_PRODCATE", "").trim());
        data.setUnitAmount(String.valueOf(getDouble(map, "AMOUNT", new Double(0))));
        data.setInternalId(MapUtils.getString(map, "INTERNAL_ID","").trim());
        data.setChannel(MapUtils.getString(map, "CHANNEL_CODE","").trim());
        data.setAgentName1(MapUtils.getString(map, "AGENT_NAME_1","").trim());
        data.setAgentName2(MapUtils.getString(map, "AGENT_NAME_2","").trim());
        data.setAgentName3(MapUtils.getString(map, "AGENT_NAME_3","").trim());
        data.setEnsureIndi(MapUtils.getString(map, "ENSURE_REQUIRE","").trim());
        data.setChildFeeIndi(MapUtils.getString(map, "CHILD_FEE","").trim());
        data.setRetireIndi(MapUtils.getString(map, "RETIRE_REQUIRE","").trim());
        data.setHouseIndi(MapUtils.getString(map, "HOUSE","").trim());
        data.setAssetIndi(MapUtils.getString(map, "ASSET","").trim());
        data.setDocketNum(MapUtils.getString(map, "DOCKET_NUM", "").trim());
		return data;
	}
	
	
	
	public BatchType2 mapToBatch2(Map<String, Object> map) {
		BatchType2 data = new BatchType2();
        data.setCalloutNum(MapUtils.getString(map, "CALLOUT_NUM", "").trim());
        data.setCalloutTarget(MapUtils.getString(map, "PH_NAME", "").trim());
        data.setHolderInfo(mapToHolder(map));
        data.setInsuredInfo(mapToInsuredBasic(map));
        data.setPolicyCode(MapUtils.getString(map, "POLICY_CODE", "").trim());
        data.setLiabilityStatus(MapUtils.getString(map, "STATE", "").trim());
        data.setApplyDate(MapUtils.getString(map, "APPLY_DATE", "").trim());
        data.setValidateDate(MapUtils.getString(map, "VALIDATE_DATE", "").trim());
        data.setIssueDate(MapUtils.getString(map, "ISSUE_DATE","").trim());
        data.setDispatchDate(MapUtils.getString(map, "DISPATCH_DATE", "").trim());
        data.setAcknowledgeDate(MapUtils.getString(map, "REPLY_DATE", "").trim());
        data.setMailToType(MapUtils.getString(map, "SEND_TYPE", "").trim());
        //data.setExchangeRate(getDecimal(map, "EXCHANGE_RATE", new BigDecimal(0)));
        data.setCurrency(MapUtils.getString(map, "MONEY_NAME", "").trim());
        data.setRenewalType(MapUtils.getString(map, "CHARGE_NAME","").trim());
        //data.setEachPrem(getDecimal(map, "EACH_PREM", new BigDecimal(0)));
        //data.setForiegnPrem(getDecimal(map, "STD_PREM", new BigDecimal(0)));
        //data.setAnnualChargeTw(getDecimal(map, "STD_TW_PREM", new BigDecimal(0)));
        data.setBizParent(MapUtils.getString(map, "BIG_PRODCATE", "").trim());
        data.setBizCate(MapUtils.getString(map, "SMALL_PRODCATE", "").trim());
        data.setUnitAmount(String.valueOf(getDouble(map, "AMOUNT", new Double(0))));
        data.setInternalId(MapUtils.getString(map, "INTERNAL_ID","").trim());
        data.setChannel(MapUtils.getString(map, "CHANNEL_CODE","").trim());
        data.setAgentName1(MapUtils.getString(map, "AGENT_NAME_1","").trim());
        data.setAgentName2(MapUtils.getString(map, "AGENT_NAME_2","").trim());
        data.setAgentName3(MapUtils.getString(map, "AGENT_NAME_3","").trim());
        data.setEnsureIndi(MapUtils.getString(map, "ENSURE_REQUIRE","").trim());
        data.setChildFeeIndi(MapUtils.getString(map, "CHILD_FEE","").trim());
        data.setRetireIndi(MapUtils.getString(map, "RETIRE_REQUIRE","").trim());
        data.setHouseIndi(MapUtils.getString(map, "HOUSE","").trim());
        data.setAssetIndi(MapUtils.getString(map, "ASSET","").trim());
        data.setDocketNum(MapUtils.getString(map, "DOCKET_NUM", "").trim());
		return data;
	}
	
	
	public BatchType3 mapToBatch3(Map<String, Object> map) {
		BatchType3 data = new BatchType3();
        data.setCalloutNum(MapUtils.getString(map, "CALLOUT_NUM", "").trim());
        data.setCalloutTarget(MapUtils.getString(map, "PH_NAME", "").trim());
        data.setHolderInfo(mapToHolder(map));
        data.setInsuredInfo(mapToInsuredBasic(map));
        data.setPolicyCode(MapUtils.getString(map, "POLICY_CODE", "").trim());
        data.setLiabilityStatus(MapUtils.getString(map, "STATE", "").trim());
        data.setApplyDate(MapUtils.getString(map, "APPLY_DATE", "").trim());
        data.setValidateDate(MapUtils.getString(map, "VALIDATE_DATE", "").trim());
        data.setIssueDate(MapUtils.getString(map, "ISSUE_DATE","").trim());
        data.setDispatchDate(MapUtils.getString(map, "DISPATCH_DATE", "").trim());
        data.setAcknowledgeDate(MapUtils.getString(map, "REPLY_DATE", "").trim());
        data.setMailToType(MapUtils.getString(map, "SEND_TYPE", "").trim());
        //data.setExchangeRate(getDecimal(map, "EXCHANGE_RATE", new BigDecimal(0)));
        data.setCurrency(MapUtils.getString(map, "MONEY_NAME", "").trim());
        data.setRenewalType(MapUtils.getString(map, "CHARGE_NAME","").trim());
        //data.setEachPrem(getDecimal(map, "EACH_PREM", new BigDecimal(0)));
        //data.setCustomizedPrem(getDecimal(map, "CUSTOMIZED_PREM", new BigDecimal(0)));
        //data.setForiegnPrem(getDecimal(map, "STD_PREM", new BigDecimal(0)));
        //data.setAnnualChargeTw(getDecimal(map, "STD_TW_PREM", new BigDecimal(0)));
        //data.setAnnualCharge(getDecimal(map, "ANNUAL_PREM", new BigDecimal(0)));
        data.setBizParent(MapUtils.getString(map, "BIG_PRODCATE", "").trim());
        data.setBizCate(MapUtils.getString(map, "SMALL_PRODCATE", "").trim());
        data.setUnitAmount(String.valueOf(getDouble(map, "AMOUNT", new Double(0))));
        data.setInternalId(MapUtils.getString(map, "INTERNAL_ID","").trim());
        data.setChannel(MapUtils.getString(map, "CHANNEL_CODE","").trim());
        data.setAgentName1(MapUtils.getString(map, "AGENT_NAME_1","").trim());
        data.setAgentName2(MapUtils.getString(map, "AGENT_NAME_2","").trim());
        data.setAgentName3(MapUtils.getString(map, "AGENT_NAME_3","").trim());
        data.setEnsureIndi(MapUtils.getString(map, "ENSURE_REQUIRE","").trim());
        data.setChildFeeIndi(MapUtils.getString(map, "CHILD_FEE","").trim());
        data.setRetireIndi(MapUtils.getString(map, "RETIRE_REQUIRE","").trim());
        data.setHouseIndi(MapUtils.getString(map, "HOUSE","").trim());
        data.setAssetIndi(MapUtils.getString(map, "ASSET","").trim());
        data.setDocketNum(MapUtils.getString(map, "DOCKET_NUM", "").trim());
		return data;
	}
	
	
	public static BatchType4 mapToBatch4(Map<String, Object> map) {
        String langId = CodeCst.LANGUAGE__CHINESE_TRAD;
        ApplicationContext context = AppUserContext.getApplicationContext();
        ApplicationService applicationService = context.getBean( ApplicationService.BEAN_DEFAULT,ApplicationService.class);
        CalloutTransService calloutServ = context.getBean(CalloutTransService.BEAN_DEFAULT,CalloutTransService.class);
        
        BatchType4 data = new BatchType4();
        data.setCalloutNum(MapUtils.getString(map, "CALLOUT_NUM", "").trim());
        data.setCalloutTarget(MapUtils.getString(map, "PH_NAME","").trim());
        HolderInfo holderInfo = new HolderInfo();
        holderInfo.setCertiCode(MapUtils.getString(map, "PH_CERTI_CODE", "").trim());
        holderInfo.setGender(MapUtils.getString(map, "PH_GENDER", "").trim());
        java.util.Date birthday = (java.util.Date) map.get("ph_birthday");
        if (birthday != null)
            holderInfo.setBirthday(com.ebao.pub.util.json.DateUtils.convertToMinguoDate(birthday));
        holderInfo.setContactNum1(MapUtils.getString(map, "PH_OFFICE_TEL", "").trim());
        holderInfo.setContactNum2(MapUtils.getString(map, "PH_HOME_TEL", "").trim());
        holderInfo.setContactNum3(MapUtils.getString(map, "PH_PHONE1", "").trim());
        holderInfo.setContactNum4(MapUtils.getString(map, "PH_PHONE2", "").trim());
        data.setHolderInfo(holderInfo);
        InsuredInfoPartlyExtend insuredInfo = new InsuredInfoPartlyExtend();
        insuredInfo.setName(MapUtils.getString(map, "I_NAME", "").trim());
        insuredInfo.setCertiCode(MapUtils.getString(map, "I_CERTI_CODE", "").trim());
        insuredInfo.setContactNum1(MapUtils.getString(map, "I_OFFICE_TEL", "").trim());
        birthday = (java.util.Date) map.get("I_BIRTH_DATE");
        if (birthday != null)
            insuredInfo.setBirthday(DateUtils.convertTWDate(birthday));
        data.setInsuredInfo(insuredInfo);
        data.setPolicyCode(MapUtils.getString(map, "POLICY_CODE", "").trim());
        Integer liabilityState = MapUtils.getInteger(map, "LIABILITY_STATE");
        data.setLiabilityStatus(CodeTable.getCodeDesc("T_LIABILITY_STATUS",String.valueOf(liabilityState),langId));
        java.util.Date validateDate = (java.util.Date) map.get("VALIDATE_DATE");
        if (validateDate != null)
            data.setValidateDate(DateUtils.convertTWDate(validateDate));
        data.setHandleName(MapUtils.getString(map, "HANDLE_NAME", "").trim());
        java.util.Date finAppTime = (java.util.Date) map.get("FIN_APP_TIME");
        data.setFinAppTime(DateUtils.convertTWDate(finAppTime));
        String sendTypeId = MapUtils.getString(map, "SEND_TYPE","").trim();
        if(StringUtils.isNotEmpty(sendTypeId)){
            data.setSendTypeDesc(CodeTable.getCodeDesc("T_DELIVER_TYPE", sendTypeId, langId));
        }
        data.setSendPersonName(MapUtils.getString(map, "SEND_PERSON_NAME", "").trim());
        java.util.Date custAppTime = (java.util.Date) map.get("CUST_APP_TIME");
        data.setCustAppTime(DateUtils.convertTWDate(custAppTime));
        String endCauseId = MapUtils.getString(map, "END_CAUSE", "").trim();
        if(StringUtils.isNotEmpty(endCauseId)){
            data.setEndCauseDesc(CodeTable.getCodeDesc("T_END_CAUSE", endCauseId,langId));
        }
        Long changeId = MapUtils.getLong(map, "CHANGE_ID");

        java.util.List<com.ebao.ls.cs.commonflow.data.bo.AlterationItem> alterationItemList = applicationService.getAlterationItemByChangeId(changeId);
        List<ChangeItem> changeItemList = new java.util.LinkedList<ChangeItem>();
        for (AlterationItem item : alterationItemList) {
            ChangeItem changeItem = new ChangeItem();
            if (item.getValidateTime() != null)
                changeItem.setValidateTime(DateUtils.convertTWDate(item.getValidateTime()));
            if(item.getServiceId() != null){
                changeItem.setAlternationItemDesc(CodeTable.getCodeDesc("T_SERVICE", String.valueOf(item.getServiceId()), langId));
                changeItem.setDetailItemDesc(CodeTable.getCodeDesc("T_SERVICE", String.valueOf(item.getServiceId()), langId));
            }
            changeItemList.add(changeItem);
        }
        data.setChangeItem(changeItemList);
        data.setSeq(MapUtils.getInteger(map, "SEQ"));
        //String docketNum = calloutServ.getDocketNum(MapUtils.getLong(map, "POLICY_ID"), YesNo.YES_NO__NO,YesNo.YES_NO__NO);
        //data.setDocketNum(docketNum);
        return data;
	}
	
	
	public static BatchType5 mapToBatch5(Map<String, Object> map) {
        String langId = CodeCst.LANGUAGE__CHINESE_TRAD;
        ApplicationContext context = AppUserContext.getApplicationContext();
        ApplicationService applicationService = context.getBean( ApplicationService.BEAN_DEFAULT,ApplicationService.class);
        CalloutTransService calloutServ = context.getBean(CalloutTransService.BEAN_DEFAULT,CalloutTransService.class);
        boolean islegalResp = false ;
        
        BatchType5 data = new BatchType5();
        data.setCalloutNum(MapUtils.getString(map, "CALLOUT_NUM", "").trim());
        //PCR 374909 新客服系統上線 新增欄位 - 電訪對象資訊
        //客戶申請日-要保人生日≥18足歲→顯示「要保人」
        //客戶申請日-要保人生日<18足歲→顯示「要保人之法定代理人」  
		Date ctBirthday = (Date)map.get("ph_birthday");					//要保人生日
		Date custApplyDate =(Date)map.get("CUST_APP_TIME"); 			//客戶申請日
		
		// PCR492019 民法成年年齡由20歲降為18歲
        Map<String, Object> ageMap = POSUtils.getGrowUpAge(ctBirthday, custApplyDate);
		Boolean isGrowUp = MapUtils.getBoolean(ageMap, "isGrowUp");//(true: 成年, false: 未成年)
		
        if (!isGrowUp){
        	data.setCalloutTarget(CodeTable.getCodeDesc("T_CALLOUT_TARGET_TYPE", CalloutTargetType.LEGAL_REPRESENTATIVE));
        	islegalResp = true ;
        }else{
        	//預設為要保人
            data.setCalloutTarget(CodeTable.getCodeDesc("T_CALLOUT_TARGET_TYPE", CalloutTargetType.POLICY_HOLDER));
        }
        
        CalloutTargetInfo calloutTargetInfo = CalloutUtils.mapToCalloutTarge(map, islegalResp); 
        data.setCalloutTargetInfo(calloutTargetInfo);
        //PCR 374909 新客服系統上線 新增欄位 - 法定代理人資訊
        LegalRepInfo legalRepInfo = CalloutUtils.mapToLegalRep(map); 
        data.setLegalRepInfo(legalRepInfo);
        
        HolderInfo holderInfo = mapToHolder(map);
        data.setHolderInfo(holderInfo);
        InsuredInfoTotallyExtend insuredInfoTotallyExtend = mapToInsuredTotally(map);
        data.setInsuredInfo(insuredInfoTotallyExtend);
        data.setPolicyCode(MapUtils.getString(map, "POLICY_CODE", "").trim());
        
        data.setHandleName(MapUtils.getString(map, "HANDLE_NAME", "").trim());

        java.util.Date finAppTime = (java.util.Date) map.get("FIN_APP_TIME");
        data.setFinAppTime(DateUtils.convertToMinguoDate(finAppTime));
        String sendTypeId = MapUtils.getString(map, "SEND_TYPE","").trim();
        if(StringUtils.isNotEmpty(sendTypeId)){
            String sendTypeDesc = CodeTable.getCodeDesc("T_DELIVER_TYPE", sendTypeId, langId);
            data.setSendTypeDesc(StringUtils.defaultString(sendTypeDesc, ""));
        }
        data.setSendPersonName(MapUtils.getString(map, "SEND_PERSON_NAME", "").trim());
        java.util.Date custAppTime = (java.util.Date) map.get("CUST_APP_TIME");
        data.setCustAppTime(DateUtils.convertTWDate(custAppTime));
        Long changeId = MapUtils.getLong(map, "CHANGE_ID");
        
        java.util.List<com.ebao.ls.cs.commonflow.data.bo.AlterationItem> alterationItemList = applicationService.getAlterationItemByChangeId(changeId);
        List<ChangeItem> changeItemList = new java.util.LinkedList<ChangeItem>();
        for (AlterationItem item : alterationItemList) {
            ChangeItem changeItem = new ChangeItem();
            if (item.getValidateTime() != null)
                changeItem.setValidateTime(DateUtils.convertTWDate(item.getValidateTime()));
            changeItem.setAlternationItemDesc("");
            if(item.getServiceId() != null){
                String alternationItemDesc = CodeTable.getCodeDesc("T_SERVICE", String.valueOf(item.getServiceId()), langId);
                changeItem.setAlternationItemDesc(StringUtils.defaultString(alternationItemDesc, ""));
            }
            changeItemList.add(changeItem);
        }
        data.setDetailItemDesc(MapUtils.getString(map, "DETAIL_ITEM_DESC","").trim());
        data.setChangeItem(changeItemList);
        data.setSeq(MapUtils.getInteger(map, "SEQ"));
        //String docketNum = calloutServ.getDocketNum(MapUtils.getLong(map, "POLICY_ID"),  null, null);
        //data.setDocketNum(docketNum);
        
		return data;
	}
	
	
	private static BigDecimal getDecimal(Map<String, Object> map, String columnName, BigDecimal dValue){
		if(map.get(columnName) == null){
			return dValue;
		} else {
			return new BigDecimal(Double.parseDouble(map.get(columnName).toString()));
		}
	}
	
	
	private static Double getDouble(Map<String, Object> map, String columnName, Double dValue){
		if(map.get(columnName) == null){
			return dValue;
		} else {
			return Double.parseDouble(map.get(columnName).toString());
		}
	}
}
