package com.ebao.ls.callout.data.bo;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.ebao.foundation.module.db.hibernate.HibernateSession3;
import com.ebao.ls.callout.data.CalloutAskSwitchDao;
import com.ebao.ls.callout.vo.CalloutAskSwitchVO;
import com.ebao.ls.callout.vo.CalloutAskVO;
import com.ebao.ls.callout.vo.CalloutProdUnfavorFactorVO;
import com.ebao.ls.callout.vo.CalloutTypeVO;
import com.ebao.ls.callout.vo.CalloutUnfavorFactorVO;
import com.ebao.ls.cs.calloutonline.service.CalloutAskSwitchService;
import com.ebao.ls.cs.calloutonline.vo.CallOutOnlineQuesItemVo;
import com.ebao.ls.foundation.service.GenericServiceImpl;
import com.ebao.pub.framework.ExceptionFactory;
import com.ebao.pub.util.BeanUtils;
import com.ebao.pub.util.Log;

public class CalloutAskSwitchServiceImpl 
extends GenericServiceImpl<CalloutAskSwitchVO, CalloutAskSwitch, CalloutAskSwitchDao> implements CalloutAskSwitchService {

	@Override
	protected CalloutAskSwitchVO newEntityVO() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<CalloutAskSwitchVO> findByConditions(Criterion... criterions) {
		List<CalloutAskSwitchVO> resultList = new ArrayList<CalloutAskSwitchVO>();
		//pcr504906
		List<Order> orders = new ArrayList<Order>();
		orders.add(Order.asc("listId"));

		if (criterions != null) {
			try {
				//List<CalloutAskSwitch> boList = dao
				//		.findByCriteria(criterions);
				List<CalloutAskSwitch> boList = dao.findByCriteriaWithOrder(orders, criterions);
				if (CollectionUtils.isNotEmpty(boList)) {
					for (CalloutAskSwitch bo : boList) {
						CalloutAskSwitchVO vo = new CalloutAskSwitchVO();
						BeanUtils.copyProperties(vo, bo);
						resultList.add(vo);
					}
				}
			} catch (Exception e) {
				Log.error(this.getClass(), e);
				throw ExceptionFactory.parse(e);
			}
		}

		return resultList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CalloutAskVO> findCalloutAsk() {
		List<CalloutAskVO> resultList = new ArrayList<CalloutAskVO>();
		List<CalloutAsk> calloutAskList = new ArrayList<CalloutAsk>();
		try {
			Session s = com.ebao.foundation.module.db.hibernate.HibernateSession3.currentSession();

			StringBuffer sb = new StringBuffer();
			sb.append(" select C ");
			sb.append(" from CalloutAsk C ");

			Query q = s.createQuery(sb.toString());
			calloutAskList = q.list();

			if (CollectionUtils.isNotEmpty(calloutAskList)) {
				
				for (CalloutAsk bo : calloutAskList) {
					CalloutAskVO vo = new CalloutAskVO();
					BeanUtils.copyProperties(vo, bo);
					resultList.add(vo);
				}
			}

		} catch (HibernateException e) {
			throw ExceptionFactory.parse(e);
		}
		return resultList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CalloutTypeVO> findCalloutType() {
		List<CalloutTypeVO> resultList = new ArrayList<CalloutTypeVO>();
		List<CalloutType> calloutTypeList = new ArrayList<CalloutType>();
		try {
			Session s = com.ebao.foundation.module.db.hibernate.HibernateSession3.currentSession();

			StringBuffer sb = new StringBuffer();
			sb.append(" select C ");
			sb.append(" from CalloutType C ");

			Query q = s.createQuery(sb.toString());
			calloutTypeList = q.list();

			if (CollectionUtils.isNotEmpty(calloutTypeList)) {
				
				for (CalloutType bo : calloutTypeList) {
					CalloutTypeVO vo = new CalloutTypeVO();
					BeanUtils.copyProperties(vo, bo);
					resultList.add(vo);
				}
			}

		} catch (HibernateException e) {
			throw ExceptionFactory.parse(e);
		}
		return resultList;
	}
	

	
	public List<CallOutOnlineQuesItemVo> findCallOutOnlineQuesItem(){
		List<CalloutAskSwitchVO> resultList = findByConditions();
		List<CalloutAskVO> askResultList = findCalloutAsk();
		List<CalloutTypeVO> typeResultList = findCalloutType();
		List<CallOutOnlineQuesItemVo> itemResultList = new ArrayList<CallOutOnlineQuesItemVo>();
		CallOutOnlineQuesItemVo itemVo = null; 
		for(CalloutAskSwitchVO casVo:resultList){
			for(CalloutTypeVO ctVo:typeResultList){
				if(casVo.getTypeCode().equals(ctVo.getCode())){
					itemVo = new CallOutOnlineQuesItemVo();
					itemVo.setTypeCode(ctVo.getCode());
					itemVo.setTypeCodeValue(ctVo.getCodeValue());
					for(CalloutAskVO caVo:askResultList){
						if(casVo.getAskCode().equals(caVo.getCode())){
							itemVo.setAskCode(caVo.getCode());
							itemVo.setAskCodeValue(caVo.getCodeValue());
							itemVo.setSubAskCode(casVo.getSubAskCode());
							itemVo.setIsCheck(casVo.getIsCheck());
							itemResultList.add(itemVo);
						}
					}
				}
			}
		}
		return itemResultList;
	}

	@Override
	public List<CalloutUnfavorFactorVO> findCalloutUnfavorFactor() {
		List<CalloutUnfavorFactorVO> resultList = new ArrayList<CalloutUnfavorFactorVO>();
		try {
			Session s = HibernateSession3.currentSession();
			Criteria criteria = s.createCriteria(CalloutUnfavorFactor.class);
			List<CalloutUnfavorFactor> list = criteria.list();

			for (CalloutUnfavorFactor bo : list) {
				CalloutUnfavorFactorVO vo = new CalloutUnfavorFactorVO();
				BeanUtils.copyProperties(vo, bo);
				resultList.add(vo);
			}

		} catch (HibernateException e) {
			throw ExceptionFactory.parse(e);
		}
		return resultList;
	}

	@Override
	public List<CalloutProdUnfavorFactorVO> findCalloutProdUnfavorFactorByInternalId(String internalId) {
		List<CalloutProdUnfavorFactorVO> resultList = new ArrayList<CalloutProdUnfavorFactorVO>();
		try {
			if (internalId == null) {
				throw new NullPointerException();
			}
			Session s = HibernateSession3.currentSession();
			Criteria criteria = s.createCriteria(CalloutProdUnfavorFactor.class);
			criteria.add(Restrictions.eq("internalId", internalId));
			List<CalloutProdUnfavorFactor> list = criteria.list();

			for (CalloutProdUnfavorFactor bo : list) {
				CalloutProdUnfavorFactorVO vo = new CalloutProdUnfavorFactorVO();
				BeanUtils.copyProperties(vo, bo);
				resultList.add(vo);
			}

		} catch (HibernateException e) {
			throw ExceptionFactory.parse(e);
		}
		return resultList;
	}

}
