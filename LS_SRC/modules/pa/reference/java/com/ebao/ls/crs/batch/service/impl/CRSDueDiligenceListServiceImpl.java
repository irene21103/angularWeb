package com.ebao.ls.crs.batch.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.ebao.ls.chl.service.AgentService;
import com.ebao.ls.chl.service.ChannelOrgService;
import com.ebao.ls.chl.vo.AgentInfo;
import com.ebao.ls.chl.vo.ChannelOrgVO;
import com.ebao.ls.chl.vo.ScAgentDataVO;
import com.ebao.ls.chl.vo.ScServiceAgentDataVO;
import com.ebao.ls.chl.vo.ScServiceRepresentDataVO;
import com.ebao.ls.crs.batch.service.CRSDueDiligenceListService;
import com.ebao.ls.crs.batch.vo.CRSTempVO;
import com.ebao.ls.crs.ci.CRSPartyCI;
import com.ebao.ls.crs.ci.CRSPartyReportVO;
import com.ebao.ls.crs.constant.CRSCodeCst;
import com.ebao.ls.crs.constant.CRSCodeType;
import com.ebao.ls.crs.data.batch.CRSDueDiligenceListDAO;
import com.ebao.ls.crs.data.bo.CRSDueDiligenceList;
import com.ebao.ls.crs.data.bo.CRSPartyLog;
import com.ebao.ls.crs.data.identity.CRSPartyLogDAO;
import com.ebao.ls.crs.identity.CRSPartyLogService;
import com.ebao.ls.crs.vo.CRSDueDiligenceListVO;
import com.ebao.ls.crs.vo.CRSPartyLogVO;
import com.ebao.ls.fatca.batch.data.FatcaBenefitSurveyListDAO;
import com.ebao.ls.fatca.constant.FatcaRole;
import com.ebao.ls.fatca.constant.FatcaSurveyType;
import com.ebao.ls.fatca.data.bo.FatcaDueDiligenceList;
import com.ebao.ls.fatca.util.FatcaDBeanQuery;
import com.ebao.ls.fatca.util.FatcaJDBC;
import com.ebao.ls.fatca.vo.FatcaDueDiligenceListVO;
import com.ebao.ls.foundation.service.GenericServiceImpl;
import com.ebao.ls.notification.event.cs.FmtPos0350Event;
import com.ebao.ls.notification.event.cs.FmtPos0510Event;
import com.ebao.ls.pa.pty.service.CertiCodeCI;
import com.ebao.ls.pty.ds.CustomerService;
import com.ebao.ls.pty.vo.CompanyCustomerVO;
import com.ebao.ls.pty.vo.CustomerVO;
import com.ebao.ls.pub.CodeCst;
import com.ebao.ls.pub.cst.YesNo;
import com.ebao.ls.sc.vo.AgentChlVO;
import com.ebao.pub.batch.type.LogLevel;
import com.ebao.pub.batch.util.BatchLogUtils;
import com.ebao.pub.event.ApplicationEventVO;
import com.ebao.pub.event.EventService;

public class CRSDueDiligenceListServiceImpl extends GenericServiceImpl<CRSDueDiligenceListVO, CRSDueDiligenceList, CRSDueDiligenceListDAO> implements CRSDueDiligenceListService{
	
    @Resource(name = FatcaBenefitSurveyListDAO.BEAN_DEFAULT)
    private FatcaBenefitSurveyListDAO fatcaBenefitSurveyListDAO;
	
	@Resource(name = CRSPartyLogService.BEAN_DEFAULT)
	private CRSPartyLogService crsPartyLogService;
	
	@Resource(name = CRSPartyCI.BEAN_DEFAULT)
	private CRSPartyCI crsPartyCI;
    
    @Resource(name=AgentService.BEAN_DEFAULT)
    private AgentService agentService;
    
    @Resource(name=ChannelOrgService.BEAN_DEFAULT)
    private ChannelOrgService channelServ;
    
    @Resource(name=CustomerService.BEAN_DEFAULT)
    private  CustomerService customerService;
    
    @Resource(name = CRSPartyLogDAO.BEAN_DEFAULT)
	private CRSPartyLogDAO crsPartyLogDAO;
    
    @Resource(name = EventService.BEAN_DEFAULT)
    private EventService eventService;
    
    private static Logger logger = Logger.getLogger(CrsBenefitSurveyListServiceImpl.class);
    
    // 孤兒保單派發至 PCD = HO
    private final Long DELIVER_TO_HO = 4L;
    
    // 虛擬業務員=孤兒保單
    private final Long ORPHAN = 2L;

	@Override
	protected CRSDueDiligenceListVO newEntityVO() {
		return new CRSDueDiligenceListVO();
	}
	
    private void log(String message) {
    	logger.info("[SYSOUT][CrsBenefitSurveyListServiceImpl]" + message);
    	BatchLogUtils.addLog(LogLevel.INFO, null, message);
    }
    
    private void logWithId(String id, String message) {
    	if (StringUtils.isNotBlank(id)) {
    		message = "[" + id + "] " + message;
    	}
    	log(message);
    }
    
    /**
	 * TODO 將Fatca盡職調查清單轉換為CRSPartyLog清單
	 * @param crsVoList
	 * @param processDate
	 * @return
	 */
    @Override
    public CRSPartyLogVO getCRSPartyLogVO(CRSTempVO crsTempVO, Date processDate) throws Exception {
    	
    	CRSPartyLogVO crsPartyLogVO = new CRSPartyLogVO();
    	BigDecimal highValue = new BigDecimal("1000000");
    	ArrayList<String> surveyTypeList = new ArrayList<String>();
    	surveyTypeList.add(FatcaSurveyType.IS_HIGH_ASSET); //高資產
    	surveyTypeList.add(FatcaSurveyType.NEED_TO_SURVEY);//法人需調查
    	surveyTypeList.add(FatcaSurveyType.IS_LOW_ASSET);  //低資產
   
    	CRSDueDiligenceListVO crsVO = crsTempVO.getCrsDueDiligenceListVO();

    	BigDecimal policyValue = crsVO.getPolicyValue();
    	String sixIndi = crsVO.getSixIndi();
		String amlIndi = crsVO.getAmlIndi();
		String surveyType = crsVO.getTargetSurveyType();
		// CHANGE_ID
		crsPartyLogVO.setChangeId(crsTempVO.getChangeId());
		// LAST_CMT_FLG (預設值給N，不處理)
		// CERTI_CODE (帳戶持有人ID)
		crsPartyLogVO.setCertiCode(crsVO.getCertiCode());
		// CERTI_TYPE
		crsPartyLogVO.setCertiType(crsVO.getPartyType().equals(CodeCst.PARTY_TYPE__INDIV_CUSTOMER)? 
				Integer.valueOf(CertiCodeCI.CERTI_TYPE_TW_PID) : Integer.valueOf(CertiCodeCI.CERTI_TYPE_COMPANY_NO));
		String partyType =crsVO.getPartyType();
		crsPartyLogVO.setCrsCode(CodeCst.PARTY_TYPE__INDIV_CUSTOMER.equals(partyType)? 
				CRSCodeCst.CRS_IDENTITY_TYPE__MAN : CRSCodeCst.CRS_IDENTITY_TYPE__COMPANY);
		// NAME (帳戶持有人姓名)
		crsPartyLogVO.setName(crsVO.getName());
		// POLICY_CODE (帳戶持有人的保單號碼)
		crsPartyLogVO.setPolicyCode(crsVO.getPolicyCode());
		// POLICY_TYPE (保單類型(個險:0,團險:1))
		crsPartyLogVO.setPolicyType(CRSCodeCst.POLICY_TYPE__NORMAL);
		// SYS_CODE (固定給保全)
		crsPartyLogVO.setSysCode(CRSCodeCst.SYS_CODE__POS);
		// CRS_TYPE (自動建檔時CRS身份類別不給值)
		// COUNTRY
		// DECLARATION_DATE
		// LEGAL_CERTI_CODE
		// LEGAL_NAME
		// AGENT_ANNOUNCE
		// DOC_SIGN_STATUS (01 已通知未簽署)
		crsPartyLogVO.setDocSignStatus(CRSCodeCst.CRS_DOC_SIGN_STATUS__NOTIFY);
		// DOC_SIGN_DATE (調查名單產生日期 = 批次業務日)
		crsPartyLogVO.setDocSignDate(processDate);
		// BUILD_TYPE 預設建檔中
		// crsPartyLogVO.setBuildType(CRSCodeCst.BUILD_TYPE__COMPLETE);
		// INPUT_SOURCE (BAT)
		crsPartyLogVO.setInputSource(CRSCodeCst.INPUT_SOURCE__BAT);		
		// 高低資產符合六大指標或AML及法人:已通知未簽署;跟催後判定:未回覆 - 待人工判定
		// IR410070 修改CRS法人不分調查來源批次判定皆為未回覆 - 待人工判定
		if(sixIndi != null || amlIndi != null || CodeCst.PARTY_TYPE__ORGAN_CUSTOMER.equals(partyType)){
			 if(!(surveyTypeList.contains(surveyType))){
				 crsPartyLogVO.setCrsType(CRSCodeType.CODE_TYPE_UNDEFINED__E);
				 crsPartyLogVO.setDocSignStatus(CRSCodeCst.CRS_DOC_SIGN_STATUS__NONE_REPLY);
				 crsPartyLogVO.setBuildType(CRSCodeCst.BUILD_TYPE__STAMPED);
			 }
			 else{
			  	 crsPartyLogVO.setCrsType(CRSCodeType.CODE_TYPE_UNDEFINED__E);
				 crsPartyLogVO.setBuildType(CRSCodeCst.BUILD_TYPE__STAMPED);
			 }

			}
		// 不符合六大指標或AML:批次判定CRSTYPE個人=CRS100
		else { 
			   crsPartyLogVO.setCrsType(CRSCodeType.CODE_TYPE_PERSON__A); 
			   crsPartyLogVO.setDocSignStatus(CRSCodeCst.CRS_DOC_SIGN_STATUS__BATCH_DETERMINE);
		}
		// 六大指標及AML檢核日 (法人只檢核AML)
		if(!FatcaSurveyType.NEED_TO_SURVEY.equals(surveyType)){
			crsPartyLogVO.setSixIndiCheckDate(processDate);
		}
		if ((policyValue != null && policyValue.compareTo(highValue) > 0) ||
			FatcaSurveyType.NEED_TO_SURVEY.equals(surveyType)){
			crsPartyLogVO.setAmlCheckDate(processDate);
		}

		// CRSID
		if (crsTempVO.getCrsId() != null) {
			crsPartyLogVO.setCrsId(crsTempVO.getCrsId());
		}
		
		return crsPartyLogVO;
	}
    
    /**
	 * CRS LOG建檔與主檔提交
	 * @param voList
	 * @param processDate
	 * @throws Exception
	 */
    @Override
	public void createCrsRecord(String id, CRSTempVO crsTempVO, Long userId, Date processDate) throws Exception {
		CRSPartyLogVO vo = getCRSPartyLogVO(crsTempVO, processDate);
		logWithId(id, "changeId = " + vo.getChangeId());
		String certiCode = vo.getCertiCode();
		if (StringUtils.isNotBlank(certiCode)) {
			logWithId(id, "certiCode = " + certiCode);
		} else {
			logWithId(id, "警告，certiCode為空值");
		}
		// 建立CRSPartyLog
		crsPartyLogService.saveOrUpdate(userId, processDate, vo, vo.getChangeId(), vo.getInputSource());
		CRSPartyLogVO logVO = crsPartyLogService.
				findByChangeIdAndCertiCode(vo.getChangeId(), certiCode);
		logWithId(id, "logId = " + logVO.getLogId());
		// 提交CRS主檔
		CRSPartyReportVO crsPartyReportVO = crsPartyCI.submitByLogId(logVO.getLogId());
		if (crsPartyReportVO != null) {
			logWithId(id, "crsId = " + crsPartyReportVO.getParty().getCrsId());
		} else {
			logWithId(id, "提交CRS主檔有異常");
		}
		
		// 產生CRS調查聲明書數據
		if(vo.getCrsType() != null && vo.getBuildType() != null && vo.getDocSignStatus() != null) {
		   if(vo.getCrsType().equals(CRSCodeType.CODE_TYPE_UNDEFINED__E) &&
			  vo.getBuildType().equals(CRSCodeCst.BUILD_TYPE__STAMPED) &&
			  vo.getDocSignStatus().equals(CRSCodeCst.CRS_DOC_SIGN_STATUS__NOTIFY)){
			  FmtPos0510Event event = new FmtPos0510Event(ApplicationEventVO.newInstance(crsTempVO.getCrsDueDiligenceListVO().getPolicyId()));
			  event.setDeclarant(crsTempVO.getCrsDueDiligenceListVO().getName());
			  event.setPrintDate(processDate);
			  event.setPartyId(crsTempVO.getCrsDueDiligenceListVO().getPartyId());
			  eventService.publishEvent(event);	
			}
			
		}

	}
    
    /**
	 * 從CRSTempVO清單中取得FatcaDueDiligenceListVO清單
	 * @param crsTempVOList
	 * @return
	 */
    @Override
	public List<FatcaDueDiligenceListVO> getFatcaList(List<CRSTempVO> crsTempVOList) throws Exception {
		List<FatcaDueDiligenceListVO> fatcaList = new ArrayList<FatcaDueDiligenceListVO>();
		for (CRSTempVO crsTempVO : crsTempVOList) {
			fatcaList.add(crsTempVO.getFatcaDueDiligenceListVO());
		}
		return fatcaList;
	}

    /**
     * 派發孤兒保單
     */
    @Override
    public void deliverOrphan(FatcaDueDiligenceListVO vo){
        // 孤兒保單派發至 PCD = HO
    	if(new Long(ORPHAN).equals(vo.getAgentBizCate())){
            vo.setChannelType(DELIVER_TO_HO);
        }
    }
    
    /**
     * 更新業務員
     */
    @Override
    public void updateServiceAgent(FatcaDueDiligenceListVO vo) throws Exception {
        Long policyId = vo.getPolicyId();
        ScAgentDataVO agentDataVO = agentService.getAgentDataByPolicyId(policyId);
        List<ScServiceAgentDataVO> serviceAgentList = agentDataVO.getServiceAgentList();
        List<ScServiceRepresentDataVO> representAgentList = agentDataVO.getServiceRepresentList();
        if(CollectionUtils.isEmpty(serviceAgentList)){
            BatchLogUtils.addLog(LogLevel.WARN, null, String.format("[BR-CMN-FAT-033][同步FATCA盡職調查清單的服務員與綜合查詢一致] [查無服務員]policy_code = %s, certi_code =%s, policy_id = %d, party_id =%d" , vo.getPolicyCode(), vo.getTargetCertiCode(), vo.getPolicyId(), vo.getTargetId()));
            return;
        }
        ScServiceAgentDataVO serviceAgentVO = serviceAgentList.get(0);
        AgentChlVO agentchlVO = agentService.findAgentByAgentId(serviceAgentVO.getAgentId());
        if(agentchlVO == null)
            return;
        
        BatchLogUtils.addLog(LogLevel.WARN, null, "-1-serviceAgentList.size()="+serviceAgentList.size());
        Boolean IsAllDummy = Boolean.TRUE;
        for(ScServiceAgentDataVO svo:serviceAgentList){
        	BatchLogUtils.addLog(LogLevel.WARN, null, "0-findAgentByAgentId.getAgentId="+svo.getAgentId());
        	AgentChlVO agentChlVO =agentService.findAgentByAgentId(svo.getAgentId());
        	if(agentChlVO!=null){
        		BatchLogUtils.addLog(LogLevel.WARN, null, "agentChlVO!=nullagentChlVO.getAgentId()="+agentChlVO.getAgentId()+";agentChlVO.getIsDummy()="+agentChlVO.getIsDummy()+";");
        		if(YesNo.YES_NO__NO.equals(agentChlVO.getIsDummy())){
        			IsAllDummy = Boolean.FALSE;;
        		}
        	}
        }
        BatchLogUtils.addLog(LogLevel.WARN, null, "1-IsAllDummy="+IsAllDummy);
        
        vo.setServiceAgent(serviceAgentVO.getAgentId());
        if(agentchlVO.getBusinessCate() != null)
            vo.setAgentBizCate(Long.valueOf(agentchlVO.getBusinessCate()));
        vo.setAgentName(serviceAgentVO.getAgentName());
        vo.setAgentRegisterCode(serviceAgentVO.getAgentRegisterCode());
        
        Boolean toPCD = Boolean.FALSE;
        if(agentchlVO.getChannelOrgId() != null){
            BatchLogUtils.addLog(LogLevel.WARN, null, "2-serviceAgentVO.getAgentChannelCode()="+serviceAgentVO.getAgentChannelCode());
        	ChannelOrgVO channelVO = channelServ.findByChannelCode(serviceAgentVO.getAgentChannelCode());
            if(channelVO != null){
            	vo.setChannelOrg(channelVO.getChannelId());
                vo.setChannelCode(serviceAgentVO.getAgentChannelCode());
                vo.setChannelName(serviceAgentVO.getAgentChannelName());
                vo.setChannelType(serviceAgentVO.getAgentChannelType());
                //PCR-519543 部門跟人員都要虛擬且排除BRBD
                BatchLogUtils.addLog(LogLevel.WARN, null, "3-channelVO.getChannelType()="+channelVO.getChannelType()+";channelVO.getIsDummy()="+channelVO.getIsDummy()+";");
                if(5l!=channelVO.getChannelType()&&6l!=channelVO.getChannelType()){
                	toPCD = YesNo.YES_NO__YES.equals(channelVO.getIsDummy()) && IsAllDummy;
                	vo.setAgentBizCate(YesNo.YES_NO__YES.equals(channelVO.getIsDummy())&& IsAllDummy ? 2L : 1L);
                }else{
                	//外部通路(BR/FID)-一律給服務單位處理，不歸入直營通路的虛擬單位PCD。
                	vo.setAgentBizCate(1L);
                }
            }
        }
        BatchLogUtils.addLog(LogLevel.WARN, null, "4-toPCD="+toPCD);
        //孤兒保單服務業務員
        if(toPCD && CollectionUtils.isNotEmpty(representAgentList)) {
			ScServiceRepresentDataVO representVO = representAgentList.get(0);
			vo.setServiceAgent(representVO.getAgentId());
			vo.setAgentName(representVO.getRepresentName());
			vo.setAgentRegisterCode(representVO.getRepresentRegisterCode());
			vo.setAgentBizCate(2L);
        }
    }

    @Override
    public void updateCrsServiceAgent(CRSDueDiligenceListVO vo) throws Exception {
        Long policyId = vo.getPolicyId();
        ScAgentDataVO agentDataVO = agentService.getAgentDataByPolicyId(policyId);
        List<ScServiceAgentDataVO> serviceAgentList = agentDataVO.getServiceAgentList();
        List<ScServiceRepresentDataVO> representAgentList = agentDataVO.getServiceRepresentList();
        if(CollectionUtils.isEmpty(serviceAgentList)){
            BatchLogUtils.addLog(LogLevel.WARN, null, String.format("[BR-CMN-FAT-033][同步FATCA盡職調查清單的服務員與綜合查詢一致] [查無服務員]policy_code = %s, certi_code =%s, policy_id = %d, party_id =%d" , vo.getPolicyCode(), vo.getCertiCode(), vo.getPolicyId(), vo.getPartyId()));
            return;
        }
        ScServiceAgentDataVO serviceAgentVO = serviceAgentList.get(0);
        AgentChlVO agentchlVO = agentService.findAgentByAgentId(serviceAgentVO.getAgentId());
        if(agentchlVO == null)
            return;
        
        BatchLogUtils.addLog(LogLevel.WARN, null, "-1-serviceAgentList.size()="+serviceAgentList.size());
        Boolean IsAllDummy = Boolean.TRUE;
        for(ScServiceAgentDataVO svo:serviceAgentList){
        	BatchLogUtils.addLog(LogLevel.WARN, null, "0-findAgentByAgentId.getAgentId="+svo.getAgentId());
        	AgentChlVO agentChlVO =agentService.findAgentByAgentId(svo.getAgentId());
        	if(agentChlVO!=null){
        		BatchLogUtils.addLog(LogLevel.WARN, null, "agentChlVO!=nullagentChlVO.getAgentId()="+agentChlVO.getAgentId()+";agentChlVO.getIsDummy()="+agentChlVO.getIsDummy()+";");
        		if(YesNo.YES_NO__NO.equals(agentChlVO.getIsDummy())){
        			IsAllDummy = Boolean.FALSE;;
        		}
        	}
        }
        BatchLogUtils.addLog(LogLevel.WARN, null, "1-IsAllDummy="+IsAllDummy);
        
        vo.setAgentId(serviceAgentVO.getAgentId());
//        if(agentchlVO.getBusinessCate() != null)
//            vo.setAgentBizCate(Long.valueOf(agentchlVO.getBusinessCate()));
        vo.setAgentName(serviceAgentVO.getAgentName());
        vo.setRegisterCode(serviceAgentVO.getAgentRegisterCode());
        
        Boolean toPCD = Boolean.FALSE;
        if(agentchlVO.getChannelOrgId() != null){
        	BatchLogUtils.addLog(LogLevel.WARN, null, "2-serviceAgentVO.getAgentChannelCode()="+serviceAgentVO.getAgentChannelCode());
        	ChannelOrgVO channelVO = channelServ.findByChannelCode(serviceAgentVO.getAgentChannelCode());
            if(channelVO != null){
            	vo.setChannelId(channelVO.getChannelId());
                vo.setChannelCode(serviceAgentVO.getAgentChannelCode());
                vo.setChannelName(serviceAgentVO.getAgentChannelName());
                vo.setChannelType(serviceAgentVO.getAgentChannelType());
                BatchLogUtils.addLog(LogLevel.WARN, null, "3-channelVO.getChannelType()="+channelVO.getChannelType()+";channelVO.getIsDummy()="+channelVO.getIsDummy()+";");
                //PCR-519543 部門跟人員都要虛擬且排除BRBD
                if(!"5".equals(channelVO.getChannelType())&&!"6".equals(channelVO.getChannelType())){
                	toPCD = YesNo.YES_NO__YES.equals(channelVO.getIsDummy()) && IsAllDummy;
                }
//                vo.setAgentBizCate(YesNo.YES_NO__YES.equals(channelVO.getIsDummy()) ? 2L : 1L);
            }
        }
        BatchLogUtils.addLog(LogLevel.WARN, null, "4-toPCD="+toPCD);
        //孤兒保單服務業務員
        if(toPCD && CollectionUtils.isNotEmpty(representAgentList)) {
			ScServiceRepresentDataVO representVO = representAgentList.get(0);
			vo.setAgentId(representVO.getAgentId());
			vo.setAgentName(representVO.getRepresentName());
			vo.setRegisterCode(representVO.getRepresentRegisterCode());
//			vo.setAgentBizCate(2L);
        }
    }
    
    /**
	 * 將資料庫查詢結果轉為CRSTempVO物件
	 * @param column
	 * @param surveyType
	 * @return
	 */
	private CRSTempVO parseMapData(java.util.Map<String, Object> resultMap, String surveyType) {
		
		CRSTempVO crsTempVO = new CRSTempVO();
		
		crsTempVO.setChangeId(MapUtils.getLong(resultMap, "change_id"));
		
		Long targetId = MapUtils.getLong(resultMap, "target_id");
		Long policyId = MapUtils.getLong(resultMap, "survey_id");

        FatcaDueDiligenceListVO fatcaVO = new FatcaDueDiligenceListVO();
        
        fatcaVO.setPolicyId(policyId);
        fatcaVO.setPolicyCode(StringUtils.trim(MapUtils.getString(resultMap, "policy_code", "")));
        fatcaVO.setPolicyStatus(MapUtils.getLong(resultMap, "policy_status"));
        fatcaVO.setTargetId(targetId);
        fatcaVO.setTargetName(StringUtils.trim(MapUtils.getString(resultMap, "target_name", "")));
        fatcaVO.setTargetCertiCode(StringUtils.trim(MapUtils.getString(resultMap, "target_certi_code", "")));
        fatcaVO.setTargetPartyType(MapUtils.getString(resultMap,"target_party_type"));
        fatcaVO.setTargetSurveyType(surveyType);
        fatcaVO.setServiceAgent(MapUtils.getLong(resultMap, "service_agent"));
        fatcaVO.setAgentRegisterCode(StringUtils.trim(MapUtils.getString(resultMap, "agent_register_code", "")));
        fatcaVO.setAgentName(StringUtils.trim(MapUtils.getString(resultMap, "agent_name", "")));
        Long agentBizCate = (Long) resultMap.get("agent_biz_cate");
        agentBizCate = agentBizCate != 0 ? agentBizCate : null;
        fatcaVO.setAgentBizCate(agentBizCate);
        Long channelOrg = (Long) resultMap.get("channel_org");
        channelOrg = channelOrg != 0 ? channelOrg : null;
        fatcaVO.setChannelOrg(channelOrg);
        fatcaVO.setChannelCode(StringUtils.trim(MapUtils.getString(resultMap, "channel_code", "")));
        fatcaVO.setChannelName(StringUtils.trim(MapUtils.getString(resultMap, "channel_name", "")));
        Long channelType = (Long)resultMap.get("channel_type");
        fatcaVO.setChannelType(channelType);
        fatcaVO.setRoleType(MapUtils.getString(resultMap, "role_type"));
        
        // 查詢T_FATCA_DUE_DILIGENCE_LIST檢查目前有無紀錄
        FatcaDueDiligenceList resultFatcaBO = fatcaBenefitSurveyListDAO.searchFatcaDueDiligenceList(targetId, policyId, surveyType);
        if (resultFatcaBO == null) {
        	// 新紀錄
        	crsTempVO.setNewRecord(true);
        	// 調查系統別為CRS
        	fatcaVO.setSurveySystemType(SURVEY_SYSTEM_TYPE_CRS);
        } else {
        	// 已有記錄
        	crsTempVO.setNewRecord(false);
        	fatcaVO.setListID(resultFatcaBO.getListID());
        	// 調查系統別為FATCA，表示是由FATCA批次建立資料，需更新SURVEY_SYSTEM_TYPE為CRS/FATCA，否則保持目前狀態(CRS or CRS/FATCA)
        	String surveySystemType = resultFatcaBO.getSurveySystemType();
        	if (StringUtils.equals(surveySystemType, SURVEY_SYSTEM_TYPE_FATCA)) {
        		fatcaVO.setSurveySystemType(SURVEY_SYSTEM_TYPE_ALL);
        	} else {
        		fatcaVO.setSurveySystemType(surveySystemType);
        	}
        }
        crsTempVO.setFatcaDueDiligenceListVO(fatcaVO);
        
        return crsTempVO;
    }
    
	/**
	 * 執行查詢，返回查詢結果清單
	 * @param sql
	 * @param processDate
	 * @param surveyType
	 * @return
	 * @throws Exception 
	 */
    private List<CRSTempVO> execute(final StringBuffer sql, final Date processDate, final String surveyType) throws Exception{
		
		List<CRSTempVO> voList = new java.util.LinkedList<CRSTempVO>();
        List<Map<String, Object>> resultMapList = FatcaJDBC.queryByMap(new FatcaDBeanQuery() {

            @Override
            public String sqlStatement() {
            	String sqlString = sql.toString();
            	log("SQL = " + sqlString);
            	return sqlString;
            }

            @Override
            public void createColumn() {
            	putColumn("change_id", Long.class);
            	
                putColumn("survey_id", Long.class);
                putColumn("policy_code", String.class);
                putColumn("policy_status", Long.class);
                putColumn("target_id", Long.class);
                
                putColumn("target_name", String.class);
                putColumn("target_certi_code", String.class);
                putColumn("target_party_type", Long.class);
                
                putColumn("service_agent", Long.class);
                putColumn("agent_register_code", String.class);
                putColumn("agent_name", String.class);
                putColumn("agent_biz_cate", Long.class);
                
                putColumn("channel_org", Long.class);
                putColumn("channel_code", String.class);
                putColumn("channel_name", String.class);
                putColumn("channel_type", Long.class);
                putColumn("role_type", String.class);
            }

            @Override
            public void createCriteria() {
                putCriteria(processDate, Date.class);
            }
        });

        //擷取查詢結果
        for (Map<String, Object> resultMap : resultMapList) {
        	CRSTempVO data = parseMapData(resultMap, surveyType);
            data.getFatcaDueDiligenceListVO().setTargetSurveyDate(processDate);
            
            // IR305650 當certi_code為空時，以[changeId+保單號碼+受益人姓名]為條件查詢CrsPartyLog建檔紀錄，避免重複建檔
            if (StringUtils.isBlank(data.getFatcaDueDiligenceListVO().getTargetCertiCode())) {
            	Long changeId = data.getChangeId();
            	String policyCode = data.getFatcaDueDiligenceListVO().getPolicyCode();
            	String targetName = data.getFatcaDueDiligenceListVO().getTargetName();
            	log("證號為空...changeId = " + changeId + ", policyCode = " + policyCode + ", targetName = " + targetName);
            	if (isCrsPartyLogExist(changeId, policyCode, targetName)) {
        			log("CrsPartyLog已有建檔紀錄，略過此筆");
        			continue;
        		}
            }
            voList.add(data);
        } 
        return voList;
    }
    
    /**
     * 是否在CRS_PARTY_LOG已經有紀錄
     */
    @Override
    public boolean isCrsPartyLogExist(Long changeId, String policyCode, String targetName)  throws Exception {
    	List<CRSPartyLog> crsPartyLogList = crsPartyLogDAO.findListByCertiCode(null, changeId);
    	for (CRSPartyLog crsPartyLog : crsPartyLogList) {
    		if (StringUtils.equals(policyCode, crsPartyLog.getPolicyCode())
    				&& StringUtils.equals(targetName, crsPartyLog.getName())) {
    			return true;
    		}
    	}
    	return false;
    }

}
