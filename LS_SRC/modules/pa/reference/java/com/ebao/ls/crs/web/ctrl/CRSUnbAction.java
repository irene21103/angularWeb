package com.ebao.ls.crs.web.ctrl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.foundation.core.logging.Log;
import com.ebao.pub.framework.GenericAction;
import com.ebao.pub.framework.GenericException;
import com.ebao.pub.framework.MultiWarning;
import com.ebao.pub.util.StringUtils;

public class CRSUnbAction extends GenericAction {

	private final Log logger = Log.getLogger(CRSUnbAction.class);

	@Override
	protected MultiWarning processWarning(
					ActionMapping mapping, ActionForm form,
					HttpServletRequest request, HttpServletResponse response)
					throws Exception {

		MultiWarning warning = new MultiWarning();

		return warning;
	}

	private enum Source {
		POS, /* 保全 */
		UNB, /* 新契約 */
		CLAIM /* 理賠 */
	}

	private enum Template {
		company, /* 自我證明法人 */
		person, /* 自我證明個人 */
		controlPerson /* 具控制權人 */
	}

	@Override
	public ActionForward process(ActionMapping mapp, ActionForm actfrom,
					HttpServletRequest request, HttpServletResponse resp)
					throws GenericException {

		//source(POS/UNB/CLM/CMN)
		//template(company/person/controlPerson) //版型(自我證明個人，自我證明法人，具控制權人)
		//policyCurId  		//保單cur_id
		//policyId			//保單id
		//controlPersonId	//控制權人id
		String template = request.getParameter("template");
		String source = request.getParameter("source");
		if (!StringUtils.isNullOrEmpty(source)) {
			switch (Enum.valueOf(Template.class, template)) {
				case company:
					return mapp.findForward("company");
				case person:
					return mapp.findForward("person");
				case controlPerson:
					return mapp.findForward("controlPerson");
			}
		}
		return mapp.findForward("display");
	}

}
