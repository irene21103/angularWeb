package com.ebao.ls.uw.ctrl.info;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;

import com.ebao.foundation.module.util.i18n.CodeTable;
import com.ebao.ls.cs.commonflow.ds.ApplicationService;
import com.ebao.ls.cs.commonflow.vo.ApplicationVO;
import com.ebao.ls.pa.nb.util.NBUtils;
import com.ebao.ls.pa.pub.TableCst;
import com.ebao.ls.pa.pub.bs.BeneficiaryService;
import com.ebao.ls.pa.pub.bs.InsuredService;
import com.ebao.ls.pa.pub.bs.PolicyService;
import com.ebao.ls.pa.pub.bs.coverage.CoverageService;
import com.ebao.ls.pa.pub.bs.coverage.impl.CoverageServiceImpl;
import com.ebao.ls.pa.pub.bs.impl.BeneficiaryServiceImpl;
import com.ebao.ls.pa.pub.bs.impl.InsuredServiceImpl;
import com.ebao.ls.pa.pub.vo.BeneficiaryVO;
import com.ebao.ls.pa.pub.vo.CoverageAgentVO;
import com.ebao.ls.pa.pub.vo.CoverageVO;
import com.ebao.ls.pa.pub.vo.InsuredVO;
import com.ebao.ls.pa.pub.vo.PolicyHolderVO;
import com.ebao.ls.pa.pub.vo.PolicyVO;
import com.ebao.ls.pub.cst.CsAppStatus;
import com.ebao.ls.pub.data.InsuredVOComparator;
import com.ebao.ls.qry.ds.policy.CSInfoVO;
import com.ebao.ls.qry.ds.policy.sp.PolicySP;
import com.ebao.pub.util.StringUtils;

import edu.emory.mathcs.backport.java.util.Collections;

/**
 * <p>Title: Module Information Andy GOD</p>
 * <p>Description: Function Description Only vegetable</p>
 * <p>Copyright: Copyright (c) 2015</p>
 * <p>Company: TGL Co., Ltd.</p>
 * <p>Create Time: Aug 6, 2015</p> 
 * @author 
 * <p>Update Time: Aug 6, 2015</p>
 * <p>Updater: BradDeng</p>
 * <p>Update Comments: </p>
 */

public class QualityRecordActionHelper {
  
    public static final String BEAN_DEFAULT = "uwQualityRecordActionHelper";

    @Resource(name = PolicyService.BEAN_DEFAULT)
    private PolicyService policyService;

    @Resource(name = InsuredService.BEAN_DEFAULT)
    private InsuredServiceImpl insuredServiceImpl;

    @Resource(name = BeneficiaryService.BEAN_DEFAULT)
    private BeneficiaryServiceImpl beneficiaryServiceImpl;
    
    @Resource(name = CoverageService.BEAN_DEFAULT)
    private CoverageServiceImpl coverageServiceImpl;
    
    @Resource(name = PolicySP.BEAN_DEFAULT)
    private PolicySP policySP;

	@Resource(name = ApplicationService.BEAN_DEFAULT)
	private ApplicationService applicationService;
	
    public String findPolicyHolderById(String policyId){
        PolicyVO policy = policyService.load(Long.valueOf(policyId));
        PolicyHolderVO policyHolder = policy.getPolicyHolder();
        return policyHolder.getCertiCode();
    }
    
    public void findPolicyHolderCertiCodeByPolicyId( Set<String> certiCodeSet, String policyId){
        PolicyVO policy = policyService.load(Long.valueOf(policyId));
        PolicyHolderVO policyHolder = policy.getPolicyHolder();
        certiCodeSet.add(policyHolder.getCertiCode());
        if( !StringUtils.isNullOrEmpty(policyHolder.getOldForeignerId())) {
        	  certiCodeSet.add(policyHolder.getOldForeignerId());
        }
    }
    
    public List<String> findInsuredById(String policyId){
        List<InsuredVO> queryList = insuredServiceImpl.findByPolicyId(Long.valueOf(policyId));
        List<String> certiCodeList = new ArrayList<String>();
        for(InsuredVO o : queryList){
            certiCodeList.add(o.getCertiCode());
        }
        return certiCodeList;
    }
    
    public void findInsuredCertiCodeByPolicyId(Set<String> certiCodeSet,String policyId){
        List<InsuredVO> queryList = insuredServiceImpl.findByPolicyId(Long.valueOf(policyId));
        for(InsuredVO o : queryList){
        	certiCodeSet.add(o.getCertiCode());
            if( !StringUtils.isNullOrEmpty( o.getOldForeignerId()) ) {
            	certiCodeSet.add(o.getOldForeignerId());
            }
        }
    }
    
    public List<String> findBeneById(String policyId){
        List<BeneficiaryVO> queryList = beneficiaryServiceImpl.findByPolicyId(Long.valueOf(policyId));
        List<String> certiCodeList = new ArrayList<String>();
        for(BeneficiaryVO o : queryList){
            certiCodeList.add(o.getCertiCode());
        }
        return certiCodeList;
    }
    
    public void findBeneCertiCodeByPolicyId(Set<String> certiCodeSet,String policyId){
        List<BeneficiaryVO> queryList = beneficiaryServiceImpl.findByPolicyId(Long.valueOf(policyId));
        for(BeneficiaryVO o : queryList){
        	certiCodeSet.add(o.getCertiCode());
        }
    }
        
    public List<String> findAgentById(String policyId){
        CoverageVO coverage = coverageServiceImpl.findMasterCoverageByPolicyId(Long.valueOf(policyId));
        List<CoverageAgentVO> queryList = coverage.getCoverageAgents();
        List<String> registerCodeList = new ArrayList<String>();
        for(CoverageAgentVO o : queryList){
            registerCodeList.add(o.getRegisterCode());
        }
        return registerCodeList;
    }
    
    public List<InsuredVO> findPartyIdOfInsuredByPolicyId(String policyId){
        List<InsuredVO> queryList = insuredServiceImpl.findByPolicyId(Long.valueOf(policyId));
        java.util.Collections.sort(queryList, new InsuredVOComparator());
        return queryList;
    }

    public List<Map<String, Object>> findInfoVOByPartyId(Long[] partyIdArray, String langId){
    	List<Long> policyIdList = insuredServiceImpl.findPolicyIdByPartyId(partyIdArray);
    	
    	//policyIdList 可能重覆
    	List<Long> distinctPolicyIdList = new ArrayList<Long>();
    	for(Long policyId : policyIdList){
    		if(distinctPolicyIdList.contains(policyId) == false){
    			distinctPolicyIdList.add(policyId);
    		}
    	}

    	List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();

    	for(Long policyId : distinctPolicyIdList){

    		//RTC 152301 by simon.huang 2017/05/06
    		//依policyId查詢保全受理檔，排除9-保全生效，10-退件，11-拒保，12-undo，-4-待保全輸入且受理文件尚未確認
    		Criterion con1 = Restrictions.eq("policyId", policyId);
    		Criterion con2 = Restrictions.not( 
    				Restrictions.in("csAppStatus", new Integer[]{
    						CsAppStatus.CS_APP_STATUS__IN_FORCE,
    						CsAppStatus.CS_APP_STATUS__REJECTED,
    						CsAppStatus.CS_APP_STATUS__CANCELLED,
    						CsAppStatus.CS_APP_STATUS__WAIT_REG_CONFIRM 
    				}));
    		List<ApplicationVO> csApplictionList = applicationService.findByCriteria(con1,con2);
    		Map<String,ApplicationVO> changeIdBindApplication = NBUtils.toMap(csApplictionList, "changeId");
    		//END RTC
    		
    		@SuppressWarnings("unchecked")
    		//保全異動記錄(包含受理，後端處理/批次等) 
    		List<CSInfoVO> csInfoList = policySP.qryCSInfoForNBUW(policyId); 

    		//組合資料回傳前端: 保單號碼 ／保全項目／作業狀態／作業人員／保全受理日期／保單生效日期 
    		for(Object vo : csInfoList){

    			CSInfoVO csInfoVO = (CSInfoVO)vo;

    			String changeId  = csInfoVO.getChangeId();
    			String serviceId = csInfoVO.getServiceId().toString();
    			String csAppStatus = StringUtils.nullToEmpty(csInfoVO.getCsAppStatus());
    			
    			ApplicationVO applicationVO = changeIdBindApplication.get(changeId);
    			
    			if(applicationVO != null){
    		
    				Map<String, Object> map = new HashMap<String, Object>();
    				Date validateDate = policyService.findValidateDateByPolicyId(policyId);
    				String policyCode = policyService.getPolicyCodeByPolicyId(policyId);
    				String serviceName = CodeTable.getCodeDesc(TableCst.T_SERVICE, 
    						serviceId, langId);
    				String csAppStatusDesc = CodeTable.getCodeDesc(TableCst.T_CS_APP_STATUS, 
    						csAppStatus, langId);

					Long operatorId = null;
					if (applicationVO.getLastOperatorId() != null) {
						operatorId = applicationVO.getLastOperatorId();
					} else if (applicationVO.getLastHandlerId() != null) {
						operatorId = applicationVO.getLastHandlerId();
					} else if (applicationVO.getCurrentOperatorId() != null) {
						operatorId = applicationVO.getCurrentOperatorId();
					}

					map.put("policyCode", policyCode);
					map.put("transactionName", serviceName);
					map.put("policyChgStatus", csAppStatusDesc);
					map.put("lastHandlerName", operatorId);
					map.put("changeId", csInfoVO.getChangeId());
					map.put("applyTime", applicationVO.getApplyTime());
					map.put("validateDate", validateDate);
					list.add(map);

    			}
    		}
    	}

        Collections.sort(list, new Comparator<Map>() {
            @Override
            public int compare(Map m1, Map m2) {
                return new CompareToBuilder().append(m1.get("policyCode"),
                                m2.get("policyCode"))
                                .append(m1.get("applyTime"),
                                                m2.get("applyTime"))
                                .toComparison();
            }
        });
        return list;
    }
}