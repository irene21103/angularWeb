package com.ebao.ls.uw.ctrl.info;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.foundation.core.logging.Log;
import com.ebao.ls.image.ci.ImageCI;
import com.ebao.ls.pa.pub.bs.PolicyService;
import com.ebao.ls.pa.pub.vo.InsuredVO;
import com.ebao.ls.pa.pub.vo.PolicyHolderVO;
import com.ebao.ls.pa.pub.vo.PolicyVO;
import com.ebao.ls.pub.CodeCst;
import com.ebao.ls.pub.cst.NBPolicySpecialRuleType;
import com.ebao.ls.uw.ds.UwInsureHistoryService;
import com.ebao.ls.uw.ds.UwInsureImageService;
import com.ebao.ls.uw.ds.vo.UwInsureClientVO;
import com.ebao.pub.framework.GenericAction;
import com.ebao.pub.util.StringUtils;

import edu.emory.mathcs.backport.java.util.Collections;

/**
 * <p>Title: 查詢影像資料</p>
 * <p>Description: Function Description Only vegetable</p>
 * <p>Copyright: Copyright (c) 2015</p>
 * <p>Company: TGL Co., Ltd.</p>
 * <p>Create Time: Aug 25, 2015</p> 
 * @author 
 * <p>Update Time: Aug 25, 2015</p>
 * <p>Updater: Alex Cheng</p>
 * <p>Update Comments: </p>
 */
public class InsureImageRecordAction extends GenericAction {

	private static String IMAGE_MODE__POLICY_IMAGE_DATA = "policyImageData";
	private static boolean NOT_POSITIVE = false;
	private  final Log  log  = Log.getLogger(InsureImageRecordAction.class);
	
	@Resource(name = UwInsureHistoryService.BEAN_DEFAULT)
	private UwInsureHistoryService uwInsureHistoryService;

	@Resource(name = UwInsureImageService.BEAN_DEFAULT)
	private UwInsureImageService uwInsureImageService;
	
	@Resource(name = ImageCI.BEAN_DEFAULT)
	private ImageCI imageCI;
	
	@Resource(name = PolicyService.BEAN_DEFAULT)
	private PolicyService policyService;

	@Override
	public ActionForward process(ActionMapping mapping, ActionForm form,
					HttpServletRequest request, HttpServletResponse response)
					throws Exception {

		UwHistoryInsureForm uwHistoryInsureForm = (UwHistoryInsureForm) form;
		String policyId = request.getParameter("policyId");
		String imageMode = request.getParameter("imageMode");
		String forward = "display";

		if (!StringUtils.isNullOrEmpty(policyId)) {
			if(StringUtils.isNullOrEmpty(imageMode)){
				uwHistoryInsureForm.setPolicyId(new Long(policyId));
				UwInsureClientVO vo = new UwInsureClientVO();
	            PolicyHolderVO policyHolderVO = uwInsureHistoryService.findPolicyHolderById(policyId);
				
				
				/* 依policyId取得所有被保險人 */
				for (InsuredVO o : uwInsureHistoryService.findInsuredById(policyId)) {
					if (o != null && !StringUtils.isNullOrEmpty(o.getCertiCode())
	                                && !StringUtils.isNullOrEmpty(o.getCertiType())) {
						
						boolean flag = true;
						for (UwInsureClientVO uwInsureClientVO : uwHistoryInsureForm.getInsureClientList()) {
							if (uwInsureClientVO.getCertiCode().equals(o.getCertiCode())) {
								flag = false;
								break;
							}
						}
						if (flag) {
							UwInsureClientVO tmpVo = new UwInsureClientVO();
							tmpVo.setCertiCode(o.getCertiCode());
							tmpVo.setCertiType(o.getCertiType());
							tmpVo.setName(o.getName());
							tmpVo.setOrderType("1");
							tmpVo.setOldForeignerId(o.getOldForeignerId());
							uwHistoryInsureForm.getInsureClientList().add(tmpVo);
						}
					}
				}
                
				/* 要保人資料 */
				if (policyHolderVO != null &&
								!StringUtils.isNullOrEmpty(policyHolderVO.getCertiCode()) &&
								!StringUtils.isNullOrEmpty(policyHolderVO.getCertiType())) {
					boolean flag = true;
					for (UwInsureClientVO uwInsureClientVO : uwHistoryInsureForm.getInsureClientList()) {
						if (uwInsureClientVO.getCertiCode().equals(policyHolderVO.getCertiCode())) {
							flag = false;
							break;
						}
					}
					if (flag) {
						vo.setCertiCode(policyHolderVO.getCertiCode());
	                    vo.setCertiType(policyHolderVO.getCertiType());
	                    vo.setName(policyHolderVO.getName());
	                    vo.setOrderType("2");
	                    vo.setOldForeignerId(policyHolderVO.getOldForeignerId());
	                    uwHistoryInsureForm.getInsureClientList().add(vo);
					}
				}
				/* 要被保人歷史投保記錄 */
				if (!CollectionUtils.isEmpty(uwHistoryInsureForm.getInsureClientList())) {
					uwInsureImageService.findInsureImageByClient(policyId, uwHistoryInsureForm.getInsureClientList());
                }
				forward = "display";
			}
		}
		
		if(!StringUtils.isNullOrEmpty(imageMode) && IMAGE_MODE__POLICY_IMAGE_DATA.equals(imageMode)){
			List<Map<String,Object>> images = new ArrayList<Map<String,Object>>();
			//			Map<Integer,String> imageMap = new HashMap<Integer,String>();
			if(!StringUtils.isNullOrEmpty(uwHistoryInsureForm.getImagePolicyId())){
				PolicyVO policy = policyService.load(new Long(uwHistoryInsureForm.getImagePolicyId()));
				images = imageCI.queryImageBySpRule(new Long(uwHistoryInsureForm.getImagePolicyId()), NBPolicySpecialRuleType.NB_UW_IMAGE_QUERY_RULE, InsureImageRecordAction.NOT_POSITIVE);
				
				if(policy != null){
					uwHistoryInsureForm.setImagePolicyCode(policy.getPolicyNumber());
				}
			}

            List<InsuredImageRecord> records = new ArrayList<InsuredImageRecord>();
            // 整理 CardCodeID 對 ImageListID
			if(images != null && !images.isEmpty()){
				Integer lastImageTypeId = null;
				Date lastScanTime = null;
				InsuredImageRecord record = new InsuredImageRecord();

				for (Map<String, Object> vo : images) {
					if (lastImageTypeId == null) {
						record = new InsuredImageRecord();
						record.setCardId((Integer) vo.get("imageTypeId"));
						record.setScanTime((Date) vo.get("scanTime"));
						List<Long> ids = new ArrayList<Long>();
						ids.add((Long) vo.get("imageId"));
						record.setImageIds(ids);

						lastImageTypeId = record.getCardId();
						lastScanTime = (Date) vo.get("scanTime");						
						
					} else if ((!lastImageTypeId.equals(vo.get("imageTypeId")))
									|| (!lastScanTime.equals(vo.get("scanTime")))) {
						records.add(record); // 放入回傳物件
						record = new InsuredImageRecord();
						record.setCardId((Integer) vo.get("imageTypeId"));
						record.setScanTime((Date) vo.get("scanTime"));
						List<Long> ids = new ArrayList<Long>();
						ids.add((Long) vo.get("imageId"));
						record.setImageIds(ids);

						lastImageTypeId = record.getCardId();
						lastScanTime = (Date) vo.get("scanTime");
					} else {
						// 同一影像類型,加入image_id
						record.getImageIds().add((Long) vo.get("imageId"));
					}
				}
				// 最後一筆
				records.add(record);
	            
			}
            
			// 將 ImageListID List 轉成 String
			if (records != null && !records.isEmpty()) {
				for (int i = 0; i < records.size(); i++) {
					InsuredImageRecord iir = records.get(i);
					Integer imageTypeId = iir.getCardId();
					List<Long> imgIds = iir.getImageIds();
					// 契審表需排序 ( 同 uwWorkSheetService.getUnb0631ImageIds 依照 ImageId 排序)；高齡投保評估量表、AML因多份重新排序
					if (Integer.valueOf(CodeCst.CARD_ID_UNBN040).equals(imageTypeId) 
							|| Integer.valueOf(CodeCst.CARD_ID_UNBN073).equals(imageTypeId) 
							|| Integer.valueOf(CodeCst.CARD_ID_UNBN051).equals(imageTypeId)) {
						Collections.sort(imgIds);
						iir.setImageIds(imgIds);
					}
					iir.setImageIdStr(org.apache.commons.lang.StringUtils.join(iir.getImageIds().toArray(), ','));
					records.set(i, iir);
				}
			}
            
			uwHistoryInsureForm.setInsureImageRecordList(records);
			forward = imageMode;
		}
		return mapping.findForward(forward);
	}

	private String genImageIdStr(Long imageId , String value){
		StringBuffer sb = null;
		if(value == null ){
			sb = new StringBuffer("");
		}else{
			sb = new StringBuffer(value).append(",");
		}
		
		if(imageId != null){
			sb.append(String.valueOf(imageId));
		}
		return sb.toString();
	}
}
