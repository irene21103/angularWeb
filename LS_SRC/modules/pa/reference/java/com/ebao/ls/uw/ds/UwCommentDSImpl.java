package com.ebao.ls.uw.ds;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.ebao.ls.pa.pub.service.PolicyService;
import com.ebao.ls.product.service.LifeProductService;
import com.ebao.ls.uw.data.TUwCommentDelegate;
import com.ebao.ls.uw.data.TUwCommentOptionDelegate;
import com.ebao.ls.uw.data.TUwCommentVersionDelegate;
import com.ebao.ls.uw.data.bo.UwComment;
import com.ebao.ls.uw.data.bo.UwCommentVersion;
import com.ebao.ls.uw.vo.UwCommentOptionVO;
import com.ebao.ls.uw.vo.UwCommentVO;
import com.ebao.pub.framework.ExceptionFactory;
import com.ebao.pub.framework.GenericDS;
import com.ebao.pub.framework.GenericException;
import com.ebao.pub.util.BeanUtils;

public class UwCommentDSImpl extends GenericDS implements UwCommentService {

    @Resource(name = LifeProductService.BEAN_DEFAULT)
    private LifeProductService lifeProductService;

    @Resource(name = TUwCommentVersionDelegate.BEAN_DEFAULT)
    private TUwCommentVersionDelegate uwCommentVersionDao;

    @Resource(name = TUwCommentOptionDelegate.BEAN_DEFAULT)
    private TUwCommentOptionDelegate uwCommentOptionDao;

    @Resource(name = TUwCommentDelegate.BEAN_DEFAULT)
    private TUwCommentDelegate uwCommentDao;

    @Resource(name = PolicyService.BEAN_DEFAULT)
    private PolicyService policyService;

    /* 核保綜合意見欄年本版*/
    public static final int UW_COMMENT_VERSION_TYPE_ANNUITY = 2;

    /* 核保綜合意見欄非年本版   */
    public static final int UW_COMMENT_VERSION_TYPE_NOT_ANNUITY = 1;
    
    /* 核保綜合意見欄OIU版   */
    public static final int UW_COMMENT_VERSION_TYPE_OIU = 3;

    @Override
    public Long getCommentVersionId(Long productId, boolean isOIU ,Date date)
                    throws GenericException {
        int commentType = isOIU ? UW_COMMENT_VERSION_TYPE_OIU:(policyService.isAnnuityProd(productId) ? UW_COMMENT_VERSION_TYPE_ANNUITY
                        : UW_COMMENT_VERSION_TYPE_NOT_ANNUITY);

        UwCommentVersion version = uwCommentVersionDao.findByDate(commentType,
                        date);
        if (version != null) {
            return version.getListId();
        } else {
            return 0L;
        }
    }

    @Override
    public Collection getCommentOptionListByVersionId(
                    Long versionId)
                    throws GenericException {
        try {
            Collection c = uwCommentOptionDao.findByVersionId(versionId);
            return BeanUtils.copyCollection(UwCommentOptionVO.class, c);
          } catch (Exception e) {
            throw ExceptionFactory.parse(e);
          }

    }

    @Override
    public Collection getCommentResultByUnderwriteId(Long underwriteId)
                    throws GenericException {
        try {
            List<UwComment> c = uwCommentDao.findByUnderwriteId(underwriteId);
            return BeanUtils.copyCollection(UwCommentVO.class, c);
        } catch (Exception e) {
            throw ExceptionFactory.parse(e);
        }
    }

    @Override
    public void saveResults(Long underwriteId,
                    List<UwCommentVO> uwCommentVOList)
                    throws GenericException {
        List<UwComment> olds = uwCommentDao.findByUnderwriteId(underwriteId);
        Map<Long, UwComment> map = new HashMap<Long,UwComment>();
        if ( olds != null) {
            for (UwComment uwComment : olds) {
                map.put(uwComment.getOptionId(), uwComment);
            }
        }

            for (UwCommentVO vo : uwCommentVOList) {
                Long optionId = vo.getOptionId();
                if (map.containsKey(optionId)) {
                /* 如果原本option_id已經存在, 把list_id加上, 用update*/
                    UwComment old = map.get(optionId);
                    vo.setListId(old.getListId());
                    vo.setUnderwriteId(underwriteId);
                    uwCommentDao.update((UwComment) BeanUtils.getBean(
                                    UwComment.class, vo));
                /* 移除map中的物件*/
                map.remove(optionId);
                } else {
                /* 如果原本option_id不存在, 用新增*/
                    vo.setUnderwriteId(underwriteId);
                    uwCommentDao.create((UwComment) BeanUtils.getBean(
                                    UwComment.class, vo));
                }
            }

        /* map中如果還有object 要刪除 */
        for (Long optionId : map.keySet()) {
            UwComment old = map.get(optionId);
            uwCommentDao.remove(old.getListId());
        }
    }
}