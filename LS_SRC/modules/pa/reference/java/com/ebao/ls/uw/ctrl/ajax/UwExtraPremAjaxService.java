package com.ebao.ls.uw.ctrl.ajax;

import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.beanutils.BeanPredicate;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.PredicateUtils;
import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.log4j.Logger;

import com.ebao.foundation.module.db.hibernate.HibernateSession3;
import com.ebao.foundation.module.web.action.ExceptionInfoUtils;
import com.ebao.ls.cs.commonflow.data.TChangeIndexDelegate;
import com.ebao.ls.cs.commonflow.data.TCsPremDelegate;
import com.ebao.ls.cs.commonflow.data.query.JobCategoryDao;
import com.ebao.ls.cs.commonflow.ds.CSPremiumRecalculateService;
import com.ebao.ls.cs.commonflow.ds.CSPremiumService;
import com.ebao.ls.cs.pub.Cst;
import com.ebao.ls.cs.pub.bo.CommonDumper;
import com.ebao.ls.cs.pub.ds.CSDifferencePremiumService;
import com.ebao.ls.cs.util.pkg.CalculatorPremSP;
import com.ebao.ls.fnd.bs.FundTransactionService;
import com.ebao.ls.fnd.bs.sp.FundTransApplyDAO;
import com.ebao.ls.letter.common.CommonLetterService;
import com.ebao.ls.pa.nb.bs.ProposalService;
import com.ebao.ls.pa.nb.bs.rule.ProposalRuleMsgService;
import com.ebao.ls.pa.nb.bs.rule.ProposalRuleResultService;
import com.ebao.ls.pa.nb.ci.ValidatorService;
import com.ebao.ls.pa.nb.ctrl.pub.ajax.AjaxBaseService;
import com.ebao.ls.pa.nb.rule.impl.ExtraPremValidator;
import com.ebao.ls.pa.nb.util.NBUtils;
import com.ebao.ls.pa.nb.util.NumericUtils;
import com.ebao.ls.pa.nb.util.UnbCst;
import com.ebao.ls.pa.nb.vo.ProposalRuleResultVO;
import com.ebao.ls.pa.pub.MsgCst;
import com.ebao.ls.pa.pub.TableCst;
import com.ebao.ls.pa.pub.bs.ExtraPremService;
import com.ebao.ls.pa.pub.bs.PolicyService;
import com.ebao.ls.pa.pub.bs.calc.CoverageCalculatorService;
import com.ebao.ls.pa.pub.bs.calc.PolicyCalculatorService;
import com.ebao.ls.pa.pub.bs.coverage.CoverageService;
import com.ebao.ls.pa.pub.bs.impl.sp.CoverageSP;
import com.ebao.ls.pa.pub.ci.CalculatorCI;
import com.ebao.ls.pa.pub.data.bo.Coverage;
import com.ebao.ls.pa.pub.data.bo.ExtraPrem;
import com.ebao.ls.pa.pub.data.bo.PremDiscount;
import com.ebao.ls.pa.pub.data.org.CoverageDao;
import com.ebao.ls.pa.pub.data.org.ExtraPremDao;
import com.ebao.ls.pa.pub.data.org.PremDiscountDao;
import com.ebao.ls.pa.pub.vo.CoveragePremium;
import com.ebao.ls.pa.pub.vo.CoverageVO;
import com.ebao.ls.pa.pub.vo.ExtraPremVO;
import com.ebao.ls.pa.pub.vo.InsuredVO;
import com.ebao.ls.pa.pub.vo.PolicyVO;
import com.ebao.ls.product.model.query.output.LifeProduct;
import com.ebao.ls.product.service.LifeProductCategoryService;
import com.ebao.ls.pub.CodeCst;
import com.ebao.ls.pub.cst.ProductType;
import com.ebao.ls.pub.cst.ProposalRuleMsg;
import com.ebao.ls.pub.cst.ProposalRuleStatus;
import com.ebao.ls.pub.cst.ProposalRuleType;
import com.ebao.ls.pub.cst.UwDocumentApprovalStatus;
import com.ebao.ls.pub.cst.YesNo;
import com.ebao.ls.pub.util.BizRounder;
import com.ebao.ls.pub.util.BizUtils;
import com.ebao.ls.uw.ctrl.pub.UwActionHelper;
import com.ebao.ls.uw.ds.UwIssuesLetterService;
import com.ebao.ls.uw.ds.UwPolicyService;
import com.ebao.ls.uw.ds.UwProcessService;
import com.ebao.ls.uw.vo.UwExtraLoadingVO;
import com.ebao.ls.uw.vo.UwLifeInsuredVO;
import com.ebao.ls.uw.vo.UwPolicyVO;
import com.ebao.ls.uw.vo.UwProductVO;
import com.ebao.pub.event.EventService;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.framework.GenericException;
import com.ebao.pub.framework.RTException;
import com.ebao.pub.framework.internal.spring.DSProxy;
import com.ebao.pub.util.DateUtils;
import com.ebao.pub.util.Log;
import java.util.Arrays;
import org.apache.commons.beanutils.BeanPropertyValueEqualsPredicate;
import org.apache.commons.lang3.ArrayUtils;

/**
 * <p>
 * Title: 新契約-核保作業
 * </p>
 * <p>
 * Description: 加費作業
 * </p>
 * <p>
 * Copyright: Copyright (c) 2016
 * </p>
 * <p>
 * Company: TGL Co., Ltd.
 * </p>
 * <p>
 * Create Time: Jun 22, 2016
 * </p>
 * 
 * @author <p>
 *         Update Time: Jun 22, 2016
 *         </p>
 *         <p>
 *         Updater: simon.huang
 *         </p>
 *         <p>
 *         Update Comments:
 *         </p>
 */
public class UwExtraPremAjaxService extends AjaxBaseService {

	public static final String BEAN_DEFAULT = "/uw/uwExtraPremAjaxService";
	
	// IR #322328 檢核是否VUL01-1商品
    private final String PRODUCT_VUL01 = ProductType.PRODUCT_LIFE_INTERNAL_VUL +  "01-1";

	@Resource(name = UwPolicyService.BEAN_DEFAULT)
	private UwPolicyService uwPolicyDS;

	@Resource(name = UwActionHelper.BEAN_DEFAULT)
	private UwActionHelper uwActionHelper;

	@Resource(name = JobCategoryDao.BEAN_DEFAULT)
	private JobCategoryDao jobCategoryDao;

	@Resource(name = ExtraPremService.BEAN_DEFAULT)
	private ExtraPremService extraPremService;

	@Resource(name = CoverageDao.BEAN_DEFAULT)
	private CoverageDao<Coverage> coverageDao;

	@Resource(name = ProposalRuleMsgService.BEAN_DEFAULT)
	private ProposalRuleMsgService proposalRuleMsgService;

	@Resource(name = ProposalRuleResultService.BEAN_DEFAULT)
	private ProposalRuleResultService proposalRuleResultService;

	@Resource(name = ValidatorService.BEAN_DEFAULT)
	private ValidatorService validatorService;

	@Resource(name = CSPremiumRecalculateService.BEAN_DEFAULT)
	private CSPremiumRecalculateService csPremiumRecalculateService;

	@Resource(name = CommonLetterService.BEAN_DEFAULT)
	protected CommonLetterService commonLetterService;

	@Resource(name = EventService.BEAN_DEFAULT)
	protected EventService eventService;

	@Resource(name = CoverageService.BEAN_DEFAULT)
	private CoverageService coverageService;

	@Resource(name = UwIssuesLetterService.BEAN_DEFAULT)
	private UwIssuesLetterService uwIssuesLetterService;

	@Resource(name = CoverageCalculatorService.BEAN_DEFAULT)
	private CoverageCalculatorService coverageCalculatorService;

	@Resource(name = PolicyCalculatorService.BEAN_DEFAULT)
	private PolicyCalculatorService policyCalculatorService;

	@Resource(name = UwProcessService.BEAN_DEFAULT)
	private UwProcessService uwProcessDS;

	@Resource(name = CSDifferencePremiumService.BEAN_DEFAULT)
	private CSDifferencePremiumService csDiffPremService;

	@Resource(name = CSPremiumService.BEAN_DEFAULT)
	private CSPremiumService csPremDS;

	@Resource(name = ProposalService.BEAN_DEFAULT)
	private ProposalService proposalService;

    @Resource(name = PremDiscountDao.BEAN_DEFAULT_VIEW)
    protected PremDiscountDao<PremDiscount> premDiscountDao;

	@Resource(name = CalculatorCI.BEAN_DEFAULT)
	private CalculatorCI calculatorCI;
	
	@Resource(name = ExtraPremDao.BEAN_DEFAULT)
	private ExtraPremDao<ExtraPrem> extraPremDao;
	
    @Resource(name = FundTransactionService.BEAN_DEFAULT)
    private FundTransactionService fundTransactionService;
    
    @Resource(name = FundTransApplyDAO.BEAN_DEFAULT)
    private FundTransApplyDAO fundTransApplyDAO;
    
    @Resource(name = PolicyService.BEAN_DEFAULT)
	private PolicyService policyService;

    @Resource(name = LifeProductCategoryService.BEAN_DEFAULT)
    private LifeProductCategoryService lifeProductCategoryService;

	Logger log4j = Logger.getLogger(getClass());

	/**
     * <p>
     * Description : 畫面初始-回傳畫面所需資料
     * </p>
     * <p>
     * Created By : simon.huang
     * </p>
     * <p>
     * Create Time : Jun 3, 2016
     * </p>
     * 
	* @param in
	* @param output
	* @throws Exception
	*/
	public void initEditPage(Map<String, Object> in, Map<String, Object> output) throws Exception {
		Long policyId = NumericUtils.parseLong(in.get("policyId"));
		Long underwriteId = NumericUtils.parseLong(in.get("underwriteId"));
		String reqFrom = (String) in.get("reqFrom");
		String itemId = (String) in.get("itemId");
//		String validDate = (String) in.get("validDate");
		
		if (policyId != null && underwriteId != null) {

			PolicyVO policyInfo = policyService.load(policyId);
			CoverageVO masterCoverage = policyInfo.gitMasterCoverage();

			List<Map<String, Object>> insuredList = uwActionHelper.getDisplayInusredList(policyInfo, in);
			List<Map<String, Object>> coverageList = uwActionHelper.getDisplayCoverageList(policyInfo, in);

			// 設定保險成本
			uwActionHelper.setupCOI(in, coverageList);
			
			// 險種加費
			Collection<UwExtraLoadingVO> uwExtraLoadingList = uwPolicyDS.findUwExtraLoadingEntitis(underwriteId);
			List<Map<String, Object>> uwExtraPremList = this.getDisplayUwxtraLoadingList(uwExtraLoadingList, reqFrom, itemId);

			// fixed CGI Stored XSS
			//System.out.println(getClass()+":uwExtraLoadingList=" + uwExtraLoadingList);
			//System.out.println("uwExtraPremList=" + uwExtraPremList);

			//2017/06/14 simon.huang,
			//RTC 155815-OIU商品職業加費金額反灰不可人工輸入
			output.put("isOIU", policyInfo.isOIU() ? CodeCst.YES_NO__YES : CodeCst.YES_NO__NO);

			if (StringUtils.equals(reqFrom, "pos")) {
				// pos計算本次加費金額
				Long changeId = NumericUtils.parseLong(in.get("changeId"));
				Long policyChgId = NumericUtils.parseLong(in.get("policyChgId"));
				Long item = Long.parseLong(itemId);
				BigDecimal healthExtraAmt = BigDecimal.ZERO;
				BigDecimal occuptExtraAmt = BigDecimal.ZERO;
				
				BigDecimal healthExtraAmtTmp = BigDecimal.ZERO;
				
				for (UwExtraLoadingVO uwExtraLoadingVO : uwExtraLoadingList) {
					if (item.equals(uwExtraLoadingVO.getItemId())) {
						if (StringUtils.equals(uwExtraLoadingVO.getExtraType(), CodeCst.EXTRA_TYPE__HEALTH_EXTRA)) {
			    healthExtraAmt = uwExtraLoadingVO.getExtraPrem(); // add
									      // for
									      // CS
							
							if(healthExtraAmt.equals(BigDecimal.ZERO)) {
								Collection<UwProductVO> uwProducts = uwPolicyDS.findUwProductEntitis(underwriteId);
								Map<String, UwProductVO> itemIdBindUwProduct = NBUtils.toMap(uwProducts, "itemId");
								UwProductVO uwProductVO = itemIdBindUwProduct.get(String.valueOf(itemId));
								
								boolean ilp = coverageService.isILPBenefit(item);
								CoverageVO coverageVO = coverageService.load(item);
								// #322328  dueDate: 投資型商品使用 下次周月日；傳統型商品使用下次應繳日
								Date dueDate = ilp ? extraPremDao.getMinChargeDueDate(item) : coverageVO.getCoverageExtend().getDueDate();
				// #322328 validateDate: 投資型商品使用
				// 下次周月日；傳統型商品使用變更生效日
								Date validateDate = ilp ? dueDate : uwExtraLoadingVO.getStartDate();
								
								BigDecimal extraPrem = BigDecimal.ZERO;
								BigDecimal extraPremAn = BigDecimal.ZERO;
								
				List<BigDecimal> calcResult = calculatorCI.getTryExtraPremAfAn(uwExtraLoadingVO.getItemId(), uwProductVO.getAmount(), // 險種保額
					uwExtraLoadingVO.getInsuredId(), dueDate, // dueDate
										validateDate,//validatedate
										"N", //nextOrThis
					uwExtraLoadingVO.getExtraType(), uwExtraLoadingVO.getExtraArith(), uwExtraLoadingVO.getExtraPara(), // 加費比例
																	    // 或每萬元加費金額
										new BigDecimal(uwExtraLoadingVO.getEmValue()));
								System.out.println("calcResult="+calcResult);
								
								extraPrem = (BigDecimal) calcResult.get(0);
								extraPremAn = (BigDecimal) calcResult.get(1);
								if(extraPrem != null && extraPrem.compareTo(BigDecimal.ZERO)!=0) {
									healthExtraAmtTmp= extraPrem;
									
								}
							}	
							
			} else if (StringUtils.equals(uwExtraLoadingVO.getExtraType(), CodeCst.EXTRA_TYPE__OCCUPATION_EXTRA)) {
							occuptExtraAmt = uwExtraLoadingVO.getExtraPrem();
						}
					}
				}
		BigDecimal healthExtraDifferAmt = extraPremService.getDifferExtraPremByExtraType(healthExtraAmt, changeId, policyChgId, item, CodeCst.EXTRA_TYPE__HEALTH_EXTRA);
		BigDecimal jobDifferAmt = extraPremService.getDifferExtraPremByExtraType(occuptExtraAmt, changeId, policyChgId, item, CodeCst.EXTRA_TYPE__OCCUPATION_EXTRA);

		// input start date/end date/charge
		// mode/commencementDate/cessationDate(coverageVO.getExpiryDate();)
		// 計算總加費金額後 需在乘上日期係數、保費折扣
		boolean isPOS = (reqFrom != null && reqFrom.equals("pos"));
		if (isPOS) {
		    CoverageVO coverageVO = coverageService.load(item);
		    String chargeMode = coverageVO.getCurrentPremium().getPaymentFreq();
		    Date commencementDate = coverageVO.getInceptionDate();
		    Date cessationDate = coverageVO.getExpiryDate();
		    if (CodeCst.LIABILITY_STATUS__TERMINATED == coverageVO.getRiskStatus().intValue()) {
			cessationDate = coverageVO.getRiskStatusDate();
		    }
		    Date startDate = DateUtils.toDate((String) in.get("validDate"));
		    Date endDate = coverageVO.getCoverageExtend().getDueDate();
		    String projectCode = policyInfo.getProjectCode();
		    CSDifferencePremiumService ds = (CSDifferencePremiumService) DSProxy.newInstance(CSDifferencePremiumService.class);
		    // 日期比例
		    BigDecimal proportion = ds.prepareProRataProportion(chargeMode, startDate, endDate, commencementDate, cessationDate);
		    // 保費折扣
		    BigDecimal discntRate = BigDecimal.ONE;
		    String list = "";

		    list = premDiscountDao.findByDiscnt(policyId, item);
		    System.out.println("startDate(validDate)=" + startDate + ", endDate(dueDate)=" + endDate + ", premDiscount list=(" + list + ")");
		    String[] discntForPOS = list.split(",");
		    if (discntForPOS != null && discntForPOS.length > 1) {
			if (startDate.before(endDate)) {
			    discntRate = discntRate.subtract(new BigDecimal(discntForPOS[0]));
			} else {
			    discntRate = discntRate.subtract(new BigDecimal(discntForPOS[1]));
			}

		    }
		    healthExtraDifferAmt = BizRounder.round(healthExtraDifferAmt.multiply(proportion).multiply(discntRate), BizRounder.RND_TYPE_PREMIUM);
		    jobDifferAmt = BizRounder.round(jobDifferAmt.multiply(proportion).multiply(discntRate), BizRounder.RND_TYPE_PREMIUM);
		}

				output.put("healthExtra", healthExtraDifferAmt); // 至前端;
				output.put("occptExtra", jobDifferAmt); // 至前端;
				System.out.println("healthExtraDifferAmt="+healthExtraDifferAmt+", jobDifferAmt="+jobDifferAmt);
				for(Map<String, Object> extraPrem : uwExtraPremList) {
					for (Entry<String, Object> entry : extraPrem.entrySet()) {
						if(StringUtils.equals(entry.getKey(), "extraPrem")) {
							if (NumberUtils.createBigDecimal(ObjectUtils.defaultIfNull(entry.getValue(), "0").toString()).compareTo(BigDecimal.ZERO) == 0) {
								extraPrem.put("extraPrem", healthExtraAmtTmp);
							}
						}
			}
				}				
			}

			Map<String, Object> codeTableMap = new HashMap<String, Object>();
			codeTableMap.put("nbGender", NBUtils.getTCodeData(TableCst.V_NB_GENDER));
			codeTableMap.put("insuredCategory", NBUtils.getTCodeData(TableCst.T_INSURED_CATEGORY));
			output.put("codeTable", codeTableMap);

			// =============================================================
			// 設定照會狀態與簽核狀態
			String documentStatus = "";
			String approvalStatus = "";
			Long msgId = null;
			Long docId = null;
			// 取出DB中的msgId,docId 正常情況所有資料msgId,docId應一致
			for (UwExtraLoadingVO extraPremVO : uwExtraLoadingList) {
				if (extraPremVO.getMsgId() != null) {
					msgId = extraPremVO.getMsgId();
					docId = extraPremVO.getDocumentId();
					break;
				}
			}

			if (msgId != null && docId != null) {
				documentStatus = uwIssuesLetterService.getMasterDocStatus(msgId);
			}

			if (msgId != null) {
				ProposalRuleResultVO ruleResult = proposalRuleResultService.load(msgId);
				approvalStatus = ruleResult.getUwDocumentApprovalStatus();
			}
			output.put("documentStatus", documentStatus);
			output.put("approvalStatus", approvalStatus);
			
			output.put("insuredList", insuredList); // 被保險人列表
			output.put("coverageList", coverageList); // 險種列表
			output.put("uwExtraPremList", uwExtraPremList); // 險種加費
	    // BC353 PCR-263273 加費頁面要以台外幣區分保險成本COI要不要顯示小數點 2018/11/01 Add by
	    // Kathy
			output.put("policyCurrency", policyInfo.getCurrency()); // 保單幣別

			if (masterCoverage != null) {//BC463 PCR-513569
				String productSpecialType = lifeProductCategoryService.getTypeProductSpecial(masterCoverage.getProductId(), CodeCst.NB_PRODUCT_SPECIAL_TYPE__VL);
				if (CodeCst.NB_PRODUCT_SPECIAL_TYPE__VL.equals(productSpecialType)) {
					String internalId = lifeProductService.getProductByVersionId(masterCoverage.getProductVersionId()).getProduct().getInternalId();
					output.put("internalId", internalId);
					output.put("feeRateDanger", ExtraPremValidator.FEE_RATE_DANGER);
					output.put("feeRateReject", ExtraPremValidator.FEE_RATE_REJECT);
				}
			}

			// =============================================================
			output.put(KEY_SUCCESS, SUCCESS);
			return;

		}
	}

	/**
     * <p>
     * Description : 試算加費金額
     * </p>
     * <p>
     * Created By : simon.huang
     * </p>
     * <p>
     * Create Time : Jul 14, 2016
     * </p>
     * 
	* @param in
	* @param output
	* @throws Exception
	*/
	@SuppressWarnings({ "unchecked", "unused" })
	private void calcPOSExtraPrem(Map<String, Object> in, Map<String, Object> output) throws Exception {
		Long policyId = NumericUtils.parseLong(in.get("policyId"));
		Long underwriteId = NumericUtils.parseLong(in.get("underwriteId"));
		String type = (String) in.get(KEY_TYPE);

		String reqFrom = (String) in.get("reqFrom");
		BigDecimal healthExtraAmt = BigDecimal.ZERO;
		BigDecimal occuptExtraAmt = BigDecimal.ZERO;
		Map<String, Object> submitData = (Map<String, Object>) in.get(KEY_DATA);
		String itemIdStr = (String) submitData.get("itemIdStr");
		Long itemId = Long.parseLong(itemIdStr);
		Long partyId = NumericUtils.parseLong(submitData.get("partyId"));

		Collection<UwProductVO> uwProducts = uwPolicyDS.findUwProductEntitis(underwriteId);
		Map<String, UwProductVO> itemIdBindUwProduct = NBUtils.toMap(uwProducts, "itemId");

		Collection<UwExtraLoadingVO> allExtra = uwPolicyDS.findUwExtraLoadingEntitis(underwriteId);
		// 過濾此被保險人險種資料
		CollectionUtils.filter(allExtra, new BeanPredicate("insuredId", PredicateUtils.equalPredicate(partyId)));
		// 同步頁面弱體加費資料至DB
		Map<String, List<UwExtraLoadingVO>> extraTypeBindExtraLoading = NBUtils.toMapList(allExtra, "extraType");

		List<UwExtraLoadingVO> returnList = new ArrayList<UwExtraLoadingVO>();

		/*
		 * IR-434786 不要區分畫面是勾選哪一項加費，弱加、職加都重新算。
		 * 原本有依畫面勾選項目計算，但是如果已做弱體加費再勾選職業加費，輸入職加的加費比率後會自動觸發重新計算。
		 * 此時沒計算弱加，會被JS誤判弱加的nextExtraPrem為0，畫面上的弱加金額會變成0。 
		 */
		List<UwExtraLoadingVO> healthExtraPremList = UwExtraPremHelper.getHealthExtraPremList(in, uwProducts);
		Log.info(getClass(), "healthByItem 弱體加費(險種)重新計算");
		for (UwExtraLoadingVO uwExtraLoadingVO : healthExtraPremList) {
			if (uwExtraLoadingVO.getItemId().longValue() == itemId) {
				UwProductVO uwProductVO = itemIdBindUwProduct.get(String.valueOf(itemId));
				uwActionHelper.calcExtraPrem(uwExtraLoadingVO, uwProductVO, in);
				returnList.add(uwExtraLoadingVO);
				healthExtraAmt = uwExtraLoadingVO.getExtraPrem(); // add for CS
				break;
			}
		}

		List<UwExtraLoadingVO> jobExtraPremList = UwExtraPremHelper.getJobExtraPremList(in, uwProducts);
		Log.info(getClass(), "job 職業加費重新計算");
		for (UwExtraLoadingVO uwExtraLoadingVO : jobExtraPremList) {
			if (uwExtraLoadingVO.getItemId().longValue() == itemId) {
				UwProductVO uwProductVO = itemIdBindUwProduct.get(String.valueOf(itemId));
				uwActionHelper.calcExtraPrem(uwExtraLoadingVO, uwProductVO, in);
				returnList.add(uwExtraLoadingVO);
				occuptExtraAmt = uwExtraLoadingVO.getExtraPrem();
				break;
			}
		}

		// 計算本次加費金額
		Long changeId = Long.parseLong((String) in.get("changeId"));
		Long policyChgId = Long.parseLong((String) in.get("policyChgId"));
		
	BigDecimal healthExtraDifferAmt = extraPremService.getDifferExtraPremByExtraType(healthExtraAmt, changeId, policyChgId, itemId, CodeCst.EXTRA_TYPE__HEALTH_EXTRA);
	BigDecimal jobDifferAmt = extraPremService.getDifferExtraPremByExtraType(occuptExtraAmt, changeId, policyChgId, itemId, CodeCst.EXTRA_TYPE__OCCUPATION_EXTRA);

		// input start date/end date/charge
		// mode/commencementDate/cessationDate(coverageVO.getExpiryDate();)
		// 計算總加費金額後 需在乘上日期係數、保費折扣
		PolicyVO policyInfo = policyService.load(policyId);
		CoverageVO coverageVO = coverageService.load(itemId);
		String chargeMode = coverageVO.getCurrentPremium().getPaymentFreq();
		Date commencementDate = coverageVO.getInceptionDate();
		Date cessationDate = coverageVO.getExpiryDate();
		if (CodeCst.LIABILITY_STATUS__TERMINATED == coverageVO.getRiskStatus().intValue()) {
			cessationDate = coverageVO.getRiskStatusDate();
		}
		Date startDate = DateUtils.toDate((String) in.get("validDate"));
		Date endDate = coverageVO.getCoverageExtend().getDueDate();
		String projectCode = policyInfo.getProjectCode();
	CSDifferencePremiumService ds = (CSDifferencePremiumService) DSProxy.newInstance(CSDifferencePremiumService.class);
		// 日期比例
	BigDecimal proportion = ds.prepareProRataProportion(chargeMode, startDate, endDate, commencementDate, cessationDate);
		// 保費折扣
		BigDecimal discntRate = BigDecimal.ONE;
		String list = "";

		list = premDiscountDao.findByDiscnt(policyId, itemId);
		System.out.println("startDate(validDate)="+startDate+", endDate(dueDate)="+endDate+", premDiscount list=("+list+")");
		String[] discntForPOS = list.split(",");
		if (discntForPOS != null && discntForPOS.length > 1) {
			if (startDate.before(endDate)) {
				discntRate = discntRate.subtract(new BigDecimal(discntForPOS[0]));
			} else {
				discntRate = discntRate.subtract(new BigDecimal(discntForPOS[1]));
			}

		}
		Log.info(getClass(), "日期比例" + proportion + "保費折扣" + discntRate);

	healthExtraDifferAmt = BizRounder.round(healthExtraDifferAmt.multiply(proportion).multiply(discntRate), BizRounder.RND_TYPE_PREMIUM);
	jobDifferAmt = BizRounder.round(jobDifferAmt.multiply(proportion).multiply(discntRate), BizRounder.RND_TYPE_PREMIUM);
		/// 試算 執行保單險種保費計算

		List<UwExtraLoadingVO> oldHealthExtra = extraTypeBindExtraLoading.get(CodeCst.EXTRA_TYPE__HEALTH_EXTRA);
		List<UwExtraLoadingVO> newHealthExtra = UwExtraPremHelper.getHealthExtraPremList(in, uwProducts);
		/* 需設定年化加費金額 ,另若為5-人工加費，需以加費金額與繳別系數取得年化加費金額 */
		for (UwExtraLoadingVO uwExtraLoadingVO : newHealthExtra) {
	    uwActionHelper.calcExtraPrem(uwExtraLoadingVO, itemIdBindUwProduct.get(uwExtraLoadingVO.getItemId().toString()), in);
		}
		String strForPOS = reqFrom + "," + itemIdStr;// POS處理時需要使用的數據
		this.syncPOSExtraPremByExtraType(newHealthExtra, oldHealthExtra, strForPOS);// reqFrom判斷是否為POS，若是則僅刪除本次修改的保項未被選擇的加費記錄
			
		// 同步頁面職業加費資料至DB
		List<UwExtraLoadingVO> oldJobExtra = extraTypeBindExtraLoading.get(CodeCst.EXTRA_TYPE__OCCUPATION_EXTRA);
		List<UwExtraLoadingVO> newJobExtra = UwExtraPremHelper.getJobExtraPremList(in, uwProducts);
		/* 需設定年化加費金額 ,另若為5-人工加費，需以加費金額與繳別系數取得年化加費金額 */
		for (UwExtraLoadingVO uwExtraLoadingVO : newJobExtra) {
	    uwActionHelper.calcExtraPrem(uwExtraLoadingVO, itemIdBindUwProduct.get(uwExtraLoadingVO.getItemId().toString()), in);
		}
		this.syncPOSExtraPremByExtraType(newJobExtra, oldJobExtra, strForPOS);// reqFrom判斷是否為POS，若是則僅刪除本次修改的保項未被選擇的加費記錄

		// 險種加費
		Collection<UwExtraLoadingVO> allNewExtra = uwPolicyDS.findUwExtraLoadingEntitis(underwriteId);
		Date validDate = DateUtils.toDate((String) in.get("validDate"));
		// 同步數據到POS
		syncDataToCS(underwriteId, policyId, changeId, policyChgId, Long.parseLong(itemIdStr));
		Long stdPremDiscnt = CoverageSP.getNbDisnctPrem(Long.parseLong(itemIdStr));
		// 重算保費
		csPremiumRecalculateService.recalcBenefitInBeforeOrAfterBlock(itemId, changeId, policyChgId, Cst.BLOCK_BEFORE);
	// IR 344740 align with PD interface, using total prem to calcualate
	// instead of gross prem & discnt rate
	BigDecimal totalPremBf = BizRounder.round(coverageVO.getCurrentPremium().getTotalPremAf().multiply(proportion), BizRounder.RND_TYPE_PREMIUM); // 變更前保費

		BigDecimal discntPremAfBeforeChange = coverageVO.getNextPremium().getDiscntPremAf();
		output.put("discntPremAfBeforeChange",discntPremAfBeforeChange);

	Log.info(getClass(), "保額=" + coverageVO.getCurrentPremium().getSumAssured() + ",高保額折讓=" + stdPremDiscnt + ",加費=" + coverageVO.getCurrentPremium().getExtraPremAf() + ",應繳保費=" + coverageVO.getCurrentPremium().getTotalPremAf() + ",折讓="
		+ coverageVO.getCurrentPremium().getDiscntPremAf() + ",下期標準保費=" + coverageVO.getNextPremium().getStdPremAf() + ",下期應繳保費=" + coverageVO.getNextPremium().getTotalPremAf());
	System.out.println("保額=" + coverageVO.getCurrentPremium().getSumAssured() + ",高保額折讓=" + stdPremDiscnt + ",加費=" + coverageVO.getCurrentPremium().getExtraPremAf() + ",應繳保費=" + coverageVO.getCurrentPremium().getTotalPremAf() + ",折讓="
		+ coverageVO.getCurrentPremium().getDiscntPremAf() + ",下期標準保費=" + coverageVO.getNextPremium().getStdPremAf() + ",下期應繳保費=" + coverageVO.getNextPremium().getTotalPremAf() + ",groosPremBf=" + totalPremBf);
		org.hibernate.Session s = com.ebao.foundation.module.db.hibernate.HibernateSession3.currentSession();
		s.flush();
		coverageVO = coverageService.load(itemId);
	// IR 344740 align with PD interface, using total prem to calcualate
	// instead of gross prem & discnt rate
	BigDecimal totalPremAf = BizRounder.round(coverageVO.getCurrentPremium().getTotalPremAf().multiply(proportion), BizRounder.RND_TYPE_PREMIUM); // 變更後保費
		BigDecimal diffPrem = totalPremAf.subtract(totalPremBf);
			
	Log.info(getClass(), "重算後" + "保額=" + coverageVO.getCurrentPremium().getSumAssured() + ",高保額折讓=" + stdPremDiscnt + ",加費=" + coverageVO.getCurrentPremium().getExtraPremAf() + ",應繳保費=" + coverageVO.getCurrentPremium().getTotalPremAf() + ",折讓="
		+ coverageVO.getCurrentPremium().getDiscntPremAf() + ",下期標準保費=" + coverageVO.getNextPremium().getStdPremAf() + ",下期應繳保費=" + coverageVO.getNextPremium().getTotalPremAf());
	System.out.println("重算後" + "保額=" + coverageVO.getCurrentPremium().getSumAssured() + ",高保額折讓=" + stdPremDiscnt + ",加費=" + coverageVO.getCurrentPremium().getExtraPremAf() + ",應繳保費=" + coverageVO.getCurrentPremium().getTotalPremAf() + ",折讓="
		+ coverageVO.getCurrentPremium().getDiscntPremAf() + ",下期標準保費=" + coverageVO.getNextPremium().getStdPremAf() + ",下期應繳保費=" + coverageVO.getNextPremium().getTotalPremAf() + ",groosPremAf=" + totalPremAf + ",diffPrem=" + diffPrem);
		// #322328 因畫面顯示使用NextExtraPrem，故將計算後結果setNextExtraPrem
		for (ExtraPremVO extraVo : coverageVO.getExtraPrems()) {
			for (UwExtraLoadingVO uwExtraLoadingVO : returnList) {					
		// update at 20191216 for IR357739
		// 儅保項有兩個extraPrem時，需要增加一個extraType的判斷條件，否則可能會重複增加加費
				if (extraVo.getItemId().equals(uwExtraLoadingVO.getItemId()) && extraVo.getExtraType().equals(uwExtraLoadingVO.getExtraType())) {
					extraVo.setNextExtraPrem(uwExtraLoadingVO.getExtraPrem());
				}
			}
		}

		Long largeSaPremDiscnt  = CoverageSP.getNbDisnctPrem(coverageVO.getItemId());
		if (largeSaPremDiscnt!=null & largeSaPremDiscnt.intValue()>0) {
			//coverageVO.getNextPremium().setStdPremAf(coverageVO.getNextPremium().getStdPremAf().add(BigDecimal.valueOf(largeSaPremDiscnt.longValue())));
			output.put("largeSaDiscntPrem",largeSaPremDiscnt);
		}

		output.put("coverageList", coverageVO); // 險種列表
	
		
		output.put("healthExtra", diffPrem); // 至前端;
		output.put("occptExtra", diffPrem); // 至前端;

		List<Map<String, Object>> uwExtraPremList = this.getDisplayUwxtraLoadingList(returnList, reqFrom, itemIdStr);

		output.put("uwExtraPremList", uwExtraPremList); // 至前端;
		output.put(KEY_SUCCESS, ROLLBACK);

	}

	@SuppressWarnings({ "unchecked" })
	private void savePOSUwExtraPrem(Map<String, Object> in, Map<String, Object> output) throws Exception {
		Long policyId = NumericUtils.parseLong(in.get("policyId"));
		Long underwriteId = NumericUtils.parseLong(in.get("underwriteId"));
		Long changeId = NumericUtils.parseLong(in.get("changeId"));
		Long policyChgId = NumericUtils.parseLong(in.get("policyChgId"));
		String reqFrom = (String) in.get("reqFrom");
		
		if (policyId != null && underwriteId != null) {

			String type = (String) in.get(KEY_TYPE);

			String uwSourceType = uwPolicyDS.getUwSourceType(underwriteId);

			if (UPDATE.equals(type)) {
				Long msgId = null;
				Long docId = null;

				Map<String, Object> submitData = (Map<String, Object>) in.get(KEY_DATA);
				String itemIdStr = (String) submitData.get("itemIdStr");
				Long partyId = NumericUtils.parseLong(submitData.get("partyId"));
				
				Long insuredId = NumericUtils.parseLong(submitData.get("insuredId"));
				PolicyVO policyVO = policyService.load(policyId);
				InsuredVO insuredVO = policyVO.gitInsured(insuredId);

				Collection<UwProductVO> uwProducts = uwPolicyDS.findUwProductEntitis(underwriteId);

				Collection<UwExtraLoadingVO> allOldExtra = uwPolicyDS.findUwExtraLoadingEntitis(underwriteId);

				// 取出DB中的msgId,docId 正常情況所有資料msgId,docId應一致
				for (UwExtraLoadingVO oldExtra : allOldExtra) {
					if (oldExtra.getMsgId() != null) {
						msgId = oldExtra.getMsgId();
						docId = oldExtra.getDocumentId();
						break;
					}
				}

				// 過濾此被保險人險種資料
				CollectionUtils.filter(allOldExtra, new BeanPredicate("insuredId", PredicateUtils.equalPredicate(partyId)));

				// 同步頁面弱體加費資料至DB
				Map<String, List<UwExtraLoadingVO>> extraTypeBindExtraLoading = NBUtils.toMapList(allOldExtra, "extraType");
				Map<String, UwProductVO> itemIdBindUwProduct = NBUtils.toMap(uwProducts, "itemId");

				List<UwExtraLoadingVO> oldHealthExtra = extraTypeBindExtraLoading.get(CodeCst.EXTRA_TYPE__HEALTH_EXTRA);
				List<UwExtraLoadingVO> newHealthExtra = UwExtraPremHelper.getHealthExtraPremList(in, uwProducts);
				/* 需設定年化加費金額 ,另若為5-人工加費，需以加費金額與繳別系數取得年化加費金額 */
				for (UwExtraLoadingVO uwExtraLoadingVO : newHealthExtra) {
		    uwActionHelper.calcExtraPrem(uwExtraLoadingVO, itemIdBindUwProduct.get(uwExtraLoadingVO.getItemId().toString()), in);
				}

				String strForPOS = reqFrom + "," + itemIdStr;// POS處理時需要使用的數據
				this.syncPOSExtraPremByExtraType(newHealthExtra, oldHealthExtra, strForPOS);// reqFrom判斷是否為POS，若是則僅刪除本次修改的保項未被選擇的加費記錄

				// 同步頁面職業加費資料至DB
				List<UwExtraLoadingVO> oldJobExtra = extraTypeBindExtraLoading.get(CodeCst.EXTRA_TYPE__OCCUPATION_EXTRA);
				List<UwExtraLoadingVO> newJobExtra = UwExtraPremHelper.getJobExtraPremList(in, uwProducts);
				/* 需設定年化加費金額 ,另若為5-人工加費，需以加費金額與繳別系數取得年化加費金額 */
				for (UwExtraLoadingVO uwExtraLoadingVO : newJobExtra) {
		    uwActionHelper.calcExtraPrem(uwExtraLoadingVO, itemIdBindUwProduct.get(uwExtraLoadingVO.getItemId().toString()), in);
				}
				this.syncPOSExtraPremByExtraType(newJobExtra, oldJobExtra, strForPOS);// reqFrom判斷是否為POS，若是則僅刪除本次修改的保項未被選擇的加費記錄

				// POS-----需更新insured.standLife() 弱加 emValue > 0
				// 或有職加表示此被保險人為次標準體
				String isStandLife = CodeCst.YES_NO__YES;
				
				for (UwExtraLoadingVO extraVO : newHealthExtra) {
					if(extraVO.getEmValue() != null) {
						isStandLife = CodeCst.YES_NO__NO;
						break;
					}
				}
				for (UwExtraLoadingVO extraVO : newJobExtra) {
					isStandLife = CodeCst.YES_NO__NO;
					break;
				}
				
				// update T_INSURED_LIST.STAND_LIFE
				insuredVO.setStandLifeIndi(isStandLife);
				insuredService.saveAfterNB(insuredVO, changeId, policyChgId);

				org.hibernate.Session s = com.ebao.foundation.module.db.hibernate.HibernateSession3.currentSession();
				s.flush();

				// 險種加費
				Collection<UwExtraLoadingVO> allNewExtra = uwPolicyDS.findUwExtraLoadingEntitis(underwriteId);
				Date validDate = DateUtils.toDate((String) in.get("validDate"));
				Long currentUser = AppContext.getCurrentUser().getUserId();
				CommonDumper.srcToLogByRecord(changeId, policyChgId, "T_CONTRACT_MASTER", policyId, CommonDumper.OPER_TYPE_UPDATE, currentUser);
				CommonDumper.srcToLogByRecord(changeId, policyChgId, "T_CONTRACT_PRODUCT", Long.parseLong(itemIdStr), CommonDumper.OPER_TYPE_UPDATE, currentUser);

				// 同步數據到POS
				syncDataToCS(underwriteId, policyId, changeId, policyChgId, Long.parseLong(itemIdStr));
				// 重算保費
				recalcSA(new ArrayList<UwExtraLoadingVO>(allNewExtra), changeId, policyChgId, validDate, in);

				List<Map<String, Object>> uwExtraPremList = this.getDisplayUwxtraLoadingList(allNewExtra, reqFrom, itemIdStr);
				output.put("uwExtraPremList", uwExtraPremList); // 至前端;
				s.flush();

				Object flag = submitData.get("isSaveProposalMsg");
				boolean isSaveProposalMsg = false;
				if (flag != null && "true".equals(flag.toString())) {
					isSaveProposalMsg = true; //判斷暫存或提交(暫存不作ruleResult新增)
				}

				if (uwExtraPremList.size() > 0) {
					// PCR423186 DEV424002 BC405-VLW/VLS EM50(含)以下次標免收加費金額(POS傳統)
					//加費金額為0不須產生「ExtraPrem.Warning1：請簽回『新契約內容變更同意書』」訊息。
					if(uwExtraPremList.get(0).get("extraPrem")!=null
							&& !uwExtraPremList.get(0).get("extraPrem").toString().equals("0")){
					// ================================================
		    updateRuleResultApprovalStatus(policyId, underwriteId, changeId, msgId, docId, isSaveProposalMsg, uwSourceType);
					// ================================================
					}
				}
				output.put(KEY_SUCCESS, SUCCESS);
			}
		}
	}

	private void syncToNBExtraPrem(Long underwriteId, Long policyId, Collection<UwExtraLoadingVO> allNewExtra) {

		List<ExtraPremVO> extraPremVOS = extraPremService.findByPolicyId(policyId);
		Map<String, ExtraPremVO> keyBindExtraPremList = new HashMap<String, ExtraPremVO>();

		String keyFmt = "%1$s-%2$s";//item_id-extra_type
		for (ExtraPremVO extraPremVO : extraPremVOS) {
	    keyBindExtraPremList.put(String.format(keyFmt, extraPremVO.getItemId(), extraPremVO.getExtraType()), extraPremVO);
		}

		for (UwExtraLoadingVO uwExtraPremVO : allNewExtra) {
			String key = String.format(keyFmt, uwExtraPremVO.getItemId(), uwExtraPremVO.getExtraType());
			
			if(uwExtraPremVO.getItemId() != null) {
				
				Coverage coverage = coverageDao.load(uwExtraPremVO.getItemId());
				
				if(coverage == null) {
					uwPolicyDS.removeUwExtraLoading(uwExtraPremVO);
					
				} else {
					ExtraPremVO extraPremVO = keyBindExtraPremList.get(key);
					if (extraPremVO != null) {
						// 更新
						extraPremVOS.remove(extraPremVO);
					} else {
						// 新增
						extraPremVO = new ExtraPremVO();
					}

					// 資料同步
					extraPremVO.setPolicyId(uwExtraPremVO.getPolicyId());
					extraPremVO.setItemId(uwExtraPremVO.getItemId());
					extraPremVO.setExtraType(uwExtraPremVO.getExtraType());
					extraPremVO.setStartDate(uwExtraPremVO.getStartDate());
					extraPremVO.setEndDate(uwExtraPremVO.getEndDate());
					extraPremVO.setExtraArith(uwExtraPremVO.getExtraArith());
					extraPremVO.setExtraPrem(uwExtraPremVO.getExtraPrem());
					extraPremVO.setExtraPremAn(uwExtraPremVO.getExtraPremAn());
					extraPremVO.setNextExtraPrem(uwExtraPremVO.getExtraPrem());
					extraPremVO.setNextExtraPremAn(uwExtraPremVO.getExtraPremAn());
					extraPremVO.setExtraPara(uwExtraPremVO.getExtraPara());
					extraPremVO.setEmValue(uwExtraPremVO.getEmValue());
					extraPremVO.setInsuredId(uwExtraPremVO.getInsuredId());
					extraPremVO.setReCalcIndi(uwExtraPremVO.getReCalcIndi());
					extraPremVO.setReason(uwExtraPremVO.getReason());
					extraPremVO.setEmCi(uwExtraPremVO.getEmCi());
					extraPremVO.setEmLife(uwExtraPremVO.getEmLife());
					extraPremVO.setEmTpd(uwExtraPremVO.getEmTpd());

					extraPremService.save(extraPremVO);
				}
			}
			
		}

		// 刪除
		for (ExtraPremVO extraPremVO : extraPremVOS) {
			extraPremService.remove(extraPremVO.getListId());
		}

	}

    /**
     * 重算保費
     * <p>
     * Description :
     * </p>
     * <p>
     * Created By : tgl151
     * </p>
     * <p>
     * Create Time : Jul 28, 2016
     * </p>
     * 
	* @param healthExtraPremList
	* @param jobExtraPremList
	* @param chgId
	* @param policyChgId
	* @param in 
	*/
	private void recalcSA(List<UwExtraLoadingVO> returnList, Long chgId, Long policyChgId, Date validDate, Map<String, Object> in) {
		Set<Long> itemIdList = new HashSet<Long>();
		if (returnList != null && returnList.size() > 0) {
			for (int i = 0; i < returnList.size(); i++) {
				UwExtraLoadingVO vo = returnList.get(i);
				itemIdList.add(vo.getItemId());
			}
		}

		// 豁免約
		Long underwriteId = NumericUtils.parseLong(in.get("underwriteId"));
		Map<String, Object> submitData = (Map<String, Object>) in.get(KEY_DATA);
		String itemIdStr = (String) submitData.get("itemIdStr");
	Collection<UwProductVO> uwProducts = uwPolicyDS.findUwProductEntitis(underwriteId);
	Map<String, UwProductVO> itemIdBindUwProduct = NBUtils.toMap(uwProducts, "itemId");

	Map<String, List<UwProductVO>> waiverBindProducts = NBUtils.toMapList(uwProducts, "waiver");
		List<UwProductVO> waiverProducts = waiverBindProducts.get("Y");
		Long waiverId = null;
		Long waiverMaster = null;
		if (waiverProducts != null && waiverProducts.size() > 0) {
			Long itemId = NumericUtils.parseLong(submitData.get("itemIdStr"));
			for (UwProductVO waiverProduct : waiverProducts) {
				if (waiverProduct.getMasterId().longValue() == itemId) {
					waiverId = waiverProduct.getItemId();
					waiverMaster = itemId;
					break;
				}
			}
		}

		for (Long itemId : itemIdList) {
			if(itemId.equals(Long.valueOf(itemIdStr)) || (waiverId != null && itemId.equals(waiverMaster))) {
			csPremiumRecalculateService.recalcBenefitInBeforeOrAfterBlock(itemId, chgId, policyChgId, Cst.BLOCK_BEFORE);
			// 生成csPrem記錄
			TCsPremDelegate.removeByItemIdPolicyChangeId(itemId, policyChgId);

			CoverageVO coverage = coverageService.load(itemId);
			
				boolean ilp = coverageService.isILPBenefit(itemId);
			
				////投資型加費 扣再coi上 不需再加費作業計算退補
				if (!ilp) {
				
					Date dueDate = coverage.getCoverageExtend().getDueDate();
				
					csPremDS.createBenefitProRataPremiumCollection(chgId, policyChgId, itemId, validDate, dueDate);
				}
							
			}			
			// 豁免約
			if (waiverId != null && itemId.equals(waiverMaster)) {
		CommonDumper.srcToLogByRecord(chgId, policyChgId, "T_CONTRACT_PRODUCT", waiverId, CommonDumper.OPER_TYPE_UPDATE, AppContext.getCurrentUser().getUserId());
		csPremDS.createBenefitProRataPremiumCollection(chgId, policyChgId, waiverId, validDate, coverageService.load(waiverId).getCoverageExtend().getDueDate());
			}

		}
	}

	/**
     * <p>
     * Description : 以em value對應出弱體加費的加費率
     * </p>
     * <p>
     * Created By : simon.huang
     * </p>
     * <p>
     * Create Time : Jul 14, 2016
     * </p>
     * 
	* @param in
	* @param output
	* @throws Exception
	*/
	public void findExtraPremRate(Map<String, Object> in, Map<String, Object> output) throws Exception {

		// Long policyId = NumericUtils.parseLong(in.get("policyId"));
		Long underwriteId = NumericUtils.parseLong(in.get("underwriteId"));
		
		UwPolicyVO uwPolicyVO = uwPolicyDS.findUwPolicy(underwriteId);
		Long itemId = null;
		if (UnbCst.WAIVER.equals(in.get("itemId"))) {

			Collection<UwProductVO> uwProducts = uwPolicyDS.findUwProductEntitis(underwriteId);

			if (uwProducts.size() > 0) {
				for (UwProductVO uwProductVO : uwProducts) {
					if (CodeCst.YES_NO__YES.equals(uwProductVO.getWaiver())) {
						itemId = uwProductVO.getItemId();
						break;
					}
				}
			}

		} else {
			itemId = NumericUtils.parseLong(in.get("itemId"));
		}

		String emValue = (String) in.get("emValue");
		Long policyChgId=NumericUtils.parseLong(in.get("policyChgId"));
		Date csValidDate = DateUtils.toDate((String) in.get("csValidDate"));//變更生效日
		if (StringUtils.isNotBlank(emValue) && itemId != null) {
			// IR-333617 改用商品模組提供的接口取得加費比率
			BigDecimal assignRate = null;
			Collection<UwProductVO> uwProducts = uwPolicyDS.findUwProductEntitis(underwriteId);
			List<UwExtraLoadingVO> healthExtraPremList = UwExtraPremHelper.getHealthExtraPremList(in, uwProducts);
			if(healthExtraPremList.size() > 0) {
				UwExtraLoadingVO loadingVO = healthExtraPremList.get(0);
				if (CodeCst.UW_SOURCE_TYPE__NB.equals(uwPolicyVO.getUwSourceType())) {
					//IR-352277-新契約調整加費檔不落地接口查詢加費比率
		    assignRate = coverageDao.getCoverageExtraPremRate(itemId, new BigDecimal(emValue), loadingVO.getExtraType(), loadingVO.getExtraArith(), loadingVO.getStartDate(), loadingVO.getEndDate());
				} else {
					CoverageVO coverage = coverageService.load(itemId);
					boolean isIlp = coverageService.isILPBenefit(itemId);
					Date dueDate = isIlp? coverage.getCoverageExtend().getIlpDueDate() : coverage.getCoverageExtend().getDueDate();
					PolicyVO policyVO = policyService.load(coverage.getPolicyId());
		            String chargeType  = policyVO.gitMasterCoverage().getNextPremium().getPaymentFreq();//繳別
		            if (isIlp && CodeCst.CHARGE_MODE__SINGLE.equals(chargeType)){  //投資型&躉繳件
		                chargeType = CodeCst.CHARGE_MODE__MONTHLY;
		                dueDate = BizUtils.getNextDueDate(csValidDate, coverage.getInceptionDate(), chargeType, BizUtils.CALC_NEXT_WAY__AFTER_OR_ON);
		            }
		            
					assignRate = coverageDao.getCoverageExtraPremRate(itemId, new BigDecimal(emValue), loadingVO.getExtraType(), loadingVO.getExtraArith(), loadingVO.getStartDate(), loadingVO.getEndDate(), dueDate, csValidDate);
					//PCR423186 DEV424002 BC405-VLW/VLS EM50(含)以下次標免收加費金額(POS傳統)
		    		//次標準體免收次標保費(VLS(E)/VLW(E))，系統新增呼叫商品提供的公式計算接口判斷險種
		            //及型別且EM符合設定並計算基本保額小於或等於保險金最低比率*TIV_Input
					//修改於coverageDao.getCoverageExtraPremRate：PKG_LS_PM_CALC_POS_SV.F_CALC_EM_PREM_VALUE()
					log4j.info("POS getAssignRate : policyId=" +coverage.getPolicyId() + ", itemId=" + itemId + ", assignRate=" + assignRate + ", extraType=" +  loadingVO.getExtraType() 
							+ ", extraArith=" + loadingVO.getExtraArith() + ",startDate=" + loadingVO.getStartDate()+ ",endDate=" + loadingVO.getEndDate()+ ",dueDate=" +dueDate);
				
				}
			}
			if (assignRate != null) {
				output.put("assignRate", assignRate);
				output.put(KEY_SUCCESS, SUCCESS);
				return;
			}
		}

		output.put(KEY_SUCCESS, FAIL);
	}

	/**
	* 
	* IR-333617 改為使用商品提供的接口查詢，此方法不再使用
	* 
     * <p>
     * Description : 核保作業>險種加費作業(因與頁面ajax配合,若查詢policyvo時效上會有遲延,直接用sql
     * join查詢減少delay) <br/>
	* 依保單險種查詢em_value及加費率的設定表
	* </p>
     * <p>
     * Created By : simon.huang
     * </p>
     * <p>
     * Create Time : Jul 24, 2016
     * </p>
     * 
	* @param itemId 
	* @param emValue
	* @return
	* @throws GenericException
	*/
	private BigDecimal queryExtraPremRate(Long ItemId, BigDecimal emValue) {
		BigDecimal assignRate = null;
		//T商品使用assign_rate，K商品使用assign_amount
		// 相關問題單RTCID-154012 #4
		//RTCID-155465 #6-使用coverage層判斷為K系統還是T系統
		CoverageVO coverageVO = coverageService.load(ItemId);
		String originalSystem = productVersionService.load(coverageVO.getProductVersionId()).getOriginalSystem();
		if ("1".equals(originalSystem)) {
			/* 返回值1 为T */
			Map<Long, BigDecimal> assignRateMap = coverageDao.findCoverageExtraPremRate(ItemId);
			assignRate = assignRateMap.get(emValue.longValue());
		} else if ("2".equals(originalSystem)) {
			/* 返回值为2 则为K */
			Map<Long, BigDecimal> assignRateMap = coverageDao.findCoverageExtraPremAssignAmount(ItemId);
			assignRate = assignRateMap.get(emValue.longValue());
		}
		return assignRate;
	}

    private void syncPOSExtraPremByExtraType(List<UwExtraLoadingVO> newList, List<UwExtraLoadingVO> oldList, String strForPOS) {

		Map<String, UwExtraLoadingVO> itemIdBindExtraLoading = NBUtils.toMap(oldList, "itemId");
		
		for (UwExtraLoadingVO newVO : newList) {
			String itemId = String.valueOf(newVO.getItemId());
			CoverageVO coverage = coverageService.load(Long.parseLong(itemId));
			UwExtraLoadingVO oldVO = itemIdBindExtraLoading.remove(itemId);
			if (oldVO != null) {
				oldVO.setEmValue(newVO.getEmValue());
				oldVO.setExtraPara(newVO.getExtraPara());
				oldVO.setExtraPrem(newVO.getExtraPrem());
				oldVO.setExtraPremAn(newVO.getExtraPremAn());
				oldVO.setNextExtraPremAn(newVO.getExtraPremAn());
				oldVO.setExtraArith(newVO.getExtraArith());
				oldVO.setExtraType(newVO.getExtraType());
				oldVO.setExtraPeriodType(newVO.getExtraPeriodType());
				oldVO.setReason(newVO.getReason());
		// IR 219437 因為計算TCP 時會根據DUE DATE和T_EXTRA_PREM裡的START_DATE比對
		// 所以更新START_DATE壓再DUEDATE上面
		Date currentDueDate = BizUtils.getLastDueDate(newVO.getStartDate(), coverage.getInceptionDate(), coverage.getCurrentPremium().getPaymentFreq(), BizUtils.CALC_LAST_WAY__BEFORE_OR_ON);

				if (currentDueDate.before(newVO.getStartDate()) || currentDueDate.equals(newVO.getStartDate())) {
					oldVO.setStartDate(currentDueDate);
				} else {
					oldVO.setStartDate(newVO.getStartDate());
				}
				//oldVO.setStartDate(newVO.getStartDate());
				oldVO.setEndDate(newVO.getEndDate());
				oldVO.setUnderwriterId(newVO.getUnderwriterId());
				oldVO.setDuration(newVO.getDuration());
				uwPolicyDS.updateUwExtraLoading(oldVO);

				try {
					BeanUtils.copyProperties(newVO, oldVO);
				} catch (Exception e) {

				}
			} else {
		// IR 219437 因為計算TCP 時會根據DUE DATE和T_EXTRA_PREM裡的START_DATE比對
		// 所以更新START_DATE壓再DUEDATE上面
		Date currentDueDate = BizUtils.getLastDueDate(newVO.getStartDate(), coverage.getInceptionDate(), coverage.getCurrentPremium().getPaymentFreq(), BizUtils.CALC_LAST_WAY__BEFORE_OR_ON);

				if (currentDueDate.before(newVO.getStartDate()) || currentDueDate.equals(newVO.getStartDate())) {
					newVO.setStartDate(currentDueDate);
				} else {
					newVO.setStartDate(newVO.getStartDate());
				}
				uwPolicyDS.createUwExtraLoading(newVO);
			}
		}

		// 執行刪除(原資料庫加費資料未被設定)
		String[] dataForPOS = strForPOS.split(",");
		if (dataForPOS != null && dataForPOS.length > 1 && dataForPOS[0].equals("pos")) {
			// stringForPOS[0]判斷是否為POS，若是則做如下處理
			if (dataForPOS[1] != null && (!dataForPOS[1].trim().isEmpty())) {
				Long itemIdUpdating = Long.parseLong(dataForPOS[1]);
				for (UwExtraLoadingVO removeExtraLoading : itemIdBindExtraLoading.values()) {
					if (!removeExtraLoading.getItemId().equals(itemIdUpdating)) {
						// 僅刪除本次修改的保項的未被設定的加費資料
						continue;
					}
					uwPolicyDS.removeUwExtraLoading(removeExtraLoading);
				}
			}
		} else {
			for (UwExtraLoadingVO removeExtraLoading : itemIdBindExtraLoading.values()) {
				uwPolicyDS.removeUwExtraLoading(removeExtraLoading);
			}
		}

	}
	
	private void syncNBExtraPremByExtraType(List<UwExtraLoadingVO> newList, List<UwExtraLoadingVO> oldList) {

		Map<String, UwExtraLoadingVO> itemIdBindExtraLoading = new HashMap<String, UwExtraLoadingVO>();

		String keyFmt = "%1$s-%2$s";//item_id-extra_type
		for (UwExtraLoadingVO extraPremVO : oldList) {
	    itemIdBindExtraLoading.put(String.format(keyFmt, extraPremVO.getItemId(), extraPremVO.getExtraType()), extraPremVO);
		}

		for (UwExtraLoadingVO newVO : newList) {
			String key = String.format(keyFmt, newVO.getItemId(), newVO.getExtraType());
			
			UwExtraLoadingVO oldVO = itemIdBindExtraLoading.remove(key);
			if (oldVO != null) {
				oldVO.setEmValue(newVO.getEmValue());
				oldVO.setExtraPara(newVO.getExtraPara());
				oldVO.setExtraPrem(newVO.getExtraPrem());
				oldVO.setExtraPremAn(newVO.getExtraPremAn());
				oldVO.setNextExtraPremAn(newVO.getExtraPremAn());
				oldVO.setExtraArith(newVO.getExtraArith());
				oldVO.setExtraType(newVO.getExtraType());
				oldVO.setExtraPeriodType(newVO.getExtraPeriodType());
				oldVO.setReason(newVO.getReason());
				oldVO.setStartDate(newVO.getStartDate());
				oldVO.setEndDate(newVO.getEndDate());
				oldVO.setUnderwriterId(newVO.getUnderwriterId());
				oldVO.setDuration(newVO.getDuration());
				uwPolicyDS.updateUwExtraLoading(oldVO);
				try {
					BeanUtils.copyProperties(newVO, oldVO);
				} catch (Exception e) {
					log4j.error(e);
				}
			} else {
				uwPolicyDS.createUwExtraLoading(newVO);
			}
		}

		// 執行刪除(原資料庫加費資料未被設定)
		for (UwExtraLoadingVO removeExtraLoading : itemIdBindExtraLoading.values()) {
			uwPolicyDS.removeUwExtraLoading(removeExtraLoading);
		}

	}

	/**
     * <p>
     * Description : 前端頁面初始-已新增的加費資料
     * </p>
     * <p>
     * Created By : simon.huang
     * </p>
     * <p>
     * Create Time : Aug 5, 2016
     * </p>
     * 
	* @param uwExtraLoadingList
	* @return
	* @throws IllegalAccessException
	* @throws InvocationTargetException
	* @throws NoSuchMethodException
	*/
	@SuppressWarnings("unchecked")
    private List<Map<String, Object>> getDisplayUwxtraLoadingList(Collection<UwExtraLoadingVO> uwExtraLoadingList, String reqFrom, String itemId) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {

		List<Map<String, Object>> uwExtraPremList = new ArrayList<Map<String, Object>>();// 險種加費

		for (UwExtraLoadingVO uwExtraLoadingVO : uwExtraLoadingList) {
	    if (reqFrom != null && reqFrom.equals("pos") && !uwExtraLoadingVO.getItemId().toString().equals(itemId)) {
				continue;
			}
			Map<String, Object> extraLoadingMap = BeanUtils.describe(uwExtraLoadingVO);

			extraLoadingMap.put("duration", uwExtraLoadingVO.getDuration());

			/* 是否為人工調整加費金額 */
			String manualIndi = CodeCst.YES_NO__NO;
			if (CodeCst.ADD_ARITH__EXTRA_TYPE_MANUAL.equals(uwExtraLoadingVO.getExtraArith())) {
				manualIndi = CodeCst.YES_NO__YES;
			}
			extraLoadingMap.put("manualIndi", manualIndi);
			uwExtraPremList.add(extraLoadingMap);
		}
		
		return uwExtraPremList;
	}

	/**
     * <p>
     * Description : 產生"請簽回『新契約內容變更同意書』"照會訊息
     * </p>
     * <p>
     * Created By : simon.huang
     * </p>
     * <p>
     * Create Time : Aug 5, 2016
     * </p>
     * 
	* @param policyId
	* @param uwExclusionList
	* @throws Exception
	*/
	private ProposalRuleResultVO saveProposalRuleResultVO(Long policyId, Long underwriteId, Long changeId) throws Exception {

		ProposalRuleResultVO resultVO = null;

	    List<UwExtraLoadingVO> uwExtraPremList = new ArrayList<UwExtraLoadingVO>();// 保單核保加費
    	uwExtraPremList.addAll(uwPolicyDS.findUwExtraLoadingEntitis(underwriteId));
    	BigDecimal totalExtraPrem = BigDecimal.ZERO;

    	for (UwExtraLoadingVO uwExtraLoadingVO : uwExtraPremList) {
	    	if (uwExtraLoadingVO.getExtraPrem() != null)
				totalExtraPrem = totalExtraPrem.add(uwExtraLoadingVO.getExtraPrem());
        }

	    // PCR 423171 如果總加費金額 > 0 才出此檢荷
    	if (uwExtraPremList.size() > 0 && totalExtraPrem.compareTo(BigDecimal.ZERO) == 1) {
	        Date now = AppContext.getCurrentUserLocalTime();
	    /*
	     * 暫不作是否存在核保訊息的判斷,batch/online列印均產生一筆 (先保留此邏輯區塊,待user測試)
	     * List<ProposalRuleResultVO> historyResultList =
	     * proposalRuleResultService.findByPolicyIdAndRuleName(policyId,
	     * ProposalRuleMsg.PROPOSAL_RULE_MSG_EXCLUSION_LETTER); if
	     * (historyResultList.size() == 0) {}
			*/
			// PCR-420669 
			CoverageVO coverageVO = coverageService.findMasterCoverageByPolicyId(policyId);
			LifeProduct lifeProduct = lifeProductService.getProductByVersionId(coverageVO.getProductVersionId());
			// 2021/3/22 後不使用
			// PCR-419140 以要保書填寫日區份新舊規則,依險種區分訊息內容		
			PolicyVO policyVO = policyService.load(policyId);
			boolean isOpqV2 = validatorService.isOpqV2(policyId);
			resultVO = proposalRuleMsgService.genProposalRuleResultVO(ProposalRuleMsg.PROPOSAL_RULE_MSG_EXTRA_PREM_LETTER, policyVO.getApplyDate(), null, isOpqV2);
			
			if (resultVO == null) {
				// 下游固定只用 ExtraPrem.Warning1 產信函,所以要再轉一手把 Warning2或3改成Warning1
				if (lifeProduct != null && lifeProduct.isInvestLink()) {
					// 主約險種為投資型商品
					resultVO = proposalRuleMsgService.genExtraPremProposalRuleResultVO( policyVO.getApplyDate(), true, isOpqV2);
				} else {
					// 主約險種非投資型商品
					resultVO = proposalRuleMsgService.genExtraPremProposalRuleResultVO( policyVO.getApplyDate(), false, isOpqV2);
				}
			}
			resultVO.setPolicyId(policyId);
			resultVO.setChangeId(changeId);
			resultVO.setRuleStatus(CodeCst.PROPOSAL_RULE_STATUS__PENDING);
			resultVO.setRuleType(CodeCst.PROPOSAL_RULE_TYPE__UW_LETTER);

			if (changeId != null) {
				// 保全無需簽核
				resultVO.setUwDocumentApprovalStatus(CodeCst.APPROVAL_NO_NEED);
			} else {
				// 新契約為待簽核
				resultVO.setUwDocumentApprovalStatus(CodeCst.APPROVAL_WAITING);
			}

			proposalRuleResultService.save(resultVO);
		}

		return resultVO;
	}

	private void syncDataToCS(Long underwriteId, Long policyId, Long changeId, Long policyChgId, Long itemId) throws IllegalAccessException, InvocationTargetException {
		long curUserId = AppContext.getCurrentUser().getUserId();
		Collection<UwExtraLoadingVO> uwExtraLoadingList = uwPolicyDS.findUwExtraLoadingEntitis(underwriteId, itemId);// findUwExtraLoadingEntitis(underwriteId);
		List<ExtraPremVO> extraPremVOS = extraPremService.findByItemId(itemId);// findByPolicyId(policyId);

		ExtraPremVO tempExtraPremVO = voGenerator.extraPremNewInstance();
		// 標識extraPremVOS中元素是否存在於uwExtraLoadingVO中
		// 若存在就會更新，若不更新就是不存在，不存在就刪除該記錄 - RTCID-102518#22
		boolean[] updatedExtraPremVO = {};
		if (extraPremVOS != null) {
			updatedExtraPremVO = new boolean[extraPremVOS.size()];
			for (int i = 0; i < updatedExtraPremVO.length; i++) {
				updatedExtraPremVO[i] = false;
			}
		}
		// dispose update and create
		for (UwExtraLoadingVO uwExtraLoadingVO : uwExtraLoadingList) {
			boolean flag = true;
			if (extraPremVOS != null) {
				for (int i = 0; i < extraPremVOS.size(); i++) {
					ExtraPremVO extraPremVO = extraPremVOS.get(i);
					// 判斷加費類型是否一致，默認一個underwriteId一個保項一個加費類型僅一條記錄
					if (StringUtils.equals(extraPremVO.getExtraType(), uwExtraLoadingVO.getExtraType())) {
						// if
						// (extraPremVO.getItemId().toString().equals(uwExtraLoadingVO.getItemId().toString()))
						// {
						// exist in all ExtraPrem and UwExtraLoading,
						// dump ExtraPrem data to log table,then update
						BeanUtils.copyProperties(tempExtraPremVO, uwExtraLoadingVO);
						tempExtraPremVO.setListId(extraPremVO.getListId());
						extraPremService.save(tempExtraPremVO);
			CommonDumper.srcToLogByRecord(Long.valueOf(changeId), policyChgId, "T_EXTRA_PREM", extraPremVO.getListId(), CodeCst.DATA_OPER_TYPE__DUMP_OPER_UPD, Long.valueOf(curUserId));
						updatedExtraPremVO[i] = true;// 標識為更新過
						flag = false;
						break;
					}
				}
			}
			if (flag) {
				// exist in UwExtraLoading, not in ExtraPrem
				// dump ExtraPrem data to log table,then insert
				BeanUtils.copyProperties(tempExtraPremVO, uwExtraLoadingVO);
				tempExtraPremVO = extraPremService.save(tempExtraPremVO);
		CommonDumper.srcToLogByRecord(Long.valueOf(changeId), policyChgId, "T_EXTRA_PREM", tempExtraPremVO.getListId(), CodeCst.DATA_OPER_TYPE__DUMP_OPER_ADD, Long.valueOf(curUserId));
			}
		}
		// 若存在就會更新，若不更新就是不存在，不存在就刪除該記錄 - RTCID-102518#22
		for (int i = 0; i < updatedExtraPremVO.length; i++) {
			if (!updatedExtraPremVO[i]) {
				Long listId = extraPremVOS.get(i).getListId();
				if (TChangeIndexDelegate.hasChanged(changeId, policyChgId, "T_EXTRA_PREM", listId, CommonDumper.OPER_TYPE_INSERT)) {
					CommonDumper.logToSrcByRecord(changeId, policyChgId, "T_EXTRA_PREM", listId);
				} else {
					CommonDumper.logToSrcByRecord(changeId, policyChgId, "T_EXTRA_PREM", listId);
		    CommonDumper.srcToLogByRecord(changeId, policyChgId, "T_EXTRA_PREM", listId, CodeCst.DATA_OPER_TYPE__DUMP_OPER_DEL, Long.valueOf(curUserId));
		    // System.out.println("extraPremVOS.get(i).getListId(): " +
		    // extraPremVOS.get(i).getListId());
					extraPremService.remove(extraPremVOS.get(i).getListId());
				}

			}
		}
	}

	/**
     * <pre>
     * Description : 
	* 依「核保訊息狀態」+「照會發放狀態」+「簽核狀態」處理以下：
	* 是否有照會事項
	* ├–否 > 新增待簽核照會事項
	* ├–是 > 是否已簽核
	*  ├–否 >	(未簽核表示未發放) 更新msgId至被保險人的批註事項 
	*  ├–是 > 是否已產生document
	*   ├–是 > document是否已發放
	*    ├ online > 產生新的待簽核照會事項
	*    ├ batch  > 提示照會事項需取消，才可執行
	*   ├否 > 更新的照會事項為待簽核
	* </pre>
     * <p>
     * Created By : simon.huang
     * </p>
     * <p>
     * Create Time : Mar 2, 2017
     * </p>
     * 
	* @param policyId
	* @param underwriteId
	* @param changeId
	* @param insuredVO
	* @param msgId
	* @param docId
	* @throws Exception
	*/
    private Long updateRuleResultApprovalStatus(Long policyId, Long underwriteId, Long changeId, Long msgId, Long docId, boolean isSaveProposalMsg, String uwSourceType) throws Exception {

		//if (log4j.isDebugEnabled()) {
	log4j.info("policyId=" + policyId + ", underwriteId=" + underwriteId + ", changeId=" + changeId + ", msgId=" + msgId + ", docId=" + docId + ", isSaveProposalMsg=" + isSaveProposalMsg);
		//}

		ProposalRuleResultVO resultVO = null;
		if (msgId != null) {
			resultVO = proposalRuleResultService.load(msgId);
		} else {
	    // 找出待處理的核保訊息(FIX RTC
	    // 153221-因加費檔ALL移除後無法比對出加費檔中的msgId，但核保信息已存在，改用查詢待處理的核保訊息避免重覆新增)
	    List<ProposalRuleResultVO> ruleResultList = proposalRuleResultService.findAllPendingList(policyId, ProposalRuleMsg.PROPOSAL_RULE_MSG_EXTRA_PREM_LETTER, changeId, null);

			if (ruleResultList.size() > 0) {

				// 2017-06-08 Kate 當信函已列印(包含批次的已列印)則msgId，要重新產生
				String documentStatus = uwIssuesLetterService.getMasterDocStatus(ruleResultList.get(0).getListId());
				if (!CodeCst.DOCUMENT_STATUS__PRINTED.equals(documentStatus)) {
					resultVO = ruleResultList.get(0);
					msgId = resultVO.getListId();
				}
			}
		}

		/* 無核保訊息或核保訊息不為待處理，則新增一筆新的核保訊息 */
		if (resultVO == null || resultVO.getRuleStatus() != CodeCst.PROPOSAL_RULE_STATUS__PENDING) {

	        if (isSaveProposalMsg) {
		        // 新增核保訊息，並更新回uwExclusionVO
		        msgId = null;
		        resultVO = this.saveProposalRuleResultVO(policyId, underwriteId, changeId);
		        if (resultVO != null) {
		        	msgId = resultVO.getListId();
		        }
		        docId = null;
	        } else {
		        msgId = null;
		        docId = null;
	        }
	        // 更新msgId及documentId
	        uwPolicyDS.updateUwExtraPremMsgIdDocumentId(underwriteId, msgId, docId);

		} else {

			if (changeId != null) {
				// 更新msgId及documentId
				uwPolicyDS.updateUwExtraPremMsgIdDocumentId(underwriteId, msgId, docId);

			} else {
				//新契約-需執行簽核判斷
				String approvalStatus = resultVO.getUwDocumentApprovalStatus();

				/* 待簽核,表示未發放照會不作處理  */
				if (UwDocumentApprovalStatus.APPROVAL_WAITING.equals(approvalStatus)) {
					// 更新msgId及清除documentId
					uwPolicyDS.updateUwExtraPremMsgIdDocumentId(underwriteId, msgId, docId);
				} else if (!UwDocumentApprovalStatus.APPROVAL_TRANSFER.equals(approvalStatus)) {

					// 已簽核，需確認照會狀態
					String documentStatus = null;
					if (docId != null) {
						documentStatus = uwIssuesLetterService.getMasterDocStatus(msgId);

			if (CodeCst.DOCUMENT_STATUS__TO_BE_PRINTED.equals(documentStatus) && UwDocumentApprovalStatus.APPROVAL_AGREE.equals(approvalStatus)) {
							// 未列印照會且已簽核，不可修改，由前端檢核
							// WSLetterUnit unit =
							// uwActionHelper.getFmtUnb0381Letter(policyId,
							// underwriteId, insuredVO.getListId());
			    // commonLetterService.updateLetterContent(docId,
			    // unit);
							throw new RTException(MsgCst.MSG_1262018);
						} else {

			    if (CodeCst.DOCUMENT_STATUS__ABORTED.equals(documentStatus) || CodeCst.DOCUMENT_STATUS__DELETED.equals(documentStatus)) {
								//信函已註銷，使用原核保訊息

				    resultVO.setUwDocumentApprovalStatus(UwDocumentApprovalStatus.APPROVAL_WAITING);
				    proposalRuleResultService.save(resultVO);
				    msgId = resultVO.getListId();
				    docId = null;
			    } else if (isSaveProposalMsg) { // 提交
				    msgId = null;
				    resultVO = this.saveProposalRuleResultVO(policyId, underwriteId, changeId);
				    if (resultVO != null) {
				    	msgId = resultVO.getListId();
				    }
				    docId = null;
			    } else { // 暫存
				    msgId = null; // 需重新提交後產生新的核保訊息
				    docId = null; // 需重新提交後產生新的核保訊息
			    }
			}
		    }
		    // docId is null=未發放照會，需將狀態更新為待簽核
		    else {
			    resultVO.setUwDocumentApprovalStatus(UwDocumentApprovalStatus.APPROVAL_WAITING);
			    proposalRuleResultService.save(resultVO);
			    msgId = resultVO.getListId();
			    docId = null;
		    }

					// 更新msgId及清除documentId
					uwPolicyDS.updateUwExtraPremMsgIdDocumentId(underwriteId, msgId, docId);
				}
			}

		}
                return msgId;
	}

	/**
     * <p>
     * Description : 試算加費金額
     * </p>
     * <p>
     * Created By : simon.huang
     * </p>
     * <p>
     * Create Time : Jul 14, 2016
     * </p>
     * 
	* @param in
	* @param output
	* @throws Exception
	*/

	public void calcExtraPrem(Map<String, Object> in, Map<String, Object> output) throws Exception {

		String reqFrom = (String) in.get("reqFrom");
		boolean isPOS = (reqFrom != null && reqFrom.equals("pos"));

		if (isPOS) {
			calcPOSExtraPrem(in, output);
		} else {
			calcNBExtraPrem(in, output);
		}
	}

	/**
     * <pre>
     * Description : 加費資料新增/修改/刪除  
	* 上傳資料格式 : var submitData = {
	* 			insuredId : $insuredId.val(),
	* 			'A' : {extraPeriodType=2, extraPremList=[{emValue=20, itemId=5424, extraPara, extraPrem}], duration=20, reason=test2},
	* 			'B' : {reason=test3, extraPara=44444.00, extraPrem}
	* };
	* </pre>
     * <p>
     * Created By : simon.huang
     * </p>
     * <p>
     * Create Time : Jun 27, 2016
     * </p>
     * 
	* @param in
	* @param output
	* @throws Exception
	*/
	public void saveUwExtraPrem(Map<String, Object> in, Map<String, Object> output) throws Exception {

		String reqFrom = (String) in.get("reqFrom");
		boolean isPOS = (reqFrom != null && reqFrom.equals("pos"));

		if (isPOS) {
			savePOSUwExtraPrem(in, output);
		} else {
			saveNBUwExtraPrem(in, output);

			// 在途保單檢核要保書填寫日，是否寫入夜批計算調整保費
			Long policyId = NumericUtils.parseLong(in.get("policyId"));
			CalculatorPremSP.clearPremiumAdjustValue(policyId);
		}
	}

	/**
     * <p>
     * Description : <b>***目前使用policyDao.load() 會因關連資料表查詢花費較多的處理時間,待優化為SQL查詢所需欄位
     * 減少SQL查詢次數 ***</b> //新契約加費頁面為該被保險人所有險種，包含多筆豁免險，調整加費計算流程為：
     * 計算一般險種加費->寫入UW加費檔
     * ->同步回T表加費檔->計算被豁免保項保費(含加費)->更新豁免險保額->重新計算豁免險保費->計算豁免險加費->
     * 寫入UW加費檔->同步回T表加費檔
	* </p>
     * <p>
     * Created By : Simon.Huang
     * </p>
     * <p>
     * Create Time : 2018年1月14日
     * </p>
     * 
	* @param in
	* @param output
	* @throws Exception
	*/
	@SuppressWarnings({ "unchecked" })
	private void calcNBExtraPrem(Map<String, Object> in, Map<String, Object> output) throws Exception {

		String reqFrom = (String) in.get("reqFrom");
		Long policyId = NumericUtils.parseLong(in.get("policyId"));
		Long underwriteId = NumericUtils.parseLong(in.get("underwriteId"));
		String type = (String) in.get(KEY_TYPE);

		Map<String, Object> submitData = (Map<String, Object>) in.get(KEY_DATA);
		Long insuredId = NumericUtils.parseLong(submitData.get("insuredId"));
		
		Date start = new Date();
		PolicyVO policyVO = policyService.load(policyId);
		log4j.info("加費效能--保單載入花費=" + (int) ((new Date().getTime() - start.getTime())));
		start = new Date();
		InsuredVO insuredVO = policyVO.gitInsured(insuredId);
		List<CoverageVO> insuredCoveragesAll = policyVO.gitCoveragesByInsured(insuredId);

		Collection<UwProductVO> uwAllProducts = uwPolicyDS.findUwProductEntitis(underwriteId);
		Map<String, UwProductVO> itemIdBindUwProduct = NBUtils.toMap(uwAllProducts, "itemId");

		Collection<UwExtraLoadingVO> allExtra = uwPolicyDS.findUwExtraLoadingEntitis(underwriteId);

		List<UwExtraLoadingVO> returnList = new ArrayList<UwExtraLoadingVO>();
		List<UwExtraLoadingVO> waiverExtraList = new ArrayList<UwExtraLoadingVO>();
		List<UwExtraLoadingVO> oldItemExtra = new ArrayList<UwExtraLoadingVO>();
		List<CoverageVO> insuredCoverages = new ArrayList<CoverageVO>();
		
		for(CoverageVO coverageVO : insuredCoveragesAll) {
			if(coverageVO.getRiskStatus() == CodeCst.LIABILITY_STATUS__NON_VALIDATE) {
				insuredCoverages.add(coverageVO);
			}
		}
		log4j.info("加費效能--加費載入花費=" + (int) ((new Date().getTime() - start.getTime())));
		for (CoverageVO coverageVO : insuredCoverages) {
			start = new Date();
			for (UwExtraLoadingVO oldExtra : allExtra) {
				if (oldExtra.getItemId().longValue() == coverageVO.getItemId().longValue()) {
					oldItemExtra.add(oldExtra);
				}
			}

	    if ("healthByItem".equals(type) || "health".equals(type) || "all".equals(type) || "update".equals(type) || "job".equals(type)) {

				List<UwProductVO> uwProducts = new ArrayList<UwProductVO>();
				if(itemIdBindUwProduct.get(coverageVO.getItemId().toString()) != null) {
					uwProducts.add(itemIdBindUwProduct.get(coverageVO.getItemId().toString()));	
				}
				if (uwProducts.size() > 0) {
					List<UwExtraLoadingVO> healthExtraPremList = UwExtraPremHelper.getHealthExtraPremList(in, uwProducts);

					//不作豁免險的加費計算(因UwProduct豁免險的保額為舊值)，改由T表的保費計算後再取豁免險的加費金額同步回UW加費檔
					if (CodeCst.YES_NO__NO.equals(coverageVO.getWaiverExt().getWaiver())) {
						for (UwExtraLoadingVO uwExtraLoadingVO : healthExtraPremList) {
							String itemId = String.valueOf(uwExtraLoadingVO.getItemId());
							UwProductVO uwProductVO = itemIdBindUwProduct.get(itemId);
							uwActionHelper.calcExtraPrem(uwExtraLoadingVO, uwProductVO, in);
						}
						
						//是否有豁免險資料
						for(UwProductVO uwProductVO : uwAllProducts) {
			    if (CodeCst.YES_NO__YES.equals(uwProductVO.getWaiver()) && coverageVO.getItemId().equals(uwProductVO.getMasterId())) {
								for(UwExtraLoadingVO waiverExtraPremVO : allExtra) {
									
				    if (uwProductVO.getItemId().equals(waiverExtraPremVO.getItemId()) && CodeCst.EXTRA_TYPE__HEALTH_EXTRA.equals(waiverExtraPremVO.getExtraType())) {
										
										if(waiverExtraList.contains(waiverExtraPremVO) == false) {
											waiverExtraList.add(waiverExtraPremVO);
										}
									}
								}
							}
						}
					} else {
						waiverExtraList.addAll(healthExtraPremList);
					}

					returnList.addAll(healthExtraPremList);
				}
			}

	    if ("healthByItem".equals(type) || "health".equals(type) || "all".equals(type) || "update".equals(type) || "job".equals(type)) {
				
				Log.info(getClass(), "job 職業加費重新計算");
				List<UwProductVO> uwProducts = new ArrayList<UwProductVO>();
				if(itemIdBindUwProduct.get(coverageVO.getItemId().toString()) != null) {
					uwProducts.add(itemIdBindUwProduct.get(coverageVO.getItemId().toString()));	
				}
				if (uwProducts.size() > 0) {
					List<UwExtraLoadingVO> jobExtraPremList = UwExtraPremHelper.getJobExtraPremList(in, uwProducts);

					//不作豁免險的加費計算(因UwProduct豁免險的保額為舊值)，改由T表的保費計算後再取豁免險的加費金額同步回UW加費檔
					if (CodeCst.YES_NO__NO.equals(coverageVO.getWaiverExt().getWaiver())) {
						for (UwExtraLoadingVO uwExtraLoadingVO : jobExtraPremList) {
							String itemId = String.valueOf(uwExtraLoadingVO.getItemId());
							UwProductVO uwProductVO = itemIdBindUwProduct.get(itemId);
							uwActionHelper.calcExtraPrem(uwExtraLoadingVO, uwProductVO, in);
			    log4j.info("加費計算 item=" + itemId + ",加費金額=" + uwExtraLoadingVO.getExtraPrem());
						}
						//是否有豁免險資料
						for(UwProductVO uwProductVO : uwAllProducts) {
			    if (CodeCst.YES_NO__YES.equals(uwProductVO.getWaiver()) && coverageVO.getItemId().equals(uwProductVO.getMasterId())) {
								for(UwExtraLoadingVO waiverExtraPremVO : allExtra) {
									
				    if (uwProductVO.getItemId().equals(waiverExtraPremVO.getItemId()) && CodeCst.EXTRA_TYPE__OCCUPATION_EXTRA.equals(waiverExtraPremVO.getExtraType())) {
										
										if(waiverExtraList.contains(waiverExtraPremVO) == false) {
											waiverExtraList.add(waiverExtraPremVO);
										}
									}
								}
							}
						}
					} else {
						waiverExtraList.addAll(jobExtraPremList);
					}

					returnList.addAll(jobExtraPremList);
				}
			}
			
			log4j.info("加費效能--保項處理花費=" + (int) ((new Date().getTime() - start.getTime())));
		}
		start = new Date();
		// 同步頁面職加加費資料至DB
		this.syncNBExtraPremByExtraType(returnList, oldItemExtra);

		log4j.info("加費效能--syncNBExtraPremByExtraType花費=" + (int) ((new Date().getTime() - start.getTime())));
		// 新契約-----需更新insured.standLife() 弱加 emValue > 0
		// 或有職加表示此被保險人為次標準體
		String isStandLife = CodeCst.YES_NO__YES;
		for (UwExtraLoadingVO extraVO : returnList) {
			if (CodeCst.EXTRA_TYPE__HEALTH_EXTRA.equals(extraVO.getExtraType())) {
				if (extraVO.getEmValue() != null && extraVO.getEmValue().intValue() > 0) {
					isStandLife = CodeCst.YES_NO__NO;
					break;
				}
			} else if (CodeCst.EXTRA_TYPE__OCCUPATION_EXTRA.equals(extraVO.getExtraType())) {
				isStandLife = CodeCst.YES_NO__NO;
				break;
			}
		}
		start = new Date();
		insuredVO.setStandLifeIndi(isStandLife);
		insuredService.saveNew(insuredVO);

		UwLifeInsuredVO uwInsuredVO = uwPolicyDS.findUwLifeInsuredByListId(underwriteId, insuredId);
		uwInsuredVO.setStandLife(isStandLife);
		uwPolicyDS.updateUwLifeInsured(uwInsuredVO);

		log4j.info("加費效能--更新standlife花費=" + (int) ((new Date().getTime() - start.getTime())));
		HibernateSession3.currentSession().flush();
		Collection<UwExtraLoadingVO> allNewExtra = uwPolicyDS.findUwExtraLoadingEntitis(underwriteId);
		// 加費作業提交將t_uw_extra_prem同步至t_extra_prem，並執行保費重新計算
		syncToNBExtraPrem(underwriteId, policyId, allNewExtra);
		
	/*
	 * 執行保單所有險種保費計算(含豁免險),2018/01/29
	 * 調整為by保項計算,因RTC-218261豁免險查無保費率造成其它保項未計算加費金額
	 */
		for(CoverageVO coverageVO : insuredCoverages) {
			if(coverageVO.isWaiver() == false) {
				start = new Date();
				try {
					 CalculatorPremSP.calcPremium(coverageVO.getItemId());
				} catch (Exception e) {
					log4j.warn(e.getMessage());
					//會有年齡不符合保費率設定無法計算保費的case，catch exception由RMS作年齡校驗
					//like:投保年齡未達QTR(20年期,K版)最低年齡限18歲。請取消該附約。
				}
				log4j.info("加費效能--calcPremium花費=" + (int) ((new Date().getTime() - start.getTime())));
			}
		}
		
		for(CoverageVO coverageVO : insuredCoverages) {
			
			if(coverageVO.isWaiver()) {
				
				start = new Date();
				try {
					 CalculatorPremSP.calcWaiverPremium(coverageVO.getPolicyId(), coverageVO.getItemId());
				} catch (Exception e) {
					log4j.warn(e.getMessage());
					//會有年齡不符合保費率設定無法計算保費的case，catch exception由RMS作年齡校驗
					//like:投保年齡未達QTR(20年期,K版)最低年齡限18歲。請取消該附約。
				}
				
				log4j.info("加費效能--calcWaiverPremium花費=" + (int) ((new Date().getTime() - start.getTime())));
			} else {
				for(CoverageVO waierCoverageVO : policyVO.getCoverages()) {
		    if (waierCoverageVO.isWaiver() && coverageVO.getItemId().equals(waierCoverageVO.getMasterId())) {
						start = new Date();
						try {
							 CalculatorPremSP.calcWaiverPremium(waierCoverageVO.getPolicyId(), waierCoverageVO.getItemId());
						} catch (Exception e) {
							log4j.warn(e.getMessage());
							//會有年齡不符合保費率設定無法計算保費的case，catch exception由RMS作年齡校驗
							//like:投保年齡未達QTR(20年期,K版)最低年齡限18歲。請取消該附約。
						}
						
						log4j.info("加費效能--calcWaiverPremium花費=" + (int) ((new Date().getTime() - start.getTime())));
					}
				}
			}
			
		}
		
//		try {
//			start = new Date();
//			coverageCalculatorService.calcPolicyPremium(policyId);
	// log4j.info("加費效能--calcPolicyPremium花費=" + (int) ((new
	// Date().getTime() - start.getTime())));
//		} catch (Exception e) {
//			//會有年齡不符合保費率設定無法計算保費的case，catch exception由RMS作年齡校驗
//			//like:投保年齡未達QTR(20年期,K版)最低年齡限18歲。請取消該附約。
//		}

		HibernateSession3.detachSession();
		//取得所有豁免險重算後的保額
		start = new Date();
		List<Coverage> waiverCoverages = coverageDao.findByPolicyId(policyId);
		log4j.info("加費效能--load waiverCoverages花費=" + (int) ((new Date().getTime() - start.getTime())));
		List<Map<String, Object>> waiverCoverageList = new ArrayList<Map<String, Object>>();
		for (Coverage coverage : waiverCoverages) {
			start = new Date();
			Map<String, Object> row = new HashMap<String, Object>();
			//險種費用相關
			CoveragePremium coveragePremium = coverage.getCurrentPremium();

			Map<String, Object> premiumMap = BeanUtils.describe(coveragePremium);
			row.putAll(premiumMap);

			BigDecimal stdPremWithoutAnyDiscnt = coveragePremium.getStdPremAf(); //期繳保費(無高保額折讓)
			Long stdPremDiscnt = new Long(0); //高保額折讓(期繳)
			
			Long policyChgId = null;
			int bizCategory = CodeCst.BIZ_CATEGORY_UNB;
			Date validateDate = null;
			Date dueDate = null;
			if( !CodeCst.YES_NO__YES.equals(coverage.getWaiverExt().getWaiver())){
		stdPremWithoutAnyDiscnt = coverageService.getStdPremWithoutAnyDiscnt(coverage.getItemId(), coverage.getProductId().longValue(), bizCategory, dueDate, validateDate, policyChgId);
				stdPremDiscnt = CoverageSP.getNbDisnctPrem(coverage.getItemId());
			}
		
			String isCOIProduct = CodeCst.YES_NO__NO;
			/* 判斷是否有coi的收費規則 */
			if (coverageService.isCOIProduct(coverage.getProductId().longValue(), coverage.getProductVersionId())) {
				isCOIProduct = CodeCst.YES_NO__YES;
			}
			row.put("stdPremWithoutAnyDiscnt", stdPremWithoutAnyDiscnt);
			row.put("stdPremDiscnt", stdPremDiscnt);
			row.put("itemId", coverage.getItemId());
			row.put("isCOIProduct", isCOIProduct);
			
			waiverCoverageList.add(row);
			
	    log4j.info("保額=" + coverage.getCurrentPremium().getSumAssured() + ",標準保費=" + stdPremWithoutAnyDiscnt + ",高保額折讓=" + stdPremDiscnt + ",加費=" + coverage.getCurrentPremium().getExtraPremAf() + ",應繳保費="
		    + coverage.getCurrentPremium().getTotalPremAf() + ",折讓=" + coverage.getCurrentPremium().getDiscntPremAf());
			
			log4j.info("加費效能--display waiverCoverages 花費=" + (int) ((new Date().getTime() - start.getTime())));
		}
		start = new Date();
		// 設定保險成本
		uwActionHelper.setupCOI(in, waiverCoverageList);
		
		output.put("waiverCoverageList", waiverCoverageList); // 至前端;

		for (Coverage coverage : waiverCoverages) {
			
			if(CodeCst.YES_NO__YES.equals(coverage.getWaiverExt().getWaiver())) {
				List<ExtraPremVO> extraPremVOS = extraPremService.findByItemId(coverage.getItemId());

				for (ExtraPremVO extraPremVO : extraPremVOS) {
					for (UwExtraLoadingVO waiverExtraVO : waiverExtraList) {
			if (extraPremVO.getExtraType().equals(waiverExtraVO.getExtraType()) && extraPremVO.getItemId().equals(waiverExtraVO.getItemId())) {
			    log4j.info(waiverExtraVO.getUwListId() + " 同步UW表加費金額=" + extraPremVO.getExtraPrem() + ",加費金額(年)=" + extraPremVO.getExtraPremAn());
							waiverExtraVO.setExtraPrem(extraPremVO.getExtraPrem());
							waiverExtraVO.setExtraPremAn(extraPremVO.getExtraPremAn());
							uwPolicyDS.updateUwExtraLoading(waiverExtraVO);
							
							returnList.add(waiverExtraVO);
						}
					}
				}
			}
		}

		HibernateSession3.detachSession();

		List<Map<String, Object>> uwExtraPremList = this.getDisplayUwxtraLoadingList(returnList, reqFrom, "");
	
		log4j.info("加費效能--finish 花費=" + (int) ((new Date().getTime() - start.getTime())));
		output.put("uwExtraPremList", uwExtraPremList); // 至前端;
		output.put(KEY_SUCCESS, ROLLBACK);

	}

	@SuppressWarnings({ "unchecked" })
	private void saveNBUwExtraPrem(Map<String, Object> in, Map<String, Object> output) throws Exception {

		Long policyId = NumericUtils.parseLong(in.get("policyId"));
		Long underwriteId = NumericUtils.parseLong(in.get("underwriteId"));

		if (policyId != null && underwriteId != null) {

			String type = (String) in.get(KEY_TYPE);

			if (UPDATE.equals(type)) {
				Long msgId = null;
				Long docId = null;

				Map<String, Object> submitData = (Map<String, Object>) in.get(KEY_DATA);

				Collection<UwExtraLoadingVO> allOldExtra = uwPolicyDS.findUwExtraLoadingEntitis(underwriteId);

				// 取出DB中的msgId,docId 正常情況所有資料msgId,docId應一致
				for (UwExtraLoadingVO oldExtra : allOldExtra) {
					if (oldExtra.getMsgId() != null) {
						msgId = oldExtra.getMsgId();
						docId = oldExtra.getDocumentId();
						break;
					}
				}

				calcNBExtraPrem(in, output);

				// 險種加費
				Collection<UwExtraLoadingVO> allNewExtra = uwPolicyDS.findUwExtraLoadingEntitis(underwriteId);

				//在途保單清除調整保費
				if (allNewExtra.size() > 0) {
					CalculatorPremSP.clearPremiumAdjustValue(policyId);
				}

				//由預覽傳入的存檔不作RMS校驗(Simon)
				Boolean preview = (Boolean) in.get("preview");

				if (preview == null) {

                            Object flag = submitData.get("isSaveProposalMsg");
                            boolean isSaveProposalMsg = false;
					if (flag != null && "true".equals(flag.toString())) {
						isSaveProposalMsg = true; //判斷暫存或提交(暫存不作ruleResult新增)
					}

					if (allNewExtra.size() == 0) {
						// 是否關閉核保訊息？？
						//保單無加費資料且未發放需系統關閉未發放的核保訊息
						if (msgId != null) {
							String documentStatus = uwIssuesLetterService.getMasterDocStatus(msgId);
							if (documentStatus == null) {
								ProposalRuleResultVO resultVO = proposalRuleResultService.load(msgId);
								resultVO.setRuleStatus(CodeCst.PROPOSAL_RULE_STATUS__SYSTEMCLOSED);
								proposalRuleResultService.save(resultVO);
							}
						}
					} else {
                                            // ================================================
                                            // 儲存T_PROPOSAL_RULE_RESULT後將新的msgId回傳給後面的流程使用
                                            msgId = updateRuleResultApprovalStatus(policyId, underwriteId, null, msgId, docId, isSaveProposalMsg, CodeCst.UW_SOURCE_TYPE__NB);
                                            // ================================================
					}
				}

				// 加費作業提交將t_uw_extra_prem同步至t_extra_prem，並執行保費重新計算
				syncToNBExtraPrem(underwriteId, policyId, allNewExtra);

				//由預覽傳入的存檔不作RMS校驗(Simon)，且有加費資料才作檢核
				if (preview == null) {
					if (allNewExtra.size() > 0) {
						try {
							HibernateSession3.detachSession();
							List<ProposalRuleResultVO> ruleResultList = proposalService.checkAddFeeRMSRules(policyId);
							proposalService.checkAddFeeOtherRules(allNewExtra, ruleResultList);
							writeRuleResultForNB(ruleResultList, policyId, msgId);//暫存或提交都必須要出ExtraPrem.Warning2或ExtraPrem.Warning3或ExtraPrem.Warning4
							if (ruleResultList.size() > 0) {
						
								StringBuffer warning = new StringBuffer();
								for (ProposalRuleResultVO ruleResult : ruleResultList) {
									if( !warning.toString().contains(ruleResult.getViolatedDesc()) ) {
										warning.append(ruleResult.getViolatedDesc());
										warning.append("\n");
									}
								}
								
								output.put(KEY_ERROR_MSG, warning.toString());
							}
						} catch (Exception e) {
							output.put(KEY_ERROR_MSG, ExceptionInfoUtils.getExceptionMsg(e));
						}
					}
				}

				//2018/01/16 by Simon-RTC-220547-調整有RMS訊息但仍可執行存檔
				/* 重算首期應繳保費 */
				policyCalculatorService.calcInforcePremium(policyId);
				output.put(KEY_SUCCESS, SUCCESS);
			}
		}
	}

	public void reloadInsuredCoverage(Map<String, Object> in, Map<String, Object> output) throws Exception {
		Long policyId = NumericUtils.parseLong(in.get("policyId"));
		Long underwriteId = NumericUtils.parseLong(in.get("underwriteId"));

		Map<String, Object> submitData = (Map<String, Object>) in.get(KEY_DATA);
		Long insuredId = NumericUtils.parseLong(submitData.get("insuredId"));
		if (policyId != null && underwriteId != null) {

			PolicyVO policyInfo = policyService.load(policyId);
			
			List<Map<String, Object>> coverageList = uwActionHelper.getDisplayCoverageList(policyInfo, in);
			Collection<UwExtraLoadingVO> uwExtraLoadingList = new ArrayList<UwExtraLoadingVO>();
			Iterator<Map<String, Object>> iterator = coverageList.iterator();
			while (iterator.hasNext()) {
				Map<String, Object> coverage = (Map<String, Object>) iterator.next();
				Long coverageInsuredId = (Long) coverage.get("insuredId");
				Long coverageItemId = (Long) coverage.get("itemId");
				if (insuredId.equals(coverageInsuredId) == false) {
					iterator.remove();
				} else {
					uwExtraLoadingList.addAll(uwPolicyDS.findUwExtraLoadingEntitis(underwriteId, coverageItemId));
				}
			}

			// 設定保險成本
			uwActionHelper.setupCOI(in, coverageList);
			// 險種加費
	    List<Map<String, Object>> uwExtraPremList = this.getDisplayUwxtraLoadingList(uwExtraLoadingList, CodeCst.UW_SOURCE_TYPE__NB, "");

			output.put("coverageList", coverageList); // 險種列表
			output.put("uwExtraPremList", uwExtraPremList); // 險種加費

			output.put(KEY_SUCCESS, SUCCESS);
			return;
		}
	}
	
	// IR #322328 檢核是否VUL01-1商品
	public void validProductVUL01(Map<String, Object> in, Map<String, Object> output) throws Exception {
		output.put("isVUL01Product", Boolean.FALSE);
		String itemId = ObjectUtils.toString(in.get("itemId"), "");
		CoverageVO vo = coverageService.load(Long.valueOf(itemId));
		if (vo != null) {
			output.put("isVUL01Product", StringUtils.equals(PRODUCT_VUL01, vo.getOldProductCode()));
		}
	}

    private void writeRuleResultForNB(List<ProposalRuleResultVO> ruleResultList, Long policyId, Long msgId) {
        if (CollectionUtils.isNotEmpty(ruleResultList)) {
            List<ProposalRuleResultVO> existVos = proposalRuleResultService.findByPolicyIdAndRuleStatus(policyId, ProposalRuleStatus.PROPOSAL_RULE_STATUS__PENDING);
            List<ProposalRuleResultVO> t_existVos = new ArrayList<ProposalRuleResultVO>();
            CollectionUtils.select(existVos, PredicateUtils.anyPredicate(Arrays.asList(
                    new BeanPropertyValueEqualsPredicate("ruleName", ProposalRuleMsg.PROPOSAL_RULE_MSG_EXTRA_PREM_LETTER_6),
                    new BeanPropertyValueEqualsPredicate("ruleName", ProposalRuleMsg.PROPOSAL_RULE_MSG_EXTRA_PREM_LETTER_5),
                    new BeanPropertyValueEqualsPredicate("ruleName", ProposalRuleMsg.PROPOSAL_RULE_MSG_EXTRA_PREM_LETTER_4)
            )), t_existVos);

            Set<String> uniStrings = new HashSet<String>();
            for (ProposalRuleResultVO proposalRuleResultVO : ruleResultList) {
                if (ArrayUtils.contains(new String[]{ProposalRuleMsg.PROPOSAL_RULE_MSG_EXTRA_PREM_LETTER_6, ProposalRuleMsg.PROPOSAL_RULE_RMS_MSG_EXTRA_PREM_LETTER_5, ProposalRuleMsg.PROPOSAL_RULE_RMS_MSG_EXTRA_PREM_LETTER_4}, proposalRuleResultVO.getRuleName())) {
                    if (!uniStrings.contains(proposalRuleResultVO.getViolatedDesc())) {
                        // 不重複新增相同訊息
                        uniStrings.add(proposalRuleResultVO.getViolatedDesc());

                        String ruleName = "";
                        if (StringUtils.equals(ProposalRuleMsg.PROPOSAL_RULE_RMS_MSG_EXTRA_PREM_LETTER_5, proposalRuleResultVO.getRuleName())) {
                            ruleName = ProposalRuleMsg.PROPOSAL_RULE_MSG_EXTRA_PREM_LETTER_5;
                        }
                        if (StringUtils.equals(ProposalRuleMsg.PROPOSAL_RULE_RMS_MSG_EXTRA_PREM_LETTER_4, proposalRuleResultVO.getRuleName())) {
                            ruleName = ProposalRuleMsg.PROPOSAL_RULE_MSG_EXTRA_PREM_LETTER_4;
                        }

                        if (StringUtils.isNotBlank(ruleName)) {
                            proposalRuleResultVO.setRuleName(ruleName);
                        }
                        proposalRuleResultVO.setRuleType(ProposalRuleType.PROPOSAL_RULE_TYPE__OPQ_NOT_SYS_CLOSE_MSG);
                        proposalRuleResultVO.setPrintable(YesNo.YES_NO__YES);
                        proposalRuleResultVO.setNotes(null);
                        proposalRuleResultVO.setRuleLetterCode(null);

                        // 如果存在相同RuleName與ViolatedDesc且待處理就不新增
                        boolean ruleExists = CollectionUtils.exists(t_existVos, PredicateUtils.allPredicate(Arrays.asList(
                                new BeanPropertyValueEqualsPredicate("ruleName", proposalRuleResultVO.getRuleName()),
                                new BeanPropertyValueEqualsPredicate("violatedDesc", proposalRuleResultVO.getViolatedDesc())
                        )));
                        if (!ruleExists) {
                            proposalRuleResultService.save(proposalRuleResultVO);
                        }
                    }
                }
            }
            // 如果有產生RMS檢核表示不能進行加費，所以將ExtraPrem.Warning1關閉
            if (!uniStrings.isEmpty()) {
                if (!ObjectUtils.equals(msgId, null)) {
                    String documentStatus = uwIssuesLetterService.getMasterDocStatus(msgId);
                    if (ObjectUtils.equals(documentStatus, null)) {
                        ProposalRuleResultVO resultVO = proposalRuleResultService.load(msgId);
                        resultVO.setRuleStatus(CodeCst.PROPOSAL_RULE_STATUS__SYSTEMCLOSED);
                        proposalRuleResultService.save(resultVO);
                    }
                }
            }
        }
    }

}
