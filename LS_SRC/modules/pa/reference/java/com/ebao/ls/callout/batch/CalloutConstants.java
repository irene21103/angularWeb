package com.ebao.ls.callout.batch;

public class CalloutConstants {
	public static final Long PATH = 3060010001L;

	public static final Long ZeroEightZeroOne = 3060010002L;

	public static final Long ZeroEightZeroTwo = 3060010003L;

	public static final Long ZeroEightZeroThree = 3060010004L;

	public static final Long ZeroEightZeroFour = 3060010005L;

	public static final Long ZeroEightZeroFive = 3060010006L;

	public static final Long ZeroEightZeroSixOne = 3060010007L;

	public static final Long ZeroEightZeroSixTwo = 3060010008L;

	public static final Long ZeroEightZeroSeven = 3060010009L;

	public static final Long ZeroEightZeroEight = 3060010010L;

	public static final Long ZeroEightZeroNine = 3060010011L;

	public static final Long ZeroEightZeroTen = 3060010012L;

	public static final Long ZeroEightZeroX = 3060010013L;

	public static final Long POSOne = 3060010014L;

	public static final Long POSTwo = 3060010015L;

	public static final Long POSThree = 3060010016L;

	public static final Long POSFour = 3060010017L;

	public static final Long POSFive = 3060010018L;

	public static final Long DEPT080 = 3060010019L;

	public static final String CalloutType_1 = "1";

	public static final String CalloutType_2 = "2";

	public static final String CalloutType_3 = "3";

	public static final String CalloutType_4 = "4";

	public static final String CalloutType_5 = "5";

	public static final String CalloutType_6 = "6";

	public static final String CalloutType_7 = "7";

	public static final String CalloutType_8 = "8";

	public static final String CalloutType_9 = "9";

	public static final String CalloutReason_1 = "1";

	public static final String CalloutReason_2 = "2";

	public static final String CalloutReason_3 = "3";

	public static final String CalloutReason_4 = "4";

	public static final String CalloutTarget_1 = "1";		//要保

	public static final String CalloutTarget_2 = "2";

	public static final String CalloutTarget_3 = "3";		//法代
	
	public static final String CalloutTarget_4 = "4";

	public static final String CalloutTarget_5 = "5";

	public static final String CalloutTarget_6 = "6";

	public static final String CalloutTarget_7 = "7";

	public static final String CalloutTarget_8 = "8";

	public static final String CalloutTarget_9 = "9";

	public static final String CalloutTarget_10 = "10";

	public static final String CalloutTarget_11 = "11";

	public static final String CalloutTarget_12 = "12";

	public static final String CalloutTarget_13 = "13";
	
	public static final String CalloutResult_type0 = "0";

	public static final String CalloutResult_type1 = "1";

	public static final String CalloutResult_type2 = "2";

	public static final String CalloutResult_type3 = "3";

	public static final String CalloutResult_type4 = "4";

	public static final String CalloutTargetTypeholder = "1";

	public static final String CalloutTargetTypeInsuer = "2";

	public static final String CalloutTargetTypelower = "3";
	
	public static final String LIABILITY_STATUS__INFORCE = "1";
	
	public static final String LIABILITY_STATUS__NON_VALIDATE = "0";
}
