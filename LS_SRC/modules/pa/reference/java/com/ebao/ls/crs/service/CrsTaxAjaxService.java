package com.ebao.ls.crs.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.ebao.foundation.module.db.hibernate.HibernateSession3;
import com.ebao.ls.crs.constant.CRSTinBizSource;
import com.ebao.ls.crs.vo.CRSControlManLogVO;
import com.ebao.ls.crs.vo.CRSEntityControlManListLogVO;
import com.ebao.ls.crs.vo.CRSEntityLogVO;
import com.ebao.ls.crs.vo.CRSIndividualLogVO;

import com.ebao.ls.crs.vo.CRSPartyLogVO;
import com.ebao.ls.crs.vo.CRSTaxIDNumberLogVO;
import com.ebao.ls.pa.nb.util.NBUtils;
import com.ebao.ls.pa.nb.util.NumericUtils;
import com.ebao.ls.pa.pub.MsgCst;
import com.ebao.ls.pa.pub.NbTaskCst;
import com.ebao.pub.util.json.EbaoStringUtils;

/**
 * <p>Title: CRS 聲明書-PCR_264035&263803_CRS專案</p>
 * <p>Description: 自我證明-稅籍編號</p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: TGL Co., Ltd.</p>
 * <p>Create Time: 2018/11/08</p> 
 * @author 
 * <p>Update Time: 2018/11/08</p>
 * <p>Updater: simon.huang</p>
 * <p>Update Comments: </p>
 */
public class CrsTaxAjaxService extends CrsAjaxService {

	public static Logger logger = Logger.getLogger(CrsTaxAjaxService.class);

    /**
     * <p>Description : 自我證明-稅籍編號 </p>
     * <p>Created By : Simon.Huang</p>
     * <p>Create Time : 2018年11月9日</p>
     * @param in
     * @param output
     * @throws Exception
     */
    public void save(Map<String, Object> in, Map<String, Object> output) throws Exception {

        Long partyLogId = NumericUtils.parseLong(in.get("partyLogId"));
        Long complementTaskId = NumericUtils.parseLong(in.get("complementTaskId"));

        if (partyLogId != null) {
            String type = (String) in.get(KEY_TYPE);
            try {
                if (UPDATE.equals(type)) {
                    CRSTaxIDNumberLogVO taxLogVO = null;
                    Map<String, Object> submitData = (Map<String, Object>) in.get(KEY_DATA);
                    Long entityId = NumericUtils.parseLong(submitData.get(KEY_ENTITY_ID));

                    Map<String, Object> tax = (Map<String, Object>) submitData.get("tax");

                    CRSPartyLogVO partyLogVO = crsPartyLogService.load(partyLogId);

                    Long complementBizId = this.clearComplementTaskBeforeData(complementTaskId, tax, partyLogVO);

                    if (entityId != null) {
                        taxLogVO = crsTaxLogService.load(entityId);
                        EbaoStringUtils.setProperties(taxLogVO, tax);
                        taxLogVO.setCrsPartyLog(partyLogVO);

                        crsTaxLogService.save(taxLogVO, true);
                    } else {
                        taxLogVO = new CRSTaxIDNumberLogVO();
                        EbaoStringUtils.setProperties(taxLogVO, tax);
                        taxLogVO.setCrsPartyLog(partyLogVO);

                        Long bizId = this.getBizId(tax, complementBizId);
                        taxLogVO.setBizId(bizId);

                        taxLogVO.setCreateOrder(crsTaxLogService.getTaxMaxCreateOrder(partyLogId, taxLogVO.getCopy()));

                        taxLogVO = crsTaxLogService.save(taxLogVO, true);
                    }

                    output.put("result", taxLogVO); //返回 結果至前端;
                    assignLogDataListId(in, taxLogVO.getLogId());

                    if (taxLogVO.getResCountry() != null) {
                        Map<String, Object> codeTableMap = new HashMap<>();
                        List<CRSTaxIDNumberLogVO> taxLogVOList = new ArrayList<>();
                        taxLogVOList.add(taxLogVO);
                        Map<String, Map<String, String>> nationality = super.getNationalityCode(taxLogVOList);
                        codeTableMap.put("nationality", nationality);
                        //列表用code table
                        output.put("codeTable", codeTableMap);
                    }
                    output.put(KEY_SUCCESS, SUCCESS);
                    return;
                } else if (DELETE.equals(type)) {

                    String deleteId = (String) in.get(KEY_DELETE_ID);
                    String listId[] = deleteId.split(",");
                    for (int i = 0; i < listId.length; i++) {
                        Long entityId = NumericUtils.parseLong(listId[i]);
                        if (entityId != null) {
                            crsTaxLogService.remove(entityId);
                            assignLogDataListId(in, entityId);
                        }
                    }
                    output.put(KEY_SUCCESS, SUCCESS);
                    return;
                }
            } catch (Exception e) {
                throw e;
            }
        }
        output.put(KEY_SUCCESS, FAIL);
        output.put(KEY_ERROR_MSG, NBUtils.getTWMsg(MsgCst.MSG_1250566));
    }

	private Long clearComplementTaskBeforeData(Long complementTaskId, Map<String, Object> tax, CRSPartyLogVO partyLogVO) {
		Long bizId = null;
		
		if (complementTaskId != null) {
			Long partyLogId = partyLogVO.getLogId();
			Long changeId = NumericUtils.parseLong(tax.get("changeId"));//自我證明個人、自我證明法人、具控制權人changeId
			String bizSource = (String)tax.get("bizSource");//自我證明個人、自我證明法人、具控制權人
			String taskName = "";
			
			if(CRSTinBizSource.BIZ_SOURCE_PERSON.equals(bizSource)) { 
				taskName = NbTaskCst.TASK_QUEUE_CRS_PERSON;
			} else if(CRSTinBizSource.BIZ_SOURCE_COMPANY.equals(bizSource)) {
				taskName = NbTaskCst.TASK_QUEUE_CRS_COMPANY;
			} 
			
			if(StringUtils.isNotBlank(taskName)
					&& 0 == nbTaskService.countTaskQueue(complementTaskId, taskName)){
				//清除補全前寫入稅藉編號資料
				List<CRSTaxIDNumberLogVO> taxLogList = crsTaxLogService.findByPartyLogIdAndChangeId(partyLogId, 
								bizSource, 
								changeId);
				for(CRSTaxIDNumberLogVO taxLog : taxLogList) {
					crsTaxLogService.remove(taxLog.getEntityId());
				}
				
				//清除補全前寫入主檔資料
				if(CRSTinBizSource.BIZ_SOURCE_PERSON.equals(bizSource)) { 
				
					List<CRSIndividualLogVO> personLogList = crsIndividualLogService.findByPartyLogId(partyLogId, false);

					if (personLogList.size() > 0) {
						CRSIndividualLogVO oldPersonLogVO = personLogList.get(0);
						
						CRSIndividualLogVO personLogVO = new CRSIndividualLogVO();
						
						personLogVO.setLogId(oldPersonLogVO.getLogId());
						personLogVO.setChangeId(oldPersonLogVO.getChangeId());
						personLogVO.setCrsPartyLog(partyLogVO);
						bizId = oldPersonLogVO.getLogId();
						crsIndividualLogService.save(personLogVO, true);
					}
				} else if(CRSTinBizSource.BIZ_SOURCE_COMPANY.equals(bizSource)) {
					
					List<CRSEntityLogVO> companyLogList = crsEntityLogService.findByPartyLogId(partyLogId, false);
					if (companyLogList.size() > 0) {
						CRSEntityLogVO oldCompanyLogVO = companyLogList.get(0);
						
						CRSEntityLogVO companyLogVO = new CRSEntityLogVO();
						
						companyLogVO.setLogId(oldCompanyLogVO.getLogId());
						companyLogVO.setChangeId(oldCompanyLogVO.getChangeId());
						companyLogVO.setCrsPartyLog(partyLogVO);
						companyLogVO.setCrsEntityControlManListLog(new ArrayList<CRSEntityControlManListLogVO>());
						bizId = oldCompanyLogVO.getLogId();
						crsEntityLogService.save(companyLogVO, true);
					}
				} 
			
				nbTaskService.saveTaskQueue(complementTaskId, taskName, "");
			}
		}
		
		return bizId;
	}

	private Long getBizId(Map<String, Object> tax, Long complementBizId) {
		Long bizId = NumericUtils.parseLong(tax.get("bizId"));//自我證明個人、自我證明法人、具控制權人id

		if (complementBizId != null) {
			bizId = complementBizId;
		}

		if (bizId == null) {
			bizId = generateBizId(tax);
		}

		return bizId;
	}
	
	private Long generateBizId(Map<String, Object> tax) {
		String bizSource = (String) tax.get("bizSource");
		Long changeId = NumericUtils.parseLong(tax.get("changeId"));
		Long crsPartyLogId = NumericUtils.parseLong(tax.get("crsPartyLogId"));

		CRSPartyLogVO partyLogVO = crsPartyLogService.load(crsPartyLogId);
		try {

			if (CRSTinBizSource.BIZ_SOURCE_PERSON.equals(bizSource)) {
				CRSIndividualLogVO bizobj = new CRSIndividualLogVO();
				bizobj.setChangeId(changeId);
				bizobj.setCrsPartyLog(partyLogVO);
				bizobj = crsIndividualLogService.save(bizobj, true);
				return bizobj.getLogId();
			} else if (CRSTinBizSource.BIZ_SOURCE_COMPANY.equals(bizSource)) {
				CRSEntityLogVO bizobj = new CRSEntityLogVO();
				bizobj.setChangeId(changeId);
				bizobj.setCrsPartyLog(partyLogVO);
				bizobj = crsEntityLogService.save(bizobj, true);
				return bizobj.getLogId();
			} else if (CRSTinBizSource.BIZ_SOURCE_CTRL_MAN.equals(bizSource)) {
				CRSControlManLogVO bizobj = new CRSControlManLogVO();
				bizobj.setChangeId(changeId);
				bizobj.setCrsPartyLog(partyLogVO);
				bizobj = crsCtrlManLogService.save(bizobj, true);
				return bizobj.getLogId();
			}
		} finally {
			HibernateSession3.currentSession().flush();
		}
		return null;
	}

			
}
