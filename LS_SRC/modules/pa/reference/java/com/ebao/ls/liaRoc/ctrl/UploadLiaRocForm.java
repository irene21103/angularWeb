package com.ebao.ls.liaRoc.ctrl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.Factory;
import org.apache.commons.collections.ListUtils;

import com.ebao.pub.framework.GenericForm;

/**
 * <h1>單筆上傳公會通報</h1><p>
 * @since getDate()<p>
 * @author Amy Hung
 * <p>Update Time: </p>
 * <p>Updater: </p>
 * <p>Update Comments: </p>
*/
@SuppressWarnings("serial")
public class UploadLiaRocForm extends GenericForm {

    String actionType;

    String policyCode;

    String liarocUploadType;
    
    String tabType;
    
    String retMsg;

    List<UploadLiaRocVO> uploadLiaRocVO; // = new ArrayList<UploadLiaRocVO>();

    @SuppressWarnings("unchecked")
    public List<UploadLiaRocVO> getUploadLiaRocVO() {
        if (uploadLiaRocVO == null) {
            uploadLiaRocVO = ListUtils.lazyList(
                            new ArrayList<UploadLiaRocVO>(), new Factory() {
                                @Override
                                public Object create() {
                                    return new UploadLiaRocVO();
                                }
                            });
        }
        return uploadLiaRocVO;
    }

    public void setUploadLiaRocVO(List<UploadLiaRocVO> uploadLiaRocVO) {
        this.uploadLiaRocVO = uploadLiaRocVO;
    }

    public String getActionType() {
        return actionType;
    }

    public void setActionType(String actionType) {
        this.actionType = actionType;
    }

    public String getPolicyCode() {
        return policyCode;
    }

    public void setPolicyCode(String policyCode) {
        this.policyCode = policyCode;
    }

    public String getLiarocUploadType() {
        return liarocUploadType;
    }

    public void setLiarocUploadType(String liarocUploadType) {
        this.liarocUploadType = liarocUploadType;
    }

	public String getTabType() {
		return tabType;
	}

	public void setTabType(String tabType) {
		this.tabType = tabType;
	}

	public String getRetMsg() {
		return retMsg;
	}

	public void setRetMsg(String retMsg) {
		this.retMsg = retMsg;
	}

}
