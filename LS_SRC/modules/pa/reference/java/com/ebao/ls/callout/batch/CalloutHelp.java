package com.ebao.ls.callout.batch;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.ApplicationContext;

import com.ebao.foundation.common.context.AppUserContext;
import com.ebao.foundation.module.util.i18n.CodeTable;
import com.ebao.ls.callout.batch.task.CalloutUtils;
import com.ebao.ls.callout.bs.CalloutTransService;
import com.ebao.ls.cs.commonflow.data.bo.AlterationItem;
import com.ebao.ls.cs.commonflow.ds.ApplicationService;
import com.ebao.ls.cs.util.POSUtils;
import com.ebao.ls.pub.CodeCst;
import com.ebao.ls.pub.cst.CalloutTargetType;
import com.ebao.ls.ws.vo.cmn.cmn170.CalloutBatchType1List;
import com.ebao.ls.ws.vo.cmn.cmn170.CalloutBatchType2List;
import com.ebao.ls.ws.vo.cmn.cmn170.CalloutBatchType3List;
import com.ebao.ls.ws.vo.cmn.cmn170.CalloutBatchType4List;
import com.ebao.ls.ws.vo.cmn.cmn170.CalloutBatchType5List;
import com.ebao.ls.ws.vo.cmn.cmn170.CalloutBatchType6List;
import com.ebao.ls.ws.vo.cmn.cmn170.roleInfo.CalloutTargetInfo;
import com.ebao.ls.ws.vo.cmn.cmn170.roleInfo.HolderInfo;
import com.ebao.ls.ws.vo.cmn.cmn170.roleInfo.InsuredInfoBasic;
import com.ebao.ls.ws.vo.cmn.cmn170.roleInfo.InsuredInfoPartlyExtend;
import com.ebao.ls.ws.vo.cmn.cmn170.roleInfo.InsuredInfoTotallyExtend;
import com.ebao.ls.ws.vo.cmn.cmn170.roleInfo.LegalRepInfo;
import com.ebao.ls.ws.vo.cmn.cmn170.type.BatchType1;
import com.ebao.ls.ws.vo.cmn.cmn170.type.BatchType2;
import com.ebao.ls.ws.vo.cmn.cmn170.type.BatchType3;
import com.ebao.ls.ws.vo.cmn.cmn170.type.BatchType4;
import com.ebao.ls.ws.vo.cmn.cmn170.type.BatchType5;
import com.ebao.ls.ws.vo.cmn.cmn170.type.BatchType6;
import com.ebao.ls.ws.vo.cmn.cmn170.type.ChangeItem;
import com.ebao.pub.util.json.DateUtils;

public class CalloutHelp {

    public final static String STR = "STR";

    public final static String NUM = "NUM";

    public static Map<String, String> createTaskOneHead() {
        // 適用於 080-1、080-2、080-6-2、080-X
        Map<String, String> map = new java.util.LinkedHashMap<String, String>();
        map.put("POLICY_CODE", STR); // 保單號碼
        map.put("INTERNAL_ID", STR); // 險種代號
        map.put("PH_NAME", STR); // 要保人姓名
        map.put("PH_CERTI_CODE", STR); // 要保人身份證字號
        map.put("PH_GENDER",STR); //要保人_姓別
        map.put("PH_BIRTHDAY", STR); // 要保人出生日期
        map.put("PH_OFFICE_TEL", STR); //要保人_連絡電話(日)
        map.put("PH_HOME_TEL", STR); //要保人_連絡電話(夜)
        map.put("PH_PHONE1", STR); //要保人_連絡電話(手機1)
        map.put("PH_PHONE2", STR); //要保人_連絡電話(手機2)
        map.put("I_NAME", STR); // 被保險人姓名
        map.put("I_CERTI_CODE", STR); // 被保險人身份證字號
        map.put("I_AGE", STR); // 被保險人投保年齡
        map.put("APPLY_DATE", STR); // 要保書申請日
        map.put("VALIDATE_DATE", STR); // 生效日
        map.put("ISSUE_DATE", STR); // 承保日
        map.put("DISPATCH_DATE", STR); // 保單寄送日
        map.put("REPLY_DATE", STR); // 簽收回條日
        map.put("SEND_TYPE", STR); // 保單遞送方式
        map.put("ENSURE_REQUIRE", STR); // 保障需求
        map.put("RETIRE_REQUIRE", STR); // 退休規劃需求
        map.put("CHILD_FEE", STR); // 子女教育經費
        map.put("ASSET", STR); // 資產配置
        map.put("HOUSE", STR); // 房屋貸款需求
        map.put("STATE", STR); // 保單狀況
        map.put("EXCHANGE_RATE", STR); // 匯率
        map.put("MONEY_NAME", STR); // 幣別
        map.put("CHANNEL_CODE", STR); // Channel
        map.put("CHANNEL_NAME", STR); // 單位
        map.put("AGENT_NAME_1", STR); // 業務員姓名
        map.put("AGENT_NAME_2", STR); // 業務員姓名
        map.put("AGENT_NAME_3", STR); // 業務員姓名
        map.put("CUSTOMIZED_PREM", STR); // 自訂保費(Type_1)
        map.put("AMOUNT", STR); // 主約的保額
        map.put("BIG_PRODCATE", STR); // 主約的商品大類
        map.put("SMALL_PRODCATE", STR); // 主約的商品小類
        map.put("EACH_PREM", STR); // 每期保費
        return map;
    }
    public static Double round(Double value, int scale){
        if( value == null || value.equals(0d)){
            return 0d;
        }
        Double result = new BigDecimal(value).round(new MathContext(scale)).doubleValue();
        return result;
    }
    public static java.util.Map<String, Map<String, Object>> mergeMap(java.util.List<Map<String, Object>> mapList){
        java.util.Map<String, Map<String, Object>> mergeMap = new java.util.HashMap<String, Map<String,Object>>(); 
        if(CollectionUtils.isEmpty(mapList)){
            return mergeMap;
        }
        for(Map<String, Object> map: mapList){
            String policyCode = MapUtils.getString(map, "POLICY_CODE");
            String agentName = MapUtils.getString(map, "AGENT_NAME");
            String orderId  = MapUtils.getString(map, "ORDER_ID");
            
            if(!mergeMap.containsKey(policyCode)){
                mergeMap.put(policyCode, map);
            }
            Map<String, Object> rowMap = mergeMap.get(policyCode);
            if("1".equals(orderId)){
                rowMap.put("AGENT_NAME_1", agentName);
            }else if("2".equals(orderId)){
                rowMap.put("AGENT_NAME_2", agentName);
            }else if("3".equals(orderId)){
                rowMap.put("AGENT_NAME_3", agentName);
            }
        }
        return mergeMap;
    }
    private static String convertTWDate(Date currentDate){
        if (currentDate == null)
            return "";
        return DateUtils.convertTWDate(currentDate);
    }
    public static CalloutBatchType1List convertToBatch1(String docketNum, java.util.List<Map<String, Object>> mapList){
        if(CollectionUtils.isEmpty(mapList)){
            return new CalloutBatchType1List();
        }
        /* 合併 業務員欄位 */
        java.util.Map<String, Map<String, Object>> mergeMap = mergeMap(mapList);
        
        /* 轉置成XML格式 */
        CalloutBatchType1List result = new CalloutBatchType1List();
        java.util.List<BatchType1> calloutBatchList = new java.util.LinkedList<BatchType1>();
        for(Map.Entry<String, Map<String, Object>> entry  : mergeMap.entrySet()){
        	boolean islegalResp = false;
            Map<String,Object> map = entry.getValue();
            BatchType1 data =  new BatchType1();
            data.setCalloutNum(MapUtils.getString(map, "CALLOUT_NUM", ""));
            //data.setCalloutTarget(CodeTable.getCodeDesc("T_CALLOUT_TARGET_TYPE", CalloutTargetType.POLICY_HOLDER));
            
            //PCR 374909 新客服系統上線 新增欄位 - 法定代理人資訊
            LegalRepInfo legalRepInfo = CalloutUtils.mapToLegalRep(map); 
            data.setLegalRepInfo(legalRepInfo);
            if (legalRepInfo!=null && legalRepInfo.getName() !=null && !legalRepInfo.getName().equals("")){
            	data.setCalloutTarget(CodeTable.getCodeDesc("T_CALLOUT_TARGET_TYPE", CalloutTargetType.LEGAL_REPRESENTATIVE));
            	islegalResp = true;
            }else{
            	//預設為要保人
                data.setCalloutTarget(CodeTable.getCodeDesc("T_CALLOUT_TARGET_TYPE", CalloutTargetType.POLICY_HOLDER));
            }
            
            //PCR 374909 新客服系統上線 新增欄位 - 電訪對象資訊(要保人)
            CalloutTargetInfo calloutTargetInfo = CalloutUtils.mapToCalloutTarge(map , islegalResp); 
            data.setCalloutTargetInfo(calloutTargetInfo);
            
            
            HolderInfo holder = new HolderInfo();
            holder.setName(MapUtils.getString(map, "PH_NAME", ""));
            holder.setCertiCode(MapUtils.getString(map, "PH_CERTI_CODE", ""));
            holder.setGender(MapUtils.getString(map, "PH_GENDER", ""));
            holder.setContactNum1(MapUtils.getString(map, "PH_OFFICE_TEL", ""));
            holder.setContactNum2(MapUtils.getString(map, "PH_HOME_TEL", ""));
            holder.setContactNum3(MapUtils.getString(map, "PH_PHONE1", ""));
            holder.setContactNum4(MapUtils.getString(map, "PH_PHONE2", ""));
            holder.setHomeAddr(MapUtils.getString(map, "PH_HOME_ADDR", ""));
            Date birthday = (Date)map.get("PH_BIRTHDAY");
            if(birthday != null){
                holder.setBirthday(DateUtils.convertToMinguoDate(birthday));
            }
            data.setHolderInfo(holder);
            InsuredInfoBasic insured = new InsuredInfoBasic();
            insured.setName(MapUtils.getString(map, "I_NAME", ""));
            insured.setCertiCode(MapUtils.getString(map, "I_CERTI_CODE", ""));
            insured.setInsuredYear(MapUtils.getString(map, "I_AGE", ""));
            data.setInsuredInfo(insured);
            data.setPolicyCode(MapUtils.getString(map, "POLICY_CODE", ""));
            data.setLiabilityStatus(MapUtils.getString(map, "STATE", ""));
            Date applyDate = (Date)map.get("APPLY_DATE");
            data.setApplyDate(convertTWDate(applyDate));
            Date validateDate = (Date)map.get("VALIDATE_DATE");
            data.setValidateDate(convertTWDate(validateDate));
            Date issueDate = (Date)map.get("ISSUE_DATE");
            data.setIssueDate(convertTWDate(issueDate));
            Date dispatchDate = (Date)map.get("DISPATCH_DATE");
            data.setDispatchDate(convertTWDate(dispatchDate));
            Date replyDate = (Date)map.get("REPLY_DATE");
            data.setAcknowledgeDate(convertTWDate(replyDate));
            Date ackReceiveDate = (Date)map.get("RECEVICE_DATE");
            data.setAckReceiveDate(convertTWDate(ackReceiveDate));		//簽收單受理/收件確認日
            data.setMailToType(MapUtils.getString(map, "SEND_TYPE", ""));
            Double exchangeRate = new Double(0);
            if(map.get("EXCHANGE_RATE") != null){
                exchangeRate = round(MapUtils.getDouble(map, "EXCHANGE_RATE"),6);
            }
            data.setExchangeRate(String.valueOf(exchangeRate));
            data.setCurrency(MapUtils.getString(map, "MONEY_NAME", ""));
			Double eachPrem = MapUtils.getDouble(map, "EACH_PREM");
			if (eachPrem != null) {
				data.setEachPrem(doubleNumberFormat(eachPrem));
			}
			Double customizedPrem = MapUtils.getDouble(map, "CUSTOMIZED_PREM");
			if (customizedPrem != null) {
				data.setCustomizedPrem(doubleNumberFormat(customizedPrem));
			}
            data.setBizParent(MapUtils.getString(map, "BIG_PRODCATE", ""));
            data.setBizCate(MapUtils.getString(map, "SMALL_PRODCATE", ""));
            data.setUnitAmount(MapUtils.getString(map, "UNIT_AMOUNT"));
            data.setInternalId(MapUtils.getString(map, "INTERNAL_ID",""));
            data.setChannel(MapUtils.getString(map, "CHANNEL_TYPE",""));
            data.setChannelName(MapUtils.getString(map, "CHANNEL_NAME"));
            data.setAgentName1(MapUtils.getString(map, "AGENT_NAME_1",""));
            data.setAgentName2(MapUtils.getString(map, "AGENT_NAME_2",""));
            data.setAgentName3(MapUtils.getString(map, "AGENT_NAME_3",""));
            data.setEnsureIndi(MapUtils.getString(map, "ENSURE_REQUIRE","N"));
            data.setChildFeeIndi(MapUtils.getString(map, "CHILD_FEE","N"));
            data.setRetireIndi(MapUtils.getString(map, "RETIRE_REQUIRE","N"));
            data.setHouseIndi(MapUtils.getString(map, "HOUSE","N"));
            data.setAssetIndi(MapUtils.getString(map, "ASSET","N"));
            data.setEmpBenefitIndi(MapUtils.getString(map, "EMP_BENEFIT","N"));		//員工福利
            data.setOtherPurposeIndi(MapUtils.getString(map, "OTHER_PURPOSE","N"));	//其他
            data.setIlpChargeType(MapUtils.getString(map, "ILP_CHARGE_TYPE",""));	//費用類型
            data.setDocketNum(docketNum);
            calloutBatchList.add(data);
        }
        
        result.setCalloutBatchList(calloutBatchList);
        return result;
    }
    
    public static CalloutBatchType2List convertToBatch2(String docketNum, java.util.List<Map<String, Object>> mapList){
        CalloutBatchType2List result =  new CalloutBatchType2List();
        if(CollectionUtils.isEmpty(mapList)){
            return result;
        }
        /* 合併 業務員欄位 */
        java.util.Map<String, Map<String, Object>> mergeMap = mergeMap(mapList);

        java.util.List<BatchType2> calloutBatchList = new java.util.LinkedList<BatchType2>();
        for(Map.Entry<String, Map<String, Object>> entry  : mergeMap.entrySet()){
        	boolean islegalResp = false ;
            Map<String,Object> map = entry.getValue();
            BatchType2 data =  new BatchType2();
            data.setCalloutNum(MapUtils.getString(map, "CALLOUT_NUM", ""));
            //data.setCalloutTarget(CodeTable.getCodeDesc("T_CALLOUT_TARGET_TYPE", CalloutTargetType.POLICY_HOLDER));
            //PCR 374909 新客服系統上線 新增欄位 - 法定代理人資訊
            LegalRepInfo legalRepInfo = CalloutUtils.mapToLegalRep(map); 
            data.setLegalRepInfo(legalRepInfo);
            if (legalRepInfo!=null && legalRepInfo.getName() !=null && !legalRepInfo.getName().equals("")){
            	data.setCalloutTarget(CodeTable.getCodeDesc("T_CALLOUT_TARGET_TYPE", CalloutTargetType.LEGAL_REPRESENTATIVE));
            	islegalResp = true;
            }else{
            	//預設為要保人
                data.setCalloutTarget(CodeTable.getCodeDesc("T_CALLOUT_TARGET_TYPE", CalloutTargetType.POLICY_HOLDER));
            }
            //PCR 374909 新客服系統上線 新增欄位 - 電訪對象資訊(要保人)
            CalloutTargetInfo calloutTargetInfo = CalloutUtils.mapToCalloutTarge(map, islegalResp); 
            data.setCalloutTargetInfo(calloutTargetInfo);
            
            HolderInfo holder = new HolderInfo();
            holder.setName(MapUtils.getString(map, "PH_NAME", ""));
            holder.setCertiCode(MapUtils.getString(map, "PH_CERTI_CODE", ""));
            holder.setGender(MapUtils.getString(map, "PH_GENDER", ""));
            holder.setContactNum1(MapUtils.getString(map, "PH_OFFICE_TEL", ""));
            holder.setContactNum2(MapUtils.getString(map, "PH_HOME_TEL", ""));
            holder.setContactNum3(MapUtils.getString(map, "PH_PHONE1", ""));
            holder.setContactNum4(MapUtils.getString(map, "PH_PHONE2", ""));
            Date birthday = (Date)map.get("PH_BIRTHDAY");
            if(birthday != null){
                holder.setBirthday(DateUtils.convertToMinguoDate(birthday));
            }
            holder.setHomeAddr(MapUtils.getString(map, "PH_HOME_ADDR", ""));
            data.setHolderInfo(holder);
            InsuredInfoBasic insured = new InsuredInfoBasic();
            insured.setName(MapUtils.getString(map, "I_NAME", ""));
            insured.setCertiCode(MapUtils.getString(map, "I_CERTI_CODE", ""));
            insured.setInsuredYear(MapUtils.getString(map, "I_AGE", ""));
            data.setInsuredInfo(insured);
            data.setPolicyCode(MapUtils.getString(map, "POLICY_CODE", ""));
            data.setLiabilityStatus(MapUtils.getString(map, "STATE", ""));
            Date applyDate = (Date)map.get("APPLY_DATE");
            data.setApplyDate(convertTWDate(applyDate));
            Date validateDate = (Date)map.get("VALIDATE_DATE");
            data.setValidateDate(convertTWDate(validateDate));
            Date issueDate = (Date)map.get("ISSUE_DATE");
            data.setIssueDate(convertTWDate(issueDate));
            Date dispatchDate = (Date)map.get("DISPATCH_DATE");
            data.setDispatchDate(convertTWDate(dispatchDate));
            Date replyDate = (Date)map.get("REPLY_DATE");
            data.setAcknowledgeDate(convertTWDate(replyDate));
            Date ackReceiveDate = (Date)map.get("RECEVICE_DATE");
            data.setAckReceiveDate(convertTWDate(ackReceiveDate));		//簽收單受理/收件確認日
            data.setMailToType(MapUtils.getString(map, "SEND_TYPE", ""));
            Double exchangeRate = new Double(0);
            if(map.get("EXCHANGE_RATE") != null){
                exchangeRate = round(MapUtils.getDouble(map, "EXCHANGE_RATE"),6);
            }
            data.setExchangeRate(String.valueOf(exchangeRate));
            data.setCurrency(MapUtils.getString(map, "MONEY_NAME", ""));
            data.setRenewalType(MapUtils.getString(map, "CHARGE_NAME",""));
			Double eachPrem = MapUtils.getDouble(map, "EACH_PREM");
			if (eachPrem != null) {
				data.setEachPrem(doubleNumberFormat(eachPrem));
			}

			Double foreignPrem = MapUtils.getDouble(map, "STD_PREM");
			if (foreignPrem != null) {
				data.setForiegnPrem(doubleNumberFormat(foreignPrem));
			}
			Double stdPremTW = MapUtils.getDouble(map, "STD_TW_PREM");
			if (stdPremTW != null) {
				data.setAnnualChargeTw(doubleNumberFormat(round(stdPremTW, 6)));
			}
            data.setBizParent(MapUtils.getString(map, "BIG_PRODCATE", ""));
            data.setBizCate(MapUtils.getString(map, "SMALL_PRODCATE", ""));
            data.setUnitAmount(MapUtils.getString(map, "UNIT_AMOUNT"));
            data.setInternalId(MapUtils.getString(map, "INTERNAL_ID",""));
            data.setChannel(MapUtils.getString(map, "CHANNEL_TYPE",""));
            data.setChannelName(MapUtils.getString(map, "CHANNEL_NAME",""));
            data.setAgentName1(MapUtils.getString(map, "AGENT_NAME_1",""));
            data.setAgentName2(MapUtils.getString(map, "AGENT_NAME_2",""));
            data.setAgentName3(MapUtils.getString(map, "AGENT_NAME_3",""));
            data.setEnsureIndi(MapUtils.getString(map, "ENSURE_REQUIRE","N"));
            data.setChildFeeIndi(MapUtils.getString(map, "CHILD_FEE","N"));
            data.setRetireIndi(MapUtils.getString(map, "RETIRE_REQUIRE","N"));
            data.setHouseIndi(MapUtils.getString(map, "HOUSE","N"));
            data.setAssetIndi(MapUtils.getString(map, "ASSET","N"));
            data.setEmpBenefitIndi(MapUtils.getString(map, "EMP_BENEFIT","N"));		//員工福利
            data.setOtherPurposeIndi(MapUtils.getString(map, "OTHER_PURPOSE","N"));	//其他
            data.setIlpChargeType(MapUtils.getString(map, "ILP_CHARGE_TYPE",""));	//費用類型
            //PCR 415241 主約險種名稱
            data.setMasterProductName(MapUtils.getString(map, "PRODUCT_NAME",""));
            //PCR 415241 保單郵寄掛號/國際快捷/宅配號碼
            data.setTrackingNumber(MapUtils.getString(map, "TRACKING_NUMBER",""));
            //PCR 415241 主約應繳保費
			Double masterPremAmount = MapUtils.getDouble(map, "MASTER_PREM_AMOUNT");
			if (masterPremAmount != null) {
				data.setMasterPremAmount(doubleNumberFormat(masterPremAmount));
			}
            //PCR 415241 繳費期間
            data.setChargePeriodText(MapUtils.getString(map, "CHARGE_PERIOD_TEXT",""));
            data.setDocketNum(docketNum);
            calloutBatchList.add(data);
        }
        result.setCalloutBatchList(calloutBatchList);
        return result;
    }
    public static CalloutBatchType3List convertToBatch3(String docketNum, java.util.List<Map<String, Object>> mapList){
        CalloutBatchType3List result = new CalloutBatchType3List();
        if(CollectionUtils.isEmpty(mapList)){
            return result;
        }
        /* 合併 業務員欄位 */
        java.util.Map<String, Map<String, Object>> mergeMap = mergeMap(mapList);
        
        /* 轉置成XML格式 */
        java.util.List<BatchType3> calloutBatchList = new java.util.LinkedList<BatchType3>();
        for(Map.Entry<String, Map<String, Object>> entry  : mergeMap.entrySet()){
        	boolean islegalResp = false ;
            Map<String,Object> map = entry.getValue();
            BatchType3 data =  new BatchType3();
            
            
            data.setCalloutNum(MapUtils.getString(map, "CALLOUT_NUM", ""));
            //data.setCalloutTarget(CodeTable.getCodeDesc("T_CALLOUT_TARGET_TYPE", CalloutTargetType.POLICY_HOLDER));
            //PCR 374909 新客服系統上線 新增欄位 - 法定代理人資訊
            LegalRepInfo legalRepInfo = CalloutUtils.mapToLegalRep(map); 
            data.setLegalRepInfo(legalRepInfo);
            //PCR 374909 新客服系統上線 新增欄位 - 電訪對象資訊
            //客要保人之法定代理人1有值→顯示「要保人」
            //要保人之法定代理人1有值→顯示「要保人之法定代理人」  
            //要保人之法定代理人1有值，應電訪法定代理人 
            if (legalRepInfo!=null && legalRepInfo.getName() !=null && !legalRepInfo.getName().equals("")){
            	data.setCalloutTarget(CodeTable.getCodeDesc("T_CALLOUT_TARGET_TYPE", CalloutTargetType.LEGAL_REPRESENTATIVE));
            	islegalResp = true ;
            }else{
            	//預設為要保人
                data.setCalloutTarget(CodeTable.getCodeDesc("T_CALLOUT_TARGET_TYPE", CalloutTargetType.POLICY_HOLDER));
            }
            
            //PCR 374909 新客服系統上線 新增欄位 - 電訪對象資訊(要保人)
            CalloutTargetInfo calloutTargetInfo = CalloutUtils.mapToCalloutTarge(map, islegalResp);  
            data.setCalloutTargetInfo(calloutTargetInfo);
            
            HolderInfo holder = new HolderInfo();
            holder.setName(MapUtils.getString(map, "PH_NAME", ""));
            holder.setCertiCode(MapUtils.getString(map, "PH_CERTI_CODE", ""));
            holder.setGender(MapUtils.getString(map, "PH_GENDER", ""));
            holder.setContactNum1(MapUtils.getString(map, "PH_OFFICE_TEL", ""));
            holder.setContactNum2(MapUtils.getString(map, "PH_HOME_TEL", ""));
            holder.setContactNum3(MapUtils.getString(map, "PH_PHONE1", ""));
            holder.setContactNum4(MapUtils.getString(map, "PH_PHONE2", ""));
            Date birthday = (Date)map.get("PH_BIRTHDAY");
            if(birthday != null){
                holder.setBirthday(DateUtils.convertToMinguoDate(birthday));
            }
            holder.setHomeAddr(MapUtils.getString(map, "PH_HOME_ADDR", ""));
            data.setHolderInfo(holder);
            InsuredInfoBasic insured = new InsuredInfoBasic();
            insured.setName(MapUtils.getString(map, "I_NAME", ""));
            insured.setCertiCode(MapUtils.getString(map, "I_CERTI_CODE", ""));
            insured.setInsuredYear(MapUtils.getString(map, "I_AGE", ""));
            data.setInsuredInfo(insured);
            data.setPolicyCode(MapUtils.getString(map, "POLICY_CODE", ""));
            data.setLiabilityStatus(MapUtils.getString(map, "STATE", ""));
            Date applyDate = (Date)map.get("APPLY_DATE");
            data.setApplyDate(convertTWDate(applyDate));
            Date validateDate = (Date)map.get("VALIDATE_DATE");
            data.setValidateDate(convertTWDate(validateDate));
            Date issueDate = (Date)map.get("ISSUE_DATE");
            data.setIssueDate(convertTWDate(issueDate));
            Date dispatchDate = (Date)map.get("DISPATCH_DATE");
            data.setDispatchDate(convertTWDate(dispatchDate));
            Date replyDate = (Date)map.get("REPLY_DATE");
            data.setAcknowledgeDate(convertTWDate(replyDate));
            data.setMailToType(MapUtils.getString(map, "SEND_TYPE", ""));
            Double exchangeRate = new Double(0);
            if(map.get("EXCHANGE_RATE") != null){
                exchangeRate = round(MapUtils.getDouble(map, "EXCHANGE_RATE"),6);
            }
            data.setExchangeRate(String.valueOf(exchangeRate));
            data.setCurrency(MapUtils.getString(map, "MONEY_NAME", ""));
            data.setRenewalType(MapUtils.getString(map, "CHARGE_NAME",""));
            
			Double eachPrem = MapUtils.getDouble(map, "EACH_PREM");
			if (eachPrem != null) {
				data.setEachPrem(doubleNumberFormat(eachPrem));
			}
			Double foreignPrem = MapUtils.getDouble(map, "EACH_PREM");
			if (foreignPrem != null) {
				data.setForiegnPrem(doubleNumberFormat(foreignPrem));
			}
			Double customizedPrem = MapUtils.getDouble(map, "CUSTOMIZED_PREM");
			if (customizedPrem != null) {
				data.setCustomizedPrem(doubleNumberFormat(customizedPrem));
			}
			Double annualChargeTW = MapUtils.getDouble(map, "STD_TW_PREM", 0d);
			if (annualChargeTW != null) {
				data.setAnnualChargeTw(doubleNumberFormat(round(annualChargeTW, 6)));
			}
			Double annualCharge = MapUtils.getDouble(map, "ANNUAL_PREM", 0d);
			if (annualCharge != null) {
				data.setAnnualCharge(doubleNumberFormat(annualCharge));
			}
            
            data.setBizParent(MapUtils.getString(map, "BIG_PRODCATE", ""));
            data.setBizCate(MapUtils.getString(map, "SMALL_PRODCATE", ""));
            data.setUnitAmount(MapUtils.getString(map, "UNIT_AMOUNT"));
            data.setInternalId(MapUtils.getString(map, "INTERNAL_ID",""));
            data.setChannel(MapUtils.getString(map, "CHANNEL_TYPE",""));
            data.setChannelName(MapUtils.getString(map, "CHANNEL_NAME",""));
            data.setAgentName1(MapUtils.getString(map, "AGENT_NAME_1",""));
            data.setAgentName2(MapUtils.getString(map, "AGENT_NAME_2",""));
            data.setAgentName3(MapUtils.getString(map, "AGENT_NAME_3",""));
            data.setEnsureIndi(MapUtils.getString(map, "ENSURE_REQUIRE","N"));
            data.setChildFeeIndi(MapUtils.getString(map, "CHILD_FEE","N"));
            data.setRetireIndi(MapUtils.getString(map, "RETIRE_REQUIRE","N"));
            data.setHouseIndi(MapUtils.getString(map, "HOUSE","N"));
            data.setAssetIndi(MapUtils.getString(map, "ASSET","N"));
            data.setIlpChargeType(MapUtils.getString(map, "ILP_CHARGE_TYPE","N"));	//費用類型
            //PCR 415241 主約險種名稱
            data.setMasterProductName(MapUtils.getString(map, "PRODUCT_NAME",""));
            //PCR 415241 保單郵寄掛號/國際快捷/宅配號碼
            data.setTrackingNumber(MapUtils.getString(map, "TRACKING_NUMBER",""));
            //PCR 415241 主約應繳保費
            Double masterPremAmount = MapUtils.getDouble(map, "MASTER_PREM_AMOUNT");
            if(masterPremAmount != null){
                data.setMasterPremAmount(doubleNumberFormat(masterPremAmount));
            }
            //PCR 415241 繳費期間
            data.setChargePeriodText(MapUtils.getString(map, "CHARGE_PERIOD_TEXT",""));
            data.setDocketNum(docketNum);
            calloutBatchList.add(data);
        }
        result.setCalloutBatchList(calloutBatchList);
        return result;
    }
    
    public static CalloutBatchType4List convertToBatch4(String docketNum, java.util.List<Map<String, Object>> mapList){
        CalloutBatchType4List  result = new CalloutBatchType4List();
        if(CollectionUtils.isEmpty(mapList)){
            return result;
        }
        java.util.List<BatchType4> calloutBatchList = new java.util.LinkedList<BatchType4>();
        for(Map<String, Object> map : mapList){
        	boolean islegalResp = false ;
            BatchType4 data = CalloutUtils.mapToBatch4(map);
            data.setDocketNum(docketNum);
            //data.setCalloutTarget(CodeTable.getCodeDesc("T_CALLOUT_TARGET_TYPE", CalloutTargetType.POLICY_HOLDER));
            
            String langId = CodeCst.LANGUAGE__CHINESE_TRAD;
            ApplicationContext context = AppUserContext.getApplicationContext();
            ApplicationService applicationService = context.getBean( ApplicationService.BEAN_DEFAULT,ApplicationService.class);
            CalloutTransService calloutServ = context.getBean(CalloutTransService.BEAN_DEFAULT,CalloutTransService.class);
            data.setCalloutNum(MapUtils.getString(map, "CALLOUT_NUM", ""));
            //data.setCalloutTarget(CodeTable.getCodeDesc("T_CALLOUT_TARGET_TYPE", CalloutTargetType.POLICY_HOLDER));
            //PCR 374909 新客服系統上線 新增欄位 - 法定代理人資訊
            LegalRepInfo legalRepInfo = CalloutUtils.mapToLegalRep(map); 
            data.setLegalRepInfo(legalRepInfo);
            //PCR 374909 新客服系統上線 新增欄位 - 電訪對象資訊
            //客戶申請日-要保人生日≥18足歲→顯示「要保人」
            //客戶申請日-要保人生日<18足歲→顯示「要保人之法定代理人」  
    		Date ctBirthday = (Date)map.get("ph_birthday");				//要保人生日
    		Date custApplyDate =(Date)map.get("CUST_APP_TIME"); 			//客戶申請日
    		 
    		// PCR492019 民法成年年齡由20歲降為18歲
            Map<String, Object> ageMap = POSUtils.getGrowUpAge(ctBirthday, custApplyDate);
			Boolean isGrowUp = MapUtils.getBoolean(ageMap, "isGrowUp");//(true: 成年, false: 未成年)
            
			if (!isGrowUp){
            	data.setCalloutTarget(CodeTable.getCodeDesc("T_CALLOUT_TARGET_TYPE", CalloutTargetType.LEGAL_REPRESENTATIVE));
            	islegalResp = true;
            }else{
            	//預設為要保人
                data.setCalloutTarget(CodeTable.getCodeDesc("T_CALLOUT_TARGET_TYPE", CalloutTargetType.POLICY_HOLDER));
            }
            
            //PCR 374909 新客服系統上線 新增欄位 - 電訪對象資訊(要保人)
            CalloutTargetInfo calloutTargetInfo = CalloutUtils.mapToCalloutTarge(map, islegalResp ); 
            data.setCalloutTargetInfo(calloutTargetInfo);
            
            
            HolderInfo holderInfo = CalloutUtils.mapToHolder(map);
            data.setHolderInfo(holderInfo);
            
            InsuredInfoPartlyExtend insuredInfo = new InsuredInfoPartlyExtend();
            insuredInfo.setName(MapUtils.getString(map, "I_NAME", ""));
            insuredInfo.setCertiCode(MapUtils.getString(map, "I_CERTI_CODE", ""));
            insuredInfo.setContactNum1(MapUtils.getString(map, "I_OFFICE_TEL", ""));
            java.util.Date birthday = (java.util.Date) map.get("I_BIRTH_DATE");
            if (birthday != null)
                insuredInfo.setBirthday(DateUtils.convertToMinguoDate(birthday));
            data.setInsuredInfo(insuredInfo);
            data.setPolicyCode(MapUtils.getString(map, "POLICY_CODE", ""));
            Integer liabilityState = MapUtils.getInteger(map, "LIABILITY_STATE");
            data.setLiabilityStatus(CodeTable.getCodeDesc("T_LIABILITY_STATUS",String.valueOf(liabilityState),langId));
            java.util.Date validateDate = (java.util.Date) map.get("VALIDATE_DATE");
            if (validateDate != null)
                data.setValidateDate(DateUtils.convertTWDate(validateDate));
            data.setHandleName(MapUtils.getString(map, "HANDLE_NAME", ""));
            java.util.Date finAppTime = (java.util.Date) map.get("FIN_APP_TIME");
            data.setFinAppTime(DateUtils.convertTWDate(finAppTime));
            String sendTypeId = MapUtils.getString(map, "SEND_TYPE","");
            if(StringUtils.isNotEmpty(sendTypeId)){
                data.setSendTypeDesc(CodeTable.getCodeDesc("T_DELIVER_TYPE", sendTypeId, langId));
            }
            data.setSendPersonName(MapUtils.getString(map, "SEND_PERSON_NAME", ""));
            java.util.Date custAppTime = (java.util.Date) map.get("CUST_APP_TIME");
            data.setCustAppTime(DateUtils.convertTWDate(custAppTime));
            String endCauseId = MapUtils.getString(map, "END_CAUSE", "");
            if(StringUtils.isNotEmpty(endCauseId)){
                data.setEndCauseDesc(CodeTable.getCodeDesc("T_END_CAUSE", endCauseId,langId));
            }
            Long changeId = MapUtils.getLong(map, "CHANGE_ID");

            java.util.List<com.ebao.ls.cs.commonflow.data.bo.AlterationItem> alterationItemList = applicationService.getAlterationItemByChangeId(changeId);
            List<ChangeItem> changeItemList = new java.util.LinkedList<ChangeItem>();
            for (AlterationItem item : alterationItemList) {
                ChangeItem changeItem = new ChangeItem();
                if (item.getValidateTime() != null)
                    changeItem.setValidateTime(DateUtils.convertTWDate(item.getValidateTime()));
                if(item.getServiceId() != null){
                    changeItem.setAlternationItemDesc(CodeTable.getCodeDesc("T_SERVICE", String.valueOf(item.getServiceId()), langId));
                    //changeItem.setDetailItemDesc(CodeTable.getCodeDesc("T_SERVICE", String.valueOf(item.getServiceId()), langId));
                }
                changeItemList.add(changeItem);
            }
            data.setDetailItemDesc(MapUtils.getString(map, "DETAIL_ITEM_DESC"));
            data.setChangeItem(changeItemList);
            data.setSeq(MapUtils.getInteger(map, "SEQ"));
            //PCR 415241 主約新險種代碼
            data.setMasterNewProductCode(MapUtils.getString(map, "MASTER_NEW_PRODUCT_CODE",""));
            //PCR 415241 主約險種名稱
            data.setMasterProductName(MapUtils.getString(map, "MASTER_PRODUCT_NAME",""));
            data.setDocketNum(docketNum);
            
            calloutBatchList.add(data);
        }
        result.setCalloutBatchType(calloutBatchList);
        return result;
    }
    
    public static CalloutBatchType5List convertToBatchInsured(String docketNum, java.util.List<java.util.Map<String,Object>> mapList ){
        CalloutBatchType5List result = new CalloutBatchType5List();
        if(CollectionUtils.isEmpty(mapList)){
            return result;
        }
        java.util.List<BatchType5> calloutBatchList = new java.util.LinkedList<BatchType5>();
        for(Map<String, Object> map : mapList){
            BatchType5 data = new BatchType5();
            boolean islegalResp = false;
            String langId = CodeCst.LANGUAGE__CHINESE_TRAD;
            ApplicationContext context = AppUserContext.getApplicationContext();
            ApplicationService applicationService = context.getBean( ApplicationService.BEAN_DEFAULT,ApplicationService.class);
            CalloutTransService calloutServ = context.getBean(CalloutTransService.BEAN_DEFAULT,CalloutTransService.class);
            //PCR 374909 新客服系統上線 新增欄位 - 電訪對象資訊
            //客戶申請日-被保人生日≥18足歲→顯示「被保人」
            //客戶申請日-被保人生日<18足歲→顯示「被保人之法定代理人」  
    		Date ctBirthday = (Date)map.get("I_BIRTH_DATE");				//被保人生日
    		Date custApplyDate =(Date)map.get("CUST_APP_TIME"); 			//客戶申請日
    		
    		// PCR492019 民法成年年齡由20歲降為18歲
            Map<String, Object> ageMap = POSUtils.getGrowUpAge(ctBirthday, custApplyDate);
			Boolean isGrowUp = MapUtils.getBoolean(ageMap, "isGrowUp");//(true: 成年, false: 未成年)
			 
            if (!isGrowUp){
            	data.setCalloutTarget(CodeTable.getCodeDesc("T_CALLOUT_TARGET_TYPE", CalloutTargetType.LEGAL_REPRESENTATIVE));
            	islegalResp = true;
            }else{
            	//預設為被保人
                data.setCalloutTarget(CodeTable.getCodeDesc("T_CALLOUT_TARGET_TYPE", CalloutTargetType.BENEFIT_INSURED));
            }
            data.setCalloutNum(MapUtils.getString(map, "CALLOUT_NUM", ""));
          //PCR 374909 新客服系統上線 新增欄位 - 電訪對象資訊(要保人)
            CalloutTargetInfo calloutTargetInfo = CalloutUtils.mapToCalloutTarge(map, islegalResp); 
            data.setCalloutTargetInfo(calloutTargetInfo);
          //PCR 374909 新客服系統上線 新增欄位 - 法定代理人資訊
            LegalRepInfo legalRepInfo = CalloutUtils.mapToLegalRep(map); 
            data.setLegalRepInfo(legalRepInfo);
            
            HolderInfo holderInfo = CalloutUtils.mapToHolder(map);
            data.setHolderInfo(holderInfo);
            InsuredInfoTotallyExtend insuredInfoTotallyExtend = CalloutUtils.mapToInsuredTotally(map);
            data.setInsuredInfo(insuredInfoTotallyExtend);
            data.setPolicyCode(MapUtils.getString(map, "POLICY_CODE", ""));
            
            data.setHandleName(MapUtils.getString(map, "HANDLE_NAME", ""));

            java.util.Date finAppTime = (java.util.Date) map.get("FIN_APP_TIME");
            data.setFinAppTime(DateUtils.convertToMinguoDate(finAppTime));
            String sendTypeId = MapUtils.getString(map, "SEND_TYPE","");
            if(StringUtils.isNotEmpty(sendTypeId)){
                String sendTypeDesc = CodeTable.getCodeDesc("T_DELIVER_TYPE", sendTypeId, langId);
                data.setSendTypeDesc(StringUtils.defaultString(sendTypeDesc, ""));
            }
            data.setSendPersonName(MapUtils.getString(map, "SEND_PERSON_NAME", ""));
            java.util.Date custAppTime = (java.util.Date) map.get("CUST_APP_TIME");
            data.setCustAppTime(DateUtils.convertTWDate(custAppTime));
            Long changeId = MapUtils.getLong(map, "CHANGE_ID");
            
            java.util.List<com.ebao.ls.cs.commonflow.data.bo.AlterationItem> alterationItemList = applicationService.getAlterationItemByChangeId(changeId);
            List<ChangeItem> changeItemList = new java.util.LinkedList<ChangeItem>();
            for (AlterationItem item : alterationItemList) {
                ChangeItem changeItem = new ChangeItem();
                if (item.getValidateTime() != null)
                    changeItem.setValidateTime(DateUtils.convertTWDate(item.getValidateTime()));
                changeItem.setAlternationItemDesc("");
                if(item.getServiceId() != null){
                    String alternationItemDesc = CodeTable.getCodeDesc("T_SERVICE", String.valueOf(item.getServiceId()), langId);
                    changeItem.setAlternationItemDesc(StringUtils.defaultString(alternationItemDesc, ""));
                }
//              String designationId = MapUtils.getString(map, "DESIGNATION");
//              if(StringUtils.isNotEmpty(designationId)){
//                  changeItem.setDetailItemDesc( CodeTable.getCodeDesc("T_BENE_DESIGNATION", designationId, langId));
//              }
//                changeItem.setDetailItemDesc(MapUtils.getString(map, "DETAIL_ITEM_DESC"));
                changeItemList.add(changeItem);
            }
            data.setDetailItemDesc(MapUtils.getString(map, "DETAIL_ITEM_DESC"));
            data.setChangeItem(changeItemList);
            data.setSeq(MapUtils.getInteger(map, "SEQ"));
            data.setDocketNum(docketNum);
            //data.setCalloutTarget(CodeTable.getCodeDesc("T_CALLOUT_TARGET_TYPE", CalloutTargetType.BENEFIT_INSURED));
            calloutBatchList.add(data);
        }
        result.setCalloutBatchList(calloutBatchList);
        return result;
    }
    
    public static CalloutBatchType5List convertToBatchBoth(String docketNum, java.util.List<java.util.Map<String,Object>> mapList ){
        CalloutBatchType5List result = new CalloutBatchType5List();
        if(CollectionUtils.isEmpty(mapList)){
            return result;
        }
        java.util.List<BatchType5> calloutBatchList = new java.util.LinkedList<BatchType5>();
        /* 電訪對象=要保人 */
        for(Map<String, Object> map : mapList){
            BatchType5 data = CalloutUtils.mapToBatch5(map);
            data.setDocketNum(docketNum);
            data.setCalloutNum(MapUtils.getString(map, "CALLOUT_NUM1"));
            //預設為要保人
            //data.setCalloutTarget(CodeTable.getCodeDesc("T_CALLOUT_TARGET_TYPE", CalloutTargetType.POLICY_HOLDER));
            calloutBatchList.add(data);
        }
        /* 電訪對象=被保人 */
        //PCR-245365取消被保人
        //for(Map<String, Object> map : mapList){
        //    BatchType5 data = CalloutUtils.mapToBatch5(map);
        //    data.setDocketNum(docketNum);
        //    data.setCalloutNum(MapUtils.getString(map, "CALLOUT_NUM2"));
        //    data.setCalloutTarget(CodeTable.getCodeDesc("T_CALLOUT_TARGET_TYPE", CalloutTargetType.BENEFIT_INSURED));
        //    calloutBatchList.add(data);
        //}
        result.setCalloutBatchList(calloutBatchList);
        return result;
    }
    
    public static CalloutBatchType6List convertToBatch6(String docketNum, java.util.List<Map<String, Object>> mapList){
        if(CollectionUtils.isEmpty(mapList)){
            return new CalloutBatchType6List();
        }
        /* 合併 業務員欄位 */
        java.util.Map<String, Map<String, Object>> mergeMap = mergeMap(mapList);
        
        /* 轉置成XML格式 */
        CalloutBatchType6List result = new CalloutBatchType6List();
        java.util.List<BatchType6> calloutBatchList = new java.util.LinkedList<BatchType6>();
        for(Map.Entry<String, Map<String, Object>> entry  : mergeMap.entrySet()){
        	boolean islegalResp = false;
            Map<String,Object> map = entry.getValue();
            BatchType6 data =  new BatchType6();
            data.setCalloutNum(MapUtils.getString(map, "CALLOUT_NUM", ""));
            //data.setCalloutTarget(CodeTable.getCodeDesc("T_CALLOUT_TARGET_TYPE", CalloutTargetType.POLICY_HOLDER));
            
            //PCR 374909 新客服系統上線 新增欄位 - 法定代理人資訊
            LegalRepInfo legalRepInfo = CalloutUtils.mapToLegalRep(map); 
            data.setLegalRepInfo(legalRepInfo);
            if (legalRepInfo!=null && legalRepInfo.getName() !=null && !legalRepInfo.getName().equals("")){
            	data.setCalloutTarget(CodeTable.getCodeDesc("T_CALLOUT_TARGET_TYPE", CalloutTargetType.LEGAL_REPRESENTATIVE));
            	islegalResp = true;
            }else{
            	//預設為要保人
                data.setCalloutTarget(CodeTable.getCodeDesc("T_CALLOUT_TARGET_TYPE", CalloutTargetType.POLICY_HOLDER));
            }
            
            //PCR 374909 新客服系統上線 新增欄位 - 電訪對象資訊(要保人)
            CalloutTargetInfo calloutTargetInfo = CalloutUtils.mapToCalloutTarge(map , islegalResp); 
            data.setCalloutTargetInfo(calloutTargetInfo);
            
            
            HolderInfo holder = new HolderInfo();
            holder.setName(MapUtils.getString(map, "PH_NAME", ""));
            holder.setCertiCode(MapUtils.getString(map, "PH_CERTI_CODE", ""));
            holder.setGender(MapUtils.getString(map, "PH_GENDER", ""));
            holder.setContactNum1(MapUtils.getString(map, "PH_OFFICE_TEL", ""));
            holder.setContactNum2(MapUtils.getString(map, "PH_HOME_TEL", ""));
            holder.setContactNum3(MapUtils.getString(map, "PH_PHONE1", ""));
            holder.setContactNum4(MapUtils.getString(map, "PH_PHONE2", ""));
            holder.setHomeAddr(MapUtils.getString(map, "PH_HOME_ADDR", ""));
            Date birthday = (Date)map.get("PH_BIRTHDAY");
            if(birthday != null){
                holder.setBirthday(DateUtils.convertToMinguoDate(birthday));
            }
            data.setHolderInfo(holder);
            InsuredInfoBasic insured = new InsuredInfoBasic();
            insured.setName(MapUtils.getString(map, "I_NAME", ""));
            insured.setCertiCode(MapUtils.getString(map, "I_CERTI_CODE", ""));
            insured.setInsuredYear(MapUtils.getString(map, "I_AGE", ""));
            data.setInsuredInfo(insured);
            data.setPolicyCode(MapUtils.getString(map, "POLICY_CODE", ""));
            data.setLiabilityStatus(MapUtils.getString(map, "STATE", ""));
            Date applyDate = (Date)map.get("APPLY_DATE");
            data.setApplyDate(convertTWDate(applyDate));
            Date validateDate = (Date)map.get("VALIDATE_DATE");
            data.setValidateDate(convertTWDate(validateDate));
            Date issueDate = (Date)map.get("ISSUE_DATE");
            data.setIssueDate(convertTWDate(issueDate));
            Date dispatchDate = (Date)map.get("DISPATCH_DATE");
            data.setDispatchDate(convertTWDate(dispatchDate));
            Date replyDate = (Date)map.get("REPLY_DATE");
            data.setAcknowledgeDate(convertTWDate(replyDate));
            Date ackReceiveDate = (Date)map.get("RECEVICE_DATE");
            data.setAckReceiveDate(convertTWDate(ackReceiveDate));		//簽收單受理/收件確認日
            data.setMailToType(MapUtils.getString(map, "SEND_TYPE", ""));
            Double exchangeRate = new Double(0);
            if(map.get("EXCHANGE_RATE") != null){
                exchangeRate = round(MapUtils.getDouble(map, "EXCHANGE_RATE"),6);
            }
            data.setExchangeRate(String.valueOf(exchangeRate));
            data.setCurrency(MapUtils.getString(map, "MONEY_NAME", ""));
			Double eachPrem = MapUtils.getDouble(map, "EACH_PREM");
			if (eachPrem != null) {
				data.setEachPrem(doubleNumberFormat(eachPrem));
			}
			Double customizedPrem = MapUtils.getDouble(map, "CUSTOMIZED_PREM");
			if (customizedPrem != null) {
				data.setCustomizedPrem(doubleNumberFormat(customizedPrem));
			}
            data.setBizParent(MapUtils.getString(map, "BIG_PRODCATE", ""));
            data.setBizCate(MapUtils.getString(map, "SMALL_PRODCATE", ""));
            data.setUnitAmount(MapUtils.getString(map, "UNIT_AMOUNT"));
            data.setInternalId(MapUtils.getString(map, "INTERNAL_ID",""));
            data.setChannel(MapUtils.getString(map, "CHANNEL_TYPE",""));
            data.setChannelName(MapUtils.getString(map, "CHANNEL_NAME"));
            data.setAgentName1(MapUtils.getString(map, "AGENT_NAME_1",""));
            data.setAgentName2(MapUtils.getString(map, "AGENT_NAME_2",""));
            data.setAgentName3(MapUtils.getString(map, "AGENT_NAME_3",""));
            data.setEnsureIndi(MapUtils.getString(map, "ENSURE_REQUIRE","N"));
            data.setChildFeeIndi(MapUtils.getString(map, "CHILD_FEE","N"));
            data.setRetireIndi(MapUtils.getString(map, "RETIRE_REQUIRE","N"));
            data.setHouseIndi(MapUtils.getString(map, "HOUSE","N"));
            data.setAssetIndi(MapUtils.getString(map, "ASSET","N"));
            data.setEmpBenefitIndi(MapUtils.getString(map, "EMP_BENEFIT","N"));		//員工福利
            data.setOtherPurposeIndi(MapUtils.getString(map, "OTHER_PURPOSE","N"));	//其他
            data.setIlpChargeType(MapUtils.getString(map, "ILP_CHARGE_TYPE",""));	//費用類型
            //PCR 415241 主約險種名稱
            data.setMasterProductName(MapUtils.getString(map, "PRODUCT_NAME",""));
            //PCR 415241 保單郵寄掛號/國際快捷/宅配號碼
            data.setTrackingNumber(MapUtils.getString(map, "TRACKING_NUMBER",""));
            //PCR 415241 主約應繳保費
			Double masterPremAmount = MapUtils.getDouble(map, "MASTER_PREM_AMOUNT");
			if (masterPremAmount != null) {
				data.setMasterPremAmount(doubleNumberFormat(masterPremAmount));
			}
            //PCR 415241 繳費期間
            data.setChargePeriodText(MapUtils.getString(map, "CHARGE_PERIOD_TEXT",""));
            //PCR 415241 繳別
            data.setRenewalType(MapUtils.getString(map, "CHARGE_NAME",""));
            data.setDocketNum(docketNum);
            calloutBatchList.add(data);
        }
        
        result.setCalloutBatchList(calloutBatchList);
        return result;
    }
    
    public static Map<String, String> createTaskThreeHead() {
        // 適用於 080-3、080-4、080-5、080-6-1
        Map<String, String> map = new java.util.LinkedHashMap<String, String>();
        map.put("POLICY_CODE", STR); // 保單號碼
        map.put("INTERNAL_ID", STR); // 險種代號
        map.put("PH_NAME", STR); // 要保人姓名
        map.put("PH_CERTI_CODE", STR); // 要保人身份證字號
        map.put("PH_GENDER",STR); //要保人_姓別
        map.put("PH_BIRTHDAY", STR); // 要保人出生日期
        map.put("PH_OFFICE_TEL", STR); //要保人_連絡電話(日)
        map.put("PH_HOME_TEL", STR); //要保人_連絡電話(夜)
        map.put("PH_PHONE1", STR); //要保人_連絡電話(手機1)
        map.put("PH_PHONE2", STR); //要保人_連絡電話(手機2)
        map.put("I_NAME", STR); // 被保險人姓名
        map.put("I_CERTI_CODE", STR); // 被保險人身份證字號
        map.put("I_AGE", STR); // 被保險人投保年齡
        map.put("APPLY_DATE", STR); // 要保書申請日
        map.put("VALIDATE_DATE", STR); // 生效日
        map.put("ISSUE_DATE", STR); // 承保日
        map.put("DISPATCH_DATE", STR); // 保單寄送日
        map.put("REPLY_DATE", STR); // 簽收回條日
        map.put("SEND_TYPE", STR); // 保單遞送方式
        map.put("ENSURE_REQUIRE", STR); // 保障需求
        map.put("RETIRE_REQUIRE", STR); // 退休規劃需求
        map.put("CHILD_FEE", STR); // 子女教育經費
        map.put("ASSET", STR); // 資產配置
        map.put("HOUSE", STR); // 房屋貸款需求
        map.put("STATE", STR); // 保單狀況
        map.put("MONEY_NAME", STR); // 幣別
        map.put("EXCHANGE_RATE", STR); // 匯率 
        map.put("CHARGE_NAME", STR); // 繳別 (Type_2)
        map.put("STD_PREM", STR); // 外幣保費 (Type_2)
        map.put("STD_TW_PREM", STR);// 換算新台幣保費 (Type_2)
        map.put("CHANNEL_CODE", STR); // Channel
        map.put("CHANNEL_NAME", STR); // 單位
        map.put("AGENT_NAME_1", STR); // 業務員姓名
        map.put("AGENT_NAME_2", STR); // 業務員姓名
        map.put("AGENT_NAME_3", STR); // 業務員姓名
        map.put("AMOUNT", STR); // 主約的保額
        map.put("BIG_PRODCATE", STR); // 主約的商品大類
        map.put("SMALL_PRODCATE", STR); // 主約的商品小類
        map.put("EACH_PREM", STR); // 每期保費

        return map;
    }

    public static Map<String, String> createTaskSevenHead() {
        // 適用於 080-7、080-8
        Map<String, String> map = new java.util.LinkedHashMap<String, String>();
        map.put("POLICY_CODE", STR); // 保單號碼
        map.put("INTERNAL_ID", STR); // 險種代號
        map.put("PH_NAME", STR); // 要保人姓名
        map.put("PH_CERTI_CODE", STR); // 要保人身份證字號
        map.put("PH_GENDER",STR); //要保人_姓別
        map.put("PH_BIRTHDAY", STR); // 要保人出生日期
        map.put("PH_OFFICE_TEL", STR); //要保人_連絡電話(日)
        map.put("PH_HOME_TEL", STR); //要保人_連絡電話(夜)
        map.put("PH_PHONE1", STR); //要保人_連絡電話(手機1)
        map.put("PH_PHONE2", STR); //要保人_連絡電話(手機2)
        map.put("I_NAME", STR); // 被保險人姓名
        map.put("I_CERTI_CODE", STR); // 被保險人身份證字號
        map.put("I_AGE", STR); // 被保險人投保年齡
        map.put("APPLY_DATE", STR); // 要保書申請日
        map.put("VALIDATE_DATE", STR); // 生效日
        map.put("ISSUE_DATE", STR); // 承保日
        map.put("DISPATCH_DATE", STR); // 保單寄送日
        map.put("REPLY_DATE", STR); // 簽收回條日
        map.put("SEND_TYPE", STR); // 保單遞送方式
        map.put("STATE", STR); // 保單狀況
        map.put("MONEY_NAME", STR); // 幣別
        map.put("EXCHANGE_RATE", STR); // 匯率
        map.put("CHARGE_NAME", STR); // 繳別 (Type_3)
        map.put("STD_PREM", STR); // 年繳化保費 (Type_3)
        map.put("STD_TW_PREM", STR); // 換算台幣後保費 (Type_3)
        map.put("CHANNEL_CODE", STR); // Channel
        map.put("CHANNEL_NAME", STR); // 單位
        map.put("AGENT_NAME_1", STR); // 業務員姓名
        map.put("AGENT_NAME_2", STR); // 業務員姓名
        map.put("AGENT_NAME_3", STR); // 業務員姓名
        map.put("AMOUNT", STR); // 主約的保額
        map.put("BIG_PRODCATE", STR); // 主約的商品大類
        map.put("SMALL_PRODCATE", STR); // 主約的商品小類
        map.put("EACH_PREM", STR); // 每期保費
        return map;
    }

    public static Map<String, String> createTaskNineHead() {
        // 適用於 080-9、080-10
        Map<String, String> map = new java.util.LinkedHashMap<String, String>();
        map.put("seq", STR);
        map.put("HANDLE_NAME", STR); //承辦人員姓名
        map.put("FIN_APP_TIME", STR);  //結案日
        map.put("SEND_TYPE_DESC", STR);
        map.put("SEND_PERSON_NAME", STR);
        map.put("CUST_APP_TIME", STR);
        map.put("VALIDATE_TIME", STR);
        map.put("POLICY_CODE", STR);
        map.put("VALIDATE_DATE", STR);
        map.put("LIABILITY_STATE_DESC", STR);
        map.put("END_CAUSE_DESC", STR);
        map.put("H_NAME", STR);
        map.put("H_CERTI_CODE", STR);
        map.put("H_BIRTH_DATE", STR);
        map.put("H_MOBILE_TEL", STR);
        map.put("I_NAME", STR);
        map.put("I_CERTI_CODE", STR);
        map.put("I_BIRTH_DATE", STR);
        map.put("I_MOBILE_TEL", STR);
        map.put("AGENT_NAME_1", STR); // 業務員姓名
        map.put("AGENT_NAME_2", STR); // 業務員姓名
        map.put("AGENT_NAME_3", STR); // 業務員姓名
        map.put("ALTERATION_ITEM_DESC", STR);
        map.put("END_CAUSE_DESC", STR); // 變更項目內容 
        return map;
    }

    public static Map<String, String> createTaskPOSHead() {
        // 適用於 POS-1、POS-2、POS-3、POS-4、POS-5
        Map<String, String> map = new java.util.LinkedHashMap<String, String>();
        map.put("seq", STR); // 序號
        map.put("HANDLE_NAME", STR); // 契變人員
        map.put("FIN_APP_TIME", STR); // 結案日
        map.put("SEND_TYPE_DESC", STR); // sendTypeName
        map.put("SEND_PERSON_NAME", STR); // 送件人員
        map.put("CUST_APP_TIME", STR); // 客戶申請日
        map.put("VALIDATE_TIME", STR); // 變更生效日
        map.put("POLICY_CODE", STR); // 保單號碼
        map.put("H_NAME", STR); // 要保人姓名
        map.put("H_CERTI_CODE", STR); // 要保人身分證字號
        map.put("H_BIRTH_DATE", STR); // 要保人出生日期
        map.put("H_MOBILE_TEL", STR); // 要保人聯絡電話
        map.put("I_NAME", STR); // 被保險人姓名
        map.put("I_CERTI_CODE", STR); // 被保險人身分證字號
        map.put("I_BIRTH_DATE", STR); // 被保險人出生日期
        map.put("I_MOBILE_TEL", STR); // 被保險人聯絡電話
        map.put("ALTERATION_ITEM_DESC", STR); // 辦理業務分類
        map.put("END_CAUSE_DESC", STR); // 變更項目內容 

        return map;
    }
    
    private static String doubleNumberFormat(double value) {
    	if(value == 0d)
    		return "0";
    	String result = String.format("%,f", value).replaceAll("0+$", "");
    	result = result.replaceAll("\\.0+$", "");
    	result = result.replaceAll("\\.$", "");
    	return result;
    }
    public static String getExeceptRule() {
        StringBuffer sql = new StringBuffer();
        // sql.append("and to_char(acknowledgement.dispatch_date,'yyyy-mm-dd') >= to_char(sysdate-1,'yyyy-mm-dd')   ");
        // sql.append("and to_char(acknowledgement.dispatch_date,'yyyy-mm-dd') < to_char(sysdate,'yyyy-mm-dd') ");
        sql.append("and master1.policy_id not in (  ");
        sql.append("select m1.policy_id ");
        sql.append("from t_contract_master m1 ");
        sql.append("join t_callout_trans trans on  trans.policy_id = m1.policy_id  ");
        sql.append("join t_policy_holder h2 on trans.HOLDER_CERTI_CODE = h2.certi_code  ");
        sql.append("where callout_target_type=1  ");
        sql.append(")and master1.policy_id not in(  ");
        sql.append("select m2.policy_id ");
        sql.append("from t_contract_master m2 ");
        sql.append("join t_policy_holder h1 on m2.policy_id = h1.policy_id ");
        sql.append("join t_contract_product cp1 on m2.policy_id = cp1.policy_id ");
        sql.append("join t_product_life PL1 on PL1.product_id = cp1.product_id ");
        sql.append("where ");
        sql.append("master1.policy_id <> m2.policy_id   ");
        sql.append("and insured.certi_code <> h1.certi_code     ");
        sql.append("and cp1.validate_date > sysdate - 90  ");
        sql.append("and substr( PL1.internal_id,0,3) = substr(p2.internal_id,0,3)  ");
        sql.append(") and master1.policy_id not in (  ");
        sql.append("select trans.policy_id ");
        sql.append("from t_callout_trans trans ");
        sql.append("where trans.callout_target_type=1 ");
        sql.append("and trans.holder_certi_code =  holder.certi_code   ");
        sql.append("and trans.CALLOUT_RESULT= 1 ");
        sql.append("and trans.CALLOUT_REQ_DEPT = '080' ");
        return sql.toString();
    }
    
}
