package com.ebao.ls.uw.ds;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import com.ebao.ls.foundation.service.GenericServiceImpl;
import com.ebao.ls.uw.data.UwLifeSurveyItemDelegate;
import com.ebao.ls.uw.data.bo.UwLifeSurveyItem;
import com.ebao.ls.uw.vo.UwLifeSurveyItemVO;
import com.ebao.pub.framework.GenericException;

/**
 * <p>Title: 核保頁面_生調交查選項列表</p>
 * <p>Description: 核保頁面_生調交查選項列表</p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company: TGL Co., Ltd.</p>
 * <p>Create Time: Apr 15, 2016</p> 
 * @author 
 * <p>Update Time: Apr 15, 2016</p>
 * <p>Updater: Alex Cheng</p>
 * <p>Update Comments: </p>
 */
public class UwLifeSurveyItemServiceImpl extends GenericServiceImpl<UwLifeSurveyItemVO, UwLifeSurveyItem, UwLifeSurveyItemDelegate> implements UwLifeSurveyItemService {

	@Resource(name = UwLifeSurveyItemDelegate.BEAN_DEFAULT)
	  private UwLifeSurveyItemDelegate uwUwLifeSurveyItemDao;
	
	@Override
	  protected UwLifeSurveyItemVO newEntityVO() {
	    return new UwLifeSurveyItemVO();
	  }

	@Override
	public UwLifeSurveyItemVO findUwLifeSurveyItemByLifeSurveyCode(Integer lifeSurveyCode) throws GenericException {
		UwLifeSurveyItemVO vo = null;
		if(lifeSurveyCode != null){
			UwLifeSurveyItem bo = uwUwLifeSurveyItemDao.findUwLifeSurveyItemByLifeSurveyCode(lifeSurveyCode);
			vo = convertToVO(bo);
		}
		return vo;
	}

	@Override
	public List<UwLifeSurveyItemVO> getUwLifeSurveyItems() throws GenericException {
		List<UwLifeSurveyItemVO> vos = new ArrayList<UwLifeSurveyItemVO>();
		List<UwLifeSurveyItem> bos = uwUwLifeSurveyItemDao.getUwLifeSurveyItems();
		if(bos != null && bos.size() > 0){
			vos = convertToVOList(bos);
		}
		return vos;
	}
}
