package com.ebao.ls.uw.ctrl.extraloading;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.ls.uw.ctrl.pub.CustomerForm;
import com.ebao.ls.uw.ctrl.pub.UwExtraLoadingForm;
import com.ebao.ls.uw.ds.UwPolicyService;
import com.ebao.ls.uw.ds.sp.QueryUwDataSp;
import com.ebao.ls.uw.vo.UwExtraLoadingVO;
import com.ebao.ls.uw.vo.UwPolicyVO;
import com.ebao.ls.uw.vo.UwProductVO;
import com.ebao.pub.framework.ExceptionFactory;
import com.ebao.pub.framework.GenericAction;
import com.ebao.pub.framework.GenericException;
import com.ebao.pub.util.BeanUtils;

/**
 * 
 * <p>Title:Underwriting</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: eBaoTech Corporation</p>
 * @author yixing.lu
 * @since  Jun 27, 2005
 * @version 1.0
 */
public class ShowReadOnlyExtraInfo extends GenericAction {

  /**
   * show read only extra infor
   *
   * @param mapping
   * @param form
   * @param req
   * @param res
   * @return
   * @throws GenericException
   */
  @Override
  public ActionForward process(ActionMapping mapping, ActionForm form,
      HttpServletRequest req, HttpServletResponse res) throws GenericException {
    // init the product and calculate the extra preminum
    try {
      Long policyId = Long.valueOf(req.getParameter("policyId"));
      Long itemId = Long.valueOf(req.getParameter("itemId"));
      Long insuredId = Long.valueOf(req.getParameter("insuredId"));
      req.setAttribute("insured", insuredId);
      UwProductVO productValue = initProductVO(policyId, itemId, insuredId);

      Collection insureds = uwPolicyDS.findUwLifeInsuredEntitis(
          productValue.getUnderwriteId(), itemId);
      insureds = BeanUtils.copyCollection(CustomerForm.class, insureds);
      CustomerForm[] insured = (CustomerForm[]) insureds
          .toArray(new CustomerForm[0]);
      req.setAttribute("insureds", insured);
      req.setAttribute("action_form", decompose(productValue, req));
    } catch (Exception e) {
      throw ExceptionFactory.parse(e);
    }
    return mapping.findForward("extraRead");
  }

  /**
   * init the product vo 
   * @param policyId
   * @param itemId
   * @param insuredId
   * @return
   * @throws Exception
   */
  public UwProductVO initProductVO(Long policyId, Long itemId, Long insuredId)
      throws Exception {
    Long underwriteId = uwPolicyDS.retrieveUwIdbyPolicyId(policyId);
    UwProductVO productValue = uwPolicyDS.findUwProduct(underwriteId, itemId);
    Collection allExtra = uwPolicyDS.findUwExtraLoadingEntitis(underwriteId,
        itemId, insuredId);
    if (allExtra != null) {
      Iterator iter = allExtra.iterator();
      List<UwExtraLoadingVO> vos = new ArrayList<UwExtraLoadingVO>(0);
      while (iter.hasNext()) {
        UwExtraLoadingVO extraLoading = (UwExtraLoadingVO) iter.next();
        vos.add(extraLoading);
      }
      productValue.setUwExtraLoadings(vos);
    }
    return productValue;
  }

  /**
   * get the UwExtraLoadingInfoForm value
   * @param productValue
   * @param req
   * @return
   * @throws Exception
   */
  public UwExtraLoadingInfoForm decompose(UwProductVO productValue,
      HttpServletRequest req) throws Exception {

    UwExtraLoadingInfoForm extraForm = new UwExtraLoadingInfoForm();

    UwPolicyVO policyVO = uwPolicyDS.findUwPolicy(productValue
        .getUnderwriteId());
    req.setAttribute("uwSourceType", policyVO.getUwSourceType());
    extraForm.setApplyCode(policyVO.getApplyCode());
    extraForm.setPolicyCode(policyVO.getPolicyCode());
    extraForm.setBenefityType(policyVO.getBenefitType() + "");
    extraForm.setUwStatus(policyVO.getUwStatus());
    try {
      BeanUtils.copyProperties(extraForm.getUwProductForm(), productValue);
    } catch (Exception e) {
      throw ExceptionFactory.parse(e);
    }
    //  get the default charge period
    int chargeYear = QueryUwDataSp.getChargeTerm(productValue.getItemId());
    extraForm.getUwProductForm().setChargeYear(Integer.valueOf(chargeYear));

    extraForm.setDuration(chargeYear + "");

    List<UwExtraLoadingVO> allExtra = productValue.getUwExtraLoadings();
    if (allExtra != null) {
      for (int i = 0; i < allExtra.size(); i++) {
        UwExtraLoadingVO extraLoading = allExtra.get(i);
        UwExtraLoadingForm extraLoadingForm = new UwExtraLoadingForm();
        if (extraLoading != null) {
          BeanUtils.copyProperties(extraLoadingForm, extraLoading);
          if (!"".equals(extraLoading.getExtraType())) {
            if (extraLoading.getExtraType() != null) {
              switch (extraLoading.getExtraType().charAt(0)) {
              case 'A':
                extraForm.getHealth().add(extraLoadingForm);
                extraForm.setHealthFlag("Y");
                break;
              case 'B':
                extraForm.getOccupation().add(extraLoadingForm);
                extraForm.setOccupationFlag("Y");
                break;
              case 'C':
                extraForm.getAvocation().add(extraLoadingForm);
                extraForm.setAvocationFlag("Y");
                break;
              case 'D':
                extraForm.getResidential().add(extraLoadingForm);
                extraForm.setResidentialFlag("Y");
                break;
              case 'E':
                extraForm.getAviation().add(extraLoadingForm);
                extraForm.setAviationFlag("Y");
                break;
              case 'F':
                extraForm.getOther().add(extraLoadingForm);
                extraForm.setOtherFlag("Y");
                break;
              }
            }
          }
        }
      }
    }

    if (extraForm.getHealth().size() == 0) {
      extraForm.getHealth().add(new UwExtraLoadingForm());
    }
    if (extraForm.getOccupation().size() == 0) {
      extraForm.getOccupation().add(new UwExtraLoadingForm());
    }
    if (extraForm.getAvocation().size() == 0) {
      extraForm.getAvocation().add(new UwExtraLoadingForm());
    }
    if (extraForm.getResidential().size() == 0) {
      extraForm.getResidential().add(new UwExtraLoadingForm());
    }
    if (extraForm.getAviation().size() == 0) {
      extraForm.getAviation().add(new UwExtraLoadingForm());
    }
    if (extraForm.getOther().size() == 0) {
      extraForm.getOther().add(new UwExtraLoadingForm());
    }
    return extraForm;
  }

  @Resource(name = UwPolicyService.BEAN_DEFAULT)
  private UwPolicyService uwPolicyDS;

  public UwPolicyService getUwPolicyDS() {
    return uwPolicyDS;
  }

  public void setUwPolicyDS(UwPolicyService uwPolicyDS) {
    this.uwPolicyDS = uwPolicyDS;
  }

}
