package com.ebao.ls.riskPrevention.ctrl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.Factory;
import org.apache.commons.collections.ListUtils;

import com.ebao.ls.riskPrevention.vo.NationalityCodeVO;
import com.ebao.pub.web.pager.PagerFormImpl;

public class NationalityCodeNewForm extends PagerFormImpl {
	
	private static final long serialVersionUID = -3555360618194492463L;
	
	private NationalityCodeVO vo = new NationalityCodeVO();
	
	@SuppressWarnings("unchecked")
	private List<NationalityCodeVO> voList =  ListUtils.lazyList(new ArrayList<NationalityCodeVO>(),
		new Factory(){
			@Override
			public NationalityCodeVO create() {
				return new NationalityCodeVO();
			}
		}
	);
	
	public List<NationalityCodeVO> getVoList() {
		return voList;
	}
	public void setVoList(List<NationalityCodeVO> voList) {
		this.voList = voList;
	}
	public NationalityCodeVO getVo() {
		return vo;
	}
	public void setVo(NationalityCodeVO vo) {
		this.vo = vo;
	}

}
