package com.ebao.ls.subprem.ctrl.extraction;

import java.util.Date;

import com.ebao.pub.framework.GenericForm;
/**
 * <p>Title:     </p>
 * <p>Description:   </p>
 * <p>Copyright: Copyright (c) 2002 </p>
 * <p>Company:  eBaoTech   </p>
 * <p>Create Time:  </p>
 * @author
 * @version
 */

public class UpdateForm extends GenericForm {
  String policyNumber;
  String policyNumberD;
  String policyHolderName;
  String policyHolderNameD;
  String firstName;
  String lastName;
  String payMethod;
  String dueDate;
  Date exDueDate;
  String policyStatus;
  String eventType;
  String policyId;
  String loadSource;

  public UpdateForm() {
  }

  //PolicyNumber
  public String getPolicyNumber() {
    return policyNumber;
  }
  public void setPolicyNumber(String policyNumber) {
    this.policyNumber = policyNumber;
  }
  //PolicyNumberD
  public String getPolicyNumberD() {
    return policyNumberD;
  }
  public void setPolicyNumberD(String policyNumberD) {
    this.policyNumberD = policyNumberD;
  }
  //policyHolderName
  public String getPolicyHolderName() {
    return policyHolderName;
  }
  public void setPolicyHolderName(String policyHolderName) {
    this.policyHolderName = policyHolderName;
  }
  //policyHolderNameD
  public String getPolicyHolderNameD() {
    return policyHolderNameD;
  }
  public void setPolicyHolderNameD(String policyHolderNameD) {
    this.policyHolderNameD = policyHolderNameD;
  }
  //payMethod
  public String getPayMethod() {
    return payMethod;
  }
  public void setPayMethod(String payMethod) {
    this.payMethod = payMethod;
  }
  //dueDate
  public String getDueDate() {
    return dueDate;
  }
  public void setDueDate(String dueDate) {
    this.dueDate = dueDate;
  }
  //exDueDate
  public Date getExDueDate() {
    return exDueDate;
  }
  public void setExDueDate(Date exDueDate) {
    this.exDueDate = exDueDate;
  }
  //policyStatus
  public String getPolicyStatus() {
    return policyStatus;
  }
  public void setPolicyStatus(String policyStatus) {
    this.policyStatus = policyStatus;
  }

  //policyId
  public String getPolicyId() {
    return policyId;
  }
  public void setPolicyId(String policyId) {
    this.policyId = policyId;
  }

  //loadSource
  public String getLoadSource() {
    return loadSource;
  }
  public void setLoadSource(String loadSource) {
    this.loadSource = loadSource;
  }

  //eventType
  public String getEventType() {
    return eventType;
  }
  public void setEventType(String eventType) {
    this.eventType = eventType;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

}
