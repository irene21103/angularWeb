package com.ebao.ls.uw.ds;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.ebao.ls.cmu.bs.document.DocumentService;
import com.ebao.ls.cmu.pub.data.bo.Document;
import com.ebao.ls.notification.message.WSItemString;
import com.ebao.ls.pa.nb.bs.rule.ProposalRuleResultService;
import com.ebao.ls.pa.nb.data.UwMedicalItemDao;
import com.ebao.ls.pa.nb.data.UwMedicalLetterDao;
import com.ebao.ls.pa.nb.data.UwMedicalReasonDao;
import com.ebao.ls.pa.nb.vo.ProposalRuleResultVO;
import com.ebao.ls.pa.pub.data.bo.UwMedicalItem;
import com.ebao.ls.pa.pub.data.bo.UwMedicalLetter;
import com.ebao.ls.pa.pub.data.bo.UwMedicalReason;
import com.ebao.ls.pa.pub.vo.UwMedicalItemVO;
import com.ebao.ls.pa.pub.vo.UwMedicalLetterVO;
import com.ebao.ls.pty.data.query.MedicalFeeDao;
import com.ebao.ls.pub.CodeCst;
import com.ebao.ls.uw.data.TUwPolicyDelegate;
import com.ebao.ls.uw.data.bo.UwPolicy;
import com.ebao.ls.uw.ds.vo.UwIssueMedicalItemVO;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.framework.GenericDS;
import com.ebao.pub.framework.GenericException;
import com.ebao.pub.i18n.lang.StringResource;
import com.ebao.pub.util.StringUtils;

/**
 * <p>
 * Title:Underwriting
 * </p>
 * <p>
 * Description:
 * </p>
 * <p>
 * Company: eBaoTech Corporation
 * </p>
 */
public class UwMedicalLetterDSImpl extends GenericDS implements UwMedicalLetterService {

	@Resource(name = UwMedicalItemDao.BEAN_DEFAULT)
	private UwMedicalItemDao<UwMedicalItem> uwMedicalItemDao;

	@Resource(name = UwMedicalLetterDao.BEAN_DEFAULT)
	private UwMedicalLetterDao<UwMedicalLetter> uwMedicalLetterDao;

	@Resource(name = UwMedicalReasonDao.BEAN_DEFAULT)
	private UwMedicalReasonDao<UwMedicalReason> uwMedicalReasonDao;

	@Resource(name = TUwPolicyDelegate.BEAN_DEFAULT)
	private TUwPolicyDelegate tUwPolicyDelegate;

	//分隔符號，用於分隔多個檢查醫院
	private static final String DELIMITER = "、";

	/**
	 * <p>Description : 核保問題頁面_體檢照會列表 </p>
	 * <p>Created By : Alex Cheng</p>
	 * <p>Create Time : Mar 17, 2016</p>
	 * @param policyId
	 * @param underwriteId
	 * @return
	 */
	@Override
	public List<UwIssueMedicalItemVO> getMedicalIssueListByUnderwriteId(Long policyId, Long underwriteId) {
		return uwMedicalItemDao.getUwMedicalItemListByUnderwriteId(policyId, underwriteId);
	}

	/**
	 * <p>Description : 更新UwMedicalItem狀態</p>
	 * <p>Created By : Alex Cheng</p>
	 * <p>Create Time : Mar 17, 2016</p>
	 * @param uwMedicalItemListId
	 * @param status
	 * @return
	 */
	@Override
	public UwMedicalItemVO updateUwMedicalItemByListId(Long uwMedicalItemListId, Integer status) {
		UwMedicalItemVO vo = null;
		if (uwMedicalItemListId != null) {
			vo = new UwMedicalItemVO();
			UwMedicalItem bo = (UwMedicalItem) uwMedicalItemDao.load(uwMedicalItemListId);
			bo.copyToVO(vo, false);
			if (bo != null && (CodeCst.PROPOSAL_RULE_STATUS__PENDING == bo.getStatus()) && !bo.getStatus().equals(status)) {
				bo.setStatus(status);
				uwMedicalItemDao.saveorUpdate(bo);
				bo.copyToVO(vo, false);
			}
		}
		return vo;
	}

	@Override
	public UwMedicalLetterVO findMedicalLetterByListId(Long listId) {
		UwMedicalLetterVO vo = null;
		UwMedicalLetter bo = (UwMedicalLetter) uwMedicalLetterDao.load(listId);
		if (bo != null) {
			vo = new UwMedicalLetterVO();
			bo.copyToVO(vo, true);
		}
		return vo;
	}

	/**
	 *   <p>Description : 核保體檢照會表(查詢)</p>
	 * <p>Created By : victor.chang</p>
	 * <p>Create Time : Mar 7, 2016</p>
	 * @param underwriteId
	 * @param documentId 
	 * @param insuredId
	 * @throws GenericException
	
	public UwMedicalLetter findMedicalLetter(Long underwriteId)
	                                throws GenericException {
	    UwMedicalLetter bo=uwMedicalLetterDao.findMedicalLetter(underwriteId);
	    return bo;
	}
	  */

	public List<UwPolicy> findByPolicyId(java.lang.Long policyId) throws GenericException {
		List<UwPolicy> po = tUwPolicyDelegate.findByPolicyId(policyId);
		return po;
	}

	public List<Map<String, String>> findMedicalItem(Long medicalLetterId) throws GenericException {
		List<Map<String, String>> po = uwMedicalItemDao.findMedicalItem(medicalLetterId);
		return po;
	}

	public List<Map<String, String>> findMedicalReason(Long medicalLetterId) throws GenericException {
		List<Map<String, String>> po = uwMedicalReasonDao.findMedicalReason(medicalLetterId);
		return po;
	}

	public UwMedicalLetter findByPolicyInsured(Long underwrtieId, Long insuredId) throws GenericException {

		UwMedicalLetter bo = uwMedicalLetterDao.findByPolicyInsured(underwrtieId, insuredId);
		return bo;
	}

	public UwMedicalLetter findByPolicyInsured(Long underwrtieId, Long changeId, Long insuredId) throws GenericException {

		UwMedicalLetter bo = uwMedicalLetterDao.findByPolicyInsured(underwrtieId, changeId, insuredId);
		return bo;
	}

	//public void updateDocumentId(Long listId, Long documentId){
	//    uwMedicalLetterDao.updateDocumentId(listId,documentId);
	//}

	public Long updateUwMedicalLetter(UwMedicalLetterVO vo, String[] reasonCodes, String[] medicalItems, String[] newItemNames, String[] specMenos) throws GenericException {
		UwMedicalLetter bo = new UwMedicalLetter();
		Long retListId = null;

		if (vo.getListId() != null && vo.getListId().longValue() > 0) {
			uwMedicalReasonDao.deleteItem(vo.getListId().longValue());
			uwMedicalItemDao.deleteItem(vo.getListId().longValue());
			bo = (UwMedicalLetter) uwMedicalLetterDao.load(vo.getListId());
			bo.setDocumentId(vo.getDocumentId());
			bo.setHospitalTxt(vo.getHospitalTxt());
			bo.setInsuredId(vo.getInsuredId());
			bo.setIsContraHospital(vo.getIsContraHospital());
			bo.setIsHospitalTxt(vo.getIsHospitalTxt());
			bo.setIsPaySelf(vo.getIsPaySelf());
			bo.setNotice(vo.getNotice());
			bo.setOtherReason(vo.getOtherReason());
			bo.setStatus(vo.getStatus());
			bo.setUnderwriteId(vo.getUnderwriteId());
			uwMedicalLetterDao.saveorUpdate(bo);
			retListId = bo.getListId();
		} else {
			bo.setDocumentId(vo.getDocumentId());
			bo.setHospitalTxt(vo.getHospitalTxt());
			bo.setInsuredId(vo.getInsuredId());
			bo.setIsContraHospital(vo.getIsContraHospital());
			bo.setIsHospitalTxt(vo.getIsHospitalTxt());
			bo.setIsPaySelf(vo.getIsPaySelf());
			bo.setNotice(vo.getNotice());
			bo.setOtherReason(vo.getOtherReason());
			bo.setStatus(vo.getStatus());
			bo.setUnderwriteId(vo.getUnderwriteId());

			uwMedicalLetterDao.save(bo);
			retListId = bo.getListId();
		}
		int length = 0;
		if (reasonCodes != null) {
			length = reasonCodes.length;
			for (int i = 0; i < length; i++) {
				UwMedicalReason rBo = new UwMedicalReason();
				rBo.setMedicalLetterId(bo.getListId());
				rBo.setReasonCode(reasonCodes[i]);
				uwMedicalReasonDao.save(rBo);
			}
		}
		// [artf410579](Internal Issue 210341)[UAT2][eBao]體檢照會暫存後關閉,再按一次體檢--特殊體檢項目被清空不見了
		if (medicalItems != null) {
			length = medicalItems.length;
			for (int i = 0; i < length; i++) {
				UwMedicalItem iBo = new UwMedicalItem();
				iBo.setMedicalLetterId(bo.getListId());
				iBo.setMedicalItem(medicalItems[i]);
				if (STATUS_SEND.equals(bo.getStatus())) {
					iBo.setStatus(1);
				}
				uwMedicalItemDao.save(iBo);
			}
		}
		if (newItemNames != null) {
			length = newItemNames.length;
			for (int i = 0; i < length; i++) {
				if (!"".equals(newItemNames[i])) {
					UwMedicalItem iBo = new UwMedicalItem();
					iBo.setMedicalLetterId(bo.getListId());

					iBo.setMedicalItem(newItemNames[i]);
					if(StringUtils.isNullOrEmpty(specMenos[i].toString())){
						iBo.setSpecMeno(" ");
					}else {
						iBo.setSpecMeno(specMenos[i]);
					}
					if (STATUS_SEND.equals(bo.getStatus())) {
						iBo.setStatus(1);
					}
					uwMedicalItemDao.save(iBo);
				}
			}
		}
		//待reason存檔，再次執行是否需更新為人工體檢件
	    uwMedicalLetterDao.saveorUpdate(bo);
	    
		return retListId;
	}

	@Override
	public List<UwIssueMedicalItemVO> getUwIssueMedicalItemByPolicyId(Long policyId) {
		return uwMedicalItemDao.getUwMedicalItemByPolicyId(policyId);
	}

	@Override
	public void updateUwMedicalLetterMsgAndDocument(Long listId, Long pListId, Long documentId) {
		uwMedicalLetterDao.updateMsgIdDocumentId(listId, pListId, documentId);
	}

	@Override
	public UwMedicalLetter findMedicalLetter(Long underwriteId) throws GenericException {
		return null;
	}

	@Override
	public List<UwMedicalLetterVO> queryAllMedicalLetter(Long underwriteId, Long insuredId) {
		List<UwMedicalLetter> list = uwMedicalLetterDao.queryAllMedicalLetter(underwriteId, insuredId);
		List<UwMedicalLetterVO> returnList = new ArrayList<UwMedicalLetterVO>();
		if (list == null || list.isEmpty()) {
			return returnList;
		}
		for (UwMedicalLetter bo : list) {
			UwMedicalLetterVO vo = new UwMedicalLetterVO();
			bo.copyToVO(vo, true);
			returnList.add(vo);
		}
		return returnList;
	}

	/**
	 * <p>Description : 取得顯示在照會上的文字</p>
	 * <p>Created By : Duke Hsiao</p>
	 * <p>Create Time : June 29, 2016</p>
	 * @param listId
	 * @return
	 */
	@Override
	public List<WSItemString> getLetterText(Long listId) {
		//List<String> list = uwMedicalLetterDao.getMedicalLetterTextByDocumentId(listId);
		//		Map<String, String> feeLetterText = MedicalFeeDao.getFeeLetterText();
		//
		List<WSItemString> item = new ArrayList<WSItemString>();
		//		int prefix = 1;
		//		for(String each:list){
		//			item.add(new WSItemString(String.valueOf(prefix++),each.substring(0,each.indexOf(","))));
		//		}
		//
		//		for(int i = 0 ; i < item.size() ; i++){
		//			String detal = item.get(i).getContents().trim();
		//			for(String key:feeLetterText.keySet()){
		//				if(detal.equals(key.trim())){
		//					item.set(i, new WSItemString(item.get(i).getPrefix(),feeLetterText.get(detal)));
		//					break;
		//				}
		//				else{
		//					item.set(i, new WSItemString(item.get(i).getPrefix(),list.get(i)));
		//				}
		//			}
		//
		//		}
		return item;

	}

	/**
	 * <p>Description : FMT_0782_取得補充說明、檢查醫院</p>
	 * <p>Created By : Duke Hsiao</p>
	 * <p>Create Time : July 29, 2016</p>
	 * @param listId
	 * @return
	 */
	@Override
	public List<StringBuffer> getDetailInfoHospitalByDocumentId(Long documentId) {

		List<UwMedicalLetter> uwMedicalLetters = uwMedicalLetterDao.getDetailInfoHospitalByDocumentId(documentId);
		UwMedicalLetter uwMedicalLetter = uwMedicalLetters.get(0);

		//檢查方式
		StringBuffer txtBuffer = new StringBuffer();
		List<String> strs = new ArrayList<String>();
		if ("Y".equals(uwMedicalLetter.getIsPaySelf())) {
			//自費
			strs.add(StringResource.getStringData("MSG_1251741", AppContext.getCurrentUser().getLangId()));
		}
		if ("Y".equals(uwMedicalLetter.getIsContraHospital())) {
			//公司簽約之體檢醫院
			strs.add(StringResource.getStringData("MSG_1256976", AppContext.getCurrentUser().getLangId()));
		}
		if ("Y".equals(uwMedicalLetter.getIsHospitalTxt())) {
			//請至 + HOSPITAL_TXT(指定體檢地點內容)
			strs.add(StringResource.getStringData("MSG_1256975", AppContext.getCurrentUser().getLangId()) + uwMedicalLetter.getHospitalTxt().trim());
		}

		int i = strs.size();
		switch (i) {
			case 3:
				txtBuffer.append(DELIMITER + strs.get(2));
			case 2:
				txtBuffer.insert(0, DELIMITER + strs.get(1));
			case 1:
				txtBuffer.insert(0, DELIMITER + strs.get(0));
			default:
				break;
		}

		//補充說明
		StringBuffer notice = new StringBuffer();
		notice.append(uwMedicalLetter.getNotice());

		List<StringBuffer> list = new ArrayList<StringBuffer>();
		list.add(notice);
		list.add(txtBuffer);

		return list;
	}

	@Resource(name = ProposalRuleResultService.BEAN_DEFAULT)
	private ProposalRuleResultService proposalRuleResultService;

	@Resource(name = DocumentService.BEAN_DEFAULT)
	private DocumentService documentService;

	@Override
	/**
	 * 將照會改為刪除
	 */
	public void drop(UwMedicalLetterVO vo) {
		Long letterId = vo.getListId();
		UwMedicalLetter bo = (UwMedicalLetter) uwMedicalLetterDao.load(letterId);
		bo.setStatus(STATUS_DROP);
		uwMedicalLetterDao.save(bo);
		/**將letter下所有的體檢項目狀態改為系統關閉**/
		uwMedicalItemDao.updateItemStatusByLetterId(letterId, CodeCst.PROPOSAL_RULE_STATUS__SYSTEMCLOSED);
		if (vo.getDocumentId() != null) {
			/**將照會狀態改為已刪除**/
			Document document = documentService.getDocument(vo.getDocumentId());
			document.setStatus(CodeCst.DOCUMENT_STATUS__DELETED);
			document.setLetterStatus(CodeCst.LETTER_STATUS_DELETED);
			documentService.updateDocument(document);
		}
		if (vo.getMsgId() != null) {
			/**將msg狀態改為系統關閉**/
			ProposalRuleResultVO ruleVO = proposalRuleResultService.load(vo.getMsgId());
			ruleVO.setRuleStatus(CodeCst.PROPOSAL_RULE_STATUS__SYSTEMCLOSED);
			proposalRuleResultService.save(ruleVO);
		}
	}

	@Override
	/**取得保單下暫存或batch發放的體檢letter**/
	public Map<Long, UwMedicalLetterVO> getSaveOrBatchLetter(Long underwriteId) {
		/**查所有暫存貨已發送letter**/
		List<UwMedicalLetter> letters = uwMedicalLetterDao.querySaveOrSendLetter(underwriteId, new String[] { STATUS_SAVE, STATUS_SEND });
		Map<Long, UwMedicalLetterVO> map = new HashMap<Long, UwMedicalLetterVO>();
		for (UwMedicalLetter bo : letters) {
			/**只取最晚的一筆, 所以有查到就不用再放入map**/
			Long insuredId = bo.getInsuredId();
			if (map.get(insuredId) == null) {
				UwMedicalLetterVO vo = new UwMedicalLetterVO();
				bo.copyToVO(vo, true);
				map.put(insuredId, vo);
			}
		}

		return map;
	}

	@Override
	/**
	 * 取得所有被保人由規則校驗產生的體檢項目
	 */
	public Map<Long, UwMedicalLetterVO> getDefaultMap(Long underwriteId) {
		List<UwMedicalLetter> letters = uwMedicalLetterDao.queryAllMedicalLetter(underwriteId, null, STATUS_DEFAULT);
		Map<Long, UwMedicalLetterVO> map = new HashMap<Long, UwMedicalLetterVO>();
		for (UwMedicalLetter bo : letters) {
			UwMedicalLetterVO vo = new UwMedicalLetterVO();
			bo.copyToVO(vo, true);
			map.put(vo.getInsuredId(), vo);
		}

		return map;
	}

	@Override
	/**
	 * 更新由規則校驗所產生的體檢項目
	 */
	public void updateDefaultItem(Long underwriteId, Long insuredId,
			List<String> newItemList) {
		List<UwMedicalLetter> letters = uwMedicalLetterDao.queryAllMedicalLetter(underwriteId, insuredId, STATUS_DEFAULT);
		UwMedicalLetterVO letterVO = new UwMedicalLetterVO();
		if (letters.isEmpty()) {
			letterVO.setInsuredId(insuredId);
			letterVO.setUnderwriteId(underwriteId);
			letterVO.setIsContraHospital(CodeCst.YES_NO__YES);
			letterVO.setStatus(STATUS_DEFAULT);
		} else {
			letters.get(0).copyToVO(letterVO, true);
			uwMedicalItemDao.deleteItem(letterVO.getListId());
		}
		String[] itemArray = new String[] {};
		if (newItemList != null) {
			itemArray = newItemList.toArray(new String[newItemList.size()]);
		}
		this.updateUwMedicalLetter(letterVO, new String[] {}, itemArray, new String[] {}, null);

	}

	@Override
	public List<WSItemString> getLetterText(String[] list) {
		List<WSItemString> items = new ArrayList<WSItemString>();
		Map<String, String> feeLetterText = MedicalFeeDao.getFeeLetterText();
		int prefix = 1;
		for (String each : list) {
			String item = each.substring(0, each.indexOf(","));
			String letterTxt = feeLetterText.get(item);
			//item.add(new WSItemString(null, each));
			items.add(new WSItemString(String.valueOf(prefix), letterTxt == null ? item : letterTxt));
			prefix++;
		}
		return items;

	}

	/**
	 * <p>Description : 核保中修改移除被保險人，同步移除體檢照會及項目</p>
	 * <p>Created By : simon.huang</p>
	 * <p>Create Time : Jun 28, 2017</p>
	 * @param insuredId
	 */
	@Override
	public void removeByInsured(Long insuredId) {
		List<UwMedicalLetter> letterList = uwMedicalLetterDao.queryAllMedicalLetter(null, insuredId);
		for (UwMedicalLetter letter : letterList) {
			uwMedicalReasonDao.deleteItem(letter.getListId());
			uwMedicalItemDao.deleteItem(letter.getListId());
			uwMedicalLetterDao.remove(letter);
		}
	}

	/**
	 * <p>Description : 取得體檢信函照會狀態</p>
	 * <p>Created By : Sun Yu</p>
	 * <p>Create Time : Oct 03, 2018</p>
	 * @param documentId
	 */
	public String getDocumentLetterStatusFixByDocumentId(Long documentId ) {
		return uwMedicalLetterDao.getDocumentLetterStatusFixByDocumentId(documentId);
	}
}