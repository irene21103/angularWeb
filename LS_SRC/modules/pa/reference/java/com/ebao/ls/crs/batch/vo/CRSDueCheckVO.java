package com.ebao.ls.crs.batch.vo;

import com.ebao.ls.fatca.vo.FatcaDueDiligenceListVO;

public class CRSDueCheckVO extends FatcaDueDiligenceListVO {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3967138684403843003L;

	private String reSixIndi;

	public String getReSixIndi() {
		return reSixIndi;
	}

	public void setReSixIndi(String reSixIndi) {
		this.reSixIndi = reSixIndi;
	}

	
	

}
