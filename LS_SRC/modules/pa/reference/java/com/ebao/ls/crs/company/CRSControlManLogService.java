package com.ebao.ls.crs.company;

import java.util.List;

import com.ebao.ls.crs.vo.CRSControlManLogVO;
import com.ebao.ls.crs.vo.CRSManAccountLogVO;
import com.ebao.ls.foundation.service.GenericService;

public interface CRSControlManLogService extends GenericService<CRSControlManLogVO> {

    public final static String BEAN_DEFAULT = "crsControlManLogService";

    /**
     * Description : 查詢同一聲明書的具控制權人<br>
     * Created By : Simon.Huang<br>
     * Create Time : 2018年12月18日<br>
     *
     * @param partyLogId
     * @param loadOtherTable
     * @return
     */
    public List<CRSControlManLogVO> findByPartyLogId(Long partyLogId, boolean loadOtherTable);

    /**
     * Description : 依頁次查詢具控制權人<br>
     * Created By : Simon.Huang<br>
     * Create Time : 2018年12月18日<br>
     *
     * @param partyLogId
     * @param copy
     * @param loadOtherTable
     * @return
     */
    public CRSControlManLogVO findByPartyLogIdAndCopy(Long partyLogId, Integer copy, boolean loadOtherTable);

    /**
     * Description : 具控制權人實體帳戶刪除<br>
     * Created By : Simon.Huang<br>
     * Create Time : 2018年12月18日<br>
     *
     * @param logId
     */
    public void removeAccountLog(Long logId);

    /**
     * Description : 具控制權人實體帳戶存檔<br>
     * Created By : Simon.Huang<br>
     * Create Time : 2018年12月18日<br>
     *
     * @param accountLogVO
     * @return
     */
    public CRSManAccountLogVO saveAccountLog(CRSManAccountLogVO accountLogVO);

    /**
     * Description : 取得最大頁次<br>
     * Created By : Simon.Huang<br>
     * Create Time : 2018年12月18日<br>
     *
     * @param partyLogId
     * @return
     */
    public Integer getMaxCopy(Long partyLogId);

    /**
     * Description : 取得最小頁次<br>
     * Created By : Simon.Huang<br>
     * Create Time : 2018年12月18日<br>
     *
     * @param partyLogId
     * @return
     */
    public Integer getMinCopy(Long partyLogId);

    /**
     * Description : 因新契約輸入具控制權人無parentId，於核保決定後重新編排新契約parentId<br>
     * Created By : Simon.Huang<br>
     * Create Time : 2018年12月18日<br>
     *
     * @param partyLogId
     */
    public void syncUNBCtrlManByName(Long partyLogId);

    public List<CRSControlManLogVO> findByChangeId(Long changeId);

}
