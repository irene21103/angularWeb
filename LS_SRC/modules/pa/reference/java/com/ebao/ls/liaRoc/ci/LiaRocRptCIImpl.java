package com.ebao.ls.liaRoc.ci;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import javax.activation.DataHandler;
import javax.annotation.Resource;

import com.ebao.foundation.common.lang.exception.ExceptionUtils;
import com.ebao.ls.pa.pub.service.ReportService;
import com.ebao.ls.report.process.IReportProcess;
import com.ebao.ls.report.process.nb.RptUnb0080;
import com.ebao.ls.ws.ci.cr.CrystalReportWSCI;
import com.ebao.ls.ws.ci.sip.SipWSCI;
import com.ebao.pub.framework.AppContext;

public class LiaRocRptCIImpl implements LiaRocRptCI {


	
	/**
     * LS_UNB_0080 公會上傳偵錯報表
     * 報表歸檔與寄送Email
     * @author Poly
     */
	@Override
    public void archiveAndSendRpt0080(String rqUID) {
		try {
			log(String.format("RqUID::%s ArchiveFileAndEmail start...", rqUID));
			DataHandler dataHandler = null;
			Long folderId = 4319L;
			Map<String, String> reportParameters = new HashMap<String, String>();
			reportParameters.put("4565", String.valueOf(AppContext.getCurrentUser().getUserId()));//使用者
			reportParameters.put("4566", "");//身分證字號
			reportParameters.put("8156", "");//保單號碼
			reportParameters.put("8157", "");//偵錯日期(起)民國年
			reportParameters.put("8158", "");//偵錯日期(迄)民國年
			reportParameters.put("14455", rqUID);//RqUID
			
			//取得CMC產出的報表
			dataHandler = reportService.getRptDataHandler(folderId, reportParameters, CrystalReportWSCI.PDF);
			
			//拆檔寄送Email
			String reportName = reportService.getFolderName(folderId);
			sipWSCI.archiveFileAndEmail(dataHandler, reportName + ".pdf", process);
			
			log(String.format("RqUID::%s ArchiveFileAndEmail end... ", rqUID));
			
		} catch (Exception e) {
			String error = ExceptionUtils.getStackTrace(e);
			log(String.format("RqUID::%s ArchiveFileAndEmail error...\r\nException: %s", rqUID, error));
		}
    	
    }
    
    private void log(String msg) {
    	Logger.getLogger(this.getClass().getName()).info(msg);
	}
	
	
	
	
	
	 @Resource(name = SipWSCI.BEAN_DEFAULT)
	 private SipWSCI sipWSCI;
	    
	 @Resource(name = RptUnb0080.BEAN_DEFAULT)
	 private IReportProcess process;
	 
	 @Resource(name = ReportService.BEAN_DEFAULT)
	 private ReportService reportService;

}
