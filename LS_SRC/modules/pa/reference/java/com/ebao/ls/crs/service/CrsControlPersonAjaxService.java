package com.ebao.ls.crs.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanComparator;
import org.apache.log4j.Logger;

import com.ebao.foundation.module.db.hibernate.HibernateSession3;
import com.ebao.ls.crs.constant.CRSTinBizSource;
import com.ebao.ls.crs.vo.CRSControlManLogVO;
import com.ebao.ls.crs.vo.CRSManAccountLogVO;
import com.ebao.ls.crs.vo.CRSPartyLogVO;
import com.ebao.ls.crs.vo.CRSTaxIDNumberLogVO;
import com.ebao.ls.pa.nb.util.NBUtils;
import com.ebao.ls.pa.nb.util.NumericUtils;
import com.ebao.ls.pa.pub.MsgCst;
import com.ebao.pub.util.json.EbaoStringUtils;

import edu.emory.mathcs.backport.java.util.Arrays;

/**
 * <p>Title: CRS 聲明書-PCR_264035&263803_CRS專案</p>
 * <p>Description: 具控制權人</p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: TGL Co., Ltd.</p>
 * <p>Create Time: 2018/11/08</p> 
 * @author 
 * <p>Update Time: 2018/11/08</p>
 * <p>Updater: simon.huang</p>
 * <p>Update Comments: </p>
 */
public class CrsControlPersonAjaxService extends CrsAjaxService {

	public static Logger logger = Logger.getLogger(CrsControlPersonAjaxService.class);

	/**
	 * <p>Description : 具控制權人 </p>
	 * <p>Created By : Simon.Huang</p>
	 * <p>Create Time : 2018年11月9日</p>
	 * @param in
	 * @param output
	 * @throws Exception
	 */
	public void init(Map<String, Object> in, Map<String, Object> output) throws Exception {

		Long partyLogId = NumericUtils.parseLong(in.get("partyLogId"));
		Integer copy = NumericUtils.parseInteger(in.get("copy"));
		
		if (copy == null) {
			copy = crsControlManLogService.getMinCopy(partyLogId); //從db最小copy開始
		}

		if (partyLogId != null) {
			CRSPartyLogVO partyLogVO = crsPartyLogService.load(partyLogId);

			CRSControlManLogVO controlLogVO = null;
			List<CRSTaxIDNumberLogVO> taxLogList = new ArrayList<CRSTaxIDNumberLogVO>();
			List<CRSManAccountLogVO> accountLogList = new ArrayList<CRSManAccountLogVO>();
			if (partyLogVO != null) {
				controlLogVO = crsControlManLogService.findByPartyLogIdAndCopy(partyLogId, copy, true);
				if (controlLogVO != null) {
					taxLogList = crsTaxLogService.findByPartyLogIdAndBizId(partyLogId, CRSTinBizSource.BIZ_SOURCE_CTRL_MAN, controlLogVO.getLogId());
					accountLogList = controlLogVO.getCrsManAccountLog();
			
				}
			} else {
				output.put(KEY_SUCCESS, FAIL);
				output.put(KEY_ERROR_MSG, NBUtils.getTWMsg(MsgCst.MSG_1250566));
				return;
			}
			if (controlLogVO == null) {
				controlLogVO = new CRSControlManLogVO();
			}

			//中斷controlLogVO與accountLogVO，連結的session，下面的 accountLogVO才能刪除
			HibernateSession3.detachSession();

			//依頁面需要順序重新設定list
			accountLogList = toPageByAccount(accountLogList);
			taxLogList = toPageByTax(taxLogList);

			output.put("crsIdentity", partyLogVO);
			output.put("crsControlPerson", controlLogVO);
			output.put("crsAccountList", accountLogList);
			output.put("crsTaxList", taxLogList);

			output.put("crsCopyInfo", this.getCrsControlPersonInfo(partyLogId));

			Map<String, Object> codeTableMap = new HashMap<String, Object>();
			Map<String, Map<String, String>> nationality = super.getNationalityCode(taxLogList);
			codeTableMap.put("nationality", nationality);

			//列表用code table
			output.put("codeTable", codeTableMap);
			output.put("lockPage", super.checkLockpage(partyLogVO, CRSTinBizSource.BIZ_SOURCE_CTRL_MAN));
			output.put("nbInfo", super.getNBInfo(partyLogVO));
			output.put(KEY_SUCCESS, SUCCESS);
			return;
		}

		output.put(KEY_SUCCESS, FAIL);
		output.put(KEY_ERROR_MSG, NBUtils.getTWMsg(MsgCst.MSG_1250566));

		return;
	}

	/**
	 * <p>Description : 刪除-具控制權人 </p>
	 * <p>Created By : Simon.Huang</p>
	 * <p>Create Time : 2018年11月9日</p>
	 * @param in
	 * @param output
	 * @throws Exception
	 */
	public void delete(Map<String, Object> in, Map<String, Object> output) throws Exception {

		Long controlId = NumericUtils.parseLong(in.get("controlId"));
		Long parentId = NumericUtils.parseLong(in.get("parentId"));
		Long partyLogId = NumericUtils.parseLong(in.get("partyLogId"));
		if (partyLogId != null) {

			CRSControlManLogVO mainVO = null;
			List<CRSControlManLogVO> subList = new ArrayList<CRSControlManLogVO>();
			List<CRSControlManLogVO> ctrlmanList = crsControlManLogService.findByPartyLogId(partyLogId, true);

			if (parentId == null) {
				for (CRSControlManLogVO ctrlman : ctrlmanList) {
					if (ctrlman.getLogId().longValue() == controlId.longValue()) {
						if (ctrlman.getParentId() == null) {
							parentId = ctrlman.getLogId();
						} else {
							parentId = ctrlman.getParentId();
						}
						break;
					}
				}
			}

			if (parentId != null) {
				for (CRSControlManLogVO ctrlman : ctrlmanList) {
					if (ctrlman.getParentId() == null) {
						if (ctrlman.getLogId().equals(parentId)) {
							mainVO = ctrlman;
						}
					} else {
						if (ctrlman.getParentId().equals(parentId)) {
							subList.add(ctrlman);
						}
					}
				}

				subList.add(mainVO);

				//中斷controlLogVO與accountLogVO，連結的session，下面的 accountLogVO才能刪除
				HibernateSession3.detachSession();

				for (CRSControlManLogVO delete : subList) {

					for (CRSManAccountLogVO accountLogVO : delete.getCrsManAccountLog()) {
						crsControlManLogService.removeAccountLog(accountLogVO.getLogId());
					}

					List<CRSTaxIDNumberLogVO> taxLogList = crsTaxLogService.findByPartyLogIdAndBizId(partyLogId,
									CRSTinBizSource.BIZ_SOURCE_CTRL_MAN, delete.getLogId());

					for (CRSTaxIDNumberLogVO taxLogVO : taxLogList) {
						crsTaxLogService.remove(taxLogVO.getLogId());
					}

					crsControlManLogService.remove(delete.getLogId());
				}
			}

			HibernateSession3.currentSession().flush();
			HibernateSession3.detachSession();

			init(in, output);
			return;
		}

		output.put(KEY_SUCCESS, FAIL);
		output.put(KEY_ERROR_MSG, NBUtils.getTWMsg(MsgCst.MSG_1250566));

		return;
	}

	/**
	 * <p>Description : 具控制權人頁面總計頁面份數、頁面資料</p>
	 * <p>Created By : Simon.Huang</p>
	 * <p>Create Time : 2018年12月5日</p>
	 * @param partyLogId
	 * @return
	 */
	private List<Map<String, Object>> getCrsControlPersonInfo(Long partyLogId) {

		List<Map<String, Object>> ctrlmanInfoList = new ArrayList<Map<String, Object>>();

		Map<String, Map<String, Object>> allCtrlManInfo = new LinkedHashMap<String, Map<String, Object>>();

		List<CRSControlManLogVO> ctrlmanVOList = crsControlManLogService.findByPartyLogId(partyLogId, false);

		//共用的頁面，因copy數不會按照順序，先處理份數，再處理每份的頁數
		for (CRSControlManLogVO ctrlmanVO : ctrlmanVOList) {
			Map<String, Object> ctrlmanMap = new HashMap<String, Object>();
			ctrlmanMap.put("logId", ctrlmanVO.getLogId());
			ctrlmanMap.put("parentId", ctrlmanVO.getParentId());
			ctrlmanMap.put("copy", ctrlmanVO.getCopy());
			ctrlmanMap.put("lname", ctrlmanVO.getlName());
			ctrlmanMap.put("fname", ctrlmanVO.getfName());
			ctrlmanMap.put("nname", ctrlmanVO.getmName());
			if (ctrlmanVO.getParentId() == null) {
				//份
				Map<String, Object> subCopy = new HashMap<String, Object>();
				subCopy.putAll(ctrlmanMap);

				//自己也是相同控制權人的子頁
				List<Map<String, Object>> subCopies = new ArrayList<Map<String, Object>>();
				subCopies.add(subCopy);
				ctrlmanMap.put("subCopies", subCopies);

				ctrlmanInfoList.add(ctrlmanMap);
			}

			allCtrlManInfo.put(String.valueOf(ctrlmanVO.getLogId()), ctrlmanMap);
		}

		for (CRSControlManLogVO ctrlmanVO : ctrlmanVOList) {

			if (ctrlmanVO.getParentId() != null) {
				//頁
				Map<String, Object> parentMap = allCtrlManInfo.get(String.valueOf(ctrlmanVO.getParentId()));
				List<Map<String, Object>> subCopies = (List<Map<String, Object>>) parentMap.get("subCopies");

				Map<String, Object> subMap = allCtrlManInfo.get(String.valueOf(ctrlmanVO.getLogId()));
				subCopies.add(subMap);
			}
		}
		return ctrlmanInfoList;
	}

	private List<CRSTaxIDNumberLogVO> toPageByTax(List<CRSTaxIDNumberLogVO> taxLogList) {

		CRSTaxIDNumberLogVO[] returnList = new CRSTaxIDNumberLogVO[10];

		Arrays.fill(returnList, new CRSTaxIDNumberLogVO());
		for (CRSTaxIDNumberLogVO taxLogVO : taxLogList) {

			int index = taxLogVO.getCreateOrder();
			if (returnList[index - 1] == null || returnList[index - 1].getLogId() == null) {
				returnList[index - 1] = taxLogVO;
			} else {
				//createOrder重覆
				crsTaxLogService.remove(taxLogVO.getLogId());
			}
		}
		return Arrays.asList(returnList);
	}

	private List<CRSManAccountLogVO> toPageByAccount(List<CRSManAccountLogVO> accountLogList) {
		CRSManAccountLogVO[] returnList = new CRSManAccountLogVO[10];

		Arrays.fill(returnList, new CRSManAccountLogVO());
		for (CRSManAccountLogVO accountLogVO : accountLogList) {

			int index = accountLogVO.getSeqId();
			if (returnList[index - 1] == null || returnList[index - 1].getLogId() == null) {
				returnList[index - 1] = accountLogVO;
			} else {
				//seqId重覆
				crsControlManLogService.removeAccountLog(accountLogVO.getLogId());
			}
		}
		return Arrays.asList(returnList);
	}

    /**<p>Description : 具控制權人 </p>
     * <p>Created By : Simon.Huang</p>
     * <p>Create Time : 2018年11月30日</p>
     * @param in
     * @param output
     * @throws Exception
     */
    public void save(Map<String, Object> in, Map<String, Object> output) throws Exception {

        Long partyLogId = NumericUtils.parseLong(in.get("partyLogId"));
        Integer copy = NumericUtils.parseInteger(in.get("copy"));
        //Integer copy = new Integer(1);
        if (partyLogId != null && copy != null) {

            String type = (String) in.get(KEY_TYPE);
            try {
                Map<String, Object> submitData = (Map<String, Object>) in.get(KEY_DATA);
                Map<String, Object> controlPerson = (Map<String, Object>) submitData.get("controlPerson");
                Map<String, Map<String, Object>> account = (Map<String, Map<String, Object>>) submitData.get("account");
                Map<String, Map<String, Object>> accountType = (Map<String, Map<String, Object>>) submitData.get("accountType");

                //將實體1，2，3 整合為同一個map
                for (String key : account.keySet()) {
                    if (accountType.containsKey(key)) {
                        account.get(key).putAll(accountType.get(key));
                    }
                }

                Map<String, Map<String, Object>> tax = (Map<String, Map<String, Object>>) submitData.get("tax");

                if (UPDATE.equals(type)) {
                    CRSPartyLogVO partyLogVO = crsPartyLogService.load(partyLogId);

                    CRSControlManLogVO controlLogVO = null;
                    List<CRSTaxIDNumberLogVO> taxLogList = new ArrayList<CRSTaxIDNumberLogVO>();
                    List<CRSManAccountLogVO> accountLogList = new ArrayList<CRSManAccountLogVO>();

                    Map<String, CRSTaxIDNumberLogVO> taxBindCreateOroderMap = new HashMap<String, CRSTaxIDNumberLogVO>();
                    Map<String, CRSManAccountLogVO> accountBindSeqIdMap = new HashMap<String, CRSManAccountLogVO>();
                    controlLogVO = crsControlManLogService.findByPartyLogIdAndCopy(partyLogId, copy, true);
                    Long logId = null;
                    if (controlLogVO != null) {
                        logId = controlLogVO.getLogId();
                        //by log_id
                        taxLogList = crsTaxLogService.findByPartyLogIdAndBizId(partyLogId, CRSTinBizSource.BIZ_SOURCE_CTRL_MAN, controlLogVO.getLogId());
                        //by seq_id
                        accountLogList = controlLogVO.getCrsManAccountLog();
                        Collections.sort(accountLogList, new BeanComparator("seqId"));
                        //轉為key對應 
                        taxBindCreateOroderMap.putAll(NBUtils.toMap(taxLogList, "createOrder"));
                        accountBindSeqIdMap.putAll(NBUtils.toMap(accountLogList, "seqId"));

                    } else {

                        if (999 == copy.intValue()) {
                            copy = crsControlManLogService.getMaxCopy(partyLogVO.getLogId());
                        }
                        controlLogVO = new CRSControlManLogVO();
                        controlLogVO.setCopy(copy.intValue());
                        controlLogVO.setCrsPartyLog(partyLogVO);
                        controlLogVO.setChangeId(partyLogVO.getChangeId());
                        //需設定為true,才能寫入關連的PartyLogVO的partyLogId
                        controlLogVO = crsControlManLogService.save(controlLogVO, true);
                        logId = controlLogVO.getLogId();
                        HibernateSession3.currentSession().flush();
                    }
                    EbaoStringUtils.setProperties(controlLogVO, controlPerson);
                    controlLogVO.setLogId(logId);
                    //不設true,只寫入自己的資料(設定true會同步accountList,accountList由下方邏輯維護)
                    controlLogVO = crsControlManLogService.save(controlLogVO);
                    assignLogDataListId(in, controlLogVO.getLogId());
                    HibernateSession3.currentSession().flush();

                    //中斷controlLogVO與accountLogVO，連結的session，下面的 accountLogVO才能刪除
                    HibernateSession3.detachSession();

                    for (int seq = 1; seq <= account.size(); seq++) {
                        //by seq_id
                        CRSManAccountLogVO accountLogVO = accountBindSeqIdMap.get(String.valueOf(seq));
                        if (accountLogVO == null) {
                            accountLogVO = new CRSManAccountLogVO();

                            accountLogVO.setControlLogId(controlLogVO.getLogId());
                            accountLogVO.setChangeId(controlLogVO.getChangeId());
                            accountLogVO.setCrsLogId(partyLogId);
                            accountBindSeqIdMap.put(String.valueOf(seq), accountLogVO);
                        }

                        Map<String, Object> jsonData = account.get("account[" + (seq - 1) + "]");
                        if (isEmptyData(jsonData)) {
                            CRSManAccountLogVO delete = accountBindSeqIdMap.remove(String.valueOf(seq));

                            if (delete != null && delete.getLogId() != null) {
                                crsControlManLogService.removeAccountLog(delete.getLogId());
                            }

                        } else {
                            EbaoStringUtils.setProperties(accountLogVO, jsonData);
                            accountLogVO.setSeqId(seq);
                            accountLogVO.setControlLogId(controlLogVO.getLogId());
                            accountLogVO = crsControlManLogService.saveAccountLog(accountLogVO);

                            accountBindSeqIdMap.put(String.valueOf(seq), accountLogVO);
                        }
                    }

                    for (int createOrder = 1; createOrder <= tax.size(); createOrder++) {
                        //by create_order
                        CRSTaxIDNumberLogVO taxLogVO = taxBindCreateOroderMap.get(String.valueOf(createOrder));
                        if (taxLogVO == null) {
                            taxLogVO = new CRSTaxIDNumberLogVO();
                            taxLogVO.setBizSource("3");
                            taxLogVO.setBizId(controlLogVO.getLogId());
                            taxLogVO.setChangeId(controlLogVO.getChangeId());

                            taxBindCreateOroderMap.put(String.valueOf(createOrder), taxLogVO);
                        }
                        Map<String, Object> jsonData = tax.get("tax[" + (createOrder - 1) + "]");
                        if (isEmptyData(jsonData)) {
                            CRSTaxIDNumberLogVO delete = taxBindCreateOroderMap.remove(String.valueOf(createOrder));
                            if (delete != null && delete.getLogId() != null) {
                                crsTaxLogService.remove(delete.getLogId());
                            }
                        } else {
                            EbaoStringUtils.setProperties(taxLogVO, jsonData);
                            taxLogVO.setCrsPartyLog(partyLogVO);
                            taxLogVO.setCreateOrder(createOrder);
                            taxLogVO.setCopy(controlLogVO.getCopy());
                            taxLogVO = crsTaxLogService.save(taxLogVO, true);
                            taxBindCreateOroderMap.put(String.valueOf(createOrder), taxLogVO);
                        }
                    }

                    //依頁面需要順序重新設定list
                    accountLogList = toPageByAccount(new ArrayList<CRSManAccountLogVO>(accountBindSeqIdMap.values()));
                    taxLogList = toPageByTax(new ArrayList<CRSTaxIDNumberLogVO>(taxBindCreateOroderMap.values()));

                    output.put("crsControlPerson", controlLogVO);
                    output.put("crsAccountList", accountLogList);
                    output.put("crsTaxList", taxLogList);

                    output.put("crsCopyInfo", this.getCrsControlPersonInfo(partyLogId));

                    output.put(KEY_SUCCESS, SUCCESS);
                    return;
                } else if (DELETE.equals(type)) {

                    return;
                }
            } catch (Exception e) {
                throw e;
            } finally {

            }

        }
        output.put(KEY_SUCCESS, FAIL);
        output.put(KEY_ERROR_MSG, NBUtils.getTWMsg(MsgCst.MSG_1250566));
    }

}
