package com.ebao.ls.crs.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.ebao.ls.crs.company.CRSControlManLogService;
import com.ebao.ls.crs.company.CRSEntityControlManListLogService;
import com.ebao.ls.crs.company.CRSEntityLogService;
import com.ebao.ls.crs.constant.CRSBuildType;
import com.ebao.ls.crs.constant.CRSCodeType;
import com.ebao.ls.crs.constant.CRSSysCode;
import com.ebao.ls.crs.constant.CRSTinBizSource;
import com.ebao.ls.crs.customer.CRSIndividualLogService;
import com.ebao.ls.crs.data.company.CRSManAccountDAO;
import com.ebao.ls.crs.identity.CRSPartyLogService;
import com.ebao.ls.crs.identity.CRSTaxIDNumberLogService;
import com.ebao.ls.crs.nb.NBcrsService;
import com.ebao.ls.crs.vo.CRSPartyLogVO;
import com.ebao.ls.crs.vo.CRSTaxIDNumberLogVO;
import com.ebao.ls.crs.vo.NBcrsVO;
import com.ebao.ls.pa.nb.ctrl.pub.ajax.AjaxBaseService;
import com.ebao.ls.pa.nb.util.NBUtils;
import com.ebao.ls.pa.pub.TableCst;
import com.ebao.ls.pa.pub.bs.NbTaskService;
import com.ebao.ls.pa.pub.data.bo.Policy;
import com.ebao.ls.pa.pub.data.org.PolicyDao;
import com.ebao.ls.pub.CodeCst;
import com.ebao.pub.util.StringUtils;

/**
 * <p>Title: CRS 聲明書-PCR_264035&263803_CRS專案</p>
 * <p>Description: 具控制權人</p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: TGL Co., Ltd.</p>
 * <p>Create Time: 2018/11/08</p> 
 * @author 
 * <p>Update Time: 2018/11/08</p>
 * <p>Updater: simon.huang</p>
 * <p>Update Comments: </p>
 */
public class CrsAjaxService extends AjaxBaseService {

	@Resource(name = CRSPartyLogService.BEAN_DEFAULT)
	protected CRSPartyLogService crsPartyLogService;

	@Resource(name = CRSControlManLogService.BEAN_DEFAULT)
	protected CRSControlManLogService crsControlManLogService;

	@Resource(name = CRSTaxIDNumberLogService.BEAN_DEFAULT)
	protected CRSTaxIDNumberLogService crsTaxLogService;

	@Resource(name = CRSManAccountDAO.BEAN_DEFAULT)
	protected CRSManAccountDAO CRSManAccountDao;

	@Resource(name = CRSIndividualLogService.BEAN_DEFAULT)
	protected CRSIndividualLogService crsIndividualLogService;

	@Resource(name = CRSEntityLogService.BEAN_DEFAULT)
	protected CRSEntityLogService crsEntityLogService;

	@Resource(name = CRSControlManLogService.BEAN_DEFAULT)
	protected CRSControlManLogService crsCtrlManLogService;

	@Resource(name = CRSEntityControlManListLogService.BEAN_DEFAULT)
	protected CRSEntityControlManListLogService crsEntityControlManListLogService;

	@Resource(name = NBcrsService.BEAN_DEFAULT)
	protected NBcrsService nbCrsService;

	@Resource(name = PolicyDao.BEAN_DEFAULT)
	protected PolicyDao<Policy> policyDao;

	@Resource(name = NbTaskService.BEAN_DEFAULT)
	protected NbTaskService nbTaskService;
	
	protected Map<String, Map<String, String>> getNationalityCode(List<CRSTaxIDNumberLogVO> taxLogList) throws Exception {
		Map<String, Map<String, String>> nationality = new HashMap<String, Map<String, String>>();
		String whereCaluse = "";
		for (CRSTaxIDNumberLogVO taxLogVO : taxLogList) {
			if (taxLogVO.getResCountry() != null) {
				if (whereCaluse.length() > 0) {
					whereCaluse += ",";
				}
				whereCaluse += "'" + taxLogVO.getResCountry() + "'";
			}
		}
		if (whereCaluse.length() > 0) {
			whereCaluse = "NATIONALITY_CODE IN (" + whereCaluse + ")";
			nationality.putAll(NBUtils.getTCodeData(TableCst.V_NATIONALITY_ISO3166, whereCaluse));
		}
		return nationality;
	}

	protected String checkLockpage(CRSPartyLogVO partyLogVO, String bizSource) {

		String crsType = partyLogVO.getCrsType();
		if (StringUtils.isNullOrEmpty(crsType)) {
			return CodeCst.YES_NO__YES;
		} else {
			if (CRSTinBizSource.BIZ_SOURCE_PERSON.equals(bizSource)) {
				//個人頁面，但crsType不為個人，頁面鎖定不可編輯
				if (NBUtils.in(crsType, CRSCodeType.PERSON) == false) {
					return CodeCst.YES_NO__YES;
				}
			} else {
				//法人頁面，但crsType不為法人，頁面鎖定不可編輯
				if (NBUtils.in(crsType, CRSCodeType.COMPANY) == false) {
					return CodeCst.YES_NO__YES;
				}
			}
		}

		//建檔狀態為鎖定不可編輯的狀態
		if (NBUtils.in(partyLogVO.getBuildType(), CRSBuildType.LOCK)) {
			return CodeCst.YES_NO__YES;
		}

		return CodeCst.YES_NO__NO;
	}

	static String[] ignores = new String[] { "copy", "logId", "createOrder", "parentId" };

	/**
	 * <p>Description : 判斷頁面上傳資料是否有一個欄位有輸入資料</p>
	 * <p>Created By : Simon.Huang</p>
	 * <p>Create Time : 2018年11月30日</p>
	 * @param jsonData
	 * @return
	 */
	protected boolean isEmptyData(Map<String, Object> jsonData) {

		for (String key : jsonData.keySet()) {
			if (!NBUtils.in(key, ignores)) {
				Object value = jsonData.get(key);
				if (value != null && value.toString().length() > 0) {
					return false;
				}
			}
		}
		return true;
	}

	public Map<String, Object> getNBInfo(CRSPartyLogVO partyLogVO) {
		Map<String, Object> nbInfo = new HashMap<String, Object>();
		if (CRSSysCode.SYS_CODE__UNB.equals(partyLogVO.getSysCode())) {
			//只找編輯中的changeId,不找keyinChangeId
			NBcrsVO nbCrsVO = nbCrsService.findByChangeId(partyLogVO.getChangeId());
			if (nbCrsVO != null) {
				Policy policy = policyDao.load(nbCrsVO.getPolicyId());
				policyDao.getSession().evict(policy);
				nbInfo.put("applyDate", policy.getApplyDate());
				nbInfo.put("policyCode", policy.getPolicyNumber());
			}
		}
		return nbInfo;
	}
}
