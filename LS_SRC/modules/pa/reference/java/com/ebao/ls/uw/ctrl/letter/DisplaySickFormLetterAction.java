package com.ebao.ls.uw.ctrl.letter;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.UserTransaction;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.hibernate.criterion.Restrictions;

import com.ebao.foundation.module.db.hibernate.HibernateSession3;
import com.ebao.foundation.module.util.i18n.CodeTable;
import com.ebao.ls.letter.common.CommonLetterService;
import com.ebao.ls.pa.nb.bs.rule.ProposalRuleMsgService;
import com.ebao.ls.pa.nb.bs.rule.ProposalRuleResultService;
import com.ebao.ls.pa.nb.ci.ValidatorService;
import com.ebao.ls.pa.nb.ctrl.letter.NbLetterHelper;
import com.ebao.ls.pa.nb.util.NBUtils;
import com.ebao.ls.pa.nb.vo.ProposalRuleMsgVO;
import com.ebao.ls.pa.nb.vo.ProposalRuleResultVO;
import com.ebao.ls.pa.pub.bs.PolicyService;
import com.ebao.ls.pa.pub.bs.coverage.CoverageService;
import com.ebao.ls.pa.pub.vo.DetailRegPolicyVO;
import com.ebao.ls.pa.pub.vo.InsuredVO;
import com.ebao.ls.pa.pub.vo.PolicyVO;
import com.ebao.ls.pa.pub.vo.UwSickFormItemVO;
import com.ebao.ls.pa.pub.vo.UwSickFormLetterVO;
import com.ebao.ls.pub.CodeCst;
import com.ebao.ls.uw.ctrl.pub.UwActionHelper;
import com.ebao.ls.uw.ds.UwPolicyService;
import com.ebao.ls.uw.ds.UwSickFormItemService;
import com.ebao.ls.uw.ds.UwSickFormLetterService;
import com.ebao.ls.uw.vo.UwPolicyVO;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.framework.ExceptionFactory;
import com.ebao.pub.framework.GenericAction;
import com.ebao.pub.framework.GenericException;
import com.ebao.pub.util.StringUtils;
import com.ebao.pub.util.Trans;

/**
 * <p>Title: 核保/疾病問卷</p>
 * <p>Description: 核保/疾病問卷</p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company: TGL Co., Ltd.</p>
 * <p>Create Time: Apr 20, 2016</p> 
 * @author 
 * <p>Update Time: Apr 20, 2016</p>
 * <p>Updater: ChiaHui Su</p>
 * <p>Update Comments: </p>
 */
public class DisplaySickFormLetterAction extends GenericAction {
	public static final String BEAN_DEFAULT = "/uw/displaySickFormLetter";

	protected final static String SUBACTION__INIT = "queryInit";
	protected final static String SUBACTION__SAVE = "saveSickForm";
	protected static final String SOURCE_TYPE__INSURED = "T_INSURED_LIST";
	/* 來源TABLE NAME,要保人:"T_POLICY_HOLDER"被保險人:"T_INSURED_LIST" */
	protected static final String UW_SICK_FORM_SOURCE_TYPE__INSURED = "MSG_926";
	
	/**
	 * Action進入點
	 */
	@Override
	public ActionForward process(ActionMapping mapping, ActionForm form,
					HttpServletRequest request, HttpServletResponse response)
					throws GenericException {
		try {
			SickLetterForm aForm = (SickLetterForm) form;
			String subAction = aForm.getSubAction();
			String forward = "display";
			
			if(StringUtils.isNullOrEmpty(subAction) || SUBACTION__INIT.equals(subAction)){
				this.queryInit(aForm, request);
				forward = "display";
				aForm.setRetMsg(null);
			}else if(SUBACTION__SAVE.equals(subAction)){
				
				UserTransaction trans = Trans.getUserTransaction();
				try {
					trans.begin();
					Long policyId = aForm.getPolicyId();
					Long underwriteId = aForm.getUnderwriteId();
					String insured = aForm.getInsured();
					String insuredName = aForm.getInsuredName();
					String[] itemAry = aForm.getSickFormItem();
					/**取得目前系統中暫存及batch的照會**/
					UwSickFormLetterVO letter = uwSickFormLetterService.getLetterVO(underwriteId, Long.parseLong(insured));
					/**取得目前系統中暫存及batch的照會**/
					List<UwSickFormLetterVO> letterVoList = uwSickFormLetterService.findByPolicyInsured(underwriteId,
							Long.parseLong(insured), "0");
					
					/**取得目前系統中疾病告知的預設問卷**/
					UwSickFormLetterVO defaultLetterVo = null;
					Map<String, UwSickFormItemVO> defaultItemMap = null ;
					if(!letterVoList.isEmpty() && letterVoList.size() == 1 ) {
						defaultLetterVo = letterVoList.get(0);
						defaultItemMap = uwSickFormItemService.findByLetterId(defaultLetterVo.getListId());
					}
					
					/**已存在的疾病問卷項目**/
					Map<String, UwSickFormItemVO> map ;
					if ( letter == null) {
						/**新增人工問卷**/
						letter = new UwSickFormLetterVO();
						letter.setUnderwriteId(underwriteId);
						letter.setInsuredId(Long.valueOf(insured));
						letter.setStatus("1");
						letter = uwSickFormLetterService.save(letter);
						map = new HashMap<String, UwSickFormItemVO>();
					} else {
						map = uwSickFormItemService.findByLetterId(letter.getListId());
					}
					
					//取得非待處理狀態的疾病問卷項目
					List<ProposalRuleResultVO> ignoredRuleResults = proposalRuleResultService.find(
							Restrictions.eq("policyId", policyId),
							Restrictions.eq("sourceType", CodeCst.PROPOSAL_RULE_SOURCE_TYPE__SICK_FORM),
							Restrictions.eq("sourceId", letter.getListId()),
							Restrictions.ne("ruleStatus", CodeCst.PROPOSAL_RULE_STATUS__PENDING));
					
					if( itemAry != null) {
						for(String item : itemAry){
							
							UwSickFormItemVO vo = map.get(item);

							if( vo != null ) {
								for(ProposalRuleResultVO rrvo : ignoredRuleResults) {
									//取得已發放被忽略的預設問卷項目,需重新新增疾病問卷項目
									if(rrvo.getListId().equals(vo.getMsgId()) && item.equals(vo.getSickFormItem())) {
										vo = null;
										break;
									}
								}
							}

							if ( vo != null) {
								map.remove(item); 
							} else {
								/**新增疾病問卷項目**/
								String sickFormName = CodeTable.getCodeDesc("T_SICK_FORM", item);
								vo = new UwSickFormItemVO();
								vo.setSickFormLetterId(letter.getListId());
								vo.setSickFormItem(item);
								vo = uwSickFormItemService.save(vo);
								ProposalRuleResultVO ruleResultVO = this.saveProposalRuleResult(insuredName, sickFormName, policyId
												, letter.getListId(), aForm.getChangeId());
								/* 回寫msg_id至 sick_form_item */
								HibernateSession3.currentSession().flush();
								vo.setMsgId(ruleResultVO.getListId());
								vo = uwSickFormItemService.save(vo);
							}
						}
					}

					//未勾選任何項目或是有項目取消勾選
					if ( !map.isEmpty() || itemAry == null ) {
						
						List<ProposalRuleResultVO> ruleResults = proposalRuleResultService.find(
								Restrictions.eq("policyId", policyId),
								Restrictions.eq("sourceType", CodeCst.PROPOSAL_RULE_SOURCE_TYPE__SICK_FORM),
								Restrictions.eq("sourceId", letter.getListId()),
								Restrictions.eq("ruleStatus", CodeCst.PROPOSAL_RULE_STATUS__PENDING));

						/* 舊的問卷項目，不在調整後的問卷項目中，需刪除 */
						for(String code : map.keySet() ) {
							
							UwSickFormItemVO vo = map.get(code);
							
							/* 清除疾病問卷選項 */
							uwSickFormItemService.remove(vo.getListId());
												
							for(ProposalRuleResultVO msgVo : ruleResults) {
								
								if ( CodeCst.PROPOSAL_RULE_STATUS__PENDING == msgVo.getRuleStatus() && msgVo.getListId().equals( vo.getMsgId() ) && code.equals(vo.getSickFormItem())) {
									
									/* 關閉待發放的疾病問卷照會訊息 */	
									msgVo.setRuleStatus(CodeCst.PROPOSAL_RULE_STATUS__SYSTEMCLOSED);
									proposalRuleResultService.save(msgVo);
									
									ruleResults.remove(msgVo);
									break;
									
								}else if( itemAry == null ) {
									
									/* 關閉待發放的疾病問卷照會訊息 */	
									msgVo.setRuleStatus(CodeCst.PROPOSAL_RULE_STATUS__SYSTEMCLOSED);
									proposalRuleResultService.save(msgVo);
									
								}
								
							}
						}
						
					}

					trans.commit();
					
					
				} catch (Exception e) {
					trans.rollback();
					aForm.setRetMsg("MSG_1258279");
					aForm.setSubmitSuccess(false);
				}
				
				this.queryInit(aForm, request);
				forward = "display";
				
			}
			
			return mapping.findForward(forward);
		} catch (Exception e) {
			throw ExceptionFactory.parse(e);
		}
	}

	

	/**
	 * <p>Description : 新增一筆疾病問卷核保問題</p>
	 * @param insuredName
	 * @param sickFormName
	 * @param policyId
	 * @return
	 */
	private ProposalRuleResultVO saveProposalRuleResult(String insuredName, String sickFormName, 
					Long policyId , Long letterId, Long changeId){
		ProposalRuleMsgVO msgVO = null;
		ProposalRuleResultVO vo = null;
		
		DetailRegPolicyVO detailRegPolicyVO = policyService.getDetailRegPolicyVO(policyId);
		PolicyVO policyVO = detailRegPolicyVO.getPolicyVO();
		vo = new ProposalRuleResultVO();
		msgVO = proposalRuleMsgService.findProposalRuleMsg(CodeCst.PROPOSAL_RULE_MSG_SICK_FORM
				, AppContext.getCurrentUserLocalTime());

		if (msgVO != null) {
			boolean isOpqV2 = validatorService.isOpqV2(policyId);
			vo.setPolicyId(policyVO.getPolicyId());
			vo.setRuleType(msgVO.getRuleType());
			vo.setRuleLetterCode(msgVO.getRuleCode());
			vo.setRuleStatus(CodeCst.PROPOSAL_RULE_STATUS__PENDING);
			vo.setRuleName(msgVO.getRuleName());
			vo.setPrintable(msgVO.getPrintable());
			vo.setSourceType(CodeCst.PROPOSAL_RULE_SOURCE_TYPE__SICK_FORM);
			vo.setSourceId(letterId);
			vo.setChangeId(changeId);
			
			String violatedDesc = NBUtils.getViolatedDescByProposalRuleMsgVO(msgVO, isOpqV2);
			String letterContent = NBUtils.getLetterContentByProposalRuleMsgVO(msgVO, isOpqV2);
			
			violatedDesc = violatedDesc.replace("{0}", insuredName);
			letterContent = letterContent.replace("{0}", insuredName);
			
			violatedDesc = violatedDesc.replace("{1}", sickFormName);
			letterContent = letterContent.replace("{1}", sickFormName);
			
			vo.setViolatedDesc(violatedDesc);
			vo.setLetterContent(letterContent);
			vo.setUpdateTime(new Date());
			
			vo = proposalRuleResultService.save(vo);
			vo = proposalRuleResultService.load(vo.getListId());
		}
		return vo;
	}
	
	/**
	 * <p>Description : 組合疾病問卷頁面所需資料</p>
	 * @param aForm
	 * @param request
	 */
	private void queryInit(SickLetterForm aForm, HttpServletRequest request){
		Long policyId = aForm.getPolicyId();
		Long underwriteId = aForm.getUnderwriteId();
		if(policyId == null && underwriteId == null){
			throw new RuntimeException("NO policyId & underwriteId");
		}
		if(policyId == null){
			UwPolicyVO uwPolicyVO = uwPolicyService.findUwPolicy(underwriteId);
			policyId = uwPolicyVO.getPolicyId();
		}
		
		PolicyVO policyVO = policyService.load(policyId);
		
		if(policyVO != null){
			List<InsuredVO> insuredVOs = policyVO.gitSortInsuredList();
			/*改用 policyVO.gitSortInsuredList();
			List<InsuredVO> insuredVOs = policyVO.getInsureds();
			Collections.sort(insuredVOs, new Comparator<InsuredVO>() {
				@Override
				public int compare(InsuredVO m1, InsuredVO m2) {
					return new CompareToBuilder()
					.append("0".equals(m1.getInsuredCategory())?"9":m1.getInsuredCategory(),
							"0".equals(m2.getInsuredCategory())?"9":m2.getInsuredCategory())
					.append(m1.getNbInsuredCategory(),m2.getNbInsuredCategory())
					.toComparison();
				}
			});
			*/
			
			/* 被保險人 */
			aForm.setInsuredVOs(insuredVOs);
			
			/* 核保作業中有被保險人列表的功能增加判斷是否有險種，無險種disabled被保險人checkbox不可勾選 */
			UwActionHelper.countInsuredCoverage(request, policyVO);
			
			/* 疾病問卷選項列表 */
			List<Map<String, String>> items = uwSickFormItemService.findSickFormItemList();
			List<SickFormItem> itemList = new ArrayList<SickFormItem>();
			for (int i = 0 ; i < items.size() ; i++) {
				SickFormItem item = new SickFormItem();
				item.setCode(items.get(i).get("CODE"));
				item.setCodeValue(items.get(i).get("CODE_VALUE"));
				itemList.add(item);
			}
			aForm.setSickFormItems(itemList);
		}
	}

	@Resource(name = UwSickFormItemService.BEAN_DEFAULT)
	private UwSickFormItemService uwSickFormItemService;
	
	@Resource(name = UwSickFormLetterService.BEAN_DEFAULT)
	private UwSickFormLetterService uwSickFormLetterService;
	
	@Resource(name = PolicyService.BEAN_DEFAULT)
	private PolicyService policyService;
	
	@Resource(name = CoverageService.BEAN_DEFAULT)
	private CoverageService coverageService;
	
	@Resource(name = UwPolicyService.BEAN_DEFAULT)
	private UwPolicyService uwPolicyService;
	
	@Resource(name = ProposalRuleMsgService.BEAN_DEFAULT)
	protected ProposalRuleMsgService proposalRuleMsgService;
	
	@Resource(name = ProposalRuleResultService.BEAN_DEFAULT)
	protected ProposalRuleResultService proposalRuleResultService;

	@Resource(name = ValidatorService.BEAN_DEFAULT)
	protected  ValidatorService validatorService;

	@Resource(name = NbLetterHelper.BEAN_DEFAULT)
	private NbLetterHelper nbLetterHelper;
	
	@Resource(name=CommonLetterService.BEAN_DEFAULT)
	protected CommonLetterService commonLetterService;
	
}
