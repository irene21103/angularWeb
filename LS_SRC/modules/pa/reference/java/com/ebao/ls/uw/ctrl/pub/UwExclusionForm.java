package com.ebao.ls.uw.ctrl.pub;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.ebao.ls.uw.util.Utils;
import com.ebao.pub.framework.GenericForm;
import com.ebao.pub.util.DateUtils;

/**
 * <p>Title: </p>  
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2004 </p>
 * <p>Company: eBaoTech Corporation </p>
 * <p>Create Time: Fri Sep 03 17:54:57 GMT+08:00 2004 </p> 
 * @author Walter Huang
 * @version 1.0
 *
 */

public class UwExclusionForm extends GenericForm {

  private java.lang.Long underwriteId = null;

  private java.lang.String exclusionCode = "";

  private java.lang.Long policyId = null;

  private java.lang.Long underwriterId = null;

  private java.lang.Long exclusionId = null;

  private java.util.Date reviewDate = null;

  private java.lang.String descLang1 = "";

  private java.lang.String descLang2 = "";

  private java.util.Date effectDate = null;

  private java.lang.Long uwListId = null;

  private Date expiryDate;


  private String declInd;

  private Long insuredId;

  private Long productId;

  public String getDeclInd() {
    return declInd;
  }

  public void setDeclInd(String declInd) {
    this.declInd = declInd;
  }

  /**
   * returns the value of the underwriteId
   *
   * @return the underwriteId
   */
  public java.lang.Long getUnderwriteId() {
    return underwriteId;
  }

  /**
   * sets the value of the underwriteId
   *
   * @param underwriteId the underwriteId
   */
  public void setUnderwriteId(java.lang.Long underwriteId) {
    this.underwriteId = underwriteId;
  }

  /**
   * returns the value of the exclusionCode
   *
   * @return the exclusionCode
   */
  public java.lang.String getExclusionCode() {
    return exclusionCode;
  }

  /**
   * sets the value of the exclusionCode
   *
   * @param exclusionCode the exclusionCode
   */
  public void setExclusionCode(java.lang.String exclusionCode) {
    this.exclusionCode = exclusionCode;
  }

  /**
   * returns the value of the policyId
   *
   * @return the policyId
   */
  public java.lang.Long getPolicyId() {
    return policyId;
  }

  /**
   * sets the value of the policyId
   *
   * @param policyId the policyId
   */
  public void setPolicyId(java.lang.Long policyId) {
    this.policyId = policyId;
  }

  /**
   * returns the value of the underwriterId
   *
   * @return the underwriterId
   */
  public java.lang.Long getUnderwriterId() {
    return underwriterId;
  }

  /**
   * sets the value of the underwriterId
   *
   * @param underwriterId the underwriterId
   */
  public void setUnderwriterId(java.lang.Long underwriterId) {
    this.underwriterId = underwriterId;
  }

  /**
   * returns the value of the exclusionId
   *
   * @return the exclusionId
   */
  public java.lang.Long getExclusionId() {
    return exclusionId;
  }

  /**
   * sets the value of the exclusionId
   *
   * @param exclusionId the exclusionId
   */
  public void setExclusionId(java.lang.Long exclusionId) {
    this.exclusionId = exclusionId;
  }

  /**
   * returns the value of the descLang1
   *
   * @return the descLang1
   */
  public java.lang.String getDescLang1() {
    return descLang1;
  }

  /**
   * sets the value of the descLang1
   *
   * @param descLang1 the descLang1
   */
  public void setDescLang1(java.lang.String descLang1) {
    this.descLang1 = descLang1;
  }

  /**
   * returns the value of the descLang2
   *
   * @return the descLang2
   */
  public java.lang.String getDescLang2() {
    return descLang2;
  }

  /**
   * sets the value of the descLang2
   *
   * @param descLang2 the descLang2
   */
  public void setDescLang2(java.lang.String descLang2) {
    this.descLang2 = descLang2;
  }

  /**
   * returns the value of the effectDate
   *
   * @return the effectDate
   */
  public java.util.Date getEffectDate() {
    return effectDate;
  }

  /**
   * sets the value of the effectDate
   *
   * @param effectDate the effectDate
   */
  public void setEffectDate(java.util.Date effectDate) {
    this.effectDate = effectDate;
  }

  /**
   * returns the value of the uwListId
   *
   * @return the uwListId
   */
  public java.lang.Long getUwListId() {
    return uwListId;
  }

  /**
   * sets the value of the uwListId
   *
   * @param uwListId the uwListId
   */
  public void setUwListId(java.lang.Long uwListId) {
    this.uwListId = uwListId;
  }

  // Begin Additional Business Methods

  //commencement date, only used for calculate reviewPeriod
  private java.util.Date commencementDate = null;

  public String getReviewPeriod() {
    return getReviewPeriod(reviewDate, effectDate);
  }

  public void setReviewPeriod(String reviewPeriod) {
    setReviewDate(Utils.getReviewDate(commencementDate, reviewPeriod));
  }

  public java.util.Date getCommencementDate() {
    return commencementDate;
  }

  public void setCommencementDate(java.util.Date commencementDate) {
    this.commencementDate = commencementDate;
  }

  /**
   * returns the value of the reviewDate
   *
   * @return the reviewDate
   */
  public java.util.Date getReviewDate() {
    return reviewDate;
  }

  /**
   * sets the value of the reviewDate
   *
   * @param reviewDate the reviewDate
   */
  public void setReviewDate(java.util.Date reviewDate) {
    this.reviewDate = reviewDate;
  }

  /**
   * Utility Method used for calculate reviewPeriod
   * @param reviewDate Date
   * @param commencementDate Date
   * @return String
   */
  public static final String getReviewPeriod(java.util.Date reviewDate,
      java.util.Date effectDate) {

    if (null == reviewDate || null == effectDate) {
      return "";
    } else {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");

      return String.valueOf(Integer.parseInt(sdf.format(reviewDate).substring(
          0, 4))
          - Integer.parseInt(sdf.format(effectDate).substring(0, 4)));
    }
  }

  // End Additional Business Methods

  /**
   * @return Returns the expiryDate.
   */
  public Date getExpiryDate() {
    return expiryDate;
  }

  /**
   * @param expiryDate The expiryDate to set.
   */
  public void setExpiryDate(Date expiryDate) {
    this.expiryDate = expiryDate;
  }

  /**
   * @return Returns the reviewNum.
   */
  public String getReviewNum() {
    //	modified by hanzhong.yan for cq:GEL00030033
    //    if (effectDate != null && reviewDate != null) {
    //      return String.valueOf(Integer.parseInt(sdf.format(reviewDate).substring(0, 4))
    //          - Integer.parseInt(sdf.format(effectDate).substring(0, 4)));
    //
    //    }
    int result = 0;
    if (effectDate != null && reviewDate != null) {
      //      result = Integer.parseInt(sdf.format(reviewDate).substring(0, 4))
      //          - Integer.parseInt(sdf.format(effectDate).substring(0, 4));
      result = (int) DateUtils.getMonthAmount(effectDate, reviewDate);
    }
    if (result > 0) {
      return String.valueOf(result);
    } else
      return "";

    //    return "0";
  }

  public Long getInsuredId() {
    return insuredId;
  }

  public void setInsuredId(Long insuredId) {
    this.insuredId = insuredId;
  }

  public Long getProductId() {
    return productId;
  }

  public void setProductId(Long productId) {
    this.productId = productId;
  }
}