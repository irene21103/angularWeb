package com.ebao.ls.beneOwner.ctrl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.foundation.core.logging.Log;
import com.ebao.ls.cs.boaudit.ds.BeneficialOwnerService;
import com.ebao.ls.pty.ci.ContactCI;
import com.ebao.ls.pty.ci.CustomerCI;
import com.ebao.ls.pub.CodeCst;
import com.ebao.pub.framework.GenericAction;
import com.ebao.pub.framework.MultiWarning;
import com.ebao.ls.cs.logger.ApplicationLogger;

public class BeneOwnerEditAction extends GenericAction {

	private final Log log = Log.getLogger(BeneOwnerEditAction.class);

	@Resource(name = CustomerCI.BEAN_DEFAULT)
	private CustomerCI customerCI;

	@Resource(name = BeneficialOwnerService.BEAN_DEFAULT)
	private BeneficialOwnerService beneficialOwnerService;

	@Resource(name = ContactCI.BEAN_DEFAULT)
	private ContactCI contactCI;

	@Override
	public ActionForward process(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		BeneOwnerAddForm boForm = (BeneOwnerAddForm) form;
		List<BeneOwnerForm> boList = boForm.getBeneOwnerList();
		Long listId = boForm.getListId();
		Long boPartyIdTemp = boForm.getBoPartyIdTemp();
		String isCompanyTemp = boForm.getIsCompanyTemp();
		String beneOwnerCodeTemp = boForm.getBeneOwnerCodeTemp();
		String beneOwnerNameTemp = boForm.getBeneOwnerNameTemp();
		String beneOwnerNationalityTemp = boForm.getBeneOwnerNationalityTemp();
		String beneOwnerNationality2Temp = boForm.getBeneOwnerNationality2Temp();
		Date birthdayTemp = boForm.getBirthdayTemp();
		BigDecimal sharesTemp = boForm.getSharesTemp();
		String positionTemp = boForm.getPositionTemp();
		String beneOwnerTypeTemp = boForm.getBeneOwnerTypeTemp();
		String actualControllerTemp = boForm.getActualControllerTemp();
		if (StringUtils.isBlank(actualControllerTemp)) {
			actualControllerTemp = CodeCst.YES_NO__NO;
		}
		
		// 儲存邏輯用listId來判斷要修改哪一筆資料，無listId為新增資料
		// 因一個PartyId可能有兩筆資料(實質受益人/高階管理人)，
		// 所以不管是新增資料或修改資料都需要判斷舊資料中是否有同PartyId資料同步更新<身分別>之外的其他欄位
		for (BeneOwnerForm boVo : boList) {
			if(listId == null && beneOwnerCodeTemp.equals(boVo.getBeneOwnerCode()) && isCompanyTemp.equals(boVo.getIsCompany())){
				// 新增資料時有比對到同ID同是否法人資料，，為同一人資料須更新
				boVo.setBeneOwnerCode(beneOwnerCodeTemp);
				boVo.setBeneOwnerName(beneOwnerNameTemp);
				boVo.setBeneOwnerNationality(beneOwnerNationalityTemp);
				boVo.setBeneOwnerNationality2(beneOwnerNationality2Temp);
				boVo.setBirthday(birthdayTemp);
				boVo.setShares(sharesTemp);
				boVo.setPosition(positionTemp);
				boVo.setActualController(actualControllerTemp);
				break;
			} else if(listId != null && listId.longValue() != boVo.getListId().longValue()){
				// 修改資料時不同列資料比對
				if (((boPartyIdTemp != null && boVo.getBoPartyId() != null && boPartyIdTemp.longValue() == boVo.getBoPartyId().longValue()) 
					|| (beneOwnerCodeTemp.equals(boVo.getBeneOwnerCode()) && isCompanyTemp.equals(boVo.getIsCompany())))){
					// 沒有PartyId但同ID同是否法人資料，為同一人資料須更新。
					// 同PartyId為同一人資料須更新。
					boVo.setBeneOwnerCode(beneOwnerCodeTemp);
					boVo.setBeneOwnerName(beneOwnerNameTemp);
					boVo.setBeneOwnerNationality(beneOwnerNationalityTemp);
					boVo.setBeneOwnerNationality2(beneOwnerNationality2Temp);
					boVo.setBirthday(birthdayTemp);
					boVo.setShares(sharesTemp);
					boVo.setPosition(positionTemp);
					boVo.setActualController(actualControllerTemp);
					break;
				}
			}
		}

		if(listId == null){
			BeneOwnerForm boVo = new BeneOwnerForm();
			boVo.setBoPartyId(boPartyIdTemp);
			boVo.setIsCompany(isCompanyTemp);
			boVo.setBeneOwnerCode(beneOwnerCodeTemp);
			boVo.setBeneOwnerName(beneOwnerNameTemp);
			boVo.setBeneOwnerNationality(beneOwnerNationalityTemp);
			boVo.setBeneOwnerNationality2(beneOwnerNationality2Temp);
			boVo.setBirthday(birthdayTemp);
			boVo.setShares(sharesTemp);
			boVo.setPosition(positionTemp);
			boVo.setActualController(actualControllerTemp);
			boVo.setBeneOwnerType(beneOwnerTypeTemp);
			boList.add(boVo);
		} else {
			for (BeneOwnerForm boVo : boList) {
				if(listId.longValue() == boVo.getListId().longValue()){
					boVo.setBoPartyId(boPartyIdTemp);
					boVo.setBeneOwnerCode(beneOwnerCodeTemp);
					boVo.setBeneOwnerName(beneOwnerNameTemp);
					boVo.setBeneOwnerNationality(beneOwnerNationalityTemp);
					boVo.setBeneOwnerNationality2(beneOwnerNationality2Temp);
					boVo.setBirthday(birthdayTemp);
					boVo.setShares(sharesTemp);
					boVo.setPosition(positionTemp);
					boVo.setActualController(actualControllerTemp);
					boVo.setBeneOwnerType(beneOwnerTypeTemp);
					break;
				}
			}
		}
		// 頁面存在異動
		boForm.setHasGlobalChange(true);
		return mapping.findForward("success");
	}

	@Override
	public MultiWarning processWarning(ActionMapping actionMapping, ActionForm actionForm, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		MultiWarning mw = new MultiWarning();
		BeneOwnerAddForm boForm = (BeneOwnerAddForm) actionForm;
		List<BeneOwnerForm> boList = boForm.getBeneOwnerList();
		// 因為前端頁面刪除，流水號可能跳號，會產生null情況
		if (boList != null) {
			Iterator<BeneOwnerForm> iter = boList.iterator();
			while (iter.hasNext()) {
				BeneOwnerForm item = iter.next();
				if (item == null) {
					iter.remove();
				}
			}
		}
		return mw;
	}

}
