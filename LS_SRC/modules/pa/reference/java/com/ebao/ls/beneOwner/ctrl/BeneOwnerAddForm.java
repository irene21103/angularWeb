package com.ebao.ls.beneOwner.ctrl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.Factory;
import org.apache.commons.collections.ListUtils;

import com.ebao.pub.web.pager.PagerFormImpl;

public class BeneOwnerAddForm extends PagerFormImpl {

	/**
	 * 紀錄紀錄頁面模式: 查詢: QUERY 新增: INSERT 修改: UPDATE
	 */
	private String submitModeFromOpen;

	private Long companyId;

	/** 統一編號 */
	private String registerCode;

	/** 法人名稱 */
	private String companyName;

	/** 註冊地國籍 */
	private String regNationality;

	/** 註冊地郵遞區號 */
	private String regPostCode;

	/** 註冊地地址 */
	private String regAddress;

	/** 營業地國籍 */
	private String bizNationality;

	/** 營業地郵遞區號 */
	private String bizPostCode;

	/** 營業地地址 */
	private String bizAddress;

	/** 行業代碼 */
	private String companyCategory;

	/** 發行無記名股票之情況 */
	private Integer bearerIndi;

	/** 無記名股數占比 */
	private BigDecimal bearerPercentage;

	/** 是否為公家機關 */
	private String isGov;
	
	/** 機構類型 */
	private String govType;
	
	private List<BeneOwnerForm> beneOwnerList;

	private Long boPartyIdTemp;

	/** 可能為法人的registerCode，也可能為自然人的certiCode 異動 */
	private String beneOwnerCodeTemp;

	/** 實質受益人/高階管理人 異動 */
	private String beneOwnerNameTemp;

	/** 法人實質受益人國籍 異動 */
	private String beneOwnerNationalityTemp;

	/** 法人實質受益人國籍2 異動 */
	private String beneOwnerNationality2Temp;

	/** 自然人生日，法人無 異動 */
	private Date birthdayTemp;

	/** 持有股份百分比 異動 */
	private BigDecimal sharesTemp;

	/** 實質控制權人 異動 */
	private String actualControllerTemp;

	/** 職稱 異動 */
	private String positionTemp;

	/** 是否法人 異動 */
	private String isCompanyTemp;

	private String registerCodeHidden;

	/** 檢核法人實質受益人複雜結構 */
	private String checkBeneOwnerRuleMsg;

	/** 法人異動時間 */
	private Date companyUpdateTime;

	/** 法人異動人員 */
	private Long companyUpdaterId;
	
	/** 身分別 異動 */
	private String beneOwnerTypeTemp;

	/** 頁面有異動 */
	private boolean hasGlobalChange;
	
	/** 存檔成功 */
	private String saveSuccess;

	public String getSubmitModeFromOpen() {
		return submitModeFromOpen;
	}

	public void setSubmitModeFromOpen(String submitModeFromOpen) {
		this.submitModeFromOpen = submitModeFromOpen;
	}

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public String getRegisterCode() {
		return registerCode;
	}

	public void setRegisterCode(String registerCode) {
		this.registerCode = registerCode;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getRegNationality() {
		return regNationality;
	}

	public void setRegNationality(String regNationality) {
		this.regNationality = regNationality;
	}

	public String getRegPostCode() {
		return regPostCode;
	}

	public void setRegPostCode(String regPostCode) {
		this.regPostCode = regPostCode;
	}

	public String getRegAddress() {
		return regAddress;
	}

	public void setRegAddress(String regAddress) {
		this.regAddress = regAddress;
	}

	public String getBizNationality() {
		return bizNationality;
	}

	public void setBizNationality(String bizNationality) {
		this.bizNationality = bizNationality;
	}

	public String getBizPostCode() {
		return bizPostCode;
	}

	public void setBizPostCode(String bizPostCode) {
		this.bizPostCode = bizPostCode;
	}

	public String getBizAddress() {
		return bizAddress;
	}

	public void setBizAddress(String bizAddress) {
		this.bizAddress = bizAddress;
	}

	public String getCompanyCategory() {
		return companyCategory;
	}

	public void setCompanyCategory(String companyCategory) {
		this.companyCategory = companyCategory;
	}

	public Integer getBearerIndi() {
		return bearerIndi;
	}

	public void setBearerIndi(Integer bearerIndi) {
		this.bearerIndi = bearerIndi;
	}

	public BigDecimal getBearerPercentage() {
		return bearerPercentage;
	}

	public void setBearerPercentage(BigDecimal bearerPercentage) {
		this.bearerPercentage = bearerPercentage;
	}

	public String getIsGov() {
		return isGov;
	}

	public void setIsGov(String isGov) {
		this.isGov = isGov;
	}

	public List<BeneOwnerForm> getBeneOwnerList() {

		if (beneOwnerList == null) {
			beneOwnerList = ListUtils.lazyList(new ArrayList<BeneOwnerForm>(), new Factory() {
				@Override
				public Object create() {
					return new BeneOwnerForm();
				}
			});
		}
		return beneOwnerList;
	}

	public void setBeneOwnerList(List<BeneOwnerForm> beneOwnerList) {
		this.beneOwnerList = beneOwnerList;
	}

	public Long getBoPartyIdTemp() {
		return boPartyIdTemp;
	}

	public void setBoPartyIdTemp(Long boPartyIdTemp) {
		this.boPartyIdTemp = boPartyIdTemp;
	}

	public String getBeneOwnerCodeTemp() {
		return beneOwnerCodeTemp;
	}

	public void setBeneOwnerCodeTemp(String beneOwnerCodeTemp) {
		this.beneOwnerCodeTemp = beneOwnerCodeTemp;
	}

	public String getBeneOwnerNameTemp() {
		return beneOwnerNameTemp;
	}

	public void setBeneOwnerNameTemp(String beneOwnerNameTemp) {
		this.beneOwnerNameTemp = beneOwnerNameTemp;
	}

	public String getBeneOwnerNationalityTemp() {
		return beneOwnerNationalityTemp;
	}

	public void setBeneOwnerNationalityTemp(String beneOwnerNationalityTemp) {
		this.beneOwnerNationalityTemp = beneOwnerNationalityTemp;
	}

	public String getBeneOwnerNationality2Temp() {
		return beneOwnerNationality2Temp;
	}

	public void setBeneOwnerNationality2Temp(String beneOwnerNationality2Temp) {
		this.beneOwnerNationality2Temp = beneOwnerNationality2Temp;
	}

	public Date getBirthdayTemp() {
		return birthdayTemp;
	}

	public void setBirthdayTemp(Date birthdayTemp) {
		this.birthdayTemp = birthdayTemp;
	}

	public BigDecimal getSharesTemp() {
		return sharesTemp;
	}

	public void setSharesTemp(BigDecimal sharesTemp) {
		this.sharesTemp = sharesTemp;
	}

	public String getActualControllerTemp() {
		return actualControllerTemp;
	}

	public void setActualControllerTemp(String actualControllerTemp) {
		this.actualControllerTemp = actualControllerTemp;
	}

	public String getPositionTemp() {
		return positionTemp;
	}

	public void setPositionTemp(String positionTemp) {
		this.positionTemp = positionTemp;
	}

	public String getIsCompanyTemp() {
		return isCompanyTemp;
	}

	public void setIsCompanyTemp(String isCompanyTemp) {
		this.isCompanyTemp = isCompanyTemp;
	}

	public String getBeneOwnerTypeTemp() {
		return beneOwnerTypeTemp;
	}

	public void setBeneOwnerTypeTemp(String beneOwnerTypeTemp) {
		this.beneOwnerTypeTemp = beneOwnerTypeTemp;
	}

	public String getRegisterCodeHidden() {
		return registerCodeHidden;
	}

	public void setRegisterCodeHidden(String registerCodeHidden) {
		this.registerCodeHidden = registerCodeHidden;
	}

	public String getCheckBeneOwnerRuleMsg() {
		return checkBeneOwnerRuleMsg;
	}

	public void setCheckBeneOwnerRuleMsg(String checkBeneOwnerRuleMsg) {
		this.checkBeneOwnerRuleMsg = checkBeneOwnerRuleMsg;
	}

	public Date getCompanyUpdateTime() {
		return companyUpdateTime;
	}

	public void setCompanyUpdateTime(Date companyUpdateTime) {
		this.companyUpdateTime = companyUpdateTime;
	}

	public Long getCompanyUpdaterId() {
		return companyUpdaterId;
	}

	public void setCompanyUpdaterId(Long companyUpdaterId) {
		this.companyUpdaterId = companyUpdaterId;
	}

	public boolean isHasGlobalChange() {
		return hasGlobalChange;
	}

	public void setHasGlobalChange(boolean hasGlobalChange) {
		this.hasGlobalChange = hasGlobalChange;
	}

	public String getGovType() {
		return govType;
	}

	public void setGovType(String govType) {
		this.govType = govType;
	}

	public String getSaveSuccess() {
		return saveSuccess;
	}

	public void setSaveSuccess(String saveSuccess) {
		this.saveSuccess = saveSuccess;
	}

	@Override
	public String toString() {
		return "BeneOwnerAddForm [submitModeFromOpen=" + submitModeFromOpen + ", companyId=" + companyId
				+ ", registerCode=" + registerCode + ", companyName=" + companyName + ", regNationality="
				+ regNationality + ", regPostCode=" + regPostCode + ", regAddress=" + regAddress + ", bizNationality="
				+ bizNationality + ", bizPostCode=" + bizPostCode + ", bizAddress=" + bizAddress + ", companyCategory="
				+ companyCategory + ", bearerIndi=" + bearerIndi + ", bearerPercentage=" + bearerPercentage + ", isGov="
				+ isGov + ", beneOwnerList=" + beneOwnerList + ", boPartyIdTemp=" + boPartyIdTemp
				+ ", beneOwnerCodeTemp=" + beneOwnerCodeTemp + ", beneOwnerNameTemp=" + beneOwnerNameTemp
				+ ", beneOwnerNationalityTemp=" + beneOwnerNationalityTemp + ", beneOwnerNationality2Temp="
				+ beneOwnerNationality2Temp + ", birthdayTemp=" + birthdayTemp + ", sharesTemp=" + sharesTemp
				+ ", actualControllerTemp=" + actualControllerTemp + ", positionTemp=" + positionTemp
				+ ", isCompanyTemp=" + isCompanyTemp + ", registerCodeHidden=" + registerCodeHidden
				+ ", checkBeneOwnerRuleMsg=" + checkBeneOwnerRuleMsg + ", companyUpdateTime=" + companyUpdateTime
				+ ", companyUpdaterId=" + companyUpdaterId + ", hasGlobalChange=" + hasGlobalChange + ", govType=" + govType + "]";
	}
}
