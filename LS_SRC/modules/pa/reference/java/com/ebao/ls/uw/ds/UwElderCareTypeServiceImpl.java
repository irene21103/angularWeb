package com.ebao.ls.uw.ds;

import com.ebao.ls.foundation.service.GenericServiceImpl;
import com.ebao.ls.pa.pub.vo.VOGenerator;
import static com.ebao.ls.pub.cst.YesNo.YES_NO__NO;
import static com.ebao.ls.pub.cst.YesNo.YES_NO__YES;
import com.ebao.ls.uw.data.UwElderCareTypeDao;
import com.ebao.ls.uw.data.bo.UwElderCareType;
import com.ebao.ls.uw.vo.UwElderCareTypeVO;
import java.util.List;
import javax.annotation.Resource;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.SQLQuery;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

/**
 * 高齡投保核保評估關懷方式服務實作
 *
 * @author Bruce Liao
 */
public class UwElderCareTypeServiceImpl extends GenericServiceImpl<UwElderCareTypeVO, UwElderCareType, UwElderCareTypeDao<UwElderCareType>> implements UwElderCareTypeService {

    protected final Logger logger = LogManager.getFormatterLogger();

    @Resource(name = VOGenerator.BEAN_DEFAULT)
    private VOGenerator voGenerator;

    @Override
    protected UwElderCareTypeVO newEntityVO() {
        return voGenerator.uwElderCareTypeVONewInstance();
    }

    @Override
    public List<UwElderCareTypeVO> findByCriteriaWithOrder(List<Order> orders, Criterion... criterion) {
        return convertToVOList(dao.findByCriteriaWithOrder(orders, criterion));
    }

    @Override
    public List<UwElderCareTypeVO> findByCriteria(Criterion... criterion) {
        return convertToVOList(dao.findByCriteria(criterion));
    }

    @Override
    public void checkAndChangeLastUwElderCare(Long careListId) {
        if (careListId != null) {
            dao.findByCriteria(
                    Restrictions.eq("careListId", careListId),
                    Restrictions.eq("lastCmtFlg", YES_NO__YES))
                    .forEach(entity -> {
                        entity.setLastCmtFlg(YES_NO__NO);
                        dao.saveorUpdate(entity);
                    });
        }
    }

    @Override
    public int updateCareDataToDefaultById(Long listId) {
        SQLQuery query = dao.getSession().createSQLQuery("UPDATE T_UW_ELDER_CARE_TYPE SET CARE_TYPE = CARE_TYPE WHERE LIST_ID = :LIST_ID");
        query.setParameter("LIST_ID", listId);
        return query.executeUpdate();
    }

}
