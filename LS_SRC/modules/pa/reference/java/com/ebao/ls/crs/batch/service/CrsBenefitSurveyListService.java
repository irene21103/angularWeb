package com.ebao.ls.crs.batch.service;


import java.util.Date;
import java.util.List;

import com.ebao.foundation.commons.exceptions.GenericException;
import com.ebao.ls.crs.batch.vo.CRSTempVO;
import com.ebao.ls.crs.vo.CRSPartyLogVO;
import com.ebao.ls.fatca.vo.FatcaDueDiligenceListVO;

public interface CrsBenefitSurveyListService {
    public final static String BEAN_DEFAULT = "crsBenefitSurveyListService";
    
    public final static String SURVEY_SYSTEM_TYPE_FATCA = "FATCA";
    public final static String SURVEY_SYSTEM_TYPE_CRS = "CRS";
    public final static String SURVEY_SYSTEM_TYPE_ALL = "CRS/FATCA";
    
    /**
	 * 查詢CRS受益人
	 * @param processDate 業務日
	 * @param surveyType 調查類型
	 * @return
	 * @throws Exception
	 */
    public List<CRSTempVO> queryBeneficiaryList(Date processDate, String surveyType) throws Exception;
    
    /**
	 * 將CRSTempVO轉換為CRSPartyLogVO
	 * @param fatcaVoList
	 * @param processDate
	 * @return
	 */
    public CRSPartyLogVO getCRSPartyLogVO(CRSTempVO crsTempVO, Date processDate) throws Exception;
    
    /**
	 * CRS LOG建檔與主檔提交
	 * @param voList
	 * @param userId 
	 * @param processDate
	 * @throws Exception
	 */
    public void createCrsRecord(String id, CRSTempVO crsTempVO, Long userId, Date processDate) throws Exception;
    
    /**
	 * 從CRSTempVO清單中取得FatcaDueDiligenceListVO清單
	 * @param crsTempVOList
	 * @return
	 */
    public List<FatcaDueDiligenceListVO> getFatcaList(List<CRSTempVO> crsTempVOList) throws Exception ;
    
    /**
     * 派發孤兒保單
     * @param voList
     * @throws Exception
     */
    public abstract void deliverOrphan(FatcaDueDiligenceListVO vo) throws Exception;
    
    /**
     * 更新業務員
     * @param vo
     */
    public void updateServiceAgent(FatcaDueDiligenceListVO vo) throws Exception;
    
    /**
     * 是否在CRS_PARTY_LOG已經有紀錄
     * @param changeId
     * @param policyCode
     * @param targetName
     * @return
     * @throws Exception
     */
    public boolean isCrsPartyLogExist(Long changeId, String policyCode, String targetName) throws Exception;
}
