package com.ebao.ls.crs.company.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.ebao.ls.crs.company.CRSEntityControlManListLogService;
import com.ebao.ls.crs.data.bo.CRSEntityControlManListLog;
import com.ebao.ls.crs.data.company.CRSEntityControlManListLogDAO;
import com.ebao.ls.crs.vo.CRSEntityControlManListLogVO;
import com.ebao.ls.foundation.service.GenericServiceImpl;

public class CRSEntityControlManListLogServiceImpl
				extends GenericServiceImpl<CRSEntityControlManListLogVO, CRSEntityControlManListLog, CRSEntityControlManListLogDAO>
				implements CRSEntityControlManListLogService {

	@Override
	protected CRSEntityControlManListLogVO newEntityVO() {
		return new CRSEntityControlManListLogVO();
	}

	@Override
	public List<CRSEntityControlManListLogVO> findByEntityLogId(Long entityLogId) {
		List<Order> orders = new ArrayList<Order>();
		orders.add(Order.asc("copy"));
		List<CRSEntityControlManListLog> ctrlManLogList = this.dao.findByCriteriaWithOrder(orders,
						Restrictions.eq("entityLogId", entityLogId));
		return convertToVOList(ctrlManLogList);
		
	}

}
