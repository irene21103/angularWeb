package com.ebao.ls.uw.ctrl.restriction;

import java.io.StringBufferInputStream;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Hashtable;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

import com.ebao.ls.fnd.ci.ChargeDeductionCI;
import com.ebao.ls.pa.nb.ci.XmlGeneratorCI;
import com.ebao.ls.pa.pub.ci.CoverageCI;
import com.ebao.ls.pa.pub.ref.api.prd.ProductService;
import com.ebao.ls.pa.pub.ref.vo.prd.ProductVO;
import com.ebao.ls.pa.pub.service.PolicyService;
import com.ebao.ls.pa.pub.vo.CoverageVO;
import com.ebao.ls.pa.pub.vo.SingleTopupVO;
import com.ebao.ls.pub.CodeCst;
import com.ebao.ls.uw.ctrl.ActionUtil;
import com.ebao.ls.uw.ctrl.decision.UwProductInfoForm;
import com.ebao.ls.uw.ctrl.pub.UwRestrictionForm;
import com.ebao.ls.uw.ds.UwPolicyService;
import com.ebao.ls.uw.ds.vo.UwRestrictionVO;
import com.ebao.ls.uw.vo.UwProductVO;
import com.ebao.pub.framework.GenericAction;
import com.ebao.pub.framework.GenericException;
import com.ebao.pub.util.BeanUtils;

/**
 * <p>
 * Title:Underwriting
 * </p>
 * <p>
 * Description:
 * </p>
 * <p>
 * Copyright: Copyright (c) 2004
 * </p>
 * <p>
 * Company: eBaoTech Corporation
 * </p>
 * 
 * @author yixing.lu
 * @since Jun 27, 2005
 * @version 1.0
 */
public class ShowRestrictAction extends GenericAction {
  public static final String BEAN_DEFAULT = "/uw/uwShowRestrict";

/**
   * show restrict
   * 
   * @param mapping
   * @param form
   * @param req
   * @param res
   * @throws java.lang.Exception
   * @return
   */
  @Override
  public ActionForward process(ActionMapping mapping, ActionForm form,
      HttpServletRequest req, HttpServletResponse res) throws Exception {
    UwProductInfoForm uwProductInfoForm = (UwProductInfoForm) form;
    Long underwriteId = uwProductInfoForm.getUnderwriteId();
    Long itemId = uwProductInfoForm.getItemIds()[0];
    String coverageDef = null;
    UwRestrictionForm restrictForm = new UwRestrictionForm();
    UwProductVO productVO = uwPolicyDS.findUwProduct(underwriteId, itemId);
    // get the limit as
    setMinSaAndMaxSa(productVO, req);
    UwRestrictionVO restrictVO = uwPolicyDS.findUwRestriction(productVO
        .getUwListId());
    BeanUtils.copyProperties(restrictForm, restrictVO, false);
    Long productId = Long.valueOf(productVO.getProductId().intValue());
    ProductVO showVO = this.getProductService().getProductByVersionId(
        Long.valueOf(productId.intValue()),
        productVO.getProductVersionId());
    restrictForm.setBenefitType(showVO.getBenefitType());
    restrictForm.setPolicyCode(uwProductInfoForm.getPolicyCode());
    restrictForm.setApplyCode(uwProductInfoForm.getApplyCode());
    /* to decide whether MS/MA factor existed in the page */
    boolean isMsFactor = false;
    boolean isMaFactor = false;
    restrictForm.setWaitPeriodDiff(showVO.isWaitPeriod());
    if (showVO.isMaFactor()) {
      isMaFactor = true;
    }
    if (showVO.isMsFactor()) {
      isMsFactor = true;
    }
    // set ma factor range
    if (isMaFactor) {
      CoverageVO policyProductVO = coverageCI.retrieveByItemId(itemId);
      if (policyProductVO.getVulExt() != null
          && policyProductVO.getVulExt().getSaFactor() != null) {
        restrictForm.setMaFactor(String.valueOf(policyProductVO.getVulExt()
            .getSaFactor().floatValue()));
        BigDecimal[] rtn = getFactorScope(productVO,
            Integer.valueOf(CodeCst.ILP_FACTOR_TYPE__MA_FACTOR));
        req.setAttribute("ma_min", rtn[0]);
        req.setAttribute("ma_max", rtn[1]);
      } else {
        isMaFactor = false;
      }
    }
    // set ms factor range
    if (isMsFactor) {
      SingleTopupVO addInvestVO = coverageCI.retrieveByItemId(itemId)
          .getSingleTopup();
      if (addInvestVO != null && addInvestVO.getSaFactor() != null) {
        restrictForm.setMsFactor(String.valueOf(addInvestVO.getSaFactor()
            .floatValue()));
        BigDecimal[] rtn = getFactorScope(productVO,
            Integer.valueOf(CodeCst.ILP_FACTOR_TYPE__MS_FACTOR));
        req.setAttribute("ms_min", rtn[0]);
        req.setAttribute("ms_max", rtn[1]);
      } else {
        isMsFactor = false;
      }
    }
    boolean isILp = false;
    boolean isSinglePremium = false;
    if (restrictForm.getProductId() != null) {
      coverageDef = xmlGeneratorCI.geneVarietyXML(productId,policyService.getActiveVersionDate(productVO)).toString();
      if (isILPProduct(productVO)) {
        isILp = true;
      }
    }
    if (productVO.getInitialType() != null) {
      if (isSinglePremium(productVO)) {
        isSinglePremium = true;
      }
    }
    String coverageFlag = "false";
    String chargeFlag = "false";
    Hashtable coverMap = new Hashtable();
    Hashtable chargeMap = new Hashtable();
    String sqlCoverage = "";
    String sqlCharge = "";
    if (coverageDef != null) {
      Document document = null;
      DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
      DocumentBuilder builder = factory.newDocumentBuilder();
      StringBufferInputStream xmlValue = new StringBufferInputStream(
          coverageDef);
      document = builder.parse(xmlValue);
      Element root = document.getDocumentElement();
      NodeList coveragePeriodList = root.getElementsByTagName("coveragePeriod");
      NodeList chargePeriodList = root.getElementsByTagName("chargePeriod");
      NodeList waitingPeriodList = root.getElementsByTagName("waitPeriod");
      StringBuffer waitingPeriod = new StringBuffer();
      for (int i = 0; i < waitingPeriodList.getLength(); i++) {
        Element wait = (Element) waitingPeriodList.item(i);
        Text text = (Text) wait.getFirstChild();
        if (text != null) {
          if (i == 0) {
            waitingPeriod.append(text.getNodeValue());
          } else {
            waitingPeriod.append(",");
            waitingPeriod.append(text.getNodeValue());
          }
        }
      }
      req.setAttribute("waitingPeriod", waitingPeriod.toString());
      sqlCoverage = "COVERAGE_PERIOD in (";
      if (coveragePeriodList.getLength() == 0) {
        coverageFlag = "true";
      }
      for (int i = 0; i < coveragePeriodList.getLength(); i++) {
        Element coveragePeriod = (Element) coveragePeriodList.item(i);
        // checking the product if having COVERAGE_PERIOD
        if (coveragePeriod.getAttribute("period").trim().length() == 0) {
          coverageFlag = "true";
        }
        if (i == 0) {
          sqlCoverage = sqlCoverage + "'"
              + coveragePeriod.getAttribute("period") + "'";
        } else {
          sqlCoverage = sqlCoverage + "," + "'"
              + coveragePeriod.getAttribute("period") + "'";
        }
        NodeList coverageYears = coveragePeriod
            .getElementsByTagName("coverageYear");
        String t = "";
        int[] date = new int[coverageYears.getLength()];
        for (int j = 0; j < coverageYears.getLength(); j++) {
          Element coverageYear = (Element) coverageYears.item(j);
          Text text = (Text) coverageYear.getFirstChild();
          t = t + " " + text.getNodeValue();
          date[j] = (Integer.valueOf(text.getNodeValue())).intValue();
        }
        Arrays.sort(date);
        if (!isSequence(date)) {
          t = date[0] + "---" + date[date.length - 1];
        }
        // t = "Please input value from "+ t;
        coverMap.put(coveragePeriod.getAttribute("period"), t);
      }
      sqlCoverage = sqlCoverage + ")";
      // getting the CHARGE_PERIOD condition
      sqlCharge = "CHARGE_PERIOD in (";
      if (chargePeriodList.getLength() == 0) {
        chargeFlag = "true";
      }
      for (int i = 0; i < chargePeriodList.getLength(); i++) {
        Element chargePeriod = (Element) chargePeriodList.item(i);
        // checking the product if having the CHARGE_PERIOD
        if (chargePeriod.getAttribute("period").trim().length() == 0) {
          chargeFlag = "true";
        }
        if (i == 0) {
          sqlCharge = sqlCharge + "'" + chargePeriod.getAttribute("period")
              + "'";
        } else {
          sqlCharge = sqlCharge + "," + "'"
              + chargePeriod.getAttribute("period") + "'";
        }
        NodeList chargeYears = chargePeriod.getElementsByTagName("chargeYear");
        String t = "";
        int[] date = new int[chargeYears.getLength()];
        for (int j = 0; j < chargeYears.getLength(); j++) {
          Element chargeYear = (Element) chargeYears.item(j);
          Text text = (Text) chargeYear.getFirstChild();
          t = t + " " + text.getNodeValue();
          date[j] = (Integer.valueOf(text.getNodeValue())).intValue();
        }
        Arrays.sort(date);
        if (!isSequence(date)) {
          t = date[0] + "---" + date[date.length - 1];
        }
        // t = "Please input value from "+ t;
        chargeMap.put(chargePeriod.getAttribute("period"), t);
      }
      sqlCharge = sqlCharge + ")";
    }
    // if (restrictForm.getReducedAmount() != null &&
    // restrictForm.getReducedAmount().equals(new BigDecimal(0))) {
    // restrictForm.setReducedAmount(null);
    // }
    req.setAttribute("coverageFlag", coverageFlag);
    req.setAttribute("chargeFlag", chargeFlag);
    req.setAttribute("sqlCharge", sqlCharge);
    req.setAttribute("sqlCoverage", sqlCoverage);
    req.setAttribute("action_form", restrictForm);
    req.setAttribute("coverage_def", coverageDef);
    req.setAttribute("coverMap", coverMap);
    req.setAttribute("chargeMap", chargeMap);
    req.setAttribute("isMaFactor", String.valueOf(isMaFactor));
    req.setAttribute("isMsFactor", String.valueOf(isMsFactor));
    req.setAttribute("isILP", String.valueOf(isILp));
    req.setAttribute("isSinglePremium", String.valueOf(isSinglePremium));
    req.setAttribute("unitIndi", ActionUtil.getUnitIndicator(itemId, productId,policyService.getActiveVersionDate(productVO)));
    return mapping.findForward("restrict");
  }

  /**
   * check the value of the int array is sequence or not
   * 
   * @param a
   * @return
   */
  protected boolean isSequence(int[] a) {
    boolean flag = false;
    if (a.length == 1) {
      return true;
    }
    for (int i = 0; i < a.length - 1; i++) {
      if (a[i] + 1 != a[i + 1]) {
        flag = true;
      }
    }
    return flag;
  }

  /**
   * checking the ILP product
   * 
   * @param productVO
   * @return boolean
   * @throws GenericException
   */
  protected boolean isILPProduct(UwProductVO vo) throws GenericException {
    return this.getProductService().getProductByVersionId(vo.getProductId().longValue(),
    		vo.getProductVersionId()).isIlp();
  }

  /**
   * get scope of ma or ma factor
   * 
   * @param productVO
   * @param req
   * @throws GenericException
   */
  protected BigDecimal[] getFactorScope(UwProductVO productVO,
      Integer factorType) throws GenericException {
    // get sa factor scope
    BigDecimal[] decimal;
    if (factorType.intValue() == 8) {
      factorType = Integer.valueOf(1);
    } else {
      factorType = Integer.valueOf(2);
    }
    decimal = chargeDeductionCI.getFactorBound(
        Long.valueOf(productVO.getProductId().intValue()),
        String.valueOf(productVO.getPayMode()), productVO.getStandLife1(),
        factorType, productVO.getPolicyId());
    return decimal;
  }

  /**
   * get min sum insured
   * 
   * @param uwProductVO
   * @return
   * @throws GenericException
   */
  protected void setMinSaAndMaxSa(UwProductVO uwProductVO,
      HttpServletRequest req) throws GenericException {
  }

  /**
   * checking the pay mode is single premium
   * 
   * @param productVO
   */
  protected boolean isSinglePremium(UwProductVO productVO) {
    if ("5".equals(productVO.getInitialType())) {
      return true;
    }
    return false;
  }

  @Resource(name = UwPolicyService.BEAN_DEFAULT)
  private UwPolicyService uwPolicyDS;

  public void setUwPolicyDS(UwPolicyService uwPolicyDS) {
    this.uwPolicyDS = uwPolicyDS;
  }

  public UwPolicyService getUwPolicyDS() {
    return uwPolicyDS;
  }

  @Resource(name = CoverageCI.BEAN_DEFAULT)
  private CoverageCI coverageCI;

  public CoverageCI getCoverageCI() {
    return coverageCI;
  }

  public void setCoverageCI(CoverageCI coverageCI) {
    this.coverageCI = coverageCI;
  }

  @Resource(name = ChargeDeductionCI.BEAN_DEFAULT)
  private ChargeDeductionCI chargeDeductionCI;

  public ChargeDeductionCI getChargeDeductionCI() {
    return chargeDeductionCI;
  }

  public void setChargeDeductionCI(ChargeDeductionCI chargeDeductionCI) {
    this.chargeDeductionCI = chargeDeductionCI;
  }

  @Resource(name = XmlGeneratorCI.BEAN_DEFAULT)
  private XmlGeneratorCI xmlGeneratorCI;

  /**
   * @return the xmlGeneratorCI
   */
  public XmlGeneratorCI getXmlGeneratorCI() {
    return xmlGeneratorCI;
  }

  /**
   * @param xmlGeneratorCI the xmlGeneratorCI to set
   */
  public void setXmlGeneratorCI(XmlGeneratorCI xmlGeneratorCI) {
    this.xmlGeneratorCI = xmlGeneratorCI;
  }

  @Resource(name = ProductService.BEAN_DEFAULT)
  private ProductService productService;

  public ProductService getProductService() {
    return productService;
  }

  public void setProductService(ProductService productService) {
    this.productService = productService;
  }
  
  @Resource(name = PolicyService.BEAN_DEFAULT)
  PolicyService policyService;
}
