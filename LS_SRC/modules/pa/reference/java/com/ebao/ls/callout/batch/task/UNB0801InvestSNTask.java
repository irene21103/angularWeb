package com.ebao.ls.callout.batch.task;

import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.ApplicationContextAware;

import com.ebao.foundation.common.lang.DateUtils;
import com.ebao.foundation.common.lang.exception.ExceptionUtils;
import com.ebao.foundation.core.logging.Log;
import com.ebao.ls.callout.batch.AbstractCalloutTask;
import com.ebao.ls.callout.batch.CalloutConstants;
import com.ebao.ls.callout.batch.CalloutDataExportService;
import com.ebao.ls.callout.bs.CalloutTransService;
import com.ebao.pub.batch.help.BatchHelp;
import com.ebao.pub.batch.type.JobStatus;
import com.ebao.pub.batch.type.LogLevel;
import com.ebao.pub.batch.util.BatchLogUtils;
import com.ebao.pub.util.TransUtils;

public class UNB0801InvestSNTask extends AbstractCalloutTask implements ApplicationContextAware {
    
    @Resource(name = CalloutTransService.BEAN_DEFAULT)
    private CalloutTransService callouttransService;

    public static final String BEAN_DEFAULT = "zeroEightZeroOneTask";
    
    public final static Long JOB_ID = 1332322L;
    
    private static final Log log =  Log.getLogger(UNB0801InvestSNTask.class);
    
    private final Long DISPLAY_ORDER = 1L;

    private String CONFIG_TYPE = "1";// 080抽樣比例
    
    @Resource(name=CalloutDataExportService.BEAN_DEFAULT)
    private CalloutDataExportService exportServ;

    @Override
    public int mainProcess() throws Exception {
        List<Map<String, Object>> mapList = null;
        UserTransaction ut = TransUtils.getUserTransaction();
        try {
            ut.begin();
            /* 保單抽樣 */
            mapList = unbSampling(
                            "[BR-CMN-TFI-005][檔案080-1 連結結構債件之保單]",
                            CalloutConstants.ZeroEightZeroOne, CONFIG_TYPE,
                            DISPLAY_ORDER, "080-1", "080");
            
            /* 匯出抽樣保單 */
            exportServ.feedMap(mapList, CalloutConstants.ZeroEightZeroOne, BatchHelp.getProcessDate());
            ut.commit();
            if (CollectionUtils.isEmpty(mapList)) {
                return JobStatus.EXECUTE_SUCCESS;
            }            
        } catch (Exception ex) {
            try {
                ut.rollback();
            } catch (SystemException sysEx) {
                BatchLogUtils.addLog(LogLevel.ERROR, null,ExceptionUtils.getFullStackTrace(sysEx));
            }
            BatchLogUtils.addLog(LogLevel.ERROR, null,ExceptionUtils.getFullStackTrace(ex));
            return JobStatus.EXECUTE_FAILED;
        }
        
        return JobStatus.EXECUTE_SUCCESS;
    }
    
    public java.util.List<Map<String, Object>> getData(Date inputDate, String policyCode){
        java.util.List<java.util.Map<String,Object>> mapList  = new java.util.LinkedList<Map<String,Object>>();
        final java.sql.Date processDate = new java.sql.Date(inputDate.getTime());
        final java.sql.Date nextDate = new java.sql.Date(DateUtils.addDay(inputDate, 1).getTime());
        java.util.Date monthDate = DateUtils.truncateMonth(DateUtils.addMonth(inputDate,-1));
        final java.sql.Date monthSqlDate = new java.sql.Date(monthDate.getTime());
        String paraVal = com.ebao.pub.para.Para.getParaValue(3060010020L);
        java.util.Date paraValDate;
        try {
            paraValDate = org.apache.commons.lang3.time.DateUtils.parseDate(paraVal, "yyyyMMdd");
        } catch (ParseException e) {
            java.util.Calendar calendar = java.util.Calendar.getInstance();
            calendar.set(2018, 3, 1);
            paraValDate= calendar.getTime();
        }
        final java.sql.Date initDate = DateUtils.toSqlDate(paraValDate);
        
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT distinct mail.policy_id, ");
        sql.append("       mail.policy_code, ");
        sql.append("       prd.internal_id, ");
        sql.append("       cmprd.product_version_id, ");
        sql.append("       ph.name ph_name, ");
        sql.append("       ph.roman_name ph_roman_name, ");
        sql.append("       ph.certi_code ph_certi_code,  ");
        sql.append("       ph.gender ph_gender,  ");
        sql.append("       ph.BIRTH_DATE ph_birthday,  ");
        sql.append("       decode(ph.office_tel_reg, null, null, '(' || ph.office_tel_reg || ')') || ph.office_tel || decode(ph.office_tel_ext, null, null, '#' || ph.office_tel_ext) ph_office_tel,  ");
        sql.append("       decode(ph.home_tel_reg, null, null, '(' || ph.home_tel_reg || ')') || ph.home_tel || decode(ph.home_tel_ext, null, null, '#' || ph.home_tel_ext) ph_home_tel,  ");
        sql.append("       ph.MOBILE_TEL ph_phone1,  ");
        sql.append("       ph.MOBILE_TEL2 ph_phone2,  ");
        sql.append("       (SELECT a.address_1 from t_address a WHERE a.address_id = ph.address_id) ph_home_addr,  ");
        sql.append("       lr.name lr_name,   ");
        sql.append("       lr.roman_name lr_roman_name,   ");
        sql.append("       lr.certi_code lr_certi_code,   ");
        sql.append("       lr.gender lr_gender,   ");
        sql.append("       lr.BIRTH_DATE lr_birthday,   ");
        sql.append("       lr.rel_to_ph_ins rel_to_ph,   ");  
        sql.append("       lr2.rel_to_ph_ins rel_to_ins,   ");
        sql.append("       decode(lrc.office_tel_reg, null, null, '(' || lrc.office_tel_reg || ')')  || lrc.office_tel || decode(lrc.office_tel_ext, null, null, '#' || lrc.office_tel_ext) lr_office_tel,   ");
        sql.append("       decode(lrc.home_tel_reg, null, null, '(' || lrc.home_tel_reg || ')')  || lrc.home_tel || decode(lrc.home_tel_ext, null, null, '#' ||  lrc.home_tel_ext) lr_home_tel,  ");
        sql.append("       lrc.MOBILE lr_phone1,  ");
        sql.append("       lrc.mobile2 lr_phone2,  ");
        sql.append("       cmprd.name i_name,  ");
        sql.append("       cmprd.certi_code i_certi_code,  ");
        sql.append("       cmprd.entry_age i_age,  ");
        sql.append("       cmprd.apply_date apply_date,  ");
        sql.append("       cmprd.VALIDATE_DATE validate_date,  ");
        sql.append("       cmprd.ISSUE_DATE issue_date,  ");
        sql.append("       cmprd.charge_period charge_period,  ");
        sql.append("       cmprd.charge_year charge_year,  ");
        sql.append("       cmprd.submission_date submission_date,  ");
        sql.append("       mail.DISPATCH_DATE dispatch_date,  ");
        sql.append("       mail.acknowledge_date reply_date,  ");
        sql.append("       mail.receive_date receive_date,  ");
        sql.append("       mail.tracking_number tracking_number,  ");//PCR-415241 保單郵寄掛號/國際快捷/宅配號碼
        sql.append("       send_type,  ");
        sql.append("       PKG_PUB_CMN_UTILS.f_is_purpose(mail.policy_id, '1') ensure_require,  ");
        sql.append("       PKG_PUB_CMN_UTILS.f_is_purpose(mail.policy_id, '2') retire_require,  ");
        sql.append("       PKG_PUB_CMN_UTILS.f_is_purpose(mail.policy_id, '3') child_fee,  ");
        sql.append("       PKG_PUB_CMN_UTILS.f_is_purpose(mail.policy_id, '4') asset,  ");
        sql.append("       PKG_PUB_CMN_UTILS.f_is_purpose(mail.policy_id, '5') house,  ");
        sql.append("       PKG_PUB_CMN_UTILS.f_is_purpose(mail.policy_id, '50') emp_benefit ,   ");
        sql.append("       PKG_PUB_CMN_UTILS.f_is_purpose(mail.policy_id, '6') other_purpose,   ");
        sql.append("       pkg_pub_cmn_utils.f_get_ilp_charge_type(cmprd.PRODUCT_ID ) AS ilp_charge_type,  ");
        sql.append("       pkg_ls_pub_util.f_get_code_mutilang_desc('T_LIABILITY_STATUS', mail.liability_state, '311') state,  ");
        sql.append("       pkg_ls_sc_callout.f_get_cm_prod_rate(prd.base_money, mail.issue_date ) exchange_rate, ");
        sql.append("       pkg_ls_pub_util.f_get_code_mutilang_desc('T_MONEY', prd.base_money, '311') money_name,  ");
        sql.append("       pkg_ls_pub_util.f_get_code_mutilang_desc('V_NBU_CALLOUT_CHARGE_MODE', cmprd.RENEWAL_TYPE, '311')  charge_name,   ");//PCR-415241 繳別
        sql.append("       cmprd.customized_prem,  ");
        sql.append("       cmprd.amount,  ");
        sql.append("       pkg_ls_sc_callout.f_get_text('MSG_1250422') as big_prodcate,  ");
        sql.append("       prd.small_prodcate,  ");
        sql.append("       pkg_ls_sc_callout.f_get_each_prem(mail.policy_id) each_prem  ");
        sql.append("  FROM (select *  ");
        sql.append("          from v_callout_v_struct m  ");
        sql.append("         where 1=1  ");
        if(StringUtils.isNotEmpty(policyCode)) {
        	sql.append(" and m.policy_code = :policy_code ");
        }
        sql.append("           and m.issue_date >= :init_date ");
        sql.append("           and m.dispatch_date >= :month_date ");
        sql.append("           and m.dispatch_date <= :process_date ");
        sql.append(" and not exists ");
        sql.append("          (select 1 ");
        sql.append("                   from t_callout_batch cb ");
        sql.append("                   join t_callout_config cfg ");
        sql.append("                     on cfg.list_id = cb.config_type ");
        sql.append("                  where cb.policy_code = m.policy_code ");
        sql.append("                    and cfg.config_type = 1 and display_order = 1) ");
        sql.append("  ) mail "); 
        sql.append("  join V_CALLOUT_CM_PROD cmprd  ");
        sql.append("    on cmprd.policy_id = mail.policy_id and cmprd.item_id = mail.item_id ");
        sql.append("  join V_callout_PROD_INVEST prd  ");
        sql.append("    on prd.product_id = cmprd.product_id and prd.product_id = mail.product_id ");
        sql.append("  join t_policy_holder_log ph  ");
        sql.append("  on ph.policy_id = mail.policy_id and ph.LAST_CMT_FLG = 'Y' ");
        sql.append("  LEFT join t_legal_representative_log lr   ");
    	//與要保人關係
    	sql.append("  on lr.policy_id = cmprd.policy_id AND lr.rel_id = ph.list_id and lr.rel_type=1 AND lr.last_cmt_flg = 'Y'  ");
    	sql.append("  LEFT join t_legal_representative_log lr2   ");
    	//與被保人關係
    	sql.append("  on lr2.policy_id = cmprd.policy_id AND lr2.rel_id =  ph.list_id and lr2.rel_type=2 AND lr2.last_cmt_flg = 'Y'  ");
        sql.append("  LEFT JOIN t_customer lrc  ");
        sql.append("  ON lr.party_id = lrc.customer_id ");
        sql.append("order by mail.policy_code ");
        
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("process_date", processDate);
        params.put("init_date", initDate);
        params.put("month_date", monthSqlDate);
        
        BatchLogUtils.addLog(LogLevel.INFO, null,
				String.format("process_date := %s, month_date := %s,  next_date := %s, policy_code :=%s ",
						DateUtils.date2String(processDate, "yyyy/MM/dd"),
						DateUtils.date2String(monthDate, "yyyy/MM/dd"),
						DateUtils.date2String(nextDate, "yyyy/MM/dd"),
						policyCode));
        if(StringUtils.isNotEmpty(policyCode)) {
        	params.put("policy_code", policyCode);
        }
        mapList = nt.queryForList(sql.toString(), params);
        return combineDataByPolicyCode(mapList);
    }

    @Override
    public String saveToTrans(Map<String, Object> map, Date processDate,String fileName, String dept) {
        String calloutNum = savePolicyHolder(map, processDate, fileName, dept);
        return calloutNum;
    }
    
    
}
