package com.ebao.ls.uw.ctrl.info;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.ls.pa.nb.util.NBUtils;
import com.ebao.ls.pa.pub.MsgCst;
import com.ebao.ls.uw.ds.UwInsureHistoryService;
import com.ebao.ls.uw.ds.vo.UwInsureClientVO;
import com.ebao.pub.framework.GenericAction;
import com.ebao.pub.util.StringUtils;

/**
 * <p>Title: 查詢投保歷史資料ACTION</p>
 * <p>Description: Function Description Only vegetable</p>
 * <p>Copyright: Copyright (c) 2015</p>
 * <p>Company: TGL Co., Ltd.</p>
 * <p>Create Time: Aug 21, 2015</p> 
 * @author 
 * <p>Update Time: Aug 21, 2015</p>
 * <p>Updater: Alex Cheng</p>
 * <p>Update Comments: </p>
 */
public class InsureHistoryRecordAction extends GenericAction {

    @Resource(name = UwInsureHistoryService.BEAN_DEFAULT)
    private UwInsureHistoryService uwInsureHistoryService;

    @Override
    public ActionForward process(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                    HttpServletResponse response) throws Exception {

        UwHistoryInsureForm uwHistoryInsureForm = (UwHistoryInsureForm) form;
        String policyId = request.getParameter("policyId").toString();
        String forward = "display";

        if (!StringUtils.isNullOrEmpty(policyId)) {
        	uwHistoryInsureForm.setPolicyId(new Long(policyId));
        	
        	List<UwInsureClientVO> insureClientList = new ArrayList<UwInsureClientVO>();
        	uwHistoryInsureForm.setInsureClientList(insureClientList);
        	
        	boolean isGroupDownlaodOK =  uwInsureHistoryService.findUwInsuredHistoryRecord(
        			insureClientList, policyId);

        	if(isGroupDownlaodOK == false){
        		//MSG_1262354 無法連接團險系統。
        		request.setAttribute("errorMsg", NBUtils.getTWMsg(MsgCst.MSG_1262354));
        	}

        }

        return mapping.findForward(forward);

    }

}
