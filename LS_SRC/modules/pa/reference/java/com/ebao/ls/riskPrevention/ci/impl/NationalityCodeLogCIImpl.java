package com.ebao.ls.riskPrevention.ci.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import com.ebao.ls.riskPrevention.ci.NationalityCodeLogCI;
import com.ebao.ls.riskPrevention.data.NationalityCodeLogDao;
import com.ebao.ls.riskPrevention.data.bo.NationalityCodeLog;
import com.ebao.ls.riskPrevention.vo.NationalityCodeLogQueryVO;
import com.ebao.pub.util.BeanUtils;

public class NationalityCodeLogCIImpl implements NationalityCodeLogCI{
	
	@Resource(name=NationalityCodeLogDao.BEAN_DEFAULT)
	NationalityCodeLogDao dao;
	
	@Override
	public List<NationalityCodeLogQueryVO> findHistoryByNationalCodeDESC(String nationalCode) {
		List<NationalityCodeLogQueryVO> voList = new ArrayList<NationalityCodeLogQueryVO>();
		NationalityCodeLog bo = new NationalityCodeLog();
		bo.setNationalCode(nationalCode);
		List<NationalityCodeLog> resultList = dao.nationalQueryBO(bo, null);
		for(NationalityCodeLog result : resultList){
			NationalityCodeLogQueryVO vo = new NationalityCodeLogQueryVO();
			BeanUtils.copyProperties(vo, result);
			voList.add(vo);
		}
		return voList;
	}

}
