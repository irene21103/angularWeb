package com.ebao.ls.cancel.web.ctrl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.foundation.core.logging.Log;
import com.ebao.foundation.module.util.i18n.StringResource;
import com.ebao.ls.cancel.web.form.ProductCancelDetailForm;
import com.ebao.ls.chl.service.ChannelOrgService;
import com.ebao.ls.cs.commonflow.ds.cmnquery.PolicyQueryService;
import com.ebao.ls.cs.util.POSUtils;
import com.ebao.ls.pa.nb.bs.DisabilityCancelDetailService;
import com.ebao.ls.pa.nb.util.NBUtils;
import com.ebao.ls.pa.nb.util.NumericUtils;
import com.ebao.ls.pa.nb.vo.DisabilityCancelDetailVO;
import com.ebao.ls.pa.pub.bs.InsuredService;
import com.ebao.ls.pa.pub.bs.coverage.CoverageService;
import com.ebao.ls.pa.pub.model.PolicyInfo;
import com.ebao.ls.pa.pub.service.PolicyService;
import com.ebao.ls.pa.pub.vo.CoverageInsuredVO;
import com.ebao.ls.pa.pub.vo.CoverageVO;
import com.ebao.ls.pa.pub.vo.InsuredVO;
import com.ebao.ls.pub.CodeCst;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.framework.GenericAction;
import com.ebao.pub.i18n.lang.CodeTable;
import com.ebao.pub.i18n.lang.Table;
import com.ebao.pub.util.StringUtils;

public class ProductCancelDetailAction extends GenericAction {
public static final String BEAN_DEFAULT = "/productCancelDetail";
    
    private static final Log log = Log.getLogger(ProductCancelDetailAction.class);
    
    @Resource(name = DisabilityCancelDetailService.BEAN_DEFAULT)
    protected DisabilityCancelDetailService disabilityCancelDetailService;
    
    @Resource(name = CoverageService.BEAN_DEFAULT)
    protected CoverageService coverageService;
    
    @Resource(name = ChannelOrgService.BEAN_DEFAULT)
    protected ChannelOrgService channelOrgService;
    
    @Resource(name = PolicyService.BEAN_DEFAULT)
    protected PolicyService policyService;
    
    @Resource(name = PolicyQueryService.BEAN_DEFAULT)
    protected PolicyQueryService policyQueryService;

	@Resource(name = InsuredService.BEAN_DEFAULT)
	protected InsuredService insuredService;

	@Override
	public ActionForward process(ActionMapping mapping, ActionForm actionForm,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
		ProductCancelDetailForm inputForm = (ProductCancelDetailForm) ((ProductCancelDetailForm)actionForm==null?new ProductCancelDetailForm():actionForm);

		inputForm.setReason1List(this.getCodeTableInfo("T_DISABILITY_CANCEL_REASON", " TYPE1=1 AND TYPE2=1 ", " code asc "));
		inputForm.setReason2List(this.getCodeTableInfo("T_DISABILITY_CANCEL_REASON", " TYPE1=1 AND TYPE2=2 ", " code asc "));
		inputForm.setReason3List(this.getCodeTableInfo("T_DISABILITY_CANCEL_REASON", " TYPE1=1 AND TYPE2=3 ", " code asc "));
		
		List<Map<String, Object>> reason4 = this.getCodeTableInfo("T_DISABILITY_CANCEL_REASON", " TYPE1=2 AND CODE NOT IN ('X01','X02','X03','X04','X05') ", " code asc ");
		reason4 = disabilityCancelDetailService.sortNonHealthCancelDetail(reason4);
		inputForm.setReason4List(reason4);
		
		inputForm.setReason5List(this.getCodeTableInfo("T_DISABILITY_CANCEL_REASON", " TYPE1=2 AND CODE IN ('X01','X02','X03','X04','X05') ", " code asc "));

		Long policyId = NumericUtils.parseLong(request.getParameter("policyId"));
		Long changeID = NumericUtils.parseLong(request.getParameter("changeId"));
		Long policyChgID = NumericUtils.parseLong(request.getParameter("policyChgId"));
		String items = (String) request.getParameter("itemId");
		String waiverProdIds = (String) request.getParameter("waiverProdId");
		String[] itemArray = null;
		if (items != null && items.length() > 0) {
			itemArray = items.split(",");
		}
		Set<Long> insuredSet = new HashSet<Long>();
		List<InsuredVO> insuredList = new ArrayList<InsuredVO>();
		if (itemArray != null) {
			for (String itemID : itemArray) {
				DisabilityCancelDetailVO vo = disabilityCancelDetailService.findByItemId(policyId, Long.valueOf(itemID), changeID, policyChgID);
				if (vo != null) {
					InsuredVO insuredVO = insuredService.load(vo.getInsuredID());
					if (insuredList.contains(insuredVO) == false) {
						insuredList.add(insuredVO);
					}
				} else {
					CoverageVO coverageVO = coverageService.load(Long.valueOf(itemID));
					CoverageInsuredVO coverageInsuredVO = coverageVO.getLifeInsured1();
					if (coverageInsuredVO != null) {
						InsuredVO insuredVO = coverageInsuredVO.getInsured();
						if (insuredList.contains(insuredVO) == false) {
							insuredList.add(insuredVO);
						}
					}
				}
			}
		}
		
		for (InsuredVO insuredVO : insuredList) {
			String name = "";
			// IR341390 豁免險因為getInsuredCategory為0會造成組name時出錯，排除InsuredCategory為0的
			if (CodeCst.INSURED_CATGORY_NA.equals(insuredVO.getInsuredCategory()) == false) {
				if (CodeCst.INSURED_CATGORY_SELF.equals(insuredVO.getInsuredCategory())) {
					// 主被保險人
					name = StringResource.getStringData("MSG_1252242", AppContext.getCurrentUser().getLangId()) + "-"
							+ insuredVO.getName();
				} else {
					// 附約被保險人
					name = NBUtils.getTCodeData("T_NB_INSURED_CATEGORY").get(insuredVO.getNbInsuredCategory())
							.get("desc") + "-" + insuredVO.getName();
				}
				inputForm.setMasterInsured(
						appendData(insuredSet, inputForm.getMasterInsured(), insuredVO.getListId(), name));
			}
		}
		
		// 豁免險Radio btn
		if (StringUtils.isNullOrEmpty(waiverProdIds) == false) {
			PolicyInfo policyInfo = policyService.load(policyId);
			String name = StringResource.getStringData("MSG_100182", AppContext.getCurrentUser().getLangId()) + "-" + policyInfo.getPolicyHolder().getName();
			Long waiverInsuredId = new Long(0);
			inputForm.setMasterInsured(appendData(insuredSet, inputForm.getMasterInsured(), waiverInsuredId, name));
		}
		
		inputForm.setPolicyId(policyId);
		inputForm.setItems(items);
		inputForm.setSourceType((String) request.getParameter("sourceType"));
		if (request.getParameter("changeId") != null && request.getParameter("changeId").length() > 0) {
			inputForm.setChangeId(Long.valueOf((String) request.getParameter("changeId")));
		}
		if (request.getParameter("policyChgId") != null && request.getParameter("policyChgId").length() > 0) {
			inputForm.setPolicyChgId(Long.valueOf((String) request.getParameter("policyChgId")));
		}
		return mapping.findForward("success");
	}
	
	/**
	 * 取得CodeTable資訊
	 * @param tableName
	 * @param whereClause
	 * @param orderBy
	 * @return
	 */
	private List<Map<String, Object>> getCodeTableInfo(String tableName, String whereClause, String orderBy) {
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		Table codeTable;
		try {
			codeTable = CodeTable.checkReload(tableName);
			String data[][] = codeTable.getData(whereClause, orderBy);
			for (int i = 0; i < data.length; i++) {
				Map<String, Object> map = new HashMap<String, Object>();
				String sCodeId = data[i][0];
				String desc = data[i][2];
				map.put("sCodeId", sCodeId);
				map.put("desc", desc);
				list.add(map);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	/**
	 * 要保人radioBtn資訊 顯示不重複資料
	 * @param insuredSet
	 * @param input
	 * @param key
	 * @param value
	 * @return
	 */
	private List<Map<String, Object>> appendData(Set<Long> insuredSet,List<Map<String, Object>> input,Long key,Object value){
		if (input == null) {
			input = new ArrayList<Map<String, Object>>();
		} 
		
		if (input.size() == 0) {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("sCodeId", key);
			map.put("desc", value);
			input.add(map);
			insuredSet.add(key);
		} else {
			Map<String, Object> map = new HashMap<String, Object>();
			if (!insuredSet.contains(key)) {
				map.put("sCodeId", key);
				map.put("desc", value);
				insuredSet.add(key);
				input.add(map);
			}
		}
		return input;
	}
}
