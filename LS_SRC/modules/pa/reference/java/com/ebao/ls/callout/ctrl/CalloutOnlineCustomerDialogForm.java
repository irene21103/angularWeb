package com.ebao.ls.callout.ctrl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.Factory;
import org.apache.commons.collections.ListUtils;

import com.ebao.ls.callout.vo.CalloutTransVO;
import com.ebao.ls.pty.vo.CustomerVO;
import com.ebao.pub.web.pager.PagerFormImpl;

public class CalloutOnlineCustomerDialogForm extends PagerFormImpl {

    private static final long serialVersionUID = 1L;

    private String certiCode;
    
    private List<CustomerVO> customerList;
    
    public String getCertiCode() {
        return certiCode;
    }

    public void setCertiCode(String certiCode) {
        this.certiCode = certiCode;
    }

	public List<CustomerVO> getCustomerList() {
		return customerList;
	}

	public void setCustomerList(List<CustomerVO> customerList) {
		this.customerList = customerList;
	}
}
