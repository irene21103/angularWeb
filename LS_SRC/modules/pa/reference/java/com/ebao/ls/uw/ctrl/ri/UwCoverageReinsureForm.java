package com.ebao.ls.uw.ctrl.ri;

import java.math.BigDecimal;

public class UwCoverageReinsureForm {
  
  private Integer jspListId;
	
	private String isCheck;

	private Long reinsurerId;
	
	private String reinsurerName;
	
	private Long itemId;
	
	private String insuredName;
	
	private String productCode;
	
	private String productName;
	
	private BigDecimal riskAmount;
	
	private BigDecimal facAmount;
	
	private String deleteIndi;

	public Long getReinsurerId() {
		return reinsurerId;
	}

	public void setReinsurerId(Long reinsurerId) {
		this.reinsurerId = reinsurerId;
	}

	public Long getItemId() {
		return itemId;
	}

	public void setItemId(Long itemId) {
		this.itemId = itemId;
	}

	public BigDecimal getRiskAmount() {
		return riskAmount;
	}

	public void setRiskAmount(BigDecimal riskAmount) {
		this.riskAmount = riskAmount;
	}

	public BigDecimal getFacAmount() {
		return facAmount;
	}

	public void setFacAmount(BigDecimal facAmount) {
		this.facAmount = facAmount;
	}

	public String getReinsurerName() {
		return reinsurerName;
	}

	public void setReinsurerName(String reinsurerName) {
		this.reinsurerName = reinsurerName;
	}

	public String getInsuredName() {
		return insuredName;
	}

	public void setInsuredName(String insuredName) {
		this.insuredName = insuredName;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getIsCheck() {
    return isCheck;
  }

  public void setIsCheck(String isCheck) {
    this.isCheck = isCheck;
  }

  public String getDeleteIndi() {
		return deleteIndi;
	}

	public void setDeleteIndi(String deleteIndi) {
		this.deleteIndi = deleteIndi;
	}

  public Integer getJspListId() {
    return jspListId;
  }

  public void setJspListId(Integer jspListId) {
    this.jspListId = jspListId;
  }

	
}
