package com.ebao.ls.uw.ds;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.transaction.UserTransaction;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.ebao.foundation.common.lang.exception.ExceptionUtils;
import com.ebao.foundation.commons.exceptions.ExceptionUtil;
import com.ebao.foundation.module.db.hibernate.HibernateSession3;
import com.ebao.ls.arap.data.ds.ColVoucherDS;
import com.ebao.ls.arap.pub.ci.CashCI;
import com.ebao.ls.arap.pub.ci.CashCIVO;
import com.ebao.ls.arap.pub.ci.PremCI;
import com.ebao.ls.arap.pub.ds.AcquirerService;
import com.ebao.ls.arap.vo.AcquirerVO;
import com.ebao.ls.arap.vo.CashRefundVO;
import com.ebao.ls.arap.vo.CashVO;
import com.ebao.ls.arap.vo.ColVoucherVO;
import com.ebao.ls.foundation.dao.SQLBuilder;
import com.ebao.ls.foundation.dao.SQLBuilder.IgnoreEmpty;
import com.ebao.ls.foundation.dao.SQLBuilder.IgnoreNull;
import com.ebao.ls.foundation.dao.pager.PagerDaoHelper;
import com.ebao.ls.foundation.util.BeanConvertor;
import com.ebao.ls.gl.ci.ChequeCI;
import com.ebao.ls.pa.nb.bs.DisabilityCancelDetailService;
import com.ebao.ls.pa.nb.bs.rule.ProposalRuleResultService;
import com.ebao.ls.pa.nb.data.UwMedicalItemDao;
import com.ebao.ls.pa.nb.data.UwMedicalLetterDao;
import com.ebao.ls.pa.nb.data.UwMedicalReasonDao;
import com.ebao.ls.pa.nb.util.Cst;
import com.ebao.ls.pa.nb.util.NBUtils;
import com.ebao.ls.pa.nb.util.NumericUtils;
import com.ebao.ls.pa.nb.vo.DisabilityCancelDetailVO;
import com.ebao.ls.pa.nb.vo.ProposalRuleResultVO;
import com.ebao.ls.pa.pub.bs.InsuredDisabilityIcfService;
import com.ebao.ls.pa.pub.bs.InsuredDisabilityListService;
import com.ebao.ls.pa.pub.bs.PolicyHolderService;
import com.ebao.ls.pa.pub.ci.CalculatorCI;
import com.ebao.ls.pa.pub.ci.CoverageCI;
import com.ebao.ls.pa.pub.data.EndorsementCodeDao;
import com.ebao.ls.pa.pub.data.bo.EndorsementCode;
import com.ebao.ls.pa.pub.data.bo.Insured;
import com.ebao.ls.pa.pub.data.bo.InsuredDisabilityIcf;
import com.ebao.ls.pa.pub.data.bo.InsuredDisabilityIcfImpl;
import com.ebao.ls.pa.pub.data.bo.InsuredDisabilityList;
import com.ebao.ls.pa.pub.data.bo.Policy;
import com.ebao.ls.pa.pub.data.bo.UwMedicalItem;
import com.ebao.ls.pa.pub.data.bo.UwMedicalLetter;
import com.ebao.ls.pa.pub.data.bo.UwMedicalReason;
import com.ebao.ls.pa.pub.data.bo.UwSickFormLetter;
import com.ebao.ls.pa.pub.data.org.InsuredDao;
import com.ebao.ls.pa.pub.data.org.InsuredDisabilityIcfDao;
import com.ebao.ls.pa.pub.data.org.InsuredDisabilityListDao;
import com.ebao.ls.pa.pub.data.org.PolicyDao;
import com.ebao.ls.pa.pub.ref.api.prd.ProductService;
import com.ebao.ls.pa.pub.ref.vo.prd.ProductVO;
import com.ebao.ls.pa.pub.service.PolicyService;
import com.ebao.ls.pa.pub.vo.CoverageVO;
import com.ebao.ls.pa.pub.vo.InsuredDisabilityListVO;
import com.ebao.ls.pa.pub.vo.PolicyHolderVO;
import com.ebao.ls.pa.pub.vo.UwAddInformLetterVO;
import com.ebao.ls.prd.product.vo.ProdVerEndorsementVO;
import com.ebao.ls.pty.ci.BankAccountCI;
import com.ebao.ls.pty.ci.CustomerCI;
import com.ebao.ls.pty.ci.EmployeeCI;
import com.ebao.ls.pty.vo.AddressVO;
import com.ebao.ls.pty.vo.BankAccountVO;
import com.ebao.ls.pty.vo.CompanyCustomerVO;
import com.ebao.ls.pty.vo.CustomerVO;
import com.ebao.ls.pty.vo.EmployeeVO;
import com.ebao.ls.pty.vo.PartyVO;
import com.ebao.ls.pub.CodeCst;
import com.ebao.ls.pub.cst.ProposalRuleMsg;
import com.ebao.ls.spi.ri.RiServiceSPI;
import com.ebao.ls.uw.bo.QueryHelper;
import com.ebao.ls.uw.bo.UwCalculation;
import com.ebao.ls.uw.bo.UwProductDecision;
import com.ebao.ls.uw.bo.UwRestriction;
import com.ebao.ls.uw.ci.UwPolicyCI;
import com.ebao.ls.uw.data.TLienEmDelegate;
import com.ebao.ls.uw.data.TUwConditionDelegate;
import com.ebao.ls.uw.data.TUwEndorsementDelegate;
import com.ebao.ls.uw.data.TUwExclusionDelegate;
import com.ebao.ls.uw.data.TUwExtraPremDelegate;
import com.ebao.ls.uw.data.TUwInsuredDisabilityIcfDelegate;
import com.ebao.ls.uw.data.TUwInsuredListDelegate;
import com.ebao.ls.uw.data.TUwPolicyDelegate;
import com.ebao.ls.uw.data.TUwProductDelegate;
import com.ebao.ls.uw.data.TUwReduceDelegate;
import com.ebao.ls.uw.data.TUwRiFacDelegate;
import com.ebao.ls.uw.data.UwSickFormItemDelegate;
import com.ebao.ls.uw.data.UwSickFormLetterDelegate;
import com.ebao.ls.uw.data.bo.UwCondition;
import com.ebao.ls.uw.data.bo.UwEndorsement;
import com.ebao.ls.uw.data.bo.UwExclusion;
import com.ebao.ls.uw.data.bo.UwExtraLoading;
import com.ebao.ls.uw.data.bo.UwInsuredDisability;
import com.ebao.ls.uw.data.bo.UwInsuredDisabilityIcf;
import com.ebao.ls.uw.data.bo.UwLien;
import com.ebao.ls.uw.data.bo.UwLifeInsured;
import com.ebao.ls.uw.data.bo.UwPolicy;
import com.ebao.ls.uw.data.bo.UwProduct;
import com.ebao.ls.uw.data.bo.UwRiFac;
import com.ebao.ls.uw.data.query.UwQueryDao;
import com.ebao.ls.uw.ds.constant.UwSourceTypeConstants;
import com.ebao.ls.uw.ds.constant.UwStatusConstants;
import com.ebao.ls.uw.ds.sp.CompleteUWProcessSp;
import com.ebao.ls.uw.ds.sp.QueryUwDataSp;
import com.ebao.ls.uw.ds.vo.QueryHelperVO;
import com.ebao.ls.uw.ds.vo.UwEmVO;
import com.ebao.ls.uw.ds.vo.UwRestrictionVO;
import com.ebao.ls.uw.vo.LienEmVO;
import com.ebao.ls.uw.vo.UwConditionVO;
import com.ebao.ls.uw.vo.UwEndorsementVO;
import com.ebao.ls.uw.vo.UwExclusionVO;
import com.ebao.ls.uw.vo.UwExtraLoadingVO;
import com.ebao.ls.uw.vo.UwLienVO;
import com.ebao.ls.uw.vo.UwLifeInsuredVO;
import com.ebao.ls.uw.vo.UwPolicyVO;
import com.ebao.ls.uw.vo.UwProductVO;
import com.ebao.ls.uw.vo.UwRiFacVO;
import com.ebao.ls.uw.vo.UwTransferVO;
import com.ebao.pub.annotation.DataModeChgInvokePoint;
import com.ebao.pub.annotation.DataModeChgModifyPoint;
import com.ebao.pub.annotation.DataModeChgMovePoint;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.framework.AppException;
import com.ebao.pub.framework.ExceptionFactory;
import com.ebao.pub.framework.GenericDS;
import com.ebao.pub.framework.GenericException;
import com.ebao.pub.framework.ObjectNotFoundException;
import com.ebao.pub.framework.RTException;
import com.ebao.pub.i18n.lang.StringResource;
import com.ebao.pub.util.BeanUtils;
import com.ebao.pub.util.DBean;
import com.ebao.pub.util.DateUtils;
import com.ebao.pub.util.Log;
import com.ebao.pub.util.StringUtils;
import com.ebao.pub.util.Trans;
import com.ebao.pub.util.TransUtils;
import com.hazelcast.util.StringUtil;

/**
 * <p>
 * Title:Underwriting
 * </p>
 * <p>
 * Description:
 * </p>
 * <p>
 * Company: eBaoTech Corporation
 * </p>
 */
public class UwPolicyDSImpl extends GenericDS implements UwPolicyService {
	public static final long APP_WHEN_CA_INDX_INDI_CANT_Y = 20510020119L;

	private Logger logger = Logger.getLogger(UwPolicyDSImpl.class);

	public static final String PENDING_ISSUE_CAUSE_OTHER = "2";

	@Resource(name = CalculatorCI.BEAN_DEFAULT)
	private CalculatorCI calculatorCI;

	@Resource(name = PolicyDao.BEAN_DEFAULT)
	private PolicyDao<Policy> policyDao;

	@Resource(name = EndorsementCodeDao.BEAN_PA_ENDORSEMENT_CODE)
	private EndorsementCodeDao endorsementCodeDao;

	@Resource(name = TUwRiFacDelegate.BEAN_DEFAULT)
	private TUwRiFacDelegate uwRiFacDao;

	@Resource(name = RiServiceSPI.BEAN_DEFAULT)
	private RiServiceSPI facRequestService;

	@Resource(name = CoverageCI.BEAN_DEFAULT)
	private CoverageCI coverageCI;

	@Resource(name = CustomerCI.BEAN_DEFAULT)
	private CustomerCI customerCI;

	@Resource(name = BankAccountCI.BEAN_DEFAULT)
	private BankAccountCI bankCI;

	@Resource(name = InsuredDao.BEAN_DEFAULT)
	private InsuredDao<Insured> insuredDao;

	public CustomerCI getCustomerCI() {
		return customerCI;
	}

	@Resource(name = UwQueryDao.BEAN_DEFAULT)
	private UwQueryDao uwQueryDao;

	public UwQueryDao getUwQueryDao() {
		return uwQueryDao;
	}

	@Resource(name = TLienEmDelegate.BEAN_DEFAULT)
	private TLienEmDelegate lienEmDao;

	@Resource(name = TUwConditionDelegate.BEAN_DEFAULT)
	private TUwConditionDelegate uwConditionDao;

	@Resource(name = TUwEndorsementDelegate.BEAN_DEFAULT)
	private TUwEndorsementDelegate uwEndorsementDao;

	@Resource(name = TUwExclusionDelegate.BEAN_DEFAULT)
	private TUwExclusionDelegate uwExclusionDao;

	@Resource(name = TUwExtraPremDelegate.BEAN_DEFAULT)
	private TUwExtraPremDelegate uwExtraPremDao;

	@Resource(name = TUwInsuredListDelegate.BEAN_DEFAULT)
	private TUwInsuredListDelegate uwInsuredListDao;

	@Resource(name = TUwReduceDelegate.BEAN_DEFAULT)
	private TUwReduceDelegate uwReduceDao;

	@Resource(name = TUwPolicyDelegate.BEAN_DEFAULT)
	private TUwPolicyDelegate uwPolicyDao;

	@Resource(name = UwTransferService.BEAN_DEFAULT)
	private UwTransferService uwTransferService;

	@Resource(name = TUwProductDelegate.BEAN_DEFAULT)
	private TUwProductDelegate uwProductDao;

	@Resource(name = ProductService.BEAN_DEFAULT)
	private ProductService productService;

	@Resource(name = CashCI.BEAN_DEFAULT)
	private CashCI cashCI;

	@Resource(name = PremCI.BEAN_DEFAULT)
	private PremCI premCI;

	@Resource(name = ChequeCI.BEAN_DEFAULT)
	private ChequeCI chequeCI;

	public ProductService getProductService() {
		return productService;
	}

	@Resource(name = PolicyService.BEAN_DEFAULT)
	PolicyService policyService;

	@Resource(name = ProposalRuleResultService.BEAN_DEFAULT)
	protected ProposalRuleResultService proposalRuleResultService;

	@Resource(name = PolicyHolderService.BEAN_DEFAULT)
	private PolicyHolderService policyHolderService;

	@Resource(name = ColVoucherDS.BEAN_DEFAULT)
	private ColVoucherDS colVoucherDS;

	@Resource(name = UwPolicyCI.BEAN_DEFAULT)
	private UwPolicyCI uwPolicyCI;

	@Resource(name = EmployeeCI.BEAN_DEFAULT)
	private EmployeeCI employeeCI;

	@Resource(name = AcquirerService.BEAN_DEFAULT)
	private AcquirerService acquirerService;

	@Resource(name = DisabilityCancelDetailService.BEAN_DEFAULT)
	private DisabilityCancelDetailService disabilityCancelDetailService;

	@Resource(name = UwInsuredDisabilityService.BEAN_DEFAULT)
	private UwInsuredDisabilityService uwInsuredDisabilityService;

	@Resource(name = InsuredDisabilityListService.BEAN_DEFAULT)
	private InsuredDisabilityListService insuredDisabilityListService;
	
	@Resource(name = InsuredDisabilityListDao.BEAN_DEFAULT)
	protected InsuredDisabilityListDao insuredDisabilityListDao;
	
	@Resource(name = InsuredDisabilityIcfDao.BEAN_DEFAULT)
	protected InsuredDisabilityIcfDao insuredDisabilityIcfDao;
	
    @Resource(name = UwInsuredDisabilityIcfService.BEAN_DEFAULT)
    private UwInsuredDisabilityIcfService uwInsuredDisabilityIcfService;
    
    @Resource(name = InsuredDisabilityIcfService.BEAN_DEFAULT)
 	private InsuredDisabilityIcfService insuredDisabilityIcfService;
    
	@Resource(name = TUwInsuredDisabilityIcfDelegate.BEAN_DEFAULT)
	private TUwInsuredDisabilityIcfDelegate uwInsuredDisabilityIcfDao;
	
	@Resource(name = UwAddInformLetterService.BEAN_DEFAULT)
	private UwAddInformLetterService uwAddInformLetterService;

	@Resource(name = UwSickFormItemDelegate.BEAN_DEFAULT)
	private UwSickFormItemDelegate uwSickFormItemDao;
	
	@Resource(name = UwSickFormLetterDelegate.BEAN_DEFAULT)
	private UwSickFormLetterDelegate uwSickFormLetterDao;
	
	@Resource(name = UwMedicalItemDao.BEAN_DEFAULT)
	private UwMedicalItemDao<UwMedicalItem> uwMedicalItemDao;

	@Resource(name = UwMedicalLetterDao.BEAN_DEFAULT)
	private UwMedicalLetterDao<UwMedicalLetter> uwMedicalLetterDao;

	@Resource(name = UwMedicalReasonDao.BEAN_DEFAULT)
	private UwMedicalReasonDao<UwMedicalReason> uwMedicalReasonDao;
	
	/**
	 * 保全項:次標加費清除變更時，要移除ProposalRuleResult
	 *
	 * @param policyId
	 * @param changeId
	 */
	@Override
	public void removeProposalRuleResultByServiceChangeExtraPremium(Long policyId, Long changeId) {
		// SERVICE__CHANGE_EXTRA_PREMIUM
		String[] ruleNames = new String[] { ProposalRuleMsg.PROPOSAL_RULE_MSG_EXCLUSION_LETTER,
				ProposalRuleMsg.PROPOSAL_RULE_MSG_EXTRA_PREM_LETTER };

		List<ProposalRuleResultVO> proposalRuleResults = proposalRuleResultService.find(
				Restrictions.eq("policyId", policyId), Restrictions.eq("changeId", changeId),
				Restrictions.eq("ruleType", CodeCst.PROPOSAL_RULE_TYPE__UW_LETTER),
				Restrictions.in("ruleName", ruleNames));
		for (ProposalRuleResultVO resultVO : proposalRuleResults) {
			boolean flag = checkProposalRuleResultSendDocStatus(resultVO.getListId());
			if (!flag) {
				proposalRuleResultService.remove(resultVO.getListId());
			}
		}

	}

	/**
	 * 此規則校驗有無發過信函
	 *
	 * @param proposalRuleResultId
	 * @return
	 */
	private boolean checkProposalRuleResultSendDocStatus(Long proposalRuleResultId) {
		DBean db = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean flag = false;
		try {
			db = new DBean(false);
			db.connect();

			StringBuffer sql = new StringBuffer();
			sql.append("select doc.list_id,doc.document_no,o.issue_result_id from t_document doc ");
			sql.append("inner join T_OUTSTANDING_ISSUES_DOC o on doc.list_id = o.document_id ");
			sql.append("where o.issue_result_id = ? ");
			ps = db.getConnection().prepareStatement(sql.toString());

			ps.setLong(1, proposalRuleResultId);
			rs = ps.executeQuery();
			while (rs.next()) {
				Long document_id = rs.getLong("LIST_ID");
				if (document_id != null) {
					flag = true;
				}
				return flag;
			}
		} catch (Exception e) {
			throw ExceptionFactory.parse(e);
		} finally {
			DBean.closeAll(rs, ps, db);
		}
		return flag;
	}

	/**
	 * @param underwriteId
	 * @throws com.ebao.pub.framework.GenericException
	 * @return
	 */
	public Collection findUwConditionEntitis(Long underwriteId) throws GenericException {
		try {
			Collection c = uwConditionDao.findByUnderwriteId(underwriteId);
			return BeanUtils.copyCollection(UwConditionVO.class, c);
		} catch (Exception e) {
			throw ExceptionFactory.parse(e);
		}
	}

	/**
	 * creates a specified UwCondition
	 *
	 * @param value
	 *            the UwConditionVO
	 * @throws com.ebao.pub.framework.GenericException
	 */
	public void createUwCondition(UwConditionVO value) throws GenericException {
		try {
			UwCondition bo = new UwCondition();
			BeanUtils.copyProperties(bo, value);
			uwConditionDao.create(bo);
		} catch (Exception e) {
			throw ExceptionFactory.parse(e);
		}
	}

	/**
	 * deletes a specified UwCondition
	 *
	 * @param value
	 *            the UwConditionVO
	 * @throws com.ebao.pub.framework.GenericException
	 */
	public void removeUwCondition(UwConditionVO value) throws GenericException {
		uwConditionDao.remove(value.getUwListId());
	}

	/**
	 * remove uw endorsement by underwrite id
	 *
	 * @param underwriteId
	 * @throws GenericException
	 */
	public void removeUwEndorsementByUnderwriteId(Long underwriteId) throws GenericException {
		Collection endorsements = getUwQueryDao().getEndorsementCodeByUnderWriteId(underwriteId);
		if (endorsements != null) {
			Iterator iter = endorsements.iterator();
			try {
				while (iter.hasNext()) {
					UwEndorsementVO endorsementVO = (UwEndorsementVO) iter.next();
					UwEndorsement endorsement = new UwEndorsement();
					BeanUtils.copyProperties(endorsement, endorsementVO);
					uwEndorsementDao.remove(endorsement.getUwListId());
				}
			} catch (Exception e) {
				throw ExceptionFactory.parse(e);
			}
		}
	}

	/**
	 * remove uw exclusions by underwriteId
	 *
	 * @param underwriteId
	 * @throws GenericException
	 */
	public void removeUwExclusionsByUnderwriteId(Long underwriteId) throws GenericException {
		List<UwExclusion> exclusions = uwExclusionDao.findByUnderwriteId(underwriteId);
		if (exclusions != null) {
			Iterator iter = exclusions.iterator();
			while (iter.hasNext()) {
				UwExclusion exclusion = (UwExclusion) iter.next();
				uwExclusionDao.remove(exclusion.getUwListId());
			}
		}
	}

	@Override
	public void removeUwExclusionsByUwIdIemId(Long underwriteId, Long itemId) throws GenericException {
		List<UwExclusion> exclusions = uwExclusionDao.findByUnderwriteIdItemId(underwriteId, itemId);
		if (exclusions != null) {
			Iterator iter = exclusions.iterator();
			while (iter.hasNext()) {
				UwExclusion exclusion = (UwExclusion) iter.next();
				uwExclusionDao.remove(exclusion.getUwListId());
			}
		}

	}

	/**
	 * @param underwriteId
	 * @throws com.ebao.pub.framework.GenericException
	 * @return
	 */
	public Collection<UwEndorsementVO> findUwEndorsementEntitis(Long underwriteId) throws GenericException {
		try {
			// Collection c = UwEndorsement.findByUnderwriteId(underwriteId);
			Collection<UwEndorsementVO> c = getUwQueryDao().getEndorsementCodeByUnderWriteId(underwriteId);
			return c;
		} catch (Exception e) {
			throw ExceptionFactory.parse(e);
		}
	}

	public Collection<UwEndorsementVO> findUwEndorsementEntitis(Long underwriteId, Long uwExclusionGroupId)
			throws GenericException {
		try {
			Collection<UwEndorsementVO> c = getUwQueryDao().getEndorsementCodeByUnderWriteId(underwriteId,
					uwExclusionGroupId);
			return c;
		} catch (Exception e) {
			throw ExceptionFactory.parse(e);
		}
	}

	public List<UwEndorsementVO> findUwEndorsementByUnderwriteId(Long underwriteId) throws GenericException {
		List<UwEndorsementVO> result = new ArrayList<UwEndorsementVO>();
		try {
			List<UwEndorsement> uwEndorsementList = uwEndorsementDao.findByUnderwriteId(underwriteId);
			if (uwEndorsementList == null || uwEndorsementList.size() == 0) {
				return null;
			}
			for (UwEndorsement uwEndorsement : uwEndorsementList) {
				UwEndorsementVO uwEndorsementVO = new UwEndorsementVO();
				uwEndorsement.copyToVO(uwEndorsementVO, false);
				result.add(uwEndorsementVO);
			}
		} catch (Exception e) {
			throw ExceptionFactory.parse(e);
		}
		return result;
	}

	/**
	 * @param underwriteId
	 * @param groupId
	 * @throws com.ebao.pub.framework.GenericException
	 * @return
	 */
	public List<UwEndorsementVO> findUwEndorsementGroupEntitis(Long underwriteId, Long groupId)
			throws GenericException {
		List<UwEndorsementVO> result = new ArrayList<UwEndorsementVO>();
		try {
			List<UwEndorsement> uwEndorsementList = uwEndorsementDao.findByUnderwriteIdAndGroupId(underwriteId,
					groupId);
			for (UwEndorsement uwEndorsement : uwEndorsementList) {
				UwEndorsementVO uwEndorsementVO = new UwEndorsementVO();
				uwEndorsement.copyToVO(uwEndorsementVO, false);
				result.add(uwEndorsementVO);
			}
		} catch (Exception e) {
			throw ExceptionFactory.parse(e);
		}
		return result;
	}

	/**
	 * creates a specified UwEndorsement
	 *
	 * @param value
	 *            the UwEndorsementVO
	 * @throws com.ebao.pub.framework.GenericException
	 */
	public void createUwEndorsement(UwEndorsementVO value) throws GenericException {
		try {
			UwEndorsement bo = new UwEndorsement();
			BeanUtils.copyProperties(bo, value);
			uwEndorsementDao.create(bo);
			bo.copyToVO(value, false);
		} catch (Exception e) {
			throw ExceptionFactory.parse(e);
		}
	}

	/**
	 * @param underwriteId
	 * @throws com.ebao.pub.framework.GenericException
	 * @return
	 */
	public Collection<UwExclusionVO> findUwExclusionEntitis(Long underwriteId) throws GenericException {
		try {
			// Collection c = UwExclusion.findByUnderwriteId(underwriteId);
			// return BeanUtils.copyCollection(UwExclusionVO.class, c);
			return getUwQueryDao().getUWExclusionCodeWithDeclIndi(underwriteId);
		} catch (Exception e) {
			throw ExceptionFactory.parse(e);
		}
	}

	public Collection<UwExclusionVO> findUwExclusionGroupEntitis(Long underwriteId, Long groupId)
			throws GenericException {
		try {
			return getUwQueryDao().getUWExclusionCodeWithGroupId(underwriteId, groupId);
		} catch (Exception e) {
			throw ExceptionFactory.parse(e);
		}
	}

	public List<Map<String, Object>> findUwExclusionMsgIdByInsuredPartyId(Long underwriteId, Long insuredPartyId)
			throws GenericException {
		try {
			return getUwQueryDao().getUwExclusionMsgIdByInsuredPartyId(underwriteId, insuredPartyId);
		} catch (Exception e) {
			throw ExceptionFactory.parse(e);
		}
	}

	/**
	 * <p>
	 * Description :
	 * </p>
	 * <p>
	 * Created By : simon.huang
	 * </p>
	 * <p>
	 * Create Time : Jul 7, 2015
	 * </p>
	 *
	 * @param underwriteId
	 * @param itemId
	 * @param insuredId
	 * @return
	 * @throws GenericException
	 */
	public Collection<UwExclusionVO> findUwExclusionGroupEntitis(Long underwriteId, Long groupId, Long insuredPartyId)
			throws GenericException {
		try {
			return getUwQueryDao().getUWExclusionCodeWithGroupIdAndInsuredId(underwriteId, groupId, insuredPartyId);
		} catch (Exception e) {
			throw ExceptionFactory.parse(e);
		}
	}

	public Collection<UwExclusionVO> findUwExclusionGroupEntitis(Long underwriteId, Long groupId, Long insuredPartyId,
			Long msgId) throws GenericException {
		try {
			return getUwQueryDao().getUWExclusionCodeWithGroupIdAndInsuredId(underwriteId, groupId, insuredPartyId,
					msgId);
		} catch (Exception e) {
			throw ExceptionFactory.parse(e);
		}
	}

	/**
	 * <p>
	 * Description : 取得保單核保批註下個group index
	 * </p>
	 * <p>
	 * Created By : simon.huang
	 * </p>
	 * <p>
	 * Create Time : Aug 18, 2016
	 * </p>
	 *
	 * @param policyId
	 * @return
	 */
	public BigDecimal getNextUwExclusionGroupId(Long underwriteId) {
		DBean db = null;
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		BigDecimal groupId = null;

		try {
			db = new DBean();
			db.connect();
			con = db.getConnection();
			// 2017-05-12 Kate 不同underwriteId時，groupId重新從一開始
			ps = con.prepareStatement(" select nvl(max(a.uw_exclusion_group_id), 0) + 1 next_group_id "
					+ " from t_uw_exclusion a " + " where underwrite_id = ?");
			ps.setLong(1, underwriteId);
			rs = ps.executeQuery();
			while (rs.next()) {
				groupId = rs.getBigDecimal("next_group_id");
				break;
			}
		} catch (SQLException e) {
			throw new RTException(e);
		} catch (ClassNotFoundException e) {
			throw new RTException(e);
		} finally {
			DBean.closeAll(rs, ps, db);
		}

		if (groupId == null) {
			groupId = new BigDecimal(1);
		}
		return groupId;
	}

	/**
	 * creates a specified UwExclusion
	 *
	 * @param value
	 *            the UwExclusionVO
	 * @throws com.ebao.pub.framework.GenericException
	 */
	public void createUwExclusion(UwExclusionVO value) throws GenericException {
		try {
			UwExclusion bo = new UwExclusion();
			BeanUtils.copyProperties(bo, value);
			uwExclusionDao.create(bo);
			bo.copyToVO(value, false);
		} catch (Exception e) {
			throw ExceptionFactory.parse(e);
		}
	}

	/**
	 * @param underwriteId
	 * @throws com.ebao.pub.framework.GenericException
	 * @return
	 */
	@DataModeChgMovePoint
	public Collection<UwExtraLoadingVO> findUwExtraLoadingEntitis(Long underwriteId) throws GenericException {
		try {
			Collection c = uwExtraPremDao.findByUnderwriteId(underwriteId);
			return BeanUtils.copyCollection(UwExtraLoadingVO.class, c);
		} catch (Exception e) {
			throw ExceptionFactory.parse(e);
		}
	}

	/**
	 * @param underwriteId
	 * @param itemId
	 * @throws com.ebao.pub.framework.GenericException
	 * @return
	 */
	public Collection findUwExtraLoadingEntitis(Long underwriteId, Long itemId) throws GenericException {
		// return TUwExtraPremDelegate.findByUnderwriteIdAndItemId(underwriteId,
		// itemId);
		try {
			Collection c = uwExtraPremDao.findByUnderwriteIdAndItemId(underwriteId, itemId);
			return BeanUtils.copyCollection(UwExtraLoadingVO.class, c);
		} catch (Exception e) {
			throw ExceptionFactory.parse(e);
		}
	}

	/**
	 * @param underwriteId
	 * @param itemId
	 * @param insuredId
	 * @throws com.ebao.pub.framework.GenericException
	 * @return
	 */
	@DataModeChgMovePoint
	public Collection findUwExtraLoadingEntitis(Long underwriteId, Long itemId, Long insuredId)
			throws GenericException {
		try {
			Collection c = uwExtraPremDao.findByUnderwriteIdAndItemIdAndInsuredId(underwriteId, itemId, insuredId);
			return BeanUtils.copyCollection(UwExtraLoadingVO.class, c);
		} catch (Exception e) {
			throw ExceptionFactory.parse(e);
		}
	}

	/**
	 * @param underwriteId
	 * @param itemId
	 * @param insuredId
	 * @throws com.ebao.pub.framework.GenericException
	 * @return
	 */
	public Collection findUwExtraLoadingEntitis(Long underwriteId, Long itemId, String extraType)
			throws GenericException {
		try {
			Criteria criteria = HibernateSession3.currentSession().createCriteria(UwExtraLoading.class);
			criteria.add(Expression.eq("underwriteId", underwriteId));
			criteria.add(Expression.eq("itemId", itemId));
			criteria.add(Expression.eq("extraType", extraType));
			Collection<UwExtraLoading> c = criteria.list();
			return BeanUtils.copyCollection(UwExtraLoadingVO.class, c);
		} catch (Exception e) {
			throw ExceptionFactory.parse(e);
		}
	}

	/**
	 * creates a specified UwExtraLoading
	 *
	 * @param value
	 *            the UwExtraLoadingVO
	 * @throws com.ebao.pub.framework.GenericException
	 */
	public void createUwExtraLoading(UwExtraLoadingVO value) throws GenericException {
		UwExtraLoading bo = new UwExtraLoading();
		BeanUtils.copyProperties(bo, value);
		Long uwListId = uwExtraPremDao.create(bo);
		value.setUwListId(uwListId);
	}

	/**
	 * deletes a specified UwExtraLoading
	 *
	 * @param value
	 *            the UwExtraLoadingVO
	 * @throws com.ebao.pub.framework.GenericException
	 */
	@DataModeChgInvokePoint
	public void removeUwExtraLoading(UwExtraLoadingVO value) throws GenericException {
		try {
			uwExtraPremDao.remove(value.getUwListId());
		} catch (Exception e) {
			throw ExceptionFactory.parse(e);
		}
	}

	@Override
	public void removeUwExtraLoadingByUnderwriteId(Long underwriteId) throws GenericException {
		try {
			List<UwExtraLoading> extraLoadings = uwExtraPremDao.findByUnderwriteId(underwriteId);
			for (int i = 0; i < extraLoadings.size(); i++) {
				UwExtraLoading uwExtraLoading = extraLoadings.get(i);
				uwExtraPremDao.remove(uwExtraLoading.getUwListId());
			}
		} catch (Exception e) {
			throw ExceptionFactory.parse(e);
		}
	}

	@Override
	public void removeUwExtraLoadingByUwIdItemId(Long underwriteId, Long itemId) throws GenericException {
		try {
			List<UwExtraLoading> extraLoadings = uwExtraPremDao.findByUnderwriteIdAndItemId(underwriteId, itemId);
			for (int i = 0; i < extraLoadings.size(); i++) {
				UwExtraLoading uwExtraLoading = extraLoadings.get(i);
				uwExtraPremDao.remove(uwExtraLoading.getUwListId());
			}
		} catch (Exception e) {
			throw ExceptionFactory.parse(e);
		}
	}

	/**
	 * updates the specified UwExtraLoading
	 *
	 * @param value
	 *            the UwExtraLoadingVO
	 * @throws com.ebao.pub.framework.GenericException
	 */
	public void updateUwExtraLoading(UwExtraLoadingVO value) throws GenericException {
		UwExtraLoading bo = uwExtraPremDao.load(value.getUwListId());
		BeanUtils.copyProperties(bo, value);
	}

	/**
	 * @param underwriteId
	 * @throws com.ebao.pub.framework.GenericException
	 * @return
	 */
	@DataModeChgMovePoint("move the logic from old bo:UwLifeInsured")
	public Collection findUwLifeInsuredEntitis(Long underwriteId) throws GenericException {
		try {
			Collection<UwLifeInsured> c = uwInsuredListDao.findByUnderwriteId(underwriteId);
			java.util.List<UwLifeInsured> list = new java.util.ArrayList<UwLifeInsured>();
			for (UwLifeInsured entity : c) {
				UwLifeInsured lifeInsured = new UwLifeInsured();
				CustomerVO customer = getCustomerCI().getPerson(entity.getInsuredId());
				com.ebao.pub.util.BeanUtils.copyProperties(lifeInsured, customer);
				BeanUtils.copyProperties(lifeInsured, entity, false);
				list.add(lifeInsured);
			}
			return BeanUtils.copyCollection(UwLifeInsuredVO.class, list);
		} catch (Exception e) {
			throw ExceptionFactory.parse(e);
		}
	}

	/**
	 * updates the specified UwLifeInsured
	 *
	 * @param value
	 *            the UwLifeInsuredVO
	 * @throws com.ebao.pub.framework.GenericException
	 */
	public void updateUwLifeInsured(UwLifeInsuredVO value) throws GenericException {
		UwLifeInsured entity = uwInsuredListDao.load(value.getUwListId());
		com.ebao.pub.util.BeanUtils.copyProperties(entity, value);

		// 同步核保頁面被保險人相關設定至被保險人主檔
		List<Insured> insuredList = insuredDao.findByPolicyIdAndPartyId(value.getPolicyId(), value.getInsuredId());
		int liabilityStatus =  policyDao.findLiabilityStatusByPolicyId(value.getPolicyId());
		for(Insured insured : insuredList) {
			 //身心障礙
			insured.setMedicalExamIndi(value.getMedicalExamIndi()); //是否體檢
			insured.setImageIndi(value.getImageIndi()); //是否調閱影像
			insured.setHoLifeSurveyIndi(value.getHoLifeSurveyIndi());//內勤人員生調

			//是否標準體
			insured.setStandLifeIndi(value.getStandLife());

			//PCR_391319 新式公會-新增被保險人受監護宣告  20201030 by Simon
			insured.setGuardianAncmnt(value.getGuardianAncmnt());
			insured.setDisabilityType(value.getDisabilityType());
			insured.setDisabilityCategory(value.getDisabilityCategory());
			insured.setDisabilityClass(value.getDisabilityClass());
			
			insuredDao.saveorUpdate(insured);

			//PCR-154024-身心障礙-核保主頁面修改被保險人身心障礙類別同步至未承保檔
			if (liabilityStatus == CodeCst.LIABILITY_STATUS__NON_VALIDATE) {
				List<DisabilityCancelDetailVO> disabilityCancelDetailVO = disabilityCancelDetailService.findByCertiCode(insured.getPolicyId(), "UNB", insured.getCertiCode());
				if (disabilityCancelDetailVO != null) {
					for (DisabilityCancelDetailVO cancelDetailVO : disabilityCancelDetailVO) {
						cancelDetailVO.setDisabilityCategory(value.getDisabilityCategory());
						cancelDetailVO.setDisabilityClass(value.getDisabilityClass());
						cancelDetailVO.setDisabilityType(value.getDisabilityType());
						disabilityCancelDetailService.saveOrUpdate(cancelDetailVO);
					}
				}
				
				
        		List<InsuredDisabilityList> oldInsuredDisabilityList = insuredDisabilityListService.findByPolicyId(insured.getPolicyId(), insured.getListId());
        		Map<String, InsuredDisabilityList> oldDisabilityTypeMap = new HashMap<String, InsuredDisabilityList>();
        		for( InsuredDisabilityList vo : oldInsuredDisabilityList) {
        			oldDisabilityTypeMap.put(vo.getDisabilityCode(), vo);
        		}


        		List<UwInsuredDisability> uwInsuredDisabilityList = uwInsuredDisabilityService.findByUwInsuredId(value.getUwListId());
        		for( UwInsuredDisability uwInsuredDisability: uwInsuredDisabilityList ) {
        			String disabilityType = uwInsuredDisability.getDisabilityType();
        			if( oldDisabilityTypeMap.containsKey(disabilityType) ) {
        				
        				InsuredDisabilityList insuredDisabilityList = oldDisabilityTypeMap.get(disabilityType);

            			List<InsuredDisabilityIcf> oldInsuredDisabilityIcfList = insuredDisabilityIcfDao.findDisabilityIcfListByCode(insuredDisabilityList.getInsured().getListId(),disabilityType);
            			Map<String, InsuredDisabilityIcf> oldDisabilityLevelMap = new HashMap<String, InsuredDisabilityIcf>();
            			for( InsuredDisabilityIcf insuredDisabilityIcf : oldInsuredDisabilityIcfList) {
            				oldDisabilityLevelMap.put(insuredDisabilityIcf.getDisabilityLevel(), insuredDisabilityIcf);
            			}
            			
            			List<UwInsuredDisabilityIcf> wInsuredDisabilityIcfList = uwInsuredDisabilityIcfDao.findByDisabilityCode( uwInsuredDisability.getUwInsuredId(), disabilityType );
            		
            			for( UwInsuredDisabilityIcf uwInsuredDisabilityIcf : wInsuredDisabilityIcfList) {
            				String disabilityLevel = uwInsuredDisabilityIcf.getDisabilityLevel();
            				
            				if( oldDisabilityLevelMap.containsKey(disabilityLevel) ) {
            					
            					oldDisabilityLevelMap.remove(disabilityLevel);
            					
            				}else {
            					
            					InsuredDisabilityIcf insuredDisabilityIcf = new InsuredDisabilityIcfImpl();
        						insuredDisabilityIcf.setInsuredDisabilityList(insuredDisabilityList);
        						insuredDisabilityIcf.setPolicyId(insuredDisabilityList.getPolicyId());
        						insuredDisabilityIcf.setDisabilityLevel(disabilityLevel);
        						insuredDisabilityIcfDao.save(insuredDisabilityIcf);
            					
            				}
            			}
            			
            			//移除剩下的icf編碼
            	  		for( String disabilityLevel: oldDisabilityLevelMap.keySet() ) {
            	  			InsuredDisabilityIcf InsuredDisabilityIcf = oldDisabilityLevelMap.get(disabilityLevel);
            	  			insuredDisabilityIcfDao.delete(InsuredDisabilityIcf);
            	  		}

        				//已經有的身障類別移除
        				oldDisabilityTypeMap.remove(disabilityType);
        			}else {
        				
        				//新增增加的被保險人身障類別
        				InsuredDisabilityListVO insuredDisabilityListVO = insuredDisabilityListService.createDisabilityCode(-1, -1, insured.getListId(), insured.getPolicyId(), disabilityType);
  
        				List<UwInsuredDisabilityIcf> wInsuredDisabilityIcfList = uwInsuredDisabilityIcfDao.findByDisabilityCode( uwInsuredDisability.getUwInsuredId(), disabilityType );
        				for( UwInsuredDisabilityIcf uwInsuredDisabilityIcf : wInsuredDisabilityIcfList) {
        					String disabilityLevel = uwInsuredDisabilityIcf.getDisabilityLevel();
        						//新增增加的身障類別對應的icf編碼
        						InsuredDisabilityList insuredDisabilityListVo1 = insuredDisabilityListDao.load(insuredDisabilityListVO.getDisabilityListId());
        						InsuredDisabilityIcf insuredDisabilityIcf = new InsuredDisabilityIcfImpl();
        						insuredDisabilityIcf.setInsuredDisabilityList(insuredDisabilityListVo1);
        						insuredDisabilityIcf.setPolicyId(insuredDisabilityListVo1.getPolicyId());
        						insuredDisabilityIcf.setDisabilityLevel(disabilityLevel);
        						insuredDisabilityIcfDao.save(insuredDisabilityIcf);
        						
        				}
        			}
        		}
        		
        		//刪除舊的身障類別及ICF編碼
        		for( String disabilityType: oldDisabilityTypeMap.keySet() ) {
        			
        			InsuredDisabilityList insuredDisabilityList = oldDisabilityTypeMap.get(disabilityType);
        			
        			List<InsuredDisabilityIcf> insuredDisabilityIcfList = insuredDisabilityIcfDao.findDisabilityIcfListByCode(insuredDisabilityList.getInsured().getListId(),disabilityType);
        			
        			for(InsuredDisabilityIcf insuredDisabilityIcf :insuredDisabilityIcfList) {
        				insuredDisabilityIcfDao.delete(insuredDisabilityIcf);
        			}
        			
        			insuredDisabilityListDao.delete( insuredDisabilityList );
        		}

        	}
		}
	}

	/**
	 * returns a UwPolicyVO associated with the given Ids
	 *
	 * @return a collection of UwPolicyVO objects
	 * @throws com.ebao.pub.framework.GenericException
	 */
	@DataModeChgInvokePoint
	public UwPolicyVO findUwPolicy(Long underwriteId) throws GenericException {
		UwPolicyVO value = new UwPolicyVO();
		UwPolicy bo = uwPolicyDao.load(underwriteId);
		boolean isSupportSqlDateType = BeanUtils.isSupportSqlDateType();
		BeanUtils.setSupportSqlDateType(true);
		BeanUtils.copyProperties(value, bo);
		BeanUtils.setSupportSqlDateType(isSupportSqlDateType);
		// added to get some sepical data(for workflow),by robert.xu on
		// 2008.4.21
		getUwQueryDao().getUwSepicalData(value);
		// added end
		return value;
	}

	/**
	 * Attach loading info into product bo
	 *
	 * @param uwExtraLoadingVOs
	 * @param uwProduct
	 * @throws Exception
	 */
	@DataModeChgModifyPoint("change the param type from Array to List")
	protected void loadLoading(List<UwExtraLoadingVO> uwExtraLoadingVOs, UwProduct uwProduct) {
		logger.info("UwPolicyDSImpl.loadLoading. = " + uwExtraLoadingVOs);
		if (null != uwExtraLoadingVOs && 0 < uwExtraLoadingVOs.size()) {
			List<UwExtraLoading> bos = (List<UwExtraLoading>) BeanUtils.copyCollection(UwExtraLoading.class,
					uwExtraLoadingVOs);
			uwProduct.setUwExtraLoadings(bos);

		}
	}

	/**
	 * Attach lien info into product bo
	 *
	 * @param uwLienVOs
	 * @param uwProduct
	 * @throws Exception
	 */
	@DataModeChgModifyPoint("change the param type from Array to List")
	protected void loadLien(List<UwLienVO> uwLienVOs, UwProduct uwProduct) {
		if (null != uwLienVOs && 0 < uwLienVOs.size()) {
			List<UwLien> bos = (List<UwLien>) BeanUtils.copyCollection(UwLien.class, uwLienVOs);
			uwProduct.setUwLiens(bos);
		}
	}

	/**
	 * creates a uw policy record when a policy enter into UW procedure
	 *
	 * @param uwPolicyVO
	 *            UwPolicyVO
	 * @throws com.ebao.pub.framework.GenericException
	 */
	public Long createUwPolicy(UwPolicyVO uwPolicyVO) throws GenericException {
		/* copy uw Policy's value from value to vo */
		UwPolicy uwPolicyBo = new UwPolicy();
		BeanUtils.copyProperties(uwPolicyBo, uwPolicyVO, false);
		Long policyId = uwPolicyVO.getPolicyId();
		// get discount type from t_uw_policy
		String discountType = uwPolicyVO.getDiscountType();
		// get previouse underwriteId of the current policy
		Long latestUnderwriteId = getUwQueryDao().findLatestUnderwriteId(policyId);
		UwPolicyVO preVO = null;
		if (latestUnderwriteId != null) {
			preVO = findUwPolicy(latestUnderwriteId);
		}
		/* copy product info from uwProductValues to uwproductEntities of bo */
		List<UwProductVO> uwproductValues = uwPolicyVO.getUwProducts();
		if (uwproductValues != null && uwproductValues.size() != 0) {
			UwProduct[] uwproductEntities = new UwProduct[uwproductValues.size()];
			for (int i = 0; i < uwproductValues.size(); i++) {
				UwProduct uwproductEntity = new UwProduct();
				BeanConvertor.convertBean(uwproductEntity, uwproductValues.get(i));
				/* load lien info */
				loadLien(uwproductValues.get(i).getUwLiens(), uwproductEntity);
				/* load extra loadings info */
				loadLoading(uwproductValues.get(i).getUwExtraLoadings(), uwproductEntity);
				uwproductEntities[i] = uwproductEntity;
				/* set the benefit type of master product into policy */
				if (uwproductValues.get(i).getMasterId() == null) {
					Long productID = Long.valueOf(uwproductValues.get(i).getProductId().longValue());
					ProductVO basicInfo = this.getProductService().getProductByVersionId(productID,
							uwproductValues.get(i).getProductVersionId());
					String benefitType = basicInfo.getBenefitType();
					uwPolicyBo.setBenefitType(benefitType);
				}
				/*
				 * if the policy is not underwriting firstly,we must deive the standard life's
				 * default value from previous record
				 */
				if (preVO != null) {
					UwProductVO preProduct = findUwProduct(latestUnderwriteId, uwproductEntities[i].getItemId());
					if (preProduct != null) {
						uwproductEntities[i].setStandLife1(preProduct.getStandLife1());
						uwproductEntities[i].setStandLife2(preProduct.getStandLife2());
					}
				}
				uwproductEntities[i].setDiscountType(discountType);
			}
			uwPolicyBo.setUwProducts(Arrays.asList(uwproductEntities));
		}
		/*
		 * copy product info from uwconditionValues to uwconditionEntities of bo
		 */
		List<UwConditionVO> uwconditionValues = uwPolicyVO.getUwConditions();
		if (uwconditionValues != null && uwconditionValues.size() != 0) {
			List<UwCondition> uwconditionEntities = (List<UwCondition>) BeanUtils.copyCollection(UwCondition.class,
					uwconditionValues);
			uwPolicyBo.setUwConditions(uwconditionEntities);
		}
		/*
		 * copy product info from uwendorsementValues to uwendorsementEntities of bo
		 */
		List<UwEndorsementVO> uwendorsementValues = uwPolicyVO.getUwEndorsements();
		 //IR 363066 修正受理次標加費批註加上其他，進入核保後批註與照會單顯示問題
		if (!StringUtils.isEmpty(uwPolicyVO.getUwSourceType())&&CodeCst.UW_SOURCE_TYPE__CS.equals(uwPolicyVO.getUwSourceType())) {
			Long preUnderwriteId = uwPolicyDao.findPreUWIdByPolicyIdAndChangeId(policyId,uwPolicyVO.getChangeId());
			//IR 423495 新增附約+加費，變更補退費內容，有批註但可能無附加條款
			if(preUnderwriteId!=null){
				List<UwEndorsementVO> preuwendorsementValues=findUwEndorsementByUnderwriteId(preUnderwriteId);
				if(preuwendorsementValues!=null &&preuwendorsementValues.size()>0){
					uwendorsementValues=preuwendorsementValues;
				}
			}
		}
		if (uwendorsementValues != null && uwendorsementValues.size() != 0) {
			List<UwEndorsement> uwendorsementEntities = (List<UwEndorsement>) BeanUtils
					.copyCollection(UwEndorsement.class, uwendorsementValues);
			uwPolicyBo.setUwEndorsements(uwendorsementEntities);
		}
		/*
		 * copy product info from uwexclusionValues to uwexclusionEntities of bo
		 */
		List<UwExclusionVO> uwexclusionValues = uwPolicyVO.getUwExclusions(); // pos
		 //IR 363066 修正受理次標加費批註加上其他，進入核保後批註與照會單顯示問題
		if (!StringUtils.isEmpty(uwPolicyVO.getUwSourceType())&&CodeCst.UW_SOURCE_TYPE__CS.equals(uwPolicyVO.getUwSourceType())) {
			Long preUnderwriteId = uwPolicyDao.findPreUWIdByPolicyIdAndChangeId(policyId,uwPolicyVO.getChangeId());
			if(preUnderwriteId!=null){
				uwexclusionValues = (List<UwExclusionVO>) findUwExclusionEntitis(preUnderwriteId);
			}
		}

		// IR 428089 保單01027839441_undo後批註內容被清掉.不完整
		if (!StringUtils.isEmpty(uwPolicyVO.getUwSourceType())
				&& CodeCst.UW_SOURCE_TYPE__NB.equals(uwPolicyVO.getUwSourceType())
				&& null != preVO && CodeCst.UW_SOURCE_TYPE__NB.equals(preVO.getUwSourceType())
				) {
					uwexclusionValues = (List<UwExclusionVO>) findUwExclusionEntitis(preVO.getUnderwriteId());
		}

		// 保全批註僅顯示單一保項，不做group處理
//		if (CodeCst.UW_SOURCE_TYPE__CS.equals(uwPolicyVO.getUwSourceType()) && uwexclusionValues != null) {
//			for (int i = 0; i < uwexclusionValues.size(); i++) {
//				uwexclusionValues.get(i).setUwExclusionGroupId((long) i + 1); // group id
//			}
//		}

		if (uwexclusionValues != null && uwexclusionValues.size() != 0) {
			List<UwExclusion> uwexclusionEntities = (List<UwExclusion>) BeanUtils.copyCollection(UwExclusion.class,
					uwexclusionValues);
			uwPolicyBo.setUwExclusions(uwexclusionEntities);
		}
		/*
		 * copy product info from uwlifeinsuredValues to uwlifeinsuredEntities of bo
		 */
		List<UwLifeInsuredVO> uwlifeinsuredValues = uwPolicyVO.getUwLifeInsureds();
		if (uwlifeinsuredValues != null && uwlifeinsuredValues.size() != 0) {
			List<UwLifeInsured> uwlifeinsuredEntities = (List<UwLifeInsured>) BeanUtils
					.copyCollection(UwLifeInsured.class, uwlifeinsuredValues);
			uwPolicyBo.setUwLifeInsureds(uwlifeinsuredEntities);
		}
		Long underwriteId = this.saveUwPolicy(uwPolicyBo);
		return underwriteId;
	}

	/**
	 * save a new UwPolicy entity
	 *
	 * @return @throws GenericException
	 */
	@DataModeChgMovePoint
	private Long saveUwPolicy(UwPolicy uwPolicyBo) throws GenericException {
		Long underwriteid = uwPolicyDao.create(uwPolicyBo);
		List<UwProduct> uwproductValues = uwPolicyBo.getUwProducts();
		if (uwproductValues != null) {
			for (UwProduct uwproductValue : uwproductValues) {
				uwproductValue.setUnderwriteId(underwriteid);
				uwProductDao.create(uwproductValue);
				List<UwExtraLoading> uwextraloadingValues = uwproductValue.getUwExtraLoadings();
				if (uwextraloadingValues != null) {
					for (UwExtraLoading uwextraloadingValue : uwextraloadingValues) {
						UwExtraLoading loading = new UwExtraLoading();
						uwextraloadingValue.setUnderwriteId(underwriteid);
						BeanUtils.copyProperties(loading, uwextraloadingValue);
						uwExtraPremDao.create(loading);
					}
				}
				List<UwLien> uwlienValues = uwproductValue.getUwLiens();
				if (uwlienValues != null) {
					for (UwLien uwlienValue : uwlienValues) {
						UwLien uwlienEntity = new UwLien();
						uwlienValue.setUnderwriteId(underwriteid);
						BeanUtils.copyProperties(uwlienEntity, uwlienValue);
						uwReduceDao.create(uwlienEntity);
					}
				}
			}
		}
		List<UwCondition> uwconditionValues = uwPolicyBo.getUwConditions();
		if (uwconditionValues != null) {
			for (UwCondition uwconditionValue : uwconditionValues) {
				UwCondition uwconditionEntity = new UwCondition();
				uwconditionValue.setUnderwriteId(underwriteid);
				BeanUtils.copyProperties(uwconditionEntity, uwconditionValue);
				uwConditionDao.create(uwconditionEntity);
			}
		}
		List<UwEndorsement> uwendorsementValues = uwPolicyBo.getUwEndorsements();
		if (uwendorsementValues != null) {
			for (UwEndorsement uwendorsementValue : uwendorsementValues) {
				UwEndorsement uwendorsementEntity = new UwEndorsement();
				uwendorsementValue.setUnderwriteId(underwriteid);
				BeanUtils.copyProperties(uwendorsementEntity, uwendorsementValue);
				uwEndorsementDao.create(uwendorsementEntity);
			}
		}

		List<UwExclusion> uwexclusionValues = uwPolicyBo.getUwExclusions();
		if (uwexclusionValues != null) {
			for (UwExclusion uwexclusionValue : uwexclusionValues) {
				UwExclusion uwexclusionEntity = new UwExclusion();
				uwexclusionValue.setUnderwriteId(underwriteid);
				BeanUtils.copyProperties(uwexclusionEntity, uwexclusionValue);
				uwExclusionDao.create(uwexclusionEntity);
			}
		}
		List<UwLifeInsured> uwlifeinsuredValues = uwPolicyBo.getUwLifeInsureds();
		if (uwlifeinsuredValues != null) {
			for (UwLifeInsured uwlifeinsuredValue : uwlifeinsuredValues) {
				UwLifeInsured uwLifeInsured = new UwLifeInsured();
				uwlifeinsuredValue.setUnderwriteId(underwriteid);
				BeanUtils.copyProperties(uwLifeInsured, uwlifeinsuredValue);
				uwInsuredListDao.create(uwLifeInsured);
			}
		}
		return underwriteid;
	}

	/**
	 * updates the specified UwPolicy
	 *
	 * @param value
	 *            the UwPolicyVO
	 * @throws com.ebao.pub.framework.GenericException
	 */
	@DataModeChgInvokePoint
	public void updateUwPolicy(UwPolicyVO value, boolean toUpdateCommencementDate) throws GenericException {
		UwPolicy bo = uwPolicyDao.load(value.getUnderwriteId());
		BeanUtils.setSupportSqlDateType(true);
		BeanUtils.copyProperties(bo, value);
		uwPolicyDao.update(bo);
		if (toUpdateCommencementDate) {
			// recalc commence_date
			CompleteUWProcessSp.updateCommencedate(value.getUnderwriteId(), value.getValidateDate(),
					value.getActualValidate(), "N");
			bo = uwPolicyDao.load(value.getUnderwriteId());
			bo.copyToVO(value, true);
		}
		// 20130905, update comment
		Policy policy = policyDao.load(value.getPolicyId());
		policy.setComments(value.getUwNotes());
		// 20150709 ADD BY SUNNY
		policy.setImageIndi(value.getImageIndi());
		policy.setIlpRefundCustIndi(value.getIlpRefundCustIndi());

		policyDao.save(policy);
		// 20130905 end
	}

	/**
	 * updates the specified UwPolicySpecialCommIndi
	 *
	 * @param value
	 *            the UwPolicyVO
	 * @throws com.ebao.pub.framework.GenericException
	 */
	public void updateUwPolicySpecialCommIndi(UwPolicyVO value) throws GenericException {
		UwPolicy bo = uwPolicyDao.load(value.getUnderwriteId());
		BeanUtils.copyProperties(bo, value);
	}

	/**
	 * find uw product by underwrite id and item id
	 */
	public UwProductVO findUwProduct(Long underwriteId, Long itemId) throws GenericException {
		try {
			UwProduct bo;
			UwProductVO value = new UwProductVO();
			try {
				bo = uwProductDao.findByUnderwriteIdAndItemId(underwriteId, itemId);
				if (bo != null) {
					BeanUtils.copyProperties(value, bo);
				}
			} catch (ObjectNotFoundException oex) {
				value = null;
			}
			return value;
		} catch (Exception e) {
			throw ExceptionFactory.parse(e);
		}
	}

	/**
	 * @param underwriteId
	 * @throws com.ebao.pub.framework.GenericException
	 * @return
	 */
	@DataModeChgInvokePoint
	public Collection findUwProductEntitis(Long underwriteId) throws GenericException {
		try {
			List<UwProduct> c = uwProductDao.findByUnderwriteId(underwriteId);
			return BeanUtils.copyCollection(UwProductVO.class, c);
		} catch (Exception e) {
			throw ExceptionFactory.parse(e);
		}
	}

	/**
	 * @param underwriteId
	 * @param insuredId
	 * @return
	 * @throws GenericException
	 */
	public Collection findUwProductEntitisByUnderwriteIdAndInsuredId(Long underwriteId, Long insuredId1)
			throws GenericException {
		try {
			Collection c = uwProductDao.findByUnderwriteIdAndInsuredId(underwriteId, insuredId1);
			return BeanUtils.copyCollection(UwProductVO.class, c);
		} catch (Exception e) {
			throw ExceptionFactory.parse(e);
		}
	}

	/**
	 * returns a UwProductVO associated with the given Ids
	 *
	 * @return a collection of UwProductVO objects
	 * @throws com.ebao.pub.framework.GenericException
	 */
	public UwProductVO findUwProduct(Long uwListId) throws GenericException {
		try {
			UwProductVO value = new UwProductVO();
			BeanUtils.copyProperties(value, uwProductDao.load(uwListId));
			return value;
		} catch (Exception e) {
			throw ExceptionFactory.parse(e);
		}
	}

	/**
	 * updates the specified UwProduct
	 *
	 * @param value
	 *            the UwProductVO
	 * @throws com.ebao.pub.framework.GenericException
	 */
	@DataModeChgInvokePoint
	public void updateUwProduct(UwProductVO value) throws GenericException {
		updateUwProduct(value, false);
	}

	/**
	 * updates the specified UwProduct
	 *
	 * @param value
	 *            the UwProductVO
	 * @throws com.ebao.pub.framework.GenericException
	 */
	public void updateUwProduct(UwProductVO value, boolean updateCommenceDate) throws GenericException {
		// When Conditional Accept,index indicator can't be Y
		Integer decesionId = value.getDecisionId();
		String indxIndicator = QueryUwDataSp.getIndxIndicator(value.getItemId());
		if (decesionId != null && decesionId.equals(CodeCst.PRODUCT_DECISION__CONDITION)
				&& CodeCst.YES_NO__YES.equals(indxIndicator)) {
			throw new AppException(APP_WHEN_CA_INDX_INDI_CANT_Y);
		}
		UwProduct bo = uwProductDao.load(value.getUwListId());
		BeanUtils.copyProperties(bo, value);
		uwProductDao.update(bo);
		// recalc commence_date
		if (updateCommenceDate) {
			CompleteUWProcessSp.updateCommencedate(value.getUnderwriteId(), value.getValidateDate(),
					value.getActualValidate(), "N");
		}
	}

	/**
	 * @param underwriteId
	 * @param itemId
	 * @throws com.ebao.pub.framework.GenericException
	 * @return
	 */
	public Collection findUwLienEntitis(Long underwriteId, Long itemId) throws GenericException {
		try {
			Collection c = uwReduceDao.findByUnderwriteIdAndItemId(underwriteId, itemId);
			return BeanUtils.copyCollection(UwLienVO.class, c);
		} catch (Exception e) {
			throw ExceptionFactory.parse(e);
		}
	}

	/**
	 * returns a collection associated with the given Ids
	 *
	 * @return a collection of UwLienVO objects
	 * @throws com.ebao.pub.framework.GenericException
	 */
	public Collection findUwLienbyUwId(Long underwriteId) throws GenericException {
		Collection lienCollection = null;
		try {
			new UwLien();
			Collection boCollection = uwReduceDao.findByUnderwriteId(underwriteId);
			lienCollection = BeanUtils.copyCollection(UwLienVO.class, boCollection);
			return lienCollection;
		} catch (Exception e) {
			throw ExceptionFactory.parse(e);
		}
	}

	/**
	 * creates a specified UwLien
	 *
	 * @param value
	 *            the UwLienVO
	 * @throws com.ebao.pub.framework.GenericException
	 */
	public void createUwLien(UwLienVO value) throws GenericException {
		UwLien bo = new UwLien();
		BeanUtils.copyProperties(bo, value);
		uwReduceDao.create(bo);
	}

	/**
	 * deletes a specified UwLien
	 *
	 * @param value
	 *            the UwLienVO
	 * @throws com.ebao.pub.framework.GenericException
	 */
	public void removeUwLien(UwLienVO value) throws GenericException {
		try {
			uwReduceDao.remove(value.getUwListId());
		} catch (Exception e) {
			throw ExceptionFactory.parse(e);
		}
	}

	/**
	 * updates the specified UwLien
	 *
	 * @param value
	 *            the UwLienVO
	 * @throws com.ebao.pub.framework.GenericException
	 */
	public void updateUwLien(UwLienVO value) throws GenericException {
		UwLien bo = uwReduceDao.load(value.getUwListId());
		if (bo != null) {
			BeanUtils.copyProperties(bo, value);
			uwReduceDao.update(bo);
		}
	}

	/**
	 * returns a UwRestrictionVO associated with the given Ids
	 *
	 * @return a collection of UwRestrictionVO objects
	 * @throws com.ebao.pub.framework.GenericException
	 */
	public UwRestrictionVO findUwRestriction(Long uwListId) throws GenericException {
		try {
			UwRestriction bo = new UwRestriction();
			UwRestrictionVO value = new UwRestrictionVO();
			bo = bo.findUwRestriction(uwListId);
			BeanUtils.copyProperties(value, bo);
			return value;
		} catch (Exception e) {
			throw ExceptionFactory.parse(e);
		}
	}

	/**
	 * updates the specified UwRestriction
	 *
	 * @param value
	 *            the UwRestrictionVO
	 * @throws com.ebao.pub.framework.GenericException
	 */
	public void updateUwRestriction(UwRestrictionVO value) throws GenericException {
		try {
			UwRestriction bo = new UwRestriction();
			BeanUtils.copyProperties(bo, value);
			bo.update();
		} catch (Exception e) {
			throw ExceptionFactory.parse(e);
		}
	}

	// Begin Additional Business Methods
	/**
	 * Retrieve Underwriting history informations according to the proposalNO,
	 * upApplicationTypeCode and uwStatusOption
	 *
	 * @param proposalNO
	 *            String Proposal NO.
	 * @param uwAppTypeCode
	 *            String Underwriting application type code
	 * @param uwStatusOption
	 *            String Underwriting status option
	 * @return Collection Underwriting history informations
	 * @throws GenericException
	 *             Application Exception
	 * @author jason.luo
	 * @version 1.0
	 * @since 07.23.2004
	 */
	public Collection<UwPolicyVO> queryUwHistory(String proposalNO, String uwAppTypeCode, String uwStatusOption)
			throws GenericException {
		try {
			Criteria criteria = HibernateSession3.currentSession().createCriteria(UwPolicy.class);
			// proposalNO must be a validate value
			// revised to TUwPolicy.APPLYCODE by jason.luo at 09.15.2004
			criteria.add(Expression.eq("applyCode", proposalNO));
			if (null != uwAppTypeCode && !uwAppTypeCode.trim().equals("") && (-1 == uwAppTypeCode.indexOf("'"))) {
				// revised to TUwPolicy.UWSOURCETYPE by jason.luo at 09.15.2004
				criteria.add(Expression.eq("uwSourceType", uwAppTypeCode));
			}
			// added by yixing.lu at 08.30.2004
			if (UwStatusConstants.FINISHED.equals(uwStatusOption)) {
				// revised to TUwPolicy.UWSTATUS by jason.luo at 09.15.2004
				criteria.add(Expression.eq("uwStatus", uwStatusOption));
			}
			// end add by yixing.lu at 08.30.2004
			Collection<UwPolicy> queryResult = criteria.list();
			if (null == queryResult) {
				return new ArrayList<UwPolicyVO>();
			}
			// change TUwPolicy to UwPolicyVO
			Iterator iter = queryResult.iterator();
			Collection<UwPolicyVO> result = new ArrayList<UwPolicyVO>();
			while (iter.hasNext()) {
				UwPolicy data = (UwPolicy) iter.next();
				UwPolicyVO vo = new UwPolicyVO();
				BeanUtils.copyProperties(vo, data);
				result.add(vo);
			}
			return result;
		} catch (Exception e) {
			throw ExceptionFactory.parse(e);
		}
	}

	/**
	 * retrive converted uw history
	 *
	 * @param policyId
	 * @return
	 * @throws GenericException
	 */
	public Collection queryConvertedUwHistory(Long policyId) throws GenericException {
		try {
			new UwPolicy();
			return BeanUtils.copyCollection(UwPolicyVO.class, getUwQueryDao().findConvertedUwHistories(policyId));
		} catch (Exception e) {
			throw ExceptionFactory.parse(e);
		}
	}

	/**
	 * @param aQueryValue
	 * @return
	 * @throws GenericException
	 */
	@DataModeChgModifyPoint("change the param from UwPolicyVO to QueryHelperVO")
	public Collection processQuery(QueryHelperVO aQueryValue) throws GenericException {
		QueryHelper queryHelper = new QueryHelper();
		return queryHelper.processQuery(aQueryValue);
	}

	/**
	 * @param aQueryValue
	 * @return
	 * @throws GenericException
	 */
	public int getSearchTotalCount(UwPolicyVO aQueryValue) throws GenericException {
		QueryHelper pool = new QueryHelper();
		return pool.getSearchTotalCount(aQueryValue);
	}

	public void syncUwExtraPrem(String extraType, List<UwExtraLoadingVO> uwExtraPremList) {
		for (UwExtraLoadingVO uwExtraLoadingVO : uwExtraPremList) {

			// 設定extraPrem,extraPrem
			BigDecimal extraPara = uwExtraLoadingVO.getExtraPara();

			if (extraPara == null) {

				// for testing 先寫死 extraPara
				String gender = CodeCst.GENDER__MALE;

				// long itemId, BigDecimal emValue, Date startDate,
				// long insuredID, String extraArith

				extraPara = new BigDecimal(0.103000);
				/*
				 * extraPara = calculatorCI.getExtraRate( uwExtraLoadingVO.getItemId(), new
				 * BigDecimal(uwExtraLoadingVO.getEmValue()), uwExtraLoadingVO.getStartDate(),
				 * uwExtraLoadingVO.getInsuredId(), uwExtraLoadingVO.getExtraArith());
				 */
				// public List<BigDecimal> getTryExtraPremAfAn(long itemId,
				// BigDecimal amountValue, long insuredId, Date dueDate, String nextOrThis,
				// String extraType, String extraArith, BigDecimal extraPara,
				// BigDecimal emValue) throws GenericException;

				// uwExtraLoadingVO.getReducedAmount(),
				BigDecimal reducedAmount = new BigDecimal(300000.00);

				List<BigDecimal> calcResult = calculatorCI.getTryExtraPremAfAn(uwExtraLoadingVO.getItemId(),
						reducedAmount, uwExtraLoadingVO.getInsuredId(), uwExtraLoadingVO.getStartDate(), "N",
						uwExtraLoadingVO.getExtraType(), uwExtraLoadingVO.getExtraArith(), extraPara,
						new BigDecimal(uwExtraLoadingVO.getEmValue()));

				uwExtraLoadingVO.setExtraPara(extraPara);
				uwExtraLoadingVO.setExtraPrem((BigDecimal) calcResult.get(0));
				uwExtraLoadingVO.setExtraPremAn((BigDecimal) calcResult.get(1));
			}

			if (uwExtraLoadingVO.getUwListId() == null) {
				createUwExtraLoading(uwExtraLoadingVO);
			} else {
				updateUwExtraLoading(uwExtraLoadingVO);
			}
		}
	}

	/**
	 * @param extraPrem
	 *            UwExtraLoadingVO[]
	 * @param emVO
	 *            UwEmVO
	 * @throws GenericException
	 */
	public void saveExtraPrem(UwExtraLoadingVO[] extraPrem, UwEmVO emVO, Long insuredId) throws GenericException {
		try {
			UwProductVO productVO = findUwProduct(emVO.getUnderwriteId(), emVO.getItemId());
			if ("Y".equals(emVO.getFlag())) {
				if (emVO.getAdvantageIND() != null) {
					if (insuredId.longValue() == productVO.getInsured1().longValue()) {
						productVO.setAdvantageIndi1(emVO.getAdvantageIND());
					} else {
						productVO.setAdvantageIndi2(emVO.getAdvantageIND());
					}
				}
			}
			Collection extraPremList = findUwExtraLoadingEntitis(emVO.getUnderwriteId(), emVO.getItemId(), insuredId);
			if ((extraPremList != null) && (extraPremList.size() > 0)) {
				Iterator iter = extraPremList.iterator();
				while (iter.hasNext()) {
					UwExtraLoadingVO extraPremVO = (UwExtraLoadingVO) iter.next();
					removeUwExtraLoading(extraPremVO);
				}
			}
			productVO.setUwExtraLoadings(Arrays.asList(extraPrem));
			if ((extraPrem != null) && (extraPrem.length > 0)) {
				for (int i = 0; i < extraPrem.length; i++) {
					// modified by hanzhong.yan for cq:GEL00039090 2008/2/21
					if (extraPrem[i].getExtraPara() == null && !"5".equals(extraPrem[i].getExtraArith())
							&& !"4".equals(extraPrem[i].getExtraArith())) {
						String gender = productVO.getGender1();
						if (!insuredId.equals(productVO.getInsured1())) {
							gender = productVO.getGender2();
						}
						BigDecimal extraPara = doCalExRate(productVO, gender, i);
						// Check if the rate is Decimal when LoadingType =
						// Rating by Age
						if ("2".equals(extraPrem[i].getExtraArith())
								&& !(extraPara.stripTrailingZeros().scale() <= 0)) {
							// throw new AppException(20520120042L);
							Hashtable hashMax = new Hashtable();
							hashMax.put("emValue", extraPrem[i].getEmValue());
							// The rate calculated from (EM_VALUE={emValue} and Loading_Type = 2 ) can not
							// be Decimal,
							// please try other EM_VALUE or Loading_Type.
							AppException rateException = new AppException(20520120046L);
							rateException.addMessageParameter(hashMax);
							throw rateException;
						}
						extraPrem[i].setExtraPara(extraPara);
					}
					UwCalculation calculation = new UwCalculation();
					// try{
					List premList = calculation.calExtraPem(extraPrem[i], productVO);
					// }catch (Exception e) {
					// throw new AppException(20520120017L); //EM表找不到
					// }
					extraPrem[i].setExtraPrem((BigDecimal) premList.get(0));
					extraPrem[i].setExtraPremAn((BigDecimal) premList.get(1));

					String extaArith = extraPrem[i].getExtraArith();
					BigDecimal extraPremValue = extraPrem[i].getExtraPrem();
					if ((!CodeCst.ADD_ARITH__INSERANCE_CHANGE_TI.equals(extaArith)
							&& !CodeCst.ADD_ARITH__PER_THOUSAND_SUM_SU.equals(extaArith)
							&& !CodeCst.ADD_ARITH__EM_OR_PER_THOUSAND_SA.equals(extaArith)
							&& !CodeCst.ADD_ARITH__NO_INCREASE_PREMIUM.equals(extaArith))
							&& extraPremValue.floatValue() == 0) {
						throw new AppException(20210120019L); // 找不到保费率
					}
					createUwExtraLoading(extraPrem[i]);
				}
			}
			productVO.setDecisionId(emVO.getDecisionId());
			updateUwProduct(productVO);
		} catch (Exception ex) {
			throw ExceptionFactory.parse(ex);
		}
	}

	/**
	 * calculate then extrapremium
	 *
	 * @param productVO
	 * @param gender
	 * @param i
	 * @throws GenericException
	 */
	private BigDecimal doCalExRate(UwProductVO productVO, String gender, int i) throws GenericException {
		Date sysDate = AppContext.getCurrentUserLocalTime();
		String insuredStatus = productVO.getInsuredStatus();
		Long productId = Long.valueOf(productVO.getProductId().intValue());
		Integer premTerm = QueryUwDataSp.getPremTerm(productVO, policyService.getActiveVersionDate(productVO));
		Integer coverTerm = QueryUwDataSp.getCoverTerm(productVO, policyService.getActiveVersionDate(productVO));
		Integer age1 = productVO.getAge1();
		String smokingIndi = productVO.getSmoking();
		Integer pastYear = Integer.valueOf(DateUtils.getYear(sysDate) - DateUtils.getYear(productVO.getValidateDate()));
		BigDecimal emValue = new BigDecimal(productVO.getUwExtraLoadings().get(i).getEmValue().intValue());
		Date startDate = productVO.getUwExtraLoadings().get(i).getStartDate();
		// calculate the extra rate
		BigDecimal rate = calculatorCI.getExtraRate(productVO.getItemId(), emValue, startDate, productVO.getInsured1(),
				productVO.getUwExtraLoadings().get(i).getExtraArith());
		return rate;
	}

	/**
	 * Retrieve Underwriting Id according to the policyId used for NBU module
	 *
	 * @param policyId
	 *            Policy Id
	 * @param isUwFinished
	 *            Uw Status Mark(True - Finished Uw)
	 * @return UnderwriteId
	 * @throws GenericException
	 *             Application Exception
	 * @author jason.luo
	 * @version 1.0
	 * @since 11.1.2004
	 */
	public Long retrieveUwIdbyPolicyId(Long policyId, boolean isUwFinished) throws GenericException {
		List<UwPolicy> list = uwPolicyDao.findByPolicyId(policyId);
		Long underwriteId = null;
		for (UwPolicy data : list) {
			Long dataUnderwriteId = data.getUnderwriteId();
			if (null == underwriteId) {
				if (!isUwFinished) {
					if (!UwStatusConstants.FINISHED.equals(data.getUwStatus())) {
						underwriteId = dataUnderwriteId;
					}
				}
				continue;
			} else {
				if (isUwFinished) {
					if (!UwStatusConstants.FINISHED.equals(data.getUwStatus())) {
						continue;
					}
				} else {
					if (UwStatusConstants.FINISHED.equals(data.getUwStatus())) {
						continue;
					}
				}
				if (underwriteId.longValue() < dataUnderwriteId.longValue()) {
					underwriteId = dataUnderwriteId;
				}
			}
		}
		return underwriteId;
	}

	/**
	 * Retrieve Underwriting Id according to the policyCode used for NBU module
	 *
	 * @param policyCode
	 * @param isUwFinished
	 * @return
	 * @throws GenericException
	 */
	public Long retrieveUwIdbyPolicyCode(String policyCode, boolean isUwFinished) throws GenericException {
		try {
			Criteria criteria = HibernateSession3.currentSession().createCriteria(UwPolicy.class);
			criteria.add(Expression.eq("policyCode", policyCode));
			Collection<UwPolicy> queryResult = criteria.list();
			Iterator iter = queryResult.iterator();
			Long underwriteId = null;
			while (iter.hasNext()) {
				UwPolicy data = (UwPolicy) iter.next();
				if (isUwFinished) {
					if (UwStatusConstants.FINISHED.equals(data.getUwStatus())
							&& UwSourceTypeConstants.NEW_BIZ.equals(data.getUwSourceType())) {
						if (null == underwriteId) {
							underwriteId = data.getUnderwriteId();
							continue;
						} else {
							if (underwriteId.longValue() < data.getUnderwriteId().longValue()) {
								underwriteId = data.getUnderwriteId();
							}
						}
					}
				} else {
					if (null == underwriteId) {
						underwriteId = data.getUnderwriteId();
						continue;
					} else {
						if (underwriteId.longValue() < data.getUnderwriteId().longValue()) {
							underwriteId = data.getUnderwriteId();
						}
					}
				}
			}
			if (null != underwriteId) {
				return underwriteId;
			} else {
				// if (!isUwFinished) {
				// StringBuffer sbf = new StringBuffer();
				// sbf.append("Can't find underwriteid using policyCode - ");
				// sbf.append(policyCode);
				// throw ExceptionFactory.parse(new
				// IllegalStateException(sbf.toString()));
				// }
				/* when no underwriting info,just return null */
				return null;
			}
		} catch (Exception e) {
			throw ExceptionFactory.parse(e);
		}
	}

	/**
	 * get the latest underwrite record through policy id
	 *
	 * @param policyId
	 *            Policy Id
	 * @return UnderwriteId
	 * @throws GenericException
	 *             Application Exception
	 * @author mingchn.shi
	 */
	public Long findLastUnderwriteIdbyPolicyId(Long policyId) throws GenericException {
		return this.getUwQueryDao().findLatestUnderwriteId(policyId);
	}

	/**
	 * Retrieve Underwriting Id according to the policyId used for NBU module
	 *
	 * @param policyId
	 *            Policy Id
	 * @return UnderwriteId
	 * @throws GenericException
	 *             Application Exception
	 * @author jason.luo
	 * @version 1.0
	 * @since 10.14.2004
	 */
	public Long retrieveUwIdbyPolicyId(Long policyId) throws GenericException {
		// Modified by hendry.xu to fix the defect of GEL00019849
		// return retrieveUwIdbyPolicyId(policyId, false);
		List<UwPolicy> list = uwPolicyDao.findByPolicyId(policyId);
		Long underwriteId = null;
		Date insertTime = null;

		for (UwPolicy data : list) {
			Long dataUnderwriteId = data.getUnderwriteId();
			Date tmpInsertTime = data.getInsertTimestamp();

			if (null == underwriteId) {
				underwriteId = dataUnderwriteId;
				insertTime = tmpInsertTime;
			} else {
				if (insertTime.getTime() < tmpInsertTime.getTime()) {
					underwriteId = dataUnderwriteId;
					insertTime = tmpInsertTime;
				}
			}
		}
		return underwriteId;
	}

	/**
	 * Retrieve Underwriting Id according to the policycode used for NBU module
	 *
	 * @param policyCode
	 * @return
	 * @throws GenericException
	 */
	public Long retrieveUwIdbyPolicyCode(String policyCode) throws GenericException {
		return retrieveUwIdbyPolicyCode(policyCode, false);
	}

	/**
	 * Retrieve Underwriting Id according to the policyId, changeId and
	 * csTransaction
	 *
	 * @param policyId
	 *            Policy Id
	 * @param changeId
	 *            CS Change Id
	 * @return UnderwriteId
	 * @throws GenericException
	 *             Application Exception
	 * @author jason.luo
	 * @version 1.0
	 * @since 09.14.2004
	 */
	public Long retrieveUwIdbyChangeId(Long policyId, Long changeId) throws GenericException {
		try {
			Criteria criteria = HibernateSession3.currentSession().createCriteria(UwPolicy.class);
			criteria.add(Expression.eq("policyId", policyId));
			criteria.add(Expression.eq("changeId", changeId));
			// 255091 撈取順序從第一筆開始
			 //IR 363066 修正受理次標加費批註加上其他，進入核保後批註與照會單顯示問題
			criteria.addOrder(Order.desc("underwriteId"));
			Collection<UwPolicy> queryResult = criteria.list();
			Iterator iter = queryResult.iterator();
			while (iter.hasNext()) {
				UwPolicy data = (UwPolicy) iter.next();
				Criteria criteria2 = HibernateSession3.currentSession().createCriteria(UwProduct.class);
				criteria2.add(Expression.eq("underwriteId", data.getUnderwriteId()));
				Collection<UwProduct> queryResult2 = criteria2.list();
				if (!queryResult2.isEmpty()) {
					return data.getUnderwriteId();
				}
			}
			StringBuffer sbf = new StringBuffer();
			sbf.append("Can't find underwriteid using policyId - ");
			sbf.append(policyId).append(", changeId - ");
			sbf.append(changeId);
			throw ExceptionFactory.parse(new IllegalStateException(sbf.toString()));
		} catch (Exception e) {
			throw ExceptionFactory.parse(e);
		}
	}

	/**
	 * find the cs or claim uw policies by change id.
	 *
	 * @param changeId
	 *            the cs or claim change id
	 * @return List of UwPolicyVO
	 * @throws GenericException
	 */
	@DataModeChgInvokePoint
	public List findUwPoliciesByChangeId(Long changeId) throws GenericException {
		List<UwPolicyVO> uwPolicyVOList = new ArrayList<UwPolicyVO>();
		if (changeId != null) {
			try {
				// UwPolicy uwPolicy = new UwPolicy();
				/*
				 * Criteria criteria = uwPolicy.getCriteria(); criteria.add(UwPolicy.CHANGEID,
				 * changeId); Collection queryResult = criteria.list();
				 */
				List<UwPolicy> queryResult = uwPolicyDao.findByChangeId(changeId);
				Iterator<UwPolicy> iter = queryResult.iterator();
				while (iter.hasNext()) {
					UwPolicy data = iter.next();
					UwPolicyVO uwPolicyVO = (UwPolicyVO) BeanUtils.getBean(UwPolicyVO.class, data);
					uwPolicyVOList.add(uwPolicyVO);
				}
			} catch (Exception e) {
				throw ExceptionFactory.parse(e);
			}
		}
		return uwPolicyVOList;
	}

	/**
	 * @param underwriteId
	 * @param itemId
	 * @throws com.ebao.pub.framework.GenericException
	 * @return
	 */
	public Collection<CustomerVO> findUwLifeInsuredEntitis(Long underwriteId, Long itemId) throws GenericException {
		try {
			if (underwriteId == null || itemId == null) {
				throw ExceptionFactory.parse(new IllegalArgumentException("UnderwriteId or ItemId cannot be null"));
			}
			UwProduct uwProduct = uwProductDao.findByUnderwriteIdAndItemId(underwriteId, itemId);
			CustomerVO[] insureds = this.getInsureds(uwProduct);
			Collection<CustomerVO> c = new ArrayList<CustomerVO>();
			for (CustomerVO insured : insureds) {
				c.add(insured);
			}
			return c;
		} catch (Exception e) {
			throw ExceptionFactory.parse(e);
		}
	}

	@DataModeChgMovePoint
	private CustomerVO[] getInsureds(UwProduct uwProduct) throws com.ebao.pub.framework.GenericException {
		CustomerVO[] insureds = null;
		try {
			if (uwProduct.getInsured1() != null || uwProduct.getInsured2() != null) {
				if (uwProduct.getInsured1() != null && uwProduct.getInsured2() != null) {
					insureds = new CustomerVO[2];
					if (uwProduct.getInsured1() != null) {
						insureds[0] = getCustomerCI().getPerson(uwProduct.getInsured1());
						if (uwProduct.getInsuredStatus() != null) {
							insureds[0].setStatus(uwProduct.getInsuredStatus());
						}
						if (uwProduct.getSmoking() != null) {
							insureds[0].setSmoking(uwProduct.getSmoking());
						}
						if (uwProduct.getJobClass1() != null) {
							insureds[0].setJob1(String.valueOf(uwProduct.getJobClass1()));
						}
						if (uwProduct.getJobCate1() != null) {
							insureds[0].setJobCateId(Long.valueOf(uwProduct.getJobCate1().intValue()));
						}
					}
					if (uwProduct.getInsured2() != null) {
						insureds[1] = getCustomerCI().getPerson(uwProduct.getInsured2());
						if (uwProduct.getInsuredStatus2() != null) {
							insureds[1].setStatus(uwProduct.getInsuredStatus2());
						}
						if (uwProduct.getSmoking() != null) {
							insureds[1].setSmoking(uwProduct.getSmokingRelated());
						}
						if (uwProduct.getJobClass2() != null) {
							insureds[1].setJob1(String.valueOf(uwProduct.getJobClass2()));
						}
						if (uwProduct.getJobCate2() != null) {
							insureds[1].setJobCateId(Long.valueOf(uwProduct.getJobCate2().intValue()));
						}
					}
				} else {
					insureds = new CustomerVO[1];
					if (uwProduct.getInsured1() != null) {
						insureds[0] = getCustomerCI().getPerson(uwProduct.getInsured1());
						if (uwProduct.getInsuredStatus() != null) {
							insureds[0].setStatus(uwProduct.getInsuredStatus());
						}
						if (uwProduct.getSmoking() != null) {
							insureds[0].setSmoking(uwProduct.getSmoking());
						}
						if (uwProduct.getJobClass1() != null) {
							insureds[0].setJob1(String.valueOf(uwProduct.getJobClass1()));
						}
						if (uwProduct.getJobCate1() != null) {
							insureds[0].setJobCateId(Long.valueOf(uwProduct.getJobCate1().intValue()));
						}
					}
					if (uwProduct.getInsured2() != null) {
						insureds[0] = getCustomerCI().getPerson(uwProduct.getInsured2());
						if (uwProduct.getInsuredStatus2() != null) {
							insureds[0].setStatus(uwProduct.getInsuredStatus2());
						}
						if (uwProduct.getSmoking() != null) {
							insureds[0].setSmoking(uwProduct.getSmokingRelated());
						}
						if (uwProduct.getJobClass2() != null) {
							insureds[0].setJob1(String.valueOf(uwProduct.getJobClass2()));
						}
						if (uwProduct.getJobCate2() != null) {
							insureds[0].setJobCateId(Long.valueOf(uwProduct.getJobCate2().intValue()));
						}
					}
				}
			}
		} catch (Exception e) {
			throw ExceptionFactory.parse(e);
		}
		return insureds;
	}

	/**
	 * reads all LienEms associated given typeid and bsarate
	 *
	 * @return a collection of LienEm objects
	 * @throws com.ebao.pub.framework.GenericException
	 */
	public Collection findLienEmByTypeIdAndBsaRate(Integer typeId, java.math.BigDecimal bsaRate)
			throws GenericException {
		try {
			Collection c = lienEmDao.findByTypeIdAndBsaRate(typeId, bsaRate);
			c = com.ebao.pub.util.BeanUtils.copyCollection(LienEmVO.class, c);
			return c;
		} catch (Exception e) {
			throw ExceptionFactory.parse(e);
		}
	}

	/**
	 * reads all LienEms associated given typeid and bsarate
	 *
	 * @return a collection of LienEm objects
	 * @throws com.ebao.pub.framework.GenericException
	 */
	public Collection findLienEmByTypeId(Integer typeId) throws GenericException {
		try {
			Collection c = lienEmDao.findByTypeId(typeId);
			c = com.ebao.pub.util.BeanUtils.copyCollection(LienEmVO.class, c);
			return c;
		} catch (Exception e) {
			throw ExceptionFactory.parse(e);
		}
	}

	/**
	 * reads all uw policy by policy Id
	 *
	 * @param policyId
	 * @return
	 * @throws GenericException
	 */
	@DataModeChgInvokePoint
	public Collection findUwPolicyByPolicyId(Long policyId) throws GenericException {
		Collection c = uwPolicyDao.findByPolicyId(policyId);
		return BeanUtils.copyCollection(UwPolicyVO.class, c);
	}

	/**
	 * store applcy fac request
	 *
	 * @param applyFacRequestVO
	 * @throws GenericException
	 */
	// public void storeApplyRequestFac(ApplyFacRequestVO applyFacRequestVO)
	// throws GenericException {
	// Long itemId = applyFacRequestVO.getItemId();
	// CoverageVO productVO = coverageCI.retrieveByItemId(itemId);
	// Long policyId = productVO.getPolicyId();
	// // invoke the RI interface
	// RiFacRequestInfo facRequestInput = new RiFacRequestInfo();
	// facRequestInput.setApplyerId(applyFacRequestVO.getApperId());
	// facRequestInput.setCedCoverageId(applyFacRequestVO.getItemId());
	// facRequestInput.setPolicyId(policyId);
	// facRequestInput.setRequestDate(applyFacRequestVO.getRequestDate());
	// facRequestInput.setRequestId(applyFacRequestVO.getRequestId());
	// facRequestInput.setRequestMemo(applyFacRequestVO.getRequestMemo());
	// facRequestInput.setStatus(applyFacRequestVO.getStatus());
	// // uwPolicyService.createFacRequest(facRequestInput);
	// facRequestService.createFacRequest(facRequestInput);
	// }
	/**
	 * store applcy fac request list
	 *
	 * @param applyFacRequestVO
	 * @throws GenericException
	 */
	// public void storeApplyRequestFac(List applyFacRequestVOList)
	// throws GenericException {
	// Iterator iterator = applyFacRequestVOList.iterator();
	// while (iterator.hasNext()) {
	// ApplyFacRequestVO element = (ApplyFacRequestVO) iterator.next();
	// storeApplyRequestFac(element);
	// }
	// }
	/**
	 * update the vo
	 *
	 * @param applyFacRequestVO
	 * @throws GenericException
	 */

	// public void updateApplyRequestFac(ApplyFacRequestVO applyFacRequestVO)
	// throws GenericException {
	// Long itemId = applyFacRequestVO.getItemId();
	// CoverageVO productVO = coverageCI.retrieveByItemId(itemId);
	// Long policyId = productVO.getPolicyId();
	// // invoke the RI interface
	// RiFacRequestInfo facRequestInput = new RiFacRequestInfo();
	// facRequestInput.setApplyerId(applyFacRequestVO.getApperId());
	// facRequestInput.setCedCoverageId(applyFacRequestVO.getItemId());
	// facRequestInput.setPolicyId(policyId);
	// facRequestInput.setRequestDate(applyFacRequestVO.getRequestDate());
	// facRequestInput.setRequestId(applyFacRequestVO.getRequestId());
	// facRequestInput.setRequestMemo(applyFacRequestVO.getRequestMemo());
	// facRequestInput.setStatus(applyFacRequestVO.getStatus());
	// facRequestService.updateFacRequest(facRequestInput);
	// }
	/**
	 * get all the apply fac request by item Id
	 *
	 * @param itemId
	 * @return
	 * @throws GenericException
	 */
	public List<UwRiFacVO> getApplyFacRequestList(Long itemId) throws GenericException {
		// List<ApplyFacRequestVO> requestlist = new
		// ArrayList<ApplyFacRequestVO>();
		// List<RiFacRequestInfo> facRequestInformation = facRequestService
		// .queryFacRequestsByCoverageId(itemId);itemId
		List<UwRiFacVO> requestList = findByItemId(itemId);
		// ApplyFacRequestVO requestVO = null;
		if ((requestList != null) && (requestList.size() > 0)) {
			// to get product Id
			// UwProduct UwProductBO = new UwProduct();
			// List tempList = UwProductBO.findByItemId(itemId);
			// UwProductBO = (UwProduct) tempList.get(0);
			// to get product Id from t_contract_product
			CoverageVO productVO = coverageCI.retrieveByItemId(itemId);
			// to get benefitType
			ProductVO basicInfoVO = this.getProductService().getProductByVersionId(
					Long.valueOf(productVO.getProductId().longValue()), productVO.getProductVersionId());
			for (UwRiFacVO uwRiFacVO : requestList) {
				// @todo
				// if (facVOs[i].getStatus().intValue() == 1) {
				// requestVO = new ApplyFacRequestVO();
				uwRiFacVO.setApperId(AppContext.getCurrentUser().getUserId());
				uwRiFacVO.setBenefitType(basicInfoVO.getBenefitType());
				// requestVO.setItemId(facRequestInfo.getCedCoverageId());
				// requestVO.setRequestDate(facRequestInfo.getRequestDate());
				// requestVO.setStatus(facRequestInfo.getStatus());
				// uwRiFacVO.setRequestId(uwRiFacVO.getUwListId());
				// requestVO.setRequestMemo(facRequestInfo.getRequestMemo());
				uwRiFacVO.setProductId(productVO.getProductId());
				// requestlist.add(requestVO);
				// }
			}
		}
		return requestList;
	}

	/**
	 * check whether these is any fac request not confirmed
	 *
	 * @param itemId
	 * @return
	 * @throws GenericException
	 */
	public boolean hasUncompleteApplyFacRequest(Long itemId) throws GenericException {
		boolean unCompleteFlag = false;
		List<UwRiFacVO> uwRiFacVOList = this.findByItemId(itemId);
		if (uwRiFacVOList != null && uwRiFacVOList.size() > 0) {
			for (UwRiFacVO uwRiFacVO : uwRiFacVOList) {
				Long status = uwRiFacVO.getStatus();
				if (status != null && status.longValue() == 0) {
					unCompleteFlag = true;
					break;
				}
			}
		}
		return unCompleteFlag;
	}

	/**
	 * remove apply fac request by listId
	 *
	 * @param listId
	 * @throws GenericException
	 */
	// public void removeApplyFacRequest(Long requestId) throws GenericException {
	// facRequestService.cancelFacRequestBeforeArrange(requestId);
	// }
	/**
	 * Retrieve Underwriting informations according to the uwSourceType, changId
	 *
	 * @author yixing.lu
	 * @version 1.0
	 *          <p>
	 *          Create Time: 2004/08/24
	 *          </p>
	 * @param uwSourceType
	 *            String
	 * @param changId
	 *            String
	 * @throws Exception
	 * @return Collection
	 */
	public Collection<UwPolicyVO> queryUwDetail(String changId, String uwSourceType) throws GenericException {
		try {
			if (Log.isDebugEnabled(this.getClass())) {
			}
			Criteria criteria = HibernateSession3.currentSession().createCriteria(UwPolicy.class);
			// proposalNO must be a validate value
			criteria.add(Expression.eq("changeId", Long.valueOf(changId.trim())));
			if (uwSourceType != null && !"".equals(uwSourceType)) {
				criteria.add(Expression.eq("uwSourceType", uwSourceType));
			}
			Collection<UwPolicy> queryResult = criteria.list();
			if (null == queryResult) {
				return new ArrayList<UwPolicyVO>();
			}
			// change UwPolicy to UwPolicyVO
			Iterator iter = queryResult.iterator();
			Collection<UwPolicyVO> result = new ArrayList<UwPolicyVO>();
			UwPolicyVO vo = null;
			while (iter.hasNext()) {
				UwPolicy data = (UwPolicy) iter.next();
				vo = new UwPolicyVO();
				BeanUtils.copyProperties(vo, data);
				result.add(vo);
			}
			return result;
		} catch (Exception e) {
			throw ExceptionFactory.parse(e);
		}
	}

	/**
	 * find privouse uw policy
	 *
	 * @param underwriteId
	 * @return
	 * @throws GenericException
	 */
	public UwPolicyVO findPreUwPolicy(Long underwriteId) throws GenericException {
		UwPolicyVO policyVO = findUwPolicy(underwriteId);
		Long policyId = policyVO.getPolicyId();
		Collection allUwPolicies = findUwPolicyByPolicyId(policyId);
		Iterator iteratorPolicy = allUwPolicies.iterator();
		// set Previous Underwriter
		long tempUnderwriteId = 0L;
		UwPolicyVO returnedUwPolicyVO = null;
		while (iteratorPolicy.hasNext()) {
			UwPolicyVO uwPolicyVO = (UwPolicyVO) iteratorPolicy.next();
			Long tempId = uwPolicyVO.getUnderwriteId();
			if (tempId.longValue() < underwriteId.longValue()) {
				if (tempId.longValue() > tempUnderwriteId) {
					tempUnderwriteId = tempId.longValue();
					returnedUwPolicyVO = uwPolicyVO;
				}
			}
		}
		return returnedUwPolicyVO;
	}

	/**
	 * get the previouse conveted policis uw history
	 *
	 * @param policyId
	 * @return
	 * @throws GenericException
	 */
	public Collection findPostponeAndDeclineReason() throws GenericException {
		UwProductDecision uwPolicy = new UwProductDecision();
		return uwPolicy.findPostponeAndDeclineReason();
	}

	public Collection findByPolicyIdAndUwStatusAndChangeId(java.lang.Long policyId, String uwStatus, Long changeId)
			throws GenericException {
		try {
			new UwPolicy();
			return BeanUtils.copyCollection(UwPolicyVO.class,
					uwPolicyDao.findByPolicyIdAndUwStatusAndChangeId(policyId, uwStatus, changeId));
		} catch (ObjectNotFoundException ex) {
			return null;
		} catch (Exception ex) {
			throw ExceptionFactory.parse(ex);
		}
	}

	public List<UwRiFacVO> findByItemId(Long itemId) throws GenericException {
		try {
			if (itemId == null) {
				throw ExceptionFactory.parse(new IllegalArgumentException("ItemId cannot be null"));
			}
			List<UwRiFacVO> uwRiFacVOList = new ArrayList<UwRiFacVO>();
			List<UwRiFac> requestlist = uwRiFacDao.findByItemId(itemId);
			for (UwRiFac UwRiFac : requestlist) {
				UwRiFacVO uwRiFacVO = new UwRiFacVO();
				BeanConvertor.convertBean(uwRiFacVO, UwRiFac);
				uwRiFacVOList.add(uwRiFacVO);
			}
			return uwRiFacVOList;
		} catch (Exception e) {
			throw ExceptionFactory.parse(e);
		}
	}

	public List<UwRiFacVO> findByListId(Long listId) throws GenericException {
		try {
			if (listId == null) {
				throw ExceptionFactory.parse(new IllegalArgumentException("listId cannot be null"));
			}
			List<UwRiFacVO> uwRiFacVOList = new ArrayList<UwRiFacVO>();
			List<UwRiFac> requestlist = uwRiFacDao.findByListId(listId);
			for (UwRiFac UwRiFac : requestlist) {
				UwRiFacVO uwRiFacVO = new UwRiFacVO();
				BeanConvertor.convertBean(uwRiFacVO, UwRiFac);
				uwRiFacVOList.add(uwRiFacVO);
			}
			return uwRiFacVOList;
		} catch (Exception e) {
			throw ExceptionFactory.parse(e);
		}
	}

	@Override
	public void addFacRequest(UwRiFacVO uwRiFacVO) throws GenericException {
		UwRiFac uwRiFac = new UwRiFac();
		BeanUtils.copyProperties(uwRiFac, uwRiFacVO);
		uwRiFacDao.addFacRequest(uwRiFac);
	}

	@Override
	public void changeFacRequest(UwRiFacVO uwRiFacVO) throws GenericException {
		UwRiFac uwRiFac = new UwRiFac();
		BeanUtils.copyProperties(uwRiFac, uwRiFacVO);
		uwRiFacDao.changeRequest(uwRiFac);
	}

	@Override
	public void cancleFacRequest(Long uwListId) throws GenericException {
		uwRiFacDao.cancleRequest(uwListId);
	}

	@Override
	public void confirmFacRequest(Long uwListId) throws GenericException {
		uwRiFacDao.confimRequest(uwListId);
	}

	@DataModeChgMovePoint
	public BigDecimal getTotalAmountOfInsured(Long insuredId) throws GenericException {
		return getUwQueryDao().getTotalAmountOfInsured(insuredId);
	}

	@DataModeChgMovePoint
	public boolean getDeclarationExitIndi(Long policyId, Long insuredId) throws GenericException {
		return getUwQueryDao().getDeclarationExistIndi(policyId, insuredId);
	}

	public Date getPolicyNextDueDate(long policyId) throws GenericException {
		return QueryUwDataSp.getPolicyNextDueDate(policyId);
	}

	/**
	 * <p>
	 * Description : 依批註管理勾選版本,同步資料庫批註資料(新/刪/修)
	 * </p>
	 * <p>
	 * Created By : simon.huang
	 * </p>
	 * <p>
	 * Create Time : Jul 9, 2015
	 * </p>
	 *
	 * @param underwriteId
	 * @param policyId
	 * @param itemIdList
	 * @param insuredPartyId
	 * @param exclusionCodeList
	 * @param exclusionDescMap
	 * @param disabilityIndiList
	 * @throws GenericException
	 */
	@Override
	public List<UwExclusionVO> syncUwExclusions(Long underwriteId, Long policyId, Long insuredPartyId, Long gorupId,
			List<String> itemIdList, List<Long> productIdList, String exclusionCode,
			Map<String, Object> exclusionDescMap, Map<String, String> disabilityIndiMap) throws GenericException {

		List<UwExclusionVO> result = new ArrayList<UwExclusionVO>();
		// 已存在批註
		Collection<UwExclusionVO> exclusionVOList = this.findUwExclusionGroupEntitis(underwriteId, gorupId,
				insuredPartyId);
		// 已存在tcp.itemId mapping表

		Map<String, UwExclusionVO> exculsionCodeMap = NBUtils.toMap(exclusionVOList, "itemId");

		// 有勾選批註，create or update
		for (int index = 0; index < itemIdList.size(); index++) {
			String itemId = itemIdList.get(index);

			UwExclusionVO uwExclusionVO = exculsionCodeMap.remove(itemId);
			if (uwExclusionVO == null) { // 批註原先不存在
				uwExclusionVO = new UwExclusionVO();
			}
			uwExclusionVO.setPolicyId(policyId);
			uwExclusionVO.setInsuredId(insuredPartyId);
			uwExclusionVO.setUnderwriteId(underwriteId);
			uwExclusionVO.setUwExclusionGroupId(gorupId);
			if (!StringUtils.isNullOrEmpty(itemId)) {
				Long productId = productIdList.get(index);

				uwExclusionVO.setItemId(new Long(itemId));
				uwExclusionVO.setProductId(productId);
			}

			uwExclusionVO.setExclusionCode(exclusionCode);
			uwExclusionVO.setDescLang1((String) exclusionDescMap.get(exclusionCode));

			String disabilityIndi = disabilityIndiMap.get(exclusionCode);
			if (StringUtils.isNullOrEmpty(disabilityIndi)) {
				disabilityIndi = CodeCst.YES_NO__NO;
			}
			uwExclusionVO.setDisabilityIndi(disabilityIndi);

			if (uwExclusionVO.getUwListId() == null) {
				createUwExclusion(uwExclusionVO);
			} else {
				UwExclusion uwExclusion = uwExclusionDao.load(uwExclusionVO.getUwListId());
				uwExclusion.copyFromVO(uwExclusionVO, false, false);
				uwExclusionDao.update(uwExclusion);
			}

			result.add(uwExclusionVO);
		}

		// 未被勾選,刪除批註
		for (UwExclusionVO uwExclusionVO : exculsionCodeMap.values()) {
			uwExclusionDao.remove(uwExclusionVO.getUwListId());
		}

		return result;
	}

	/**
	 * <p>
	 * Description : 依批註管理勾選保單附加條款,同步資料庫資料(新/刪/修)
	 * </p>
	 * <p>
	 * Created By : simon.huang
	 * </p>
	 * <p>
	 * Create Time : Jun 10, 2016
	 * </p>
	 *
	 * @param underwriteId
	 * @param policyId
	 * @param groupId
	 *            t_uw_exclusion.uw_exclusion_group_id
	 * @param version
	 *            (NB or CS)
	 * @param prodVerEndorsementList
	 */
	@Override
	public List<UwEndorsementVO> syncUwEndorsement(Long underwriteId, Long policyId, Long groupId, Long version,
			List<Map<String, String>> itemIdEndorIdList, Map<String, ProdVerEndorsementVO> endorementVOMap) {

		List<UwEndorsementVO> result = new ArrayList<UwEndorsementVO>();

		// 已存在條款
		List<UwEndorsementVO> endorsementVOList = this.findUwEndorsementGroupEntitis(underwriteId, groupId);

		// 有勾選條款，create or update
		for (Map<String, String> itemIdEndorIdMap : itemIdEndorIdList) {

			Long itemId = NumericUtils.parseLong(itemIdEndorIdMap.get("itemId"));
			Long endorId = NumericUtils.parseLong(itemIdEndorIdMap.get("endorId"));

			EndorsementCode endorsementCode = endorsementCodeDao.load(endorId);
			String endoCode = endorsementCode.getEndoCode();

			UwEndorsementVO uwEndorsementVO = filterExistsEndorsementVO(endorsementVOList, itemId, endoCode, version);

			if (uwEndorsementVO == null) { // 原先不存在
				uwEndorsementVO = new UwEndorsementVO();

				uwEndorsementVO.setPolicyId(policyId);
				uwEndorsementVO.setUnderwriteId(underwriteId);
				uwEndorsementVO.setUwExclusionGroupId(groupId);

				uwEndorsementVO.setEndoCode(endoCode);
				uwEndorsementVO.setItemId(itemId);
				uwEndorsementVO.setDescLang1(endorsementCode.getEndoName());
				uwEndorsementVO.setEndorsementVersion(version);
				createUwEndorsement(uwEndorsementVO);
			}

			result.add(uwEndorsementVO);

		}

		// 未被勾選,刪除條款
		for (UwEndorsementVO uwEndorsementVO : endorsementVOList) {
			uwEndorsementDao.remove(uwEndorsementVO.getUwListId());
		}

		return result;
	}

	/**
	 * <p>
	 * Description : 依itemId,endoCode,version過濾已存在UwEndorsementVO
	 * </p>
	 * <p>
	 * Created By : simon.huang
	 * </p>
	 * <p>
	 * Create Time : Aug 21, 2016
	 * </p>
	 *
	 * @param endorsementVOList
	 * @param
	 * @param endoCode
	 * @param version
	 * @return
	 */
	private UwEndorsementVO filterExistsEndorsementVO(List<UwEndorsementVO> endorsementVOList, Long itemId,
			String endoCode, Long version) {

		UwEndorsementVO findObj = null;
		for (UwEndorsementVO uwEndorsementVO : endorsementVOList) {
			boolean sameItemId = false;
			boolean sameEndoCode = false;
			boolean sameVersion = false;
			if (uwEndorsementVO.getItemId() != null && itemId != null) {
				if (uwEndorsementVO.getItemId().longValue() == itemId) {
					sameItemId = true;
				}
			}
			if (NBUtils.in(uwEndorsementVO.getEndoCode(), endoCode)) {
				sameEndoCode = true;
			}
			if (version.longValue() == uwEndorsementVO.getEndorsementVersion()) {
				sameVersion = true;
			}

			if (sameItemId && sameEndoCode && sameVersion) {
				findObj = uwEndorsementVO;
				break;
			}
		}

		if (findObj != null) {
			endorsementVOList.remove(findObj);
		}
		return findObj;
	}

	@Override
	public UwLifeInsuredVO findUwLifeInsuredByListId(Long underwriteId, Long listId) throws GenericException {

		try {
			UwLifeInsured uwLifeInsured = uwInsuredListDao.findByUnderwriteIdAndListId(underwriteId, listId);

			return (UwLifeInsuredVO) BeanUtils.getBean(UwLifeInsuredVO.class, uwLifeInsured);

		} catch (Exception e) {
			throw ExceptionFactory.parse(e);
		}
	}

	/**
	 * <p>
	 * Description : 取得新契約保單核保員ID(含在途件邏輯)
	 * </p>
	 * <p>
	 * Created By : simon.huang
	 * </p>
	 * <p>
	 * Create Time : Aug 4, 2017
	 * </p>
	 *
	 * @param policyId
	 * @return
	 * @throws GenericException
	 */
	@Override
	public Long getUNBUnderwriterId(Long policyId) throws GenericException {

		Long underwriterId = uwPolicyCI.getUNBUnderwriterId(policyId);
		/*
		 * DBean db = null; PreparedStatement ps = null; ResultSet rs = null; Long
		 * underwriterid = null; try { db = new DBean(false); db.connect();
		 *
		 * StringBuffer sql = new StringBuffer();
		 * sql.append("select underwriter_id from t_uw_policy a where policy_id = ? ");
		 * sql.append("and underwriter_id is not null ");
		 * sql.append("and uw_source_type = ? ");
		 * //原使用underwrite_time排序但因測試階段日期常被異動,改以underwrite_id
		 * sql.append("order by underwrite_id desc"); ps =
		 * db.getConnection().prepareStatement(sql.toString());
		 *
		 * ps.setLong(1, policyId); ps.setString(2, CodeCst.UW_SOURCE_TYPE__NB); rs =
		 * ps.executeQuery(); while (rs.next()) { underwriterid =
		 * rs.getLong("UNDERWRITER_ID"); break; } } catch (Exception e) { throw
		 * ExceptionFactory.parse(e); } finally { DBean.closeAll(rs, ps, db); }
		 */
		return underwriterId;
	}

	@Override
	public int getUwPolicyCount(Long policyId) throws GenericException {
		DBean db = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		int count = 0;
		try {
			db = new DBean(false);
			db.connect();

			StringBuffer sql = new StringBuffer();
			sql.append("select nvl(count(policy_id),0 ) count from t_uw_policy a where policy_id = ? ");
			ps = db.getConnection().prepareStatement(sql.toString());

			ps.setLong(1, policyId);
			rs = ps.executeQuery();
			while (rs.next()) {
				count = rs.getInt("count");
				break;
			}
		} catch (Exception e) {
			throw ExceptionFactory.parse(e);
		} finally {
			DBean.closeAll(rs, ps, db);
		}
		return count;
	}

	@Override
	public String getChannelOrgName(Long channelOrgId) throws GenericException {
		DBean db = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String orgName = null;
		try {
			db = new DBean(false);
			db.connect();
			ps = db.getConnection().prepareStatement("select channel_name from t_channel_org where channel_id = ?");

			ps.setLong(1, channelOrgId);
			rs = ps.executeQuery();
			while (rs.next()) {
				orgName = rs.getString("CHANNEL_NAME");
				break;
			}
		} catch (Exception e) {
			throw ExceptionFactory.parse(e);
		} finally {
			DBean.closeAll(rs, ps, db);
		}
		return orgName;
	}

	/**
	 * <p>
	 * Description : 移除商品批註
	 * </p>
	 * <p>
	 * Created By : simon.huang
	 * </p>
	 * <p>
	 * Create Time : Jun 10, 2016
	 * </p>
	 *
	 * @param underwriteId
	 * @param underwriteId
	 */
	@Override
	public void removeUwExclusionGroup(Long underwriteId, Long groupId) {

		Collection<UwExclusionVO> delUwExclusionList = findUwExclusionGroupEntitis(underwriteId, groupId);
		for (UwExclusionVO uwExclusionVO : delUwExclusionList) {
			uwExclusionDao.remove(uwExclusionVO.getUwListId());
		}
	}

	/**
	 * <p>
	 * Description : 移除商品批註條款
	 * </p>
	 * <p>
	 * Created By : simon.huang
	 * </p>
	 * <p>
	 * Create Time : Jun 10, 2016
	 * </p>
	 *
	 * @param groupId
	 * @param underwriteId
	 */
	@Override
	public void removeUwEndorsementGroup(Long underwriteId, Long groupId) {

		Collection<UwEndorsementVO> delUwEndorsementList = findUwEndorsementGroupEntitis(underwriteId, groupId);
		for (UwEndorsementVO uwEndorsementVO : delUwEndorsementList) {
			uwEndorsementDao.remove(uwEndorsementVO.getUwListId());
		}
	}

	@Override
	public void removeUwTransferByUnderwriterId(Long underwriteId) {
		Map<String, Object> queryPara = new HashMap<String, Object>();
		queryPara.put("underwriteId", underwriteId);
		List<UwTransferVO> uwTransferVOs = uwTransferService.find(queryPara);
		if (uwTransferVOs != null) {
			for (UwTransferVO uwTransferVO : uwTransferVOs) {
				uwTransferService.remove(uwTransferVO.getListId());
			}
		}
	}

	@Override
	public void removeUwPolicyByUnderwriterId(Long underwriteId) {
		UwPolicy bo = uwPolicyDao.load(underwriteId);
		if (bo != null) {
			uwPolicyDao.remove(underwriteId);
		}
	}

	@Override
	public void removeUwProductByUnderwriterId(Long underwriteId) {
		List<UwProduct> uwProducts = uwProductDao.findByUnderwriteId(underwriteId);
		if (uwProducts != null && uwProducts.size() > 0) {
			for (UwProduct uwProduct : uwProducts) {
				uwProductDao.remove(uwProduct.getUwListId());
			}
		}
	}

	@Override
	public void removeUwProductByUwListId(Long uwListId) {
		uwProductDao.remove(uwListId);
	}

	@Override
	public void removeUwInsuredByUnderwriterId(Long underwriteId) {
		List<UwLifeInsured> uwLifeInsureds = uwInsuredListDao.findByUnderwriteId(underwriteId);
		if (uwLifeInsureds != null && uwLifeInsureds.size() > 0) {
			for (UwLifeInsured bo : uwLifeInsureds) {
				uwInsuredDisabilityService.removeByUwInsuredId(bo.getUwListId());
				uwInsuredListDao.remove(bo.getUwListId());
			}
		}
	}

	@Override
	public Boolean checkIfSuspendExist(Long policyId) {

		DBean db = null;
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Long result = 0L;

		try {

			StringBuffer sql = new StringBuffer();

			sql.append("select NVL(sum(t.pay_balance), 0) as amount");
			sql.append("  from t_cash t");
			sql.append(" where t.policy_id = ? ");
			sql.append(" and prem_purpose = 1 and t.offset_status = 0");

			db = new DBean();
			db.connect();
			conn = db.getConnection();
			pstmt = conn.prepareStatement(sql.toString());

			pstmt.setLong(1, policyId);

			rs = pstmt.executeQuery();

			if (rs.next()) {
				result = rs.getLong(1);
				if (rs.wasNull()) {
					result = null;
				}
			}

		} catch (Exception e) {
			throw ExceptionFactory.parse(e);
		} finally {
			DBean.closeAll(rs, pstmt, db);
		}

		if (result > 0) {
			return true;
		} else {
			return false;
		}

	}

	@Override
	public String getImageRemakeByPolicyId(Long policyId) {
		String remake = "";
		DBean db = new DBean(true);
		java.sql.PreparedStatement stmt = null;
		Connection conn = null;
		java.sql.ResultSet rows = null;
		try {
			db.connect();
			conn = db.getConnection();

			stmt = conn.prepareStatement("SELECT PKG_LS_PM_NB_CI.f_get_image_remark(?) AS VAL FROM DUAL");
			stmt.setLong(1, policyId);
			stmt.execute();
			rows = stmt.executeQuery();
			while (rows.next()) {
				remake = rows.getString("VAL");
			}
		} catch (Exception ex) {
			// throw ExceptionUtil.parse(ex);
		} finally {
			DBean.closeAll(null, stmt, db);
		}
		return remake;
	}

	@Override
	public String getCalloutMemoByPolicyId(Long policyId) {
		String remake = "";
		DBean db = new DBean(true);
		java.sql.PreparedStatement stmt = null;
		Connection conn = null;
		java.sql.ResultSet rows = null;
		try {
			db.connect();
			conn = db.getConnection();

			stmt = conn.prepareStatement("SELECT PKG_LS_PM_NB_CI.f_get_callout_memo(?) AS VAL FROM DUAL");
			stmt.setLong(1, policyId);
			stmt.execute();
			rows = stmt.executeQuery();
			while (rows.next()) {
				remake = rows.getString("VAL");
			}
		} catch (Exception ex) {
			// throw ExceptionUtil.parse(ex);
		} finally {
			DBean.closeAll(null, stmt, db);
		}
		return remake;
	}

	@Override
	public String getUwEscaComments(Long underwriteId) {
		String comments = "";
		DBean db = new DBean(true);
		java.sql.PreparedStatement stmt = null;
		Connection conn = null;
		java.sql.ResultSet rows = null;
		try {
			db.connect();
			conn = db.getConnection();
			/* 取得陳核備註說明 */
			stmt = conn.prepareStatement("SELECT PKG_LS_PM_NB_CI.f_get_comments_Uw_Esca(?) AS VAL FROM DUAL");
			stmt.setLong(1, underwriteId);
			stmt.execute();
			rows = stmt.executeQuery();
			while (rows.next()) {
				comments = rows.getString("VAL");
			}
		} catch (Exception ex) {
			throw ExceptionUtil.parse(ex);
		} finally {
			DBean.closeAll(null, stmt, db);
		}
		return comments;
	}

	@Override
	public String getUwManagerComments(Long underwriteId) {
		String comments = "";
		DBean db = new DBean(true);
		java.sql.PreparedStatement stmt = null;
		Connection conn = null;
		java.sql.ResultSet rows = null;
		try {
			db.connect();
			conn = db.getConnection();
			/* 取得覆核主管核保意見 */
			stmt = conn.prepareStatement("SELECT PKG_LS_PM_NB_CI.f_get_comments_uw_manager(?) AS VAL FROM DUAL");
			stmt.setLong(1, underwriteId);
			stmt.execute();
			rows = stmt.executeQuery();
			while (rows.next()) {
				comments = rows.getString("VAL");
			}
		} catch (Exception ex) {
			throw ExceptionUtil.parse(ex);
		} finally {
			DBean.closeAll(null, stmt, db);
		}
		return comments;
	}

	/**
	 * <p>
	 * Description : 險種資料刪除時查詢是否有險種的批註資料
	 * </p>
	 * <p>
	 * Created By : simon.huang
	 * </p>
	 * <p>
	 * Create Time : Nov 6, 2016
	 * </p>
	 *
	 * @param itemId
	 * @return
	 * @throws GenericException
	 */
	@Override
	public int getUWExclusionCountByItemId(Long itemId, Long underwriteId) throws GenericException {
		DBean db = new DBean();
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer buf = new StringBuffer();
		int count = 0;
		buf.append("select count(*) from T_UW_EXCLUSION ");
		buf.append(" where 1=1 and ITEM_ID = ? and UNDERWRITE_ID = ? ");
		try {
			db.connect();
			Connection con = db.getConnection();
			ps = con.prepareStatement(buf.toString());
			int pos = 1;
			ps.setLong(pos++, itemId);
			ps.setLong(pos++, underwriteId);
			rs = ps.executeQuery();
			while (rs.next()) {
				count = rs.getInt(1);
			}
		} catch (SQLException e) {
			throw new RTException(e);
		} catch (ClassNotFoundException e) {
			throw new RTException(e);
		} finally {
			DBean.closeAll(rs, ps, db);
		}
		return count;
	}

	/**
	 * <p>
	 * Description : 險種資料刪除時查詢是否有險種的加費資料
	 * </p>
	 * <p>
	 * Created By : simon.huang
	 * </p>
	 * <p>
	 * Create Time : Nov 6, 2016
	 * </p>
	 *
	 * @param itemId
	 * @return
	 * @throws GenericException
	 */
	@Override
	public int getUWExtraPremCountByItemId(Long itemId, Long underwriteId) throws GenericException {
		DBean db = new DBean();
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer buf = new StringBuffer();
		int count = 0;
		buf.append("select count(*) from T_UW_EXTRA_PREM ");
		buf.append(" where 1=1 and ITEM_ID = ? and UNDERWRITE_ID = ? ");
		try {
			db.connect();
			Connection con = db.getConnection();
			ps = con.prepareStatement(buf.toString());
			int pos = 1;
			ps.setLong(pos++, itemId);
			ps.setLong(pos++, underwriteId);
			rs = ps.executeQuery();
			while (rs.next()) {
				count = rs.getInt(1);
			}
		} catch (SQLException e) {
			throw new RTException(e);
		} catch (ClassNotFoundException e) {
			throw new RTException(e);
		} finally {
			DBean.closeAll(rs, ps, db);
		}
		return count;
	}

	@Override
	public int getUWExclusionCountByInsuredPartyId(Long underwriteId, Long orgPartyId) {
		DBean db = new DBean();
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer buf = new StringBuffer();
		int count = 0;
		buf.append("select count(*) from T_UW_EXCLUSION ");
		buf.append(" where 1=1 and UNDERWRITE_ID = ? and INSURED_ID = ?");
		try {
			db.connect();
			Connection con = db.getConnection();
			ps = con.prepareStatement(buf.toString());
			int pos = 1;
			ps.setLong(pos++, underwriteId);
			ps.setLong(pos++, orgPartyId);
			rs = ps.executeQuery();
			while (rs.next()) {
				count = rs.getInt(1);
			}
		} catch (SQLException e) {
			throw new RTException(e);
		} catch (ClassNotFoundException e) {
			throw new RTException(e);
		} finally {
			DBean.closeAll(rs, ps, db);
		}
		return count;
	}

	@Override
	public int getUWExtraPremCountByInsuredPartyId(Long underwriteId, Long orgPartyId) {
		DBean db = new DBean();
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer buf = new StringBuffer();
		int count = 0;
		buf.append("select count(*) from T_UW_EXTRA_PREM ");
		buf.append(" where 1=1 and UNDERWRITE_ID = ? ");

		if (orgPartyId != null) {
			buf.append("and INSURED_ID = ?");
		}
		try {
			db.connect();
			Connection con = db.getConnection();
			ps = con.prepareStatement(buf.toString());
			int pos = 1;
			ps.setLong(pos++, underwriteId);
			if (orgPartyId != null) {
				ps.setLong(pos++, orgPartyId);
			}
			rs = ps.executeQuery();
			while (rs.next()) {
				count = rs.getInt(1);
			}
		} catch (SQLException e) {
			throw new RTException(e);
		} catch (ClassNotFoundException e) {
			throw new RTException(e);
		} finally {
			DBean.closeAll(rs, ps, db);
		}
		return count;
	}

	/**
	 * <p>
	 * Description : 更新msgId及documentId回核保批註檔
	 * </p>
	 * <p>
	 * Created By : simon.huang
	 * </p>
	 * <p>
	 * Create Time : Aug 21, 2016
	 * </p>
	 *
	 * @param underwriteId
	 * @param insuredPartyId
	 * @param msgId
	 * @param documentId
	 */
	@Override
	public void updateUwExclusionMsgIdDocumentId(Long underwriteId, Long insuredPartyId, Long msgId, Long documentId) {

		DBean db = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		StringBuffer sb = new StringBuffer();
		sb.append(" update T_UW_EXCLUSION set msg_id = ?, document_Id = ?");
		sb.append(" where  underwrite_id = ?  and insured_id = ?");
		try {
			db = new DBean();
			db.connect();
			pst = db.getConnection().prepareStatement(sb.toString());
			if (msgId == null) {
				pst.setNull(1, Types.NUMERIC);
			} else {
				pst.setLong(1, msgId);
			}
			if (documentId == null) {
				pst.setNull(2, Types.NUMERIC);
			} else {
				pst.setLong(2, documentId);
			}

			pst.setLong(3, underwriteId);
			pst.setLong(4, insuredPartyId);

			pst.execute();
		} catch (SQLException e) {
			throw new RTException(e);
		} catch (ClassNotFoundException e) {
			throw new RTException(e);
		} finally {
			DBean.closeAll(rs, pst, db);
		}

	}

	/**
	 * <p>
	 * Description : 更新documentId回核保批註檔
	 * </p>
	 * <p>
	 * Created By : simon.huang
	 * </p>
	 * <p>
	 * Create Time : Aug 21, 2016
	 * </p>
	 *
	 * @param underwriteId
	 * @param insuredPartyId
	 * @param documentId
	 */
	@Override
	public void updateUwExclusionDocumentId(Long underwriteId, Long insuredPartyId, Long documentId) {

		DBean db = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		StringBuffer sb = new StringBuffer();
		sb.append(" update T_UW_EXCLUSION set document_Id = ?");
		sb.append(" where  underwrite_id = ?  and insured_id = ?");
		try {
			db = new DBean();
			db.connect();
			pst = db.getConnection().prepareStatement(sb.toString());

			if (documentId == null) {
				pst.setNull(1, Types.NUMERIC);
			} else {
				pst.setLong(1, documentId);
			}

			pst.setLong(2, underwriteId);
			pst.setLong(3, insuredPartyId);

			pst.execute();
		} catch (SQLException e) {
			throw new RTException(e);
		} catch (ClassNotFoundException e) {
			throw new RTException(e);
		} finally {
			DBean.closeAll(rs, pst, db);
		}

	}

	/**
	 * <p>
	 * Description : 更新msgId及documentId回核保加費檔
	 * </p>
	 * <p>
	 * Created By : simon.huang
	 * </p>
	 * <p>
	 * Create Time : Aug 21, 2016
	 * </p>
	 *
	 * @param underwriteId
	 * @param msgId
	 * @param documentId
	 */
	@Override
	public void updateUwExtraPremMsgIdDocumentId(Long underwriteId, Long msgId, Long documentId) {

		DBean db = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		StringBuffer sb = new StringBuffer();
		sb.append(" update T_UW_EXTRA_PREM set msg_id = ?, document_Id = ?");
		sb.append(" where  underwrite_id = ?  ");
		try {
			db = new DBean();
			db.connect();
			pst = db.getConnection().prepareStatement(sb.toString());
			if (msgId == null) {
				pst.setNull(1, Types.NUMERIC);
			} else {
				pst.setLong(1, msgId);
			}
			if (documentId == null) {
				pst.setNull(2, Types.NUMERIC);
			} else {
				pst.setLong(2, documentId);
			}

			pst.setLong(3, underwriteId);

			pst.execute();
		} catch (SQLException e) {
			throw new RTException(e);
		} catch (ClassNotFoundException e) {
			throw new RTException(e);
		} finally {
			DBean.closeAll(rs, pst, db);
		}

	}

	/**
	 * <p>
	 * Description : 更新documentId回核保加費檔
	 * </p>
	 * <p>
	 * Created By : simon.huang
	 * </p>
	 * <p>
	 * Create Time : Aug 21, 2016
	 * </p>
	 *
	 * @param underwriteId
	 * @param documentId
	 */
	@Override
	public void updateUwExtraPremDocumentId(Long underwriteId, Long documentId) {

		DBean db = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		StringBuffer sb = new StringBuffer();
		sb.append(" update T_UW_EXTRA_PREM set  document_Id = ?");
		sb.append(" where  underwrite_id = ?  ");
		try {
			db = new DBean();
			db.connect();
			pst = db.getConnection().prepareStatement(sb.toString());

			if (documentId == null) {
				pst.setNull(1, Types.NUMERIC);
			} else {
				pst.setLong(1, documentId);
			}

			pst.setLong(2, underwriteId);

			pst.execute();
		} catch (SQLException e) {
			throw new RTException(e);
		} catch (ClassNotFoundException e) {
			throw new RTException(e);
		} finally {
			DBean.closeAll(rs, pst, db);
		}

	}

	@Override
	public String getUwSourceType(Long underwriteId) {
		String uwSourceType = "";
		DBean db = new DBean(true);
		java.sql.PreparedStatement stmt = null;
		Connection conn = null;
		java.sql.ResultSet rows = null;
		try {
			db.connect();
			conn = db.getConnection();

			stmt = conn.prepareStatement("SELECT UW_SOURCE_TYPE FROM T_UW_POLICY WHERE UNDERWRITE_ID = ?");
			stmt.setLong(1, underwriteId);
			rows = stmt.executeQuery();
			while (rows.next()) {
				uwSourceType = rows.getString("UW_SOURCE_TYPE");
			}
		} catch (Exception ex) {
			// throw ExceptionUtil.parse(ex);
		} finally {
			DBean.closeAll(null, stmt, db);
		}
		return uwSourceType;
	}

	@Override
	public UwPolicy findLastestUWPolicy(Long policyId, String uwSourceType) {
		Session session = HibernateSession3.currentSession();
		Criteria criteria = session.createCriteria(UwPolicy.class);
		criteria.add(Restrictions.eq("policyId", policyId)).add(Restrictions.eq("uwSourceType", uwSourceType));
		criteria.addOrder(Order.desc("underwriteId"));
		java.util.List<UwPolicy> uwPolicyList = (java.util.List<UwPolicy>) criteria.list();
		if (org.apache.commons.collections.CollectionUtils.isEmpty(uwPolicyList))
			return new UwPolicy();
		return uwPolicyList.get(0);
	}

	/**
	 * @param tableName
	 * @param underwriteId
	 * @param msgId
	 * @throws com.ebao.pub.framework.GenericException
	 * @return
	 */
	public List<Long> findSubDocMsgByMsgId(String tableName, Long underwriteId, Long msgId) throws GenericException {

		if (1 == 0 || underwriteId == null || msgId == null) {
			throw new NullPointerException();
		}
		List<Long> list = new ArrayList<Long>();

		DBean db = new DBean(true);
		java.sql.PreparedStatement stmt = null;
		Connection conn = null;
		java.sql.ResultSet rows = null;
		try {
			StringBuffer buf = new StringBuffer();
			buf.append(" select msg_id from v_uw_msg_subdoc_for_check o where 1=1");
			buf.append("  and o.table_name = ?");
			buf.append("  and o.underwrite_id = ?");
			buf.append("  and o.msg_id = ?");

			db.connect();
			conn = db.getConnection();

			stmt = conn.prepareStatement(buf.toString());
			stmt.setString(1, tableName);
			stmt.setLong(2, underwriteId);
			stmt.setLong(3, msgId);
			rows = stmt.executeQuery();
			while (rows.next()) {
				list.add(rows.getLong("MSG_ID"));
			}
		} catch (Exception ex) {
			throw ExceptionUtil.parse(ex);
		} finally {
			DBean.closeAll(null, stmt, db);
		}
		return list;

	}

	/**
	 * remove uw endorsement by underwriteId no endorsement code
	 *
	 * @param underwriteId
	 * @throws GenericException
	 */
	public void removeUwEndorsementByUnderwriteIdNoCode(Long underwriteId) throws GenericException {
		List<UwEndorsement> endorsements = uwEndorsementDao.findByUnderwriteId(underwriteId);
		if (endorsements != null) {
			Iterator iter = endorsements.iterator();
			while (iter.hasNext()) {
				UwEndorsement endorsement = (UwEndorsement) iter.next();
				uwEndorsementDao.remove(endorsement.getUwListId());
			}
		}
	}

	/**
	 * 查詢此次保全變更下有哪些UnderwriteId，但是排除次標加費保全項的UnderwriteId
	 *
	 * @param changeId
	 * @param policyId
	 * @return
	 * @throws GenericException
	 */
	public List<Long> findUnderwriteIdByNotExtraPrem(Long changeId, Long policyId) throws GenericException {

		if (changeId == null || policyId == null) {
			return null;
		}
		List<Long> list = new ArrayList<Long>();

		DBean db = new DBean(true);
		java.sql.PreparedStatement stmt = null;
		Connection conn = null;
		java.sql.ResultSet rows = null;
		try {
			StringBuffer buf = new StringBuffer();
			buf.append("      SELECT MIN ( t.underwrite_id ) underwrite_id ");
			buf.append("        FROM T_UW_POLICY t ");
			buf.append("        WHERE t.cs_transaction = 165 ");
			buf.append("        AND t.uw_status IN (9) ");
			buf.append("        AND t.uw_source_type = 2 ");
			buf.append("        AND t.policy_id = ? ");
			buf.append("        AND t.change_id = ? ");

			db.connect();
			conn = db.getConnection();
			logger.error("findUnderwriteIdByNotExtraPrem.sql=" + buf.toString() + "\n,policyId=" + policyId
					+ ",changeId=" + changeId);

			stmt = conn.prepareStatement(buf.toString());
			stmt.setLong(1, policyId);
			stmt.setLong(2, changeId);

			rows = stmt.executeQuery();
			Long underwriteIdBy165 = null;
			if (rows.next() && rows.getLong("UNDERWRITE_ID") != 0) {
				underwriteIdBy165 = rows.getLong("UNDERWRITE_ID");
			}
			buf = new StringBuffer();
			buf.append("SELECT UNDERWRITE_ID FROM T_UW_POLICY ");
			buf.append(" WHERE policy_id = ? ");
			buf.append(" AND change_id = ? ");
			 //IR 363066 修正受理次標加費批註加上其他，進入核保後批註與照會單顯示問題
			buf.append(" AND uw_status IN ( 0 , 1, 5 ) ");
			buf.append(" AND uw_source_type = 2 ");

			if (underwriteIdBy165 != null) {
				buf.append(" AND underwrite_id != " + underwriteIdBy165);
			}
			logger.error("findUnderwriteIdByNotExtraPrem.sql=" + buf.toString());
			stmt = conn.prepareStatement(buf.toString());
			stmt.setLong(1, policyId);
			stmt.setLong(2, changeId);

			rows = stmt.executeQuery();
			while (rows.next()) {
				Long temp = rows.getLong("UNDERWRITE_ID");
				list.add(temp);
				logger.error("findUnderwriteIdByNotExtraPrem.UNDERWRITE_ID=" + temp);
			}
			logger.error("findUnderwriteIdByNotExtraPrem.listSize=" + list.size());
		} catch (Exception ex) {
			logger.error("findUnderwriteIdByNotExtraPrem.Exception=" + ex);
			throw ExceptionUtil.parse(ex);
		} finally {
			DBean.closeAll(null, stmt, db);
		}
		return list;
	}

	/**
	 * <p>
	 * Description : 取得再途件預設核保員
	 * </p>
	 * <p>
	 * Created By : simon.huang
	 * </p>
	 * <p>
	 * Create Time : Jun 6, 2017
	 * </p>
	 *
	 * @return
	 */
	public Long getOldPolicyUnderwriterId(Long policyId) {
		Long underwriterId = NBUtils.getUserIdFromParam(Cst.PARA_NB_OLD_POLICY_UNDERWRITER);
		return underwriterId;
	}

	/**
	 * <p>
	 * Description : 查詢是否有保全受理紀錄且已經成立
	 * </p>
	 * <p>
	 * Created By : simon.huang
	 * </p>
	 * <p>
	 * Create Time : Jul 11, 2017
	 * </p>
	 *
	 * @param policyId
	 * @return
	 */
	public int getCsApplicationCount(long policyId) {
		DBean db = new DBean(false);
		java.sql.PreparedStatement stmt = null;
		Connection conn = null;
		java.sql.ResultSet rows = null;
		int count = 0;
		try {
			String sql = "select count(*) from t_cs_application p where p.policy_id=? "
					+ " and p.cs_app_status not in (?,?)";

			db.connect();
			conn = db.getConnection();

			stmt = conn.prepareStatement(sql);

			stmt.setLong(1, policyId);
			stmt.setInt(2, CodeCst.CS_APP_STATUS__REJECTED);
			stmt.setInt(3, CodeCst.CS_APP_STATUS__CANCELLED);

			rows = stmt.executeQuery();
			while (rows.next()) {
				count = rows.getInt(1);
				break;
			}
		} catch (Exception ex) {
			throw ExceptionUtil.parse(ex);
		} finally {
			DBean.closeAll(null, stmt, db);
		}

		return count;
	}

	@Override
	public Map<String, String> refundPolicySuspenses(Long policyId, String withdrawType) throws GenericException {

		Map<String, String> result = new HashMap<String, String>();
		/* 查詢懸帳金額 */
		BigDecimal cashValue = cashCI.getSuspenseByIdPremPurpose(policyId, CodeCst.PREM_PURPOSE__GENERAL);
		if (cashValue == null || cashValue.longValue() == 0) {
			return result;
		}

		logger.info(String.format("[refundPolicySuspenses][懸帳金額 ] policy_id := %d, cashValue := %d", policyId,
				cashValue.longValue()));

		/* 未兌現支票 */
		Boolean hasUnCashCheque = chequeCI.hasUnCashCheque(policyId);
		if (hasUnCashCheque) {
			logger.info("[refundPolicySuspenses][未兌現支票] policy_id := " + policyId);
			Policy policy = policyDao.load(policyId);
			String refundLetIndi = policy.getProposalInfo().getRefundLetIndi();
			String langId = AppContext.getCurrentUser().getLangId();
			if (StringUtil.isNullOrEmpty(refundLetIndi)) {
				policy.getProposalInfo().setRefundLetIndi("B");// 設定flag讓PA判斷需出照會
				policyDao.saveorUpdate(policy);

				// 註解以下統一訊息文字
				// result.put("rtnMsg", StringResource.getStringData("MSG_1263025",
				// langId));//支票未兌現，請PA協助處理(照會將於夜間批次發放)
				// } else if ("B".equals(refundLetIndi)) {
				// result.put("rtnMsg", StringResource.getStringData("MSG_1263025", langId));
				// } else if ("F".equals(refundLetIndi)) {
				// result.put("rtnMsg", StringResource.getStringData("MSG_1263026",
				// langId));//支票未兌現，請PA協助處理(照會已發放)
			}
			// 統一顯示訊息為：MSG_1263023 支票未兌現，請會辦保費入帳人員
			result.put("rtnMsg", StringResource.getStringData("MSG_1263023", langId));
			result.put("rtnCode", "error");

			return result;
		}

		/* 查詢懸帳紀錄 */
		int premPurpose = Integer.valueOf(CodeCst.PREM_PURPOSE__GENERAL);
		List<CashVO> cashList = new ArrayList<CashVO>();
		try {
			List<CashVO> checkCashList = cashCI.findByPolicyIdStatusAndPremPurpose(policyId,
					CodeCst.FEE_STATUS__PAY_CONFIRMED, premPurpose);

			// 2017/09/21 simon:
			// RTC-190312-若已沖銷,pay_balance=0,則不可再執行退費作業
			for (CashVO cashVO : checkCashList) {
				if (cashVO.getPayBalance() != null && cashVO.getPayBalance().compareTo(BigDecimal.ZERO) > 0) {
					cashList.add(cashVO);
				}
			}
		} catch (Exception e) {
			throw ExceptionUtil.parse(e);
		}
		if (cashList.size() == 0) {
			return result;
		}

		/* 外幣無法支票退費 */
		boolean notTWDCheck = false;
		for (CashVO cashVO : cashList) {
			Integer payMode = cashVO.getPayMode();
			if (CodeCst.MONEY__NTD != cashVO.getMoneyId()) {
				if (payMode != null && payMode != CodeCst.PAY_MODE__CREDIT_CARD) {
					notTWDCheck = true;
					logger.info(String.format("[refundPolicySuspenses][外幣無法支票退費] policy_id := %d, currency := %d",
							policyId, cashVO.getMoneyId()));
					break;
				}
			}

			String langId = AppContext.getCurrentUser().getLangId();
			if (notTWDCheck) {
				result.put("rtnCode", "error");
				// MSG_1263027 外幣無法支票退費，請PA協助處理
				result.put("rtnMsg", StringResource.getStringData("MSG_1263027", langId));
				return result;
			}
		}

		// 對每筆T_CASH作退費
		UserTransaction ut = null;
		PolicyHolderVO phVO = policyHolderService.getPolicyHolder(policyId);
		AddressVO addressVO = phVO.getAddress();
		String policyHolderName = phVO.getName();
		Integer payMode = cashList.get(0).getPayMode();
		Long voucherId = cashList.get(0).getVoucherId();
		Long payeeId = cashList.get(0).getPayeeId();
		Integer paySource = cashList.get(0).getPaySource(); //PCR 499988 永豐網路投保
		try {

			List<Long> feeList = new ArrayList<Long>();
			for (CashVO cashVO : cashList) {
				feeList.add(cashVO.getFeeId());
			}

			String orderChequeIndi = "N";
			CashRefundVO cashRefundVO = new CashRefundVO();
			cashRefundVO.setUrgentIndi(CodeCst.YES_NO__NO);
			if (payMode != null && payMode != CodeCst.PAY_MODE__CREDIT_CARD) {
				cashRefundVO.setPayeeName(policyHolderName);
				payMode = CodeCst.PAY_MODE__CHEQUE;
				orderChequeIndi = "Y";
				if (addressVO == null || StringUtils.isNullOrEmpty(addressVO.getAddress1())) {
					logger.info(String.format(
							"[refundPolicySuspenses][要保人無填寫地址] policy_id := %d, policyHolder.partyId := %d, policyHolder.addressId",
							policyId, phVO.getPartyId(), addressVO.getAddressId()));
					result.put("rtnCode", "error");
					result.put("rtnMsg", "支票退費無地址");
					return result;
				}
				cashRefundVO.setChequeSendType(2);
				cashRefundVO.setSendAddress(addressVO.getAddress1());
				cashRefundVO.setSendZipCode(addressVO.getPostCode());
				if (phVO.isPerson()) {
					cashRefundVO.setRecipientName(phVO.getName());
				} else {
					cashRefundVO.setCompanyName(phVO.getName());
				}
			} else {

				if (voucherId != null) {
					ColVoucherVO vo = colVoucherDS.get(voucherId);
					//PCR 499988 永豐網路投保信用卡 paysource = 41-網路投保(永豐銀)
					if ( paySource != null && paySource.equals(41)) {
						cashRefundVO.setAcquirerId(102L);// 信用卡收單行,t_acquirer.acquirerid = 102  永豐商業銀行
						cashRefundVO.setPropoSerialNum(vo.getVoucherCode()); //要保書檔流水編號 / 永豐訂單編號
						cashRefundVO.setVoucherTransNo(vo.getTransNo());;    //永豐豐收款交易編號
					}else {	
						cashRefundVO.setCreditCode(vo.getVoucherCode());
						cashRefundVO.setCreditWarrant(vo.getVoucherCode2());
						cashRefundVO.setCreditCardExp(DateUtils.date2String(vo.getExpiredDate(), "yyyy/MM"));
						// IR_300597 增加信用卡退費收單行邏輯
						AcquirerVO acquirerVO = acquirerService.findAcquirerByLastCash(policyId);
						if (acquirerVO != null) {
							cashRefundVO.setAcquirerId(acquirerVO.getAcquirerId());// 信用卡收單行
							cashRefundVO.setBankSsno(acquirerService.getBankSSNO(acquirerVO.getAcquirerId(),policyId));  // PCR 354366 台新信用卡取得特店代碼
						}					
					}
				}
			}

			String policyCode = policyDao.findPolicyCodeByPolicyId(policyId);
			Long underwriterId = this.getUNBUnderwriterId(policyId);

			EmployeeVO underwriterVO = employeeCI.getEmployeeByEmpId(underwriterId);
			logger.info(String.format(
					"[refundPolicySuspenses][premCI.refundPolicySuspenses]policy_id :=%d , policy_code := %s, payMode := %d, withdrawType := %s, payeeId := %s, orderChequeIndi := %s, ",
					policyId, policyCode, payMode, withdrawType, payeeId, orderChequeIndi.charAt(0)));
			ut = Trans.getUserTransaction();
			ut.begin();
			Long feeId = premCI.refundPolicySuspenses(policyId, policyCode, Long.valueOf(payMode),
					Long.valueOf(withdrawType), payeeId, orderChequeIndi.charAt(0), "N", underwriterId,
					AppContext.getCurrentUserLocalTime().getTime(), feeList, cashRefundVO);

			// 需更新cashier為核保員
			CashCIVO cashCIVO = cashCI.findTCashByFeeId(feeId);
			cashCIVO.setCashierId(underwriterId);
			cashCIVO.setCashierOrganId(underwriterVO.getOrganId());
			cashCIVO.setCashierDeptId(underwriterVO.getDeptId());
			cashCI.updateTCash(cashCIVO);
			// end

			result.put("rtnCode", "success");
			ut.commit();
		} catch (Exception e) {
			TransUtils.rollback(ut);
			throw ExceptionUtil.parse(e);
		}

		return result;
	}

	@Override
	public String isUwTransfer(Long policyId) {
		DBean db = new DBean(false);
		java.sql.PreparedStatement stmt = null;
		Connection conn = null;
		java.sql.ResultSet rows = null;
		try {
			StringBuffer sb = new StringBuffer();

			sb.append("select count(1) from t_uw_transfer t  ");
			sb.append("  where t.flow_id in (2,3,4,5,6,90,91,92) ");
			sb.append("    and t.policy_id  = ? ");

			db.connect();
			conn = db.getConnection();
			stmt = conn.prepareStatement(sb.toString());

			stmt.setLong(1, policyId);

			rows = stmt.executeQuery();
			if (rows.next()) {
				BigDecimal count = rows.getBigDecimal(1);
				if (count != null && count.intValue() > 0) {
					return CodeCst.YES_NO__YES;
				}
			}
			return "";
		} catch (Exception ex) {
			throw ExceptionUtil.parse(ex);
		} finally {
			DBean.closeAll(null, stmt, db);
		}

	}

	@Override
	public String[] getPolicyDecisionAndCnacelDesc(Long policyId) {
		DBean db = new DBean(false);
		java.sql.PreparedStatement stmt = null;
		Connection conn = null;
		java.sql.ResultSet rows = null;
		try {
			StringBuffer sb = new StringBuffer("select t.policy_decision, t.cnacel_desc");
			sb.append(" from t_uw_policy t ");
			sb.append("where t.uw_source_type = '1' ");
			sb.append("  and t.uw_status = '4' ");
			sb.append("  and t.policy_id = ?  order by underwrite_id desc ");

			db.connect();
			conn = db.getConnection();
			stmt = conn.prepareStatement(sb.toString());

			stmt.setLong(1, policyId);
			rows = stmt.executeQuery();
			String[] returnArray = new String[2];
			if (rows.next()) {
				returnArray[0] = rows.getString(1);
				returnArray[1] = rows.getString(2);
				return returnArray;
			} else {
				return null;
			}

		} catch (Exception ex) {
			throw ExceptionUtil.parse(ex);
		} finally {
			DBean.closeAll(null, stmt, db);
		}

	}

	private List<CashVO> querySuspenseCash(Long policyId) {
		java.util.List<CashVO> list = new java.util.LinkedList<CashVO>();
		/* 查詢懸帳紀錄 */
		int premPurpose = Integer.valueOf(CodeCst.PREM_PURPOSE__GENERAL);
		try {
			List<CashVO> checkCashList = cashCI.findByPolicyIdStatusAndPremPurpose(policyId,
					CodeCst.FEE_STATUS__PAY_CONFIRMED, premPurpose);

			// 2017/09/21 simon:
			// RTC-190312-若已沖銷,pay_balance=0,則不可再執行退費作業
			for (CashVO cashVO : checkCashList) {
				if (cashVO.getPayBalance() != null && cashVO.getPayBalance().compareTo(BigDecimal.ZERO) > 0) {
					list.add(cashVO);
				}
			}
		} catch (Exception e) {
			throw ExceptionUtil.parse(e);
		}
		return list;
	}

	/**
	 * 邏輯調整需同步調整 PKG_LS_PM_NB_SPECAIL_FUND.p_nb_policy_withdraw_refund()
	 */
	@Override
	public Map<String, String> refundPolicyWithdraw(Long policyId, String withdrawType) {
		Map<String, String> result = new HashMap<String, String>();
		/* 查詢懸帳金額 */
		BigDecimal cashValue = cashCI.getSuspenseByIdPremPurpose(policyId, CodeCst.PREM_PURPOSE__GENERAL);
		if (cashValue == null || cashValue.longValue() == 0) {
			return result;
		}
		java.util.List<CashVO> cashList = querySuspenseCash(policyId);// 查詢懸帳紀錄
		if (CollectionUtils.isEmpty(cashList)) {
			return result;
		}

		// 對每筆T_CASH作退費
		PolicyHolderVO phVO = policyHolderService.getPolicyHolder(policyId);
		AddressVO addressVO = phVO.getAddress();
		String policyHolderName = phVO.getName();
		int curr = policyDao.findPolicyCurrency(policyId);

		List<Long> unCashCheque = chequeCI.getUnCashCheque(policyId); // ChequeId list

		if (unCashCheque != null && unCashCheque.size() > 0) {
			Policy policy = policyDao.load(policyId);
			String refundLetIndi = policy.getProposalInfo().getRefundLetIndi();
			if (StringUtil.isNullOrEmpty(refundLetIndi)) {
				policy.getProposalInfo().setRefundLetIndi("B"); // 設定flag讓PA判斷需出照會
				policyDao.saveorUpdate(policy);
			}
		}

		for (CashVO cash : cashList) {
			Integer payMode = cash.getPayMode();
			Long voucherId = cash.getVoucherId();
			Long payeeId = null;
			Integer paySource = cash.getPaySource(); //PCR 499988 永豐網路投保

			// 2017/09/21 simon:
			// RTC-190312-若已沖銷,pay_balance=0,則不可再執行退費作業
			if (cash.getPayBalance() != null && cash.getPayBalance().compareTo(BigDecimal.ZERO) > 0) {

				List<Long> feeList = new ArrayList<Long>();
				feeList.add(cash.getFeeId());

				String orderChequeIndi = "N";
				CashRefundVO cashRefundVO = new CashRefundVO();
				cashRefundVO.setUrgentIndi(CodeCst.YES_NO__NO);
				if (CodeCst.PAY_MODE__CREDIT_CARD == payMode && voucherId != null) {

					/* 信用卡 */
					ColVoucherVO vo = colVoucherDS.get(voucherId);
					
					//PCR 499988 永豐網路投保信用卡 paysource = 41-網路投保(永豐銀)
					if (paySource != null && paySource.equals(41)) {
						cashRefundVO.setAcquirerId(102L);// 信用卡收單行,t_acquirer.acquirerid = 102  永豐商業銀行
						cashRefundVO.setPropoSerialNum(vo.getVoucherCode()); //要保書檔流水編號 / 永豐訂單編號
						cashRefundVO.setVoucherTransNo(vo.getTransNo());     //永豐豐收款交易編號
						cashRefundVO.setCreditCode(null);
					}else {
						cashRefundVO.setCreditCode(vo.getVoucherCode());
						cashRefundVO.setCreditWarrant(vo.getVoucherCode2());
						cashRefundVO.setCreditCardExp(DateUtils.date2String(vo.getExpiredDate(), "yyyy/MM"));
						// IR_300597 增加信用卡退費收單行邏輯
						AcquirerVO acquirerVO = acquirerService.findAcquirerByLastCash(policyId);
						if (acquirerVO != null) {
							cashRefundVO.setAcquirerId(acquirerVO.getAcquirerId());// 信用卡收單行
							cashRefundVO.setBankSsno(acquirerService.getBankSSNO(acquirerVO.getAcquirerId(),policyId));  // PCR 354366 台新信用卡取得特店代碼
						}
					}
					payeeId = cash.getPayeeId();

					if (payeeId != null) {
						PartyVO partyVO = customerCI.getParty(payeeId);
						cashRefundVO.setPayeeName(partyVO.getPartyName());

						// RTC-250393-FIX 受款人證件號碼 未帶入 by Simon 2018/08/29
						if (partyVO.isPerson()) {
							CustomerVO customerVO = customerCI.getPerson(payeeId);
							cashRefundVO.setPayeeCertiCode(customerVO.getCertiCode());
						} else if (CodeCst.PARTY_TYPE__ORGAN_CUSTOMER.equals(partyVO.getPartyType())) {
							CompanyCustomerVO companyVO = customerCI.getCompany(payeeId);
							cashRefundVO.setPayeeCertiCode(companyVO.getRegisterCode());
						}
					} else {
						cashRefundVO.setPayeeName(policyHolderName);
						cashRefundVO.setPayeeCertiCode(phVO.getCertiCode());
					}

				} else if (CodeCst.PAY_MODE__GIRO == payMode && cash.getAccountId() != null) {

					/* 銀行轉帳 */
					cashRefundVO.setBankAccount(null);
					BankAccountVO bankAccountVO = bankCI.getBankAccountbyId(cash.getAccountId());
					String bankCode = bankAccountVO.getBankCode();
					String payeeAccount = bankAccountVO.getBankAccount();

					// 退款轉帳銀行代碼需七碼，檢核為三碼者，截取帳號的前四碼，補足至七碼。
					int bankCodeLength = StringUtils.getStringLength(bankCode);
					int payeeAccountLength = StringUtils.getStringLength(payeeAccount);
					if (bankCodeLength == 3 && payeeAccountLength > 4) {
						if ("700".equals(bankCode)) {// 郵局一律帶入7000021郵政存簿儲金
							bankCode = "7000021";
						} else {
							bankCode = bankCode.concat(payeeAccount.substring(0, 4));
						}
					}
					cashRefundVO.setPayeeBankCode(bankCode);
					cashRefundVO.setPayeeAccount(bankAccountVO.getBankAccount());
					cashRefundVO.setPayeeName(bankAccountVO.getAccoName());

					payeeId = cash.getPayeeId();

					if (payeeId != null) {
						PartyVO partyVO = customerCI.getParty(payeeId);
						cashRefundVO.setPayeeName(partyVO.getPartyName());

						// RTC-250393-FIX 受款人證件號碼 未帶入 by Simon 2018/08/29
						if (partyVO.isPerson()) {
							CustomerVO customerVO = customerCI.getPerson(payeeId);
							cashRefundVO.setPayeeCertiCode(customerVO.getCertiCode());
						} else if (CodeCst.PARTY_TYPE__ORGAN_CUSTOMER.equals(partyVO.getPartyType())) {
							CompanyCustomerVO companyVO = customerCI.getCompany(payeeId);
							cashRefundVO.setPayeeCertiCode(companyVO.getRegisterCode());
						}
					} else {
						cashRefundVO.setPayeeName(policyHolderName);
						cashRefundVO.setPayeeCertiCode(phVO.getCertiCode());
					}
				} else {

					// 外幣保單不可支票退費
					if (CodeCst.MONEY__NTD != cash.getMoneyId()) {// #225334
						logger.info(
								String.format("[refundPolicyWithdraw][外幣無法支票退費] policy_id := %d, currency := %d",
										policyId, curr));
						continue;
					}

					/* 該CASH為未兌現支票,不作退費處理 */
					if (cash.getChequeId() != null && unCashCheque.contains(cash.getChequeId())) {
						continue;
					}

					Boolean hasUnCashCheque = chequeCI.hasUnCashCheque(policyId);
					if (hasUnCashCheque) {
						logger.info("[refundPolicySuspenses][未兌現支票] policy_id := " + policyId);
						Policy policy = policyDao.load(policyId);
						String refundLetIndi = policy.getProposalInfo().getRefundLetIndi();
						if (StringUtil.isNullOrEmpty(refundLetIndi)) {
							policy.getProposalInfo().setRefundLetIndi("B");// 設定flag讓PA判斷需出照會
							policyDao.saveorUpdate(policy);
						}
						continue;
					}

					/* 支票退費 #220491 */
					payMode = CodeCst.PAY_MODE__CHEQUE;
					cashRefundVO.setPayeeName(policyHolderName);
					// RTC-250393-FIX 受款人證件號碼 未帶入 by Simon 2018/08/29
					cashRefundVO.setPayeeCertiCode(phVO.getCertiCode());
					payeeId = phVO.getPartyId();
					orderChequeIndi = "Y";

					if (addressVO == null || StringUtils.isNullOrEmpty(addressVO.getAddress1())) {
						logger.error(String.format(
								"[refundPolicyWithdraw][要保人無填寫地址] policy_id := %d, policyHolder.partyId := %d, policyHolder.addressId",
								policyId, phVO.getPartyId(), addressVO.getAddressId()));
						continue;
					}
					cashRefundVO.setChequeSendType(2);
					cashRefundVO.setSendAddress(addressVO.getAddress1());
					cashRefundVO.setSendZipCode(addressVO.getPostCode());
					if (phVO.isPerson()) {
						cashRefundVO.setRecipientName(phVO.getName());
					} else {
						cashRefundVO.setCompanyName(phVO.getName());
					}
				}

				/* 填寫 T_CASH */
				String policyCode = policyDao.findPolicyCodeByPolicyId(policyId);
				Long underwriterId = this.getUNBUnderwriterId(policyId);
				EmployeeVO underwriterVO = employeeCI.getEmployeeByEmpId(underwriterId);
				logger.info(String.format(
						"[refundPolicyWithdraw][premCI.refundPolicySuspenses]policy_id :=%d , policy_code := %s, payMode := %d, withdrawType := %s, payeeId := %s, orderChequeIndi := %s, ",
						policyId, policyCode, payMode, withdrawType, payeeId, orderChequeIndi.charAt(0)));
				Long feeId = premCI.refundPolicySuspenses(policyId, policyCode, Long.valueOf(payMode),
						Long.valueOf(withdrawType), payeeId, orderChequeIndi.charAt(0), "N", underwriterId,
						AppContext.getCurrentUserLocalTime().getTime(), feeList, cashRefundVO);

				// 需更新cashier為核保員
				CashCIVO cashCIVO = cashCI.findTCashByFeeId(feeId);
				cashCIVO.setCashierId(underwriterId);
				cashCIVO.setCashierOrganId(underwriterVO.getOrganId());
				cashCIVO.setCashierDeptId(underwriterVO.getDeptId());
				cashCI.updateTCash(cashCIVO);
				
				//目前接口premCI.refundPolicySuspenses()只作一筆全退,執行第2筆會因無金額會退拋出錯誤
				break;
			}
		}

		return result;
	}

	@Override
	public Collection findUwExtraLoadingsByinsuredId(Long underwriteId, Long insuredId, String extraType)
			throws GenericException {
		try {
			Criteria criteria = HibernateSession3.currentSession().createCriteria(UwExtraLoading.class);
			criteria.add(Expression.eq("underwriteId", underwriteId));
			criteria.add(Expression.eq("insuredId", insuredId));
			criteria.add(Expression.eq("extraType", extraType));
			Collection<UwExtraLoading> c = criteria.list();
			return BeanUtils.copyCollection(UwExtraLoadingVO.class, c);
		} catch (Exception e) {
			throw ExceptionFactory.parse(e);
		}
	}

	// IR 264793 -- start
	/**
	 * Retrieve Underwriting Id according to the policyId and userId
	 *
	 * @param policyId
	 *            Policy Id
	 * @param userId
	 *            User Id
	 * @return UnderwriteId
	 * @throws GenericException
	 *             Application Exception
	 * @author youchun.chen
	 * @version 1.0
	 * @since 20.9.2018
	 */
	public Long retrieveUwIdbyPolicyId(Long policyId, Long userId) throws GenericException {
		List<UwPolicy> list = uwPolicyDao.findByPolicyId(policyId);
		Long underwriteId = null;
		for (UwPolicy data : list) {
			Long tempUnderwriteId = data.getUnderwriterId();
			if (null != tempUnderwriteId && tempUnderwriteId.longValue() == userId.longValue()) {
				underwriteId = tempUnderwriteId;
				break;
			} else {
				continue;
			}
		}
		return underwriteId;
	}

	// IR 264793 -- end
	@Override
	public void writeInsuredCancelDetail(Long insuredListId) throws GenericException {
		Connection con = null;
		CallableStatement pst = null;
		DBean db = new DBean();
		ResultSet rs = null;
		try {
			db.connect();
			con = db.getConnection();
			pst =  con.prepareCall("{call  pkg_ls_pm_nb_special_func.p_write_cancel_detail(?) }");
			pst.setLong(1, insuredListId);
			pst.execute();
		} catch (Exception e) {
			logger.error(ExceptionUtils.getFullStackTrace(e));
			throw ExceptionFactory.parse(e);
		} finally {
			DBean.closeAll(rs, pst, db);
		}
	}
	

	
	/**
	 * <p>Description : 取得財務資訊-業報書及財告書收入</p>
	 * <p>Created By : Sun Yu</p>
	 * <p>Create Time : Mar 17, 2020</p>
	 * <p>PCR ： 305397</p>
	 * @param policyId
	 * @return
	 */
	public List<Map<String, Object>> getFinanceInfoDetail(Long policyId) {
		
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		SQLBuilder sql = new SQLBuilder(new StringBuffer(), IgnoreNull.TRUE, IgnoreEmpty.TRUE);
		sql.write( "select distinct POLICY_ID, CUSTOMER_NAME, CERTI_CODE, OLD_FOREIGNER_ID, POLICY_CODE, APPLY_DATE, ");
		sql.write("              UPDATE_TIME, PROPOSAL_STATUS, PROPOSAL_STATUS_DESC, ");
		sql.write("              AN_INCOME, AN_INCOME_OTHER, AN_INCOME_FAMILY, ");
		sql.write("              FN_INCOME, FN_INCOME_OTHER, FN_INCOME_FAMILY, ");
		sql.write("      listagg(POLICY_IDENTITY, '') within GROUP(order by POLICY_ID) over(  ");
		sql.write("        partition by POLICY_ID, CUSTOMER_NAME, CERTI_CODE, OLD_FOREIGNER_ID, POLICY_CODE, APPLY_DATE, ");
		sql.write("        UPDATE_TIME, PROPOSAL_STATUS, PROPOSAL_STATUS_DESC, AN_INCOME, AN_INCOME_OTHER, AN_INCOME_FAMILY, ");
		sql.write("        FN_INCOME, FN_INCOME_OTHER, FN_INCOME_FAMILY) POLICY_IDENTITY ");
		sql.writeAnyway("      from table(pkg_ls_rpt_unb_0631.f_get_report_finance_info(?)) ", policyId);

		try {
			org.hibernate.Session s = com.ebao.foundation.module.db.hibernate.HibernateSession3.currentSession();
			list = PagerDaoHelper.paging(s, sql, null);

		} catch (Exception ex) {
			throw ExceptionFactory.parse(ex);
		}

		return list;

	}
	
	@Override
	public void removeUwAddInformLetterByUnderwriteId(Long underwriteId) throws GenericException {
		if (underwriteId != null) {
			List<UwAddInformLetterVO> uwAddInfromLetterVoList = uwAddInformLetterService.findByUnderwriteId(underwriteId);
			for (UwAddInformLetterVO uwAddInformLetterVO : uwAddInfromLetterVoList) {
				uwAddInformLetterService.remove(uwAddInformLetterVO.getListId());
			}			
		}
	}
	
	@Override
	public void removeUwSickFormLetterByUnderwriteId(Long underwriteId) throws GenericException {
		if (underwriteId != null) {
			List<UwSickFormLetter> uwSickFormLetterVoList = uwSickFormLetterDao.findByPolicyInsured(underwriteId, null, null);
			for (UwSickFormLetter uwSickFormLetterVO : uwSickFormLetterVoList) {
				uwSickFormItemDao.deleteItem(uwSickFormLetterVO.getListId());
				uwSickFormLetterDao.remove(uwSickFormLetterVO);
			}			
		}
	}
	
	@Override
	public void removeUwMedicalLetterByUnderwriteId(Long underwriteId) throws GenericException {
		if (underwriteId != null) {
			List<UwMedicalLetter> letterList = uwMedicalLetterDao.queryAllMedicalLetter(underwriteId, null);
			for (UwMedicalLetter letter : letterList) {
				uwMedicalReasonDao.deleteItem(letter.getListId());
				uwMedicalItemDao.deleteItem(letter.getListId());
				uwMedicalLetterDao.remove(letter);
			}		
		}
	}
}