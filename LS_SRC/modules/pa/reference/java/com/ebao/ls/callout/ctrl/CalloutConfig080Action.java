package com.ebao.ls.callout.ctrl;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.UserTransaction;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.ls.callout.bs.CalloutConfigService;
import com.ebao.ls.callout.vo.CalloutConfigVO;
import com.ebao.ls.chl.service.ChannelOrgService;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.framework.ExceptionFactory;
import com.ebao.pub.framework.GenericAction;
import com.ebao.pub.i18n.lang.StringResource;
import com.ebao.pub.util.StringUtils;
import com.ebao.pub.util.Trans;
import com.ebao.pub.util.TransUtils;

public class CalloutConfig080Action extends GenericAction {

    private static final String ACTION_SAVE = "save";

    private static final String ACTION_DELETE = "delete";

    private static final String ACTION_DELETE2 = "delete2";

    private static final String ACTION_ADD = "add";

    private static final String ACTION_ADD2 = "add2";

    private static final String ACTION_MODIFY = "modify";

    private static final String ACTION_MODIFY2 = "modify2";

    private static final String FORWARD_RESULT = "success";

    private static final String CONFIG_TYPE_OF_1 = "1"; // 1 080抽樣比例

    private static final String CONFIG_TYPE_OF_2 = "2"; // 2 080_BR通路單位

    private static final String CONFIG_TYPE_OF_3 = "3"; // 3 080抽樣通路

    private static final String SAVE_SUCCESSED = "MSG_1257200";// 設定成功！

    private static final String SAVE_FAILED = "MSG_1256002"; // 儲存失敗!

    private static final String SAVE_DUPLICATED = "MSG_1257359"; // 資料重複

    private static final String BR_SAMPLING = "MSG_1257237"; // BR抽樣

    private static final String SC_SAMPLING = "MSG_1257035"; // 通路抽樣

    @Resource(name = CalloutConfigService.BEAN_DEFAULT)
    private CalloutConfigService calloutConfigService;

    @Resource(name = ChannelOrgService.BEAN_DEFAULT)
    private ChannelOrgService channelOrgService;

    @Override
    public ActionForward process(ActionMapping mapping, ActionForm form,
                    HttpServletRequest request, HttpServletResponse response) throws Exception {

        CalloutConfigForm calloutConfigForm = (CalloutConfigForm) form;
        CalloutConfigVO vo1 = calloutConfigForm.getCalloutConfigVO1();
        CalloutConfigVO vo2 = calloutConfigForm.getCalloutConfigVO2();
        String saction = calloutConfigForm.getActionType();

        if (ACTION_ADD.equals(saction) || ACTION_MODIFY.equals(saction)) {
            UserTransaction ut = null;
            try {
                ut = Trans.getUserTransaction();
                vo1.setConfigType(String.valueOf(CONFIG_TYPE_OF_2)); // 080_BR通路單位
                vo1.setFileName(StringResource.getStringData("MSG_1257240", AppContext.getCurrentUser().getLangId())); // 檔案080-X
                vo1.setFileDesc(StringResource.getStringData("MSG_1257241", AppContext.getCurrentUser().getLangId()));// BR通路投資型保單
                if (StringUtils.isEmpty(vo1.getChannelOrg() == null ? "" : vo1.getChannelOrg().toString())) {
                    request.setAttribute("message", SAVE_FAILED);
                } else if (calloutConfigService.findCalloutConfig(vo1)) {
                    request.setAttribute("message", SAVE_DUPLICATED);
                } else {
                    ut.begin();
                    calloutConfigService.saveOrUpdateForNBU(vo1);
                    ut.commit();
                    request.setAttribute("message", SAVE_SUCCESSED);
                }
            } catch (Exception e) {
                TransUtils.rollback(ut);
                request.setAttribute("message", SAVE_FAILED);
            }
            request.setAttribute("configType", BR_SAMPLING);
            renewDataTable(request, saction);
            return mapping.findForward(FORWARD_RESULT);
        } else if (ACTION_DELETE.equals(saction)) {
            UserTransaction ut = null;
            try {
                ut = Trans.getUserTransaction();
                ut.begin();
                calloutConfigService.remove(vo1.getEntityId());
                ut.commit();
                request.setAttribute("message", SAVE_SUCCESSED);
            } catch (Exception e) {
                TransUtils.rollback(ut);
                request.setAttribute("message", SAVE_FAILED);
            }
            request.setAttribute("configType", BR_SAMPLING);
            renewDataTable(request, saction);
            return mapping.findForward(FORWARD_RESULT);
        } else if (ACTION_ADD2.equals(saction) || ACTION_MODIFY2.equals(saction)) {
            UserTransaction ut = null;
            try {
                ut = Trans.getUserTransaction();
                vo2.setConfigType(String.valueOf(CONFIG_TYPE_OF_3)); // 080抽樣通路
                vo2.setFileName(StringResource.getStringData("MSG_1257242", AppContext.getCurrentUser().getLangId()));
                vo2.setFileDesc(StringResource.getStringData("MSG_1257243", AppContext.getCurrentUser().getLangId()));
                if (StringUtils.isEmpty(vo2.getSalesChannelType() == null ? "" : vo2.getSalesChannelType().toString())) {
                    request.setAttribute("message", SAVE_FAILED);
                } else if (calloutConfigService.findCalloutConfig(vo2)) {
                    request.setAttribute("message", SAVE_DUPLICATED);
                } else {
                    ut.begin();
                    calloutConfigService.saveOrUpdateForNBU(vo2);
                    ut.commit();
                    request.setAttribute("message", SAVE_SUCCESSED);
                }
            } catch (Exception e) {
                TransUtils.rollback(ut);
                request.setAttribute("message", SAVE_FAILED);
            }
            request.setAttribute("configType", SC_SAMPLING);
            renewDataTable(request, saction);
            return mapping.findForward(FORWARD_RESULT);
        } else if (ACTION_DELETE2.equals(saction)) {
            UserTransaction ut = null;
            try {
                ut = Trans.getUserTransaction();
                ut.begin();

                calloutConfigService.remove(vo2.getEntityId());
                ut.commit();
                request.setAttribute("message", SAVE_SUCCESSED);
            } catch (Exception e) {
                TransUtils.rollback(ut);
                request.setAttribute("message", SAVE_FAILED);
            }
            request.setAttribute("configType", SC_SAMPLING);
            renewDataTable(request, saction);
            return mapping.findForward(FORWARD_RESULT);
        } else if (ACTION_SAVE.equals(saction)) {
            UserTransaction transaction = null;
            try {
                transaction = Trans.getUserTransaction();
                transaction.begin();
                List<CalloutConfigVO> listVO = calloutConfigForm.getGridList();
                for (int i = 0; i < listVO.size(); i++) {
                    CalloutConfigVO calloutConfigVO = listVO.get(i);
                    calloutConfigService.saveOrUpdateCalloutConfigVO(calloutConfigVO);
                }
                transaction.commit();
                request.setAttribute("message", SAVE_SUCCESSED);
            } catch (Exception e1) {
                TransUtils.rollback(transaction);
                request.setAttribute("message", SAVE_FAILED);
                throw ExceptionFactory.parse(e1);
            }
            request.setAttribute("configType", "MSG_1257033"); // 抽樣比例
            // return mapping.findForward(ACTION_SAVE);
            renewDataTable(request, saction);
            return mapping.findForward(FORWARD_RESULT);
        } else {
            if (StringUtils.isNullOrEmpty(saction))
                saction = "search";

            renewDataTable(request, saction);
            return mapping.findForward(FORWARD_RESULT);
        }
    }

    private void renewDataTable(HttpServletRequest request, String saction) {

        List<Map<String, Object>> gridList = calloutConfigService.query(CONFIG_TYPE_OF_1, null, null); // 1
        // 080抽樣比例，不分頁
        List<Map<String, Object>> gridList1 = calloutConfigService.query(CONFIG_TYPE_OF_2, null, null); // 2
        // 080_BR通路單位，不分頁
        List<Map<String, Object>> gridList2 = calloutConfigService.query(CONFIG_TYPE_OF_3, null, null); // 3
        // 080抽樣通路，不分頁
        CalloutConfigForm calloutConfigForm;
        calloutConfigForm = new CalloutConfigForm();

        for (int index = 0; index < gridList.size(); index++) {
            CalloutConfigVO calloutConfigVO = new CalloutConfigVO();
            Long listId = ((BigDecimal) gridList.get(index).get("LIST_ID")).longValue();
            String fileName = (String) gridList.get(index).get("FILE_NAME");
            String fileDesc = (String) gridList.get(index).get("FILE_DESC");
            BigDecimal data = (BigDecimal) gridList.get(index).get("DATA");
            BigDecimal display = (BigDecimal) gridList.get(index).get("DISPLAY_ORDER");
            calloutConfigVO.setListId(listId);
            calloutConfigVO.setFileName(fileName);
            calloutConfigVO.setFileDesc(fileDesc);
            calloutConfigVO.setDisplayOrder(display != null ? display.longValue() : 0);
            calloutConfigVO.setData(data);
            calloutConfigForm.getGridList().add(calloutConfigVO);
        }

        for (int index = 0; index < gridList1.size(); index++) {
            CalloutConfigVO calloutConfigVO = new CalloutConfigVO();
            Long listId = ((BigDecimal) gridList1.get(index).get("LIST_ID")).longValue();
            String fileName = (String) gridList1.get(index).get("FILE_NAME");
            String fileDesc = (String) gridList1.get(index).get("FILE_DESC");
            BigDecimal data = (BigDecimal) gridList1.get(index).get("DATA");
            BigDecimal channelOrgId = (BigDecimal) gridList1.get(index).get("CHANNEL_ORG");
            calloutConfigVO.setSalesChannelType(channelOrgId.longValue()); // FIXME nullPointer Exception
            calloutConfigVO.setChannelOrg(channelOrgId.longValue());
            calloutConfigVO.setListId(listId);
            calloutConfigVO.setFileName(fileName);
            calloutConfigVO.setFileDesc(fileDesc);
            calloutConfigVO.setData(data);
            calloutConfigForm.getGridList1().add(calloutConfigVO);
        }

        for (int index = 0; index < gridList2.size(); index++) {
            CalloutConfigVO calloutConfigVO = new CalloutConfigVO();
            Long listId = ((BigDecimal) gridList2.get(index).get("LIST_ID")).longValue();
            String fileName = (String) gridList2.get(index).get("FILE_NAME");
            String fileDesc = (String) gridList2.get(index).get("FILE_DESC");
            BigDecimal data = (BigDecimal) gridList2.get(index).get("DATA");
            BigDecimal channelType = (BigDecimal) gridList2.get(index).get("SALES_CHANNEL_TYPE");

            calloutConfigVO.setListId(listId);
            calloutConfigVO.setSalesChannelType(channelType.longValue());
            calloutConfigVO.setFileName(fileName);
            calloutConfigVO.setFileDesc(fileDesc);
            calloutConfigVO.setData(data);
            calloutConfigForm.getGridList2().add(calloutConfigVO);
        }

        // calloutConfigForm.setActionType(saction);
        request.setAttribute("calloutConfigForm", calloutConfigForm);
    }

}
