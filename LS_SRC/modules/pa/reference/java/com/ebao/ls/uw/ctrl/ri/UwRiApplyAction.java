package com.ebao.ls.uw.ctrl.ri;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.UserTransaction;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.foundation.module.db.hibernate.HibernateSession3;
import com.ebao.ls.cmu.pub.document.DocumentFormFile;
import com.ebao.ls.notification.extension.letter.nb.FmtUnb0110ExtensionListVO;
import com.ebao.ls.notification.extension.letter.nb.NbLetterExtensionVO;
import com.ebao.ls.notification.message.WSLetterUnit;
import com.ebao.ls.pa.nb.ctrl.helper.UwRiDocumentHelper;
import com.ebao.ls.pa.nb.ctrl.helper.UwRiHelper;
import com.ebao.ls.pa.pub.vo.PolicyVO;
import com.ebao.ls.pa.pub.vo.UwCheckListStatusVO;
import com.ebao.ls.pub.CodeCst;
import com.ebao.ls.uw.ci.vo.UwRiInsuredCIVO;
import com.ebao.ls.uw.ctrl.letter.UNBLetterAction;
import com.ebao.ls.uw.ds.UwPolicyService;
import com.ebao.ls.uw.vo.UwPolicyVO;
import com.ebao.pub.Cst;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.util.TransUtils;

import edu.emory.mathcs.backport.java.util.Arrays;

public class UwRiApplyAction extends UNBLetterAction {

  @Resource(name = UwPolicyService.BEAN_DEFAULT)
  protected UwPolicyService uwPolicyService;

  @Resource(name = com.ebao.ls.pa.pub.bs.PolicyService.BEAN_DEFAULT)
  private com.ebao.ls.pa.pub.bs.PolicyService policyDS;

  @Resource(name = UwRiHelper.BEAN_DEFAULT)
  UwRiHelper uwRiHelper;

  @Resource(name = UwRiDocumentHelper.BEAN_DEFAULT)
  UwRiDocumentHelper uwRiDocumentHelper;

  org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(this.getClass());

  private static final String ACTION_SAVE = "save";

  private static final String ACTION_PREVIEW = "preview";

  private static final String ACTION_ONLINE = "online";

  private static final String ACTION_DELETE = "delete";
  
  private static final String ACTION_QUERY = "queryOnly";

  /**
   * @param path
   * @return
   */

  @Override
  public ActionForward process(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
      throws Exception {

    UwRiApplyForm uwRiApplyForm = (UwRiApplyForm) form;

    long underwritingId = uwRiApplyForm.getUnderwriteId();
    UwPolicyVO uwPolicyVO = uwPolicyService.findUwPolicy(underwritingId);

    String subAction = uwRiApplyForm.getSubAction();

    // Action 處理
    if (StringUtils.isNotEmpty(subAction) && !ACTION_QUERY.equals(subAction)) {
      actionHandler(uwRiApplyForm, request, response, uwPolicyVO);
    }

    // 頁面初始帶值 : 預覽不重新LOAD
    if (!ACTION_PREVIEW.equals(subAction)) {
      doPageInit(uwRiApplyForm, request, response, uwPolicyVO);
      return mapping.findForward("uwRiApply");
    } else {
        return mapping.findForward(FORWARD_WINDOW_CLOSE);
    }
  }

  public void doPageInit(UwRiApplyForm uwRiApplyForm, HttpServletRequest request, HttpServletResponse response, UwPolicyVO uwPolicyVO) {

    Long policyId = uwRiApplyForm.getPolicyId();
    Long underwriteId = uwRiApplyForm.getUnderwriteId();
    PolicyVO policyVO = policyDS.retrieveById(policyId, false);
    boolean isQueryOnly = ACTION_QUERY.equals(uwRiApplyForm.getSubAction());
    
    uwRiApplyForm = new UwRiApplyForm();

    uwRiApplyForm.setPolicyId(policyId);
    uwRiApplyForm.setUnderwriteId(underwriteId);

    // 再保公司清單
    List<UwReinsurerForm> reinsurerSpecs = uwRiHelper.loadReinsurer();
    List<Map<String, Object>> reinsurerMapList = new ArrayList<Map<String, Object>>();
    for (UwReinsurerForm reinsurer : reinsurerSpecs) {

      Map<String, Object> reinsurerMap = new HashMap<String, Object>();
      reinsurerMap.put("reinsurerName", reinsurer.getReinsurerName());
      reinsurerMap.put("reinsurerId", reinsurer.getReinsurerId());
      reinsurerMap.put("contact", reinsurer.getContact());
      reinsurerMap.put("tel", uwRiDocumentHelper.mergePhone(reinsurer.getTelReg(), reinsurer.getTel(), reinsurer.getTelExt()));
      reinsurerMap.put("fax", uwRiDocumentHelper.mergePhone(reinsurer.getFaxReg(), reinsurer.getFax(), null));
      reinsurerMap.put("email", reinsurer.getEmail());
      reinsurerMapList.add(reinsurerMap);

    }

    // 被保險人資料
    List<Map<String, Object>> insuredList = uwRiHelper.loadInsuredInfos(policyVO, reinsurerSpecs);

    request.setAttribute("reinsurerList", reinsurerMapList);
    request.setAttribute("insureds", insuredList);
    request.setAttribute("isQueryOnly", isQueryOnly);
    
    UwRiApplyForm actionForm = new UwRiApplyForm();
    actionForm.setPolicyId(uwRiApplyForm.getPolicyId());
    actionForm.setUnderwriteId(uwRiApplyForm.getUnderwriteId());
    request.setAttribute(Cst.ACTION_FORM, actionForm);

  }

  public void actionHandler(UwRiApplyForm uwRiApplyForm, HttpServletRequest request, HttpServletResponse response, UwPolicyVO uwPolicyVO)
      throws Exception {

    Long policyId = uwRiApplyForm.getPolicyId();
    Long underWriteId = uwRiApplyForm.getUnderwriteId();
    PolicyVO policyVO = policyDS.retrieveById(policyId, false);

    // 操作
    String subAction = uwRiApplyForm.getSubAction();
    UwRiApplyInsuredForm insuredForm = uwRiApplyForm.getOperateInsured();

    UserTransaction ut = TransUtils.getUserTransaction();

    try {
      ut.begin();

      if (ACTION_DELETE.equals(subAction)) {
        uwRiHelper.deleteTempExtend(uwRiApplyForm.getDeleteInsured());
      } else {

        // 儲存
        if(!"0".equals(uwRiApplyForm.getIsSave())) {
          UwRiInsuredCIVO civo = uwRiHelper.convertToCIVO(insuredForm, underWriteId, policyId, subAction);
          Long extendId = uwRiHelper.saveExtendPersion(civo);
          
          String[] billCards = uwRiApplyForm.getApplyImageList();
          uwRiHelper.saveBillCardList(extendId, billCards);
          
          List<DocumentFormFile> uploadFileList = nbLetterHelper.getUploadFileList(request, uwRiApplyForm);
          uwRiHelper.saveImageList(extendId, uploadFileList);
        }

        // 發送
        if (!ACTION_SAVE.equals(subAction)) {
          List<FmtUnb0110ExtensionListVO> extsVOList = uwRiDocumentHelper.documentHandler(response, uwRiApplyForm, policyVO);
          if (ACTION_ONLINE.equals(subAction)) {
            if(extsVOList.isEmpty()) {
              request.setAttribute("isSend", "無暫存資料");
            } else {
              uwRiDocumentHelper.sendLetter(response, uwRiApplyForm, extsVOList, uwPolicyVO.getPolicyCode());
              request.setAttribute("isSend", "已發送");
            }
            // GEN Document
          } else if (ACTION_PREVIEW.equals(subAction)) {
            if(!extsVOList.isEmpty()) {
              WSLetterUnit[] units = uwRiDocumentHelper.sendPreviewLetter(response, uwRiApplyForm, extsVOList);
              super.previewLetter(request, response, UwRiDocumentHelper.TEMPLATE_ID, units);
            }
          }
        }
      }

      ut.commit();
    } catch (Exception e) {
      TransUtils.rollback(ut);
      throw e;
    }
  }

  @Override
  protected NbLetterExtensionVO getExtension(ActionForm unbLetterForm) {
    return null;
  }

}
