package com.ebao.ls.uw.ctrl.fileUpload;
        
import org.apache.struts.upload.FormFile;

import com.ebao.pub.framework.GenericForm;
import com.ebao.pub.web.pager.PagerFormImpl;

/**
 * PCR_41834 核保轄區設置作業調整及保留核保決定資料記錄
 * 
 * @author Alex Chang
 * @since 2020.12.10
 */
public class UserAreaDownUploadForm extends PagerFormImpl {
//	public class UserAreaDownUploadForm extends GenericForm {

	/**
	 * <code>serialVersionUID</code> 的註解
	 */
	private static final long serialVersionUID = 8812251132141351880L;

	private FormFile uploadFile;
	
	public FormFile getUploadFile() {
		return uploadFile;
	}
	public void setUploadFile(FormFile uploadFile) {
		this.uploadFile = uploadFile;
	}



}
