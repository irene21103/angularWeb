package com.ebao.ls.uw.ctrl.leaveUwAuth;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.sl.usermodel.Sheet;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.ls.pa.pub.auth.UwAreaLeaveAuthServiceImpl;
import com.ebao.ls.pub.auth.AuthItemCst;
import com.ebao.ls.uw.ctrl.fileUpload.UserAreaDownUploadUtil;
import com.ebao.ls.uw.ds.UwAreaService;
import com.ebao.ls.uw.ds.UwAuthLeaveService;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.framework.GenericAction;
import com.ebao.pub.framework.MultiWarning;
import com.ebao.pub.util.StringUtils;

import com.ebao.ls.escape.helper.EscapeHelper;
import com.ebao.ls.leave.service.UserLeaveManagerService;
import com.ebao.ls.uw.ds.vo.LeaveAuthEdit;
import com.ebao.ls.uw.ds.vo.UwHistoryInsureVO;
import com.ebao.pub.sys.sysmgmt.usermgr.UserDS;
import com.ebao.foundation.app.sys.sysmgmt.ds.SysMgmtDSLocator;
import com.ebao.foundation.app.sys.sysmgmt.ds.UserVO;

import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

/**
 * <p>Title: 休假管理及核保轄區設置-查詢頁面</p>
 * <p>Description: Function Description Only vegetable</p>
 * <p>Copyright: Copyright (c) 2015</p>
 * <p>Company: TGL Co., Ltd.</p>
 * <p>Create Time: Aug 13, 2015</p> 
 * @author 
 * <p>Update Time: Aug 13, 2015</p>
 * <p>Updater: Alex Cheng</p>
 * <p>Update Comments: </p>
 */
public class LeaveAuthAction extends GenericAction {

	//PCR_41834 核保轄區設置作業調整及保留核保決定資料記錄 
	private static Logger logger = Logger.getLogger(LeaveAuthAction.class);  
	private static final String DOWNLOAD_EXCEL_FILE_NAME= "T_USER_AREA_CONF.xlsx";
	//END of PCR_41834 By Alex.Chang 12.21
	
  protected static final String ACTION_SEARCH = "search";
  
  @Resource(name = UwAreaService.BEAN_DEFAULT)
  private UwAreaService uwAreaService;
  
  @Resource(name = UwAuthLeaveService.BEAN_DEFAULT)
  private UwAuthLeaveService uwAuthLeaveService;
  
  @Resource(name = UwAreaLeaveAuthServiceImpl.BEAN_DEFAULT)
  private UwAreaLeaveAuthServiceImpl uwAreaLeaveAuthService;

  @Resource(name = UserLeaveManagerService.BEAN_DEFAULT)
  private UserLeaveManagerService userLeaveManagerService;
  
	//PCR_41834 核保轄區設置作業調整及保留核保決定資料記錄 
	@Resource(name = UserAreaDownUploadUtil.BEAN_DEFAULT)
	private UserAreaDownUploadUtil userAreaDownUploadUtil;
	//END of PCR_41834 By Alex.Chang 12.28
  
  @Override
  protected MultiWarning processWarning(ActionMapping mapping,
                  ActionForm form,
                  HttpServletRequest request, HttpServletResponse response)
                  throws Exception {
   //   LeaveAuthForm leaveAuthForm = (LeaveAuthForm) form;
      MultiWarning mw = new MultiWarning();
      mw.setContinuable(false);
      return mw;
  }
  
  @Override
  public ActionForward process(ActionMapping mapping, ActionForm form, HttpServletRequest request,
          HttpServletResponse response) throws Exception {
    LeaveAuthForm leaveAuthForm = (LeaveAuthForm) form;
    String findForward = "success";

    String saction = leaveAuthForm.getActionType();
    if((leaveAuthForm.getOrgId() == null || "".equals(leaveAuthForm.getOrgId()))
        && (leaveAuthForm.getDeptId() == null || "".equals(leaveAuthForm.getDeptId()))
        && (leaveAuthForm.getEmpName() == null || "".equals(leaveAuthForm.getEmpName()))
       ){
  
      UserVO userVO = UserDS.getUserById(AppContext.getCurrentUser().getUserId());    

    
        leaveAuthForm.setOrgId(userVO.getOrganId());    
    
        leaveAuthForm.setDeptId(userVO.getDeptId());
        leaveAuthForm.setEmpName(Long.toString(AppContext.getCurrentUser().getUserId()));
   }
    boolean isEditRole = uwAreaLeaveAuthService.hasAccessRight(AuthItemCst.AUTH_ITEM_LEAVE_UW_AREA_PAGE);
    leaveAuthForm.setIsEditRole(isEditRole);
    
    //預收與覆核人員設置作業權限
    boolean isDataEntryRole= uwAreaLeaveAuthService.hasAccessRight(AuthItemCst.AUTH_ITEM_DATA_ENTRY_VREIF);
    //覆核/主管設置作業權限
    boolean isVerifManagerRole= uwAreaLeaveAuthService.hasAccessRight(AuthItemCst.AUTH_ITEM_VERIF_MANAGER);
	//PCR_41834 核保轄區設置作業調整及保留核保決定資料記錄 
    //轄區設置作業權限
    boolean isUserAreaRole= uwAreaLeaveAuthService.hasAccessRight(AuthItemCst.AUTH_ITEM_USER_AREA);
    boolean isUserAreaDownloadRole= uwAreaLeaveAuthService.hasAccessRight(AuthItemCst.AUTH_ITEM_USER_AREA_DOWNLOAD);
    boolean isUserAreaUploadRole= uwAreaLeaveAuthService.hasAccessRight(AuthItemCst.AUTH_ITEM_USER_AREA_UPLOAD);
	//END of PCR_41834 By Alex.Chang 12.21
    
    leaveAuthForm.setIsDataEntryRole(isDataEntryRole);
    leaveAuthForm.setIsVerifManagerRole(isVerifManagerRole);
    
	//PCR_41834 核保轄區設置作業調整及保留核保決定資料記錄 
    leaveAuthForm.setIsUserAreaRole(isUserAreaRole);
    leaveAuthForm.setIsUserAreaDownloadRole(isUserAreaDownloadRole);
    leaveAuthForm.setIsUserAreaUploadRole(isUserAreaUploadRole);
	//END of PCR_41834 By Alex.Chang 12.21

    LeaveAuthEdit loginEmpData = userLeaveManagerService.findDeptByEmpId(Long.toString(AppContext.getCurrentUser().getUserId()));
    Long loginDeptId = loginEmpData.getSectionDeptId() != null ? loginEmpData.getSectionDeptId(): loginEmpData.getMasterDeptId();
    if(!StringUtils.isNullOrEmpty(saction)){
      if(LeaveAuthAction.ACTION_SEARCH.equals(saction)){
        List<LeaveAuthEdit> leaveUwAuthList = null;
//        List<LeaveAuthEdit> leaveUwAuthTemp = null;
        if("1".equals(leaveAuthForm.getSearchType())){
            Long orgId = StringUtils.isNullOrEmpty(leaveAuthForm.getOrgId())? 
                            null : new Long(leaveAuthForm.getOrgId());
          Long deptId = StringUtils.isNullOrEmpty(leaveAuthForm.getDeptId())? 
                  null : new Long(leaveAuthForm.getDeptId());
//          Long sectionDeptId = StringUtils.isNullOrEmpty(leaveAuthForm.getSectionDeptId())? 
//                  null : new Long(leaveAuthForm.getSectionDeptId());
          Long empId = StringUtils.isNullOrEmpty(leaveAuthForm.getEmpName())? 
                  null : new Long(leaveAuthForm.getEmpName());
          String empCode = StringUtils.isNullOrEmpty(leaveAuthForm.getEmpId())? 
                  null : leaveAuthForm.getEmpId();
          
          leaveUwAuthList = userLeaveManagerService.searchLeaveDataByEmp(orgId,deptId, empId, empCode, isEditRole, loginDeptId);
          Long empIdtmp =0L;
          for (Iterator<LeaveAuthEdit> iter=leaveUwAuthList.listIterator(); iter.hasNext();){
      
              LeaveAuthEdit vo = new LeaveAuthEdit();
              vo = iter.next();
              
              if(vo.getEmpId() != null){
                  if(empIdtmp.equals(vo.getEmpId())){
                      iter.remove();
                    }else{
                        empIdtmp = vo.getEmpId();
                    }
              }
              
          }    
          
        }else if("2".equals(leaveAuthForm.getSearchType())){
          Long channelType = StringUtils.isNullOrEmpty(leaveAuthForm.getChannelType())? 
                  null : new Long(leaveAuthForm.getChannelType());
          Long channelId = StringUtils.isNullOrEmpty(leaveAuthForm.getChannelId())? 
                  null : new Long(leaveAuthForm.getChannelId());
          leaveUwAuthList = userLeaveManagerService.searchLeaveDataByArea(channelType, channelId, isEditRole, loginDeptId);
 
          
        }
//        if(leaveUwAuthList.size()>0){
//            leaveUwAuthTemp =leaveUwAuthList.subList(0, 1);
//        }


        request.setAttribute("leaveUwAuthList", leaveUwAuthList);
		//PCR_41834 核保轄區設置作業調整及保留核保決定資料記錄 
		}else if("download_UserArea".equals(saction)){
			String fileName = DOWNLOAD_EXCEL_FILE_NAME;
			OutputStream out = null; 
			try {
				response.setHeader("Content-disposition", "attachment;filename=\"" + fileName + "\"");
				response.setContentType("application/msexcel");

				out= userAreaDownUploadUtil.genUserAreaExcelData(response.getOutputStream() );	
			} catch (Exception e) {
				throw new ServletException("Exception in Excel Sample Servlet", e);
			} finally {
				if (out != null)
					out.close();
			}
		//END of PCR_41834 By Alex.Chang 12.09
        }
    }
    
    return mapping.findForward(findForward);
  }

}
