package com.ebao.ls.uw.ctrl;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.ls.pa.nb.util.NumericUtils;
import com.ebao.ls.pa.pub.vo.UwCheckListStatusVO;
import com.ebao.ls.pa.pub.vo.UwPatchImageVO;
import com.ebao.ls.uw.data.TUwPatchImageDelegate;
import com.ebao.ls.uw.ds.UwCheckListStatusService;
import com.ebao.ls.uw.ds.UwPatchImageService;
import com.ebao.pub.framework.GenericAction;

public class UwPatchImageAndChecklistStatusAction extends GenericAction {
	// prevent initialization
	@Resource(name = TUwPatchImageDelegate.BEAN_DEFAULT)
	private TUwPatchImageDelegate TUwPatchImageDao;

	@Resource(name = UwPatchImageService.BEAN_DEFAULT)
	protected UwPatchImageService uwPatchImageService;

	@Resource(name = UwCheckListStatusService.BEAN_DEFAULT)
	protected UwCheckListStatusService uwCheckListStatusService;

	private String actionType = "";

	private UwPatchImageAndChecklistStatusAction() {
	}

	/**
	 * @param path
	 * @return
	 */

	@Override
	public ActionForward process(ActionMapping mapping, ActionForm form,
					HttpServletRequest request, HttpServletResponse response)
					throws Exception {
		// UwRiFacExtendVO uwRiFacExtendVO = new UwRiFacExtendVO();
		UwPatchImageVO uwPatchImageVO = new UwPatchImageVO();
		UwCheckListStatusVO uwCheckListStatusVO = new UwCheckListStatusVO();
		UwPatchImageAndCheckListStatusForm uwPatchAndListStatus = (UwPatchImageAndCheckListStatusForm) form;

		if (actionType.equals("save")) {

			// set Patch Image VO
			uwPatchImageVO.setListId(uwPatchAndListStatus.getPatchListId());
			uwPatchImageVO.setUploadDate(uwPatchAndListStatus.getPatchUploadDate());
			uwPatchImageVO.setImageName(uwPatchAndListStatus.getPatchImageName());
			uwPatchImageVO.setConfirmComplete(uwPatchAndListStatus.getPatchConfirmComplete());
			// set Check List Status VO
			uwCheckListStatusVO.setListId(uwPatchAndListStatus.getCheckListId());
			uwCheckListStatusVO.setListType(NumericUtils.parseLong(uwPatchAndListStatus.getCheckCheckListType()));
			uwCheckListStatusVO.setClientName(uwPatchAndListStatus.getCheckTargetClientName());
			uwCheckListStatusVO.setApplyDate(uwPatchAndListStatus.getCheckUwApplyDate());
			uwCheckListStatusVO.setConfirmComplete(uwPatchAndListStatus.getCheckConfirmComplete());

			uwPatchImageService.save(uwPatchImageVO);
			uwCheckListStatusService.save(uwCheckListStatusVO);
			
		}
		uwPatchImageService.load(1);
		uwCheckListStatusService.load(1);


		return mapping.findForward("uwPatchImageAndCheckListStatus");
	}
}
