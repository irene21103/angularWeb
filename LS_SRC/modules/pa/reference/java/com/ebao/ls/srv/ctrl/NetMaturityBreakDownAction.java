package com.ebao.ls.srv.ctrl;

import java.math.BigDecimal;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.ls.pa.pub.ref.vo.prd.ProductVO;
import com.ebao.ls.pa.pub.service.PolicyService;
import com.ebao.ls.pa.pub.vo.CoverageVO;
import com.ebao.ls.pa.pub.vo.PolicyVO;
import com.ebao.ls.pub.CodeCst;
import com.ebao.ls.srv.ds.SurvivalRepayService;
import com.ebao.ls.srv.ds.SurvivalRepaymentTool;
import com.ebao.ls.srv.vo.PayDueVO;
import com.ebao.pub.annotation.PAPubAPIUpdate;
import com.ebao.pub.annotation.PrdAPIUpdate;
import com.ebao.pub.framework.GenericAction;

/**
 * <p>
 * Title: NetMaturityBreakDownAction
 * </p>
 * <p>
 * Description: This class NetMaturityBreakDown 's action class.
 * </p>
 * <p>
 * Copyright: Copyright (c) 2002
 * </p>
 * <p>
 * Company: eBaoTech
 * </p>
 * <p>
 * Create Time: 2005/04/11
 * </p>
 * 
 * @author simen.li
 * @version 1.0
 */
public class NetMaturityBreakDownAction extends GenericAction {
  @Resource(name = SurvivalRepaymentTool.BEAN_DEFAULT)
  private SurvivalRepaymentTool survivalRepaymentTool;

  /**
   * <p>
   * Description: process some action
   * </p>
   * 
   * @param mapping
   * @param form,
   * @param request
   * @param response
   * @return ActionForward
   * @throws Exception <P>
   *           Create time: 2005/4/11
   *           </P>
   */
  @PrdAPIUpdate
  @PAPubAPIUpdate("for survivalRepaymentTool")
  public ActionForward process(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse reponse) throws Exception {
    NetMaturityBreakDownForm form1 = (NetMaturityBreakDownForm) form;

    Long itemId = (Long) request.getAttribute("itemId");
    if (null == itemId) {
      itemId = Long.valueOf(Long.parseLong(request.getParameter("itemId")));
    }
    PayDueVO payDueVO = survivalRepayDS.getLastPayDueByItemIdAndLiabId(itemId,
        Long.valueOf(CodeCst.LIABILITY__MATURITY));
    form1.setPayDue(payDueVO);

    BigDecimal netAmount = survivalRepayDS.getNetMaturityAmount(payDueVO);
    form1.setNetMB(netAmount);
    PolicyVO policyVO = survivalRepaymentTool.getPolicyByPolicyId(payDueVO
        .getPolicyId());
    form1.setPolicyNumber(policyVO.getPolicyNumber());
    CoverageVO policyProductVO = survivalRepaymentTool
        .getCoverageByItemId(payDueVO.getItemId());
    ProductVO basicInfoVO = survivalRepaymentTool
        .getBasicProductLifeByProductId(policyProductVO.getProductId(),policyService.getActiveVersionDate(policyProductVO));
    form1.setProductCode(basicInfoVO.getInternalId());
    return mapping.findForward("netMaturityBreakDown");
  }

  @Resource(name = SurvivalRepayService.BEAN_DEFAULT)
  private SurvivalRepayService survivalRepayDS;

  @Resource(name = PolicyService.BEAN_DEFAULT)
  PolicyService policyService;
}
