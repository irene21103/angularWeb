package com.ebao.ls.beneOwner.ctrl;

import java.util.List;

import com.ebao.ls.pty.vo.CompanyCustomerVO;
import com.ebao.ls.pty.vo.CustomerVO;
import com.ebao.pub.web.pager.PagerFormImpl;

public class BeneOwnerCheckPartyForm extends PagerFormImpl {

	private List<CustomerVO> customerVOList;

	private List<CompanyCustomerVO> companyCustomerVOList;

	public List<CustomerVO> getCustomerVOList() {
		return customerVOList;
	}

	public void setCustomerVOList(List<CustomerVO> customerVOList) {
		this.customerVOList = customerVOList;
	}

	public List<CompanyCustomerVO> getCompanyCustomerVOList() {
		return companyCustomerVOList;
	}

	public void setCompanyCustomerVOList(List<CompanyCustomerVO> companyCustomerVOList) {
		this.companyCustomerVOList = companyCustomerVOList;
	}

}
