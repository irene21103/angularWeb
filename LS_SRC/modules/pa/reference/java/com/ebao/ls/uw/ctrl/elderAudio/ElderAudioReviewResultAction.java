package com.ebao.ls.uw.ctrl.elderAudio;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.foundation.core.logging.Log;
import com.ebao.ls.escape.helper.EscapeHelper;
import com.ebao.ls.pa.pub.bs.PolicyService;
import com.ebao.ls.pa.pub.vo.PolicyVO;
import com.ebao.ls.uw.ctrl.helper.ElderAudioHelper;
import com.ebao.ls.uw.ctrl.info.PolicyForm;
import com.ebao.ls.uw.ds.UwElderAudioService;
import com.ebao.pub.framework.GenericAction;

public class ElderAudioReviewResultAction extends GenericAction {

	private final Log logger = Log.getLogger(ElderAudioReviewResultAction.class);

	public static final String BEAN_DEFAULT = "/uw/elderAudioReviewResult";

	private String forward = "display";

	@Resource(name = PolicyService.BEAN_DEFAULT)
	private PolicyService policyService;

	@Resource(name = ElderAudioHelper.BEAN_DEFAULT)
	private ElderAudioHelper elderAudioHelper;

	@Resource(name = UwElderAudioService.BEAN_DEFAULT)
	private UwElderAudioService uwElderAudioService;

	@Override
	public ActionForward process(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		PolicyForm aForm = (PolicyForm) form;
		logger.debug("process().PolicyForm >> \n" + ToStringBuilder.reflectionToString(aForm, ToStringStyle.MULTI_LINE_STYLE));

		HttpSession session = request.getSession();

		// 取得本保單會辦對象清單
		Long policyId = aForm.getPolicyId();
		PolicyVO policyVO = policyService.load(policyId);
		Date applyDate = policyVO.getApplyDate();
		List<ElderAudioObject> elderAudioObjects = elderAudioHelper.getElderAudioObjects(policyId, applyDate);
		EscapeHelper.escapeHtml(session).setAttribute(ElderAudioHelper.AUDIO_OBJECTS_ATTR, elderAudioObjects); /** Session Fixation */

		List<Map<String, Object>> reviewTaskList = uwElderAudioService.findAllElderAudioReviewReplyByPolicyID(policyId);
		request.setAttribute(ElderAudioHelper.REVIEW_TASK_LIST_ATTR, reviewTaskList);

		return mapping.findForward(forward);
	}

}
