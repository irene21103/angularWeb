package com.ebao.ls.beneOwner.ctrl;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.foundation.core.logging.Log;
import com.ebao.ls.pty.ci.CustomerCI;
import com.ebao.ls.pty.ds.vo.CompanySearchConditionVO;
import com.ebao.ls.pty.vo.CompanyCustomerVO;
import com.ebao.pub.framework.GenericAction;

public class BeneOwnerOpenAction extends GenericAction {

	private final Log log = Log.getLogger(BeneOwnerOpenAction.class);

	@Resource(name = CustomerCI.BEAN_DEFAULT)
	private CustomerCI customerCI;

	@Override
	public ActionForward process(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		BeneOwnerOpenForm boForm = (BeneOwnerOpenForm) form;
		// 頁面輸入-統一編(證)號
		String registerCode = boForm.getRegisterCodeSearch();
		if (StringUtils.isNotBlank(registerCode)) {
			CompanySearchConditionVO conditionVo = new CompanySearchConditionVO();
			conditionVo.setRegisterCode(registerCode);
			List<CompanyCustomerVO> companyCustomerList = customerCI.getCompanyList(conditionVo);
			if (CollectionUtils.isNotEmpty(companyCustomerList)) {
				boForm.setCompanyCustomerVOList(companyCustomerList);
			}
		}
		return mapping.findForward("success");
	}
}
