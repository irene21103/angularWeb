package com.ebao.ls.uw.ctrl.qm;

import com.ebao.ls.uw.vo.CSQualityManageListVO;
import com.ebao.ls.uw.vo.QualityManageListVO;
import com.ebao.pub.web.pager.PagerFormImpl;

public class UwQualityManageForm extends PagerFormImpl {
    /**
     * <code>serialVersionUID</code> 的註解
     */
    private static final long serialVersionUID = -3513781458914893374L;
    
    private QualityManageListVO qualityManageListVO = new QualityManageListVO();
    private CSQualityManageListVO csQualityManageListVO = new CSQualityManageListVO();
    private String actionType;
    private String condQualityManageType;
    private String condCertiCode;
    private String condRegisterCode;
    private String condName;
    private String saveRtnMsg;
    //業務員品質管理－新增權限
    private Boolean isAddButtonRole;
    //業務員品質管理－修改權限
    private Boolean isEditButtonRole;
    //業務員品質管理－刪除權限
    private Boolean isDeleteButtonRole;
    //業務員品質管理－查詢權限
    private Boolean isSearchButtonRole;
    
    public QualityManageListVO getQualityManageListVO() {
        return qualityManageListVO;
    }

    public void setQualityManageListVO(QualityManageListVO qualityManageListVO) {
        this.qualityManageListVO = qualityManageListVO;
    }
    
    public CSQualityManageListVO getCsQualityManageListVO() {
		return csQualityManageListVO;
	}

	public void setCsQualityManageListVO(CSQualityManageListVO csQualityManageListVO) {
		this.csQualityManageListVO = csQualityManageListVO;
	}

	public String getActionType() {
        return actionType;
    }

    public void setActionType(String actionType) {
        this.actionType = actionType;
    }
    
    public String getQualityManageType() {
        return qualityManageListVO.getQualityManageType();
    }

    public void setQualityManageType(String qualityManageType) {
        qualityManageListVO.setQualityManageType(qualityManageType);
    }

    public String getCause() {
        return qualityManageListVO.getCause();
    }

    public void setCause(String cause) {
        qualityManageListVO.setCause(cause);
    }

    public String getCondQualityManageType() {
        return condQualityManageType;
    }

    public void setCondQualityManageType(String condQualityManageType) {
        this.condQualityManageType = condQualityManageType;
    }

    public String getCondCertiCode() {
        return condCertiCode;
    }

    public void setCondCertiCode(String condCertiCode) {
        this.condCertiCode = condCertiCode;
    }

    public String getCondRegisterCode() {
        return condRegisterCode;
    }

    public void setCondRegisterCode(String condRegisterCode) {
        this.condRegisterCode = condRegisterCode;
    }

    public String getCondName() {
        return condName;
    }

    public void setCondName(String condName) {
        this.condName = condName;
    }

	public String getSaveRtnMsg() {
		return saveRtnMsg;
	}

	public void setSaveRtnMsg(String saveRtnMsg) {
		this.saveRtnMsg = saveRtnMsg;
	}

	public Boolean getIsAddButtonRole() {
		return isAddButtonRole;
	}

	public void setIsAddButtonRole(Boolean isAddButtonRole) {
		this.isAddButtonRole = isAddButtonRole;
	}

	public Boolean getIsEditButtonRole() {
		return isEditButtonRole;
	}

	public void setIsEditButtonRole(Boolean isEditButtonRole) {
		this.isEditButtonRole = isEditButtonRole;
	}

	public Boolean getIsDeleteButtonRole() {
		return isDeleteButtonRole;
	}

	public void setIsDeleteButtonRole(Boolean isDeleteButtonRole) {
		this.isDeleteButtonRole = isDeleteButtonRole;
	}

	public Boolean getIsSearchButtonRole() {
		return isSearchButtonRole;
	}

	public void setIsSearchButtonRole(Boolean isSearchButtonRole) {
		this.isSearchButtonRole = isSearchButtonRole;
	}
	
}
