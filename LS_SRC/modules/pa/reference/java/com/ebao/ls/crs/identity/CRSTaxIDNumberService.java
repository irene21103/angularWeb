package com.ebao.ls.crs.identity;

import com.ebao.ls.crs.vo.CRSTaxIDNumberVO;
import com.ebao.ls.foundation.service.GenericService;

public interface CRSTaxIDNumberService extends GenericService<CRSTaxIDNumberVO>{
	public final static String BEAN_DEFAULT = "crsTaxIDNumberService";
}
