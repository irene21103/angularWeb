package com.ebao.ls.uw.ctrl.pub;

import java.math.BigDecimal;

import com.ebao.pub.framework.GenericForm;

/**
 * 
 * <p>Title:Underwriting</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: eBaoTech Corporation</p>
 * @author Walter Huang
 * @since Jun 27, 2005
 * @version 1.0
 */
public class UwRestrictionForm extends GenericForm {

  private java.lang.Long underwriteId = null;

  private java.lang.Long itemId = null;

  private java.lang.Integer productId = null;

  private java.lang.String chargePeriod = "";

  private java.lang.Integer chargeYear = null;

  private java.lang.String coveragePeriod = "";

  private java.lang.Integer coverageYear = null;

  private java.math.BigDecimal amount = null;

  private java.math.BigDecimal reducedAmount = null;

  private java.lang.Integer uwChargeYear = null;

  private java.lang.Integer uwCoverageYear = null;

  private java.lang.String uwChargePeriod = "";

  private java.lang.String uwCoveragePeriod = "";

  private java.math.BigDecimal unit = null;

  private java.math.BigDecimal reducedUnit = null;

  private java.lang.Integer waitPeriod = null;

  private java.lang.Long uwListId = null;

  private String benefitType;

  private String applyCode;

  private String policyCode;

  private String maFactor;

  private String msFactor;

  private BigDecimal uwsafactor;

  private boolean waitPeriodDiff;
  
  private String reducedLevel;
  
  private String benefitLevel;

  public boolean isWaitPeriodDiff() {
    return waitPeriodDiff;
  }

  public void setWaitPeriodDiff(boolean waitPeriodDiff) {
    this.waitPeriodDiff = waitPeriodDiff;
  }

  /** nullable persistent field */
  private BigDecimal uwmsfactor;

  public BigDecimal getUwmsfactor() {
    return uwmsfactor;
  }

  public void setUwmsfactor(BigDecimal uwmsfactor) {
    this.uwmsfactor = uwmsfactor;
  }

  public BigDecimal getUwsafactor() {
    return uwsafactor;
  }

  public void setUwsafactor(BigDecimal uwsafactor) {
    this.uwsafactor = uwsafactor;
  }

  /**
   * returns the value of the underwriteId
   * 
   * @return the underwriteId
   */
  public java.lang.Long getUnderwriteId() {
    return underwriteId;
  }

  /**
   * sets the value of the underwriteId
   * 
   * @param underwriteId
   *          the underwriteId
   */
  public void setUnderwriteId(java.lang.Long underwriteId) {
    this.underwriteId = underwriteId;
  }

  /**
   * returns the value of the itemId
   * 
   * @return the itemId
   */
  public java.lang.Long getItemId() {
    return itemId;
  }

  /**
   * sets the value of the itemId
   * 
   * @param itemId
   *          the itemId
   */
  public void setItemId(java.lang.Long itemId) {
    this.itemId = itemId;
  }

  /**
   * returns the value of the productId
   * 
   * @return the productId
   */
  public java.lang.Integer getProductId() {
    return productId;
  }

  /**
   * sets the value of the productId
   * 
   * @param productId
   *          the productId
   */
  public void setProductId(java.lang.Integer productId) {
    this.productId = productId;
  }

  /**
   * returns the value of the chargePeriod
   * 
   * @return the chargePeriod
   */
  public java.lang.String getChargePeriod() {
    return chargePeriod;
  }

  /**
   * sets the value of the chargePeriod
   * 
   * @param chargePeriod
   *          the chargePeriod
   */
  public void setChargePeriod(java.lang.String chargePeriod) {
    this.chargePeriod = chargePeriod;
  }

  /**
   * returns the value of the chargeYear
   * 
   * @return the chargeYear
   */
  public java.lang.Integer getChargeYear() {
    return chargeYear;
  }

  /**
   * sets the value of the chargeYear
   * 
   * @param chargeYear
   *          the chargeYear
   */
  public void setChargeYear(java.lang.Integer chargeYear) {
    this.chargeYear = chargeYear;
  }

  /**
   * returns the value of the coveragePeriod
   * 
   * @return the coveragePeriod
   */
  public java.lang.String getCoveragePeriod() {
    return coveragePeriod;
  }

  /**
   * sets the value of the coveragePeriod
   * 
   * @param coveragePeriod
   *          the coveragePeriod
   */
  public void setCoveragePeriod(java.lang.String coveragePeriod) {
    this.coveragePeriod = coveragePeriod;
  }

  /**
   * returns the value of the coverageYear
   * 
   * @return the coverageYear
   */
  public java.lang.Integer getCoverageYear() {
    return coverageYear;
  }

  /**
   * sets the value of the coverageYear
   * 
   * @param coverageYear
   *          the coverageYear
   */
  public void setCoverageYear(java.lang.Integer coverageYear) {
    this.coverageYear = coverageYear;
  }

  /**
   * returns the value of the amount
   * 
   * @return the amount
   */
  public java.math.BigDecimal getAmount() {
    return amount;
  }

  /**
   * sets the value of the amount
   * 
   * @param amount
   *          the amount
   */
  public void setAmount(java.math.BigDecimal amount) {
    this.amount = amount;
  }

  /**
   * returns the value of the reducedAmount
   * 
   * @return the reducedAmount
   */
  public java.math.BigDecimal getReducedAmount() {
    return reducedAmount;
  }

  /**
   * sets the value of the reducedAmount
   * 
   * @param reducedAmount
   *          the reducedAmount
   */
  public void setReducedAmount(java.math.BigDecimal reducedAmount) {
    this.reducedAmount = reducedAmount;
  }

  /**
   * returns the value of the uwChargeYear
   * 
   * @return the uwChargeYear
   */
  public java.lang.Integer getUwChargeYear() {
    return uwChargeYear;
  }

  /**
   * sets the value of the uwChargeYear
   * 
   * @param uwChargeYear
   *          the uwChargeYear
   */
  public void setUwChargeYear(java.lang.Integer uwChargeYear) {
    this.uwChargeYear = uwChargeYear;
  }

  /**
   * returns the value of the uwCoverageYear
   * 
   * @return the uwCoverageYear
   */
  public java.lang.Integer getUwCoverageYear() {
    return uwCoverageYear;
  }

  /**
   * sets the value of the uwCoverageYear
   * 
   * @param uwCoverageYear
   *          the uwCoverageYear
   */
  public void setUwCoverageYear(java.lang.Integer uwCoverageYear) {
    this.uwCoverageYear = uwCoverageYear;
  }

  /**
   * returns the value of the uwChargePeriod
   * 
   * @return the uwChargePeriod
   */
  public java.lang.String getUwChargePeriod() {
    return uwChargePeriod;
  }

  /**
   * sets the value of the uwChargePeriod
   * 
   * @param uwChargePeriod
   *          the uwChargePeriod
   */
  public void setUwChargePeriod(java.lang.String uwChargePeriod) {
    this.uwChargePeriod = uwChargePeriod;
  }

  /**
   * returns the value of the uwCoveragePeriod
   * 
   * @return the uwCoveragePeriod
   */
  public java.lang.String getUwCoveragePeriod() {
    return uwCoveragePeriod;
  }

  /**
   * sets the value of the uwCoveragePeriod
   * 
   * @param uwCoveragePeriod
   *          the uwCoveragePeriod
   */
  public void setUwCoveragePeriod(java.lang.String uwCoveragePeriod) {
    this.uwCoveragePeriod = uwCoveragePeriod;
  }

  /**
   * returns the value of the unit
   * 
   * @return the unit
   */
  public java.math.BigDecimal getUnit() {
    return unit;
  }

  /**
   * sets the value of the unit
   * 
   * @param unit
   *          the unit
   */
  public void setUnit(java.math.BigDecimal unit) {
    this.unit = unit;
  }

  /**
   * returns the value of the reducedUnit
   * 
   * @return the reducedUnit
   */
  public java.math.BigDecimal getReducedUnit() {
    return reducedUnit;
  }

  /**
   * sets the value of the reducedUnit
   * 
   * @param reducedUnit
   *          the reducedUnit
   */
  public void setReducedUnit(java.math.BigDecimal reducedUnit) {
    this.reducedUnit = reducedUnit;
  }

  /**
   * returns the value of the waitPeriod
   * 
   * @return the waitPeriod
   */
  public java.lang.Integer getWaitPeriod() {
    return waitPeriod;
  }

  /**
   * sets the value of the waitPeriod
   * 
   * @param waitPeriod
   *          the waitPeriod
   */
  public void setWaitPeriod(java.lang.Integer waitPeriod) {
    this.waitPeriod = waitPeriod;
  }

  /**
   * returns the value of the uwListId
   * 
   * @return the uwListId
   */
  public java.lang.Long getUwListId() {
    return uwListId;
  }

  /**
   * sets the value of the uwListId
   * 
   * @param uwListId
   *          the uwListId
   */
  public void setUwListId(java.lang.Long uwListId) {
    this.uwListId = uwListId;
  }

  public String getApplyCode() {
    return applyCode;
  }

  public void setApplyCode(String applyCode) {
    this.applyCode = applyCode;
  }

  public String getPolicyCode() {
    return policyCode;
  }

  public void setPolicyCode(String policyCode) {
    this.policyCode = policyCode;
  }

  public String getMaFactor() {
    return maFactor;
  }

  public void setMaFactor(String maFactor) {
    this.maFactor = maFactor;
  }

  public String getMsFactor() {
    return msFactor;
  }

  public void setMsFactor(String msFactor) {
    this.msFactor = msFactor;
  }

  // End Additional Business Methods

  /**
   * @return Returns the benefitType.
   */
  public String getBenefitType() {
    return benefitType;
  }

  /**
   * @param benefitType
   *          The benefitType to set.
   */
  public void setBenefitType(String benefitType) {
    this.benefitType = benefitType;
  }

  public String getReducedLevel() {
    return reducedLevel;
  }

  public void setReducedLevel(String reducedLevel) {
    this.reducedLevel = reducedLevel;
  }

  public String getBenefitLevel() {
    return benefitLevel;
  }

  public void setBenefitLevel(String benefitLevel) {
    this.benefitLevel = benefitLevel;
  }
}
