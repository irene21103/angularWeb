package com.ebao.ls.liaRoc.ci;

import java.sql.CallableStatement;
import java.sql.Types;


import java.util.Date;

import com.ebao.pub.framework.AppContext;
import com.ebao.pub.framework.ExceptionFactory;
import com.ebao.pub.framework.GenericException;
import com.ebao.pub.util.DBean;
import com.ebao.pub.util.json.DateUtils;

public class LiaRocFileCIImpl implements LiaRocFileCI {

    @Override
    public String getFileName(String fileType) throws GenericException {
        return getFileName(fileType, AppContext.getCurrentUserLocalTime());
    }

	@Override
	public String getFileName(String fileType, Date sysDate) throws GenericException {
		//Date today = DateUtils.truncateDay(AppContext.getCurrentUserLocalTime());
        //int maxLen = 4;
        //int start = 1;
        //int add = 2;
        DBean db = new DBean();
        CallableStatement stmt = null;
        try {
          db.connect();
          stmt = db.getConnection().prepareCall(
              "{call PKG_LS_PM_PUB.p_get_liaRoc_fileName(?,?,?)} ");
          stmt.setString(1, fileType);
          stmt.setDate(2, DateUtils.getSqlDateByUtilDate(sysDate));
          stmt.registerOutParameter(3, Types.VARCHAR);
          stmt.executeUpdate();
          return stmt.getString(3);
        } catch (Exception ex) {
          throw ExceptionFactory.parse(ex);
        } finally {
          DBean.closeAll(null, stmt, db);
        }       
	}

}
