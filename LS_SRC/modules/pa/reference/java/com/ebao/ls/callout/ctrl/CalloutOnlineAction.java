package com.ebao.ls.callout.ctrl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.UserTransaction;

import net.sf.json.JSONArray;
import net.sf.json.JSONSerializer;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ebao.foundation.common.lang.exception.ExceptionUtils;
import com.ebao.foundation.commons.exceptions.AppException;
import com.ebao.foundation.core.logging.Log;
import com.ebao.foundation.module.db.Trans;
import com.ebao.foundation.module.util.beanutils.BeanUtils;
import com.ebao.ls.callout.bs.CalloutTransService;
import com.ebao.ls.callout.ci.CalloutServiceCI;
import com.ebao.ls.callout.vo.CalloutAskVO;
import com.ebao.ls.callout.vo.CalloutOnlineVO;
import com.ebao.ls.callout.vo.CalloutProdUnfavorFactorVO;
import com.ebao.ls.callout.vo.CalloutQuestionnaireVO;
import com.ebao.ls.callout.vo.CalloutTransVO;
import com.ebao.ls.callout.vo.CalloutUnfavorFactorVO;
import com.ebao.ls.callout.vo.EVoiceResultVO;
import com.ebao.ls.cs.calloutonline.service.CalloutAskSwitchService;
import com.ebao.ls.cs.calloutonline.vo.CallOutOnlineQuesItemVo;
import com.ebao.ls.cs.commonflow.ds.AlterationItemService;
import com.ebao.ls.cs.commonflow.ds.ApplicationService;
import com.ebao.ls.cs.commonflow.ds.CSPremiumService;
import com.ebao.ls.cs.commonflow.ds.rule.vo.PosRuleIlpRiskHistVO;
import com.ebao.ls.cs.commonflow.vo.AlterationItemVO;
import com.ebao.ls.cs.commonflow.vo.ApplicationVO;
import com.ebao.ls.escape.helper.EscapeHelper;
import com.ebao.ls.pa.pub.bs.PosIlpRiskService;
import com.ebao.ls.pa.pub.bs.coverage.CoverageService;
import com.ebao.ls.pa.pub.ci.PolicyCI;
import com.ebao.ls.pa.pub.service.PolicyService;
import com.ebao.ls.pa.pub.vo.CoverageVO;
import com.ebao.ls.product.service.LifeProductService;
import com.ebao.ls.pty.ci.EmployeeCI;
import com.ebao.ls.pty.ds.DeptService;
import com.ebao.ls.pty.service.PartyService;
import com.ebao.ls.pub.CodeCst;
import com.ebao.ls.pub.cst.YesNo;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.framework.ExceptionFactory;
import com.ebao.pub.framework.GenericAction;
import com.ebao.pub.i18n.lang.StringResource;
import com.ebao.pub.para.Para;
import com.ebao.pub.security.AppUser;
import com.ebao.pub.util.DateUtils;
import com.ebao.pub.util.StringUtils;
import com.ebao.pub.util.TransUtils;
import com.tgl.tools.ListTool;

@Controller("calloutOnlineAction")
public class CalloutOnlineAction extends GenericAction {
	private static final String CALLOUT_REASON_ACK_TASK_SEC_3 = "3";

	@Resource(name = CalloutServiceCI.BEAN_DEFAULT)
	private CalloutServiceCI calloutServiceCI;
    
    @Resource(name = CalloutTransService.BEAN_DEFAULT)
    private CalloutTransService calloutTransService;

    @Resource(name = DeptService.BEAN_DEFAULT)
    private DeptService deptDS;

    @Resource(name = PolicyService.BEAN_DEFAULT)
    private PolicyService policyService;

    @Resource(name = PartyService.BEAN_DEFAULT)
    private PartyService partyService;

    @Resource(name = EmployeeCI.BEAN_DEFAULT)
    private EmployeeCI employeeService;
    
    @Resource(name = PolicyCI.BEAN_DEFAULT)
    private PolicyCI policyCI;
    
	@Resource(name = CoverageService.BEAN_DEFAULT)
	private CoverageService coverageService;

	@Resource(name = LifeProductService.BEAN_DEFAULT)
	private LifeProductService lifeProductService;
	
	@Resource(name = CalloutAskSwitchService.BEAN_DEFAULT)
	private CalloutAskSwitchService calloutAskSwitchService;	

	@Resource(name = PosIlpRiskService.BEAN_DEFAULT)
	private PosIlpRiskService posIlpRiskService;

	@Resource(name = AlterationItemService.BEAN_DEFAULT)
    private AlterationItemService alterationItemService;
	
	@Resource(name = ApplicationService.BEAN_DEFAULT)
	private ApplicationService applicationDS;

	@Resource(name = CSPremiumService.BEAN_DEFAULT)
	private CSPremiumService csPremiumService;
	
    private final Log log = Log.getLogger(CalloutOnlineAction.class);

    private void syso(Object msg){
        log.info("[SYSOUT][CMN-TPI-007]" + msg);
    }
    
    private void syso(String layout, Object... msg){
        syso(String.format(layout, msg));
    }
    
    @Override
	public ActionForward process(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
        
        CalloutOnlineForm cForm = (CalloutOnlineForm) form;
		HashMap<String,String> hm = new HashMap<String,String>();

        // 頁面設置
        Boolean isSendEVoice = initPageSetting(cForm, request);
		String isDetail = null;
		String agentId = request.getParameter("agentId");
		String sendType = request.getParameter("sendType");
		String sendPerson = request.getParameter("sendPerson");
		String agentEqualsSend = "";
		if(null != agentId && null != sendPerson){
			if(!StringUtils.isEmpty(agentId) && !StringUtils.isEmpty(sendPerson)){
				if(agentId.trim().equals(sendPerson.trim())){
					agentEqualsSend = "true";
				}
			}
		}

		// 二審啟動新增電訪
		String isFromAckTaskSec = request.getParameter("isFromAckTaskSec");
		// 核保規則校驗啟動電訪
		String isFromUWIssues = request.getParameter("isFromUWIssues");
		 if ("true".equals(isFromAckTaskSec)) {
			request.setAttribute("isFromAckTaskSec", CodeCst.YES_NO__YES);
		} else if ("true".equals(isFromUWIssues)) {
			request.setAttribute("isFromUWIssues", CodeCst.YES_NO__YES);
		}

        switch (cForm.getAction()) {
            case SAVE:
                request.setAttribute("isSave", true);
                save(cForm, request, isSendEVoice);

                // 刷新Form
                CalloutOnlineForm nForm = new CalloutOnlineForm();
                nForm.setActionType(cForm.getActionType());
                nForm.setCalloutReason(cForm.getCalloutReason());
                BeanUtils.copyProperties(cForm, nForm);
                //頁面設置
                initPageSetting(cForm, request);
                break;
            case LOAD:
                request.setAttribute("readOnly", true);
                break;
            default:
				// 綜合查詢 - NB查電訪原因明細
				isDetail = request.getParameter("isDetail");
				if ("true".equals(isDetail)) {
					StringBuilder sb = new StringBuilder();
					String reasonItemStr = "";

					String calloutNum = request.getParameter("calloutNum");
					List<CalloutQuestionnaireVO> quesList = calloutTransService
									.getCalloutQuestionnaireByCalloutNum(calloutNum);

					for (CalloutQuestionnaireVO vo : quesList) {
						sb.append(vo.getReasonItem()).append(",");
						if (vo.getRemarks() != null && vo.getRemarks().length() > 0) {
							hm.put(vo.getReasonItem() + "_remark", vo.getRemarks());
						}
					}
					if (sb.length() > 0) {
						reasonItemStr = sb.toString();
						reasonItemStr = reasonItemStr.substring(0,
										reasonItemStr.length() - 1);
					}
					request.setAttribute("reasonItemStr", reasonItemStr);

					// PCR 448703 遠距投保3.0 - 顯示會辦人員於「新增電訪任務頁面」>「輸入電訪對象」區塊的「備註」欄位輸入的資料
					CalloutTransVO calloutTransVO = calloutTransService.findCalloutTransVOByCalloutNum(calloutNum);
					request.setAttribute("calloutTransVO", calloutTransVO);
				}
                break;
        }
        
        getAndSetCalloutVOList(request, cForm, isSendEVoice);
        setCallOutItems(request, cForm);
        
        /*
         * PCR-460064
         * 若來自這些頁面，則採用原CATEGORY方式判斷要走POS或UNB
         * 1.綜合查詢 - NB查電訪原因明細:isDetail=true
         * 2.二審啟動新增電訪:isFromAckTaskSec=true
         * 3.核保規則校驗啟動電訪:isFromUWIssues=true
         * 
         * 若來自選單，則由選單forward=pos判斷要走POS或UNB
         * POS:forward=pos
         * UNB:forward=null
         */
		if ("true".equals(isDetail) || "true".equals(isFromAckTaskSec)
				|| "true".equals(isFromUWIssues)) {
			/*
			 * 原CATEGORY方式判斷要走POS或UNB
			 */
			// 355468 NB及綜查頁面獨立出來
			if ("true".equals(isDetail)
					|| CodeCst.BIZ_CATEGORY_UNB == partyService
							.getBizCategory(AppContext.getCurrentUser()
									.getUserId())) {
				request.setAttribute("reasonMapping",
						org.apache.commons.lang.StringUtils.join(
								calloutServiceCI
										.getCalloutReasonDisplayMapping()
										.toArray(), ","));
				request.setAttribute("calloutReasons",
						calloutServiceCI.findCallReasonsWithExceptionItems());
				request.setAttribute("remarks", hm);
				request.setAttribute("unbOtherColumns",
						calloutServiceCI.findRemarkItems());

				if ("true".equals(isDetail)) {
					return mapping.findForward("detail"); // 綜合查詢-明細
				} else {
					return mapping.findForward("nb"); // 電訪任務
				}
			}

			return mapping.findForward("success"); // 新增電訪任務-POS
		} else {
			/*
			 * PCR-460064
			 * 若來自選單，則由選單forward=pos判斷要走POS或UNB
			 */
			if ((!org.apache.commons.lang3.StringUtils.equals("pos",
					request.getParameter("forward")) && cForm.getChangeId() == null)) {
				request.setAttribute("reasonMapping",
						org.apache.commons.lang.StringUtils.join(
								calloutServiceCI
										.getCalloutReasonDisplayMapping()
										.toArray(), ","));
				request.setAttribute("calloutReasons",
						calloutServiceCI.findCallReasonsWithExceptionItems());
				request.setAttribute("remarks", hm);
				request.setAttribute("unbOtherColumns",
						calloutServiceCI.findRemarkItems());

				return mapping.findForward("nb"); // 新增電訪任務-UNB
			}

			request.setAttribute("agentEqualsSend", agentEqualsSend);
			request.setAttribute("sendType", sendType);
			return mapping.findForward("success"); // 新增電訪任務-POS
		}
    }
    
	private Boolean initPageSetting(CalloutOnlineForm cForm,
			HttpServletRequest request) {
		
		Boolean isSendEVoice = Boolean.TRUE; // true:UNB false:POS
		AppUser user = AppContext.getCurrentUser();

		if ("true".equals(request.getParameter("isDetail"))
				|| "true".equals(request.getParameter("isFromAckTaskSec"))
				|| "true".equals(request.getParameter("isFromUWIssues"))) {
			/*
			 * 原CATEGORY方式判斷要走POS或UNB
			 */
	        //根據當前使用者帶出設置
			Long userCategory = partyService.getBizCategory(user.getUserId());
			if (userCategory == CodeCst.BIZ_CATEGORY_POS) { // POS
				isSendEVoice = Boolean.FALSE;
			} else if (cForm.getChangeId() != null) {
				isSendEVoice = Boolean.FALSE;
			} else if (userCategory == CodeCst.BIZ_CATEGORY_PA) { // PA
				isSendEVoice = Boolean.FALSE;
			}
		} else {
			/*
			 * PCR-460064 新增電訪頁籤拆分權限
			 * 若來自選單，則由選單forward=pos判斷要走POS或UNB
			 */
			if (org.apache.commons.lang3.StringUtils.equals("pos",
					request.getParameter("forward"))) {
				// POS
				isSendEVoice = Boolean.FALSE;
			} else if (request.getParameter("forward") == null) {
				// UNB
				isSendEVoice = Boolean.TRUE;
			}
			// 保全輸入頁面進入，進入新增電訪任務時，以changeId查詢電訪紀錄
			if (cForm.getChangeId() != null) {
				isSendEVoice = Boolean.FALSE;
			}
		}

        // 頁面傳值 FOR 接口帶值
        request.setAttribute("polcyId", cForm.getPolicyId()); // UNB POS
        request.setAttribute("chagneId", cForm.getChangeId()); // POS
        request.setAttribute("calloutReason", cForm.getCalloutReason()); // UNB:二審
        
        // 頁面設置
        if(StringUtils.isNullOrEmpty(request.getParameter("title"))) {
			request.setAttribute("title",
					cForm.getPolicyId() == null ? "NORMAL" : "NONE"); // 接口
        } else {
			EscapeHelper.escapeHtml(request).setAttribute("title",
					request.getParameter("title")); // 接口
        }
        request.setAttribute("isSendEVoice", isSendEVoice);
        request.setAttribute("readOnly", false);
        request.setAttribute("isSave", false);
        request.setAttribute("saveResult", false);
        request.setAttribute("voListSize", 0);
		request.setAttribute("currentDept", deptDS
				.getDeptById(user.getDeptId()).getDeptName());
        
        // 頁面需求
		request.setAttribute("currentTime",
				AppContext.getCurrentUserLocalTime());
		request.setAttribute("voListJson",
				JSONSerializer.toJSON(new ArrayList<CalloutTransVO>()));
        CalloutTransVO cFormVO = cForm.getCalloutTransVO();
        cFormVO.setCalloutReqDept(AppContext.getCurrentUser().getDeptId());
        if(!isSendEVoice){
            cFormVO.setCallerParty(user.getUserId());
			cFormVO.setCallerExt(employeeService.getEmployeeByEmpId(
					user.getUserId()).getOfficeTelExt());
		} else if (cForm.getPolicyId() != null) {
			// UNB執行此頁面前，檢核保單各險種若期繳保費及自定保費皆為０，
			// 彈出視窗顯示錯誤訊息「因<XXXXX、XXXXX…>應繳保費為0，不可送出電訪任務，請確認。」，不可執行。
			// 註：< XXXXX、XXXXX…>表保費錯誤的險種代號。
			List<String> planCodeList = new ArrayList<String>();
			List<CoverageVO> cvgList = coverageService.findByPolicyId(cForm
					.getPolicyId());
			boolean hasWaiver = false;
			BigDecimal waiverPrem = new BigDecimal("0");
			Integer waiverProductId = null;
			
			for (CoverageVO vo : cvgList) {
				if(vo.isWaiver()){
					hasWaiver = true;
					waiverProductId = vo.getProductId();
					waiverPrem = waiverPrem.add(vo.getCurrentPremium().getTotalPremAf());
				} else {
					if ((vo.getCustomizedPrem() == null || vo.getCustomizedPrem()
									.equals(new BigDecimal("0")))
									&& (vo.getCurrentPremium().getTotalPremAf()
													.equals(new BigDecimal("0")))) {
						planCodeList.add(lifeProductService
										.getProduct(vo.getProductId().longValue())
										.getProduct().getInternalId());
					}
        }
			}
			if (hasWaiver && waiverPrem.equals(new BigDecimal("0"))) {
				planCodeList.add(lifeProductService.getProduct(waiverProductId.longValue())
								.getProduct().getInternalId());
			}
			if (!planCodeList.isEmpty()) {
				StringBuilder sb = new StringBuilder();
				sb.append("<")
						.append(org.apache.commons.lang.StringUtils.join(
								planCodeList.toArray(), "、")).append(">");
				Map<String, String> hm = new HashMap<String, String>();
				hm.put("planCode", sb.toString());
				String alertMsg = StringResource.getStringData("MSG_1265458",
						AppContext.getCurrentUser().getLangId(), hm);
				request.setAttribute("alertMsg", alertMsg);
				request.setAttribute("readOnly", true);
			}

		}

        return isSendEVoice;
    }
    
    private List<Long> getIDList(CalloutOnlineForm cForm) {
    	List<Long> idList = new ArrayList<Long>();
    	
    	String[] originalIDs = cForm.getoCalloutIds().split(",");
    	
    	for (String ids : originalIDs) {
    		if (!StringUtils.isNullOrEmpty(ids)) {
    			idList.add(Long.parseLong(ids));
    		}
    	}
    	
    	return idList;
    }
    
	private void save(CalloutOnlineForm cForm, HttpServletRequest request,
			Boolean isSendEVoice) throws Exception {
    	
    	CalloutTransVO newParentPolicyIdVO = null;
        List<CalloutTransVO> saveList = new ArrayList<CalloutTransVO>();
        List<CalloutTransVO> updateList = new ArrayList<CalloutTransVO>();
        List<Long> deleteList = getIDList(cForm);
        
        // 取會辦次數
		Integer liabilityStatus = policyCI.getLiabilityStatusByPolicyCode(cForm
				.getPolicyCode());
        
        String policyIdStr = request.getParameter("policyId");
        Long policyId = cForm.getPolicyId();
        if (!StringUtils.isNullOrEmpty(policyIdStr)) {
            policyId = Long.parseLong(policyIdStr);
        } else {
            policyId = policyCI.getPolicyIdByPolicyCode(cForm.getPolicyCode());
        }
        
        
		Date validateTime = new Date(); // 變更生效日
        // 取得保單最新的風險屬性問卷資料(包含3個月內取消、退件的問卷)
		Integer addMonths = Integer.valueOf(Para.getParaValue(2061000057L));
		Date beforeDate = DateUtils.addMonth(validateTime, addMonths);
		PosRuleIlpRiskHistVO curPosIlpRisk = posIlpRiskService.getLatestIlpRiskInfoByPolicyId(policyId,true,beforeDate);
		//風險屬性問卷資料可承受範圍比率
		String lossAns = null;
		if(curPosIlpRisk == null || null == curPosIlpRisk.getAnswer5() || StringUtils.isEmpty(curPosIlpRisk.getAnswer5()) || curPosIlpRisk.getCustAppTime().compareTo(beforeDate) <= 0){
			lossAns = "";
		}else{
			switch(curPosIlpRisk.getAnswer5())
		      {
		         case "A" :
		        	 lossAns = "15%以上"; 
		             break;
		         case "B" :
		        	 lossAns = "5~14%"; 
		             break;
		         case "C" :
		        	 lossAns = "4%以下"; 
		             break;
		         default :
		        	 lossAns = "";
		      }
		}		
		
		Integer transCount = calloutTransService.getCalloutTransCount(policyId,
				liabilityStatus) + 1;
        
        // 取出  同保單號碼 記錄
        // 挑出頁面上 新增/修改 完的VO，不含刪除的VO
        
		for (int index = 0; index<cForm.getCalloutTransVOList().size(); index++) {
			CalloutTransVO vo = cForm.getCalloutTransVOList().get(index);
        	// 過濾 NULL VO
            if (vo!=null){
            	// IR-505773 calleeCertiCode去空白後全轉大寫字母
            	if(!StringUtils.isNullOrEmpty(vo.getCalleeCertiCode())) {
					vo.setCalleeCertiCode(vo.getCalleeCertiCode().trim().toUpperCase());
				}
            	
            	vo.setChangeId(cForm.getChangeId());
            	vo.setBatchIndi(YesNo.YES_NO__NO);
            	
            	// 同保單號碼的VO
                if(vo.getParentPolicyId() != null || "Y".equals(vo.getSenderFlag()) || !StringUtils.isNullOrEmpty(vo.getNoCalloutReason())){
                    newParentPolicyIdVO = vo;
            	//PCR 382493-如選中此欄位該筆電訪記錄儲存時，須一併將系統日更新電訪檔中的權益通知函產生日期
                if(org.apache.commons.lang3.StringUtils.equals("Y", vo.getSenderFlag())){
                    	newParentPolicyIdVO.setCalloutImportantSender(AppContext.getCurrentUserLocalTime());
                }
                }else{
                	
                	// 新增       : 無LIST_ID(NULL)、有CALLOUT_NUM(空字串)
                	// 修改       : 有LIST_ID 、 有CALLOUT_NUM
                	// 無變動  : 有LIST_ID 、 無CALLOUT_NUM(NULL)
                	// 刪除       : 不存在於 voList
                	
                	 // 已存在LIST_ID，代表 沒有動 或是 修改
                	if (vo.getListId() != null) {
                		
                		// 修改 : 有電訪編號
                		if (!StringUtils.isNullOrEmpty(vo.getCalloutNum())) {
                			updateList.add(vo);
                		}
                		
                		// 若存在就從清單移除，剩下就是要刪除的
                		deleteList.remove(vo.getListId()); 
                		
                	} else {
                		
                		// 新增 : 電訪編號為空字串
                		if ("".equals(vo.getCalloutNum())) {
                			if (isSendEVoice) {
								// 新契約 線上電訪需填入檔暗號  PCR355486  改call 新的method
								String docketNum = calloutTransService
										.getDocketNum(cForm.getPolicyCode());
                    			vo.setCalloutDocketNum(docketNum);
                                vo.setTransCount(transCount);

								// 重組 T_CALLOUT_QUESTIONNAIRE 電訪原因/異常項項
								String questionStr = vo
										.getCalloutQuestionnaireStr();
								if (!StringUtils.isEmpty(questionStr)) {
									List<CalloutQuestionnaireVO> questionVOList = calloutTransService
											.parseCalloutQuestionnaire(questionStr);
									if(questionVOList != null ){
										// set Remarks
										for(CalloutQuestionnaireVO quesVO: questionVOList){
											String dispReasonItem = quesVO
													.getReasonItem();
											// 針對簽收單二審發起的任務,回寫CALLOUT_REASON欄位,供二審SQL撈取
											if(CALLOUT_REASON_ACK_TASK_SEC_3.equals(quesVO.getCalloutReasonCode())){
												vo.setCalloutReason(CALLOUT_REASON_ACK_TASK_SEC_3);
											}
											// String remarks[] =
											// request.getParameterValues(dispReasonItem+"_remark");
											String remark = request
													.getParameter("calloutTransVOList["
															+ index
															+ "]."
															+ dispReasonItem
															+ "_remark");
											if(!StringUtils.isEmpty(remark)){
												quesVO.setRemarks(remark);	
                    		}
										}
									}
									vo.setQuestionnaireList(questionVOList);
								}
							}
                    		saveList.add(vo);
                		}
                	}
                	
                }
            }
        }
        
        String infoMessage = "";
        
        /* 新增電訪作業  */
        infoMessage += insertVO(saveList, isSendEVoice, lossAns);
        
        if (!isSendEVoice) { // POS 變更項
            
            /* 更新電訪作業 */
            infoMessage += updateVO(updateList, isSendEVoice, lossAns);
            
            /* 刪除電訪作業 */
            deleteVO(deleteList);
            
			doUpdateParentPolicyId(newParentPolicyIdVO,
					cForm.getModifyParentPolicyId(),
					cForm.getParentPolicyListId(),
					cForm.getCalloutNum());
            
        }
        
        request.setAttribute("returnMsg", infoMessage);
        
    }

	private String updateVO(List<CalloutTransVO> updateList,
			Boolean isSendEVoice, String lossAns) throws Exception {
    	List<CalloutOnlineVO> successList = new java.util.LinkedList<CalloutOnlineVO>();
        List<CalloutOnlineVO> failList = new java.util.LinkedList<CalloutOnlineVO>();
        
    	UserTransaction ut= Trans.getUserTransaction();
        String lossMsg = "";
        if(CollectionUtils.isNotEmpty(updateList)){
            for(CalloutTransVO vo : updateList){
            	if(!StringUtils.isNullOrEmpty(vo.getCalloutAskRadioAnsStr()) && !StringUtils.isEmpty(lossAns)){
            		if(!StringUtils.isNullOrEmpty(vo.getCalloutAskRadioAnsStr()) && StringUtils.isEmpty(lossAns)){
            			lossMsg = StringResource.getStringData("MSG_1267188", AppContext.getCurrentUser().getLangId());
            		}else if(!vo.getCalloutAskRadioAnsStr().trim().contains(lossAns.trim())){
            			lossMsg = StringResource.getStringData("MSG_1267189", AppContext.getCurrentUser().getLangId());
            			lossMsg = lossMsg.replaceAll("<#1>", lossAns);
            		}
            	}
                CalloutOnlineVO onlineVO = new CalloutOnlineVO();
                onlineVO.setCalloutNum(vo.getCalloutNum());
                try{
                    ut = TransUtils.getUserTransaction();
                    ut.begin();
                    calloutTransService.updateVO(vo, isSendEVoice);
                    successList.add(onlineVO);
                    ut.commit();
                }catch(Exception ex){
					syso("[ERROR][callee Certi_Code :=%s ] %s",
							vo.getCalleeCertiCode(),
							ExceptionUtils.getFullStackTrace(ex));
					String policyCode = policyService.load(
							new Long(vo.getPolicyId())).getPolicyNumber();
                    Map<String, String> message = new java.util.HashMap<String, String>();
                    message.put("calleeCertiCode", vo.getCalleeCertiCode());
                    message.put("policyCode",  policyCode);
                    AppException exception = new AppException(30710020013L);
                    exception.addMessageParameter(message);
					onlineVO.setEVoiceResult(
							CodeCst.CALLOUT_EVOICE_RESULT_FAILED,
							exception.getMessage());
                    failList.add(onlineVO);
                    ut.rollback();
                }
            }
        }
        
		return getOperateInfo(successList, failList, "MSG_1262911",
				"MSG_1262912", lossMsg);
    }
    
	private String insertVO(List<CalloutTransVO> saveList, Boolean isSendEVoice, String lossAns)
			throws Exception {
    	List<CalloutOnlineVO> successList = new java.util.LinkedList<CalloutOnlineVO>();
        List<CalloutOnlineVO> failList = new java.util.LinkedList<CalloutOnlineVO>();
        
    	UserTransaction ut= Trans.getUserTransaction();
        String lossMsg = "";    	
        if(CollectionUtils.isNotEmpty(saveList)){
            for(CalloutTransVO vo : saveList){
            	if(!StringUtils.isNullOrEmpty(vo.getCalloutAskRadioAnsStr()) && !StringUtils.isEmpty(lossAns)){
            		if(!StringUtils.isNullOrEmpty(vo.getCalloutAskRadioAnsStr()) && StringUtils.isEmpty(lossAns)){
            			lossMsg = StringResource.getStringData("MSG_1267188", AppContext.getCurrentUser().getLangId());
            		}else if(!vo.getCalloutAskRadioAnsStr().trim().contains(lossAns.trim())){
            			lossMsg = StringResource.getStringData("MSG_1267189", AppContext.getCurrentUser().getLangId());
            			lossMsg = lossMsg.replaceAll("<#1>", lossAns);
            		}
            	}
                try{
                    ut = TransUtils.getUserTransaction();
                    ut.begin();
					CalloutOnlineVO onlineVO = calloutTransService.bindSaveVO(
							vo, isSendEVoice, false); //PCR-502953 新增參數，如非CRM則為false

					if (onlineVO != null
							&& onlineVO.geteVoiceResult() != null
							&& org.apache.commons.lang3.StringUtils
									.isNotEmpty(onlineVO.geteVoiceResult()
											.getCalloutTransResult())
							&& CodeCst.CALLOUT_SENT_EVOICE_SYSTEM_ERROR
									.equals(onlineVO.geteVoiceResult()
											.getCalloutTransResult())) {
                        failList.add(onlineVO);
                        ut.rollback();
                        continue;
                    }
                    successList.add(onlineVO);
                    ut.commit();
                }catch(AppException ex){
                    CalloutOnlineVO failVO = new CalloutOnlineVO();
                    failVO.setCalleeCertiCode(vo.getCalleeCertiCode());
					failVO.setEVoiceResult(
							CodeCst.CALLOUT_EVOICE_RESULT_FAILED,
							ex.getMessage());
                    failList.add(failVO);
                    ut.rollback();
                }
            }
        }
        
		return getOperateInfo(successList, failList, "MSG_1262913",
				"MSG_1262914", lossMsg);
    }
    
	private String getOperateInfo(List<CalloutOnlineVO> successList,
			List<CalloutOnlineVO> failList, String msgSuccess, String msgFail, String lossMsg) {

        StringBuffer successMsg  =  new StringBuffer();
        successMsg.setLength(0);
        if(CollectionUtils.isNotEmpty(successList)){
			String infoMsg = StringResource.getStringData(msgSuccess,
					AppContext.getCurrentUser().getLangId());
            successMsg.append(infoMsg);
            for(CalloutOnlineVO vo : successList){
                successMsg.append(String.format("%s<br/>", vo.getCalloutNum()));
            }
        }
        StringBuffer failMsg  =  new StringBuffer();
        failMsg.setLength(0);
        if(CollectionUtils.isNotEmpty(failList) || !StringUtils.isEmpty(lossMsg)){
			String infoMsg = StringResource.getStringData(msgFail, AppContext
					.getCurrentUser().getLangId());
            failMsg.append(infoMsg);
            failMsg.append(String.format("%s<br/>",lossMsg));
            for(CalloutOnlineVO vo : failList){
                EVoiceResultVO resultVO = vo.geteVoiceResult();
				if (resultVO != null
						&& org.apache.commons.lang3.StringUtils
								.isNotEmpty(resultVO
										.getCalloutTransResultDetail()))
					failMsg.append(String.format("%s<br/>",
							resultVO.getCalloutTransResultDetail()));
            }
        }
        return successMsg.toString() + failMsg.toString();
    }
    
    private void deleteVO(List<Long> deleteList) throws Exception {
    	UserTransaction ut= Trans.getUserTransaction();
    	try{
            // 刪除作業
            ut = TransUtils.getUserTransaction();
            ut.begin();
            for (Long id : deleteList) {
                calloutTransService.deleteByListId(id);
            }
            ut.commit();
        } catch(Exception e){
            ut.rollback();
            throw e;
        }
    }
    
	private boolean doUpdateParentPolicyId(CalloutTransVO parentPolicyVO,
			String modifyParentPolicyId, String listId, String calloutNum) throws Exception {
    
        String policyIds[] = modifyParentPolicyId.split(",");
        if(policyIds.length==2 &&  !"-,-".equals(modifyParentPolicyId)){

        	UserTransaction ut= Trans.getUserTransaction();
        	try {
        		ut = TransUtils.getUserTransaction();
                ut.begin();
            	
                if(policyIds[0].equals(policyIds[1])){ // 更新
                	if(null == parentPolicyVO.getListId()&& null != listId){
                		parentPolicyVO.setListId(Long.valueOf(listId));
                	}
                	
                	if(StringUtils.isNullOrEmpty(parentPolicyVO.getCalloutNum())  && null != calloutNum){
                		parentPolicyVO.setCalloutNum(calloutNum);
                	}
					calloutTransService.updateVOList(
							ListTool.toList(parentPolicyVO), false);
                } else {
                    if(!"-".equals(policyIds[0])){   // 新增
						//if ((policyIds[0]).equals(parentPolicyVO
						//		.getParentPolicyId().toString())) {
                        	// 同保單電訪註記
                        	// 配合MultiAP 修改儲存接口
							String identity = calloutTransService
									.getCalloutIdentity(AppContext
											.getCurrentUserLocalTime(), "PPI");
							calloutTransService
									.saveVO(parentPolicyVO, identity);
                        //}
                        }
                    if(!"-".equals(policyIds[1])){   // 刪除
						calloutTransService.deleteByListId(Long
								.parseLong(policyIds[1]));
                    }
                }
                
                ut.commit();
        	} catch (Exception e) {
                ut.rollback();
                throw e;
        	}
            
            return true;
        }
        return false;
    }

	private void getAndSetCalloutVOList(HttpServletRequest request,
			CalloutOnlineForm cForm, boolean isSendEVoice) {
    	
    	// 頁面帶值
    	String policyIdStr = request.getParameter("policyId");
    	if (!StringUtils.isNullOrEmpty(policyIdStr)) {
        	Long policyId = Long.parseLong(policyIdStr);
        	cForm.setPolicyId(policyId);
        	cForm.setPolicyCode(policyService.load(policyId).getPolicyNumber());
        	cForm.setCalloutTransVOList(new ArrayList<CalloutTransVO>());
        	cForm.setCalloutTransVO(new CalloutTransVO());
        	cForm.getCalloutTransVO().setPolicyId(cForm.getPolicyId());
            if(!isSendEVoice){
            	cForm.getCalloutTransVO().setCallerParty(AppContext.getCurrentUser().getUserId());
            	cForm.getCalloutTransVO().setCallerExt(employeeService.getEmployeeByEmpId(AppContext.getCurrentUser().getUserId()).getOfficeTelExt());
            }
    	}
    	
    	// POS 需帶入資料
        if(!isSendEVoice){ 
        	
        	String changeIdStr = request.getParameter("changeId");
        	if (!StringUtils.isNullOrEmpty(changeIdStr)) {
        		cForm.setChangeId(Long.parseLong(changeIdStr));
        	}
        	
            getAndSetCalloutVOListForPOSValue(request, cForm);
            
        }
    }
    
	private void getAndSetCalloutVOListForPOSValue(HttpServletRequest request,
			CalloutOnlineForm cForm) {
		if (cForm.getChangeId() != null && !"".equals(cForm.getChangeId())) { // 僅帶有ChangeId
																				// 才調出紀錄
            List<CalloutTransVO> list ;
            CalloutTransVO search=new CalloutTransVO();
            search.setChangeId(cForm.getChangeId());
            list = calloutTransService.query(search);
            // Initialize
            cForm.setModifyParentPolicyId("-,-");
            cForm.setParentPolicyId("");
            cForm.setParentPolicyListId("");
            cForm.getCalloutTransVO().setListId(null);
            cForm.getCalloutTransVO().setParentPolicyId(null);
            String originalIDs="";
            // Find Records
            for(CalloutTransVO vo:list){
                if(vo.getParentPolicyId()!=null || "Y".equals(vo.getSenderFlag()) || !StringUtils.isNullOrEmpty(vo.getNoCalloutReason())){
                	
                if(vo.getParentPolicyId()!=null){
					cForm.setParentPolicyId(Long.toString(vo
							.getParentPolicyId()));
                		cForm.getCalloutTransVO().setParentPolicyId(vo.getParentPolicyId());
                	}
                	
                	cForm.setCalloutNum(vo.getCalloutNum());
                	
                    cForm.setParentPolicyListId(Long.toString(vo.getListId()));
					
                    cForm.setCalloutTypeStr(vo.getCalloutTypesStr());
                    
					cForm.getCalloutTransVO().setCalloutTypeOther(
							vo.getCalloutTypeOther());
					
					cForm.setSenderFlag(vo.getSenderFlag());
					
					cForm.setNoCalloutReason(vo.getNoCalloutReason());
					
					cForm.setNoCalloutReasonDesc(vo.getNoCalloutReasonDesc());
					
                    search = vo; //記錄此VO 之後刪除
                } else{ 
                	if(vo.getCalleeBirthday()!=null) {
                		vo.calcYear(AppContext.getCurrentUserLocalTime());
                    }
                    //紀錄找出來的calloutId 編輯後可能刪除
                    originalIDs += "," + vo.getListId();
                }
            }
            if(search.getParentPolicyId()!=null || "Y".equals(search.getSenderFlag()) || !StringUtils.isNullOrEmpty(search.getNoCalloutReason())){
                list.remove(search); // 刪除紀錄同保單號碼之VO
            }
            
			request.setAttribute("hasItemAmount", hasItemAmount(cForm.getChangeId()));
            request.setAttribute("voList", list);
            request.setAttribute("voListJson", JSONSerializer.toJSON(list));
            request.setAttribute("voListSize", list.size());
            cForm.setoCalloutIds(originalIDs);
        }
    }
	
	public boolean hasItemAmount(Long ChangeId) {
		// 引用CSValidationSubmitAppServiceImpl.validateCallOutTransAmount()
		// 退費金額
		HashMap<String, BigDecimal> resultMap = csPremiumService.getAllItemAmountIncludAll(ChangeId);
		// 淨退補費金額
		BigDecimal netAmt = resultMap.get("netAmt");
		if (netAmt == null) {
			netAmt = BigDecimal.ZERO;
		}
		
		boolean actualPay = netAmt.compareTo(BigDecimal.ZERO) < 0 ? true : false;
		return actualPay;
	}
	
	public void setCallOutItems(HttpServletRequest request, CalloutOnlineForm cForm) {
		List<CallOutOnlineQuesItemVo> itemResultList = calloutAskSwitchService.findCallOutOnlineQuesItem();
		StringBuilder sb = new StringBuilder();
		
		if (cForm.getChangeId() != null) {
			List<Integer> serviceList = new ArrayList<Integer>();
			Map<String, String> calloutAskMap = getCalloutAskMap();
			Map<String, String> calloutUnfavorFactorMap = getCalloutUnfavorFactorMap();
			ApplicationVO applicationVO = applicationDS.getApplication(cForm.getChangeId());

			for (AlterationItemVO alterationItemVO : applicationVO.getAlterationItemList()) {
				serviceList.add(alterationItemVO.getServiceId());
				if (alterationItemVO.getServiceId().equals(CodeCst.SERVICE__ADD_RIDER)) {
					int no = 1;
					String askItem26 = calloutAskMap.get("26");
					List<CoverageVO> coverageVOList = coverageService.findAddRiderAfListByPolicyIdAndPolicyChgId(alterationItemVO.getPolicyId(), alterationItemVO.getPolicyChgId());
					for (CoverageVO coverageVO : coverageVOList) {
						String internalId = lifeProductService.getProduct(coverageVO.getProductId().longValue()).getProduct().getInternalId();
						List<CalloutProdUnfavorFactorVO> calloutProdUnfavorFactorVOList = calloutAskSwitchService.findCalloutProdUnfavorFactorByInternalId(internalId);
						if (!calloutProdUnfavorFactorVOList.isEmpty()) {
							List<String> factors = new ArrayList<String>();
							for (int i = 0; i < calloutProdUnfavorFactorVOList.size(); i++) {
								String factorCode = calloutProdUnfavorFactorVOList.get(i).getFactorCode();
								factors.add((i + 1) + "." + calloutUnfavorFactorMap.get(factorCode));
							}
							String replaceItem = askItem26.replace("{item}", internalId).replace("{factor}", org.apache.commons.lang3.StringUtils.join(factors, "/"));
							sb.append("(" + no + ")" + replaceItem + "<br>");
							no++;
						}
					}
				}
			}
			request.setAttribute("serviceList", JSONSerializer.toJSON(serviceList).toString());
		}
		
		Iterator<CallOutOnlineQuesItemVo> iterator = itemResultList.iterator();
		while (iterator.hasNext()) {
			CallOutOnlineQuesItemVo vo = iterator.next();
			if ("26".equals(vo.getAskCode())) {
				if (org.apache.commons.lang3.StringUtils.isBlank(sb.toString())) {
					iterator.remove();
				} else {
					vo.setAskCodeValue(sb.toString());
				}
			}
		}

		String jsonStr = JSONArray.fromObject(itemResultList).toString();
		request.setAttribute("callOutItemList", jsonStr);
	}
	
	public Map<String, String> getCalloutAskMap() {
		Map<String, String> calloutAskMap = new HashMap<String, String>();
		List<CalloutAskVO> calloutAskVOList = calloutAskSwitchService.findCalloutAsk();
		for (CalloutAskVO vo : calloutAskVOList) {
			calloutAskMap.put(vo.getCode(), vo.getCodeValue());
		}
		return calloutAskMap;
	}

	public Map<String, String> getCalloutUnfavorFactorMap() {
		Map<String, String> calloutUnfavorFactorMap = new HashMap<String, String>();
		List<CalloutUnfavorFactorVO> calloutUnfavorFactorVOList = calloutAskSwitchService.findCalloutUnfavorFactor();
		for (CalloutUnfavorFactorVO vo : calloutUnfavorFactorVOList) {
			calloutUnfavorFactorMap.put(vo.getCode(), vo.getCodeValue());
		}
		return calloutUnfavorFactorMap;
	}
	
	@RequestMapping(value = "/cs/callout/checkLossAns.d")
    @ResponseBody
	public Object checkLossAns(Model model,
            @RequestBody Map<String, Object> map,
            HttpServletRequest request) throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();
        try{      
        	String policyId = (String)map.get("policyId");
        	String radioAns = (String)map.get("radioAns");
        	String lossMsg = "";
        	Date validateTime = new Date(); // 變更生效日
            // 取得保單最新的風險屬性問卷資料(包含3個月內取消、退件的問卷)
    		Integer addMonths = Integer.valueOf(Para.getParaValue(2061000057L));
    		Date beforeDate = DateUtils.addMonth(validateTime, addMonths);
    		PosRuleIlpRiskHistVO curPosIlpRisk = posIlpRiskService.getLatestIlpRiskInfoByPolicyId(Long.parseLong(policyId),true,beforeDate);
    		//風險屬性問卷資料可承受範圍比率
    		String lossAns = null;
    		if(curPosIlpRisk == null || null == curPosIlpRisk.getAnswer5() || StringUtils.isEmpty(curPosIlpRisk.getAnswer5()) || curPosIlpRisk.getCustAppTime().compareTo(beforeDate) <= 0){
    			lossAns = "";
    		}else{
    			switch(curPosIlpRisk.getAnswer5())
    		      {
    		         case "A" :
    		        	 lossAns = "15%以上"; 
    		             break;
    		         case "B" :
    		        	 lossAns = "5~14%"; 
    		             break;
    		         case "C" :
    		        	 lossAns = "4%以下"; 
    		             break;
    		         default :
    		        	 lossAns = "";
    		      }
    		}
    		if(lossAns == ""){
    			lossMsg = StringResource.getStringData("MSG_1267188", AppContext.getCurrentUser().getLangId());
    			//lossMsg = lossMsg.replaceAll("<#1>", lossAns);
    		}else if(!lossAns.trim().contains(radioAns.trim())){
    			lossMsg = StringResource.getStringData("MSG_1267189", AppContext.getCurrentUser().getLangId());
    		}
//    		if(!lossAns.contains(radioAns)){
//    			lossMsg = StringResource.getStringData("MSG_1267189", AppContext.getCurrentUser().getLangId());
//    			//lossMsg = lossMsg.replaceAll("<#1>", lossAns);
//    		}else if(lossAns == ""){
//    			lossMsg = StringResource.getStringData("MSG_1267188", AppContext.getCurrentUser().getLangId());
//    		}
    		result.put("lossMsg", lossMsg);
    		result.put("ans", curPosIlpRisk.getAnswer5());
        }catch(Exception e){
            result.put("lossMsg","");
            throw ExceptionFactory.parse(e);
        }
		return result;
	}
    
}
