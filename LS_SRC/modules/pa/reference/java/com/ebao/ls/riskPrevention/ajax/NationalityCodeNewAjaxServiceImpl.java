package com.ebao.ls.riskPrevention.ajax;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.json.JSONObject;

import com.ebao.ls.pub.ajax.AjaxService;
import com.ebao.ls.riskPrevention.bs.NationalityCodeService;
import com.ebao.ls.riskPrevention.vo.NationalityCodeVO;

public class NationalityCodeNewAjaxServiceImpl implements AjaxService<Map<String, Object>, String> {

    @Resource(name = NationalityCodeService.BEAN_DEFAULT)
    NationalityCodeService nationalityCodeService;

    @Override
    public String execute(Map<String, Object> in) {
        String nationalName = (String) in.get("nationalName");
        String enNationalShortName = (String) in.get("enNatioanlShortName");
        // boolean result = false;
        NationalityCodeVO formalNameVO = new NationalityCodeVO();
        formalNameVO.setNationalName(nationalName);
        NationalityCodeVO enNameVO = new NationalityCodeVO();
        enNameVO.setEnNationalShortName(enNationalShortName);

        Map<String, Object> data = new HashMap<String, Object>();
        if (nationalityCodeService.query(formalNameVO, null).size() == 0 && 
                        nationalityCodeService.query(enNameVO, null).size() == 0) {
            data.put("result", true);
        } else {
            data.put("result", false);
        }

        return new JSONObject(data).toString();
    }

}
