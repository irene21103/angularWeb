package com.ebao.ls.crs.nb;

import com.ebao.ls.crs.vo.NBcrsVO;
import com.ebao.ls.foundation.service.GenericService;

public interface NBcrsService extends GenericService<NBcrsVO> {
	
	public final static String BEAN_DEFAULT = "nBcrsService";
	
	public abstract NBcrsVO findFirstByPolicyId(Long policyId);
	
	public abstract java.util.List<NBcrsVO> findByPolicyId(Long policyId);
	
	public abstract NBcrsVO findByChangeId(Long changeId);
}
