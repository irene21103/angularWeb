package com.ebao.ls.uw.cmu.event;

import com.ebao.foundation.commons.annotation.TriggerPoint;
import com.ebao.ls.cmu.event.NbUwBacktoEntry;
import com.ebao.pub.event.AbstractIntegrationListener;
import com.ebao.pub.integration.IntegrationMessage;

public class NbUwBacktoEntryIntegrationListener
    extends
      AbstractIntegrationListener<NbUwBacktoEntry> {
  final static String code = "uw.nbuwbacktoDataEntry";

  @Override
  @TriggerPoint(component = "Underwriting", description = "During underwriting, if underwriters need to update proposal information, they have to return it to data entry staff.<br/>When a proposal has been returned to data entry worklist, the event is triggered.<br/>Menu navigation: New business > Worklist > Underwriting.<br/>", event = "direct:event."
      + code, id = "com.ebao.tp.ls." + code, howToUse="Can trigger some actions which is unrelated with main process to this event such as letters, SMS etc. It is commonly seen that system will generate proposal status change reminder to producers or customers.<br/>")
  protected String getEventCode(NbUwBacktoEntry event,
      IntegrationMessage<Object> in) {
    return "event." + code;
  }
}
