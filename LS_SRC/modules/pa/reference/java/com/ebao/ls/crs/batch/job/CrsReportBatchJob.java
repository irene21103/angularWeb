package com.ebao.ls.crs.batch.job;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.ebao.foundation.commons.exceptions.AppException;
import com.ebao.foundation.commons.exceptions.GenericException;
import com.ebao.ls.batch.TGLBasePieceableJob;
import com.ebao.ls.cs.integration.batch.helper.BatchQueryHelper;
import com.ebao.pub.batch.help.BatchHelp;
import com.ebao.pub.batch.type.JobStatus;
import com.ebao.pub.batch.type.LogLevel;
import com.ebao.pub.batch.util.BatchLogUtils;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.framework.ExceptionFactory;
import com.ebao.pub.util.DBean;

/**
 * CRS申報資料檔
 */
public class CrsReportBatchJob extends TGLBasePieceableJob {

	@Override
	protected String getJobName() {
		return BatchHelp.getJobName();
	}

	@Override
	protected Object[] prepareIds() throws Throwable {
		
		clearTable();
		List<String> dataList = this.getFetchDataList();
		return dataList.toArray();
	}

	@Override
	protected int execute(String id) throws Exception {
	    try {
	    	  execPlSql(id);  
	    	  return JobStatus.EXECUTE_SUCCESS;
	        } catch (Exception e) {
	          BatchLogUtils.addLog(LogLevel.DEBUG, null, "Id = " + id);
	        }
	    return JobStatus.EXECUTE_FAILED;
	}
	
	private List<String> getFetchDataList() {
		DBean db = null;
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;

		List<String> dataList = new ArrayList<String>();
		try {
			String querySql = new BatchQueryHelper().getSql(CrsReportBatchJob.class);

			db = new DBean();
			db.connect();
			conn = db.getConnection();
			
			pst = conn.prepareStatement(querySql);
			rs = pst.executeQuery();

			while (rs.next()) {
				dataList.add(rs.getString(1));
			}
			return dataList;
		} catch (Exception ex) {
			throw ExceptionFactory.parse(ex);
		} finally {
			DBean.closeAll(rs, pst, db);
		}
	}
	
    public void execPlSql(String logId) throws GenericException {
    	Connection con = null;
        CallableStatement stmt = null;
        DBean db = new DBean();
        ResultSet rs = null;
        try {
          // db connect
          db = new DBean();
          db.connect();
          con = db.getConnection();
          
          stmt = con.prepareCall("{call PKG_LS_CRS_REPORT.p_crs_report_r(?) }");
          stmt.setString(1, logId);
          stmt.execute();
          stmt.close();
          
        } catch (Exception e) {
          throw ExceptionFactory.parse(e);
        } finally {
          DBean.closeAll(null, stmt, db);
        }

    }
    
    private void clearTable() {
    	Connection con = null;
        CallableStatement stmt = null;
        DBean db = new DBean();
        ResultSet rs = null;
        try {
          // db connect
          db = new DBean();
          db.connect();
          con = db.getConnection();
          
          stmt = con.prepareCall("{call PKG_LS_CRS_REPORT.p_insert_mesg()}");
          stmt.execute();
          stmt.close();
          
        } catch (Exception e) {
          throw ExceptionFactory.parse(e);
        } finally {
          DBean.closeAll(null, stmt, db);
        }

    }

}
