package com.ebao.ls.liaRoc.batch;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.scheduling.annotation.Scheduled;

import com.ebao.ls.pa.nb.batch.NBSpringCorntabTrigger;

public class LiaRocUploadReceiveBatchTrigger extends NBSpringCorntabTrigger {

	private Logger logger = Logger.getLogger(getClass());

	@Resource(name = LiaRocUploadReceiveBatch.BEAN_DEFAULT)
	private LiaRocUploadReceiveBatch liaRocUploadReceiveBatch;
	//IR-296739-eBao公會資料送給ESP 新增排程時間22:15、22:25、23:00、23:15、23:25
	@Override
	@Scheduled(cron = "0 0 13,18,20,21,22 * * *") // 13:00, 18:00, 23:00 every day.
	protected void execute() {

		try {
			logger.info("公會收件通報排程");
			//submit(1331942L);	
			if (super.isBatch()) {
				liaRocUploadReceiveBatch.mainProcess();
			}
		} catch (Exception e) {
			logger.error(e);
		}
	}
	//IR-296739-eBao公會資料送給ESP 新增排程時間22:15、22:25、23:00、23:15、23:25
	//排程2
	@Scheduled(cron = "0 15,25 22,23 * * *") 
	protected void execute2() {
		this.execute();
	}
	//PCR-338437 -eBao公會資料送給ESP 新增排程時間21:50 2019/09/30 Add by Kathy
	//50分太壓線了,跟Kathy討論,我們兩個決定改40分,因為不想因為22:00提早過版，或維護時server shutdown,無法送至公會 Add by Simon
	//排程3
	@Scheduled(cron = "0 40 21 * * *") 
	protected void execute3() {
		this.execute();
	}
}
