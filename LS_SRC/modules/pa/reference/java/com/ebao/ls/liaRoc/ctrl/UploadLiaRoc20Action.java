package com.ebao.ls.liaRoc.ctrl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.ls.escape.helper.EscapeHelper;
import com.ebao.pub.framework.GenericAction;
import com.ebao.pub.util.StringUtils;

/**
 *  <h1>單筆上傳公會通報</h1><p>
 * @author Simon.Huang
 * @since 20021/05/10<p>
 */
public class UploadLiaRoc20Action extends GenericAction {

	public static String TYPE_SHOW_COVERAGE_LIST = "show_coverage_list";

	@Override
	public ActionForward process(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		String subTabType = EscapeHelper.escapeHtml(request.getParameter("subTabType")); // popup頁面傳入

		if (StringUtils.isNullOrEmpty(subTabType) == false) {
			return mapping.findForward(TYPE_SHOW_COVERAGE_LIST);
		}

		return mapping.findForward("main");
	}
}
