package com.ebao.ls.uw.ctrl.restriction;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.UserTransaction;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.ls.pa.pub.ci.CoverageCI;
import com.ebao.ls.pa.pub.ref.api.prd.ProductService;
import com.ebao.ls.pa.pub.ref.vo.prd.AgeLimit;
import com.ebao.ls.pa.pub.ref.vo.prd.ProductBasicVarietySimpleVO;
import com.ebao.ls.pa.pub.ref.vo.prd.ProductVO;
import com.ebao.ls.pa.pub.service.PolicyService;
import com.ebao.ls.pa.pub.vo.CoverageVO;
import com.ebao.ls.product.model.query.criteria.LimitVO;
import com.ebao.ls.pty.ci.CustomerCI;
import com.ebao.ls.pty.vo.CustomerVO;
import com.ebao.ls.pub.util.BizUtils;
import com.ebao.ls.uw.ctrl.ActionUtil;
import com.ebao.ls.uw.ctrl.pub.UwRestrictionForm;
import com.ebao.ls.uw.ds.UwPolicyService;
import com.ebao.ls.uw.ds.sp.CompleteUWProcessSp;
import com.ebao.ls.uw.ds.sp.QueryUwDataSp;
import com.ebao.ls.uw.ds.vo.UwRestrictionVO;
import com.ebao.ls.uw.util.Utils;
import com.ebao.ls.uw.vo.UwConditionVO;
import com.ebao.ls.uw.vo.UwPolicyVO;
import com.ebao.ls.uw.vo.UwProductVO;
import com.ebao.pub.annotation.PrdAPIUpdate;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.framework.ExceptionFactory;
import com.ebao.pub.framework.GenericAction;
import com.ebao.pub.framework.GenericException;
import com.ebao.pub.framework.MultiWarning;
import com.ebao.pub.util.BeanUtils;
import com.ebao.pub.util.Log;
import com.ebao.pub.util.Trans;
import com.ebao.pub.util.TransUtils;

/**
 * <p>
 * Title:Underwriting
 * </p>
 * <p>
 * Description:
 * </p>
 * <p>
 * Copyright: Copyright (c) 2004
 * </p>
 * <p>
 * Company: eBaoTech Corporation
 * </p>
 * 
 * @author mingchun.shi
 * @since Jun 27, 2005
 * @version 1.0
 */
public class SaveRestrictAction extends GenericAction {
  public static final String BEAN_DEFAULT = "/uw/uwSaveRestrict";

/**
   * save restrict
   * 
   * @param mapping
   * @param form
   * @param req
   * @param res
   * @throws java.lang.Exception
   * @return
   */
  @Override
  public ActionForward process(ActionMapping mapping, ActionForm form,
      HttpServletRequest req, HttpServletResponse res) throws Exception {
    UwRestrictionForm restrictForm = (UwRestrictionForm) form;
    if (restrictForm.getWaitPeriod() == null) {
      restrictForm.setWaitPeriod(Integer.valueOf(0));
    }
    Long underwriteId = restrictForm.getUnderwriteId();
    Long itemId = restrictForm.getItemId();
    UwProductVO productVO = uwPolicyDS.findUwProduct(underwriteId, itemId);
    if (ActionUtil.getUnitIndicator(restrictForm.getItemId(),
            Long.valueOf(restrictForm.getProductId().intValue()),policyService.getActiveVersionDate(productVO)).booleanValue()){
    	restrictForm.setUnit(new BigDecimal(req.getParameter("amount1")));
    	restrictForm.setAmount(BigDecimal.ZERO);
    }else{
    	restrictForm.setAmount(new BigDecimal(req.getParameter("amount1")));
    	restrictForm.setUnit(BigDecimal.ZERO);
    }
    // when no restrict on unit,set previous value
    restrictForm.setReducedUnit(productVO.getReducedUnit());
    // if (req.getParameter("miniSA") != null){
    // restrictForm.setReducedAmount(new
    // BigDecimal(req.getParameter("miniSA")));
    // }else{
    // restrictForm.setReducedAmount(new
    // BigDecimal(req.getParameter("reducedAmount1")));
    // }
    if (ActionUtil.getUnitIndicator(restrictForm.getItemId(),
        Long.valueOf(restrictForm.getProductId().intValue()),policyService.getActiveVersionDate(productVO)).booleanValue()) {
      restrictForm.setReducedUnit(new BigDecimal(com.ebao.ls.encode.encoder.Encode.encodeForHTML(req)
          .getParameter("reducedAmount1")));
      restrictForm.setReducedAmount(BigDecimal.ZERO);
    } else {
      restrictForm.setReducedAmount(new BigDecimal(com.ebao.ls.encode.encoder.Encode.encodeForHTML(req)
          .getParameter("reducedAmount1")));
      restrictForm.setReducedUnit(BigDecimal.ZERO);
    }
    UwRestrictionVO restrictVO = new UwRestrictionVO();
    BeanUtils.copyProperties(restrictVO, restrictForm);
    // restrictVO.setReducedUnit(restrictVO.getReducedAmount());
    // restrictVO.setUnit(restrictVO.getReducedUnit());
    Log.info(SaveRestrictAction.class, "restrictVO.getReducedAmount=="
        + restrictVO.getReducedAmount());
    UserTransaction trans = null;
    try {
      trans = Trans.getUserTransaction();
      trans.begin();
      if ("true".equals(req.getParameter("del"))) {
        delRestric(underwriteId, itemId);
      } else {
        generatorConCode(restrictVO);
        uwPolicyDS.updateUwRestriction(restrictVO);
      }
      // when cs-uw,synchronize records
      UwPolicyVO uwPolicyVO = uwPolicyDS.findUwPolicy(restrictForm
          .getUnderwriteId());
      if (Utils.isCsUw(uwPolicyVO.getUwSourceType())) {
        CompleteUWProcessSp.synUWInfo4CS(uwPolicyVO.getPolicyId(),
            uwPolicyVO.getUnderwriteId(), uwPolicyVO.getChangeId(),
            Integer.valueOf(3));
      }
      trans.commit();
    } catch (Exception e) {
      TransUtils.rollback(trans);
      throw ExceptionFactory.parse(e);
    }
    req.setAttribute("action_form", restrictForm);
    req.setAttribute("underwriteId", underwriteId);
    // req.setAttribute("itemId", itemId);
    return mapping.findForward("product");
  }

  /**
   * delete the restrict coverage information
   * 
   * @param underwriteId
   * @param itemId
   * @throws Exception
   */
  private void delRestric(Long underwriteId, Long itemId) throws Exception {
    UwProductVO productVO = uwPolicyDS.findUwProduct(underwriteId, itemId);
    if (productVO != null) {
      List<CoverageVO> nbuVOs = coverageCI.findByPolicyId(productVO
          .getPolicyId());
      for (CoverageVO vo : nbuVOs) {
        if (vo.getItemId().equals(productVO.getItemId())) {
          if (vo.getVulExt() != null) {
            productVO.setSaFactor(vo.getVulExt().getSaFactor());
          }
        }
        productVO.setWaitPeriod(vo.getWaitPeriod());
      }
    }
    if (productVO.getReducedAmount().compareTo(productVO.getAmount()) < 0) {
      productVO.setReducedAmount(productVO.getAmount());
      productVO.setUwChargePeriod(productVO.getChargePeriod());
      productVO.setUwChargeYear(productVO.getChargeYear());
      productVO.setUwCoveragePeriod(productVO.getCoveragePeriod());
      productVO.setUwCoverageYear(productVO.getCoverageYear());
      uwPolicyDS.updateUwProduct(productVO);
    }
  }

  private void generatorConCode(UwRestrictionVO restrictVO)
      throws GenericException {
    UwProductVO productVO = uwPolicyDS.findUwProduct(
        restrictVO.getUnderwriteId(), restrictVO.getItemId());
    if (((productVO.getSaFactor() != null && restrictVO.getMaFactor() != null && !productVO
        .getSaFactor().toString().equals(restrictVO.getMaFactor())) || productVO
        .getAmount().compareTo(restrictVO.getReducedAmount()) > 0)) {
      boolean isCreat = true;
      UwConditionVO conditionVO = new UwConditionVO();
      try {
        BeanUtils.copyProperties(conditionVO, productVO);
      } catch (Exception ex) {
        throw ExceptionFactory.parse(ex);
      }
      conditionVO.setUnderwriterId(Long.valueOf(AppContext.getCurrentUser()
          .getUserId()));
      conditionVO.setConditionCode("0006");
      conditionVO.setUwListId(null);
      Collection conditionCo = uwPolicyDS.findUwConditionEntitis(productVO
          .getUnderwriteId());
      if (!conditionCo.isEmpty()) {
        Iterator conditonIt = conditionCo.iterator();
        while (conditonIt.hasNext()) {
          UwConditionVO conditonVO = (UwConditionVO) conditonIt.next();
          if (conditonVO.getConditionCode().equals("0006")) {
            isCreat = false;
          }
        }
      }
      if (isCreat) {
        uwPolicyDS.createUwCondition(conditionVO);
      }
    }
    if ((!productVO.getChargeYear().equals(restrictVO.getUwChargeYear()))
        || productVO.getCoverageYear().intValue() != restrictVO
            .getUwCoverageYear().intValue()) {
      boolean isCreat = true;
      UwConditionVO conditionVO = new UwConditionVO();
      try {
        BeanUtils.copyProperties(conditionVO, productVO);
      } catch (Exception ex) {
        throw ExceptionFactory.parse(ex);
      }
      conditionVO.setUnderwriterId(Long.valueOf(AppContext.getCurrentUser()
          .getUserId()));
      conditionVO.setConditionCode("0007");
      conditionVO.setUwListId(null);
      Collection conditionCo = uwPolicyDS.findUwConditionEntitis(productVO
          .getUnderwriteId());
      if (!conditionCo.isEmpty()) {
        Iterator conditonIt = conditionCo.iterator();
        while (conditonIt.hasNext()) {
          UwConditionVO conditonVO = (UwConditionVO) conditonIt.next();
          if (conditonVO.getConditionCode().equals("0007")) {
            isCreat = false;
          }
        }
      }
      if (isCreat) {
        uwPolicyDS.createUwCondition(conditionVO);
      }
    }
    if (productVO.getWaitPeriod().intValue() != restrictVO.getWaitPeriod()
        .intValue()) {
      boolean isCreat = true;
      UwConditionVO conditionVO = new UwConditionVO();
      try {
        BeanUtils.copyProperties(conditionVO, productVO);
      } catch (Exception ex) {
        throw ExceptionFactory.parse(ex);
      }
      conditionVO.setUnderwriterId(Long.valueOf(AppContext.getCurrentUser()
          .getUserId()));
      conditionVO.setConditionCode("0031");
      conditionVO.setUwListId(null);
      Collection conditionCo = uwPolicyDS.findUwConditionEntitis(productVO
          .getUnderwriteId());
      if (!conditionCo.isEmpty()) {
        Iterator conditonIt = conditionCo.iterator();
        while (conditonIt.hasNext()) {
          UwConditionVO conditonVO = (UwConditionVO) conditonIt.next();
          if (conditonVO.getConditionCode().equals("0031")) {
            isCreat = false;
          }
        }
      }
      if (isCreat) {
        uwPolicyDS.createUwCondition(conditionVO);
      }
    }
  }

  /**
   * added to check product preium term and coverage term (GEL00024716,request
   * by#6280),by robert.xu on 2007.5.18
   */
  @Override
  @PrdAPIUpdate
  public MultiWarning processWarning(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {
    UwRestrictionForm restrictForm = (UwRestrictionForm) form;
    
    MultiWarning warning = new MultiWarning();
    if (restrictForm.getProductId() == null) {
      return warning;
    }
    Long productId = Long.valueOf(restrictForm.getProductId().longValue());
    Long itemId = restrictForm.getItemId();
    CoverageVO policyProductVO = coverageCI.retrieveByItemId(itemId);
    Boolean isUnit  = ActionUtil.getUnitIndicator(itemId, productId,policyService.getActiveVersionDate(policyProductVO));
    if(isUnit){
    	restrictForm.setReducedUnit(new BigDecimal(request
    	        .getParameter("reducedAmount1")));
    	restrictForm.setReducedAmount(BigDecimal.ZERO);
    }else{
    	restrictForm.setReducedAmount(new BigDecimal(request
    	        .getParameter("reducedAmount1")));
    	restrictForm.setReducedUnit(BigDecimal.ZERO);
    }
    // get benefit attribute
    ProductVO benefitVO = this.getProductService().getProduct(productId,policyService.getActiveVersionDate(policyProductVO));
    //BenefitAttributeCIVO benefitVO = prdDefCI.getBenefitAttribute(productId);
    // check charge period, charge term, coverage period,coverage term base on
    // t_life_basic
    checkPolicyTerm(restrictForm, warning, productService, productId, benefitVO);
    // to check age limit
    checkPolicyAge(restrictForm, warning, productId, benefitVO);
    return warning;
  }

  /**
   * to check policy age limit
   * 
   * @param restrictForm
   * @param warning
   * @param api
   * @param productId
   * @param benefitVO
   * @throws GenericException
   * @throws Exception
   */
  @PrdAPIUpdate
  private void checkPolicyAge(UwRestrictionForm restrictForm,
      MultiWarning warning, Long productId, ProductVO benefitVO)
      throws GenericException, Exception {
    UwProductVO productVO = uwPolicyDS.findUwProduct(
        restrictForm.getUnderwriteId(), restrictForm.getItemId());
    LimitVO criteria = new LimitVO();
    criteria.setProductId(productId);
    criteria.setChargePeriodType(productVO.getChargePeriod());
    criteria.setChargeYear(productVO.getChargeYear());
    criteria.setCoveragePeriodType(productVO.getCoveragePeriod());
    criteria.setCoverageYear(productVO.getCoverageYear());
    criteria.setEndPeriodType(productVO.getEndPeriod());
    criteria.setEndYear(productVO.getEndYear());
    criteria.setGender(productVO.getGender1());
    criteria.setPayMode(productVO.getPayMode());
    criteria.setPayEnsure(productVO.getPayEnsure());
    criteria.setPayPeriodType(productVO.getPayPeriod());
    criteria.setPayYear(productVO.getPayYear());
    criteria.setInsuredStatus(productVO.getInsuredStatus()); 
    criteria.setBenefitLevel(productVO.getBenefitLevel());
    criteria.setProductVersionDate(policyService.getActiveVersionDate(productVO));
    
    AgeLimit ageLimit = this.getProductService().getAgeLimits(criteria);
    if (ageLimit != null) {
      Long maxNBAge = Long.valueOf(ageLimit.getMaxInsdNbExAge());
      if (maxNBAge != null && maxNBAge.compareTo(Long.valueOf(0)) > 0) {
        int maxAge = maxNBAge.intValue();
        String periodCalType = QueryUwDataSp.getCalType(productId,policyService.getActiveVersionDate(productVO));
        Date endDate = BizUtils.getEndDate(restrictForm.getUwCoveragePeriod(),
            Long.valueOf(restrictForm.getUwCoverageYear().longValue()),
            periodCalType, productVO.getValidateDate(),
            Long.valueOf(productVO.getAge1().longValue()), Long.valueOf(0));
        // Amend [productVO.getUwCoveragePeriod()],
        // Defect[GEL00040701]
        // 2008-3-14
        // Willis.Wang
        checkAgeLimit(warning, benefitVO, maxAge, productVO.getInsured1(),
            productVO.getUwCoveragePeriod(), endDate);
        if (productVO.getInsured2() != null) {
          // Amend [productVO.getUwCoveragePeriod()],
          // Defect[GEL00040701]
          // 2008-3-14
          // Willis.Wang
          checkAgeLimit(warning, benefitVO, maxAge, productVO.getInsured2(),
              productVO.getUwCoveragePeriod(), endDate);
        }
      }
    }
  }

  /**
   * check charge period, charge term, coverage period,coverage term base on
   * t_life_basic
   * 
   * @param restrictForm
   * @param warning
   * @param api
   * @param productId
   * @param benefitVO
   * @throws GenericException
   */
  protected void checkPolicyTerm(UwRestrictionForm restrictForm,
      MultiWarning warning, ProductService api, Long productId,
      ProductVO benefitVO) throws GenericException {
    boolean validPass = false;
    // For cover to certain age type, needn't check againt t_life_basic.
    // GEL00042372 ning.qi
    String uwChargePeriod = restrictForm.getUwChargePeriod();
    String uwCoveragePeriod = restrictForm.getUwCoveragePeriod();
    Long itemId = restrictForm.getItemId();
    Integer uwChargeYear = restrictForm.getUwChargeYear();
    Integer uwCoverageYear = restrictForm.getUwCoverageYear();
    if ("3".equals(uwChargePeriod) && "3".equals(uwCoveragePeriod)) {
      CoverageVO policyProductVO = coverageCI.retrieveByItemId(itemId);
      Integer entryAge = policyProductVO.getLifeInsured1().getEntryAge();
      if (entryAge.compareTo(uwChargeYear) >= 0
          || entryAge.compareTo(uwCoverageYear) >= 0) {
        StringBuffer waringMsg = new StringBuffer("Product ");
        waringMsg.append(benefitVO.getInternalId());
        waringMsg.append(" has invalid term parameters: premium term type (");
        waringMsg.append(restrictForm.getUwChargePeriod());
        waringMsg.append("); premium term (");
        waringMsg.append(restrictForm.getUwChargeYear());
        waringMsg.append("); coverage term type (");
        waringMsg.append(restrictForm.getUwCoveragePeriod());
        waringMsg.append("); coverage term (");
        waringMsg.append(restrictForm.getUwCoverageYear());
        waringMsg.append(")");
        warning.addWarning("ERR_10214020002", waringMsg.toString());
        warning.setContinuable(false);
        return;
      } else {
        return;
      }
    }
    // GEL00042372 end
    ProductBasicVarietySimpleVO criteria = new ProductBasicVarietySimpleVO();
    criteria.setCoveragePeriod(restrictForm.getUwCoveragePeriod());
    criteria.setCoverageYear(restrictForm.getUwCoverageYear());
    criteria.setChargePeriod(restrictForm.getUwChargePeriod());
    criteria.setChargeYear(restrictForm.getUwChargeYear());
    
    CoverageVO policyProductVO = coverageCI.retrieveByItemId(itemId);
    criteria.setProductVersionDate(policyService.getActiveVersionDate(policyProductVO));
    validPass = api.checkTermLimit(productId, criteria);

    if (!validPass) {
      StringBuffer waringMsg = new StringBuffer("Product ");
      waringMsg.append(benefitVO.getInternalId());
      waringMsg.append(" has invalid term parameters: premium term type (");
      waringMsg.append(restrictForm.getUwChargePeriod());
      waringMsg.append("); premium term (");
      waringMsg.append(restrictForm.getUwChargeYear());
      waringMsg.append("); coverage term type (");
      waringMsg.append(restrictForm.getUwCoveragePeriod());
      waringMsg.append("); coverage term (");
      waringMsg.append(restrictForm.getUwCoverageYear());
      waringMsg.append(")");
      warning.addWarning("ERR_10214020002", waringMsg.toString());
      warning.setContinuable(false);
    }
  }

  /**
   * @param warning
   * @param benefitVO
   * @param maxAge
   * @param insuredId
   * @param validateDate
   * @throws GenericException
   * @throws Exception
   */
  private void checkAgeLimit(MultiWarning warning, ProductVO benefitVO,
      int maxAge, Long insuredId, String uwCoveragePeriod, Date validateDate)
      throws GenericException, Exception {
    CustomerVO customerVO = customerCI.getPerson(insuredId);
    // Amend [add one more parameter: uwCoveragePeriod],
    // Defect[GEL00040701]
    // 2008-3-14
    // Willis.Wang
    int entryAge = QueryUwDataSp.getAgeMonth(benefitVO.getAgeMethod(),
        customerVO.getBirthday(), uwCoveragePeriod, validateDate);
    if (entryAge > maxAge * 12) {
      StringBuffer m_message = new StringBuffer("For product ");
      m_message.append(benefitVO.getInternalId());
      m_message.append(",the maximum insured newbiz expiry age should be:");
      m_message.append(maxAge);
      m_message.append(" years while current is ");
      m_message.append(convert2MonthYear(entryAge));
      warning.addWarning("ERR_10213020003", m_message.toString());
      warning.setContinuable(false);
    }
  }

  private String convert2MonthYear(int months) {
    StringBuffer str = new StringBuffer();
    if (months / 12 > 1) {
      str.append(months / 12).append(" years ");
    } else {
      if (months / 12 > 0) {
        str.append(months / 12).append(" year ");
      }
    }
    if (months % 12 > 1) {
      str.append(months % 12).append(" months");
    } else {
      if (months % 12 > 0) {
        str.append(months % 12).append(" month");
      }
    }
    return str.toString();
  }

  @Resource(name = UwPolicyService.BEAN_DEFAULT)
  private UwPolicyService uwPolicyDS;

  public void setUwPolicyDS(UwPolicyService uwPolicyDS) {
    this.uwPolicyDS = uwPolicyDS;
  }

  public UwPolicyService getUwPolicyDS() {
    return uwPolicyDS;
  }

  @Resource(name = CoverageCI.BEAN_DEFAULT)
  private CoverageCI coverageCI;

  public CoverageCI getCoverageCI() {
    return coverageCI;
  }

  public void setCoverageCI(CoverageCI coverageCI) {
    this.coverageCI = coverageCI;
  }

  @Resource(name = ProductService.BEAN_DEFAULT)
  private ProductService productService;

  public ProductService getProductService() {
    return productService;
  }

  public void setProductService(ProductService productService) {
    this.productService = productService;
  }

  @Resource(name = CustomerCI.BEAN_DEFAULT)
  private CustomerCI customerCI;

  /**
   * @return the customerCI
   */
  public CustomerCI getCustomerCI() {
    return customerCI;
  }

  /**
   * @param customerCI the customerCI to set
   */
  public void setCustomerCI(CustomerCI customerCI) {
    this.customerCI = customerCI;
  }
  
  @Resource(name = PolicyService.BEAN_DEFAULT)
  PolicyService policyService;
}
