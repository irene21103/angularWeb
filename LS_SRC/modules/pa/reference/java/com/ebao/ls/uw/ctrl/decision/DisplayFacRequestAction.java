package com.ebao.ls.uw.ctrl.decision;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.ls.escape.helper.EscapeHelper;
import com.ebao.ls.uw.ctrl.ActionUtil;
import com.ebao.ls.uw.ds.UwPolicyService;
import com.ebao.pub.framework.GenericAction;
import com.ebao.pub.framework.GenericException;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: eBaoTech Corporation</p>
 * @author mingchun.shi
 * @version 1.0
 */
public class DisplayFacRequestAction extends GenericAction {

  @Resource(name=UwPolicyService.BEAN_DEFAULT)
  private UwPolicyService uwPolicyService;
	
  /**
   * display the apply fac request
   * @param mapping
   * @param form
   * @param req
   * @param res
   * @return
   * @throws Exception
   */
  public ActionForward process(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse res)
      throws GenericException {
    String[] itemIds = EscapeHelper.escapeHtml(request.getParameterValues("itemId"));
    
    ActionUtil.setFacRequestList(uwPolicyService, request, itemIds);
     
    String underwriteId=EscapeHelper.escapeHtml(request.getParameter("underwriteId"));
     
    EscapeHelper.escapeHtml(request).setAttribute("underwriteId", underwriteId);
    
    return mapping.findForward("display");
  }


}
