package com.ebao.ls.uw.ds;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.ebao.foundation.app.sys.sysmgmt.ds.SysMgmtDSLocator;
import com.ebao.foundation.app.sys.sysmgmt.ds.UserVO;
import com.ebao.ls.foundation.service.GenericServiceImpl;
import com.ebao.ls.pa.pub.data.org.UwTransferDao;
import com.ebao.ls.pub.CodeCst;
import com.ebao.ls.pub.cst.UwTransferFlowType;
import com.ebao.ls.uw.bo.UwTransferRoleBO;
import com.ebao.ls.uw.data.UwTransferDelegate;
import com.ebao.ls.uw.data.UwTransferLevelLimitDelegate;
import com.ebao.ls.uw.data.UwTransferRoleDelegate;
import com.ebao.ls.uw.data.UwTransferRoleLimitDelegate;
import com.ebao.ls.uw.data.bo.UwTransfer;
import com.ebao.ls.uw.data.bo.UwTransferLevelLimit;
import com.ebao.ls.uw.data.bo.UwTransferRole;
import com.ebao.ls.uw.data.bo.UwTransferRoleLimit;
import com.ebao.ls.uw.vo.UwPolicyVO;
import com.ebao.ls.uw.vo.UwTransferLevelLimitVO;
import com.ebao.ls.uw.vo.UwTransferRoleLimitVO;
import com.ebao.ls.uw.vo.UwTransferRoleVO;
import com.ebao.ls.uw.vo.UwTransferVO;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.framework.GenericException;
import com.ebao.pub.security.Role;
import com.ebao.pub.util.StringUtils;

/**
 * <p>Title: 預收輸入-核保作業</p>
 * <p>Description: 核保陳核授權</p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company: TGL Co., Ltd.</p>
 * <p>Create Time: Mar 11, 2016</p> 
 * @author 
 * <p>Update Time: Mar 11, 2016</p>
 * <p>Updater: simon.huang</p>
 * <p>Update Comments: </p>
 */
public class UwTransferServiceImpl
		extends GenericServiceImpl<UwTransferVO, UwTransfer, UwTransferDelegate>
		implements UwTransferService {

	@Resource(name = UwTransferLevelLimitDelegate.BEAN_DEFAULT)
	UwTransferLevelLimitDelegate uwTransferLevelLimitDao;

	@Resource(name = UwTransferRoleLimitDelegate.BEAN_DEFAULT)
	UwTransferRoleLimitDelegate uwTransferRoleLimitDao;

	@Resource(name = UwTransferRoleDelegate.BEAN_DEFAULT)
	UwTransferRoleDelegate uwTransferRoleDao;

	@Resource(name = UwPolicyService.BEAN_DEFAULT)
	private UwPolicyService uwPolicyDS;

	/** 核保陳核角色表*/
	public UwTransferRoleBO uwTrnsfrRoleBO;

	/** 初始化 核保陳核角色表 PCR-480318*/
	@PostConstruct
	public void init() {
		uwTrnsfrRoleBO= uwTransferRoleDao.getRole();
	}

	@Override
	public boolean reLoadUwTransferRoleBO() {
		uwTrnsfrRoleBO = uwTransferRoleDao.getRole();
		if (null != uwTrnsfrRoleBO && uwTrnsfrRoleBO.size() > 0)
			return true;
		return false;
	}

	@Override
	public UwTransferRoleBO getUwTransferRoleBO() {
		return uwTrnsfrRoleBO;
	}

	@Override
	protected UwTransferVO newEntityVO() {
		return new UwTransferVO();
	}

	@Override
	public UwTransferVO createFirstlyUwTransfer(Long policyId, Long underwriteId) {

		Long currUser = AppContext.getCurrentUser().getUserId();
	
		UwTransferVO uwTransferVO = newEntityVO();

		uwTransferVO.setUnderwriteId(underwriteId);
		uwTransferVO.setInitUnderwriterId(currUser);
		uwTransferVO.setTransUnderwriterId(currUser);
		uwTransferVO.setCreateTime(AppContext.getCurrentUserLocalTime());
		uwTransferVO.setSubmitTime(null);
		uwTransferVO.setPolicyId(policyId);
		uwTransferVO.setFlowId(CodeCst.UW_TRANS_FLOW_FIRSTER);
		uwTransferVO = super.save(uwTransferVO);

		return uwTransferVO;
	}

	@Override
	public UwTransferVO findFirstlyUwTransfer(Long underwriteId) {
		
		List<Order> orders = getAscOrderList();
		
		List<UwTransfer> resultList = this.dao.findByCriteriaWithOrder(orders,
				Restrictions.eq("underwriteId", underwriteId),
				Restrictions.eq("flowId", CodeCst.UW_TRANS_FLOW_FIRSTER));
		
		UwTransferVO vo = null;
		for(UwTransfer uwTransfer : resultList){
			if(vo == null){
				vo = new UwTransferVO();
				uwTransfer.copyToVO(vo, false);
			} else {
				this.dao.remove(uwTransfer);
			}
		}
		return vo;
	}

	@Override
	public UwTransferVO findLastestCompleteUwTransfer(Long underwriteId) {
		
		List<Order> orders = getDescOrderList();
		
		List<UwTransfer> resultList = this.dao.findByCriteriaWithOrder(orders,
					Restrictions.eq("underwriteId", underwriteId)
				);
		
		if (resultList.size() > 0) {
			return convertToVO(resultList.get(0));
		}
		return null;
	}


	/**
	 * <p>Description : 查詢核保最新一筆待處理陳核記錄 by underwriteId<br/>
	 * 過濾 人工派工/加費(批註)簽核完成的資料
	 * </p>
	 * <p>Created By : simon.huang</p>
	 * <p>Create Time : Mar 18, 2016</p>
	 * @param underwriteId
	 * @return
	 */
	@Override
	public UwTransferVO findLastestWorkUwTransfer(Long underwriteId) {
		
		List<Order> orders = getDescOrderList();
		
		List<UwTransfer> resultList = this.dao.findByCriteriaWithOrder(orders,
				Restrictions.eq("underwriteId", underwriteId),
				Restrictions.not(Restrictions.in("flowId", new Object[]{
						CodeCst.UW_TRANS_FLOW_MANUAL_REASSIGN,
						CodeCst.UW_TRANS_FLOW_EXTRA_PREM_APPROVAL,
						CodeCst.UW_TRANS_FLOW_EXCLUSION_APPROVAL,
						CodeCst.UW_TRANS_FLOW_EXTRA_ALL_APPROVAL,
						CodeCst.UW_TRANS_FLOW_ROLE_FINISH
				})));
		
		if (resultList.size() > 0) {
			return convertToVO(resultList.get(0));
		}
		return null;
	}

	@Override
	public Map<Integer, UwTransferRoleLimitVO> findRoleLimitInfo(Long esclateRoleId) {

		Map<Integer, UwTransferRoleLimitVO> roleLimitMap = new LinkedHashMap<Integer, UwTransferRoleLimitVO>();
		Map<Long, UwTransferLevelLimitVO> levelLimitMap = findAllLevelLimit(); //查詢所有核保陳核權限層級表

		List<UwTransferRoleLimit> roleLimitList = uwTransferRoleLimitDao.findByProperty("roleId", esclateRoleId);
		for (UwTransferRoleLimit roleLimit : roleLimitList) {
			UwTransferRoleLimitVO roleLimitVO = new UwTransferRoleLimitVO();
			roleLimit.copyToVO(roleLimitVO, true);

			//設定陳核權限層級表VO
			roleLimitVO.setLimitVO(levelLimitMap.get(roleLimitVO.getLimitId()));
			roleLimitMap.put(roleLimit.getLimitTypeId(), roleLimitVO);
		}
		return roleLimitMap;
	}

	@Override
	public Map<Long, Map<Integer, UwTransferRoleLimitVO>> findAllRoleLimit() {

		Map<Long, Map<Integer, UwTransferRoleLimitVO>> allRoleLimitMap = new LinkedHashMap<Long, Map<Integer, UwTransferRoleLimitVO>>();
		Map<Long, UwTransferLevelLimitVO> levelLimitMap = findAllLevelLimit();//查詢所有核保陳核權限層級表

		List<UwTransferRoleLimit> roleLimitList = uwTransferRoleLimitDao.findAll();
		for (UwTransferRoleLimit roleLimit : roleLimitList) {

			Map<Integer, UwTransferRoleLimitVO> roleLimitMap = allRoleLimitMap.get(roleLimit.getRoleId());
			if (roleLimitMap == null) {
				roleLimitMap = new LinkedHashMap<Integer, UwTransferRoleLimitVO>();
				allRoleLimitMap.put(roleLimit.getRoleId(), roleLimitMap);
			}

			UwTransferRoleLimitVO roleLimitVO = new UwTransferRoleLimitVO();
			roleLimit.copyToVO(roleLimitVO, true);
			//設定陳核權限層級表VO
			roleLimitVO.setLimitVO(levelLimitMap.get(roleLimitVO.getLimitId()));
			roleLimitMap.put(roleLimit.getLimitTypeId(), roleLimitVO);
		}

		return allRoleLimitMap;
	}

	@Override
	public Map<Long, UwTransferLevelLimitVO> findAllLevelLimit() {
		Map<Long, UwTransferLevelLimitVO> levelLimitMap = new LinkedHashMap<Long, UwTransferLevelLimitVO>();
		List<UwTransferLevelLimit> levelLimitList = uwTransferLevelLimitDao.findAll();
		for (UwTransferLevelLimit levelLimit : levelLimitList) {
			UwTransferLevelLimitVO levelLimitVO = new UwTransferLevelLimitVO();
			levelLimit.copyToVO(levelLimitVO, true);
			levelLimitMap.put(levelLimit.getLimitId(), levelLimitVO);
		}
		return levelLimitMap;
	}

	@Override
	public UwTransferRoleVO findUwTransferRole(Long roleId) {
		UwTransferRole uwTransferRole = uwTransferRoleDao.findUniqueByProperty("roleId", roleId);
		UwTransferRoleVO uwTransferRoleVO = new UwTransferRoleVO();
		uwTransferRole.copyToVO(uwTransferRoleVO, false);
		return uwTransferRoleVO;
	}

	@SuppressWarnings("unchecked")
	@Override
	public UwTransferRoleVO findUwTransferRoleByUser(Long userId, UwTransferRoleBO bo) {

		List<Role> roleList = (List<Role>) Role.getRoles(userId, CodeCst.ROLE_TYPE__DATA,
				CodeCst.DATA_ROLE_TYPE__NBU_UW_ESCALATE);

		if (null == bo || bo.size() == 0)
			return null;

		// 比對核保人員陳核角色，要找最高職級角色
		int biggerIdx = -1;
		if (roleList.size() > 0)
			biggerIdx = bo.getIndx(roleList.get(0).getRoleId());
		if (bo.isMax(biggerIdx))
			return bo.retrieveMaxUwTransferRole();
		for (int i = 1; i < roleList.size(); i++) {
			int userRoleIdx = bo.getIndx(roleList.get(i).getRoleId());
			if (userRoleIdx > biggerIdx)
				biggerIdx = userRoleIdx;
		}
		if (biggerIdx > -1) {
			return bo.retrieveUwTransferRoleByIndex(biggerIdx);
		} else {
			return null;
		}
	}

	/**
	 * Description : 取得指定使用者所被賦予的的最大核保陳核角色<BR/>
	 * 核保陳核角色表(uwTrnsfrRoleBO)採用系統初始化時的物件<BR/>
	 * Update Time : Jun 6, 2022
	 * 
	 * @param userId 使用者
	 * @return 所被賦予的的最大核保陳核角色(擁有的陳核角色,要上報陳核角色)，若找不到匯回傳null
	 * PCR-480318
	 */
	@Override
	public UwTransferRoleVO findUwTransferRoleByUser(Long userId) {
		return findUwTransferRoleByUser(userId, this.uwTrnsfrRoleBO);
	}

	private List<Order> getAscOrderList(){
		List<Order> orders = new ArrayList<Order>();
		orders.add(Order.asc("insertTimestamp"));
		orders.add(Order.asc("listId")); // ASC 排序
		return orders;
	}
	

	private List<Order> getDescOrderList(){
		List<Order> orders = new ArrayList<Order>();
		orders.add(Order.desc("insertTimestamp"));
		orders.add(Order.desc("listId")); 
		return orders;
	}


	@Override
	public void saveReassignTask(Long underwriteId,
			String oldActorId, String newActorId) {

		UwPolicyVO uwPolicyVO = uwPolicyDS.findUwPolicy(underwriteId);

		/* 寫入一筆人工重派的記錄 */
		UwTransferVO reAssignRecordVO = newEntityVO();
		reAssignRecordVO.setUnderwriteId(underwriteId);
		reAssignRecordVO.setInitUnderwriterId(new Long(oldActorId));
		reAssignRecordVO.setTransUnderwriterId(new Long(newActorId));
		reAssignRecordVO.setCreateTime(AppContext.getCurrentUserLocalTime());
		reAssignRecordVO.setSubmitTime(AppContext.getCurrentUserLocalTime());
		reAssignRecordVO.setPolicyId(uwPolicyVO.getPolicyId());
		reAssignRecordVO.setFlowId(CodeCst.UW_TRANS_FLOW_MANUAL_REASSIGN);
		reAssignRecordVO = super.save(reAssignRecordVO);
		
		uwPolicyDS.updateUwPolicy(uwPolicyVO, false);
		
	}
	
	@Override
	public void saveReassignTask(UwTransferVO oldUwTransferVO,
			String oldActorId, String newActorId) {

		Long policyId = oldUwTransferVO.getPolicyId();
		Long underwriteId = oldUwTransferVO.getUnderwriteId();
		
		/* 寫入一筆人工重派的記錄 */
		UwTransferVO reAssignRecordVO = newEntityVO();

		reAssignRecordVO.setUnderwriteId(underwriteId);
		reAssignRecordVO.setInitUnderwriterId(new Long(oldActorId));
		reAssignRecordVO.setTransUnderwriterId(new Long(newActorId));
		reAssignRecordVO.setCreateTime(AppContext.getCurrentUserLocalTime());
		reAssignRecordVO.setSubmitTime(AppContext.getCurrentUserLocalTime());
		reAssignRecordVO.setPolicyId(policyId);
		reAssignRecordVO.setFlowId(CodeCst.UW_TRANS_FLOW_MANUAL_REASSIGN);
		reAssignRecordVO = super.save(reAssignRecordVO);
		
		if(oldUwTransferVO.getFlowId() == CodeCst.UW_TRANS_FLOW_FIRSTER){
			/* 初始核保員僅作欄位更新  */
			oldUwTransferVO.setInitUnderwriterId(new Long(newActorId));
			oldUwTransferVO.setTransUnderwriterId(new Long(newActorId));
			super.save(oldUwTransferVO);
			
			UwPolicyVO uwPolicyVO = uwPolicyDS.findUwPolicy(underwriteId);
			/* 更新初始核保人員ID */
			uwPolicyVO.setUnderwriterId(new Long(newActorId));
			//PCR-463250 綜合查詢-新契約資訊要新增顯示核保單位 2023/01/19 Add by Kathy
			UserVO userVO = SysMgmtDSLocator.getUserDS().getUserByUserID(uwPolicyVO.getUnderwriterId());
			uwPolicyVO.setUwDeptId(Long.parseLong(userVO == null ? null : userVO.getDeptId()));  			
			uwPolicyDS.updateUwPolicy(uwPolicyVO, false);
		} else {

			/* 以舊陳核記錄複製一份出來  (最後一筆是人工陳核回覆關閉，不作處理) */
			if(oldUwTransferVO.isResponseClose() == false){
				
				UwTransferVO newUwTransferVO = oldUwTransferVO.clone();	
				newUwTransferVO.setListId(null);
				newUwTransferVO.setTransComment(null);
				if(oldUwTransferVO.isResponseOpen()){
					newUwTransferVO.setInitUnderwriterId(new Long(newActorId));
				} else {
					newUwTransferVO.setTransUnderwriterId(new Long(newActorId));	
				}
				newUwTransferVO = super.save(newUwTransferVO);
			}
			
			/* 關閉舊陳核記錄 */
			if(oldUwTransferVO.getSubmitTime() == null){
				oldUwTransferVO.setSubmitTime(AppContext.getCurrentUserLocalTime());
				//寫入刪除註記
				String comment = StringUtils.nullToEmpty(oldUwTransferVO.getTransComment());
				oldUwTransferVO.setTransComment(CodeCst.UW_TRANS_FLOW_DELETE_MARK + comment);
				super.save(oldUwTransferVO);
			}
			
		}
	}

	/**
	 * 判斷人工核保件可否轉派
	 * @param policyId
	 * @return
	 * @throws GenericException
	 */
	public boolean isUwTransferReassignByPolicyId(Long policyId) throws GenericException {
		
		return new UwTransferDao().isUwTransferReassignByPolicyId(policyId);
	}

	@Override
	public int compareUwTransferRole(UwTransferRoleVO role1, UwTransferRoleVO role2) {
		if (role1 == null && role2 == null) {
			return 0;
		} else if (role1 == null && role2 != null) {
			return -1;
		} else if (role1 != null && role2 == null) {
			return 1;
		} else {
			int role1Indx = this.uwTrnsfrRoleBO.getIndx(role1.getRoleId(), true);
			int role2Indx = this.uwTrnsfrRoleBO.getIndx(role2.getRoleId(), true);
			return Integer.compare(role1Indx, role2Indx);
		}
	}

    @Override
    public List<UwTransferVO> getUwTransferByPolicyID(Long policyId) {
        if (policyId == null) {
            return Collections.emptyList();
        }

        Criteria criteria = getDao().getCriteria(null, Collections.EMPTY_MAP, null);

        criteria.add(Restrictions.eq("policyId", policyId));
        criteria.add(Restrictions.in("flowId", Arrays.asList(
                UwTransferFlowType.UW_TRANS_FLOW_ESCALATE_REQUEST,
                UwTransferFlowType.UW_TRANS_FLOW_MANAGER))
        );
        criteria.addOrder(Order.asc("underwriteId"));
        criteria.addOrder(Order.asc("listId"));

        return convertToVOList(criteria.list());
    }

    @Override
    public List<UwTransferVO> getUwTransferByUNB0631Condition(Long policyId) {
        if (policyId == null) {
            return Collections.emptyList();
        }

        Criteria criteria = getDao().getCriteria(null, Collections.EMPTY_MAP, null);

        criteria.add(Restrictions.eq("policyId", policyId));
        criteria.add(Restrictions.isNotNull("submitTime"));
        criteria.add(Restrictions.in("flowId", Arrays.asList(
                UwTransferFlowType.UW_TRANS_FLOW_ESCALATE_RESPONSE,
                UwTransferFlowType.UW_TRANS_FLOW_MANAGER,
                UwTransferFlowType.UW_TRANS_FLOW_EXTRA_PREM_MANAGER))
        );
        criteria.add(Restrictions.or(
                Restrictions.isNull("transComment"),
                Restrictions.not(Restrictions.like("transComment", UwTransferFlowType.UW_TRANS_FLOW_DELETE_MARK, MatchMode.START))
        ));
        criteria.addOrder(Order.asc("listId"));

        return convertToVOList(criteria.list());
    }

}
