package com.ebao.ls.callout.batch.task;

import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.ApplicationContextAware;

import com.ebao.foundation.common.lang.DateUtils;
import com.ebao.foundation.common.lang.exception.ExceptionUtils;
import com.ebao.foundation.core.logging.Log;
import com.ebao.ls.callout.batch.AbstractCalloutTask;
import com.ebao.ls.callout.batch.CalloutConstants;
import com.ebao.ls.callout.batch.CalloutDataExportService;
import com.ebao.pub.batch.help.BatchHelp;
import com.ebao.pub.batch.type.JobStatus;
import com.ebao.pub.batch.type.LogLevel;
import com.ebao.pub.batch.util.BatchLogUtils;
import com.ebao.pub.util.TransUtils;

public class UNB0807TheSameComTask extends AbstractCalloutTask implements ApplicationContextAware {
    public static final String BEAN_DEFAULT = "zeroEightZeroSevenTask";
    
    private final Log log = Log.getLogger(UNB0807TheSameComTask.class);
    
    public final static Long JOB_ID = 1332329L;
    
    private final Long DISPLAY_ORDER = 7L;

    private String CONFIG_TYPE = "1";// 080抽樣比例
    
    @Resource(name=CalloutDataExportService.BEAN_DEFAULT)
    private CalloutDataExportService exportServ;
    
    @Override
    public int mainProcess() throws Exception {
        List<Map<String, Object>> mapList = null;
        UserTransaction ut = TransUtils.getUserTransaction();
        try{
            ut.begin();
            /* 保單抽樣 */
            mapList = unbSampling(
                        "[BR-CMN-TFI-005][檔案080-7 同公司有效契約(分期且年繳保費>=10萬)的保單]",
                        CalloutConstants.ZeroEightZeroSeven, CONFIG_TYPE, DISPLAY_ORDER, "080-7", "080");
            /* 匯出抽樣保單 */
            exportServ.feedMap(mapList, CalloutConstants.ZeroEightZeroSeven, BatchHelp.getProcessDate());
            ut.commit();
            if(CollectionUtils.isEmpty(mapList)){
                return JobStatus.EXECUTE_SUCCESS;
            }
        } catch (Exception ex) {
            try {
                ut.rollback();
            } catch (SystemException sysEx) {
                BatchLogUtils.addLog(LogLevel.ERROR, null,ExceptionUtils.getFullStackTrace(sysEx));
            }
            BatchLogUtils.addLog(LogLevel.ERROR, null,ExceptionUtils.getFullStackTrace(ex));
            return JobStatus.EXECUTE_FAILED;
        }
        
        
        return JobStatus.EXECUTE_SUCCESS;
    }

    public java.util.List<Map<String, Object>> getData(Date inputDate, String policyCode){
        java.util.List<java.util.Map<String,Object>> mapList  = new java.util.LinkedList<Map<String,Object>>();
        
        final java.sql.Date processDate = new java.sql.Date(inputDate.getTime());
        final java.sql.Date nextDate = new java.sql.Date(DateUtils.addDay(inputDate, 1).getTime());
        java.util.Date monthDate = DateUtils.truncateMonth(DateUtils.addMonth(inputDate,-1));
        final java.sql.Date monthSqlDate = new java.sql.Date(monthDate.getTime());
        
        String paraVal = com.ebao.pub.para.Para.getParaValue(3060010020L);
        java.util.Date paraValDate;
        try {
            paraValDate = org.apache.commons.lang3.time.DateUtils.parseDate(paraVal, "yyyyMMdd");
        } catch (ParseException e) {
            java.util.Calendar calendar = java.util.Calendar.getInstance();
            calendar.set(2018, 3, 1);
            paraValDate= calendar.getTime();
        }
        final java.sql.Date initDate = DateUtils.toSqlDate(paraValDate);
        //#370720 投資型保單，[下次期繳保費]和[下次表定保費]，兩者取其大 
        StringBuffer sql = new StringBuffer();
        sql.append("select distinct mail.policy_id, ");
        sql.append("        mail.policy_code, ");
        sql.append("        prd.internal_id, ");
        sql.append("        tcp.product_version_id, ");
        sql.append("        ph.name ph_name, ");
        sql.append("       	ph.roman_name ph_roman_name, ");
        sql.append("        ph.certi_code ph_certi_code, ");
        sql.append("        ph.gender ph_gender, ");
        sql.append("        ph.birth_date ph_birthday, ");
        sql.append("      	decode(ph.office_tel_reg, null, null, '(' || ph.office_tel_reg || ')') || ph.office_tel || ");
        sql.append("        decode(ph.office_tel_ext, null, null, '#' || ph.office_tel_ext) ph_office_tel, ");
        sql.append("        decode(ph.home_tel_reg, null, null, '(' || ph.home_tel_reg || ')') || ph.home_tel || ");
        sql.append("        decode(ph.home_tel_ext, null, null, '#' || ph.home_tel_ext) ph_home_tel, ");
        sql.append("        ph.mobile_tel ph_phone1, ");
        sql.append("        ph.mobile_tel2 ph_phone2, ");
        sql.append("       (SELECT a.address_1 from t_address a WHERE a.address_id = ph.address_id) ph_home_addr,  ");
        sql.append("       lr.name lr_name,   ");
        sql.append("       	lr.roman_name lr_roman_name,   ");
        sql.append("       lr.certi_code lr_certi_code,   ");
        sql.append("       lr.gender lr_gender,   ");
        sql.append("       lr.BIRTH_DATE lr_birthday,   ");
        sql.append("       lr.rel_to_ph_ins rel_to_ph,   ");  
        sql.append("       lr2.rel_to_ph_ins rel_to_ins,   ");
        sql.append("       decode(lrc.office_tel_reg, null, null, '(' || lrc.office_tel_reg || ')') || lrc.office_tel || decode(lrc.office_tel_ext, null, null, '#' || lrc.office_tel_ext) lr_office_tel,   ");
        sql.append("       decode(lrc.home_tel_reg, null, null, '(' || lrc.home_tel_reg || ')') || lrc.home_tel || decode(lrc.home_tel_ext, null, null, '#' ||  lrc.home_tel_ext) lr_home_tel,  ");
        sql.append("       lrc.MOBILE lr_phone1,  ");
        sql.append("       lrc.mobile2 lr_phone2,  ");
        sql.append("       tcp.charge_period charge_period,  ");
        sql.append("       tcp.charge_year charge_year,  ");
        sql.append("        belst.name i_name, ");
        sql.append("        belst.certi_code i_certi_code, ");
        sql.append("        be.entry_age i_age, ");
        sql.append("        mail.apply_date apply_date, ");
        sql.append("        mail.validate_date validate_date, ");
        sql.append("        mail.issue_date issue_date, ");
        sql.append("        mail.submission_date submission_date, ");
        sql.append("        mail.dispatch_date dispatch_date, ");
        sql.append("        mail.acknowledge_date reply_date, ");
        sql.append("       mail.receive_date receive_date,  ");
        sql.append("       mail.tracking_number tracking_number,  ");//PCR-415241 保單郵寄掛號/國際快捷/宅配號碼
        sql.append("        send_type, ");
        sql.append("       pkg_pub_cmn_utils.f_get_ilp_charge_type(tcp.PRODUCT_ID ) AS ilp_charge_type,  ");
        sql.append("        pkg_ls_pub_util.f_get_code_mutilang_desc('T_LIABILITY_STATUS', mail.liability_state, '311') state, ");
        sql.append("        pkg_ls_pub_util.f_get_code_mutilang_desc('T_MONEY', prd.base_money , '311') money_name, ");
        sql.append("        pkg_ls_sc_callout.f_get_cm_prod_rate(prd.base_money, mail.issue_date) exchange_rate, ");
        sql.append("        pkg_ls_pub_util.f_get_code_mutilang_desc('V_NBU_CALLOUT_CHARGE_MODE', tcp.RENEWAL_TYPE, '311') charge_name, ");
        sql.append("        pkg_ls_sc_callout.f_get_annual_prem(mail.policy_id) annual_prem, ");
        sql.append("        pkg_ls_sc_callout.f_get_annual_prem(mail.policy_id) * pkg_ls_sc_callout.f_get_cm_prod_rate(prd.base_money, mail.issue_date) std_tw_prem, ");
        sql.append("        nvl(tcp.customized_prem,0) customized_prem, ");
        sql.append("        pkg_ls_sc_callout.f_get_contract_amount(tcp.item_id) amount, ");
        sql.append("        pkg_ls_sc_callout.f_get_contract_amount_unit(tcp.item_id) amount_unit, ");
        sql.append("        prd.big_prodCate, ");
        sql.append("        prd.small_prodcate, ");
        sql.append("        pkg_ls_sc_callout.f_get_each_prem(mail.policy_id) each_prem , ");
        sql.append("        pkg_pub_cmn_utils.f_is_purpose(mail.policy_id, '1') ensure_require, ");
        sql.append("        pkg_pub_cmn_utils.f_is_purpose(mail.policy_id, '2') retire_require, ");
        sql.append("        pkg_pub_cmn_utils.f_is_purpose(mail.policy_id, '3') child_fee, ");
        sql.append("        pkg_pub_cmn_utils.f_is_purpose(mail.policy_id, '4') asset, ");
        sql.append("        pkg_pub_cmn_utils.f_is_purpose(mail.policy_id, '5') house ");
        sql.append("  from (select * ");
        sql.append("         from v_callout_mail m ");
        sql.append("        where 1=1 ");
        if(StringUtils.isNotEmpty(policyCode)) {
        	sql.append("and m.policy_code = :policy_code ");
        }
        sql.append("          and m.issue_date >= :init_date ");
        sql.append("          and m.dispatch_date >= :month_date ");
        sql.append("          and m.dispatch_date <= :process_date ");
        sql.append("and not exists ");
        sql.append("         ( select 1 ");
        sql.append("            from t_callout_batch cb ");
        sql.append("            join t_callout_config cfg on cfg.list_id = cb.config_type ");
        sql.append("           where 1=1 ");
        sql.append("           and cb.policy_code = m.policy_code ");
        sql.append("           and cfg.config_type = 1 ");
        sql.append("           and display_order = 7) ");
        sql.append("  ) mail ");
        sql.append("  join t_benefit_insured_log be ");
        sql.append("   on be.policy_id = mail.policy_id ");
        sql.append("   and be.entry_age > 69 ");
        sql.append("   and be.order_id=1 ");
        sql.append("   and be.last_cmt_flg='Y' ");
        sql.append("  join t_contract_product_log tcp ");
        sql.append("   on tcp.policy_id=mail.policy_id ");
        sql.append("   and tcp.item_id = be.item_id ");
        sql.append("   and tcp.master_id is null ");
        sql.append("   and tcp.renewal_type != 5 ");
        sql.append("   and tcp.last_cmt_flg='Y' ");
        sql.append("  join v_callout_prod_loan prd ");
        sql.append("   on prd.product_id = tcp.product_id ");
        sql.append("  join t_insured_list_log belst ");
        sql.append("   on be.insured_id = belst.list_id ");
        sql.append("   and belst.last_cmt_flg='Y' ");
        sql.append("  join t_policy_holder_log ph ");
        sql.append("   on ph.policy_id = mail.policy_id ");
        sql.append("   and ph.last_cmt_flg='Y' ");
        //與要保人關係
        sql.append("  LEFT join t_legal_representative_log lr   ");
		sql.append("  on lr.policy_id = ph.policy_id AND lr.rel_id = ph.list_id and lr.rel_type=1 AND lr.last_cmt_flg = 'Y'  ");
    	sql.append("  LEFT join t_legal_representative_log lr2   ");
    	//與被保人關係
    	sql.append("  on lr2.policy_id = ph.policy_id AND lr2.rel_id =  ph.list_id and lr2.rel_type=2 AND lr2.last_cmt_flg = 'Y'  ");
		sql.append("  LEFT JOIN t_customer lrc  ");
		sql.append("  ON lr.party_id = lrc.customer_id ");
        sql.append("  where 1=1 ");
        sql.append("  and exists ( ");
        sql.append("       select sum( 12 * greatest(nvl(tcp2.customized_prem,0) , nvl(tcp2.next_gross_prem_af,0)) * pkg_ls_sc_callout.f_get_cm_prod_rate(pd2.base_money, tcm.issue_date)) ");
        sql.append("        from t_benefit_insured_log be2 ");
        sql.append("        join t_insured_list_log belst2 ");
        sql.append("         on be2.insured_id= belst2.list_id ");
        sql.append("         and belst2.certi_code= belst.certi_code ");
        sql.append("         and be2.entry_age <= be.entry_age ");
        sql.append("         and be2.last_cmt_flg='Y' ");
        sql.append("         and belst2.last_cmt_flg='Y' ");
        sql.append("        join t_contract_product_log tcp2 ");
        sql.append("         on tcp2.item_id=be2.item_id ");
        sql.append("         and tcp2.renewal_type != 5 ");
        sql.append("         and tcp2.last_cmt_flg='Y' ");
        sql.append("         and tcp2.paidup_date > :process_date ");
        sql.append("         and tcp2.liability_state = 1 ");
        sql.append("        join t_contract_master_log tcm ");
        sql.append("         on tcm.policy_id=tcp2.policy_id ");
        sql.append("         and tcm.last_cmt_flg='Y' ");
        sql.append("        join t_product_life pd2 ");
        sql.append("         on pd2.product_id=tcp2.product_id ");
        sql.append("        join t_charge_mode cmod ");
        sql.append("         on cmod.charge_type = tcp2.renewal_type ");
        sql.append("         and cmod.months > 0 ");
        sql.append("       where exists (select 1 ");
        sql.append("                     from t_contract_product_log tcp3 ");
        sql.append("                     join t_product_life prd3 ");
        sql.append("                      on tcp3.item_id = tcp2.item_id ");
        sql.append("                      and tcp3.product_id = prd3.product_id ");
        sql.append("                      and tcp3.last_cmt_flg = 'Y' ");
        sql.append("                 where exists (select 1 ");
        sql.append("                                from t_contract_product_log tcp4 ");
        sql.append("                               join v_callout_prod_loan prdLoan ");
        sql.append("                                on prdLoan.product_id = tcp4.product_id ");
        sql.append("                                and  tcp4.last_cmt_flg='Y' ");
        sql.append("                                and tcp3.policy_id = tcp4.policy_id ");
        sql.append("                                and tcp4.master_id is null) ");
        sql.append("           ) ");
        sql.append("     having sum( 12 * greatest(nvl(tcp2.customized_prem,0), nvl(tcp2.next_gross_prem_af,0) )/cmod.months * pkg_ls_sc_callout.f_get_cm_prod_rate(pd2.base_money, tcm.issue_date)) >= 100000 ");
        sql.append("  ) ");
        sql.append("  and not exists( ");
        sql.append("  select policy_code from t_callout_batch cb ");
        sql.append("      where cb.policy_code = mail.policy_code ");
        sql.append("      and cb.callout_file_name like '080-2_%'  and to_char(cb.batch_time, 'yyyy/MM') = to_char( mail.dispatch_date ,'yyyy/MM') ");
        sql.append("  union all ");
        sql.append("  select policy_code from t_callout_batch cb ");
        sql.append("      where cb.policy_code = mail.policy_code ");
        sql.append("      and cb.callout_file_name like '080-3_%'  and to_char(cb.batch_time, 'yyyy/MM') = to_char( mail.dispatch_date ,'yyyy/MM') ");
        sql.append("  union all ");
        sql.append("  select policy_code from t_callout_batch cb ");
        sql.append("      where cb.policy_code = mail.policy_code ");
        sql.append("      and cb.callout_file_name like '080-4_%'  and to_char(cb.batch_time, 'yyyy/MM') = to_char( mail.dispatch_date ,'yyyy/MM') ");
        sql.append("  union all ");
        sql.append("  select policy_code from t_callout_batch cb ");
        sql.append("      where cb.policy_code = mail.policy_code ");
        sql.append("      and cb.callout_file_name like '080-5_%'  and to_char(cb.batch_time, 'yyyy/MM') = to_char( mail.dispatch_date ,'yyyy/MM') ");
        sql.append("  ) ");
        sql.append("  order by mail.policy_code ");
        
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("process_date", processDate);
        params.put("init_date", initDate);
        params.put("month_date", monthSqlDate);
        
		BatchLogUtils.addLog(LogLevel.INFO, null,
				String.format("process_date := %s, month_date := %s,  next_date := %s, policy_code :=%s ",
						DateUtils.date2String(processDate, "yyyy/MM/dd"),
						DateUtils.date2String(monthDate, "yyyy/MM/dd"),
						DateUtils.date2String(nextDate, "yyyy/MM/dd"),
						policyCode));
        if(StringUtils.isNotEmpty(policyCode)) {
        	params.put("policy_code", policyCode);
        }
        mapList = nt.queryForList(sql.toString(), params);
        return combineDataByPolicyCode(mapList);
    }
    @Override
    public String saveToTrans(Map<String, Object> map, Date processDate,
                    String fileName, String dept) {
        String calloutNum = savePolicyHolder(map, processDate, fileName, dept);
        return calloutNum;
    }
}
