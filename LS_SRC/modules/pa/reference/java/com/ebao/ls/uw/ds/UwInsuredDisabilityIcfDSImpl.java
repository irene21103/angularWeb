package com.ebao.ls.uw.ds;

import java.util.List;

import javax.annotation.Resource;

import com.ebao.ls.pa.pub.bs.InsuredDisabilityIcfService;
import com.ebao.ls.pa.pub.bs.InsuredDisabilityListService;
import com.ebao.ls.pa.pub.vo.InsuredDisabilityDisplayVO;
import com.ebao.ls.pa.pub.vo.InsuredDisabilityIcfVO;
import com.ebao.ls.pa.pub.vo.InsuredDisabilityListVO;
import com.ebao.ls.pub.CodeCst;
import com.ebao.ls.uw.data.TUwInsuredDisabilityIcfDelegate;
import com.ebao.ls.uw.data.bo.UwInsuredDisability;
import com.ebao.ls.uw.data.bo.UwInsuredDisabilityIcf;
import com.ebao.ls.uw.vo.UwLifeInsuredVO;
import com.ebao.pub.framework.GenericDS;

public class UwInsuredDisabilityIcfDSImpl extends GenericDS implements UwInsuredDisabilityIcfService {

	@Resource(name = TUwInsuredDisabilityIcfDelegate.BEAN_DEFAULT)
	private TUwInsuredDisabilityIcfDelegate uwInsuredDisabilityIcfDao;
	
    @Resource(name = UwInsuredDisabilityService.BEAN_DEFAULT)
    private UwInsuredDisabilityService uwInsuredDisabilityService;
	
    @Resource(name = UwPolicyService.BEAN_DEFAULT)
    private UwPolicyService uwPolicyDS;
    
	@Resource(name = InsuredDisabilityListService.BEAN_DEFAULT)
	protected InsuredDisabilityListService insuredDisabilityListService;
	
	@Resource(name = InsuredDisabilityIcfService.BEAN_DEFAULT)
	protected InsuredDisabilityIcfService insuredDisabilityIcfService;

	@Override
	public UwInsuredDisabilityIcf create(UwInsuredDisability uwDisability, String disabilityLevel) {
		UwInsuredDisabilityIcf uwIcf = new UwInsuredDisabilityIcf();
		uwIcf.setUwInsuredDisability(uwDisability);
	    uwIcf.setDisabilityLevel(disabilityLevel);
	    uwInsuredDisabilityIcfDao.create(uwIcf);
	    return uwIcf;
	}

	@Override
	public void remove(long uwIcfListId) {
		uwInsuredDisabilityIcfDao.remove(uwIcfListId);
	}
	
	@Override
	public void remove(UwInsuredDisabilityIcf entity) {
		uwInsuredDisabilityIcfDao.remove(entity);
	}

	@Override
	public void update(UwInsuredDisabilityIcf entity) {
		uwInsuredDisabilityIcfDao.update(entity);
	}

	@Override
	public UwInsuredDisabilityIcf load(long uwIcfListId) {
		return uwInsuredDisabilityIcfDao.load(uwIcfListId);
	}

	@Override
	public void update(long uwIcfListId, String disabilityLevel) {
		UwInsuredDisabilityIcf icf = load(uwIcfListId);
		icf.setDisabilityLevel(disabilityLevel);
		update(icf);
	}
	
	@Override
	public UwInsuredDisabilityIcf findByDisabilityCodeAndIcf(long underwriteId, long insuredListId, String disabilityCode, String disabilityLevel) {
		UwLifeInsuredVO uwLifeInsuredVO = uwPolicyDS.findUwLifeInsuredByListId(underwriteId, insuredListId);
		UwInsuredDisabilityIcf icf = uwInsuredDisabilityIcfDao.findByDisabilityCodeAndIcf(uwLifeInsuredVO.getUwListId(), disabilityCode, disabilityLevel);
		return icf;
	}
	
	@Override
	public boolean findByDisabilityCodeAndNotExistsIcf(long underwriteId, long insuredListId, String disabilityCode) {
		UwLifeInsuredVO uwLifeInsuredVO = uwPolicyDS.findUwLifeInsuredByListId(underwriteId, insuredListId);
		List<UwInsuredDisabilityIcf> result = uwInsuredDisabilityIcfDao.findByDisabilityCodeAndNotExistsIcf(uwLifeInsuredVO.getUwListId(), disabilityCode, CodeCst.disabilityTypeNoICF);
		if (result.size()>0) {
			return true;
		}else {
			return false;
		}
	}
	

	@Override
	public InsuredDisabilityDisplayVO convertToDisplayVOByUw(UwInsuredDisabilityIcf icf) {
		InsuredDisabilityDisplayVO vo = new InsuredDisabilityDisplayVO();
		vo.setDisabilityPk(icf.getUwInsuredDisability().getListId());
		vo.setDisabilityType(icf.getUwInsuredDisability().getDisabilityType());
		vo.setIcfPk(icf.getUwIcfListId());
		vo.setDisabilityLevel(icf.getDisabilityLevel());
		boolean existsFinMsrIcf = insuredDisabilityIcfService.existsFinMsrIcf(vo.getDisabilityType(), vo.getDisabilityLevel());
		vo.setExistsIcf(existsFinMsrIcf?"Y":"N");
		return vo;
	}
	
	/**
	 * 共用刪除ICF編碼(核保/被保人)
	 * @param underwriteId
	 * @param insuredListId
	 * @param icfs
	 */
	private void deleteUwIcfCommon(long underwriteId, long insuredListId, String[] icfs, long changeId, long policyChgId) {
		UwLifeInsuredVO uwLife = uwPolicyDS.findUwLifeInsuredByListId(underwriteId, insuredListId);

		for (String pk : icfs) {
			//核保ICF編碼
			UwInsuredDisabilityIcf uwIcf = load(Long.parseLong(pk));
			String disabilityCode = uwIcf.getUwInsuredDisability().getDisabilityType();
			remove(uwIcf);
			
			if (policyChgId!=-1) {
				//被保人ICF編碼
				InsuredDisabilityIcfVO icf = insuredDisabilityIcfService.findByDisabilityCodeAndIcf(uwLife.getListId(), disabilityCode, uwIcf.getDisabilityLevel());
				insuredDisabilityIcfService.remove(changeId, policyChgId, icf.getIcfListId());				
			}
		}

		//檢查都無ICF編碼時，需同步刪除身心障礙類別
		List<UwInsuredDisability> uwDisList = uwInsuredDisabilityService.findByUwInsuredId(uwLife.getUwListId());
		for (UwInsuredDisability uwDis : uwDisList) {
			if (uwDis.getUwInsuredDisabilityIcfs().size() == 0) {
				//刪除核保身心障礙類別
				uwInsuredDisabilityService.removeByUwInsuredIdDisabilityCode(uwLife.getUwListId(), uwDis.getDisabilityType());
			
				if (policyChgId!=-1) {
					//刪除被保人身心障礙類別
					InsuredDisabilityListVO dis = insuredDisabilityListService.findByDisabilityCode(insuredListId, uwLife.getPolicyId(), uwDis.getDisabilityType());
					insuredDisabilityListService.remove(changeId, policyChgId, dis.getDisabilityListId());					
				}
			}
		}
	}
	
	@Override
	public void deleteUwIcf(long underwriteId, long insuredListId, String[] icfs) {
		deleteUwIcfCommon(underwriteId, insuredListId, icfs, -1, -1);
	}

	@Override
	public void deleteUwIcf(long underwriteId, Long insuredListId, String[] icfs, long changeId, long policyChgId) {
		deleteUwIcfCommon(underwriteId, insuredListId, icfs, changeId, policyChgId);
	}
	
	@Override
	public InsuredDisabilityDisplayVO insertUwIcf(long underwriteId, Long insuredListId, String disabilityCode, String disabilityLevel, boolean noICF) {
		UwInsuredDisability dis = uwInsuredDisabilityService.findByUwInsuredIdDisabilityCode(underwriteId, insuredListId, disabilityCode);

		if (dis==null) {
			//被保人身心障礙類別
			UwLifeInsuredVO uwLife = uwPolicyDS.findUwLifeInsuredByListId(underwriteId, insuredListId);
			dis = uwInsuredDisabilityService.create(uwLife.getUwListId(), disabilityCode);
		}

		//依需求「無」：99999999，不定義在T_FIN_MSR_DISABILITY_ICF。
		if (noICF) {
			disabilityLevel = CodeCst.disabilityTypeNoICF; 
		}

		//被保人ICF編碼
		UwInsuredDisabilityIcf icf = create(dis, disabilityLevel);
		InsuredDisabilityDisplayVO displayVO = convertToDisplayVOByUw(icf);
		return displayVO;
	}

	
}