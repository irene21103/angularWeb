package com.ebao.ls.liaRoc.ctrl;

import org.apache.struts.upload.FormFile;

import com.ebao.pub.web.pager.PagerFormImpl;

/**
 * <h1>公會延遲報表上傳</h1><p>
 * @since 2022/05/19<p>
 * @author Simon Huang
 * <p>Update Time: </p>
 * <p>Updater: </p>
 * <p>Update Comments: </p>
*/
@SuppressWarnings("serial")
public class UploadLiaRocDelayReportForm extends PagerFormImpl {

	private String actionType;

	private Long reportId ; 
	
	private String reportName;

	private String liaRocType;

	private FormFile uploadFile;

	public Long getReportId() {
		return reportId;
	}

	public void setReportId(Long reportId) {
		this.reportId = reportId;
	}

	public String getReportName() {
		return reportName;
	}

	public void setReportName(String reportName) {
		this.reportName = reportName;
	}

	public String getLiaRocType() {
		return liaRocType;
	}

	public void setLiaRocType(String liaRocType) {
		this.liaRocType = liaRocType;
	}

	public FormFile getUploadFile() {
		return uploadFile;
	}

	public String getActionType() {
		return actionType;
	}

	public void setActionType(String actionType) {
		this.actionType = actionType;
	}

	public void setUploadFile(FormFile uploadFile) {
		this.uploadFile = uploadFile;
	}

}
