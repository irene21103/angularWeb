package com.ebao.ls.uw.ctrl.leaveUwAuth;

import com.ebao.ls.pub.leave.vo.DataentryVerifConfVO;
import com.ebao.ls.uw.ds.vo.LeaveAuthEdit;
import com.ebao.pub.framework.GenericForm;

/**
 * <p>Title: Module Information Andy GOD</p>
 * <p>Description: Function Description Only vegetable</p>
 * <p>Copyright: Copyright (c) 2015</p>
 * <p>Company: TGL Co., Ltd.</p>
 * <p>Create Time: Aug 6, 2015</p> 
 * @author 
 * <p>Update Time: Aug 6, 2015</p>
 * <p>Updater: Alex Cheng</p>
 * <p>Update Comments: </p>
 */
public class LeaveAuthForm extends GenericForm {

  private static final long serialVersionUID = 7560275134473822767L;

  //mode type
  private String actionType;
  
  private String leaveUwAuthListId;
  
  private String editEmpId;
  
  private String masterEmpId;
  
  private LeaveAuthEdit leaveAuthEdit = new LeaveAuthEdit();
  
  private String retMsg;
  
  private String searchType;
  
  private String deptId;
  
  private String sectionDeptId;
  
  private String empName;
  
  private String empId;
  
  private String channelType;
  
  private String channelId;
  
  private Boolean isEditRole;
  
  private String orgId;
  
  //預收與覆核人員設置作業權限
  private Boolean isDataEntryRole;
  //覆核/主管設置作業權限
  private Boolean isVerifManagerRole;
  //轄區設置作業權限
  private Boolean isUserAreaRole;
  
  private DataentryVerifConfVO dataentryVerifConf = new DataentryVerifConfVO();

  public LeaveAuthEdit getLeaveAuthEdit() {
    if(leaveAuthEdit == null){
      this.setLeaveAuthEdit(new LeaveAuthEdit());
    }
    return leaveAuthEdit;
  }

  public void setLeaveAuthEdit(LeaveAuthEdit leaveAuthEdit) {
    this.leaveAuthEdit = leaveAuthEdit;
  }

  public String getActionType() {
    return actionType;
  }

  public void setActionType(String actionType) {
    this.actionType = actionType;
  }

  public String getLeaveUwAuthListId() {
    return leaveUwAuthListId;
  }

  public void setLeaveUwAuthListId(String leaveUwAuthListId) {
    this.leaveUwAuthListId = leaveUwAuthListId;
  }

  public String getEmpId() {
    return empId;
  }

  public void setEmpId(String empId) {
    this.empId = empId;
  }

  public String getRetMsg() {
    return retMsg;
  }

  public void setRetMsg(String retMsg) {
    this.retMsg = retMsg;
  }

  public String getMasterEmpId() {
    return masterEmpId;
  }

  public void setMasterEmpId(String masterEmpId) {
    this.masterEmpId = masterEmpId;
  }

  public String getEditEmpId() {
    return editEmpId;
  }

  public void setEditEmpId(String editEmpId) {
    this.editEmpId = editEmpId;
  }

  public String getSearchType() {
    return searchType;
  }

  public void setSearchType(String searchType) {
    this.searchType = searchType;
  }

  public String getDeptId() {
    return deptId;
  }

  public void setDeptId(String deptId) {
    this.deptId = deptId;
  }

  public String getSectionDeptId() {
    return sectionDeptId;
  }

  public void setSectionDeptId(String sectionDeptId) {
    this.sectionDeptId = sectionDeptId;
  }

  public String getEmpName() {
    return empName;
  }

  public void setEmpName(String empName) {
    this.empName = empName;
  }

  public String getChannelType() {
    return channelType;
  }

  public void setChannelType(String channelType) {
    this.channelType = channelType;
  }

  public String getChannelId() {
    return channelId;
  }

  public void setChannelId(String channelId) {
    this.channelId = channelId;
  }

  public Boolean getIsEditRole() {
    return isEditRole;
  }

  public void setIsEditRole(Boolean isEditRole) {
    this.isEditRole = isEditRole;
  }

public DataentryVerifConfVO getDataentryVerifConf() {
    return dataentryVerifConf;
}

public void setDataentryVerifConf(DataentryVerifConfVO dataentryVerifConf) {
    this.dataentryVerifConf = dataentryVerifConf;
}

public String getOrgId() {
    return orgId;
}

public void setOrgId(String orgId) {
    this.orgId = orgId;
}

public Boolean getIsDataEntryRole() {
    return isDataEntryRole;
}

public void setIsDataEntryRole(Boolean isDataEntryRole) {
    this.isDataEntryRole = isDataEntryRole;
}

public Boolean getIsVerifManagerRole() {
    return isVerifManagerRole;
}

public void setIsVerifManagerRole(Boolean isVerifManagerRole) {
    this.isVerifManagerRole = isVerifManagerRole;
}

//PCR_41834 核保轄區設置作業調整及保留核保決定資料記錄 
public Boolean getIsUserAreaRole() {
    return isUserAreaRole;
}

public void setIsUserAreaRole(Boolean isUserAreaRole) {
    this.isUserAreaRole = isUserAreaRole;
}

//轄區設置下載作業權限
private Boolean isUserAreaDownloadRole;
//轄區設置上傳作業權限
private Boolean isUserAreaUploadRole;

public Boolean getIsUserAreaDownloadRole() {
    return isUserAreaDownloadRole;
}

public void setIsUserAreaDownloadRole(Boolean isUserAreaDownloadRole) {
    this.isUserAreaDownloadRole = isUserAreaDownloadRole;
}

public Boolean getIsUserAreaUploadRole() {
    return isUserAreaUploadRole;
}

public void setIsUserAreaUploadRole(Boolean isUserAreaUploadRole) {
    this.isUserAreaUploadRole = isUserAreaUploadRole;
}
//END of PCR_41834 By Alex.Chang 12.21

}
