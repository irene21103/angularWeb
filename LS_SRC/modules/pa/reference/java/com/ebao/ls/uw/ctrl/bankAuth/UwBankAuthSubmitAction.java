package com.ebao.ls.uw.ctrl.bankAuth;

import java.util.Date;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.UserTransaction;

import org.apache.commons.lang.ClassUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.foundation.module.db.Trans;
import com.ebao.foundation.module.db.hibernate.HibernateSession3;
import com.ebao.ls.arap.cash.ds.bankauth.BankAuthService;
import com.ebao.ls.cs.logger.ApplicationLogger;
import com.ebao.ls.cs.logger.helper.AppLogDataHelper;
import com.ebao.ls.pa.nb.bs.ProposalProcessService;
import com.ebao.ls.pa.nb.bs.ProposalService;
import com.ebao.ls.pa.nb.bs.rule.ProposalRuleResultService;
import com.ebao.ls.pa.nb.bs.rule.ProposalRuleService;
import com.ebao.ls.pa.nb.ctrl.detailreg.DetailRegHelper;
import com.ebao.ls.pa.nb.ctrl.helper.UnbPaHelper;
import com.ebao.ls.pa.nb.util.NBUtils;
import com.ebao.ls.pa.nb.util.NumericUtils;
import com.ebao.ls.pa.pub.bs.PaymentMethodService;
import com.ebao.ls.pa.pub.bs.PolicyBankAuthService;
import com.ebao.ls.pa.pub.bs.PolicyService;
import com.ebao.ls.pa.pub.bs.coverage.CoverageService;
import com.ebao.ls.pa.pub.data.bo.Policy;
import com.ebao.ls.pa.pub.data.org.PolicyDao;
import com.ebao.ls.pa.pub.vo.CoveragePremium;
import com.ebao.ls.pa.pub.vo.CoverageVO;
import com.ebao.ls.pa.pub.vo.PayerVO;
import com.ebao.ls.pa.pub.vo.PolicyBankAuthVO;
import com.ebao.ls.pa.pub.vo.PolicyProposalInfoVO;
import com.ebao.ls.pa.pub.vo.PolicyVO;
import com.ebao.ls.pub.CodeCst;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.framework.GenericAction;
import com.ebao.pub.framework.GenericException;
import com.ebao.pub.i18n.lang.StringResource;
import com.ebao.pub.util.DateUtils;
import com.ebao.pub.util.StringUtils;
import com.ebao.pub.util.TransUtils;

/**
 * <p>Title: 新契約</p>
 * <p>Description: 特定功能-待承保件付款授權書資料重送核印存檔作業</p>
 * <p>Copyright: Copyright (c) 2020</p>
 * <p>Company: TGL Co., Ltd.</p>
 * <p>Create Time: 2020年6月8日</p> 
 * @author 
 * <p>Update Time: 2020年6月8日</p>
 * <p>Updater: Kathy.Yeh</p>
 * <p>Update Comments: PCR-363527</p>
 */
public class UwBankAuthSubmitAction extends GenericAction {
	
	public static final String BEAN_DEFAULT = "/uw/uwBankAuthSubmit";
        
	//IR-407770 更新報盤方式 接口 2020/09/17 Add by Vince Cheng
	@Resource(name = UnbPaHelper.BEAN_DEFAULT)
	private UnbPaHelper unbPaHelper;

    @Resource(name = PolicyService.BEAN_DEFAULT)
    private PolicyService policyService;
    
    @Resource(name = BankAuthService.BEAN_DEFAULT)
    private BankAuthService bankAuthService;
    
	@Resource(name = PolicyDao.BEAN_DEFAULT)
	private PolicyDao<Policy> policyDao;

	@Resource(name = PolicyBankAuthService.BEAN_DEFAULT)
	private PolicyBankAuthService policyBankAuthService;

	@Resource(name = DetailRegHelper.BEAN_DEFAULT)
	private DetailRegHelper detailRegHelper;
	
    @Resource(name = ProposalService.BEAN_DEFAULT)
    protected ProposalService proposalService;
    
	@Resource(name = ProposalRuleResultService.BEAN_DEFAULT)
	private ProposalRuleResultService proposalRuleResultService;
	
    @Resource(name = ProposalProcessService.BEAN_DEFAULT)
    private ProposalProcessService proposalProcessService;
	
	@Resource(name = PaymentMethodService.BEAN_DEFAULT)
	private PaymentMethodService paymentMethodService;
	
	@Resource(name = CoverageService.BEAN_DEFAULT)
	protected CoverageService coverageService;
	
    /**
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return ActionForwrd
    */
    public ActionForward process(ActionMapping mapping, ActionForm form,
                    HttpServletRequest request, HttpServletResponse response)
                    throws GenericException {

        UwBankAuthQueryForm uwBankAuthQueryForm = (UwBankAuthQueryForm) form;
		String retMsg = "";
		String policyNo = uwBankAuthQueryForm.getPolicyNo();
		Long  policyId = policyService.getPolicyIdByPolicyCode(policyNo);
        UserTransaction ut = null;

        ApplicationLogger.setSystemCode(AppLogDataHelper.SYSTEM_CODE_UNB);
		ApplicationLogger.setJobName(ClassUtils.getShortClassName(UwBankAuthSubmitAction.class));
		ApplicationLogger.setPolicyCode(policyNo);
		ApplicationLogger.setOptionId(DateUtils.date2String(new Date(),"yyyyMMdd HHmmss"));
		
        try {
            ut = Trans.getUserTransaction();
            ut.begin();


	        /*基本參數物件*/
	        PolicyVO policyVO = policyService.load(policyId);
			ApplicationLogger.addLoggerData("policyService.load("+policyId+") ");	        
	        
	        /*首、續期繳費方式變更*/
            boolean isPayModeChange = false;
	        for (CoverageVO coverageVO : policyVO.getCoverages()) {
				CoveragePremium currentPremium = coverageVO.getCurrentPremium();
				if (!currentPremium.getPaymentMethod().equals(NumericUtils.parseInteger(uwBankAuthQueryForm.getPayMode()))){
					currentPremium.setPaymentMethod(NumericUtils.parseInteger(uwBankAuthQueryForm.getPayMode()));
					isPayModeChange = true;
				}

				CoveragePremium nextPremium = coverageVO.getNextPremium();
				if (!nextPremium.getPaymentMethod().equals(NumericUtils.parseInteger(uwBankAuthQueryForm.getPayNext()))){
					nextPremium.setPaymentMethod(NumericUtils.parseInteger(uwBankAuthQueryForm.getPayNext()));
					isPayModeChange = true;
				}

				/*單一繳別或1年期商品,續期繳費方式為不可輸入,自動同首期繳費方式*/
				if (nextPremium.getPaymentMethod() == null
								&& policyVO.gitMasterCoverage().isSinglePay()) {
					nextPremium.setPaymentMethod(currentPremium.getPaymentMethod());
				}
				coverageService.saveWithoutCalc(coverageVO);
			}
	        /*繳費方式改為4-單位刷卡時要更新T_CONTRACT_PROPOASL.FOA_CREDIT_CARD_INDI */
			PolicyProposalInfoVO proposal = policyVO.getProposalInfo();
			if (!StringUtils.isNullOrEmpty(proposal.getFoaCreditCardIndi())){
				if (!proposal.getFoaCreditCardIndi().equals(uwBankAuthQueryForm.getFoaCreditCardIndi())){
					//單位刷卡indi
					proposal.setFoaCreditCardIndi(uwBankAuthQueryForm.getFoaCreditCardIndi());
					policyService.save(policyVO);
					isPayModeChange = true;
				}				
			}
			
	        //查找變更前授權書資料
			NBUtils.logger(this.getClass(), "policyBankAuthService.findMapByPolicyId() begin");
	        Map<String, PolicyBankAuthVO> beforeBankAuthMap = policyBankAuthService.findMapByPolicyId(policyId);
			HibernateSession3.currentSession().flush();

			NBUtils.logger(this.getClass(), "detailRegHelper.getPolicyBankAuth() begin");
			Map<String, PolicyBankAuthVO> policyBankAuth = detailRegHelper.getPolicyBankAuth(uwBankAuthQueryForm);

			NBUtils.logger(this.getClass(), "policyBankAuthService.syncBankAuthOnUNB() begin");			
			policyBankAuthService.syncBankAuthOnUNB(policyId, policyBankAuth);

			//新增檢核有勾選「重新產生核印資抖」產生一筆核印資料 
			boolean authReSendBank = uwBankAuthQueryForm.getAuthReSendBank() != null ? true : false;
			boolean nextReSendBank = uwBankAuthQueryForm.getNextReSendBank() != null ? true : false;
			boolean authReSendCreditCard = uwBankAuthQueryForm.getAuthReSendCreditCard() != null ? true : false;
			boolean nextReSendCreditCard = uwBankAuthQueryForm.getNextReSendCreditCard() != null ? true : false;
			boolean isReSend = false;
			if 	(authReSendBank || nextReSendBank || authReSendCreditCard || nextReSendCreditCard)
			{
				NBUtils.logger(this.getClass(), "policyBankAuthService.doReSendBankAuth() begin");
				isReSend = policyBankAuthService.isReSendBankAuth(policyId,
								authReSendBank, nextReSendBank, 
								authReSendCreditCard, nextReSendCreditCard);
			}			
			
			NBUtils.logger(this.getClass(), "callPremCICreateSealRecord() begin");
			HibernateSession3.currentSession().flush();
			
			policyBankAuthService.callPremCICreateSealRecord(policyId, beforeBankAuthMap);
			NBUtils.logger(this.getClass(), "callPremCICreateSealRecord() end");      
			
			HibernateSession3.currentSession().flush();
			if (!isReSend || isPayModeChange || authReSendCreditCard || nextReSendCreditCard){
				
				/*同步T_PAYER, T_PAYER_ACCOUNT 相關授權人資料*/		
				NBUtils.logger(this.getClass(), "detailRegHelper.syncPayer() begin");		
				detailRegHelper.syncPayer(policyVO);
				PayerVO payerVO = (policyVO.getPayers() != null && policyVO.getPayers().size() > 0)
								? policyVO.getPayers().get(0)
								: new PayerVO();
								
				NBUtils.logger(this.getClass(), "detailRegHelper.syncPayer() begin");										
				detailRegHelper.syncPayerAccount(policyVO, payerVO);
				
				policyBankAuth = policyBankAuthService.findMapByPolicyId(policyId);
				//不為核印處理中(表示資料不完整),若信用卡效期、授權人姓名有異動需同步回t_bank_account
				for (PolicyBankAuthVO bankAuthVO : policyBankAuth.values()) {
					if (bankAuthVO.getAccountId() != null &&
									!CodeCst.APPROVAL_STATUS__IN_PROCESS.equals(bankAuthVO.getApprovalStatus())) {

						//授權人姓名有異動需更新t_bank_account.account_name
						boolean isUpdateAccountName = false;
						PolicyBankAuthVO beforeBankAuthVO = beforeBankAuthMap.get(bankAuthVO.getSourceType());
						if (beforeBankAuthVO != null
										&& bankAuthVO.getAccountName() != null) {
							//目前頁面有輸入姓名且與前次姓名不同,需作姓名更新
							if (!bankAuthVO.getAccountName().equals(beforeBankAuthVO.getAccountName())) {
								isUpdateAccountName = true;
							}
						}
						policyBankAuthService.updatePayerAccountByBankAuth(policyId, bankAuthVO, isUpdateAccountName, false);
					}
				}

				/*計算保費與生效日*/
				NBUtils.logger(this.getClass(), "proposalService.doSubmitDetailRegistration() begin");			
				proposalService.doSubmitDetailRegistration(policyVO.getPolicyId());
				NBUtils.logger(this.getClass(), "proposalService.doSubmitDetailRegistration() end");
				
				//先提交需存檔資料,因 AmlQueryCIImpl.step4WriteAmlQueryLog();若發生錯誤會rollback造成核印資料未寫入，但undo會執行。
				ut.commit(); 
				ut.begin();
				//end fix AmlQueryCIImpl rollback 
				
				/*重新執行OPQ欄規則校驗*/
				NBUtils.logger(this.getClass(), "proposalService.checkProposalRules() begin");	
                boolean isEcOffline = policyService.isEcOffline(policyId);
    			if (isEcOffline) {
    				proposalService.checkProposalRules(policyId, ProposalRuleService.ROLESSET_VERIFICATION_EC_OFFLINE);
    			} else {
    				proposalService.checkProposalRules(policyId, ProposalRuleService.ROLESSET_UW_MODIFIY);
    			}
				NBUtils.logger(this.getClass(), "proposalService.checkProposalRules() end");	

				/* 檢核是否因為再次檢核產生，新的OPQ未處理訊息 */
				boolean hasPendingIssue = proposalRuleResultService.hasPendingIssue(policyId);
				if (hasPendingIssue) {
					/* 系統自動將保單UNDO回原核保員任務池，並記錄此保單為系統自動UNDO件    */
					NBUtils.logger(this.getClass(), "proposalProcessService.unbUwAutoUndo() begin");
					proposalProcessService.unbUwAutoUndo(policyId);
					NBUtils.logger(this.getClass(), "proposalProcessService.unbUwAutoUndo() end");
					
					//保單號碼:{policyCode}已返回核保中
					retMsg = StringResource.getStringData("MSG_1265421", AppContext.getCurrentUser().getLangId());

				} else {
					//保單號碼:{policyCode}存檔成功
					retMsg = StringResource.getStringData("MSG_1265420", AppContext.getCurrentUser().getLangId());
				}
			}else{
				//保單號碼:{policyCode}存檔成功
				retMsg = StringResource.getStringData("MSG_1265420", AppContext.getCurrentUser().getLangId());
			}
			ut.commit();
			
            //IR-407770 異動繳費方式即檢核並更新報盤銀行 接口 2020/09/17 Add by Vince Cheng
            // IR-418372一律更新報盤銀行
			NBUtils.logger(this.getClass(), "updateAcquirerId(N,N) begin");
            unbPaHelper.updateAcquirerId(policyId, CodeCst.YES_NO__NO, CodeCst.YES_NO__NO);
            NBUtils.logger(this.getClass(), "updateAcquirerId() end");
			
	        ApplicationLogger.flush();
            //return mapping.findForward("success");

		} catch (Exception e) {
			TransUtils.rollback(ut);
			//保單號碼:{policyCode}提交錯誤!
			retMsg = StringResource.getStringData("MSG_1253858", AppContext.getCurrentUser().getLangId());
		}
        
		if (!StringUtils.isNullOrEmpty(retMsg)) {
			if(StringUtils.isNullOrEmpty(policyNo) == false){
				retMsg = retMsg.replace("{policyCode}", policyNo);	
			}
			uwBankAuthQueryForm.setResMessageType("alert");
			uwBankAuthQueryForm.setResMessage(retMsg);
			uwBankAuthQueryForm.setPolicyNo("");
	        request.setAttribute("actionForm", uwBankAuthQueryForm); 
		}		

		return mapping.findForward("success");
    }
}
