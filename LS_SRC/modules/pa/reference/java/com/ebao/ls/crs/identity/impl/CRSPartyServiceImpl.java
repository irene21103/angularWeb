package com.ebao.ls.crs.identity.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.crystaldecisions.reports.exporters.format.page.pdf.fontembedding.opentype.types.Char;
import com.ebao.foundation.commons.exceptions.GenericException;
import com.ebao.ls.crs.constant.CRSCodeCst;
import com.ebao.ls.crs.data.bo.CRSParty;
import com.ebao.ls.crs.data.bo.CRSPartyLog;
import com.ebao.ls.crs.data.identity.CRSPartyDAO;
import com.ebao.ls.crs.identity.CRSPartyService;
import com.ebao.ls.crs.vo.CRSPartyLogVO;
import com.ebao.ls.crs.vo.CRSPartyVO;
import com.ebao.ls.cs.commonflow.data.TChangeDelegate;
import com.ebao.ls.cs.commonflow.data.bo.Change;
import com.ebao.ls.foundation.service.GenericServiceImpl;
import com.ebao.ls.pa.nb.util.NBUtils;
import com.ebao.ls.pty.ci.EmployeeCI;
import com.ebao.ls.pty.vo.EmployeeVO;
import com.ebao.ls.pub.CodeCst;
import com.ebao.pub.i18n.lang.CodeTable;
import com.ebao.pub.util.BeanUtils;

import org.hibernate.Criteria;

public class CRSPartyServiceImpl extends GenericServiceImpl<CRSPartyVO, CRSParty, CRSPartyDAO>
		implements CRSPartyService {
	
	@Resource(name = EmployeeCI.BEAN_DEFAULT)
	private EmployeeCI empServ;

	@Resource(name = CRSPartyDAO.BEAN_DEFAULT)
	private CRSPartyDAO crsPartyDAO;
	
	@Override
	protected CRSPartyVO newEntityVO() {
		return new CRSPartyVO();
	}

	@Override
	public CRSPartyVO findByCertiCode(String certiCode) {
		CRSParty party = dao.findByCertiCode(certiCode);
		if(party == null)
			return null;
		return this.convertToVO(party);
	}

	@Override
	public CRSPartyVO findByCrsId(Long crsId) {
		CRSParty party = dao.findByCrsId(crsId);
		if(party == null)
			return null;
		return this.convertToVO(party);
	}

	@Override
	public void saveOrUpdate(Long handerId, Date processDate, CRSPartyVO partyVO, Long changeId, String inputSource)
			throws GenericException {
		Long insertChangeId = changeId;
		if (partyVO.getLogId() == null
						&& changeId == null
						&& NBUtils.in(inputSource,CRSCodeCst.INPUT_SOURCE__CMN,CRSCodeCst.INPUT_SOURCE__WS)) {
			Change tChange = new Change();
			tChange.setChangeSource(CodeCst.CHANGE_SOURCE__PARTY);
			tChange.setChangeStatus(CodeCst.CHANGE_STATUS__WAITING_ISSUE);
			// TODO: 暫時先用台北總公司
			tChange.setOrgId(101L);
			insertChangeId = TChangeDelegate.create(tChange);
		}
		partyVO.setChangeId(insertChangeId);
		if (StringUtils.isNotEmpty(partyVO.getCrsType())) {
			String crsCode = CodeTable.getIdByCode("T_CRS_IDENTITY_TYPE", partyVO.getCrsType());
			partyVO.setCrsCode((StringUtils.isEmpty(crsCode)) ? CRSCodeCst.CRS_IDENTITY_TYPE__NONE : crsCode);
		}
		
		this.save(partyVO);
		
	}
	/**
	 * PCR 257607 330030 e-Approval ITR-1801051_CRS共同申報及盡職審查專案- 保全變更及CRS確認申報國作業
	 */
	@Override
	public List<CRSParty> findListByCertiCode(String certiCode, String effType) {
		Criteria criteria = crsPartyDAO.getSession().createCriteria(CRSParty.class);
		List<CRSPartyVO> crsPartyVOList = new ArrayList<CRSPartyVO>();
		if ( StringUtils.isEmpty(certiCode)) {
			return null;
		}
		
		if(StringUtils.isNotEmpty(certiCode))
			criteria.add(Restrictions.eq("certiCode", certiCode));
		//if(StringUtils.isNotEmpty(policyCode))
		//	criteria.add(Restrictions.eq("policyCode", policyCode));
		
		// 有效建檔狀態 = 01已建檔、06已註記
		String[] effTypeArray = { CRSCodeCst.BUILD_TYPE__COMPLETE,
				CRSCodeCst.BUILD_TYPE__STAMPED };
		
		if (effType.equals(CodeCst.YES_NO__YES)) {
			criteria.add(Restrictions.in("buildType", effTypeArray));
		} else {
			if (effType.equals(CodeCst.YES_NO__NO)) {
				criteria.add(Restrictions.not(Restrictions.in("buildType",
						effTypeArray)));
			}
		}

		criteria.addOrder(Order.desc("logId")); //List<CRSParty> 
		
		return  criteria.list();
	}
	/**
	 * PCR 257607 330030 e-Approval ITR-1801051_CRS共同申報及盡職審查專案- 保全變更及CRS確認申報國作業
	 */
	@Override
	public List<CRSParty> findListByNameAndPolicyCode(String name, String policyCode, String effType) {
		Criteria criteria = crsPartyDAO.getSession().createCriteria(CRSParty.class);

		if ( StringUtils.isEmpty(name)) {
			return null;
		}
		if(StringUtils.isNotEmpty(name))
			criteria.add(Restrictions.eq("name", name));
		
		if ( StringUtils.isEmpty(policyCode)) {
			return null;
		}
		if(StringUtils.isNotEmpty(policyCode))
			criteria.add(Restrictions.eq("policyCode", policyCode));
		
		criteria.add(Restrictions.isNull("certiCode"));
		
		// 有效建檔狀態 = 01已建檔、06已註記
		String[] effTypeArray = { CRSCodeCst.BUILD_TYPE__COMPLETE,
				CRSCodeCst.BUILD_TYPE__STAMPED };
		if(StringUtils.isNotEmpty(effType)){
			if (effType.equals(CodeCst.YES_NO__YES)) {
				criteria.add(Restrictions.in("buildType", effTypeArray));
			} else {
				if (effType.equals(CodeCst.YES_NO__NO)) {
					criteria.add(Restrictions.not(Restrictions.in("buildType",
							effTypeArray)));
				}
			}
		}

		criteria.addOrder(Order.desc("logId"));
		return  criteria.list();
	}

	/**
	 * PCR 257607 330030 e-Approval ITR-1801051_CRS共同申報及盡職審查專案- 保全變更及CRS確認申報國作業
	 * 1    優先以證號 找主檔的 crs_id 做為key值，依照以下順序取得關聯檔
	 * 1*   先以姓名找主檔的 crs_id 做為key值，依照以下順序取得關聯檔　
	 */
	@Override
	public CRSPartyVO findPosCrsParty(String policyCode, String certiCode,  String name) {
		List<CRSParty> crsPartyList =  new ArrayList<CRSParty>(); 						//CRS PARTY 主檔
		if (certiCode != null && !certiCode.equals("")) {
			//1    優先以證號 找主檔的 crs_id 做為key值，依照以下順序取得關聯檔　
			crsPartyList = this
					.findListByCertiCode(certiCode, CodeCst.YES_NO__YES);
		}else{
			//1* 先以姓名找主檔的 crs_id 做為key值，依照以下順序取得關聯檔　　
			crsPartyList = this		
					.findListByNameAndPolicyCode(name, policyCode, CodeCst.YES_NO__YES);
		}
		
		if (CollectionUtils.isEmpty(crsPartyList))
			return null;
		return this.convertToVO(crsPartyList.get(0));
	}
	
	@Override
	public List<CRSPartyVO> findPosCrsPartyList(String policyCode, String certiCode,  String name) {
		List<CRSParty> crsPartyList =  new ArrayList<CRSParty>(); 						//CRS PARTY 主檔
		if (certiCode != null && !certiCode.equals("")) {
			//1    優先以證號 找主檔的 crs_id 做為key值，依照以下順序取得關聯檔　
			crsPartyList = this
					.findListByCertiCode(certiCode, CodeCst.YES_NO__YES);
		}else{
			//1* 先以姓名找主檔的 crs_id 做為key值，依照以下順序取得關聯檔　　
			crsPartyList = this		
					.findListByNameAndPolicyCode(name, policyCode, CodeCst.YES_NO__YES);
		}
		
		if (CollectionUtils.isEmpty(crsPartyList))
			return null;
		return this.convertToVOList(crsPartyList);
	}
}
