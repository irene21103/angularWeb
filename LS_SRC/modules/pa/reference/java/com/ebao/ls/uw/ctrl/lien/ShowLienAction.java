package com.ebao.ls.uw.ctrl.lien;

import java.math.BigDecimal;
import java.util.Collection;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.ls.pa.pub.ci.CoverageCI;
import com.ebao.ls.pa.pub.ref.api.prd.ProductService;
import com.ebao.ls.pa.pub.ref.vo.prd.ProductVO;
import com.ebao.ls.pa.pub.service.PolicyService;
import com.ebao.ls.pub.CodeCst;
import com.ebao.ls.uw.ctrl.UwGenericAction;
import com.ebao.ls.uw.ctrl.pub.UwLienForm;
import com.ebao.ls.uw.vo.UwPolicyVO;
import com.ebao.ls.uw.vo.UwProductVO;
import com.ebao.pub.annotation.PrdAPIUpdate;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.framework.ExceptionFactory;
import com.ebao.pub.framework.GenericException;
import com.ebao.pub.util.BeanUtils;
import com.ebao.pub.util.DateUtils;

/**
 * <p>
 * Title: GEL-UW
 * </p>
 * <p>
 * Description: show lien information Action
 * </p>
 * <p>
 * Copyright: Copyright (c) 2004
 * </p>
 * <p>
 * Company: eBaoTech
 * </p>
 * 
 * @author yixing.lu
 * @version 1.0
 * @since 09.08.2004
 */
public class ShowLienAction extends UwGenericAction {
  private static final BigDecimal PAR_RATE = new BigDecimal(1.25);
  private static final BigDecimal NON_PAR_RATE_1 = new BigDecimal(1);
public static final String BEAN_DEFAULT = "/uw/uwShowLien";

  /**
   * show lien information
   * 
   * @param mapping
   * @param form
   * @param req
   * @param res
   * @return
   * @throws GenericException
   */
  @Override
  @PrdAPIUpdate
  public ActionForward uwProcess(ActionMapping mapping, ActionForm form,
      HttpServletRequest req, HttpServletResponse res) throws GenericException {
    try {
      UwLienForms uwLienForms = (UwLienForms) form;
      Long itemId = Long.valueOf(req.getParameter("itemIds"));
      uwLienForms.setItemId(itemId);
      // get underwriting policy information by underwriteId
      Long underwriteId = uwLienForms.getUnderwriteId();
      UwPolicyVO uwPolicyVO = getUwPolicyDS().findUwPolicy(underwriteId);
      // get product information by underwriteId and itemId
      UwProductVO productVO = getUwPolicyDS().findUwProduct(underwriteId,
          itemId);
      // get the product benefitType
      ProductVO basicInfoVO = this.getProductService().getProduct(
          Long.valueOf(productVO.getProductId().intValue()),policyService.getActiveVersionDate(productVO));
      req.setAttribute("beneType", basicInfoVO.getBenefitType());
      // get emValue and factor collection
      Collection emAndFactorC = null;
      // get the emValue && factor Collection depended on lien table typeId and
      // baseRate
      Integer typeId = null;
      BigDecimal bsaRate = null;
      BigDecimal amount = productVO.getAmount();
      String initialType = productVO.getInitialType();
      if (isSinglePremium(initialType)) {
        BigDecimal stdPremAn = productVO.getStdPremAn();
        bsaRate = amount.divide(stdPremAn, 2, BigDecimal.ROUND_HALF_UP);
        String parNonILP = "2"; //basicInfoVO.getParNonIlp();
        boolean isParRate = PAR_RATE.compareTo(bsaRate) == 0;
        if ("1".equals(parNonILP)) {
          if (isParRate) {
            // the lien table is par
            typeId = Integer.valueOf(1);
          }
        } else {
          if (NON_PAR_RATE_1.compareTo(bsaRate) == 0
              || NON_PAR_RATE_1.compareTo(bsaRate) == 0) {
            typeId = Integer.valueOf(2);
          }
        }
      } else {
        // lien table is regular
        typeId = Integer.valueOf(3);
      }
      if (typeId != null && bsaRate != null && typeId.intValue() != 3) {
        emAndFactorC = getUwPolicyDS().findLienEmByTypeIdAndBsaRate(typeId,
            bsaRate);
      } else if (typeId != null && typeId.intValue() == 3) {
        emAndFactorC = getUwPolicyDS().findLienEmByTypeId(typeId);
      }
      req.setAttribute("Lien_EM", emAndFactorC);
      // get the lien information by underwriteId and itemId
      Collection uwLienEntitis = getUwPolicyDS().findUwLienEntitis(
          underwriteId, itemId);
      Collection lienList = BeanUtils.copyCollection(UwLienForm.class,
          uwLienEntitis);
      // get the lienForm base information
      UwLienForm lienForm = new UwLienForm();
      lienForm.setPolicyCode(uwLienForms.getPolicyCode());
      lienForm.setApplyCode(uwLienForms.getApplyCode());
      lienForm.setChargeYear(productVO.getChargeYear());
      lienForm.setCommencementDate(productVO.getValidateDate());
      lienForm.setConverYear(productVO.getCoverageYear());
      // add by hanzhong.yan for cq:gel00028427 2007-7-16
      Integer coverageTerm = coverageCI.getCoverageTerm(itemId);
      lienForm.setCoverageTerm(coverageTerm.longValue());
      // add end
      lienForm.setCoveragePeriod(productVO.getCoveragePeriod());
      lienForm.setCurrPolicyYear(Integer.valueOf(DateUtils.getYear(AppContext
          .getCurrentUserLocalTime())
          - DateUtils.getYear(productVO.getValidateDate())));
      // lienForm.setAmount(amount);
      lienForm.setAmount(productVO.getReducedAmount());
      lienForm.setUnderwriteId(uwLienForms.getUnderwriteId());
      lienForm.setItemId(uwLienForms.getItemId());
      lienForm.setProductId(productVO.getProductId());
      lienForm.setUwSourceType(uwPolicyVO.getUwSourceType());
      lienForm.setChargePeriod(productVO.getChargePeriod());
      req.setAttribute("action_form", lienForm);
      req.setAttribute("lien", lienList);
      req.setAttribute("initialType", initialType);
    } catch (Exception e) {
      throw ExceptionFactory.parse(e);
    }
    // ActionForward the page lien.jsp
    return mapping.findForward("lien");
  }

  /**
   * checking the pay mode is single premium
   * 
   * @param initialType
   * @return
   */
  private boolean isSinglePremium(String initialType) {
    if (CodeCst.CHARGE_MODE__SINGLE.equals(initialType)) {
      return true;
    }
    return false;
  }

  @Resource(name = ProductService.BEAN_DEFAULT)
  private ProductService productService;

  public ProductService getProductService() {
    return productService;
  }

  public void setProductService(ProductService productService) {
    this.productService = productService;
  }

  @Resource(name = CoverageCI.BEAN_DEFAULT)
  private CoverageCI coverageCI;

  /**
   * @return the coverageCI
   */
  public CoverageCI getCoverageCI() {
    return coverageCI;
  }

  /**
   * @param coverageCI the coverageCI to set
   */
  public void setCoverageCI(CoverageCI coverageCI) {
    this.coverageCI = coverageCI;
  }
  
  @Resource(name = PolicyService.BEAN_DEFAULT)
  PolicyService policyService;
}