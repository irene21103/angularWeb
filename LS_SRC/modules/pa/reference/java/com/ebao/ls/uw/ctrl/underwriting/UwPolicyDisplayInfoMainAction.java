package com.ebao.ls.uw.ctrl.underwriting;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.ls.escape.helper.EscapeHelper;
import com.ebao.ls.uw.ctrl.UwGenericAction;
import com.ebao.pub.framework.GenericException;

/**
 * <p>
 * Title:Underwriting
 * </p>
 * <p>
 * Description:
 * </p>
 * <p>
 * Copyright: Copyright (c) 2004
 * </p>
 * <p>
 * Company: eBaoTech Corporation
 * </p>
 * 
 * @author sunny
 * @since 2015-05-22
 * @version 1.0
 */
public class UwPolicyDisplayInfoMainAction extends UwGenericAction {
    public static final String BEAN_DEFAULT = "/uw/uwPolicyDisplayInfoMain";

    @Override
    public ActionForward uwProcess(ActionMapping mapping, ActionForm form,
                    HttpServletRequest request, HttpServletResponse response)
                    throws GenericException {
        EscapeHelper.escapeHtml(request).setAttribute("applyCodePrefix", EscapeHelper.escapeHtml(request.getParameter("applyCodePrefix")));
        EscapeHelper.escapeHtml(request).setAttribute("spServiceBranch_desc", EscapeHelper.escapeHtml(request.getParameter("spServiceBranch_desc")));
        EscapeHelper.escapeHtml(request).setAttribute("spServiceBranch", EscapeHelper.escapeHtml(request.getParameter("spServiceBranch")));
        EscapeHelper.escapeHtml(request).setAttribute("spPolicyNo", EscapeHelper.escapeHtml(request.getParameter("spPolicyNo")));
        EscapeHelper.escapeHtml(request).setAttribute("spSubmissionChannel", EscapeHelper.escapeHtml(request.getParameter("spSubmissionChannel")));
        EscapeHelper.escapeHtml(request).setAttribute("spProposalNo",EscapeHelper.escapeHtml(request.getParameter("spProposalNo")));
        EscapeHelper.escapeHtml(request).setAttribute("spProductCategory", EscapeHelper.escapeHtml(request.getParameter("spProductCategory")));
        EscapeHelper.escapeHtml(request).setAttribute("spIACSpecailGroup", EscapeHelper.escapeHtml(request.getParameter("spIACSpecailGroup")));
        EscapeHelper.escapeHtml(request).setAttribute("spPendingReason", EscapeHelper.escapeHtml(request.getParameter("spPendingReason")));
        EscapeHelper.escapeHtml(request).setAttribute("startDate", EscapeHelper.escapeHtml(request.getParameter("startDate")));
        EscapeHelper.escapeHtml(request).setAttribute("endDate", EscapeHelper.escapeHtml(request.getParameter("endDate")));
        EscapeHelper.escapeHtml(request).setAttribute("spProposalStatus", EscapeHelper.escapeHtml(request.getParameter("spProposalStatus")));
        String policyId = EscapeHelper.escapeHtml(request.getParameter("policyId"));

        EscapeHelper.escapeHtml(request).setAttribute("policyId", policyId);
        
        EscapeHelper.escapeHtml(request).setAttribute("lockPage", EscapeHelper.escapeHtml(request.getParameter("lockPage")));
        EscapeHelper.escapeHtml(request).setAttribute("displayPolicyInfo", EscapeHelper.escapeHtml(request.getParameter("displayPolicyInfo")));

        // request
        // .setAttribute("annuitantSelectedIndi",
        // Para.getParaValue(2042100006));// ANNUITANT_SELECTED_INDI
        // request.setAttribute("annuitantDefualtType",
        // Para.getParaValue(2042100007));// NB_ANNUITANT_DEFUALT_TYPE
        EscapeHelper.escapeHtml(request).setAttribute("underwriteId",
                        request.getParameter("underwriteId"));
        EscapeHelper.escapeHtml(request).setAttribute("originType",
                        request.getParameter("originType"));
        return mapping.findForward("display");

    }

}