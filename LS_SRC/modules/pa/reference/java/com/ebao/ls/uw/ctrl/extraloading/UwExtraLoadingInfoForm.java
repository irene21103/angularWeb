package com.ebao.ls.uw.ctrl.extraloading;

import java.util.ArrayList;
import java.util.List;

import com.ebao.ls.uw.ctrl.pub.UwProductForm;
import com.ebao.pub.framework.GenericForm;

/**
 * <p>Title: </p>
 * <p>Description:the collection of all extra </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: eBaoTech Corporation</p>
 * @author Bill Tian
 * @version 1.0
 */

public class UwExtraLoadingInfoForm extends GenericForm {
  private UwProductForm uwProductForm = new UwProductForm();

  private List health = new ArrayList();

  private List occupation = new ArrayList();

  private List avocation = new ArrayList();

  private List residential = new ArrayList();

  private List aviation = new ArrayList();

  private List other = new ArrayList();

  private String applyCode;

  private String policyCode;

  private String healthFlag = "N";

  private String occupationFlag = "N";

  private String avocationFlag = "N";

  private String residentialFlag = "N";

  private String aviationFlag = "N";

  private String otherFlag = "N";

  private String cond = "";

  private String uwStatus = "";

  private String benefityType = "";

  private String duration = "0";

  private Integer emCi;
  private Integer emLife;
  private Integer emTpd;

  public Integer getEmCi() {
    return emCi;
  }

  public void setEmCi(Integer emCi) {
    this.emCi = emCi;
  }

  public Integer getEmLife() {
    return emLife;
  }

  public void setEmLife(Integer emLife) {
    this.emLife = emLife;
  }

  public Integer getEmTpd() {
    return emTpd;
  }

  public void setEmTpd(Integer emTpd) {
    this.emTpd = emTpd;
  }

  public String getDuration() {
    return duration;
  }

  public void setDuration(String duration) {
    this.duration = duration;
  }

  /**
   * @return applyCode
   */
  public String getApplyCode() {
    return applyCode;
  }

  /**
   * @param applyCode will be set applyCode
   */
  public void setApplyCode(String applyCode) {
    this.applyCode = applyCode;
  }

  /**
   * @return aviation
   */
  public List getAviation() {
    return aviation;
  }

  /**
   * @param aviation will be set aviation
   */
  public void setAviation(List aviation) {
    this.aviation = aviation;
  }

  /**
   * @return aviationFlag
   */
  public String getAviationFlag() {
    return aviationFlag;
  }

  /**
   * @param aviationFlag will be set aviationFlag
   */
  public void setAviationFlag(String aviationFlag) {
    this.aviationFlag = aviationFlag;
  }

  /**
   * @return avocation
   */
  public List getAvocation() {
    return avocation;
  }

  /**
   * @param avocation will be set avocation
   */
  public void setAvocation(List avocation) {
    this.avocation = avocation;
  }

  /**
   * @return avocationFlag
   */
  public String getAvocationFlag() {
    return avocationFlag;
  }

  /**
   * @param avocationFlag will be set avocationFlag
   */
  public void setAvocationFlag(String avocationFlag) {
    this.avocationFlag = avocationFlag;
  }

  /**
   * @return benefityType
   */
  public String getBenefityType() {
    return benefityType;
  }

  /**
   * @param benefityType will be set benefityType
   */
  public void setBenefityType(String benefityType) {
    this.benefityType = benefityType;
  }

  /**
   * @return cond
   */
  public String getCond() {
    return cond;
  }

  /**
   * @param cond will be set cond
   */
  public void setCond(String cond) {
    this.cond = cond;
  }

  /**
   * @return health
   */
  public List getHealth() {
    return health;
  }

  /**
   * @param health will be set health
   */
  public void setHealth(List health) {
    this.health = health;
  }

  /**
   * @return healthFlag
   */
  public String getHealthFlag() {
    return healthFlag;
  }

  /**
   * @param healthFlag will be set healthFlag
   */
  public void setHealthFlag(String healthFlag) {
    this.healthFlag = healthFlag;
  }

  /**
   * @return occupation
   */
  public List getOccupation() {
    return occupation;
  }

  /**
   * @param occupation will be set occupation
   */
  public void setOccupation(List occupation) {
    this.occupation = occupation;
  }

  /**
   * @return occupationFlag
   */
  public String getOccupationFlag() {
    return occupationFlag;
  }

  /**
   * @param occupationFlag will be set occupationFlag
   */
  public void setOccupationFlag(String occupationFlag) {
    this.occupationFlag = occupationFlag;
  }

  /**
   * @return other
   */
  public List getOther() {
    return other;
  }

  /**
   * @param other will be set other
   */
  public void setOther(List other) {
    this.other = other;
  }

  /**
   * @return otherFlag
   */
  public String getOtherFlag() {
    return otherFlag;
  }

  /**
   * @param otherFlag will be set otherFlag
   */
  public void setOtherFlag(String otherFlag) {
    this.otherFlag = otherFlag;
  }

  /**
   * @return policyCode
   */
  public String getPolicyCode() {
    return policyCode;
  }

  /**
   * @param policyCode will be set policyCode
   */
  public void setPolicyCode(String policyCode) {
    this.policyCode = policyCode;
  }

  /**
   * @return residential
   */
  public List getResidential() {
    return residential;
  }

  /**
   * @param residential will be set residential
   */
  public void setResidential(List residential) {
    this.residential = residential;
  }

  /**
   * @return residentialFlag
   */
  public String getResidentialFlag() {
    return residentialFlag;
  }

  /**
   * @param residentialFlag will be set residentialFlag
   */
  public void setResidentialFlag(String residentialFlag) {
    this.residentialFlag = residentialFlag;
  }

  /**
   * @return uwProductForm
   */
  public UwProductForm getUwProductForm() {
    return uwProductForm;
  }

  /**
   * @param uwProductForm will be set uwProductForm
   */
  public void setUwProductForm(UwProductForm uwProductForm) {
    this.uwProductForm = uwProductForm;
  }

  /**
   * @return uwStatus
   */
  public String getUwStatus() {
    return uwStatus;
  }

  /**
   * @param uwStatus will be set uwStatus
   */
  public void setUwStatus(String uwStatus) {
    this.uwStatus = uwStatus;
  }

}
