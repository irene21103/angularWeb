package com.ebao.ls.crs.batch.job;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.transaction.UserTransaction;

import org.apache.commons.collections.ListUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.util.CollectionUtils;

import com.ebao.foundation.common.lang.exception.ExceptionUtils;
import com.ebao.foundation.core.logging.Log;
import com.ebao.ls.batch.TGLBasePieceableJob;
import com.ebao.ls.crs.batch.service.CRSDueDiligenceListService;
import com.ebao.ls.crs.batch.vo.CRSDueCheckVO;
import com.ebao.ls.crs.batch.vo.CRSTempVO;
import com.ebao.ls.crs.constant.CRSCodeCst;
import com.ebao.ls.crs.data.batch.CRSDueDiligenceListDAO;
import com.ebao.ls.crs.identity.CRSPartyService;
import com.ebao.ls.crs.vo.CRSDueDiligenceListVO;
import com.ebao.ls.cs.commonflow.data.TChangeDelegate;
import com.ebao.ls.cs.commonflow.data.bo.Change;
import com.ebao.ls.fatca.constant.FatcaSurveyType;
import com.ebao.ls.fatca.data.bo.FatcaDueDiligenceList;
import com.ebao.ls.pty.ci.CustomerCI;
import com.ebao.ls.pty.vo.CustomerVO;
import com.ebao.ls.pty.vo.PartyVO;
import com.ebao.ls.pub.CodeCst;
import com.ebao.ls.pub.batch.impl.BatchJdbcTemplate;
import com.ebao.ls.pub.batch.impl.DataSourceWrapper;
import com.ebao.pub.batch.HibernateSession;
import com.ebao.pub.batch.help.BatchHelp;
import com.ebao.pub.batch.type.JobStatus;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.para.Para;
import com.ebao.pub.util.Trans;
import com.ebao.pub.util.TransUtils;

/**
 * CRS批次判定作業 提取跟催作業，產出報表用數據，並執行建檔
 */

public class CrsNonResponseBatchJob extends TGLBasePieceableJob implements ApplicationContextAware {

	public static final String BEAN_DEFAULT = "crsNonResponseBatchJob";

	private final Log logger = Log.getLogger(CrsNonResponseBatchJob.class);

	private static BatchJdbcTemplate batchJdbcTemplate = new BatchJdbcTemplate(new DataSourceWrapper());

	private static NamedParameterJdbcTemplate namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(
			batchJdbcTemplate);

	private ApplicationContext ctx;	

	@Resource(name = CRSDueDiligenceListDAO.BEAN_DEFAULT)
	private CRSDueDiligenceListDAO crsDueDiligenceListDAO;

	@Resource(name = CRSPartyService.BEAN_DEFAULT)
	private CRSPartyService crsPartyService;

	@Resource(name = CRSDueDiligenceListService.BEAN_DEFAULT)
	private CRSDueDiligenceListService crsDueDiligenceListService;
	
	@Resource(name = CustomerCI.BEAN_DEFAULT)
	private CustomerCI customerCI;		    

	private Date processDate;

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		ctx = applicationContext;
	}

	@Override
	protected String getJobName() {
		return BatchHelp.getJobName();
	}

	/**
	 * 單筆測試用
	 * 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public int executeById(String id) throws Exception {
		return execute(id);
	}
	@Override
	protected Object[] prepareIds() throws Throwable {		
		List<String> resultListUnion = new java.util.LinkedList<String>();  //PCR375449 Luisa
		// 批次判定天數
		String dueDay = Para.getParaValue(2061000051L);
		StringBuilder sql = new StringBuilder();
		sql.append(" select distinct (FIRST_VALUE(a.list_id) ");
		sql.append(" over(PARTITION by a.TARGET_CERTI_CODE,a.TARGET_NAME order by ");
		sql.append("   a.TARGET_SURVEY_DATE desc))||','||a.crs_id list_id from (SELECT t.*, c.crs_id ");
		sql.append("  FROM T_FATCA_DUE_DILIGENCE_LIST t , t_crs_party c  ");
		//PCR375449		sql.append(" WHERE t.TARGET_SURVEY_TYPE IN  ('08', '09', '10', '11','13', '14', '16')  ");
		sql.append(" WHERE t.TARGET_SURVEY_TYPE IN  ('08', '09', '10', '11','13', '14', '16','17')  ");  //PCR375449	
		sql.append(" AND SURVEY_SYSTEM_TYPE IN ('CRS','CRS/FATCA')");
		sql.append("  and t.target_certi_code = c.certi_code ");
		sql.append("  and c.certi_code is not null ");
		sql.append("  and t.TARGET_CERTI_CODE is not null  ");
		sql.append("  and c.doc_sign_status = '01' ");
		sql.append("  and c.build_type = '06' ");
		//IR428348  sql.append("  and t.TARGET_SURVEY_DATE > c.doc_sign_date  ");
		sql.append("  and ( (t.TARGET_SURVEY_TYPE ='17' and t.TARGET_SURVEY_DATE >= c.doc_sign_date ) "); //IR428348
		sql.append("       or (t.TARGET_SURVEY_TYPE <> '17' AND t.TARGET_SURVEY_DATE > c.doc_sign_date )) ");  //IR428348
		sql.append(" AND TARGET_SURVEY_DATE + " + dueDay + " <= :processDate ");
		sql.append(" ) a  union ");
		sql.append(" select distinct (FIRST_VALUE(b.list_id) ");
		sql.append(" over(PARTITION by b.TARGET_CERTI_CODE,b.TARGET_NAME order by ");
		sql.append("   b.TARGET_SURVEY_DATE desc))||','||b.crs_id list_id  from (SELECT t.*, c.crs_id ");
		sql.append("  FROM T_FATCA_DUE_DILIGENCE_LIST t , t_crs_party c  ");
		//PCR375449	sql.append(" WHERE t.TARGET_SURVEY_TYPE IN  ('08', '09', '10', '11','13', '14', '16')  ");
		sql.append(" WHERE t.TARGET_SURVEY_TYPE IN  ('08', '09', '10', '11','13', '14', '16','17')  "); //PCR375449
		sql.append(" AND SURVEY_SYSTEM_TYPE IN ('CRS','CRS/FATCA')");
		sql.append("  and c.certi_code is null ");
		sql.append("  and t.TARGET_CERTI_CODE is null  ");
		sql.append("  and c.POLICY_CODE = t.POLICY_CODE ");
		sql.append("  and c.NAME = t.TARGET_NAME ");
		sql.append("  and c.doc_sign_status = '01' ");
		sql.append("  and c.build_type = '06' ");
		//IR428348		sql.append("  and t.TARGET_SURVEY_DATE > c.doc_sign_date  ");
		sql.append("  and ( (t.TARGET_SURVEY_TYPE ='17' and t.TARGET_SURVEY_DATE >= c.doc_sign_date ) "); //IR428348
		sql.append("       or (t.TARGET_SURVEY_TYPE <> '17' AND t.TARGET_SURVEY_DATE > c.doc_sign_date )) ");  //IR428348		 
		sql.append(" AND TARGET_SURVEY_DATE + " + dueDay + " <= :processDate ");
		sql.append(" ) b ");

		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("processDate", BatchHelp.getProcessDate());
		List<String> results = namedParameterJdbcTemplate.queryForList(sql.toString(), params, String.class);

		//PCR375449,Luisa start		
		resultListUnion = ListUtils.union(resultListUnion, results);  //PCR375449,Luisa
		
		//當日有批次判定件，才要撈取不願簽署件
		if (results !=null && results.size() > 0){						
			StringBuilder sql2 = new StringBuilder();
			//無T_FATCA_DUE_DILIGENCE_LIST資料,list_id設為0
			sql2.append(" SELECT '0,'||a.crs_id list_id ");
			sql2.append("   FROM t_crs_party a ");
			sql2.append("  WHERE a.doc_sign_status ='03' ");
			sql2.append("  AND a.certi_code IS NOT NULL ");			
			sql2.append("  AND NOT EXISTS ( SELECT 1  ");
			sql2.append("                     FROM T_CRS_DUE_DILIGENCE_LIST x ");
			sql2.append("                    WHERE x.certi_code IS NOT NULL ");
			sql2.append("                      AND x.certi_code = a.certi_code ");
			sql2.append("                      AND x.target_survey_type = '18') "); //不願簽署 
			
			MapSqlParameterSource params2 = new MapSqlParameterSource();
			List<String> results2 = namedParameterJdbcTemplate.queryForList(sql2.toString(), params2, String.class);			
			resultListUnion = ListUtils.union(resultListUnion, results2);  //PCR375449,Luisa
		}
		//PCR375449,Luisa end

		//PCR375449  return results.toArray();
		return resultListUnion.toArray(); //PCR375449,Luisa		
	}

	@Override
	protected int execute(String id) throws Exception {

		String[] ids = id.split(",");
		String listId = ids[0];
		String crsId = ids[1];
		logger.info("execute - start list_id =" + id);
		//PCR375449,Luisa  start
		UserTransaction trans = null;
		int successCount = 0;
		int errorCount = 0;
		//PCR375449,Luisa  end

		if (!"0".equals(listId) ){   //PCR375449,Luisa			
			List<CRSDueCheckVO> fatcaList = getFatcaDueDiligenceListByListId(listId);
			if (CollectionUtils.isEmpty(fatcaList)) {
				logger.info("不符合處理條件，略過");
				return JobStatus.EXECUTE_SUCCESS;
			}
			/*  move up  -PCR375449,Luisa
			UserTransaction trans = null;
			int successCount = 0;
			int errorCount = 0;
			 */
			for (CRSDueCheckVO crsDueCheckVO : fatcaList) {
				try {
					trans = TransUtils.getUserTransaction();
					trans.begin();
					// 寫入 T_CRS_DUE_DILIGENCE_LIST
					CRSTempVO crsTempVO = new CRSTempVO();
					// create change
					Long insertChangeId;
					Change tChange = new Change();
					tChange.setChangeSource(CodeCst.CHANGE_SOURCE__PARTY);
					tChange.setChangeStatus(CodeCst.CHANGE_STATUS__WAITING_ISSUE);
					tChange.setOrgId(101L);
					insertChangeId = TChangeDelegate.create(tChange);
					crsTempVO.setChangeId(insertChangeId);
					crsTempVO.setCrsId(Long.parseLong(crsId));
					CRSDueDiligenceListVO crsDLVO = insertCRSDueDiligenceList(crsDueCheckVO);
					crsTempVO.setCrsDueDiligenceListVO(crsDLVO);
					// 判定
					String[] array = { crsDueCheckVO.getTargetId().toString(), crsDueCheckVO.getTargetCertiCode() };
					crsDueDiligenceListService.createCrsRecord(StringUtils.join(array, ","), crsTempVO,
							AppContext.getCurrentUser().getUserId(), BatchHelp.getProcessDate());
					successCount++;
					trans.commit();				
					logger.info("CRS判定完成");
				} catch (Exception ex) {
					batchLogger.error(" FatcaListId " + id + " 發生異常! 錯誤訊息 = " + ExceptionUtils.getFullStackTrace(ex));					
					errorCount++;
					trans.rollback();
				} finally {
					trans = null;
				}
			}
		} //PCR375449  listId <> "0"

		//PCR375449,Luisa start   listId=0, crs不願簽署件
		if ("0".equals(listId) ) {			
			CRSDueCheckVO crsDueCheckVO = createFatcaDueDiligenceListByCrsId(crsId) ;
			if (crsDueCheckVO != null) {							
				try {
					trans = TransUtils.getUserTransaction();
					trans.begin();
					CRSDueDiligenceListVO crsDLVO = insertCRSDueDiligenceList(crsDueCheckVO);
					successCount++;
					trans.commit();
				} catch (Exception ex) {
					batchLogger.error(" 不願簽署件  " + id + " 發生異常! 錯誤訊息 = " + ExceptionUtils.getFullStackTrace(ex));
					errorCount++;
					trans.rollback();
				} finally {
					trans = null;
				}
			}else 
				errorCount++;
		}
		//PCR375449,Luisa end
		logger.info("successCount = " + successCount + ", errorCount = " + errorCount);		
		logger.info("execute - end");

		if (errorCount == 0) {
			return JobStatus.EXECUTE_SUCCESS;
		} else if (successCount > 0) {
			return JobStatus.EXECUTE_PARTIAL_SUCESS;
		} else {
			return JobStatus.EXECUTE_FAILED;
		}
	}

	public List<CRSDueCheckVO> getFatcaDueDiligenceListByListId(String listId) {
		StringBuilder sql = new StringBuilder();
		//PCR375449  sql.append(" SELECT (CASE WHEN t.TARGET_SURVEY_TYPE in ( '09', '10', '11', '13') "
		sql.append(" SELECT (CASE WHEN t.TARGET_SURVEY_TYPE in ( '09', '10', '11', '13', '17') "  //PCR375449
				+ " THEN pkg_ls_crs_foreign_indi.f_get_six_indi(t.TARGET_ID, t.TARGET_CERTI_CODE, :processDate) "
				+ " WHEN t.TARGET_SURVEY_TYPE in ( '08', '14', '16')  THEN t.SIX_INDI "
				+ " ELSE SIX_INDI END) RE_SIX_INDI , t.* ");
		sql.append("  FROM T_FATCA_DUE_DILIGENCE_LIST t  ");
		sql.append(" WHERE t.list_id = :listId  ");
		// sql.append(" and t.target_certi_code = c.certi_code ");
		// sql.append(" and c.certi_code is not null ");

		MapSqlParameterSource params = new MapSqlParameterSource();

		params.addValue("listId", listId);
		params.addValue("processDate", BatchHelp.getProcessDate());
		List<CRSDueCheckVO> results = namedParameterJdbcTemplate.query(sql.toString(), params,
				new BeanPropertyRowMapper(CRSDueCheckVO.class));
		if (CollectionUtils.isEmpty(results)) {
			return null;
		}
		return results;
	}
	
	//PCR375449, Luisa -start
		 public CRSDueCheckVO createFatcaDueDiligenceListByCrsId(String crsId) {			 
			 StringBuffer sql = new StringBuffer();
			 sql.append("  SELECT b.policy_id, b.policy_code, ");		
			 sql.append("         a.certi_code as TARGET_CERTI_CODE, ");
			 sql.append("         p.party_id AS TARGET_ID, ");
			 sql.append("         a.name AS TARGET_NAME, ");
			 sql.append("         p.party_type AS TARGET_PARTY_TYPE, ");
			 sql.append("         b.LIABILITY_STATE as POLICY_STATUS, ");
			 sql.append("  pkg_ls_crs_foreign_indi.f_get_six_indi(p.party_id, a.certi_code, :processDate) as RE_SIX_INDI ");
			 sql.append("    FROM t_crs_party a, ");
			 sql.append("         T_CONTRACT_MASTER_C b, ");
			 sql.append("         T_CUSTOMER c, ");
			 sql.append("         T_PARTY p ");
			 sql.append("   WHERE a.crs_id = :crs_id ");
			 sql.append("   and a.certi_code IS NOT NULL ");
			 sql.append("   AND b.policy_code = a.policy_code ");
			 sql.append("   AND c.certi_code = a.certi_code ");
			 sql.append("   AND c.Status = 1 "); //有效資料
			 sql.append("   AND p.party_id = c.customer_id ");
			 
			 
			 MapSqlParameterSource params = new MapSqlParameterSource();
	         params.addValue("crs_id", crsId);
	         params.addValue("processDate", BatchHelp.getProcessDate());
	         
	         List<Map<String,Object>> mapList = namedParameterJdbcTemplate.queryForList(sql.toString(), params);         
	         if(CollectionUtils.isEmpty(mapList)) {
	        	 batchLogger.error("CRS_ID:"+crsId+" can not find customer data.");
	         	return null;
	         }
	         Map<String,Object> map = mapList.get(0); 
	         FatcaDueDiligenceList data = this.parseMapSN(map) ;
	         	         
	         CRSDueCheckVO vo = new CRSDueCheckVO();
		     data.copyToVO(vo, Boolean.FALSE);
		     Long listID = 0L ; //虛擬id 避免NullPoint錯誤
		     vo.setListID(listID); 
		     //更新業務員資料
		     try {
				crsDueDiligenceListService.updateServiceAgent(vo);
			} catch (Exception e) {				
				batchLogger.error("CRS_ID:"+crsId+" updateServiceAgent error. "+ExceptionUtils.getFullStackTrace(e));
			}
		     vo.setTargetSurveyType(FatcaSurveyType.NON_DOC_SIGN); 
		     vo.setSurveySystemType(CRSCodeCst.SURVEY_SYSTEM_TYPE_CRS); 
		     vo.setTargetSurveyDate(processDate);
			 
			 return vo;
		 }
		//PCR375449, Luisa -end


	public CRSDueDiligenceListVO insertCRSDueDiligenceList(CRSDueCheckVO fatcaVO) {
		logger.info("[CRS判定] T_CRS_DUE_DILIGENCE_LIST [LIST_ID] = " + fatcaVO.getListID());
		CRSDueDiligenceListVO crsDLVO = new CRSDueDiligenceListVO();
		crsDLVO.setPolicyId(fatcaVO.getPolicyId());
		crsDLVO.setPolicyCode(fatcaVO.getPolicyCode());
		crsDLVO.setLiabilityStatus(fatcaVO.getPolicyStatus().intValue());

		crsDLVO.setPartyId(fatcaVO.getTargetId());
		crsDLVO.setPartyType(fatcaVO.getTargetPartyType());
		crsDLVO.setCertiCode(fatcaVO.getTargetCertiCode());
		// crsDLVO.setCertiType(fatcaVO.getTargetPartyType());
		crsDLVO.setName(fatcaVO.getTargetName());
		crsDLVO.setAgentId(fatcaVO.getServiceAgent());
		crsDLVO.setAgentName(fatcaVO.getAgentName());
		crsDLVO.setRegisterCode(fatcaVO.getAgentRegisterCode());
		crsDLVO.setChannelId(fatcaVO.getChannelOrg());
		crsDLVO.setChannelCode(fatcaVO.getChannelCode());
		crsDLVO.setChannelType(fatcaVO.getChannelType());
		// crsDLVO.setToPCD(toPCD);
		// crsDLVO.setPolicyValue(fatcaVO.getP);

		crsDLVO.setTargetSurveyDate(BatchHelp.getProcessDate());
		crsDLVO.setTargetSurveyType(fatcaVO.getTargetSurveyType());
		crsDLVO.setSixIndi(fatcaVO.getReSixIndi());
		crsDLVO.setAmlIndi(fatcaVO.getAmlIndi());
		
		//PCR426100  更新caseId
		if(fatcaVO.getTargetSurveyType().equals(FatcaSurveyType.FATCA_SURVEY_TYPE__CLAIM_PAYMENT_FOLLOW_UP_TARGET)) {
			for (String caseId : getCaseId(fatcaVO)) {
				crsDLVO.setCaseId(Long.parseLong(caseId));
			}
		}

		crsDueDiligenceListDAO.createOrUpdateVO(crsDLVO);

		return crsDLVO;
	}


	/**
	 * 父類別的transation是以一片為單位，為了避免同片內一筆rollback時，把上一筆的變更也一同rollback，覆寫父類別的方法
	 */
	@Override
	public int exeSingleRecord(String id) throws Exception {
		long start = System.currentTimeMillis();

		UserTransaction trans = null;
		int ret = 0;
		try {
			trans = Trans.getUserTransaction();
			trans.begin();

			ret = this.execute(id);
			this.updateStatus(id, ret);
			if (ret != JobStatus.EXECUTE_SUCCESS) {
				batchLogger.error("執行不成功! ID:{0} 狀態:{1}", id, ret);
			}
			long stop = System.currentTimeMillis();
			if (enableStopwatch()) {
				batchLogger.debug("ID:{0} 執行花費時間:{1} ms", id, stop - start);
			}
			trans.commit();
		} catch (Exception e) {
			trans.rollback();
			batchLogger.error("執行不成功! ID:{0} errMsg:{1}", id, e.toString());
		}
		return ret;
	}

	/**
	 * 更新t_batch_job_record的分片紀錄執行結果
	 * 
	 * @param id
	 * @param status
	 */
	private void updateStatus(String id, int status) {
		Long runId = BatchHelp.getRunId();

		Session s = HibernateSession.currentSession();
		String sql = "update t_batch_job_record set status=?, finish_time=sysdate where run_id = ? and identity=? ";
		Connection con = s.connection();
		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement(sql);
			ps.setInt(1, status);
			ps.setLong(2, runId);
			ps.setString(3, id);
			ps.execute();
		} catch (SQLException e) {
			// e.printStackTrace();
			batchLogger.error("TGLBasePieceableJob.updateStatus error", e);
		} finally {
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception e) {

			}
		}
	}

	public Date getProcessDate() {
		return processDate;
	}

	public void setProcessDate(Date processDate) {
		this.processDate = processDate;
	}

	//PCR375449 luisa	
	private FatcaDueDiligenceList parseMapSN(Map<String, Object> map) { 
		FatcaDueDiligenceList data = new FatcaDueDiligenceList();
		data.setPolicyId(MapUtils.getLong(map, "POLICY_ID"));
		data.setPolicyCode(MapUtils.getString(map, "POLICY_CODE"));
		data.setPolicyStatus(MapUtils.getLong(map, "POLICY_STATUS"));
		data.setTargetId(MapUtils.getLong(map, "TARGET_ID"));
		data.setTargetName(MapUtils.getString(map, "TARGET_NAME"));
		data.setTargetCertiCode(MapUtils.getString(map, "TARGET_CERTI_CODE"));
	
		data.setTargetPartyType(MapUtils.getString(map,  "TARGET_PARTY_TYPE"));
		//data.setTargetSurveyType(surveyType);

		if(map.containsKey("LIST_ID")){
			data.setListID(MapUtils.getLong(map, "LIST_ID"));
		}
		return data;
	}
	
	//PCR426100  取得理賠caseId For 理賠符合國外跡象報表
	private List<String> getCaseId(CRSDueCheckVO fatcaVO) {
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT  XMLTYPE(C.CONTENT).EXTRACT('//caseId/text()').GETSTRINGVAL() ");
		sql.append("  FROM   t_fatca_due_diligence_list fdd1, t_fatca_due_diligence_list fdd2, t_clob c");
		sql.append("  WHERE  fdd1.policy_code = :policyCode");
		sql.append("  AND    fdd1.target_certi_code = :targetCertiCode ");
		sql.append("  AND    fdd1.target_survey_type = '11' ");
		sql.append("  AND    fdd1.policy_code = fdd2.policy_code ");
		sql.append("  AND    fdd1.target_certi_code = fdd2.target_certi_code ");
		sql.append("  AND    fdd2.target_survey_type ='07' ");
		sql.append("  AND    fdd2.report_id=c.clob_id ");
		
		MapSqlParameterSource params = new MapSqlParameterSource();
		
		params.addValue("policyCode", fatcaVO.getPolicyCode());
		params.addValue("targetCertiCode", fatcaVO.getTargetCertiCode());
  
		List<String> results = null;
		try {
			 results = namedParameterJdbcTemplate.queryForList(sql.toString(), params, String.class);
		} catch (Exception e) {	
			 batchLogger.debug(fatcaVO.getTargetCertiCode()+" get caseId error. "+ExceptionUtils.getFullStackTrace(e));
		}
		
		if (CollectionUtils.isEmpty(results)) {
			return null;
		}
		return results;
	}

}