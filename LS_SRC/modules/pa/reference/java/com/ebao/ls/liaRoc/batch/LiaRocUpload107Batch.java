package com.ebao.ls.liaRoc.batch;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.SequenceInputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.activation.DataHandler;
import javax.annotation.Resource;
import javax.transaction.UserTransaction;

import org.apache.commons.io.FileUtils;

import com.ebao.foundation.module.para.Para;
import com.ebao.foundation.module.web.action.ExceptionInfoUtils;
import com.ebao.ls.batch.file.filehelper.FileGenericHelper;
import com.ebao.ls.cs.logger.ApplicationLogger;
import com.ebao.ls.cs.logger.helper.AppLogDataHelper;
import com.ebao.ls.liaRoc.LiaRocCst;
import com.ebao.ls.liaRoc.bo.LiaRocUpload107Detail2020VO;
import com.ebao.ls.liaRoc.bo.LiaRocUpload107DetailVO;
import com.ebao.ls.liaRoc.ci.LiaRocFileCI;
import com.ebao.ls.liaRoc.ci.LiaRocUpload107CI;
import com.ebao.ls.liaRoc.vo.LiaRocUploadDetail1072020WrapperVO;
import com.ebao.ls.liaRoc.vo.LiaRocUploadDetail107WrapperVO;
import com.ebao.ls.liaRoc.vo.LiaRocUploadDetailBaseVO;
import com.ebao.ls.ws.ci.esp.ESPWSServiceCI;
import com.ebao.ls.ws.vo.InputStreamDataSoure;
import com.ebao.ls.ws.vo.client.esp.FileObject;
import com.ebao.ls.ws.vo.client.esp.RqHeader;
import com.ebao.ls.ws.vo.client.esp.StandardResponse;
import com.ebao.ls.ws.vo.cust.liaRoc.LiaRocUploadRequestRoot;
import com.ebao.pub.batch.exe.BatchDB;
import com.ebao.pub.batch.help.BatchHelp;
import com.ebao.pub.batch.type.JobStatus;
import com.ebao.pub.fileresolver.writer.FixedCharLengthWriter;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.util.BeanUtils;
import com.ebao.pub.util.DateUtils;
import com.ebao.pub.util.EnvUtils;
import com.ebao.pub.util.StringUtils;
import com.ebao.pub.util.Trans;

public class LiaRocUpload107Batch extends LiaRocUploadBatch {

	public static final String BEAN_DEFAULT = "liaRocUpload107Batch";
	
	private String requestFileName = null;

	private String prefixReqFileName = null;

	//IR353084
	//protected String charset = "utf8";

	@Resource(name = ESPWSServiceCI.BEAN_DEFAULT)
	private ESPWSServiceCI espWSServiceCI;

	@Resource(name = LiaRocUpload107CI.BEAN_DEFAULT)
	private LiaRocUpload107CI liaRocUpload107CI;

	@Resource(name = LiaRocFileCI.BEAN_DEFAULT)
	private LiaRocFileCI liaRocFileCI;

	/**
	 * 用來將通報明細資料轉換成 txt 檔的 writer
	 */
	@Resource(name = "liaRocUpload107RequestWriter")
	private FixedCharLengthWriter fixedLengthWriter;

	protected String getUploadType() {
		return LiaRocCst.LIAROC_UL_TYPE_107;
	}

	protected String getUploadModule() {
		return LiaRocCst.LIAROC_UL_UPLOAD_TYPE_CLM;
	}

	/**
	 * <p>Description : 查詢要通報的明細資料</p>
	 * <p>Created By : tgl143</p>
	 * <p>Create Time : Sep 21, 2016</p>
	 * @return
	 */
	@SuppressWarnings("unchecked")
	protected List<LiaRocUploadDetail107WrapperVO> getUploadDetails() {
		// 查詢107通報明細資料
		List<LiaRocUpload107DetailVO> uploadList = liaRocUpload107CI.findNotify107Detail();
		List<LiaRocUploadDetail107WrapperVO> voList = new ArrayList<LiaRocUploadDetail107WrapperVO>();
	    voList = (List<LiaRocUploadDetail107WrapperVO>) BeanUtils.copyCollection(LiaRocUploadDetail107WrapperVO.class,
	        		uploadList);
		return voList;
	}

	/**
	 * <p>Description : 取得上傳通報資料檔檔名</p>
	 * <p>Created By : tgl143</p>
	 * <p>Create Time : Sep 21, 2016</p>
	 * @return
	 */
	public String getUploadRequestFileName() {
		if (StringUtils.isNullOrEmpty(requestFileName)) {
			requestFileName = liaRocFileCI.getFileName(getPrefixReqFileName());
		}
		return requestFileName;
	}

	/**
	 * <p>Description : 取得公會通報檔名前綴</p>
	 * <p>Created By : tgl143</p>
	 * <p>Create Time : Sep 21, 2016</p>
	 * @return
	 */
	public String getPrefixReqFileName() {
		// 公會通報檔檔名前綴設在 t_para_def 裡。
		if (StringUtils.isNullOrEmpty(prefixReqFileName)) {
			prefixReqFileName = Para.getParaValue(LiaRocCst.PARAM_LIAROC_UL_FILENAME_REQUEST);
		}
		return prefixReqFileName;
	}

	/**
	 * <h1>批處理主程式<h1>
	 * <p>通報主檔資料是以保單為單位，通報明細資料是以保項為單位，因此一筆通報主檔資料會有多筆的通報明細資料;
	 * 上傳公會時，若該主檔下的任一筆明細資料解析發生錯誤，則該筆通報主檔狀態會是P(解析有錯)並且不會發送公會<p>
	 */
	@Override
	public int mainProcess() throws Exception {
		int result = -1;
		if (!liaRocUploadCommonService.isLiaRocV2020Process()) {
			result = mainProcess2019();
		}
		result = mainProcess2020();
		return result;
	}

	public int mainProcess2019() throws Exception {

		ApplicationLogger.setSystemCode(AppLogDataHelper.SYSTEM_CODE_UNB);
		ApplicationLogger.setJobName("理賠107通報");
		ApplicationLogger.setPolicyCode("LiaRocUpload107");
		ApplicationLogger.setOptionId(DateUtils.date2String(new Date(), "yyyyMMdd HHmmss"));

		if (BatchDB.getSubId() != null) {
			ApplicationLogger.addLoggerData("[INFO]INFO]Batch Job Starting.. RunId=" + BatchHelp.getRunId());
		}

		ApplicationLogger.addLoggerData("[INFO]107 Batch Job Starting..");

		// 批處理狀況預設為成功
		int executeResult = JobStatus.EXECUTE_SUCCESS;

		// 記錄各通報主檔資料更新回 DB 的狀態
		Map<Long, String> uploadStatus = new HashMap<Long, String>();

		UserTransaction trans = null;
		try {

			trans = Trans.getUserTransaction();
			trans.begin();

			// 查詢新契約-收件的通報明細資料
			List<LiaRocUploadDetail107WrapperVO> uploadListW = this.getUploadDetails();
			
			List<LiaRocUploadDetailBaseVO> uploadList =  new ArrayList<LiaRocUploadDetailBaseVO>();

			if (uploadListW.size() > 0) {
				for(LiaRocUploadDetail107WrapperVO vo: uploadListW) {
            		uploadList.add(vo);
            	}
				String fileName = this.getUploadRequestFileName();
				ApplicationLogger.addLoggerData("[INFO]上傳通報檔名: " + fileName);

				// 傳送通報資料
				executeResult = super.processUpload(uploadList, fileName, uploadStatus);

			} else {
				ApplicationLogger.addLoggerData("[INFO]沒有要通報的資料");
			}

			ApplicationLogger.addLoggerData("[INFO]Batch Job finished..");
			
			trans.commit();
		} catch (Throwable e) {
			trans.rollback();
			String message = ExceptionInfoUtils.getExceptionMsg(e);
			ApplicationLogger.addLoggerData("[ERROR]發生不可預期的錯誤, Error Message:" + message);
			executeResult = JobStatus.EXECUTE_FAILED;
		} finally {
			ApplicationLogger.flush();
		}

		return executeResult;

	}

	/**
	 * <p>Description : 呼叫服務平台，傳送公會通報資料</p>
	 * <p>Created By : tgl143</p>
	 * <p>Create Time : Sep 21, 2016</p>
	 * @param uploadList 傳送公會的通報明細資料 VO
	 * @param uploadFileName 公會通報檔名
	 * @param uploadStatus 公會通報主檔資料的處理狀態(回傳值)
	 * @return JobStatus.EXECUTE_SUCCESS/JobStatus.EXECUTE_FAILED
	 */
	@SuppressWarnings("deprecation")
	public int processUpload107(List<LiaRocUpload107DetailVO> uploadList, String uploadFileName,
			Map<Long, String> uploadStatus) {

		// 預設為成功
		int executeResult = JobStatus.EXECUTE_SUCCESS;

		// 記錄失敗的通報主檔 list_id
		ArrayList<Long> failUploadIds = new ArrayList<Long>();

		// 有資料要通報
		if (uploadList != null && uploadList.size() > 0) {

			int totalMasterCount = this.getUploadMasterCount(uploadList);
			String requestUUID = null;

			ApplicationLogger.addLoggerData("[INFO]通報主檔資料筆數: " + totalMasterCount);
			ApplicationLogger.addLoggerData("[INFO]通報明細資料筆數: " + uploadList.size());

			InputStream inStream = null;

			// @ 解析通報資料
			try {

				ApplicationLogger.addLoggerData("[INFO]解析通報資料..");

				// 把要通報的資料解析成 txt 格式
				inStream = this.parseToText107(uploadList, failUploadIds);

				// 不為 null，有要發送公會的資料
				// TODO:測試階段使用
				if (inStream != null) {
					// this.saveUploadFile(uploadFileName, inStream);
				}

				ApplicationLogger.addLoggerData("[INFO]通報資料解析完成..");

			} catch (Exception e) {

				// 將所有通報主檔狀態設為 P
				for (LiaRocUpload107DetailVO detail : uploadList) {
					if (!uploadStatus.containsKey(detail.getUploadListId())) {
						uploadStatus.put(detail.getUploadListId(), LiaRocCst.LIAROC_UL_STATUS_PARSE_ERROR);
					}
				}

				ApplicationLogger.addLoggerData("[ERROR]通報資料解析失敗，發生不可預期的錯誤, Error Message: " + e.getMessage());
				executeResult = JobStatus.EXECUTE_FAILED;

			}
			// end.

			// @ 資料解析正確，呼叫服務平台傳送通報資料
			if (executeResult == JobStatus.EXECUTE_SUCCESS) {

				// 失敗的明細資料筆數
				int failDetailCount = 0;

				// 有解析失敗的主檔ID
				if (failUploadIds.size() > 0) {
					for (Long id : failUploadIds) {
						if (!uploadStatus.containsKey(id)) {
							uploadStatus.put(id, LiaRocCst.LIAROC_UL_STATUS_PARSE_ERROR);
						}
					}
					failDetailCount = this.calUploadDetailCount(uploadList, failUploadIds);
					ApplicationLogger.addLoggerData("[WARN]警告：有解析失敗的通報明細資料..");
					ApplicationLogger.addLoggerData("[WARN]有 " + failUploadIds.size() + " 筆主檔(" + failDetailCount
							+ " 筆明細) 資料解析失敗..");
				}

				// 有要發送資料
				if (inStream != null) {

					LiaRocUploadRequestRoot requestVO = new LiaRocUploadRequestRoot();

					requestVO.setCreateTime(DateUtils.date2string(AppContext.getCurrentUserLocalTime(),
							"yyyyMMddHHmmss"));
					requestVO.setTotalCount(uploadList.size() - failDetailCount);
					requestVO.setUploadModule(getUploadModule());
					requestVO.setUploadType(getUploadType());
					requestVO.setUploadFileName(uploadFileName);

					try {

						//                        List<DataHandler> dataHandlers = new ArrayList<DataHandler>();
						//                        DataHandler dataHandler = new DataHandler(new InputStreamDataSoure(inStream));
						//                        dataHandlers.add(dataHandler);

						List<FileObject> files = new ArrayList<FileObject>();
						FileObject file = new FileObject();
						DataHandler dh = new DataHandler(new InputStreamDataSoure(inStream));
						file.setFile(dh);
						file.setFileName(dh.getName());
						files.add(file);

						ApplicationLogger.addLoggerData("[INFO]準備呼叫服務平台....");

						RqHeader head = new RqHeader();

						// Call ESP...
						// 呼叫服務平台時，有可能它會回報失敗，則要將通報狀態設定為F
						// 以區分是 call ESP 時發生錯誤，還是 ESP 有接到，只是它回傳資料接收失敗
						StandardResponse rs = espWSServiceCI.liaROCUpload(head, requestVO, files);
						//RespXml resp = commonWSCI.call(WebServiceName.ESP_LIAROC_UPLOAD_NB, requestVO, dataHandlers);

						String statusIndi = LiaRocCst.LIAROC_UL_STATUS_SEND;

						if (rs != null
								&& !rs.getRsHeader().getReturnCode()
										.equals(LiaRocCst.LIAROC_UPLOAD_ESP_SUCCESSFUL_CODE)) {
							statusIndi = LiaRocCst.LIAROC_UL_STATUS_SEND_FAIL;
						}

						requestUUID = (rs != null) ? rs.getRsHeader().getRqUID() : null;

						for (LiaRocUpload107DetailVO detail : uploadList) {
							if (!uploadStatus.containsKey(detail.getUploadListId())) {
								uploadStatus.put(detail.getUploadListId(), statusIndi);
							}
						}

						ApplicationLogger.addLoggerData("[INFO]呼叫服務平台發送通報完成..");

						ApplicationLogger.addLoggerData("[INFO]發送通報 - 主檔: " + (totalMasterCount - failUploadIds.size())
										+ " 筆, 明細: " + (uploadList.size() - failDetailCount) + " 筆..");

					} catch (Exception e) {

						// 傳送到服務平台時發生錯誤，將通報狀態設定為(E:通報流程有誤)
						for (LiaRocUpload107DetailVO detail : uploadList) {
							if (!uploadStatus.containsKey(detail.getUploadListId())) {
								uploadStatus.put(detail.getUploadListId(), LiaRocCst.LIAROC_UL_STATUS_PROCESS_ERROR);
							}
						}

						ApplicationLogger.addLoggerData("[ERROR]呼叫服務平台發送通報資料失敗，Error Message: " + e.getMessage());
						executeResult = JobStatus.EXECUTE_FAILED;
					}

				}

			}
			// end..

			// 將通報的狀態更新回 DB
			ApplicationLogger.addLoggerData("[INFO]更新通報狀態..");
			int serialNum = Integer.parseInt(uploadFileName.replaceAll(getPrefixReqFileName(), ""));
			liaRocUpload2019CI.updateUploadStatus(uploadStatus, requestUUID, serialNum,
							new Date(), AppContext.getCurrentUser().getUserId());

		} else {
			ApplicationLogger.addLoggerData("[INFO]沒有要通報的資料..");
		}

		return executeResult;

	}

	/**
	 * <p>Description : 將明細資料 VO 解析為 txt 格式的資料</p>
	 * <p>Created By : tgl143</p>
	 * <p>Create Time : Sep 21, 2016</p>
	 * @param detailList 明細資料 VO List
	 * @param failUploadIds 解析失敗的通報主檔 LIST_ID(當做回傳值)
	 * @return
	 * @throws UnsupportedEncodingException 
	 * @throws Exception
	 */
	public InputStream parseToText107(List<LiaRocUpload107DetailVO> detailList, ArrayList<Long> failUploadIds)
			throws UnsupportedEncodingException {

		if (detailList == null || detailList.size() == 0)
			return null;

		Map<String, InputStream> detailStreams = new LinkedHashMap<String, InputStream>();
		InputStream singleStream = null;

		// 解析明細資料
		for (LiaRocUpload107DetailVO detail : detailList) {

			// 這筆明細資料被排除，因為它所屬的通報主檔(以保單為群組單位)，有明細檔資料解析有錯，
			// 因此該通報主檔下的明細資料一律都不通報，離開不處理
			if (failUploadIds.contains(detail.getUploadListId())) {
				ApplicationLogger.addLoggerData("[WARN]此筆明細在排除通報的主檔清單中，不予處理 (LIST_ID:" + detail.getListId()
						+ ", POLICY_CODE:" + detail.getPolicyCode() + ")");
				continue;
			}

			List<Map<String, Object>> maps = new ArrayList<Map<String, Object>>();
			Map<String, Object> map = null;
			boolean isParsePass = true;

			try {
				map = this.getMap(detail);
				maps.add(map);
			} catch (Exception e) {
				isParsePass = false;
				ApplicationLogger.addLoggerData("[ERROR]通報明細資料 {LIST_ID:" + detail.getListId() + ", POLICY_CODE:" + detail.getPolicyCode()
								+ "} getMap() 執行失敗, Error Message: " + e.getMessage());
			}

			if (isParsePass) {

				try {

					fixedLengthWriter.setCharset(charset);
					singleStream = fixedLengthWriter.write(maps);

					// key 為:主檔 id + 明細檔 id
					detailStreams.put(detail.getUploadListId() + ":" + detail.getListId(), singleStream);

				} catch (NullPointerException e) {
					isParsePass = false;
					ApplicationLogger.addLoggerData("[ERROR]通報明細資料 {LIST_ID:" + detail.getListId()
							+ ", POLICY_CODE:" + detail.getPolicyCode() + "} 解析失敗, 請檢查是否存在 NULL 的欄位資料..");
				} catch (Exception e) {
					isParsePass = false;
					ApplicationLogger.addLoggerData("[ERROR]通報明細資料 {LIST_ID:" + detail.getListId()
							+ ", POLICY_CODE:" + detail.getPolicyCode() + "} 解析失敗, Error Message:"
							+ e.getMessage());
				}

			}

			if (!isParsePass) {

				// 解析失敗，記錄此筆通報為失敗
				failUploadIds.add(detail.getUploadListId());

				String policyCode = detail.getPolicyCode();
				//policyCode = policyCode.substring(0, policyCode.length() - 2); // 只取保單號碼的部份

				ApplicationLogger.addLoggerData("[WARN]列入排除發送通報主檔名單 {主檔 LIST_ID:" + detail.getUploadListId()
						+ ", POLICY_CODE:" + policyCode + "}");

			}

		}
		// end.

		// 移除掉有錯的明細資料，將有錯的主檔下的所有明細資料移除掉
		if (failUploadIds.size() > 0) {
			for (Long failId : failUploadIds) {
				Iterator<String> itr = detailStreams.keySet().iterator();
				while (itr.hasNext()) {
					String[] key = itr.next().split(":");
					if (failId.equals(Long.valueOf(key[0]))) {
						itr.remove();
					}
				}
			}
		}
		// end.

		// 將多筆資料的 InputStream 合併為一個..
		Vector<InputStream> streamList = new Vector<InputStream>();
		int lastIndex = 0;
		for (Map.Entry<String, InputStream> entry : detailStreams.entrySet()) {

			streamList.add(entry.getValue());
			lastIndex++;

			if (lastIndex < detailStreams.size()) { // 最後一行不用加換行符號..
				streamList.add(new ByteArrayInputStream("\r\n".getBytes(charset)));
			}

		}

		InputStream sequenceInputStream = new SequenceInputStream(streamList.elements());

		return sequenceInputStream;

	}

	/**
	 * <p>Description : 儲存上傳檔</p>
	 * <p>Created By : tgl143</p>
	 * <p>Create Time : Sep 22, 2016</p>
	 * @param fileName
	 * @param inputStream
	 * @throws Exception
	 */
	public void saveUploadFile(String fileName, InputStream inputStream) throws Exception {

		File folder = new File(EnvUtils.getSendToInternalSharePath() , "liaRoc107");
		if(folder.exists() == false) {
			FileUtils.forceMkdir(folder);
			// 2016-11-29 依長官指示加上 read write
			FileGenericHelper.setFolderReadWritable(folder);
		}

		File uploadFile =  new File(folder, fileName + ".txt" );

		ApplicationLogger.addLoggerData("[INFO]LOG 107檔案至=" + uploadFile.getAbsolutePath());

		OutputStream oStream = new FileOutputStream(uploadFile);

		byte[] buffer = new byte[8192];
		int bRead;

		while ((bRead = inputStream.read(buffer)) != -1) {
			oStream.write(buffer, 0, bRead);
		}

		inputStream.close();
		oStream.flush();
		oStream.close();

	}

	/**
	 * <p>Description : 依主檔的 id list 判斷有多少明細資料筆數</p>
	 * <p>Created By : tgl143</p>
	 * <p>Create Time : Sep 22, 2016</p>
	 * @param uploadList
	 * @param uploadIds
	 * @return
	 */
	private int calUploadDetailCount(List<LiaRocUpload107DetailVO> uploadList, ArrayList<Long> uploadIds) {

		int count = 0;

		for (LiaRocUpload107DetailVO detail : uploadList) {
			if (uploadIds.contains(detail.getUploadListId())) {
				count += 1;
			}
		}

		return count;

	}

	/**
	 * <p>Description : 取得通報主檔資料筆數</p>
	 * <p>Created By : tgl143</p>
	 * <p>Create Time : Sep 21, 2016</p>
	 * @param uploadList 通報明細資料
	 * @return 通報主檔資料筆數
	 */
	private int getUploadMasterCount(List<LiaRocUpload107DetailVO> uploadList) {

		List<Long> countedIds = new ArrayList<Long>();

		int count = 0;

		for (LiaRocUpload107DetailVO detail : uploadList) {
			if (!countedIds.contains(detail.getUploadListId())) {
				count++;
				countedIds.add(detail.getUploadListId());
			}
		}

		return count;
	}

	/**
	 * <p>Description : 將明細資料轉為 HasMap 型別的資料</p>
	 * <p>Created By : tgl143</p>
	 * <p>Create Time : Sep 21, 2016</p>
	 * @param uploadList
	 * @return
	 * @throws Exception
	 */
	private Map<String, Object> getMap(LiaRocUpload107DetailVO uploadList) throws Exception {

		Map<String, Object> result = new HashMap<String, Object>();
		BeanInfo info = Introspector.getBeanInfo(uploadList.getClass());

		SimpleDateFormat df2 = new SimpleDateFormat("yyyyMMdd");

		for (PropertyDescriptor pdr : info.getPropertyDescriptors()) {

			Method reader = pdr.getReadMethod();
			String key = pdr.getName();
			Object value = reader.invoke(uploadList);

			// 如果該欄位為日期，將它轉成民國年
			if (value != null && value instanceof Date) {
				String mDate = convertTWDate((Date) value, df2);
				result.put(key, mDate);
			} else {
				result.put(key, value);
			}

		}

		return result;

	}

	@Override
	protected FixedCharLengthWriter getFixedLengthWriter() {
		return fixedLengthWriter;
	}


	/* 2019 */
	/* ************************************************************************************************** */
	/* 2020 */	
	public int mainProcess2020() throws Exception {

		ApplicationLogger.setSystemCode(AppLogDataHelper.SYSTEM_CODE_UNB);
		ApplicationLogger.setJobName("理賠107通報2020");
		ApplicationLogger.setPolicyCode("LiaRocUpload107");
		ApplicationLogger.setOptionId(DateUtils.date2String(new Date(), "yyyyMMdd HHmmss"));

		
		if (BatchDB.getSubId() != null) {
			ApplicationLogger.addLoggerData("[INFO]INFO]Batch Job Starting.. RunId=" + BatchHelp.getRunId());
		}

		int result = this.subProcess2020(LiaRocCst.LIAROC_UL_TYPE_107);

		ApplicationLogger.flush();

		return result;
	}
	
	/**
	 * <p>Description : 查詢要通報的明細資料</p>
	 * @return
	 */
	@SuppressWarnings("unchecked")
	protected List<LiaRocUploadDetail1072020WrapperVO> getUpload2020Details() {
		// 查詢107通報明細資料
		List<LiaRocUpload107Detail2020VO> uploadList = liaRocUpload1072020CI.findNotify107Detail();
		List<LiaRocUploadDetail1072020WrapperVO> voList = new ArrayList<LiaRocUploadDetail1072020WrapperVO>();
	    voList = (List<LiaRocUploadDetail1072020WrapperVO>) BeanUtils.copyCollection(LiaRocUploadDetail1072020WrapperVO.class,
	        		uploadList);
		return voList;
	}

}
