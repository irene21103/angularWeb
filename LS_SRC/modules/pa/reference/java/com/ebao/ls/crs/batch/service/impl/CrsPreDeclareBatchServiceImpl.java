package com.ebao.ls.crs.batch.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.ebao.ls.chl.service.AgentService;
import com.ebao.ls.chl.service.ChannelOrgService;
import com.ebao.ls.chl.vo.ChannelOrgVO;
import com.ebao.ls.chl.vo.ScAgentDataVO;
import com.ebao.ls.chl.vo.ScServiceAgentDataVO;
import com.ebao.ls.chl.vo.ScServiceRepresentDataVO;
import com.ebao.ls.crs.batch.service.CRSDueDiligenceListService;
import com.ebao.ls.crs.batch.service.CrsPreDeclareBatchService;
import com.ebao.ls.crs.batch.vo.CRSTempVO;
import com.ebao.ls.crs.ci.CRSPartyCI;
import com.ebao.ls.crs.ci.CRSPartyReportVO;
import com.ebao.ls.crs.constant.CRSCodeCst;
import com.ebao.ls.crs.constant.CRSCodeType;
import com.ebao.ls.crs.data.batch.CRSDueDiligenceListDAO;
import com.ebao.ls.crs.data.bo.CRSDueDiligenceList;
import com.ebao.ls.crs.data.bo.CRSPartyLog;
import com.ebao.ls.crs.data.identity.CRSPartyLogDAO;
import com.ebao.ls.crs.identity.CRSPartyLogService;
import com.ebao.ls.crs.identity.CRSPartyService;
import com.ebao.ls.crs.vo.CRSDueDiligenceListVO;
import com.ebao.ls.crs.vo.CRSPartyLogVO;
import com.ebao.ls.crs.vo.CRSPartyVO;
import com.ebao.ls.cs.commonflow.data.TChangeDelegate;
import com.ebao.ls.cs.commonflow.data.bo.Change;
import com.ebao.ls.fatca.batch.data.FatcaBenefitSurveyListDAO;
import com.ebao.ls.fatca.constant.FatcaRole;
import com.ebao.ls.fatca.constant.FatcaSurveyType;
import com.ebao.ls.fatca.data.bo.FatcaDueDiligenceList;
import com.ebao.ls.fatca.util.FatcaDBeanQuery;
import com.ebao.ls.fatca.util.FatcaJDBC;
import com.ebao.ls.fatca.vo.FatcaDueDiligenceListVO;
import com.ebao.ls.foundation.service.GenericServiceImpl;
import com.ebao.ls.notification.event.cs.FmtPos0350Event;
import com.ebao.ls.notification.event.cs.FmtPos0510Event;
import com.ebao.ls.pa.pty.service.CertiCodeCI;
import com.ebao.ls.pa.pub.bs.PolicyHolderService;
import com.ebao.ls.pa.pub.service.PolicyService;
import com.ebao.ls.pty.ds.CustomerService;
import com.ebao.ls.pty.vo.CompanyCustomerVO;
import com.ebao.ls.pty.vo.CustomerVO;
import com.ebao.ls.pub.CodeCst;
import com.ebao.ls.pub.batch.impl.BatchJdbcTemplate;
import com.ebao.ls.pub.batch.impl.DataSourceWrapper;
import com.ebao.ls.pub.cst.YesNo;
import com.ebao.ls.sc.vo.AgentChlVO;
import com.ebao.pub.batch.help.BatchHelp;
import com.ebao.pub.batch.type.LogLevel;
import com.ebao.pub.batch.util.BatchLogUtils;
import com.ebao.pub.event.ApplicationEventVO;
import com.ebao.pub.event.EventService;
import com.hazelcast.util.StringUtil;
import com.ebao.ls.pa.pub.vo.PolicyHolderVO;

public class CrsPreDeclareBatchServiceImpl
		extends GenericServiceImpl<CRSDueDiligenceListVO, CRSDueDiligenceList, CRSDueDiligenceListDAO>
		implements CrsPreDeclareBatchService {

	private static BatchJdbcTemplate batchJdbcTemplate = new BatchJdbcTemplate(new DataSourceWrapper());
	private static NamedParameterJdbcTemplate namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(
			batchJdbcTemplate);

	@Resource(name = CRSPartyCI.BEAN_DEFAULT)
	private CRSPartyCI crsPartyCI;

	@Resource(name = CustomerService.BEAN_DEFAULT)
	private CustomerService customerService;

	@Resource(name = CRSPartyLogDAO.BEAN_DEFAULT)
	private CRSPartyLogDAO crsPartyLogDAO;

	@Resource(name = EventService.BEAN_DEFAULT)
	private EventService eventService;

	@Resource(name = CRSPartyService.BEAN_DEFAULT)
	private CRSPartyService crsPartyService;

	@Resource(name = PolicyService.BEAN_DEFAULT)
	private PolicyService policyService;

	@Resource(name = CRSPartyLogService.BEAN_DEFAULT)
	private CRSPartyLogService crsPartyLogService;

	@Resource(name = CRSPartyCI.BEAN_DEFAULT)
	private CRSPartyCI partyCI;
	
	@Resource(name = PolicyHolderService.BEAN_DEFAULT)
	protected PolicyHolderService phService;

	private static Logger logger = Logger.getLogger(CrsBenefitSurveyListServiceImpl.class);

	// 孤兒保單派發至 PCD = HO
	private final Long DELIVER_TO_HO = 4L;

	// 虛擬業務員=孤兒保單
	private final Long ORPHAN = 2L;

	@Override
	protected CRSDueDiligenceListVO newEntityVO() {
		return new CRSDueDiligenceListVO();
	}

	private void log(String message) {
		logger.info("[SYSOUT][CrsBenefitSurveyListServiceImpl]" + message);
		BatchLogUtils.addLog(LogLevel.INFO, null, message);
	}

	private void logWithId(String id, String message) {
		if (StringUtils.isNotBlank(id)) {
			message = "[" + id + "] " + message;
		}
		log(message);
	}

	/**
	 * CRS LOG建檔與主檔提交
	 * 
	 * @param voList
	 * @param processDate
	 * @throws Exception
	 */
	@Override
	public void createCrsRecord(String crsId, String flag, Long userId, Date processDate) throws Exception {
		logWithId(crsId, "start");
		CRSPartyVO party = crsPartyService.findByCrsId(Long.valueOf(crsId));
		String certiCode = party.getCertiCode();
		// 產生新的changeId
		Long insertChangeId;
		Change tChange = new Change();
		tChange.setChangeSource(CodeCst.CHANGE_SOURCE__PARTY);
		tChange.setChangeStatus(CodeCst.CHANGE_STATUS__WAITING_ISSUE);
		tChange.setOrgId(101L);
		insertChangeId = TChangeDelegate.create(tChange);
		
		// Flag = 1，不會發送聲明書
		if (flag.equals("1")) {
			// 取得crsPartyLogVO
			CRSPartyLogVO crsPartyLog = crsPartyLogService.copyPosLog(insertChangeId, null, Long.valueOf(crsId), null);
			crsPartyLog.setCrsAnnounceStatus("N-0");
			// save CRS_PARTY_LOG
			CRSPartyLogVO crsPartyLogAf = crsPartyLogService.save(crsPartyLog);
			// save CRS_PARTY
			partyCI.submitByLogId(crsPartyLogAf.getLogId());
		}
		// Flag = 2，發送聲明書
		else if (flag.equals("2")) {
			logWithId(crsId, "policyCode = " + party.getPolicyCode());
			Long policyId = policyService.getPolicyIdByPolicyNumber(party.getPolicyCode()); // policyCode 轉 policyId
			// 取得crsPartyLogVO
			CRSPartyLogVO crsPartyLog = crsPartyLogService.copyPosLog(insertChangeId, null, Long.valueOf(crsId), null);
			if (party.getCrsAnnounceStatus().equals("N-0")) {
				crsPartyLog.setCrsAnnounceStatus("N-1");
			} else if (party.getCrsAnnounceStatus().equals("N-1")) {
				crsPartyLog.setCrsAnnounceStatus("N-2");
			} else if (party.getCrsAnnounceStatus().equals("N-2")) {
				crsPartyLog.setCrsAnnounceStatus("N-3");
			}
			// save CRS_PARTY_LOG
			CRSPartyLogVO crsPartyLogAf = crsPartyLogService.save(crsPartyLog);
			// save CRS_PARTY
			partyCI.submitByLogId(crsPartyLogAf.getLogId());
			// 判斷身分證字號是否空白
			if (StringUtils.isNotBlank(certiCode)) {
				logWithId(crsId, "certiCode = " + certiCode);
				// 身分證字號 或 統一編號 找到 partyId
				Long partyId = getPartyIdByCertiCode(certiCode, party.getCrsCode());
				// IR434123 OIU保單要保人certiCode <> t_company_customer.register_code,改以policyId取要保人partyId
				if (partyId == null) {
					PolicyHolderVO phVO = phService.getPolicyHolder(policyId);
					partyId = phVO.getPartyId();
				}
				
				if (partyId != null) {
					// 產生CRS調查聲明書數據
					if (party.getBuildType().equals(CRSCodeCst.BUILD_TYPE__COMPLETE)) {
						FmtPos0510Event event = new FmtPos0510Event(ApplicationEventVO.newInstance(policyId));
						event.setDeclarant(party.getName());
						event.setPrintDate(processDate);
						event.setPartyId(partyId);
						eventService.publishEvent(event);
					}
				}
			} else {
				logWithId(crsId, "警告，certiCode為空值");
			}

		}
		logWithId(crsId, "end");
	}

	/**
	 * 用policyId和certiCodet查找partyId
	 */
	@Override
	public Long getPartyIdByCertiCode(String certiCode, String crsCode) throws Exception {
		ArrayList<String> list = new ArrayList<String>();
		Long result = null;
		StringBuilder sql = new StringBuilder();
		// 自然人(個人戶)
		if ("1".equals(crsCode)) {
			sql.append(" select customer_id ");
			sql.append(" from t_customer ");
			sql.append(" where certi_code = :certiCode ");
		}
		// 法人(公司戶)
		else if ("2".equals(crsCode)) {
			sql.append(" select company_id ");
			sql.append(" from t_company_customer ");
			sql.append(" where register_code = :certiCode ");
		}

		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("certiCode", certiCode);
		list = (ArrayList<String>) namedParameterJdbcTemplate.queryForList(sql.toString(), params, String.class);
		if (list != null && list.size() != 0) {
			result = Long.valueOf(list.get(0));
		}
		return result;
	}

}
