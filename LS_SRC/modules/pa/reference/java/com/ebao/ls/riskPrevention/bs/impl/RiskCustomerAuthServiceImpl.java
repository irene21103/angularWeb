package com.ebao.ls.riskPrevention.bs.impl;

import com.ebao.ls.pub.auth.service.CommonAuthService;

public class RiskCustomerAuthServiceImpl extends CommonAuthService{
	
	public static final String BEAN_DEFAULT="riskCustomerAuthService";
	
	private static Integer LIMITATION_SUB_TYPE=49;
	
	@Override
	protected Integer getLimitationType() {
		return LIMITATION_SUB_TYPE;
	}

}
