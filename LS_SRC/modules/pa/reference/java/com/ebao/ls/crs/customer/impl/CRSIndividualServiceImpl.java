package com.ebao.ls.crs.customer.impl;

import com.ebao.ls.crs.customer.CRSIndividualService;
import com.ebao.ls.crs.data.bo.CRSIndividual;
import com.ebao.ls.crs.data.customer.CRSIndividualDAO;
import com.ebao.ls.crs.vo.CRSIndividualVO;
import com.ebao.ls.foundation.service.GenericServiceImpl;

public class CRSIndividualServiceImpl extends GenericServiceImpl<CRSIndividualVO, CRSIndividual, CRSIndividualDAO>
		implements CRSIndividualService {

	@Override
	protected CRSIndividualVO newEntityVO() {
		
		return new CRSIndividualVO();
	}

	@Override
	public CRSIndividualVO findByCertiCode(String certiCode) {
		CRSIndividual individual = dao.findByCertiCode(certiCode);
		if(individual == null)
			return null;
		return this.convertToVO(individual);
	}
	/**
	 *  PCR 257607 330030 e-Approval ITR-1801051_CRS共同申報及盡職審查專案- 保全變更及CRS確認申報國作業
	 */
	@Override
	public CRSIndividualVO findByCrsIdAndPolicyCode(Long  crsId, String policyCode){
		CRSIndividual individual = dao.findByCrsIdAndPolicyCode(crsId, policyCode);
		if(individual == null)
			return null;
		return this.convertToVO(individual);
	}

}
