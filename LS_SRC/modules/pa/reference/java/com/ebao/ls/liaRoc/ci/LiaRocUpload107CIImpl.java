package com.ebao.ls.liaRoc.ci;

import java.util.List;

import javax.annotation.Resource;

import com.ebao.ls.foundation.vo.GenericEntityVO;
import com.ebao.ls.liaRoc.bo.LiaRocUpload;
import com.ebao.ls.liaRoc.bo.LiaRocUpload107Detail;
import com.ebao.ls.liaRoc.bo.LiaRocUpload107DetailVO;
import com.ebao.ls.liaRoc.data.LiaRocUpload107DetailDao;
import com.ebao.ls.liaRoc.data.LiaRocUploadDao;
import com.ebao.ls.liaRoc.vo.LiaRocUploadSendVO;
import com.ebao.pub.util.BeanUtils;

public class LiaRocUpload107CIImpl implements LiaRocUpload107CI{

    @Resource(name = LiaRocUploadDao.BEAN_DEFAULT)
    private LiaRocUploadDao liaRocUploadDao;

    @Resource(name = LiaRocUpload107DetailDao.BEAN_DEFAULT)
    private LiaRocUpload107DetailDao liaRocUpload107DetailDao;

    @Override
    public void save107RocUpload(LiaRocUploadSendVO liaRocUploadSendVO) {
        LiaRocUpload liaRocUploadBO = new LiaRocUpload();
        BeanUtils.copyProperties(liaRocUploadBO, liaRocUploadSendVO);;

        // 儲存主檔資料
        liaRocUploadDao.save(liaRocUploadBO);

        // 儲存明細資料
        for (GenericEntityVO detailVO : liaRocUploadSendVO.getDataList()) {

            LiaRocUpload107Detail detailBO = new LiaRocUpload107Detail();
            BeanUtils.copyProperties(detailBO, detailVO);
            detailBO.setUploadListId(liaRocUploadBO.getListId());

            liaRocUpload107DetailDao.save(detailBO);
        }
    }
    

    @Override
    public List<LiaRocUpload107DetailVO> findNotify107Detail() {
        return liaRocUpload107DetailDao.findNotify107Detail();
    }

}
