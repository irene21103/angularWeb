package com.ebao.ls.crs.batch.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.derby.tools.sysinfo;
import org.apache.log4j.Logger;

import com.ebao.ls.chl.service.AgentService;
import com.ebao.ls.chl.service.ChannelOrgService;
import com.ebao.ls.chl.vo.ChannelOrgVO;
import com.ebao.ls.chl.vo.ScAgentDataVO;
import com.ebao.ls.chl.vo.ScServiceAgentDataVO;
import com.ebao.ls.chl.vo.ScServiceRepresentDataVO;
import com.ebao.ls.crs.batch.service.CrsBenefitSurveyListService;
import com.ebao.ls.crs.batch.vo.CRSTempVO;
import com.ebao.ls.crs.ci.CRSPartyCI;
import com.ebao.ls.crs.ci.CRSPartyReportVO;
import com.ebao.ls.crs.constant.CRSCodeCst;
import com.ebao.ls.crs.constant.CRSCodeType;
import com.ebao.ls.crs.data.bo.CRSPartyLog;
import com.ebao.ls.crs.data.identity.CRSPartyLogDAO;
import com.ebao.ls.crs.identity.CRSPartyLogService;
import com.ebao.ls.crs.vo.CRSPartyLogVO;
import com.ebao.ls.fatca.batch.data.FatcaBenefitSurveyListDAO;
import com.ebao.ls.fatca.constant.FatcaSurveyType;
import com.ebao.ls.fatca.data.bo.FatcaDueDiligenceList;
import com.ebao.ls.fatca.util.FatcaDBeanQuery;
import com.ebao.ls.fatca.util.FatcaJDBC;
import com.ebao.ls.fatca.vo.FatcaDueDiligenceListVO;
import com.ebao.ls.fatca.vo.FatcaReportVO;
import com.ebao.ls.notification.event.cs.FmtPos0510Event;
import com.ebao.ls.pa.pty.service.CertiCodeCI;
import com.ebao.ls.pty.ds.CustomerService;
import com.ebao.ls.pty.vo.CompanyCustomerVO;
import com.ebao.ls.pty.vo.CustomerVO;
import com.ebao.ls.pub.CodeCst;
import com.ebao.ls.pub.cst.YesNo;
import com.ebao.ls.pub.util.ClobDao;
import com.ebao.ls.pub.util.data.bo.Clob;
import com.ebao.ls.sc.vo.AgentChlVO;
import com.ebao.ls.ws.util.JaxbTool;
import com.ebao.pub.batch.type.LogLevel;
import com.ebao.pub.batch.util.BatchLogUtils;
import com.ebao.pub.event.ApplicationEventVO;
import com.ebao.pub.event.EventService;
import com.ebao.pub.framework.AppContext;

public class CrsBenefitSurveyListServiceImpl implements CrsBenefitSurveyListService {

    @Resource(name = FatcaBenefitSurveyListDAO.BEAN_DEFAULT)
    private FatcaBenefitSurveyListDAO fatcaBenefitSurveyListDAO;
	
	@Resource(name = CRSPartyLogService.BEAN_DEFAULT)
	private CRSPartyLogService crsPartyLogService;
	
	@Resource(name = CRSPartyCI.BEAN_DEFAULT)
	private CRSPartyCI crsPartyCI;
    
    @Resource(name=AgentService.BEAN_DEFAULT)
    private AgentService agentService;
    
    @Resource(name=ChannelOrgService.BEAN_DEFAULT)
    private ChannelOrgService channelServ;
    
    @Resource(name=CustomerService.BEAN_DEFAULT)
    private  CustomerService customerService;
    
    @Resource(name = CRSPartyLogDAO.BEAN_DEFAULT)
	private CRSPartyLogDAO crsPartyLogDAO;
    
    @Resource(name = EventService.BEAN_DEFAULT)
    private EventService eventService;
    
    private static Logger logger = Logger.getLogger(CrsBenefitSurveyListServiceImpl.class);
    
    // 孤兒保單派發至 PCD = HO
    private final Long DELIVER_TO_HO = 4L;
    
    // 虛擬業務員=孤兒保單
    private final Long ORPHAN = 2L;
    
    /**
	 * SQL: 查詢受益人變更的新受益人
	 */
	private static final String SQL_QUERY_RENEW_BENEFICIARY = 
			"select distinct " + 
			"	bene.change_id, " + 
			"	cm.policy_id survey_id, " + 
			"	cm.policy_code, " + 
			"	bene.policy_chg_id, " + 
			"	bene.bene_type, " + 
			"	party.party_id target_id, " + 
			"	bene.name target_name, " + 
			"	trim(bene.certi_code) target_certi_code, " + 
			"	party.party_type target_party_type, " + 
			"	bene.validate_time, " + 
			"	cm.liability_state policy_status, " + 
			"	agt.agent_id as service_agent, " + 
			"	agt.register_code as agent_register_code, " + 
			"	agt.agent_name, " + 
			"	agt.business_cate as agent_biz_cate, " + 
			"	org.channel_id as channel_org, " + 
			"	org.channel_code, " + 
			"	org.channel_name, " + 
			"	org.channel_type, " + 
			"	bene.finish_time, " + 
			"	'03' role_type " + 
			"from ( " + 
			"	select " + 
			// IR299075 受益人變更的新受益人，新增(oper_type = 1)或更新(oper_type = 2)都須納入，但更新須比對是否為更換受益人
			"		cx.oper_type, " +
			"		cx.pre_log_id, " +
			"		cmBene.policy_id, " + 
			"		cmbene.item_id, " + 
			"		cmbene.party_id, " + 
			"		cmbene.name, " + 
			"		cmbene.certi_code, " + 
			"		cmbene.certi_type, " + 
			"		chg.change_id, " + 
			"		pch.policy_chg_id, " + 
			"		chg.finish_time, " + 
			"		pch.validate_time, " + 
			"		cmbene.bene_type, " + 
			"		rank() over(partition by nvl( trim(cmbene.certi_code), 'X'), cmbene.name order by pch.validate_time desc, chg.change_id desc, cmbene.list_id desc) seq " + 
			"	from t_change chg " + 
			"	join t_policy_change pch " + 
			"		on chg.change_id = pch.master_chg_id " + 
			"		and pch.service_id = 103 " + 
			"		and trunc(chg.finish_time) = trunc(?) " + 
			//PCR257607受益人變更的新受益人排除送件方式為內部傳送
			"	join t_cs_application tca " + 
			"		on chg.change_id = tca.change_id " + 
			"		and tca.send_type <> 'I' " + 
			"	join t_contract_bene_log cmbene " + 
			"		on cmbene.policy_id = pch.policy_id " + 
			"		and cmbene.change_id = pch.master_chg_id " + 
			"		and cmbene.last_cmt_flg = 'Y' " + 
			"		and cmbene.bene_type not in (2,13,14) " +
			//IR316960產生受益人調查名單時請排除受益人與被保險人關係為法定繼承人
			"		and cmbene.party_id > 0 " +
			"	join t_contract_bene_cx cx " + 
			"		on cx.list_id = cmbene.list_id " + 
			"		and cx.policy_chg_id = pch.policy_chg_id " + 
			// IR299075 受益人變更的新受益人，新增(oper_type = 1)或更新(oper_type = 2)都須納入
			"		and cx.oper_type in (1, 2) " + 
			//"		and cx.pre_log_id is null " + 
			"	) bene " +
			// IR-332461 去除主約限制，使其與FATCA修改過的撈件條件一致
			"	join t_contract_product_log cmprod " + 
			"		on cmprod.item_id = bene.item_id " +
			"       and cmprod.last_cmt_flg = 'Y' " +
			"		and seq=1 " + 
			"	join t_contract_master cm " + 
			"		on cm.policy_id = cmprod.policy_id " + 
			"	join t_product_life prod " + 
			"		on prod.product_id = cmprod.product_id " + 
			"	join t_party party " + 
			"		on party.party_id = bene.party_id " + 
			"	join t_agent agt " + 
			"		on cm.service_agent = agt.agent_id " + 
			"	join t_channel_org org " + 
			"		on agt.channel_org_id = org.channel_id " + 
			"	left join t_customer cust " + 
			"		on cust.customer_id = bene.party_id " + 
			"	left join t_company_customer comp " + 
			"		on comp.company_id = bene.party_id " + 
			"where 1 = 1 " + 
			"and exists " + 
			"	(select 1 " + 
			"	from t_liability_config cfg " + 
			"	where cfg.product_id = cmprod.product_id " + 
			"	and cfg.product_version_id = cmprod.product_version_id " + 
			"	and cfg.fatca_ind = 'Y' " + 
			"	and bene.bene_type not in (2,13,14) " + 
			"	) " + 
			"and (exists " + 
			"		(select * " + 
			"		from t_product_category prodcate " + 
			"		where prodcate.product_id = cmprod.product_id " + 
			"		and prodcate.category_id in (40031, 40154) " + 
			"		) " + 
			"	or exists " + 
			"		(select * " + 
			"		from t_pay_plan payplan " + 
			"		where payplan.policy_id = cmprod.policy_id " + 
			"		and payplan.item_id = cmprod.item_id " + 
			"		and payplan.pay_plan_type in ('2', '6') " + 
			"		) " + 
			"	) " + 
			// IR299075 當收益人紀錄更新時，只有受益人被更換的狀況才納入CRS清單，以證號是否改變作為判斷基準
			"and not exists " + 
			"	(select 1 " + 
			"	from T_CONTRACT_BENE_LOG pre_cbl" + 
			"	where 1=1 " + 
			"	and bene.oper_type = 2 " + 
			"	and pre_cbl.log_id = bene.pre_log_id " + 
			"	and pre_cbl.CERTI_CODE = bene.certi_code " +
			"	) " +
			// 該對象在Fatca盡職調查名單中沒有CRS相關的建檔紀錄(SURVEY_SYSTEM_TYPE = CRS 或 CRS/FATCA)
			// PCR257607 mark for 受益人CRS未回覆或批次判定需重新調查
			// "and not exists " + 
			// "	(select 1 " + 
			// "	from T_FATCA_DUE_DILIGENCE_LIST fddl " + 
			// "	where 1=1 " + 
			// "	and fddl.target_certi_code = bene.certi_code " +
			// "	and fddl.target_survey_type = '04' " +
			// "	and fddl.SURVEY_SYSTEM_TYPE in ('CRS', 'CRS/FATCA')) " +
			// 該對象未有CRS完成建檔記錄,定義:該對象在T_CRS_PARTY不存在聲明書簽署狀態=已簽署(02)的紀錄
			// PCR257607 or 聲明書簽署狀態=已通知未簽署且為POS發送 (已註記)
			// PCR396290 or 聲明書簽署狀態=CRS240
			"and not exists " + 
			"	(select 1 " + 
			"	from T_CRS_PARTY tcp " + 
			"	where 1=1 " + 
			"	and ((bene.certi_code is not null and tcp.CERTI_CODE = bene.certi_code) or " + 
			"	     (bene.certi_code is null and tcp.policy_code = cm.policy_code and tcp.name=bene.name)) " + 
			"	and (tcp.DOC_SIGN_STATUS = '02' or " + 
			"        tcp.CRS_TYPE = 'CRS240' or " +
			"	     (tcp.DOC_SIGN_STATUS = '01' and tcp.sys_code='POS' and tcp.build_type in ('01','06'))) " + 
			"	) ";
	
	/**
	 * SQL: 查詢即期年金受益人(被保險人)
	 */
	private static final String SQL_QUERY_IMMEDIATE_ANNUITY_BENEFICIARY = 
			"select distinct " + 
			"	bene.change_id, " + 
			"	bene.POLICY_ID survey_id, " + 
			"	bene.policy_code, " + 
			"	bene.liability_state policy_status, " + 
			"	bene.PARTY_ID target_id, " + 
			"	bene.name target_name, " + 
			"	trim(bene.certi_code) target_certi_code, " + 
			"	party.party_type target_party_type, " + 
			"	AGT.AGENT_ID  service_agent, " + 
			"	AGT.REGISTER_CODE agent_register_code, " + 
			"	AGT.agent_name, " + 
			"	AGT.BUSINESS_CATE  agent_biz_cate, " + 
			"	ORG.CHANNEL_ID  channel_org, " + 
			"	ORG.channel_code, " + 
			"	ORG.channel_name, " + 
			"	ORG.channel_type, " + 
			"	'02' role_type " + 
			"from " + 
			"	(select " + 
			"		tpc.MASTER_CHG_ID as change_id, " + 
			"		cm.policy_id, " + 
			"		cm.policy_code, " + 
			"		cm.service_agent, " + 
			"		cm.liability_state, " + 
			"		cmBene.item_id, " + 
			"		cmbene.name, " + 
			"		cmbene.party_id, " + 
			"		cmbene.certi_code, " + 
			"		cm.validate_date " +  
			"	from t_contract_master cm " + 
			"	join t_contract_product cmPrd " + 
			"		on cmPrd.Policy_Id = cm.policy_id " + 
			"		and cm.validate_date = ? " + 
			"		and cmPrd.master_id is null " + 
			"	join t_product_category prdCate " + 
			"		on prdCate.category_id = 40244 " + 
			"		and prdCate.product_id = cmPrd.Product_Id " + 
			"	join t_contract_bene cmBene " + 
			"		on cmBene.policy_id = cmPrd.Policy_Id " + 
			"		and cmBene.certi_code is not null " + 
			"		and cmBene.certi_type is not null " + 
			"		and cmBene.item_id = cmPrd.Item_Id " + 
			"		and cmBene.Designation = 0 " + 
			//補IR316960產生受益人調查名單時請排除受益人與被保險人關係為法定繼承人
			"		and cmbene.party_id > 0 " +
			"	join T_POLICY_CHANGE tpc " + 
			"		on tpc.policy_id = cm.policy_id " + 
			"		and trunc(tpc.validate_time) = trunc(cm.validate_date) " + 
			"		and tpc.SERVICE_ID = 32 " + 
			"	) bene " + 
			"	join t_party party " + 
			"		on party.party_id = bene.party_id " + 
			"	join t_agent agt " + 
			"		on bene.service_agent = agt.agent_id " + 
			"	join t_channel_org org " + 
			"		on agt.channel_org_id = org.channel_id " + 
			"	left join t_customer cust " + 
			"		on cust.customer_id = bene.Party_Id " + 
			"	left join t_company_customer comp " + 
			"		on comp.company_id = bene.Party_Id " + 
			"where 1 = 1 " + 
			// 該對象在Fatca盡職調查名單中沒有CRS相關的建檔紀錄(SURVEY_SYSTEM_TYPE = CRS 或 CRS/FATCA)
			// PCR257607 mark for 受益人CRS未回覆或批次判定需重新調查
			// "and not exists " + 
			// "	(select 1 " + 
			// "	from T_FATCA_DUE_DILIGENCE_LIST fddl " + 
			// "	where 1=1 " + 
			// "	and fddl.target_certi_code = bene.certi_code " +
			// "	and fddl.target_survey_type = '05' " +
			// "	and fddl.SURVEY_SYSTEM_TYPE in ('CRS', 'CRS/FATCA')) " +
			// 該對象未有CRS完成建檔記錄,定義:該對象在T_CRS_PARTY不存在聲明書簽署狀態=已簽署(02)的紀錄
			// PCR257607 or 聲明書簽署狀態=已通知未簽署且為POS發送 (已註記)
			// PCR-415240 改Call PKG判斷受益人是否需調查
			" and pkg_ls_crs_foreign_indi.f_chk_bene_survey(policy_code, bene.certi_code) = 'Y' ";
	
	/**
	 * SQL: 查詢保險金受益人
	 */
	private static final String SQL_QUERY_BENIFIT_OWNER = 
			"select distinct " + 
			"	bene.change_id, " + 
			"	bene.policy_id survey_id, " + 
			"	bene.policy_code, " + 
			"	bene.liability_state policy_status, " + 
			"	bene.party_id target_id, " + 
			"	bene.name target_name, " + 
			"	trim(bene.certi_code) target_certi_code, " + 
			"	party.party_type target_party_type, " + 
			"	agt.agent_id service_agent, " + 
			"	agt.register_code agent_register_code, " + 
			"	agt.agent_name, " + 
			"	agt.business_cate agent_biz_cate, " + 
			"	org.channel_id channel_org, " + 
			"	org.channel_code, " + 
			"	org.channel_name, " + 
			"	decode(agt.business_cate, 2, 4, org.channel_type) channel_type, " + 
			"	'03' role_type " + 
			"from " + 
			"	(select " + 
			"		fmt.change_id, " + 
			"		cm.policy_id, " + 
			"		cm.policy_code, " + 
			"		cm.liability_state, " + 
			"		cm.service_agent, " + 
			"		cmbene.party_id, " + 
			"		cmbene.certi_code, " + 
			"		cmbene.certi_type, " + 
			"		cmbene.name, " + 
			"		cmprd.item_id, " + 
			"		cmprd.product_id, " + 
			"		cmprd.product_version_id, " + 
			"		cm.validate_date, " + 
			"		pp.update_timestamp " + 
			"	from " + 
			"		(select " + 
			"			p150.change_id, " + 
			"			p150.planid, " + 
			"			p150.itemId " + 
			// 年金到期通知書 含年金給付變更通知書 
			"		from v_fmt_pos_0150_batch p150 " + 
			"		where 1 = 1 " + 
			"			and p150.beginDate is not null " + 
			"			and p150.beginDate > " +
			" ( select process_date + 70 " +   // 最後一次執行CRS受益人調查名單PROCESS_DATE  + 70天 
			"	from ( select bjr.process_date " + 
			"		   from ( select process_date " +
			"		          from t_batch_job_run  " +
			"		          where job_id = 1336083  " + // JOBID:1336083 (CRS受益人調查名單)
			"		          order by process_date desc ) bjr " +
			"		   where ROWNUM <= 2 " +
			"		   order by bjr.process_date ) " +          
			"	where ROWNUM = 1 ) " +
			"           and p150.beginDate <=  ? + 70" +
			"		) fmt " + 
			"		join t_pay_plan pp " + 
			"			on fmt.planid = pp.plan_id " + 
			"		join t_contract_product cmPrd " + 
			"			on pp.policy_id = cmPrd.policy_id " + 
			"			and pp.item_id = cmPrd.item_id " + 
			"		join ( " + 
			"			select " + 
			"				cmBene.policy_id, " + 
			"				cmBene.item_id, " + 
			"				cmBene.party_id, " + 
			"				cmBene.certi_code, " + 
			"				cmBene.certi_type, " + 
			"				cmBene.name, " + 
			"				cmBene.share_order, " + 
			"				decode(cmbene.bene_type, 4, 2, 1, 3, 3, 4, 5, 10, 6, 9) bene_type " + 
			"			from t_contract_bene cmBene " + 
			"			where cmBene.Bene_Type in (4, 1, 3, 5, 6) " + 
			"			) cmBene  " + 
			"            on cmBene.item_id = cmprd.item_id " + 
			"			and cmBene.Bene_Type = pp.pay_plan_type " + 
			//補IR316960產生受益人調查名單時請排除受益人與被保險人關係為法定繼承人
			"		    and cmbene.party_id > 0 " +
			"		    and cmbene.share_order = 1 " +
			"		join t_contract_master cm " + 
			"			on cm.policy_id = cmPrd.policy_id " + 
			"			and cm.liability_state > 0 " + 
			"	) bene " + 
			"	join t_agent agt " + 
			"		on bene.service_agent = agt.agent_id " + 
			"	join t_channel_org org " + 
			"		on agt.channel_org_id = org.channel_id " + 
			"	join t_party party " + 
			"		on party.party_id = bene.party_id " + 
			"		and exists " + 
			"			(select 1 from t_liability_config cfg " + 
			"				where cfg.product_id = bene.Product_Id " + 
			"				and cfg.product_version_id = bene.product_version_id " + 
			"				and cfg.fatca_ind = 'Y' " + 
			"			) " + 
			"		and (exists " + 
			"				(select * from t_product_category prodCate " + 
			"					where prodCate.product_id = bene.product_id " + 
			"					and prodCate.category_id in (40031, 40154) " + 
			"				) or exists " + 
			"				(select * from t_pay_plan payPlan " + 
			"					where payPlan.policy_id = bene.policy_id " + 
			"					and payPlan.item_id = bene.item_id " + 
			"					and payPlan.pay_plan_type in ('2', '6') " + 
			"				) " + 
			"			) " + 
			"	left join t_customer cust on cust.customer_id = bene.party_id " + 
			"	left join t_company_customer comp on comp.company_id = bene.party_id  " + 
			"where 1=1 " + 
			// 該對象在Fatca盡職調查名單中沒有CRS相關的建檔紀錄(SURVEY_SYSTEM_TYPE = CRS 或 CRS/FATCA)
			// PCR257607 mark for 受益人CRS未回覆或批次判定需重新調查
			// "and not exists " + 
			// "	(select 1 " + 
			// "	from T_FATCA_DUE_DILIGENCE_LIST fddl " + 
			// "	where 1=1 " + 
			// "	and fddl.target_certi_code = bene.certi_code " + 
			// "	and fddl.target_survey_type = '06' " +
			// "	and fddl.SURVEY_SYSTEM_TYPE in ('CRS', 'CRS/FATCA')) " +
			// 該對象未有CRS 完成建檔記錄，意指未符合下述
			// 聲明書簽署狀態=已簽署" + 
			// or 聲明書簽署狀態=不願簽署 且 [稅務居住者之國家/地區]非空值 
			// or 聲明書簽署狀態=未回覆 且 [稅務居住者之國家/地區]非空值 
			// PCR257607 取消判斷聲明書簽署狀態=不願簽署 /未回覆
			// 增加聲明書簽署狀態=已通知未簽署且為POS發送 (已註記)
			// PCR-415240 改Call PKG判斷受益人是否需調查
			" and pkg_ls_crs_foreign_indi.f_chk_bene_survey(policy_code, bene.certi_code) = 'Y'"
			;
	
	/**
	 * SQL: PCR257607 查詢理賠給付受益人
	 */
	private static final String SQL_QUERY_CLM_BENEFICIARY = 
			"select distinct case_id change_id," + 
			"	clm.policy_id survey_id, " + 
			"	clm.policy_code, " + 
			"	decode(party.party_type, 1, cust.customer_id, comp.company_id) target_id, " + 
			"	decode(party.party_type, 1, cust.first_name, comp.company_name) target_name, " + 
			"	decode(party.party_type, 1, trim(cust.certi_code), trim(comp.register_code)) target_certi_code, " + 
			"	party.party_type target_party_type, " + 
			"	clm.liability_state policy_status, " + 
			"	agt.agent_id as service_agent, " + 
			"	agt.register_code as agent_register_code, " + 
			"	agt.agent_name, " + 
			"	agt.business_cate as agent_biz_cate, " + 
			"	org.channel_id as channel_org, " + 
			"	org.channel_code, " + 
			"	org.channel_name, " + 
			"	org.channel_type, " + 
			"	clm.approve_time, " + 
			"	'' role_type " + 
			"from ( " + 
			"	select " + 
			"		cm.policy_id, " +
			"		cm.policy_code, " +
			"		cm.service_agent, " + 
			"		cm.liability_state, " + 
			"		bene.party_id, " + 
			"		clmpay.item_id, " +
			"		cas.case_id, " + 
			"		cas.reporter, " + 
			"       cas.rptr_foa_code, " +
			"		cas.approve_time, " + 
			"		rank() over(partition by bene.party_id order by cm.validate_date desc, cas.case_no desc, clmpay.item_id desc) seq  " + 
			"	from t_claim_pay clmpay " + 
			"	join t_claim_pay_bene bene " + 
			"		on bene.case_id = clmpay.case_id " + 
			"		and bene.party_id = clmpay.bene_id " + 
			"		and nvl(bene.crs_indi,'N') = 'Y'  " + 
			"		and bene.party_id > 0  " + 
			"	join t_claim_case cas " + 
			"		on cas.case_id = bene.case_id " + 
			"		and cas.case_status = 10  " + 
			"		and trunc(cas.approve_time) = trunc(?)  " + 
			"	join t_contract_master cm " + 
			"		on cm.policy_id = clmpay.policy_id) clm" + 
			"	left join t_agent agt " + 
			"		on clm.reporter = agt.agent_id  " + 
			"	left join (select channel_id, channel_type, channel_code, channel_name " + 
			"		       from   t_channel_org " + 
			"	           union " + 
			"		       select channel_org_id channel_id, channel_type, " + 
			"	                  communicate_code channel_code, communicate_name channel_name" + 
			"		       from   t_sc_communicate)  org  " + 
			"	on clm.rptr_foa_code = org.channel_code  " + 
			"	join t_contract_product cmprd  " + 
			"		on cmprd.policy_id = clm.policy_id  " +
			"		and cmprd.item_id = clm.item_id  " +
			"	join t_party party  " + 
			"		on party.party_id = clm.party_id " +
			"	left join t_customer cust " + 
			"		on cust.customer_id = party.party_id  " + 
			"	left join t_company_customer comp " + 
			"		on comp.company_id = party.party_id " + 
			"where 1 = 1 " + 
			"  and seq=1 " + 
			// 該對象在Fatca盡職調查名單中沒有CRS相關的建檔紀錄(SURVEY_SYSTEM_TYPE = CRS 或 CRS/FATCA)
			"and not exists " + 
			"	(select 1 " + 
			"	from T_FATCA_DUE_DILIGENCE_LIST fddl " + 
			"	where 1=1 " + 
			"	and fddl.target_certi_code = decode(party.party_type, 1, trim(cust.certi_code), trim(comp.register_code)) " +
			"	and fddl.target_survey_type = '07' " +
			"	and fddl.SURVEY_SYSTEM_TYPE in ('CRS', 'CRS/FATCA')) " +
			// 該對象存在CRS已通知未簽署，發送單位為CLM
			"and exists " + 
			"	(select 1 " + 
			"	from T_CRS_PARTY_LOG tcp " + 
			"	where 1=1 " + 
			"	and tcp.CERTI_CODE = decode(party.party_type, 1, trim(cust.certi_code), trim(comp.register_code)) " + 
			"	and tcp.DOC_SIGN_STATUS = '01' " + 
			"	and tcp.sys_code='CLM' " + 
			"	) " +
			// 該對象未有CRS完成建檔記錄,定義:該對象在T_CRS_PARTY不存在聲明書簽署狀態=已簽署(02)的紀錄
			"and not exists " + 
			"	(select 1 " + 
			"	from T_CRS_PARTY tcp " + 
			"	where 1=1 " + 
			"	and tcp.CERTI_CODE = decode(party.party_type, 1, trim(cust.certi_code), trim(comp.register_code)) " + 
			"	and (tcp.DOC_SIGN_STATUS = '02' or tcp.CRS_TYPE = 'CRS240') " + 
			"	) " ;
			 

    private void log(String message) {
    	logger.info("[SYSOUT][CrsBenefitSurveyListServiceImpl]" + message);
    	BatchLogUtils.addLog(LogLevel.INFO, null, message);
    }
    
    private void logWithId(String id, String message) {
    	if (StringUtils.isNotBlank(id)) {
    		message = "[" + id + "] " + message;
    	}
    	log(message);
    }
    
    /**
	 * 查詢CRS受益人
	 * @param processDate 業務日
	 * @param surveyType 調查類型
	 * @return
	 * @throws Exception
	 */
    @Override
	public List<CRSTempVO> queryBeneficiaryList(Date processDate, String surveyType) throws Exception {
		List<CRSTempVO> voList = new ArrayList<CRSTempVO>();
		
		StringBuffer sqlbuffer = new StringBuffer();
		if (StringUtils.equals(surveyType, FatcaSurveyType.RENEW_BENEFICIARY)) {
			sqlbuffer.append(SQL_QUERY_RENEW_BENEFICIARY);
		} else if (StringUtils.equals(surveyType, FatcaSurveyType.IMMEDIATE_ANNUITY_BENEFICIARY)) {
			sqlbuffer.append(SQL_QUERY_IMMEDIATE_ANNUITY_BENEFICIARY);
		} else if (StringUtils.equals(surveyType, FatcaSurveyType.THE_BENEFITS_OWNER)) {
			sqlbuffer.append(SQL_QUERY_BENIFIT_OWNER);
		} else if (StringUtils.equals(surveyType, FatcaSurveyType.CLAIM_PAYMENT_BENEFICIARY)) {
			sqlbuffer.append(SQL_QUERY_CLM_BENEFICIARY);
		} else {
			throw new IllegalArgumentException("Unsupport surveyType!");
		}

		voList = execute(sqlbuffer, processDate, surveyType);
        
		return voList;
	}
    
    /**
	 * TODO 將Fatca盡職調查清單轉換為CRSPartyLog清單
	 * @param fatcaVoList
	 * @param processDate
	 * @return
	 */
    @Override
    public CRSPartyLogVO getCRSPartyLogVO(CRSTempVO crsTempVO, Date processDate) throws Exception {
    	
    	CRSPartyLogVO crsPartyLogVO = new CRSPartyLogVO();
    	FatcaDueDiligenceListVO fatcaVO = crsTempVO.getFatcaDueDiligenceListVO();
		
		// CHANGE_ID
		crsPartyLogVO.setChangeId(crsTempVO.getChangeId());
		// LAST_CMT_FLG (預設值給N，不處理)
		// CERTI_CODE (符合受益人調查的受益人ID)
		crsPartyLogVO.setCertiCode(fatcaVO.getTargetCertiCode());
		// IR294389 若原資料來源t_contract_bene_log受益人無證號，嘗試以partyId另外取得證號，避免後續建檔異常
		if(StringUtils.isBlank(fatcaVO.getTargetCertiCode())) {
            if(CodeCst.PARTY_TYPE__INDIV_CUSTOMER.equals(fatcaVO.getTargetPartyType())) {
                CustomerVO customerVO = customerService.getIndivCustomer(fatcaVO.getTargetId());
                if(customerVO != null) {
                	crsPartyLogVO.setCertiCode(StringUtils.trim(customerVO.getCertiCode()));
                } 
            } else {
                CompanyCustomerVO companyVO = customerService.getOrganCustomer(fatcaVO.getTargetId());
                if(companyVO != null) {
                	crsPartyLogVO.setCertiCode(StringUtils.trim(companyVO.getRegisterCode()));
                }
            }
        }
		// CERTI_TYPE
		crsPartyLogVO.setCertiType(fatcaVO.getTargetPartyType().equals(CodeCst.PARTY_TYPE__INDIV_CUSTOMER)? 
				Integer.valueOf(CertiCodeCI.CERTI_TYPE_TW_PID) : Integer.valueOf(CertiCodeCI.CERTI_TYPE_COMPANY_NO));
		String partyType =fatcaVO.getTargetPartyType();
		crsPartyLogVO.setCrsCode(CodeCst.PARTY_TYPE__INDIV_CUSTOMER.equals(partyType)? 
				CRSCodeCst.CRS_IDENTITY_TYPE__MAN : CRSCodeCst.CRS_IDENTITY_TYPE__COMPANY);
		// NAME (符合受益人調查的受益人姓名)
		crsPartyLogVO.setName(fatcaVO.getTargetName());
		// POLICY_CODE (符合受益調查的保單號碼)
		crsPartyLogVO.setPolicyCode(fatcaVO.getPolicyCode());
		// POLICY_TYPE (保單類型(個險:0,團險:1))
		crsPartyLogVO.setPolicyType(CRSCodeCst.POLICY_TYPE__NORMAL);
		// SYS_CODE (固定給保全)
		crsPartyLogVO.setSysCode(CRSCodeCst.SYS_CODE__POS);
		// CRS_TYPE (自動建檔時CRS身份類別不給值)
		// COUNTRY
		// DECLARATION_DATE
		// LEGAL_CERTI_CODE
		// LEGAL_NAME
		// AGENT_ANNOUNCE
		// DOC_SIGN_STATUS (01 已通知未簽署)
		crsPartyLogVO.setDocSignStatus(CRSCodeCst.CRS_DOC_SIGN_STATUS__NOTIFY);
		// DOC_SIGN_DATE (調查名單產生日期 = 批次業務日)
		crsPartyLogVO.setDocSignDate(processDate);
		// BUILD_TYPE 預設建檔中 
		// PCR257607  CRS身分聲明狀態 = [CRS000] 建檔狀態 = [已註記] 
		 crsPartyLogVO.setBuildType(CRSCodeCst.BUILD_TYPE__STAMPED);
		 crsPartyLogVO.setCrsType(CRSCodeType.CODE_TYPE_UNDEFINED__E); 
		// INPUT_SOURCE (BAT)
		crsPartyLogVO.setInputSource(CRSCodeCst.INPUT_SOURCE__BAT);		
		
		return crsPartyLogVO;
	}
    
    /**
	 * CRS LOG建檔與主檔提交
	 * @param voList
	 * @param processDate
	 * @throws Exception
	 */
    @Override
	public void createCrsRecord(String id, CRSTempVO crsTempVO, Long userId, Date processDate) throws Exception {
		CRSPartyLogVO vo = getCRSPartyLogVO(crsTempVO, processDate);
		logWithId(id, "changeId = " + vo.getChangeId());
		String certiCode = vo.getCertiCode();
		if (StringUtils.isNotBlank(certiCode)) {
			logWithId(id, "certiCode = " + certiCode);
		} else {
			logWithId(id, "警告，certiCode為空值");
		}
		// 建立CRSPartyLog
		crsPartyLogService.saveOrUpdate(userId, processDate, vo, vo.getChangeId(), vo.getInputSource());
		CRSPartyLogVO logVO = crsPartyLogService.
				findByChangeIdAndCertiCode(vo.getChangeId(), certiCode);
		logWithId(id, "logId = " + logVO.getLogId());
		// 提交CRS主檔
		CRSPartyReportVO crsPartyReportVO = crsPartyCI.submitByLogId(logVO.getLogId());
		if (crsPartyReportVO != null) {
			logWithId(id, "crsId = " + crsPartyReportVO.getParty().getCrsId());
		} else {
			logWithId(id, "提交CRS主檔有異常");
		}
		
		// PCR257607 補10801及10802受益人調查產生CRS調查聲明書數據
		if(crsTempVO.getTempBatch() != null && crsTempVO.getTempBatch() == "Y"){
			FmtPos0510Event event = new FmtPos0510Event(ApplicationEventVO.newInstance(crsTempVO.getFatcaDueDiligenceListVO().getPolicyId()));
	        event.setDeclarant(crsTempVO.getFatcaDueDiligenceListVO().getTargetName());
	        event.setPrintDate(processDate);
			event.setPartyId(crsTempVO.getFatcaDueDiligenceListVO().getTargetId());
	        eventService.publishEvent(event);	
		}

	}
    
    /**
	 * 從CRSTempVO清單中取得FatcaDueDiligenceListVO清單
	 * @param crsTempVOList
	 * @return
	 */
    @Override
	public List<FatcaDueDiligenceListVO> getFatcaList(List<CRSTempVO> crsTempVOList) throws Exception {
		List<FatcaDueDiligenceListVO> fatcaList = new ArrayList<FatcaDueDiligenceListVO>();
		for (CRSTempVO crsTempVO : crsTempVOList) {
			fatcaList.add(crsTempVO.getFatcaDueDiligenceListVO());
		}
		return fatcaList;
	}

    /**
     * 派發孤兒保單
     */
    @Override
    public void deliverOrphan(FatcaDueDiligenceListVO vo){
        // 孤兒保單派發至 PCD = HO
    	if(new Long(ORPHAN).equals(vo.getAgentBizCate())){
            vo.setChannelType(DELIVER_TO_HO);
        }
    }
    
    /**
     * 更新業務員
     */
    @Override
    public void updateServiceAgent(FatcaDueDiligenceListVO vo) throws Exception {
        Long policyId = vo.getPolicyId();
        ScAgentDataVO agentDataVO = agentService.getAgentDataByPolicyId(policyId);
        List<ScServiceAgentDataVO> serviceAgentList = agentDataVO.getServiceAgentList();
        List<ScServiceRepresentDataVO> representAgentList = agentDataVO.getServiceRepresentList();
        if(CollectionUtils.isEmpty(serviceAgentList)){
            BatchLogUtils.addLog(LogLevel.WARN, null, String.format("[BR-CMN-FAT-033][同步FATCA盡職調查清單的服務員與綜合查詢一致] [查無服務員]policy_code = %s, certi_code =%s, policy_id = %d, party_id =%d" , vo.getPolicyCode(), vo.getTargetCertiCode(), vo.getPolicyId(), vo.getTargetId()));
            return;
        }
        ScServiceAgentDataVO serviceAgentVO = serviceAgentList.get(0);
        AgentChlVO agentchlVO = agentService.findAgentByAgentId(serviceAgentVO.getAgentId());
        if(agentchlVO == null)
            return;
        
        BatchLogUtils.addLog(LogLevel.WARN, null, "-1-serviceAgentList.size()="+serviceAgentList.size());
        Boolean IsAllDummy = Boolean.TRUE;
        for(ScServiceAgentDataVO svo:serviceAgentList){
        	BatchLogUtils.addLog(LogLevel.WARN, null, "0-findAgentByAgentId.getAgentId="+svo.getAgentId());
        	AgentChlVO agentChlVO =agentService.findAgentByAgentId(svo.getAgentId());
        	if(agentChlVO!=null){
        		BatchLogUtils.addLog(LogLevel.WARN, null, "agentChlVO!=nullagentChlVO.getAgentId()="+agentChlVO.getAgentId()+";agentChlVO.getIsDummy()="+agentChlVO.getIsDummy()+";");
        		if(YesNo.YES_NO__NO.equals(agentChlVO.getIsDummy())){
        			IsAllDummy = Boolean.FALSE;;
        		}
        	}
        }
        BatchLogUtils.addLog(LogLevel.WARN, null, "1-IsAllDummy="+IsAllDummy);
        
        vo.setServiceAgent(serviceAgentVO.getAgentId());
        if(agentchlVO.getBusinessCate() != null)
            vo.setAgentBizCate(Long.valueOf(agentchlVO.getBusinessCate()));
        vo.setAgentName(serviceAgentVO.getAgentName());
        vo.setAgentRegisterCode(serviceAgentVO.getAgentRegisterCode());
        
        Boolean toPCD = Boolean.FALSE;
        if(agentchlVO.getChannelOrgId() != null){
        	ChannelOrgVO channelVO = channelServ.findByChannelCode(serviceAgentVO.getAgentChannelCode());
            if(channelVO != null){
            	vo.setChannelOrg(channelVO.getChannelId());
                vo.setChannelCode(serviceAgentVO.getAgentChannelCode());
                vo.setChannelName(serviceAgentVO.getAgentChannelName());
                vo.setChannelType(serviceAgentVO.getAgentChannelType());
                //PCR-519543 部門跟人員都要虛擬且排除BRBD
                if(5l!=channelVO.getChannelType()&&6l!=channelVO.getChannelType()){
                	toPCD = YesNo.YES_NO__YES.equals(channelVO.getIsDummy()) && IsAllDummy;
                	vo.setAgentBizCate(YesNo.YES_NO__YES.equals(channelVO.getIsDummy())&& IsAllDummy ? 2L : 1L);
                }else{
                	//外部通路(BR/FID)-一律給服務單位處理，不歸入直營通路的虛擬單位PCD。
                	vo.setAgentBizCate(1L);
                }
                
            }
        }
        
        //孤兒保單服務業務員
        if(toPCD && CollectionUtils.isNotEmpty(representAgentList)) {
			ScServiceRepresentDataVO representVO = representAgentList.get(0);
			vo.setServiceAgent(representVO.getAgentId());
			vo.setAgentName(representVO.getRepresentName());
			vo.setAgentRegisterCode(representVO.getRepresentRegisterCode());
			vo.setAgentBizCate(2L);
        }
    }
    
    /**
	 * 將資料庫查詢結果轉為CRSTempVO物件
	 * @param column
	 * @param surveyType
	 * @return
	 */
	private CRSTempVO parseMapData(java.util.Map<String, Object> resultMap, String surveyType) {
		
		CRSTempVO crsTempVO = new CRSTempVO();
		
		crsTempVO.setChangeId(MapUtils.getLong(resultMap, "change_id"));
		
		Long targetId = MapUtils.getLong(resultMap, "target_id");
		Long policyId = MapUtils.getLong(resultMap, "survey_id");

        FatcaDueDiligenceListVO fatcaVO = new FatcaDueDiligenceListVO();
        
        fatcaVO.setPolicyId(policyId);
        fatcaVO.setPolicyCode(StringUtils.trim(MapUtils.getString(resultMap, "policy_code", "")));
        fatcaVO.setPolicyStatus(MapUtils.getLong(resultMap, "policy_status"));
        fatcaVO.setTargetId(targetId);
        fatcaVO.setTargetName(StringUtils.trim(MapUtils.getString(resultMap, "target_name", "")));
        fatcaVO.setTargetCertiCode(StringUtils.trim(MapUtils.getString(resultMap, "target_certi_code", "")));
        fatcaVO.setTargetPartyType(MapUtils.getString(resultMap,"target_party_type"));
        fatcaVO.setTargetSurveyType(surveyType);
        fatcaVO.setServiceAgent(MapUtils.getLong(resultMap, "service_agent"));
        fatcaVO.setAgentRegisterCode(StringUtils.trim(MapUtils.getString(resultMap, "agent_register_code", "")));
        fatcaVO.setAgentName(StringUtils.trim(MapUtils.getString(resultMap, "agent_name", "")));
        Long agentBizCate = (Long) resultMap.get("agent_biz_cate");
        if (agentBizCate != null){
        agentBizCate = agentBizCate != 0 ? agentBizCate : null;
        }
        fatcaVO.setAgentBizCate(agentBizCate);
        Long channelOrg = (Long) resultMap.get("channel_org");
        if (channelOrg != null){
        channelOrg = channelOrg != 0 ? channelOrg : null;
        }
        fatcaVO.setChannelOrg(channelOrg);
        fatcaVO.setChannelCode(StringUtils.trim(MapUtils.getString(resultMap, "channel_code", "")));
        fatcaVO.setChannelName(StringUtils.trim(MapUtils.getString(resultMap, "channel_name", "")));
        Long channelType = (Long)resultMap.get("channel_type");
        fatcaVO.setChannelType(channelType);
        fatcaVO.setRoleType(MapUtils.getString(resultMap, "role_type"));
        
        // PCR257607 紀錄理賠case_id
        org.hibernate.Session session = com.ebao.foundation.module.db.hibernate.HibernateSession3.currentSession();
        if(StringUtils.equals(surveyType, FatcaSurveyType.CLAIM_PAYMENT_BENEFICIARY)){
        	 Long caseId =  MapUtils.getLong(resultMap, "change_id");
             FatcaReportVO report = new FatcaReportVO();
             report.setCaseId(caseId);
             String xml = JaxbTool.toXml(report);
             Clob clob = new Clob();
             clob.setContent(xml);
             ClobDao.save(clob);
             session.flush();
             fatcaVO.setReportId(clob.getId());
        }

        // 查詢T_FATCA_DUE_DILIGENCE_LIST檢查目前有無紀錄
        FatcaDueDiligenceList resultFatcaBO = fatcaBenefitSurveyListDAO.searchFatcaDueDiligenceList(targetId, policyId, surveyType);
        if (resultFatcaBO == null) {
        	// 新紀錄
        	crsTempVO.setNewRecord(true);
        	// 調查系統別為CRS
        	fatcaVO.setSurveySystemType(SURVEY_SYSTEM_TYPE_CRS);
        } else {
        	// 已有記錄
        	crsTempVO.setNewRecord(false);
        	fatcaVO.setListID(resultFatcaBO.getListID());
        	// 調查系統別為FATCA，表示是由FATCA批次建立資料，需更新SURVEY_SYSTEM_TYPE為CRS/FATCA，否則保持目前狀態(CRS or CRS/FATCA)
        	String surveySystemType = resultFatcaBO.getSurveySystemType();
        	if (StringUtils.equals(surveySystemType, SURVEY_SYSTEM_TYPE_FATCA)) {
        		fatcaVO.setSurveySystemType(SURVEY_SYSTEM_TYPE_ALL);
        	} else {
        		fatcaVO.setSurveySystemType(surveySystemType);
        	}
        }
        crsTempVO.setFatcaDueDiligenceListVO(fatcaVO);
        
        return crsTempVO;
    }
    
	/**
	 * 執行查詢，返回查詢結果清單
	 * @param sql
	 * @param processDate
	 * @param surveyType
	 * @return
	 * @throws Exception 
	 */
    private List<CRSTempVO> execute(final StringBuffer sql, final Date processDate, final String surveyType) throws Exception{
		
		List<CRSTempVO> voList = new java.util.LinkedList<CRSTempVO>();
        List<Map<String, Object>> resultMapList = FatcaJDBC.queryByMap(new FatcaDBeanQuery() {

            @Override
            public String sqlStatement() {
            	String sqlString = sql.toString();
            	log("SQL = " + sqlString);
            	return sqlString;
            }

            @Override
            public void createColumn() {
            	putColumn("change_id", Long.class);
            	
                putColumn("survey_id", Long.class);
                putColumn("policy_code", String.class);
                putColumn("policy_status", Long.class);
                putColumn("target_id", Long.class);
                
                putColumn("target_name", String.class);
                putColumn("target_certi_code", String.class);
                putColumn("target_party_type", Long.class);
                
                putColumn("service_agent", Long.class);
                putColumn("agent_register_code", String.class);
                putColumn("agent_name", String.class);
                putColumn("agent_biz_cate", Long.class);
                
                putColumn("channel_org", Long.class);
                putColumn("channel_code", String.class);
                putColumn("channel_name", String.class);
                putColumn("channel_type", Long.class);
                putColumn("role_type", String.class);
            }

            @Override
            public void createCriteria() {
                putCriteria(processDate, Date.class);
            }
        });

        //擷取查詢結果
        for (Map<String, Object> resultMap : resultMapList) {
        	CRSTempVO data = parseMapData(resultMap, surveyType);
            data.getFatcaDueDiligenceListVO().setTargetSurveyDate(processDate);
            
            // IR305650 當certi_code為空時，以[changeId+保單號碼+受益人姓名]為條件查詢CrsPartyLog建檔紀錄，避免重複建檔
            if (StringUtils.isBlank(data.getFatcaDueDiligenceListVO().getTargetCertiCode())) {
            	Long changeId = data.getChangeId();
            	String policyCode = data.getFatcaDueDiligenceListVO().getPolicyCode();
            	String targetName = data.getFatcaDueDiligenceListVO().getTargetName();
            	log("證號為空...changeId = " + changeId + ", policyCode = " + policyCode + ", targetName = " + targetName);
            	if (isCrsPartyLogExist(changeId, policyCode, targetName)) {
        			log("CrsPartyLog已有建檔紀錄，略過此筆");
        			continue;
        		}
            }
            voList.add(data);
        } 
        return voList;
    }
    
    /**
     * 是否在CRS_PARTY_LOG已經有紀錄
     */
    @Override
    public boolean isCrsPartyLogExist(Long changeId, String policyCode, String targetName)  throws Exception {
    	List<CRSPartyLog> crsPartyLogList = crsPartyLogDAO.findListByCertiCode(null, changeId);
    	for (CRSPartyLog crsPartyLog : crsPartyLogList) {
    		if (StringUtils.equals(policyCode, crsPartyLog.getPolicyCode())
    				&& StringUtils.equals(targetName, crsPartyLog.getName())) {
    			return true;
    		}
    	}
    	return false;
    }
}
