package com.ebao.ls.callout.batch;


import java.math.BigDecimal;
import java.math.MathContext;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.transaction.SystemException;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.json.JSONObject;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.ebao.foundation.commons.exceptions.ExceptionUtil;
import com.ebao.foundation.core.logging.Log;
import com.ebao.foundation.module.para.Para;
import com.ebao.ls.callout.batch.task.UNB0802NoneInvestSNTask;
import com.ebao.ls.callout.batch.task.UNB08061TraditionTask;
import com.ebao.ls.callout.batch.task.UNB08062InvestTask;
import com.ebao.ls.callout.bs.CalloutBatchService;
import com.ebao.ls.callout.bs.CalloutConfigService;
import com.ebao.ls.callout.bs.CalloutTransService;
import com.ebao.ls.callout.data.CalloutConfigDao;
import com.ebao.ls.callout.data.bo.CalloutBatch;
import com.ebao.ls.callout.data.bo.CalloutConfig;
import com.ebao.ls.callout.vo.CalloutBatchVO;
import com.ebao.ls.callout.vo.CalloutTransVO;
import com.ebao.ls.chl.service.AgentService;
import com.ebao.ls.chl.vo.ScServiceAgentDataVO;
import com.ebao.ls.cs.ci.TeleInterviewCI;
import com.ebao.ls.cs.commonflow.ds.cmnquery.PolicyQueryService;
import com.ebao.ls.cs.util.POSUtils;
import com.ebao.ls.pa.nb.bs.AcknowledgeProcessService;
import com.ebao.ls.pa.nb.vo.PolicyAckTaskVO;
import com.ebao.ls.pa.pub.TableCst;
import com.ebao.ls.pa.pub.bs.coverage.CoverageService;
import com.ebao.ls.pa.pub.ci.PolicyCI;
import com.ebao.ls.pa.pub.model.CoverageInfo;
import com.ebao.ls.pa.pub.model.PolicyInfo;
import com.ebao.ls.pa.pub.ref.api.prd.ProductService;
import com.ebao.ls.pa.pub.ref.vo.prd.ProductVO;
import com.ebao.ls.pa.pub.service.PolicyService;
import com.ebao.ls.pub.CodeCst;
import com.ebao.ls.pub.batch.impl.BatchJdbcTemplate;
import com.ebao.ls.pub.batch.impl.DataSourceWrapper;
import com.ebao.ls.pub.cst.CalloutTargetType;
import com.ebao.ls.pub.cst.YesNo;
import com.ebao.ls.qry.ds.QryBaseVO;
import com.ebao.ls.qry.ds.policy.FinancialInfoNewVO;
import com.ebao.ls.qry.ds.policy.PolicyDS;
import com.ebao.ls.uw.ci.UwPolicyCI;
import com.ebao.pub.batch.help.BatchHelp;
import com.ebao.pub.batch.help.UnpieceableBatchProgram;
import com.ebao.pub.batch.type.JobStatus;
import com.ebao.pub.batch.type.LogLevel;
import com.ebao.pub.batch.util.BatchLogUtils;
import com.ebao.pub.framework.GenericException;
import com.ebao.pub.i18n.lang.CodeTable;
import com.ebao.pub.util.DateUtils;
import com.ebao.pub.wincony.ui.CodeTableUtils;

public abstract class AbstractCalloutTask implements UnpieceableBatchProgram{
    
    private final Log log = Log.getLogger(AbstractCalloutTask.class);

    @Resource(name = CalloutConfigService.BEAN_DEFAULT)
    protected CalloutConfigService calloutConfigService;

    @Resource(name = CalloutTransService.BEAN_DEFAULT)
    protected CalloutTransService calloutTransService;

    @Resource(name = CalloutBatchService.BEAN_DEFAULT)
    protected CalloutBatchService calloutBatchService;

    @Resource(name = TeleInterviewCI.BEAN_DEFAULT)
    protected TeleInterviewCI pos;

    @Resource(name = PolicyCI.BEAN_DEFAULT)
    protected PolicyCI policyCI;

    @Resource(name = UwPolicyCI.BEAN_DEFAULT)
    protected UwPolicyCI uwpolicyservice;

    @Resource(name = PolicyService.BEAN_DEFAULT)
    protected PolicyService policyservice;

    @Resource(name = AcknowledgeProcessService.BEAN_DEFAULT)
    protected AcknowledgeProcessService acknowledgeProcessService;
    
    @Resource(name=CalloutBatchService.BEAN_DEFAULT)
    private CalloutBatchService batchServ;
    
    @Resource(name = AgentService.BEAN_DEFAULT)
    private AgentService agentService;
    
    @Resource(name=CalloutConfigDao.BEAN_DEFAULT)
    private CalloutConfigDao configDao;
    
    @Resource(name=ProductService.BEAN_DEFAULT)
    private ProductService prdServ;

    @Resource(name=PolicyQueryService.BEAN_DEFAULT)
    private PolicyQueryService policyQueryService;

    @Resource(name = CoverageService.BEAN_DEFAULT)
    private CoverageService coverageService;

    @Resource(name = PolicyDS.BEAN_DEFAULT)
    private PolicyDS qryPolicyDS;
    
    protected static final int MAX_SAMPLE_ROUND = 1000;

    protected static final int MAX_WRITE_ROW = 10000;

    protected static final JdbcTemplate jdbc = new JdbcTemplate(new DataSourceWrapper());

    protected ApplicationContext ctx;

    protected SimpleDateFormat sdfyyyyMMdd = new SimpleDateFormat("yyyy/MM/dd");

    protected String yyyMMdd = "\\d{3}/\\d{2}/\\d{2}";
    
    private static BatchJdbcTemplate t = new BatchJdbcTemplate(new DataSourceWrapper());
    
    protected static NamedParameterJdbcTemplate nt = new NamedParameterJdbcTemplate(t);
    
    public void setApplicationContext(ApplicationContext arg0) throws BeansException {
        ctx = arg0;
    }

    private void syso(Object msg){
        log.info("[BR-CMN-TFI-005]"+msg);
        BatchLogUtils.addLog(LogLevel.INFO, null, "[BR-CMN-TFI-005]" + msg);
    }
    
    private void syso(String layout, Object... msg){
        syso(String.format(layout, msg));
    }
    public abstract java.util.List<Map<String, Object>> getData(Date inputDate, String policyCode);
    
    /**
     * <p>Description : 抽取樣本資料(備註：以保單為單位)</p>
     * <p>Created By : Calvin Chen</p>
     * <p>Create Time : Nov 3, 2016</p>
     * @param entireList
     * @param type
     * @param ConfigClass
     * @return 抽樣筆數
     */
    public Integer readSample(List<Map<String, Object>> mapList, String type, Long DisplayOrder) {
        Map<String,Integer> sampleMap = new HashMap<String, Integer>();
        int sampleSize = 0;
        /* 抽樣筆數 */
        List<Map<String, Object>> getSampleRate = calloutConfigService.query(type, DisplayOrder, null);
        if (getSampleRate != null && getSampleRate.size() > 0) {
            BigDecimal sampleRate = (BigDecimal) getSampleRate.get(0).get("DATA"); // 拿取抽樣比例。
            BigDecimal sampleNumCalc = new BigDecimal(Float.valueOf(mapList.size()) * (sampleRate.floatValue() / 100f));// 計算需抽取。
            sampleNumCalc = sampleNumCalc.setScale(0, BigDecimal.ROUND_UP); // 先用無條件進位。
            sampleSize = sampleNumCalc.intValue();
        }
        /* 不滿一筆，不用抽 */
        if (sampleSize < 1) {
            return sampleSize;
        }
        
        /* 填滿值，且不能出現重覆狀態 */
        int bufferSize = 0;
        int mapSize = mapList.size();
        for (int now = 0; bufferSize < sampleSize && now < MAX_SAMPLE_ROUND; now++) {
            int rowNum = new java.util.Random().nextInt(mapSize);
            Map<String, Object> map = mapList.get(rowNum);
            String policyCode = MapUtils.getString(map, "POLICY_CODE");
            if(!sampleMap.containsKey(policyCode)){//無重覆抽取保單
                map.put("IS_CALLOUT",  YesNo.YES_NO__YES);
                sampleMap.put(policyCode, new Integer(rowNum));
                syso("抽出保單號碼 := %s, 流水號 :=%d ", policyCode, rowNum);
                bufferSize += 1;
            }
        }
        return bufferSize;
    }
    
    /**
     * 	
     * <p>Description:  儲存電訪對象為要保人</p>
     * <p>Create Time: Mar 20, 2017   </p>
     * @author tgl15
     * @param map
     * @param processDate
     * @param fileName
     * @param dept
     * @return
     */
    protected String savePolicyHolder(Map<String, Object> map, java.util.Date processDate, String fileName, String dept){
        String calloutIdentity = calloutTransService.getCalloutIdentity(processDate, dept);
//        //配合 MultiAP 將取號邏輯放入 saveVO 中
//        String calloutNum = calloutTransService.getCalloutNum(calloutIdentity);
//        syso("[savePolicyHolder] calloutIdentity :=" + calloutIdentity + "callout_num :=" + calloutNum);
        CalloutTransVO vo = new CalloutTransVO();
        Long policyId = MapUtils.getLong(map, "POLICY_ID");
//        vo.setCalloutNum(calloutNum);
        // 是否是批次資料.
        vo.setBatchIndi(YesNo.YES_NO__YES);
        // 要保人姓名.
        vo.setHolderName(MapUtils.getString(map, "PH_NAME"));
        // 要保人身分證字號.
        vo.setHolderCertiCode(MapUtils.getString(map, "PH_CERTI_CODE"));
        // 主被保險人姓名.
        vo.setMainInsuredName(MapUtils.getString(map, "I_NAME"));
        // 主被保險人身分證字號.
        vo.setMainInsuredCertiCode(MapUtils.getString(map, "I_CERTI_CODE"));
        // 電訪申請單位.
        vo.setCalloutReqDept(500L);
        // 通路別.
        // channel_type
        if (map.get("CHANNEL_TYPE_ID") != null) {
            vo.setChannelType(MapUtils.getLong(map, "CHANNEL_TYPE_ID"));
        }
        // 單位.
        Long channelOrgId = MapUtils.getLong(map, "CHANNEL_ID");// channel_org_id
        if (channelOrgId != null)
            vo.setChannelOrgId(channelOrgId);
        // 業務員.
        Long agentId = MapUtils.getLong(map, "AGENT_ID");// service_agent
        if (agentId != null)
            vo.setAgentId(agentId);
        // 核保人員.
        vo.setUnderwriteId(uwpolicyservice.getUnderwriterId(policyId));
        // 會辦次數.
        vo.setTransCount(0);
        // 最近一次電訪日期.
        vo.setLastTransDate(processDate);
        // 承保日.
        Date issueDate = (Date)map.get("ISSUE_DATE");
        vo.setIssueDate(issueDate);
        // 生效日.
        Date validateDate = (Date)map.get("VALIDATE_DATE");
        vo.setValidateDate(validateDate);
        // 保戶簽收日.
        Date replyDate =  (Date)map.get("REPLY_DATE");
        vo.setAcknowledgeDate(replyDate);
        // 實際郵寄保單日
        Date dispatchDate = (Date)map.get("DISPATCH_DATE");
        vo.setDispatchDate(dispatchDate);
        // 契約受理日.SUBMISSION_DATE
        Date submissionDate = (Date)map.get("SUBMISSION_DATE");
        vo.setRegDate(submissionDate);

        PolicyAckTaskVO ackTaskVO = acknowledgeProcessService.getNewestValidateTask(policyId);
        if (ackTaskVO != null) {
            // 要保人簽名審核結果.
            vo.setSignResult(ackTaskVO.getSignResult());
            // 法代簽名審核結果.
            vo.setLegalSignResult(ackTaskVO.getLegalSignResult());
        }
        // 原因 抽檔.
        vo.setCalloutReason(CalloutConstants.CalloutReason_2);
        
        // 電訪對象要保人.
        vo.setCalloutTargetType(CalloutConstants.CalloutTarget_1);
        /* 電訪對象 */
        vo.setCalleeName(MapUtils.getString(map, "PH_NAME"));
        Date birthDate = (Date)map.get("PH_BIRTHDAY");
        vo.setCalleeBirthday(birthDate);
        vo.setCalleeCertiCode(MapUtils.getString(map, "PH_CERTI_CODE"));
        
        // 判斷要保人是否成年
        if("POS-1".equals(MapUtils.getString(map, "DOCKET_NUM")) 
        		|| "POS-2".equals(MapUtils.getString(map, "DOCKET_NUM"))
        		|| "080-09".equals(MapUtils.getString(map, "DOCKET_NUM"))
        		|| "080-10".equals(MapUtils.getString(map, "DOCKET_NUM"))
        		){
        	//客戶申請日-要保人生日≥18足歲→顯示「要保人」
            //客戶申請日-要保人生日<18足歲→顯示「要保人之法定代理人」  
    		Date custApplyDate =(Date)map.get("CUST_APP_TIME"); 			//客戶申請日
    		
    		// PCR492019 民法成年年齡由20歲降為18歲
            Map<String, Object> ageMap = POSUtils.getGrowUpAge(birthDate, custApplyDate);
    		Boolean isGrowUp = MapUtils.getBoolean(ageMap, "isGrowUp");//(true: 成年, false: 未成年)
    		
    		// 未成年電訪法定代理人
            if (!isGrowUp){
            	// 電訪對象要保人之法定代理人
                vo.setCalloutTargetType(CalloutConstants.CalloutTarget_3);
                vo.setCalleeName(MapUtils.getString(map, "LR_NAME", ""));
                Date lrBirthday = (Date)map.get("LR_BIRTHDAY");
                vo.setCalleeBirthday(lrBirthday);
                vo.setCalleeCertiCode(MapUtils.getString(map, "LR_CERTI_CODE", ""));
            }
        }
        
        // 聯絡電話.
        vo.setContactNum1(org.apache.commons.collections.MapUtils.getString(map, "PH_PHONE1"));
        vo.setContactNum2(org.apache.commons.collections.MapUtils.getString(map, "PH_PHONE2"));
        
        // 電訪結果:未電訪.
        vo.setCalloutResult(CalloutConstants.CalloutResult_type0);
        // 產生電訪日期.
        vo.setExpectedCalloutDate(processDate);
        
        String filename = String.format(fileName, DateUtils.date2String(processDate, "yyyyMMdd"));
        vo.setCalloutFileName(filename);
        /* 問卷編號 */
        vo.setCalloutDocketNum(MapUtils.getString(map, "DOCKET_NUM"));
        vo.setPolicyId(policyId);
        vo.setChangeId(MapUtils.getLong(map, "CHANGE_ID"));
        CalloutTransVO newVO = calloutTransService.saveVO(vo, calloutIdentity);
        org.springframework.util.Assert.notNull(newVO.getCalloutNum());
        map.put("CALLOUT_NUM", newVO.getCalloutNum());
        map.put("CALLOUT_NUM1", newVO.getCalloutNum());
        syso("[儲存電訪對象=要保人] callout_num = %s, callee_name = %s, callee_certi_code = %s ", newVO.getCalloutNum(), newVO.getCalleeName(), newVO.getCalleeCertiCode());
        return newVO.getCalloutNum();
    }
    
    protected String saveInsured(Map<String, Object> map, java.util.Date processDate, String fileName, String dept){
        String calloutIdentity = calloutTransService.getCalloutIdentity(processDate, dept);
//      //配合 MultiAP 將取號邏輯放入 saveVO 中
//        String calloutNum = calloutTransService.getCalloutNum(calloutIdentity); // <-- 統一接口
//        syso("[saveInsured] calloutIdentity :=" + calloutIdentity + "callout_num :=" + calloutNum);
        CalloutTransVO vo = new CalloutTransVO();
        Long policyId = org.apache.commons.collections.MapUtils.getLong(map, "POLICY_ID");
//        vo.setCalloutNum(calloutNum);
        // 是否是批次資料.
        vo.setBatchIndi(YesNo.YES_NO__YES);
        // 要保人姓名.
        vo.setHolderName(MapUtils.getString(map, "PH_NAME"));
        // 要保人身分證字號.
        vo.setHolderCertiCode(MapUtils.getString(map, "PH_CERTI_CODE"));
        // 主被保險人姓名.
        vo.setMainInsuredName(MapUtils.getString(map, "I_NAME"));
        // 主被保險人身分證字號.
        vo.setMainInsuredCertiCode(MapUtils.getString(map, "I_CERTI_CODE"));
        // 電訪申請單位.
        vo.setCalloutReqDept(500L);
        // 通路別.
        // channel_type
        if (map.get("CHANNEL_TYPE") != null) {
            vo.setChannelType(MapUtils.getLong(map, "CHANNEL_TYPE"));
        }
        // 單位.
        Long channelOrgId = MapUtils.getLong(map, "CHANNEL_ID");// channel_org_id
        if (channelOrgId != null)
            vo.setChannelOrgId(channelOrgId);
        // 業務員.
        Long agentId = MapUtils.getLong(map, "AGENT_ID");// service_agent
        if (agentId != null)
            vo.setAgentId(agentId);
        // 核保人員.
        vo.setUnderwriteId(uwpolicyservice.getUnderwriterId(policyId));
        // 會辦次數.
        vo.setTransCount(0);
        // 最近一次電訪日期.
        vo.setLastTransDate(processDate);
        // 承保日
        java.util.Date issueDate = (Date)map.get("ISSUE_DATE");
        vo.setIssueDate(issueDate);
        // 生效日.
        java.util.Date validateDate = (Date)map.get("VALIDATE_DATE");
        vo.setValidateDate(validateDate);
        // 保戶簽收日.
        Date replyDate = (Date)map.get("REPLY_DATE");
        vo.setAcknowledgeDate(replyDate);
        // 實際郵寄保單日
        Date dispatchDate = (Date)map.get("DISPATCH_DATE");
        vo.setDispatchDate(dispatchDate);
        // 契約受理日SUBMISSION_DATE
        Date submissionDate = (Date)map.get("SUBMISSION_DATE");
        vo.setRegDate(submissionDate);

        PolicyAckTaskVO ackTaskVO = acknowledgeProcessService.getNewestValidateTask(policyId);
        if (ackTaskVO != null) {
            // 要保人簽名審核結果.
            vo.setSignResult(ackTaskVO.getSignResult());
            // 法代簽名審核結果.
            vo.setLegalSignResult(ackTaskVO.getLegalSignResult());
        }
        // 原因 抽檔.
        vo.setCalloutReason(CalloutConstants.CalloutReason_2);
        // 電訪對象被保人.
        vo.setCalloutTargetType(CalloutConstants.CalloutTarget_2);
        /* 電訪對象 = 被保人 */
        vo.setCalleeName(MapUtils.getString(map, "I_NAME"));
        Date calleeBirthDate = (Date)map.get("I_BIRTH_DATE"); 
        vo.setCalleeBirthday(calleeBirthDate);
        vo.setCalleeCertiCode(MapUtils.getString(map, "I_CERTI_CODE"));
        
        // 判斷被保人是否成年
        if("POS-3".equals(MapUtils.getString(map, "DOCKET_NUM"))){
        	//客戶申請日-被保人生日≥18足歲→顯示「被保人」
            //客戶申請日-被保人生日<18足歲→顯示「被保人之法定代理人」  
    		Date custApplyDate =(Date)map.get("CUST_APP_TIME"); 			//客戶申請日
    		
    		// PCR492019 民法成年年齡由20歲降為18歲
            Map<String, Object> ageMap = POSUtils.getGrowUpAge(calleeBirthDate, custApplyDate);
			Boolean isGrowUp = MapUtils.getBoolean(ageMap, "isGrowUp");//(true: 成年, false: 未成年)
			 
    		// 未成年電訪法定代理人
            if (!isGrowUp){
            	// 電訪對象被保人之法定代理人
                vo.setCalloutTargetType(CalloutConstants.CalloutTarget_3);
                vo.setCalleeName(MapUtils.getString(map, "LR_NAME", ""));
                Date lrBirthday = (Date)map.get("LR_BIRTHDAY");
                vo.setCalleeBirthday(lrBirthday);
                vo.setCalleeCertiCode(MapUtils.getString(map, "LR_CERTI_CODE", ""));
            }
        }
        
        // 聯絡電話.
        vo.setContactNum1(MapUtils.getString(map, "I_PHONE1"));
        vo.setContactNum2(MapUtils.getString(map, "I_PHONE2"));
        
        // 電訪結果:未電訪.
        vo.setCalloutResult(CalloutConstants.CalloutResult_type0);
        // 產生電訪日期.
        vo.setExpectedCalloutDate(processDate);
        
        
        /* 輸出檔案名稱 */
        String filename = String.format(fileName, DateUtils.date2String(processDate, "yyyyMMdd"));
        vo.setCalloutFileName(filename);
        /* 問卷編號 */
        vo.setCalloutDocketNum(MapUtils.getString(map, "DOCKET_NUM"));
        vo.setPolicyId(policyId);
        vo.setChangeId(MapUtils.getLong(map, "CHANGE_ID"));
        CalloutTransVO newVO = calloutTransService.saveVO(vo, calloutIdentity);
        org.springframework.util.Assert.notNull(newVO.getCalloutNum());
        map.put("CALLOUT_NUM", newVO.getCalloutNum());
        map.put("CALLOUT_NUM2", newVO.getCalloutNum());
        syso("[儲存電訪對象=被保人] callout_num = %s, callee_name = %s, callee_certi_code = %s ", newVO.getCalloutNum(), newVO.getCalleeName(), newVO.getCalleeCertiCode());
        return newVO.getCalloutNum();
    }
    
    public abstract String saveToTrans(Map<String, Object> map, java.util.Date processDate, String fileName, String dept);
    

    protected void saveToBatch(Map<String, Object> map, String fileName, String calloutNum, java.util.Date processDate, Long sampleType) {
        BatchLogUtils.addLog(LogLevel.INFO, null, "[ACT][save] Save to CalloutBatch. PolicyCode: " + map.get("POLICY_CODE").toString());
        CalloutBatchVO vo = new CalloutBatchVO();
        vo.setPolicyCode(map.get("POLICY_CODE").toString());
        vo.setCalloutNum(calloutNum);
        vo.setCalloutFileName(String.format(fileName, DateUtils.date2String(processDate, "yyyyMMdd")));
        vo.setBatchTime(processDate);
        vo.setConfigType(sampleType);
        calloutBatchService.save(vo);
        log.info("Save to CalloutBatch. PolicyCode: " + vo.getPolicyCode());
    }
    
    protected void nbSaveToBatch(List<Map<String, Object>> mapList, String fileName, Long sampleType, java.util.Date processDate){
        if(CollectionUtils.isEmpty(mapList))
            return;
        
        for (Map<String, Object> map : mapList) {
            String policyCode =MapUtils.getString(map, "POLICY_CODE");
            Boolean exists = batchServ.isSampled(policyCode, sampleType, processDate);
            if(exists){
                CalloutBatchVO calloutBatchVO = batchServ.query(policyCode, sampleType, processDate);
                map.put("BATCH_LIST_ID", calloutBatchVO.getListId());
            }else{
                CalloutBatch calloutBatch = batchServ.save(MapUtils.getString(map, "POLICY_CODE"), fileName, sampleType,processDate);
                map.put("BATCH_LIST_ID", calloutBatch.getListId());
            }
        }
    }
    
    protected void save(List<Map<String, Object>> mapList, java.util.Date processDate, String fileName, String dept, String docketNum, String configType, Long displayOrder)
                    throws IllegalStateException, SecurityException, SystemException {
        if (CollectionUtils.isEmpty(mapList))
            return;        
        /* 樣本抽取 */
        List<CalloutConfig> configList = calloutConfigService.query(configType, displayOrder);
        CalloutConfig cfg = null;
        if (CollectionUtils.isEmpty(configList)) {
            syso("尚未設定抽樣比例數");
            return ;
        }
        cfg = configList.get(0);
        if (cfg.getData().intValue() == 0) {
            syso("抽樣比例 := 0 %");
            return ;
        }
        Long sampleType = cfg.getListId();
        
        for (Map<String, Object> map : mapList) {
            try{
                String calloutNum = null;
                map.put("DETAIL_ITEM_DESC", cfg.getFileDesc());
                if (YesNo.YES_NO__YES.equals(MapUtils.getString(map, "IS_CALLOUT"))) {
                    /* 電訪問卷編號 */
                    map.put("DOCKET_NUM", docketNum);
                    /* 取得電訪編號 */
                    calloutNum = saveToTrans(map, processDate, fileName, dept);//寫入電訪表格
                    map.put("CALLOUT_NUM", calloutNum);
                }
                /* 寫入批次抽檔清單 */
                saveToBatch(map, fileName, calloutNum, processDate, sampleType);
            }catch (Exception ex){
                JSONObject json = new JSONObject(map);
                BatchLogUtils.addLog(LogLevel.ERROR, null, String.format("[BR-CMN-TFI-005][policy_id= %d]\n %s\n%s", MapUtils.getLong(map, "POLICY_ID"), json.toString(), ExceptionUtil.parse(ex)));
                throw ExceptionUtil.parse(ex);
            }
        }
    }
    
    protected Date toDate(String dateString) {
        if (" ".equals(dateString.substring(0, 1))) {
            dateString = dateString.substring(1, dateString.length());
        }
        if (dateString != null && dateString.matches(yyyMMdd)) {
            try {
                String newDateString = (Integer.parseInt(dateString.substring(0, 3)) + 1911)
                                + dateString.substring(3, dateString.length());
                return sdfyyyyMMdd.parse(newDateString);
            } catch (ParseException e) {
                return null;
            }
        } else {
            return DateUtils.toDate(dateString);
        }
    }

    protected List<Map<String, Object>> combineDataByPolicyCode(List<Map<String, Object>> inputMapList) {
        List<Map<String, Object>> mList = new java.util.LinkedList<Map<String, Object>>();
        if (inputMapList == null)
            return mList;

        /* 相同保單號碼，匯整服務業務員 */
        Map<String, Map<String, Object>> resultMap = new LinkedHashMap<String, Map<String, Object>>();
        for (Map<String, Object> map : inputMapList) {
            Long policyId = MapUtils.getLong(map, "POLICY_ID");
            String policyCode = MapUtils.getString(map, "POLICY_CODE");
            //PCR 415241 主約險種名稱
            map.put("PRODUCT_NAME", policyQueryService.getProductName(MapUtils.getLong(map, "PRODUCT_VERSION_ID")));
            QryBaseVO qryBaseVO = new QryBaseVO();
            qryBaseVO.setPolicyId(policyId);
            FinancialInfoNewVO financialInfoNewVO = qryPolicyDS.qryFinancialInfo_new(qryBaseVO);
            //PCR 415241 主約應繳保費
        	map.put("MASTER_PREM_AMOUNT", financialInfoNewVO.getMaster_prem_amount());
            //PCR 415241 繳費期間
            String chargePeriod = MapUtils.getString(map, "CHARGE_PERIOD");
            Integer chargeYear = MapUtils.getInteger(map, "CHARGE_YEAR");
            map.put("CHARGE_PERIOD_TEXT", policyQueryService.generateChargePeriodText(chargePeriod, chargeYear));
            
            
//            ScAgentDataVO agentDataVO = agentService.getAgentDataByPolicyId(policyId);
//            List<ScServiceAgentDataVO> serviceAgentList = agentDataVO.getServiceAgentList();
            List<ScServiceAgentDataVO> serviceAgentList = agentService.getServiceAgentDataByPolicyId(policyId);
            
            if(CollectionUtils.isEmpty(serviceAgentList))
                continue;
            if (!resultMap.containsKey(policyCode) && serviceAgentList.size() > 0) {
                ScServiceAgentDataVO agentVO = serviceAgentList.get(0);
                map.put("AGENT_NAME_1", agentVO.getAgentName());
                map.put("AGENT_REGISTER_CODE_1", agentVO.getAgentRegisterCode());
                // PCR_539272 表 T_SALES_CHANNEL 欄位  SALES_CHANNEL_NAME -> EXTERNAL_CHANNEL_CODE
//                String channelType = CodeTable.getCodeById("T_SALES_CHANNEL", String.valueOf(agentVO.getAgentChannelType()));
				String extChannelCode = CodeTable.getCodeById(TableCst.T_SALES_CHANNEL_EXT_CODE, String.valueOf(agentVO.getAgentChannelType()));
				map.put("CHANNEL_TYPE", extChannelCode);
                map.put("CHANNEL_TYPE_ID", agentVO.getAgentChannelType());
                map.put("CHANNEL_CODE",  agentVO.getAgentChannelCode());
                map.put("CHANNEL_NAME", agentVO.getAgentChannelName());
                resultMap.put(policyCode, map);
//                continue;
            }
            if(serviceAgentList.size() < 2)
                continue;
            ScServiceAgentDataVO agentVO = serviceAgentList.get(1);
            resultMap.get(policyCode).put("AGENT_NAME_2", agentVO.getAgentName());
            resultMap.get(policyCode).put("AGENT_REGISTER_CODE_2", agentVO.getAgentRegisterCode());
            if(serviceAgentList.size() < 3)
                continue;
            agentVO = serviceAgentList.get(2);
            resultMap.get(policyCode).put("AGENT_NAME_3", agentVO.getAgentName());
            resultMap.get(policyCode).put("AGENT_REGISTER_CODE_3", agentVO.getAgentRegisterCode());
        }

        /* distinct by policyCode 重新匯出結果 */
        for (Map.Entry<String, Map<String, Object>> entry : resultMap.entrySet())
            mList.add(entry.getValue());

        return mList;
    }

    @Override
    public int preProcess() throws Exception {
        
        return JobStatus.EXECUTE_SUCCESS;
    }

    @Override
    public int exeSingleRecord(String id) throws Exception {
        return JobStatus.EXECUTE_SUCCESS;
    }

    @Override
    public int postProcess() throws Exception {
        return JobStatus.EXECUTE_SUCCESS;
    }
    
    protected Boolean isProcess(String calloutFileName, java.util.Date processDate){
        StringBuffer sql = new StringBuffer();
        sql.append("select count(1) from t_callout_batch where callout_file_name like ':calloutFileName%' and batch_time= :processDate");
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("calloutFileName", calloutFileName);
        paramMap.put("processDate", processDate);
        int count = nt.queryForInt(sql.toString(), paramMap);
        if(count > 0)
            return Boolean.TRUE;
        return Boolean.FALSE;
    }
    
    public Boolean isFirstWorkDay(Date processDate){
        Date prevMothLatestDay = getPrevMonthLatestDay(processDate);
        Date firstWorkDay = DateUtils.getNextWorkday(prevMothLatestDay, 1);
        return firstWorkDay.equals(processDate);
    }
    
    private Date getPrevMonthLatestDay(Date processDate){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(DateUtils.addMonth(processDate,-1));
        calendar.set(Calendar.DAY_OF_MONTH,  calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        return calendar.getTime();
    }
    
    protected CalloutConfig getConfig(String configType, Long displayOrder){
        List<CalloutConfig> configList = calloutConfigService.query(configType, displayOrder);
        CalloutConfig cfg = null;
        if (CollectionUtils.isEmpty(configList)) {
            syso("尚未設定抽樣比例數");
        }
        cfg = configList.get(0);
        return cfg;
    }
    
    private List<Map<String, Object>> get080Sample(java.util.List<java.util.Map<String,Object>> origMapList, int sampleSize, Long sampleType){
    	java.util.List<java.util.Map<String,Object>> mapList = new java.util.LinkedList<Map<String,Object>>();
    	/* 先抽取本日保單件 */
		Map<String, Integer> sampleMap = new HashMap<String, Integer>();
		Integer redundantSampleSize = sampleSize;// 剩餘需抽取件數
		if (CollectionUtils.isNotEmpty(origMapList) && CollectionUtils.size(origMapList) == redundantSampleSize) {
			for (java.util.Map<String, Object> map : origMapList) {
				if (this instanceof UNB0802NoneInvestSNTask || this instanceof UNB08061TraditionTask || this instanceof UNB08062InvestTask) {
					if (map.get("REPLY_DATE") != null) {
						continue;
					}
				}
				String policyCode = MapUtils.getString(map, "POLICY_CODE");
				if (!sampleMap.containsKey(policyCode)) {// 無重覆抽取保單
					sampleMap.put(policyCode, new Integer(1));
					map.put("IS_CALLOUT", YesNo.YES_NO__YES);
					mapList.add(map);
				}
			}
			redundantSampleSize = sampleSize - mapList.size();
		}
		if (redundantSampleSize == 0)
			return mapList;
    	
    	/*當日保單件優先抽取 */
    	int bufferSize = 0;
        for (int now = 0; bufferSize < redundantSampleSize && now < MAX_SAMPLE_ROUND; now++) {
            int rowNum = new java.util.Random().nextInt(origMapList.size());
            Map<String,Object> randomMap = origMapList.get(rowNum);
            if (this instanceof UNB0802NoneInvestSNTask || this instanceof UNB08061TraditionTask || this instanceof UNB08062InvestTask) {
            	if (randomMap.get("REPLY_DATE") != null) {
            		continue;
            	}
            }
            String policyCode = MapUtils.getString(randomMap, "POLICY_CODE");
            Boolean notSampled = !batchServ.isSampled(policyCode, sampleType);
            if (!sampleMap.containsKey(policyCode) && notSampled) {// 無重覆抽取保單
            	randomMap.put("IS_CALLOUT", YesNo.YES_NO__YES);
                sampleMap.put(policyCode, new Integer(rowNum));
                mapList.add(randomMap);
                syso("抽出保單號碼 := %s, 隨機抽取序號 :=%d ", policyCode, rowNum);
                bufferSize += 1;
            }
        }
        redundantSampleSize = redundantSampleSize - bufferSize;
        if(redundantSampleSize == 0)
        	return mapList;
    	/* 不足件需往前抽樣 */
    	Date processDate = BatchHelp.getProcessDate();
    	Date previousDate = DateUtils.addDay(processDate, -1);
    	Date firstDayOfMonth = DateUtils.truncateMonth(processDate);
    	List<CalloutBatchVO> batchVOList = calloutBatchService.queryUnSamplingData(sampleType, firstDayOfMonth, previousDate);
    	int batchVOListSize = CollectionUtils.size(batchVOList);
    	syso("不足件_抽樣區間 start_date := %s, end_date := %s, sample_type := %d, CALLOUT_BATCH 本月尚未電訪件數 := %d, 還需抽取 := %d  ", 
    			DateUtils.date2string(firstDayOfMonth, "yyyy/MM/dd"), DateUtils.date2string(previousDate, "yyyy/MM/dd"), sampleType, batchVOListSize, redundantSampleSize);
    	if( batchVOListSize == 0 )
    		return mapList;
    	
        bufferSize = mapList.size();
        for (int now = 0; bufferSize < redundantSampleSize && now < MAX_SAMPLE_ROUND; now++) {
            int rowNum = new java.util.Random().nextInt(batchVOList.size());
            CalloutBatchVO calloutBatchVO = batchVOList.get(rowNum);
            String policyCode = calloutBatchVO.getPolicyCode();
            Boolean notSampled = !batchServ.isSampled(policyCode, sampleType);
            if (!sampleMap.containsKey(policyCode) && notSampled) {// 無重覆抽取保單
                List<Map<String,Object>> previousMapList = getData(calloutBatchVO.getBatchTime(), policyCode);
                if(CollectionUtils.size(previousMapList) == 0 )
                	continue;
                Map<String,Object> previousMap = previousMapList.get(0);
                if (this instanceof UNB0802NoneInvestSNTask || this instanceof UNB08061TraditionTask || this instanceof UNB08062InvestTask) {
                	if (previousMap.get("REPLY_DATE") != null) {
                		continue;
                	}
                }
                previousMap.put("IS_CALLOUT", YesNo.YES_NO__YES);
                sampleMap.put(policyCode, new Integer(rowNum));
                mapList.add(previousMap);
                syso("抽出保單號碼 := %s, 隨機抽取序號 :=%d ", policyCode, rowNum);
                bufferSize += 1;
            }
        }
        
    	return mapList;
    }
    
    protected List<Map<String, Object>> unbSampling(String logTitle, Long fileParam,String configType, Long displayOrder, String ducketNum, String dept) throws GenericException{
        String fileName = Para.getParaValue(fileParam);
        java.util.List<java.util.Map<String,Object>> mapList = new java.util.LinkedList<Map<String,Object>>();
        /* 樣本抽取 */
        CalloutConfig cfg = getConfig(configType, displayOrder);
        Long sampleType = cfg.getListId();
        Date processDate = BatchHelp.getProcessDate();
        if (sampleType == null)
            return mapList;
        BigDecimal samplePercent = cfg.getData();
        if ( samplePercent == null || samplePercent.floatValue() == 0f) {
            syso("抽樣比例 := 0 %");
        }else {
            syso("抽樣比例 := %.2f", samplePercent.floatValue());
        }
        /* 當日抽取樣本資料 */
        mapList = getData(processDate, null);
        int totalSize = mapList != null ? mapList.size() : 0;
        syso("當日抽取樣本資料 List.size = " + totalSize);
        if (totalSize == 0)
            return mapList;
        
        /* 寫入母體資料 */
        
        fileName = String.format(fileName,DateUtils.date2String(processDate, "yyyyMMdd"));
        ///不重覆寫入批次 */
        mapList = cleanDuplicateBatch(mapList, sampleType, processDate);//防重跑批
        org.hibernate.Session session = com.ebao.foundation.module.db.hibernate.HibernateSession3.currentSession();
        
        nbSaveToBatch(mapList, fileName, sampleType, processDate);
        session.flush();

        /* 當月累積件數 */
        List<CalloutBatchVO> batchVOList = calloutBatchService.queryByCurrentMonth(sampleType, processDate);
        syso("當月累積件數 := " + batchVOList.size());
        if (CollectionUtils.isEmpty(batchVOList)) {
            return new java.util.LinkedList<Map<String,Object>>();
        }

        /* 總抽樣數 */
        int totalSampleSize = new BigDecimal(batchVOList.size() * samplePercent.floatValue() / 100f).round(new MathContext(0)).intValue();
        
        Boolean isFirstDay = isFirstWorkDay(processDate);
        int samplingMothDataSize = calloutBatchService.querySamplingMonthData(sampleType, processDate).size();// 當月已抽取
        int sampleSize = totalSampleSize - samplingMothDataSize;//當月應抽取件數-當月已電訪件數
        syso("抽樣比率 := %.2f, 當月累積總件數 := %d, 當月應抽取件數 := %d, 當月已電訪件數 := %d, 抽樣件數 := %d" ,samplePercent.floatValue(), batchVOList.size(), totalSampleSize, samplingMothDataSize, sampleSize);
        
        if(BigDecimal.valueOf(0).compareTo(samplePercent) == 0) {//抽樣比例為零
        	return new java.util.LinkedList<Map<String,Object>>();
        }
        if (BigDecimal.valueOf(0).compareTo(samplePercent) != 0 && 
        		sampleSize == 0 && isFirstDay) {// 當月第一天,不滿一件, and 抽樣比率大於零
            sampleSize = 1;
        }
        syso("當月已抽取件數 %d, 尚需抽取件數  %d", samplingMothDataSize, sampleSize);
        /* 當月母體尚未電訪,才可抽取 */
        java.util.List<CalloutBatchVO> emptyList = batchServ.queryEmptyCalloutNumOfMonth(sampleType, processDate);
        if(CollectionUtils.isEmpty(emptyList)){
            syso("抽樣類型:= %d, 無剩餘可抽件數", sampleType);
            return new java.util.LinkedList<Map<String,Object>>();
        }
        
        /* 隨機樣本抽取 */
        mapList = get080Sample(mapList,  sampleSize,  sampleType);

        /* 寫入電訪資料表格 */
        for (Map<String, Object> map : mapList) {
            try {
                String calloutNum = null;
                if (YesNo.YES_NO__YES.equals(MapUtils.getString(map, "IS_CALLOUT"))) {
                    /* 電訪問卷編號 */
                    map.put("DOCKET_NUM", ducketNum);
                    /* 取得電訪編號 */
                    calloutNum = saveToTrans(map, processDate, fileName, dept);// 寫入電訪表格
                    /* 更新母體資料 */
                    calloutBatchService.updateCalloutNumber(MapUtils.getLong(map, "BATCH_LIST_ID"), calloutNum);
                    
                    /* 計算保額 */
                    map.put("UNIT_AMOUNT",  getMasterUnitAmount(MapUtils.getLong(map, "POLICY_ID")));
                }
            } catch (Exception ex) {
                JSONObject json = new JSONObject(map);
                BatchLogUtils.addLog(LogLevel.ERROR, null, String.format("[BR-CMN-TFI-005][policy_id= %d]\n %s\n%s", MapUtils.getLong(map, "POLICY_ID"),json.toString(),ExceptionUtil.parse(ex)));
                throw ExceptionUtil.parse(ex);
            }
        }
        return mapList;
    }
    
    protected Boolean existsCurrentDayBatch(String policyCode, Long sampleType, java.sql.Date processDate){
        StringBuffer sql = new StringBuffer();
        
        sql.append("select cb.* from t_callout_batch cb where cb.policy_code = :policy_code and cb.config_type = :sample_type and batch_time = :process_date ");
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("policy_code", policyCode);
        params.put("sample_type", sampleType);
        params.put("process_date", processDate);
        java.util.List<java.util.Map<String,Object>> mapList = nt.queryForList(sql.toString(), params);
        return CollectionUtils.isNotEmpty(mapList);
    }
    
    protected Boolean existsCalloutTrans(String policyCode, Long sampleType, Long changeId){
        StringBuffer sql = new StringBuffer();
        
        sql.append("select cb.* ");
        sql.append("  from t_callout_batch cb ");
        sql.append("  join t_callout_trans ct on ct.callout_num = cb.callout_num ");
        sql.append(" where cb.policy_code = :policy_code ");
        sql.append("   and cb.config_type = :sample_type ");
        sql.append("   and ct.change_id = :change_id ");
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("policy_code", policyCode);
        params.put("sample_type", sampleType);
        params.put("change_id", changeId);
        java.util.List<java.util.Map<String,Object>> mapList = nt.queryForList(sql.toString(), params);
        return CollectionUtils.isNotEmpty(mapList);
    }
    
    /**
     * <pre>
     * AND 符合排除規則：該保單的要保人已發起過該保單號的電訪，該保單不抽出。
     * 		詳細規則為：
	 *		系統已存在電訪任務，其中：
	 *		a)	電訪任務的保單號=該保單號，
	 *		b)	AND電訪對象類型=要保人，
	 *		c)	AND 電訪對象身分證字號=該保單的要保人身分證字號。
     * @param policyId
     * @param calleeCertiCode
     * @return
     */
    protected String addSqlWhereBy0801Of4(String policyId, String calleeCertiCode){
        StringBuffer sql = new StringBuffer();
        sql.append(" and not exists ( ");
    	sql.append(" 	select 1 from T_CALLOUT_TRANS tct ");
		sql.append(" 	where tct.policy_id = ").append(policyId);
		sql.append(" 	and tct.callout_target_type = '").append(CalloutTargetType.POLICY_HOLDER).append("'");
		sql.append(" 	and tct.callee_certi_code = ").append(calleeCertiCode).append(" ");
		sql.append(" ) ");
		return sql.toString();
    }
    
    /**
     * <pre>
     * AND 符合排除規則：該保單的要保人90天內已投保同一商品且當次電訪結果成功，該保單不抽出。詳細規則為：
     * 		i.	系統已存在其他保單，其中：
     * 			a)	其他保單的要保人=該保單的要保人，
     * 			b)	AND其他保單的生效日期≥當前系統日期-90天，
     * 			c)	AND其他保單的主約商品代碼前三碼=該保單的主約商品代碼前三碼，
     * 		ii.	且系統已存在其他保單的電訪任務，其中：
     * 			a)	電訪對象類型=要保人，
     * 			b)	AND 電訪對象身分證字號=該保單的要保人身分證字號，
     * 			c)	AND電訪結果=成功，
     * 			d)	AND執行單位為080。
     * @param policyId
     * @param certiCode
     * @param internalId
     * @param process_date
     * @return
     */
    protected String addSqlWhereBy0801Of5(String policyId, String certiCode, String internalId, String process_date){
        StringBuffer sql = new StringBuffer();
        sql.append(" and not exists ( ");
        sql.append(" 	select tph.certi_code, tph.name ");
        sql.append("        , tcm.policy_code, tcm.policy_id, tcm.validate_date ");
        sql.append("        , tcp.product_id ");
        sql.append("        , tpl.internal_id ");
        sql.append(" 	from t_Policy_Holder tph ");
        sql.append(" 	join t_contract_master tcm on tcm.policy_id = tph.policy_id and tcm.policy_id != ").append(policyId).append(" ");
        sql.append(" 	join t_contract_product tcp on tcp.policy_id = tcm.policy_id and tcp.master_id is null ");
        sql.append(" 	join t_product_life tpl on tpl.product_id = tcp.product_id ");
        sql.append(" 	where ");
        sql.append(" 	trunc(tcm.validate_date) >= trunc(:").append(process_date).append(")-numtodsinterval(90, 'day') ");
        sql.append(" 	and tph.certi_code = ").append(certiCode).append(" ");
        sql.append(" 	and substr(tpl.internal_id,0,3) = substr(").append(internalId).append(", 0, 3) ");
        sql.append(" 	and exists( ");
        sql.append("     	select 1 from T_CALLOUT_TRANS tct ");
        sql.append("     	where tct.policy_id = tcm.policy_id  ");
        sql.append("     	and tct.callout_target_type = '").append(CalloutTargetType.POLICY_HOLDER).append("'");
        sql.append("     	and tct.callee_certi_code = tph.certi_code ");
        sql.append("     	and tct.callout_result = '").append(CodeCst.CALLOUT_RESULT_SUCCESS).append("'");
        sql.append("     	and tct.callout_docket_num like '080%' ");
        sql.append(" 	) ");
		sql.append(" ) ");
		return sql.toString();
    }
    
    /**
     * <pre>
     * AND 符合排除規則：該保單的要保人已發起過該保單號的電訪，該保單不抽出。
     * 		詳細規則為：
	 *		系統已存在電訪任務，其中：
	 *		a)	電訪任務的保單號=該保單號，
     * @param policyId
     * @return
     */
    protected String addSqlWhereBy0802Of6(String policyId){
        StringBuffer sql = new StringBuffer();
        sql.append(" and not exists ( ");
    	sql.append(" 	select 1 from T_CALLOUT_TRANS tct ");
		sql.append(" 	where tct.policy_id = ").append(policyId);
		sql.append(" ) ");
		return sql.toString();
    }
    
    /**
     * <pre>
     * AND 符合排除規則：該保單的要保人90天內已投保同一商品且當次電訪結果成功，該保單不抽出。詳細規則為：
     * 		i.	系統已存在其他保單，其中：
     * 			a)	其他保單的要保人=該保單的要保人，
     * 			b)	AND其他保單的生效日期≥當前系統日期-90天，
     * 			c)	AND其他保單的主約商品代碼前三碼=該保單的主約商品代碼前三碼，
     * 		ii.	且系統已存在其他保單的電訪任務，其中：
     * 			a)	電訪對象類型=要保人，
     * 			b)	AND 電訪對象身分證字號=該保單的要保人身分證字號，
     * 			c)	AND電訪結果=成功，
     * 			d)	AND執行單位為080。
     * 		或
     *     iii.	系統已存在其他保單，其中：
     *			a)	其他保單的要保人之法定代理人=該保單的要保人之法定代理人，
     *			b)	AND其他保單的生效日期≥當前系統日期-90天，
     *			c)	AND其他保單的主約商品代碼前三碼=該保單的主約商品代碼前三碼，
     *		iv.	且系統已存在其他保單的電訪任務，其中：
     *			a)	電訪對象類型=要保人之法定代理人，
     *			b)	AND 電訪對象身分證字號=該保單的要保人之法定代理人身分證字號，
     *			c)	AND電訪結果=成功，
     *			d)	AND執行單位為080
     * @param policyId
     * @param certiCode
     * @param internalId
     * @param process_date
     * @return
     */
    protected String addSqlWhereBy0802Of7(String policyId, String certiCode, String internalId, String process_date){
        StringBuffer sql = new StringBuffer();
        sql.append(" and not exists ( ");
        sql.append(" 	select tph.certi_code, tph.name ");
        sql.append("        , tcm.policy_code, tcm.policy_id, tcm.validate_date ");
        sql.append("        , tcp.product_id ");
        sql.append("        , tpl.internal_id ");
        sql.append(" 	from t_Policy_Holder tph ");
        sql.append(" 	join t_contract_master tcm on tcm.policy_id = tph.policy_id and tcm.policy_id != ").append(policyId).append(" ");
        sql.append(" 	join t_contract_product tcp on tcp.policy_id = tcm.policy_id and tcp.master_id is null ");
        sql.append(" 	join t_product_life tpl on tpl.product_id = tcp.product_id ");
        sql.append(" 	where ");
        sql.append(" 	trunc(tcm.validate_date) >= trunc(:").append(process_date).append(")-numtodsinterval(90, 'day') ");
        sql.append(" 	and tph.certi_code = ").append(certiCode).append(" ");
        sql.append(" 	and substr(tpl.internal_id,0,3) = substr(").append(internalId).append(", 0, 3) ");
        sql.append(" 	and exists( ");
        sql.append("     	select 1 from T_CALLOUT_TRANS tct ");
        sql.append("     	where tct.policy_id = tcm.policy_id  ");
        sql.append("     	and tct.callout_target_type = '").append(CalloutTargetType.POLICY_HOLDER).append("'");
        sql.append("     	and tct.callee_certi_code = tph.certi_code ");
        sql.append("     	and tct.callout_result = '").append(CodeCst.CALLOUT_RESULT_SUCCESS).append("'");
        sql.append("     	and tct.callout_docket_num like '080%' ");
        sql.append(" 	) ");
        sql.append("  union all ");
        sql.append(" 	select tlr.certi_code, tlr.name ");
        sql.append("        , tcm.policy_code, tcm.policy_id, tcm.validate_date ");
        sql.append("        , tcp.product_id ");
        sql.append("        , tpl.internal_id ");
        sql.append(" 	from t_legal_representative tlr ");
        sql.append(" 	join t_contract_master tcm on tcm.policy_id = tlr.policy_id and tcm.policy_id != ").append(policyId).append(" ");
        sql.append(" 	join t_contract_product tcp on tcp.policy_id = tcm.policy_id and tcp.master_id is null ");
        sql.append(" 	join t_product_life tpl on tpl.product_id = tcp.product_id ");
        sql.append(" 	where ");
        sql.append(" 	trunc(tcm.validate_date) >= trunc(:").append(process_date).append(")-numtodsinterval(90, 'day') ");
        // 要保人之法定代理人
        sql.append(" 	and tlr.certi_code = ").append(certiCode).append(" and tlr.rel_type = '1' ");
        sql.append(" 	and substr(tpl.internal_id,0,3) = substr(").append(internalId).append(", 0, 3) ");
        sql.append(" 	and exists( ");
        sql.append("     	select 1 from T_CALLOUT_TRANS tct ");
        sql.append("     	where tct.policy_id = tcm.policy_id  ");
        sql.append("     	and tct.callout_target_type = '").append(CalloutTargetType.LEGAL_REPRESENTATIVE).append("'");
        sql.append("     	and tct.callee_certi_code = tlr.certi_code ");
        sql.append("     	and tct.callout_result = '").append(CodeCst.CALLOUT_RESULT_SUCCESS).append("'");
        sql.append("     	and tct.callout_docket_num like '080%' ");
        sql.append(" 	) ");
		sql.append(" ) ");
		return sql.toString();
    }
    
    protected java.util.List<java.util.Map<String,Object>> posCleanDuplicateBatch(java.util.List<java.util.Map<String,Object>> mapList, Long sampleType){
        java.util.List<java.util.Map<String,Object>> resultList = new java.util.LinkedList<java.util.Map<String,Object>>();
        if(CollectionUtils.isEmpty(mapList))
            return resultList;
        java.util.List<String> policyCodeList = new java.util.LinkedList<String>();
        
        for(int i = 0 ; i < mapList.size(); i ++ ){
            java.util.Map<String,Object> map = mapList.get(i);
            String policyCode = MapUtils.getString(map, "POLICY_CODE");
            Long changeId = MapUtils.getLong(map, "CHANGE_ID", -1L);
            if(changeId > 0 && existsCalloutTrans(policyCode, sampleType, changeId)){
                policyCodeList.add(policyCode);
            }
        }
        if(CollectionUtils.isNotEmpty(policyCodeList)){
            syso("母體已存在保單號碼 := " + StringUtils.join(policyCodeList.iterator(), ","));
        }
        for(java.util.Map<String,Object> map : mapList) {
            String policyCode = MapUtils.getString(map, "POLICY_CODE");
            if(policyCodeList.contains(policyCode)) {
                continue;
            }
            resultList.add(map);
        }
        return resultList;
    }
    protected java.util.List<java.util.Map<String,Object>> cleanDuplicateBatch(java.util.List<java.util.Map<String,Object>> mapList, Long sampleType, java.util.Date processDate){
    	java.util.List<java.util.Map<String,Object>> resultList = new java.util.LinkedList<java.util.Map<String,Object>>();
        if(CollectionUtils.isEmpty(mapList))
            return resultList;
        java.util.List<String> policyCodeList = new java.util.LinkedList<String>();
        /* 當日已存在母體的抽樣保單 */
        java.sql.Date sqlDate = new java.sql.Date(processDate.getTime());
        for(int i = 0 ; i < mapList.size(); i ++ ){
            java.util.Map<String,Object> map = mapList.get(i);
            String policyCode = MapUtils.getString(map, "POLICY_CODE");
            if(existsCurrentDayBatch(policyCode, sampleType, sqlDate)){
                policyCodeList.add(policyCode);
            }
        }
        if(CollectionUtils.isNotEmpty(policyCodeList)){
            syso("母體已存在保單號碼 := " + StringUtils.join(policyCodeList.iterator(), ","));
        }
        /* 擷取未抽樣保單 */
        for(java.util.Map<String,Object> map : mapList) {
        	String policyCode = MapUtils.getString(map, "POLICY_CODE");
        	if(policyCodeList.contains(policyCode)) {
        		continue;
        	}
        	resultList.add(map);
        }
        return resultList;
    }
    
    private String getMasterUnitAmount(Long policyId){
        String unitAmount="";
        PolicyInfo policyInfo =policyservice.load(policyId);
        java.util.List<CoverageInfo>  coverageList = policyInfo.getCoverages();
        List<String> amountFlag = Arrays.asList(new String[] { "0", "6", "7", "9" });
        if(CollectionUtils.isEmpty(coverageList))
            return unitAmount;
        for(CoverageInfo itemVO : coverageList){
            if(itemVO.getMasterCoverageId() != null)
                continue;
            ProductVO productVO = prdServ.getProduct(new Long(itemVO.getProductId()));
            String unitFlag = productVO.getUnitFlag();
            if(productVO != null && StringUtils.isEmpty(unitFlag))
                continue;
            
            if ("1".equals(unitFlag)) {
                // 按單位計算
                Integer unit = itemVO.getCurrentPremium().getUnit().intValue();
                try {
                    unitAmount = String.format("%,d %s", unit, CodeTableUtils.getDesc("V_CALLOUT_UNIT_FLAG", "1"));
                } catch (Exception e) {
                    log.info("[ERROR][GET_UNIT]" + ExceptionUtils.getFullStackTrace(e));
                    unitAmount = String.valueOf(unit);
                }
            } else if ("4".equals(unitFlag)) {
                // 按計劃
                String benefitLevel = itemVO.getCurrentPremium().getBenefitLevel();
                try {
                    unitAmount = String.format("%s %s", CodeTableUtils.getDesc("V_CALLOUT_UNIT_FLAG", "4"), benefitLevel);
                } catch (Exception e) {
                    log.info("[ERROR][GET_BENEFIT]" + ExceptionUtils.getFullStackTrace(e));
                    unitAmount = benefitLevel;
                }
            } else if (amountFlag.contains(unitFlag)) {
                // 按保額計算sumAssured
                Integer amountVal = 0;
                if(itemVO.getCurrentPremium() != null && itemVO.getCurrentPremium().getSumAssured() != null){
                    amountVal = itemVO.getCurrentPremium().getSumAssured().intValue();
                }
                try {
                    unitAmount = String.format("%,d %s", amountVal, CodeTableUtils.getDesc("V_CALLOUT_UNIT_FLAG", "6"));
                } catch (Exception e) {
                    log.info("[ERROR][GET_AMOUNT]" + ExceptionUtils.getFullStackTrace(e));
                    unitAmount = String.valueOf(amountVal);
                }
            }
        }
        return unitAmount;
    }
}
