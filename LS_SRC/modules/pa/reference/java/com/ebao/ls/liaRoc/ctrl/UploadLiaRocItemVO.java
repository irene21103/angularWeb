package com.ebao.ls.liaRoc.ctrl;

import java.util.Date;
import com.ebao.ls.pa.pub.vo.CoverageVO;

public class UploadLiaRocItemVO {

	String policyCode;
	
	Long itemId;
	
	Integer itemOrder;

	Integer riskStatus;

	long productId;
	
	long productVersionId;

	String inceptionDate;

	Integer chargeYear;

	Integer coverageYear;

	String expiryDate;

	String name;

	String certiCode;
	
	Integer relationToPh;

	String insuredCategory;

	String amtUnitLvlNumber;

	String chargeMode;
	
	CoverageVO coverageVo;

	public String getPolicyCode() {
		return policyCode;
	}

	public void setPolicyCode(String policyCode) {
		this.policyCode = policyCode;
	}

	public Long getItemId() {
		return itemId;
	}

	public void setItemId(Long itemId) {
		this.itemId = itemId;
	}

	public Integer getItemOrder() {
		return itemOrder;
	}

	public void setItemOrder(Integer itemOrder) {
		this.itemOrder = itemOrder;
	}

	public Integer getRiskStatus() {
		return riskStatus;
	}

	public void setRiskStatus(Integer riskStatus) {
		this.riskStatus = riskStatus;
	}

	public long getProductId() {
		return productId;
	}

	public long getProductVersionId() {
		return productVersionId;
	}

	public void setProductVersionId(long productVersionId) {
		this.productVersionId = productVersionId;
	}

	public void setProductId(long productId) {
		this.productId = productId;
	}

	public String getInceptionDate() {
		return inceptionDate;
	}

	public void setInceptionDate(String inceptionDate) {
		this.inceptionDate = inceptionDate;
	}

	public Integer getChargeYear() {
		return chargeYear;
	}

	public void setChargeYear(Integer chargeYear) {
		this.chargeYear = chargeYear;
	}

	public Integer getCoverageYear() {
		return coverageYear;
	}

	public void setCoverageYear(Integer coverageYear) {
		this.coverageYear = coverageYear;
	}

	public String getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCertiCode() {
		return certiCode;
	}

	public void setCertiCode(String certiCode) {
		this.certiCode = certiCode;
	}

	public Integer getRelationToPh() {
		return relationToPh;
	}

	public void setRelationToPh(Integer relationToPh) {
		this.relationToPh = relationToPh;
	}

	public String getInsuredCategory() {
		return insuredCategory;
	}

	public void setInsuredCategory(String insuredCategory) {
		this.insuredCategory = insuredCategory;
	}

	public String getAmtUnitLvlNumber() {
		return amtUnitLvlNumber;
	}

	public void setAmtUnitLvlNumber(String amtUnitLvlNumber) {
		this.amtUnitLvlNumber = amtUnitLvlNumber;
	}

	public String getChargeMode() {
		return chargeMode;
	}

	public void setChargeMode(String chargeMode) {
		this.chargeMode = chargeMode;
	}

	public CoverageVO getCoverageVo() {
		return coverageVo;
	}

	public void setCoverageVo(CoverageVO coverageVo) {
		this.coverageVo = coverageVo;
	}

}
