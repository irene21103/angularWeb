package com.ebao.ls.uw.ctrl.decision;

import java.math.BigDecimal;
import java.util.Date;

import com.ebao.pub.framework.GenericForm;

/**
 * <p>Title:Underwriting</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: eBaoTech Corporation</p>
 *
 * @author mingchun.shi
 * @since  Jun 27, 2005
 * @version 1.0 
 */
public class UwProductInfoForm extends GenericForm {
  private String renewDesc;

  private String renewDecision;

  private String facInsurance;

  private java.math.BigDecimal cedingPercentage;

  private java.util.Date lifeTimeLimit;

  private java.util.Date commDate;

  private java.util.Date nextPaymentDate;

  //all insured
  private java.util.ArrayList allInsured = new java.util.ArrayList();

  //all products
  private java.util.ArrayList products = new java.util.ArrayList();

  private Integer decisionId;

  private Long reInsurer;

  private Long underwriteId;

  private Long[] itemIds;

  private Long itemId1;

  private String applyCode;

  private String policyCode;

  private Long organId;

  private String uwStatus;

  private Long underwriterId;

  private Long preUnderwriterId;

  private String decisionReason;

  private String decisionReasonDesc;

  private String[] benefitType;

  private String operatorId;

  private String decisionDesc2;

  private boolean isApplyFac;

  private String status;

  private Long uwListId1;

  private String smoking1;

  private String smoking2;

  private String maFactor;
  private String msFactor;
  private String isMaFactor;
  private String isMsFactor;
  private String ma_max;
  private String ma_min;
  private String ms_max;
  private String ms_min;
  private String indxIndicator_decision;
  private java.lang.Integer postponePeriod;
  
  private Date applyDate; //要保書填寫日
  private Date submissionDate; //受理日
  private Date validateDate; //生效日
  private BigDecimal firstPayPremium; //首期應繳保費
  private BigDecimal suspensePremium; //一般懸帳

  public java.lang.Integer getPostponePeriod() {
    return postponePeriod;
  }

  public void setPostponePeriod(java.lang.Integer postponePeriod) {
    this.postponePeriod = postponePeriod;
  }

  private java.util.ArrayList riList = new java.util.ArrayList();

  public java.util.ArrayList getRiList() {
    return riList;
  }

  public void setRiList(java.util.ArrayList riList) {
    this.riList = riList;
  }

  public String getIsMaFactor() {
    return isMaFactor;
  }

  public void setIsMaFactor(String isMaFactor) {
    this.isMaFactor = isMaFactor;
  }

  public String getIsMsFactor() {
    return isMsFactor;
  }

  public void setIsMsFactor(String isMsFactor) {
    this.isMsFactor = isMsFactor;
  }

  public String getMa_max() {
    return ma_max;
  }

  public void setMa_max(String ma_max) {
    this.ma_max = ma_max;
  }

  public String getMa_min() {
    return ma_min;
  }

  public void setMa_min(String ma_min) {
    this.ma_min = ma_min;
  }

  public String getMaFactor() {
    return maFactor;
  }

  public void setMaFactor(String maFactor) {
    this.maFactor = maFactor;
  }

  public String getMs_max() {
    return ms_max;
  }

  public void setMs_max(String ms_max) {
    this.ms_max = ms_max;
  }

  public String getMs_min() {
    return ms_min;
  }

  public void setMs_min(String ms_min) {
    this.ms_min = ms_min;
  }

  public String getMsFactor() {
    return msFactor;
  }

  public void setMsFactor(String msFactor) {
    this.msFactor = msFactor;
  }

  /**
   * @return allInsured
   */
  public java.util.ArrayList getAllInsured() {
    return allInsured;
  }

  /**
   * @param allInsured will be set allInsured
   */
  public void setAllInsured(java.util.ArrayList allInsured) {
    this.allInsured = allInsured;
  }

  /**
   * @return applyCode
   */
  public String getApplyCode() {
    return applyCode;
  }

  /**
   * @param applyCode will be set applyCode
   */
  public void setApplyCode(String applyCode) {
    this.applyCode = applyCode;
  }

  /**
   * @return benefitType
   */
  public String[] getBenefitType() {
    return benefitType;
  }

  /**
   * @param benefitType will be set benefitType
   */
  public void setBenefitType(String[] benefitType) {
    this.benefitType = benefitType;
  }

  /**
   * @return cedingPercentage
   */
  public java.math.BigDecimal getCedingPercentage() {
    return cedingPercentage;
  }

  /**
   * @param cedingPercentage will be set cedingPercentage
   */
  public void setCedingPercentage(java.math.BigDecimal cedingPercentage) {
    this.cedingPercentage = cedingPercentage;
  }

  /**
   * @return commDate
   */
  public java.util.Date getCommDate() {
    return commDate;
  }

  /**
   * @param commDate will be set commDate
   */
  public void setCommDate(java.util.Date commDate) {
    this.commDate = commDate;
  }

  /**
   * @return decisionDesc2
   */
  public String getDecisionDesc2() {
    return decisionDesc2;
  }

  /**
   * @param decisionDesc2 will be set decisionDesc2
   */
  public void setDecisionDesc2(String decisionDesc2) {
    this.decisionDesc2 = decisionDesc2;
  }

  /**
   * @return decisionId
   */
  public Integer getDecisionId() {
    return decisionId;
  }

  /**
   * @param decisionId will be set decisionId
   */
  public void setDecisionId(Integer decisionId) {
    this.decisionId = decisionId;
  }

  /**
   * @return decisionReason
   */
  public String getDecisionReason() {
    return decisionReason;
  }

  /**
   * @param decisionReason will be set decisionReason
   */
  public void setDecisionReason(String decisionReason) {
    this.decisionReason = decisionReason;
  }

  /**
   * @return decisionReasonDesc
   */
  public String getDecisionReasonDesc() {
    return decisionReasonDesc;
  }

  /**
   * @param decisionReasonDesc will be set decisionReasonDesc
   */
  public void setDecisionReasonDesc(String decisionReasonDesc) {
    this.decisionReasonDesc = decisionReasonDesc;
  }

  /**
   * @return facInsurance
   */
  public String getFacInsurance() {
    return facInsurance;
  }

  /**
   * @param facInsurance will be set facInsurance
   */
  public void setFacInsurance(String facInsurance) {
    this.facInsurance = facInsurance;
  }

  /**
   * @return isApplyFac
   */
  public boolean getIsApplyFac() {
    return isApplyFac;
  }

  /**
   * @param isApplyFac will be set isApplyFac
   */
  public void setApplyFac(boolean isApplyFac) {
    this.isApplyFac = isApplyFac;
  }

  /**
   * @return itemId1
   */
  public Long getItemId1() {
    return itemId1;
  }

  /**
   * @param itemId1 will be set itemId1
   */
  public void setItemId1(Long itemId1) {
    this.itemId1 = itemId1;
  }

  /**
   * @return itemIds
   */
  public Long[] getItemIds() {
    return itemIds;
  }

  /**
   * @param itemIds will be set itemIds
   */
  public void setItemIds(Long[] itemIds) {
    this.itemIds = itemIds;
  }

  /**
   * @return lifeTimeLimit
   */
  public java.util.Date getLifeTimeLimit() {
    return lifeTimeLimit;
  }

  /**
   * @param lifeTimeLimit will be set lifeTimeLimit
   */
  public void setLifeTimeLimit(java.util.Date lifeTimeLimit) {
    this.lifeTimeLimit = lifeTimeLimit;
  }

  /**
   * @return nextPaymentDate
   */
  public java.util.Date getNextPaymentDate() {
    return nextPaymentDate;
  }

  /**
   * @param nextPaymentDate will be set nextPaymentDate
   */
  public void setNextPaymentDate(java.util.Date nextPaymentDate) {
    this.nextPaymentDate = nextPaymentDate;
  }

  /**
   * @return operatorId
   */
  public String getOperatorId() {
    return operatorId;
  }

  /**
   * @param operatorId will be set operatorId
   */
  public void setOperatorId(String operatorId) {
    this.operatorId = operatorId;
  }

  /**
   * @return policyCode
   */
  public String getPolicyCode() {
    return policyCode;
  }

  /**
   * @param policyCode will be set policyCode
   */
  public void setPolicyCode(String policyCode) {
    this.policyCode = policyCode;
  }

  /**
   * @return preUnderwriterId
   */
  public Long getPreUnderwriterId() {
    return preUnderwriterId;
  }

  /**
   * @param preUnderwriterId will be set preUnderwriterId
   */
  public void setPreUnderwriterId(Long preUnderwriterId) {
    this.preUnderwriterId = preUnderwriterId;
  }

  /**
   * @return products
   */
  public java.util.ArrayList getProducts() {
    return products;
  }

  /**
   * @param products will be set products
   */
  public void setProducts(java.util.ArrayList products) {
    this.products = products;
  }

  /**
   * @return reInsurer
   */
  public Long getReInsurer() {
    return reInsurer;
  }

  /**
   * @param reInsurer will be set reInsurer
   */
  public void setReInsurer(Long reInsurer) {
    this.reInsurer = reInsurer;
  }

  /**
   * @return renewDecision
   */
  public String getRenewDecision() {
    return renewDecision;
  }

  /**
   * @param renewDecision will be set renewDecision
   */
  public void setRenewDecision(String renewDecision) {
    this.renewDecision = renewDecision;
  }

  /**
   * @return renewDesc
   */
  public String getRenewDesc() {
    return renewDesc;
  }

  /**
   * @param renewDesc will be set renewDesc
   */
  public void setRenewDesc(String renewDesc) {
    this.renewDesc = renewDesc;
  }

  /**
   * @return smoking1
   */
  public String getSmoking1() {
    return smoking1;
  }

  /**
   * @param smoking1 will be set smoking1
   */
  public void setSmoking1(String smoking1) {
    this.smoking1 = smoking1;
  }

  /**
   * @return smoking2
   */
  public String getSmoking2() {
    return smoking2;
  }

  /**
   * @param smoking2 will be set smoking2
   */
  public void setSmoking2(String smoking2) {
    this.smoking2 = smoking2;
  }

  /**
   * @return status
   */
  public String getStatus() {
    return status;
  }

  /**
   * @param status will be set status
   */
  public void setStatus(String status) {
    this.status = status;
  }

  /**
   * @return underwriteId
   */
  public Long getUnderwriteId() {
    return underwriteId;
  }

  /**
   * @param underwriteId will be set underwriteId
   */
  public void setUnderwriteId(Long underwriteId) {
    this.underwriteId = underwriteId;
  }

  /**
   * @return underwriterId
   */
  public Long getUnderwriterId() {
    return underwriterId;
  }

  /**
   * @param underwriterId will be set underwriterId
   */
  public void setUnderwriterId(Long underwriterId) {
    this.underwriterId = underwriterId;
  }

  /**
   * @return uwListId1
   */
  public Long getUwListId1() {
    return uwListId1;
  }

  /**
   * @param uwListId1 will be set uwListId1
   */
  public void setUwListId1(Long uwListId1) {
    this.uwListId1 = uwListId1;
  }

  /**
   * @return uwStatus
   */
  public String getUwStatus() {
    return uwStatus;
  }

  /**
   * @param uwStatus will be set uwStatus
   */
  public void setUwStatus(String uwStatus) {
    this.uwStatus = uwStatus;
  }

  public String getIndxIndicator_decision() {
    return indxIndicator_decision;
  }

  public void setIndxIndicator_decision(String indxIndicator_decision) {
    this.indxIndicator_decision = indxIndicator_decision;
  }

  public Long getOrganId() {
    return organId;
  }

  public void setOrganId(Long organId) {
    this.organId = organId;
  }
  /**
   * <p>Description : 要保書填寫日</p>
   * <p>Created By : simon.huang</p>
   * <p>Create Time : Jul 2, 2015</p>
   * @return
   */
  public Date getApplyDate() {
      return applyDate;
  }

  public void setApplyDate(Date applyDate) {
      this.applyDate = applyDate;
  }
  /**
   * <p>Description : 受理日</p>
   * <p>Created By : simon.huang</p>
   * <p>Create Time : Jul 2, 2015</p>
   * @return
   */
  public Date getSubmissionDate() {
      return submissionDate;
  }

  public void setSubmissionDate(Date submissionDate) {
      this.submissionDate = submissionDate;
  }
  /**
   * <p>Description : 生效日</p>
   * <p>Created By : simon.huang</p>
   * <p>Create Time : Jul 2, 2015</p>
   * @return
   */
  public Date getValidateDate() {
      return validateDate;
  }

  public void setValidateDate(Date validateDate) {
      this.validateDate = validateDate;
  }

  /**
   * <p>Description : 首期應繳保費 </p>
   * <p>Created By : simon.huang</p>
   * <p>Create Time : Jul 2, 2015</p>
   * @return
   */
  public BigDecimal getFirstPayPremium() {
      return firstPayPremium;
  }

  public void setFirstPayPremium(BigDecimal firstPayPremium) {
      this.firstPayPremium = firstPayPremium;
  }
  /**
   * <p>Description : 一般懸帳</p>
   * <p>Created By : simon.huang</p>
   * <p>Create Time : Jul 2, 2015</p>
   * @return
   */
  public BigDecimal getSuspensePremium() {
      return suspensePremium;
  }

  public void setSuspensePremium(BigDecimal suspensePremium) {
      this.suspensePremium = suspensePremium;
  }

}
