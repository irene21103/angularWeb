package com.ebao.ls.riskPrevention.ajax;

import java.util.Map;

import javax.annotation.Resource;

import org.json.JSONObject;

import com.ebao.ls.pub.ajax.AjaxService;
import com.ebao.ls.riskPrevention.data.NationalityCodeDao;
import com.ebao.ls.riskPrevention.data.bo.NationalityCode;
import com.ebao.pub.util.BeanUtils;

public class NationalityCodeAjaxServiceImpl implements AjaxService<Map<String,Object>,String>{

	@Resource(name = NationalityCodeDao.BEAN_DEFAULT)
	NationalityCodeDao nationalityCodeDao;
	
	@Override
	public String execute(Map<String, Object> in) {
		Integer nationalCode=(Integer)in.get("nationalCode");
		NationalityCode bo = nationalityCodeDao.load(String.valueOf(nationalCode));
		return new JSONObject(BeanUtils.getMap(bo)).toString();
	}

}
