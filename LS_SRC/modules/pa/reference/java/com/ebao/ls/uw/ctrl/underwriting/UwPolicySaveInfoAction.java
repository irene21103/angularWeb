package com.ebao.ls.uw.ctrl.underwriting;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.UserTransaction;

import org.apache.commons.lang.ClassUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.foundation.module.db.hibernate.HibernateSession3;
import com.ebao.foundation.module.web.action.ExceptionHandler;
import com.ebao.ls.arap.pub.ci.PremCI;
import com.ebao.ls.chl.service.ChannelOrgService;
import com.ebao.ls.cs.logger.ApplicationLogger;
import com.ebao.ls.cs.logger.helper.AppLogDataHelper;
import com.ebao.ls.cs.util.pkg.CalculatorPremSP;
import com.ebao.ls.escape.helper.EscapeHelper;
import com.ebao.ls.fatca.vo.FatcaIdentityVO;
import com.ebao.ls.liaRoc.LiarocDownloadLogCst;
import com.ebao.ls.liaRoc.ci.LiaRocDownload2020CI;
import com.ebao.ls.liaRoc.ds.LiarocDownloadLogHelper;
import com.ebao.ls.pa.nb.bs.ProposalProcessService;
import com.ebao.ls.pa.nb.bs.ProposalService;
import com.ebao.ls.pa.nb.bs.rule.ProposalRuleService;
import com.ebao.ls.pa.nb.ctrl.detailreg.DetailRegAllInOneForm;
import com.ebao.ls.pa.nb.ctrl.detailreg.DetailRegForm;
import com.ebao.ls.pa.nb.ctrl.detailreg.DetailRegHelper;
import com.ebao.ls.pa.nb.ctrl.helper.UnbPaHelper;
import com.ebao.ls.pa.nb.util.NBUtils;
import com.ebao.ls.pa.pty.certicode.CertiCodeCheckService;
import com.ebao.ls.pa.pub.NbTaskCst;
import com.ebao.ls.pa.pub.bs.Agent3monChgService;
import com.ebao.ls.pa.pub.bs.AgentNotifyEncoService;
import com.ebao.ls.pa.pub.bs.AgentNotifyPremSourceService;
import com.ebao.ls.pa.pub.bs.AgentNotifyPurposeService;
import com.ebao.ls.pa.pub.bs.AgentNotifyService;
import com.ebao.ls.pa.pub.bs.AgentRelService;
import com.ebao.ls.pa.pub.bs.AgentRemoteStayService;
import com.ebao.ls.pa.pub.bs.EddaService;
import com.ebao.ls.pa.pub.bs.ElderScaleRoleService;
import com.ebao.ls.pa.pub.bs.IlpRiskService;
import com.ebao.ls.pa.pub.bs.IlpTargetPriceService;
import com.ebao.ls.pa.pub.bs.InvestCashAccountService;
import com.ebao.ls.pa.pub.bs.LegalRepresentativeService;
import com.ebao.ls.pa.pub.bs.MailToService;
import com.ebao.ls.pa.pub.bs.MposNotifyService;
import com.ebao.ls.pa.pub.bs.NbAutoWithdrawService;
import com.ebao.ls.pa.pub.bs.NbPolicyChgService;
import com.ebao.ls.pa.pub.bs.NbYouthNoticeService;
import com.ebao.ls.pa.pub.bs.PaySurveyService;
import com.ebao.ls.pa.pub.bs.PayerService;
import com.ebao.ls.pa.pub.bs.PaymentMethodService;
import com.ebao.ls.pa.pub.bs.PolicyBankAuthService;
import com.ebao.ls.pa.pub.bs.PolicyConversionService;
import com.ebao.ls.pa.pub.bs.PolicyMiscFeeService;
import com.ebao.ls.pa.pub.bs.PolicyService;
import com.ebao.ls.pa.pub.bs.RemoteAgreementService;
import com.ebao.ls.pa.pub.bs.RemotePrsnInfAgrmntService;
import com.ebao.ls.pa.pub.bs.ReviewService;
import com.ebao.ls.pa.pub.bs.ServicesBenefitService;
import com.ebao.ls.pa.pub.bs.SettlementLetterService;
import com.ebao.ls.pa.pub.bs.SharedCashAccountService;
import com.ebao.ls.pa.pub.bs.TargetMaturityFundService;
import com.ebao.ls.pa.pub.vo.AgentNotifyVO;
import com.ebao.ls.pa.pub.vo.ConvertedConversionVO;
import com.ebao.ls.pa.pub.vo.EddaVO;
import com.ebao.ls.pa.pub.vo.IlpRiskVO;
import com.ebao.ls.pa.pub.vo.IlpTargetPriceVO;
import com.ebao.ls.pa.pub.vo.InsuredVO;
import com.ebao.ls.pa.pub.vo.InvestCashAccountVO;
import com.ebao.ls.pa.pub.vo.LegalRepresentativeVO;
import com.ebao.ls.pa.pub.vo.MailToVO;
import com.ebao.ls.pa.pub.vo.MposNotifyVO;
import com.ebao.ls.pa.pub.vo.NbAutoWithdrawVO;
import com.ebao.ls.pa.pub.vo.NbPolicyChgVO;
import com.ebao.ls.pa.pub.vo.NbYouthNoticeVO;
import com.ebao.ls.pa.pub.vo.PaySurveyVO;
import com.ebao.ls.pa.pub.vo.PayerAccountVO;
import com.ebao.ls.pa.pub.vo.PayerVO;
import com.ebao.ls.pa.pub.vo.PolicyBankAuthVO;
import com.ebao.ls.pa.pub.vo.PolicyMiscFeeVO;
import com.ebao.ls.pa.pub.vo.PolicyVO;
import com.ebao.ls.pa.pub.vo.RemoteAgreementVO;
import com.ebao.ls.pa.pub.vo.RemotePrsnInfAgrmntVO;
import com.ebao.ls.pa.pub.vo.ServicesBenefitVO;
import com.ebao.ls.pa.pub.vo.SettlementLetterVO;
import com.ebao.ls.pa.pub.vo.SharedCashAccountVO;
import com.ebao.ls.pa.pub.vo.TargetMaturityFundVO;
import com.ebao.ls.pa.pub.vo.VOGenerator;
import com.ebao.ls.pub.CodeCst;
import com.ebao.ls.pub.action.ElapsedTimeLoggerAction;
import com.ebao.ls.pub.cst.NBDummy;
import com.ebao.ls.pub.cst.Service;
import com.ebao.ls.pub.util.TGLDateUtil;
import com.ebao.ls.rstf.ci.liaroc20.Liaroc20Cst;
import com.ebao.ls.uw.ci.UwPolicyCI;
import com.ebao.ls.uw.ds.UwPolicyService;
import com.ebao.ls.uw.ds.UwProcessService;
import com.ebao.ls.uw.ds.UwWorkSheetService;
import com.ebao.pub.event.EventService;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.framework.ExceptionFactory;
import com.ebao.pub.util.DateUtils;
import com.ebao.pub.util.Log;
import com.ebao.pub.util.StringUtils;
import com.ebao.pub.util.Trans;
import com.ebao.pub.util.TransUtils;
import com.ebao.pub.workflow.jbpm.JbpmContextUtil;

/**
 * <p> Title:Underwriting </p>
 * <p> Description: </p>
 * <p> Copyright: Copyright (c) 2004 </p>
 * <p> Company: eBaoTech Corporation </p>
 * 
 * @author sunny
 * @since 2015-05-22
 * @version 1.0
 */
public class UwPolicySaveInfoAction extends ElapsedTimeLoggerAction {
	public static final String BEAN_DEFAULT = "/uw/uwPolicySaveInfoAction";

	@Resource(name = UwPolicyService.BEAN_DEFAULT)
	private UwPolicyService uwPolicyDS;

	@Resource(name = UwProcessService.BEAN_DEFAULT)
	private UwProcessService uwProcessDS;

	@Resource(name = EventService.BEAN_DEFAULT)
	private EventService eventService;

	@Resource(name = UwWorkSheetService.BEAN_DEFAULT)
	private UwWorkSheetService uwWorkSheetService;

	@Resource(name = UwPolicyCI.BEAN_DEFAULT)
	private UwPolicyCI uwPolicyCI;
	
	@Resource(name = MposNotifyService.BEAN_DEFAULT)
	private MposNotifyService mposNotifyService;

	@Resource(name = NbYouthNoticeService.BEAN_DEFAULT)
	private NbYouthNoticeService youthNoticeService;

	@Resource(name = RemoteAgreementService.BEAN_DEFAULT)
	private RemoteAgreementService rmtAgreementService;

	@Resource(name = RemotePrsnInfAgrmntService.BEAN_DEFAULT)
	private RemotePrsnInfAgrmntService rmtPrsnInfAgrmntService;
	
	@Resource(name = NbPolicyChgService.BEAN_DEFAULT)
	private NbPolicyChgService nbPolicyChgService;
	
	Logger log4j = Logger.getLogger(getClass());
	@Override
	public ActionForward doProcess(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		UserTransaction ut = null;
		
		DetailRegAllInOneForm detailRegForm = (DetailRegAllInOneForm) form;
		Long policyId = Long.valueOf(detailRegForm.getPolicyId()) ;
		String policyCode = policyService.getPolicyCodeByPolicyId(policyId);
		ActionForward forword = null;
		
		ApplicationLogger.setSystemCode(AppLogDataHelper.SYSTEM_CODE_UNB);
		ApplicationLogger.setJobName(ClassUtils.getShortClassName(UwPolicySaveInfoAction.class));
		ApplicationLogger.setPolicyCode(policyCode);
		ApplicationLogger.setOptionId(DateUtils.date2String(new Date(),"yyyyMMdd HHmmssS"));
		//先執行授權書資料存檔，因為會影響保單初始生效日規則
		Map<String, PolicyBankAuthVO> uiBankAuth = null;
		try {
			
			ApplicationLogger.addLoggerData("updateNBApplyDateAutoCommit() begin");
			Date uiApplyDate = TGLDateUtil.parseROCString(NBDummy.DUMMY_BIRTHDAY);
			if (!StringUtils.isNullOrEmpty(detailRegForm.getApplyDate())) {
				uiApplyDate = DateUtils.toDate(detailRegForm.getApplyDate());
			}
			policyDS.updateNBApplyDateAutoCommit(policyId, uiApplyDate);
			ApplicationLogger.addLoggerData("updateNBApplyDateAutoCommit() end");
			
			ApplicationLogger.addLoggerData(com.ebao.pub.util.EnvUtils.getEnvLabel());
			this.beforeCopyFormData(detailRegForm);
			
			ApplicationLogger.addLoggerData("policyService.load("+policyId+") begin");
			PolicyVO policy = policyService.load(policyId);
			ApplicationLogger.addLoggerData("policyService.load("+policyId+") end");
			ut = Trans.getUserTransaction();
			ut.begin();

			// 2019/03/27 RTC-305116:電子化轉帳付款約定書
			EddaVO eddaVO = detailRegHelper.getEdda(detailRegForm);
			eddaService.saveOrUpdate(eddaVO);

			// PCR 481172 BC443 被保險人未滿15足歲投保聲明書
			NbYouthNoticeVO youthNoticeVO = detailRegHelper.getYouthNotice(detailRegForm);
			if (null != youthNoticeVO)
				youthNoticeService.saveOrUpdate(youthNoticeVO);

			// PCR 481174 遠距投保試辦被保險人境外投保 之 遠距投保聲明及同意書
			RemoteAgreementVO remoteAgreement = detailRegHelper.getRemoteAgreement(detailRegForm);
			if (null != remoteAgreement)
				rmtAgreementService.saveOrUpdate(remoteAgreement);

			// PCR 481174 遠距投保試辦被保險人境外投保 之 遠距-個人資料蒐集處理利用同意書
			RemotePrsnInfAgrmntVO remotePrsnInfAgrmnt = detailRegHelper.getRemotePrsnInfAgrmnt(detailRegForm);
			if (null != remotePrsnInfAgrmnt)
				rmtPrsnInfAgrmntService.saveOrUpdate(remotePrsnInfAgrmnt);
			
			if (detailRegHelper.isInvestProduct(policy)) { //投資型保單
				// 2019/06/25 BC-364 PCR-315427 RTC-325154:要保人之現金給付帳戶資訊
				InvestCashAccountVO investCashAccountVO = detailRegHelper.getInvestCashAccount(detailRegForm);
				investCashAccountService.saveOrUpdate(investCashAccountVO);

				if (detailRegHelper.isPolicyCurrencyInNTD(policy)) {
					// 2019/08/22 Jyun-yu BC-373 PCR-336494 RTC-332528:要保人委託壽險業者辦理結匯暨額度查詢授權書
					SettlementLetterVO settlementLetterVO = detailRegHelper.getSettlementLetter(detailRegForm);
					settlementLetterService.saveOrUpdate(settlementLetterVO);
				}

				if (detailRegHelper.isTargetMaturityFund(policy)) {
					// 2019/08/20 Yvon BC-373 PCR-332528 RTC-336493: 將資料庫中投資型定期買回暨投資標的運用期屆滿處理方式相關欄位資料存至表單物件中進行呈現。
					TargetMaturityFundVO targetMaturityFundVO = detailRegHelper.getTargetMaturityFund(detailRegForm);
					targetMaturityFundService.saveOrUpdate(targetMaturityFundVO);
					// 2019/08/20 Yvon BC-373 PCR-332528 RTC-336493: 將資料庫中投資型定期買回暨投資標的運用期屆滿處理方式金融帳戶資訊相關欄位資料存至表單物件中進行呈現。
					SharedCashAccountVO targetMaturityFundCashAccountVO = detailRegHelper.getTargetMaturityFundCashAccount(detailRegForm);
					sharedCashAccountService.saveOrUpdate(targetMaturityFundCashAccountVO);
				}
				boolean isAutodraw = detailRegHelper.isAutoWithdraw(policy);
				if (isAutodraw) {// 投資型保險自動提領 Add by Bingo
                    NbAutoWithdrawVO nbAutoWithdrawVO = detailRegHelper.getAutoWithdraw(detailRegForm, policy.gitMasterCoverage());
                    // BC-374 PCR-356470 RTC-364868 投資型保險自動提領 版次、自動提領頻率、自動提領比例(%)、自動提領基準日
                    nbAutoWithdrawService.saveOrUpdate(nbAutoWithdrawVO);
                    SharedCashAccountVO targetAutoWithdrawCashAccountVO = detailRegHelper.getTargetAutoWithdrawCashAccount(detailRegForm);
                    // BC-374 PCR-356470 RTC-364868 投資型保險自動提領 帳戶資訊相關欄位資料
                    sharedCashAccountService.saveOrUpdate(targetAutoWithdrawCashAccountVO);
                }
			}
		
			if (CodeCst.SUBMIT_CHANNEL__MPOS.equals(policy.getSubmitChannel())) {
				MposNotifyVO mposNotifyVO = detailRegHelper.getMposNotify(detailRegForm);
				mposNotifyService.saveOrUpdate(mposNotifyVO);
			}

	        //查找變更前授權書資料
	        Map<String, PolicyBankAuthVO> beforeBankAuthMap = policyBankAuthService.findMapByPolicyId(policyId);
			//PCR-298625 2019/04/12 Add by Kathy
			HibernateSession3.currentSession().flush();
			
			Map<String, PolicyBankAuthVO> policyBankAuth = detailRegHelper.getPolicyBankAuth(detailRegForm);

			//多執行一次取得頁面輸入授權書資料
			uiBankAuth = detailRegHelper.getPolicyBankAuth(detailRegForm);
			
			//PCR-298625 新增檢核保戶有無申請eDDA授權資料，如果有回寫policyBankAuthVO 20190327 Add by Kathy
//			policyBankAuthService.syncBankAuthOnUNB(policy.getPolicyId(), policyBankAuth);
			NBUtils.logger(this.getClass(), "detailRegHelper.eDDACheck() begin");
			Map<String, PolicyBankAuthVO> policyBankAuthEdda = 
					detailRegHelper.eDDACheck(policyId, policyBankAuth, detailRegForm);

			policyBankAuthService.syncBankAuthOnUNB(policy.getPolicyId(), policyBankAuthEdda);

			//PCR-363527 新增檢核有勾選「重新產生核印資抖」產生一筆核印資料 2020/05/31 Add by Kathy
			boolean authReSendBank = detailRegForm.getAuthReSendBank() != null ? true : false;
			boolean nextReSendBank = detailRegForm.getNextReSendBank() != null ? true : false;
			boolean authReSendCreditCard = detailRegForm.getAuthReSendCreditCard() != null ? true : false;
			boolean nextReSendCreditCard = detailRegForm.getNextReSendCreditCard() != null ? true : false;
			boolean isReSend = false;
			if 	(authReSendBank || nextReSendBank || authReSendCreditCard || nextReSendCreditCard)
			{
				NBUtils.logger(this.getClass(), "policyBankAuthService.doReSendBankAuth() begin");
				isReSend = policyBankAuthService.isReSendBankAuth(policyId,
								authReSendBank, nextReSendBank, 
								authReSendCreditCard, nextReSendCreditCard);
			}
						
			NBUtils.logger(this.getClass(), "callPremCICreateSealRecord() begin");
			HibernateSession3.currentSession().flush();
			
			policyBankAuthService.callPremCICreateSealRecord(policyId, beforeBankAuthMap);
			NBUtils.logger(this.getClass(), "callPremCICreateSealRecord() end");
			policyBankAuth = policyBankAuthService.findMapByPolicyId(policyId);
			//IR-332522-不為核印處理中(表示資料不完整),若信用卡效期、授權人姓名有異動需同步回t_bank_account
			for(PolicyBankAuthVO bankAuthVO : policyBankAuth.values()) {
				if(bankAuthVO.getAccountId() != null && 
								!CodeCst.APPROVAL_STATUS__IN_PROCESS.equals(bankAuthVO.getApprovalStatus())) {
					
					//IR-332522-授權人姓名有異動需更新t_bank_account.account_name
					boolean isUpdateAccountName = false;
					PolicyBankAuthVO beforeBankAuthVO = beforeBankAuthMap.get(bankAuthVO.getSourceType());
					if(beforeBankAuthVO != null 
									&& bankAuthVO.getAccountName() != null) {
						//目前頁面有輸入姓名且與前次姓名不同,需作姓名更新
						if(!bankAuthVO.getAccountName().equals(beforeBankAuthVO.getAccountName())) {
							isUpdateAccountName = true;
						}
					}
					//END IR-332522
					policyBankAuthService.updatePayerAccountByBankAuth(policyId, bankAuthVO, isUpdateAccountName, false);
				}
			}
			
			//邏輯參照DetailRegAjaxService.saveInsured() 被保險人身份證修改邏輯
			String oldHolderCertiCode = policy.getPolicyHolder().getCertiCode();
			if(oldHolderCertiCode == null){
				oldHolderCertiCode = "";
			}
			String newHolderCertiCode = detailRegForm.getPolicyHolderCertiCode();
			if(!oldHolderCertiCode.equals(newHolderCertiCode)){
				//修改且身份證號不相同刪除原體檢照會及原被保險人問卷資料
				for(InsuredVO insuredVO : policy.gitSortInsuredList()){
					if(insuredVO.isSamePolicyHolder(oldHolderCertiCode)){
						uwPolicyCI.removeUWInfoRelatedToInsuredRemoved(policyId,insuredVO.getListId());
						break;
					}
				}
			}
			Integer oldHolderJobClass = policy.getPolicyHolder().getOccupClass();
			/*首、續期繳費方式變更*/
            boolean isPayModeChange = false;
			boolean isDcPolicyChange = false;
			String applyDate = DateUtils.date2String(policy.getApplyDate());
			Integer payMode = policy.gitMasterCoverage().getCurrentPremium().getPaymentMethod();
			Integer payNext = policy.gitMasterCoverage().getNextPremium().getPaymentMethod();
			String payTogetherCode = StringUtils.nullToEmpty(policy.getPayTogetherCode());
			
			
			// 要在執行fillPolicyVer2更新資料前驗，才能知道證號有沒有被更新
			// 若 要保人證號有被異動 && 新證號屬於舊的外國人證號格式，呼叫接口
			String chgCertiCode = detailRegForm.getPolicyHolderCertiCode();	// 要保人新證號
			String oriCertiCode  = policy.getPolicyHolder().getCertiCode();	// 要保人舊證號
			if (chgCertiCode != null && !chgCertiCode.equalsIgnoreCase(oriCertiCode) && CertiCodeCheckService.isOldForeignerId(chgCertiCode)) {
				try {
					// 公會下載失敗不影響其他流程，cache住錯誤
					liaRocDownload2020CI.downloadResidentId(Liaroc20Cst.MN_UNB.getCode(), policy.getPolicyId(), chgCertiCode);
				} catch (Exception e) {
					NBUtils.logger(this.getClass(), "liaRocDownload2020CI.downloadResidentId() exception: " + e.getMessage());
					Log.error(this.getClass(), e);
				}
			}
			
			detailRegHelper.savePolicyHolderNbChg(policy, detailRegForm);//PCR 509354 配合投資型保險商品客戶風險屬性分析評估表調整檢核規則

			//頁面資料設定回policyVO
			detailRegHelper.fillPolicyVer2(detailRegForm, policy);
			
			String chgApplyDate = DateUtils.date2String(policy.getApplyDate());
			Integer chgPayMode = policy.gitMasterCoverage().getCurrentPremium().getPaymentMethod();
			Integer chgPayNext = policy.gitMasterCoverage().getNextPremium().getPaymentMethod();
			String chgPayTogetherCode = StringUtils.nullToEmpty(policy.getPayTogetherCode());
			
			if (NBUtils.in(payMode + "", chgPayMode + "") == false
						|| NBUtils.in(payNext + "", chgPayNext + "") == false) {
				isPayModeChange = true;
			}
			if(NBUtils.in(applyDate, chgApplyDate) == false
						|| NBUtils.in(payMode+"", chgPayMode+"") == false
						|| NBUtils.in(payNext+"", chgPayNext+"") == false
						|| NBUtils.in(payTogetherCode, chgPayTogetherCode) == false) {
				isDcPolicyChange = true;
			}
			
			// when store, place detail comment in t_contract_proposal
			policy.setComments(detailRegForm.getDetailRegComment());

			ConvertedConversionVO conversion = detailRegHelper
					.getConvertedPolicy(detailRegForm);
			PolicyMiscFeeVO policyMiscFee = detailRegHelper
					.getPolicyMiscFee(detailRegForm);

			/*
			 * add by robert.xu to update the pending_cause on 2005/08/29
			 */
			// Save Benefit Agent Info.
			detailRegHelper.updateCommissionAgent(detailRegForm, policy);
			org.hibernate.Session s = com.ebao.foundation.module.db.hibernate.HibernateSession3
					.currentSession();
			if (s != null) {
				s.flush();
			}

			// save the policy data
			if ((conversion != null)
					&& CodeCst.PREM_PLAN_TYPE__CONVERSION_FEE
							.equals(conversion
									.getPremPlanType())) {
				policyConversionService.saveConvertedConversion(conversion);
			}

			policyMiscFeeService.save(policyMiscFee);
			s.flush();
			FatcaIdentityVO fatcaIdentityVO = null;
			MailToVO mailToVO = null;
			AgentNotifyVO agentNotifyVO = null;
			PaySurveyVO paySurveyVO = null;
			ServicesBenefitVO servicesBenefitVO = null;
			LegalRepresentativeVO legalReprVO = null;
			
			{// add by simon.huang to overwrite New Bussiness flow on 2015-05-08

				agentNotifyVO = detailRegHelper
						.getAgentNotify(detailRegForm);

				IlpRiskVO riskVO = detailRegHelper.getIlpRisk(detailRegForm, policy);
				mailToVO = detailRegHelper.getMailTo(detailRegForm);
				paySurveyVO = detailRegHelper.getPaySurvey(detailRegForm);
	
				servicesBenefitVO = detailRegHelper.getServicesBenefit(detailRegForm);
				{
					s.flush();
					
					ApplicationLogger.addLoggerData("detailRegHelper.saveFatcaIdentityVo()");
					fatcaIdentityVO = detailRegHelper.saveFatcaIdentityVo(detailRegForm, policy);
					if (s != null) {
						s.flush();
					}
				}
				
				/* PCR-366497
				 * 1.	若修改要保人區塊的職業等級，執行確認時判斷若業報書版本=FID版，需同時更新業報書的要保人職業等級。
				 * 2.	若修改被保險人區塊的被保險人職業等級/婚姻狀況，執行確認時判斷若業報書版本=FID版，需同時更新業報書的被保險人職業等級/婚姻狀況。
				 */
				List<AgentNotifyVO> lastAgentNotifyVOList = agentNotifyService.findByPolicyId(policyId);
				detailRegHelper.syncAgentNotifyJobClass(policy, agentNotifyVO, lastAgentNotifyVOList.get(0), oldHolderJobClass);

				ApplicationLogger.addLoggerData("proposalService.saveProposalVer2() begin");
				proposalService.saveProposalVer2(policy); // replace ebao method:proposalService.saveProposal(policy);
				ApplicationLogger.addLoggerData("proposalService.saveProposalVer2() end");
				// artf124033: add by alex.cheng to add save payer Data on
				// 2015-05-29
				PayerVO payerVO = detailRegHelper.getPayer(detailRegForm);
				payerVO = payerService.save(payerVO);

				/* 2015/07/14 alex cheng 增加豁免險資料擴充到每個險項 */
				{
					ApplicationLogger.addLoggerData("detailRegHelper.saveWaiverCoverage() begin");
					detailRegHelper.saveWaiverCoverage(policy.getPolicyId());
					ApplicationLogger.addLoggerData("detailRegHelper.saveWaiverCoverage() end");
				}
				
				
				ApplicationLogger.addLoggerData("save other info begin");
				agentNotifyService.saveOrUpdate(agentNotifyVO);
				agentNotifyEncoService.syncEnco(policy.getPolicyId(), detailRegForm.getEconSources());
				agentNotifyPurposeService.syncPurpose(policy.getPolicyId(), detailRegForm.getApplyPurpose());
				agentNotifyPremSourceService.syncPremSource(policy.getPolicyId(), detailRegForm.getPremSource());
				agentRelService.syncAgentRel(policy.getPolicyId(), detailRegForm.getAgentRel());
				agent3monChgService.syncAgent3monChg(policy.getPolicyId(), detailRegForm.getAgent3monChg());
				
				/*  PCR_481174 遠距投保試辦被保險人境外投保   */
				agentRemoteStayService.syncAgentRemoteStay(policy.getPolicyId(), detailRegForm.getAgentRemoteStay());
				/*  END of PCR_481174 by Alex Chang 2022.4.8  */
				
				// artf124033: add by alex.cheng to add save payerAccount Data
				// on 2015-05-29
				PayerAccountVO payerAccountVO = detailRegHelper
						.getPayerAccount(detailRegForm, payerVO, policyBankAuth);
				payerAccountService.save(payerAccountVO,true);
				riskService.saveOrUpdate(riskVO);

				// PCR 454240 配合 BC414 投資型保險投資標的價格適用日批註核保檢核需求
				IlpTargetPriceVO ilpTargetPrice = detailRegHelper.getIlpTargetPrice(detailRegForm);
				if (null != ilpTargetPrice)
					ilpTargetPriceService.saveOrUpdate(ilpTargetPrice);

				mailToService.saveOrUpdate(mailToVO);
				// 2017/11/25 YCsu 新增 : 以外幣收付非投資型人身保險客戶適合度調查評估
				paySurveyService.saveOrUpdate(paySurveyVO);
				
				// 2018/06/14 SunYu 新增 :實物給付型保險商品客戶適合度調查評估表
				servicesBenefitService.saveOrUpdate(servicesBenefitVO);

				//2016-05-26 save分期定額info start
				detailRegHelper.saveInvestInfo(policy, detailRegForm);
				//2016-05-26 save分期定額info end

				//2016-05-17 save年金定期提領info start
				detailRegHelper.saveAnnuityInfo(
						detailRegForm,
						policy.getPolicyId(),
						policy.gitMasterCoverage());

				// 2018/08/14 Simon PCR-258201:法定代理人
				ApplicationLogger.addLoggerData("legalRepresentativeService.saveUNBLegalRepresentative()");
				legalReprVO = detailRegHelper.getLegalRepresentative(detailRegForm);
				legalRepresentativeService.saveUNBLegalRepresentative(policy.getPolicyId(),legalReprVO,policy.getApplyVersion());
				elderScaleRoleService.syncElderScaleRole(policyId);

				//生成法定代理人partyId
				legalRepresentativeService.saveUNBUnderwriting(policyId);
				
				ApplicationLogger.addLoggerData("save other info end");
				//2016-05-17 save年金定期提領info end
				//artf124383:[Coding] [Gap_UNB_031-SR001] by simon.huang on 2015-06-05
				//proposalService.updateRiskCommenceDate(policy.getPolicyId());
			}
			
			//在途保單檢核要保書填寫日，是否寫入夜批計算調整保費			
			if(isDcPolicyChange) {
				CalculatorPremSP.clearPremiumAdjustValue(policyId);
			}
			
			//預設值處理
			
			
			ApplicationLogger.addLoggerData("policyService.reload("+policyId+") begin");
			HibernateSession3.detachSession();
			s = HibernateSession3.currentSession();
			policy = policyService.load(policy.getPolicyId());
			ApplicationLogger.addLoggerData("policyService.reload("+policyId+") end");
			
			ApplicationLogger.addLoggerData("proposalService.synchronizeToParty() begin");
			proposalService.synchronizeToParty(policy);
			ApplicationLogger.addLoggerData("proposalService.synchronizeToParty() end");
			s.flush();
			
			
			ApplicationLogger.addLoggerData("policyService.save() begin");
			//獲得法定代理人
			List<LegalRepresentativeVO> regalRepList = legalRepresentativeService.findByPolicyId(policy.getPolicyId());
			LegalRepresentativeVO regalRep = null;
			if (regalRepList != null && regalRepList.size() > 0) {
				regalRep = regalRepList.get(0);
			}
			detailRegHelper.setVerificationDefaultValue(policy, fatcaIdentityVO, mailToVO, null, regalRep);
            
			if (regalRep != null) {
				legalRepresentativeService.saveOrUpdate(regalRep);
			}
			policyService.save(policy);
			mailToService.saveOrUpdate(mailToVO);
			ApplicationLogger.addLoggerData("policyService.save() end");
			s.flush();
			
			/*
			* <p>2018/04/23 Add : YCSu</p>
			* <p>[artf410579](Internal Issue 241631) [PROD]保險業務員核保報告(所有版本)-要保人與被保人為同一人,XX收入欄位有值另一欄位空白,於覆核註記時空白欄位寫入值</p>
			*/			
			ApplicationLogger.addLoggerData("detailRegHelper.setAgentDefeuleValue() begin");
			detailRegHelper.setAgentDefeuleValue(policy, agentNotifyVO);
			agentNotifyService.saveOrUpdate(agentNotifyVO);
			ApplicationLogger.addLoggerData("detailRegHelper.setAgentDefeuleValue() end");
			s.flush();	

			/* 執行保單所有險種保費計算(含豁免險),2018/01/29 調整為by保項計算,因RTC-218261豁免險查無保費率造成其它保項未計算加費金額 */
			/* 效能問題不執行每個保項計算保費，統一由保單層計算
			for(CoverageVO coverageVO : policy.getCoverages()) {
				if(coverageVO.isWaiver() == false) {
					try {
						
						ApplicationLogger.addLoggerData("CalculatorPremSP.calcPremium("+coverageVO.getItemId()+") begin");
						CalculatorPremSP.calcPremium(coverageVO.getItemId());
					} catch (Exception e) {
						log4j.warn(e.getMessage());
						ApplicationLogger.addLoggerData("CalculatorPremSP.calcPremium("+coverageVO.getItemId()+") error:\n" + 
										e.getMessage() );
						//會有年齡不符合保費率設定無法計算保費的case，catch exception由RMS作年齡校驗
						//like:投保年齡未達QTR(20年期,K版)最低年齡限18歲。請取消該附約。
					} finally {
						ApplicationLogger.addLoggerData("CalculatorPremSP.calcPremium("+coverageVO.getItemId()+") end");
					}
				}
			}
			
			for(CoverageVO coverageVO : policy.getCoverages()) {
				if(coverageVO.isWaiver()) {
					try {
						ApplicationLogger.addLoggerData("CalculatorPremSP.calcWaiverPremium("+coverageVO.getPolicyId()
							+", "+coverageVO.getItemId()+") begin");
						 CalculatorPremSP.calcWaiverPremium(coverageVO.getPolicyId(), coverageVO.getItemId());
					} catch (Exception e) {
						log4j.warn(e.getMessage());
						ApplicationLogger.addLoggerData("CalculatorPremSP.calcWaiverPremium("+coverageVO.getPolicyId()
							+", "+coverageVO.getItemId()+") error:\n" + e.getMessage() );
						
						//會有年齡不符合保費率設定無法計算保費的case，catch exception由RMS作年齡校驗
						//like:投保年齡未達QTR(20年期,K版)最低年齡限18歲。請取消該附約。
					} finally {
						ApplicationLogger.addLoggerData("CalculatorPremSP.calcWaiverPremium("+coverageVO.getPolicyId()
						+", "+coverageVO.getItemId()+") end");
					}
				}
			}
			*/
			proposalService.doSubmitDetailRegistration(policy);
		
			ut.commit();
			
			/* 無保單收費 */
			unbPaHelper.updateNonPolicy(policy);
			
            //IR-407770 異動繳費方式即檢核並更新報盤銀行 接口 2020/09/17 Add by Vince Cheng
            // IR-418372一律更新報盤銀行
			ApplicationLogger.addLoggerData("updateAcquirerId("+detailRegForm.getInitEddaFlag()+","+detailRegForm.getNextEddaFlag()+") begin");
            unbPaHelper.updateAcquirerId(policyId, detailRegForm.getInitEddaFlag(), detailRegForm.getNextEddaFlag());
            ApplicationLogger.addLoggerData("updateAcquirerId() end");
			
		} catch (Exception e) {
			TransUtils.rollback(ut);
			ApplicationLogger.addLoggerData(ExceptionUtils.getFullStackTrace(e));
			ApplicationLogger.flush();
			return ExceptionHandler.handleAppException(request, response, ExceptionFactory.parse(e));
		} 
		
		boolean isSuccess = false;
		try {
			HibernateSession3.detachSession();
			ut = Trans.getUserTransaction();
			ut.begin();
			LiarocDownloadLogHelper.initNB(LiarocDownloadLogCst.NB_UW_MODIFY_PAGE_CHECK_RULE, policyId);

			// 更新批次簽署資訊
			proposalService.syncBatchSignInfoFromChannelOrg(policyId);
			//核保中修改，重新執行OPQ欄規則校驗
			boolean isEcOffline = policyService.isEcOffline(policyId);
			if (isEcOffline) {
				proposalService.checkProposalRules(policyId, ProposalRuleService.ROLESSET_VERIFICATION_EC_OFFLINE, uiBankAuth);
			} else {
				proposalService.checkProposalRules(policyId, ProposalRuleService.ROLESSET_UW_MODIFIY, uiBankAuth);
			}

			HibernateSession3.currentSession().flush();

			ut.commit();

			isSuccess = true;
		} catch (Exception e) {
			TransUtils.rollback(ut);
			ApplicationLogger.addLoggerData(ExceptionUtils.getFullStackTrace(e));
			ApplicationLogger.flush();
			return ExceptionHandler.handleAppException(request, response, ExceptionFactory.parse(e));
		} finally {
			LiarocDownloadLogHelper.flush();
		}
	
		try{
			ut = Trans.getUserTransaction();
			ut.begin();

            if(isSuccess) {
				//增加核保流程控管機制
				NBUtils.logger(this.getClass(), "policyDS.updatePolicyOPQCheckIndi() begin");
				policyService.updatePolicyOPQCheckIndi(policyId, CodeCst.OPQ_CHECK_FINISH);	
				NBUtils.logger(this.getClass(), "policyDS.updatePolicyOPQCheckIndi() end");
            }
            
			String underwriteId = com.ebao.ls.encode.encoder.Encode.encodeForHTML(request).getParameter("underwriteId");
			Long userId = AppContext.getCurrentUser().getUserId();
			uwProcessDS.updateNBInfo(Long.valueOf(underwriteId), userId);
			EscapeHelper.escapeHtml(request).setAttribute("underwriteId", underwriteId);
			ut.commit();
		}catch(Exception e){
			TransUtils.rollback(ut);
			ApplicationLogger.addLoggerData(ExceptionUtils.getFullStackTrace(e));
			ApplicationLogger.flush();
			return ExceptionHandler.handleAppException(request, response, ExceptionFactory.parse(e));
		}
		
		// update to control NBU process with workflow engine,by robert.xu
		// on
		// 2008.4.17
		// add to control NBU process with workflow engine,by robert.xu on
		// 2008.4.17
		// WfHelper.processManagerForSaveOrCancel(policyVO.getPolicyId());
		// WfUtils.maintainVariable(policyVO.getPolicyId(), null, null);
		try {
			HibernateSession3.detachSession();
			//若有系統關閉已發放核保訊息，可能會會更新照會單狀態，會執行一次workflow變數更新
			//執行detachSession會造成jbpm底層session抓到舊的,先close讓底層重抓
			JbpmContextUtil.closeJbpmContext();
			//END
			ut = Trans.getUserTransaction();
			ut.begin();
			proposalProcessService.maintainFlowVariable(policyId);
			ut.commit();
		} catch (Exception e) {
			TransUtils.rollback(ut);
			ApplicationLogger.addLoggerData(ExceptionUtils.getFullStackTrace(e));
			ApplicationLogger.flush();
			throw ExceptionFactory.parse(e);
		}
		// add end
		ApplicationLogger.flush();
		
		request.setAttribute("policyId", policyId);
		EscapeHelper.escapeHtml(request).setAttribute("originType",EscapeHelper.escapeHtml(request.getParameter("originType")));
		forword = mapping.findForward("success");
		// update end
		return forword;
		
	}

	protected void beforeCopyFormData(DetailRegForm form) {
		if (!StringUtils.isNullOrEmpty(form.getInitialPrem())) {
			form.setInitialPrem(StringUtils.replace(form.getInitialPrem(), ",",
					""));
		}
		if (!StringUtils.isNullOrEmpty(form.getPremAmount())) {
			form.setPremAmount(StringUtils.replace(form.getPremAmount(), ",",
					""));
		}
		if (!StringUtils.isNullOrEmpty(form.getMiscFee())) {
			form.setMiscFee(StringUtils.replace(form.getMiscFee(), ",", ""));
		}
		if (!"99".equals(form.getUwReason())) {
			form.setUwReasonDesc(null);
		}
		// if the commencement date is null, set it to system time
		// and set backdating indicator to "N"
		if (StringUtils.isNullOrEmpty(form.getValidateDate())) {
			String sysDate = DateUtils.date2String(AppContext
					.getCurrentUserLocalTime());
			form.setValidateDate(sysDate);
			form.setBackdatingIndi("N");
		}
	}

	public int findIndexOfArray(String key, String[] array) {
		if (StringUtils.isNullOrEmpty(key)) {
			return -1;
		}
		if (array == null) {
			return -1;
		}
		for (int i = 0; i < array.length; i++) {
			if (key.equals(array[i])) {
				return i;
			}
		}
		return -1;
	}
	
	  @Override
	  protected String getModule() {
	      return ElapsedTimeLoggerAction.MODULE_UNB;
	  }

	  @Override
	  protected Class<?> getApplicationId() {
	      return UwPolicySaveInfoAction.class;
	  }

	  @Override
	  protected String getApplicationDesc() {
	      return "Uw Policy Save Info(預收輸入存檔)";
	  }

	@Resource(name = PolicyService.BEAN_DEFAULT)
	protected PolicyService policyService;

	@Resource(name = com.ebao.ls.pa.pub.service.PolicyService.BEAN_DEFAULT)
	protected com.ebao.ls.pa.pub.service.PolicyService policyDS;

	@Resource(name = ProposalService.BEAN_DEFAULT)
	protected ProposalService proposalService;

	@Resource(name = PolicyMiscFeeService.BEAN_DEFAULT)
	protected PolicyMiscFeeService policyMiscFeeService;

	@Resource(name = PolicyConversionService.BEAN_DEFAULT)
	protected PolicyConversionService policyConversionService;

	@Resource(name = VOGenerator.BEAN_DEFAULT)
	protected VOGenerator voGenerator;

	@Resource(name = DetailRegHelper.BEAN_DEFAULT)
	protected DetailRegHelper detailRegHelper;

	@Resource(name = ProposalProcessService.BEAN_DEFAULT)
	protected ProposalProcessService proposalProcessService;

	@Resource(name = AgentNotifyService.BEAN_DEFAULT)
	private AgentNotifyService agentNotifyService;

	@Resource(name = AgentNotifyEncoService.BEAN_DEFAULT)
	private AgentNotifyEncoService agentNotifyEncoService;

	@Resource(name = AgentNotifyPurposeService.BEAN_DEFAULT)
	private AgentNotifyPurposeService agentNotifyPurposeService;
	
	@Resource(name = AgentNotifyPremSourceService.BEAN_DEFAULT)
	private AgentNotifyPremSourceService agentNotifyPremSourceService;
	
	@Resource(name = AgentRelService.BEAN_DEFAULT)
	private AgentRelService agentRelService;
	  
	@Resource(name = Agent3monChgService.BEAN_DEFAULT)
	private Agent3monChgService agent3monChgService;
	
	@Resource(name = PolicyBankAuthService.BEAN_DEFAULT)
	private PolicyBankAuthService policyBankAuthService;

	@Resource(name = IlpRiskService.BEAN_DEFAULT)
	private IlpRiskService riskService;

	@Resource(name = IlpTargetPriceService.BEAN_DEFAULT)
	private IlpTargetPriceService ilpTargetPriceService;

	@Resource(name = MailToService.BEAN_DEFAULT)
	private MailToService mailToService;

	@Resource(name = ReviewService.BEAN_DEFAULT)
	private ReviewService reviewService;

	@Resource(name = PayerService.BEAN_DEFAULT)
	private PayerService payerService;

	@Resource(name = PaymentMethodService.BEAN_DEFAULT)
	private PaymentMethodService payerAccountService;

	@Resource(name = PaySurveyService.BEAN_DEFAULT)
	private PaySurveyService paySurveyService;
	

	@Resource(name = ServicesBenefitService.BEAN_DEFAULT)
	private ServicesBenefitService servicesBenefitService;

	@Resource(name = PremCI.BEAN_DEFAULT)
	private PremCI premCI;
	
	@Resource(name = ChannelOrgService.BEAN_DEFAULT)
	private ChannelOrgService channelOrgService;

	@Resource(name = LegalRepresentativeService.BEAN_DEFAULT)
	private LegalRepresentativeService legalRepresentativeService;

	@Resource(name = EddaService.BEAN_DEFAULT)
	private EddaService eddaService;

	@Resource(name = InvestCashAccountService.BEAN_DEFAULT)
	private InvestCashAccountService investCashAccountService;
	
    @Resource(name = SettlementLetterService.BEAN_DEFAULT)
    private SettlementLetterService settlementLetterService;

	@Resource(name = TargetMaturityFundService.BEAN_DEFAULT)
	private TargetMaturityFundService targetMaturityFundService;

	@Resource(name = SharedCashAccountService.BEAN_DEFAULT)
	private SharedCashAccountService sharedCashAccountService;
	
	@Resource(name = NbAutoWithdrawService.BEAN_DEFAULT)
	private NbAutoWithdrawService nbAutoWithdrawService;
	
	//IR-407770 異動繳費方式即檢核並更新報盤銀行 接口 2020/09/17 Add by Vince Cheng
	@Resource(name = UnbPaHelper.BEAN_DEFAULT)
	private UnbPaHelper unbPaHelper;

	@Resource(name = ElderScaleRoleService.BEAN_DEFAULT)
	private ElderScaleRoleService elderScaleRoleService;

	/*  PCR_481174 遠距投保試辦被保險人境外投保   */
	@Resource(name = AgentRemoteStayService.BEAN_DEFAULT)
	private AgentRemoteStayService agentRemoteStayService;
	/*  END of PCR_481174 by Alex Chang 2022.4.8  */
	
	@Resource(name = LiaRocDownload2020CI.BEAN_DEFAULT)
	private LiaRocDownload2020CI liaRocDownload2020CI;
}