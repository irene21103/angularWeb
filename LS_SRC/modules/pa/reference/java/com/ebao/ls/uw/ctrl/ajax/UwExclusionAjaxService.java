package com.ebao.ls.uw.ctrl.ajax;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.lang.BooleanUtils;
import org.apache.log4j.Logger;

import com.ebao.foundation.module.db.DBean;
import com.ebao.foundation.module.db.hibernate.HibernateSession3;
import com.ebao.foundation.module.web.action.ExceptionInfoUtils;
import com.ebao.ls.cs.commonflow.ds.ApplicationService;
import com.ebao.ls.cs.commonflow.vo.ApplicationVO;
import com.ebao.ls.cs.pub.bo.CommonDumper;
import com.ebao.ls.letter.common.CommonLetterService;
import com.ebao.ls.liaRoc.LiarocDownloadLogCst;
import com.ebao.ls.liaRoc.ds.LiarocDownloadLogHelper;
import com.ebao.ls.pa.nb.bs.rule.ProposalRuleMsgService;
import com.ebao.ls.pa.nb.bs.rule.ProposalRuleResultService;
import com.ebao.ls.pa.nb.ci.ValidatorService;
import com.ebao.ls.pa.nb.ctrl.pub.ajax.AjaxBaseService;
import com.ebao.ls.pa.nb.util.NBUtils;
import com.ebao.ls.pa.nb.util.NumericUtils;
import com.ebao.ls.pa.nb.vo.ProposalRuleResultVO;
import com.ebao.ls.pa.pub.MsgCst;
import com.ebao.ls.pa.pub.TableCst;
import com.ebao.ls.pa.pub.bs.PolicyEndorsementService;
import com.ebao.ls.pa.pub.bs.PolicyExclusionService;
import com.ebao.ls.pa.pub.vo.CoverageVO;
import com.ebao.ls.pa.pub.vo.InsuredVO;
import com.ebao.ls.pa.pub.vo.PolicyExclusionVO;
import com.ebao.ls.pa.pub.vo.PolicyVO;
import com.ebao.ls.prd.product.pub.data.TProdVerEndorsementDelegate;
import com.ebao.ls.prd.product.vo.ProdVerEndorsementVO;
import com.ebao.ls.pub.CodeCst;
import com.ebao.ls.pub.cst.ProposalRuleMsg;
import com.ebao.ls.pub.cst.UwDocumentApprovalStatus;
import com.ebao.ls.uw.ci.UwProcessControlCI;
import com.ebao.ls.uw.ctrl.pub.UwActionHelper;
import com.ebao.ls.uw.ds.UwExclusionService;
import com.ebao.ls.uw.ds.UwIssuesLetterService;
import com.ebao.ls.uw.ds.UwPolicyService;
import com.ebao.ls.uw.util.Utils;
import com.ebao.ls.uw.vo.UwEndorsementVO;
import com.ebao.ls.uw.vo.UwExclusionVO;
import com.ebao.ls.uw.vo.UwPolicyVO;
import com.ebao.pub.event.EventService;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.framework.ExceptionFactory;
import com.ebao.pub.framework.RTException;
import com.ebao.pub.util.BeanUtils;
import com.ebao.pub.util.StringUtils;

/**
 * <p>Title: 新契約-核保作業</p>
 * <p>Description: 除外批註管理作業</p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company: TGL Co., Ltd.</p>
 * <p>Create Time: Jun 22, 2016</p> 
 * @author 
 * <p>Update Time: Jun 22, 2016</p>
 * <p>Updater: simon.huang</p>
 * <p>Update Comments: </p>
 */
public class UwExclusionAjaxService extends AjaxBaseService {

	@Resource(name = UwPolicyService.BEAN_DEFAULT)
	private UwPolicyService uwPolicyDS;

	@Resource(name = UwActionHelper.BEAN_DEFAULT)
	private UwActionHelper uwActionHelper;

	@Resource(name = PolicyExclusionService.BEAN_DEFAULT)
	private PolicyExclusionService policyExclusionService;

	@Resource(name = UwProcessControlCI.BEAN_DEFAULT)
	private UwProcessControlCI uwProcessControlCI;

	@Resource(name = PolicyEndorsementService.BEAN_DEFAULT)
	private PolicyEndorsementService policyEndorsementService;

	@Resource(name = ProposalRuleMsgService.BEAN_DEFAULT)
	private ProposalRuleMsgService proposalRuleMsgService;

	@Resource(name = ProposalRuleResultService.BEAN_DEFAULT)
	private ProposalRuleResultService proposalRuleResultService;

	@Resource(name = ValidatorService.BEAN_DEFAULT)
	private ValidatorService validatorService;

	@Resource(name = CommonLetterService.BEAN_DEFAULT)
	protected CommonLetterService commonLetterService;

	@Resource(name = EventService.BEAN_DEFAULT)
	protected EventService eventService;

	@Resource(name = UwIssuesLetterService.BEAN_DEFAULT)
	private UwIssuesLetterService uwIssuesLetterService;

	@Resource(name = UwExclusionService.BEAN_DEFAULT)
	private UwExclusionService uwExclusionService;

	@Resource(name = ApplicationService.BEAN_DEFAULT)
	private ApplicationService applicationDS;

	//private static String PRODUCT_NAME_TEMPLATE = "〇〇〇〇";

	Logger log4j = Logger.getLogger(getClass());

	/**
	 * <p>Description : 畫面初始-回傳畫面所需資料</p>
	 * <p>Created By : simon.huang</p>
	 * <p>Create Time : Jun 3, 2016</p>
	 * @param in
	 * @param output
	 * @throws Exception
	 */
	public void initEditPage(Map<String, Object> in, Map<String, Object> output) throws Exception {
		Long policyId = NumericUtils.parseLong(in.get("policyId"));
		Long underwriteId = NumericUtils.parseLong(in.get("underwriteId"));
		String reqFrom = (String) in.get("reqFrom");
		String itemIdsStr = (String) in.get("itemId");
		if (policyId != null && underwriteId != null) {

			PolicyVO policyVO = policyService.load(policyId);

			List<Map<String, Object>> insuredList = uwActionHelper.getDisplayInusredList(policyVO, in);
			List<Map<String, Object>> coverageList = uwActionHelper.getDisplayCoverageList(policyVO, in);
			List<Map<String, Object>> endorsementList = uwActionHelper.getDisplayCoverageEndorsement(policyVO, in);

			output.put("insuredList", insuredList); //被保險人列表
			output.put("coverageList", coverageList); //險種列表
			output.put("endorsementList", endorsementList);//險種保單附加條款

			Map<String, Object> codeTableMap = new HashMap<String, Object>();
			codeTableMap.put("nbGender", NBUtils.getTCodeData(TableCst.V_NB_GENDER));
			codeTableMap.put("insuredCategory", NBUtils.getTCodeData(TableCst.T_INSURED_CATEGORY));
			output.put("codeTable", codeTableMap);

			this.setDisplayUwExclusionToOutput(output, underwriteId, reqFrom, itemIdsStr);
			output.put(KEY_SUCCESS, SUCCESS);
			return;
		}
	}

	public void setDisplayUwExclusionToOutput(Map<String, Object> output, Long underwriteId, String reqFrom, String itemIdsStr) {
		//險種保單批註及附加條款
		List<UwExclusionVO> uwExclusionList = new ArrayList<UwExclusionVO>();//保單險種批註設定
		List<UwEndorsementVO> uwEndorsementList = new ArrayList<UwEndorsementVO>();//保單險種條款設定

		// RTC#157188 重新取得保全批註的underwriteId，規則與FMT_POS_0030 Letter一樣
		UwPolicyVO uwPolicyVO = uwPolicyDS.findUwPolicy(underwriteId);
		if (Utils.isCsUw(uwPolicyVO.getUwSourceType())) {
			Long policyId = uwPolicyVO.getPolicyId();
			Long changeId = uwPolicyVO.getChangeId();

			// 取得非保全項產生的underwriteId
			List<Long> underwriteIdList = uwPolicyDS.findUnderwriteIdByNotExtraPrem(changeId, policyId);
			Long[] underwriteIdAry = null;
			if (underwriteIdList != null && !underwriteIdList.isEmpty()) {
				underwriteIdAry = (Long[]) underwriteIdList.toArray(new Long[underwriteIdList.size()]);
				underwriteId = uwExclusionService.findByUnderwriteIdsMsgId(underwriteIdAry, null);
			}

		}

		if (reqFrom != null && reqFrom.equals("pos")) {
			Collection<UwExclusionVO> uwExclusionVOS = uwPolicyDS.findUwExclusionEntitis(underwriteId);
			Collection<UwEndorsementVO> uwEndorsementVOS = uwPolicyDS.findUwEndorsementEntitis(underwriteId);
			//			String[] itemIdArray = itemIdsStr.split(",");
			for (UwExclusionVO vo : uwExclusionVOS) {
				if (vo.getItemId() != null) { //R版批註 
					if (itemIdsStr.contains(vo.getItemId().toString())) {
						//					if (vo.getItemId().toString().equals(itemId)) {
						uwExclusionList.add(vo);
					}
				} else {
					//S版批註
					uwExclusionList.add(vo);
				}
			}
			for (UwEndorsementVO vo : uwEndorsementVOS) {
				if (itemIdsStr.contains(vo.getItemId().toString())) {
					//				if (vo.getItemId().toString().equals(itemId)) {
					uwEndorsementList.add(vo);
				}
			}
		} else {
			Collection<UwExclusionVO> uwExclusionVOS = uwPolicyDS.findUwExclusionEntitis(underwriteId);
			Collection<UwEndorsementVO> uwEndorsementVOS = uwPolicyDS.findUwEndorsementEntitis(underwriteId);
			for (UwExclusionVO vo : uwExclusionVOS) {
				if ("R99".equals(vo.getExclusionCode()) == false) {
					uwExclusionList.add(vo);
				}
			}
			uwEndorsementList.addAll(uwEndorsementVOS);
		}
		//Map<String, List<UwExclusionVO>> uwExclusionMapList = NBUtils.toMapList(uwExclusionList, "uwListId");
		Map<String, List<UwExclusionVO>> uwExclusionMapList = NBUtils.toMapList(uwExclusionList, "uwExclusionGroupId");
		output.put("uwExclusionMapList", uwExclusionMapList);
		output.put("uwEndorsementList", uwEndorsementList);

		//被保險人的批註單發放狀態
		Map<String, List<UwExclusionVO>> insuredPartyIdBindUwExclusion = NBUtils.toMapList(uwExclusionList, "insuredId");
		Map<String, String> insuredPartyIdDocumentStatus = new HashMap<String, String>();
		Map<String, String> insuredPartyIdApprovaltStatus = new HashMap<String, String>();
		for (String insuredPartyId : insuredPartyIdBindUwExclusion.keySet()) {
			//取第一筆的msgId及documentId
			List<UwExclusionVO> list = insuredPartyIdBindUwExclusion.get(insuredPartyId);

			insuredPartyIdDocumentStatus.put(insuredPartyId, "");
			insuredPartyIdApprovaltStatus.put(insuredPartyId, "");

			if (list.size() > 0) {
				UwExclusionVO uwExclusionVO = list.get(0);
				Long msgId = uwExclusionVO.getMsgId();
				Long docId = uwExclusionVO.getDocumentId();
				if (msgId != null && docId != null) {
					String status = uwIssuesLetterService.getMasterDocStatus(msgId);
					insuredPartyIdDocumentStatus.put(insuredPartyId, status);
				}

				if (msgId != null) {
					ProposalRuleResultVO ruleResult = proposalRuleResultService.load(msgId);
					insuredPartyIdApprovaltStatus.put(insuredPartyId, ruleResult.getUwDocumentApprovalStatus());
				}
			}
		}

		output.put("insuredDocumentStatus", insuredPartyIdDocumentStatus);
		output.put("insuredApprovalStatus", insuredPartyIdApprovaltStatus);
	}

	/**
	 * 這個method與新契約共用，需要判斷來源，避免changeId，policyChgId為空
	 * <p>Description : 批註資料新增/修改/刪除</p>
	 * <p>Created By : simon.huang</p>
	 * <p>Create Time : Jun 3, 2016</p>
	 * @param in
	 * @param output
	 * @throws Exception
	 */
	@SuppressWarnings({ "unused", "unchecked" })
	public void saveUwExclusion(Map<String, Object> in, Map<String, Object> output) throws Exception {
		Long policyId = NumericUtils.parseLong(in.get("policyId"));
		Long underwriteId = NumericUtils.parseLong(in.get("underwriteId"));
		Long changeId = NumericUtils.parseLong(in.get("changeId"));
		/* 409095 修改取值方式，避免null*/
		Long policyChgId = NumericUtils.parseLong(in.get("policyChgId"));
		String reqFrom = (String) in.get("reqFrom");
		String itemIdStr = (String) in.get("itemId");
		//判斷來源為 1.保全變更 2.新契約
		boolean isCsUw = BooleanUtils.toBoolean((Boolean) in.get("isCsUw"));
		if (policyId != null && underwriteId != null) {
			/* 404993 批註變更前都要先清除變更，避免使用者刪除又新增同一筆資料，undo時會比對不到被刪除的ID而報錯
			* 409095 1.檢核資料來源 2.檢核changeId, policyChgId不為空
			* */
			//IR411192 原IR404993，應在刪除時正確清除CX，不應在新增時清除變更。
			//			if (isCsUw && changeId != null && policyChgId != null) {
			//				CommonDumper.clearAllChanges(changeId, policyChgId);
			//				HibernateSession3.clear();
			//			}
			/* 404993 */
			org.hibernate.Session s = com.ebao.foundation.module.db.hibernate.HibernateSession3.currentSession();
			String type = (String) in.get(KEY_TYPE);

			String uwSourceType = uwPolicyDS.getUwSourceType(underwriteId);
			boolean isCS = (reqFrom != null && reqFrom.equals("pos"));
			if (UPDATE.equals(type)) {

				Map<String, Object> submitData = (Map<String, Object>) in.get(KEY_DATA);

				Long insuredId = NumericUtils.parseLong(submitData.get("insuredId"));
				Long groupId = NumericUtils.parseLong(submitData.get("groupId")); //修改時會有groupId

				List<String> itemIdList = (List<String>) submitData.get("itemId");
				List<Map<String, String>> itemIdEndorIdList = (List<Map<String, String>>) submitData.get("endorId"); // Map<String,String> like {itemId,endorId}
				List<String> itemIdBindEndorIdList = (List<String>) submitData.get("itemIdBindEndorIdList");

				List<String> exclusionCodeList = (List<String>) submitData.get("exclusionCode");
				Map<String, Object> exclusionDescMap = (Map<String, Object>) submitData.get("exclusionDesc");
				Long entityId = NumericUtils.parseLong(submitData.get(KEY_ENTITY_ID));

				Map<String, String> disabilityIndiMap = new HashMap<String, String>();

				//區分R,S版
				List<String> codeRList = filterExclusionCode("R", exclusionCodeList);
				List<String> codeSList = filterExclusionCode("S", exclusionCodeList);
				// 2017-02-25 Kate POS的P版，等於UNB的R版
				if (org.apache.commons.lang.StringUtils.equals(reqFrom, "pos") || isCsUw) {
					codeRList = filterExclusionCode("P", exclusionCodeList);
				}
				List<String> codeOList = filterExclusionCode("O", exclusionCodeList);

				//取得itemId相關資訊
				List<Long> productIdList = new ArrayList<Long>();
				List<ProdVerEndorsementVO> endorsementVOList = new ArrayList<ProdVerEndorsementVO>();

				List<UwExclusionVO> allExlusionList = new ArrayList<UwExclusionVO>();
				if (itemIdList != null && itemIdList.size() > 0) {
					for (String itemId : itemIdList) {
						CoverageVO coverageVO = coverageService.load(NumericUtils.parseLong(itemId));
						productIdList.add(coverageVO.getProductId().longValue());
						endorsementVOList.addAll(TProdVerEndorsementDelegate.findByProductId(
										coverageVO.getProductId(),
										coverageVO.getProductVersionId(),
										coverageVO.getApplyDate())
										);
					}
				}
				Map<String, ProdVerEndorsementVO> endorementVOMap = NBUtils.toMap(endorsementVOList, "endorId");

				InsuredVO insuredVO = insuredService.load(insuredId);
				Long insuredPartyId = insuredVO.getPartyId();
				Long endorsementVersion = new Long(CodeCst.ENDORSEMENT_VERSION__NB);
				if (org.apache.commons.lang.StringUtils.equals(reqFrom, "pos") || isCsUw) {
					endorsementVersion = new Long(CodeCst.ENDORSEMENT_VERSION__CS);
				}

				// 2017-05-25 [RTC#153857]Kate POS模組P,S,O版皆要勾選險種 
				// 2016-08-25 [RTC#39553]Simon 檢核S版不用勾選險種,R版需勾選險種 
				if (codeRList.size() > 0) {
					/*//一個版本 1 個 group id
					for (String exclusionCode : codeRList) {
						// 2017-05-12 Kate 不同underwriteId時，groupId重新從一開始 
						//CoverageVO coverageVO = coverageService.load(NumericUtils.parseLong(itemId));
						//String productName = policyDS.getCoverageProudctName(coverageVO);
						//不為修改時groupId為空,需產生新的groupId
						Long saveGroupId = null;
						if (groupId == null) {
							saveGroupId = uwPolicyDS.getNextUwExclusionGroupId(underwriteId).longValue();
						} else {
							saveGroupId = groupId;
						}

						//險種批註新增
						List<UwExclusionVO> subExlusionList = uwPolicyDS.syncUwExclusions(
								underwriteId, policyId, insuredPartyId, saveGroupId,
								itemIdList, productIdList,
								exclusionCode, exclusionDescMap, disabilityIndiMap);

						//寫入欄位:underwriteId,endoCode,polcyId,effectDate,expiryDate,manualIndi,itemId,version
						List<UwEndorsementVO> subEndorsementList = uwPolicyDS.syncUwEndorsement(underwriteId, policyId,
								saveGroupId,
								endorsementVersion, itemIdEndorIdList, endorementVOMap);

						allExlusionList.addAll(subExlusionList);
					}*/
					this.saveUwExclusionEndorsements(policyId, codeRList, groupId, underwriteId
									, insuredVO.getPartyId(), productIdList, exclusionDescMap, disabilityIndiMap
									, itemIdList, endorsementVersion, itemIdEndorIdList, endorementVOMap
									, allExlusionList);
				}

				if (codeSList.size() > 0) {
					if (isCS) {
						this.saveUwExclusionEndorsements(policyId, codeSList, groupId, underwriteId
										, insuredVO.getPartyId(), productIdList, exclusionDescMap, disabilityIndiMap
										, itemIdList, endorsementVersion, itemIdEndorIdList, endorementVOMap
										, allExlusionList);
					} else {
						this.saveUwExclusions(policyId, codeSList, groupId, underwriteId
										, insuredVO.getPartyId(), productIdList, exclusionDescMap, disabilityIndiMap
										, allExlusionList);
					}
				}
				if (codeOList.size() > 0 && isCS) {
					this.saveUwExclusionEndorsements(policyId, codeOList, groupId, underwriteId
									, insuredVO.getPartyId(), productIdList, exclusionDescMap, disabilityIndiMap
									, itemIdList, endorsementVersion, itemIdEndorIdList, endorementVOMap
									, allExlusionList);
				}

				//新增需查找其它相同被保險人批註的msgId/docId，並更新回此新增批註
				if (allExlusionList.size() > 0) {

					List<Map<String, Object>> resultList = uwPolicyDS.findUwExclusionMsgIdByInsuredPartyId(underwriteId, insuredPartyId);
					Long msgId = null;
					Long docId = null;

					//已存在msgId及documentId統一更新相同被保險人的批註資料為同一個msgId及documentId
					if (resultList.size() > 0) {
						Map<String, Object> msgIdAndDocIdMap = resultList.get(0);
						msgId = (Long) msgIdAndDocIdMap.get("MSG_ID");
						docId = (Long) msgIdAndDocIdMap.get("DOCUMENT_ID");
						for (UwExclusionVO uwExclusionVO : allExlusionList) {
							uwExclusionVO.setMsgId(msgId);
							uwExclusionVO.setDocumentId(docId);
						}
						//統一更新msgId及documentId
						uwPolicyDS.updateUwExclusionMsgIdDocumentId(underwriteId, insuredPartyId, msgId, docId);
					}

					//================================================
					if (isCS || msgId != null) { //1.保全
						//2.或新契約且msgId不為空時才確認 ruleReulst的簽核狀態
						updateRuleResultApprovalStatus(policyId, underwriteId, changeId, insuredVO,
										msgId, docId);
					}
					//================================================
				}

				if (isCS) {
					//IR411192 批註頁面，同時選擇XHR及XHQ，按”新增”，有錯誤訊息
					String[] itemIdAry = itemIdStr.split(",");
					for (String itemId : itemIdAry) {
						syncDataToCS(policyId, changeId, policyChgId, Long.parseLong(itemId), type);
					}
				}
				s.flush();

				this.setDisplayUwExclusionToOutput(output, underwriteId, reqFrom, itemIdStr);
				output.put(KEY_SUCCESS, SUCCESS);

				return;
			} else if (DELETE.equals(type)) {
				String deleteId = (String) in.get(KEY_DELETE_ID);
				String groupIdList[] = deleteId.split(",");

				List<UwExclusionVO> allDelUwExclusionList = new ArrayList<UwExclusionVO>();
				for (int i = 0; i < groupIdList.length; i++) {
					Long groupId = NumericUtils.parseLong(groupIdList[i]);
					if (groupId != null) {
						allDelUwExclusionList.addAll(uwPolicyDS.findUwExclusionGroupEntitis(underwriteId, groupId));
					}
				}

				//需判斷刪除的group uwExclusion的insuredPartyId,msgId,documentId是否需清除重新產生
				Map<String, List<UwExclusionVO>> partyIdBindUwExclusion = NBUtils.toMapList(allDelUwExclusionList, "insuredId");
				List<InsuredVO> insuredVOList = insuredService.findByPolicyId(policyId, false);
				Map<String, InsuredVO> partyIdBindInsuredVO = NBUtils.toMap(insuredVOList, "partyId");

				//設定被保險人MSG ID及STATUS，最後需要處理被保險人無批註資料需系統關閉未發放的核保訊息
				Map<Long, Long> insuredPartyIdBindMsgId = new HashMap<Long, Long>(); //<insuredPartyId,msgId>

				for (String partyId : partyIdBindUwExclusion.keySet()) {
					List<UwExclusionVO> uwExclusionList = partyIdBindUwExclusion.get(partyId);
					InsuredVO insuredVO = partyIdBindInsuredVO.get(partyId);

					Long insuredPartyId = new Long(partyId);

					if (uwExclusionList.size() > 0 && insuredVO != null) {
						//相同被保險人的msgId及documentId會是相同的，因此取第一筆的msgId及documentId
						UwExclusionVO uwExclusionVO = uwExclusionList.get(0);
						Long msgId = uwExclusionVO.getMsgId();
						Long docId = uwExclusionVO.getDocumentId();

						insuredPartyIdBindMsgId.put(insuredPartyId, msgId);

						//================================================
						if (msgId != null) { //1.保全  IR 277363 保全取消批註無須出
							//2.或新契約且msgId不為空時才確認 ruleReulst的簽核狀態
							updateRuleResultApprovalStatus(policyId, underwriteId, changeId, insuredVO,
											msgId, docId);
						}
						//================================================					
					}
				}

				//執行刪除
				for (int i = 0; i < groupIdList.length; i++) {
					Long groupId = NumericUtils.parseLong(groupIdList[i]);
					if (groupId != null) {
						uwPolicyDS.removeUwExclusionGroup(underwriteId, groupId);
						uwPolicyDS.removeUwEndorsementGroup(underwriteId, groupId);
						s.flush();
					}
				}

				s.flush();
				if (reqFrom != null && reqFrom.equals("pos")) {
					String[] itemIdAry = itemIdStr.split(",");
					for (String itemId : itemIdAry) {
						syncDataToCS(policyId, changeId, policyChgId, Long.parseLong(itemId), type);
					}
				}
				s.flush();

				this.setDisplayUwExclusionToOutput(output, underwriteId, reqFrom, itemIdStr);

				if (CodeCst.UW_SOURCE_TYPE__NB.equals(uwSourceType)) {

					//需判斷被保險人是否有批註資料,無批註資料需系統關閉該被保險人核保訊息
					for (InsuredVO insuredVO : insuredVOList) {
						int count = uwPolicyDS.getUWExclusionCountByInsuredPartyId(underwriteId, insuredVO.getPartyId());
						if (count == 0) {
							//找出待處理的核保訊息(FIX RTC 153221-因批註檔ALL移除後無法比對出批註檔中的msgId，但核保信息已存在，改用查詢待處理的核保訊息避免重覆新增)
							List<ProposalRuleResultVO> ruleResultList = proposalRuleResultService.findAllPendingList(
											policyId,
											ProposalRuleMsg.PROPOSAL_RULE_MSG_EXCLUSION_LETTER,
											null,
											insuredVO.getListId());

							for (ProposalRuleResultVO resultVO : ruleResultList) {
								String documentStatus = uwIssuesLetterService.getMasterDocStatus(resultVO.getListId());
								//被保險人無批註資料且未發放需系統關閉未發放的核保訊息
								if (documentStatus == null) {
									log4j.info("被保險人無批註資料系統關閉核保訊息:" + insuredVO.getCertiCode() + ",result_id=" + resultVO.getListId());
									resultVO.setRuleStatus(CodeCst.PROPOSAL_RULE_STATUS__SYSTEMCLOSED);
									proposalRuleResultService.save(resultVO);
								}
							}
						}
					}
				}

				output.put(KEY_SUCCESS, SUCCESS);
				return;
			}
		}

		output.put(KEY_SUCCESS, FAIL);
		output.put(KEY_ERROR_MSG, NBUtils.getTWMsg(MsgCst.MSG_1250566));
	}

	private void saveUwExclusionEndorsements(Long policyId, List<String> codeList, Long groupId
					, Long underwriteId, Long partyId, List<Long> productIdList
					, Map<String, Object> exclusionDescMap, Map<String, String> disabilityIndiMap
					, List<String> itemIdList, Long endorsementVersion
					, List<Map<String, String>> itemIdEndorIdList
					, Map<String, ProdVerEndorsementVO> endorementVOMap
					, List<UwExclusionVO> allExlusionList) {
		//一個版本 1 個 group id
		for (String exclusionCode : codeList) {
			// 2017-05-12 Kate 不同underwriteId時，groupId重新從一開始 
			//不為修改時groupId為空,需產生新的groupId
			Long saveGroupId = null;
			if (groupId == null) {
				saveGroupId = uwPolicyDS.getNextUwExclusionGroupId(underwriteId).longValue();
			} else {
				saveGroupId = groupId;
			}

			//險種批註新增
			List<UwExclusionVO> subExlusionList = uwPolicyDS.syncUwExclusions(
							underwriteId, policyId, partyId, saveGroupId,
							itemIdList, productIdList,
							exclusionCode, exclusionDescMap, disabilityIndiMap);
			//寫入欄位:underwriteId,endoCode,polcyId,effectDate,expiryDate,manualIndi,itemId,version
			List<UwEndorsementVO> subEndorsementList = uwPolicyDS.syncUwEndorsement(underwriteId, policyId,
							saveGroupId,
							endorsementVersion, itemIdEndorIdList, endorementVOMap);
			allExlusionList.addAll(subExlusionList);
		}

	}

	private void saveUwExclusions(Long policyId, List<String> codeSList, Long groupId
					, Long underwriteId, Long partyId, List<Long> productIdList
					, Map<String, Object> exclusionDescMap, Map<String, String> disabilityIndiMap
					, List<UwExclusionVO> allExlusionList) {
		//一個版本 1 個 group id
		for (String exclusionCode : codeSList) {
			// 2017-05-12 Kate 不同underwriteId時，groupId重新從一開始 
			//不為修改時groupId為空,需產生新的groupId
			Long saveGroupId = null;
			if (groupId == null) {
				saveGroupId = uwPolicyDS.getNextUwExclusionGroupId(underwriteId).longValue();
			} else {
				saveGroupId = groupId;
			}

			List<String> emptyItemIdList = new ArrayList<String>();
			emptyItemIdList.add(""); //S 版不需要itemId,為符合syncUwExclusions結構,帶入一個空字串的list
			//險種批註新增
			List<UwExclusionVO> subExlusionList = uwPolicyDS.syncUwExclusions(
							underwriteId, policyId, partyId, saveGroupId,
							emptyItemIdList, productIdList,
							exclusionCode, exclusionDescMap, disabilityIndiMap);

			allExlusionList.addAll(subExlusionList);

		}
	}

	/**
	 * <pre>Description : 
	 * 依「核保訊息狀態」+「照會發放狀態」+「簽核狀態」處理以下：
	 * 是否有照會事項
	 * ├–否 > 新增待簽核照會事項
	 * ├–是 > 是否已簽核
	 *  ├–否 >	(未簽核表示未發放) 更新msgId至被保險人的批註事項 
	 *  ├–是 > 是否已產生document
	 *   ├–是 > document是否已發放
	 *    ├ online > 產生新的待簽核照會事項
	 *    ├ batch  > 提示照會事項需取消，才可執行
	 *   ├否 > 更新的照會事項為待簽核
	 * </pre>
	 * <p>Created By : simon.huang</p>
	 * <p>Create Time : Mar 2, 2017</p>
	 * @param policyId
	 * @param underwriteId
	 * @param changeId
	 * @param insuredVO
	 * @param msgId
	 * @param docId
	 * @throws Exception
	 */
	private void updateRuleResultApprovalStatus(Long policyId, Long underwriteId,
					Long changeId, InsuredVO insuredVO, Long msgId, Long docId) throws Exception {

		Long insuredPartyId = insuredVO.getPartyId();

		if (log4j.isDebugEnabled()) {
			log4j.debug("policyId=" + policyId
							+ ", underwriteId=" + underwriteId
							+ ", changeId=" + changeId
							+ ", msgId=" + msgId
							+ ", docId=" + docId
							+ ", insuredPartyId=" + insuredPartyId);
		}

		ProposalRuleResultVO resultVO = null;
		if (msgId != null) {
			resultVO = proposalRuleResultService.load(msgId);
		} else {
			//找出待處理的核保訊息(FIX RTC 153221-因批註檔ALL移除後無法比對出批註檔中的msgId，但核保信息已存在，改用查詢待處理的核保訊息避免重覆新增)
			List<ProposalRuleResultVO> ruleResultList = proposalRuleResultService.findAllPendingList(
							policyId,
							ProposalRuleMsg.PROPOSAL_RULE_MSG_EXCLUSION_LETTER,
							changeId,
							insuredVO.getListId());

			if (ruleResultList.size() > 0) {
				resultVO = ruleResultList.get(0);
				msgId = resultVO.getListId();
			}
		}

		/* 無核保訊息或核保訊息不為待處理，則新增一筆新的核保訊息 */
		if (resultVO == null || resultVO.getRuleStatus() != CodeCst.PROPOSAL_RULE_STATUS__PENDING) {
			//新增核保訊息，並更新回uwExclusionVO
			resultVO = this.saveProposalRuleResultVO(policyId, underwriteId, insuredVO, changeId);
			msgId = resultVO.getListId();
			docId = null;
			//更新msgId及documentId
			uwPolicyDS.updateUwExclusionMsgIdDocumentId(underwriteId, insuredPartyId, msgId, docId);

		} else {

			if (changeId != null) {
				//更新msgId及documentId
				uwPolicyDS.updateUwExclusionMsgIdDocumentId(underwriteId, insuredPartyId, msgId, docId);
			} else {
				//新契約-需執行簽核判斷
				String approvalStatus = resultVO.getUwDocumentApprovalStatus();
				/* 待簽核,表示未發放照會不作處理  */
				if (UwDocumentApprovalStatus.APPROVAL_WAITING.equals(approvalStatus)) {
					//更新msgId及documentId
					uwPolicyDS.updateUwExclusionMsgIdDocumentId(underwriteId, insuredPartyId, msgId, docId);
				} else if (!UwDocumentApprovalStatus.APPROVAL_TRANSFER.equals(approvalStatus)) {

					//已簽核，需確認照會狀態
					String documentStatus = null;
					if (docId != null) {
						documentStatus = uwIssuesLetterService.getMasterDocStatus(msgId);

						if (CodeCst.DOCUMENT_STATUS__TO_BE_PRINTED.equals(documentStatus)
										&& UwDocumentApprovalStatus.APPROVAL_AGREE.equals(approvalStatus)) {
							//未列印照會且已簽核，不可修改，由前端檢核
							//WSLetterUnit unit = uwActionHelper.getFmtUnb0381Letter(policyId, underwriteId, insuredVO.getListId());
							//commonLetterService.updateLetterContent(docId, unit);
							throw new RTException(MsgCst.MSG_1262017);
						} else {
							if (CodeCst.DOCUMENT_STATUS__ABORTED.equals(documentStatus)
											|| CodeCst.DOCUMENT_STATUS__DELETED.equals(documentStatus)) {
								//信函已註銷，使用原核保訊息
								resultVO.setUwDocumentApprovalStatus(UwDocumentApprovalStatus.APPROVAL_WAITING);
								proposalRuleResultService.save(resultVO);
								msgId = resultVO.getListId();
								;
								docId = null;
							} else {
								//其它則新增核保訊息，並更新回uwExclusionVO
								resultVO = this.saveProposalRuleResultVO(policyId, underwriteId, insuredVO, changeId);
								msgId = resultVO.getListId();
								docId = null;
							}
						}
					}
					// docId is null=未發放照會，需將狀態更新為待簽核					
					else {
						//未發放照會，需將狀態更新為待簽核
						resultVO.setUwDocumentApprovalStatus(UwDocumentApprovalStatus.APPROVAL_WAITING);
						proposalRuleResultService.save(resultVO);
					}

					//更新msgId及documentId
					uwPolicyDS.updateUwExclusionMsgIdDocumentId(underwriteId, insuredPartyId, msgId, docId);
				}
			}

		}
	}

	/**
	 * <p>Description : 依被保險人產生核保訊息</p>
	 * <p>Created By : simon.huang</p>
	 * <p>Create Time : Aug 15, 2016</p>
	 * @param in
	 * @param output
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public void saveProposalMsg(Map<String, Object> in, Map<String, Object> output) throws Exception {
		Long policyId = NumericUtils.parseLong(in.get("policyId"));
		Long underwriteId = NumericUtils.parseLong(in.get("underwriteId"));
		Long changeId = NumericUtils.parseLong(in.get("changeId"));
		String reqFrom = (String) in.get("reqFrom");
		String itemId = (String) in.get("itemId");
		if (policyId != null && underwriteId != null) {

			List<InsuredVO> insuredVOList = insuredService.findByPolicyId(policyId, false);
			Map<String, InsuredVO> partyIdBindInsuredVO = NBUtils.toMap(insuredVOList, "partyId");

			this.setDisplayUwExclusionToOutput(output, underwriteId, reqFrom, itemId);
			//Map key是uwExclusionGroupId
			Map<String, List<UwExclusionVO>> uwExclusionMapList = (Map<String, List<UwExclusionVO>>) output.get("uwExclusionMapList");
			if (uwExclusionMapList.size() == 0) {
				//是否關閉核保訊息？？
			} else {

				//1個被保險人的批註資料會對應1筆批註單核保訊息
				Map<Long, Long> insuredPartyIdBindMsgId = new HashMap<Long, Long>(); //<insuredPartyId,msgId>
				Map<Long, Long> insuredPartyIdBindDocId = new HashMap<Long, Long>(); //<insuredPartyId,docId>
				for (List<UwExclusionVO> uwExclusionList : uwExclusionMapList.values()) {
					UwExclusionVO uwExclusionVO = uwExclusionList.get(0);
					insuredPartyIdBindMsgId.put(uwExclusionVO.getInsuredId(), uwExclusionVO.getMsgId());
					insuredPartyIdBindDocId.put(uwExclusionVO.getInsuredId(), uwExclusionVO.getDocumentId());
				}

				for (Long insuredPartyId : insuredPartyIdBindMsgId.keySet()) {
					InsuredVO insuredVO = partyIdBindInsuredVO.get(String.valueOf(insuredPartyId));

					Long msgId = insuredPartyIdBindMsgId.get(insuredPartyId);
					Long docId = insuredPartyIdBindDocId.get(insuredPartyId);

					//================================================
					updateRuleResultApprovalStatus(policyId, underwriteId, changeId, insuredVO,
									msgId, docId);
					//================================================					
				}

				//檢核BR-RMS-060規則
				try {
					HibernateSession3.detachSession();
					
					LiarocDownloadLogHelper.initNB(LiarocDownloadLogCst.NB_UW_EXCLUSION_CHECK_RULE, policyId);
					
					List<ProposalRuleResultVO> ruleResultList = proposalService.checkNBRMSRules(policyId);
					if (ruleResultList != null && ruleResultList.size() > 0) {

						StringBuffer warning = new StringBuffer();
						Set<String> descSet = new HashSet<String>();
						for (ProposalRuleResultVO ruleResult : ruleResultList) {
							if ("060_CHECK_SOMATOPSYCHIC_DISTURBANCE".equals(ruleResult.getRuleName())) {
								// 過濾相同提示訊息
								descSet.add(ruleResult.getViolatedDesc());
							}
						}
						if (!descSet.isEmpty()) {
							// 如本次批註險種包含下述險種，請留意
							warning.append(NBUtils.getTWMsg("MSG_1266011"));

							for (String violatedDesc : descSet) {
								warning.append("\n");
								warning.append(violatedDesc);
							}
							// MSG_1265674 ※提醒~請核保員自行新增照會(照會代碼RA179)
							warning.append("\n\n");							
							warning.append(NBUtils.getTWMsg("MSG_1265674"));
							output.put(KEY_ERROR_MSG, warning.toString());
							return;
						}						
					}
				} catch (Exception e) {
					output.put(KEY_ERROR_MSG, ExceptionInfoUtils.getExceptionMsg(e));
					return;
				} finally {
					try {
						LiarocDownloadLogHelper.flush();
					} catch (Exception e) {
						log4j.error(e);
					}
				}
			}

			output.put(KEY_SUCCESS, SUCCESS);
			return;
		}

		output.put(KEY_SUCCESS, FAIL);
		output.put(KEY_ERROR_MSG, NBUtils.getTWMsg(MsgCst.MSG_1250566));
	}

	/**
	 * <p>Description : 依批註版本過濾前端上傳的Exclusion Code</p>
	 * <p>Created By : simon.huang</p>
	 * <p>Create Time : Aug 5, 2016</p>
	 * @param versionCode
	 * @param exclusionCodeList
	 * @return
	 */
	private List<String> filterExclusionCode(String versionCode, List<String> exclusionCodeList) {

		List<String> versionCodeList = new ArrayList<String>();
		for (String exclusionCode : exclusionCodeList) {
			if (StringUtils.isNullOrEmpty(exclusionCode) == false) {
				if (versionCode.equals(exclusionCode.substring(0, 1))) {
					versionCodeList.add(exclusionCode);
				}
			}
		}
		return versionCodeList;
	}

	/**
	 * <p>Description : 產生"請簽回『批註書』"照會訊息</p>
	 * <p>Created By : simon.huang</p>
	 * <p>Create Time : Aug 5, 2016</p>
	 * @param policyId
	 * @param underwriteId
	 * @param insuredVO
	 * @throws Exception
	 */
	private ProposalRuleResultVO saveProposalRuleResultVO(Long policyId, Long underwriteId
					, InsuredVO insuredVO, Long changeId) throws Exception {

		ProposalRuleResultVO resultVO = null;

		Date now = AppContext.getCurrentUserLocalTime();

		List<String> params = new ArrayList<String>();
		params.add(insuredVO.getName());

		boolean isOpqV2 = validatorService.isOpqV2(policyId);
		resultVO = proposalRuleMsgService.genProposalRuleResultVO(ProposalRuleMsg.PROPOSAL_RULE_MSG_EXCLUSION_LETTER, now, params, isOpqV2);

		resultVO.setPolicyId(policyId);
		resultVO.setChangeId(changeId);
		resultVO.setRuleStatus(CodeCst.PROPOSAL_RULE_STATUS__PENDING);
		resultVO.setRuleType(CodeCst.PROPOSAL_RULE_TYPE__UW_LETTER);

		if (changeId != null) {
			//保全無需簽核
			resultVO.setUwDocumentApprovalStatus(CodeCst.APPROVAL_NO_NEED);
		} else {
			//新契約為待簽核
			resultVO.setUwDocumentApprovalStatus(CodeCst.APPROVAL_WAITING);
		}

		//update source_type/source_id
		resultVO.setSourceType(CodeCst.PROPOSAL_RULE_SOURCE_TYPE__INSURED);
		resultVO.setSourceId(insuredVO.getListId());

		proposalRuleResultService.save(resultVO);

		return resultVO;
	}

	private void syncDataToCS(Long policyId, Long changeId, Long policyChgId, Long itemId, String type) {
		//此处要单独处理R版  S版批注
		long curUserId = AppContext.getCurrentUser().getUserId();
		UwExclusionVO[] uwExclusionVOList = uwProcessControlCI.getUwExclusions(policyId, Long.valueOf(changeId));

		List<UwExclusionVO> uwExclusionVO4R = new ArrayList<UwExclusionVO>();
		List<UwExclusionVO> uwExclusionVO4S = new ArrayList<UwExclusionVO>();
		if (DELETE.equals(type)) {
			for (int i = 0; i < uwExclusionVOList.length; i++) {
				UwExclusionVO uwExclusionVO = uwExclusionVOList[i];
				if (uwExclusionVO.getItemId() != null) {
					uwExclusionVO4R.add(uwExclusionVO);
				} else {
					uwExclusionVO4S.add(uwExclusionVO);
				}
			}
		} else {
			for (int i = 0; i < uwExclusionVOList.length; i++) {
				UwExclusionVO uwExclusionVO = uwExclusionVOList[i];
				if (uwExclusionVO.getItemId() != null) {
					if (uwExclusionVO.getItemId().toString().equals(itemId.toString())) {
						uwExclusionVO4R.add(uwExclusionVO);
					}
				} else {
					uwExclusionVO4S.add(uwExclusionVO);
				}
			}
		}

		List<PolicyExclusionVO> policyExclusionCol4R = new ArrayList<PolicyExclusionVO>();
		List<PolicyExclusionVO> policyExclusionCol = new ArrayList<PolicyExclusionVO>();
		List<PolicyExclusionVO> policyExclusionCol4S = new ArrayList<PolicyExclusionVO>();
		if (DELETE.equals(type)) {
			policyExclusionCol = policyExclusionService.findByPolicyId(policyId);
			for (int i = 0; i < policyExclusionCol.size(); i++) {
				PolicyExclusionVO exclusionVO = policyExclusionCol.get(i);
				if (exclusionVO.getItemId() == null) {
					policyExclusionCol4S.add(exclusionVO);
				} else {
					policyExclusionCol4R.add(exclusionVO);
				}
			}
		} else {
			policyExclusionCol4R = policyExclusionService.findByPolicyIdAndItemId(policyId, itemId);
			policyExclusionCol = policyExclusionService.findByPolicyId(policyId);
			for (int i = 0; i < policyExclusionCol.size(); i++) {
				PolicyExclusionVO exclusionVO = policyExclusionCol.get(i);
				if (exclusionVO.getItemId() == null) {
					policyExclusionCol4S.add(exclusionVO);
				}
			}
		}

		syncData(changeId, policyChgId, curUserId, uwExclusionVO4R,
						policyExclusionCol4R);
		syncData(changeId, policyChgId, curUserId, uwExclusionVO4S,
						policyExclusionCol4S);

	}

	private void syncData(Long changeId, Long policyChgId, long curUserId,
					List<UwExclusionVO> uwExclusionVOS,
					List<PolicyExclusionVO> policyExclusionCol) {
		PolicyExclusionVO tempPolicyExclusion = null;
		// dispose update and create
		for (UwExclusionVO uwExclusionVO : uwExclusionVOS) {
			boolean isNeedToAdd = true;
			for (PolicyExclusionVO policyExclusion : policyExclusionCol) {
				// exist in all policyExclusion and uwExclusion, dump
				// policyExclusion data to log table,then update
				// policyExclusion
				
				// 若有ExclusionId就用ExclusionId比對
				if(uwExclusionVO.getExclusionId() != null){
					if (policyExclusion.getExclusionId().equals(uwExclusionVO.getExclusionId())) {
						isNeedToAdd = false; // 有找到相符資料就不新增
					}
				}else{
					// 否則用ExclusionCode+DescLang1+itemId比對
					if (policyExclusion.getExclusionCode().equals(uwExclusionVO.getExclusionCode())
									&& policyExclusion.getDescLang1().equals(uwExclusionVO.getDescLang1())
									&& (policyExclusion.getItemId() == null 
									|| (policyExclusion.getItemId() != null && policyExclusion.getItemId().toString().equals(uwExclusionVO.getItemId().toString())))) {

						// 若ExclusionCode + DescLang1 +ItemId 相符 但無ExclusionId的, 先回寫 ExclusionId
						uwExclusionVO.setExclusionId(policyExclusion.getExclusionId());
						uwExclusionVO.setExpiryDate(policyExclusion.getExpiryDate());
						applicationDS.updateUwExclusion(uwExclusionVO, policyExclusion.getExclusionId());
						
						isNeedToAdd = false; // 有找到相符資料就不新增
					}
				}
				
				// 若有相符資料, 以當前uwExclusionVO更新對應的policyExclusion
				if (!isNeedToAdd) {
					CommonDumper.srcToLogByRecord(Long.valueOf(changeId), policyChgId, "T_POLICY_EXCLUSION",
									policyExclusion.getExclusionId(), CodeCst.DATA_OPER_TYPE__DUMP_OPER_UPD,
									Long.valueOf(curUserId));
					// modified by emerson.zhang end.
					tempPolicyExclusion = voGenerator.policyExclusionNewInstance();
					BeanUtils.copyProperties(tempPolicyExclusion, uwExclusionVO);
					policyExclusionService.save(tempPolicyExclusion);

					break;
				}
			}

			// exist in uwExclusion but not exist in policyExclusion,then
			// create policyExclusion
			if (isNeedToAdd) {
				tempPolicyExclusion = voGenerator.policyExclusionNewInstance();
				BeanUtils.copyProperties(tempPolicyExclusion, uwExclusionVO);
				PolicyExclusionVO addedVo = policyExclusionService.save(tempPolicyExclusion);
				CommonDumper.srcToLogByRecord(Long.valueOf(changeId), policyChgId, "T_POLICY_EXCLUSION",
								tempPolicyExclusion.getExclusionId(), CodeCst.DATA_OPER_TYPE__DUMP_OPER_ADD,
								Long.valueOf(curUserId));
				
				// PCR_534446 更新ExclusionId回寫到T_UW_EXCLUSION去
				uwExclusionVO.setExclusionId(addedVo.getExclusionId());
				uwExclusionVO.setExpiryDate(addedVo.getExpiryDate());
				applicationDS.updateUwExclusion(uwExclusionVO, addedVo.getExclusionId());
				policyExclusionCol.add(addedVo); // 不重新讀取, 將新增的資料加入policyExclusion內
			}
		}

		// dispose delete. Only exist in policyExclusion ,not exist
		// uwExclusion,delete from policyExclusion
		// 反向policyExclusionCol對回uwExclusionVOS, 不存在uwExclusionVOS內的, 就刪除policyExclusion多餘的VO
		for (PolicyExclusionVO policyExclusion : policyExclusionCol) {
			boolean isNeedToDelete = true; 
			
			if (uwExclusionVOS == null || uwExclusionVOS.size() == 0) {
				isNeedToDelete = true;
			} else {
				for (UwExclusionVO uwExclusionVO : uwExclusionVOS) { 

					// 若有ExclusionId就用ExclusionId比對
					if (uwExclusionVO.getExclusionId() != null) {
						if (policyExclusion.getExclusionId().equals(uwExclusionVO.getExclusionId())) {
							isNeedToDelete = false; // 有找到成對uwExclusionVO不需刪除
							break;
						}
					} else {
						// 否則用ExclusionCode + DescLang1 + itemId比對
						if (policyExclusion.getExclusionCode().equals(uwExclusionVO.getExclusionCode())
										&& policyExclusion.getDescLang1().equals(uwExclusionVO.getDescLang1())
										&& (uwExclusionVO.getItemId() == null
										|| (uwExclusionVO.getItemId() != null && uwExclusionVO.getItemId().toString().equals(policyExclusion.getItemId().toString())))) {
							isNeedToDelete = false; // 有找到成對uwExclusionVO不需刪除
							break;
						}
					}
				}
			}

			if (isNeedToDelete) {
				// dump data from src table to log table bases different
				// policyChgId
				// modified by emerson.zhang, for defect CRDB00016399
				Long doExclusionId = policyExclusion.getExclusionId();
				Long preChgId = null;
				if (doExclusionId != null) {
					preChgId = getPreChgId(doExclusionId);
				}

				if (preChgId != null) {
					ApplicationVO appliaton = applicationDS.getApplication(preChgId);
					
					// 判斷新契約件
					if (appliaton.getChangeId() != null) {
						//IR411192 原IR404993，若是刪除前一次生效之批註，才須新增一筆CX
						if (appliaton.getCsAppStatus().intValue() == CodeCst.CS_APP_STATUS__IN_FORCE) {
							CommonDumper.srcToLogByRecord(Long.valueOf(changeId), policyChgId, "T_POLICY_EXCLUSION",
											policyExclusion.getExclusionId(), CodeCst.DATA_OPER_TYPE__DUMP_OPER_DEL, Long.valueOf(curUserId));
							policyExclusionService.remove(policyExclusion.getExclusionId());
						} else {
							//IR411192 原IR404993，若是刪除當次未生效之批註，應在刪除時正確清除CX，不應在新增時清除變更。
							CommonDumper.logToSrcByRecord(changeId, policyChgId, "T_POLICY_EXCLUSION", policyExclusion.getExclusionId());
						}
					} else {
						CommonDumper.srcToLogByRecord(Long.valueOf(changeId), policyChgId, "T_POLICY_EXCLUSION",
										policyExclusion.getExclusionId(), CodeCst.DATA_OPER_TYPE__DUMP_OPER_DEL, Long.valueOf(curUserId));
						policyExclusionService.remove(policyExclusion.getExclusionId());
					}
				} else {
					//IR411192 原IR404993，若是刪除當次未生效之批註，應在刪除時正確清除CX，不應在新增時清除變更。
					CommonDumper.logToSrcByRecord(changeId, policyChgId, "T_POLICY_EXCLUSION", policyExclusion.getExclusionId());
				}
				// modified by emerson.zhang end.
				//policyExclusionService.remove(policyExclusion.getExclusionId());
			}
		}

		CommonDumper.completePolicyChange(changeId, policyChgId);
	}

	/**
	 * IR411192 原IR404993，若是刪除前一次生效之批註，才須新增一筆CX
	 * @param exclusionId
	 * @return preChgId
	 */
	private Long getPreChgId(Long exclusionId) {
		Long preChgId = null;
		DBean db = null;
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer buf = new StringBuffer();
		buf.append(" SELECT TPEL.CHANGE_ID ");
		buf.append(" FROM T_POLICY_EXCLUSION_LOG TPEL  ");
		buf.append(" WHERE TPEL.EXCLUSION_ID = ? ");
		buf.append(" AND TPEL.LAST_CMT_FLG = 'Y' ");
		buf.append("");
		try {
			db = new DBean();
			db.connect();
			con = db.getConnection();
			ps = con.prepareStatement(buf.toString());
			ps.setLong(1, exclusionId);
			rs = ps.executeQuery();

			while (rs.next()) {
				preChgId = rs.getLong(1);
			}
		} catch (Exception e) {
			throw ExceptionFactory.parse(e);
		} finally {
			DBean.closeAll(rs, ps, db);
		}
		return preChgId;
	}
}
