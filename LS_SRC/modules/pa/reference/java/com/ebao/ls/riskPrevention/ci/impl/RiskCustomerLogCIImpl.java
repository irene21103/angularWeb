package com.ebao.ls.riskPrevention.ci.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import com.ebao.ls.riskPrevention.bs.RiskCustomerLogService;
import com.ebao.ls.riskPrevention.ci.RiskCustomerLogCI;
import com.ebao.ls.riskPrevention.vo.RiskCustomerLogQueryVO;
import com.ebao.ls.riskPrevention.vo.RiskCustomerLogVO;
import com.ebao.pub.util.BeanUtils;

public class RiskCustomerLogCIImpl implements RiskCustomerLogCI{
	
	@Resource(name = RiskCustomerLogService.BEAN_DEFAULT)
	RiskCustomerLogService logService;
	
	@Override
	public List<RiskCustomerLogQueryVO> queryCustomerLogsList(String certiCode) {
		List<RiskCustomerLogQueryVO> data = new ArrayList<RiskCustomerLogQueryVO>();
		List<RiskCustomerLogVO> results = logService.queryByCertiCode(certiCode);
		for(RiskCustomerLogVO result : results){
			RiskCustomerLogQueryVO vo = new RiskCustomerLogQueryVO();
			BeanUtils.copyProperties(vo, result);
			data.add(vo);
		}
		return data;
	}

}
