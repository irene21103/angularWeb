package com.ebao.ls.uw.ctrl.pub;

import com.ebao.ls.uw.vo.UwPolicyVO;
import com.ebao.pub.framework.GenericForm;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>Title: </p>  
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2004 </p>
 * <p>Company: eBaoTech Corporation </p>
 * <p>Create Time: Thu Sep 09 11:31:20 GMT+08:00 2004 </p> 
 * @author Walter Huang
 * @version 1.0
 *
 */

public class UwPolicyForm extends GenericForm {

	private java.lang.Long underwriteId = null;

	private java.lang.String uwSourceType = "";

	private java.lang.String uwStatus = "";

	private java.lang.Long policyId = null;

	private java.lang.String policyCode = "";

	private java.lang.String applyCode = "";

	private java.lang.Long deptId = null;

	private java.lang.Long changeId = null;

	private java.lang.Long caseId = null;

	private java.lang.String caseNo = "";

	private java.lang.Long proposerId = null;

	private java.util.Date insertTime = null;

	private java.lang.String assumpsit = "";

	private java.lang.Long applicantId = null;

	private java.lang.Long jobCateId = null;

	private java.lang.Integer jobClass1 = null;

	private java.lang.Integer emLife = null;

	private java.lang.Integer emIll = null;

	private java.lang.Long organId = null;

	private java.util.Date validateDate = null;

	private java.util.Date endDate = null;

	private java.util.Date underwriteTime = null;

	private java.lang.Long underwriterId = null;

	private java.util.Date applyDate = null;

	private java.lang.String uwType = "";

	private java.lang.String fullDeclare = "";

	private java.lang.String uwPending = "";

	private java.lang.Integer cancelCause = null;

	private java.lang.String linkTel = "";

	private java.lang.String generateLcaIndi = "";

	private java.lang.String lcaDateIndi = "";

	private java.util.Date lcaDate = null;

	private java.lang.Long serviceAgent = null;

	private java.lang.String submitChannel = "";

	private java.util.Date actualValidate = null;

	private java.lang.String backdatingIndi = "";

	private java.lang.String riskBackdatingIndi = "";

	private java.util.Date submissionDate = null;

	private java.lang.String initialRegComment = "";

	private java.lang.String detailRegComment = "";

	private java.lang.String proofingComment = "";
	
	private java.lang.String calloutTransComment = "";

	private java.lang.String iacGroup = "";

	private java.lang.String benefitType = "";

	private java.lang.String reUwInd = "";

	private java.lang.String uwReason = "";

	private java.lang.String consentGivenIndi = "";

	private java.lang.String amendmentComment = "";

	private java.lang.String discountType = "";

	private java.lang.Integer csTransaction = null;

	private java.lang.String uwReasonDesc = "";

	protected UwProductForm[] uwproducts;

	protected UwConditionForm[] uwconditions;

	protected UwEndorsementForm[] uwendorsements;

	protected UwExclusionForm[] uwexclusions;

	protected UwLifeInsuredForm[] uwlifeinsureds;

	private Integer proposalDecision;

	private String appealReasonDesc;

	private Date queryReceivedDate;

	private String appealIndi;

	private String deciAmenIndi;

	private String specialCommIndi;

	private String appealReason;

	private Integer gibStatus;

	private String reversalReason;

	private String magnumHoldIndi;

	private String queryReceivedStartTime;

	private String queryReceivedEndTime;

	private String scanStartDate;

	private String scanEndDate;

	private String expertDecision;

	private String countryCode = "";

	private Long operatorId = null;

	private UwPolicyVO[] ppVOs;

	private int pageSize = 20;

	private String startTime;

	private String endTime;

	private String applyStartTime;

	private String applyEndTime;

	private String agentCode;

	private String apilpDesc;

	private BigDecimal apilpMaximumLimit;

	private String ProposalOrigin = "";

	/**
	 * sales channel staff code
	 */
	private String channelStaffCode;

	/** 2015-06-25  add by Sunny for TGL */
	private String salesChannel;

	private String agentOrgan;

	private Long userId;

	private String initialType;

	private String payMode;

	private String payNext;

	private String payTogetherCode;
	
	/** 集彙/繳費折扣%*/
	private BigDecimal discntRate;

	/** 首期表定保費  各險種折扣前保費之和*/
	private BigDecimal totalStdPrem;

	/** 首期應繳保費 各險種折扣後保費及加費之和 */
	private BigDecimal totalDiscntedPrem;

	/** 首期應繳保費*/
	private BigDecimal installPrem;

	private BigDecimal suspense;

	private String moneyId;
	
	/** 連結保單 */
	private String formerPolicyCode;
	
	private String policyDecision;

	private String imageIndi;

	private String cancelDesc;

	private String hiPremIndi;

	private String ilpRefundCustIndi;

	private String reportStandLife;

	private String pendingIssueStatus;

	private String pendingIssueCause;

	private String lastReportNotes;

	private Date underwriterTime;

	private String lastUnderwriterId;

	private Date lastUnderwriterTime;

	//上次核保人員
	private String prevUnderwriterId;

	private String commentVersionId;

	private String commentType;

	private String pendingIssueDesc;

	/** 首期核印狀態*/
	private String approvalStatus;

	/** 續期核印狀態*/
	private String nextApprovalStatus;

	/** 陳核案件 (上次核保人員)*/
	private Long historyUnderwriterId;

	/** 歷史-核保特別說明  history transComment flow_id=2 */
	private String historyEscaComment;

	/** 歷史-陳核主管說明  history transComment flow_id=3 */
	private String historyEscaFeeback;

	/** 歷史-核保人員核保意見   reportNotes */
	private String reportNotes;

	/** 歷史-覆核/主管核保意見  history transComment flow_id=4,5*/
	private String historyUwComment;

	/** 是否為初始核保員-僅開啟核保員可編輯欄位  */
	private boolean isUnderwriter = false;

	/** 是否為陳核對象-僅開啟陳核可編輯欄位  */
	private boolean isEscaUser = false;

	/** 是否為覆核主管/授權額度主管-僅開啟主管可編輯欄位  */
	private boolean isManager = false;
	
	/** 是否為加費/批註簽核主管-僅開啟加費批註主管可編輯欄位  */
	private boolean isExtraPremManager = false;
	
	/** 是否為加費簽核  */
	private boolean isExtraPremApproval = false;
	/** 是否為批註簽核  */
	private boolean isExculsionApproval = false;
	
	
	/** 是否為綜合查詢頁面進入Iframe */
	private String isIframe;
	
	/** 掃描影像備註 */
	private String imageRemakeComment;
	
	/** 當來源是保全時，判斷登入人員是此案件的核保人員 */
	private boolean isCsTransOperatorId;

	/** 續保件-人工修改生效日 */
	private Date renewValidateDate ;
	
	/** 主要保人creditCode */
	private String holderCertiCode ;
	
	/* 客戶申請日 */
	private Date custApplyTime;
	
	/* eDDA約定書*/
	private String eddaBankPay;
	private String nextEddaBankPay;
	/* 透過eDDA 核印*/
	private String eddaBankAuth;
	private String nextEddaBankAuth;
	
	/** 洗錢資恐高風險客戶核保審查意見   riskReportNotes */
	private String riskReportNotes;
	
	// PCR364900-新契約補全建檔 
	private String complementTaskMemo;

    // 補全覆核特別說明
    private String complementTaskReviewMemo;

    // 批次簽署
    private Integer batchSignRange;
    private String remoteIndi;//遠距投保
	private String remoteOverseas; // 境外遠距投保
    private String remoteCallout;//錄音錄影覆核抽檢件
    private String remoteCheck;//身分驗證結果
    // 法人實質受益人辨識評估結果
    private String legalBeneEvalNotes;
    // 高齡投保件(核保高齡關懷提問)
    private boolean underwritingElderCareQuestion = false;
    private String elderCalloutFlag; //保經/保代公司依其管理規則33-1辦理作業

    /**
	 * 增額批註個案融通件
	 */
	private String insurabilityExCase;

    public String getComplementTaskReviewMemo() {
        return complementTaskReviewMemo;
    }

    public void setComplementTaskReviewMemo(String complementTaskReviewMemo) {
        this.complementTaskReviewMemo = complementTaskReviewMemo;
    }

    public String getInsurabilityExCase() {
		return insurabilityExCase;
	}

	public void setInsurabilityExCase(String insurabilityExCase) {
		this.insurabilityExCase = insurabilityExCase;
	}

	/**
     * @return 高齡投保件(核保高齡關懷提問)
     */
    public boolean isUnderwritingElderCareQuestion() {
        return underwritingElderCareQuestion;
    }

    /**
     * @param underwritingElderCareQuestion 高齡投保件(核保高齡關懷提問)
     */
    public void setUnderwritingElderCareQuestion(boolean underwritingElderCareQuestion) {
        this.underwritingElderCareQuestion = underwritingElderCareQuestion;
    }

    /**
     * @return 法人實質受益人辨識評估結果
     */
    public String getLegalBeneEvalNotes() {
        return legalBeneEvalNotes;
    }

    /**
     * @param legalBeneEvalNotes 法人實質受益人辨識評估結果
     */
    public void setLegalBeneEvalNotes(String legalBeneEvalNotes) {
        this.legalBeneEvalNotes = legalBeneEvalNotes;
    }

    /**
     * @return the batchSignRange
     */
    public Integer getBatchSignRange() {
        return batchSignRange;
    }

    /**
     * @param batchSignRange the batchSignRange to set
     */
    public void setBatchSignRange(Integer batchSignRange) {
        this.batchSignRange = batchSignRange;
    }

	/**
	 * @return 傳回 historyEscaComment。
	 */
	public String getHistoryEscaComment() {
		return historyEscaComment;
	}

	/**
	 * @param historyEscaComment 要設定的 historyEscaComment。
	 */
	public void setHistoryEscaComment(String historyEscaComment) {
		this.historyEscaComment = historyEscaComment;
	}

	/**
	 * @return 傳回 historyEscaFeeback。
	 */
	public String getHistoryEscaFeeback() {
		return historyEscaFeeback;
	}

	/**
	 * @param historyEscaFeeback 要設定的 historyEscaFeeback。
	 */
	public void setHistoryEscaFeeback(String historyEscaFeeback) {
		this.historyEscaFeeback = historyEscaFeeback;
	}

	/**
	 * @return 傳回 historyUwComment。
	 */
	public String getHistoryUwComment() {
		return historyUwComment;
	}

	/**
	 * @param historyUwComment 要設定的 historyUwComment。
	 */
	public void setHistoryUwComment(String historyUwComment) {
		this.historyUwComment = historyUwComment;
	}

	/** 主管簽核意見(Y/N)*/
	private String uwCommentIndi;

	/** 覆核/主管簽核意見(comments)*/
	private String uwComment;

	/** 是否陳核 */
	private String manuEscIndi = null;

	/** 陳核主管 */
	private Long uwEscaUser = null;

	/** 核保特別說明 (陳核原因) */
	private String proposeDesc;

	/** 陳核主管說明 (陳核主管回覆用) */
	private String checkNote;

	public String getPendingIssueDesc() {
		return pendingIssueDesc;
	}

	public void setPendingIssueDesc(String pendingIssueDesc) {
		this.pendingIssueDesc = pendingIssueDesc;
	}

	public String getPolicyDecision() {
		return policyDecision;
	}

	public void setPolicyDecision(String policyDecision) {
		this.policyDecision = policyDecision;
	}

	public String getChannelStaffCode() {
		return channelStaffCode;
	}

	public void setChannelStaffCode(String channelStaffCode) {
		this.channelStaffCode = channelStaffCode;
	}

	private Date regDate;

	public Date getRegDate() {
		return regDate;
	}

	public void setRegDate(Date regDate) {
		this.regDate = regDate;
	}

	public String getProposalOrigin() {
		return ProposalOrigin;
	}

	public void setProposalOrigin(String proposalOrigin) {
		ProposalOrigin = proposalOrigin;
	}

	public String getApilpDesc() {
		return apilpDesc;
	}

	public void setApilpDesc(String apilpDesc) {
		this.apilpDesc = apilpDesc;
	}

	public BigDecimal getApilpMaximumLimit() {
		return apilpMaximumLimit;
	}

	public void setApilpMaximumLimit(BigDecimal apilpMaximumLimit) {
		this.apilpMaximumLimit = apilpMaximumLimit;
	}

	/**
	 * @return actualValidate
	 */
	public java.util.Date getActualValidate() {
		return actualValidate;
	}

	/**
	 * @param actualValidate will be set actualValidate
	 */
	public void setActualValidate(java.util.Date actualValidate) {
		this.actualValidate = actualValidate;
	}

	/**
	 * @return amendmentComment
	 */
	public java.lang.String getAmendmentComment() {
		return amendmentComment;
	}

	/**
	 * @param amendmentComment will be set amendmentComment
	 */
	public void setAmendmentComment(java.lang.String amendmentComment) {
		this.amendmentComment = amendmentComment;
	}

	/**
	 * @return appealIndi
	 */
	public String getAppealIndi() {
		return appealIndi;
	}

	/**
	 * @param appealIndi will be set appealIndi
	 */
	public void setAppealIndi(String appealIndi) {
		this.appealIndi = appealIndi;
	}

	/**
	 * @return appealReason
	 */
	public String getAppealReason() {
		return appealReason;
	}

	/**
	 * @param appealReason will be set appealReason
	 */
	public void setAppealReason(String appealReason) {
		this.appealReason = appealReason;
	}

	/**
	 * @return appealReasonDesc
	 */
	public String getAppealReasonDesc() {
		return appealReasonDesc;
	}

	/**
	 * @param appealReasonDesc will be set appealReasonDesc
	 */
	public void setAppealReasonDesc(String appealReasonDesc) {
		this.appealReasonDesc = appealReasonDesc;
	}

	/**
	 * @return applicantId
	 */
	public java.lang.Long getApplicantId() {
		return applicantId;
	}

	/**
	 * @param applicantId will be set applicantId
	 */
	public void setApplicantId(java.lang.Long applicantId) {
		this.applicantId = applicantId;
	}

	/**
	 * @return applyCode
	 */
	public java.lang.String getApplyCode() {
		return applyCode;
	}

	/**
	 * @param applyCode will be set applyCode
	 */
	public void setApplyCode(java.lang.String applyCode) {
		this.applyCode = applyCode;
	}

	/**
	 * @return applyDate
	 */
	public java.util.Date getApplyDate() {
		return applyDate;
	}

	/**
	 * @param applyDate will be set applyDate
	 */
	public void setApplyDate(java.util.Date applyDate) {
		this.applyDate = applyDate;
	}

	/**
	 * @return applyEndTime
	 */
	public String getApplyEndTime() {
		return applyEndTime;
	}

	/**
	 * @param applyEndTime will be set applyEndTime
	 */
	public void setApplyEndTime(String applyEndTime) {
		this.applyEndTime = applyEndTime;
	}

	/**
	 * @return applyStartTime
	 */
	public String getApplyStartTime() {
		return applyStartTime;
	}

	/**
	 * @param applyStartTime will be set applyStartTime
	 */
	public void setApplyStartTime(String applyStartTime) {
		this.applyStartTime = applyStartTime;
	}

	/**
	 * @return assumpsit
	 */
	public java.lang.String getAssumpsit() {
		return assumpsit;
	}

	/**
	 * @param assumpsit will be set assumpsit
	 */
	public void setAssumpsit(java.lang.String assumpsit) {
		this.assumpsit = assumpsit;
	}

	/**
	 * @return backdatingIndi
	 */
	public java.lang.String getBackdatingIndi() {
		return backdatingIndi;
	}

	/**
	 * @param backdatingIndi will be set backdatingIndi
	 */
	public void setBackdatingIndi(java.lang.String backdatingIndi) {
		this.backdatingIndi = backdatingIndi;
	}

	/**
	 * @return benefitType
	 */
	public java.lang.String getBenefitType() {
		return benefitType;
	}

	/**
	 * @param benefitType will be set benefitType
	 */
	public void setBenefitType(java.lang.String benefitType) {
		this.benefitType = benefitType;
	}

	/**
	 * @return cancelCause
	 */
	public java.lang.Integer getCancelCause() {
		return cancelCause;
	}

	/**
	 * @param cancelCause will be set cancelCause
	 */
	public void setCancelCause(java.lang.Integer cancelCause) {
		this.cancelCause = cancelCause;
	}

	/**
	 * @return caseId
	 */
	public java.lang.Long getCaseId() {
		return caseId;
	}

	/**
	 * @param caseId will be set caseId
	 */
	public void setCaseId(java.lang.Long caseId) {
		this.caseId = caseId;
	}

	/**
	 * @return caseNo
	 */
	public java.lang.String getCaseNo() {
		return caseNo;
	}

	/**
	 * @param caseNo will be set caseNo
	 */
	public void setCaseNo(java.lang.String caseNo) {
		this.caseNo = caseNo;
	}

	/**
	 * @return changeId
	 */
	public java.lang.Long getChangeId() {
		return changeId;
	}

	/**
	 * @param changeId will be set changeId
	 */
	public void setChangeId(java.lang.Long changeId) {
		this.changeId = changeId;
	}

	/**
	 * @return checkNote
	 */
	public java.lang.String getCheckNote() {
		return checkNote;
	}

	/**
	 * @param checkNote will be set checkNote
	 */
	public void setCheckNote(java.lang.String checkNote) {
		this.checkNote = checkNote;
	}

	/**
	 * @return consentGivenIndi
	 */
	public java.lang.String getConsentGivenIndi() {
		return consentGivenIndi;
	}

	/**
	 * @param consentGivenIndi will be set consentGivenIndi
	 */
	public void setConsentGivenIndi(java.lang.String consentGivenIndi) {
		this.consentGivenIndi = consentGivenIndi;
	}

	/**
	 * @return countryCode
	 */
	public String getCountryCode() {
		return countryCode;
	}

	/**
	 * @param countryCode will be set countryCode
	 */
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	/**
	 * @return csTransaction
	 */
	public java.lang.Integer getCsTransaction() {
		return csTransaction;
	}

	/**
	 * @param csTransaction will be set csTransaction
	 */
	public void setCsTransaction(java.lang.Integer csTransaction) {
		this.csTransaction = csTransaction;
	}

	/**
	 * @return deciAmenIndi
	 */
	public String getDeciAmenIndi() {
		return deciAmenIndi;
	}

	/**
	 * @param deciAmenIndi will be set deciAmenIndi
	 */
	public void setDeciAmenIndi(String deciAmenIndi) {
		this.deciAmenIndi = deciAmenIndi;
	}

	/**
	 * @return deptId
	 */
	public java.lang.Long getDeptId() {
		return deptId;
	}

	/**
	 * @param deptId will be set deptId
	 */
	public void setDeptId(java.lang.Long deptId) {
		this.deptId = deptId;
	}

	/**
	 * @return detailRegComment
	 */
	public java.lang.String getDetailRegComment() {
		return detailRegComment;
	}

	/**
	 * @param detailRegComment will be set detailRegComment
	 */
	public void setDetailRegComment(java.lang.String detailRegComment) {
		this.detailRegComment = detailRegComment;
	}

	/**
	 * @return discountType
	 */
	public java.lang.String getDiscountType() {
		return discountType;
	}

	/**
	 * @param discountType will be set discountType
	 */
	public void setDiscountType(java.lang.String discountType) {
		this.discountType = discountType;
	}

	/**
	 * @return emIll
	 */
	public java.lang.Integer getEmIll() {
		return emIll;
	}

	/**
	 * @param emIll will be set emIll
	 */
	public void setEmIll(java.lang.Integer emIll) {
		this.emIll = emIll;
	}

	/**
	 * @return emLife
	 */
	public java.lang.Integer getEmLife() {
		return emLife;
	}

	/**
	 * @param emLife will be set emLife
	 */
	public void setEmLife(java.lang.Integer emLife) {
		this.emLife = emLife;
	}

	/**
	 * @return endDate
	 */
	public java.util.Date getEndDate() {
		return endDate;
	}

	/**
	 * @param endDate will be set endDate
	 */
	public void setEndDate(java.util.Date endDate) {
		this.endDate = endDate;
	}

	/**
	 * @return endTime
	 */
	public String getEndTime() {
		return endTime;
	}

	/**
	 * @param endTime will be set endTime
	 */
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	/**
	 * @return expertDecision
	 */
	public String getExpertDecision() {
		return expertDecision;
	}

	/**
	 * @param expertDecision will be set expertDecision
	 */
	public void setExpertDecision(String expertDecision) {
		this.expertDecision = expertDecision;
	}

	/**
	 * @return fullDeclare
	 */
	public java.lang.String getFullDeclare() {
		return fullDeclare;
	}

	/**
	 * @param fullDeclare will be set fullDeclare
	 */
	public void setFullDeclare(java.lang.String fullDeclare) {
		this.fullDeclare = fullDeclare;
	}

	/**
	 * @return generateLcaIndi
	 */
	public java.lang.String getGenerateLcaIndi() {
		return generateLcaIndi;
	}

	/**
	 * @param generateLcaIndi will be set generateLcaIndi
	 */
	public void setGenerateLcaIndi(java.lang.String generateLcaIndi) {
		this.generateLcaIndi = generateLcaIndi;
	}

	/**
	 * @return gibStatus
	 */
	public Integer getGibStatus() {
		return gibStatus;
	}

	/**
	 * @param gibStatus will be set gibStatus
	 */
	public void setGibStatus(Integer gibStatus) {
		this.gibStatus = gibStatus;
	}

	/**
	 * @return iacGroup
	 */
	public java.lang.String getIacGroup() {
		return iacGroup;
	}

	/**
	 * @param iacGroup will be set iacGroup
	 */
	public void setIacGroup(java.lang.String iacGroup) {
		this.iacGroup = iacGroup;
	}

	/**
	 * @return initialRegComment
	 */
	public java.lang.String getInitialRegComment() {
		return initialRegComment;
	}

	/**
	 * @param initialRegComment will be set initialRegComment
	 */
	public void setInitialRegComment(java.lang.String initialRegComment) {
		this.initialRegComment = initialRegComment;
	}

	/**
	 * @return insertTime
	 */
	public java.util.Date getInsertTime() {
		return insertTime;
	}

	/**
	 * @param insertTime will be set insertTime
	 */
	public void setInsertTime(java.util.Date insertTime) {
		this.insertTime = insertTime;
	}

	/**
	 * @return jobCateId
	 */
	public java.lang.Long getJobCateId() {
		return jobCateId;
	}

	/**
	 * @param jobCateId will be set jobCateId
	 */
	public void setJobCateId(java.lang.Long jobCateId) {
		this.jobCateId = jobCateId;
	}

	/**
	 * @return jobClass1
	 */
	public java.lang.Integer getJobClass1() {
		return jobClass1;
	}

	/**
	 * @param jobClass1 will be set jobClass1
	 */
	public void setJobClass1(java.lang.Integer jobClass1) {
		this.jobClass1 = jobClass1;
	}

	/**
	 * @return lcaDate
	 */
	public java.util.Date getLcaDate() {
		return lcaDate;
	}

	/**
	 * @param lcaDate will be set lcaDate
	 */
	public void setLcaDate(java.util.Date lcaDate) {
		this.lcaDate = lcaDate;
	}

	/**
	 * @return lcaDateIndi
	 */
	public java.lang.String getLcaDateIndi() {
		return lcaDateIndi;
	}

	/**
	 * @param lcaDateIndi will be set lcaDateIndi
	 */
	public void setLcaDateIndi(java.lang.String lcaDateIndi) {
		this.lcaDateIndi = lcaDateIndi;
	}

	/**
	 * @return linkTel
	 */
	public java.lang.String getLinkTel() {
		return linkTel;
	}

	/**
	 * @param linkTel will be set linkTel
	 */
	public void setLinkTel(java.lang.String linkTel) {
		this.linkTel = linkTel;
	}

	/**
	 * @return magnumHoldIndi
	 */
	public String getMagnumHoldIndi() {
		return magnumHoldIndi;
	}

	/**
	 * @param magnumHoldIndi will be set magnumHoldIndi
	 */
	public void setMagnumHoldIndi(String magnumHoldIndi) {
		this.magnumHoldIndi = magnumHoldIndi;
	}

	/**
	 * @return manuEscIndi
	 */
	public String getManuEscIndi() {
		return manuEscIndi;
	}

	/**
	 * @param manuEscIndi will be set manuEscIndi
	 */
	public void setManuEscIndi(String manuEscIndi) {
		this.manuEscIndi = manuEscIndi;
	}

	/**
	 * @return operatorId
	 */
	public Long getOperatorId() {
		return operatorId;
	}

	/**
	 * @param operatorId will be set operatorId
	 */
	public void setOperatorId(Long operatorId) {
		this.operatorId = operatorId;
	}

	/**
	 * @return organId
	 */
	public java.lang.Long getOrganId() {
		return organId;
	}

	/**
	 * @param organId will be set organId
	 */
	public void setOrganId(java.lang.Long organId) {
		this.organId = organId;
	}

	/**
	 * @return pageSize
	 */
	public int getPageSize() {
		return pageSize;
	}

	/**
	 * @param pageSize will be set pageSize
	 */
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	/**
	 * @return policyCode
	 */
	public java.lang.String getPolicyCode() {
		return policyCode;
	}

	/**
	 * @param policyCode will be set policyCode
	 */
	public void setPolicyCode(java.lang.String policyCode) {
		this.policyCode = policyCode;
	}

	/**
	 * @return policyId
	 */
	public java.lang.Long getPolicyId() {
		return policyId;
	}

	/**
	 * @param policyId will be set policyId
	 */
	public void setPolicyId(java.lang.Long policyId) {
		this.policyId = policyId;
	}

	/**
	 * @return ppVOs
	 */
	public UwPolicyVO[] getPpVOs() {
		return ppVOs;
	}

	/**
	 * @param ppVOs will be set ppVOs
	 */
	public void setPpVOs(UwPolicyVO[] ppVOs) {
		this.ppVOs = ppVOs;
	}

	/**
	 * @return proofingComment
	 */
	public java.lang.String getProofingComment() {
		return proofingComment;
	}

	/**
	 * @param proofingComment will be set proofingComment
	 */
	public void setProofingComment(java.lang.String proofingComment) {
		this.proofingComment = proofingComment;
	}

	public java.lang.String getCalloutTransComment() {
		return calloutTransComment;
	}

	public void setCalloutTransComment(java.lang.String calloutTransComment) {
		this.calloutTransComment = calloutTransComment;
	}

	/**
	 * @return proposalDecision
	 */
	public Integer getProposalDecision() {
		return proposalDecision;
	}

	/**
	 * @param proposalDecision will be set proposalDecision
	 */
	public void setProposalDecision(Integer proposalDecision) {
		this.proposalDecision = proposalDecision;
	}

	/**
	 * @return proposeDesc
	 */
	public java.lang.String getProposeDesc() {
		return proposeDesc;
	}

	/**
	 * @param proposeDesc will be set proposeDesc
	 */
	public void setProposeDesc(java.lang.String proposeDesc) {
		this.proposeDesc = proposeDesc;
	}

	/**
	 * @return proposerId
	 */
	public java.lang.Long getProposerId() {
		return proposerId;
	}

	/**
	 * @param proposerId will be set proposerId
	 */
	public void setProposerId(java.lang.Long proposerId) {
		this.proposerId = proposerId;
	}

	/**
	 * @return queryReceivedDate
	 */
	public Date getQueryReceivedDate() {
		return queryReceivedDate;
	}

	/**
	 * @param queryReceivedDate will be set queryReceivedDate
	 */
	public void setQueryReceivedDate(Date queryReceivedDate) {
		this.queryReceivedDate = queryReceivedDate;
	}

	/**
	 * @return queryReceivedEndTime
	 */
	public String getQueryReceivedEndTime() {
		return queryReceivedEndTime;
	}

	/**
	 * @param queryReceivedEndTime will be set queryReceivedEndTime
	 */
	public void setQueryReceivedEndTime(String queryReceivedEndTime) {
		this.queryReceivedEndTime = queryReceivedEndTime;
	}

	/**
	 * @return queryReceivedStartTime
	 */
	public String getQueryReceivedStartTime() {
		return queryReceivedStartTime;
	}

	/**
	 * @param queryReceivedStartTime will be set queryReceivedStartTime
	 */
	public void setQueryReceivedStartTime(String queryReceivedStartTime) {
		this.queryReceivedStartTime = queryReceivedStartTime;
	}

	/**
	 * @return reUwInd
	 */
	public java.lang.String getReUwInd() {
		return reUwInd;
	}

	/**
	 * @param reUwInd will be set reUwInd
	 */
	public void setReUwInd(java.lang.String reUwInd) {
		this.reUwInd = reUwInd;
	}

	/**
	 * @return reversalReason
	 */
	public String getReversalReason() {
		return reversalReason;
	}

	/**
	 * @param reversalReason will be set reversalReason
	 */
	public void setReversalReason(String reversalReason) {
		this.reversalReason = reversalReason;
	}

	/**
	 * @return riskBackdatingIndi
	 */
	public java.lang.String getRiskBackdatingIndi() {
		return riskBackdatingIndi;
	}

	/**
	 * @param riskBackdatingIndi will be set riskBackdatingIndi
	 */
	public void setRiskBackdatingIndi(java.lang.String riskBackdatingIndi) {
		this.riskBackdatingIndi = riskBackdatingIndi;
	}

	/**
	 * @return scanEndDate
	 */
	public String getScanEndDate() {
		return scanEndDate;
	}

	/**
	 * @param scanEndDate will be set scanEndDate
	 */
	public void setScanEndDate(String scanEndDate) {
		this.scanEndDate = scanEndDate;
	}

	/**
	 * @return scanStartDate
	 */
	public String getScanStartDate() {
		return scanStartDate;
	}

	/**
	 * @param scanStartDate will be set scanStartDate
	 */
	public void setScanStartDate(String scanStartDate) {
		this.scanStartDate = scanStartDate;
	}

	/**
	 * @return serviceAgent
	 */
	public java.lang.Long getServiceAgent() {
		return serviceAgent;
	}

	/**
	 * @param serviceAgent will be set serviceAgent
	 */
	public void setServiceAgent(java.lang.Long serviceAgent) {
		this.serviceAgent = serviceAgent;
	}

	/**
	 * @return specialCommIndi
	 */
	public String getSpecialCommIndi() {
		return specialCommIndi;
	}

	/**
	 * @param specialCommIndi will be set specialCommIndi
	 */
	public void setSpecialCommIndi(String specialCommIndi) {
		this.specialCommIndi = specialCommIndi;
	}

	/**
	 * @return startTime
	 */
	public String getStartTime() {
		return startTime;
	}

	/**
	 * @param startTime will be set startTime
	 */
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	/**
	 * @return submissionDate
	 */
	public java.util.Date getSubmissionDate() {
		return submissionDate;
	}

	/**
	 * @param submissionDate will be set submissionDate
	 */
	public void setSubmissionDate(java.util.Date submissionDate) {
		this.submissionDate = submissionDate;
	}

	/**
	 * @return submitChannel
	 */
	public java.lang.String getSubmitChannel() {
		return submitChannel;
	}

	/**
	 * @param submitChannel will be set submitChannel
	 */
	public void setSubmitChannel(java.lang.String submitChannel) {
		this.submitChannel = submitChannel;
	}

	/**
	 * @return underwriteId
	 */
	public java.lang.Long getUnderwriteId() {
		return underwriteId;
	}

	/**
	 * @param underwriteId will be set underwriteId
	 */
	public void setUnderwriteId(java.lang.Long underwriteId) {
		this.underwriteId = underwriteId;
	}

	/**
	 * @return underwriterId
	 */
	public java.lang.Long getUnderwriterId() {
		return underwriterId;
	}

	/**
	 * @param underwriterId will be set underwriterId
	 */
	public void setUnderwriterId(java.lang.Long underwriterId) {
		this.underwriterId = underwriterId;
	}

	/**
	 * @return underwriteTime
	 */
	public java.util.Date getUnderwriteTime() {
		return underwriteTime;
	}

	/**
	 * @param underwriteTime will be set underwriteTime
	 */
	public void setUnderwriteTime(java.util.Date underwriteTime) {
		this.underwriteTime = underwriteTime;
	}

	/**
	 * @return uwComment
	 */
	public String getUwComment() {
		return uwComment;
	}

	/**
	 * @param uwComment will be set uwComment
	 */
	public void setUwComment(String uwComment) {
		this.uwComment = uwComment;
	}

	/**
	 * @return uwconditions
	 */
	public UwConditionForm[] getUwconditions() {
		return uwconditions;
	}

	/**
	 * @param uwconditions will be set uwconditions
	 */
	public void setUwconditions(UwConditionForm[] uwconditions) {
		this.uwconditions = uwconditions;
	}

	/**
	 * @return uwendorsements
	 */
	public UwEndorsementForm[] getUwendorsements() {
		return uwendorsements;
	}

	/**
	 * @param uwendorsements will be set uwendorsements
	 */
	public void setUwendorsements(UwEndorsementForm[] uwendorsements) {
		this.uwendorsements = uwendorsements;
	}

	/**
	 * @return uwEscaUser
	 */
	public Long getUwEscaUser() {
		return uwEscaUser;
	}

	/**
	 * @param uwEscaUser will be set uwEscaUser
	 */
	public void setUwEscaUser(Long uwEscaUser) {
		this.uwEscaUser = uwEscaUser;
	}

	/**
	 * @return uwexclusions
	 */
	public UwExclusionForm[] getUwexclusions() {
		return uwexclusions;
	}

	/**
	 * @param uwexclusions will be set uwexclusions
	 */
	public void setUwexclusions(UwExclusionForm[] uwexclusions) {
		this.uwexclusions = uwexclusions;
	}

	/**
	 * @return uwlifeinsureds
	 */
	public UwLifeInsuredForm[] getUwlifeinsureds() {
		return uwlifeinsureds;
	}

	/**
	 * @param uwlifeinsureds will be set uwlifeinsureds
	 */
	public void setUwlifeinsureds(UwLifeInsuredForm[] uwlifeinsureds) {
		this.uwlifeinsureds = uwlifeinsureds;
	}

	/**
	 * @return uwPending
	 */
	public java.lang.String getUwPending() {
		return uwPending;
	}

	/**
	 * @param uwPending will be set uwPending
	 */
	public void setUwPending(java.lang.String uwPending) {
		this.uwPending = uwPending;
	}

	/**
	 * @return uwproducts
	 */
	public UwProductForm[] getUwproducts() {
		return uwproducts;
	}

	/**
	 * @param uwproducts will be set uwproducts
	 */
	public void setUwproducts(UwProductForm[] uwproducts) {
		this.uwproducts = uwproducts;
	}

	/**
	 * @return uwReason
	 */
	public java.lang.String getUwReason() {
		return uwReason;
	}

	/**
	 * @param uwReason will be set uwReason
	 */
	public void setUwReason(java.lang.String uwReason) {
		this.uwReason = uwReason;
	}

	/**
	 * @return uwReasonDesc
	 */
	public java.lang.String getUwReasonDesc() {
		return uwReasonDesc;
	}

	/**
	 * @param uwReasonDesc will be set uwReasonDesc
	 */
	public void setUwReasonDesc(java.lang.String uwReasonDesc) {
		this.uwReasonDesc = uwReasonDesc;
	}

	/**
	 * @return uwSourceType
	 */
	public java.lang.String getUwSourceType() {
		return uwSourceType;
	}

	/**
	 * @param uwSourceType will be set uwSourceType
	 */
	public void setUwSourceType(java.lang.String uwSourceType) {
		this.uwSourceType = uwSourceType;
	}

	/**
	 * @return uwStatus
	 */
	public java.lang.String getUwStatus() {
		return uwStatus;
	}

	/**
	 * @param uwStatus will be set uwStatus
	 */
	public void setUwStatus(java.lang.String uwStatus) {
		this.uwStatus = uwStatus;
	}

	/**
	 * @return agentCode
	 */
	public java.lang.String getAgentCode() {
		return agentCode;
	}

	/**
	 * @param uwType will be set agentCode
	 */
	public void setAgentCode(java.lang.String agentCode) {
		this.agentCode = agentCode;
	}

	/**
	 * @return uwType
	 */
	public java.lang.String getUwType() {
		return uwType;
	}

	/**
	 * @param uwType will be set uwType
	 */
	public void setUwType(java.lang.String uwType) {
		this.uwType = uwType;
	}

	/**
	 * @return validateDate
	 */
	public java.util.Date getValidateDate() {
		return validateDate;
	}

	/**
	 * @param validateDate will be set validateDate
	 */
	public void setValidateDate(java.util.Date validateDate) {
		this.validateDate = validateDate;
	}

	public String getSalesChannel() {
		return salesChannel;
	}

	public void setSalesChannel(String salesChannel) {
		this.salesChannel = salesChannel;
	}

	public String getAgentOrgan() {
		return agentOrgan;
	}

	public void setAgentOrgan(String agentOrgan) {
		this.agentOrgan = agentOrgan;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getInitialType() {
		return initialType;
	}

	public void setInitialType(String initialType) {
		this.initialType = initialType;
	}

	public String getPayMode() {
		return payMode;
	}

	public void setPayMode(String payMode) {
		this.payMode = payMode;
	}

	public String getPayNext() {
		return payNext;
	}

	public void setPayNext(String payNext) {
		this.payNext = payNext;
	}

	public String getPayTogetherCode() {
		return payTogetherCode;
	}

	public void setPayTogetherCode(String payTogetherCode) {
		this.payTogetherCode = payTogetherCode;
	}

	public BigDecimal getDiscntRate() {
		return discntRate;
	}

	public void setDiscntRate(BigDecimal discntRate) {
		this.discntRate = discntRate;
	}

	public BigDecimal getSuspense() {
		return suspense;
	}

	public void setSuspense(BigDecimal suspense) {
		this.suspense = suspense;
	}

	public String getMoneyId() {
		return moneyId;
	}

	public void setMoneyId(String moneyId) {
		this.moneyId = moneyId;
	}

	public String getImageIndi() {
		return imageIndi;
	}

	public void setImageIndi(String imageIndi) {
		this.imageIndi = imageIndi;
	}

	public String getHiPremIndi() {
		return hiPremIndi;
	}

	public void setHiPremIndi(String hiPremIndi) {
		this.hiPremIndi = hiPremIndi;
	}

	public String getIlpRefundCustIndi() {
		return ilpRefundCustIndi;
	}

	public void setIlpRefundCustIndi(String ilpRefundCustIndi) {
		this.ilpRefundCustIndi = ilpRefundCustIndi;
	}

	public String getReportStatusLife() {
		return reportStandLife;
	}

	public void setReportStatusLife(String reportStatusLife) {
		this.reportStandLife = reportStatusLife;
	}

	public String getPendingIssueStatus() {
		return pendingIssueStatus;
	}

	public void setPendingIssueStatus(String pendingIssueStatus) {
		this.pendingIssueStatus = pendingIssueStatus;
	}

	public String getPendingIssueCause() {
		return pendingIssueCause;
	}

	public void setPendingIssueCause(String pendingIssueCause) {
		this.pendingIssueCause = pendingIssueCause;
	}

	public String getReportNotes() {
		return reportNotes;
	}

	public void setReportNotes(String reportNotes) {
		this.reportNotes = reportNotes;
	}

	public String getLastReportNotes() {
		return lastReportNotes;
	}

	public void setLastReportNotes(String lastReportNotes) {
		this.lastReportNotes = lastReportNotes;
	}

	public Date getLastUnderwriterTime() {
		return lastUnderwriterTime;
	}

	public void setLastUnderwriterTime(Date lastUnderwriterTime) {
		this.lastUnderwriterTime = lastUnderwriterTime;
	}

	public String getCommentVersionId() {
		return commentVersionId;
	}

	public void setCommentVersionId(String commentVersionId) {
		this.commentVersionId = commentVersionId;
	}

	public String getCommentType() {
		return commentType;
	}

	public void setCommentType(String commentType) {
		this.commentType = commentType;
	}

	public String getReportStandLife() {
		return reportStandLife;
	}

	public void setReportStandLife(String reportStandLife) {
		this.reportStandLife = reportStandLife;
	}

	public Date getUnderwriterTime() {
		return underwriterTime;
	}

	public void setUnderwriterTime(Date underwriterTime) {
		this.underwriterTime = underwriterTime;
	}

	public String getLastUnderwriterId() {
		return lastUnderwriterId;
	}

	public void setLastUnderwriterId(String lastUnderwriterId) {
		this.lastUnderwriterId = lastUnderwriterId;
	}

	public String getCancelDesc() {
		return cancelDesc;
	}

	public void setCancelDesc(String cancelDesc) {
		this.cancelDesc = cancelDesc;
	}

	/**
	 * @return 傳回 totalStdPrem。
	 */
	public BigDecimal getTotalStdPrem() {
		return totalStdPrem;
	}

	/**
	 * @param totalStdPrem 要設定的 totalStdPrem。
	 */
	public void setTotalStdPrem(BigDecimal totalStdPrem) {
		this.totalStdPrem = totalStdPrem;
	}

	/**
	 * @return 傳回 totalDiscntedPrem。
	 */
	public BigDecimal getTotalDiscntedPrem() {
		return totalDiscntedPrem;
	}

	/**
	 * @param totalDiscntedPrem 要設定的 totalDiscntedPrem。
	 */
	public void setTotalDiscntedPrem(BigDecimal totalDiscntedPrem) {
		this.totalDiscntedPrem = totalDiscntedPrem;
	}

	public BigDecimal getInstallPrem() {
		return installPrem;
	}

	public void setInstallPrem(BigDecimal installPrem) {
		this.installPrem = installPrem;
	}

	public String getApprovalStatus() {
		return approvalStatus;
	}

	public void setApprovalStatus(String approvalStatus) {
		this.approvalStatus = approvalStatus;
	}

	public String getNextApprovalStatus() {
		return nextApprovalStatus;
	}

	public void setNextApprovalStatus(String nextApprovalStatus) {
		this.nextApprovalStatus = nextApprovalStatus;
	}

	public String getUwCommentIndi() {
		return uwCommentIndi;
	}

	public void setUwCommentIndi(String uwCommentIndi) {
		this.uwCommentIndi = uwCommentIndi;
	}

	/**
	 * @return 傳回 historyUnderwriterId。
	 */
	public Long getHistoryUnderwriterId() {
		return historyUnderwriterId;
	}

	/**
	 * @param historyUnderwriterId 要設定的 historyUnderwriterId。
	 */
	public void setHistoryUnderwriterId(Long historyUnderwriterId) {
		this.historyUnderwriterId = historyUnderwriterId;
	}

	public String getIsIframe() {
		return isIframe;
	}

	public void setIsIframe(String isIframe) {
		this.isIframe = isIframe;
	}

	/**
	 * @return 傳回 isUnderwriter。
	 */
	public boolean getIsUnderwriter() {
		return isUnderwriter;
	}

	/**
	 * @param isUnderwriter 要設定的 isUnderwriter。
	 */
	public void setIsUnderwriter(boolean isUnderwriter) {
		this.isUnderwriter = isUnderwriter;
	}

	/**
	 * @return 傳回 isEscaUser。
	 */
	public boolean getIsEscaUser() {
		return isEscaUser;
	}

	/**
	 * @param isEscaUser 要設定的 isEscaUser。
	 */
	public void setIsEscaUser(boolean isEscaUser) {
		this.isEscaUser = isEscaUser;
	}

	/**
	 * @return 傳回 isManager。
	 */
	public boolean getIsManager() {
		return isManager;
	}

	/**
	 * @param isManager 要設定的 isManager。
	 */
	public void setIsManager(boolean isManager) {
		this.isManager = isManager;
	}

	/**
	 * @return 傳回 isExtraPremManager。
	 */
	public boolean getIsExtraPremManager() {
		return isExtraPremManager;
	}

	/**
	 * @param isExtraPremManager 要設定的 isExtraPremManager。
	 */
	public void setIsExtraPremManager(boolean isExtraPremManager) {
		this.isExtraPremManager = isExtraPremManager;
	}

	public String getImageRemakeComment() {
		return imageRemakeComment;
	}

	public void setImageRemakeComment(String imageRemakeComment) {
		this.imageRemakeComment = imageRemakeComment;
	}

	/**
	 * @return 傳回 prevUnderwriterId。
	 */
	public String getPrevUnderwriterId() {
		return prevUnderwriterId;
	}

	/**
	 * @param prevUnderwriterId 要設定的 prevUnderwriterId。
	 */
	public void setPrevUnderwriterId(String prevUnderwriterId) {
		this.prevUnderwriterId = prevUnderwriterId;
	}

	/**
	 * <p>Description :當來源是保全時，判斷登入人員是此案件的核保人員 </p>
	 * <p>Created By : TGL144</p>
	 * <p>Create Time : Nov 4, 2016</p>
	 * @return
	 */
	public boolean isCsTransOperatorId() {
		return isCsTransOperatorId;
	}

	/**
	 * <p>Description :當來源是保全時，判斷登入人員是此案件的核保人員 </p>
	 * <p>Created By : TGL144</p>
	 * <p>Create Time : Nov 4, 2016</p>
	 * @param isCsTransOperatorId
	 */
	public void setCsTransOperatorId(boolean isCsTransOperatorId) {
		this.isCsTransOperatorId = isCsTransOperatorId;
	}

	/**
	 * @return 傳回 renewValidateDate。
	 */
	public Date getRenewValidateDate() {
		return renewValidateDate;
	}

	/**
	 * @param renewValidateDate 要設定的 renewValidateDate。
	 */
	public void setRenewValidateDate(Date renewValidateDate) {
		this.renewValidateDate = renewValidateDate;
	}

	public String getHolderCertiCode() {
		return holderCertiCode;
	}

	public void setHolderCertiCode(String holderCertiCode) {
		this.holderCertiCode = holderCertiCode;
	}

	/**
	 * @return 傳回 isExtraPremApproval。
	 */
	public boolean getIsExtraPremApproval() {
		return isExtraPremApproval;
	}

	/**
	 * @param isExtraPremApproval 要設定的 isExtraPremApproval。
	 */
	public void setIsExtraPremApproval(boolean isExtraPremApproval) {
		this.isExtraPremApproval = isExtraPremApproval;
	}

	/**
	 * @return 傳回 isExculsionApproval。
	 */
	public boolean getIsExculsionApproval() {
		return isExculsionApproval;
	}

	/**
	 * @param isExculsionApproval 要設定的 isExculsionApproval。
	 */
	public void setIsExculsionApproval(boolean isExculsionApproval) {
		this.isExculsionApproval = isExculsionApproval;
	}
	/**
	 * 客戶申請日
	 * @return
	 */
	public Date getCustApplyTime() {
		return custApplyTime;
	}
	/**
	 * 客戶申請日
	 * @param custApplyDate
	 */
	public void setCustApplyTime(Date custApplyTime) {
		this.custApplyTime = custApplyTime;
	}

	public String getFormerPolicyCode() {
		return formerPolicyCode;
	}

	public void setFormerPolicyCode(String formerPolicyCode) {
		this.formerPolicyCode = formerPolicyCode;
	}

	public String getEddaBankPay() {
		return eddaBankPay;
	}

	public void setEddaBankPay(String eddaBankPay) {
		this.eddaBankPay = eddaBankPay;
	}

	public String getNextEddaBankPay() {
		return nextEddaBankPay;
	}

	public void setNextEddaBankPay(String nextEddaBankPay) {
		this.nextEddaBankPay = nextEddaBankPay;
	}

	public String getEddaBankAuth() {
		return eddaBankAuth;
	}

	public void setEddaBankAuth(String eddaBankAuth) {
		this.eddaBankAuth = eddaBankAuth;
	}

	public String getNextEddaBankAuth() {
		return nextEddaBankAuth;
	}

	public void setNextEddaBankAuth(String nextEddaBankAuth) {
		this.nextEddaBankAuth = nextEddaBankAuth;
	}

	public String getRiskReportNotes() {
		return riskReportNotes;
	}

	public void setRiskReportNotes(String riskReportNotes) {
		this.riskReportNotes = riskReportNotes;
	}

	/**
	 * PCR364900-新契約補全建檔
	 * @return the complementTaskMemo
	 */
	public String getComplementTaskMemo() {
		return complementTaskMemo;
	}

	/**
	 * PCR364900-新契約補全建檔
	 * @param complementTaskMemo the complementTaskMemo to set
	 */
	public void setComplementTaskMemo(String complementTaskMemo) {
		this.complementTaskMemo = complementTaskMemo;
	}

	public String getRemoteIndi() {
		return remoteIndi;
	}

	public void setRemoteIndi(String remoteIndi) {
		this.remoteIndi = remoteIndi;
	}

	public String getRemoteOverseas() {
		return remoteOverseas;
	}

	public void setRemoteOverseas(String remoteOverseas) {
		this.remoteOverseas = remoteOverseas;
	}

	public String getRemoteCallout() {
		return remoteCallout;
	}

	public void setRemoteCallout(String remoteCallout) {
		this.remoteCallout = remoteCallout;
	}

	public String getRemoteCheck() {
		return remoteCheck;
	}

	public void setRemoteCheck(String remoteCheck) {
		this.remoteCheck = remoteCheck;
	}

	public String getElderCalloutFlag() {
		return elderCalloutFlag;
	}

	public void setElderCalloutFlag(String elderCalloutFlag) {
		this.elderCalloutFlag = elderCalloutFlag;
	}	
	

}
