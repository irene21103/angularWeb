package com.ebao.ls.uw.ds;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import com.ebao.ls.image.data.bo.Image;
import com.ebao.ls.image.vo.ImageVO;
import com.ebao.ls.foundation.service.GenericServiceImpl;
import com.ebao.ls.pa.pub.data.bo.UwRiFacExtend;
import com.ebao.ls.pa.pub.vo.UwRiFacExtendVO;
import com.ebao.ls.pty.data.bo.Reinsurer;
import com.ebao.ls.pty.ds.ReinsurerService;
import com.ebao.ls.pty.ds.vo.ReinsurerConditionVO;
import com.ebao.ls.pty.vo.ReinsurerVO;
import com.ebao.ls.uw.data.ReinsurerDelegate;
import com.ebao.ls.uw.data.TUwQmQueryDelegate;
import com.ebao.ls.uw.data.TUwRiFacExtendDelegate;
import com.ebao.ls.uw.data.bo.QualityManageList;
import com.ebao.ls.uw.vo.QualityManageListVO;
import com.ebao.pub.framework.GenericException;
import com.ebao.pub.util.BeanUtils;
import com.ebao.pub.web.pager.Pager;

public class ReinsurerServiceImpl extends GenericServiceImpl<ReinsurerVO, Reinsurer, ReinsurerDelegate> implements ReinsurerService{

	@Override
	public Long create(ReinsurerVO vo) throws GenericException {
		return null;
	}

	@Override
	public void update(ReinsurerVO vo) throws GenericException {
	}

	@Override
	public ReinsurerVO getReinsurerByReinsuerId(Long reinsurerId) throws GenericException {
		return null;
	}


	@Override
	public ReinsurerVO getReinsurerByReinsuerCodeIgnoreCase(String reinsurerCode) throws GenericException {
		return null;
	}

	@Override
	public ReinsurerVO[] getReinsurerArrayByCondition(ReinsurerConditionVO reinsurerConditionVO) throws GenericException {
		return null;
	}

	@Override
	protected ReinsurerVO newEntityVO() {
		return null;
	}

	@Override
	public List<ReinsurerVO> getReinsurerListByCondition(ReinsurerConditionVO condition) throws GenericException {
		return null;
	}

    

}
