package com.ebao.ls.callout.ajax;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
//import org.apache.log4j.Logger;

import com.ebao.ls.callout.bs.CalloutTransService;
import com.ebao.ls.chl.service.AgentService;
import com.ebao.ls.chl.service.ChannelOrgService;
import com.ebao.ls.chl.vo.ChannelOrgVO;
import com.ebao.ls.chl.vo.ScAgentDataVO;
import com.ebao.ls.chl.vo.ScServiceAgentDataVO;
import com.ebao.ls.cs.commonflow.ds.ApplicationService;
import com.ebao.ls.cs.commonflow.vo.ApplicationVO;
import com.ebao.ls.pa.nb.bs.AcknowledgeProcessService;
import com.ebao.ls.pa.nb.data.PolicyAcknowledgementDao;
import com.ebao.ls.pa.nb.vo.PolicyAckTaskVO;
import com.ebao.ls.pa.pub.ci.PolicyCI;
import com.ebao.ls.pa.pub.model.CoverageAgentInfo;
import com.ebao.ls.pa.pub.model.CoverageInfo;
import com.ebao.ls.pa.pub.model.InsuredInfo;
import com.ebao.ls.pa.pub.model.PolicyHolderInfo;
import com.ebao.ls.pa.pub.model.PolicyInfo;
import com.ebao.ls.pa.pub.service.PolicyService;
import com.ebao.ls.product.service.LifeProductService;
import com.ebao.ls.prt.data.bo.PolicyAcknowledgement;
import com.ebao.ls.pty.ds.DeptService;
import com.ebao.ls.pty.service.PartyService;
import com.ebao.ls.pub.CodeCst;
import com.ebao.ls.pub.ajax.AjaxService;
import com.ebao.ls.pub.cst.InsuredCategory;
import com.ebao.ls.uw.ci.UwPolicyCI;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.i18n.lang.CodeTable;
import com.ebao.pub.i18n.lang.StringResource;

import edu.emory.mathcs.backport.java.util.Arrays;

public class PolicyInfoAjaxService implements
                AjaxService<Map<String, Object>, Map<String, Object>> {

    //private static Logger logger = Logger.getLogger(PolicyInfoAjaxService.class);
    
    public static final String BEAN_DEFAUL = "policyInfoAjax";
    
    public static final String FIND_POLICY_CODE = "findPolicyCode";
    
    public static final String FIND_POLICY_INFO = "findPolicyInfo";
    
    @Resource(name = CalloutTransService.BEAN_DEFAULT)
    private CalloutTransService calloutTransService;

    @Resource(name = PolicyCI.BEAN_DEFAULT)
    private PolicyCI policyci;

    @Resource(name = PolicyService.BEAN_DEFAULT)
    private PolicyService policyService;
    
    @Resource(name = PartyService.BEAN_DEFAULT)
    private PartyService partyService;

    @Resource(name = ChannelOrgService.BEAN_DEFAULT)
    private ChannelOrgService channelService;

    @Resource(name=AgentService.BEAN_DEFAULT)
    private AgentService agentService;

    @Resource(name = UwPolicyCI.BEAN_DEFAULT)
    private UwPolicyCI uwpolicyservice;

    @Resource(name = PolicyAcknowledgementDao.BEAN_DEFAULT)
    private PolicyAcknowledgementDao policyAckDao;
    
    @Resource(name = AcknowledgeProcessService.BEAN_DEFAULT)
    private AcknowledgeProcessService acknowledgeProcessService;

    @Resource(name = ApplicationService.BEAN_DEFAULT)
    private ApplicationService appservice;
    
    @Resource(name = DeptService.BEAN_DEFAULT)
    private DeptService deptService;
    
	@Resource(name = LifeProductService.BEAN_DEFAULT)
	private LifeProductService lifeProductService;
    
    @Override
    public Map<String, Object> execute(Map<String, Object> in) {
        
        String ajaxType = (String) in.get("ajaxType");
        if(FIND_POLICY_CODE.equals(ajaxType)){
            return findPolicyCode(in);
        } else if (FIND_POLICY_INFO.equals(ajaxType)) {
            return findPolicyInfo(in);
        } else {
            return new HashMap<String, Object>();
        }
        
    }
    
    public Map<String, Object> findPolicyCode(Map<String,Object> in ){
        Map<String, Object> outMap = new HashMap<String, Object>();
        Long policyId = Long.valueOf((String) in.get("policyId"));
        String policyCode = policyci.findPolicyCodeByPolicyId(policyId);
        outMap.put("policyCode", policyCode);
        return outMap;
    }
    
    public Map<String, Object> findPolicyInfo(Map<String,Object> in){
        Map<String, Object> outMap = new HashMap<String, Object>();
        try {
            Long policyId = policyService.getPolicyIdByPolicyNumber(String.valueOf(in.get("policyCode")));
            if (in.get("policyId") != null) {
                PolicyInfo npolicy = policyService.load(Long.valueOf((Integer) in.get("policyId")));
                outMap.put("policyCode", npolicy.getPolicyNumber());
                outMap.put("policyId", npolicy.getPolicyId());
            }
            // 日期格式轉換
            SimpleDateFormat dateformate = new SimpleDateFormat("dd/MM/yyyy");
            if (policyId != null) {
                PolicyInfo policy = policyService.load(policyId);
                outMap.put("tiedPolicy", StringUtils.defaultIfEmpty(policy.getTiedPolicy(),""));
                // 要保人 info
                PolicyHolderInfo holdervo = policy.getPolicyHolder();
                outMap.put("policyId", policy.getPolicyId().toString());
                outMap.put("holderName", holdervo.getName());
                outMap.put("holderCertiCode", holdervo.getCertiCode());
                outMap.put("policyHolderOfficeTelReg", holdervo.getOfficeTelReg() != null ? holdervo.getOfficeTelReg() : "");       // 聯絡電話日區碼.
                outMap.put("policyHolderOfficeTel", holdervo.getOfficeTel() != null ? holdervo.getOfficeTel() : "");                // 聯絡電話日電話碼.
                outMap.put("policyHolderOfficeTelExt", holdervo.getOfficeTelExt() != null ? holdervo.getOfficeTelExt() : "");       // 聯絡電話日電話分機.
                outMap.put("policyHolderHomeTelReg", holdervo.getHomeTelReg() != null ? holdervo.getHomeTelReg() : "");             // 聯絡電話夜區碼.
                outMap.put("policyHolderHomeTel", holdervo.getHomeTel() != null ? holdervo.getHomeTelReg() : "");                   // 聯絡電話夜電話碼.
                outMap.put("policyHolderHomeTelExt", holdervo.getHomeTelExt() != null ? holdervo.getHomeTelExt() : "");             // 聯絡電話夜電話分機.
                outMap.put("policyHolderMobileTel", holdervo.getMobileTelephone() != null ? holdervo.getMobileTelephone() : "");    // 聯絡電話手機.
                outMap.put("policyHolderMobileTel2", holdervo.getMobileTel2() != null ? holdervo.getMobileTel2() : "");             // 聯絡電話手機2.
                outMap.put("holderAdds", holdervo.getAddress()!=null?(holdervo.getAddress().getAddress1()!=null?holdervo.getAddress().getAddress1():""):"");                       // 住所地址.
                outMap.put("policyHolderBirthDate", holdervo.getBirthDate() != null ? dateformate.format(holdervo.getBirthDate()) : "");// 要保人生日.

                // 主要被保人Info
                List<InsuredInfo> insuredInfos = policy.getInsureds();
                InsuredInfo insured = null;

                // 取出眾多被保人判斷最主要的那位 InsuredCategory = "1".
                for (int i = 0; i < insuredInfos.size(); i++) {
                    if (InsuredCategory.INSURED_CATGORY_SELF.equals(insuredInfos.get(i).getInsuredCategory())) {
                        insured = insuredInfos.get(i);
                    }
                }
                //
                if (insured != null) {
                    outMap.put("mainInsuredName", insured.getName());
                    outMap.put("mainInsuredCertiCode", insured.getCertiCode());
                    outMap.put("mainInsuredTelReg", insured.getOfficeTelReg() != null ? insured.getOfficeTelReg() : "");            // 聯絡電話日區碼.
                    outMap.put("mainInsuredofficeTel", insured.getOfficeTel() != null ? insured.getOfficeTel() : "");               // 聯絡電話日電話碼.
                    outMap.put("mainInsuredofficeTelExt", insured.getOfficeTelExt() != null ? insured.getOfficeTelExt() : "");      // 聯絡電話日電話分機.
                    outMap.put("mainInsuredhomeTelReg", insured.getHomeTelReg() != null ? insured.getHomeTelReg() : "");            // 聯絡電話夜區碼.
                    outMap.put("mainInsuredhomeTel", insured.getHomeTel() != null ? insured.getHomeTel() : "");                     // 聯絡電話夜電話碼.
                    outMap.put("mainInsuredhomeTelExt", insured.getHomeTelExt() != null ? insured.getHomeTelExt() : "");            // 聯絡電話夜電話分機.
                    outMap.put("mainInsuredTelephone", insured.getMobileTelephone() != null ? insured.getMobileTelephone() : "");   // 聯絡電話手機.
                    outMap.put("mainInsuredMobileTel2", insured.getMobileTel2() != null ? insured.getMobileTel2() : "");            // 聯絡電話手機2.
                    outMap.put("mainInsuredAdds", insured.getAddress()!=null?(insured.getAddress().getAddress1()!=null?insured.getAddress().getAddress1():""):"");                                              // 住所地址.
                    outMap.put("insuredBirthDate", insured.getBirthDate() != null ? dateformate.format(insured.getBirthDate()) : "");
                }
                outMap.put("calloutReqDeptName", deptService.getDeptById(AppContext.getCurrentUser().getDeptId()).getDeptName() );
                outMap.put("calloutReqDept", AppContext.getCurrentUser().getDeptId());
                // 保單主業務員.
                Map<String,Object> agentChlMap = getServiceAgent(policyId);
                outMap.putAll(agentChlMap);
                
                Long userID = uwpolicyservice.getUnderwriterId(policyId);
                if(userID!=null){
                    outMap.put("underwriteId", String.valueOf(userID));
                    outMap.put("underwriteName",CodeTable.getCodeById("T_USER",String.valueOf(userID)));
                } else {
                    outMap.put("underwriteId", "");
                    outMap.put("underwriteName", "");
                }
                
                // 承保日.
                outMap.put("issueDate", policy.getIssueDate() == null ? "" : dateformate.format(policy.getIssueDate()));
                // 生效日.
                outMap.put("validateDate", policy.getInitialInceptionDate() == null ? "" : dateformate.format(policy.getInitialInceptionDate()));

                List<PolicyAcknowledgement> ackvolist = policyAckDao.findByPolicyId(policyId);
                if (ackvolist != null && ackvolist.size() > 0) {
                    PolicyAcknowledgement ackvo = ackvolist.get(0);
                    // 實際郵寄保單日
                    outMap.put("dispatchDate", ackvo.getDispatchDate() == null ? "" : dateformate.format(ackvo.getDispatchDate()));
                } else {
                    // 實際郵寄保單日
                    outMap.put("dispatchDate", "");
                }
                PolicyAckTaskVO ackTaskVO = acknowledgeProcessService.getNewestValidateTask(policyId);
                if(ackTaskVO!=null) {
                    // 保戶簽收日
                    outMap.put("acknowledgeDate", ackTaskVO.getAckDate() == null ? "" : dateformate.format(ackTaskVO.getAckDate()));
                    // 要保人簽名審核結果.
                    if(ackTaskVO.getHolderSignResult() != null){
                        outMap.put("signResult", ackTaskVO.getHolderSignResult());
                        outMap.put("signResultDESC", ackTaskVO.getHolderSignResult().equals("0")?"":CodeTable.getCodeDesc("T_POLICY_SIGN_RESULT", String.valueOf(ackTaskVO.getHolderSignResult())));
                    } else {
                        outMap.put("signResult", "");
                        outMap.put("signResultDESC", "");
                    }
                    // 法代簽名審核結果.
                    if (ackTaskVO.getLegalSignResult() != null){
                        outMap.put("legalSignResult", ackTaskVO.getLegalSignResult());
                        outMap.put("legalSignResultDESC", ackTaskVO.getLegalSignResult().equals("0")?"":CodeTable.getCodeDesc("T_POLICY_SIGN_RESULT", String.valueOf(ackTaskVO.getLegalSignResult())));
                    } else {
                        outMap.put("legalSignResult", "");
                        outMap.put("legalSignResultDESC", "");
                    }
                } else {
                    // 保戶簽收日
                    outMap.put("acknowledgeDate", "");
                    // 要保人簽名審核結果.
                    outMap.put("signResult", "");
                    outMap.put("signResultDESC", "");
                    // 法代簽名審核結果.
                    outMap.put("legalSignResult", "");
                    outMap.put("legalSignResultDESC", "");
                }
                // 保單會辦次數(頁面要+1)
                // [PCR2][RTC50353] 會辦次數 依保單狀況 (未生效/生效) 分別計數會辦次數
                Long userCategory = partyService.getBizCategory(AppContext.getCurrentUser().getUserId());
                if (StringUtils.isNotBlank((String) in.get("changeId"))) {
                    outMap.put("transCount", 0); // 契變電訪
                } else if (userCategory == CodeCst.BIZ_CATEGORY_POS){
                    outMap.put("transCount", 0); // POS電訪
                } else {
                    outMap.put("transCount", calloutTransService.getCalloutTransCount(policyId, policy.getRiskStatus())+1); // UNB
                }
                // 最近一次電訪日
                List<Map<String, Object>> listTrans = calloutTransService.findLastTransDateBypolicyID(policyId);
                if (listTrans.size() > 0) {
                    outMap.put("lastTransDate", listTrans.get(0).get("UPDATE_TIME") == null ? "" : dateformate.format(listTrans.get(0).get("UPDATE_TIME")));
                }
                else {
                    outMap.put("lastTransDate", "");
                }
				// PCR 377520 eBao電訪會辦優化需求
				if (CodeCst.YES_NO__YES.equals((String) in.get("fromCallout"))) {
					List<CoverageInfo> coveragelist = policy.getCoverages();
					List<String> planCodeList = new ArrayList<String>();
					boolean hasWaiver = false;
					BigDecimal waiverPrem = new BigDecimal("0");
					Integer waiverProductId = null;
					for (CoverageInfo info : coveragelist) {
						String waiver = info.getWaiverExt().getWaiver();
						if (CodeCst.YES_NO__YES.equals(waiver)) {
							hasWaiver = true;
							waiverProductId = info.getProductId();
							waiverPrem = waiverPrem.add(info.getCurrentPremium().getTotalPremAf());
						} else {
							if ((info.getCustomizedPrem() == null || info
											.getCustomizedPrem()
											.equals(new BigDecimal("0")))
											&& (info.getCurrentPremium().getTotalPremAf()
															.equals(new BigDecimal("0")))) {
								planCodeList
												.add(lifeProductService
																.getProduct(
																				info.getProductId()
																								.longValue())
																.getProduct().getInternalId());
							}
						}
					}
					
					if (hasWaiver && waiverPrem.equals(new BigDecimal("0"))) {
						planCodeList.add(lifeProductService.getProduct(waiverProductId.longValue())
										.getProduct().getInternalId());
					}

					if (!planCodeList.isEmpty()) {
						StringBuilder sb = new StringBuilder();
						sb.append("<")
								.append(org.apache.commons.lang.StringUtils
										.join(planCodeList.toArray(), "、"))
								.append(">");
						Map<String, String> hm = new HashMap<String, String>();
						hm.put("planCode", sb.toString());
						String alertMsg = StringResource.getStringData(
								"MSG_1265458", AppContext.getCurrentUser()
										.getLangId(), hm);
						outMap.put("alertMsg", alertMsg);
					}
				}
                ApplicationVO appvo = null;
                if (StringUtils.isNotBlank((String) in.get("changeId"))) {
                    Long changeId = Long.valueOf(((String) in.get("changeId")));
                    appvo = appservice.getMainAlterationItemByChangeId(changeId);
                }
                if(appvo!=null){
                    // 契變受理日
                    outMap.put("regDate", appvo.getApplyTime() == null ? "" : dateformate.format(appvo.getApplyTime()));
                    // 契變承辦人員
                    outMap.put("changeServiceName", appvo.getHandleId() == null ? "" : CodeTable.getCodeById("T_USER",String.valueOf(appvo.getHandleId())));
                    outMap.put("serviceAgent", appvo.getHandleId() == null ? "" : appvo.getHandleId());
                }else{
                    outMap.put("regDate", "");
                    outMap.put("changeServiceName", "");
                    outMap.put("serviceAgent", "");
                }
                outMap.put("Result", true);
            }
            else {
                outMap.put("Result", false);
            }
        } catch (Exception e){
            return outMap;
        }
        return outMap;
    }
    
    private Map<String,Object> getServiceAgent(Long policyId) {
    	Map<String,Object> map = new java.util.HashMap<String,Object>();
    	PolicyInfo policyVO = policyService.load(policyId);
    	Integer proposalStatus = policyVO.getProposalInfo().getProposalStatus();
    	
    	map.put("channelStaffCode", ""); // 業務員字號
        map.put("agentName", "");
        map.put("agentId", "");
        map.put("agentMobile", ""); // 業務員手機
        map.put("agentMobile2", ""); // 業務員聯絡電話日
        map.put("agentMobile3", "");// 業務員聯絡電話夜
        
        map.put("channelType", "");
        map.put("salesChannelName", "");
        map.put("channelOrgId", "");
        map.put("channelName", "");
        
    	/* 核保中取業務員 */
        List<Integer> statusList = Arrays.asList(new Integer[] {CodeCst.PROPOSAL_STATUS__UW,CodeCst.PROPOSAL_STATUS__WAIT_UW});
    	if(policyVO.getServiceAgent() == null && proposalStatus != null && statusList.contains(proposalStatus.intValue())) {
    		List<CoverageInfo> coverageVOList = policyVO.getCoverages();
    		CoverageInfo itemVO = null;
    		
    		for(CoverageInfo coverageVO : coverageVOList) {
    			if(coverageVO.getMasterCoverageId() == null) {
    				itemVO = coverageVO;
    				break;
    			}
    		}
    		/* 取得 benefit_agent */
    		CoverageAgentInfo benefitAgentVO = null;
    		Integer minOrderId = 999;
    		for(CoverageAgentInfo agentVO : itemVO.getCoverageAgents()) {
    			if(agentVO.getOrderId() != null && agentVO.getOrderId() < minOrderId) {
    				minOrderId = agentVO.getOrderId();
    				benefitAgentVO = agentVO;
    			}
    		}
    		
    		if(benefitAgentVO == null) {
    			benefitAgentVO = itemVO.getCoverageAgents().get(0);
    		}
    		
    		/* 取出 benefit_agent 對應 channel_org */
    		String channelCode = benefitAgentVO.getChannelCode();
    		ChannelOrgVO channelVO = channelService.findByChannelCode(channelCode);
    		map.put("channelStaffCode", benefitAgentVO.getRegisterCode());
    		map.put("agentName",  benefitAgentVO.getAgentName());
    		
    		if(channelVO != null) {
    			String channelTypeId = String.valueOf(channelVO.getChannelType());
    			map.put("channelType", channelTypeId);
    			map.put("salesChannelName", CodeTable.getCodeById("T_SALES_CHANNEL", channelTypeId) );
    			map.put("channelOrgId", String.valueOf(channelVO.getChannelId()));
    			map.put("channelName", channelVO.getChannelName());
    		}
    		return map;
    	}
    	
    	/* 非核保中保單，服務員與綜合查詢頁面一致 */
    	ScAgentDataVO agentDataVO = agentService.getAgentDataByPolicyId(policyId);
        List<ScServiceAgentDataVO> serviceAgentList = agentDataVO.getServiceAgentList();
    	if(CollectionUtils.isEmpty(serviceAgentList)) {
    		return map;
    	}
    	
    	ScServiceAgentDataVO agentVO = serviceAgentList.get(0);
    	map.put("channelStaffCode", agentVO.getAgentRegisterCode()); 
        map.put("agentName", agentVO.getAgentName());
        map.put("agentId", agentVO.getAgentId());
        com.ebao.ls.sc.vo.AgentChlVO agentChlVO = agentService.findAgentByAgentId(agentVO.getAgentId());
        map.put("agentMobile", StringUtils.defaultIfEmpty(agentChlVO.getMobile(),"")); // 業務員手機
        map.put("agentMobile2", StringUtils.defaultIfEmpty(agentChlVO.getOfficeTel(),"")); // 業務員聯絡電話日
        map.put("agentMobile3", StringUtils.defaultIfEmpty(agentChlVO.getHomeTel(),""));// 業務員聯絡電話夜
        
        map.put("channelType", agentVO.getAgentChannelType());
        map.put("salesChannelName", agentVO.getAgentChannelTypeName());
        map.put("channelName", agentVO.getAgentChannelName());
        
        ChannelOrgVO channelVO = channelService.findByChannelCode(agentVO.getAgentChannelCode());
        if(channelVO != null) {
            map.put("channelOrgId", channelVO.getChannelId());
		}
        
    	return map;
    }
}
