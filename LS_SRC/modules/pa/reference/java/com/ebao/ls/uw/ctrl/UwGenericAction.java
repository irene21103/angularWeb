package com.ebao.ls.uw.ctrl;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.UserTransaction;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.ls.pa.pub.service.PolicyService;
import com.ebao.ls.pub.action.ElapsedTimeLoggerAction;
import com.ebao.ls.pub.workflow.WfContextLs;
import com.ebao.ls.uw.ds.UwPolicyService;
import com.ebao.ls.uw.ds.constant.ProposalStatusConstants;
import com.ebao.ls.uw.ds.constant.UwExceptionConstants;
import com.ebao.ls.uw.ds.constant.UwSourceTypeConstants;
import com.ebao.ls.uw.ds.constant.UwStatusConstants;
import com.ebao.ls.uw.util.Utils;
import com.ebao.ls.uw.vo.UwPolicyVO;
import com.ebao.pub.annotation.PAPubAPIUpdate;
import com.ebao.pub.framework.AppException;
import com.ebao.pub.framework.ExceptionFactory;
import com.ebao.pub.framework.GenericException;
import com.ebao.pub.util.Log;
import com.ebao.pub.util.StringUtils;
import com.ebao.pub.util.Trans;
import com.ebao.pub.util.TransUtils;

/*
 * <p>Title:GEL-UW</p>
 * <p>Description:Underwriting Generic Action  </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: eBaoTech Corporation.</p>
 * @author jason.luo
 * @version 1.0
 * @since 09.24.2004
 */

/**
 * Underwriting Generic Action
 * Currently fulfilling following responsibility:
 * 1.Determines whether the underwriter has the access right on the 
 *  proposal he worked on;
 * 2.Invoke the corresponding business method if the underwriter has the 
 *  access right;
 * 3.Manage the uw module business transaction procedure.
 */
public abstract class UwGenericAction extends ElapsedTimeLoggerAction {

  /**
   * Add Condition related information into the corresponding proposal.
   * 
   * @param mapping The ActionMapping used to select this instance
   * @param form The optional ActionForm bean for this request (if any)
   * @param request                   HttpRequest
   * @param response                  HttpResponse
   * @return ActionForward            ActionForward
   * @throws GenericException         Application Exception
   */
  @Override
  @PAPubAPIUpdate("loadPolicyByPolicyId")
  public ActionForward doProcess(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws GenericException {
    // Long underwriteId = Long.valueOf(request.getParameter("underwriteId"));
    // update for workflow,by robert.xu on 2008.4.22
    Long underwriteId = null;
    String isIframe = request.getParameter("isIframe");
    String reqFrom = request.getParameter("reqFrom");
    if (!StringUtils.isNullOrEmpty(request.getParameter("underwriteId"))&& !request.getParameter("underwriteId").equals("null")) {
      underwriteId = Long.valueOf(request.getParameter("underwriteId"));
    } else {
      WfContextLs wfLs = WfContextLs.getInstance();
      String taskId = (String) request
          .getAttribute("com.ebao.pub.workflow.taskId");

      if (taskId != null) {
        underwriteId = (Long) wfLs.getLocalVariable(taskId, "underWriteId");
      }
      Log.info(UwGenericAction.class, "===- underwriteId is===" + underwriteId);
    }
    // update end
    if(underwriteId != null){
    UwPolicyVO uwPolicyVO = uwPolicyDS.findUwPolicy(underwriteId);
    /*
     * Try to find out whether the underwriter has the access right on the   
     * proposal or policy. 
     */
    Long underwriterId = uwPolicyVO.getUnderwriterId();
    String uwStatus = uwPolicyVO.getUwStatus();
    Long policyId = uwPolicyVO.getPolicyId();
    String uwSourceType = uwPolicyVO.getUwSourceType();
    // added condition for defect CRDB00199829(NBU workflow reassign issue),by robert.xu on 2008.5.26
	// 2016-11-08 Kate Hsiao 任何保全核保員都可以查看保全核保 
	if (!UwSourceTypeConstants.NEW_BIZ.equals(uwSourceType) && !Utils.isCsUw(uwSourceType)) {
		if (UwStatusConstants.IN_PROGRESS.equals(uwStatus)
					&& underwriterId != null && !getUnderwriterId().equals(underwriterId)) {
			throw ExceptionFactory.parse(new AppException(
						UwExceptionConstants.APP_PROPOSAL_LOCKED_BY_OTHERS, 
						String.valueOf(underwriterId.longValue())));
		}
	}

    Integer curProposalStatus = policyService.loadPolicyByPolicyId(policyId)
        .getProposalStatus();
    // Only new biz need to check proposal status

		if (StringUtils.isNullOrEmpty(isIframe)) {
			if (UwSourceTypeConstants.NEW_BIZ.equals(uwSourceType)) {
				if (!ProposalStatusConstants.WAITING_FOR_UNDERWRITING
								.equals(curProposalStatus)
								&& !ProposalStatusConstants.UNDERWRITING_IN_PROGRESS
												.equals(curProposalStatus)) {
					if(curProposalStatus > ProposalStatusConstants.UNDERWRITING_IN_PROGRESS){
						throw new AppException(
								UwExceptionConstants.APP_UW_COMPLETE_PRE_PROPOSAL_STATUS,
								"Proposal must be in 'Waiting For Underwriting' or 'Underwriting In Progress' status.");						
					}
				}
			}
			// add a more check whether uw record is valid
			if (!UwStatusConstants.IN_PROGRESS.equals(uwStatus)
							&& !UwStatusConstants.WAITING.equals(uwStatus)
							&& !UwStatusConstants.ESCALATED.equals(uwStatus)
							//RTCID-105002-保全加費變更（次標加費/除外）進入加費頁面時不作此限制
							&& !(reqFrom != null && reqFrom.equals("pos"))) {
				throw new AppException(
								UwExceptionConstants.APP_UW_COMPLETE_PRE_PROPOSAL_STATUS,
								"Invalid UnderWriting Status.");
			}
		}
    }
    UserTransaction trans = null;
    try {
      trans = Trans.getUserTransaction();
      trans.begin();
      ActionForward forward = uwProcess(mapping, form, request, response);
      trans.commit();
      return forward;
    } catch (Exception e) {
      TransUtils.rollback(trans);
      throw ExceptionFactory.parse(e);
    }
  }

  /**
   * Get current underwriter
   * 
   * @return  		 Underwriter id
   * @throws GenericException  Application Exception
   */
  protected Long getUnderwriterId() throws GenericException {
    return ActionUtil.getUnderwriterId();
  }

  /**
   * Get underwrite id
   * 
   * @return  		 Underwrite id
   * @throws GenericException  Application Exception
   */
  public static Long getUnderwriteId(HttpServletRequest request)
      throws GenericException {
    return ActionUtil.getUnderwriteId(request);
  }

  /**
   * Real business method of the uw module
   * 
   * @param mapping The ActionMapping used to select this instance
   * @param form The optional ActionForm bean for this request (if any)
   * @param request                   HttpRequest
   * @param response                  HttpResponse
   * @return ActionForward            ActionForward
   * @throws GenericException         Application Exceptions
   */
  public abstract ActionForward uwProcess(ActionMapping mapping,
      ActionForm form, HttpServletRequest request, HttpServletResponse response)
      throws GenericException;
  

	@Override
	protected String getModule() {
		return ElapsedTimeLoggerAction.MODULE_UNB;
	}

	@Override
	protected Class<?> getApplicationId() {
		return UwGenericAction.class;
	}

	@Override
	protected String getApplicationDesc() {
		return  "Uw Policy Main Fix Action(领取核保案件)";
	}

  @Resource(name = UwPolicyService.BEAN_DEFAULT)
  protected UwPolicyService uwPolicyDS;

  public void setUwPolicyDS(UwPolicyService uwPolicyDS) {
    this.uwPolicyDS = uwPolicyDS;
  }

  public UwPolicyService getUwPolicyDS() {
    return uwPolicyDS;
  }

  @Resource(name = PolicyService.BEAN_DEFAULT)
  private PolicyService policyService;

}