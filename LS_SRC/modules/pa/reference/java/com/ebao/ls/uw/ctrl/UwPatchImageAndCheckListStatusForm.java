package com.ebao.ls.uw.ctrl;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionMapping;

import com.ebao.ls.pa.nb.ctrl.pub.CommissionAgentForm;
import com.ebao.ls.pub.CodeCst;
import com.ebao.pub.framework.GenericForm;

/**
 * Title: 補全影像與會辦狀態 <br>
 * Description: Uw PatchImage and CheckListStatus form <br>
 * Copyright: Copyright (c) 2004 <br>
 * Company: eBaoTech Corporation <br>
 * Create Time: 2008/11/06 <br>
 * 
 * @author lucky.xie
 * @version 1.0
 */
public class UwPatchImageAndCheckListStatusForm extends GenericForm {

	/*
	 * T_Patch_Image
	 */
	private Long patchListId;

	private Date patchUploadDate;

	private String patchImageName;

	private String patchConfirmComplete;
	
	/*
	 * T_Check_List_Status
	 */

	private Long checkListId;

	private Date checkUwApplyDate;

	private String checkTargetClientName;

	private String checkCheckListType;

	private String checkConfirmComplete;

	public Long getPatchListId() {
		return patchListId;
	}

	public void setPatchListId(Long patchListId) {
		this.patchListId = patchListId;
	}

	public Date getPatchUploadDate() {
		return patchUploadDate;
	}

	public void setPatchUploadDate(Date patchUploadDate) {
		this.patchUploadDate = patchUploadDate;
	}

	public String getPatchImageName() {
		return patchImageName;
	}

	public void setPatchImageName(String patchImageName) {
		this.patchImageName = patchImageName;
	}

	public String getPatchConfirmComplete() {
		return patchConfirmComplete;
	}

	public void setPatchConfirmComplete(String patchConfirmComplete) {
		this.patchConfirmComplete = patchConfirmComplete;
	}

	public Long getCheckListId() {
		return checkListId;
	}

	public void setCheckListId(Long checkListId) {
		this.checkListId = checkListId;
	}

	public Date getCheckUwApplyDate() {
		return checkUwApplyDate;
	}

	public void setCheckUwApplyDate(Date checkUwApplyDate) {
		this.checkUwApplyDate = checkUwApplyDate;
	}

	public String getCheckTargetClientName() {
		return checkTargetClientName;
	}

	public void setCheckTargetClientName(String checkTargetClientName) {
		this.checkTargetClientName = checkTargetClientName;
	}

	public String getCheckCheckListType() {
		return checkCheckListType;
	}

	public void setCheckCheckListType(String checkCheckListType) {
		this.checkCheckListType = checkCheckListType;
	}

	public String getCheckConfirmComplete() {
		return checkConfirmComplete;
	}

	public void setCheckConfirmComplete(String checkConfirmComplete) {
		this.checkConfirmComplete = checkConfirmComplete;
	}
	
	

}
