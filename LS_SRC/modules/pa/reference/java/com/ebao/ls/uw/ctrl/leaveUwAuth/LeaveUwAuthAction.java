package com.ebao.ls.uw.ctrl.leaveUwAuth;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.ls.pa.pub.auth.UwAreaLeaveAuthServiceImpl;
import com.ebao.ls.pub.auth.AuthItemCst;
import com.ebao.ls.uw.bo.LeaveUwAuthEdit;
import com.ebao.ls.uw.ds.UwAreaService;
import com.ebao.ls.uw.ds.UwAuthLeaveService;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.framework.GenericAction;
import com.ebao.pub.framework.MultiWarning;
import com.ebao.pub.util.StringUtils;

/**
 * <p>Title: 休假管理及核保轄區設置-查詢頁面</p>
 * <p>Description: Function Description Only vegetable</p>
 * <p>Copyright: Copyright (c) 2015</p>
 * <p>Company: TGL Co., Ltd.</p>
 * <p>Create Time: Aug 13, 2015</p> 
 * @author 
 * <p>Update Time: Aug 13, 2015</p>
 * <p>Updater: Alex Cheng</p>
 * <p>Update Comments: </p>
 */
public class LeaveUwAuthAction extends GenericAction {
  
  protected static final String ACTION_SEARCH = "search";
  
  @Resource(name = UwAreaService.BEAN_DEFAULT)
  private UwAreaService uwAreaService;
  
  @Resource(name = UwAuthLeaveService.BEAN_DEFAULT)
  private UwAuthLeaveService uwAuthLeaveService;
  
  @Resource(name = UwAreaLeaveAuthServiceImpl.BEAN_DEFAULT)
  private UwAreaLeaveAuthServiceImpl uwAreaLeaveAuthService;
  
  @Override
  protected MultiWarning processWarning(ActionMapping mapping,
                  ActionForm form,
                  HttpServletRequest request, HttpServletResponse response)
                  throws Exception {
//      LeaveUwAuthForm leaveUwAuthForm = (LeaveUwAuthForm) form;
      MultiWarning mw = new MultiWarning();
      mw.setContinuable(false);
      return mw;
  }
  
  @Override
  public ActionForward process(ActionMapping mapping, ActionForm form, HttpServletRequest request,
          HttpServletResponse response) throws Exception {
    LeaveUwAuthForm leaveUwAuthForm = (LeaveUwAuthForm) form;
    String findForward = "success";

    String saction = leaveUwAuthForm.getActionType();

    boolean isEditRole = uwAreaLeaveAuthService.hasAccessRight(AuthItemCst.AUTH_ITEM_LEAVE_UW_AREA_PAGE);
    leaveUwAuthForm.setIsEditRole(isEditRole);
    LeaveUwAuthEdit loginEmpData = uwAuthLeaveService.findDeptByEmpId(Long.toString(AppContext.getCurrentUser().getUserId()));
    Long loginDeptId = loginEmpData.getSectionDeptId() != null ? loginEmpData.getSectionDeptId(): loginEmpData.getMasterDeptId();
    if(!StringUtils.isNullOrEmpty(saction)){
      if(LeaveUwAuthAction.ACTION_SEARCH.equals(saction)){
        List<LeaveUwAuthEdit> leaveUwAuthList = null;
        if("1".equals(leaveUwAuthForm.getSearchType())){
          Long deptId = StringUtils.isNullOrEmpty(leaveUwAuthForm.getDeptId())? 
                  null : new Long(leaveUwAuthForm.getDeptId());
          Long sectionDeptId = StringUtils.isNullOrEmpty(leaveUwAuthForm.getSectionDeptId())? 
                  null : new Long(leaveUwAuthForm.getSectionDeptId());
          Long empId = StringUtils.isNullOrEmpty(leaveUwAuthForm.getEmpName())? 
                  null : new Long(leaveUwAuthForm.getEmpName());
          String empCode = StringUtils.isNullOrEmpty(leaveUwAuthForm.getEmpId())? 
                  null : leaveUwAuthForm.getEmpId();
          
          leaveUwAuthList = uwAuthLeaveService.searchLeaveDataByEmp(deptId, sectionDeptId, empId, empCode, isEditRole, loginDeptId);
        }else if("2".equals(leaveUwAuthForm.getSearchType())){
          Long channelType = StringUtils.isNullOrEmpty(leaveUwAuthForm.getChannelType())? 
                  null : new Long(leaveUwAuthForm.getChannelType());
          Long channelId = StringUtils.isNullOrEmpty(leaveUwAuthForm.getChannelId())? 
                  null : new Long(leaveUwAuthForm.getChannelId());
          leaveUwAuthList = uwAuthLeaveService.searchLeaveDataByArea(channelType, channelId, isEditRole, loginDeptId);
        }
        request.setAttribute("leaveUwAuthList", leaveUwAuthList);
      }
    }
    
    return mapping.findForward(findForward);
  }

}
