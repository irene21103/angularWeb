package com.ebao.ls.uw.ci;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;

import com.ebao.ls.arap.pub.ci.CashCI;
import com.ebao.ls.pa.nb.ci.ProposalCI;
import com.ebao.ls.pa.nb.vo.ProposalPremVO;
import com.ebao.ls.pa.pub.ci.CoverageCI;
import com.ebao.ls.pa.pub.service.PolicyService;
import com.ebao.ls.pa.pub.vo.CoverageVO;
import com.ebao.ls.pa.pub.vo.ExtraPremVO;
import com.ebao.ls.pa.pub.vo.LiabilityReduceVO;
import com.ebao.ls.pa.pub.vo.PolicyHolderVO;
import com.ebao.ls.pa.pub.vo.PolicyVO;
import com.ebao.ls.prt.ci.PolicyAcknowCI;
import com.ebao.ls.prt.ci.PolicyAcknowCIVO;
import com.ebao.ls.pub.CodeCst;
import com.ebao.ls.uw.ci.vo.UwBenefitUwDecisonCIVO;
import com.ebao.ls.uw.ci.vo.UwPolicyCIVO;
import com.ebao.ls.uw.ci.vo.UwPolicyRelateCIVO;
import com.ebao.ls.uw.data.query.UwQueryDao;
import com.ebao.ls.uw.ds.UwPolicyService;
import com.ebao.ls.uw.ds.UwProcessService;
import com.ebao.ls.uw.ds.constant.UwDecisionConstants;
import com.ebao.ls.uw.ds.constant.UwStatusConstants;
import com.ebao.ls.uw.ds.vo.UwProductDecisionVO;
import com.ebao.ls.uw.vo.UwConditionVO;
import com.ebao.ls.uw.vo.UwEndorsementVO;
import com.ebao.ls.uw.vo.UwExclusionVO;
import com.ebao.ls.uw.vo.UwLienVO;
import com.ebao.ls.uw.vo.UwLifeInsuredVO;
import com.ebao.ls.uw.vo.UwPolicyVO;
import com.ebao.ls.uw.vo.UwProductVO;
import com.ebao.pub.annotation.PAPubAPIUpdate;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.framework.ExceptionFactory;
import com.ebao.pub.framework.GenericException;
import com.ebao.pub.framework.ObjectNotFoundException;
import com.ebao.pub.i18n.lang.CodeTable;
import com.ebao.pub.util.BeanUtils;
import com.ebao.pub.util.DateUtils;
import com.ebao.pub.util.format.NumberFormat;

/**
 * Title:Underwriting <br>
 * Description: <br>
 * Copyright: Copyright (c) 2004 <br>
 * Company: eBaoTech Corporation <br>
 * 
 * @author walter.huang
 * @since Jul 6, 2004
 * @version 1.0
 */
public class UwProcessControlCIImpl implements UwProcessControlCI {
  @Resource(name = UwProcessService.BEAN_DEFAULT)
  private UwProcessService uwProcessDS;
  @Resource(name = UwPolicyService.BEAN_DEFAULT)
  private UwPolicyService uwPolicyDS;
  @Resource(name = ProposalCI.BEAN_DEFAULT)
  private ProposalCI proposalCI;

  public UwProcessService getUwProcessDS() {
    return uwProcessDS;
  }

  public UwPolicyService getUwPolicyDS() {
    return uwPolicyDS;
  }

  /*
   * (non-Javadoc)
   * @see
   * com.ebao.ls.uw.ci.UwProcessControlCI#processCSTransaction(java.lang.Long,
   * java.lang.Long, java.lang.Integer[], java.lang.Long, java.lang.Long[])
   */
  public void processCSTransaction(Long policyId, Long changeId,
      Integer[] csTransactions, Long uwProposerId, Long[] items)
      throws GenericException {
    csParameterValidation(policyId, changeId, csTransactions);
    getUwProcessDS().processCSTransaction(policyId, changeId, csTransactions,
        uwProposerId, items);
  }

  /*
   * (non-Javadoc)
   * @see
   * com.ebao.ls.uw.ci.UwProcessControlCI#processClaimTransaction(java.lang.
   * Long, java.lang.Long, java.lang.Long, java.lang.String)
   */
  public Long processClaimTransaction(Long policyId, Long changeId,
      Long uwProposerId, String uwSourceType) throws GenericException {
    return getUwProcessDS().processClaimTransaction(policyId, changeId,
        uwProposerId, uwSourceType);
  }

  /**
   * CS Parameter Validation
   * 
   * @param policyId Policy Id
   * @param changeId CS Change Id
   * @author jason.luo
   * @since 09.14.2004
   */
  private void csParameterValidation(Long policyId, Long changeId,
      Integer[] csTransactions) {
    csParameterValidation(policyId, changeId);
    if (null == csTransactions || csTransactions.length == 0) {
      throw new IllegalArgumentException("CSTransactioin can't be null.");
    }
  }

  /**
   * CS Parameter Validation - Used when exporting data
   * 
   * @param policyId Policy Id
   * @param changeId CS Change Id
   * @author jason.luo
   * @since 09.20.2004
   */
  private void csParameterValidation(Long policyId, Long changeId) {
    if (null == policyId || null == changeId) {
      throw new IllegalArgumentException("PolicyId and changeId can't be null.");
    }
  }

  /*
   * (non-Javadoc)
   * @see
   * com.ebao.ls.uw.ci.UwProcessControlCI#processNBTransaction(java.lang.Long,
   * java.lang.Long, java.lang.String)
   */
  public String processNBTransaction(Long policyId, Long uwProposerId,
      String manualUwInd) throws GenericException {
    if (null == policyId || null == uwProposerId) {
      throw new IllegalArgumentException(
          "PolicyId and uwProposerId can not be null.");
    }
    return getUwProcessDS().processNBTransaction(policyId, uwProposerId,
        manualUwInd);
  }

  /*
   * (non-Javadoc)
   * @see com.ebao.ls.uw.ci.UwProcessControlCI#getUwProducts(java.lang.Long,
   * java.lang.Long)
   */
  public UwProductVO[] getUwProducts(Long policyId, Long changeId)
      throws GenericException {
    csParameterValidation(policyId, changeId);
    // find underwrite id using policyId changeId and csTransaction
    Long underwriteId = getUwPolicyDS().retrieveUwIdbyChangeId(policyId,
        changeId);
    // find the products under the underwrite id
    Collection entities = getUwPolicyDS().findUwProductEntitis(underwriteId);
    try {
      Collection entityColl = BeanUtils.copyCollection(UwProductVO.class,
          entities);
      return (UwProductVO[]) entityColl.toArray(new UwProductVO[entityColl
          .size()]);
    } catch (Exception e) {
      throw ExceptionFactory.parse(e);
    }
  }

  /*
   * (non-Javadoc)
   * @see
   * com.ebao.ls.uw.ci.UwProcessControlCI#getUwProductDecisions(java.lang.Long,
   * java.lang.Long)
   */
  public UwProductDecisionVO[] getUwProductDecisions(Long policyId,
      Long changeId) throws GenericException {
    UwProductVO[] products = getUwProducts(policyId, changeId);
    try {
      return (UwProductDecisionVO[]) BeanUtils.copyArray(
          UwProductDecisionVO.class, products);
    } catch (Exception ex) {
      throw ExceptionFactory.parse(ex);
    }
  }

  /*
   * (non-Javadoc)
   * @see com.ebao.ls.uw.ci.UwProcessControlCI#getUwConditions(java.lang.Long,
   * java.lang.Long)
   */
  public UwConditionVO[] getUwConditions(Long policyId, Long changeId)
      throws GenericException {
    csParameterValidation(policyId, changeId);
    // find underwrite id using policyId changeId and csTransaction
    Long underwriteId = getUwPolicyDS().retrieveUwIdbyChangeId(policyId,
        changeId);
    // find the entities under the underwrite idfind the products under the
    // underwrite id
    Collection entities = getUwPolicyDS().findUwConditionEntitis(underwriteId);
    return (UwConditionVO[]) entities
        .toArray(new UwConditionVO[entities.size()]);
  }

  /*
   * (non-Javadoc)
   * @see com.ebao.ls.uw.ci.UwProcessControlCI#getUwExclusions(java.lang.Long,
   * java.lang.Long)
   */
  public UwExclusionVO[] getUwExclusions(Long policyId, Long changeId)
      throws GenericException {
    csParameterValidation(policyId, changeId);
    // find underwrite id using policyId changeId and csTransaction
    Long underwriteId = getUwPolicyDS().retrieveUwIdbyChangeId(policyId,
        changeId);
    // find the entities under the underwrite id
    Collection entities = getUwPolicyDS().findUwExclusionEntitis(underwriteId);
    return (UwExclusionVO[]) entities
        .toArray(new UwExclusionVO[entities.size()]);
  }

  /*
   * (non-Javadoc)
   * @see com.ebao.ls.uw.ci.UwProcessControlCI#getUwLiens(java.lang.Long,
   * java.lang.Long)
   */
  public UwLienVO[] getUwLiens(Long policyId, Long changeId)
      throws GenericException {
    csParameterValidation(policyId, changeId);
    // find underwrite id using policyId changeId and csTransaction
    Long underwriteId = getUwPolicyDS().retrieveUwIdbyChangeId(policyId,
        changeId);
    // get UwLienVOs
    Collection liens = getUwPolicyDS().findUwLienbyUwId(underwriteId);
    try {
      Collection entityColl = BeanUtils.copyCollection(UwLienVO.class, liens);
      return (UwLienVO[]) entityColl.toArray(new UwLienVO[entityColl.size()]);
    } catch (Exception e) {
      throw ExceptionFactory.parse(e);
    }
  }

  /*
   * (non-Javadoc)
   * @see com.ebao.ls.uw.ci.UwProcessControlCI#getUwEndorsements(java.lang.Long,
   * java.lang.Long)
   */
  public UwEndorsementVO[] getUwEndorsements(Long policyId, Long changeId)
      throws GenericException {
    csParameterValidation(policyId, changeId);
    // find underwrite id using policyId changeId and csTransaction
    Long underwriteId = getUwPolicyDS().retrieveUwIdbyChangeId(policyId,
        changeId);
    // find the entities under the underwrite id
    Collection entities = getUwPolicyDS()
        .findUwEndorsementEntitis(underwriteId);
    return (UwEndorsementVO[]) entities.toArray(new UwEndorsementVO[entities
        .size()]);
  }

  /*
   * (non-Javadoc)
   * @see com.ebao.ls.uw.ci.UwProcessControlCI#cancelUw(java.lang.Long)
   */
  public void cancelUw(Long policyId) throws GenericException {
    if (null == policyId) {
      throw new IllegalArgumentException("Policy Id can't be null.");
    }
    Long underwriteId = getUwPolicyDS().retrieveUwIdbyPolicyId(policyId);
    Long underwriterId = null;
    String hintDesc = "Cancel underwriting.";
    getUwProcessDS().quashUnderwriting(underwriteId, underwriterId, hintDesc,
        false);
  }

  /*
   * (non-Javadoc)
   * @see
   * com.ebao.ls.uw.ci.UwProcessControlCI#updateQueryReceived(java.lang.Long,
   * java.util.Date)
   */
  public void updateQueryReceived(Long policyId, Date queryReceivedDate)
      throws GenericException {
    if (null == policyId) {
      throw new IllegalArgumentException("Policy Id can't be null.");
    }
    Long underwriteId = getUwPolicyDS().retrieveUwIdbyPolicyId(policyId);
    if (underwriteId == null) {
      return;
    }
    UwPolicyVO policyVO = getUwPolicyDS().findUwPolicy(underwriteId);
    policyVO.setQueryReceivedDate(queryReceivedDate);
    getUwPolicyDS().updateUwPolicy(policyVO, false);
    String magnumHoldIndi = policyVO.getMagnumHoldIndi();
    if ("Y".equals(magnumHoldIndi)) {
      policyVO.setMagnumHoldIndi("N");
      policyVO.setUwStatus("4");
      getUwPolicyDS().updateUwPolicy(policyVO, false);
      // update decision id for t_contract_product
      List<CoverageVO> policyProducts = coverageCI.findByPolicyId(policyId);
      Integer decisionId = Integer
          .valueOf(UwDecisionConstants.STANDARD_CASE_UNDERWRITTEN);
      for (CoverageVO vo : policyProducts) {
        vo.setDecisionId(decisionId);
        coverageCI.updateCoverage(vo);
      }
      Long underwriterId = Long
          .valueOf(AppContext.getCurrentUser().getUserId());
      proposalCI.finishUpc(policyId,
          Integer.valueOf(CodeCst.PROPOSAL_STATUS__ACCEPTED),
          "Policy go through in force at magnum phase.", underwriterId);
      proposalCI.inforceProposal(policyVO.getPolicyId(),
          Long.valueOf(AppContext.getCurrentUser().getUserId()));
    } else {
      getUwPolicyDS().updateUwPolicy(policyVO, false);
    }
  }

  /*
   * (non-Javadoc)
   * @see com.ebao.ls.uw.ci.UwProcessControlCI#getUwPolicyInfo(java.lang.String)
   */
  @PAPubAPIUpdate("loadPOlicyByPolicyId")
  public UwPolicyRelateCIVO getUwPolicyInfo(String policyCode)
      throws GenericException {
    if (null == policyCode) {
      throw new IllegalArgumentException("policyCode can't be null.");
    }
    // get the external interface
    Long underwriteId = null;
    try {
      underwriteId = getUwPolicyDS().retrieveUwIdbyPolicyCode(policyCode);
    } catch (GenericException e1) {
      throw ExceptionFactory.parse(e1);
    }
    if (underwriteId == null) {
      return null;
    }
    UwPolicyVO uwPolicyVO = getUwPolicyDS().findUwPolicy(underwriteId);
    Long policyId = uwPolicyVO.getPolicyId();
    PolicyVO policyVO = policyService.loadPolicyByPolicyId(policyId);
    PolicyHolderVO proposer = policyVO.getPolicyHolder();
    UwPolicyRelateCIVO policyRelateVO = new UwPolicyRelateCIVO();
    policyRelateVO.setPolicyCode(uwPolicyVO.getPolicyCode());
    policyRelateVO.setProposalCode(uwPolicyVO.getApplyCode());
    policyRelateVO.setUwDecision(uwPolicyVO.getUwDecision());
    policyRelateVO.setUwDecisionDesc(CodeTable.getCodeDesc(
        "T_PRODUCT_DECISION", String.valueOf(uwPolicyVO.getUwDecision())));
    policyRelateVO.setProposalStatus(policyVO.getProposalStatus());
    policyRelateVO.setProposalStatusDesc(CodeTable.getCodeDesc(
        "T_PROPOSAL_STATUS", String.valueOf(policyVO.getProposalStatus())));
    policyRelateVO.setProposalDate(policyVO.getApplyDate());
    if (proposer.getParty().isPerson()) {
      policyRelateVO.setProposerName(proposer.getPerson().getFirstName());
    } else {
      policyRelateVO.setProposerName(proposer.getCompany().getCompanyName());
    }
    policyRelateVO.setUnderwritingDate(uwPolicyVO.getUnderwriteTime());
    // get insured1 and insured2
    List insuredLists = (List) getUwPolicyDS().findUwLifeInsuredEntitis(
        underwriteId);
    UwLifeInsuredVO insured1 = (UwLifeInsuredVO) insuredLists.get(0);
    policyRelateVO.setLifeAssured1(insured1.getInsuredId());
    if (insuredLists.size() > 1) {
      UwLifeInsuredVO insured2 = (UwLifeInsuredVO) insuredLists.get(1);
      policyRelateVO.setLifeAssured2(insured2.getInsuredId());
    }
    policyRelateVO.setCommencementDate(uwPolicyVO.getValidateDate());
    // added the check to fix defect GEL00027311 ning.qi 2007-07-06
    // if (policyRelateVO.getProposalDate() == null) {
    // policyRelateVO.setProposalDate(policyVO.getInsertTime());
    // }
    policyRelateVO.setPolicyIssueDate(policyVO.getIssueDate());
    PolicyAcknowCIVO policyAcknowVO = new PolicyAcknowCIVO();
    policyAcknowVO.setPolicyId(uwPolicyVO.getPolicyId());
    PolicyAcknowCIVO ackVO = policyAcknowCI.getPolicyAcknow(policyAcknowVO);
    if (ackVO.getDispatchDate() != null
        && DateUtils.getYear(ackVO.getDispatchDate()) != 1900) {
      policyRelateVO.setPolicyDispatchDate(ackVO.getDispatchDate());
    }
    policyRelateVO.setUwType(uwPolicyVO.getUwStatus());
    policyRelateVO.setUwTypeDesc(CodeTable.getCodeDesc("T_UW_TYPE",
        uwPolicyVO.getUwType()));
    Long underwriterId = uwPolicyVO.getUnderwriterId();
    policyRelateVO.setCurrentUnderwriter(underwriterId);
    if (underwriterId != null) {
      policyRelateVO.setCurrentUnderwriterName(CodeTable.getCodeDesc(
          "T_EMPLOYEE", underwriterId.toString()));
    }
    policyRelateVO.setConsentGivenIND(uwPolicyVO.getConsentGivenIndi());
    policyRelateVO.setConsentGivenDate(policyVO.getConsentGivenDate());
    policyRelateVO.setLCADate(uwPolicyVO.getLcaDate());
    List<CoverageVO> policyProducts = coverageCI.findByPolicyId(uwPolicyVO
        .getPolicyId());
    // get extra prem
    List<ExtraPremVO> extraVOs = new ArrayList<ExtraPremVO>();
    for (CoverageVO cvo : policyProducts) {
      if (cvo.getExtraPrems() != null && cvo.getExtraPrems().size() > 0) {
        extraVOs.addAll(cvo.getExtraPrems());
      }
    }
    if (extraVOs.size() > 0) {
      policyRelateVO.setExtraPremVOs(extraVOs);
    }
    float basePrem = 0;
    for (CoverageVO vo : policyProducts) {
      if (vo.getCurrentPremium().getSumAssured() != null) {
        basePrem = basePrem
            + vo.getCurrentPremium().getSumAssured().floatValue();
      }
      if (vo.getMasterId() == null) {
        policyRelateVO.setPaymentMethod(vo.getCurrentPremium()
            .getPaymentMethod());
        policyRelateVO.setPaymentMethodDesc(CodeTable.getCodeDesc("T_PAY_MODE",
            String.valueOf(vo.getCurrentPremium().getPaymentMethod())));
      }
    }
    policyRelateVO.setPaymentFrequency(policyVO.getInitialType());
    policyRelateVO.setPaymentFrequencyDesc(CodeTable.getCodeDesc(
        "T_CHARGE_MODE", policyVO.getInitialType()));
    policyRelateVO.setTotalInstallmentPrem(policyVO.getInstallPrem());
    policyRelateVO.setTotalInforcingPrem(policyVO.getInstallPrem());
    ProposalPremVO premVO = proposalCI.getNBPremInfo(policyId);
    if (premVO != null) {
      policyRelateVO.setServiceTax(premVO.getServiceTax());
      policyRelateVO.setHiPolicyFees(premVO.getHiPolicyFee());
      policyRelateVO.setStampDuty(premVO.getStampDuty());
      policyRelateVO.setMiscFee(premVO.getMiscFee());
    }
    float sBa = 0;
    sBa = cashCI.getSuspenseById(uwPolicyVO.getPolicyId(),
        Boolean.valueOf(true)).floatValue()
        + cashCI.getSuspenseById(uwPolicyVO.getPolicyId(),
            Boolean.valueOf(false)).floatValue();
    policyRelateVO.setSuspenseBalance(new BigDecimal(sBa));
    policyRelateVO.setBasicSumAssured(new BigDecimal(basePrem));
    policyRelateVO.setHealthWarrantyDate(policyVO.getHealthExpiryDate());
    // get benefit
    int k = 0;
    Collection productCo = getUwPolicyDS().findUwProductEntitis(underwriteId);
    UwBenefitUwDecisonCIVO[] benefits = new UwBenefitUwDecisonCIVO[productCo
        .size()];
    Iterator productIt = productCo.iterator();
    while (productIt.hasNext()) {
      UwBenefitUwDecisonCIVO benefit = new UwBenefitUwDecisonCIVO();
      UwProductVO productVO = (UwProductVO) productIt.next();
      benefit.setBenefit(productVO.getProductId());
      benefit.setBenefitName(CodeTable.getCodeDesc("V_PRODUCT_LIFE",
          String.valueOf(productVO.getProductId())));
      benefit.setUwDecision(productVO.getDecisionId());
      // benefit.setUwDecisionDesc(CodeTable.getCodeDesc("T_PRODUCT_DECISION",
      // String.valueOf(productVO.getDecisionId())));
      benefit.setUnderwritingDate(productVO.getUnderwriteTime());
      benefit.setAccidentClass(productVO.getJobClass1());
      benefit.setOccupationClass(productVO.getJobClass1());
      benefits[k] = benefit;
      // BeanUtils.copyProperties(benefits[k], benefit);
      if (productVO.getMasterId() == null) {
        policyRelateVO.setDecisionReason(productVO.getDecisionReason());
        if (productVO.getDecisionReason() != null) {
          policyRelateVO.setDecisionReasonDesc(CodeTable.getCodeDesc(
              "T_UW_DECISION_REASON", productVO.getDecisionReason()));
        }
      }
      k = k + 1;
    }
    policyRelateVO.setUwBenefitUwDecisonVOs(benefits);
    // get condition code
    List<com.ebao.ls.pa.pub.vo.PolicyConditionVO> conditionVO = policyVO
        .getPolicyConditions();
    if (null != conditionVO && 0 < conditionVO.size()) {
      policyRelateVO.setConditionVOs(conditionVO);
    }
    // get exclusion code
    List<com.ebao.ls.pa.pub.vo.PolicyExclusionVO> exclusionVO = policyVO
        .getPolicyExclusions();
    if (null != exclusionVO && 0 < exclusionVO.size()) {
      policyRelateVO.setExclusionVOs(exclusionVO);
    }
    // get endorsement code
    List<com.ebao.ls.pa.pub.vo.PolicyEndorsementVO> endorsementVO = policyVO
        .getPolicyEndorsements();
    if (null != endorsementVO && 0 < endorsementVO.size()) {
      policyRelateVO.setEndorsementVOs(endorsementVO);
    }
    // get slqvos
    // SlqCIVO[] slqVOs = documentCI.getSlqQueryInfo(uwPolicyVO.getPolicyId());
    // if (slqVOs != null) {
    // policyRelateVO.setSlqVOs(slqVOs);
    // }
    // return policy relate vo
    return policyRelateVO;
  }

  /*
   * (non-Javadoc)
   * @see
   * com.ebao.ls.uw.ci.UwProcessControlCI#getEmValuebyPolicyCodeAndInsuredId
   * (java.lang.String, java.lang.Long)
   */
  @PAPubAPIUpdate("getPolicyIdByPolicyNumber and change the parameter to policyId")
  public String getEmValuebyPolicyIdAndInsuredId(Long policyId, Long insuredId)
      throws GenericException {
    return getEmValuesByPolicyInfoAndInsuredId(policyId, insuredId);
    // modify end
  }

  /*
   * (non-Javadoc)
   * @see
   * com.ebao.ls.uw.ci.UwProcessControlCI#getUWStatusByPolicyIdAndChangeId(java
   * .lang.Long, java.lang.Long)
   */
  public String getUWStatusByPolicyIdAndChangeId(Long policyId, Long changeId)
      throws GenericException {
    return getUwProcessDS()
        .getUWStatusByPolicyIdAndChangeId(policyId, changeId);
  }

  /*
   * (non-Javadoc)
   * @see com.ebao.ls.uw.ci.UwProcessControlCI#cancelUW(java.lang.Long)
   */
  public void cancelUW(Long changeId) throws GenericException {
    UwQueryDao dao = new UwQueryDao();
    dao.updateReUwPolicyUwStatus(null, null, UwStatusConstants.CANCELLATION,
        null, changeId);
  }

  /*
   * (non-Javadoc)
   * @see
   * com.ebao.ls.uw.ci.UwProcessControlCI#findByPolicyIdAndUwStatusAndChangeId
   * (java.lang.Long, java.lang.String, java.lang.Long)
   */
  public UwPolicyCIVO[] findByPolicyIdAndUwStatusAndChangeId(
      java.lang.Long policyId, String uwStatus, Long changeId)
      throws GenericException {
    try {
      Collection uwPolicyList = BeanUtils.copyCollection(
          UwPolicyVO.class,
          getUwPolicyDS().findByPolicyIdAndUwStatusAndChangeId(policyId,
              uwStatus, changeId));
      return (UwPolicyCIVO[]) uwPolicyList.toArray(new UwPolicyVO[uwPolicyList
          .size()]);
    } catch (ObjectNotFoundException ex) {
      // do nothing
      return new UwPolicyCIVO[0];
    } catch (Exception ex) {
      throw ExceptionFactory.parse(ex);
    }
  }

  private String getEmValuesByPolicyInfoAndInsuredId(Long policyId,
      Long insuredId) throws GenericException {
    UwQueryDao dao = new UwQueryDao();
    Long oldUnderwriteId = dao.getUnfinishedNBUW(policyId);
    if (oldUnderwriteId != null) {
      return geEmValuesForUnFinishedUw(policyId, insuredId);
    } else {
      return findEmValuebyPolicyIdAndInsuredId(policyId, insuredId);
    }
  }

  private String findEmValuebyPolicyIdAndInsuredId(Long policyId, Long insuredId)
      throws GenericException {
    StringBuffer strBuf = new StringBuffer();
    List<CoverageVO> list = coverageCI.findByPolicyId(policyId);
    if (list != null) {
      List<Integer> emList = new ArrayList<Integer>();
      for (CoverageVO vo : list) {
        List<ExtraPremVO> extraList = vo.getExtraPrems();
        if (extraList != null) {
          for (ExtraPremVO extra : extraList) {
            if (insuredId.equals(extra.getInsuredId())) {
              emList.add(extra.getEmValue());
            }
          }
        }
        List<LiabilityReduceVO> reduceList = vo.getLiabilityReduces();
        if (reduceList != null) {
          for (LiabilityReduceVO reduce : reduceList) {
            if (vo.getItemId().equals(reduce.getItemId())) {
              emList.add(reduce.getEmValue());
            }
          }
        }
      }
      if (emList.size() > 0) {
        for (Integer em : emList) {
          if (strBuf.length() == 0) {
            strBuf.append(em);
          } else {
            strBuf.append(",").append(em);
          }
        }
      }
    }
    return strBuf.toString();
  }

  @PAPubAPIUpdate("getPolicyIdByPolicyNumber, and change the parameter to policyId and remove the policySerivce invoke")
  private String geEmValuesForUnFinishedUw(Long policyId, Long insuredId)
      throws GenericException {
    StringBuffer strEmValueIn = new StringBuffer();
    UwQueryDao dao = new UwQueryDao();
    Long underwriteId = dao.findLatestUnderwriteId(policyId);
    if (underwriteId == null) {// added for policy not do underwriting,by
      // robert.xu on 2007.1.11
      return "";
    }
    /* get em value from t_uw_extra_prem */
    Collection emValueColIn = dao.findEmValuebyUnderwriteIdAndInsuredId(
        underwriteId, insuredId);
    Iterator iter = emValueColIn.iterator();
    int i = 0;
    try {
      while (iter.hasNext()) {
        BigDecimal emValueIn = (BigDecimal) iter.next();
        if (emValueIn == null || emValueIn.intValue() == 0) {
          continue;
        }
        if (i == 0) {
          strEmValueIn.append(NumberFormat.format(emValueIn.floatValue()));
          i++;
        } else {
          strEmValueIn
              .append("," + NumberFormat.format(emValueIn.floatValue()));
          i++;
        }
      }
    } catch (Exception e) {
      throw ExceptionFactory.parse(e);
    }
    return strEmValueIn.toString();
  }
	
	/**
	 * <p>Description : 將T_UW_POLICY,T_UW_PRODUCT狀態改成取消</p>
	 * <p>Created By : Kate Hsiao</p>
	 * <p>Create Time : May 23, 2017</p>
	 * @param underwriteId
	 * @throws GenericException
	 */
	public void processToCancel(Long underwriteId) throws GenericException {
		if (null == underwriteId) {
			throw new IllegalArgumentException("underwriteId can't be null.");
		}
		getUwProcessDS().cancelUnderwriting(underwriteId);
	}
	/**
	 * <p>Description : 將T_UW_POLICY,T_UW_PRODUCT狀態改成無效</p>
	 * <p>Created By : Kate Hsiao</p>
	 * <p>Create Time : May 23, 2017</p>
	 * @param underwriteId
	 * @throws GenericException
	 */
	public void csExtraPremInvalid(Long underwriteId) throws GenericException {
		if (null == underwriteId) {
			throw new IllegalArgumentException("underwriteId can't be null.");
		}
		getUwProcessDS().updateUwStatus(underwriteId, UwStatusConstants.INVALID);
	}
	/**
	 * <p>Description : 將T_UW_POLICY,T_UW_PRODUCT狀態改成待核保</p>
	 * <p>Created By : Kate Hsiao</p>
	 * <p>Create Time : May 23, 2017</p>
	 * @param underwriteId
	 * @throws GenericException
	 */
	public void csExtraPremBack(Long underwriteId) throws GenericException {
		if (null == underwriteId) {
			throw new IllegalArgumentException("underwriteId can't be null.");
		}
		getUwProcessDS().updateUwStatus(underwriteId, UwStatusConstants.WAITING);
	}
	@Resource(name = PolicyAcknowCI.BEAN_DEFAULT)
	private PolicyAcknowCI policyAcknowCI;
	@Resource(name = CashCI.BEAN_DEFAULT)
	private CashCI cashCI;
	@Resource(name = CoverageCI.BEAN_DEFAULT)
	private CoverageCI coverageCI;
	@Resource(name = PolicyService.BEAN_DEFAULT)
	private PolicyService policyService;
}
