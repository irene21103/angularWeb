package com.ebao.ls.beneOwner.ctrl;

import java.math.BigDecimal;
import java.util.Date;

import com.ebao.pub.web.pager.PagerFormImpl;

public class BeneOwnerForm extends PagerFormImpl {

	private Long boPartyId;

	/** 可能為法人的registerCode，也可能為自然人的certiCode */
	private String beneOwnerCode;

	/** 實質受益人/高階管理人 */
	private String beneOwnerName;

	/** 法人實質受益人國籍 */
	private String beneOwnerNationality;

	/** 法人實質受益人國籍2 */
	private String beneOwnerNationality2;

	/** 自然人生日，法人無 */
	private Date birthday;

	/** 持有股份百分比 */
	private BigDecimal shares;

	/** 實質控制權人 */
	private String actualController;

	/** 職稱 */
	private String position;

	/** 是否法人 */
	private String isCompany;

	/** 實質受益人異動時間 */
	private Long bOUpdateBy;

	/** 實質受益人異動人員 */
	private Date bOUpdateTime;
	
	/** 身分別 */
	private String beneOwnerType;
	
	public Long getBoPartyId() {
		return boPartyId;
	}

	public void setBoPartyId(Long boPartyId) {
		this.boPartyId = boPartyId;
	}

	public String getBeneOwnerCode() {
		return beneOwnerCode;
	}

	public void setBeneOwnerCode(String beneOwnerCode) {
		this.beneOwnerCode = beneOwnerCode;
	}

	public String getBeneOwnerName() {
		return beneOwnerName;
	}

	public void setBeneOwnerName(String beneOwnerName) {
		this.beneOwnerName = beneOwnerName;
	}

	public String getBeneOwnerNationality() {
		return beneOwnerNationality;
	}

	public void setBeneOwnerNationality(String beneOwnerNationality) {
		this.beneOwnerNationality = beneOwnerNationality;
	}

	public String getBeneOwnerNationality2() {
		return beneOwnerNationality2;
	}

	public void setBeneOwnerNationality2(String beneOwnerNationality2) {
		this.beneOwnerNationality2 = beneOwnerNationality2;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public BigDecimal getShares() {
		return shares;
	}

	public void setShares(BigDecimal shares) {
		this.shares = shares;
	}

	public String getActualController() {
		return actualController;
	}

	public void setActualController(String actualController) {
		this.actualController = actualController;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getIsCompany() {
		return isCompany;
	}

	public void setIsCompany(String isCompany) {
		this.isCompany = isCompany;
	}

	public Long getbOUpdateBy() {
		return bOUpdateBy;
	}

	public void setbOUpdateBy(Long bOUpdateBy) {
		this.bOUpdateBy = bOUpdateBy;
	}

	public Date getbOUpdateTime() {
		return bOUpdateTime;
	}

	public void setbOUpdateTime(Date bOUpdateTime) {
		this.bOUpdateTime = bOUpdateTime;
	}

	public String getBeneOwnerType() {
		return beneOwnerType;
	}

	public void setBeneOwnerType(String beneOwnerType) {
		this.beneOwnerType = beneOwnerType;
	}

	@Override
	public String toString() {
		return "BeneOwnerForm [boPartyId=" + boPartyId + ", beneOwnerCode=" + beneOwnerCode + ", beneOwnerName="
				+ beneOwnerName + ", beneOwnerNationality=" + beneOwnerNationality + ", beneOwnerNationality2="
				+ beneOwnerNationality2 + ", birthday=" + birthday + ", shares=" + shares + ", actualController="
				+ actualController + ", position=" + position + ", isCompany=" + isCompany + ", bOUpdateBy="
				+ bOUpdateBy + ", bOUpdateTime=" + bOUpdateTime + ", beneOwnerType=" + beneOwnerType + "]";
	}

}
