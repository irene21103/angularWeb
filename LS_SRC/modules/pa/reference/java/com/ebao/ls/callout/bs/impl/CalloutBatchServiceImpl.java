package com.ebao.ls.callout.bs.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.dao.DataAccessException;

import com.ebao.foundation.common.lang.DateUtils;
import com.ebao.foundation.common.lang.exception.ExceptionUtils;
import com.ebao.foundation.commons.exceptions.ExceptionUtil;
import com.ebao.foundation.commons.exceptions.GenericException;
import com.ebao.foundation.core.logging.Log;
import com.ebao.ls.callout.bs.CalloutBatchService;
import com.ebao.ls.callout.data.CalloutBatchDao;
import com.ebao.ls.callout.data.bo.CalloutBatch;
import com.ebao.ls.callout.vo.CalloutBatchVO;
import com.ebao.ls.foundation.service.GenericServiceImpl;

public class CalloutBatchServiceImpl extends GenericServiceImpl<CalloutBatchVO, CalloutBatch, CalloutBatchDao> implements CalloutBatchService{
    
    private final Log log = Log.getLogger(CalloutBatchServiceImpl.class);
    
    private void syso(Object msg){
        log.info("[SYSO][BR-CMN-TFI-005]" + msg);
    }
    
    @Override
    protected CalloutBatchVO newEntityVO() {
        return new CalloutBatchVO();
    }
    
    @Resource(name = CalloutBatchDao.BEAN_DEFAULT)
    public void setDao(CalloutBatchDao dao) {
        super.setDao(dao);
    }

    @Override
    public void deleteByCalloutNum(String calloutNumList) {
        dao.deleteBycalloutNum(calloutNumList);
    }

    @Override
    public CalloutBatch save(String policyCode, String fileName, Long configType, Date processDate) throws DataAccessException {
        CalloutBatch entity = new CalloutBatch();
        entity.setPolicyCode(policyCode);
        entity.setCalloutFileName(fileName);
        entity.setBatchTime(processDate);
        entity.setConfigType(configType);
        this.dao.save(entity);
        return entity;
    }

    @Override
    public List<CalloutBatchVO> queryByCurrentMonth(Long configType, Date processDate) throws DataAccessException {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(processDate);
        calendar.set(Calendar.DAY_OF_MONTH,  calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
        java.util.Date firstDate = calendar.getTime();
        calendar.set(Calendar.DAY_OF_MONTH,  calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        java.util.Date endDate = DateUtils.addDay(calendar.getTime(), 1);
        List<CalloutBatchVO> resultList = new LinkedList<CalloutBatchVO>();
        try {
            resultList = dao.queryByCurrentMonth(configType, firstDate, endDate);
        } catch (Exception e) {
            throw ExceptionUtil.parse(e);
        }
        return resultList;
    }

    @Override
    public List<CalloutBatchVO> querySamplingMonthData(Long sampleType,
                    Date processDate) throws DataAccessException {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(processDate);
        calendar.set(Calendar.DAY_OF_MONTH,  calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
        java.util.Date firstDate = calendar.getTime();
        calendar.set(Calendar.DAY_OF_MONTH,  calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        java.util.Date endDate = DateUtils.addDay(calendar.getTime(), 1);
        List<CalloutBatchVO> resultList = new LinkedList<CalloutBatchVO>();
        try {
            resultList = dao.querySamplingMonthData(sampleType, firstDate, endDate);
        } catch (Exception e) {
            throw ExceptionUtil.parse(e);
        }
        return resultList;
    }

    @Override
    public void updateCalloutNumber(Long listId, String calloutNum)
                    throws DataAccessException {
        CalloutBatch data = dao.load(new Long(listId));
        data.setCalloutNum(calloutNum);
        dao.saveorUpdate(data);
    }

    @Override
    public Boolean isSampled(String policyCode, Long configType,
                    Date processDate) throws GenericException {
        try{
            return CollectionUtils.isNotEmpty(dao.isSampled(policyCode, configType, processDate));
        }catch(Exception ex){
            syso("[ERROR]" + ExceptionUtils.getFullStackTrace(ex));
            throw ExceptionUtil.parse(ex);
        }
    }

    @Override
    public CalloutBatchVO query(String policyCode,
                    Long sampleType, Date processDate)
                    throws DataAccessException {
        try{
            return dao.query(policyCode, processDate, sampleType);
        }catch(Exception ex){
            syso("[ERROR]" + ExceptionUtils.getFullStackTrace(ex));
            throw ExceptionUtil.parse(ex);
        }
    }

    @Override
    public List<CalloutBatchVO> queryEmptyCalloutNumOfMonth(Long sampleType, Date  processDate) throws DataAccessException {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(processDate);
        calendar.set(Calendar.DAY_OF_MONTH,  calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
        java.util.Date firstDate = calendar.getTime();
        calendar.set(Calendar.DAY_OF_MONTH,  calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        java.util.Date endDate = DateUtils.addDay(calendar.getTime(), 1);
        try {
            return  dao.queryEmptyCalloutNumOfMonth(sampleType, firstDate, endDate);
        } catch (Exception e) {
            throw ExceptionUtil.parse(e);
        }
    }

	@Override
	public List<CalloutBatchVO> queryUnSamplingData(Long sampleType, Date startDate, Date endDate)
			throws DataAccessException {
		try {
            return  dao.queryEmptyCalloutNumOfMonth(sampleType, startDate, endDate);
        } catch (Exception e) {
            throw ExceptionUtil.parse(e);
        }
	}

	@Override
	public Boolean isSampled(String policyCode, Long configType) throws GenericException {
		
		try{
            return CollectionUtils.isNotEmpty(dao.isSampled(policyCode, configType));
        }catch(Exception ex){
            syso("[ERROR]" + ExceptionUtils.getFullStackTrace(ex));
            throw ExceptionUtil.parse(ex);
        }
	}
}
