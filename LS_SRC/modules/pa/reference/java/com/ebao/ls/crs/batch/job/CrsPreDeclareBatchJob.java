package com.ebao.ls.crs.batch.job;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.transaction.UserTransaction;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.ebao.foundation.common.lang.exception.ExceptionUtils;
import com.ebao.foundation.commons.exceptions.ExceptionUtil;
import com.ebao.foundation.core.logging.Log;
import com.ebao.ls.batch.TGLBaseUnpieceableJob;
import com.ebao.ls.crs.batch.service.CrsBenefitSurveyListService;
import com.ebao.ls.crs.batch.service.CrsPreDeclareBatchService;
import com.ebao.ls.crs.batch.vo.CRSTempVO;
import com.ebao.ls.fatca.batch.data.FatcaBenefitSurveyListDAO;
import com.ebao.ls.fatca.constant.FatcaSurveyType;
import com.ebao.ls.fatca.vo.FatcaDueDiligenceListVO;
import com.ebao.ls.pub.batch.impl.BatchJdbcTemplate;
import com.ebao.ls.pub.batch.impl.DataSourceWrapper;
import com.ebao.pub.batch.help.BatchHelp;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.para.Para;
import com.ebao.pub.util.TransUtils;

/**
 * CRS提取受益人調查對象，產出報表用數據，並執行建檔
 */
public class CrsPreDeclareBatchJob extends TGLBaseUnpieceableJob {

	public static final String BEAN_DEFAULT = "crsPreDeclareBatchJob";
	
	 private final Log logger = Log.getLogger(CrsPreDeclareBatchJob.class);

	 private static BatchJdbcTemplate batchJdbcTemplate = new BatchJdbcTemplate(new DataSourceWrapper());

	 private static NamedParameterJdbcTemplate namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(
	   batchJdbcTemplate);

	@Override
	protected String getJobName() {
		return BatchHelp.getJobName();
	}

	@Resource(name = CrsBenefitSurveyListService.BEAN_DEFAULT)
	private CrsBenefitSurveyListService crsBenefitSurveyListService;

	@Resource(name = FatcaBenefitSurveyListDAO.BEAN_DEFAULT)
	private FatcaBenefitSurveyListDAO fatcaBenefitSurveyListDAO;
	
	@Resource(name = CrsPreDeclareBatchService.BEAN_DEFAULT)
	 private CrsPreDeclareBatchService crsPreDeclareBatchService;

	/**
	 * 版本號
	 */
	private static final String VERSION = "0.24";

	/**
	 * 業務日期
	 */
	private Date processDate = null;

	/**
	 * 寫info log
	 * 
	 * @param layout
	 * @param msg
	 */
	private void log(String msg) {
		logger.info("[SYSOUT][CrsBenefitSurveyListTask] " + msg);
		batchLogger.info(msg);
	}

	/**
	 * 批次進入點
	 */
	@Override
	public int processJob() throws Exception {
		log("processJob - start");
		log("VERSION = " + VERSION);

		if (!isMock()) {
			processDate = BatchHelp.getProcessDate();
		} else {
			log("測試模式");
			if (processDate == null) {
				throw new IllegalArgumentException("processDate is null");
			}
		}
		try {
			// PCR330030  CRS確認申報國作業
			process(processDate);
		} catch (Exception ex) {
			log(ExceptionUtils.getFullStackTrace(ex));
			throw ExceptionUtil.parse(ex);
		}
		log("processJob - end");
		if (!isMock()) {
			return getExecuteStatus();
		} else {
			log("成功筆數 = " + getBatchContext().getSuccessCount());
			log("失敗筆數 = " + getBatchContext().getErrorCount());
			return getExecuteStatus(getBatchContext().getSuccessCount(), getBatchContext().getErrorCount());
		}
	}

	private void process(Date processDate) throws Exception {

		List<String> finalResult = new ArrayList<String>();
		// 1.排除退出CRS申報國
		String dueDay = Para.getParaValue(2061000051L);
		StringBuilder sql1 = new StringBuilder();
		sql1.append(" select distinct tcp.crs_id || '/' || 1 ");
		sql1.append(" from t_crs_party tcp, t_crs_res_tin tin ");
		sql1.append(" where nvl(tcp.Crs_Announce_Status,'N') in ('N-1','N-2','N-3') ");
		sql1.append(" and tcp.crs_id = tin.crs_id ");
		sql1.append(" and nvl(tin.RES_COUNTRY,'ZZ') not in (select value from t_crs_foreign_indi where type='F') ");

		
		MapSqlParameterSource params1 = new MapSqlParameterSource();
		List<String> result1 = namedParameterJdbcTemplate.queryForList(sql1.toString(), params1, String.class);
		// 2.檢核申報國並產生信函
		StringBuilder sql2 = new StringBuilder();
		sql2.append(" select distinct tcp.crs_id || '/' || 2 ");
		sql2.append(" from t_crs_party tcp, t_crs_res_tin tin ");
		sql2.append(" where nvl(tcp.Crs_Announce_Status,'N') in ('N-0','N-1','N-2') ");
		sql2.append(" and tcp.build_type = '01' ");
		sql2.append(" and tcp.crs_id = tin.crs_id ");
		sql2.append(" and nvl(tin.RES_COUNTRY,'ZZ') in (select value from t_crs_foreign_indi where type='F') ");

		MapSqlParameterSource params2 = new MapSqlParameterSource();
		List<String> result2 = namedParameterJdbcTemplate.queryForList(sql2.toString(), params2, String.class);
		if (!(CollectionUtils.isEmpty(result1)) && !(CollectionUtils.isEmpty(result2))) {
			result1.addAll(result2);
			finalResult.addAll(result1);
		} else if (CollectionUtils.isEmpty(result1) && !(CollectionUtils.isEmpty(result2))) {
			finalResult.addAll(result2);
		} else if (!(CollectionUtils.isEmpty(result1)) && CollectionUtils.isEmpty(result2)) {
			finalResult.addAll(result1);
		} 
		
		UserTransaction trans = null;
		if(!(CollectionUtils.isEmpty(finalResult))){
			log("符合申報國條件對象總筆數 finalResult.size() = " + finalResult.size());
			for (String crsIdAndFlag : finalResult) {
				try {
					String[] crsString = crsIdAndFlag.split("/");
					String crsId = crsString[0];
					String flag = crsString[1];
					trans = TransUtils.getUserTransaction();
					trans.begin();
						crsPreDeclareBatchService.createCrsRecord(crsId, flag, AppContext.getCurrentUser().getUserId(), processDate);
					incSuccessCount();
					trans.commit();
				} catch (Exception ex) {
					log("發生異常");
					log(ExceptionUtils.getFullStackTrace(ex));
					incErrorCount();
					trans.rollback();
				} finally {
					trans = null;
				}
			}
		}
	}

	public void setProcessDate(Date date) {
		this.processDate = date;
	}
}
