package com.ebao.ls.uw.ctrl.extraloading;

import java.util.ArrayList;
import java.util.List;

import com.ebao.ls.uw.ctrl.pub.UwExtraLoadingForm;
import com.ebao.ls.uw.ctrl.pub.UwProductGenericForm;

/**
 * <p>Title: 核保加費</p>
 * <p>Description: 核保加費</p>
 * <p>Copyright: Copyright (c) 2015</p>
 * <p>Company: TGL Co., Ltd.</p>
 * <p>Create Time: Jul 15, 2015</p> 
 * @author 
 * <p>Update Time: Jul 15 2015</p>
 * <p>Updater: simon.huang</p>
 * <p>Update Comments: </p>
 */
public class UwProductExtraPremForm extends UwProductGenericForm {

	private UwExtraLoadingForm health;
	
	private UwExtraLoadingForm occupation;
	
	private String subAction;
	
	/* 保全變更申請ID */
	private Long changeId;
	
	/** 來源為保全核保 */
	private boolean isCsUw;
	
	/**
	 * @return 傳回 health。
	 */
	public UwExtraLoadingForm getHealth() {
		return health;
	}

	/**
	 * @param health 要設定的 health。
	 */
	public void setHealth(UwExtraLoadingForm health) {
		this.health = health;
	}

	/**
	 * @return 傳回 occupation。
	 */
	public UwExtraLoadingForm getOccupation() {
		return occupation;
	}

	/**
	 * @param occupation 要設定的 occupation。
	 */
	public void setOccupation(UwExtraLoadingForm occupation) {
		this.occupation = occupation;
	}

	public String getSubAction() {
		return subAction;
	}

	public void setSubAction(String subAction) {
		this.subAction = subAction;
	}
	
	/**
	 * <p>Description :保全變更申請ID </p>
	 * <p>Created By : TGL144</p>
	 * <p>Create Time : Sep 10, 2016</p>
	 * @return
	 */
	public Long getChangeId() {
		return changeId;
	}

	/**
	 * <p>Description :保全變更申請ID </p>
	 * <p>Created By : TGL144</p>
	 * <p>Create Time : Sep 10, 2016</p>
	 * @param changeId
	 */
	public void setChangeId(Long changeId) {
		this.changeId = changeId;
	}
	/**
	 * <p>Description :來源為保全核保 </p>
	 * <p>Created By : TGL144</p>
	 * <p>Create Time : Jan 9, 2017</p>
	 * @return
	 */
	public boolean isCsUw() {
		return isCsUw;
	}
	/**
	 * <p>Description :來源為保全核保 </p>
	 * <p>Created By : TGL144</p>
	 * <p>Create Time : Jan 9, 2017</p>
	 * @param isCsUw
	 */
	public void setCsUw(boolean isCsUw) {
		this.isCsUw = isCsUw;
	}
}
