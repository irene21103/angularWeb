package com.ebao.ls.uw.ctrl.decision;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.UserTransaction;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.ls.pa.pub.ci.CoverageCI;
import com.ebao.ls.pa.pub.ref.api.prd.ProductService;
import com.ebao.ls.pa.pub.ref.vo.prd.ProductVO;
import com.ebao.ls.pa.pub.service.PolicyService;
import com.ebao.ls.pa.pub.vo.CoverageVO;
import com.ebao.ls.pa.pub.vo.SingleTopupVO;
import com.ebao.ls.pub.CodeCst;
import com.ebao.ls.uw.bo.UwCalculation;
import com.ebao.ls.uw.ds.UwPolicyService;
import com.ebao.ls.uw.ds.UwProcessService;
import com.ebao.ls.uw.ds.constant.UwStatusConstants;
import com.ebao.ls.uw.ds.sp.CompleteUWProcessSp;
import com.ebao.ls.uw.ds.sp.QueryUwDataSp;
import com.ebao.ls.uw.util.Utils;
import com.ebao.ls.uw.vo.UwExtraLoadingVO;
import com.ebao.ls.uw.vo.UwLienVO;
import com.ebao.ls.uw.vo.UwPolicyVO;
import com.ebao.ls.uw.vo.UwProductVO;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.framework.AppException;
import com.ebao.pub.framework.ExceptionFactory;
import com.ebao.pub.framework.GenericAction;
import com.ebao.pub.framework.GenericException;
import com.ebao.pub.framework.MultiWarning;
import com.ebao.pub.util.BeanUtils;
import com.ebao.pub.util.StringUtils;
import com.ebao.pub.util.Trans;
import com.ebao.pub.util.TransUtils;

/**
 * Amend to inherited from UwGenericAction
 * <p>
 * Title:Underwriting
 * </p>
 * <p>
 * Description:
 * </p>
 * <p>
 * Copyright: Copyright (c) 2004
 * </p>
 * <p>
 * Company: eBaoTech Corporation
 * </p>
 * 
 * @author mingchun.shi
 * @since Jun 27, 2005
 * @version 1.0
 */
public class UwDecisionSaveAction extends GenericAction {
  /** 發現留置權資訊，加費要重新計算 */
  public static final long APP_RECAL_PREM = 20510020022L;
  /** 無效的核保資訊選擇 */
  public static final long APP_DECISION = 20510020023L;
  /** 當結論是接受時，留置權資訊將被刪除*/
  public static final long APP_DELE_LIEN = 20520120025L;
  /** 當結論是接受時，限制保障資訊將被刪除*/
  public static final long APP_DELE_REST = 20520120024L;
  /** 對於此決定無許可權*/
  public static final long APP_NO_RIGHT = 20520120026L;
 // public static final long APP_NO_PERF_NO = 20520120029L;
  /** 這個產品需要職業等級及事故等級來計算保險費率*/
  public static final long APP_NEED_OCCUPATION_CLASS = 20510020018L;
  /** 在原始核保決定時不存在原始期 */
  public static final String NOT_ALLOW_ORINGIN_TERM = "ERR_20510020017";
  /** 險種不能有續期屬性*/
  public static final String APP_NOT_ALLOW_RENEW = "ERR_20520120031";
  // add by hanzhong.yan for defect:GEL00039205 2008/2/25
  /** 發現不同的核保決定，請個別更新核保決定*/
  public static final long APP_INCONSISTENT_UW_DECISION = 20510020026L;
  /* Indexation **/
  /** The index indicator can't be changed from W to Y or N to Y*/
  public static final long APP_INDX_INDI_CANT_FORM_WN_TO_Y = 20510020027L;
  /** The index indicator can't be changed from W to N*/
  public static final long APP_INDX_INDI_CANT_FORM_W_TO_N = 20510020117L;
  /** The index indicator can't be changed from Y to W or N to W*/
  public static final long APP_INDX_INDI_CANT_FORM_YN_TO_W = 20510020118L;
  
  public static final String BEAN_DEFAULT = "/uw/uwDecisionSave";

  /**
   * process warning
   * 
   * @param mapping
   * @param form
   * @param request
   * @param response
   * @return
   * @throws Exception
   */
  @Override
  public MultiWarning processWarning(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {
    UwProductInfoForm productInfoForm = (UwProductInfoForm) form;
    MultiWarning warning = new MultiWarning();
    // get value from page
    Long underwriteId = Long.valueOf(request.getParameter("underwriteId"));
    String[] itemIds = request.getParameterValues("itemIds");
   
    Long underwriterId = Long.valueOf(AppContext.getCurrentUser().getUserId());
    String sourceType = request.getParameter("uwSourceType");
    // check decision
    UserTransaction trans = Trans.getUserTransaction();
    try {
      trans.begin();
      int previousBenefitDecision = 0;// initial is zero
      boolean exitDiffDecisionAlongMultiBene = false;// idicate wheather there
      // is diff decision along
      // multi-bene
      for (String itemId : itemIds) {
        int currentBenefitDecision = checkUwDecisionIndividually(request,
            productInfoForm, warning, underwriteId, underwriterId,
            sourceType, Long.valueOf(itemId));
        /* mark by simon.huang on 2015-07-01,不需判斷核保決定, 關閉相關檢核
        if (previousBenefitDecision != 0
            && currentBenefitDecision != previousBenefitDecision) {
          // display warning:Different underwriting decision found. Please
          // update underwriting decision individually.
          exitDiffDecisionAlongMultiBene = true;
        }
        previousBenefitDecision = currentBenefitDecision;
        */
      }
      if (exitDiffDecisionAlongMultiBene) {
        // if the decision is not consistent , then only display
        // "please make decision individually";
        warning.getWarnings().clear();
        warning.addWarning("ERR_" + APP_INCONSISTENT_UW_DECISION);
        warning.setContinuable(false);
      }
      if (!warning.isEmpty()) {
        trans.rollback();
        return warning;
      }
      trans.commit();
    } catch (Exception e) {
      TransUtils.rollback(trans);
      throw ExceptionFactory.parse(e);
    }
    return warning;
  }

  private int checkUwDecisionIndividually(HttpServletRequest request,
      UwProductInfoForm productInfoForm, MultiWarning warning,
      Long underwriteId, Long underwriterId, String sourceType,
      Long itemId) throws Exception {
    UwProductVO productVO = uwPolicyDS.findUwProduct(underwriteId, itemId);
    // check authority
    int finalDecision = 0;
    
    /* mark by simon.huang on 2015-07-01,不需判斷核保決定, 關閉相關檢核
    //processCheckAuthority(request, productInfoForm, warning, underwriteId,
    //    decisionId, underwriterId, sourceType, itemId);
    */
    
    // check whether it should re-computer premium
    processReCompute4LienOrExtraLoading(request, warning, underwriteId, itemId,
        productVO, productInfoForm);
    // check insured status
    // processCheckInsuredStatus(request, warning, productVO);
    
    /* mark by simon.huang on 2015-07-01,不需判斷核保決定, 關閉相關檢核
    // check whether unstandard decision consist with items
    //processCheckDecision(request, warning, underwriteId, itemId, decisionId,
    //    productVO, productInfoForm);
     */
    
    /* mark by simon.huang on 2015-07-01,畫面已經沒有職業等級, 關閉相關檢核 
    // check occupation class
    processCheckOccuption(request, productInfoForm, warning, productVO);
    */
    
    /* mark by simon.huang on 2015-07-01,不需判斷核保決定, 關閉相關檢核
    // check whether it can do sub-standard operation
    finalDecision = processCheckdelLienorRC(request, warning, underwriteId,
        itemId, decisionId, productVO, productInfoForm);
    */
    return finalDecision;
  }

  /**
   * check whether the policy can be imposed with restrict/lien/extra loading
   * 
   * @param request
   * @param warning
   * @param underwriteId
   * @param itemId
   * @param decisionId
   * @param productVO
   * @throws GenericException
   * @return return the final decision
   */
  protected int processCheckdelLienorRC(HttpServletRequest request,
      MultiWarning warning, Long underwriteId, Long itemId, int decisionId,
      UwProductVO productVO, UwProductInfoForm productInfoForm)
      throws GenericException {
    int finalDecisionId = decisionId;
    if (decisionId == CodeCst.PRODUCT_DECISION__ORIGINAL
        || decisionId == CodeCst.PRODUCT_DECISION__STANDARD) {
      finalDecisionId = convertUwDecision(decisionId, underwriteId, itemId);
    }
    if (finalDecisionId == CodeCst.PRODUCT_DECISION__ACCEPTED
        && !"lien".equals(request.getParameter("reAction"))
        && !"extra".equals(request.getParameter("reAction"))
        && !"restrict".equals(request.getParameter("reAction"))) {
      if (!uwPolicyDS.findUwLienEntitis(underwriteId, itemId).isEmpty()) {
        warning.addWarning("ERR_" + APP_DELE_LIEN);
        warning.setContinuable(true);
      }
      if (isRestrictCoverImposed(underwriteId, itemId, productVO,
          productInfoForm)) {
        warning.addWarning("ERR_" + APP_DELE_REST);
        warning.setContinuable(true);
      }
    }
    // add by hanzhong.yan for defect GEL00039205 2008/2/25
    boolean isSubstandard = isSubStandLife(warning, underwriteId, itemId,
        decisionId, productVO, productInfoForm);
    if (isSubstandard && CodeCst.PRODUCT_DECISION__ACCEPTED == finalDecisionId) {
      finalDecisionId = CodeCst.PRODUCT_DECISION__CONDITION;
    }
    if (!isSubstandard
        && CodeCst.PRODUCT_DECISION__CONDITION == finalDecisionId) {
      finalDecisionId = CodeCst.PRODUCT_DECISION__ACCEPTED;
    }
    return finalDecisionId;
  }

  /**
   * check whether occupation class exists
   * 
   * @param request
   * @param productInfoForm
   * @param warning
   * @param productVO
   * @throws GenericException
   */
  private void processCheckOccuption(HttpServletRequest request,
      UwProductInfoForm productInfoForm, MultiWarning warning,
      UwProductVO productVO) throws GenericException {
    /*
     * if the premium calculate requires occupation class,we need check it only
     * when single premium
     */
    if (isSingleBenefit(productInfoForm)) {
      if (this.getProductService()
          .getProductByVersionId(Long.valueOf(productVO.getProductId().intValue()),
        		  productVO.getProductVersionId())
          .isJobDiff()) {
        // to judge whether these is occupationClass2
        String occupation2 = request.getParameter("occupation2");
        if (StringUtils.isNullOrEmpty(request.getParameter("occupation1"))
            || (occupation2 != null && occupation2.trim().length() == 0) /*
             * &&
             * class2ExistFlag
             */) {
          warning.addWarning("ERR_" + APP_NEED_OCCUPATION_CLASS);
          warning.setContinuable(false);
        }
      }
    }
  }

  /**
   * to check whether the decision consist with extra loading,restrict cover and
   * lien information modified by hanzhong.yan 2008/2/25 of
   * "return the final decision"
   * 
   * @param request
   * @param warning
   * @param underwriteId
   * @param itemId
   * @param decisionId
   * @throws GenericException
   */
  protected int processCheckDecision(HttpServletRequest request,
      MultiWarning warning, Long underwriteId, Long itemId, int decisionId,
      UwProductVO productVO, UwProductInfoForm productInfoForm)
      throws GenericException {
    int finalDecisionId = decisionId;
    if (!"lien".equals(request.getParameter("reAction"))
        && !"extra".equals(request.getParameter("reAction"))
        && !"restrict".equals(request.getParameter("reAction"))) {
      boolean isExtraLoadingImposed = isSubStandardExtraLoadngImposed(warning,
          underwriteId, itemId, decisionId);
      boolean isRestrictCoverImposed = isRestrictCoverImposed(underwriteId,
          itemId, productVO, productInfoForm);
      boolean isLienImposed = false;
      Collection lienC = uwPolicyDS.findUwLienEntitis(underwriteId, itemId);
      if (lienC.size() > 0) {
        isLienImposed = true;
      }
      // if any substandard term has been imposed,then substandard
      boolean isSubstandard = isExtraLoadingImposed || isRestrictCoverImposed
          || isLienImposed;
      // int finalDecisionId = decisionId;
      if (decisionId == CodeCst.PRODUCT_DECISION__ORIGINAL
          || decisionId == CodeCst.PRODUCT_DECISION__STANDARD) {
        finalDecisionId = convertUwDecision(decisionId, underwriteId, itemId);
      }
      if ((isSubstandard && CodeCst.PRODUCT_DECISION__ACCEPTED == finalDecisionId)
          || (!isSubstandard && CodeCst.PRODUCT_DECISION__CONDITION == finalDecisionId)) {
        warning.addWarning("ERR_" + APP_DECISION);
        warning.setContinuable(false);
      }
      // add by hanzhong.yan for defect GEL00039205 2008/2/25
      if (isSubstandard
          && CodeCst.PRODUCT_DECISION__ACCEPTED == finalDecisionId) {
        finalDecisionId = CodeCst.PRODUCT_DECISION__CONDITION;
      }
      if (!isSubstandard
          && CodeCst.PRODUCT_DECISION__CONDITION == finalDecisionId) {
        finalDecisionId = CodeCst.PRODUCT_DECISION__ACCEPTED;
      }
    }
    return finalDecisionId;
  }

  protected boolean isSubStandLife(MultiWarning warning, Long underwriteId,
      Long itemId, int decisionId, UwProductVO productVO,
      UwProductInfoForm productInfoForm) throws GenericException {
    boolean isExtraLoadingImposed = isSubStandardExtraLoadngImposed(warning,
        underwriteId, itemId, decisionId);
    boolean isRestrictCoverImposed = isRestrictCoverImposed(underwriteId,
        itemId, productVO, productInfoForm);
    boolean isLienImposed = false;
    Collection lienC = uwPolicyDS.findUwLienEntitis(underwriteId, itemId);
    if (lienC.size() > 0) {
      isLienImposed = true;
    }
    // if any substandard term has been imposed,then substandard
    boolean isSubstandard = isExtraLoadingImposed || isRestrictCoverImposed
        || isLienImposed;
    return isSubstandard;
  }

  /**
   * to check whether substandard extra loading is imposed
   * 
   * @param warning
   * @param underwriteId
   * @param itemId
   * @param decisionId
   * @throws GenericException
   */
  protected boolean isSubStandardExtraLoadngImposed(MultiWarning warning,
      Long underwriteId, Long itemId, int decisionId) throws GenericException {
    boolean isSubStandard = false;
    Collection heExtraC = null;
    heExtraC = uwPolicyDS.findUwExtraLoadingEntitis(underwriteId, itemId);
    // to check whether it has substandare terms
    if (heExtraC.isEmpty() == false) {
      /*
       * modified by hanzhong.yan for cq:GEL00035519 2007/11/5 isSubStandard =
       * true;
       */
      for (Iterator iter = heExtraC.iterator(); iter.hasNext();) {
        UwExtraLoadingVO element = (UwExtraLoadingVO) iter.next();
        if (isEffectiveHE(element)) {
          isSubStandard = true;
          break;
        }
      }
    }
    return isSubStandard;
  }

  protected boolean isEffectiveHE(UwExtraLoadingVO extraLoading)
      throws GenericException {
    boolean isEffective = false;
    SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
    long endTime = 0l;
    long currentDate = 0l;
    try {
      endTime = format.parse(format.format(extraLoading.getEndDate()))
          .getTime();
      currentDate = format.parse(
          format.format(AppContext.getCurrentUserLocalTime())).getTime();
      if (currentDate < endTime) {
        isEffective = true;
      } else {
        isEffective = false;
      }
      return isEffective;
    } catch (ParseException e) {
      throw ExceptionFactory.parse(e);
    }
  }

  /**
   * to check product decision
   * 
   * @param request
   * @param warning
   * @param underwriteId
   * @param itemId
   * @throws GenericException
   */
  private int processCheckAuthority(HttpServletRequest req,
      UwProductInfoForm productInfoForm, MultiWarning warning,
      Long underwriteId, int decisionId, Long underwriterId, String sourceType,
      Long itemId) throws GenericException {
    // check uw decision
    boolean isAuthority = true;
    // for (int i = 0; i < productInfoForm.getItemIds().length; i++) {
    int finalDecisionId = decisionId;
    // if decisionId = orginal/standard,it should be converted
    if (decisionId == CodeCst.PRODUCT_DECISION__ORIGINAL
        || decisionId == CodeCst.PRODUCT_DECISION__STANDARD) {
      finalDecisionId = convertUwDecision(decisionId, underwriteId, itemId);
    }
    // if decisionId = 0,it means no previous decision,then prompt a error
    if (finalDecisionId == 0) {
      warning.addWarning(NOT_ALLOW_ORINGIN_TERM);
      warning.setContinuable(false);
      return decisionId;
    }
    if (!isAuthority) {
      warning.addWarning("ERR_" + APP_NO_RIGHT);
      productInfoForm.setUwStatus(CodeCst.UW_STATUS__ESCALATED);
      warning.setContinuable(true);
    } else {
      productInfoForm.setUwStatus(CodeCst.UW_STATUS__FINISHED);
    }
    // put it in session,get it when process
    req.setAttribute("isAuthority", Boolean.valueOf(isAuthority));
    return finalDecisionId;
  }

  /**
   * when these is lien/extra loading and restrict cover , it should prompt
   * recal premium
   * 
   * @param request
   * @param warning
   * @param underwriteId
   * @param itemId
   * @throws GenericException
   */
  private void processReCompute4LienOrExtraLoading(HttpServletRequest request,
      MultiWarning warning, Long underwriteId, Long itemId,
      UwProductVO productVO, UwProductInfoForm productForm)
      throws GenericException {
    if (isRestrictCoverImposed(underwriteId, itemId, productVO, productForm)) {
      // check lien info
      Collection lienC = uwPolicyDS.findUwLienEntitis(underwriteId, itemId);
      if (lienC.size() > 0) {
        warning.addWarning("ERR_" + APP_RECAL_PREM);
        return;
      }
      Collection extraC = uwPolicyDS.findUwExtraLoadingEntitis(underwriteId,
          itemId);
      // check extraLoading
      if (extraC.size() > 0) {
        warning.addWarning("ERR_" + APP_RECAL_PREM);
        return;
      }
    }
  }

  /**
   * to check whether restrict cover has been imposed
   * 
   * @param underwriteId
   * @param itemId
   * @return
   * @throws GenericException
   */
  protected boolean isRestrictCoverImposed(Long underwriteId, Long itemId,
      UwProductVO productVO, UwProductInfoForm productInfoForm)
      throws GenericException {
    // if charge year has reduced,then true
    if (productVO.getUwChargeYear() != null) {
      if (productVO.getUwChargeYear().compareTo(productVO.getChargeYear()) < 0) {
        return true;
      }
    }
    // if coverage year has reduced,then true
    if (productVO.getUwCoverageYear() != null) {
      if (productVO.getUwCoverageYear().compareTo(productVO.getCoverageYear()) < 0) {
        return true;
      }
    }
    // if amount has reduced,then true
    if (productVO.getReducedAmount() != null
        && productVO.getReducedAmount().compareTo(productVO.getAmount()) < 0) {
      return true;
    }
    // if ma factor has reduced,then true
    if (productInfoForm.getMaFactor() != null
        && productVO.getSaFactor() != null) {
      if (new BigDecimal(productInfoForm.getMaFactor()).compareTo(productVO
          .getSaFactor()) < 0) {
        return true;
      }
    }
    // if ms factor has reduced,then true
    BigDecimal originalMsFactor = getOriginalMSFactor(underwriteId, itemId,
        Long.valueOf(productVO.getProductId().intValue()));
    if (productInfoForm.getMsFactor() != null && originalMsFactor != null) {
      if (new BigDecimal(productInfoForm.getMsFactor())
          .compareTo(originalMsFactor) < 0) {
        return true;
      }
    }
    // if unit has reduced,then true
    if (productVO.getReducedUnit() != null
        && productVO.getReducedUnit().compareTo(productVO.getUnit()) < 0) {
      return true;
    }
    
    // if benefit level has reduced,then true
    if (!StringUtils.isNullOrEmpty(productVO.getReducedLevel())
        && productVO.getReducedLevel().compareTo(productVO.getBenefitLevel()) < 0) {
      return true;
    }
    
    return false;
  }

  /**
   * get origianl ms factor from t_add_invest
   * 
   * @param underwriteId
   * @param itemId
   * @param productId
   * @return
   * @throws GenericException
   */
  private BigDecimal getOriginalMSFactor(Long underwriteId, Long itemId,
      Long productId) throws GenericException {
    boolean isMsFactor = false;
    BigDecimal msFactor = new BigDecimal(0);
    CoverageVO coverageVO = coverageCI.retrieveByItemId(itemId);
    ProductVO showVO = this.getProductService().getProductByVersionId(productId,
    		coverageVO.getProductVersionId());
    if (showVO.isMsFactor()) {
      isMsFactor = true;
    }
    // get ms factor
    if (isMsFactor) {
      SingleTopupVO addInvestVO = coverageVO.getSingleTopup();
      if (addInvestVO != null && addInvestVO.getSaFactor() != null) {
        msFactor = addInvestVO.getSaFactor();
      } else {
        isMsFactor = false;
      }
    }
    return msFactor;
  }

  /**
   * save the uw decision
   * 
   * @param mapping
   * @param form
   * @param req
   * @param res
   * @return
   * @throws GenericException
   */
  @Override
  public ActionForward process(ActionMapping mapping, ActionForm form,
      HttpServletRequest req, HttpServletResponse res) throws GenericException {
      UwProductInfoForm productInfoForm = (UwProductInfoForm) form;

      Long underwriteId = Long.valueOf(req.getParameter("underwriteId"));
      Long itemId = Long.valueOf(req.getParameter("itemIds"));
      UserTransaction trans = null;
      try {
        trans = Trans.getUserTransaction();
        trans.begin();
      
        if (isSingleBenefit(productInfoForm)) {
            
          //mark by simon.huang on 2015-07-01
          // convert to proper decisionId
          // productInfoForm.setDecisionId(Integer.valueOf(convertUwDecision(
           //                productInfoForm.getDecisionId(), underwriteId, itemId)));
          // recal extra loading or lien
          //UwProductVO productVO = uwPolicyDS.findUwProduct(underwriteId, itemId);
          //reCalLien(underwriteId, itemId, productVO, productInfoForm);
          
          //mark by simon.huang on 2015-07-01
          // delete the lien and restric information when decision is accept
          //doLienAndReCDelete(underwriteId, itemId,
          //    productInfoForm.getDecisionId());
          
          //mark by simon.huang on 2015-07-01
          //productInfoForm.setDecisionId(Integer.valueOf(decisionId));
          // add by hanzhong.yan for defect GEL00028431 2007/7/23
          productInfoForm.setUnderwriterId(Long.valueOf(AppContext
              .getCurrentUser().getUserId()));
          // add end
          try {
            if (!StringUtils.isNullOrEmpty(productInfoForm
                .getIndxIndicator_decision())) {
              resetIndxIndicator(itemId,
                  productInfoForm.getIndxIndicator_decision());
            }
          } catch (AppException appe) {
            TransUtils.rollback(trans);
            throw appe;
          }
         
          storeUWDecision(req, productInfoForm);
        } else {
          // multi items been selected
          boolean isMasterAutoRenew = getMasterRenew(productInfoForm
              .getUnderwriteId());
          for (int i = 0; i < productInfoForm.getItemIds().length; i++) {
            Long subItemId = productInfoForm.getItemIds()[i];
            /*
             * If there are products need to be updated then we must load the
             * record from db first and update the value which are not null
             */
            UwProductVO upvo = uwPolicyDS.findUwProduct(
                productInfoForm.getUnderwriteId(), subItemId);
            // recal extra loading or lien
            reCalLien(underwriteId, subItemId, upvo, productInfoForm);
            // convert to proper decisionId
            //productInfoForm.setDecisionId(Integer.valueOf(convertUwDecision(
            //    decisionId, underwriteId, subItemId)));
            // set ma/ms factor
            //if (productInfoForm.getMaFactor() != null) {
            //  upvo.setUwsafactor(new BigDecimal(productInfoForm.getMaFactor()));
            //}
            //if (productInfoForm.getMsFactor() != null) {
            //  upvo.setUwmsfactor(new BigDecimal(productInfoForm.getMsFactor()));
            //}
            // the decline or postpone reason, will only be captured on the first
            // benefit.
            //upvo.setDecisionId(Integer.valueOf(decisionId));
            //pvo.setDecisionDesc2(productInfoForm.getDecisionDesc2() != null ? productInfoForm
            //    .getDecisionDesc2() : null);
            // description only be stored in first product
            /*
            if (null != productInfoForm.getDecisionId()) {
              if (CodeCst.PRODUCT_DECISION__DECLINED == productInfoForm
                  .getDecisionId().intValue()
                  || CodeCst.PRODUCT_DECISION__POSTPONED == productInfoForm
                      .getDecisionId().intValue()) {
                if (0 == i) {
                  upvo.setDecisionReason(req.getParameter("decisionReason"));
                  upvo.setDecisionReasonDesc(req
                      .getParameter("decisionReasonDesc"));
                } else {
                  upvo.setDecisionReason("");
                  upvo.setDecisionReasonDesc("");
                }
              }
            }
            */
            // get previous uw status from DB when process warning
            // UwProductVO vo =
            // uwPolicyDS.findUwProduct(underwriteId,productInfoForm
            // .getItemId1());
            // productInfoForm.setUwStatus(vo.getUwStatus());
            // distinguish the decision is save or finish
            if (!"finish".equals(req.getParameter("status"))) {
              upvo.setUwStatus(UwStatusConstants.IN_PROGRESS);
            } else {
              if (req.getParameter("isAuthority") != null
                  && !Boolean.valueOf(req.getParameter("isAuthority"))
                      .booleanValue()) {
                upvo.setUwStatus(UwStatusConstants.ESCALATED);
              } else {
                upvo.setUwStatus(UwStatusConstants.FINISHED);
              }
            }
            // checking the underwriter
            upvo.setUnderwriterId(Long.valueOf(AppContext.getCurrentUser()
                .getUserId()));
            // cheching the renewDecision
            if (productInfoForm.getRenewDecision() != null) {
              // upvo.setRenewDecision(productInfoForm.getRenewDecision());
              setRenewDecision(productInfoForm.getUnderwriteId(), upvo,
                  isMasterAutoRenew);
            }
            if (upvo.getInsuredStatus2() == null) {
              upvo.setInsuredStatus2("0");
            }
            upvo.setUnderwriteTime(AppContext.getCurrentUserLocalTime());
            if (this.getProductService()
                .getProductByVersionId(Long.valueOf(upvo.getProductId().intValue()),
                		upvo.getProductVersionId())
                .isJobDiff()) {
              if (!StringUtils.isNull(req.getParameter("occupation1"))) {
                upvo.setJobClass1(Integer.valueOf(com.ebao.ls.encode.encoder.Encode.encodeForHTML(req).getParameter("occupation1")));
              }
              if (!StringUtils.isNull(req.getParameter("occupation2"))) {
                upvo.setJobClass2(Integer.valueOf(com.ebao.ls.encode.encoder.Encode.encodeForHTML(req).getParameter("occupation2")));
              }
            }
            uwPolicyDS.updateUwProduct(upvo);
          }
        }
        trans.commit();
      } catch (Exception e) {
        TransUtils.rollback(trans);
        throw ExceptionFactory.parse(e);
      }
      // recal extra loading
      if (isSingleBenefit(productInfoForm)) {
        UwProductVO productVO = uwPolicyDS.findUwProduct(underwriteId, itemId);
        reCalExtraLoading(underwriteId, itemId, productVO, productInfoForm);
      } else {
        for (int i = 0; i < productInfoForm.getItemIds().length; i++) {
          Long subItemId = productInfoForm.getItemIds()[i];
          UwProductVO upvo = uwPolicyDS.findUwProduct(
              productInfoForm.getUnderwriteId(), subItemId);
          reCalExtraLoading(underwriteId, subItemId, upvo, productInfoForm);
        }
      }
      // when cs-uw,synchronize records
      UwPolicyVO uwPolicyVO = uwPolicyDS.findUwPolicy(underwriteId);
      if (Utils.isCsUw(uwPolicyVO.getUwSourceType())) {
        CompleteUWProcessSp.synUWInfo4CS(uwPolicyVO.getPolicyId(),
            uwPolicyVO.getUnderwriteId(), uwPolicyVO.getChangeId(),
            Integer.valueOf(5));
      }
      req.setAttribute("underwriteId", productInfoForm.getUnderwriteId());
      req.setAttribute("itemId", productInfoForm.getItemIds());
      String action = "";
      action = req.getParameter("reAction");
      if ("lien".equals(action)) {
        return mapping.findForward("lien");
      }
      if ("extra".equals(action)) {
        return mapping.findForward("extra");
      }
      if ("restrict".equals(action)) {
        return mapping.findForward("restrict");
      }
      return mapping.findForward("product");
  }
  
  /**
   * In order to recal extra loading whether restrict cover or not (scenario:
   * firstly impose RC,then clear it,it should also recal)
   * 
   * @param underwriteId
   * @param itemId
   * @param productVO
   * @param productForm
   * @throws GenericException
   */
  protected void reCalExtraLoading(Long underwriteId, Long itemId,
      UwProductVO productVO, UwProductInfoForm productForm)
      throws GenericException {
    Map extraMap = new HashMap();
    UserTransaction trans = null;
    // if (!isRestrictCoverImposed(underwriteId, itemId, productVO,productForm))
    // {
    // return;
    // }
    Collection extraC = uwPolicyDS.findUwExtraLoadingEntitis(underwriteId,
        itemId);
    try {
      trans = Trans.getUserTransaction();
      trans.begin();
      CoverageVO policyProductVO = coverageCI.retrieveByItemId(itemId);
      if (policyProductVO.getCurrentPremium() != null) {
        policyProductVO.getCurrentPremium().setSumAssured(
            productVO.getReducedAmount());
      }
      policyProductVO.setChargePeriod(productVO.getUwChargePeriod());
      policyProductVO.setChargeYear(productVO.getUwChargeYear());
      policyProductVO.setCoveragePeriod(productVO.getUwCoveragePeriod());
      policyProductVO.setCoverageYear(productVO.getUwCoverageYear());
      coverageCI.updateCoverage(policyProductVO);
      QueryUwDataSp.CalcMain(productVO.getItemId(),
          productVO.getValidateDate(), productVO.getDerivation(), false,
          productVO.getReducedAmount());
      // re-calculate extraLoading
      if (extraC.size() > 0) {
        UwCalculation calculation = new UwCalculation();
        List premList = null;
        for (Iterator iter = extraC.iterator(); iter.hasNext();) {
          UwExtraLoadingVO extraLoadingVO = (UwExtraLoadingVO) iter.next();
          premList = calculation.calExtraPem(extraLoadingVO, productVO);
          extraMap.put(extraLoadingVO.getUwListId(), premList);
        }
      }
      TransUtils.rollback(trans);
    } catch (Exception e) {
      TransUtils.rollback(trans);
      throw ExceptionFactory.parse(e);
    }
    try {
      trans = Trans.getUserTransaction();
      trans.begin();
      for (Iterator iter = extraC.iterator(); iter.hasNext();) {
        UwExtraLoadingVO extraLoadingVO = (UwExtraLoadingVO) iter.next();
        if (extraMap.containsKey(extraLoadingVO.getUwListId())) {
          List premList = (List) extraMap.get(extraLoadingVO.getUwListId());
          extraLoadingVO.setExtraPrem((BigDecimal) premList.get(0));
          extraLoadingVO.setExtraPremAn((BigDecimal) premList.get(1));
          uwPolicyDS.updateUwExtraLoading(extraLoadingVO);
        }
      }
      trans.commit();
    } catch (Exception e) {
      TransUtils.rollback(trans);
      throw ExceptionFactory.parse(e);
    }
  }

  /**
   * re-calculate lien info
   * 
   * @param underwriteId
   * @param itemId
   * @param productVO
   * @param productForm
   * @throws GenericException
   */
  protected void reCalLien(Long underwriteId, Long itemId,
      UwProductVO productVO, UwProductInfoForm productForm)
      throws GenericException {
    // if (isRestrictCoverImposed(underwriteId,itemId,productVO,productForm)){
    // re-calculate lien info
    Collection lienC = uwPolicyDS.findUwLienEntitis(underwriteId, itemId);
    if (lienC.size() > 0) {
      for (Iterator iter = lienC.iterator(); iter.hasNext();) {
        UwLienVO lienVO = (UwLienVO) iter.next();
        lienVO.setReducedAmount(productVO.getReducedAmount().multiply(
            lienVO.getReduceRate()));
        uwPolicyDS.updateUwLien(lienVO);
      }
    }
    // }
  }

  /**
   * store uw decision
   * 
   * @param req
   * @param productInfoForm
   * @throws GenericException
   * @throws NumberFormatException
   */
  protected void storeUWDecision(HttpServletRequest req,
      UwProductInfoForm productInfoForm) throws GenericException {
    // distinguish the decision is save or finish
    if (!"finish".equals(req.getParameter("status"))) {
      productInfoForm.setUwStatus(UwStatusConstants.IN_PROGRESS);
    } else {
      if (req.getParameter("isAuthority") != null
          && !Boolean.valueOf(req.getParameter("isAuthority")).booleanValue()) {
        productInfoForm.setUwStatus(UwStatusConstants.ESCALATED);
      } else {
        productInfoForm.setUwStatus(UwStatusConstants.FINISHED);
      }
    }
    UwProductVO uwProductDecisionVO = uwPolicyDS.findUwProduct(productInfoForm
        .getUwListId1());
    
    try {
      BeanUtils.copyProperties(uwProductDecisionVO, productInfoForm, false);
    } catch (Exception ex) {
      throw ExceptionFactory.parse(ex);
    }
   // if master product decision is is Declination or Postpone ,set sub product
    // decision is Declination or Postpone
    setSubProductDecsion(productInfoForm, 0);
   
 
    uwProductDecisionVO.setItemId(productInfoForm.getItemId1());
    uwProductDecisionVO.setUwListId(productInfoForm.getUwListId1());
    if (!isNullOrEmptyString(req.getParameter("smoking1"))) {
      uwProductDecisionVO.setSmoking(com.ebao.ls.encode.encoder.Encode.encodeForHTML(req).getParameter("smoking1").trim());
    }
    if (!isNullOrEmptyString(req.getParameter("smoking2"))) {
      uwProductDecisionVO
          .setSmokingRelated(com.ebao.ls.encode.encoder.Encode.encodeForHTML(req).getParameter("smoking2").trim());
    }
    if (!isNullOrEmptyString(req.getParameter("facInsurance"))) {
      uwProductDecisionVO.setFacReinsurIndi(com.ebao.ls.encode.encoder.Encode.encodeForHTML(req).getParameter("facInsurance")
          .trim());
    }
    if (!isNullOrEmptyString(req.getParameter("occupationClass1"))) {
      uwProductDecisionVO.setJobCate1(Long.valueOf(com.ebao.ls.encode.encoder.Encode.encodeForHTML(req).getParameter(
          "occupationClass1").trim()));
    }
    if (!isNullOrEmptyString(req.getParameter("occupationClass2"))) {
      uwProductDecisionVO.setJobCate2(Long.valueOf(com.ebao.ls.encode.encoder.Encode.encodeForHTML(req).getParameter(
          "occupationClass2").trim()));
    }
    if (!isNullOrEmptyString(req.getParameter("occupation1"))) {
      uwProductDecisionVO.setJobClass1(Integer.valueOf(com.ebao.ls.encode.encoder.Encode.encodeForHTML(req).getParameter(
          "occupation1").trim()));
    }
    if (!isNullOrEmptyString(req.getParameter("occupation2"))) {
      uwProductDecisionVO.setJobClass2(Integer.valueOf(com.ebao.ls.encode.encoder.Encode.encodeForHTML(req).getParameter(
          "occupation2").trim()));
    }
    if (!isNullOrEmptyString(req.getParameter("insuredStatus1"))) {
      if ("Y".equals(req.getParameter("insuredStatus1"))) {
        uwProductDecisionVO.setInsuredStatus("2");
      } else if ("N".equals(req.getParameter("insuredStatus1"))) {
        uwProductDecisionVO.setInsuredStatus("1");
      }
    }
    if (!isNullOrEmptyString(req.getParameter("insuredStatus2"))) {
      if ("Y".equals(req.getParameter("insuredStatus2"))) {
        uwProductDecisionVO.setInsuredStatus2("2");
      } else if ("N".equals(req.getParameter("insuredStatus2"))) {
        uwProductDecisionVO.setInsuredStatus2("1");
      }
    } else {
      uwProductDecisionVO.setInsuredStatus2("0");
    }
    // update all indicator to the other product of the same insured in current
    // policy.
    synchronizeIndicatorAmongProducts(uwProductDecisionVO);
    uwProductDecisionVO.setUnderwriteTime(AppContext.getCurrentUserLocalTime());
    if (productInfoForm.getMaFactor() != null) {
      uwProductDecisionVO.setUwsafactor(new BigDecimal(productInfoForm
          .getMaFactor()));
    }
    if (productInfoForm.getMsFactor() != null) {
      uwProductDecisionVO.setUwmsfactor(new BigDecimal(productInfoForm
          .getMsFactor()));
    }
    if (productInfoForm.getPostponePeriod() != null) {
      uwProductDecisionVO
          .setPostponePeriod(productInfoForm.getPostponePeriod());
    }
    uwPolicyDS.updateUwProduct(uwProductDecisionVO, false);
  }

  /**
   * @param req
   * @param productInfoForm
   * @throws GenericException
   */
  private void synchronizeIndicatorAmongProducts(UwProductVO uwProductDecisionVO)
      throws GenericException {
    Long insured1 = uwProductDecisionVO.getInsured1();
    Long insured2 = uwProductDecisionVO.getInsured2();
    if (insured2 != null) {
      synchronizeIndicatorAmongProductsByInsured(uwProductDecisionVO, insured2,
          false);
    }
    synchronizeIndicatorAmongProductsByInsured(uwProductDecisionVO, insured1,
        true);
  }

  /**
   * @param req
   * @param productInfoForm
   * @param insured2
   * @throws GenericException
   */
  private void synchronizeIndicatorAmongProductsByInsured(UwProductVO uwPrdVO,
      Long insured, boolean isFirstInsured) throws GenericException {
    Collection otherProductVO = uwPolicyDS
        .findUwProductEntitisByUnderwriteIdAndInsuredId(
            uwPrdVO.getUnderwriteId(), insured);
    Iterator it1 = otherProductVO.iterator();
    while (it1.hasNext()) {
      UwProductVO occupationVO = (UwProductVO) it1.next();
      if (insured.equals(occupationVO.getInsured1())) {
        // occupationVO.setJobCate1(isFirstInsured?uwPrdVO.getJobCate1():uwPrdVO.
        // getJobCate2());
        occupationVO.setInsuredStatus(isFirstInsured ? uwPrdVO
            .getInsuredStatus() : uwPrdVO.getInsuredStatus2());
        occupationVO.setSmoking(isFirstInsured ? uwPrdVO.getSmoking() : uwPrdVO
            .getSmokingRelated());
        // occupationVO.setJobClass1(isFirstInsured?uwPrdVO.getJobClass1():uwPrdVO
        // .getJobClass2());
      } else {
        // occupationVO.setJobCate2(isFirstInsured?uwPrdVO.getJobCate1():uwPrdVO.
        // getJobCate2());
        occupationVO.setInsuredStatus2(isFirstInsured ? uwPrdVO
            .getInsuredStatus() : uwPrdVO.getInsuredStatus2());
        occupationVO.setSmokingRelated(isFirstInsured ? uwPrdVO.getSmoking()
            : uwPrdVO.getSmokingRelated());
        // occupationVO.setJobClass2(isFirstInsured?uwPrdVO.getJobClass1():uwPrdVO
        // .getJobClass2());
      }
      uwPolicyDS.updateUwProduct(occupationVO);
    }
  }

  /**
   * Determines whether single benefit
   * 
   * @param formBean page input form obj
   * @return True - just single benefit been selected False - multiply benefit
   *         been selected
   */
  protected boolean isSingleBenefit(UwProductInfoForm formBean) {
    return formBean.getItemIds().length == 1;
  }

  /**
   * Determines whether the input string value represents for a null String or
   * an empty String
   * 
   * @param value String value
   * @return False - Not null and not empty True - Null or empty String
   */
  private boolean isNullOrEmptyString(String value) {
    if (null == value || 0 == value.trim().length()) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * set sub,waver product decsion when product decsion is Declination or
   * Postpone
   * 
   * @param formBean
   * @param i
   * @throws GenericException
   */
  private void setSubProductDecsion(UwProductInfoForm formBean, int i)
      throws GenericException {
    Integer decFlag = Integer.valueOf(3);
    Integer postFlag = Integer.valueOf(4);
    if ((decFlag.equals(formBean.getDecisionId()))
        || (postFlag.equals(formBean.getDecisionId()))) {
      UwProductVO productVO = uwPolicyDS.findUwProduct(
          formBean.getUnderwriteId(), formBean.getItemIds()[i]);
      // product is master
      if (productVO.getMasterId() == null) {
        Collection subVOs = uwPolicyDS.findUwProductEntitis(formBean
            .getUnderwriteId());
        Iterator it = subVOs.iterator();
        while (it.hasNext()) {
          UwProductVO subVO = (UwProductVO) it.next();
          // get sub product
          if (subVO.getMasterId() != null) {
            if (formBean.getItemIds()[i].equals(subVO.getMasterId())) {
              subVO.setDecisionId(formBean.getDecisionId());
              subVO.setUwStatus(formBean.getUwStatus());
              // update sub product decison is Declination or Postpone
              uwPolicyDS.updateUwProduct(subVO);
              // update waver prodcut of sub product decision
              Collection waverVOs = subVOs;
              Iterator itWaver = waverVOs.iterator();
              while (itWaver.hasNext()) {
                UwProductVO waverVO = (UwProductVO) itWaver.next();
                if (subVO.getItemId().equals(waverVO.getMasterId())) {
                  waverVO.setDecisionId(subVO.getDecisionId());
                  waverVO.setUwStatus(subVO.getUwStatus());
                  uwPolicyDS.updateUwProduct(waverVO);
                }
              }
            }
          }
        }
        // product is sub
      } else {
        // get the waver of sub product
        Collection waverVOs = uwPolicyDS.findUwProductEntitis(formBean
            .getUnderwriteId());
        Iterator itWaver = waverVOs.iterator();
        while (itWaver.hasNext()) {
          UwProductVO waverVO = (UwProductVO) itWaver.next();
          if (formBean.getItemIds()[i].equals(waverVO.getMasterId())) {
            waverVO.setDecisionId(formBean.getDecisionId());
            waverVO.setUwStatus(formBean.getUwStatus());
            uwPolicyDS.updateUwProduct(waverVO);
          }
        }
      }
    }
  }

  /**
   * delete the lien and restrict information when the decision is accept
   * 
   * @param underwriteId
   * @param itemId
   * @throws Exception
   */
  protected void doLienAndReCDelete(Long underwriteId, Long itemId,
      Integer decisionId) throws Exception {
    UwPolicyVO policyVO = uwPolicyDS.findUwPolicy(underwriteId);
    if (decisionId.intValue() == CodeCst.PRODUCT_DECISION__ACCEPTED) {
      // delete lien info
      if (!uwPolicyDS.findUwLienEntitis(underwriteId, itemId).isEmpty()) {
        policyVO.setDeciAmenIndi("Y");
        Collection lienc = uwPolicyDS.findUwLienEntitis(underwriteId, itemId);
        Iterator it = lienc.iterator();
        while (it.hasNext()) {
          UwLienVO vo = (UwLienVO) it.next();
          uwPolicyDS.removeUwLien(vo);
        }
      }
      // delete restrict cover info
      UwProductVO productVO = uwPolicyDS.findUwProduct(underwriteId, itemId);
      if (uwProcessDS.isRestrictCoverImposed(productVO)) {
        productVO.setReducedAmount(productVO.getAmount());
        productVO.setUwCoverageYear(productVO.getCoverageYear());
        productVO.setUwChargeYear(productVO.getChargeYear());
        productVO.setUwsafactor(productVO.getSaFactor());
        productVO.setReducedUnit(productVO.getUnit());
        productVO.setReducedLevel(productVO.getBenefitLevel());
        BigDecimal originalMSFactor = getOriginalMSFactor(underwriteId, itemId,
            Long.valueOf(productVO.getProductId().intValue()));
        if (originalMSFactor.compareTo(new BigDecimal(0)) > 0) {
          productVO.setUwmsfactor(originalMSFactor);
        }
        uwPolicyDS.updateUwProduct(productVO);
        policyVO.setDeciAmenIndi("Y");
      }
    }
  }

  /**
   * weather it is Preferred product
   * 
   * @param productId
   * @return
   * @throws GenericException
   */
  // private boolean isPreferred(Integer productId) throws GenericException {
  // PrdDefAPI prdDefAPI = (PrdDefAPI)Utils.getExternalObject(PrdDefAPI.class);
  // CSRelatedInfoVO value =
  // prdDefAPI.getCSRelatedInfo(Long.valueOf(productId.longValue()));
  // return value.isPreferLifeIndi();
  // }
  /**
   * convert the decision id when the decision Id is accept with
   * original/standard term
   * 
   * @param decisionId
   * @param underwriteId
   * @param itemId
   * @return decision Id after be converted
   * @throws GenericException
   */
  protected int convertUwDecision(int decisionId, Long underwriteId, Long itemId)
      throws GenericException {
    int resultId = decisionId;
    Integer preUwDecision = Utils.findPreProductDecisonId(underwriteId, itemId);
    // if user select the decison accept as original ,then the policy must
    // have previous decision.
    if (resultId == CodeCst.PRODUCT_DECISION__ORIGINAL) {
      if (preUwDecision != null) {
        resultId = preUwDecision.intValue();
      } else {
        resultId = 0;
      }
    } else {
      // if user select the decision is accept as standard,the decision
      // must be check as accepted
      if (resultId == CodeCst.PRODUCT_DECISION__STANDARD) {
        resultId = CodeCst.PRODUCT_DECISION__ACCEPTED;
      }
    }
    return resultId;
  }

  /**
   * get master product's Renew indicator
   * 
   * @param underWriteId
   * @return
   * @throws GenericException
   */
  protected boolean getMasterRenew(Long underWriteId) throws GenericException {
    UwPolicyVO policyVO = uwPolicyDS.findUwPolicy(underWriteId);
    CoverageVO policyProductVO = coverageCI.getMainBenefitByPolicyId(policyVO
        .getPolicyId());
    ProductVO masterInfoVO = this.getProductService().getProductByVersionId(
        Long.valueOf(policyProductVO.getProductId().longValue()),
        policyProductVO.getProductVersionId());
    // get master product's is auto Renew indicator
    boolean isMasterAutoRenew = masterInfoVO.isRenew();
    return isMasterAutoRenew;
  }

  /**
   * convert rider's renew decision
   * 
   * @param underWriteId
   * @param uwProductVO
   * @throws GenericException
   */
  protected void setRenewDecision(Long underWriteId, UwProductVO uwProductVO,
      boolean isMasterAutoRenew) throws GenericException {
    UwProductVO upvo = uwPolicyDS.findUwProduct(underWriteId,
        uwProductVO.getItemId());
    ProductVO basicInfoVO = this.getProductService().getProductByVersionId(
        Long.valueOf(upvo.getProductId().longValue()),
        upvo.getProductVersionId());
    /**
     * 1) main benefit allow auto renew, at least one rider dose not allow auto
     * renew, the defautl value of auto renew ind=Y (follow main benefit), when
     * user click submit, system should auto update auto renew ind=N for the
     * benefit not allow auto renew. 2) main benefit allow auto renew, at least
     * one rider dose not allow auto renew, the default value of auto renew
     * ind=Y (follow main benefit), user modify auto renew indicator to be N,
     * when user click submit , system auto update auto renew ind=N for all the
     * benefit. If user want to set auto renew ind of the rider to be "Y",
     * please make benefit decision of the rider again. 3) main benefit not
     * allow auto renw, at least one rider allow auto renew, the defautl value
     * of auto renew ind=N (follow main benefit), and is allowed to amend to be
     * "Y". When click submit with auto renew ind=N, system update auto renew
     * ind of all the benefit to be "N". If user want to set auto renew ind of
     * the rider to be "Y", please make benefit decision of the rider again.
     */
    if (isMasterAutoRenew) {
      if (!basicInfoVO.isRenew() && "Y".equals(upvo.getRenewDesc())) {
        uwProductVO.setRenewDecision("N");
      }
    }
  }

  protected Long resetIndxIndicator(Long itemId, String indxIndicator)
      throws GenericException {
    Long success = CompleteUWProcessSp.updateProductIndxIndi(itemId,
        indxIndicator);
    if (success.equals(APP_INDX_INDI_CANT_FORM_WN_TO_Y)) {
      throw new AppException(APP_INDX_INDI_CANT_FORM_WN_TO_Y);
    }
    if (success.equals(APP_INDX_INDI_CANT_FORM_W_TO_N)) {
      throw new AppException(APP_INDX_INDI_CANT_FORM_W_TO_N);
    }
    if (success.equals(APP_INDX_INDI_CANT_FORM_YN_TO_W)) {
      throw new AppException(APP_INDX_INDI_CANT_FORM_YN_TO_W);
    }
    return success;
  }


  /**
   * process warning
   * 
   * @param mapping
   * @param form
   * @param request
   * @param response
   * @return
   * @throws Exception
   */
  @Deprecated
  public MultiWarning processWarningOldVersion(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {
    UwProductInfoForm productInfoForm = (UwProductInfoForm) form;
    MultiWarning warning = new MultiWarning();
    // get value from page
    Long underwriteId = Long.valueOf(request.getParameter("underwriteId"));
    String[] itemIds = request.getParameterValues("itemIds");
    int decisionId = Integer.valueOf(request.getParameter("decisionId"))
        .intValue();
    Long underwriterId = Long.valueOf(AppContext.getCurrentUser().getUserId());
    String sourceType = request.getParameter("uwSourceType");
    // check decision
    UserTransaction trans = Trans.getUserTransaction();
    try {
      trans.begin();
      int previousBenefitDecision = 0;// initial is zero
      boolean exitDiffDecisionAlongMultiBene = false;// idicate wheather there
      // is diff decision along
      // multi-bene
      for (String itemId : itemIds) {
        int currentBenefitDecision = checkUwDecisionIndividually(request,
            productInfoForm, warning, underwriteId, underwriterId,
            sourceType, Long.valueOf(itemId));
        if (previousBenefitDecision != 0
            && currentBenefitDecision != previousBenefitDecision) {
          // display warning:Different underwriting decision found. Please
          // update underwriting decision individually.
          exitDiffDecisionAlongMultiBene = true;
        }
        previousBenefitDecision = currentBenefitDecision;
      }
      if (exitDiffDecisionAlongMultiBene) {
        // if the decision is not consistent , then only display
        // "please make decision individually";
        warning.getWarnings().clear();
        warning.addWarning("ERR_" + APP_INCONSISTENT_UW_DECISION);
        warning.setContinuable(false);
      }
      if (!warning.isEmpty()) {
        trans.rollback();
        return warning;
      }
      trans.commit();
    } catch (Exception e) {
      TransUtils.rollback(trans);
      throw ExceptionFactory.parse(e);
    }
    return warning;
  }
  
  /**
   * save the uw decision
   * 險種核保決定 舊版本, 保單核保決定由保項層移至保單層, 不作核保決定更新及相關計算
   * @param mapping
   * @param form
   * @param req
   * @param res
   * @return
   * @throws GenericException
   */
  @Deprecated
  public ActionForward processOldVersion(ActionMapping mapping, ActionForm form,
      HttpServletRequest req, HttpServletResponse res) throws GenericException {
    UwProductInfoForm productInfoForm = (UwProductInfoForm) form;
    int decisionId = Integer.valueOf(com.ebao.ls.encode.encoder.Encode.encodeForHTML(req).getParameter("decisionId")).intValue();
    Long underwriteId = Long.valueOf(com.ebao.ls.encode.encoder.Encode.encodeForHTML(req).getParameter("underwriteId"));
    Long itemId = Long.valueOf(com.ebao.ls.encode.encoder.Encode.encodeForHTML(req).getParameter("itemIds"));
    UserTransaction trans = null;
    try {
      trans = Trans.getUserTransaction();
      trans.begin();
      if (productInfoForm.getRenewDesc().equals("N")) {
        productInfoForm.setRenewDecision(CodeCst.RENEW_DECISION__NOT_RENEW);
      } else {
        productInfoForm.setRenewDecision(CodeCst.RENEW_DECISION__RENEWABLE);
      }
      if (isSingleBenefit(productInfoForm)) {
        // convert to proper decisionId
        productInfoForm.setDecisionId(Integer.valueOf(convertUwDecision(
            decisionId, underwriteId, itemId)));
        // recal extra loading or lien
        UwProductVO productVO = uwPolicyDS.findUwProduct(underwriteId, itemId);
        reCalLien(underwriteId, itemId, productVO, productInfoForm);
        // delete the lien and restric information when decision is accept
        doLienAndReCDelete(underwriteId, itemId,
            productInfoForm.getDecisionId());
        productInfoForm.setDecisionId(Integer.valueOf(decisionId));
        // add by hanzhong.yan for defect GEL00028431 2007/7/23
        productInfoForm.setUnderwriterId(Long.valueOf(AppContext
            .getCurrentUser().getUserId()));
        // add end
        try {
          if (!StringUtils.isNullOrEmpty(productInfoForm
              .getIndxIndicator_decision())) {
            resetIndxIndicator(itemId,
                productInfoForm.getIndxIndicator_decision());
          }
        } catch (AppException appe) {
          TransUtils.rollback(trans);
          throw appe;
        }
        storeUWDecision(req, productInfoForm);
      } else {
        // multi items been selected
        boolean isMasterAutoRenew = getMasterRenew(productInfoForm
            .getUnderwriteId());
        for (int i = 0; i < productInfoForm.getItemIds().length; i++) {
          Long subItemId = productInfoForm.getItemIds()[i];
          /*
           * If there are products need to be updated then we must load the
           * record from db first and update the value which are not null
           */
          UwProductVO upvo = uwPolicyDS.findUwProduct(
              productInfoForm.getUnderwriteId(), subItemId);
          // recal extra loading or lien
          reCalLien(underwriteId, subItemId, upvo, productInfoForm);
          // convert to proper decisionId
          productInfoForm.setDecisionId(Integer.valueOf(convertUwDecision(
              decisionId, underwriteId, subItemId)));
          // set ma/ms factor
          if (productInfoForm.getMaFactor() != null) {
            upvo.setUwsafactor(new BigDecimal(productInfoForm.getMaFactor()));
          }
          if (productInfoForm.getMsFactor() != null) {
            upvo.setUwmsfactor(new BigDecimal(productInfoForm.getMsFactor()));
          }
          // the decline or postpone reason, will only be captured on the first
          // benefit.
          upvo.setDecisionId(Integer.valueOf(decisionId));
          upvo.setDecisionDesc2(productInfoForm.getDecisionDesc2() != null ? productInfoForm
              .getDecisionDesc2() : null);
          // description only be stored in first product
          if (null != productInfoForm.getDecisionId()) {
            if (CodeCst.PRODUCT_DECISION__DECLINED == productInfoForm
                .getDecisionId().intValue()
                || CodeCst.PRODUCT_DECISION__POSTPONED == productInfoForm
                    .getDecisionId().intValue()) {
              if (0 == i) {
                upvo.setDecisionReason(com.ebao.ls.encode.encoder.Encode.encodeForHTML(req).getParameter("decisionReason"));
                upvo.setDecisionReasonDesc(com.ebao.ls.encode.encoder.Encode.encodeForHTML(req).getParameter("decisionReasonDesc"));
              } else {
                upvo.setDecisionReason("");
                upvo.setDecisionReasonDesc("");
              }
            }
          }
          // get previous uw status from DB when process warning
          // UwProductVO vo =
          // uwPolicyDS.findUwProduct(underwriteId,productInfoForm
          // .getItemId1());
          // productInfoForm.setUwStatus(vo.getUwStatus());
          // distinguish the decision is save or finish
          if (!"finish".equals(req.getParameter("status"))) {
            upvo.setUwStatus(UwStatusConstants.IN_PROGRESS);
          } else {
            if (req.getParameter("isAuthority") != null
                && !Boolean.valueOf(req.getParameter("isAuthority"))
                    .booleanValue()) {
              upvo.setUwStatus(UwStatusConstants.ESCALATED);
            } else {
              upvo.setUwStatus(UwStatusConstants.FINISHED);
            }
          }
          // checking the underwriter
          upvo.setUnderwriterId(Long.valueOf(AppContext.getCurrentUser()
              .getUserId()));
          // cheching the renewDecision
          if (productInfoForm.getRenewDecision() != null) {
            // upvo.setRenewDecision(productInfoForm.getRenewDecision());
            setRenewDecision(productInfoForm.getUnderwriteId(), upvo,
                isMasterAutoRenew);
          }
          if (upvo.getInsuredStatus2() == null) {
            upvo.setInsuredStatus2("0");
          }
          upvo.setUnderwriteTime(AppContext.getCurrentUserLocalTime());
          if (this.getProductService()
              .getProductByVersionId(Long.valueOf(upvo.getProductId().intValue()),
            		  upvo.getProductVersionId())
              .isJobDiff()) {
            if (!StringUtils.isNull(req.getParameter("occupation1"))) {
              upvo.setJobClass1(Integer.valueOf(com.ebao.ls.encode.encoder.Encode.encodeForHTML(req).getParameter("occupation1")));
            }
            if (!StringUtils.isNull(req.getParameter("occupation2"))) {
              upvo.setJobClass2(Integer.valueOf(com.ebao.ls.encode.encoder.Encode.encodeForHTML(req).getParameter("occupation2")));
            }
          }
          uwPolicyDS.updateUwProduct(upvo);
        }
      }
      trans.commit();
    } catch (Exception e) {
      TransUtils.rollback(trans);
      throw ExceptionFactory.parse(e);
    }
    // recal extra loading
    if (isSingleBenefit(productInfoForm)) {
      UwProductVO productVO = uwPolicyDS.findUwProduct(underwriteId, itemId);
      reCalExtraLoading(underwriteId, itemId, productVO, productInfoForm);
    } else {
      for (int i = 0; i < productInfoForm.getItemIds().length; i++) {
        Long subItemId = productInfoForm.getItemIds()[i];
        UwProductVO upvo = uwPolicyDS.findUwProduct(
            productInfoForm.getUnderwriteId(), subItemId);
        reCalExtraLoading(underwriteId, subItemId, upvo, productInfoForm);
      }
    }
    // when cs-uw,synchronize records
    UwPolicyVO uwPolicyVO = uwPolicyDS.findUwPolicy(underwriteId);
    if (Utils.isCsUw(uwPolicyVO.getUwSourceType())) {
      CompleteUWProcessSp.synUWInfo4CS(uwPolicyVO.getPolicyId(),
          uwPolicyVO.getUnderwriteId(), uwPolicyVO.getChangeId(),
          Integer.valueOf(5));
    }
    req.setAttribute("underwriteId", productInfoForm.getUnderwriteId());
    req.setAttribute("itemId", productInfoForm.getItemIds());
    String action = "";
    action = req.getParameter("reAction");
    if ("lien".equals(action)) {
      return mapping.findForward("lien");
    }
    if ("extra".equals(action)) {
      return mapping.findForward("extra");
    }
    if ("restrict".equals(action)) {
      return mapping.findForward("restrict");
    }
    return mapping.findForward("product");
  }

  
  @Resource(name = UwProcessService.BEAN_DEFAULT)
  private UwProcessService uwProcessDS;

  public void setUwProcessDS(UwProcessService uwProcessDS) {
    this.uwProcessDS = uwProcessDS;
  }

  @Resource(name = UwPolicyService.BEAN_DEFAULT)
  protected UwPolicyService uwPolicyDS;

  public void setUwPolicyDS(UwPolicyService uwPolicyDS) {
    this.uwPolicyDS = uwPolicyDS;
  }

  public UwPolicyService getUwPolicyDS() {
    return uwPolicyDS;
  }

  public UwProcessService getUwProcessDS() {
    return uwProcessDS;
  }

  @Resource(name = ProductService.BEAN_DEFAULT)
  private ProductService productService;

  public ProductService getProductService() {
    return productService;
  }

  public void setProductService(ProductService productService) {
    this.productService = productService;
  }

  @Resource(name = CoverageCI.BEAN_DEFAULT)
  private CoverageCI coverageCI;

  /**
   * @return the coverageCI
   */
  public CoverageCI getCoverageCI() {
    return coverageCI;
  }

  /**
   * @param coverageCI the coverageCI to set
   */
  public void setCoverageCI(CoverageCI coverageCI) {
    this.coverageCI = coverageCI;
  }
  
  @Resource(name = PolicyService.BEAN_DEFAULT)
  PolicyService policyService;  
}
