package com.ebao.ls.uw.ds;

import java.util.List;

import com.ebao.foundation.commons.exceptions.GenericException;
import com.ebao.ls.foundation.service.GenericServiceImpl;
import com.ebao.ls.pa.nb.data.bo.AGYRetiredAgent;
import com.ebao.ls.pa.nb.vo.AGYRetiredAgentVO;
import com.ebao.ls.uw.data.AGYRetiredAgentDao;

public class AGYRetiredAgentServiceImpl extends GenericServiceImpl<AGYRetiredAgentVO, AGYRetiredAgent, AGYRetiredAgentDao> implements AGYRetiredAgentService {

	@Override
	protected AGYRetiredAgentVO newEntityVO() {
		return new AGYRetiredAgentVO();
	}
	
	@Override
	public void uploadList(List<AGYRetiredAgentVO> voList) throws GenericException {
		for (AGYRetiredAgentVO vo:voList) {
			AGYRetiredAgent bo = new AGYRetiredAgent();
			bo.copyFromVO(vo, true, true);
			super.dao.save(bo);
		}
	}

	@Override
	public void removeAll() throws GenericException {
		super.dao.deleteAllRecord();
	}

}
