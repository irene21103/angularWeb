package com.ebao.ls.crs.nb.impl;

import java.util.Collections;
import java.util.List;

import org.apache.commons.beanutils.BeanComparator;
import org.apache.commons.collections4.CollectionUtils;
import org.hibernate.criterion.Restrictions;

import com.ebao.foundation.commons.exceptions.GenericException;
import com.ebao.ls.crs.data.bo.NBcrsAttachment;
import com.ebao.ls.crs.nb.NBcrsAttachmentDAO;
import com.ebao.ls.crs.nb.NBcrsAttachmentService;
import com.ebao.ls.crs.vo.NBcrsAttachmentVO;
import com.ebao.ls.crs.vo.NBcrsVO;
import com.ebao.ls.fatca.vo.FatcaIdentityAttachmentVO;
import com.ebao.ls.foundation.service.GenericServiceImpl;
import com.ebao.ls.pa.nb.util.NBUtils;
import com.ebao.pub.util.StringUtils;

public class NBcrsAttachmentServiceImpl extends GenericServiceImpl<NBcrsAttachmentVO, NBcrsAttachment, NBcrsAttachmentDAO>
				implements NBcrsAttachmentService {

	@Override
	protected NBcrsAttachmentVO newEntityVO() {
		return new NBcrsAttachmentVO();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<NBcrsAttachmentVO> findByChangeId(Long changeId) {
		List<NBcrsAttachment> boList = dao.findByCriteria(
						Restrictions.eq("changeId", changeId));
		Collections.sort(boList, new BeanComparator("listId"));
		return convertToVOList(boList);
	}

	/* (non-Javadoc)
	 * @see com.ebao.ls.crs.nb.NBcrsAttachmentService#saveOrUpdate(com.ebao.ls.crs.vo.NBcrsVO, java.util.List)
	 */
	@Override
	public void saveOrUpdate(
					Long nbCrsId,
					Long changeId, 
					java.util.List<FatcaIdentityAttachmentVO> attachmentVOList) throws GenericException {

		java.util.List<NBcrsAttachmentVO> backAttachList = findByChangeId(changeId);

		try {

			List<String> formAttachment = NBUtils.getPropertyStringList(attachmentVOList, "attatchment");
			List<String> dbAttachment = NBUtils.getPropertyStringList(backAttachList, "attachmentCode");

			if (CollectionUtils.isNotEmpty(backAttachList)) {
				for (NBcrsAttachmentVO backAttach : backAttachList) {
					//不在前端送入的資料，則刪除
					String code = backAttach.getAttachmentCode();
					if (formAttachment.contains(code) == false) {
						this.remove(backAttach.getListId());
					}
				}
			}
			if (CollectionUtils.isEmpty(attachmentVOList)) {
				return;
			}

			for (FatcaIdentityAttachmentVO attachVO : attachmentVOList) {
				if (!StringUtils.isNullOrEmpty(attachVO.getAttatchment())) {
					//不在資料表中，作新增
					if (dbAttachment.contains(attachVO.getAttatchment()) == false) {
						NBcrsAttachmentVO crsAttachVO = new NBcrsAttachmentVO();
						crsAttachVO.setNbCRSId(nbCrsId);
						crsAttachVO.setChangeId(changeId);
						crsAttachVO.setAttachmentCode(attachVO.getAttatchment());
						save(crsAttachVO);
					}
				} 
			}

		} catch (Exception e) {
			throw new com.ebao.pub.framework.GenericException(GenericException.CODE_UNKNOW_SYS_ERROR, e);
		}
	}

}
