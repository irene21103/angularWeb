package com.ebao.ls.uw.ctrl.uwIssues;

import java.math.BigDecimal;
import java.util.List;

import com.ebao.ls.cmu.bs.slq.SlqContentVO;
import com.ebao.ls.cmu.pub.document.DocumentFormFile;
import com.ebao.ls.letter.pa.nb.uwIssues.vo.UwIssuesLetterInfoVO;
import com.ebao.pub.framework.GenericForm;

public class IssuesForm extends GenericForm {
	/**
	 * <code>serialVersionUID</code> 的註解
	 */
	private static final long serialVersionUID = 3353496395917114120L;

	private String IsIframe;
	
	private Long policyId;

	private String productId;

	private String policyCode;

	private String applyCode;

	private Integer moneyId;

	private Long branchId;

	private BigDecimal initialPrem;

	private String applyDate;

	private String submitChannel; // sales channel???

	private Integer liabilityState = java.lang.Integer.valueOf(0);

	private Integer proposalStatus;

	private Long serviceAgent; // policy level agent

	private String initialType;

	private String agentCate;

	private Long nonUWCount = 0L;

	private Long uWCount = 0L;

	private Long[] listId;

	private String[] letterContent;

	private String[] notes;

	private Integer[] ruleStatus;

	private String[] replyTypes;

	private Long[] documentIdList;

	private SlqContentVO[] slqContents;

	private String issueType;

	private String agentName;
	
	private String agentOrgan;

	private String productName;

	private Long salesChannel;

	// for manual issues
	private String issues;

	private String comment;

	// for reply date
	private Long[] docId;

	private String[] receivedDates;

	private String subAction;

	private Long[] docListId;

	private String[] letterStatus;

	private String[] newLetterStatus;

	private String[] replyDates;

	private String newQueryClasses1;

	private String newQueryCode1;

	private String newFreeContent1;

	private Integer newQuestionRole1;

	private String newQueryCode1_desc;
	
	private java.lang.Long underwriteId = null;
	
	private Boolean isAccommodativeRole;
	
	private Long[] uWIssuesCheckListId;	/* 核保問題勾選List_Id */
	
	private Long[] uWManualIssuesCheckListId;	/* 人工核保問題勾選List_Id */
	
	private String[] uWIssuesletterContent;
	
	private String[] uWManualIssuesletterContent;
	
	private UwIssuesLetterInfoVO uwIssuesLetterInfoVO;
	
	
	
	private String uwRuleObjStr;
	
	private String retMsg;
	
	/**影像*/
	private String[] applyImageList;
	
	/**附加檔案*/
	private List<DocumentFormFile> uploadFileList;
	
	/** 主要保人creditCode */
	private String holderCertiCode ;

	/** 來源為保全核保 */
	private boolean isCsUw;
	
	private Long changeId;
	
	/** 是否為核保員  */
	private boolean isUnderwriter = true;

    // 批次簽署
    private Integer batchSignRange;
    private String remoteIndi;//遠距投保
	private String remoteOverseas; // 境外遠距投保
    private String remoteCallout;//錄音錄影覆核抽檢件
    private String remoteCheck;//身分驗證結果
    // 保經/保代公司依其管理規則33-1辦理作業
    private String elderCalloutFlag;
    

    /**
     * @return the batchSignRange
     */
    public Integer getBatchSignRange() {
        return batchSignRange;
    }

    /**
     * @param batchSignRange the batchSignRange to set
     */
    public void setBatchSignRange(Integer batchSignRange) {
        this.batchSignRange = batchSignRange;
    }

	public String getNewQueryCode1_desc() {
		return newQueryCode1_desc;
	}

	public void setNewQueryCode1_desc(String newQueryCode1_desc) {
		this.newQueryCode1_desc = newQueryCode1_desc;
	}

	public Long getPolicyId() {
		return policyId;
	}

	public void setPolicyId(Long policyId) {
		this.policyId = policyId;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getPolicyCode() {
		return policyCode;
	}

	public void setPolicyCode(String policyCode) {
		this.policyCode = policyCode;
	}

	public String getApplyCode() {
		return applyCode;
	}

	public void setApplyCode(String applyCode) {
		this.applyCode = applyCode;
	}

	public Integer getMoneyId() {
		return moneyId;
	}

	public void setMoneyId(Integer moneyId) {
		this.moneyId = moneyId;
	}

	public Long getBranchId() {
		return branchId;
	}

	public void setBranchId(Long branchId) {
		this.branchId = branchId;
	}

	public BigDecimal getInitialPrem() {
		return initialPrem;
	}

	public void setInitialPrem(BigDecimal initialPrem) {
		this.initialPrem = initialPrem;
	}

	public String getApplyDate() {
		return applyDate;
	}

	public void setApplyDate(String applyDate) {
		this.applyDate = applyDate;
	}

	public String getSubmitChannel() {
		return submitChannel;
	}

	public void setSubmitChannel(String submitChannel) {
		this.submitChannel = submitChannel;
	}

	public Integer getLiabilityState() {
		return liabilityState;
	}

	public void setLiabilityState(Integer liabilityState) {
		this.liabilityState = liabilityState;
	}

	public Integer getProposalStatus() {
		return proposalStatus;
	}

	public void setProposalStatus(Integer proposalStatus) {
		this.proposalStatus = proposalStatus;
	}

	public String getInitialType() {
		return initialType;
	}

	public void setInitialType(String initialType) {
		this.initialType = initialType;
	}

	public String getAgentCate() {
		return agentCate;
	}

	public void setAgentCate(String agentCate) {
		this.agentCate = agentCate;
	}

	public Long getNonUWCount() {
		return nonUWCount;
	}

	public void setNonUWCount(Long nonUWCount) {
		this.nonUWCount = nonUWCount;
	}

	public Long getUWCount() {
		return uWCount;
	}

	public void setUWCount(Long count) {
		uWCount = count;
	}

	public Long[] getListId() {
		return listId;
	}

	public void setListId(Long[] listId) {
		this.listId = listId;
	}

	public String[] getLetterContent() {
		return letterContent;
	}

	public void setLetterContent(String[] letterContent) {
		this.letterContent = letterContent;
	}

	public String[] getNotes() {
		return notes;
	}

	public void setNotes(String[] notes) {
		this.notes = notes;
	}

	public Integer[] getRuleStatus() {
		return ruleStatus;
	}

	public void setRuleStatus(Integer[] ruleStatus) {
		this.ruleStatus = ruleStatus;
	}

	public String getIssues() {
		return issues;
	}

	public void setIssues(String issues) {
		this.issues = issues;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Long getServiceAgent() {
		return serviceAgent;
	}

	public void setServiceAgent(Long serviceAgent) {
		this.serviceAgent = serviceAgent;
	}

	public String[] getReceivedDates() {
		return receivedDates;
	}

	public void setReceivedDates(String[] receivedDates) {
		this.receivedDates = receivedDates;
	}

	public Long[] getDocId() {
		return docId;
	}

	public void setDocId(Long[] docId) {
		this.docId = docId;
	}

	public String getSubAction() {
		return subAction;
	}

	public void setSubAction(String subAction) {
		this.subAction = subAction;
	}

	public String[] getLetterStatus() {
		return letterStatus;
	}

	public void setLetterStatus(String[] letterStatus) {
		this.letterStatus = letterStatus;
	}

	public String[] getNewLetterStatus() {
		return newLetterStatus;
	}

	public void setNewLetterStatus(String[] newLetterStatus) {
		this.newLetterStatus = newLetterStatus;
	}

	public Long[] getDocListId() {
		return docListId;
	}

	public void setDocListId(Long[] docListId) {
		this.docListId = docListId;
	}

	public String[] getReplyDates() {
		return replyDates;
	}

	public void setReplyDates(String[] replyDates) {
		this.replyDates = replyDates;
	}

	public String getIssueType() {
		return issueType;
	}

	public void setIssueType(String issueType) {
		this.issueType = issueType;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public Long getSalesChannel() {
		return salesChannel;
	}

	public void setSalesChannel(Long salesChannel) {
		this.salesChannel = salesChannel;
	}

	public String[] getReplyTypes() {
		return replyTypes;
	}

	public void setReplyTypes(String[] replyTypes) {
		this.replyTypes = replyTypes;
	}
	
	public SlqContentVO[] getSlqContents() {
		return slqContents;
	}

	public void setSlqContents(SlqContentVO[] slqContents) {
		this.slqContents = slqContents;
	}

	public Long[] getDocumentIdList() {
		return documentIdList;
	}

	public void setDocumentIdList(Long[] documentIdList) {
		this.documentIdList = documentIdList;
	}
	
	public String getNewQueryClasses1() {
		return newQueryClasses1;
	}

	public void setNewQueryClasses1(String newQueryClasses1) {
		this.newQueryClasses1 = newQueryClasses1;
	}

	public String getNewQueryCode1() {
		return newQueryCode1;
	}

	public void setNewQueryCode1(String newQueryCode1) {
		this.newQueryCode1 = newQueryCode1;
	}

	public Integer getNewQuestionRole1() {
		return newQuestionRole1;
	}

	public void setNewQuestionRole1(Integer newQuestionRole1) {
		this.newQuestionRole1 = newQuestionRole1;
	}

	public String getNewFreeContent1() {
		return newFreeContent1;
	}

	public void setNewFreeContent1(String newFreeContent1) {
		this.newFreeContent1 = newFreeContent1;
	}

	public String getAgentOrgan() {
		return agentOrgan;
	}

	public void setAgentOrgan(String agentOrgan) {
		this.agentOrgan = agentOrgan;
	}

	public java.lang.Long getUnderwriteId() {
		return underwriteId;
	}

	public void setUnderwriteId(java.lang.Long underwriteId) {
		this.underwriteId = underwriteId;
	}

	public Boolean getIsAccommodativeRole() {
		return isAccommodativeRole;
	}

	public void setIsAccommodativeRole(Boolean isAccommodativeRole) {
		this.isAccommodativeRole = isAccommodativeRole;
	}

	public Long[] getuWIssuesCheckListId() {
		return uWIssuesCheckListId;
	}

	public void setuWIssuesCheckListId(Long[] uWIssuesCheckListId) {
		this.uWIssuesCheckListId = uWIssuesCheckListId;
	}

	public Long[] getuWManualIssuesCheckListId() {
		return uWManualIssuesCheckListId;
	}

	public void setuWManualIssuesCheckListId(Long[] uWManualIssuesCheckListId) {
		this.uWManualIssuesCheckListId = uWManualIssuesCheckListId;
	}

	public UwIssuesLetterInfoVO getUwIssuesLetterInfoVO() {
		return uwIssuesLetterInfoVO;
	}

	public void setUwIssuesLetterInfoVO(UwIssuesLetterInfoVO uwIssuesLetterInfoVO) {
		this.uwIssuesLetterInfoVO = uwIssuesLetterInfoVO;
	}

	public String getRetMsg() {
		return retMsg;
	}

	public void setRetMsg(String retMsg) {
		this.retMsg = retMsg;
	}

	public String[] getuWIssuesletterContent() {
		return uWIssuesletterContent;
	}

	public void setuWIssuesletterContent(String[] uWIssuesletterContent) {
		this.uWIssuesletterContent = uWIssuesletterContent;
	}

	public String[] getuWManualIssuesletterContent() {
		return uWManualIssuesletterContent;
	}

	public void setuWManualIssuesletterContent(String[] uWManualIssuesletterContent) {
		this.uWManualIssuesletterContent = uWManualIssuesletterContent;
	}

	public String getUwRuleObjStr() {
		return uwRuleObjStr;
	}

	public void setUwRuleObjStr(String uwRuleObjStr) {
		this.uwRuleObjStr = uwRuleObjStr;
	}

	public String getIsIframe() {
		return IsIframe;
	}

	public void setIsIframe(String isIframe) {
		IsIframe = isIframe;
	}

	public String[] getApplyImageList() {
		return applyImageList;
	}

	public void setApplyImageList(String[] applyImageList) {
		this.applyImageList = applyImageList;
	}

	public List<DocumentFormFile> getUploadFileList() {
		return uploadFileList;
	}

	public void setUploadFileList(List<DocumentFormFile> uploadFileList) {
		this.uploadFileList = uploadFileList;
	}

	public String getHolderCertiCode() {
		return holderCertiCode;
	}

	public void setHolderCertiCode(String holderCertiCode) {
		this.holderCertiCode = holderCertiCode;
	}

	/**
	 * <p>Description :來源為保全核保 </p>
	 * <p>Created By : TGL144</p>
	 * <p>Create Time : Nov 29, 2016</p>
	 * @return
	 */
	public boolean isCsUw() {
		return isCsUw;
	}
	/**
	 * <p>Description :來源為保全核保 </p>
	 * <p>Created By : TGL144</p>
	 * <p>Create Time : Nov 29, 2016</p>
	 * @param isCsUw
	 */
	public void setCsUw(boolean isCsUw) {
		this.isCsUw = isCsUw;
	}

	public Long getChangeId() {
		return changeId;
	}

	public void setChangeId(Long changeId) {
		this.changeId = changeId;
	}

	/**
	 * @return 傳回 isUnderwriter。
	 */
	public boolean getIsUnderwriter() {
		return isUnderwriter;
	}

	/**
	 * @param isUnderwriter 要設定的 isUnderwriter。
	 */
	public void setIsUnderwriter(boolean isUnderwriter) {
		this.isUnderwriter = isUnderwriter;
	}

	public String getRemoteIndi() {
		return remoteIndi;
	}

	public void setRemoteIndi(String remoteIndi) {
		this.remoteIndi = remoteIndi;
	}

	public String getRemoteOverseas() {
		return remoteOverseas;
	}

	public void setRemoteOverseas(String remoteOverseas) {
		this.remoteOverseas = remoteOverseas;
	}

	public String getRemoteCallout() {
		return remoteCallout;
	}

	public void setRemoteCallout(String remoteCallout) {
		this.remoteCallout = remoteCallout;
	}

	public String getRemoteCheck() {
		return remoteCheck;
	}

	public void setRemoteCheck(String remoteCheck) {
		this.remoteCheck = remoteCheck;
	}

	public String getElderCalloutFlag() {
		return elderCalloutFlag;
	}

	public void setElderCalloutFlag(String elderCalloutFlag) {
		this.elderCalloutFlag = elderCalloutFlag;
	}
	
}
