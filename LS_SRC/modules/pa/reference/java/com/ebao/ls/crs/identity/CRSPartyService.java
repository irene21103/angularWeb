package com.ebao.ls.crs.identity;

import java.util.List;

import com.ebao.foundation.commons.exceptions.GenericException;
import com.ebao.ls.crs.data.bo.CRSParty;
import com.ebao.ls.crs.vo.CRSPartyLogVO;
import com.ebao.ls.crs.vo.CRSPartyVO;
import com.ebao.ls.foundation.service.GenericService;
import com.ebao.ls.pub.CodeCst;

public interface CRSPartyService extends GenericService<CRSPartyVO>{
	public final static String BEAN_DEFAULT = "crsPartyService";

	public abstract CRSPartyVO findByCertiCode(String certiCode);
	
	public abstract CRSPartyVO findByCrsId(Long crsId);
	
	public abstract void saveOrUpdate(Long handerId, java.util.Date processDate, CRSPartyVO partyVO, Long changeId, String inputSource) throws GenericException;
	/**
	 * PCR 257607 330030 e-Approval ITR-1801051_CRS共同申報及盡職審查專案- 保全變更及CRS確認申報國作業
	 * @param name
	 * @param policyCode
	 * @param effType
	 * @return
	 */
	public List<CRSParty> findListByNameAndPolicyCode(String name, String policyCode, String effType);
	/**
	 * PCR 257607 330030 e-Approval ITR-1801051_CRS共同申報及盡職審查專案- 保全變更及CRS確認申報國作業
	 * @param certiCode
	 * @param effType
	 * @return
	 */
	public List<CRSParty> findListByCertiCode(String certiCode, String effType);
	/**
	 * PCR 257607 330030 e-Approval ITR-1801051_CRS共同申報及盡職審查專案- 保全變更及CRS確認申報國作業
	 * pos取得同一保單CRS 關聯主檔(by CertiCode or Name ) 
	 * @param policyCode
	 * @param certiCode
	 * @param name
	 * @return CRSPartyVO 
	 */
	public CRSPartyVO findPosCrsParty(String policyCode, String certiCode,  String name) ;
	/**
	 * IR354118
	 * pos取得同一保單CRS 關聯主檔(by CertiCode or Name 取得多筆) 
	 * @param policyCode
	 * @param certiCode
	 * @param name
	 * @return CRSPartyVO 
	 */
	public List<CRSPartyVO> findPosCrsPartyList(String policyCode, String certiCode,  String name) ;
}
