package com.ebao.ls.crs.batch.service;

import com.ebao.ls.fatca.vo.FatcaDueDiligenceListVO;
import com.ebao.ls.foundation.service.GenericService;
import com.ebao.ls.pty.vo.FatcaSurveyPopulationVO;

public interface CrsMotherService extends GenericService<FatcaSurveyPopulationVO>{
	public static final String BEAN_DEFAULT = "crsSurveyPopulationService";
	
	public abstract FatcaDueDiligenceListVO findLastestAgent(Long partyId, String certiCode, Long policyId, String policyCode);
}
