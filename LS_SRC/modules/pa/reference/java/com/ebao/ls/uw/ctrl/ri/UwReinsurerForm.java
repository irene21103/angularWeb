package com.ebao.ls.uw.ctrl.ri;

import com.ebao.pub.framework.GenericForm;

public class UwReinsurerForm extends GenericForm {

	private static final long serialVersionUID = 2884803086396294698L;

	private String reinsurerName;
	
	private Long reinsurerId;

	private String contact;

	private Long telReg;

	private Long tel;

	private Long telExt;

	private Long faxReg;

	private Long fax;

	private String email;

	public String getReinsurerName() {
		return reinsurerName;
	}

	public void setReinsurerName(String reinsurerName) {
		this.reinsurerName = reinsurerName;
	}

	public Long getReinsurerId() {
		return reinsurerId;
	}

	public void setReinsurerId(Long reinsurerId) {
		this.reinsurerId = reinsurerId;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public Long getTelReg() {
		return telReg;
	}

	public void setTelReg(Long telReg) {
		this.telReg = telReg;
	}

	public Long getTel() {
		return tel;
	}

	public void setTel(Long tel) {
		this.tel = tel;
	}

	public Long getTelExt() {
		return telExt;
	}

	public void setTelExt(Long telExt) {
		this.telExt = telExt;
	}

	public Long getFaxReg() {
		return faxReg;
	}

	public void setFaxReg(Long faxReg) {
		this.faxReg = faxReg;
	}

	public Long getFax() {
		return fax;
	}

	public void setFax(Long fax) {
		this.fax = fax;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
