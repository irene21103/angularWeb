package com.ebao.ls.crs.nb.impl;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.hibernate.criterion.Restrictions;

import com.ebao.ls.crs.constant.CRSCodeCst;
import com.ebao.ls.crs.data.bo.NBcrs;
import com.ebao.ls.crs.identity.CRSPartyLogService;
import com.ebao.ls.crs.nb.NBCrsCI;
import com.ebao.ls.crs.nb.NBcrsDAO;
import com.ebao.ls.crs.nb.NBcrsService;
import com.ebao.ls.crs.vo.CRSPartyLogVO;
import com.ebao.ls.crs.vo.NBcrsVO;
import com.ebao.ls.foundation.service.GenericServiceImpl;
import com.ebao.ls.pa.nb.util.NBUtils;
import com.ebao.pub.framework.AppContext;

public class NBcrsServiceImpl extends GenericServiceImpl<NBcrsVO, NBcrs, NBcrsDAO>
				implements NBcrsService, NBCrsCI {

	@Resource(name = CRSPartyLogService.BEAN_DEFAULT)
	private CRSPartyLogService crsPartyLogServ;

	@Override
	protected NBcrsVO newEntityVO() {
		// TODO Auto-generated method stub
		return new NBcrsVO();
	}

	@Override
	public NBcrsVO findFirstByPolicyId(Long policyId) {
		List<NBcrsVO> resultList = this.findByPolicyId(policyId);
		if (CollectionUtils.isEmpty(resultList))
			return null;
		return resultList.get(0);
	}


	@Override
	public NBcrsVO findByChangeId(Long changeId) {
		List<NBcrsVO> resultList = this.find(Restrictions.eq("changeId", changeId));
		if (CollectionUtils.isEmpty(resultList))
			return null;
		return resultList.get(0);
	}
	
	@Override
	public List<NBcrsVO> findByPolicyId(Long policyId) {
		return this.convertToVOList(dao.findByPolicyId(policyId));
	}

	@Override
	public void reversalCRS(Long policyId) {

		//更新CRS主檔為取消
		NBcrsVO nbCRSVO = this.findFirstByPolicyId(policyId);
		if (nbCRSVO != null) {
			CRSPartyLogVO backLogVO = this.crsPartyLogServ.findByChangeIdAndCertiCode(nbCRSVO.getChangeId(), null);
			//撤件後回退重新存檔，將狀態更新為建檔中
			if (backLogVO != null && NBUtils.in(backLogVO.getBuildType(),
							CRSCodeCst.BUILD_TYPE__REMOVE)) {
				NBUtils.logger(this.getClass(), "更新CRS主檔狀態02");
				backLogVO.setBuildType(CRSCodeCst.BUILD_TYPE__BUILDING);
				
				
				Long userId = AppContext.getCurrentUser().getUserId();
				//更新維護人員/維護部門/維護日
				this.crsPartyLogServ.saveOrUpdate(userId, 
								AppContext.getCurrentUserLocalTime(), 
								backLogVO, backLogVO.getChangeId(), 
								CRSCodeCst.INPUT_SOURCE__LINK);
			}
		}
	}

}
