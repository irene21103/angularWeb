package com.ebao.ls.uw.ctrl.decision;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.TreeMap;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.ls.arap.pub.ci.CashCI;
import com.ebao.ls.cs.ci.CSCI;
import com.ebao.ls.cs.ci.vo.PolicyChangeCIVO;
import com.ebao.ls.fnd.ci.ChargeDeductionCI;
import com.ebao.ls.pa.nb.ci.ProposalCI;
import com.ebao.ls.pa.nb.ctrl.detailreg.DetailRegHelper;
import com.ebao.ls.pa.pub.ci.CoverageCI;
import com.ebao.ls.pa.pub.ref.api.prd.ProductService;
import com.ebao.ls.pa.pub.ref.vo.prd.ProductBasicVarietyVO;
import com.ebao.ls.pa.pub.ref.vo.prd.ProductVO;
import com.ebao.ls.pa.pub.service.PolicyService;
import com.ebao.ls.pa.pub.vo.CoverageVO;
import com.ebao.ls.pub.CodeCst;
import com.ebao.ls.uw.ctrl.ActionUtil;
import com.ebao.ls.uw.ctrl.ReqParamNameConstants;
import com.ebao.ls.uw.ctrl.UwGenericAction;
import com.ebao.ls.uw.ctrl.pub.CustomerForm;
import com.ebao.ls.uw.ctrl.pub.UwProductForm;
import com.ebao.ls.uw.ds.constant.UwDecisionConstants;
import com.ebao.ls.uw.ds.constant.UwExceptionConstants;
import com.ebao.ls.uw.ds.sp.QueryUwDataSp;
import com.ebao.ls.uw.ds.vo.PonstponeAndDeclineReasonVO;
import com.ebao.ls.uw.vo.UwPolicyVO;
import com.ebao.ls.uw.vo.UwProductVO;
import com.ebao.pub.annotation.PrdAPIUpdate;
import com.ebao.pub.framework.AppException;
import com.ebao.pub.framework.ExceptionFactory;
import com.ebao.pub.framework.GenericException;
import com.ebao.pub.util.BeanUtils;

/**
 * <p>
 * Title:Underwriting
 * </p>
 * <p>
 * show make benefit decision page
 * </p>
 * <p>
 * Copyright: Copyright (c) 2004
 * </p>
 * <p>
 * Company: eBaoTech Corporation*
 * </p>
 *
 * @author Johnson.sun
 * @version 1.3
 */
public class ShowProductAction extends UwGenericAction {
  private static final Integer CONDITIONAL_ACCEPTED = UwDecisionConstants.CONDITIONAL_UNDERWRITTEN;
public static final String BEAN_DEFAULT = "/uw/uwMakeDecision";

  /**
   * @param mapping
   * @param form
   * @param request
   * @param res
   * @return
   * @throws GenericException
   */
  @Override
  @PrdAPIUpdate
  public ActionForward uwProcess(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse res)
      throws GenericException {
    try {
        
      Long[] itemIds = getItemIds(request);
      Long underwriteId = getUnderwriteId(request);
      
      if (!matchDecision(underwriteId, itemIds)) {
        throw new AppException(UwExceptionConstants.APP_DIFF_BENEFITS_DECISION);
      }
      // find the previous underwriter id.
      UwProductInfoForm uwProductInfoForm = new UwProductInfoForm();
      uwProductInfoForm.setUnderwriteId(underwriteId);
      uwProductInfoForm.setItemIds(itemIds);
      uwProductInfoForm.setPolicyCode(request.getParameter("proposalCode"));
      uwProductInfoForm.setApplyCode(request
          .getParameter(ReqParamNameConstants.PROPOSALNO));
      uwProductInfoForm.setUwStatus(request.getParameter("proposalStatus"));

      String[] benefitType = new String[itemIds.length];

      UwPolicyVO uwPolicyVo = getUwPolicyDS().findUwPolicy(underwriteId);
      uwProductInfoForm.setOrganId(uwPolicyVo.getOrganId());
      // add by simon.huang on 2015-07-01 
      uwProductInfoForm.setApplyDate(uwPolicyVo.getApplyDate());
      
      uwProductInfoForm.setSubmissionDate(uwPolicyVo.getSubmissionDate());
      uwProductInfoForm.setValidateDate(uwPolicyVo.getValidateDate());

      BigDecimal  suspendsePrem = cashCI.getSuspenseByIdPremPurpose(uwPolicyVo.getPolicyId(), 
                      CodeCst.PREM_PURPOSE__GENERAL);
      uwProductInfoForm.setSuspensePremium(suspendsePrem);
      //end 2017-07-01
      
      /* find product info by itemId */
      for (int i = 0; i < itemIds.length; i++) {
        UwProductVO uwProductVo = getUwPolicyDS().findUwProduct(underwriteId,
            itemIds[i]);
        // get the scope of occupation class
        Long productId = Long.valueOf(uwProductVo.getProductId().intValue());
        if (i == 0) {
          getJobClassScope(request, uwProductVo);
        }
        ProductVO showAttributeVO = getProductService().getProduct(productId,policyService.getActiveVersionDate(uwProductVo));
        benefitType[i] = showAttributeVO.getBenefitType();
        UwProductForm uwProductForm = new UwProductForm();
        // get Status Date
        CoverageVO coverageVO = coverageCI.retrieveByItemId(itemIds[i]);
        uwProductForm.setStatusDate(coverageVO.getRiskStatusDate());
        BeanUtils.copyProperties(uwProductForm, uwProductVo);
        // get Next Payment Date of the policy
        Date dueDate;
        if (coverageVO.getCoverageExtend() == null
            || coverageVO.getCoverageExtend().getDueDate() == null) {
          dueDate = uwProductVo.getValidateDate();
        } else {
          dueDate = coverageVO.getCoverageExtend().getDueDate();
        }
        uwProductForm.setPaidupDate(dueDate);
        
        //add by simon.huang on 2015-07-01,取得商品名稱
        String productName = policyDS.getCoverageProudctName(coverageVO);
        		
        uwProductForm.setProductName(productName);
        uwProductInfoForm.setFirstPayPremium(uwProductVo.getTotalPremAf()); // totalPrem = discntedPrem+extraPrem
        //end 2017-07-01
        
        // added by hendry.xu to meet the cs uw add rider.
        // when the cs uw add rider
        // 1.Next payment date: Blank
        // 2.Benefit Status: waiting for validate
        // 3.Status Date: Blank

        Long changeId = uwPolicyVo.getChangeId();
        if (changeId != null
            && (uwPolicyVo.getUwSourceType().equals(CodeCst.UW_SOURCE_TYPE__CS) || uwPolicyVo
                .getUwSourceType().equals(CodeCst.UW_SOURCE_TYPE__CS_RE))) {

          PolicyChangeCIVO[] policyChangeIds = csCI
              .getPolicyChangesByMasterChangeIdOrderByOrderIdDesc(changeId);
          for (int j = 0; j < policyChangeIds.length; j++) {
            if (uwPolicyVo.getCsTransaction().intValue() == CodeCst.SERVICE__ADD_RIDER) {
              PolicyChangeCIVO policyChange = policyChangeIds[j];
              Long policyChangeId = policyChange.getPolicyChgId();
              boolean isAddRider = csCI.hasChanged(changeId, policyChangeId,
                  "T_CONTRACT_PRODUCT", itemIds[i], "1");
              if (isAddRider) {
                uwProductForm.setLiabilityState(Integer
                    .valueOf(CodeCst.LIABILITY_STATUS__NON_VALIDATE));
                uwProductForm.setPaidupDate(null);
                uwProductForm.setStatusDate(null);
              }
            }
          }
        }
        boolean isRenew = showAttributeVO.isRenew();
        // add for defect GEL00039088 2008/2/1 by hanzhong.yan
        if (uwProductForm.getRenewDesc() == null) {
          if (isRenew) {
            uwProductForm.setRenewDesc("Y");
          } else {
            uwProductForm.setRenewDesc("N");
          }
        }
        // end add
        Collection insureds = getUwPolicyDS().findUwLifeInsuredEntitis(
            underwriteId, itemIds[i]);
        insureds = BeanUtils.copyCollection(CustomerForm.class, insureds);
        getMAMSFactorInfo(insureds, uwProductVo);
        uwProductForm.setInsureds((CustomerForm[]) insureds
            .toArray(new CustomerForm[insureds.size()]));
        /** set indx_indx from TCP level **/
        String indxIndicator = QueryUwDataSp.getIndxIndicator(itemIds[i]);
        uwProductForm.setIndxIndicator(indxIndicator);
        /*********************************/
        uwProductInfoForm.getProducts().add(uwProductForm);
        ProductVO jobClassVO = getProductService().getProduct(
            Long.valueOf(uwProductVo.getProductId()),policyService.getActiveVersionDate(uwProductVo));
        uwProductForm.setJobDiff(jobClassVO.isJobDiff());
        uwProductForm.setUnderwriteJob(jobClassVO.getUnderwriteJob());
        List<String> extraAriths = getProductService().getProduct(
            Long.valueOf(uwProductVo.getProductId()),policyService.getActiveVersionDate(uwProductVo)).getExtraTypes();
        uwProductForm.setExtraArithsAvailable(extraAriths.size() > 0);
        request.setAttribute("isRenew", String.valueOf(isRenew));
      }
      uwProductInfoForm.setPreUnderwriterId(QueryUwDataSp
          .getPreviousUnderwriter(underwriteId));
      /* get userId by method ,Maybe could get it by session */
      uwProductInfoForm.setBenefitType(benefitType);
      // @todo ommit the return value
      // upiForm.setApplyFac(true);
      uwProductInfoForm.setApplyFac(proposalCI.hasPrivilegeToApplyFac());
      uwProductInfoForm.setOperatorId(String.valueOf(ActionUtil
          .getUnderwriterId()));
      request.setAttribute("action_form", uwProductInfoForm);
      /* set the policy's uw source type */
      UwPolicyVO uwPolicyVO = getUwPolicyDS().findUwPolicy(underwriteId);
      request.setAttribute("uwSourceType", uwPolicyVO.getUwSourceType());
      getPostponeAndDeclineReasonList(request);
      return mapping.findForward("product");
    } catch (Exception ex) {
      throw ExceptionFactory.parse(ex);
    }
  }

  /**
   * get the postpone and decline reason
   *
   * @param request
   * @throws GenericException
   */
  protected void getPostponeAndDeclineReasonList(HttpServletRequest request)
      throws GenericException {
    List postponeAndDeclineReasons = (List) getUwPolicyDS()
        .findPostponeAndDeclineReason();
    StringBuffer postponebuffer = new StringBuffer();
    StringBuffer declinebuffer = new StringBuffer();
    int i = 0;
    int j = 0;
    for (Iterator iter = postponeAndDeclineReasons.iterator(); iter.hasNext();) {
      PonstponeAndDeclineReasonVO element = (PonstponeAndDeclineReasonVO) iter
          .next();
      String decisonType = element.getDecisionType();
      String decisonReason = element.getDecisionReason();
      String reasondesc = element.getReasonDesc();
      if ("POSTPONE".equals(decisonType)) {
        if (i > 0) {
          postponebuffer.append(",");
        }
        postponebuffer.append("new Array('");
        postponebuffer.append(decisonReason);
        postponebuffer.append("','");
        postponebuffer.append(decisonReason);
        postponebuffer.append("','");
        postponebuffer.append(reasondesc);
        postponebuffer.append("')");
        i++;
      } else {
        if (j > 0) {
          declinebuffer.append(",");
        }
        declinebuffer.append("new Array('");
        declinebuffer.append(decisonReason);
        declinebuffer.append("','");
        declinebuffer.append(decisonReason);
        declinebuffer.append("','");
        declinebuffer.append(reasondesc);
        declinebuffer.append("')");
        j++;
      }
    }
    request.setAttribute("declineReason", declinebuffer.toString());
    request.setAttribute("postponeReason", postponebuffer.toString());
  }

  /**
   * match selected products decisions
   *
   * @param underwriteId
   * @param itemId
   * @return boolean
   * @throws Exception
   */
  protected boolean matchDecision(Long underwriteId, Long[] itemId)
      throws GenericException {
    if (itemId.length > 1) {
      UwProductVO[] products = new UwProductVO[itemId.length];
      for (int i = 0; i < itemId.length; i++) {
        products[i] = getUwPolicyDS().findUwProduct(underwriteId, itemId[i]);
      }
      for (int i = 0; i < itemId.length; i++) {
        /*
         * If underwriting decision is "Conditional Accepted" system to display
         * error message "Different underwriting decision found. Please update
         * underwriting decision individually."
         */
        if (CONDITIONAL_ACCEPTED.equals(products[i].getDecisionId())) {
          return false;
        }
        /*
         * If the products were given underwriting decision before and/or the
         * decisions are not the same for all benefits, system to display error
         * message "Different underwriting decision found. Please update
         * underwriting decision individually."
         */
        for (int j = 0; j < itemId.length; j++) {
          if (null != products[i].getDecisionId()) {
            if (!products[i].getDecisionId()
                .equals(products[j].getDecisionId())) {
              return false;
            }
          } else {
            if (null != products[j].getDecisionId()) {
              return false;
            }
          }
        }// end of inner for
      }// end for
    }
    return true;
  }

  /**
   * Get Underwrite Id from request parameter or request attribute
   *
   * @param request HttpServletRequest
   * @return Underwrite Id
   * @throws GenericException Application Exception
   * @author jason.luo
   * @since 10.19.2004
   */
  // a re-orginize of the original code
  public static Long getUnderwriteId(HttpServletRequest request)
      throws GenericException {
    if (request.getParameter(ReqParamNameConstants.POLICY_UNDERWRITEID) != null) {
      return UwGenericAction.getUnderwriteId(request);
    } else {
      return (Long) request
          .getAttribute(ReqParamNameConstants.POLICY_UNDERWRITEID);
    }
  }

  /**
   * Get Underwrite Id from request parameter or request attribute
   *
   * @param request HttpServletRequest
   * @return Product Item Ids
   * @author jason.luo
   * @since 10.19.2004
   */
  protected Long[] getItemIds(HttpServletRequest request) {
    String underwriteId = request
        .getParameter(ReqParamNameConstants.POLICY_UNDERWRITEID);
    if (underwriteId != null) {
      String[] tempId = request
          .getParameterValues(ReqParamNameConstants.BENEFIT_ITEM_ID);
      if (tempId == null) {
        tempId = request.getParameterValues("itemIds");
      }
      Long[] itemId = new Long[tempId.length];
      for (int i = 0; i < tempId.length; i++) {
        if (null != tempId[i] && tempId[i].trim().length() > 0) {
          itemId[i] = Long.valueOf(tempId[i].trim());
        }
      }
      return itemId;
    } else {
      return (Long[]) request
          .getAttribute(ReqParamNameConstants.BENEFIT_ITEM_ID);
    }
  }

  /**
   * get job class scope from t_premium_rate
   *
   * @param request
   * @param productId
   * @return "jobClass1,jobClass2"
   * @throws GenericException
   */
  protected void getJobClassScope(HttpServletRequest request, UwProductVO uwProudctVO)
      throws GenericException {
    StringBuffer jobClassScope = new StringBuffer();
    ProductBasicVarietyVO productBasicVarietyCIVO = getProductService()
        .getBasicVariety(uwProudctVO.getProductId().longValue(),policyService.getActiveVersionDate(uwProudctVO));
    String strTemp = "";
    // get job class
    if (productBasicVarietyCIVO != null
        && productBasicVarietyCIVO.getTmJobCate() != null) {
      TreeMap<String, String> tmJobCate = productBasicVarietyCIVO
          .getTmJobCate();
      if (tmJobCate != null) {
        Iterator<String> jobCateSet = tmJobCate.keySet().iterator();
        while (jobCateSet.hasNext()) {
          String jobCate = jobCateSet.next();
          jobClassScope.append(strTemp);
          jobClassScope.append(jobCate);
          strTemp = ",";
        }
      }
    }
    if (jobClassScope != null) {
      request.setAttribute("OccScope", jobClassScope.toString());
    }
  }

  @PrdAPIUpdate
  protected void getMAMSFactorInfo(Collection insureds, UwProductVO uwProductVo)
      throws GenericException {
    Iterator iter = insureds.iterator();
    CustomerForm customer = null;
    while (iter.hasNext()) {
      customer = (CustomerForm) iter.next();
      /* to decide whether MS/MA factor existed in the page */
      boolean isMsFactor = false;
      boolean isMaFactor = false;
      ProductVO showVO = getProductService().getProduct(
          Long.valueOf(uwProductVo.getProductId().intValue()),policyService.getActiveVersionDate(uwProductVo));
      if (showVO.isMaFactor()) {
        isMaFactor = true;
      }
      if (showVO.isMsFactor()) {
        isMsFactor = true;
      }
      // set ma factor range
      if (isMaFactor) {
        if (uwProductVo.getUwsafactor() != null) {
          customer.setMaFactor(String.valueOf(uwProductVo.getUwsafactor()
              .floatValue()));
        } else {
          isMaFactor = false;
        }
        customer.setIsMaFactor(String.valueOf(isMaFactor));
        BigDecimal[] rtn = getFactorScope(uwProductVo,
            Integer.valueOf(CodeCst.ILP_FACTOR_TYPE__MA_FACTOR));
        if (rtn[0] != null) {
          customer.setMa_min(String.valueOf(rtn[0]));
        } else {
          customer.setMa_min(String.valueOf(0));
        }
        if (rtn[1] != null) {
          customer.setMa_max(String.valueOf(rtn[1]));
        } else {
          customer.setMa_max(String.valueOf(999));
        }
      }
      // set ms factor range
      if (isMsFactor) {
        if (uwProductVo.getUwmsfactor() != null) {
          customer.setMsFactor(String.valueOf(uwProductVo.getUwmsfactor()
              .floatValue()));
        } else {
          isMsFactor = false;
        }
        customer.setIsMsFactor(String.valueOf(isMsFactor));
        BigDecimal[] rtn = getFactorScope(uwProductVo,
            Integer.valueOf(CodeCst.ILP_FACTOR_TYPE__MS_FACTOR));
        if (rtn[0] != null) {
          customer.setMs_min(String.valueOf(rtn[0]));
        } else {
          customer.setMs_min(String.valueOf(0));
        }
        if (rtn[1] != null) {
          customer.setMs_max(String.valueOf(rtn[1]));
        } else {
          customer.setMs_max(String.valueOf(999));
        }
      }
    }
  }

  /*
   * get scope of ma or ma factor @param productVO @param req @throws
   * GenericException
   */
  private BigDecimal[] getFactorScope(UwProductVO uwProductVo,
      Integer factorType) throws GenericException {
    // get sa factor scope
    BigDecimal[] decimal;
    if (factorType.intValue() == 8) {
      factorType = Integer.valueOf(1);
    } else {
      factorType = Integer.valueOf(2);
    }
    decimal = chargeDeductionCI.getFactorBound(
        Long.valueOf(uwProductVo.getProductId().intValue()),
        String.valueOf(uwProductVo.getPayMode()), uwProductVo.getStandLife1(),
        factorType, uwProductVo.getPolicyId());
    return decimal;
  }

  @Resource(name = ProposalCI.BEAN_DEFAULT)
  private ProposalCI proposalCI;

  public ProposalCI getProposalCI() {
    return proposalCI;
  }

  public void setProposalCI(ProposalCI proposalCI) {
    this.proposalCI = proposalCI;
  }

  @Resource(name = ChargeDeductionCI.BEAN_DEFAULT)
  private ChargeDeductionCI chargeDeductionCI;

  public ChargeDeductionCI getChargeDeductionCI() {
    return chargeDeductionCI;
  }

  public void setChargeDeductionCI(ChargeDeductionCI chargeDeductionCI) {
    this.chargeDeductionCI = chargeDeductionCI;
  }

  @Resource(name = ProductService.BEAN_DEFAULT)
  private ProductService productService;

  public ProductService getProductService() {
    return productService;
  }

  public void setProductService(ProductService productService) {
    this.productService = productService;
  }

  @Resource(name = CoverageCI.BEAN_DEFAULT)
  private CoverageCI coverageCI;

  /**
   * @return the coverageCI
   */
  public CoverageCI getCoverageCI() {
    return coverageCI;
  }

  /**
   * @param coverageCI the coverageCI to set
   */
  public void setCoverageCI(CoverageCI coverageCI) {
    this.coverageCI = coverageCI;
  }

  @Resource(name = CSCI.BEAN_DEFAULT)
  private CSCI csCI;

  public CSCI getCsCI() {
    return csCI;
  }

  public void setCsCI(CSCI csCI) {
    this.csCI = csCI;
  }
  
  @Resource(name = PolicyService.BEAN_DEFAULT)
  PolicyService policyService;
  
  @Resource(name = DetailRegHelper.BEAN_DEFAULT)
  protected DetailRegHelper detailRegHelper;
  
  @Resource(name = CashCI.BEAN_DEFAULT)
  protected CashCI cashCI;

  @Resource(name = com.ebao.ls.pa.pub.service.PolicyService.BEAN_DEFAULT)
  com.ebao.ls.pa.pub.service.PolicyService policyDS;

  
  
}