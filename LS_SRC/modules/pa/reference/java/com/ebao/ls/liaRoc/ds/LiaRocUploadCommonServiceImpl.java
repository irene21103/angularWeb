package com.ebao.ls.liaRoc.ds;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.Resource;
import javax.transaction.UserTransaction;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;

import com.ebao.foundation.common.lang.exception.ExceptionUtils;
import com.ebao.foundation.commons.exceptions.ExceptionUtil;
import com.ebao.foundation.module.para.Para;
import com.ebao.ls.gl.bs.sp.ExchangeRateServiceSP;
import com.ebao.ls.liaRoc.LiaRocCst;
import com.ebao.ls.liaRoc.LiaRocCst.LiarocPolicyStatus;
import com.ebao.ls.liaRoc.bo.LiaRocUpload;
import com.ebao.ls.liaRoc.bo.LiaRocUpload107Detail;
import com.ebao.ls.liaRoc.bo.LiaRocUpload107Detail2020;
import com.ebao.ls.liaRoc.bo.LiaRocUpload2020;
import com.ebao.ls.liaRoc.bo.LiaRocUploadDetail;
import com.ebao.ls.liaRoc.bo.LiaRocUploadDetail2020;
import com.ebao.ls.liaRoc.bo.LiaRocUploadError;
import com.ebao.ls.liaRoc.bo.LiaRocUploadError902;
import com.ebao.ls.liaRoc.ci.LiaRocRptCI;
import com.ebao.ls.liaRoc.ci.LiaRocUploadCommonService;
import com.ebao.ls.liaRoc.data.LiaRocUpload107Detail2020Dao;
import com.ebao.ls.liaRoc.data.LiaRocUpload107DetailDao;
import com.ebao.ls.liaRoc.data.LiaRocUpload2020Dao;
import com.ebao.ls.liaRoc.data.LiaRocUploadDao;
import com.ebao.ls.liaRoc.data.LiaRocUploadDetail2020Dao;
import com.ebao.ls.liaRoc.data.LiaRocUploadDetailDao;
import com.ebao.ls.liaRoc.data.LiaRocUploadError902Dao;
import com.ebao.ls.liaRoc.data.LiaRocUploadErrorDao;
import com.ebao.ls.liaRoc.data.LiaRocUploadPolicyDao;
import com.ebao.ls.pa.nb.util.NBUtils;
import com.ebao.ls.pa.pub.vo.UwNotintoRecvVO;
import com.ebao.ls.product.service.LifeProductCategoryService;
import com.ebao.ls.product.service.LifeProductService;
import com.ebao.ls.pub.CodeCst;
import com.ebao.ls.rstf.ci.liaroc20.vo.internal.Liaroc20UploadRs902DataVO;
import com.ebao.ls.rstf.ci.liaroc20.vo.internal.Liaroc20UploadRs902VO;
import com.ebao.ls.uw.ds.UwNotintoRecvService;
import com.ebao.ls.ws.vo.RqHeader;
import com.ebao.ls.ws.vo.cust.liaRoc.LiaRocDetailInfoWSVO;
import com.ebao.ls.ws.vo.cust.liaRoc.LiaRocHeaderVO;
import com.ebao.ls.ws.vo.cust.liaRoc.LiaRocUploadProcessResultRootVO;
import com.ebao.pub.framework.ExceptionFactory;
import com.ebao.pub.util.BeanUtils;
import com.ebao.pub.util.DBean;
import com.ebao.pub.util.DateUtils;
import com.ebao.pub.util.Trans;
import com.ebao.pub.util.TransUtils;
import com.tgl.tools.jaxb.adapter.date.TaiwanDateAdapter;

public class LiaRocUploadCommonServiceImpl implements LiaRocUploadCommonService {

	@Resource(name = LifeProductCategoryService.BEAN_DEFAULT)
	private LifeProductCategoryService lifeProductCategoryService;

	@Resource(name = LifeProductService.BEAN_DEFAULT)
	private LifeProductService lifeProductService;

	@Resource(name = LiaRocUploadDao.BEAN_DEFAULT)
	private LiaRocUploadDao liaRocUploadDao;
	
	@Resource(name = LiaRocUpload2020Dao.BEAN_DEFAULT)
	private LiaRocUpload2020Dao liaRocUpload2020Dao;
	
	@Resource(name = UwNotintoRecvService.BEAN_DEFAULT)
	private UwNotintoRecvService uwNotintoRecvService;

	@Resource(name = LiaRocUploadPolicyDao.BEAN_DEFAULT)
	private LiaRocUploadPolicyDao liaRocUploadPolicyDao;

	@Resource(name = LiaRocUploadDetailDao.BEAN_DEFAULT)
	private LiaRocUploadDetailDao liaRocUploadDetailDao;

	@Resource(name = LiaRocUploadDetail2020Dao.BEAN_DEFAULT)
	private LiaRocUploadDetail2020Dao liaRocUploadDetail2020Dao;

	@Resource(name = LiaRocUploadErrorDao.BEAN_DEFAULT)
	private LiaRocUploadErrorDao<LiaRocUploadError> liaRocUploadErrorDao;

	@Resource(name = LiaRocUploadError902Dao.BEAN_DEFAULT)
	private LiaRocUploadError902Dao<LiaRocUploadError902> liaRocUploadError902Dao;

	@Resource(name = LiaRocUpload107DetailDao.BEAN_DEFAULT)
	private LiaRocUpload107DetailDao liaRocUpload107DetailDao;

	@Resource(name = LiaRocUpload107Detail2020Dao.BEAN_DEFAULT)
	private LiaRocUpload107Detail2020Dao liaRocUpload1072020DetailDao;

	@Resource(name = LiaRocRptCI.BEAN_DEFAULT)
	private LiaRocRptCI liaRocRptCI;

	org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(this.getClass());
	/**
	 * 由ExchangeServiceImpl.execute(StandardRequest request) 呼叫
	 */
	@Override
	public void execute(com.ebao.ls.ws.vo.RqHeader rqHeader, LiaRocUploadProcessResultRootVO vo) {
		if (rqHeader == null || vo == null || vo.getHeader() == null)
			return;

		LiaRocUploadProcessResultRootVO responseVO = vo;
		LiaRocHeaderVO responseHeaderVO = responseVO.getHeader();
		
		logger.info("[LiaRocUpload Result]=RequestUUID =" + rqHeader.getPrevRqUID() + "|" + vo.toString());
		if("2020".equals(responseHeaderVO.getVersion())) {
			execute2020(rqHeader, vo);
		} else if("902".equals(responseHeaderVO.getVersion())) {
			execute902(rqHeader, vo);
		} else {
			execute2019(rqHeader, vo);
		}
	
	}
	

	public void execute2019(com.ebao.ls.ws.vo.RqHeader rqHeader, LiaRocUploadProcessResultRootVO vo) {

		if (rqHeader == null || vo == null)
			return;

		LiaRocUploadProcessResultRootVO responseVO = vo;
		LiaRocHeaderVO responseHeaderVO = responseVO.getHeader();
		String version = "2019";
		java.util.Date responseTime = null;
		if(responseHeaderVO != null && StringUtils.isNotEmpty(responseHeaderVO.getResponseTime())) {
			try {
				responseTime = org.apache.commons.lang3.time.DateUtils.parseDate(responseHeaderVO.getResponseTime(), "yyyyMMddHHmmssSSS");
			} catch (ParseException e) {
				logger.error(String.format( "LiaRocHeaderVO.responseTime := %s, execute := %s" , responseHeaderVO.getResponseTime(), ExceptionUtils.getFullStackTrace(e)));
			}
		}
		///Date responseDate = AppContext.getCurrentUserLocalTime();
		
		logger.info("[LiaRocUpload Result]=RequestUUID =" + rqHeader.getPrevRqUID() + "|" + vo.toString());

		// 以 UUID 查詢出該次上傳公會通報的所有資料 ListID
		List<Long> allRequestUploadIds = liaRocUploadDao.queryListIDsByRequestUUID(rqHeader.getPrevRqUID());
		if (allRequestUploadIds == null || allRequestUploadIds.size() == 0) {
			return;
		}

		List<Long> errorUploadIDs = new ArrayList<Long>();

		UserTransaction trans = null;
		try {
			trans = Trans.getUserTransaction();
			trans.begin();

			// 有失敗的錯誤, 將失敗的通報資料寫入 LiaRocUploadError
			if (responseVO.getHeader().getFail() > 0) {

				for (LiaRocDetailInfoWSVO responseDetailVO : responseVO.getBody().getList()) {
					if (responseDetailVO.getLiability16() != null) {
						//承保收件結果
						// 查出此筆錯誤的回覆通報資料，當初上傳通報的那一筆
						Criteria criteria = liaRocUploadDetailDao.getSession().createCriteria(LiaRocUploadDetail.class);
						criteria.add(Restrictions.eq("liaRocType", responseDetailVO.getLiaRocType()));
						criteria.add(Restrictions.eq("liaRocCompanyCode", responseDetailVO.getLiaRocCompanyCode()));
						criteria.add(this.getEqualOrNullCriterion("name", responseDetailVO.getName()));
						criteria.add(this.getEqualOrNullCriterion("certiCode", responseDetailVO.getCertiCode()));
						criteria.add(this.getEqualOrNullCriterion("birthday", responseDetailVO.getBirthday()));
						criteria.add(this.getEqualOrNullCriterion("gender", responseDetailVO.getGender()));
						criteria.add(this.getEqualOrNullCriterion("policyId", responseDetailVO.getPolicyId()));
						criteria.add(this.getEqualOrNullCriterion("liaRocPolicyType", responseDetailVO.getLiaRocPolicyType()));

						//會因保單分類碼或險種分類碼或險種碼錯誤所以資料表中為空值，增加NULL判斷處理
						criteria.add(this.getEqualOrNullCriterion("liaRocProductCategory", responseDetailVO.getLiaRocProductCategory()));
						criteria.add(this.getEqualOrNullCriterion("liaRocProductType", responseDetailVO.getLiaRocProductType()));
						criteria.add(this.getEqualOrNullCriterion("liaRocPolicyStatus", responseDetailVO.getLiaRocPolicyStatus()));

						criteria.add(Restrictions.eq("checkStatus", LiaRocCst.CHECK_STATUS_UPLOAD));
						
						criteria.add(Restrictions.sqlRestriction("this_.upload_List_Id in (Select list_Id from t_liaroc_upload where request_trans_uuid = '"
										+ rqHeader.getPrevRqUID() + "' ) "));

						List<LiaRocUploadDetail> existDetail = criteria.list();

						if (existDetail != null && existDetail.size() > 0) {

							LiaRocUploadDetail detailBO = existDetail.get(0);

							LiaRocUploadError uploadError = new LiaRocUploadError();
							BeanUtils.copyProperties(uploadError, detailBO);
							String errorMsg = responseDetailVO.getMessage();
							if (errorMsg != null && errorMsg.length() > 170) {
								errorMsg = errorMsg.substring(0, 170);
							}
							uploadError.setErrorMsg(errorMsg);
							uploadError.setLiaRocVersion(version);
							uploadError.setUploadDetailListId(detailBO.getListId());
							liaRocUploadErrorDao.save(uploadError);

							detailBO.setCheckStatus(LiaRocCst.CHECK_STATUS_UPLOAD_ERROR);
							liaRocUploadDetailDao.saveorUpdate(detailBO);
							
							// 記錄有保項失敗的 upload_list_id
							if (!errorUploadIDs.contains(detailBO.getUploadListId())) {
								errorUploadIDs.add(detailBO.getUploadListId());
							}

						} else {
							saveUploadError(responseDetailVO, version);
							logger.info("[LiaRocUpload Result]無法對應 PolicyID=" + responseDetailVO.getPolicyId()
											+ ",errorMsg=" + responseDetailVO.getMessage()
											+ ",criteria=" + criteria.toString());
						}
					} else {
						//理賠通報結果
						// 查出此筆錯誤的回覆通報資料，當初上傳通報的那一筆
						Criteria criteria = liaRocUpload107DetailDao.getSession().createCriteria(LiaRocUpload107Detail.class);
						criteria.add(Restrictions.eq("liaRocType", responseDetailVO.getLiaRocType()));
						criteria.add(Restrictions.eq("liaRocCompanyCode", responseDetailVO.getLiaRocCompanyCode()));
						criteria.add(this.getEqualOrNullCriterion("name", responseDetailVO.getName()));
						criteria.add(this.getEqualOrNullCriterion("certiCode", responseDetailVO.getCertiCode()));
						criteria.add(this.getEqualOrNullCriterion("birthday", responseDetailVO.getBirthday()));
						criteria.add(Restrictions.eq("policyCode", responseDetailVO.getPolicyId()));
						criteria.add(this.getEqualOrNullCriterion("validateDate", responseDetailVO.getValidateDate()));
						criteria.add(this.getEqualOrNullCriterion("expiredDate", responseDetailVO.getExpiredDate()));
						criteria.add(this.getEqualOrNullCriterion("auditorDate", responseDetailVO.getStatusEffectDate()));
						criteria.add(Restrictions.eq("amount", responseDetailVO.getLiability01()));
						criteria.add(Restrictions.eq("insuredType", responseDetailVO.getLiaRocRelationToPh()));
						criteria.add(Restrictions.sqlRestriction("this_.upload_List_Id in (Select list_Id from t_liaroc_upload where request_trans_uuid = '"
										+ rqHeader.getPrevRqUID() + "' ) "));
						List<LiaRocUpload107Detail> existDetail = criteria.list();

						if (existDetail != null && existDetail.size() > 0) {

							LiaRocUpload107Detail detailBO = existDetail.get(0);

							LiaRocUploadError uploadError = new LiaRocUploadError();
							BeanUtils.copyProperties(uploadError, detailBO);
							uploadError.setErrorMsg(responseDetailVO.getMessage());
							uploadError.setLiaRocVersion(version);
							uploadError.setUploadDetail107ListId(detailBO.getListId());
							liaRocUploadErrorDao.save(uploadError);

							// 記錄有保項失敗的 upload_list_id
							if (!errorUploadIDs.contains(detailBO.getUploadListId())) {
								errorUploadIDs.add(detailBO.getUploadListId());
							}

						} else {
							saveUploadError(responseDetailVO, version);
							logger.info("[LiaRocUpload Result]無法對應 PolicyID=" + responseDetailVO.getPolicyId()
											+ ",errorMsg=" + responseDetailVO.getMessage());
						}
					}
				}
			}

			// 修正該批次上傳公會通報資料的通報狀態
			for (Long listId : allRequestUploadIds) {

				LiaRocUpload uploadBO = liaRocUploadDao.load(listId);

				if (errorUploadIDs.contains(uploadBO.getListId())) {
					uploadBO.setLiaRocUploadStatus(LiaRocCst.LIAROC_UL_STATUS_RETURN_ERROR);
				} else {
					uploadBO.setLiaRocUploadStatus(LiaRocCst.LIAROC_UL_STATUS_RETURN);
				}

				uploadBO.setResponseTransUUid(rqHeader.getRqUID());
				uploadBO.setResponseTime(responseTime);

				liaRocUploadDao.save(uploadBO);

			}

			trans.commit();

			if (responseVO.getHeader().getFail() > 0) {
				//公會上傳偵錯報表 發送email與歸檔
				liaRocRptCI.archiveAndSendRpt0080(rqHeader.getPrevRqUID());
			}

			logger.info(" 上傳公會通報資料結果回傳寫檔成功 RequestUUID =" + rqHeader.getPrevRqUID());
		} catch (Exception e) {
			TransUtils.rollback(trans);
			logger.info(" 上傳公會通報資料結果回傳寫檔錯誤" + e);

			throw ExceptionFactory.parse(e);
		}

	}
	
	//新公會回覆檔會一併回傳成功記錄，需排除
	public static final String LIAROC20_SUCCESS = "成功";

	public void execute2020(com.ebao.ls.ws.vo.RqHeader rqHeader, LiaRocUploadProcessResultRootVO vo) {

		if (rqHeader == null || vo == null)
			return;

		LiaRocUploadProcessResultRootVO responseVO = vo;
		LiaRocHeaderVO responseHeaderVO = responseVO.getHeader();
		java.util.Date responseTime = null;
		java.util.Date liaRocReceiveDate = null;
		String version = "2020";
		if(responseHeaderVO != null) {
			try {
				if( StringUtils.isNotEmpty(responseHeaderVO.getResponseTime())) {
					responseTime = org.apache.commons.lang3.time.DateUtils.parseDate(responseHeaderVO.getResponseTime(), "yyyyMMddHHmmssSSS");
				}
				if( StringUtils.isNotEmpty(responseHeaderVO.getLiaRocReceiveDate())) {
					liaRocReceiveDate = org.apache.commons.lang3.time.DateUtils.parseDate(responseHeaderVO.getLiaRocReceiveDate(), "yyyyMMddHHmmssSSS");
				}
			} catch (ParseException e) {
				logger.error(String.format( "LiaRocHeaderVO.responseTime := %s, execute := %s" , responseHeaderVO.getResponseTime(), ExceptionUtils.getFullStackTrace(e)));
			}
		}
		
		// 以 UUID 查詢出該次上傳公會通報的所有資料 ListID
		List<Long> allRequestUploadIds = liaRocUpload2020Dao.queryListIDsByRequestUUID(rqHeader.getPrevRqUID());
		if (allRequestUploadIds == null || allRequestUploadIds.size() == 0) {
			return;
		}

		List<Long> errorUploadIDs = new ArrayList<Long>();

		UserTransaction trans = null;
		try {
			trans = Trans.getUserTransaction();
			trans.begin();

			// 有失敗的錯誤, 將失敗的通報資料寫入 LiaRocUploadError
			if (responseVO.getHeader().getFail() > 0) {

				for (LiaRocDetailInfoWSVO responseDetailVO : responseVO.getBody().getList()) {
					
					//新公會回覆檔會一併回傳成功記錄，需排除
					if(NBUtils.in(LIAROC20_SUCCESS, responseDetailVO.getMessage())) {
						continue;
					}
					
					if (responseDetailVO.getLiability16() != null) {
						//承保收件結果
						// 查出此筆錯誤的回覆通報資料，當初上傳通報的那一筆
						Criteria criteria = liaRocUploadDetail2020Dao.getSession().createCriteria(LiaRocUploadDetail2020.class);
						criteria.add(Restrictions.eq("liaRocType", responseDetailVO.getLiaRocType()));
						criteria.add(Restrictions.eq("liaRocCompanyCode", responseDetailVO.getLiaRocCompanyCode()));
						criteria.add(this.getEqualOrNullCriterion("name", responseDetailVO.getName()));
						criteria.add(this.getEqualOrNullCriterion("certiCode", responseDetailVO.getCertiCode()));
						criteria.add(this.getEqualOrNullCriterion("birthday", responseDetailVO.getBirthday()));
						criteria.add(this.getEqualOrNullCriterion("gender", responseDetailVO.getGender()));
						criteria.add(this.getEqualOrNullCriterion("liaRocPolicyCode", responseDetailVO.getPolicyId()));
						criteria.add(this.getEqualOrNullCriterion("liaRocPolicyType", responseDetailVO.getLiaRocPolicyType()));

						//會因保單分類碼或險種分類碼或險種碼錯誤所以資料表中為空值，增加NULL判斷處理
						criteria.add(this.getEqualOrNullCriterion("liaRocProductCategory", responseDetailVO.getLiaRocProductCategory()));
						criteria.add(this.getEqualOrNullCriterion("liaRocProductType", responseDetailVO.getLiaRocProductType()));
						criteria.add(this.getEqualOrNullCriterion("liaRocPolicyStatus", responseDetailVO.getLiaRocPolicyStatus()));
						
						criteria.add(Restrictions.eq("checkStatus", LiaRocCst.CHECK_STATUS_UPLOAD));
						
						criteria.add(Restrictions.sqlRestriction("this_.upload_List_Id in (Select list_Id from t_liaroc_upload_2020 where request_trans_uuid = '"
										+ rqHeader.getPrevRqUID() + "' ) "));

						List<LiaRocUploadDetail2020> existDetail = criteria.list();

						if (existDetail != null && existDetail.size() > 0) {

							LiaRocUploadDetail2020 detailBO = existDetail.get(0);

							LiaRocUploadError uploadError = new LiaRocUploadError();
							BeanUtils.copyProperties(uploadError, detailBO);
							String errorMsg = responseDetailVO.getMessage();
							if (errorMsg != null && errorMsg.length() > 170) {
								errorMsg = errorMsg.substring(0, 170);
							}
							uploadError.setErrorMsg(errorMsg);
							uploadError.setLiaRocVersion(version);
							uploadError.setUploadDetailListId(detailBO.getListId());
							uploadError.setPolicyId(responseDetailVO.getPolicyId());
							liaRocUploadErrorDao.save(uploadError);

							detailBO.setCheckStatus(LiaRocCst.CHECK_STATUS_UPLOAD_ERROR);
							liaRocUploadDetail2020Dao.saveorUpdate(detailBO);
							
							// 記錄有保項失敗的 upload_list_id
							if (!errorUploadIDs.contains(detailBO.getUploadListId())) {
								errorUploadIDs.add(detailBO.getUploadListId());
							}

						} else {
							saveUploadError(responseDetailVO, version);
							logger.info("[LiaRocUpload Result]無法對應 PolicyID=" + responseDetailVO.getPolicyId()
											+ ",errorMsg=" + responseDetailVO.getMessage()
											+ ",criteria=" + criteria.toString());
						}
					} else {
						//理賠通報結果
						// 查出此筆錯誤的回覆通報資料，當初上傳通報的那一筆
						Criteria criteria = liaRocUpload1072020DetailDao.getSession().createCriteria(LiaRocUpload107Detail2020.class);
						criteria.add(Restrictions.eq("liaRocType", responseDetailVO.getLiaRocType()));
						criteria.add(Restrictions.eq("liaRocCompanyCode", responseDetailVO.getLiaRocCompanyCode()));
						criteria.add(this.getEqualOrNullCriterion("name", responseDetailVO.getName()));
						criteria.add(this.getEqualOrNullCriterion("certiCode", responseDetailVO.getCertiCode()));
						criteria.add(this.getEqualOrNullCriterion("birthday", responseDetailVO.getBirthday()));
						criteria.add(Restrictions.eq("liaRocPolicyCode", responseDetailVO.getPolicyId()));
					
						criteria.add(Restrictions.sqlRestriction("this_.upload_List_Id in (Select list_Id from t_liaroc_upload_2020 where request_trans_uuid = '"
										+ rqHeader.getPrevRqUID() + "' ) "));
						List<LiaRocUpload107Detail2020> existDetail = criteria.list();

						if (existDetail != null && existDetail.size() > 0) {

							LiaRocUpload107Detail2020 detailBO = existDetail.get(0);

							LiaRocUploadError uploadError = new LiaRocUploadError();
							BeanUtils.copyProperties(uploadError, detailBO);
							uploadError.setErrorMsg(responseDetailVO.getMessage());
							uploadError.setLiaRocVersion(version);
							uploadError.setUploadDetail107ListId(detailBO.getListId());
							uploadError.setPolicyId(responseDetailVO.getPolicyId());
							liaRocUploadErrorDao.save(uploadError);

							// 記錄有保項失敗的 upload_list_id
							if (!errorUploadIDs.contains(detailBO.getUploadListId())) {
								errorUploadIDs.add(detailBO.getUploadListId());
							}

						} else {
							saveUploadError(responseDetailVO, version);
							logger.info("[LiaRocUpload Result]無法對應 PolicyID=" + responseDetailVO.getPolicyId()
											+ ",errorMsg=" + responseDetailVO.getMessage());
						}
					}
				}
			}

			// 修正該批次上傳公會通報資料的通報狀態
			for (Long listId : allRequestUploadIds) {

				LiaRocUpload2020 uploadBO = liaRocUpload2020Dao.load(listId);

				if (errorUploadIDs.contains(uploadBO.getListId())) {
					uploadBO.setLiaRocUploadStatus(LiaRocCst.LIAROC_UL_STATUS_RETURN_ERROR);
				} else {
					uploadBO.setLiaRocUploadStatus(LiaRocCst.LIAROC_UL_STATUS_RETURN);
				}

				uploadBO.setResponseTransUUid(rqHeader.getRqUID());
				uploadBO.setResponseTime(responseTime);
				uploadBO.setLiaRocReceiveDate(liaRocReceiveDate);
				liaRocUpload2020Dao.save(uploadBO);

			}

			trans.commit();

			if (responseVO.getHeader().getFail() > 0) {
				//公會上傳偵錯報表 發送email與歸檔
				liaRocRptCI.archiveAndSendRpt0080(rqHeader.getPrevRqUID());
			}

			logger.info(" 上傳公會通報資料結果回傳寫檔成功 RequestUUID =" + rqHeader.getPrevRqUID());
		} catch (Exception e) {
			TransUtils.rollback(trans);
			logger.info(" 上傳公會通報資料結果回傳寫檔錯誤" + e);

			throw ExceptionFactory.parse(e);
		}

	}

	public void save902Response(String rqUid, Liaroc20UploadRs902VO rs092VO) {

		if (rs092VO == null)
			return;

		java.util.Date responseTime = new Date();
		java.util.Date liaRocReceiveDate = new Date();
		

		// 以 UUID 查詢出該次上傳公會通報的所有資料 ListID
		List<Long> allRequestUploadIds = liaRocUpload2020Dao.queryListIDsByRequest092UUID(rqUid);
		if (allRequestUploadIds == null || allRequestUploadIds.size() == 0) {
			return;
		}

		List<Long> errorUploadIDs = new ArrayList<Long>();
		TaiwanDateAdapter twDateParser = new TaiwanDateAdapter();
		try {

			// 有失敗的錯誤, 將失敗的通報資料寫入 LiaRocUploadError902
			for (Liaroc20UploadRs902DataVO responseDetailVO : rs092VO.getFailResDatas()) {

				String liaRocRqUID = responseDetailVO.getDataserno();
				// 承保收件結果
				// 查出此筆錯誤的回覆通報資料，當初上傳通報的那一筆
				Criteria criteria = liaRocUploadDetail2020Dao.getSession()
						.createCriteria(LiaRocUploadDetail2020.class);
				criteria.add(Restrictions.eq("liaRocCompanyCode", responseDetailVO.getCmpno()));
				criteria.add(this.getEqualOrNullCriterion("certiCode", responseDetailVO.getIdno()));
				criteria.add(this.getEqualOrNullCriterion("birthday", twDateParser.unmarshal(responseDetailVO.getBirdate())));
				criteria.add(this.getEqualOrNullCriterion("liaRocPolicyCode", responseDetailVO.getInsno()));
				criteria.add(this.getEqualOrNullCriterion("liaRocPolicyType", responseDetailVO.getInsclass()));
				criteria.add(this.getEqualOrNullCriterion("liaRocProductCategory", responseDetailVO.getInskind()));
				criteria.add(this.getEqualOrNullCriterion("liaRocProductType", responseDetailVO.getInsitem()));

				criteria.add(Restrictions.sqlRestriction(
						"this_.upload_List_Id in (Select list_Id from t_liaroc_upload_2020 where request_trans_uuid_902 = '"
								+ rqUid + "' ) "));

				List<LiaRocUploadDetail2020> existDetail = criteria.list();

				LiaRocUploadError902 uploadError902 = new LiaRocUploadError902();
				uploadError902.setLiaRocType(responseDetailVO.getCmptype());
				uploadError902.setLiaRocCompanyCode(responseDetailVO.getCmpno());	
				uploadError902.setCertiCode(responseDetailVO.getIdno());
				uploadError902.setBirthday(twDateParser.unmarshal(responseDetailVO.getBirdate()));
				uploadError902.setLiaRocPolicyCode(responseDetailVO.getInsno());
				uploadError902.setLiaRocPolicyType(responseDetailVO.getInsclass());
				uploadError902.setLiaRocProductCategory(responseDetailVO.getInskind());
				uploadError902.setLiaRocProductType(responseDetailVO.getInsitem());

				String errorMsg = responseDetailVO.getMsg();
				if (errorMsg != null && errorMsg.length() > 170) {
					errorMsg = errorMsg.substring(0, 170);
				}
				uploadError902.setErrorMsg(errorMsg);

				uploadError902.setDataSerialNum(liaRocRqUID);

				if (existDetail != null && existDetail.size() > 0) {
					LiaRocUploadDetail2020 detailBO = existDetail.get(0);
					uploadError902.setUploadDetailListId(detailBO.getListId());
					// 記錄有保項失敗的 upload_list_id
					if (!errorUploadIDs.contains(detailBO.getUploadListId())) {
						errorUploadIDs.add(detailBO.getUploadListId());
					}
				}
				liaRocUploadError902Dao.save(uploadError902);
			}
		
			// 修正該批次上傳公會通報資料的通報狀態
			for (Long listId : allRequestUploadIds) {
				LiaRocUpload2020 uploadBO = liaRocUpload2020Dao.load(listId);
				if (errorUploadIDs.contains(uploadBO.getListId())) {
					uploadBO.setLiaRocUploadStatus902(LiaRocCst.LIAROC_UL_STATUS_RETURN_ERROR);
				} else {
					uploadBO.setLiaRocUploadStatus902(LiaRocCst.LIAROC_UL_STATUS_RETURN);
				}
				uploadBO.setResponseTransUUid902(rqUid); // 902 為即時api回覆無response uuid,用rqUid回寫
				uploadBO.setResponseTime902(responseTime);
				uploadBO.setLiaRocReceiveDate902(liaRocReceiveDate);
				liaRocUpload2020Dao.save(uploadBO);
			}
			NBUtils.appLogger(" 上傳公會902通報資料結果回傳寫檔成功 RequestUUID =" + rqUid);
		} catch (Exception e) {
			NBUtils.appLogger(" 上傳公會902通報資料結果回傳寫檔錯誤" + e);
			throw ExceptionFactory.parse(e);
		}
	}
	
	private void execute902(RqHeader rqHeader, LiaRocUploadProcessResultRootVO vo) {

		if (rqHeader == null || vo == null)
			return;

		LiaRocUploadProcessResultRootVO responseVO = vo;
		LiaRocHeaderVO responseHeaderVO = responseVO.getHeader();
		java.util.Date responseTime = null;
		java.util.Date liaRocReceiveDate = null;
		String liaRocRqUID = "";
		if(responseHeaderVO != null) {
			try {
				if( StringUtils.isNotEmpty(responseHeaderVO.getResponseTime())) {
					responseTime = org.apache.commons.lang3.time.DateUtils.parseDate(responseHeaderVO.getResponseTime(), "yyyyMMddHHmmssSSS");
				}
				if( StringUtils.isNotEmpty(responseHeaderVO.getLiaRocReceiveDate())) {
					liaRocReceiveDate = org.apache.commons.lang3.time.DateUtils.parseDate(responseHeaderVO.getLiaRocReceiveDate(), "yyyyMMddHHmmssSSS");
				}
				liaRocRqUID = responseHeaderVO.getLiaRocRqUID();
			} catch (ParseException e) {
				logger.error(String.format( "LiaRocHeaderVO.responseTime := %s, execute := %s" , responseHeaderVO.getResponseTime(), ExceptionUtils.getFullStackTrace(e)));
			}
		}
	
		// 以 UUID 查詢出該次上傳公會通報的所有資料 ListID
		List<Long> allRequestUploadIds = liaRocUpload2020Dao.queryListIDsByRequestUUID(rqHeader.getPrevRqUID());
		if (allRequestUploadIds == null || allRequestUploadIds.size() == 0) {
			return;
		}

		List<Long> errorUploadIDs = new ArrayList<Long>();

		UserTransaction trans = null;
		try {
			trans = Trans.getUserTransaction();
			trans.begin();

			// 有失敗的錯誤, 將失敗的通報資料寫入 LiaRocUploadError
			if (responseVO.getHeader().getFail() > 0) {

				for (LiaRocDetailInfoWSVO responseDetailVO : responseVO.getBody().getList()) {
					
					//承保收件結果
					// 查出此筆錯誤的回覆通報資料，當初上傳通報的那一筆
					Criteria criteria = liaRocUploadDetail2020Dao.getSession().createCriteria(LiaRocUploadDetail2020.class);
					criteria.add(Restrictions.eq("liaRocType", responseDetailVO.getLiaRocType()));
					criteria.add(Restrictions.eq("liaRocCompanyCode", responseDetailVO.getLiaRocCompanyCode()));
					criteria.add(this.getEqualOrNullCriterion("certiCode", responseDetailVO.getCertiCode()));
					criteria.add(this.getEqualOrNullCriterion("birthday", responseDetailVO.getBirthday()));
					criteria.add(this.getEqualOrNullCriterion("liaRocPolicyCode", responseDetailVO.getPolicyId()));
					criteria.add(this.getEqualOrNullCriterion("liaRocPolicyType", responseDetailVO.getLiaRocPolicyType()));
					criteria.add(this.getEqualOrNullCriterion("liaRocProductCategory", responseDetailVO.getLiaRocProductCategory()));
					criteria.add(this.getEqualOrNullCriterion("liaRocProductType", responseDetailVO.getLiaRocProductType()));
					
					criteria.add(Restrictions.eq("checkStatus", LiaRocCst.CHECK_STATUS_UPLOAD));
					
					criteria.add(Restrictions.sqlRestriction("this_.upload_List_Id in (Select list_Id from t_liaroc_upload_2020 where request_trans_uuid = '"
									+ rqHeader.getPrevRqUID() + "' ) "));

					List<LiaRocUploadDetail2020> existDetail = criteria.list();

					LiaRocUploadError902 uploadError = new LiaRocUploadError902();
					uploadError.setLiaRocType(responseDetailVO.getLiaRocType());
					uploadError.setLiaRocCompanyCode(responseDetailVO.getLiaRocCompanyCode());
					uploadError.setCertiCode(responseDetailVO.getCertiCode());
					uploadError.setBirthday(responseDetailVO.getBirthday());
					uploadError.setLiaRocPolicyCode(responseDetailVO.getPolicyId());
					uploadError.setLiaRocPolicyType(responseDetailVO.getLiaRocPolicyType());
					uploadError.setLiaRocProductCategory(responseDetailVO.getLiaRocProductCategory());
					uploadError.setLiaRocProductType(responseDetailVO.getLiaRocProductType());
					
					String errorMsg = responseDetailVO.getMessage();
					if (errorMsg != null && errorMsg.length() > 170) {
						errorMsg = errorMsg.substring(0, 170);
					}
					uploadError.setErrorMsg(errorMsg);
				
					uploadError.setDataSerialNum(liaRocRqUID);
		
					if (existDetail != null && existDetail.size() > 0) {
						LiaRocUploadDetail2020 detailBO = existDetail.get(0);
						uploadError.setUploadDetailListId(detailBO.getListId());
						// 記錄有保項失敗的 upload_list_id
						if (!errorUploadIDs.contains(detailBO.getUploadListId())) {
							errorUploadIDs.add(detailBO.getUploadListId());
						}
					}
					
					liaRocUploadError902Dao.save(uploadError);
				}
			}

			// 修正該批次上傳公會通報資料的通報狀態
			for (Long listId : allRequestUploadIds) {
				LiaRocUpload2020 uploadBO = liaRocUpload2020Dao.load(listId);
				if (errorUploadIDs.contains(uploadBO.getListId())) {
					uploadBO.setLiaRocUploadStatus902(LiaRocCst.LIAROC_UL_STATUS_RETURN_ERROR);
				} else {
					uploadBO.setLiaRocUploadStatus902(LiaRocCst.LIAROC_UL_STATUS_RETURN);
				}
				uploadBO.setResponseTransUUid902(rqHeader.getRqUID());
				uploadBO.setResponseTime902(responseTime);
				uploadBO.setLiaRocReceiveDate902(liaRocReceiveDate);
				liaRocUpload2020Dao.save(uploadBO);
			}

			trans.commit();

			if (responseVO.getHeader().getFail() > 0) {
				//公會上傳偵錯報表 發送email與歸檔
				//liaRocRptCI.archiveAndSendRpt0080(rqHeader.getPrevRqUID());
			}
			
			logger.info(" 上傳公會902通報資料結果回傳寫檔成功 RequestUUID =" + rqHeader.getPrevRqUID());
		} catch (Exception e) {
			TransUtils.rollback(trans);
			logger.info(" 上傳公會902通報資料結果回傳寫檔錯誤" + e);
			throw ExceptionFactory.parse(e);
		}
	}
	@Override
	public String getLiaRocTargetCodeValue(String sourceCodeTable, String sourceCodeVal, String targetCate,
					String targetCodeTable) {
		return liaRocUploadDao.getLiarocTargetCodeValue(sourceCodeTable, sourceCodeVal, targetCate, targetCodeTable);
	}

	@Override
	public String getLiaRocTargetCodeValue(String sourceCodeTable, String sourceCodeVal, String targetCate) {
		return liaRocUploadDao.getLiarocTargetCodeValue(sourceCodeTable, sourceCodeVal, targetCate,
						"T_LIAROC_CODE_TYPE");
	}

	@Override
	public UwNotintoRecvVO saveAfinsNotInfoRecv(String line) {
		if (line == null) {
			return null;
		}

		UwNotintoRecvVO vo = new UwNotintoRecvVO();
		String[] item = line.split("\\|");
		int len = item.length;
		try {
			vo.setSeqNo(new Long(item[0]));
		} catch (NumberFormatException ex) {
			throw ExceptionUtil.parse(ex);
		}
		vo.setSource(item[1]);
		vo.setDataType(item[2]);
		vo.setCompanyNo(item[3]);
		vo.setUploadPolicyNo(item[4]);
		String policyCode = item[5];
		if (StringUtils.isNotBlank(policyCode)) {
			vo.setPolicyCode(policyCode);
			//vo.setPolicyId(policyService.getPolicyIdByPolicyCode(policyCode));
		}
		String applyDateString = item[6];
		if (org.apache.commons.lang.StringUtils.isNotEmpty(applyDateString)) {
			if (applyDateString.length() == 6) {
				applyDateString = "0" + applyDateString;//補足7碼
			}
			String twy = String.valueOf(Integer.valueOf(applyDateString.substring(0, 3)) + 1911);
			String month = applyDateString.substring(3, 5);
			String day = applyDateString.substring(5, 7);
			vo.setApplyDate(DateUtils.toDate(twy + month + day, "yyyyMMdd"));
		}
		vo.setCoverage(item[7]);
		vo.setName(item[8]);
		vo.setCeritCode(item[9]);

		if (item[10].length() == 6) {
			vo.setInsurantBDay("0" + item[10]);//補足7碼
		} else {
			vo.setInsurantBDay(item[10]);
		}

		vo.setInternalId(item[11]);
		vo.setAmount(item[12]);
		vo.setPaidModx(len > 13 ? item[13] : "");
		vo.setCurrency(len > 14 ? item[14] : "");
		vo.setReceiveDate(len > 15 ? item[15] : "");
		vo.setInputName(len > 16 ? item[16] : "");
		vo.setSerialNum(len > 17 ? item[17] : "");
		//243784 add by Kathy 2018/07/20
		vo.setRegisterCode(len > 18 ? item[18] : ""); //登錄證字號
		vo.setPhCertiCode(len > 19 ? item[19] : ""); //要保人ID
		vo.setPrem(len > 20 ? item[20] : ""); //[PCR_391319][DEV_405901][2020新式公會通報]繳別保費
		vo.setLiarocPolicyStatus(len > 21 ? item[21] : ""); //[PCR_492881][DEV_513262]開發AFINS 取消收件通報
		List<UwNotintoRecvVO> recList = uwNotintoRecvService.find(Restrictions.eq("seqNo", vo.getSeqNo()));
		if (recList == null || recList.size() == 0) {
			uwNotintoRecvService.save(vo);

			return vo;
		} else {
			
			boolean save = false;
			
			// 理論上要先01才06，因此06才會走進此情境
			if(LiarocPolicyStatus._06_FREELOOK.getCode().equals(vo.getLiarocPolicyStatus())) {
				save = true;
				// 查出的舊vo，若有重複就代表已經上傳過06，不重複上傳
				for (UwNotintoRecvVO uwNotintoRecvVO : recList) {
					if(LiarocPolicyStatus._06_FREELOOK.getCode().equals(uwNotintoRecvVO.getLiarocPolicyStatus())) {
						save = false;
					}
				}
			}

			if(save) {
				uwNotintoRecvService.save(vo);
				return vo;
			}
			
			NBUtils.appLogger("[INFO]比對重覆不作寫入=" + vo.getSeqNo());
		}

		return null;
	}

	/**
	 * <p>Description : 1.保費外幣保單換匯規則:  保單幣別不為台幣, 將保費依保單幣別以要保書申請日依實際匯率換匯後通報，若要保書申請日當日無匯率資料，以最近日但不超過要保書申請日的買價作換匯.</p>
	 * <p>Created By : Simon.Huang</p>
	 * <p>Create Time : 2018年2月20日</p>
	 * @param sourceCurrency
	 * @param transferDate
	 * @param prem
	 * @return
	 */
	public BigDecimal transferToTwCurrency(int sourceCurrency, Date transferDate, BigDecimal prem) {
		BigDecimal rate = null;
		if (CodeCst.MONEY__NTD != sourceCurrency) {
			try {
				/* @param method		2 抓現值，抓不到找過去最近的
				 * @param rateType		6:固定匯率
				 */
				rate = ExchangeRateServiceSP.findLatestExchangeRate(sourceCurrency,
								Integer.valueOf(CodeCst.MONEY__NTD), 2,
								CodeCst.EXCHANGE_RATE_TYPE_FIXED, DateUtils.truncateDay(transferDate))
								.getBuyRate();
				prem = prem.multiply(rate).setScale(0, BigDecimal.ROUND_HALF_UP);
			} catch (Exception ex) {
				Logger.getLogger(this.getClass().getName()).log(Level.SEVERE,
								"ExchangeRateServiceSP.findLatestExchangeRate(" + sourceCurrency + ",4,2,1," + transferDate + ")");
				Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, ",prem=" + prem + ", 匯率:" + rate + ",ex=" + ex.toString());
			}
		}
		return prem;
	}

	/**
	 * PCR-188901
	 * <br/>取得公會通報用保單號(或受理編號)
	 * <br/>新契約新件於頁面輸入受理編號並執行公會收件通報後,新契約承保及保全通報均用此受理編號執行通報
	 * @param policyId
	 * @return
	 */
	public String getLiaRocPolicyCodePrefix(Long policyId) {
		return liaRocUploadPolicyDao.getLiaRocPolicyCode(policyId);
	}

	private static String UNB_QUERY_LAST_NB_BATCH_DATE = "SELECT MAX(T.INSERT_TIMESTAMP) INSERT_TIMESTAMP "
					+ "FROM T_LIAROC_NB_BATCH T WHERE T.POLICY_ID = ? AND  T.CHANGE_TYPE = ? ";

	/**
	 * <p>Description : 查詢新契約異動通報最新異動日</p>
	 * <p>Created By : Simon.Huang</p>
	 * <p>Create Time : 2019年10月29日</p>
	 * @param policyId
	 * @param changeType
	 * @return
	 */
	@Override
	public Date getLastNBBatchDate(Long policyId, String changeType) {

		DBean db = null;
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Date lastBatchDate = null;
		try {

			db = new DBean();
			db.connect();
			conn = db.getConnection();
			pstmt = conn.prepareStatement(UNB_QUERY_LAST_NB_BATCH_DATE);
			pstmt.setLong(1, policyId);
			pstmt.setString(2, changeType);

			rs = pstmt.executeQuery();

			while (rs.next()) {
				lastBatchDate = rs.getDate("INSERT_TIMESTAMP");
			}
		} catch (Exception e) {
			throw ExceptionFactory.parse(e);
		} finally {
			DBean.closeAll(null, pstmt, db);
		}
		return lastBatchDate;
	}

	/**
	 * <p>Description : 寫入新契約通報異動檔作每日報表比對用, <br/>
	 *  check_status預設寫入Y,若有特別批次處理寫入N,需調整PKG_LS_PM_NB_LIAROC.P_NB_LIAROC_BATCH_DATA()</p>
	 * <p>Created By : Simon.Huang</p>
	 * <p>Create Time : 2019年10月30日</p>
	 * @param policyId
	 * @param changeType
	 * @return
	 */
	@Override
	public Long recordNBBatchData(Long policyId, String changeType) {

		Connection conn = null;
		CallableStatement stmt = null;
		DBean db = new DBean(true);
		try {

			db.connect();
			conn = db.getConnection();
			
			stmt = conn.prepareCall("{call PKG_LS_PM_NB_LIAROC.P_NB_LIAROC_BATCH_DATA(?, ?, ?)}");
			stmt.setLong(1, policyId);
			stmt.setString(2, changeType);
			stmt.registerOutParameter(3, Types.DECIMAL);

			stmt.execute();
			BigDecimal listId = stmt.getBigDecimal(3);

			return listId.longValue();
		} catch (Exception e) {
			throw ExceptionFactory.parse(e);
		} finally {
			DBean.closeAll(null, stmt, db);
		}
	}

	private Criterion getEqualOrNullCriterion(String propName, Object value) {

		if (value == null || StringUtils.isEmpty(value.toString())) {
			return Restrictions.isNull(propName);
		} else {
			return Restrictions.eq(propName, value);
		}
	}

	/**
	 * <p>Description : IR-299630-公會通報偵錯報表之偵錯原因應包含"保單號碼不可空白
	 * <br/> 無法對應出時仍需記錄偵錯資料
	 * </p>
	 * <p>Created By : Simon.Huang</p>
	 * <p>Create Time : 2019年4月19日</p>
	 * @param responseDetailVO
	 */
	private void saveUploadError(LiaRocDetailInfoWSVO responseDetailVO,String version) {
		LiaRocUploadError uploadError = new LiaRocUploadError();
		
		uploadError.setName(responseDetailVO.getName());
		uploadError.setCertiCode(responseDetailVO.getCertiCode());
		uploadError.setBirthday(responseDetailVO.getBirthday());
		uploadError.setPolicyId(responseDetailVO.getPolicyId());
		uploadError.setLiaRocPolicyType(responseDetailVO.getLiaRocPolicyType());
		uploadError.setLiaRocProductCategory(responseDetailVO.getLiaRocProductCategory());
		uploadError.setLiaRocProductType(responseDetailVO.getLiaRocProductType());
		String errorMsg = responseDetailVO.getMessage();
		if (errorMsg != null && errorMsg.length() > 170) {
			errorMsg = errorMsg.substring(0, 170);
		}
		uploadError.setErrorMsg(errorMsg);
		uploadError.setLiaRocVersion(version);
		liaRocUploadErrorDao.save(uploadError);
	}

	/**
	 * <p>Description : PCR-391319 取得是否開始做新式公會上傳通報
	 * </p>
	 * <p>Created By : Kathy.Yeh</p>
	 * <p>Create Time : 202011月16日</p>
	 * @return  isLiaRocV2020Process
	 */
	public boolean isLiaRocV2020Process() {
		// 新式公會通報生效日期 2021/07/01
		String liaRocV2020ProcDate = Para.getParaValue(LiaRocCst.PARAM_LIAROC_V2020_PROC_DATE);
		String currentDateStr = DateUtils.date2String(new Date(), "yyyyMMdd");
		boolean isLiaRocV2020Process = true;
		// 執行日小於新式公會通報生效日期(2021/07/01)，要執行舊式公會更正通報
		if (currentDateStr.compareTo(liaRocV2020ProcDate) < 0) {
			isLiaRocV2020Process = false;
		}	
		return isLiaRocV2020Process;
	}
	
}
