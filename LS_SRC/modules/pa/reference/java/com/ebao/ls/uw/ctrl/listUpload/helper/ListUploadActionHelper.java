package com.ebao.ls.uw.ctrl.listUpload.helper;

import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;

import com.ebao.foundation.commons.exceptions.GenericException;
import com.ebao.pub.framework.ExceptionFactory;
import com.ebao.pub.framework.GenericAction;
import com.ebao.pub.i18n.lang.StringResource;
import com.ibm.icu.math.BigDecimal;

public abstract class ListUploadActionHelper<V, F> extends GenericAction {

	@Override
	public abstract ActionForward process(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception;
	
	protected abstract int getExcelCellSize();

	protected abstract String getFileErrMsgLable();

	protected abstract String getProcessMsgLable();

	protected abstract V readRowData(Row row, Integer countNum, ActionForm actionForm);
	
	protected abstract SimpleDateFormat getDateFormat();
	
	protected abstract boolean errorCondition(V v);

	public List<V> readExcelFile(ActionForm actionForm, HttpServletRequest request,
			FormFile uploadFile) throws GenericException {

		InputStream fileInputStream = null;
		Workbook workbook = null;

		try {

			if (uploadFile != null
					&& uploadFile.getInputStream().available() == 0) {
				throw new NullPointerException(
						"Notes:Excel record does not exist!");
			}

			ArrayList<V> uploadVOs = new ArrayList<V>();
			fileInputStream = uploadFile.getInputStream();

			if (uploadFile.getFileName().toUpperCase().endsWith("XLS")) {
				workbook = new HSSFWorkbook(fileInputStream);
			} else {
				workbook = new XSSFWorkbook(fileInputStream);
			}

			Sheet sheet = workbook.getSheetAt(0);

			// @ check 檔案。
			if (sheet == null || sheet.getRow(0) == null) {
				// 無法讀取檔案內容，請檢查上傳檔案是否正確。
				request.setAttribute(getFileErrMsgLable(),
						StringResource.getStringData("MSG_1252789", request));
				return new ArrayList<V>();
			} else if (sheet.getRow(0).getPhysicalNumberOfCells() != getExcelCellSize()) {
				// 欄位數目不正確。
				request.setAttribute(getFileErrMsgLable(),
						StringResource.getStringData("MSG_1252788", request));
				return new ArrayList<V>();
			} else if (sheet.getPhysicalNumberOfRows() <= 1) {
				// Excel 沒有資料,第一列是表頭。
				request.setAttribute(getFileErrMsgLable(),
						StringResource.getStringData("MSG_1251040", request));
				return new ArrayList<V>();
			}
			// end 以上 check 沒問題，開始讀檔。

			// @ 逐一將 Excel 檔案的資料讀取到 Map 中。
			for (int i = 1; i < sheet.getPhysicalNumberOfRows(); i++) {
				Row excelRow = sheet.getRow(i);
				V uploadVO = readRowData(excelRow, i, actionForm);
				uploadVOs.add(uploadVO);
			}
			// end.

			// 上傳成功
			if (!uploadVOs.isEmpty()) {
				request.setAttribute(getProcessMsgLable(), "已載入成功");
			}

			return uploadVOs;
			
		} catch (Exception e) {
			throw ExceptionFactory.parse(e);
		} finally {
			if (workbook != null) {
				try {
					workbook.close();
				} catch (IOException e) {
				}
			}
			if (fileInputStream != null) {
				try {
					fileInputStream.close();
				} catch (IOException e) {
				}
			}
		}
	}
	
	public List<V> getErrorList(List<V> voList) {
		if (voList == null) {
			return new ArrayList<V>();
		}
		List<V> errList = new ArrayList<V>();
		for(V vo : voList) {
			if (errorCondition(vo)) {
				errList.add(vo);
			}
		}
		return errList;
	}
	
	
	@SuppressWarnings({ "unchecked", "deprecation" })
	public <T> T getCellValue(Row row, Integer columnNum, Class<T> classType) throws Exception {
		if(columnNum < row.getPhysicalNumberOfCells()) {
			
			Cell cell = row.getCell(columnNum);
			
			if (classType.equals(Date.class)) {
				if (DateUtil.isCellDateFormatted(cell)) {
					return (T) cell.getDateCellValue();
				}
			}
			
			cell.setCellType(CellType.STRING);
			String columnValue = cell.getStringCellValue().trim();
			
			if (StringUtils.isEmpty(columnValue)) {
				return null;
			}
			
			if (classType.equals(String.class)) {
				return (T) columnValue;
			} else if (classType.equals(Integer.class)) {
				columnValue = columnValue.replace(",", "");
				return (T) Integer.valueOf(columnValue);
			} else if (classType.equals(Long.class)) {
				columnValue = columnValue.replace(",", "");
				return (T) Long.valueOf(columnValue);
			} else if (classType.equals(BigDecimal.class)) {
				columnValue = columnValue.replace(",", "");
				return (T) BigDecimal.valueOf(Double.valueOf(columnValue));
			} else if (classType.equals(Date.class)) {
				return (T) this.getDateFormat().parse(columnValue);
			}
			
		} else {
			return null;
		}
		return null;
	}
	
	protected abstract void getVoList(HttpServletRequest request, F f);
	
	protected abstract void cleanSession(HttpServletRequest request);
	
	protected abstract void setSession(HttpServletRequest request, F f);
	
	@SuppressWarnings("unchecked")
	protected List<V> getVoList(HttpServletRequest request, String voKey) {
		return (List<V>) request.getSession().getAttribute(voKey);
	}

}
