package com.ebao.ls.uw.ctrl.pub;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

import com.ebao.pub.framework.GenericForm;

/**
 * <p>Title: </p>  
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2004 </p>
 * <p>Company: eBaoTech Corporation </p>
 * <p>Create Time: Thu Sep 09 11:31:20 GMT+08:00 2004 </p> 
 * @author Walter Huang
 * @version 1.0
 *
 */

public class UwLifeInsuredForm extends GenericForm {

  private java.lang.Long underwriteId = null;
  private java.lang.Long listId = null;
  private java.lang.Long policyId = null;
  private java.lang.Long insuredId = null;
  private java.lang.String medicalExamIndi = "";
  private java.lang.Long uwListId = null;
  private java.lang.String standLife = "";

  /**
   * returns the value of the underwriteId
   *
   * @return the underwriteId
   */
  public java.lang.Long getUnderwriteId() {
    return underwriteId;
  }

  /**
   * sets the value of the underwriteId
   *
   * @param underwriteId the underwriteId
   */
  public void setUnderwriteId(java.lang.Long underwriteId) {
    this.underwriteId = underwriteId;
  }
  /**
   * returns the value of the listId
   *
   * @return the listId
   */
  public java.lang.Long getListId() {
    return listId;
  }

  /**
   * sets the value of the listId
   *
   * @param listId the listId
   */
  public void setListId(java.lang.Long listId) {
    this.listId = listId;
  }
  /**
   * returns the value of the policyId
   *
   * @return the policyId
   */
  public java.lang.Long getPolicyId() {
    return policyId;
  }

  /**
   * sets the value of the policyId
   *
   * @param policyId the policyId
   */
  public void setPolicyId(java.lang.Long policyId) {
    this.policyId = policyId;
  }
  /**
   * returns the value of the insuredId
   *
   * @return the insuredId
   */
  public java.lang.Long getInsuredId() {
    return insuredId;
  }

  /**
   * sets the value of the insuredId
   *
   * @param insuredId the insuredId
   */
  public void setInsuredId(java.lang.Long insuredId) {
    this.insuredId = insuredId;
  }

  /**
   * returns the value of the medicalExamIndi
   *
   * @return the medicalExamIndi
   */
  public java.lang.String getMedicalExamIndi() {
    return medicalExamIndi;
  }

  /**
   * sets the value of the medicalExamIndi
   *
   * @param medicalExamIndi the medicalExamIndi
   */
  public void setMedicalExamIndi(java.lang.String medicalExamIndi) {
    this.medicalExamIndi = medicalExamIndi;
  }
  /**
   * returns the value of the uwListId
   *
   * @return the uwListId
   */
  public java.lang.Long getUwListId() {
    return uwListId;
  }

  /**
   * sets the value of the uwListId
   *
   * @param uwListId the uwListId
   */
  public void setUwListId(java.lang.Long uwListId) {
    this.uwListId = uwListId;
  }
  /**
   * returns the value of the standLife
   *
   * @return the standLife
   */
  public java.lang.String getStandLife() {
    return standLife;
  }

  /**
   * sets the value of the standLife
   *
   * @param standLife the standLife
   */
  public void setStandLife(java.lang.String standLife) {
    this.standLife = standLife;
  }

  // Contract methods

  /**
   * validate the submitted values 
   */
  public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {

    ActionErrors errors = new ActionErrors();

    return errors;

  }
}