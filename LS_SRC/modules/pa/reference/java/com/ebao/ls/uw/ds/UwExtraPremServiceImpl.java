package com.ebao.ls.uw.ds;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import com.ebao.ls.foundation.service.GenericServiceImpl;
import com.ebao.ls.uw.data.UwExtraPremDao;
import com.ebao.ls.uw.data.bo.UwExtraLoading;
import com.ebao.ls.uw.vo.UwExtraLoadingVO;


public class UwExtraPremServiceImpl extends GenericServiceImpl<UwExtraLoadingVO, UwExtraLoading, UwExtraPremDao> implements UwExtraPremService {

	public List<UwExtraLoadingVO> findByUnderwriteIdInsuredIdMsgId(Long underwriteId, Long insuredId,Long msgId){
		
		List<UwExtraLoading> bos = uwExtraPremDao.findByUnderwriteIdInsuredIdMsgId(underwriteId, insuredId, msgId);
		if(bos == null){
			return null;
		}else{
			List<UwExtraLoadingVO> vos = new ArrayList<UwExtraLoadingVO>();
			for (UwExtraLoading bo : bos) {
				UwExtraLoadingVO vo = new UwExtraLoadingVO();
				bo.copyToVO(vo, false);
				vos.add(vo);
			}
			return vos;
		}
	}
	public Long findByUnderwriteIdsMsgId(Long[] underwriteIds, Long msgId){
		Long underwriteId = uwExtraPremDao.findByUnderwriteIdsMsgId(underwriteIds, msgId);
		return underwriteId;
	}
	@Override
	protected UwExtraLoadingVO newEntityVO() {
		return null;
	}

	@Resource(name = UwExtraPremDao.BEAN_DEFAULT)
	private UwExtraPremDao uwExtraPremDao;
}
