package com.ebao.ls.uw.ctrl.underwriting;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.ls.uw.ctrl.ActionUtil;
import com.ebao.pub.framework.ExceptionFactory;
import com.ebao.pub.framework.GenericAction;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: eBaoTech Corporation</p>
 * @author mingchun.shi
 * @version 1.0
 */
public class EnterLIARecordAction extends GenericAction {

  /**
   * show the bankrupt record
   * @param mapping
   * @param form
   * @param req
   * @param res
   * @return
   * @throws Exception
   */
  public ActionForward process(ActionMapping mapping, ActionForm form,
      HttpServletRequest req, HttpServletResponse res) throws Exception {
    try {
      String insuredId = req.getParameter("insuredId");
      String standLifeIndi = req.getParameter("standLifeIndi");
      String oldStandLifeIndi = req.getParameter("oldStandLifeIndi");
      String policyCode = req.getParameter("proposalCode");

      String actionCode = "";
      if ("Y".equals(standLifeIndi) && "N".equals(oldStandLifeIndi)) {
        actionCode = "X";
      }
      String path = "../pty/maintainLIAFromNB.do?partyId=" + insuredId
          + "&policyCode=" + policyCode + "&actionCode=" + actionCode;
      ActionForward actionForward = ActionUtil.getActionForward(path);
      return actionForward;
    } catch (Exception e) {
      throw ExceptionFactory.parse(e);
    }
  }

}
