package com.ebao.ls.ws.provider.endpoint;



//@Endpoint
public class GatewayEndpoint {

//    @Resource(name = WSEntry.BEAN_DEFAULT)
//    private WSEntry serv;

//    private com.ebao.ls.ws.provider.schema.ObjectFactory objectFactory;
//
//    private final DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory
//                    .newInstance();

    public GatewayEndpoint() {
//        objectFactory = new com.ebao.ls.ws.provider.schema.ObjectFactory();
    }

    private void syso(Object msg) {
        // System.out.println("[SYSOUT][GatewayEndpoint]" + msg);
    }

//    @PayloadRoot(namespace = WSServiceProvider.NAME_SPACE, localPart =
//                    WSServiceProvider.INVOKE)
//    @ResponsePayload
//    public Element invoke(
//                    @RequestPayload Element reqElement)
//                    throws ParserConfigurationException {
//        syso(String.format("Invoke->Request IP: %s",
//                        getLoginIPAddress()));
//        Log.info(getClass(), String.format("Invoke->Request IP: %s",
//                        getLoginIPAddress()));
//        syso("ebao.pub.util.Log mechanism is on ");
//
//        /* 請求訊息轉換 */
//        WSInternalReq msg = getNoneFileRequest(reqElement);
//
//        /* 後端處理訊息服務*/
//        String content = serv.invoke(msg).getMessage();
//        Log.info(getClass(),
//                        String.format("Invoke->Response Message = ", content));
//
//        /* 回應訊息封裝 */
//        DocumentBuilder documentBuilder = documentBuilderFactory
//                        .newDocumentBuilder();
//        Document document = documentBuilder.newDocument();
//        Element responseElement = document.createElementNS(
//                        WSServiceProvider.NAME_SPACE,
//                        WSServiceProvider.RESPONSE);
//        Element contentElement = document.createElementNS(
//                        WSServiceProvider.NAME_SPACE, "content");
//
//        contentElement.setTextContent(content);
//        responseElement.appendChild(contentElement);
//        syso("invoke->return Message: " + content);
//        return responseElement;
//    }
//
//    @PayloadRoot(namespace = WSServiceProvider.NAME_SPACE, localPart = WSMtomProcessor.STORE)
//    @ResponsePayload
//    public Element send(
//                    @RequestPayload Element reqElement)
//                    throws IOException, ParserConfigurationException {
//        /* 請求訊息轉換 */
//        Log.info(getClass(), String.format("send->Request IP: %s",
//                        getLoginIPAddress()));
//        syso(String.format("send->Request IP: %s",
//                        getLoginIPAddress()));
//        WSInternalMtomReq msg = getFileRequest(reqElement);
//
//        /* 後端處理訊息服務*/
//        WSInternalResp result = serv.send(msg);
//        Log.debug(getClass(),
//                        String.format("send->Request Data: %s",
//                                        result.getMessage()));
//        syso(String.format("send->Request Data: %s",
//                        result.getMessage()));
//
//        /* 回應訊息封裝 */
//        DocumentBuilder documentBuilder = documentBuilderFactory
//                        .newDocumentBuilder();
//        Document document = documentBuilder.newDocument();
//        Element responseElement = document.createElementNS(
//                        WSServiceProvider.NAME_SPACE,
//                        WSMtomProcessor.NONE_FILE_RESPONSE);
//        Element contentElement = document.createElementNS(
//                        WSServiceProvider.NAME_SPACE, "content");
//        contentElement.setTextContent(result.getMessage());
//        responseElement.appendChild(contentElement);
//        syso("send->Response Data: " + result.getMessage());
//        return responseElement;
//    }
//
//    @PayloadRoot(namespace = WSServiceProvider.NAME_SPACE, localPart = WSMtomProcessor.LOAD)
//    @ResponsePayload
//    public Element acquire(
//                    @RequestPayload Element reqElement)
//                    throws IOException, SOAPException,
//                    ParserConfigurationException {
//        syso(String.format("acquire->Request IP: %s",
//                        getLoginIPAddress()));
//        /* 請求訊息轉換 */
//        WSInternalReq msg = getNoneFileRequest(reqElement);
//
//        /* 後端處理訊息服務*/
//        WSInternalMtomResp result = serv.acquire(msg);
//
//        /* 回應訊息封裝 + 帶多檔傳輸 */
//        DocumentBuilder documentBuilder = documentBuilderFactory
//                        .newDocumentBuilder();
//        Document document = documentBuilder.newDocument();
//        Element responseElement = document.createElementNS(
//                        WSServiceProvider.NAME_SPACE,
//                        WSMtomProcessor.FILE_RESPONSE);
//        Element contentElement = document.createElementNS(
//                        WSServiceProvider.NAME_SPACE, "content");
//        contentElement.setTextContent(result.getMessage());
//        responseElement.appendChild(contentElement);
//
//        SOAPFactory omFactory = OMAbstractFactory.getSOAP11Factory();
//
//        // 夾帶多附件檔
//        if (result.getFileHandlers() != null
//                        && result.getFileHandlers().size() > 0)
//            for (FileHandler fileItem : result.getFileHandlers()) {
//                Element attachElement = document.createElementNS(
//                                WSServiceProvider.NAME_SPACE, "attachFile");
//                Element nameElement = document.createElementNS(
//                                WSServiceProvider.NAME_SPACE, "name");
//                nameElement.setTextContent(fileItem.getFilename());
//                attachElement.appendChild(nameElement); // add to attachment
//                DataHandler dataHandler = new DataHandler(
//                                new ByteArrayDataSource(
//                                                fileItem.getIs(),
//                                                WSConstants.MTOM_MIME));
//                OMText textData = omFactory.createOMText(dataHandler,
//                                Boolean.TRUE);
//                Element fileElement = document.createElementNS(
//                                WSServiceProvider.NAME_SPACE, "file");
//                Text newText = document.createTextNode(textData.getText());
//                fileElement.appendChild(newText);
//                attachElement.appendChild(fileElement); // add to attachment
//                responseElement.appendChild(attachElement); // add to response
//                fileItem.getIs().close();
//            }
//        syso("acquire->Response Data: " + result.getMessage());
//        return responseElement;
//    }
//
//    @PayloadRoot(localPart = WSMtomProcessor.EXCHANGE, namespace = WSMtomProcessor.NAME_SPACE)
//    @ResponsePayload
//    public Element exchange(
//                    @RequestPayload Element reqElement)
//                    throws IOException, SOAPException,
//                    ParserConfigurationException {
//        syso(String.format("exchange->Request IP: %s",
//                        getLoginIPAddress()));
//        /* 請求訊息轉換 */
//        WSInternalMtomReq msg = getFileRequest(reqElement);
//
//        /* 後端處理訊息服務*/
//        WSInternalMtomResp result = serv.exchange(msg);
//
//        /* 回應訊息封裝 + 帶多檔傳輸 */
//        DocumentBuilder documentBuilder = documentBuilderFactory
//                        .newDocumentBuilder();
//        Document document = documentBuilder.newDocument();
//        Element responseElement = document.createElementNS(
//                        WSServiceProvider.NAME_SPACE,
//                        WSMtomProcessor.FILE_RESPONSE);
//        Element contentElement = document.createElementNS(
//                        WSServiceProvider.NAME_SPACE, "content");
//        contentElement.setTextContent(result.getMessage());
//
//        responseElement.appendChild(contentElement);
//
//        SOAPFactory omFactory = OMAbstractFactory.getSOAP11Factory();
//
//        // 夾帶多附件檔
//        if (result.getFileHandlers() != null
//                        && result.getFileHandlers().size() > 0)
//            for (FileHandler fileItem : result.getFileHandlers()) {
//                Element attachElement = document.createElementNS(
//                                WSServiceProvider.NAME_SPACE, "attachFile");
//                Element nameElement = document.createElementNS(
//                                WSServiceProvider.NAME_SPACE, "name");
//                nameElement.setTextContent(fileItem.getFilename());
//                attachElement.appendChild(nameElement); // add to attachment
//
//                DataHandler dataHandler = new DataHandler(
//                                new ByteArrayDataSource(
//                                                fileItem.getIs(),
//                                                WSConstants.MTOM_MIME));
//                OMText textData = omFactory.createOMText(dataHandler,
//                                Boolean.TRUE);
//                Element fileElement = document.createElementNS(
//                                WSServiceProvider.NAME_SPACE, "file");
//                Text newText = document.createTextNode(textData.getText());
//                fileElement.appendChild(newText);
//                attachElement.appendChild(fileElement); // add to attachment
//                responseElement.appendChild(attachElement); // add to response
//                fileItem.getIs().close();
//            }
//
//        syso("exchange->Response Data: " + result.getMessage());
//        return responseElement;
//    }
//
//    public void setWSEntry(WSEntry wsentry) {
//        this.serv = wsentry;
//    }
//
//    /*　登入系統 IP Address */
//    public String getLoginIPAddress() {
//        TransportContext context = TransportContextHolder.getTransportContext();
//        HttpServletConnection connection = (HttpServletConnection) context
//                        .getConnection();
//        HttpServletRequest request = connection.getHttpServletRequest();
//        String ipAddress = request.getRemoteAddr();
//        return ipAddress;
//    }
//
//    private WSInternalReq getNoneFileRequest(Element reqElement) {
//        WSInternalReq request = new WSInternalReq();
//        int sysNodeIndex = 0, transNodeIndex = 1, contentNodeIndex = 2;
//        NodeList list = reqElement.getChildNodes();
//        for (int i = 0; i < list.getLength(); i++) {
//            syso(i + ", nodeName: " + list.item(i).getLocalName() + ", value: "
//                            + list.item(i).getTextContent());
//        }
//        org.w3c.dom.Node sysNode = list.item(sysNodeIndex);
//        org.springframework.util.Assert.isTrue(sysNode.getLocalName().equals(
//                        "systemId"));
//        request.setClientCode(sysNode.getTextContent());
//        syso("systemId: " + sysNode.getTextContent());
//
//        org.w3c.dom.Node transNode = list.item(transNodeIndex);
//        org.springframework.util.Assert.isTrue(transNode.getLocalName().equals(
//                        "serviceName"));
//        request.setTransType(transNode.getTextContent());
//        syso("serviceName: " + transNode.getTextContent());
//
//        org.w3c.dom.Node contentNode = list.item(contentNodeIndex);
//        org.springframework.util.Assert.isTrue(contentNode.getLocalName()
//                        .equals("content"));
//        request.setMessage(contentNode.getTextContent());
//        syso("content: " + contentNode.getTextContent());
//        return request;
//    }
//
//    private WSInternalMtomReq getFileRequest(Element reqElement)
//                    throws IOException {
//        WSInternalMtomReq request = new WSInternalMtomReq();
//        NodeList list = reqElement.getChildNodes();
//        int sysNodeIndex = 0, transNodeIndex = 1, contentNodeIndex = 2, filesNodeIndex = 3;
//        syso("[send] element len: " + list.getLength());
//        for (int k = 0; k < list.getLength(); k++) {
//            syso("[send] Local Name: " + list.item(k).getLocalName()
//                            + ", Text:" + list.item(k).getTextContent());
//        }
//        org.w3c.dom.Node sysNode = list.item(sysNodeIndex);
//        org.springframework.util.Assert.isTrue(sysNode.getLocalName().equals(
//                        "systemId"));
//        request.setClientCode(sysNode.getTextContent());
//
//        org.w3c.dom.Node transNode = list.item(transNodeIndex);
//        org.springframework.util.Assert.isTrue(transNode.getLocalName().equals(
//                        "serviceName"));
//        request.setTransType(transNode.getTextContent());
//
//        org.w3c.dom.Node contentNode = list.item(contentNodeIndex);
//        org.springframework.util.Assert.isTrue(contentNode.getLocalName()
//                        .equals("content"));
//        request.setMessage(contentNode.getTextContent());
//
//        /* 擷取內容檔案 */
//        java.util.List<FileHandler> fileList = request.getFileHandlers();
//        for (int k = filesNodeIndex; k < list.getLength(); k++) {
//            org.w3c.dom.Node filesNode = list.item(k);
//            org.springframework.util.Assert.isTrue(filesNode.getLocalName()
//                            .equals(
//                                            "attachFile"));
//            FileHandler file = new FileHandler();
//            NodeList attatchFileNode = filesNode.getChildNodes();
//            int nameIndex = 0, fileIndex = 1;
//            org.w3c.dom.Node nameNode = attatchFileNode.item(nameIndex);
//            org.springframework.util.Assert.isTrue(nameNode.getLocalName()
//                            .equals("name"));
//            file.setFilename(nameNode.getTextContent());
//
//            org.w3c.dom.Node fileNode = attatchFileNode.item(fileIndex);
//            org.springframework.util.Assert.isTrue(fileNode.getLocalName()
//                            .equals("file"));
//            DataSource source = new ByteArrayDataSource(Base64.decode(fileNode
//                            .getTextContent()),
//                            WSConstants.MTOM_MIME);
//            file.setIs(source.getInputStream());
//            fileList.add(file);
//        }
//        return request;
//    }
}
