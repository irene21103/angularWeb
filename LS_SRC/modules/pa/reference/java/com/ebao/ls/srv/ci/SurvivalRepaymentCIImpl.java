package com.ebao.ls.srv.ci;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.ebao.ls.pa.pub.ci.CoverageCI;
import com.ebao.ls.pa.pub.vo.CoverageVO;
import com.ebao.ls.srv.ds.SurvivalRepayService;
import com.ebao.ls.srv.ds.sp.SurvivalDSSp;
import com.ebao.ls.srv.ds.vo.PayPlanPayeeVO;
import com.ebao.ls.srv.ds.vo.PayPlanVO;
import com.ebao.ls.srv.vo.PayDueVO;
import com.ebao.pub.framework.GenericException;

/**
 * Title: SurvivalRepaymentCI provide ci for other COM <br>
 * Description: . <br>
 * Copyright: Copyright (c) 2002 <br>
 * Company: eBaoTech <br>
 * Create Time: 2005/04/12 <br>
 * 
 * @author simen.li
 * @version 1.0
 */
public class SurvivalRepaymentCIImpl implements SurvivalRepaymentCI {
  /**
   * Description:get Maturity amount <br>
   * 
   * @param itemId
   * @return BigDecimal
   * @throws GenericException Create time:2005/04/12 <br>
   */
  public BigDecimal getMaturityAmount(Long itemId) throws GenericException {
    return getSurvivalRepayDS().getMaturityAmountByItem(itemId);
  }

  /**
   * Description:get Maturity amount <br>
   * 
   * @param itemId
   * @return BigDecimal
   * @throws GenericException Create time:2005/04/12 <br>
   */
  public BigDecimal getNetMaturityAmountByItem(Long itemId)
      throws GenericException {
    return getSurvivalRepayDS().getNetMaturityAmountByItem(itemId);
  }

  /**
   * Description:for cs change commoncement date <br>
   * 
   * @param itemId
   * @param changeId
   * @param policyChgId
   * @param oprId
   * @throws GenericException Create time:2005/04/12 <br>
   */
  public void changeStartPayDate(Long itemId, Long changeId, Long policyChgId,
      Long oprId) throws GenericException {
    getSurvivalRepayDS().changeSurvivalStartPayDate(itemId, changeId,
        policyChgId, oprId);
  }

  /**
   * Description:get last pay date by spec item id
   * 
   * @param itemId
   * @param birthDay
   * @param curChargeDate
   * @return Date
   * @throws GenericException Create time: 2005/07/26 <br>
   */
  public Date getSurvivalLastPayDate(Long itemId) throws GenericException {
    return getSurvivalRepayDS().getSurvivalLastPayDate(itemId);
  }

  /**
   * Description:get the template type and the xml string of Maturity & Annuity
   * Remittance Advice
   * 
   * @param itemId
   * @param feetype
   * @param dueTime
   * @param chequeAmount
   * @return String [2]
   * @throws GenericException Create time: 2005/08/07 <br>
   */
  public String[] getSurvivalAdvice(Long itemId, Long feeType, Date dueTime,
      BigDecimal chequeAmount) throws GenericException {
    return getSurvivalRepayDS().getSurvivalAdvice(itemId, feeType, dueTime,
        chequeAmount);
  }

  /**
   * @param itemId
   * @param productId
   * @param liabId
   * @param sourceIndi
   * @return
   * @throws GenericException
   */
  public Date getLiabStartPayDate(Long itemId, Integer productId,
      Integer liabId, String sourceIndi) throws GenericException {
    return getSurvivalRepayDS().getLiabStartPayDate(itemId,
        Long.valueOf(productId), String.valueOf(liabId), sourceIndi);
  }
  
  /**
   * @param itemId
   * @param liabId
   * @return
   * @throws GenericException
   */
  @Override
  public Date getLiabEndPayDate(Long itemId, int liabId) throws GenericException {
    return getSurvivalRepayDS().getLiabEndPayDate(itemId, liabId);
  }

  /*
   * (non-Javadoc) @seecom.ebao.ls.srv.ci.SurvivalRepaymentCI#
   * getLastPayDueByPolicyIdAndItemIdAndLiabId(java.lang.Long, java.lang.Long,
   * java.lang.Long)
   */
  public PayDueVO getLastPayDueByPolicyIdAndItemIdAndLiabId(Long policyId,
      Long itemId, Long liabId) throws GenericException {
    return survivalRepayDS.getLastPayDueByPolicyIdAndItemIdAndLiabId(policyId,
        itemId, liabId);
  }

  /*
   * (non-Javadoc)
   * @see
   * com.ebao.ls.srv.ci.SurvivalRepaymentCI#getNetMaturityAmount(com.ebao.ls
   * .srv.ci.PayDueCIVO)
   */
  public BigDecimal getNetMaturityAmount(PayDueVO paydueCIVO) {
    return survivalRepayDS.getNetMaturityAmount(paydueCIVO);
  }

  /*
   * (non-Javadoc)
   * @see
   * com.ebao.ls.srv.ci.SurvivalRepaymentCI#getTotalAdjustAmount(java.lang.Long,
   * java.util.Date)
   */
  public BigDecimal getTotalAdjustAmount(Long itemId, Date dueDate)
      throws GenericException {
    return SurvivalDSSp.getTotalAdjustAmount(itemId, dueDate);
  }

  public String getPeriodType(Long itemId, Date eventDate)
      throws GenericException {
    return survivalRepayDS.getPeriodType(itemId, eventDate);
  }

  public BigDecimal getUnpaidAnnuityAmount(Long itemId) throws GenericException {
    return survivalRepayDS.getUnpaidAnnuityAmount(itemId);
  }

  public BigDecimal getUnpaidWithdrawAmount(Long itemId)
      throws GenericException {
    return survivalRepayDS.getUnpaidWithdrawAmount(itemId);
  }

  /*
   * (non-Javadoc)
   * @see
   * com.ebao.ls.srv.ci.SurvivalRepaymentCI#findByItemIdAndPayPlanType(java.
   * lang.Long, java.lang.String)
   */
  public PayPlanVO findByItemIdAndPayPlanType(Long itemId, String payPlanType)
      throws GenericException {
    return survivalRepayDS.findByItemIdAndPayPlanType(itemId, payPlanType);
  }

  /*
   * (non-Javadoc)
   * @see com.ebao.ls.srv.ci.SurvivalRepaymentCI#findByPlanId(java.lang.Long)
   */
  public PayPlanPayeeVO[] findByPlanId(Long planId) throws GenericException {
    return survivalRepayDS.findByPlanId(planId);
  }
  
  public BigDecimal getPaidMaturityAmount(Long policyId)
      throws GenericException {
    BigDecimal netMaturity = new BigDecimal(0);
    List<CoverageVO> voList = coverageCI.findByPolicyId(policyId, true);
    if(voList!=null) {
      for(CoverageVO vo : voList) {
        
        BigDecimal tempAmount = survivalRepayDS.getPaidMaturityAmount(policyId, vo.getItemId());
        
        netMaturity = netMaturity.add(tempAmount);
      }
    }
    
    return netMaturity;
  }
  
  
  
  @Override
public Map<String, String> getAnnuityPaidInfo(Map<String, String> params)
                throws GenericException {
    return survivalRepayDS.getAnnuityPaidInfo(params);
}

@Override
  public BigDecimal calcMaturityAmount4Rep(Long policyId)
                  throws GenericException {
      return survivalRepayDS.calcMaturityAmount4Rep(policyId);
  }
  
    /**
     * Description:for cs change benefit <br>
     * 
     * @param itemId
     * @param changeId
     * @param policyChgId
     * @param oprId
     * @throws GenericException Create time:2017/09/12 <br>
     */
    public void delPayPlan(Long itemId, Long changeId, Long policyChgId,
                    Long oprId) throws GenericException {
        getSurvivalRepayDS().delPayPlan(itemId, changeId,
                        policyChgId, oprId);
    }

  @Resource(name = SurvivalRepayService.BEAN_DEFAULT)
  private SurvivalRepayService survivalRepayDS;

  public SurvivalRepayService getSurvivalRepayDS() {
    return this.survivalRepayDS;
  }
  
  @Resource(name = CoverageCI.BEAN_DEFAULT)
  private CoverageCI coverageCI;
}
