package com.ebao.ls.liaRoc.ci;

import java.io.InputStream;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.activation.DataHandler;
import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;

import com.ebao.foundation.common.lang.exception.ExceptionUtils;
import com.ebao.foundation.commons.exceptions.ExceptionUtil;
import com.ebao.foundation.module.para.Para;
import com.ebao.foundation.module.util.i18n.CodeTable;
import com.ebao.ls.chl.service.AgentService;
import com.ebao.ls.chl.service.ChannelOrgService;
import com.ebao.ls.chl.vo.ChannelOrgVO;
import com.ebao.ls.cs.commonflow.data.bo.Application;
import com.ebao.ls.cs.logger.ApplicationLogger;
import com.ebao.ls.cs.logger.helper.AppLogDataHelper;
import com.ebao.ls.foundation.vo.GenericEntityVO;
import com.ebao.ls.liaRoc.LiaRocCst;
import com.ebao.ls.liaRoc.bo.LiaRocUpload;
import com.ebao.ls.liaRoc.bo.LiaRocUploadCoverage;
import com.ebao.ls.liaRoc.bo.LiaRocUploadDetail;
import com.ebao.ls.liaRoc.bo.LiaRocUploadDetailVO;
import com.ebao.ls.liaRoc.bo.LiaRocUploadVO;
import com.ebao.ls.liaRoc.data.LiaRocUploadCoverageDao;
import com.ebao.ls.liaRoc.data.LiaRocUploadDao;
import com.ebao.ls.liaRoc.data.LiaRocUploadDetailDao;
import com.ebao.ls.liaRoc.vo.LiaRocUploadDetailWrapperVO;
import com.ebao.ls.liaRoc.vo.LiaRocUploadSendVO;
import com.ebao.ls.pa.nb.ctrl.foa.FoaAcceptanceHelper;
import com.ebao.ls.pa.nb.util.NBUtils;
import com.ebao.ls.pa.pub.bs.coverage.CoverageService;
import com.ebao.ls.pa.pub.data.org.PolicyDao;
import com.ebao.ls.pa.pub.service.PolicyService;
import com.ebao.ls.pa.pub.vo.CoverageAgentVO;
import com.ebao.ls.pa.pub.vo.CoverageInsuredVO;
import com.ebao.ls.pa.pub.vo.CoveragePremium;
import com.ebao.ls.pa.pub.vo.CoverageVO;
import com.ebao.ls.pa.pub.vo.InsuredVO;
import com.ebao.ls.pa.pub.vo.PolicyHolderVO;
import com.ebao.ls.pa.pub.vo.PolicyVO;
import com.ebao.ls.pa.pub.vo.UwNotintoRecvVO;
import com.ebao.ls.prd.product.bs.productlife.ProductLifeDS;
import com.ebao.ls.prd.product.vo.ProductVO;
import com.ebao.ls.product.model.query.output.LifeProduct;
import com.ebao.ls.product.model.query.output.ProdBizCategorySub;
import com.ebao.ls.product.service.LifeProductCategoryService;
import com.ebao.ls.product.service.LifeProductService;
import com.ebao.ls.product.service.ProductVersionService;
import com.ebao.ls.pub.CodeCst;
import com.ebao.ls.pub.util.BizUtils;
import com.ebao.ls.pub.util.TGLDateUtil;
import com.ebao.ls.sc.service.foa.FoaAcceptanceCI;
import com.ebao.ls.sc.service.foa.vo.FoaPolicyVO;
import com.ebao.ls.sc.service.foa.vo.FoaPsAcceptanceVO;
import com.ebao.ls.sc.vo.AgentChlVO;
import com.ebao.ls.sc.vo.ScFoaAcceptanceVO;
import com.ebao.ls.sc.vo.ScFoaLiarocUploadVO;
import com.ebao.ls.sc.vo.ScFoaUnbProductVO;
import com.ebao.ls.uw.ds.UwNotintoRecvService;
import com.ebao.ls.ws.ci.esp.ESPWSServiceCI;
import com.ebao.ls.ws.vo.InputStreamDataSoure;
import com.ebao.ls.ws.vo.client.esp.FileObject;
import com.ebao.ls.ws.vo.client.esp.RqHeader;
import com.ebao.ls.ws.vo.client.esp.StandardResponse;
import com.ebao.ls.ws.vo.cust.liaRoc.LiaRocUploadProcessResultRootVO;
import com.ebao.ls.ws.vo.cust.liaRoc.LiaRocUploadRequestRoot;
import com.ebao.pub.fileresolver.writer.FixedCharLengthWriter;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.framework.ExceptionFactory;
import com.ebao.pub.framework.GenericException;
import com.ebao.pub.util.BeanUtils;
import com.ebao.pub.util.DBean;
import com.ebao.pub.util.DateUtils;
import com.hazelcast.util.StringUtil;

public class LiaRocUploadCIImpl implements LiaRocUploadCI, LiaRocUpload2019CI {

	@Resource(name = LiaRocFileCI.BEAN_DEFAULT)
	private LiaRocFileCI liaRocFileCI;

	@Resource(name = PolicyService.BEAN_DEFAULT)
	private PolicyService policyService;

	@Resource(name = ESPWSServiceCI.BEAN_DEFAULT)
	private ESPWSServiceCI espWSServiceCI;

	@Resource(name = LiaRocUploadDao.BEAN_DEFAULT)
	private LiaRocUploadDao liaRocUploadDao;

	@Resource(name = LiaRocUploadDetailDao.BEAN_DEFAULT)
	private LiaRocUploadDetailDao liaRocUploadDetailDao;

	@Resource(name = "liaRocUploadRequestWriter")
	private FixedCharLengthWriter fixedLengthWriter;

	@Resource(name = com.ebao.ls.pa.pub.bs.PolicyService.BEAN_DEFAULT)
	private com.ebao.ls.pa.pub.bs.PolicyService policyDS;

	@Resource(name = ProductLifeDS.BEAN_DEFAULT)
	private ProductLifeDS productLifeDS;

	@Resource(name = LifeProductService.BEAN_DEFAULT)
	private LifeProductService lifeProductService;

	@Resource(name = PolicyDao.BEAN_DEFAULT)
	private PolicyDao policyDao;

	@Resource(name = FoaAcceptanceHelper.BEAN_DEFAULT)
	private FoaAcceptanceHelper foaAcceptanceHelper;

	@Resource(name = FoaAcceptanceCI.BEAN_DEFAULT)
	private FoaAcceptanceCI foaAcceptanceCI;

	@Resource(name = CoverageService.BEAN_DEFAULT)
	private CoverageService coverageService;

	@Resource(name = ChannelOrgService.BEAN_DEFAULT)
	private ChannelOrgService channelOrgService;

	@Resource(name = ProductVersionService.BEAN_DEFAULT)
	private ProductVersionService productVersionService;

	@Resource(name = AgentService.BEAN_DEFAULT)
	private AgentService agentService;

	@Resource(name = LiaRocUploadCoverageDao.BEAN_DEFAULT)
	private LiaRocUploadCoverageDao liaRocUploadCoverageDao;

	@Resource(name = LiaRocUploadCommonService.BEAN_DEFAULT)
	private LiaRocUploadCommonService liaRocUploadCommonService;

	@Resource(name = LifeProductCategoryService.BEAN_DEFAULT)
	private LifeProductCategoryService lifeProductCategoryService;
	
	@Resource(name = UwNotintoRecvService.BEAN_DEFAULT)
	private UwNotintoRecvService uwNotintoRecvService;

	@Resource(name = com.ebao.ls.pa.pub.service.PolicyService.BEAN_DEFAULT)
	com.ebao.ls.pa.pub.service.PolicyService pubPolicyService;

	org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(this.getClass());
	
	@Override
	public void claimSaveUpload(Long policyId, Map<Long, String> itemIdMap,
					String liaRocUploadType, Map<Long, Date> itemEffectStatusDate) {
		@SuppressWarnings("deprecation")
		PolicyVO policyVO = policyService.loadPolicyByPolicyId(policyId);

		if (policyVO == null) {
			throw new IllegalArgumentException("Policy ID " + policyId + " does not exists in system.");
		}

		// make 主檔 vo
		LiaRocUploadSendVO uploadVO = new LiaRocUploadSendVO();
		uploadVO.setPolicyId(policyId);
		//change by sunny: 理賠用3
		uploadVO.setBizSource(LiaRocCst.LIAROC_UL_BIZ_SOURCE_CLM);
		uploadVO.setBizId(policyId);
		uploadVO.setBizCode(policyVO.getPolicyNumber());
		uploadVO.setLiaRocUploadType(liaRocUploadType);
		uploadVO.setUploadId(AppContext.getCurrentUser().getUserId());
		uploadVO.setParseBean(fixedLengthWriter);
		uploadVO.setLiaRocUploadStatus(LiaRocCst.LIAROC_UL_STATUS_WAITING);

		// get 明細資料 vo。
		// setup 通報明細資料
		List<LiaRocUploadDetailVO> details = new ArrayList<LiaRocUploadDetailVO>();
		for (CoverageVO coverage : policyVO.getCoverages()) {
			if (this.needUploadItem(coverage) && itemIdMap.get(coverage.getItemId()) != null) {
				LiaRocUploadDetailWrapperVO detailMapVO = this.generateUploadDetail(policyVO, coverage, liaRocUploadType);
				if (StringUtils.isNotEmpty(detailMapVO.getLiaRocProductType())) {
					detailMapVO.setLiaRocPolicyStatus(itemIdMap.get(coverage.getItemId()));
					detailMapVO.setInternalId(getInternalIdByProuctId(coverage.getProductId()));
					Date effectStatusDate = itemEffectStatusDate.get(coverage.getItemId());
					detailMapVO.setStatusEffectDate(effectStatusDate);
					details.add(detailMapVO);
				}
			}
		}
		uploadVO.setDataList(details);
		this.saveUpload(uploadVO);
	}

	@Override
	public LiaRocUploadSendVO getUploadVOByPolicyId(Long policyId, String liaRocUploadType, String bizSource) {

		PolicyVO policyVO = policyDS.retrieveById(policyId, false);

		if (policyVO == null) {
			throw new IllegalArgumentException("Policy ID " + policyId + " does not exists in system.");
		}
	
		LiaRocUploadSendVO uploadVO = createLiarocUploadMaster(policyVO, liaRocUploadType, bizSource);

		// get 明細資料 vo。
		List<GenericEntityVO> details = (List<GenericEntityVO>) this.getDetailList(policyVO, liaRocUploadType);
		uploadVO.setDataList(details);
		if (details.isEmpty()) {
			uploadVO.setLiaRocUploadStatus(LiaRocCst.LIAROC_UL_STATUS_SEND_NO_ITEM);
			uploadVO.setRequestTransUUid("NO_ITEM");
		}

		return uploadVO;

	}

	@Override
	public Long saveUpload(LiaRocUploadSendVO liaRocUploadVO) {

		List<? extends GenericEntityVO> dataList = liaRocUploadVO.getDataList();
		boolean isSaveDetail = true;
		if (dataList == null || dataList.size() == 0) {
			liaRocUploadVO.setLiaRocUploadStatus(LiaRocCst.LIAROC_UL_STATUS_SEND_NO_ITEM);
			liaRocUploadVO.setRequestTransUUid("NO_ITEM");
			isSaveDetail = false;
		} else {
			//PCR 188901 承保通報,依收件通報的保項通報號作承保通報
			if(LiaRocCst.LIAROC_UL_TYPE_INFORCE.equals(liaRocUploadVO.getLiaRocUploadType())) {
				for (GenericEntityVO genericEntityVO : dataList) {
					LiaRocUploadDetailVO detailVO = (LiaRocUploadDetailVO) genericEntityVO;
					String liarocPolicyCode = this.getLiaRocPolicyCode(liaRocUploadVO.getPolicyId(), detailVO.getItemId());
					detailVO.setPolicyId(liarocPolicyCode);
				}
			}
			//PCR 188901:新契約承保通報作業,於收件通報因會有AFINS以受理編號通報但新契約未輸入受理編號以保單號通報
			//承保通報需重新以新契約是否有輸入受理編號統一所有保項的通報編號,才不會有受理編號及保單號混合通報的情形
			if(LiaRocCst.LIAROC_UL_TYPE_INFORCE.equals(liaRocUploadVO.getLiaRocUploadType())
							&& LiaRocCst.LIAROC_UL_BIZ_SOURCE_UNB.equals(liaRocUploadVO.getBizSource())) {
				/* mark by Simon
				 * 2019/11/13 程式上線後，因核保中的保單承保通報與收件通報會不一致，UNB Joanne 決定不執行以下邏輯
				for (GenericEntityVO genericEntityVO : dataList) {
					LiaRocUploadDetailVO detailVO = (LiaRocUploadDetailVO) genericEntityVO;
					String bfLiarocPolicyCode = detailVO.getPolicyId();
					String afLiarocPolicyCode = this.syncNBInforceLiaRocPolicyCode(
									liaRocUploadVO.getPolicyId(), 
									detailVO.getItemId(),
									bfLiarocPolicyCode);
					detailVO.setPolicyId(afLiarocPolicyCode);
				}
				*/
			}
			//end PCR 188901
		}
		
		LiaRocUpload liaRocUploadBO = new LiaRocUpload();
		BeanUtils.copyProperties(liaRocUploadBO, liaRocUploadVO);

		// 儲存主檔資料
		liaRocUploadDao.save(liaRocUploadBO);

		liaRocUploadVO.setListId(liaRocUploadBO.getListId());

		if (isSaveDetail) {
			// 儲存明細資料
			this.saveUploadDetail(liaRocUploadVO, liaRocUploadVO.getDataList());
		}

		return liaRocUploadBO.getListId();

	}

	
	public void saveAcceptanceUploadPos(long aceptId)  throws Exception{
		FoaPsAcceptanceVO vo = foaAcceptanceCI.findFoaCsCaseByCaseId(aceptId);
		saveAcceptanceUploadPos(vo);
	}

	private static String FOA_UPLOAD_POS = "saveAcceptanceUploadPos";

	private void saveAcceptanceUploadPos(FoaPsAcceptanceVO vo)  throws Exception{

		ApplicationLogger.setSystemCode(AppLogDataHelper.SYSTEM_CODE_UNB);
		ApplicationLogger.setJobName(FOA_UPLOAD_POS);
		ApplicationLogger.setOptionId(DateUtils.date2String(new Date(), "yyyyMMdd HHmmss"));

		try {
			ScFoaAcceptanceVO acceptanceVO = vo.getAcceptanceVO();
			String policyCode = acceptanceVO.getPolicyCode();

			ApplicationLogger.setPolicyCode(policyCode);
			ApplicationLogger.addLoggerData("acceptanceId=" + acceptanceVO.getAceptId());

			PolicyVO fixPolicyVO = policyDS.retrieveByPolicyNumber(policyCode);
			Long channelOrgId = acceptanceVO.getSalesOrg(); 
			if(channelOrgId != null) {
				fixPolicyVO.setChannelOrgId(channelOrgId);
				fixPolicyVO.setChannelType(channelOrgService.load(channelOrgId).getChannelType());				
			}
			fixPolicyVO.setSubmissionDate(acceptanceVO.getAceptDate());
			
			//PCR-337606 判斷FOA 會有重複送件問題，所以要檢核，第二筆壓 D 2020/09/09 Add by Kathy
			String liarocUploadStatus = LiaRocCst.LIAROC_UL_STATUS_WAITING ;
			int count = countFoaLiarocUpload(vo.getAcceptanceVO().getAceptId(),  LiaRocCst.LIAROC_UL_BIZ_SOURCE_FOA_POS) ;
			if(count > 0) {
				liarocUploadStatus = LiaRocCst.LIAROC_UL_STATUS_SEND_DELETE ;
			}
			
			// 公會通報主檔
			LiaRocUploadSendVO info = new LiaRocUploadSendVO();
			info.setBizId(vo.getAcceptanceVO().getAceptId());
			/** 受理ID當key **/
			info.setPolicyId(fixPolicyVO.getPolicyId());
			info.setBizCode(policyCode);
			info.setBizSource(LiaRocCst.LIAROC_UL_BIZ_SOURCE_FOA_POS);
			info.setLiaRocUploadType(LiaRocCst.LIAROC_UL_TYPE_RECEIVE);
			info.setUploadId(AppContext.getCurrentUser().getUserId());
			info.setParseBean(fixedLengthWriter);
			info.setLiaRocUploadStatus(liarocUploadStatus);
			ScFoaAcceptanceVO acceptVO = vo.getAcceptanceVO();
			info.setChannelOrgId(acceptVO.getDeliverAgentChannelOrgId()); //送件業務員單位

			info.setReceiveDate(acceptVO.getAceptDate());
			Long agentId = acceptVO.getDeliverAgent();
			if (agentId != null) {
				AgentChlVO agentVO = agentService.findAgentByAgentId(agentId);
				if (agentVO != null) {
					info.setRegisterCode(agentVO.getRegisterCode());
				}
			}
			info.setInputId(AppContext.getCurrentUser().getUserId());

			/*
			LiaRocUpload liaRocUploadBO = new LiaRocUpload();
			BeanUtils.copyProperties(liaRocUploadBO, info);
			liaRocUploadDao.save(liaRocUploadBO);
			Long uploadListId = liaRocUploadBO.getListId();
			info.setListId(uploadListId);
			*/
			//主約的角別
			String initialType = fixPolicyVO.gitMasterCoverage().getCurrentPremium().getPaymentFreq();
			//清除原本所有險種資料,加入加保的CoverageVO
			List<CoverageVO> coverages = fixPolicyVO.getCoverages();
			coverages.clear();
			//取得 ITEM 最大值
			int itemOrder = policyDao.generateItemOrderByPolicyIdNoWaiver(fixPolicyVO.getPolicyId());
			
			//IR-254758-延遲通報表報(FOA POS收件通報,生效日與保障終止日因FOA填入資訊有限，以通報受理日作生效日，一年期作終止日通報)
			Calendar expireDate = Calendar.getInstance();
			expireDate.setTime(acceptanceVO.getAceptDate());
			expireDate.add(Calendar.YEAR, 1);
			
			List<ScFoaLiarocUploadVO> liarocList = vo.getScFoaLiarRocUploadList();
			for (ScFoaLiarocUploadVO scVo : liarocList) {
				
				CoverageVO coverage = new CoverageVO();
				coverage.setApplyDate(acceptVO.getApplyDate());
				coverage.setItemOrder(itemOrder++);
				coverage.setProductId(scVo.getProductId().intValue());
				coverage.setInceptionDate(acceptanceVO.getApplyDate());//IR-254758-受理日期作生效日
				coverage.setExpiryDate(expireDate.getTime()); //IR-254758-一年期作終止日通報
				//設定商品版本. 給附金額會用到
				coverage.setProductVersionId(productVersionService.getProductVersion(scVo.getProductId(), coverage.getApplyDate()).getVersionId());
				//設定保額/計畫/單位
				CoveragePremium currentPremium = coverage.getCurrentPremium();
				currentPremium.setSumAssured(scVo.getAmount());
				currentPremium.setUnit(scVo.getUnit());
				currentPremium.setBenefitLevel(scVo.getBenefitLevel());
				//TODO 給附金額會用到繳費年期
				coverage.setChargePeriod(CodeCst.CHARGE_PERIOD__CERTAIN_YEAR);
				coverage.setChargeYear(1);
				
				BigDecimal prem = scVo.getPremAmt();
				coverage.getCurrentPremium().setTotalPremAf(scVo.getPremAmt());
				if (prem != null && prem.compareTo(BigDecimal.ZERO) > 0) {
					//新增附約的角別會跟主約一樣					
					if (CodeCst.CHARGE_MODE__MONTHLY.equals(initialType)) {
						currentPremium.setStdPremAn(
										prem.multiply(new BigDecimal("6")));
					} else if (CodeCst.CHARGE_MODE__QTRLY.equals(initialType)) {
						currentPremium.setStdPremAn(
										prem.multiply(new BigDecimal("4")));
					} else if (CodeCst.CHARGE_MODE__HALF_YEARLY
									.equals(initialType)) {
						currentPremium.setStdPremAn(
										prem.multiply(new BigDecimal("2")));
					} else {
						currentPremium.setStdPremAn(prem);
					}
				}
				//設定被保人資訊
				InsuredVO insured = new InsuredVO();
				insured.setCertiCode(scVo.getCertiCode());
				insured.setName(scVo.getName());
				if( fixPolicyVO.getInsureds() != null) {
					for(InsuredVO insuredVO : fixPolicyVO.getInsureds()) {
						if(NBUtils.in(insuredVO.getCertiCode(), scVo.getCertiCode()) ) {
							insured = insuredVO;
							break;
						}
					}
				}
			
				CoverageInsuredVO coverInsured = new CoverageInsuredVO();
				coverInsured.setOrderId(1);
				coverInsured.setInsured(insured);
				List<CoverageInsuredVO> insureds = new ArrayList<CoverageInsuredVO>();
				insureds.add(coverInsured);
				coverage.setInsureds(insureds);
				coverages.add(coverage);
			}
			List<LiaRocUploadDetailVO> newDetails = (List<LiaRocUploadDetailVO>) this
							.getDetailList(fixPolicyVO, LiaRocCst.LIAROC_UL_TYPE_RECEIVE,
											true);
			
			for (LiaRocUploadDetailVO detailVO : newDetails) {
				detailVO.setStatusEffectDate(AppContext.getCurrentUserLocalTime());//IR-254758-POS 收件通報–延遲通報管理報表
				//detailBO.setValidateDate(acceptanceVO.getAceptDate());//IR-254758-受理日期作生效日
				//20180824 Email-要保書/契變書填寫日為生效日
				//like:T400019700
				//t_sc_foa_acceptance.apply_date=2018/08/22
				//t_contract_product.actual_validate_date=2018/08/22
				detailVO.setValidateDate(acceptanceVO.getApplyDate());
				//20180824 Email-滿期日清空
				detailVO.setExpiredDate(null);//Email-
				//liaRocUploadDetailDao.save(detailBO);

			}
			info.setDataList(newDetails);
			
			//20180824 Email-POS收件通報作AFINS POS收件通報比對
			//POS 只用到policyVO.policyCode,所以直接傳入
			Long uploadListId = this.saveReceiveUploadData(fixPolicyVO, info);

			ApplicationLogger.addLoggerData("uploadId=" + uploadListId);
			
		} catch (Exception e) {
			logger.error(ExceptionUtils.getFullStackTrace(e));
			ApplicationLogger.addLoggerData(ExceptionUtils.getFullStackTrace(e));
			ApplicationLogger.setReturnCode(ApplicationLogger.RETURN_CODE_FAIL);
			try {
				ApplicationLogger.flush();
			} catch (Exception e1) {
				logger.error(e1);
			}
			throw e;
		} finally {
			try {
				ApplicationLogger.flush();
			} catch (Exception e) {
				logger.error(e);
			}
		}
	}

	/**
	 * <p>取得通報明細檔資料，同時會將保單及保項資料轉換為符合公會的資料格式</p>
	 * <p>Created By : Amy Hung</p>
	 * <p>Create Time : Mar 7, 2016</p>
	 * @param policyVO 保單 VO
	 * @param liaRocUploadType 通報種類: 1.收件, 2.承保, 3.107通報(理賠)
	 * @return
	 */
	public List<? extends GenericEntityVO> getDetailList(PolicyVO policyVO, String liaRocUploadType) {
		return getDetailList(policyVO, liaRocUploadType, false);
	}

	/**
	 * <p>取得通報明細檔資料，同時會將保單及保項資料轉換為符合公會的資料格式</p>
	 * <p>Created By : Amy Hung</p>
	 * <p>Create Time : Mar 7, 2016</p>
	 * @param policyVO 保單 VO
	 * @param liaRocUploadType 通報種類: 1.收件, 2.承保, 3.107通報(理賠)
	 * @return
	 */
	public List<? extends GenericEntityVO> getDetailList(PolicyVO policyVO, String liaRocUploadType, boolean isFoa) {

		List<LiaRocUploadDetailVO> details = new ArrayList<LiaRocUploadDetailVO>();

		// setup 通報明細資料
		for (CoverageVO coverage : policyVO.gitSortCoverageList()) {
			if (this.needUploadItem(coverage)) {
				LiaRocUploadDetailWrapperVO detailMapVO = this.generateUploadDetail(policyVO, coverage, liaRocUploadType, false);
				if (!StringUtil.isNullOrEmpty(detailMapVO.getLiaRocProductCategory())
								&& !StringUtil.isNullOrEmpty(detailMapVO.getLiaRocProductType())) {
					details.add(detailMapVO);
				}
			}
		}

		return details;

	}

	/**
	 * <p>儲存通報明細檔資料</p>
	 * <p>若通報明細資料不是收件或承保的格式，可覆寫此方法，自行撰寫儲存明細資料的邏輯</p>
	 * <p>Created By : Amy Hung</p>
	 * <p>Create Time : Mar 7, 2016</p>
	 * @param liaRocUploadVO 通報主檔資料
	 * @param detailList 通報明細資料
	 */
	protected void saveUploadDetail(LiaRocUploadSendVO liaRocUploadVO, List<? extends GenericEntityVO> detailList) {

		for (GenericEntityVO detailVO : detailList) {

			LiaRocUploadDetail detailBO = new LiaRocUploadDetail();
			BeanUtils.copyProperties(detailBO, detailVO);
			detailBO.setUploadListId(liaRocUploadVO.getListId());

			liaRocUploadDetailDao.save(fixDetailNameLength(detailBO));

		}

	}

	@Override
	@SuppressWarnings("unchecked")
	public List<LiaRocUploadDetailWrapperVO> findUploadDetailDataByCriteria(List<String> bizSource, String liarocUploadType,
					String orderBy, boolean isOrderByAsc) {

		List<LiaRocUploadDetail> boResult = liaRocUploadDetailDao.findUploadDetailDataByCriteria(bizSource,
						liarocUploadType, orderBy, isOrderByAsc);

		List<LiaRocUploadDetailWrapperVO> voList = new ArrayList<LiaRocUploadDetailWrapperVO>();
		voList = (List<LiaRocUploadDetailWrapperVO>) BeanUtils.copyCollection(LiaRocUploadDetailWrapperVO.class,
						boResult);

		return voList;

	}

	@Override
	public void updateUploadStatus(Map<Long, String> uploadStatus, String reqUUID, int serialNum, Date requestTime,
					Long uploadId) {
		liaRocUploadDao.updateUploadStatus(uploadStatus, reqUUID, serialNum, requestTime, uploadId);
	}

	@Override
	public List<LiaRocUploadSendVO> send(List<LiaRocUploadSendVO> uploadVOs, String uploadType, String uploadModule)
					throws GenericException {

		// 依保單逐一執行上傳通報
		for (LiaRocUploadSendVO uploadVO : uploadVOs) {
			if (uploadVO.getDataList().size() > 0) {
				// 取得上傳通報的檔名
				String preFix = Para.getParaValue(LiaRocCst.PARAM_LIAROC_UL_FILENAME_REQUEST);
				String fileName = liaRocFileCI.getFileName(preFix);

				LiaRocUploadRequestRoot requestVO = new LiaRocUploadRequestRoot();

				requestVO.setCreateTime(DateUtils.date2string(AppContext.getCurrentUserLocalTime(), "yyyyMMddHHmmss"));
				requestVO.setTotalCount(uploadVO.getDataList().size());
				requestVO.setUploadModule(uploadModule);
				requestVO.setUploadType(uploadType);
				requestVO.setUploadFileName(fileName);

				InputStream inStream = null;
				String requestUUID = null;

				try {
					inStream = uploadVO.parseToText();
					//logger.info(
					//		"Policy:"+ uploadVO.getBizCode() + "公會通報內容=>"+new String(IoUtil.readBytes(inStream)));
				} catch (Exception e) {
					uploadVO.setLiaRocUploadStatus(LiaRocCst.LIAROC_UL_STATUS_PARSE_ERROR);
					logger.info("Policy:" + uploadVO.getBizCode() + " 上傳公會通報資料解析失敗, ErrMsg:" + e.getMessage());
				}

				if (inStream != null) {

					//                List<DataHandler> dataHandlers = new ArrayList<DataHandler>();
					//                DataHandler dataHandler;
					//                dataHandler = new DataHandler(new InputStreamDataSoure(inStream));
					//                dataHandlers.add(dataHandler);
					List<FileObject> files = new ArrayList<FileObject>();
					FileObject file = new FileObject();
					DataHandler dh = new DataHandler(new InputStreamDataSoure(inStream));
					logger.info("File:" + dh.getDataSource());
					file.setFile(dh);
					file.setFileName(dh.getName());
					files.add(file);
					try {
						RqHeader head = new RqHeader();
						//                  head.setCustID("");
						//                  head.setPrevReturnCode("");
						//                  head.setPrevReturnMsg("");
						//                  head.setPrevRqUID("1234567890-0987654321");
						// 呼叫服務平台
						StandardResponse rs = espWSServiceCI.liaROCUpload(head, requestVO, files);
						//                    RespXml resp = commonWSCI.callEsp(WebServiceName.ESP_LIAROC_UPLOAD_NB, requestVO, dataHandlers, null);

						String statusIndi = LiaRocCst.LIAROC_UL_STATUS_SEND;

						// 判斷服務平台回來的是成功還是失敗
						if (rs != null
										&& !rs.getRsHeader().getReturnCode()
														.equals(LiaRocCst.LIAROC_UPLOAD_ESP_SUCCESSFUL_CODE)) {
							statusIndi = LiaRocCst.LIAROC_UL_STATUS_SEND_FAIL;
						}

						requestUUID = (rs != null) ? rs.getRsHeader().getRqUID() : null;

						uploadVO.setLiaRocUploadStatus(statusIndi);

					} catch (Exception e) {
						uploadVO.setLiaRocUploadStatus(LiaRocCst.LIAROC_UL_STATUS_PROCESS_ERROR);
						logger.info("Policy:" + uploadVO.getBizCode() + " 呼叫服務平台上傳公會通報失敗, ErrMsg:" + e.getMessage());
					}
				}

				Map<Long, String> uploadStatus = new HashMap<Long, String>();
				Long listId = uploadVO.getListId();
				uploadStatus.put(listId, uploadVO.getLiaRocUploadStatus());

				int serialNum = Integer.parseInt(fileName.replaceAll(preFix, ""));
				this.updateUploadStatus(uploadStatus, requestUUID, serialNum, new Date(),
								AppContext.getCurrentUser().getUserId());

			}
		}

		return uploadVOs;

	}



	private Boolean isPOSMan(Long agentId) {
		if(agentId == null)
			return Boolean.FALSE;
		AgentChlVO agentChlVO = this.agentService.findAgentByAgentId(agentId);
		if(agentChlVO == null )
			return Boolean.FALSE;
		return Integer.valueOf("3").equals(agentChlVO.getBusinessCate());
	}
	/**
	 * see 受理編號107041306921
	 * CUST_APP_TIME(客戶申請日)	2018/03/26
	 * APPLY_TIME	(系統受理日)	2018/03/28
	 * 
	 * 保全收件通報的保單需要符合以下條件：
	 * i.	保全項目=新增附約（含豁免險）；
	 * ii.	保全狀態=錄入完成；
	 * iii.保全錄入完成日期=系統日期
	 * 依照送件單位處理如下 : 
	 * (1)	送件單位 : 空白或非簽約保經代。
	 * 比對未通報部份進行通報：收件通報系統(AFINS)與核心系統的資料比對【被保險人ID】+【系統受理日】+【險種代碼】，收件通報系統(AFINS)未通報者。
	 * 當比對相同時，視為“收件通報系統”已通報 ,核心系統就不須再通報。
	 * (2)	送件單位 : 直營通路AGY、STD、保全員。
	 * 比對未通報部份進行通報：收件通報系統(AFINS)與核心系統的資料比對【被保險人ID】+【客戶申請日】+【險種代碼】，收件通報系統(AFINS)未通報者。
	 * 當比對相同時，視為“收件通報系統”已通報 ,核心系統就不須再通報。
	 * (3)	送件單位 : 保經代代碼及BD，加保保項分類為：97-保經代傳真件、99-保經代壽險與傷害險。
	 * 比對未通報部份進行通報：收件通報系統(AFINS)與核心系統的資料比對【被保險人ID】+【客戶申請日】+【險種代碼】，收件通報系統(AFINS)未通報者。
	 * 當比對相同時，視為“收件通報系統”已通報 ,核心系統就不須再通報。
	 * (4)	送件單位 : 保經代代碼及BD，加保保項分類為：98-保經代健康險及傷害醫療險。
	 * 比對未通報部份進行通報：收件通報系統(AFINS)與核心系統的資料比對【被保險人ID】+【系統受理日】+【險種代碼】，收件通報系統(AFINS)未通報者。
	 * 當比對相同時，視為“收件通報系統”已通報 ,核心系統就不須再通報。
	 * c. 已完成收件通報件，系統自動判斷以下：
	 * i.	新契約通報件 : 要保書狀態如為拒絕、延期、撤件
	 * ii.	保全處理狀態 : Cancel、Rejected
	 * 該筆保單資料須再進行收件通報 以06-未承保取消件。
	 * (參考ATN-COR-UNB-BSD-005-PCR 承保作業-v0.2 BR-UNB-INF-002 儲存和更新承保資訊 描述)
	 * d. 新契約與保全通報資料，如被保險人身分證字號、生日錯誤，以12-鍵值欄位通報錯誤終止，並以15-通報更正重新通報。
	 * 
	 * 問題單241196是承保通報取值問題
	 * 收件通報【狀況生效日】&【契約生效日】應帶入【實際生效日】之欄位值非【生效日】之欄位值
	 */
	@Override
	public LiaRocUploadDetailWrapperVO generateCSUploadDetail(Application application, PolicyVO policy, CoverageVO coverage,
					String liaRocUploadType) {
	
		if (LiaRocCst.LIAROC_UL_TYPE_RECEIVE.equals(liaRocUploadType)) {
			/*
			//	 * CUST_APP_TIME(客戶申請日)	2018/03/26
			//	 * APPLY_TIME	(系統受理日)	2018/03/28	
			 */
			Long sendUnit = application.getSendUnit();
			ChannelOrgVO channelVO = sendUnit != null ? channelOrgService.getChannelOrg(sendUnit) : null;
			Boolean isPOSMan = isPOSMan(application.getSendPerson());
			
			//IR-275159-送件單位作保項分類
			if(channelVO != null) {
				if(NBUtils.in(channelVO.getChannelType().toString(), 
								CodeCst.SALES_CHANNEL_TYPE__BD, CodeCst.SALES_CHANNEL_TYPE__BR)) {
					policy.setChannelOrgId(sendUnit);
					policy.setChannelType(channelVO.getChannelType());
				}
			}
			
			LiaRocUploadDetailWrapperVO wrapperVO = generateUploadDetail(policy, coverage, liaRocUploadType);
			
			if (channelVO == null) {
				//(1)	送件單位 : 空白或非簽約保經代。【系統受理日】
				wrapperVO.setValidateDate(application.getApplyTime());
			} else if (NBUtils.in(channelVO.getChannelType().toString(), CodeCst.SALES_CHANNEL_TYPE__BD, CodeCst.SALES_CHANNEL_TYPE__BR)) {
				Boolean isNotContractChannel = NBUtils.isNotContractUnit(channelVO.getChannelCode());
				if(isNotContractChannel) {
					//(1)	送件單位 : 空白或非簽約保經代。【系統受理日】
					wrapperVO.setValidateDate(application.getApplyTime());
				} else {
			
					if(NBUtils.in(wrapperVO.getLiaRocRelationToPh(), LiaRocCst.LIAROC_RELA_TO_PH_97, LiaRocCst.LIAROC_RELA_TO_PH_99)) {
						//(3)	送件單位 : 保經代代碼及BD，加保保項分類為：97-保經代傳真件、99-保經代壽險與傷害險。【客戶申請日】
						wrapperVO.setValidateDate(application.getCustAppTime());
					}else if(NBUtils.in(wrapperVO.getLiaRocRelationToPh(), LiaRocCst.LIAROC_RELA_TO_PH_98)) {
						//(4)	送件單位 : 保經代代碼及BD，加保保項分類為：98-保經代健康險及傷害醫療險。【系統受理日】		
						wrapperVO.setValidateDate(application.getApplyTime());
					} else {
						//其它
						wrapperVO.setValidateDate(application.getCustAppTime());
					}
				}
			} else if(NBUtils.in(channelVO.getChannelType().toString(),
					CodeCst.SALES_CHANNEL_TYPE__AGY, CodeCst.SALES_CHANNEL_TYPE__STD,CodeCst.SALES_CHANNEL_TYPE__HO)
					|| isPOSMan){
				//(2)	送件單位 : 直營通路AGY、STD、HO、保全員。【客戶申請日】
				wrapperVO.setValidateDate(application.getCustAppTime());
			}

			//PCR_Gap_UNB_006-POS 的比對原則，不分通路，一律為：【要保險人 ID】 +【契變書填寫日】 +【險種代碼】
			//wrapperVO.setValidateDate(application.getCustAppTime());

			//保單狀況生效日期，保全變更時，此欄位請填異動生效日期(系統日期)
			wrapperVO.setStatusEffectDate(AppContext.getCurrentUserLocalTime());
			
			return wrapperVO;
		} else {
			LiaRocUploadDetailWrapperVO wrapperVO = generateUploadDetail(policy, coverage, liaRocUploadType);

			//保全收件通報使用實際生效日
			if (coverage.getActualInceptionDate() != null) {
				wrapperVO.setValidateDate(coverage.getActualInceptionDate());
				//2018/06/29 Zoe email, BSD如下： 
				//保單狀況生效日期，保全變更時，此欄位請填異動生效日期(系統日期)
				wrapperVO.setStatusEffectDate(AppContext.getCurrentUserLocalTime());
			}
			return wrapperVO;
		}

	}

	public LiaRocUploadDetailWrapperVO generateUploadDetail(PolicyVO policy, CoverageVO coverage,
					String liaRocUploadType) {
		return generateUploadDetail(policy, coverage, liaRocUploadType, false);
	}
	/**
	 * <p>Description : </p>
	 * <p>Created By : Simon Huang</p>
	 * <p>Create Time : 2020年2月24日</p>
	 * @param policy
	 * @param coverage
	 * @param liaRocUploadType
	 * @param isAfinsLiarocPolicyCode 是否以AFINS，格式保項通報編號A01,A02
	 * @return
	 */
	public LiaRocUploadDetailWrapperVO generateUploadDetail(PolicyVO policy, CoverageVO coverage,
					String liaRocUploadType, boolean isAfinsLiarocPolicyCode) {

		LifeProduct lifeProduct = lifeProductService.getProductByVersionId(coverage.getProductVersionId());
		//取得險種的要保書分類
		ProdBizCategorySub sub = pubPolicyService.getProposalProdCategory(lifeProduct.getProduct().getProductId());

		LiaRocUploadDetailWrapperVO detailVo = new LiaRocUploadDetailWrapperVO();

		detailVo.setItemId(coverage.getItemId());
		detailVo.setProductId(Long.valueOf(coverage.getProductId()));
		detailVo.setProductVersionId(Long.valueOf(coverage.getProductVersionId()));

		detailVo.setInternalId(getInternalIdByProuctId(coverage.getProductId()));

		// 產壽險別
		detailVo.setLiaRocType("1".equals(liaRocUploadType) ? LiaRocCst.RECEIVE_LIAROC_TYPE_LIFE
						: LiaRocCst.INFORCE_LIAROC_TYPE_LIFE);

		// 保險公司代號
		detailVo.setLiaRocCompanyCode(Para.getParaValue(LiaRocCst.PARAM_LIAROC_TGL_COMPANY_CODE));
		Date birthday = null;
		String isGuardianAncmnt = null;
		// setup 被保險人資料
		
		PolicyHolderVO holder = policy.getPolicyHolder();
		InsuredVO masterInsured = policy.gitMasterInsured();
		if (coverage.getLifeInsured1() != null) {

			InsuredVO insured = coverage.getLifeInsured1().getInsured();
			if(masterInsured == null) {
				masterInsured = insured;
			}
			// 被保險人姓名
			String insuredName = insured.getName();
			//  PCR-243784  配合OIU通報公會修改姓名欄位長度判斷方式 modify by Simon 2018.08.05 	
			insuredName = substring(insuredName, 18);
			detailVo.setName(insuredName);

			// 被保險人身份證號
			detailVo.setCertiCode(insured.getCertiCode());
			birthday = insured.getBirthDate();
			// 被保險人出生年月日
			if(!NBUtils.isEmptyOrDummyDate(birthday)) {
				detailVo.setBirthday(birthday);	
			}
			

			// 被保險人性別
			detailVo.setGender(liaRocUploadCommonService.getLiaRocTargetCodeValue("T_GENDER", insured.getGender(), "sex"));
			//被保人是否受有監護宣告
			isGuardianAncmnt = insured.getGuardianAncmnt();
			// 要保人與被保險人關係/保經代傳真件(97)及健康/醫療險(98)以「受理/收件確認日期」通報；含死亡給付險種(99)以「要保書填寫日」;
			if ("1".equals(liaRocUploadType)) {
				Long salesChannelId = policy.getChannelType();
				String channelExtInt = policyDS.getChannelExInt(salesChannelId);
				
				//判斷是否外部通路(保經代件);
				//97  保經代傳真件
				//98　保經代健康險及傷害醫療險
				//99　保經代壽險與傷害險
				if (CodeCst.YES_NO__YES.equals(channelExtInt)) {
					
					String isBrbdFax = policy.getIsBrbdFax();
					if ((CodeCst.YES_NO__YES).equals(isBrbdFax)) {
						//保經代傳真件(97);
						detailVo.setLiaRocRelationToPh(LiaRocCst.LIAROC_RELA_TO_PH_97);
					} else if ((CodeCst.YES_NO__YES).equals(channelExtInt)) {
						Long productId = Long.valueOf(coverage.getProductId());
						Long versionId = Long.valueOf(coverage.getProductVersionId());
						//IR-275342-PCH 公會通報 保經代分類通報98， 但PCH通報有身故給付 保經代分類通報應為99
						//修正判斷通報身故判斷為是否有身故給付
						
						/*
						boolean isDeathLiab = lifeProductLiabilityService.isContainLiability(
										productId,	
										versionId,
										LiabilityConfigSub.SUB_2001);
						*/
						boolean isDeathLiab = false;
						//PCR-291881 2019/01/24 Add by Kathy 
						//BD/BR通路, 以商品ID+版本ID判斷是否有設定在t_liaroc_product_conf
						//有存在-設定為99(保經代壽險與傷害險)
						//不存在-設定為98(保經代健康險及傷害醫療險)

						isDeathLiab = liaRocUploadDao.getLiarocProductConf(productId);
						
						/* PCR-291881 2019/01/24 Delete by Kathy 
						 * 以liaRocUploadDao.getLiarocProductConf(productId, versionId) 取代
						List<Map<String,Object>> liabs = liaRocUploadDao.getLiarocLiabilityConf(productId, versionId);
						if(liabs != null) {
							for(Map<String,Object> liab : liabs) {
								if(liab.get("LIAROC_LIA_ID") != null) {
									String liaRocLiabId = liab.get("LIAROC_LIA_ID").toString();
									if (LiaRocCst.LIAROC_LIAB_CODE_11.equals(liaRocLiabId)) {
										isDeathLiab = true;
										break;//break foreach;
									}
								}
							}
						}
						*/
						if (isDeathLiab) {
							//含死亡給付險種(99);
							detailVo.setLiaRocRelationToPh(LiaRocCst.LIAROC_RELA_TO_PH_99);
						} else {
							//健康/醫療險(98);
							detailVo.setLiaRocRelationToPh(LiaRocCst.LIAROC_RELA_TO_PH_98);
						}
					} 
				}else {
					//IR-359214  要保人與被保險人關係移至LiaRocUploadDetailDao.getLiaRocRelationToPH處理  2019/12/22 modify by Kathy
					//detailVo.setLiaRocRelationToPh(getLiaRocRelationToPH(policy, insured));
					detailVo.setLiaRocRelationToPh(liaRocUploadDetailDao.getLiaRocRelationToPH(
									holder.getPartyType(),
									masterInsured.getRelationToPH(),
									insured.getNbInsuredCategory()));
				}
				
				
			} else {
				/**承保通報此欄位定義為要保人與被保險人關係 **/
				//IR-359214  要保人與被保險人關係移至LiaRocUploadDetailDao.getLiaRocRelationToPH處理  2019/12/22 modify by Kathy
				//detailVo.setLiaRocRelationToPh(getLiaRocRelationToPH(policy, insured));
				detailVo.setLiaRocRelationToPh(liaRocUploadDetailDao.getLiaRocRelationToPH(
								holder.getPartyType(),
								masterInsured.getRelationToPH(),
								insured.getNbInsuredCategory()));
			}
		}

	
		
		// 保單分類
		String policyType = "1";
		if (CodeCst.SUBMIT_CHANNEL__EC.equals(policy.getSubmitChannel())) {
			policyType = "5";
		}
		detailVo.setLiaRocPolicyType(policyType);

		// 險種分類
		detailVo.setLiaRocProductCategory(this.getLiaRocProdCateCode(Long.valueOf(coverage.getProductId())));
		if (detailVo.getLiaRocProductCategory() == null)
			detailVo.setLiaRocProductCategory("");

		// 險種
		detailVo.setLiaRocProductType(this.getLiaRocProdCode(Long.valueOf(coverage.getProductId())));
		if (detailVo.getLiaRocProductType() == null)
			detailVo.setLiaRocProductType("");

		Date validateDate = policy.getApplyDate();

		// 保單狀況生效日：收件為要保書填寫日，承保為承保日期;
		boolean isMicro = lifeProductCategoryService.isMicro(coverage.getProductId().longValue());
		// 契約生效日：收件為要保書填寫日，承保為生效日
		if (LiaRocCst.LIAROC_UL_TYPE_RECEIVE.equals(liaRocUploadType)) {

			String liaRocRelationToPh = detailVo.getLiaRocRelationToPh();
			//(1)內部通路：以「要保書填寫日」通報。;
			//(2)外部通路：保經代傳真件(97)及健康/醫療險(98)以「受理/收件確認日期」通報；含死亡給付險種(99)以「要保書填寫日」;
			//(3)微型保單 收件通報以通路受理日通報;
			if (isMicro && !NBUtils.isEmptyOrDummyDate(policy.getSubmissionDate())) {
				validateDate = policy.getSubmissionDate();
			} else if ("97".equals(liaRocRelationToPh) || "98".equals(liaRocRelationToPh)) {
				validateDate = policy.getReceiveDate();
			} else {
				validateDate = policy.getApplyDate();
			}

			//IR-297620-保全 DC件會有ReceiveDate為空的情況,先以applyDate,會由generateCSUploadDetail覆寫validateDate;
			if(validateDate == null) {
				validateDate = policy.getApplyDate();
			}
			
		} else if (LiaRocCst.LIAROC_UL_TYPE_INFORCE.equals(liaRocUploadType)) {
			validateDate = coverage.getInceptionDate();
		}

		// 給付金額的計算(非台幣需依要保書填寫日換匯至台幣)
		detailVo = this.calcLiabilityMoney(detailVo, coverage,
						lifeProduct.getProduct().getBaseMoney());

		detailVo.setValidateDate(validateDate);
		// 契約滿期日
		//IR 186881 經與使用者討論後, 因公會偵錯"(契約滿期年-被保險人出生年)≦112。", 故若商品設定設商品為"終身"(滿期日=9999/12/31),
		//公會通報的契約滿期日期的年, 請以契約生效日期的年+112-被保險人年齡通報(含收件/承保)
		//IR 188586 因為保險年齡未滿半歲不算, 所以匯有(契約滿期年-被保險人出生年)≦113 的錯誤
		//改為(生日年+112) + 生效日年月
		Date expiryDate = coverage.getExpiryDate();
		if (expiryDate != null && !NBUtils.isEmptyOrDummyDate(birthday)
						&& (DateUtils.getYear(expiryDate) - DateUtils.getYear(birthday)) > 112) {
			Date vDate = detailVo.getValidateDate();
			int addYear = DateUtils.getYear(birthday) + 112 - DateUtils.getYear(vDate);
			detailVo.setExpiredDate(DateUtils.addYear(vDate, addYear));
		} else {
			detailVo.setExpiredDate(expiryDate);
		}

		//保費(需作匯率轉換)
		if (coverage.getCurrentPremium().getTotalPremAf() != null) {
			BigDecimal prem = coverage.getCurrentPremium().getTotalPremAf();
			if (coverage.getCustomizedPrem() != null
							&& lifeProduct.isInvestLink()) {
				prem = coverage.getCustomizedPrem();
			}

			if (CodeCst.MONEY__NTD != lifeProduct.getProduct().getBaseMoney()) {
				prem = liaRocUploadCommonService.transferToTwCurrency(lifeProduct.getProduct().getBaseMoney(), coverage.getApplyDate(), prem);
			}
			detailVo.setPrem(prem.longValue());
		} else {
			detailVo.setPrem(BigDecimal.ZERO.longValue());
		}

		// 保費繳別(transfer to 公會代碼)
		String initialType = coverage.getCurrentPremium().getPaymentFreq();
		
		//PCR-391319 主約險種若為投資型商品 且 商品設定->服務規則->投資型規則的「可否單次追加投資」欄位=是，
		//繳別非躉繳者，依實際繳別通報(如.年半季月)
		//繳別為躉繳者，如商品設定中投資型規則的‘可否單次追加投資欄位’=是，保費繳別通報6-彈性繳
		//                           ‘投資型規則的可否單次追加投資’欄位=否，則通報1-躉繳
		if (ProdBizCategorySub.SUB_12_002.equals(sub) //投資型商品
				 && (lifeProduct.getProduct().getTopupPermit().equals(CodeCst.YES_NO__YES)) //可否單次追加投資
			     && (CodeCst.CHARGE_MODE__SINGLE.equals(initialType))) //繳別為躉繳
		{
			initialType= "6"; //彈性繳
		}
		detailVo.setLiaRocChargeMode(liaRocUploadCommonService.getLiaRocTargetCodeValue("T_CHARGE_MODE", initialType, "bamttype"));
		if (detailVo.getLiaRocChargeMode() == null) {
			detailVo.setLiaRocChargeMode(" ");
		}
		detailVo.setLiaRocPolicyStatus(LiaRocCst.LIAROC_POLICY_STATUS__INFORCE);			
		// BC398 PCR-392013 判斷商品險種給付項目是否含喪葬費用保險金商品 2020/06/22 Add by Kathy
		//承保通報時再去判斷給付項目是否含有喪葬費用保險金商品 2020/07/31 Add by Kathy
		if (LiaRocCst.LIAROC_UL_TYPE_INFORCE.equals(liaRocUploadType)){
			String liaRocPolicyStatus50 = liaRocUploadDao.getProductLiaRocPolicyStatus50(detailVo.getCertiCode(),
							policy.getPolicyId(),
							policy.getApplyDate(),
							detailVo.getProductId(),
							detailVo.getProductVersionId());
				// 保險法107條第二次修正生效日期 2020/06/12
				Date law107EffectDate = com.ebao.pub.util.json.DateUtils.parse(Para.getParaValue(2042100051L), com.ebao.pub.util.json.DateUtils.datePatternYYMMDD);
				//足歲
				long ifAge = BizUtils.getAge(CodeCst.AGE_METHOD__ALAB,birthday, validateDate);

				// 保單狀況
				if ( (coverage.getApplyDate().compareTo(law107EffectDate) >= 0) &&
								(ifAge < 15 )&& (CodeCst.YES_NO__YES.equals(liaRocPolicyStatus50))){
					//要保書填寫日>=109.06.12(含)，被保險人未滿15足歲，購買險種給付項目含喪葬費用需以50(一○七條/一○七條之一承保資料)通報
					detailVo.setLiaRocPolicyStatus(LiaRocCst.LIAROC_POLICY_STATUS__107_INFORCE);
				}
				
				//被保人有註記受監護宣告時，承保通報要通報50 2021/01/22 Add by Kathy
				if (CodeCst.YES_NO__YES.equals(isGuardianAncmnt)) {
					detailVo.setLiaRocPolicyStatus(LiaRocCst.LIAROC_POLICY_STATUS__107_INFORCE);
				}
		}

		// 保單狀況生效日：收件為要保書填寫日，承保為承保日期;
		if (LiaRocCst.LIAROC_UL_TYPE_RECEIVE.equals(liaRocUploadType)) {
			detailVo.setStatusEffectDate(detailVo.getValidateDate());
		} else if (LiaRocCst.LIAROC_UL_TYPE_INFORCE.equals(liaRocUploadType)) {
			if(policy.getIssueDate() != null 
							&& policy.getRiskStatus() != null 
							&& policy.getRiskStatus() == CodeCst.LIABILITY_STATUS__IN_FORCE) {
				detailVo.setStatusEffectDate(policy.getIssueDate());	
			} else {
				//承保後UNDO執行生效06通報，無issue_date，抓取系統日送出
				detailVo.setStatusEffectDate(new Date());
			}
		}

		// 要保人姓名
		String holderName = policy.getPolicyHolder().getName();
		//  PCR-243784  配合OIU通報公會修改姓名欄位長度判斷方式 modify by Simon 2018.08.05
		holderName = substring(holderName, 18);
		detailVo.setPhName(holderName);

		// 要保人身份證號
		detailVo.setPhCertiCode(policy.getPolicyHolder().getCertiCode());

		// 要保人生日
		if(!NBUtils.isEmptyOrDummyDate(policy.getPolicyHolder().getBirthDate())) {
			detailVo.setPhBirthday(policy.getPolicyHolder().getBirthDate());	
		}
		

		// 保單號碼
		int itemOrder = (coverage.getItemOrder() == null ? 0 : coverage.getItemOrder());
		LiaRocUploadCoverage uploadCoverage = null;
		if (coverage.getItemId() != null && coverage.getItemId().longValue() > 0L) {
			uploadCoverage = liaRocUploadCoverageDao.findByPolicyIdItemId(policy.getPolicyId(), coverage.getItemId());
		}

		if (uploadCoverage != null) {
			//PCR 188901 已落地保項需以先前保項通報編號作通報
			detailVo.setPolicyId(uploadCoverage.getLiarocPolicyCode());
		} else {

			String originalSystem = productVersionService.getProductVersion(coverage.getProductId().longValue(),
							coverage.getApplyDate()).getOriginalSystem();
			String systemType = "";

			String liaRocPolicyCodePrefix = policy.getPolicyNumber();
			if (policy.getPolicyId() != null && policy.getPolicyId().longValue() > 0L) {
				//PCR 188901 落地保單舊件以保單號,新件以受理編號(無受理編號則以保單號)
				liaRocPolicyCodePrefix = liaRocUploadCommonService.getLiaRocPolicyCodePrefix(policy.getPolicyId());
			}

			//IR-354714-受理編號/流水編號欄位如>=18碼時系統會報錯無法存檔
			liaRocPolicyCodePrefix = NBUtils.getSerialNumFixLength17(liaRocPolicyCodePrefix);
			
			if ("1".equals(originalSystem)) {
				/* 返回值1 为T */
				systemType = "T";
			}
			if ("2".equals(originalSystem)) {
				/* 返回值为2 则为K */
				systemType = "K";
			}

			if (!"K".equals(systemType)) {
				if(isAfinsLiarocPolicyCode == false) {
					String checkFmt = LiaRocCst.ITEM_ORDER_FMT_NORMAL;
					detailVo.setPolicyId(liaRocPolicyCodePrefix + String.format(checkFmt, itemOrder));	
				} else {
					//IR-353680-調整AFINS傳送保項時保項編號以A01、A02...續編。
					String tmpPolicyCode = this.generateAfinsPolicyCode(
									policy.getPolicyId(),
									detailVo.getCertiCode(),
									detailVo.getInternalId(),
									detailVo.getValidateDate(),
									liaRocPolicyCodePrefix, itemOrder,
									detailVo.getLiaRocRelationToPh());
					detailVo.setPolicyId(tmpPolicyCode);
				}
			} else {
				//K系統保單是保單號碼+險種代碼
				String planCode = coverage.getOldProductCode();
				//20180824 Email-判斷需為DC-315轉入舊險種才作old_product_code
				boolean isOld = false;
				if (coverage.getItemId() != null) {
					Long insertedBy = liaRocUploadDao.getInsertedBy(coverage.getItemId());
					if (insertedBy != null && insertedBy.intValue() == 315) {
						isOld = true;
					}
				}

				if (isOld && StringUtils.isNotBlank(planCode)) {
					//去除版本險種代
					int index = planCode.indexOf("-", 0);
					if (index > 0) {
						planCode = planCode.substring(0, index - 1).trim();
					}
				} else {
					//新增附約找不到舊險種代號, 需用新險種代號
					planCode = this.productLifeDS.getProduct(coverage.getProductId().longValue()).getInternalId();
				}
				detailVo.setPolicyId(liaRocPolicyCodePrefix + "-" + planCode);
			}
		}
		return detailVo;

	}

	/**
	 * <p>Description : 公會通報給付金額轉換</p>
	 * 注意：呼叫此方法時，傳入之 detailVO 必須  assign 好要通報之保項的 item_id, product_id, product_version_id
	 * <p>Created By : Amy Hung</p>
	 * <p>Create Time : Aug 17, 2016</p>
	 * @param detailVO 通報明細 VO
	 * @param integer 
	 * @param validateDate 
	 * @return 將傳立之 detailVO 填入需要通報的給付項目之給付金額後回傳
	 */
	public LiaRocUploadDetailWrapperVO calcLiabilityMoney(LiaRocUploadDetailWrapperVO detailVO,
					CoverageVO coverage, Integer sourceCurrency) {

		detailVO.setLiability01(0L);
		detailVO.setLiability02(0L);
		detailVO.setLiability03(0L);
		detailVO.setLiability04(0L);
		detailVO.setLiability05(0L);
		detailVO.setLiability06(0L);
		detailVO.setLiability07(0L);
		detailVO.setLiability08(0L);
		detailVO.setLiability09(0L);
		detailVO.setLiability10(0L);
		detailVO.setLiability11(0L);
		detailVO.setLiability12(0L);
		detailVO.setLiability13(0L);
		detailVO.setLiability14(0L);
		detailVO.setLiability15(0L);
		detailVO.setLiability16(0L);

		// 查詢這個商品要通報公會的理賠給付項目資料
		List<Map<String, Object>> liabs = liaRocUploadDao.getProductLiaRocLiabs(detailVO.getProductId(),
						detailVO.getProductVersionId());

		Long amount = 0L;

		if (liabs != null) {

			// 逐一的去計算各給付項目的給付金額
			for (Map<String, Object> liab : liabs) {

				amount = 0L;

				if (liab.get("LIAROC_LIA_ID") != null) {
					Long liarocLiaId = Long.valueOf(liab.get("LIAROC_LIA_ID").toString());

					BigDecimal liabAmount = calcLiabAmountFormula(coverage, liarocLiaId, false);

					if (liabAmount != null) {
						amount = liabAmount.longValue();

						//需轉為台幣
						if (CodeCst.MONEY__NTD != sourceCurrency) {
							BigDecimal twLiabAmount = liaRocUploadCommonService.transferToTwCurrency(sourceCurrency, coverage.getApplyDate(), liabAmount);
							amount = twLiabAmount.longValue();
						}
					}

				}

				// 依不同的給付項目把計算結果 assign 到相應的欄位
				if (liab.get("LIAROC_LIA_ID") != null) {

					String liaRocLiabId = liab.get("LIAROC_LIA_ID").toString();
					//2020/02/24 因AFINS上傳保額過大,無法寫入通報明細檔,造成收件延遲通報,增加判斷過大保額強制寫入9999999999
					if(LiaRocCst.LIAB_MAXIMUM_AMOUNT < amount) {
						amount = LiaRocCst.LIAB_MAXIMUM_AMOUNT;
					}
					
					if (LiaRocCst.LIAROC_LIAB_CODE_11.equals(liaRocLiabId)) {
						detailVO.setLiability01(amount);
					} else if (LiaRocCst.LIAROC_LIAB_CODE_12.equals(liaRocLiabId)) {
						detailVO.setLiability02(amount);
					} else if (LiaRocCst.LIAROC_LIAB_CODE_13.equals(liaRocLiabId)) {
						detailVO.setLiability03(amount);
					} else if (LiaRocCst.LIAROC_LIAB_CODE_14.equals(liaRocLiabId)) {
						detailVO.setLiability04(amount);
					} else if (LiaRocCst.LIAROC_LIAB_CODE_15.equals(liaRocLiabId)) {
						detailVO.setLiability05(amount);
					} else if (LiaRocCst.LIAROC_LIAB_CODE_16.equals(liaRocLiabId)) {
						detailVO.setLiability06(amount);
					} else if (LiaRocCst.LIAROC_LIAB_CODE_17.equals(liaRocLiabId)) {
						detailVO.setLiability07(amount);
					} else if (LiaRocCst.LIAROC_LIAB_CODE_18.equals(liaRocLiabId)) {
						detailVO.setLiability08(amount);
					} else if (LiaRocCst.LIAROC_LIAB_CODE_19.equals(liaRocLiabId)) {
						detailVO.setLiability09(amount);
					} else if (LiaRocCst.LIAROC_LIAB_CODE_20.equals(liaRocLiabId)) {
						detailVO.setLiability10(amount);
					} else if (LiaRocCst.LIAROC_LIAB_CODE_21.equals(liaRocLiabId)) {
						detailVO.setLiability11(amount);
					} else if (LiaRocCst.LIAROC_LIAB_CODE_22.equals(liaRocLiabId)) {
						detailVO.setLiability12(amount);
					} else if (LiaRocCst.LIAROC_LIAB_CODE_23.equals(liaRocLiabId)) {
						detailVO.setLiability13(amount);
					} else if (LiaRocCst.LIAROC_LIAB_CODE_24.equals(liaRocLiabId)) {
						detailVO.setLiability14(amount);
					} else if (LiaRocCst.LIAROC_LIAB_CODE_25.equals(liaRocLiabId)) {
						detailVO.setLiability15(amount);
					} else if (LiaRocCst.LIAROC_LIAB_CODE_26.equals(liaRocLiabId)) {
						detailVO.setLiability16(amount);
					}

				}
				// end.

			}

		}

		return detailVO;

	}

	/**
	 * <p>Description : 公會通報理賠給付項目金額計算</p>
	 * <p>Created By : tgl139</p>
	 * <p>Create Time : Nov 10, 2016</p>
	 * @param itemId 保項Id
	 * @param productId 商品Id
	 * @param productVersionId 商品版本Id
	 * @param liarocLiaId 公會通報理賠給付項目
	 * @return
	 */
	public BigDecimal calcLiabAmountFormula(Long itemId, Long productId, Long productVersionId, Long liarocLiaId) {
		return liaRocUploadDao.calcLiabAmountFormula(itemId, productId, productVersionId, liarocLiaId);
	}

	/**
	 * 公會通報理賠給付項目金額計算 
	 * @param coverage
	 * @param liarocLiaId
	 * @return
	 */
	public BigDecimal calcLiabAmountFormula(CoverageVO coverage, Long liarocLiaId, boolean isPos) {
		return liaRocUploadDao.calcLiabAmountFormula(coverage, liarocLiaId, false);
	}


	@Override
	public Long sendNB(Long policyId, String liaRocUploadType) {

		Long uploadId = null;
		if (LiaRocCst.LIAROC_UL_TYPE_RECEIVE.equals(liaRocUploadType)) {
			uploadId = sendNBReceiveNew(policyId);
		} else if (LiaRocCst.LIAROC_UL_TYPE_INFORCE.equals(liaRocUploadType)) {
			uploadId = sendNBInforce(policyId);
		}
		return uploadId;
	}

	//PCR-288932 mPos外部通路與一般進件收件通報拆開處理 2018/12/22 Add by Kathy
	@Override
	public Long sendmPosNB(Long policyId, String liaRocUploadType) {

		Long uploadId = null;
		if (LiaRocCst.LIAROC_UL_TYPE_RECEIVE.equals(liaRocUploadType)) {
			//2020/04/23 Joanne's eMail 保經代的99險種不進行通報，通報責任在保經代 by Kathy
			uploadId = sendMPosReceiveNew(policyId);

		} else if (LiaRocCst.LIAROC_UL_TYPE_INFORCE.equals(liaRocUploadType)) {
			uploadId = sendNBInforce(policyId);
		}
		return uploadId;
	}
	
	
	private Long sendNBInforce(Long policyId) {

		Long uploadId = null;
		//OIU商品不需要收件通報與承保通報，且無須索引(下載)同業投保紀錄。TGL32/VICTOR CHANG;
		//EC(網路投保=>列TODO)不報
		PolicyVO policyVO = this.policyDS.retrieveById(policyId, false);
		if (policyVO.getRiskStatus() != 1) {
			//若有UNDO 保單為未生效不用報公會 
			return uploadId;
		}
		Date lastBatchDate = liaRocUploadCommonService.getLastNBBatchDate(policyId, LiaRocCst.LIAROC_NB_BATCH_CHANGE_TYPE_11);
		if(lastBatchDate != null 
						&& DateUtils.date2String(lastBatchDate).equals(DateUtils.date2String(new Date()))){
			//若同有承保UNDO後通報承保06的情況, 再次承保需判斷 是否有同日生效,有則不 執行承保通報,由SMART QUERY 產生報表後人工通報
			liaRocUploadCommonService.recordNBBatchData(policyId, LiaRocCst.LIAROC_NB_BATCH_CHANGE_TYPE_13);
			return uploadId;
		}
		
		//PCR-243784 OIU自2018/08/01 開始要通報公會故移除OIU判斷 delete by Kathy
		//if ((policyVO.isOIU()) == false) {

		List uploads = this.liaRocUploadDao.queryByPolicyIdAndTypeAndSource(policyId, LiaRocCst.LIAROC_UL_TYPE_INFORCE, LiaRocCst.LIAROC_UL_BIZ_SOURCE_UNB);
		if (uploads == null || uploads.size() == 0) {
			/**非undo 直接報公會**/
			LiaRocUploadSendVO sendVO = checkLiaRocProduct(getUploadVOByPolicyId(policyId, LiaRocCst.LIAROC_UL_TYPE_INFORCE, LiaRocCst.LIAROC_UL_BIZ_SOURCE_UNB));
		
			uploadId = this.saveUpload(sendVO);
			return uploadId;
		} else {
			LiaRocUpload oldUpload = (LiaRocUpload) uploads.get(0);
			LiaRocUploadVO oldUploadVO = new LiaRocUploadVO();
			BeanUtils.copyProperties(oldUploadVO, oldUpload);
			String status = oldUploadVO.getLiaRocUploadStatus();
			if (LiaRocCst.LIAROC_UL_STATUS_SEND.equals(status) || LiaRocCst.LIAROC_UL_STATUS_RETURN.equals(status)
							|| LiaRocCst.LIAROC_UL_STATUS_RETURN_ERROR.equals(status)) {
				/**通報資料已經送出,重新報一次**/
				// make 主檔 vo
				LiaRocUpload liaRocUploadBO = new LiaRocUpload();
				liaRocUploadBO.setPolicyId(policyId);
				liaRocUploadBO.setBizSource(LiaRocCst.LIAROC_UL_BIZ_SOURCE_UNB); // 新契約。
				liaRocUploadBO.setBizId(policyId);
				liaRocUploadBO.setBizCode(oldUploadVO.getBizCode());
				liaRocUploadBO.setLiaRocUploadType(LiaRocCst.LIAROC_UL_TYPE_INFORCE);
				liaRocUploadBO.setUploadId(AppContext.getCurrentUser().getUserId());
				liaRocUploadBO.setLiaRocUploadStatus(LiaRocCst.LIAROC_UL_STATUS_WAITING);
				// 儲存主檔資料
				liaRocUploadDao.save(liaRocUploadBO);

				uploadId = liaRocUploadBO.getListId();

				Map<String, LiaRocUploadDetail> detailMap = new HashMap<String, LiaRocUploadDetail>();
				List<LiaRocUploadDetail> details = liaRocUploadDetailDao.findByUploadId(oldUploadVO.getListId());
				for (LiaRocUploadDetail detail : details) {
					/* 01, 50 均為有效通報 */
					if (NBUtils.in(detail.getLiaRocPolicyStatus(), 
							LiaRocCst.LIAROC_POLICY_STATUS__107_INFORCE, LiaRocCst.LIAROC_POLICY_STATUS__INFORCE)) {
						detailMap.put(detail.getPolicyId(), detail);
					}
				}
				// get 明細資料 vo。
				List<GenericEntityVO> newDetails = (List<GenericEntityVO>) this.getDetailList(policyService.loadPolicyByPolicyId(policyId), LiaRocCst.LIAROC_UL_TYPE_INFORCE);
				for (GenericEntityVO detailVO : newDetails) {
					LiaRocUploadDetail detailBO = new LiaRocUploadDetail();
					BeanUtils.copyProperties(detailBO, detailVO);
					detailBO.setUploadListId(liaRocUploadBO.getListId());
					LiaRocUploadDetail detail = detailMap.get(detailBO.getPolicyId());
					if (detail != null) {
						/** 已經通報 **/
						//2020/09/08 Simon 生日相同但卻通報12,01
						//detail.getBirthday() 為java.sql.Timestamp, detailBO.getBirthday()為java.util.Date,必須要用getTime()作比對
						boolean isBirthdayChange = false;
						if(detail.getBirthday() != null && detailBO.getBirthday() != null) {
							isBirthdayChange = !(detail.getBirthday().getTime() == detailBO.getBirthday().getTime()) ;
						}
						
						if (!detail.getCertiCode().equals(detailBO.getCertiCode()) || isBirthdayChange) {
							/** 身分證字號或生日不一樣要先報12再重報 **/
							LiaRocUploadDetail detailBO12 = createFixUploadDetail(detail, liaRocUploadBO.getListId(), LiaRocCst.LIAROC_POLICY_STATUS__ERROR_TERMINATION);
							//12-鍵值錯誤的通報狀態變更日與最新通報的更新日相同
							detailBO12.setStatusEffectDate(detailBO.getStatusEffectDate());
							liaRocUploadDetailDao.save(detailBO12);
							liaRocUploadDetailDao.save(detailBO);
						} else {
							/** 只修改資料 **/
							detailBO.setListId(liaRocUploadBO.getListId());
							liaRocUploadDetailDao.save(detailBO);
						}
						/**移除Map中已處理的,剩下就是最後要刪除的保項**/
						detailMap.remove(detailBO.getPolicyId());
					} else {
						/**尚未通報**/
						liaRocUploadDetailDao.save(detailBO);
					}

				}
				if (!detailMap.isEmpty()) {
					/**比對完還存在表示已被刪除要新增一筆狀態為6(契約撤銷)**/
					for (String policyCode : detailMap.keySet()) {
						LiaRocUploadDetail detailBO = new LiaRocUploadDetail();
						LiaRocUploadDetail detail = detailMap.get(policyCode);
						BeanUtils.copyProperties(detailBO, detail);
						detailBO.setLiaRocPolicyStatus(LiaRocCst.LIAROC_POLICY_STATUS__FREELOOK);
						detailBO.setUploadListId(oldUploadVO.getListId());
						detailBO.setListId(null);
						liaRocUploadDetailDao.save(detailBO);
					}
				}

			} else if (LiaRocCst.LIAROC_UL_STATUS_WAITING.equals(status)) {
				/**待通報要修正原本資料**/
				List<LiaRocUploadDetail> details = liaRocUploadDetailDao.findByUploadId(oldUploadVO.getListId());

				uploadId = oldUploadVO.getListId();

				for (LiaRocUploadDetail detail : details) {
					liaRocUploadDetailDao.remove(detail);
				}
				
				LiaRocUploadSendVO sendVO = checkLiaRocProduct(getUploadVOByPolicyId(policyId, LiaRocCst.LIAROC_UL_TYPE_INFORCE, LiaRocCst.LIAROC_UL_BIZ_SOURCE_UNB));
				uploadId = this.saveUpload(sendVO);
				return uploadId;
			} else {
				/**通報流程失敗重新發送**/
				//TODO 原本通報若有錯是否要註記已發送第二次
				uploadId = this.saveUpload(checkLiaRocProduct(getUploadVOByPolicyId(policyId, LiaRocCst.LIAROC_UL_TYPE_INFORCE, LiaRocCst.LIAROC_UL_BIZ_SOURCE_UNB)));
			}

			return uploadId;
		}
		//}

		//return null;
	}

	/**
	 * FOA NB受理改call此method
	 * @param foaPolicyVOs
	 */
	public void saveAcceptanceUploadNB(long caseId) throws Exception{

		FoaPolicyVO vo = foaAcceptanceCI.findFoaPolicyByCaseId(caseId);
		saveAcceptanceUploadNB(vo);
	}

	private static String FOA_UPLOAD_NB = "saveAcceptanceUploadNB";

	private void saveAcceptanceUploadNB(FoaPolicyVO vo)  throws Exception{

		ApplicationLogger.setSystemCode(AppLogDataHelper.SYSTEM_CODE_UNB);
		ApplicationLogger.setJobName(FOA_UPLOAD_NB);
		ApplicationLogger.setOptionId(DateUtils.date2String(new Date(), "yyyyMMdd HHmmss"));
		try {
			PolicyVO policyVO = foaAcceptanceHelper.convert2PolicyVO(vo, false);
			ApplicationLogger.setPolicyCode(policyVO.getPolicyNumber());
			
			//PCR-337606 判斷FOA 會有重複送件問題，所以要檢核，第二筆壓 D 2020/09/09 Add by Kathy
			String liarocUploadStatus = LiaRocCst.LIAROC_UL_STATUS_WAITING ;
			int count = countFoaLiarocUpload(vo.getUnbCaseVO().getListId(),  LiaRocCst.LIAROC_UL_BIZ_SOURCE_FOA_UNB) ;
			if(count > 0) {
				liarocUploadStatus = LiaRocCst.LIAROC_UL_STATUS_SEND_DELETE ;
			}
			
			// 公會通報主檔
			LiaRocUploadSendVO info = this.generateFoaUpload(policyVO, vo);
			info.setLiaRocUploadStatus(liarocUploadStatus);
			
			int itemOrder = 1;
			// 補上順序
			List<ScFoaUnbProductVO> prods = vo.getUnbProductList();
			int index = 0;
			for (CoverageVO coverage : policyVO.gitSortCoverageList()) {
				Date applyDate = policyVO.getApplyDate();
				coverage.setApplyDate(applyDate);
				coverage.setItemOrder(itemOrder++);
				LifeProduct lifeProduct = lifeProductService.getProductByVersionId(
								coverage.getProductId().longValue(),
								coverage.getProductVersionId());
				String ageMethod = lifeProduct.getProduct().getAgeMethod();
				// calc and set entry age and entry age month for insured1
				CoverageInsuredVO insured1 = coverage.getLifeInsured1();
				long entryAge1 = 0;
				long ageMonth1 = 0;
				InsuredVO insured = insured1.getInsured();
				if (!NBUtils.isEmptyOrDummyDate(insured.getBirthDate())
								&& !NBUtils.isEmptyOrDummyDate(applyDate)) {
					entryAge1 = BizUtils.getAge(ageMethod, insured1.getInsured()
									.getBirthDate(), applyDate);
					ageMonth1 = BizUtils.getAgeMonth(ageMethod, insured1.getInsured()
									.getBirthDate(), applyDate);
				}
				insured1.setEntryAge(Integer.valueOf(String.valueOf(entryAge1)));
				insured1.setEntryAgeMonth(Integer.valueOf(String.valueOf(ageMonth1)));
			}

			for (CoverageVO coverage : policyVO.gitSortCoverageList()) {
				//RTC-250475-FOA通報滿期日有誤,因上傳charge_preiod,無法由期限表中定義出來,收件通報先執行1年期避免收件通報逾時
				if (coverage.getCoveragePeriod() == null && coverage.getCoverageYear() == null) {
					coverage.setCoverageYear(1);
					coverage.setCoveragePeriod(CodeCst.COVERAGE_PERIOD__CERTAIN_YEA);
				}
				
				try {
					coverageService.calcDate2(policyVO, coverage);	
				}catch(Exception e) {
					logger.error(ExceptionUtils.getFullStackTrace(e));
					ApplicationLogger.addLoggerData(ExceptionUtils.getFullStackTrace(e));
					//IR-254758-延遲通報表報(FOA POS收件通報,生效日與保障終止日因FOA填入資訊有限，以通報受理日作生效日，一年期作終止日通報)
					Calendar expireDate = Calendar.getInstance();
					expireDate.setTime(policyVO.getApplyDate());
					expireDate.add(Calendar.YEAR, 1);
					coverage.setExpiryDate(expireDate.getTime());
				}
				
				ApplicationLogger.addLoggerData("coverage=" + coverage.getProductId()
								+ ",expire_date=" + DateUtils.date2String(coverage.getExpiryDate(), "yyyy/MM/dd"));

				BigDecimal prem = prods.get(index++).getPremAmt();
				coverage.getCurrentPremium().setTotalPremAf(prem);
				if (prem != null && prem.compareTo(BigDecimal.ZERO) > 0) {
					String initialType = coverage.getCurrentPremium()
									.getPaymentFreq();
					if (CodeCst.CHARGE_MODE__MONTHLY.equals(initialType)) {
						coverage.getCurrentPremium().setStdPremAn(
										prem.multiply(new BigDecimal("6")));
					} else if (CodeCst.CHARGE_MODE__QTRLY.equals(initialType)) {
						coverage.getCurrentPremium().setStdPremAn(
										prem.multiply(new BigDecimal("4")));
					} else if (CodeCst.CHARGE_MODE__HALF_YEARLY
									.equals(initialType)) {
						coverage.getCurrentPremium().setStdPremAn(
										prem.multiply(new BigDecimal("2")));
					} else {
						coverage.getCurrentPremium().setStdPremAn(prem);
					}
				}
			}

			List<GenericEntityVO> newDetails = (List<GenericEntityVO>) this
							.getDetailList(policyVO, LiaRocCst.LIAROC_UL_TYPE_RECEIVE,
											true);
			info.setDataList(newDetails);

			/* 改調用 this.saveReceiveUploadData(policyVO, info);
				// 沒有明細資料
				if (newDetails.size() == 0) {
					info.setLiaRocUploadStatus(LiaRocCst.LIAROC_UL_STATUS_SEND_NO_ITEM);
					info.setRequestTransUUid("NO_ITEM");
					liaRocUploadDao.save(liaRocUploadBO);
					return;
				}
			
				for (GenericEntityVO detailVO : newDetails) {
					LiaRocUploadDetail detailBO = new LiaRocUploadDetail();
					BeanUtils.copyProperties(detailBO, detailVO);
					detailBO.setUploadListId(uploadListId);
					liaRocUploadDetailDao.save(detailBO);
				}
			 */
			Long uploadId = this.saveReceiveUploadData(policyVO, info);

			ApplicationLogger.addLoggerData("uploadId=" + uploadId);
			//}
		} catch (Exception e) {
			logger.error(ExceptionUtils.getFullStackTrace(e));
			ApplicationLogger.addLoggerData(ExceptionUtils.getFullStackTrace(e));
			ApplicationLogger.setReturnCode(ApplicationLogger.RETURN_CODE_FAIL);
			try {
				ApplicationLogger.flush();
			} catch (Exception e1) {
				logger.error(e1);
			}
			throw e ;
		} finally {
			try {
				ApplicationLogger.flush();
			} catch (Exception e) {
				logger.error(e);
			}
		}
	}

	/**
	 * <p>Description : 產生foa 公會上傳主檔資料</p>
	 * <p>Created By : Kathy.Yeh</p>
	 * <p>Create Time : 2020年09月03日</p>
	 * @param policyVO
	 * @param FoaPolicyVO
	 * @return LiaRocUploadSendVO 
	 */
	private LiaRocUploadSendVO generateFoaUpload(PolicyVO policyVO, FoaPolicyVO vo)  throws Exception{

		ApplicationLogger.setPolicyCode(policyVO.getPolicyNumber());

		// 公會通報主檔
		LiaRocUploadSendVO info = new LiaRocUploadSendVO();
		info.setBizId(vo.getUnbCaseVO().getListId());
		ApplicationLogger.addLoggerData("unbCaseId=" + vo.getUnbCaseVO().getListId());
		/** 受理ID當key **/
		info.setBizCode(policyVO.getPolicyNumber());
		info.setBizSource(LiaRocCst.LIAROC_UL_BIZ_SOURCE_FOA_UNB);
		info.setLiaRocUploadType(LiaRocCst.LIAROC_UL_TYPE_RECEIVE);
		info.setUploadId(AppContext.getCurrentUser().getUserId());
		info.setParseBean(fixedLengthWriter);
		ScFoaAcceptanceVO acceptVO = vo.getAcceptanceVO();
		info.setChannelOrgId(acceptVO.getDeliverAgentChannelOrgId());
		info.setReceiveDate(acceptVO.getAceptDate());
		List<CoverageAgentVO> agents = policyVO.gitMasterCoverage().getCoverageAgents();
		if (agents != null && agents.size() > 0) {
			info.setRegisterCode(agents.get(0).getRegisterCode());
		}
		info.setInputId(AppContext.getCurrentUser().getUserId());

		return info;
	}
	
	/**
	 * ATN-COR-UNB-BSD-006-PCR 公會資料上下傳-_PCR_v1.0 Sign-Off
	 * BR-UNB-DWN-004獲取公會通報保單
	 * <pre>a. 新契約收件通報（除旅平險外）的保單需要同時符合以下條件：
	 * i. 內部通路的保單：
	 * (1)要保書狀態=待覆核。
	 * (2)比對未通報部份進行通報：收件通報系統(AFINS)與核心系統的資料比對
	 * 【保單號碼】 +【要保書填寫日】 +【險種代號】 +【被保險人 ID】，收件通報系統(AFINS)未通報者。
	 * ※當比對相同時，視為“收件通報系統”已通報 ,核心系統就不須再通報。
	 * ii.外部通路的保單
	 * (1)要保書狀態=待覆核。
	 * (2)死亡給付無關之險種則以收件通報項目第 36 項保經代分類= 98(保經代健康險及傷害醫療險)進行通報。
	 * (3)保經代傳真件：【外部通路受理作業-新件受理】頁面「保經代傳真件」欄位=「是」之保單，
	 * 則以收件通報項目第 36 項保經代分類=97(保經代傳真件)進行通報。
	 * (4)比對未通報部份進行通報：收件通報系統(AFINS)與核心系統的資料比對
	 * 【要保書填寫日】 +【險種代號】 +【被保險人 ID】，收件通報系統(AFINS)未通報者。
	 * ※當比對相同時，視為“收件通報系統”已通報 ,核心系統就不須再通報。
	 * iii.通報注意事項_收件通報項目 27 項(欄位名稱：要保書填寫日)：
	 * (1)內部通路：以「要保書填寫日」通報。
	 * (2)外部通路：保經代傳真件(97)及健康/醫療險(98)以「受理/收件確認日期」通報；
	 * 含死亡給付險種(99)以「要保書填寫日」</pre>
	 * <p>Created By : Simon.Huang</p>
	 * <p>Create Time : 2018年1月28日</p>
	 * @param policyId
	 */
	private Long sendNBReceiveNew(Long policyId) {

		Long uploadId = null;
		//EC(網路投保=>列TODO)不報
		PolicyVO policyVO = policyDS.retrieveById(policyId, false);

		//OIU商品不需要收件通報與承保通報，且無須索引(下載)同業投保紀錄
		//PCR-243784 OIU商品自2018/08/01要收件通報與承保通報且須索引(下載)同業投保紀錄
		//delete by Kathy
		//if (policyVO.isOIU() == false) {
		//這次要傳的資料
		LiaRocUploadSendVO uploadVO = getUploadVOByPolicyId(policyId, LiaRocCst.LIAROC_UL_TYPE_RECEIVE,
						LiaRocCst.LIAROC_UL_BIZ_SOURCE_UNB);

		uploadId = this.saveReceiveUploadData(policyVO, uploadVO);
		//}
		return uploadId;
	}

	/**
	 * <pre>a. 新契約mPos收件通報（除旅平險外）的保單需要同時符合以下條件：
	 * i. 內部通路的保單：
	 * (1)要保書狀態=待覆核。
	 * (2)比對未通報部份進行通報：收件通報系統(AFINS)與核心系統的資料比對
	 * 【保單號碼】 +【要保書填寫日】 +【險種代號】 +【被保險人 ID】，收件通報系統(AFINS)未通報者。
	 * ※當比對相同時，視為“收件通報系統”已通報 ,核心系統就不須再通報。
	 * ii.外部通路的保單
	 * (1)要保書狀態=待覆核。
	 * (2)死亡給付無關之險種則以收件通報項目第 36 項保經代分類= 98(保經代健康險及傷害醫療險)進行通報。
	 * (3)保經代傳真件：【外部通路受理作業-新件受理】頁面「保經代傳真件」欄位=「是」之保單，
	 * 則以收件通報項目第 36 項保經代分類=97(保經代傳真件)進行通報。
	 * (4)比對未通報部份進行通報：收件通報系統(AFINS)與核心系統的資料比對
	 * 【要保書填寫日】 +【險種代號】 +【被保險人 ID】，收件通報系統(AFINS)未通報者。
	 * ※當比對相同時，視為“收件通報系統”已通報 ,核心系統就不須再通報。
	 * iii.通報注意事項_收件通報項目 27 項(欄位名稱：要保書填寫日)：
	 * (1)內部通路：以「要保書填寫日」通報。
	 * (2)外部通路：保經代傳真件(97)及健康/醫療險(98)以「受理/收件確認日期」通報；
	 * mPos進件不寫入死亡給付險種(99)，由AFINS傳入死亡給付險種(99)做通報</pre>
	 * <p>Created By : Kathy Yeh</p>
	 * <p>Create Time : 2018年12月22日</p>
	 * @param policyId
	 */
	private Long sendMPosReceiveNew(Long policyId) {

		Long uploadId = null;
		//EC(網路投保=>列TODO)不報
		PolicyVO policyVO = policyDS.retrieveById(policyId, false);

		//這次要傳的資料
		LiaRocUploadSendVO uploadVO = getUploadVOByPolicyId(policyId, LiaRocCst.LIAROC_UL_TYPE_RECEIVE,
						LiaRocCst.LIAROC_UL_BIZ_SOURCE_UNB);
		List<GenericEntityVO> newDetails = new ArrayList<GenericEntityVO>();
	
		for (GenericEntityVO detailVO : uploadVO.getDataList()) {
			LiaRocUploadDetail newDetailBO = new LiaRocUploadDetail();
			BeanUtils.copyProperties(newDetailBO, detailVO);
			//mPos進件之外部通路(BD,BR)保單不寫入死亡給付險種(99)，由AFINS傳入死亡給付險種(99)做通報
			if(LiaRocCst.LIAROC_RELA_TO_PH_99.equals(newDetailBO.getLiaRocRelationToPh())) {
				NBUtils.logger(this.getClass(), newDetailBO.getLiaRocRelationToPh()
						+ "->mPos ChannelType= BR/BD");
			} else {
				newDetails.add(detailVO);	
			}
		}
		uploadVO.setDataList(newDetails);
		
		uploadId = this.saveReceiveUploadData(policyVO, uploadVO);

		return uploadId;
	}
	
	
	/**
	 * @deprecated : 改sendNBReceiveNew 處理，保留此處理邏輯作之後比對用
	 */
	@Deprecated
	private void sendNBReceive(Long policyId) {
		//OIU商品不需要收件通報與承保通報，且無須索引(下載)同業投保紀錄。TGL32/VICTOR CHANG;
		//EC(網路投保=>列TODO)不報
		PolicyVO policyVO = policyService.loadPolicyByPolicyId(policyId);
		if ((policyVO.isOIU()) == false) {

			//找出最後傳送的資料
			List<LiaRocUploadDetail> detailsOld = liaRocUploadDetailDao.queryByPolicyLastUpload(policyId);
			//這次要傳的資料
			LiaRocUploadSendVO uploadVO = getUploadVOByPolicyId(policyId, LiaRocCst.LIAROC_UL_TYPE_RECEIVE, LiaRocCst.LIAROC_UL_BIZ_SOURCE_UNB);

			if (detailsOld != null && detailsOld.size() > 0) {
				//找出最後傳送的資料的主檔
				LiaRocUpload uploadOld = liaRocUploadDao.load(detailsOld.get(0).getUploadListId());
				//公會通報檔業務來源-FOA受理(UNB)
				if ((LiaRocCst.LIAROC_UL_BIZ_SOURCE_FOA_UNB).equals(uploadOld.getBizSource())) {

					// make 主檔 vo
					LiaRocUpload liaRocUploadBO = new LiaRocUpload();
					liaRocUploadBO.setPolicyId(policyId);
					liaRocUploadBO.setBizSource(LiaRocCst.LIAROC_UL_BIZ_SOURCE_UNB); // 新契約。
					liaRocUploadBO.setBizId(policyId);
					liaRocUploadBO.setBizCode(uploadVO.getBizCode());
					liaRocUploadBO.setLiaRocUploadType(LiaRocCst.LIAROC_UL_TYPE_RECEIVE);
					liaRocUploadBO.setUploadId(AppContext.getCurrentUser().getUserId());
					liaRocUploadBO.setLiaRocUploadStatus(LiaRocCst.LIAROC_UL_STATUS_WAITING);
					liaRocUploadBO.setChannelOrgId(policyVO.getChannelOrgId());
					List<CoverageAgentVO> agents = policyVO.gitMasterCoverage().gitSortCoverageAgents();
					if (agents != null && agents.size() > 0) {
						liaRocUploadBO.setRegisterCode(agents.get(0).getRegisterCode());
					}
					liaRocUploadBO.setInputId(AppContext.getCurrentUser().getUserId());
					// 儲存主檔資料
					liaRocUploadDao.save(liaRocUploadBO);
					String status = uploadOld.getLiaRocUploadStatus();

					//若原本有為收件通報系統(AFINS),依通路比對通報檔, 若為FOA受理(UNB)通報用保單號碼+要保書填寫日,險種代碼比對, 已放送就不再發送;
					if ((LiaRocCst.LIAROC_UL_STATUS_SEND).equals(status)
									|| (LiaRocCst.LIAROC_UL_STATUS_RETURN).equals(status)
									|| LiaRocCst.LIAROC_UL_STATUS_RETURN_ERROR.equals(status)) {

						// get 明細資料 vo。
						Map<String, LiaRocUploadDetail> detailMap = new HashMap<String, LiaRocUploadDetail>();
						for (LiaRocUploadDetail detail : detailsOld) {
							//  String statusEffectDate = String.valueOf(detail.getStatusEffectDate());
							String internalId = detail.getInternalId();
							String certiCode = detail.getCertiCode();
							//保單號碼+要保書填寫日,險種代碼比對
							detailMap.put(internalId + "@@" + certiCode, detail);

						}
						List<GenericEntityVO> newDetails = (List<GenericEntityVO>) this.getDetailList(policyService.loadPolicyByPolicyId(policyId), LiaRocCst.LIAROC_UL_TYPE_RECEIVE);

						for (GenericEntityVO detailVO : newDetails) {
							LiaRocUploadDetail detailBO = new LiaRocUploadDetail();
							BeanUtils.copyProperties(detailBO, detailVO);
							detailBO.setUploadListId(liaRocUploadBO.getListId());

							String indexBo = detailBO.getInternalId() + "@@" + detailBO.getCertiCode();
							LiaRocUploadDetail detail = detailMap.get(String.valueOf(indexBo));

							//要保書填寫日
							if ((detail != null) && (detail.getValidateDate()).equals(detailBO.getValidateDate())) {

								/** 已經通報 **/

								if (!detail.getBirthday().equals(detailBO.getBirthday())) {
									/** 生日不一樣要先報12再重報 **/
									LiaRocUploadDetail detailBO12 = createFixUploadDetail(detail, liaRocUploadBO.getListId(), LiaRocCst.LIAROC_POLICY_STATUS__ERROR_TERMINATION);
									LiaRocUploadDetail detailBO15 = createFixUploadDetail(detailBO, liaRocUploadBO.getListId(), LiaRocCst.LIAROC_POLICY_STATUS__FIX_ERROR);
									//12-鍵值錯誤的通報狀態變更日與最新通報的更新日相同
									detailBO12.setStatusEffectDate(detailBO15.getStatusEffectDate());
									liaRocUploadDetailDao.save(detailBO12);
									liaRocUploadDetailDao.save(detailBO15);
								} else {
									/** 只修改資料 **/
									detailBO.setListId(liaRocUploadBO.getListId());
									liaRocUploadDetailDao.save(detailBO);
								}
								/**移除Map中已處理的,剩下就是最後要刪除的保項**/
								detailMap.remove(indexBo);
							} else {
								/**尚未通報**/
								//不必通報險種則不用寫明細檔
								if (isLiaRocProduct(detailBO)) {
									liaRocUploadDetailDao.save(detailBO);
								}
							}

						}
						if (!detailMap.isEmpty()) {
							/**比對完還存在表示已被刪除要新增一筆狀態為6(契約撤銷)**/
							for (String policyCode : detailMap.keySet()) {
								LiaRocUploadDetail detailBO = new LiaRocUploadDetail();
								LiaRocUploadDetail detail = detailMap.get(policyCode);
								BeanUtils.copyProperties(detailBO, detail);
								detailBO.setLiaRocPolicyStatus(LiaRocCst.LIAROC_POLICY_STATUS__FREELOOK);
								detailBO.setUploadListId(uploadOld.getListId());
								detailBO.setListId(null);

								//不必通報險種則不用寫明細檔
								if (isLiaRocProduct(detailBO)) {
									liaRocUploadDetailDao.save(detailBO);
								}

							}
						}

					} else if (LiaRocCst.LIAROC_UL_STATUS_WAITING.equals(status)) {
						/**待通報要修正原本資料**/
						List<LiaRocUploadDetail> details = liaRocUploadDetailDao.findByUploadId(uploadOld.getListId());
						for (LiaRocUploadDetail detail : details) {
							liaRocUploadDetailDao.remove(detail);
						}
						// get 明細資料 vo。
						//#1835行
						List<GenericEntityVO> newDetails = (List<GenericEntityVO>) this.getDetailList(policyService.loadPolicyByPolicyId(policyId), LiaRocCst.LIAROC_UL_TYPE_RECEIVE);
						if (CollectionUtils.isNotEmpty(newDetails)) {
							for (GenericEntityVO detailVO : newDetails) {
								LiaRocUploadDetail detailBO = new LiaRocUploadDetail();
								BeanUtils.copyProperties(detailBO, detailVO);
								detailBO.setUploadListId(liaRocUploadBO.getListId());
								liaRocUploadDetailDao.save(detailBO);
							}
						}
					} else {
						/**通報流程失敗重新發送**/
						//TODO 雲本通報若有錯是否要註記已發送第二次
						this.saveUpload(checkLiaRocProduct(getUploadVOByPolicyId(policyId, LiaRocCst.LIAROC_UL_TYPE_RECEIVE, LiaRocCst.LIAROC_UL_BIZ_SOURCE_UNB)));
					}

				} else {
					this.saveUpload(checkLiaRocProduct(uploadVO));
				}

			} else {

				this.saveUpload(checkLiaRocProduct(uploadVO));
			}
		}
	}

	/**
	 * <p>Description : 提供給FOA受理，新契約受理通報共用存檔流程</p>
	 * <p>Created By : Simon.Huang</p>
	 * <p>Create Time : 2018年2月5日</p>
	 * @param policyVO
	 * @param uploadVO
	 */
	public Long saveReceiveUploadData(PolicyVO policyVO, LiaRocUploadSendVO uploadVO) {

		List<GenericEntityVO> fixnewDetails = new ArrayList<GenericEntityVO>(uploadVO.getDataList());

		LiaRocUpload oldLastUpload = null;
		Long policyId = policyVO.getPolicyId();
		if (policyId == null) {
			policyId = -1L;
		}
		if (CollectionUtils.isNotEmpty(uploadVO.getDataList())) {

			//記錄比對重覆保項數量
			Map<String, List<String>> sameCodeMapList = new HashMap<String, List<String>>();

			for (GenericEntityVO newDetailVO : uploadVO.getDataList()) {
				LiaRocUploadDetail newDetailBO = new LiaRocUploadDetail();
				BeanUtils.copyProperties(newDetailBO, newDetailVO);

				//不必通報險種則不用寫明細檔
				if (isLiaRocProduct(newDetailBO) == false) {
					NBUtils.logger(this.getClass(), newDetailBO.getInternalId()
									+ "->isLiaRocProduct()=false"
									+ ",LiaRocProductCategory=" + newDetailBO.getLiaRocProductCategory()
									+ ",LiaRocProductType=" + newDetailBO.getLiaRocProductType());

					fixnewDetails.remove(newDetailVO);

					continue;
				}

				boolean isUnb = true;
				if (NBUtils.in(uploadVO.getBizSource(), LiaRocCst.LIAROC_UL_BIZ_SOURCE_AFINS_POS,
								LiaRocCst.LIAROC_UL_BIZ_SOURCE_FOA_POS,
								LiaRocCst.LIAROC_UL_BIZ_SOURCE_POS)) {
					isUnb = false;
				}

				//PCR-338437 比對是否有重覆的收件通報資料不含BIZ_SOURCE_UNB的 2019/11/12 Add by Kathy
				boolean is1Key = false;
				//PCR-337603 比對保單狀態是否需要含06-撤件 2020/11/20 Add by Kathy
				boolean isLiaRocPolicyStatus06 = false; 
				//PCR-391319 通路= BDR時，讀取AFINS收件檔(T_UW_NOTINTO_RECV)取得受理編號做資料比對 2020/09/12 Add by Kathy
				String serialNum = policyVO.getSerialNum();
				//保單號與受理編號相同，判斷為無受理編號
				if (NBUtils.isBRBD(String.valueOf(policyVO.getChannelType())) 
								&& (StringUtils.isEmpty(serialNum) || NBUtils.in(policyVO.getPolicyNumber(), serialNum))) {
					List<UwNotintoRecvVO> allUwNotintoRecvList = new ArrayList<UwNotintoRecvVO>();
					allUwNotintoRecvList = uwNotintoRecvService.getListByPolicyId(policyId); //AFINS此保單被保險人ID通報記錄
					for (UwNotintoRecvVO uwNotintoRecvVO : allUwNotintoRecvList) {
						if (NBUtils.in(newDetailBO.getInternalId(), uwNotintoRecvVO.getInternalId())
								&& NBUtils.in(newDetailBO.getCertiCode(), uwNotintoRecvVO.getCeritCode())) {
							serialNum = uwNotintoRecvVO.getSerialNum();
						}
					}
				}

				//比對是否有重覆的收件通報資料
				List<LiaRocUploadDetail> lastUploadDetails = liaRocUploadDetailDao.queryLastReceiveUploadDetail(
								isUnb,
								String.valueOf(policyVO.getChannelType()),
								policyVO.getPolicyNumber(),
								newDetailBO.getCertiCode(),
								newDetailBO.getInternalId(),
								newDetailBO.getValidateDate(),
								serialNum,
								is1Key,
								isLiaRocPolicyStatus06
								);

				NBUtils.logger(this.getClass(), "queryLastReceiveUploadDetail() params{"
								+ ",certiCode=" + newDetailBO.getCertiCode()
								+ ",internalId=" + newDetailBO.getInternalId()
								+ ",validateDate=" + DateUtils.date2String(newDetailBO.getValidateDate())
								+ ",serialNum=" + serialNum 
								+ "},result_size=" + lastUploadDetails.size());
				
				//有重覆送件的記錄
				if (CollectionUtils.isNotEmpty(lastUploadDetails)) {

					LiaRocUploadDetail lastUploadDetail = lastUploadDetails.get(0);

					//查明細的主檔，ID不同才查
					if (oldLastUpload == null ||
									!lastUploadDetail.getUploadListId().equals(oldLastUpload.getListId())) {
						oldLastUpload = liaRocUploadDao.load(lastUploadDetail.getUploadListId());
					}

					
					//PCR 188901 受理編號作通報保單號
					newDetailVO.setDynamicProperty(LiaRocCst.LIAROC_POLICY_CODE, lastUploadDetail.getPolicyId());
					newDetailVO.setDynamicProperty(LiaRocCst.LIAROC_BIZ_SOURCE, oldLastUpload.getBizSource());
					newDetailVO.setDynamicProperty(LiaRocCst.LIAROC_BIZ_CODE, oldLastUpload.getBizCode());
					if (isUnb) {
						String bizCode = oldLastUpload.getBizCode();
						String bizSource = oldLastUpload.getBizSource();
						if (sameCodeMapList.get(bizSource + "-" + bizCode) == null) {
							//時間序號(biz_code=upload_policy_id 表示為時間序號)不進DB記錄通報序號
							if(lastUploadDetail.getPolicyId().equals(bizCode) == false) {
								List<String> oldPolicyCodes = this.getPolicyListByBizCode(bizSource, bizCode);
								sameCodeMapList.put(bizSource + "-" + bizCode, oldPolicyCodes);
							}
						}
					}
					//END PCR-188901

					/* 公會通報狀態-待通報
					 *  0	待通報
					 *  1	通報發送完成
					 *  2	通報回傳完成
					 *  9	沒有險種需要通報
					 *  D	註記刪除不通報
					 *  E	通報流程發生未預期的錯誤
					 *  F	ESP端回傳接收失敗
					 *  N	ESP端回傳接收失敗
					 *  P	通報資料parse有錯
					 *  Q	通報佇列中
					 *  X	保項存在於回傳偵錯報表中
					 */
					String status = oldLastUpload.getLiaRocUploadStatus();

					//若原本有為收件通報系統(AFINS),依通路比對通報檔, 若為FOA受理(UNB)通報用保單號碼+要保書填寫日,險種代碼比對, 已放送就不再發送;
					if (LiaRocCst.LIAROC_UL_STATUS_SEND.equals(status)
									|| LiaRocCst.LIAROC_UL_STATUS_RETURN.equals(status)
									|| LiaRocCst.LIAROC_UL_STATUS_RETURN_ERROR.equals(status)) {

						if (LiaRocCst.LIAROC_UL_BIZ_SOURCE_AFINS.equals(oldLastUpload.getBizSource())
										|| LiaRocCst.LIAROC_UL_BIZ_SOURCE_FOA_UNB.equals(oldLastUpload.getBizSource())
										|| LiaRocCst.LIAROC_UL_BIZ_SOURCE_AFINS_POS.equals(oldLastUpload.getBizSource())) {
							//2020/04/08 add FOA_UNB不作生日比對,由頁面生日修改時批次通報
							//IR-372064[Simon Huang]AFINS通報保項出偵錯，新契約此保項需通報(比對規則需排除出偵錯誤保項)
							if(LiaRocCst.CHECK_STATUS_UPLOAD_ERROR.equals(lastUploadDetail.getCheckStatus()) == false) {
								NBUtils.logger(this.getClass(), "Skip by AFINS,BizSource=" + oldLastUpload.getBizSource());
								//已通報此處不作通報
								//DEV-340097 增加邏輯：2：收件通報比對重覆不上傳,註解remove邏輯 
								//fixnewDetails.remove(newDetailVO);
								((LiaRocUploadDetailVO) newDetailVO).setCheckStatus(LiaRocCst.CHECK_STATUS_CANCEL_BY_AFINS);
								((LiaRocUploadDetailVO) newDetailVO).setRefUploadListId(lastUploadDetail.getListId());
							}
						} else {

							Date oldBirthday = lastUploadDetail.getBirthday();
							Date newBirthday = newDetailBO.getBirthday();

							boolean isSend12 = false;
							//IR-316003 保全若FOA保全收件通報生日為空則移除且不作12/15通報更新
							if (lastUploadDetail != null
											&& oldBirthday != null && newBirthday != null
											&& !DateUtils.truncateDay(oldBirthday).equals(
															DateUtils.truncateDay(newBirthday))) {
								isSend12 = true;
							}

							if (isSend12) {
								/** 生日不一樣要先報12再重報 **/
								/** IR-314914 新增一筆15(15-公會承保通報保單狀況 - 通報更正) */
								NBUtils.logger(this.getClass(), "Birthday is change "
												+ ",oldBirthday=" + DateUtils.date2String(oldBirthday)
												+ ",newBirthday=" + DateUtils.date2String(newBirthday));
								fixnewDetails.remove(newDetailVO);
								LiaRocUploadDetail detailBo12 = this.createFixUploadDetail(lastUploadDetail, null, LiaRocCst.LIAROC_POLICY_STATUS__ERROR_TERMINATION);
								LiaRocUploadDetail detailBo15 = this.createFixUploadDetail(newDetailBO, null, LiaRocCst.LIAROC_POLICY_STATUS__FIX_ERROR);
								LiaRocUploadDetailVO detailVO12 = new LiaRocUploadDetailVO();
								LiaRocUploadDetailVO detailVO15 = new LiaRocUploadDetailVO();
								detailBo12.copyToVO(detailVO12, Boolean.TRUE);
								detailBo15.copyToVO(detailVO15, Boolean.TRUE);
								//PCR-338437 12-鍵值錯誤與15-通報更正的保單狀況生效日期=異動生效日期(系統日期) 2019/10/25 modify by Kathy
								detailVO15.setStatusEffectDate(DateUtils.truncateDay(AppContext.getCurrentUserLocalTime()));
								fixnewDetails.add(detailVO12);
								fixnewDetails.add(detailVO15); //IR-314914
								
								//PCR 188901 受理編號作通報保單號
								detailVO12.setDynamicProperty(LiaRocCst.LIAROC_POLICY_CODE, lastUploadDetail.getPolicyId());
								detailVO12.setDynamicProperty(LiaRocCst.LIAROC_BIZ_SOURCE, oldLastUpload.getBizSource());
								detailVO12.setDynamicProperty(LiaRocCst.LIAROC_BIZ_CODE, oldLastUpload.getBizCode());
								//PCR 188901 受理編號作通報保單號
								detailVO15.setDynamicProperty(LiaRocCst.LIAROC_POLICY_CODE, lastUploadDetail.getPolicyId());
								detailVO15.setDynamicProperty(LiaRocCst.LIAROC_BIZ_SOURCE, oldLastUpload.getBizSource());
								detailVO15.setDynamicProperty(LiaRocCst.LIAROC_BIZ_CODE, oldLastUpload.getBizCode());
							} else {
								NBUtils.logger(this.getClass(), "Skip by duplicate data");
								//已通報此處不作通報
								//DEV-340097 增加邏輯：2：收件通報比對重覆不上傳,註解remove邏輯 
								//fixnewDetails.remove(newDetailVO);
								((LiaRocUploadDetailVO) newDetailVO).setCheckStatus(LiaRocCst.CHECK_STATUS_CANCEL_BY_AFINS);
								((LiaRocUploadDetailVO) newDetailVO).setRefUploadListId(lastUploadDetail.getListId());
							}
							/* sendNBReceive有此邏輯： 比對完還存在表示已被刪除要新增一筆狀態為6(契約撤銷)
							 * 但與Hebe溝通後因通報流程AFINS->FOA(BRBD)->UNB 三段收件通報,每段都是差異通報,無法系統比對出最後通報保項,不在先前通報的記錄中
							 * 若有此種情況，由人工自行上傳 06  
							 */

						}
					} else if (LiaRocCst.LIAROC_UL_STATUS_WAITING.equals(status)
									|| LiaRocCst.LIAROC_UL_STATUS_QUEUE.equals(status)) {

						//已通報此處不作通報
						NBUtils.logger(this.getClass(), "Skip by duplicate data,Status=" + status);
						//DEV-340097 增加邏輯：2：收件通報比對重覆不上傳,註解remove邏輯 
						//fixnewDetails.remove(newDetailVO);
						((LiaRocUploadDetailVO) newDetailVO).setCheckStatus(LiaRocCst.CHECK_STATUS_CANCEL_BY_AFINS);
						((LiaRocUploadDetailVO) newDetailVO).setRefUploadListId(lastUploadDetail.getListId());
					} else {

						/*通報流程失敗重新發送*/
						//TODO 原本通報若有錯是否要註記已發送第二次
					}
				}
			}

			//PCR 188901-以item_id查找保項的通報編號,並替換為真正要上傳的保項通報編號
			int countSameCode = 0;
			List<String> totalSameCodeList = new ArrayList<String>();
			for (List<String> sameCodeList : sameCodeMapList.values()) {
				for (String sameCode : sameCodeList) {
					if (totalSameCodeList.contains(sameCode) == false) {
						totalSameCodeList.add(sameCode);
					}
				}
			}

			if (NBUtils.in(uploadVO.getBizSource(), LiaRocCst.LIAROC_UL_BIZ_SOURCE_UNB)) {
				//將新契約 AFINS/FOA 未落地前的通報保單號寫入T_LIAROC_UPLOAD_COVERAGE
				Collections.sort(totalSameCodeList);
				for (String oldPolicyCode : totalSameCodeList) {
					NBUtils.logger(this.getClass(), "syncLiarocUploadCoverageUNB(" + policyId + ", -1L, " + oldPolicyCode + ")");
					this.syncLiarocUploadCoverageUNB(policyId, -1L, oldPolicyCode);
				}
			}

			for (GenericEntityVO genericEntityVO : fixnewDetails) {
				LiaRocUploadDetailVO detailVO = (LiaRocUploadDetailVO) genericEntityVO;
				String oldPolicyCode = detailVO.getDynamicProperty(LiaRocCst.LIAROC_POLICY_CODE);
				String oldBizSource = detailVO.getDynamicProperty(LiaRocCst.LIAROC_BIZ_SOURCE);
				String oldBizCode = detailVO.getDynamicProperty(LiaRocCst.LIAROC_BIZ_CODE);

				if (oldPolicyCode == null) {//比對無重覆的通報記錄需重新設定保項通報號
					if (policyId.longValue() > 0) {
						//PCR 188901 落地保單舊件以保單號,新件以受理編號(無受理編號則以保單號)
						//落地保單case有保全FOA受理收件通報,新契約收件通報,保全收件通報
						oldPolicyCode = this.getLiaRocPolicyCode(policyId, detailVO.getItemId());
						NBUtils.logger(this.getClass(), "policyID > 0 ->getLiaRocPolicyCode(" + policyId + "," + detailVO.getItemId() + ")");
					} else {
						//未落地保單case有新契約 FOA受理收件通報
						countSameCode++;
						oldPolicyCode = policyVO.getPolicyNumber() + String.format("%03d", countSameCode + totalSameCodeList.size());
					}
				} else {
					if (NBUtils.in(oldBizSource, LiaRocCst.LIAROC_UL_BIZ_SOURCE_AFINS_POS,
									LiaRocCst.LIAROC_UL_BIZ_SOURCE_FOA_POS,
									LiaRocCst.LIAROC_UL_BIZ_SOURCE_POS)) {
						//item_id 綁定未落地保項通報編號
						this.syncLiarocUploadCoveragePOS(policyId, detailVO.getItemId(), oldPolicyCode);
						NBUtils.logger(this.getClass(), "syncLiarocUploadCoveragePOS(" + policyId + ", " + detailVO.getItemId() + ", "
										+ oldPolicyCode + ")");
					} else {

						if (policyId.longValue() > 0
										&& detailVO.getItemId() != null
										&& detailVO.getItemId().longValue() > 0
										&& oldPolicyCode.length() == 17
										&& NBUtils.in(oldBizSource, LiaRocCst.LIAROC_UL_BIZ_SOURCE_AFINS)
										&& oldPolicyCode.equals(oldBizCode)) {
							//落地保單通報且保項也落地,但前次通報保單號為17碼時間序號且為AFINS通報,重新取得編號記錄在t_liaroc_upload_coverage
							oldPolicyCode = this.getLiaRocPolicyCode(policyId, detailVO.getItemId());
							NBUtils.logger(this.getClass(), "時間序重新取號 ->getLiaRocPolicyCode(" + policyId + "," + detailVO.getItemId() + ")");
						} else {
							if (NBUtils.in(uploadVO.getBizSource(), LiaRocCst.LIAROC_UL_BIZ_SOURCE_UNB)) {
								//保單落地後需同保全一樣item_id 綁定未落地保項通報編號
								this.syncLiarocUploadCoveragePOS(policyId, detailVO.getItemId(), oldPolicyCode);
								NBUtils.logger(this.getClass(), "syncLiarocUploadCoveragePOS(" + policyId + ", " + detailVO.getItemId() + ", "
												+ oldPolicyCode + ")");
							}
						}
					}
				}

				NBUtils.logger(this.getClass(), "上傳保項編號=" + oldPolicyCode);
				detailVO.setPolicyId(oldPolicyCode);
			}
			//END PCR 188901
			//＊＊＊替換有被移除及設定12-重報的上傳清單＊＊＊
			uploadVO.setDataList(fixnewDetails);
		}

		return this.saveUpload(uploadVO);
	}

	/**
	 * <p>Description : AFINS POS收件通報後保全FOA或保全收件通報呼叫
	 * 將AFINS POS,或FOA POS通報時產生的保項通報號碼與item_id綁定
	 * </p>
	 * <p>Created By : Simon.Huang</p>
	 * <p>Create Time : 2019年10月23日</p>
	 * @param policyId
	 * @param itemId
	 * @param liarocPolicyCode
	 */
	private void syncLiarocUploadCoveragePOS(Long policyId, Long itemId, String liarocPolicyCode) {
		
		if(itemId == null) {
			itemId = -1L;
		}
		
		Connection conn = null;
		CallableStatement stmt = null;
		DBean db = new DBean(true);
		try {

			db.connect();
			conn = db.getConnection();

			stmt =  conn.prepareCall("{call PKG_LS_PM_NB_LIAROC.P_SYNC_LIAROC_COVERAGE_POS(?, ?, ?)}");
			stmt.setLong(1, policyId);
			stmt.setLong(2, itemId);
			stmt.setString(3, liarocPolicyCode);
			
			stmt.execute();
		}catch (Exception e) {
			throw ExceptionFactory.parse(e);
		} finally {
			DBean.closeAll(null, stmt, db);
		}
	}

	/**
	 * <p>Description : AFINS POS收件通報後保全FOA或保全收件通報呼叫
	 * 將AFINS POS,或FOA POS通報時產生的保項通報號碼與item_id綁定
	 * </p>
	 * <p>Created By : Simon.Huang</p>
	 * <p>Create Time : 2019年10月23日</p>
	 * @param policyId
	 * @param itemId
	 * @param liarocPolicyCode
	 */
	private void syncLiarocUploadCoverageUNB(Long policyId, Long itemId, String liarocPolicyCode) {
		
		if(itemId == null) {
			itemId = -1L;
		}
		
		Connection conn = null;
		CallableStatement stmt = null;
		DBean db = new DBean(true);
		try {

			db.connect();
			conn = db.getConnection();

			stmt =  conn.prepareCall("{call PKG_LS_PM_NB_LIAROC.P_SYNC_LIAROC_COVERAGE_UNB(?, ?, ?)}");
			stmt.setLong(1, policyId);
			stmt.setLong(2, itemId);
			stmt.setString(3, liarocPolicyCode);
			
			stmt.execute();
		}catch (Exception e) {
			throw ExceptionFactory.parse(e);
		} finally {
			DBean.closeAll(null, stmt, db);
		}
	}

	@Override
	public Long sendNBRecCancel(Long policyId) {

		//RTC-223610-只要保單做拒保、延期、撤件就做收件通報06-未承保件取消
		Connection conn = null;
		CallableStatement stmt = null;
		DBean db = new DBean(true);
		try {

			db.connect();
			conn = db.getConnection();

			stmt =  conn.prepareCall("{call PKG_LS_PM_NB_LIAROC.P_NB_LIAROC_CANCEL_RECEIVE(?, ?)}");
			stmt.setLong(1, policyId);
			stmt.registerOutParameter(2,Types.DECIMAL);

			stmt.execute();
			BigDecimal uploadListId = stmt.getBigDecimal(2);

			return uploadListId.longValue();
		}catch (Exception e) {
			throw ExceptionFactory.parse(e);
		} finally {
			DBean.closeAll(null, stmt, db);
		}
	}

	//不必通報險種則不用寫明細檔
	private boolean isLiaRocProduct(LiaRocUploadDetail detail) {

		if (StringUtil.isNullOrEmpty(detail.getLiaRocProductCategory())
						|| StringUtil.isNullOrEmpty(detail.getLiaRocProductType())) {
			return false;
		} else {
			return true;
		}
	}

	//檢查不必通報險種則不用寫明細檔
	private LiaRocUploadSendVO checkLiaRocProduct(LiaRocUploadSendVO vo) {
		List<? extends GenericEntityVO> oldDetails = vo.getDataList();
		List details = new ArrayList();
		for (GenericEntityVO detailVO : oldDetails) {
			LiaRocUploadDetail detailBO = new LiaRocUploadDetail();
			BeanUtils.copyProperties(detailBO, detailVO);
			if (isLiaRocProduct(detailBO)) {
				details.add(detailVO);
			}
		}
		vo.setDataList(details);
		return vo;

	}

	/**
	 * <p>Description : 系統收到收件通報接口資料(包含新契約跟保全的資料)</p>
	 * <p>Created By : Victor Chang</p>
	 * <p>Create Time : Oct 4, 2016</p>
	 * @param policyId     
	 */
	@Override
	public Long sendAfinsNBReceive(UwNotintoRecvVO orgRecvVO) {
		
		UwNotintoRecvVO recvVO = (UwNotintoRecvVO)BeanUtils.cloneBean(orgRecvVO); 
		
		//boolean unbFlag = false;
		//找出最後傳送的資料
		//String certiCode = vo.getCeritCode();
		String internalId = recvVO.getInternalId();
		//Date applyDate = vo.getApplyDate();
		String source = recvVO.getSource();

		boolean isUnb = false;

		
		//IR-367257-調整全通路通報保項，如AFINS已通報過之保項以ID、險種代號、保單號碼/受理編號為比對條件，不再重覆通報。
		//2021-01-19 調整已落地保單且mPOS進件, 因mPOS僅通報非99, AFINS只通報mPOS 99 
		String submitChannel = "";
		//END IR-367257
		Long policyId = null;
		if("POS".equals(recvVO.getDataType())) {
			isUnb = false;
			//POS來源一定要有保單號,沒有的話下面會報錯,
			policyId = policyDao.findPolicyIdByPolicyCode(orgRecvVO.getPolicyCode()); 
			orgRecvVO.setPolicyId(policyId);
		} else {
			isUnb = true;
			//t_uw_notinto_recv.seq_no=1989750(UNB來源,只有保單號,不為待承保,視為舊件)
			//t_uw_notinto_recv.seq_no=1954667(UNB來源,有受理編號無保單號,受理編號有對應的核心保單號,此件是舊件)
			//t_uw_notinto_recv.seq_no=1983645(UNB來源,有受理編號無保單號,受理編號有對應的核心保單號,但此件是新件)
			//seq_no=1954667,1983645案例相同但無法以受理編號來決定是否為新/舊件
			//只能處理seq_no=1989750的案例,保單號已存在核心且狀態不為待承保,視為保全件
			if(StringUtils.isNotBlank(orgRecvVO.getPolicyCode())) { 	
				policyId = policyDao.findPolicyIdByPolicyCode(orgRecvVO.getPolicyCode());
				orgRecvVO.setPolicyId(policyId);
				if(policyId != null) {
					submitChannel = policyDao.findSubmitChannelByPolicyId(policyId);
					int liabilityState = policyDao.findLiabilityStatusByPolicyId(policyId);
					if(liabilityState != CodeCst.LIABILITY_STATUS__NON_VALIDATE) {
						//有效保單
						isUnb = false;
					} else {
						//待生效保單
						policyId = null;
						isUnb = true;
					}
				}
			}
		}

		String channelType = "";
		//String policyCdoe = "";
		//boolean isBRBD1 = false;
		Long channelId = null;
		boolean isPolicyCode = false;

		if (StringUtils.isNotBlank(recvVO.getPolicyCode())) {
			//AGY.STD才會有//有些BDR也會有
			isPolicyCode = true;
		}
		if (isUnb && !StringUtils.isEmpty(recvVO.getSerialNum())) {//新契約才作受理編號上傳
			//有受理編號上來，強制下面流程均使用受理編號作policyCode作通報
			isPolicyCode = true;
			recvVO.setPolicyCode(recvVO.getSerialNum());
		}

		if ("AGY".equals(recvVO.getSource())) {
			channelType = CodeCst.SALES_CHANNEL_TYPE__AGY;
			//policyCdoe = vo.getPolicyCode();
		} else if ("STD".equals(source)) {
			channelType = CodeCst.SALES_CHANNEL_TYPE__STD;
			//policyCdoe = vo.getPolicyCode();
		} else if ("BDR".equals(source)) {
			//isBRBD = true;
			channelType = CodeCst.SALES_CHANNEL_TYPE__BR;
			if (StringUtils.isNotBlank(recvVO.getCompanyNo())) {
				channelId = liaRocUploadDao.getChannelIdByCompanyNo(recvVO.getCompanyNo());
				if (channelId != null) {
					channelType = channelOrgService.load(channelId).getChannelType().toString();
				}
			}
		}

		LifeProduct lifeProduct = lifeProductService.getProductByInternalId(internalId);
		if (lifeProduct == null) {
			NBUtils.appLogger("無保項資料不作寫入 " + recvVO.getSeqNo() + "internalId=:=" + internalId);
			return null;
		}
		Long productId = lifeProduct.getProductId();
		
		// make 主檔 vo
		LiaRocUploadSendVO uploadVO = new LiaRocUploadSendVO();
		uploadVO.setBizSource(isUnb ? LiaRocCst.LIAROC_UL_BIZ_SOURCE_AFINS : LiaRocCst.LIAROC_UL_BIZ_SOURCE_AFINS_POS);
		uploadVO.setBizId(recvVO.getListId());
		if (isPolicyCode) {
			//AGY.STD才會有//有些BDR也會有
			if(StringUtils.isNotBlank(orgRecvVO.getPolicyCode())) {
				uploadVO.setBizCode(orgRecvVO.getPolicyCode());
			} else {
				uploadVO.setBizCode(recvVO.getPolicyCode());	
			}
			//只有直營通路會有policyId
			uploadVO.setPolicyId(policyId);
		} else {
			//BRBD 日期+時間+4碼序
			uploadVO.setBizCode(recvVO.getUploadPolicyNo());
		}
		uploadVO.setLiaRocUploadType(LiaRocCst.LIAROC_UL_TYPE_RECEIVE);
		uploadVO.setUploadId(AppContext.getCurrentUser().getUserId());
		uploadVO.setParseBean(fixedLengthWriter);
		uploadVO.setLiaRocUploadStatus(LiaRocCst.LIAROC_UL_STATUS_WAITING);

		uploadVO.setChannelOrgId(channelId);
		if (recvVO.getReceiveDate() != null) {
			try {
				Date receiveDate = TGLDateUtil.parseROCString("eeeMMddHHmmss", recvVO.getReceiveDate());
				uploadVO.setReceiveDate(receiveDate);
			} catch (ParseException e) {
				throw ExceptionUtil.parse(e);
			}
		}
		//PCR-243784 將AG登錄證字寫入公會主檔(t_liaroc_upload) add by Kathy
		uploadVO.setRegisterCode(recvVO.getRegisterCode());

		// setup 通報明細資料
		List<LiaRocUploadDetailVO> details = new ArrayList<LiaRocUploadDetailVO>();

		if (this.needUploadItem(lifeProduct.getProduct())
						&& StringUtils.isNotBlank(this.getLiaRocProdCateCode(productId))) {
			PolicyVO policy = new PolicyVO();
			policy.setApplyDate(recvVO.getApplyDate());
			policy.setPolicyId(policyId);
			policy.setChannelOrgId(channelId);
			policy.setChannelType(Long.parseLong(channelType));
			if (recvVO.getCurrency() != null && !StringUtil.isNullOrEmpty(recvVO.getCurrency().trim())) {
				policy.setCurrency(Integer.parseInt(recvVO.getCurrency()));
			}
			if (isPolicyCode) {
				//AGY.STD才會有
				policy.setPolicyNumber(recvVO.getPolicyCode());
				//只有直營通路會有policyId
				uploadVO.setPolicyId(policyId);
			} else {
				//BRBD 日期+時間+4碼序
				policy.setPolicyNumber(recvVO.getUploadPolicyNo());
			}

			//需補上要保人, 不然與被保人關係判斷會有誤
			//PCR-243784 取得要保人ID modify by Kathy
			PolicyHolderVO policyHolder = new PolicyHolderVO();
			policyHolder.setCertiCode(recvVO.getPhCertiCode());
			policy.setPolicyHolder(policyHolder);

			CoverageVO coverage = new CoverageVO();
			coverage.setProductId(productId.intValue());
			coverage.setProductVersionId(lifeProductService.getProductVersion(productId, recvVO.getApplyDate()).getVersionId());
			CoveragePremium prem = new CoveragePremium();
			coverage.setCurrentPremium(prem);
			//繳費年期預設為1
			coverage.setChargeYear(1);
			coverage.setApplyDate(recvVO.getApplyDate());

			if (isPolicyCode) {
				//AFINS 會傳入新增coverage item order
				int itemOrder = Integer.parseInt(recvVO.getCoverage()) ;
				if (isUnb) {
					coverage.setItemOrder(itemOrder + 1);
				} else {
					
					//POS需看以報公會最後一筆 , 若沒有報公會的資料(DC件) 需找主檔的最大排序Order
					//int maxItemOrder = liaRocUploadDao.getMaxLiarocUploadItemOrder(recvVO.getPolicyCode(), "R");
					
					//2018/09/13 FIX取現行保項最大itemOrder加上AFINS傳入的itemOrder
					int maxItemOrder = 1;
					if(policyId != null) {
						maxItemOrder = policyDao.generateItemOrderByPolicyIdNoWaiver(policyId);	
					}
					coverage.setItemOrder(maxItemOrder);//maxItemOrder已經加過1
				}
			}
			//看商品設定決定amount屬於單位, 計畫別或是保額
			String unitFlag = lifeProduct.getProduct().getUnitFlag();
			if (CodeCst.UNIT_FLAG_BY_UNIT.equals(unitFlag)) {
				prem.setUnit(new BigDecimal(recvVO.getAmount()));
			} else if (CodeCst.UNIT_FLAG_BY_BENEFIE_LEVEL.equals(unitFlag)) {
				if ("0".equals(recvVO.getAmount())) {
					//DD 說明：元/單位/計劃別(核心將0轉B)
					prem.setBenefitLevel("B");
				} else {
					prem.setBenefitLevel(recvVO.getAmount());
				}
			} else {
				prem.setSumAssured(new BigDecimal(recvVO.getAmount()));
			}

			if(StringUtils.isNotBlank(recvVO.getPrem())) {
				prem.setStdPremAf(new BigDecimal(recvVO.getPrem()));
				prem.setTotalPremAf(new BigDecimal(recvVO.getPrem()));
			}
			
			if (!StringUtil.isNullOrEmpty(recvVO.getPaidModx())) {
				int paidMod = Integer.parseInt(recvVO.getPaidModx()) - 1;
				if (paidMod > 0 && paidMod < 5) {
					coverage.setInitialChargeMode(String.valueOf(paidMod));
				} else {
					coverage.setInitialChargeMode(CodeCst.CHARGE_MODE__SINGLE);
				}
			}

			//被保人資料
			InsuredVO insured = new InsuredVO();
			insured.setCertiCode(recvVO.getCeritCode());

			insured.setName(recvVO.getName());
			if (!StringUtil.isNullOrEmpty(recvVO.getInsurantBDay())) {
				String birthday = recvVO.getInsurantBDay();
				if (birthday != null && birthday.length() == 6) {
					birthday = "0" + birthday;
				}

				insured.setBirthDate(TGLDateUtil.parseROCString(birthday
								.substring(0, 3)
								+ "/"
								+ birthday.substring(3, 5)
								+ "/" + birthday.substring(5, 7)));
			}
			CoverageInsuredVO coverInsured = new CoverageInsuredVO();
			coverInsured.setOrderId(1);
			coverInsured.setInsured(insured);
			List<CoverageInsuredVO> insureds = new ArrayList<CoverageInsuredVO>();
			insureds.add(coverInsured);
			coverage.setInsureds(insureds);

			LiaRocUploadDetailWrapperVO detailvo = generateUploadDetail(policy, coverage, LiaRocCst.LIAROC_UL_TYPE_RECEIVE, true);
			if (isPolicyCode == false) {
				//2018/06/28 Simon:未上傳PolicyCode以UploadPolicyNo作上傳policyNo,
				detailvo.setPolicyId(recvVO.getUploadPolicyNo());
			} else {
				if(policyId != null) { //POS 通報需記錄此次item_num取號
					/* PCR 188901 通報保單號調整以接口getLiarocPolicyCode(policyId,itemId)取得 */
					String liarocPolicyCode = this.getLiaRocPolicyCode(policyId, -1L);
					detailvo.setPolicyId(liarocPolicyCode);
				}
			}
			detailvo.setStatusEffectDate(recvVO.getApplyDate());
			//AFINS 進件生效日一律使用要保書填寫日
			detailvo.setValidateDate(recvVO.getApplyDate());

			detailvo.setPhCertiCode(recvVO.getPhCertiCode());
			
			//IR-367257-調整全通路通報保項，如AFINS已通報過之保項以ID、險種代號、保單號碼/受理編號為比對條件，不再重覆通報。
			//mPos進件之外部通路(BD,BR)保單不寫入死亡給付險種(99)，由AFINS傳入死亡給付險種(99)做通報 (afins排除mPOS保經97,98)
			if (CodeCst.SUBMIT_CHANNEL__MPOS.equals(submitChannel)) {
				if (NBUtils.in(LiaRocCst.LIAROC_RELA_TO_PH_97, LiaRocCst.LIAROC_RELA_TO_PH_98, detailvo.getLiaRocRelationToPh())) {
					NBUtils.logger(this.getClass(), detailvo.getLiaRocRelationToPh() + "->AFINS mPOS 不通報");
					detailvo.setCheckStatus(LiaRocCst.CHECK_STATUS_CANCEL_BY_AFINS);
					detailvo.setRefUploadListId(null);
				}
			}
	
			details.add(detailvo);
			uploadVO.setDataList(details);
			
		} else {
			uploadVO.setLiaRocUploadStatus(LiaRocCst.LIAROC_UL_STATUS_SEND_NO_ITEM);
			uploadVO.setRequestTransUUid("NO_ITEM");
		}
		
		Long uploadId = this.saveUpload(uploadVO);
		return uploadId;
	}


	//檢查明細檔人名長度不可超過6碼
	private LiaRocUploadDetail fixDetailNameLength(LiaRocUploadDetail bo) {
		String detailName = bo.getName();
		//  PCR-243784  配合OIU通報公會修改姓名欄位長度判斷方式 modify by Kathy 2018.07.30 	
		detailName = substring(detailName, 18);
		bo.setName(detailName);

		String holderName = bo.getPhName();
		//  PCR-243784  配合OIU通報公會修改姓名欄位長度判斷方式 modify by Kathy 2018.07.30
		holderName = substring(holderName, 18);
		bo.setPhName(holderName);

		return bo;

	}
	
	/**
	 * 判斷險種是否需通報(豁免跟年金險不必通報)
	 */
	@Override
	public boolean needUploadItem(CoverageVO coverage) {
		if (coverage.isWaiver()) {
			NBUtils.logger(getClass(), "needUploadItem(" + coverage.getItemId() + "," + coverage.getProductId() + "),Skip by isWaiver");
			return false;
		}

		if (lifeProductCategoryService.isAnnuity(Long.valueOf(coverage.getProductId()))) {

			NBUtils.logger(getClass(), "needUploadItem(" + coverage.getItemId() + "," + coverage.getProductId() + "),Skip by isAnnuity");
			return false;
		}

		//取得商品的公會通報-險種代碼, XX為無需通報(XGA/XGB)
		String liaRocProdCode = this.getLiaRocProdCode(coverage.getProductId().longValue());
		if (LiaRocCst.LIAROC_PORD_CODE_XX.equals(liaRocProdCode)) {
			NBUtils.logger(getClass(), "needUploadItem(" + coverage.getItemId() + "," + coverage.getProductId() + "),Skip by LIAROC_PORD_CODE_XX");
			return false;
		}

		if (coverage.getLifeInsured1() != null && coverage.getLifeInsured1().getInsured() != null) {
			InsuredVO insuredVO = coverage.getLifeInsured1().getInsured();
			if (NBUtils.isEmptyOrDummyName(insuredVO.getName())) {
				//IR-241196 comment, 2.無記名及原失效之附約資料,不需上傳公會.
				NBUtils.logger(getClass(), "needUploadItem(" + coverage.getItemId() + "," + coverage.getProductId() + "),Skip by isEmptyOrDummyName");
				return false;
			}
		}

		return true;
	}

	/**
	 * 判斷險種是否需通報(豁免跟年金險不必通報)
	 */
	@Override
	public boolean needUploadItem(ProductVO productVO) {
		if (CodeCst.YES_NO__YES.equals(productVO.getWaiver())) {
			return false;
		}
		//PCR-243784 OIU商品須要通報公會 delete by Kathy
		//if (CodeCst.YES_NO__YES.equals(productVO.getIsOiu())) {
		//	return false;
		//}
		if (lifeProductCategoryService.isAnnuity(productVO.getProductId())) {
			return false;
		}

		//取得商品的公會通報-險種代碼, XX為無需通報(XGA/XGB)
		String liaRocProdCode = this.getLiaRocProdCode(productVO.getProductId());
		if (LiaRocCst.LIAROC_PORD_CODE_XX.equals(liaRocProdCode)) {
			return false;
		}

		return true;
	}
	
	/**
	 * PCR-188901
	 * <br/>取得保項公會通報用保單號,item_id查找t_liaroc_upload_coverage
	 * <br/>1.已有通報保項號直接取用
	 * <br/>2.無通報保項項則依目前保項序號最大值+1 序編
	 * @param policyId
	 * @param itemId (若-1為AFINS POS或FOA POS傳入)
	 * @return
	 */
	public String getLiaRocPolicyCode(Long policyId, Long itemId) {
		DBean db = new DBean();
		CallableStatement stmt = null;
		String liarocPolicyCode = "";
		if (itemId == null) {
			itemId = -1L;
		}
		try {
			db.connect();
			stmt = db.getConnection().prepareCall(
							"{? = call pkg_ls_cs_liaroc_bl.f_get_item_policy_id(?, ?) }");
			stmt.registerOutParameter(1, Types.VARCHAR);
			stmt.setLong(2, policyId);
			stmt.setLong(3, itemId);
			stmt.execute();
			liarocPolicyCode = stmt.getString(1);
			return liarocPolicyCode;
		} catch (Exception ex) {
			throw ExceptionFactory.parse(ex);
		} finally {
			DBean.closeAll(null, stmt, db);
		}
	}
	
	/**
	 * <p>Description : 截取字串的bytes_size(UTF-8 3 bytes,半形英數字為1 bytes)</p>
	 * <p>Created By : Simon.Huang</p>
	 * <p>Create Time : 2018年7月30日</p>
	 * @param str
	 * @param bytesize
	 * @return
	 */
	public static String substring(String str, int bytesize) {
		if (str != null) {
			StringBuffer sbf = new StringBuffer();
			int size = 0;
			while (bytesize > size && !com.ebao.pub.util.StringUtils.isNullOrEmpty(str)) {
				String sub = str.substring(0, 1);
				str = str.substring(1);
				size += sub.getBytes(Charset.forName("UTF-8")).length;
				if (size <= bytesize) {
					sbf.append(sub);
				}
			}
			return sbf.toString();
		}
		return str;
	}

	@Override
	public LiaRocUploadVO load(Long listId) {
		LiaRocUpload bo = liaRocUploadDao.load(listId);
		LiaRocUploadVO liaRocUploadVO = new LiaRocUploadVO();
		BeanUtils.copyProperties(liaRocUploadVO, bo);
		return liaRocUploadVO;
	}


	@Override
	public void uploadLiaRocUploadStatusToQ(List<String> bizSource,
					String liarocUploadType) {
		liaRocUploadDao.updateStatusToQ(bizSource, liarocUploadType);

	}

	@Override
	public void uploadLiaRocUploadStatusTo9() {
		liaRocUploadDao.updateStatusTo9();
	}
	

	private LiaRocUploadDetail createFixUploadDetail(LiaRocUploadDetail input, Long listId, String liaRocStatus) {
		LiaRocUploadDetail detailBO = new LiaRocUploadDetail();
		BeanUtils.copyProperties(detailBO, input);
		detailBO.setLiaRocPolicyStatus(liaRocStatus);
		detailBO.setUploadListId(listId);
		detailBO.setListId(null);
		return detailBO;
	}

	private static String UNB_QUERY_BY_SAME_CODE_LIST   = "select lud.policy_id from t_liaroc_upload lu "
					+ " join t_liaroc_upload_detail lud on lu.list_id = lud.upload_list_id and lud.check_status = '1' "
					+ " where lu.biz_source = ? and lu.biz_code = ? ";

	private List<String> getPolicyListByBizCode(String bizSource, String bizCode) {

		List<String> oldPolicyCodes = new ArrayList<String>();
		DBean db = null;
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			db = new DBean();
			db.connect();
			conn = db.getConnection();
			pstmt = conn.prepareStatement(UNB_QUERY_BY_SAME_CODE_LIST);
			pstmt.setString(1, bizSource);
			pstmt.setString(2, bizCode);

			rs = pstmt.executeQuery();

			while (rs.next()) {
				String oldPolicyCode = rs.getString("POLICY_ID");
				if(oldPolicyCodes.contains(oldPolicyCode) == false) {
					oldPolicyCodes.add(oldPolicyCode);
				}
			}
        } catch (Exception e) {
            throw ExceptionFactory.parse(e);
        } finally {
            DBean.closeAll(null, pstmt, db);
        }
		return oldPolicyCodes;
	}

	/**
	 * PCR 188901:新契約承保通報作業,
	 * <br/>因於收件通報因會有AFINS以受理編號通報但新契約未輸入受理編號以保單號通報
	 * <br/>承保通報需重新以新契約是否有輸入受理編號統一所有保項的通報編號,才不會有受理編號及保單號混合通報的情形
	 * @param policyId 
	 * @param itemId
	 * @param liaRocPolicyCode  
	 * @return
	 */
	private String syncNBInforceLiaRocPolicyCode(Long policyId, Long itemId, String liaRocPolicyCode) {
		DBean db = new DBean();
		CallableStatement stmt = null;
		String liarocPolicyCode = "";
		try {
			db.connect();
			stmt = db.getConnection().prepareCall(
							"{call PKG_LS_PM_NB_LIAROC.P_SYNC_NB_INFORCE_POLICY_CODE(?, ?, ?, ?) }");
			stmt.setLong(1, policyId);
			stmt.setLong(2, itemId);
			stmt.setString(3, liaRocPolicyCode);
			stmt.registerOutParameter(4, Types.VARCHAR);
			stmt.execute();
			liarocPolicyCode = stmt.getString(4);
			return liarocPolicyCode;
		} catch (Exception ex) {
			throw ExceptionFactory.parse(ex);
		} finally {
			DBean.closeAll(null, stmt, db);
		}
	}
	
	/**
	 *  新契約UNDO 後保單全量執行通報作業：
	 *  當承保後UNDO,以該筆保單的所有保項進行承保通報06-契約撤銷
	 *  當撤銷後UNDO,以該筆保單的所有保項進行收件通報01-有效
	 * @Author  : Simon Huang
	 * @Modify Date: 2019/07/07
	 * @Param liaRocUploadType (1:收件, 2:承保)
	 */
	@Override
	public Long sendNBUndo(Long policyId, String liaRocUploadType) {

		Long uploadListId = null;
		String changeType  ="";
		//一律通報不作保項比對
		LiaRocUploadSendVO sendVO = this.getUploadVOByPolicyId(policyId,
						liaRocUploadType, LiaRocCst.LIAROC_UL_BIZ_SOURCE_UNB);

		for (GenericEntityVO genericEntityVO : sendVO.getDataList()) {
			LiaRocUploadDetailVO detailVO = (LiaRocUploadDetailVO) genericEntityVO;
			if (LiaRocCst.LIAROC_UL_TYPE_RECEIVE.equals(liaRocUploadType)) {
				
				detailVO.setLiaRocPolicyStatus(LiaRocCst.LIAROC_POLICY_STATUS__INFORCE);
				//12-撤件UNDO後通報收件01 
				changeType = LiaRocCst.LIAROC_NB_BATCH_CHANGE_TYPE_12;
			} else {
			
				detailVO.setLiaRocPolicyStatus(LiaRocCst.LIAROC_POLICY_STATUS__FREELOOK);
				//11-承保UNDO後通報承保06
				changeType = LiaRocCst.LIAROC_NB_BATCH_CHANGE_TYPE_11;
			}
		}
		uploadListId = this.saveUpload(sendVO);

		//寫入新契約通報異動檔作每日報表比對用
		liaRocUploadCommonService.recordNBBatchData(policyId, changeType);
		
		return uploadListId;
	}
	
	/**
	 *  PCR- 337606 FOA已送件，但新契約未收件之保單執行公會收件通報作業：
	 *  提供一個接口給通路(SC)在做保單號碼變更時將舊保單號碼通報06(終止)
	 *  foa 曾通報過公會01生效資料做06 終止通報
	 * @Author  : Vince Cheng
	 * @Modify Date: 2020/07/14
	 * @Param policyCode (foa 資料尚未落地只有保單號碼，無policy_id)
	 * @Param caseId (t_sc_foa_unb_case.list_id)
	 */
	@Override
	public Long sendFoaUndo(Long caseId ) {
		Long uploadListId = null;
        
		FoaPolicyVO vo = foaAcceptanceCI.findFoaPolicyByCaseId(caseId);
		PolicyVO policyVO = foaAcceptanceHelper.convert2PolicyVO(vo, false);
		ApplicationLogger.setPolicyCode(policyVO.getPolicyNumber());
 
		//先取出foa 曾通報過公會資料
		List<LiaRocUploadDetail> lastUploadDetails = liaRocUploadDetailDao.queryFoaUploadDetailByPolicyCode(caseId);
		//執行通報06-終止通報
		if (lastUploadDetails != null && lastUploadDetails.size() > 0) {
			try {
				// 公會通報主檔
				LiaRocUploadSendVO info = this.generateFoaUpload(policyVO, vo);
				info.setLiaRocUploadStatus(LiaRocCst.LIAROC_UL_STATUS_WAITING);
				
				List<LiaRocUploadDetailVO> fixnewDetails = new ArrayList<LiaRocUploadDetailVO>();
				
				for (LiaRocUploadDetail uploadDetail : lastUploadDetails) {
					LiaRocUploadDetail detailBO = new LiaRocUploadDetail();
					BeanUtils.copyProperties(detailBO, uploadDetail);
					detailBO.setLiaRocPolicyStatus(LiaRocCst.LIAROC_POLICY_STATUS__FREELOOK);
					detailBO.setStatusEffectDate(DateUtils.truncateDay(AppContext.getCurrentUserLocalTime()));
					detailBO.setListId(null);
					detailBO.setUploadListId(uploadListId);
					LiaRocUploadDetailVO detailVO06 = new LiaRocUploadDetailVO();
					detailBO.copyToVO(detailVO06, Boolean.TRUE);
					fixnewDetails.add(detailVO06);
				}

				info.setDataList(fixnewDetails);
				//寫入公會收件通報上傳主檔和明細檔
				uploadListId = this.saveUpload(info);
					
			} catch (Exception e) {
				logger.error(ExceptionUtils.getFullStackTrace(e));
				ApplicationLogger.addLoggerData(ExceptionUtils.getFullStackTrace(e));
				ApplicationLogger.setReturnCode(ApplicationLogger.RETURN_CODE_FAIL);
			} finally {
				try {
					ApplicationLogger.flush();
				} catch (Exception e) {
					logger.error(e);
				}
			}
		}
		return uploadListId;
	}
	
	/**
	 * <p>Description : 依保單資料產生公會上傳主檔(無上傳明細)</p>
	 * <p>Created By : Simon.Huang</p>
	 * <p>Create Time : 2019年10月30日</p>
	 * @param policyVO
	 * @param liaRocUploadType
	 * @param bizSource
	 * @return
	 */
	@Override
	public LiaRocUploadSendVO createLiarocUploadMaster(PolicyVO policyVO, String liaRocUploadType, String bizSource) {
		// make 主檔 vo
		LiaRocUploadSendVO uploadVO = new LiaRocUploadSendVO();
		uploadVO.setPolicyId(policyVO.getPolicyId());
		uploadVO.setBizSource(bizSource);
		uploadVO.setBizId(policyVO.getPolicyId());
		uploadVO.setBizCode(policyVO.getPolicyNumber());
		
		uploadVO.setLiaRocUploadType(liaRocUploadType);
		uploadVO.setUploadId(AppContext.getCurrentUser().getUserId());
		uploadVO.setParseBean(fixedLengthWriter);
		uploadVO.setLiaRocUploadStatus(LiaRocCst.LIAROC_UL_STATUS_WAITING);

		uploadVO.setChannelOrgId(policyVO.getChannelOrgId());
		uploadVO.setReceiveDate(policyVO.getSubmissionDate());
		List<CoverageAgentVO> agents = policyVO.gitMasterCoverage().gitSortCoverageAgents();
		if (agents != null && agents.size() > 0) {
			uploadVO.setRegisterCode(agents.get(0).getRegisterCode());
		}
		uploadVO.setInputId(AppContext.getCurrentUser().getUserId());
		return uploadVO;
	}
	
	/**
	 * <p>Description : PCR-338437-調整公會通報更正作業及比對規則<br/>
	 * 新契約-每日收件通報批次作業，依傳入保項產生通報明細檔</p>
	 * <p>Created By : Simon.Huang</p>
	 * <p>Create Time : 2019年11月22日</p>
	 * @param policy
	 * @param coverageList
	 * @param liaRocUploadType
	 * @return
	 */
	public List<LiaRocUploadDetailWrapperVO> generateNBBatchUploadDetail(PolicyVO policy, List<CoverageVO> coverageList,
					String liaRocUploadType) {
		List<LiaRocUploadDetailWrapperVO> uploadDetailList = new ArrayList<LiaRocUploadDetailWrapperVO>();
		for( CoverageVO coverage : coverageList) {
			LiaRocUploadDetailWrapperVO uploadDetailVO = generateUploadDetail(policy, coverage, liaRocUploadType, false);
			String liarocPolicyCode  = null;
			boolean isLiaRocPolicyStatus06 = true; 
			boolean isUnb = true;
			boolean is1Key = true;
			List<LiaRocUploadDetail> lastUploadDetails = liaRocUploadDetailDao.queryLastReceiveUploadDetail(
							isUnb,
							String.valueOf(policy.getChannelType()),
							policy.getPolicyNumber(),
							uploadDetailVO.getCertiCode(),
							uploadDetailVO.getInternalId(),
							uploadDetailVO.getValidateDate(),
							policy.getSerialNum(),
							is1Key,
							isLiaRocPolicyStatus06);
			//若此新增險種有曾通報過公會要以原有號碼做通報，不再重新取號 2020/11/20 Add by Kathy
			if (lastUploadDetails.size() != 0) {
				LiaRocUploadDetail lastUploadDetail = lastUploadDetails.get(0);
				liarocPolicyCode = lastUploadDetail.getPolicyId();
			} else {
				liarocPolicyCode = this.getLiaRocPolicyCode(policy.getPolicyId(), coverage.getItemId());
			}
			uploadDetailVO.setPolicyId(liarocPolicyCode);
			uploadDetailList.add(uploadDetailVO);
		}
		return uploadDetailList;
	}

	/**
	 * <p>Description : IR-353680-調整AFINS傳送保項時保項編號以A01、A02...續編。	
	 * 新契約案件如險種已由核心系統自動通報，而AFINS是保單資料已進核心之後才有通報資料時，
	 * 應增加判斷AFINS的受理編號與核心系統的保單號碼相同時，保項號碼應以最大值往後編。不應產生保單號碼+保項重覆，但險種不相同之情形。
	 * </p>
	 * <p>Created By : Simon.Huang</p>
	 * <p>Create Time : 2020年2月4日</p>
	 * @param policyId
	 * @param certiCode
	 * @param internalId
	 * @param validateDate
	 * @param liaRocPolicyCode
	 * @param itemOrder
	 * @return
	 */
	private String generateAfinsPolicyCode(Long policyId, String certiCode, String internalId, Date validateDate,
					String liaRocPolicyCode, int itemOrder,String liaRocRelationToPh) {
		
		String checkFmt = LiaRocCst.ITEM_ORDER_FMT_NORMAL;
		if (LiaRocCst.LIAROC_RELA_TO_PH_99.equals(liaRocRelationToPh)) {
			checkFmt = LiaRocCst.ITEM_ORDER_FMT_AFINS_99;
		} else if (LiaRocCst.LIAROC_RELA_TO_PH_98.equals(liaRocRelationToPh)) {
			checkFmt = LiaRocCst.ITEM_ORDER_FMT_AFINS_98;
		}

		String tmpPolicyCode = liaRocPolicyCode + String.format(checkFmt, itemOrder);
		int breakCnt = 0;
		
		//AFINS 未落地保單才作已通報保項判斷(新件保單)
		if (policyId == null || policyId.longValue() < 1) {
			List<LiaRocUploadDetail> resultList = liaRocUploadDetailDao.queryAfinsUploadDetailByCertiCode(certiCode, validateDate);

			if (resultList != null && resultList.size() > 0) {
				try {
					List<String> historyPolicyCode = NBUtils.getPropertyStringList(resultList, "policyId");
					//若上傳公會保單號已重覆需重新編號
					while (historyPolicyCode.contains(tmpPolicyCode) == true
									&& breakCnt < 50) {
						
						//增加判斷若之前公會保單號相同且險種相同，不作重新編號，並以上次通報公會保單號作通報
						for(LiaRocUploadDetail oldUploadDetail : resultList) {
							if(oldUploadDetail.getPolicyId().indexOf(liaRocPolicyCode) != -1
											&& NBUtils.in(oldUploadDetail.getInternalId(), internalId) ) {
								return oldUploadDetail.getPolicyId();
							}
						}
						breakCnt++;
						itemOrder++;
						tmpPolicyCode = liaRocPolicyCode + String.format(checkFmt, itemOrder);
					}
				} catch (Exception e) {
					//do nothing
				}
			}
		}
		return tmpPolicyCode;
	}

	/**
	 * <p>Description : FOA 會有重複送件問題，所以要檢核，第二筆壓 D</p>
	 * <p>Created By : Simon Huang</p>
	 * <p>Create Time : 2020年9月9日</p>
	 * @param bizId
	 * @param bizSource
	 * @return
	 */
	private int countFoaLiarocUpload(long bizId, String bizSource) {
		DBean db = null;
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		int result = 0;

		try {

			StringBuffer sql = new StringBuffer();

			sql.append("select count(1) count from T_LIAROC_UPLOAD t where t.biz_source = ? and t.biz_id = ? ");

			db = new DBean();
			db.connect();
			conn = db.getConnection();
			pstmt = conn.prepareStatement(sql.toString());

			pstmt.setString(1, bizSource);
			pstmt.setLong(2, bizId);

			rs = pstmt.executeQuery();

			while (rs.next()) {
				result = rs.getInt("COUNT");
			}

		} catch (Exception e) {
			throw ExceptionFactory.parse(e);
		} finally {
			DBean.closeAll(rs, pstmt, db);
		}

		return result;
	}
	
	private String getInternalIdByProuctId(int productId) {
		return CodeTable.getCodeById("V_PRODUCT_LIFE_1", String.valueOf(productId));
	}

	@Override
	public void execute(com.ebao.ls.ws.vo.RqHeader rqHeader, LiaRocUploadProcessResultRootVO vo) {
		liaRocUploadCommonService.execute(rqHeader, vo);
	}
	
	@Override
	public String getLiaRocProdCateCode(Long productI) {
		return liaRocUploadDao.getLiaRocProdCateCode(productI);
	}

	@Override
	public String getLiaRocProdCode(Long productI) {
		return liaRocUploadDao.getLiaRocProdCode(productI);
	}	
}
