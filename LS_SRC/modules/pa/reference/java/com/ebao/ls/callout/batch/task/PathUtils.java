package com.ebao.ls.callout.batch.task;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FilenameUtils;

public class PathUtils {

    public static String getCurrentPath() {
        return System.getProperty("user.dir");
    }

    public static String join(String[] path) {
        File start = null;
        if (path != null && path.length > 1) {
            for (String file : path)
                if (start == null) {
                    start = new File(file);
                } else
                    start = new File(start, file);
            return start.getAbsolutePath();
        } else if (path != null) {
            return path[0];
        }
        return null;
    }

    public static void mkdir(String path) throws IOException {
        new File(path).mkdirs();
    }

    public static String baseName(String path) {
        return FilenameUtils.getName(path);
    }

    public static void moveDirectory(String from, String to) {
        File afile = new File(from);
        for (File fileItem : afile.listFiles()) {
            File newFile = new File(PathUtils.join(new String[] { to,
                            fileItem.getName() }));
            fileItem.renameTo(newFile);
        }
    }

}
