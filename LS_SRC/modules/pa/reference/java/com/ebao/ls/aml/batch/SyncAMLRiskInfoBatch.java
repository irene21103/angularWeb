package com.ebao.ls.aml.batch;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

import com.ebao.ls.batch.BatchLogger;
import com.ebao.ls.pub.batch.impl.DataSourceWrapper;
import com.ebao.pub.batch.job.BaseUnpieceableJob;
import com.ebao.pub.batch.type.JobStatus;

public class SyncAMLRiskInfoBatch extends BaseUnpieceableJob{
	private JdbcTemplate jdbcTemplate;
	private TransactionTemplate transactionTemplate;
	protected BatchLogger batchLogger = BatchLogger.getLogger(SyncAMLRiskInfoBatch.class);
	
	@Override
	public int mainProcess() throws Exception {
		batchLogger.info("job start");
		DataSource dataSource =new DataSourceWrapper();
		DataSourceTransactionManager transactionManager = new DataSourceTransactionManager(dataSource);
        jdbcTemplate = new JdbcTemplate(transactionManager.getDataSource());
        transactionTemplate = new TransactionTemplate(transactionManager);
		
		int rc = JobStatus.EXECUTE_SUCCESS;
		
		final StringBuffer sql = new StringBuffer("");
		sql.append("MERGE INTO T_AML_RISK_INFO A ");
		sql.append("USING( ");
		sql.append("	SELECT CIN_NO, CLIENT_TYPE, RISK_LEVEL_LVW, LVW_VIEW_DATE, RISK_LEVEL, VIEW_DATE, DEC_TYPE, LOCAL_PEP, OS_PEP, NEG_NEWS, FIN_CRIME, SAN_FLAG, ML_FLAG ");
		sql.append("	FROM T_RISKLEVEL_TRANSFER_AML ");
		sql.append(") B ");
		sql.append("ON ( ");
		sql.append("	A.CERTI_CODE = REPLACE(B.CIN_NO,'_EBAO') AND ");					
		sql.append("	A.CLIENT_TYPE = B.CLIENT_TYPE  ");
		sql.append(") ");
		sql.append("WHEN MATCHED THEN UPDATE SET A.RISK_LEVEL_LVW=B.RISK_LEVEL_LVW, A.RISK_LEVEL=B.RISK_LEVEL, A.SAN_FLAG=B.SAN_FLAG, A.LOCAL_PEP=B.LOCAL_PEP, A.OS_PEP=B.OS_PEP, A.NEG_NEWS=B.NEG_NEWS, A.FIN_CRIME=B.FIN_CRIME, A.ML_FLAG=B.ML_FLAG, A.DEPT_ID=null, A.USER_ID=401, A.LVW_VIEW_DATE=TO_DATE(B.LVW_VIEW_DATE,'yyyy-MM-dd'), A.VIEW_DATE=TO_DATE(B.VIEW_DATE,'yyyy-MM-dd'), A.DEC_TYPE=B.DEC_TYPE ");
		sql.append("WHEN NOT MATCHED THEN INSERT(LIST_ID, CERTI_CODE, RISK_LEVEL_LVW, RISK_LEVEL, SAN_FLAG, LOCAL_PEP, OS_PEP, NEG_NEWS, FIN_CRIME, ML_FLAG, USER_ID, CLIENT_TYPE, LVW_VIEW_DATE, VIEW_DATE, DEC_TYPE) ");
		sql.append("	VALUES (S_AML_RISK_INFO__LIST_ID.NEXTVAL, REPLACE(B.CIN_NO,'_EBAO'), B.RISK_LEVEL_LVW, B.RISK_LEVEL, B.SAN_FLAG, B.LOCAL_PEP, B.OS_PEP, B.NEG_NEWS, B.FIN_CRIME, B.ML_FLAG, 401, B.CLIENT_TYPE, TO_DATE(B.LVW_VIEW_DATE,'yyyy-MM-dd'), TO_DATE(B.VIEW_DATE,'yyyy-MM-dd'), B.DEC_TYPE ) ");
		
		final StringBuffer sqlDelete = new StringBuffer("DELETE FROM T_RISKLEVEL_TRANSFER_AML ");

		try {
			batchLogger.info("start transfer data ");
			Object status=transactionTemplate.execute(new TransactionCallback<Object>() {
	            @Override
	            public Object doInTransaction(TransactionStatus status) {
	            	try {						
						
						jdbcTemplate.update(sql.toString());
						jdbcTemplate.update(sqlDelete.toString());
			            
	            	}catch(Exception exception) {
	            		batchLogger.error("ERROR:", exception.getMessage());
	            		exception.printStackTrace();
	            		return "fail";
	            	}
					
					return "success";
	            }
			});
			if(!status.toString().equals("success")) {
				rc = JobStatus.EXECUTE_FAILED;
			}
			batchLogger.info("batch job success");
		}catch(Exception e) {
			e.printStackTrace();
			batchLogger.error("ERROR:", e);
			rc = JobStatus.EXECUTE_FAILED;
		}		
		batchLogger.info("batch job end");
		return rc;
	}
}
