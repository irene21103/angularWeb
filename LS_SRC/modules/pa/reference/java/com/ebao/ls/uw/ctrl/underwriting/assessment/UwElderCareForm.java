package com.ebao.ls.uw.ctrl.underwriting.assessment;

import com.ebao.ls.uw.ctrl.underwriting.assessment.vo.UwElderCareTargetVO;
import com.ebao.pub.framework.GenericForm;
import com.tgl.tools.web.enums.Action;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.commons.lang3.time.DateFormatUtils;

/**
 * 高齡投保核保評估ActionForm
 *
 * @author Bruce Liao
 */
public class UwElderCareForm extends GenericForm {

	private static final long serialVersionUID = -16902404619586265L;

	private Long policyId;

    private Long underwriteId;

    private boolean isIframe = false;

    private final Map<String, String> actionEnums = new HashMap<>();

    private Action actionType;

    // 目前選擇的證號
    private String roleSel;

    // 評估對象清單
    private List<UwElderCareTargetVO> targetList = new ArrayList<>();

    // 人員-確認儲存之結果(Json String)
    private String submitData;

    public String getSubmitData() {
        return submitData;
    }

    public void setSubmitData(String submitData) {
        this.submitData = submitData;
    }

    public Action getActionType() {
        return actionType;
    }

    public void setActionType(Action actionType) {
        this.actionType = actionType;
    }

    /**
     * @return 目前選擇的證號
     */
    public String getRoleSel() {
        return roleSel;
    }

    /**
     * @param roleSel 目前選擇的證號
     */
    public void setRoleSel(String roleSel) {
        this.roleSel = roleSel;
    }

    /**
     * @return 評估對象清單
     */
    public List<UwElderCareTargetVO> getTargetList() {
        return targetList;
    }

    /**
     * @param targetList 評估對象清單
     */
    public void setTargetList(List<UwElderCareTargetVO> targetList) {
        this.targetList = targetList;
    }

    public boolean isIsIframe() {
        return isIframe;
    }

    public void setIsIframe(boolean isIframe) {
        this.isIframe = isIframe;
    }

    public Long getUnderwriteId() {
        return underwriteId;
    }

    public void setUnderwriteId(Long underwriteId) {
        this.underwriteId = underwriteId;
    }

    public Long getPolicyId() {
        return policyId;
    }

    public void setPolicyId(Long policyId) {
        this.policyId = policyId;
    }

    public Map<String, String> getActionEnums() {
        return actionEnums;
    }

    public String getTimeVerString() {
        return DateFormatUtils.format(new Date(), "yyyyMMddhhmm");
    }

    public UwElderCareForm() {
        Action[] values = Action.values();
        for (Action act : values) {
            actionEnums.put(act.name(), act.name());
        }
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }

}
