package com.ebao.ls.uw.ctrl.underwriting;

import java.math.BigDecimal;
import java.util.Date;

import com.ebao.ls.uw.ctrl.pub.UwLifeInsuredForm;

/**
 * <p>Title: GEL-UW</p>
 * <p>Description: Underwriting InsuredList Decision View Form</p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: eBaoTech</p>
 * @author jason.luo
 * @version 1.0
 * @since 08.11.2004
 */

/**
 * Underwriting InsuredList Decision View Form
 */
public class UwInsuredListDecisionViewForm extends UwLifeInsuredForm {

  /**
   * Name Of the life assured
   */
  private String lifeAssuredName = "";

  /**
   * 被保險人姓名-羅馬拼音
   */
  private String lifeAssuredRomanName = "";

  /**
   * Gender name of the life assured
   */
  private String genderName = "";
  
  /**
   * 20160810 國籍
   */
  private String nationality = "";
  
  /**
   * JOB_CLASS
   */
  private Integer jobClass;
  
  /**
   * Current Occupation ID
   */
  private Long occupCate;

  /**
   * DOB(Day Of Birthday) value of life assured
   */
  private Date dobValue = null;

  /**
   * Standard life indicator
   */
  private String stdLifeIndi = "";

  /**
   * Party ID type
   */
  private String partyIdType = "";

  /**
   * Party ID NO.
   */
  private String partyIdNO = "";

    private Long listId;

    private int entryAge;

    private BigDecimal bmi;

    private String medicalExamIndi;
    
    private String imageIndi;
    
    private String disabilityIndi;
    
    private String hoLifeSurveyIndi;
    
    /**PCR_391319 新式公會-新增被保險人受監護宣告  20201030 by Simon */
    private String guardianAncmnt ;
    
    /**
     * JOB_CATE_ID_PT
     */
    private Long jobCateIdPt;
    
    /**
     * JOB_CLASS_PT
     */
    private Integer jobClassPt;
    
    /**
     * JOB_CATE_ID_FT
     */
    private Long jobCateIdFt;
    
    /**
     * JOB_CLASS_FT
     */
    private Integer jobClassFt;
    
    
    public int getEntryAge() {
        return entryAge;
    }

    public void setEntryAge(int entryAge) {
        this.entryAge = entryAge;
    }

    /**
       * Default Constructor
       */
  public UwInsuredListDecisionViewForm() {

  }

  /**
   * Returns DOB value
   * 
   * @return  DOb value
   */
  public Date getDobValue() {
    return dobValue;
  }

  /**
   * Returns gender name
   * 
   * @return  gender name
   */
  public String getGenderName() {
    return genderName;
  }

  /**
   * Returns name of the LA
   * 
   * @return name of the LA
   */
  public String getLifeAssuredName() {
    return lifeAssuredName;
  }

  /**
   * Returns standard life indicator 1
   * 
   * @return standard life indicator 1
   */
  public String getStdLifeIndi() {
    return stdLifeIndi;
  }

  /**
   * Sets DOB value
   * 
   * @param dobValue  DOB value
   */
  public void setDobValue(Date dobValue) {
    this.dobValue = dobValue;
  }

  /**
   * Sets name of the LA
   * 
   * @param lifeAssuredName  name of the LA
   */
  public void setLifeAssuredName(String lifeAssuredName) {
    this.lifeAssuredName = lifeAssuredName;
  }

  /**
   * Sets gender name
   * 
   * @param genderName gender name
   */
  public void setGenderName(String genderName) {
    this.genderName = genderName;
  }

  /**
   * Sets standard life indicator 1
   * 
   * @param stdLifeIndi standard life indicator 1
   */
  public void setStdLifeIndi(String stdLifeIndi) {
    this.stdLifeIndi = stdLifeIndi;
  }

  /**
   * Returns party Id NO.
   * 
   * @return party Id NO.
   */
  public String getPartyIdNO() {
    return partyIdNO;
  }

  /**
   * Returns party Id type
   * 
   * @return party Id type
   */
  public String getPartyIdType() {
    return partyIdType;
  }

  /**
   * Sets party Id NO.
   * 
   * @param partyIdNO party Id NO.
   */
  public void setPartyIdNO(String partyIdNO) {
    this.partyIdNO = partyIdNO;
  }

  /**
   * Sets party Id type
   * 
   * @param partyIdType party Id type
   */
  public void setPartyIdType(String partyIdType) {
    this.partyIdType = partyIdType;
  }

    public Long getListId() {
        return listId;
    }

    public void setListId(Long listId) {
        this.listId = listId;
    }

    public BigDecimal getBmi() {
        return bmi;
    }

    public void setBmi(BigDecimal bmi) {
        this.bmi = bmi;
    }

    public String getMedicalExamIndi() {
        return medicalExamIndi;
    }

    public void setMedicalExamIndi(String medicalExamIndi) {
        this.medicalExamIndi = medicalExamIndi;
    }

	public String getImageIndi() {
		return imageIndi;
	}

	public void setImageIndi(String imageIndi) {
		this.imageIndi = imageIndi;
	}

	public String getDisabilityIndi() {
		return disabilityIndi;
	}

	public void setDisabilityIndi(String disabilityIndi) {
		this.disabilityIndi = disabilityIndi;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getHoLifeSurveyIndi() {
		return hoLifeSurveyIndi;
	}

	public void setHoLifeSurveyIndi(String hoLifeSurveyIndi) {
		this.hoLifeSurveyIndi = hoLifeSurveyIndi;
	}

	public Integer getJobClass() {
		return jobClass;
	}

	public void setJobClass(Integer jobClass) {
		this.jobClass = jobClass;
	}

	public Long getOccupCate() {
		return occupCate;
	}

	public void setOccupCate(Long occupCate) {
		this.occupCate = occupCate;
	}

	public String getGuardianAncmnt() {
		return guardianAncmnt;
	}

	public void setGuardianAncmnt(String guardianAncmnt) {
		this.guardianAncmnt = guardianAncmnt;
	}
	
	public String getLifeAssuredRomanName() {
		return lifeAssuredRomanName;
	}

	public void setLifeAssuredRomanName(String lifeAssuredRomanName) {
		this.lifeAssuredRomanName = lifeAssuredRomanName;
	}

	public Long getJobCateIdPt() {
		return jobCateIdPt;
	}

	public void setJobCateIdPt(Long jobCateIdPt) {
		this.jobCateIdPt = jobCateIdPt;
	}

	public Integer getJobClassPt() {
		return jobClassPt;
	}

	public void setJobClassPt(Integer jobClassPt) {
		this.jobClassPt = jobClassPt;
	}

	public Long getJobCateIdFt() {
		return jobCateIdFt;
	}

	public void setJobCateIdFt(Long jobCateIdFt) {
		this.jobCateIdFt = jobCateIdFt;
	}

	public Integer getJobClassFt() {
		return jobClassFt;
	}

	public void setJobClassFt(Integer jobClassFt) {
		this.jobClassFt = jobClassFt;
	}
	
}