package com.ebao.ls.liaRoc.ci;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.ConnectException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.beanutils.BeanPredicate;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.collections.PredicateUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.criterion.Restrictions;

import com.ebao.foundation.module.para.Para;
import com.ebao.foundation.module.web.action.ExceptionInfoUtils;
import com.ebao.ls.liaRoc.LiaRocCst;
import com.ebao.ls.liaRoc.bo.LiaRocDownload2020;
import com.ebao.ls.liaRoc.bo.LiaRocDownload2020VO;
import com.ebao.ls.liaRoc.bo.LiaRocDownloadDetail2020VO;
import com.ebao.ls.liaRoc.bo.LiaRocResidentIdVO;
import com.ebao.ls.liaRoc.data.LiaRocDownload2020Dao;
import com.ebao.ls.liaRoc.ds.LiaRocDownload2020Service;
import com.ebao.ls.liaRoc.ds.LiaRocResidentIdService;
import com.ebao.ls.pa.nb.bs.rule.vo.InsuredVO;
import com.ebao.ls.pa.nb.bs.rule.vo.ProposalVO;
import com.ebao.ls.pa.nb.ctrl.detailreg.DetailRegHelper;
import com.ebao.ls.pa.nb.rule.ValidatorUtils;
import com.ebao.ls.pa.nb.util.NBUtils;
import com.ebao.ls.pa.nb.util.NumericUtils;
import com.ebao.ls.pa.pub.bs.InsuredService;
import com.ebao.ls.pa.pub.bs.PolicyBankAuthService;
import com.ebao.ls.pa.pub.bs.PolicyService;
import com.ebao.ls.pa.pub.vo.PolicyBankAuthVO;
import com.ebao.ls.pa.pub.vo.PolicyVO;
import com.ebao.ls.pa.pub.vo.UnbRelationRole;
import com.ebao.ls.pub.CodeCst;
import com.ebao.ls.pub.util.TGLDateUtil;
import com.ebao.ls.riskAggregation.data.bo.AggregationResult;
import com.ebao.ls.rstf.ci.liaroc20.Liaroc20CI;
import com.ebao.ls.rstf.ci.liaroc20.Liaroc20Cst;
import com.ebao.ls.rstf.ci.liaroc20.vo.external.Liaroc20DownloadResponseData;
import com.ebao.ls.rstf.ci.liaroc20.vo.internal.Liaroc20DownloadRqDataVO;
import com.ebao.ls.rstf.ci.liaroc20.vo.internal.Liaroc20DownloadRsDataRowVO;
import com.ebao.ls.rstf.ci.liaroc20.vo.internal.Liaroc20DownloadRsDataVO;
import com.ebao.ls.rstf.ci.liaroc20.vo.internal.Liaroc20DownloadRsHeadVO;
import com.ebao.ls.rstf.ci.liaroc20.vo.internal.Liaroc20ResidentRsDataVO;
import com.ebao.ls.rstf.ci.liaroc20.vo.internal.Liaroc20ResidentRsVO;
import com.ebao.ls.rstf.util.RstfClientUtils;
import com.ebao.ls.uw.ds.vo.LiaRocLiability17DetailVO;
import com.ebao.ls.uw.ds.vo.LiaRocPaidInjuryMedicalDetailVO;
import com.ebao.ls.ws.enums.WSReturnCode;
import com.ebao.ls.ws.exception.WSCIException;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.framework.GenericException;
import com.ebao.pub.i18n.lang.StringResource;
import com.ebao.pub.util.DateUtils;
import com.ibm.icu.util.Calendar;

public class LiaRocDownload2020CIImpl implements LiaRocDownload2020CI {

	@Resource(name = Liaroc20CI.BEAN_DEFAULT)
	protected Liaroc20CI liaroc20CI;
	
	@Resource(name = LiaRocDownload2020Service.BEAN_DEFAULT)
	private LiaRocDownload2020Service liaRocDownload2020Service;

	@Resource(name = com.ebao.ls.pa.nb.bs.rule.CoverageInsuredService.BEAN_DEFAULT)
	public com.ebao.ls.pa.nb.bs.rule.CoverageInsuredService coverageInsuredService;

	@Resource(name = InsuredService.BEAN_DEFAULT)
	public InsuredService insuredService;

	@Resource(name = LiaRocDownload2020Dao.BEAN_DEFAULT)
	private LiaRocDownload2020Dao<LiaRocDownload2020> liaRocDownload2020Dao;

	@Resource(name = PolicyService.BEAN_DEFAULT)
	private PolicyService policyService;

	@Resource(name = DetailRegHelper.BEAN_DEFAULT)
	protected DetailRegHelper detailRegHelper;

	@Resource(name = PolicyBankAuthService.BEAN_DEFAULT)
	private PolicyBankAuthService policyBankAuthService;

	@Resource(name = LiaRocResidentIdService.BEAN_DEFAULT)
	private LiaRocResidentIdService liaRocResidentIdService;
	
	
	Logger logger = Logger.getLogger(getClass());

	@Override
	public List<LiaRocDownload2020VO> download(String moduleName, String roleType, Long policyId,
			List<String[]> sendIdnos) throws GenericException {

		List<LiaRocDownload2020VO> downloadList = _download(moduleName, roleType,
				Liaroc20Cst.DOWNLOAD_KIND_RMS_CHECK.getCode(), policyId, sendIdnos);

		return downloadList;
	}

	@Override
	public List<LiaRocDownload2020VO> downloadTerminateCheck(String moduleName, String roleType, Long policyId,
			List<String[]> sendIdnos) throws GenericException {

		List<LiaRocDownload2020VO> downloadList = _download(moduleName, roleType,
				Liaroc20Cst.DOWNLOAD_KIND_TERMINATE_CHECK.getCode(), policyId, sendIdnos);

		return downloadList;
	}

	private List<LiaRocDownload2020VO> _download(String moduleName, String roleType, String useKind, Long policyId,
			List<String[]> sendIdnos) throws GenericException {
		
		String liarocDownloadFlag = Para.getParaValue(com.ebao.ls.pub.CodeCstTgl.OPQ_LIAROC_DOWNLOAD_FLAG);
		
		//String certiCode, String oldForeignerId
		if (sendIdnos == null || sendIdnos.size() == 0) {
			new IllegalArgumentException("ids should not be null or empty");
		}

		List<Liaroc20DownloadResponseData> rsList = new ArrayList<Liaroc20DownloadResponseData>();
		List<Liaroc20DownloadRqDataVO> rqDataList = new ArrayList<Liaroc20DownloadRqDataVO>();

		//依傳入查詢ids產生download及detail list
		List<LiaRocDownload2020VO> downloadList = new ArrayList<LiaRocDownload2020VO>();

		Set<String> totalIdNo = new HashSet<String>();
		for (String[] idnos : sendIdnos) {
			for (String idno : idnos) {
				if (NBUtils.isEmptyOrDummyCertiCode(idno) == false && totalIdNo.contains(idno) == false) {
					Liaroc20DownloadRqDataVO rqVO = new Liaroc20DownloadRqDataVO();
					if (Liaroc20Cst.DOWNLOAD_ROLE_HOLDER.getCode().equals(roleType)) {
						rqVO.setKeyaskidno(idno);
					} else if (Liaroc20Cst.DOWNLOAD_ROLE_INSURED.getCode().equals(roleType)) {
						rqVO.setKeyidno(idno);
					}
					rqDataList.add(rqVO);
					totalIdNo.add(idno);
				}
			}
		
			// 不執行公會下載
			if (CodeCst.YES_NO__YES.equals(liarocDownloadFlag) == false) {
				Long downloadId = null;
				if (useKind.equals(Liaroc20Cst.DOWNLOAD_KIND_TERMINATE_CHECK.getCode())) {
					downloadId = liaRocDownload2020Dao.getLastTerminateDownloadListId(policyId, idnos[0], roleType);
				} else {
					downloadId = liaRocDownload2020Dao.getLastDownloadListId(policyId, idnos[0]);
				}
				LiaRocDownload2020VO liaRocDownload2020VO = null;
				if (downloadId != null) {
					liaRocDownload2020VO = liaRocDownload2020Service.load(downloadId);
				}

				if (liaRocDownload2020VO == null) {
					liaRocDownload2020VO = new LiaRocDownload2020VO();
					liaRocDownload2020VO.setCertiCode(idnos[0]);
					liaRocDownload2020VO.setOldForeignerId("");
					liaRocDownload2020VO.setRequestTime(new Date());
					liaRocDownload2020VO.setResponseTime(new Date());
					liaRocDownload2020VO.setDownloadModule(moduleName);
					liaRocDownload2020VO.setLiarocDownloadStatus(LiaRocCst.LIAROC_DL_STATUS_SUCCESS);
					liaRocDownload2020VO.setPolicyId(policyId);
					liaRocDownload2020VO.setRoleType(roleType);
					liaRocDownload2020VO.setUsedKind(useKind);
				}
				downloadList.add(liaRocDownload2020VO);
			}
		}

		if (rqDataList == null || rqDataList.size() == 0) {
			new IllegalArgumentException("rqDataList should not be null or empty");
		}

		// 不執行公會下載
		if (CodeCst.YES_NO__YES.equals(liarocDownloadFlag) == false) {
			if("E".equals(liarocDownloadFlag)) {
				return null; //模擬ERROR
			} else {
				return downloadList ;	
			}
		}
		
		String rqUid = RstfClientUtils.genRqUid(Liaroc20Cst.TF_DOWNLOAD.getCode(), Thread.currentThread().getId());
		boolean isSuccessful = false;
		Date requestTime = null;
		Date responseTime = null;
		try {

			requestTime = new Date();

			rsList = liaroc20CI.doLiaroc20DownloadAll(rqUid, moduleName, Liaroc20Cst.PT_NEW.getCode(), rqDataList,
					Liaroc20Cst.TM_YST.getCode());

			isSuccessful = true;
			responseTime = new Date();

			logger.info("doLiaroc20DownloadAll cost=" + (responseTime.getTime() - requestTime.getTime()));
		} catch (Exception e) {
			// 呼叫發生錯誤
			isSuccessful = false;
			if (e instanceof ConnectException) {
				logger.error(e.getMessage());
			} else {
				logger.error(ExceptionInfoUtils.getExceptionMsg(e.getCause()));
			}

		}

		
		for (String[] idnos : sendIdnos) {
			String certiCode = idnos[0];
			String oldForeignerId = idnos.length > 1 ? idnos[1] : null;

			List<Liaroc20DownloadRsDataRowVO> rsDataRows = new ArrayList<Liaroc20DownloadRsDataRowVO>();
			List<LiaRocDownloadDetail2020VO> detailList = new ArrayList<LiaRocDownloadDetail2020VO>();

			if (rsList != null) {
				for (Liaroc20DownloadResponseData responseData : rsList) {

					for (Liaroc20DownloadRsDataVO rsVO : responseData.getData()) {
						//rsList包含收件(當前身份證號，舊式身份證號) +承保(當前身份證號，舊式身份證號)四種組合
						//需全量迴圈判斷
						if (Liaroc20Cst.DOWNLOAD_ROLE_HOLDER.getCode().equals(roleType)
								&& NBUtils.in(rsVO.getKeyaskidno(), certiCode, oldForeignerId)) {

							rsDataRows.addAll(rsVO.getRow());
						} else if (Liaroc20Cst.DOWNLOAD_ROLE_INSURED.getCode().equals(roleType)
								&& NBUtils.in(rsVO.getKeyidno(), certiCode, oldForeignerId)) {

							rsDataRows.addAll(rsVO.getRow());
						}
					}
					
					Liaroc20DownloadRsHeadVO head = responseData.getHead();
					if("00".equals(head.getRtncode()) == false) {
						isSuccessful = false;
						logger.error(NBUtils.getMaskCerticode(certiCode) + " 新公會下載失敗："+ head.getRtnmsg());
					}
				}
			}

			for (Liaroc20DownloadRsDataRowVO rsDataVO : rsDataRows) {
				detailList.add(convertDownloadDetail2020VO(rsDataVO));
			}

			LiaRocDownload2020VO liaRocDownload2020VO = new LiaRocDownload2020VO();
			liaRocDownload2020VO.setCertiCode(certiCode);
			liaRocDownload2020VO.setOldForeignerId(oldForeignerId);
			liaRocDownload2020VO.setRequestTime(requestTime);
			liaRocDownload2020VO.setResponseTime(responseTime);
			liaRocDownload2020VO.setDownloadModule(moduleName);
			
			if (isSuccessful) {
				liaRocDownload2020VO.setLiarocDownloadStatus(LiaRocCst.LIAROC_DL_STATUS_SUCCESS);
			} else {
				liaRocDownload2020VO.setLiarocDownloadStatus(LiaRocCst.LIAROC_DL_STATUS_ERROR);
			}
			if (detailList.size() > 0) {
				liaRocDownload2020VO.setNoDataFlag(CodeCst.YES_NO__NO);
				liaRocDownload2020VO.setDownloadDetails(detailList);
			} else {
				liaRocDownload2020VO.setNoDataFlag(CodeCst.YES_NO__YES);
				liaRocDownload2020VO.setDownloadDetails(new ArrayList<LiaRocDownloadDetail2020VO>());
			}

			liaRocDownload2020VO.setPolicyId(policyId);
			liaRocDownload2020VO.setRoleType(roleType);
			liaRocDownload2020VO.setUsedKind(useKind);

			if(Liaroc20Cst.DOWNLOAD_KIND_TGL_FIXED_UPLOAD.getCode().equals(useKind)) {
				//公會下載給更正通報使用(不落地)
			} else {
				liaRocDownload2020Service.save(liaRocDownload2020VO, true);	
			}
	
			downloadList.add(liaRocDownload2020VO);
		}

		return downloadList;
	}

	@Override
	public boolean hasLiaroc20(Long policyId) {

		boolean hasLiaroc20 = false;

		int countLiaroc20 = liaRocDownload2020Dao.countPolicyDownloadCount(policyId);
		if (countLiaroc20 > 0) {
			hasLiaroc20 = true;
		}

		return hasLiaroc20;
	}


	@Override
	public boolean hasLiaroc20(String certiCode) {

		boolean hasLiaroc20 = false;

		int countLiaroc20 = liaRocDownload2020Dao.countCertiCodeDownloadCount(certiCode);
		if (countLiaroc20 > 0) {
			hasLiaroc20 = true;
		}

		return hasLiaroc20;
	}
	
	private LiaRocDownloadDetail2020VO convertDownloadDetail2020VO(Liaroc20DownloadRsDataRowVO rsDataVO) {
		LiaRocDownloadDetail2020VO downloadDetailVO = new LiaRocDownloadDetail2020VO();

		downloadDetailVO.setLiarocType(rsDataVO.getCmptype());

		/** 公司代號. */
		downloadDetailVO.setLiarocCompanyCode(rsDataVO.getCmpno());

		/** 被保險人姓名. */
		downloadDetailVO.setName(rsDataVO.getName());

		/** 被保險人身分證號碼. */
		downloadDetailVO.setCertiCode(rsDataVO.getIdno());

		/** 被保險人出生日期. 8碼民國年月，例：01090101 */
		downloadDetailVO.setBirthday(convertTWDate(rsDataVO.getBirdate()));

		/** 被保險人性別. */
		downloadDetailVO.setGender(rsDataVO.getSex());

		/** 主約保單號碼. */
		downloadDetailVO.setPolicyCode(rsDataVO.getInsnom());

		/** 保單號碼. */
		downloadDetailVO.setLiarocPolicyCode(rsDataVO.getInsno());

		/** 來源別. 1:OIU保單，預設為０ */
		downloadDetailVO.setSourceType(rsDataVO.getOrigin());

		/** 銷售通路別. */
		downloadDetailVO.setSalesChannel(rsDataVO.getChannel());

		/** 20201011調整長度 商品代碼. */
		if(rsDataVO.getPrdcode() != null && rsDataVO.getPrdcode().length() > 20) {
			downloadDetailVO.setInternalId(rsDataVO.getPrdcode().substring(0, 20));
		} else {
			downloadDetailVO.setInternalId(rsDataVO.getPrdcode());	
		}
		

		/** 保單分類. */
		downloadDetailVO.setLiarocPolicyType(rsDataVO.getInsclass());

		/** 險種分類. */
		downloadDetailVO.setLiarocProductCategory(rsDataVO.getInskind());

		/** 險種. */
		downloadDetailVO.setLiarocProductType(rsDataVO.getInsitem());

		/** 公、自費件. */
		downloadDetailVO.setPayType(rsDataVO.getPaytype());

		/** 保險金額(申請理賠金額). / 給付項目(身故). */
		downloadDetailVO.setLiability01(convertLong(rsDataVO.getItema()));

		/** 給付項目(完全失能或最高級失能). */
		downloadDetailVO.setLiability02(convertLong(rsDataVO.getItemb()));

		/** 給付項目(失能扶助金). */
		downloadDetailVO.setLiability03(convertLong(rsDataVO.getItemc()));

		/** 給付項目(特定事故保險金). */
		downloadDetailVO.setLiability04(convertLong(rsDataVO.getItemd()));

		/** 給付項目(初次罹患). */
		// @Field(length = 10, minOccurs = 1)
		downloadDetailVO.setLiability05(convertLong(rsDataVO.getIteme()));

		/** 給付項目(醫療限額). */
		downloadDetailVO.setLiability06(convertLong(rsDataVO.getItemf()));

		/** 給付項目(醫療限額自負額). */
		downloadDetailVO.setLiability07(convertLong(rsDataVO.getItemg()));

		/** 給付項目(日額). */
		downloadDetailVO.setLiability08(convertLong(rsDataVO.getItemh()));

		/** 給付項目(住院手術). */
		downloadDetailVO.setLiability09(convertLong(rsDataVO.getItemi()));

		/** 給付項目(門診手術). */
		downloadDetailVO.setLiability10(convertLong(rsDataVO.getItemj()));

		/** 給付項目(門診). */
		downloadDetailVO.setLiability11(convertLong(rsDataVO.getItemk()));

		/** 給付項目(重大疾病(含特定傷病)). */
		downloadDetailVO.setLiability12(convertLong(rsDataVO.getIteml()));

		/** 給付項目(重大燒燙傷). */
		downloadDetailVO.setLiability13(convertLong(rsDataVO.getItemm()));

		/** 給付項目(癌症療養). */
		downloadDetailVO.setLiability14(convertLong(rsDataVO.getItemn()));

		/** 給付項目(出院療養). */
		downloadDetailVO.setLiability15(convertLong(rsDataVO.getItemo()));

		/** 給付項目(喪失工作能力). */
		downloadDetailVO.setLiability16(convertLong(rsDataVO.getItemp()));

		/** 給付項目(喪葬費用). */
		downloadDetailVO.setLiability17(convertLong(rsDataVO.getItemq()));

		/** 給付項目(醫療險自付額). */
		downloadDetailVO.setLiability18(convertLong(rsDataVO.getItemr()));

		/** 給付項目(分期給付). */
		downloadDetailVO.setLiability19(convertLong(rsDataVO.getItems()));

		/** 契約生效日期. 8碼民國年，例01090101 */
		/** 契約生效時分. 4碼時分，旅平險專用 */
		downloadDetailVO.setValidateDate(convertTWDate(rsDataVO.getValdate()));
		downloadDetailVO.setValidateTime(rsDataVO.getValtime());
		/** 契約滿期日期. 8碼民國年，例01090101 */
		/** 契約滿期時分. 4碼時分，旅平險專用 */
		downloadDetailVO.setExpiredDate(convertTWDate(rsDataVO.getOvrdate()));
		downloadDetailVO.setExpiredTime(rsDataVO.getOvrtime());
		/** 保費. */
		downloadDetailVO.setPrem(convertLong(rsDataVO.getPrm()));

		/** 保費繳別. */
		downloadDetailVO.setLiarocChargeMode(rsDataVO.getBamttype());

		/** 保費繳費年期. */
		downloadDetailVO.setLiarocChargeYear(convertLong(rsDataVO.getPrmyears()));

		/** 保單狀況. */
		downloadDetailVO.setLiarocPolicyStatus(rsDataVO.getCon());

		/** 受理理賠日/ 保單狀況生效日期. 8碼民國年，例01090101 */
		/** 保單狀況生效時分. 4碼時分，旅平險專用 */
		downloadDetailVO.setStatusEffectDate(convertTWDate(rsDataVO.getCondate()));
		downloadDetailVO.setStatusEffectTime(rsDataVO.getContime());
		/** 要保人姓名. */
		downloadDetailVO.setPhName(rsDataVO.getAskname());

		/** 要保人身分證號碼. */
		downloadDetailVO.setPhCertiCode(rsDataVO.getAskidno());

		/** 要保人出生日期. 8碼民國年，例01090101 */
		downloadDetailVO.setPhBirthday(convertTWDate(rsDataVO.getAskbirdate()));

		/** 要保人與被保險人關係. */
		downloadDetailVO.setLiarocRelationToPh(rsDataVO.getAsktype());

		/** 要保書填寫日期(收件) OR 要保日期(一0七條理賠). 8碼民國年，例01090101 */
		downloadDetailVO.setApplyDate(convertTWDate(rsDataVO.getFilldate()));
		/** 保經代分類. */
		downloadDetailVO.setBrbdType(rsDataVO.getBroktype());

		System.out.println("公會下載 " + downloadDetailVO.getLiarocPolicyCode() + "/要保書填寫日:" + rsDataVO.getFilldate()
		  + "/要保書填寫日:" + downloadDetailVO.getApplyDate()
		  + "/保經代分類:" + rsDataVO.getBroktype()) ;
		/** 通報時間 0110-02-03 17:49:54*/
		String adddate = StringUtils.trimToEmpty(rsDataVO.getAdddate());
		adddate = adddate.replaceAll("-", "").replaceAll(":", "");
		String[] datetimes = adddate.split(" ");
		String date = datetimes.length > 0 ? datetimes[0] : "";
		String time = datetimes.length > 1 ? datetimes[1] : "";
		if(StringUtils.isNotEmpty(date)) {
			downloadDetailVO.setSendDate(convertTWDate(date, time));
				
		}
		
		return downloadDetailVO;
	}

	/**
	 * 
	 * @param itema
	 * @return
	 */
	private Long convertLong(String itema) {
		Long prem = NumericUtils.parseLong(itema);
		return prem;
	}

	private Date convertTWDate(String date) {
		return convertTWDate(date, null);
	}

	/**
	 * 轉換01390101字串為date object
	 * @param birdate
	 * @return
	 */
	private Date convertTWDate(String dateStr, String timeStr) {
		Date dateObj = null;
		if (StringUtils.isNotEmpty(dateStr)) {
			Calendar calendar = Calendar.getInstance();
			try {
				dateObj = TGLDateUtil.parseROCString("eeeeMMdd", dateStr);
				calendar.setTime(dateObj);
				if (dateStr != null && StringUtils.isNotEmpty(timeStr) && timeStr.length() >= 4) {
					calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(timeStr.substring(0, 2)));
					calendar.set(Calendar.MINUTE, Integer.parseInt(timeStr.substring(2, 4)));
				}

				dateObj = calendar.getTime();
			} catch (ParseException e) {
				logger.error(ExceptionInfoUtils.getExceptionMsg(e));
			}
		}

		return dateObj;
	}

	/**
	 * <p>Description : 新式公會(V2020)下載開始日
	 * </p>
	 * <p>Created By : Simon Huang</p>
	 * <p>Create Time : 2021年04月09日</p>
	 * @return  boolean( true 執行新式公會下載)
	 */
	public boolean isLiaRocV2020DownloadDate() {
		// 新式公會通報下載開始日期 2021/07/01
		String liaRocV2020DownloadDate = Para.getParaValue(LiaRocCst.PARAM_LIAROC_V2020_DOWNLOAD_DATE);
		String currentDateStr = DateUtils.date2String(new Date(), "yyyyMMdd");
		boolean isLiaRocV2020DownloadDate = false;
		// 執行日大於新式公會通報下載日期(2021/07/01)，要執行新式公會下載
		if (currentDateStr.compareTo(liaRocV2020DownloadDate) >= 0) {
			isLiaRocV2020DownloadDate = true;
		}
		return isLiaRocV2020DownloadDate;
	}

	/**
	 * <p>Description : 查詢保單公會下載次數</p>
	 * <p>Created By : simon.huang</p>
	 * <p>Create Time : Oct 20, 2017</p>
	 * @param policyId
	 * @return
	 */
	public int countPolicyDownloadCount(Long policyId) {
		int count = liaRocDownload2020Dao.countPolicyDownloadCount(policyId);
		return count;
	}

	@Override
	public List<AggregationResult> calcRA306(ProposalVO proposal) {
		return this.calcRiskAggrResult(LiaRocCst.RISK_AGGR_CALC_RA306, proposal, null);
	}

	@Override
	public List<AggregationResult> calcRA319(ProposalVO proposal) {
		return this.calcRiskAggrResult(LiaRocCst.RISK_AGGR_CALC_RA319, proposal, null);
	}
	
	@Override
	public List<AggregationResult> calcRA319_2(ProposalVO proposal) {
		return this.calcRiskAggrResult(LiaRocCst.RISK_AGGR_CALC_RA319_2, proposal, null);
	}

	@Override
	public List<AggregationResult> calcRA325(ProposalVO proposal) {
		return this.calcRiskAggrResult(LiaRocCst.RISK_AGGR_CALC_RA325, proposal, null);
	}

	@Override
	public List<AggregationResult> calcRA327(ProposalVO proposal) {
		return this.calcRiskAggrResult(LiaRocCst.RISK_AGGR_CALC_RA327, proposal, null);
	}

	@Override
	public List<AggregationResult> calcRA330(ProposalVO proposal) {
		return this.calcRiskAggrResult(LiaRocCst.RISK_AGGR_CALC_RA330, proposal, null);
	}

	@Override
	public List<AggregationResult> calcRA346(ProposalVO proposal) {
		return this.calcRiskAggrResult(LiaRocCst.RISK_AGGR_CALC_RA346, proposal, null);
	}

	@Override
	public List<AggregationResult> calcRA369(ProposalVO proposal, Date dateBfEffectiveDate) {
		return this.calcRiskAggrResult(LiaRocCst.RISK_AGGR_CALC_RA369, proposal, dateBfEffectiveDate);
	}

	@Override
	public List<AggregationResult> calcRA373(ProposalVO proposal) {
		return this.calcRiskAggrResult(LiaRocCst.RISK_AGGR_CALC_RA373, proposal, null);
	}

	@Override
	public List<AggregationResult> calcRA384(ProposalVO proposal) {
		return this.calcRiskAggrResult(LiaRocCst.RISK_AGGR_CALC_RA384, proposal, null);
	}

	@Override
	public List<AggregationResult> calcRA390(ProposalVO proposal) {
		return this.calcRiskAggrResult(LiaRocCst.RISK_AGGR_CALC_RA390, proposal, null);
	}

	@Override
	public List<AggregationResult> calcRA393(ProposalVO proposal) {
		return this.calcRiskAggrResult(LiaRocCst.RISK_AGGR_CALC_RA393, proposal, null);
	}	
	
	@Override
	public List<AggregationResult> calcRA395(ProposalVO proposal) {
		return this.calcRiskAggrResult(LiaRocCst.RISK_AGGR_CALC_RA395, proposal, null);
	}	
	
	@Override
	public List<AggregationResult> calcRA396(ProposalVO proposal) {
		return this.calcRiskAggrResult(LiaRocCst.RISK_AGGR_CALC_RA396, proposal, null);
	}	
	
	@Override
	public List<AggregationResult> calcRA397(ProposalVO proposal) {
		return this.calcRiskAggrResult(LiaRocCst.RISK_AGGR_CALC_RA397, proposal, null);
	}	

	@Override
	public List<AggregationResult> calcRA398(ProposalVO proposal) {
		return this.calcRiskAggrResult(LiaRocCst.RISK_AGGR_CALC_RA398, proposal, null);
	}	

	@Override
	public List<AggregationResult> calcRA399(ProposalVO proposal) {
		return this.calcRiskAggrResult(LiaRocCst.RISK_AGGR_CALC_RA399, proposal, null);
	}	

	@Override
	public List<AggregationResult> calcRA400(ProposalVO proposal) {
		return this.calcRiskAggrResult(LiaRocCst.RISK_AGGR_CALC_RA400, proposal, null);
	}

    /**
     * <p>Description : 風險累積計算共用接口，因為處理邏輯很像，所以寫成共用的</p>
     * <p>Created By : Amy Hung</p>
     * <p>Create Time : Apr 29, 2016</p>
     * @param riskType 傳入各風險累積接口相應的 RiskType
     * @param proposal ProposalVO
     * @param dateBfEffectiveDate 只有同業傷害醫療險346接口才會傳入值，其它傳入 null
     * @return
     */
    private List<AggregationResult> calcRiskAggrResult(Integer riskType, ProposalVO proposal,
            Date dateBfEffectiveDate) {

        List<AggregationResult> aggrList = new ArrayList<>();

        // 逐一累計被保人的風險累積資料
        Long policyId = proposal.getPolicyChgId() == null ? proposal.getPolicyId() : null;
        for (InsuredVO vo : proposal.getInsureds()) {

            BigDecimal riskLAmmout = new BigDecimal(0);
            BigDecimal riskRAmmout = new BigDecimal(0);
            String certiCode = vo.getCertiCode();

            NBUtils.logger(this.getClass(),
                    "calcRiskAggrResult riskType=" + riskType + ",certiCode=" + certiCode + " begin");

            switch (riskType) {
                case 77: //同業醫療住院日額306
                    riskLAmmout = liaRocDownload2020Dao.queryRA306Amount(certiCode, LiaRocCst.INFORCE_LIAROC_TYPE_LIFE,
                            dateBfEffectiveDate, policyId);
                    riskRAmmout = liaRocDownload2020Dao.queryRA306Amount(certiCode, LiaRocCst.RECEIVE_LIAROC_TYPE_LIFE,
                            dateBfEffectiveDate, policyId);
                    break;
                case 78: //同業壽險傷害險319
                    riskLAmmout = liaRocDownload2020Dao.queryRA319Amount(certiCode, LiaRocCst.INFORCE_LIAROC_TYPE_LIFE,
                            policyId);
                    riskRAmmout = liaRocDownload2020Dao.queryRA319Amount(certiCode, LiaRocCst.RECEIVE_LIAROC_TYPE_LIFE,
                            policyId);
                    break;
                case 79: //同業壽險傷害險325
                    riskLAmmout = liaRocDownload2020Dao.queryRA325Amount(certiCode, LiaRocCst.INFORCE_LIAROC_TYPE_LIFE,
                            policyId);
                    riskRAmmout = liaRocDownload2020Dao.queryRA325Amount(certiCode, LiaRocCst.RECEIVE_LIAROC_TYPE_LIFE,
                            policyId);
                    break;
                case 80: //同業壽險傷害險327
                    riskLAmmout = liaRocDownload2020Dao.queryRA327Amount(certiCode, LiaRocCst.INFORCE_LIAROC_TYPE_LIFE,
                            policyId);
                    riskRAmmout = liaRocDownload2020Dao.queryRA327Amount(certiCode, LiaRocCst.RECEIVE_LIAROC_TYPE_LIFE,
                            policyId);
                    break;
                case 81: //同業壽險傷害險330
                    riskLAmmout = liaRocDownload2020Dao.queryRA330Amount(certiCode, LiaRocCst.INFORCE_LIAROC_TYPE_LIFE,
                            policyId);
                    riskRAmmout = liaRocDownload2020Dao.queryRA330Amount(certiCode, LiaRocCst.RECEIVE_LIAROC_TYPE_LIFE,
                            policyId);
                    break;
                case 82: //同業傷害醫療險346
                    riskLAmmout = liaRocDownload2020Dao.queryRA346Amount(certiCode, LiaRocCst.INFORCE_LIAROC_TYPE_LIFE,
                            policyId);
                    riskRAmmout = liaRocDownload2020Dao.queryRA346Amount(certiCode, LiaRocCst.RECEIVE_LIAROC_TYPE_LIFE,
                            policyId);
                    break;
                case 83: //同業傷害微型險373
                    riskLAmmout = liaRocDownload2020Dao.queryRA373Amount(certiCode, LiaRocCst.INFORCE_LIAROC_TYPE_LIFE,
                            policyId);
                    riskRAmmout = liaRocDownload2020Dao.queryRA373Amount(certiCode, LiaRocCst.RECEIVE_LIAROC_TYPE_LIFE,
                            policyId);
                    break;
                case 121: //同業傷害微型險384
                    riskLAmmout = liaRocDownload2020Dao.queryRA384Amount(certiCode, LiaRocCst.INFORCE_LIAROC_TYPE_LIFE,
                            policyId);
                    riskRAmmout = liaRocDownload2020Dao.queryRA384Amount(certiCode, LiaRocCst.RECEIVE_LIAROC_TYPE_LIFE,
                            policyId);
                    break;
                case 135: //同業小額終老保險390
                    riskLAmmout = liaRocDownload2020Dao.queryRA390Amount(certiCode, LiaRocCst.INFORCE_LIAROC_TYPE_LIFE,
                            policyId);
                    riskRAmmout = liaRocDownload2020Dao.queryRA390Amount(certiCode, LiaRocCst.RECEIVE_LIAROC_TYPE_LIFE,
                            policyId);
                    break;
                case 161: //同業(新式公會通報)網路投保變額年金商品保費393(BC415-EVB)
                    riskLAmmout = liaRocDownload2020Dao.queryRA393Amount(certiCode, LiaRocCst.INFORCE_LIAROC_TYPE_LIFE,
                            policyId);
                    riskRAmmout = liaRocDownload2020Dao.queryRA393Amount(certiCode, LiaRocCst.RECEIVE_LIAROC_TYPE_LIFE,
                            policyId);
                    break;
                case 173: //同業(新式公會通報)同業「網路投保定期壽險」限額395(BC432)
                    riskLAmmout = liaRocDownload2020Dao.queryRA395Amount(certiCode, LiaRocCst.INFORCE_LIAROC_TYPE_LIFE,
                            policyId);
                    riskRAmmout = liaRocDownload2020Dao.queryRA395Amount(certiCode, LiaRocCst.RECEIVE_LIAROC_TYPE_LIFE,
                            policyId);
                    break;
                case 174: //同業(新式公會通報)同業「小額終老傷害險」保額396(BC432)
                    riskLAmmout = liaRocDownload2020Dao.queryRA396Amount(certiCode, LiaRocCst.INFORCE_LIAROC_TYPE_LIFE,
                            policyId);
                    riskRAmmout = liaRocDownload2020Dao.queryRA396Amount(certiCode, LiaRocCst.RECEIVE_LIAROC_TYPE_LIFE,
                            policyId);
                    break;
                case 175: //同業(新式公會通報)同業「網路投保重大疾病保險」保額397(BC432)
                    riskLAmmout = liaRocDownload2020Dao.queryRA397Amount(certiCode, LiaRocCst.INFORCE_LIAROC_TYPE_LIFE,
                            policyId);
                    riskRAmmout = liaRocDownload2020Dao.queryRA397Amount(certiCode, LiaRocCst.RECEIVE_LIAROC_TYPE_LIFE,
                            policyId);
                    break;
                case 182: //同業(新式公會通報)同業「傷害險」保額319 (PCR464240)
                    riskLAmmout = liaRocDownload2020Dao.queryRA319Amount2(certiCode, LiaRocCst.INFORCE_LIAROC_TYPE_LIFE,
                            policyId);
                    riskRAmmout = liaRocDownload2020Dao.queryRA319Amount2(certiCode, LiaRocCst.RECEIVE_LIAROC_TYPE_LIFE,
                            policyId);
                    break;
                case 188: // 同業微型壽險保額(PCR-499927)
                    riskLAmmout = liaRocDownload2020Dao.queryRA398Amount(certiCode, LiaRocCst.INFORCE_LIAROC_TYPE_LIFE, policyId);
                    riskRAmmout = liaRocDownload2020Dao.queryRA398Amount(certiCode, LiaRocCst.RECEIVE_LIAROC_TYPE_LIFE, policyId);
                    break;
                case 189: // 同業微型傷害醫療保額(PCR-499927)
                    riskLAmmout = liaRocDownload2020Dao.queryRA399Amount(certiCode, LiaRocCst.INFORCE_LIAROC_TYPE_LIFE, policyId);
                    riskRAmmout = liaRocDownload2020Dao.queryRA399Amount(certiCode, LiaRocCst.RECEIVE_LIAROC_TYPE_LIFE, policyId);
                    break;
                case 190: // 同業微型實支實付型傷害醫療件數(PCR-499927)
                    riskLAmmout = liaRocDownload2020Dao.queryRA400Amount(certiCode, LiaRocCst.INFORCE_LIAROC_TYPE_LIFE, policyId);
                    riskRAmmout = liaRocDownload2020Dao.queryRA400Amount(certiCode, LiaRocCst.RECEIVE_LIAROC_TYPE_LIFE, policyId);
                    break;
            }

            riskLAmmout = ((riskLAmmout == null) ? BigDecimal.ZERO : riskLAmmout);
            riskRAmmout = ((riskRAmmout == null) ? BigDecimal.ZERO : riskRAmmout);

            NBUtils.logger(this.getClass(),
                    "calcRiskAggrResult riskType=" + riskType + ",certiCode=" + certiCode + " end");
            NBUtils.logger(this.getClass(), "calcRiskAggrResult riskType=" + riskType + ",certiCode=" + certiCode
                    + ",承保累額=" + riskLAmmout + ",收件累額=" + riskRAmmout);

            // 承保
            AggregationResult aggrL = new AggregationResult();
            aggrL.setPolicyId(policyId);
            aggrL.setPartyId(vo.getPartyId());
            aggrL.setAggregationCategory(2);
            aggrL.setRiskType(riskType);
            aggrL.setRiskAmount(riskLAmmout);
            aggrL.setCertiCode(certiCode);
            aggrList.add(aggrL);

            // 收件
            AggregationResult aggrR = new AggregationResult();
            aggrR.setPolicyId(policyId);
            aggrR.setPartyId(vo.getPartyId());
            aggrR.setAggregationCategory(1);
            aggrR.setRiskType(riskType);
            aggrR.setRiskAmount(riskRAmmout);
            aggrR.setCertiCode(certiCode);
            aggrList.add(aggrR);
        }

        return aggrList;
    }

	@Override
	public Map<String, Long> getOtherTotalAnnPremByPolicyId(Long policyId) {
		Map<String, Long> annPremMap = new HashMap<String, Long>();
		List<com.ebao.ls.pa.pub.vo.InsuredVO> insuredVOs = insuredService.findByPolicyId(policyId);
		for (com.ebao.ls.pa.pub.vo.InsuredVO insuredVO : insuredVOs) {
			Long total = liaRocDownload2020Dao.queryIssueAnnuPrem(insuredVO.getCertiCode(), null, policyId)
					+ liaRocDownload2020Dao.queryReceiveAnnuPrem(insuredVO.getCertiCode(), null, policyId);
			annPremMap.put(insuredVO.getPartyId().toString(), total);
		}
		return annPremMap;
	}

	@Override
	public Map<String, Long> getCompTotalAnnPremByPolicyId(Long policyId) {
		Map<String, Long> annPremMap = new HashMap<String, Long>();
		List<com.ebao.ls.pa.pub.vo.InsuredVO> insuredVOs = insuredService.findByPolicyId(policyId);
		Long policyChgId = null;
		for (com.ebao.ls.pa.pub.vo.InsuredVO insuredVO : insuredVOs) {
			BigDecimal annualPremOurCompany = coverageInsuredService.getAnnualPremOurCompany(insuredVO.getCertiCode(),
					policyId, policyChgId);

			//RTC-217681-以小數點後2位4捨5入至整數
			annualPremOurCompany = NBUtils.specialScale(annualPremOurCompany, 2, 0, RoundingMode.HALF_UP);

			annPremMap.put(insuredVO.getPartyId().toString(), annualPremOurCompany.longValue());
		}

		return annPremMap;
	}

	@Override
	public List<Map<String, Object>> findGroupLiaRocDetailData(String certiCode, String type, Long policyId) {

		// 查詢承保/收件之團險通報統計資料
		List<Map<String, Object>> resultAll = new ArrayList<Map<String, Object>>();
		// 要將查詢到之資料區分自費(Y)及公費(N)
		List<Map<String, Object>> resultN = liaRocDownload2020Dao.findGroupLiaRocDetailData(certiCode, type, policyId,
				"N");
		for (Map<String, Object> map : resultN) {
			map.put("NONSELFPAY", "Y");
			map.put("SELFPAY", "N");
			map.put("INDEX", type);
		}
		List<Map<String, Object>> resultY = liaRocDownload2020Dao.findGroupLiaRocDetailData(certiCode, type, policyId,
				"Y");
		for (Map<String, Object> map : resultY) {
			map.put("NONSELFPAY", "N");
			map.put("SELFPAY", "Y");
			map.put("INDEX", type);
		}

		resultAll.addAll(resultN);
		resultAll.addAll(resultY);

		return resultAll;
	}


	/**
	 * <p>Description : 公會通報資訊-喪葬費用保險金(Liability17)通報資料</p>
	 * <p>Created By : Kathy.Yeh</p>
	 * <p>Create Time : Dec 14, 2021</p>
	 * @param request
	 */
	public List<LiaRocLiability17DetailVO> findLiability17DetailInfo(Long policyId) {
		
		// 客戶險種資料區塊 VO;
		List<LiaRocLiability17DetailVO> liaRocLiability17DetailVoList = new ArrayList<LiaRocLiability17DetailVO>();
		LiaRocLiability17DetailVO vo = null;
		List<Map<String, Object>> detailMapList = null;

		// 公會喪葬費用保險金通報資訊;
		List<Map<String, Object>> listMap = liaRocDownload2020Dao.getLiability17DetailInfo(policyId);

		if (listMap != null && listMap.size() > 0) {

			for (Map<String, Object> map : listMap) {
				
				String insuredName = (String) map.get("INSURED_NAME");
				String certiCode = (String) map.get("CERTI_CODE");
				String oldForeignerId = (String) map.get("OLD_FOREIGNER_ID"); //舊式統一證號

				if (liaRocLiability17DetailVoList.isEmpty()) { //第一筆資料

					vo = new LiaRocLiability17DetailVO();
					vo.setCustomerName(insuredName);
					vo.setCertiCode(certiCode);
					vo.setOldForeignerId(oldForeignerId);
					detailMapList = new ArrayList<Map<String, Object>>();
					detailMapList.add(map);
					vo.setLiaRocLiability17DetailListMap(detailMapList);
					liaRocLiability17DetailVoList.add(vo);

				} else {

					boolean isExist = Boolean.FALSE;
					for (LiaRocLiability17DetailVO Liability17DetailVo : liaRocLiability17DetailVoList) {
						if (insuredName.equals(vo.getCustomerName()) && certiCode.equals(Liability17DetailVo.getCertiCode())) {
							Liability17DetailVo.getLiaRocLiability17DetailListMap().add(map);
							isExist = Boolean.TRUE;
							break;
						}
					}

					if (!isExist) {
						vo = new LiaRocLiability17DetailVO();
						vo.setCustomerName(insuredName);
						vo.setCertiCode(certiCode);
						vo.setOldForeignerId(oldForeignerId);
						detailMapList = new ArrayList<Map<String, Object>>();
						detailMapList.add(map);
						vo.setLiaRocLiability17DetailListMap(detailMapList);
						liaRocLiability17DetailVoList.add(vo);
					}

				}
			}
		}
		return liaRocLiability17DetailVoList;
	}	
		
	@Override
	public List<LiaRocPaidInjuryMedicalDetailVO> findLiarocPaidInjuryMedicalDetailInfo(Long policyId) {

		List<LiaRocPaidInjuryMedicalDetailVO> liaRocPaidInjuryMedicalDetailVoList = new ArrayList<LiaRocPaidInjuryMedicalDetailVO>();
		LiaRocPaidInjuryMedicalDetailVO vo = null;
		List<Map<String, Object>> detailMapList = null;

		List<Map<String, Object>> listMap = liaRocDownload2020Dao.getLiarocPaidInjuryMedicalDetailInfo(policyId);

		for (Map<String, Object> map : listMap) {

			String insuredName = (String) map.get("INSURED_NAME");
			String certiCode = (String) map.get("CERTI_CODE");
			String oldForeignerId = (String) map.get("OLD_FOREIGNER_ID"); //舊式統一證號

			if (liaRocPaidInjuryMedicalDetailVoList.isEmpty()) { //第一筆資料

				vo = new LiaRocPaidInjuryMedicalDetailVO();
				vo.setCustomerName(insuredName);
				vo.setCertiCode(certiCode);
				vo.setOldForeignerId(oldForeignerId);
				detailMapList = new ArrayList<Map<String, Object>>();
				detailMapList.add(map);
				vo.setLiaRocPaidInjuryMedicalDetailListMap(detailMapList);
				liaRocPaidInjuryMedicalDetailVoList.add(vo);

			} else {

				boolean isExist = Boolean.FALSE;
				for (LiaRocPaidInjuryMedicalDetailVO lpmdVo : liaRocPaidInjuryMedicalDetailVoList) {
					if (insuredName.equals(lpmdVo.getCustomerName()) && certiCode.equals(lpmdVo.getCertiCode())) {
						lpmdVo.getLiaRocPaidInjuryMedicalDetailListMap().add(map);
						isExist = Boolean.TRUE;
						break;
					}
				}

				if (!isExist) {
					vo = new LiaRocPaidInjuryMedicalDetailVO();
					vo.setCustomerName(insuredName);
					vo.setCertiCode(certiCode);
					vo.setOldForeignerId(oldForeignerId);
					detailMapList = new ArrayList<Map<String, Object>>();
					detailMapList.add(map);
					vo.setLiaRocPaidInjuryMedicalDetailListMap(detailMapList);
					liaRocPaidInjuryMedicalDetailVoList.add(vo);
				}

			}
		}
		return liaRocPaidInjuryMedicalDetailVoList;
	}

	@Override
	public List<String> getLiarocPolicyStatusCount(Long policyId, String certiCode, String liarocPolicyStatus) {
		return liaRocDownload2020Dao.getLiarocPolicyStatusCount(policyId, certiCode, liarocPolicyStatus);
	}

	public List<LiaRocDownload2020VO> downloadUnbTerminateCheck(Long policyId)
			throws IllegalArgumentException, Exception {

		List<String[]> sendIdnos = new ArrayList<String[]>();

		PolicyVO policyVO = policyService.load(policyId);
		// 3.授權人(IR415629-原付款人改為授權人)
		Map<String, PolicyBankAuthVO> bankAuthMap = policyBankAuthService.findMapByPolicyId(policyId);
		// 4.實際繳交保費之利害關係人
		List<Map<String, ?>> paTraderList = detailRegHelper.findPaTraderCertiByPolicyId(policyId);
		//PCR-357807 要新增要保人/各授權人/實際繳交保費之利害關係人的ID做公會下載 2020/02/24 Add by Kathy
		Map<String, UnbRelationRole> certiMap = ValidatorUtils.getAllRelationCertiCode(policyVO, bankAuthMap,
				paTraderList);

		for (String idno : certiMap.keySet()) {
			UnbRelationRole unbRelationRole = certiMap.get(idno);
			if (unbRelationRole != null
					&& NBUtils.isEmptyOrDummyCertiCode(unbRelationRole.getOldForeignerId()) == false) {
				sendIdnos.add(new String[] { idno, unbRelationRole.getOldForeignerId() });
			} else {
				sendIdnos.add(new String[] { idno });
			}
		}

		if (sendIdnos.size() == 0) {
			//請輸入被保險人ID號
			new IllegalArgumentException(
					StringResource.getStringData("MSG_214902", AppContext.getCurrentUser().getLangId()));
		} else {

			//被保險人身份下載
			List<LiaRocDownload2020VO> insuredDownloadList = downloadTerminateCheck(Liaroc20Cst.MN_UNB.getCode(),
					Liaroc20Cst.DOWNLOAD_ROLE_INSURED.getCode(), policyId, sendIdnos);

			//要保人身份下載
			if (insuredDownloadList != null && insuredDownloadList.size() > 0) {
				if (LiaRocCst.LIAROC_DL_STATUS_ERROR
						.equals(insuredDownloadList.get(0).getLiarocDownloadStatus()) == false) {
					List<LiaRocDownload2020VO> holderDownloadList = downloadTerminateCheck(
							Liaroc20Cst.MN_UNB.getCode(), Liaroc20Cst.DOWNLOAD_ROLE_HOLDER.getCode(), policyId,
							sendIdnos);
					if (holderDownloadList != null) {
						insuredDownloadList.addAll(holderDownloadList);
					}
				}
			}
			return insuredDownloadList;
		}

		return null;
	}

	@Override
	public LiaRocDownload2020VO downloadForFixedUpload(String certiCode) throws GenericException {
		Long policyId = (new Date().getTime() * -1);

		if (StringUtils.isNotEmpty(certiCode)) {
			List<String[]> sendIdnos = new ArrayList<String[]>();
			sendIdnos.add(new String[] { certiCode });

			// 被保險人身份下載
			List<LiaRocDownload2020VO> downloadList = _download(Liaroc20Cst.MN_UNB.getCode(),
					Liaroc20Cst.DOWNLOAD_ROLE_INSURED.getCode(), Liaroc20Cst.DOWNLOAD_KIND_TGL_FIXED_UPLOAD.getCode(),
					policyId, sendIdnos);

			if (downloadList != null && downloadList.size() > 0) {
				LiaRocDownload2020VO download2020VO = downloadList.get(0);
				String tglCompayCode = Para.getParaValue(LiaRocCst.PARAM_LIAROC_TGL_COMPANY_CODE);

				if (download2020VO.getDownloadDetails() != null) {

					//只要TGL的公會下載資料
					Predicate predicate = new BeanPredicate("liarocCompanyCode",
							PredicateUtils.equalPredicate(tglCompayCode));

					List<LiaRocDownloadDetail2020VO> tglUploadDetailList = (List<LiaRocDownloadDetail2020VO>) CollectionUtils
							.select(download2020VO.getDownloadDetails(), predicate);

					download2020VO.setDownloadDetails(tglUploadDetailList);
				}
				return download2020VO;
			}
		}

		return null;
	}

	@Override
	public BigDecimal query107LawAmount(String certiCode, Long policyId, int aggrRiskType) {
		BigDecimal sum = liaRocDownload2020Dao.query107LawAmount(certiCode, policyId, aggrRiskType);
		if(sum == null) {
			sum = BigDecimal.ZERO;
		}
		return sum;
	}

	@Override
	public int countOtherCompany107LawReceive(Long policyId, String certiCode) {
		int count = liaRocDownload2020Dao.countOtherCompany107LawReceive(policyId, certiCode);
		return count;
	}

	@Override
	public List<LiaRocDownload2020VO> downloadUnbInsuredTerminate(Long policyId) throws Exception {

		List<String[]> sendIdnos = new ArrayList<String[]>();

		PolicyVO policyVO = policyService.load(policyId);

		for (com.ebao.ls.pa.pub.vo.InsuredVO insuredVO : policyVO.gitSortInsuredList()) {

			if (insuredVO != null && NBUtils.isEmptyOrDummyCertiCode(insuredVO.getOldForeignerId()) == false) {
				sendIdnos.add(new String[] { insuredVO.getCertiCode(), insuredVO.getOldForeignerId() });
			} else {
				sendIdnos.add(new String[] { insuredVO.getCertiCode() });
			}
		}

		if (sendIdnos.size() == 0) {
			// 請輸入被保險人ID號
			new IllegalArgumentException(
					StringResource.getStringData("MSG_214902", AppContext.getCurrentUser().getLangId()));
		} else {
			// 被保險人身份下載
			List<LiaRocDownload2020VO> insuredDownloadList = downloadTerminateCheck(Liaroc20Cst.MN_UNB.getCode(),
					Liaroc20Cst.DOWNLOAD_ROLE_INSURED.getCode(), policyId, sendIdnos);

			// 不可重下要保人身份資料，因為會renew終止保單資料，若renew就要重起同業終止保單檢核

			return insuredDownloadList;
		}

		return null;
	}

	@Override
	public Map<String, LiaRocResidentIdVO> downloadResidentId(String bizModule, Long bizId, String... oldResdientIds)
			throws WSCIException {
		Map<String, LiaRocResidentIdVO> resultMap = new HashMap<String, LiaRocResidentIdVO>();

		List<String> allList = new ArrayList<String>();
		allList.addAll(Arrays.asList(oldResdientIds));

		int startIndex = 0;
		//call公會最多100筆(切每100筆)
		while (startIndex < allList.size()) {
			int endIndex = startIndex + 100;
			if (endIndex > allList.size()) {
				endIndex = allList.size();
			}
			List<String> subList = allList.subList(startIndex, endIndex);
			startIndex = endIndex;
			Date requestTime = new Date();
			Liaroc20ResidentRsVO rs  = liaroc20CI.doLiaroc20ResidentQuery(bizModule,
					subList.toArray(new String[subList.size()]));
			Date responseTime = new Date();
			if ("000".equals(rs.getProcessCode()) == false) {

				logger.error(String.format("新舊證號查詢失敗:%s,bizId=%s,rqUid=%s \n%s-%s", bizModule, bizId,
						rs.getRqUid(), rs.getProcessCode(), rs.getProcessErrorMsg()));

				throw new WSCIException(rs.getProcessCode(), String.format("新舊證號查詢失敗:rqUid=%s \n%s-%s",
						rs.getRqUid(), rs.getProcessCode(), rs.getProcessErrorMsg()));
			}

			for (String oldResidentId : oldResdientIds) {
				Liaroc20ResidentRsDataVO rsData = rs.getQueryResultMap().get(oldResidentId);
				LiaRocResidentIdVO result = new LiaRocResidentIdVO();
				result.setBizModule(bizModule);
				result.setBizId(bizId);
				result.setOldResidentId(oldResidentId);
				result.setRequestTime(requestTime);
				result.setResponseTime(responseTime);
				if (rsData != null) {
					result.setNewResidentId(rsData.getLatestId());
					result.setBirthDate(rsData.getBirthDate());
					result.setLastUpdateTime(rsData.getLastUpdateTime());
					if("0".equals(rsData.getCode()) == false) {
						result.setErrorMsg(rsData.getMsg());
					}
					if(StringUtils.isNotEmpty(rsData.getLatestId())) {
						result.setNoDataFlag(CodeCst.YES_NO__NO);
					}
				}
				result = liaRocResidentIdService.save(result);
				resultMap.put(oldResidentId, result);
			}
		}

		return resultMap;
	}

	@Override
	public Map<String, LiaRocResidentIdVO> downloadUNBResidentId(Long policyId, String... oldResdientIds)
			throws WSCIException {
		Map<String, LiaRocResidentIdVO> resultMap = new HashMap<String, LiaRocResidentIdVO>();

		List<LiaRocResidentIdVO> lists = liaRocResidentIdService.find(
				Restrictions.eq("bizModule", Liaroc20Cst.MN_UNB.getCode()), Restrictions.eq("bizId", policyId),
				Restrictions.in("oldResidentId", oldResdientIds),
				Restrictions.eq("houseKeepingFlag", CodeCst.YES_NO__NO));

		// 同個ID, 回傳最後新增的那筆
		for (LiaRocResidentIdVO item : lists) {
			LiaRocResidentIdVO temp = resultMap.get(item.getOldResidentId());
			if (temp == null) {
				resultMap.put(item.getOldResidentId(), item);
			} else {
				if (item.getInsertTimestamp().compareTo(temp.getInsertTimestamp()) > 0) {
					resultMap.put(item.getOldResidentId(), item);
				}
			}
		}
		// 未落地的ID,打接口查找
		List<String> needDownloadList = new ArrayList<String>();
		needDownloadList.addAll(Arrays.asList(oldResdientIds));

		needDownloadList.removeAll(resultMap.keySet());
		if (needDownloadList.size() > 0) {
			Map<String, LiaRocResidentIdVO> queryMap = downloadResidentId(Liaroc20Cst.MN_UNB.getCode(), policyId,
					needDownloadList.toArray(new String[needDownloadList.size()]));
			resultMap.putAll(queryMap);
		}

		return resultMap;
	}
}
