package com.ebao.ls.uw.ds;

import com.ebao.ls.foundation.service.GenericServiceImpl;
import com.ebao.ls.pa.pub.vo.PolicyVO;
import com.ebao.ls.uw.data.UwPolicyChangeDao;
import com.ebao.ls.uw.data.bo.UwPolicyChange;
import com.ebao.ls.uw.vo.UwPolicyChangeVO;
import com.ebao.pub.framework.ExceptionFactory;
import com.ebao.pub.framework.GenericException;

public class UwPolicyChangeServiceImpl
		extends GenericServiceImpl<UwPolicyChangeVO, UwPolicyChange, UwPolicyChangeDao>
		implements UwPolicyChangeService {

	@Override
	protected UwPolicyChangeVO newEntityVO() {
		return new UwPolicyChangeVO();
	}
	
	@Override
	public UwPolicyChangeVO save(UwPolicyChangeVO entity) throws GenericException {
		try {
			UwPolicyChange bo = this.dao.load(entity.getPolicyId());
			if(bo == null){
				bo = new UwPolicyChange();
			}
			bo.copyFromVO(entity, true, false);
			dao.save(bo);
			return entity;
		} catch (Exception e) {
			throw ExceptionFactory.parse(e);
		}
	}

	@Override
	public void saveOrUpdateByPolicyVO(PolicyVO policyVO) throws Exception {

		Long policyId = policyVO.getPolicyId();
		UwPolicyChangeVO uwPolicyChangeVO = this.load(policyId);

		if (uwPolicyChangeVO == null) {
			uwPolicyChangeVO = newEntityVO();
		}

		this.copyByPolicyVO(uwPolicyChangeVO, policyVO);
		this.save(uwPolicyChangeVO);

	}

	@Override
	public void createWhenNullByPolicyVO(PolicyVO policyVO) throws Exception {

		Long policyId = policyVO.getPolicyId();
		UwPolicyChangeVO uwPolicyChangeVO = this.load(policyId);

		if (uwPolicyChangeVO == null) {
			uwPolicyChangeVO = newEntityVO();
			this.copyByPolicyVO(uwPolicyChangeVO, policyVO);
			this.save(uwPolicyChangeVO);
		}
	}

	/**
	 * <p>Description : 將所以比對欄位欄位由PolicyVO同步至UwPolicyChangeVO</p>
	 * <p>Created By : simon.huang</p>
	 * <p>Create Time : Feb 18, 2017</p>
	 * @param uwPolicyChangeVO
	 * @param policyVO
	 */
	private void copyByPolicyVO(UwPolicyChangeVO uwPolicyChangeVO, PolicyVO policyVO) {

		uwPolicyChangeVO.setPolicyId(policyVO.getPolicyId());
		uwPolicyChangeVO.setHolderCertiCode(policyVO.getPolicyHolder().getCertiCode());
		//......後續若有PCR比對其它欄位,實作於此 

	}
}
