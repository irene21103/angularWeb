package com.ebao.ls.uw.ctrl.letter;

import java.util.List;

import com.ebao.ls.pa.pub.vo.InsuredVO;
import com.ebao.pub.framework.GenericForm;

public class SickLetterForm extends GenericForm {

	/**
	 * <code>serialVersionUID</code> 的註解
	 */
	private static final long serialVersionUID = 1202986722981718885L;
	
	private Long ListId;

	private Long policyId;

	private Long underwriteId;
	
	private String insured;
	
	private String insuredName;
	
	private String subAction;
	
	private List<InsuredVO> insuredVOs;
	
	private List<Boolean> isLockInsureds;
	
	private String sourceType;

	private List<SickFormItem> sickFormItems;
	
	private String retMsg;
	
	private Boolean submitSuccess = new Boolean(false);
	
	private Long option;
	
	private String[] sickFormItem;
	
	private String policyNo;
	
	private long templateId;
	
	private String forWordMode;
	
	/* 保全變更申請ID */
	private Long changeId;

	public Long getPolicyId() {
		return policyId;
	}

	public void setPolicyId(Long policyId) {
		this.policyId = policyId;
	}

	public Long getUnderwriteId() {
		return underwriteId;
	}

	public void setUnderwriteId(Long underwriteId) {
		this.underwriteId = underwriteId;
	}

	public String getInsured() {
		return insured;
	}

	public void setInsured(String insured) {
		this.insured = insured;
	}
	
	public String getInsuredName() {
		return insuredName;
	}

	public void setInsuredName(String insuredName) {
		this.insuredName = insuredName;
	}

	public String getRetMsg() {
		return retMsg;
	}

	public void setRetMsg(String retMsg) {
		this.retMsg = retMsg;
	}

	public List<InsuredVO> getInsuredVOs() {
		return insuredVOs;
	}

	public void setInsuredVOs(List<InsuredVO> insuredVOs) {
		this.insuredVOs = insuredVOs;
	}

	public List<SickFormItem> getSickFormItems() {
		return sickFormItems;
	}

	public void setSickFormItems(List<SickFormItem> sickFormItems) {
		this.sickFormItems = sickFormItems;
	}

	public String getSubAction() {
		return subAction;
	}

	public void setSubAction(String subAction) {
		this.subAction = subAction;
	}

	public String getSourceType() {
		return sourceType;
	}

	public void setSourceType(String sourceType) {
		this.sourceType = sourceType;
	}
	
	public List<Boolean> getIsLockInsureds() {
		return isLockInsureds;
	}

	public void setIsLockInsureds(List<Boolean> isLockInsureds) {
		this.isLockInsureds = isLockInsureds;
	}

	public Long getOption() {
		return option;
	}

	public void setOption(Long option) {
		this.option = option;
	}
	
	public String[] getSickFormItem() {
		return sickFormItem;
	}

	public void setSickFormItem(String[] sickFormItem) {
		this.sickFormItem = sickFormItem;
	}

	public Boolean getSubmitSuccess() {
		return submitSuccess;
	}

	public void setSubmitSuccess(Boolean submitSuccess) {
		this.submitSuccess = submitSuccess;
	}

	public String getPolicyNo() {
		return policyNo;
	}

	public void setPolicyNo(String policyNo) {
		this.policyNo = policyNo;
	}

	public long getTemplateId() {
		return templateId;
	}

	public void setTemplateId(long templateId) {
		this.templateId = templateId;
	}

	public String getForWordMode() {
		return forWordMode;
	}

	public void setForWordMode(String forWordMode) {
		this.forWordMode = forWordMode;
	}
	/**
	 * <p>Description :保全變更申請ID </p>
	 * <p>Created By : TGL144</p>
	 * <p>Create Time : Sep 10, 2016</p>
	 * @return
	 */
	public Long getChangeId() {
		return changeId;
	}
	/**
	 * <p>Description :保全變更申請ID </p>
	 * <p>Created By : TGL144</p>
	 * <p>Create Time : Sep 10, 2016</p>
	 * @param changeId
	 */
	public void setChangeId(Long changeId) {
		this.changeId = changeId;
	}
}
