package com.ebao.ls.subprem.ctrl.extraction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.ls.escape.helper.EscapeHelper;
import com.ebao.ls.subprem.bs.SearchActionService;
import com.ebao.ls.subprem.bs.UpdateActionService;
import com.ebao.pub.framework.GenericAction;
import com.ebao.pub.framework.GenericException;
import com.ebao.pub.util.Log;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002 </p>
 * <p>Company: eBaoTech </p>
 * <p>Create Time: </p>
 * @author
 * @version
 *          $//Id: SearchAction.java,v 1.0 2004/10/11 Jackie.Xue Exp $
 *          $//Log: SearchAction.java,v $
 *          Revision 1.0 2004/09/08 Jackie.Xue
 *          no message
 * 
 */

public class SearchAction extends GenericAction {
  public SearchAction() {
  }

  @Override
  public ActionForward process(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws GenericException {

    String policyCode;
    String policyId = "";
    String policyNumber = "";
    // String policyCode = "";
    String policyNumberD = "";
    String policyHolderName = "";
    String firstName = "";
    String lastName = "";
    String policyHolderId = "";
    String dueDate = "";
    String policyStatus = "";
    String payMethod = "";
    String partyIdType = "";
    String partyIdNumber = "";
    String eventType = "";
    String tempStr = "";
    String loadSource = "s"; // "s":load extraction_update.jsp from extraction_search.jsp
    // "c":load extraction_update.jsp from Counter Collection
    String forward = "display";

    ArrayList list1 = null; // query by policyId or (partyId && partyIdType)
    ArrayList list2 = null; // query by policyholder

    try {
      SearchForm searchform = (SearchForm) form;
      // if policyCode is not empty,then it means the page is from Counter Collection to Single draw
      policyCode = ((com.ebao.ls.encode.encoder.Encode.encodeForHTML(request).getParameter("policyCode") == null) ? "" :
              com.ebao.ls.encode.encoder.Encode.encodeForHTML(request).getParameter("policyCode"));      if (policyCode.compareTo("") != 0) {
        eventType = "forward";
        loadSource = "c";

        list1 = searchActionDS.getPolicyByPolicyNumber(policyCode);
        if (list1 != null) {
          for (Iterator it = list1.iterator(); it.hasNext();) {
            HashMap model = (HashMap) it.next();
            policyNumberD = (String) model.get("policyNumberD");
            dueDate = (String) model.get("dueDate");
            policyStatus = (String) model.get("policyStatus");
            payMethod = (String) model.get("payMethod");
          }
        }
        policyId = updateActionDS.getPolicyIdByPolicyCode(policyCode);
        tempStr = policyCode;
        request.setAttribute("policyId", policyId);

        request.setAttribute("policyNumber", policyNumber);
        EscapeHelper.escapeHtml(request).setAttribute("policyNumberD", policyNumberD);
        request.setAttribute("policyHolderName", policyHolderName);
        EscapeHelper.escapeHtml(request).setAttribute("dueDate", dueDate);
        EscapeHelper.escapeHtml(request).setAttribute("policyStatus", policyStatus);
        EscapeHelper.escapeHtml(request).setAttribute("payMethod", payMethod);
        request.setAttribute("partyIdType", partyIdType);
        request.setAttribute("partyIdNumber", partyIdNumber);
        request.setAttribute("loadSource", loadSource);
      } else {
        if (searchform != null) {
          policyId = searchform.getPolicyId();
          policyNumber = searchform.getPolicyNumber();
          policyNumberD = searchform.getPolicyNumberD();
          policyHolderName = searchform.getPolicyHolderName();
          firstName = searchform.getFirstName();
          lastName = searchform.getLastName();
          policyHolderId = searchform.getPolicyHolderId();
          dueDate = searchform.getDueDate();
          policyStatus = searchform.getPolicyStatus();
          payMethod = searchform.getPayMethod();

          partyIdType = searchform.getPartyIdType();
          partyIdNumber = searchform.getPartyIdNumber();
          tempStr = searchform.getTempStr();
          eventType = searchform.getEventType();
        }
      }
      // SearchActionDS searchactionds = new SearchActionDS();
      // james.chen modify it for GEL00038121
      if ("search".equals(eventType)) {
        if (policyNumber != null && policyNumber.compareTo("") != 0) {
          // get data by DS where policyId = policyid
          list1 = searchActionDS.getPolicyByPolicyNumber(policyNumber);
        } else if (partyIdNumber != null && partyIdNumber.compareTo("") != 0
            && partyIdType != null && partyIdType.compareTo("") != 0) {
          // get data by DS where partyIdNumber = partyIdNumber and partyIdType = partyIdType
          list2 = searchActionDS.getPolicyByPartyInfo(partyIdType,
              partyIdNumber);
          //        } else if (policyHolderName != null
          //            && policyHolderName.compareTo("") != 0) {
        } else if (firstName != null && !firstName.equals("")
            || lastName != null && !lastName.equals("")) {

          // get data by DS where policyHolder = policyHolder
          //list2 = searchActionDS.getPolicyByHolderName(policyHolderName);
          list2 = searchActionDS.getPolicyByHolderName(firstName + ";"
              + lastName);
        }
        EscapeHelper.escapeHtml(request).setAttribute("list1", list1);
        request.setAttribute("list2", list2);
        request.setAttribute("policyId", policyId);
        request.setAttribute("policyNumber", policyNumber);
        request.setAttribute("policyHolderName", policyHolderName);
        request.setAttribute("policyHolderNameD", policyHolderName);
        request.setAttribute("policyHolderId", policyHolderId);
        request.setAttribute("partyIdType", partyIdType);
        request.setAttribute("partyIdNumber", partyIdNumber);
        EscapeHelper.escapeHtml(request).setAttribute("payMethod", payMethod);
        // request.setAttribute("SearchForm", searchform);
        forward = "display";
      } else if ("research".equals(eventType)) {
        //

        list1 = searchActionDS.getPolicyByHolderId(policyHolderId);

        request.setAttribute("list1", list1);
        request.setAttribute("policyId", policyId);
        request.setAttribute("policyNumber", policyNumber);
        request.setAttribute("policyHolderName", policyHolderName);
        request.setAttribute("policyHolderId", policyHolderId);
        request.setAttribute("partyIdType", partyIdType);
        request.setAttribute("partyIdNumber", partyIdNumber);

        forward = "display";
      } else if ("forward".equals(eventType)) {
        list1 = searchActionDS.getPolicyByPolicyNumberN0Escape(tempStr);
        String policyHolderNameD = "";
        if (list1 != null) {
          for (Iterator it = list1.iterator(); it.hasNext();) {
            HashMap model = (HashMap) it.next();
            policyNumberD = (String) model.get("policyNumberD");
            dueDate = (String) model.get("dueDate");
            policyStatus = (String) model.get("policyStatus");
            payMethod = (String) model.get("payMethod");
            policyHolderNameD = (String) model.get("policyHolderNameD");
          }
        }

        request.setAttribute("policyNumber", policyNumber);
        EscapeHelper.escapeHtml(request).setAttribute("policyNumberD", policyNumberD);
        request.setAttribute("policyHolderName", policyHolderName);
        /*
         * MODIFICATION START by boyd.wu Oct 10, 2009 CRDB00369679 Policy holder name not properly setted
         */
        EscapeHelper.escapeHtml(request).setAttribute("policyHolderNameD", policyHolderNameD);
        /*
         * MODIFICATION END by boyd.wu Oct 10, 2009
         */
        EscapeHelper.escapeHtml(request).setAttribute("dueDate", dueDate);
        EscapeHelper.escapeHtml(request).setAttribute("policyStatus", policyStatus);
        EscapeHelper.escapeHtml(request).setAttribute("payMethod", payMethod);
        request.setAttribute("partyIdType", partyIdType);
        request.setAttribute("partyIdNumber", partyIdNumber);
        request.setAttribute("loadSource", loadSource);

        forward = "forward";
      } else if ("exit".equals(eventType)) {
        forward = "exit";
      } else {
        request.setAttribute("list1", null);
        request.setAttribute("list2", null);
        request.setAttribute("eventType", "");
        searchform.setEventType("");
      }
    } catch (Exception ex) {
      Log.error(this.getClass(), ex);

    }
    return mapping.findForward(forward);
  }

  @Resource(name = SearchActionService.BEAN_DEFAULT)
  private SearchActionService searchActionDS;

  @Resource(name = UpdateActionService.BEAN_DEFAULT)
  private UpdateActionService updateActionDS;


}
