package com.ebao.ls.uw.ctrl.info;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.ls.pa.pub.ci.PolicyCI;
import com.ebao.ls.pa.pub.vo.InsuredVO;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.framework.GenericAction;

public class InsuredPolicyHistoryRecordAction extends GenericAction {

    private String forward = "display";
    @Resource(name = PolicyCI.BEAN_DEFAULT)
    private PolicyCI policyCI;
    
    @Resource(name = QualityRecordActionHelper.BEAN_DEFAULT)
    private QualityRecordActionHelper qualityRecordActionHelper;
    
    @Override
    public ActionForward process(ActionMapping mapping, ActionForm form,
                    HttpServletRequest request, HttpServletResponse response)
                    throws Exception {
        
        String policyId = request.getParameter("policyId").toString();
        List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();     
        
        List<InsuredVO> insuredVOList = qualityRecordActionHelper.findPartyIdOfInsuredByPolicyId(policyId);
        for(InsuredVO o : insuredVOList){
            Long[] partyIdArray = new Long[]{o.getPartyId()};
            List<Map<String, Object>> queryList= qualityRecordActionHelper.findInfoVOByPartyId(partyIdArray, AppContext.getCurrentUser().getLangId());
            for(int i=0 ;i<queryList.size();i++){
                
                String tmp=queryList.get(i).get("policyCode").toString();
                long policyIdVal = policyCI.findPolicyIdByPolicyCode(tmp);
                queryList.get(i).put("policyId", policyIdVal);
               
            } 
            
            Map<String, Object> queryMap = new HashMap<String, Object>();
            queryMap.put("name", o.getName());
            queryMap.put("queryList", queryList);
            
            resultList.add(queryMap);
        }
        
        request.setAttribute("resultList", resultList);  
        return mapping.findForward(forward);
    }

}
