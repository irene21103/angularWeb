package com.ebao.ls.callout.ctrl;

import com.ebao.ls.callout.bs.CalloutConfigService;
import com.ebao.ls.callout.vo.CalloutConfigVO;
import com.ebao.ls.pa.pub.bs.NbSQLOnlyService;
import com.ebao.ls.product.service.LifeProductService;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.framework.GenericAction;
import com.ebao.pub.framework.GenericException;
import com.ebao.pub.i18n.lang.CodeTable;
import com.ebao.pub.util.Trans;
import com.ebao.pub.util.TransUtils;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;
import java.util.Optional;
import javax.annotation.Resource;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class CalloutConfigUNBAction extends GenericAction {

    protected final Logger logger = LogManager.getFormatterLogger();

    private static final String ACTION_DELETE = "delete";

    private static final String ACTION_ADD = "add";

    private static final String ACTION_MODIFY = "modify";

    private static final String ACTION_SAVESAMPLINGDATE = "savesamplingdate";

    private static final String FORWARD_RESULT = "success";

    private static final String CONFIG_TYPE_OF_5 = "5"; // 5 NB抽樣參數

    @Resource(name = CalloutConfigService.BEAN_DEFAULT)
    private CalloutConfigService calloutConfigService;

    @Resource(name = LifeProductService.BEAN_DEFAULT)
    private LifeProductService lifeProductService;

    @Resource(name = NbSQLOnlyService.BEAN_DEFAULT)
    private NbSQLOnlyService paNbSQLOnlyService;

    @Override
    public ActionForward process(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {

        CalloutConfigForm calloutConfigForm = (CalloutConfigForm) form;

        CalloutConfigVO vo = calloutConfigForm.getCalloutConfigVO();

        String saction = StringUtils.trimToEmpty(calloutConfigForm.getActionType());

        switch (saction) {
            case ACTION_ADD: {
                UserTransaction ut = null;
                try {
                    String langId = AppContext.getCurrentUser().getLangId();
                    ut = Trans.getUserTransaction();
                    vo.setConfigType(String.valueOf(CONFIG_TYPE_OF_5));
                    vo.setFileName("UNB-" + CodeTable.getCodeDescById("T_SALES_CHANNEL", vo.getSalesChannelType().toString(), langId)[0]);
                    String channelType = CodeTable.getCodeDescById("T_SALES_CHANNEL", vo.getSalesChannelType().toString(), langId)[0];
                    String channelOrg = CodeTable.getCodeDescById("T_CHANNEL_ORG", vo.getChannelOrg() != null ? vo.getChannelOrg().toString() : "", langId)[0];
                    vo.setFileDesc(String.format("UNB-%s-%s", channelType, channelOrg));
                    vo.setProductId(null);
                    try {
                        if (StringUtils.isNotBlank(vo.getInternalId())) {
                            Optional.ofNullable(lifeProductService.getProductByInternalId(vo.getInternalId())).ifPresent(lifeProduct -> {
                                vo.setProductId(lifeProduct.getProductId());
                            });
                        }
                    } catch (GenericException ex) {
                        logger.error("", ex);
                    }
                    resetRatioAndScore(vo);

                    ut.begin();
                    calloutConfigService.saveOrUpdateForNBU(vo);
                    calloutConfigForm.setCalloutConfigVO(null);

                    request.setAttribute("message", "MSG_FMS_497");
                    ut.commit();
                } catch (GenericException | IllegalStateException | SecurityException | NamingException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {
                    TransUtils.rollback(ut);
                    request.setAttribute("message", "MSG_1256002");
                }
                renewDataTable(calloutConfigForm);
                return mapping.findForward(FORWARD_RESULT);
            }
            case ACTION_MODIFY: {
                CalloutConfigVO config = calloutConfigService.load(vo.getListId());
                try {
                    if (Objects.nonNull(config.getProductId())) {
                        Optional.ofNullable(lifeProductService.getProduct(config.getProductId())).ifPresent(lifeProduct -> {
                            config.setInternalId(lifeProduct.getProduct().getInternalId());
                        });
                    }
                } catch (Exception ex) {
                    logger.error("", ex);
                }
                calloutConfigForm.setCalloutConfigVO(config);

                request.setAttribute("show", "Y");
                renewDataTable(calloutConfigForm);
                return mapping.findForward(FORWARD_RESULT);
            }
            case ACTION_DELETE: {
                UserTransaction ut = null;
                try {
                    ut = Trans.getUserTransaction();
                    ut.begin();
                    calloutConfigForm.setCalloutConfigVO(null);
                    calloutConfigService.remove(vo.getEntityId());
                    ut.commit();
                    request.setAttribute("message", "MSG_FMS_497");
                } catch (GenericException | IllegalStateException | SecurityException | NamingException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException e) {
                    TransUtils.rollback(ut);
                    request.setAttribute("message", "MSG_1256002");
                }
                renewDataTable(calloutConfigForm);
                return mapping.findForward(FORWARD_RESULT);
            }
            case ACTION_SAVESAMPLINGDATE: {
                paNbSQLOnlyService.saveNewestCalloutSamplingDate(calloutConfigForm.getSamplingDate());
                request.setAttribute("message", "MSG_FMS_497");
                renewDataTable(calloutConfigForm);
                setupSamplingDate(calloutConfigForm);
                return mapping.findForward(FORWARD_RESULT);
            }
            default:
                renewDataTable(calloutConfigForm);
                setupSamplingDate(calloutConfigForm);
                request.getSession().setAttribute("minSamplingDate", DateUtils.addDays(new Date(), 1));
                return mapping.findForward(FORWARD_RESULT);
        }
    }

    private void renewDataTable(CalloutConfigForm calloutConfigForm) {
        calloutConfigForm.getGridList().clear();
        calloutConfigService.query(CONFIG_TYPE_OF_5, null, null).forEach(map -> {
            CalloutConfigVO calloutConfigVO = new CalloutConfigVO();
            calloutConfigVO.setChannelOrg(MapUtils.getLong(map, "CHANNEL_ORG"));
            calloutConfigVO.setSalesChannelType(MapUtils.getLong(map, "SALES_CHANNEL_TYPE"));
            calloutConfigVO.setProductId(MapUtils.getLong(map, "PRODUCT_ID"));
            calloutConfigVO.setListId(MapUtils.getLong(map, "LIST_ID"));
            calloutConfigVO.setFileName(MapUtils.getString(map, "FILE_NAME"));
            calloutConfigVO.setFileDesc(MapUtils.getString(map, "FILE_DESC"));
            calloutConfigVO.setData(tryConvertBigDecimal(MapUtils.getString(map, "DATA")));
            calloutConfigVO.setPursueRatio(tryConvertBigDecimal(MapUtils.getString(map, "PURSUE_RATIO")));
            calloutConfigVO.setPursueScore(tryConvertBigDecimal(MapUtils.getString(map, "PURSUE_SCORE")));
            calloutConfigVO.setSamplingScore(tryConvertBigDecimal(MapUtils.getString(map, "SAMPLING_SCORE")));
            try {
                if (Objects.nonNull(calloutConfigVO.getProductId())) {
                    Optional.ofNullable(lifeProductService.getProduct(calloutConfigVO.getProductId())).ifPresent(lifeProduct -> {
                        calloutConfigVO.setInternalId(lifeProduct.getProduct().getInternalId());
                    });
                }
            } catch (Exception ex) {
                logger.error("", ex);
            }
            calloutConfigForm.getGridList().add(calloutConfigVO);
        });
    }

    private void resetRatioAndScore(CalloutConfigVO vo) {
        BigDecimal bd_100 = new BigDecimal("100");
        BigDecimal bd_0 = new BigDecimal("0");
        BigDecimal data = vo.getData() == null ? BigDecimal.ZERO : vo.getData();
        BigDecimal pursueRatio = vo.getPursueRatio() == null ? BigDecimal.ZERO : vo.getPursueRatio();
        BigDecimal samplingScore = vo.getSamplingScore() == null ? BigDecimal.ZERO : vo.getSamplingScore();
        BigDecimal pursueScore = vo.getPursueScore() == null ? BigDecimal.ZERO : vo.getPursueScore();
        // 比例介於0 ~ 100
        if (data.compareTo(bd_0) < 0) {
            vo.setData(bd_0);
        }
        if (data.compareTo(bd_100) > 0) {
            vo.setData(bd_100);
        }
        if (pursueRatio.compareTo(bd_0) < 0) {
            vo.setPursueRatio(bd_0);
        }
        if (pursueRatio.compareTo(bd_100) > 0) {
            vo.setPursueRatio(bd_100);
        }
        // 分數介於0 ~ 100
        if (samplingScore.compareTo(bd_0) < 0) {
            vo.setSamplingScore(bd_0);
        }
        if (samplingScore.compareTo(bd_100) > 0) {
            vo.setSamplingScore(bd_100);
        }
        if (pursueScore.compareTo(bd_0) < 0) {
            vo.setPursueScore(bd_0);
        }
        if (pursueScore.compareTo(bd_100) > 0) {
            vo.setPursueScore(bd_100);
        }
    }

    private BigDecimal tryConvertBigDecimal(String val) {
        if (StringUtils.isBlank(val)) {
            return null;
        }
        try {
            return NumberUtils.createBigDecimal(val);
        } catch (Exception ex) {
            return null;
        }
    }

    private void setupSamplingDate(CalloutConfigForm calloutConfigForm) {
        paNbSQLOnlyService.getNewestCalloutSamplingDate().ifPresent(date -> {
            calloutConfigForm.setSamplingDate(date);
        });
    }

}
