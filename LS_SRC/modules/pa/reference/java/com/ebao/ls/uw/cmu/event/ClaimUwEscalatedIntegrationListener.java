package com.ebao.ls.uw.cmu.event;

import com.ebao.foundation.commons.annotation.TriggerPoint;
import com.ebao.ls.cmu.event.ClaimUwEscalated;
import com.ebao.pub.event.AbstractIntegrationListener;
import com.ebao.pub.integration.IntegrationMessage;

public class ClaimUwEscalatedIntegrationListener
    extends
      AbstractIntegrationListener<ClaimUwEscalated> {
  final static String code = "uw.claimuwEscalated";

  @Override
  @TriggerPoint(component = "Underwriting", description = "During claim underwriting, if the underwriter escalates the case successfully to senior staff, the event is triggered.<br/>Menu navigation: Customer service > Customer service underwriting<br/>", event = "direct:event."
      + code, id = "com.ebao.tp.ls." + code)
  protected String getEventCode(ClaimUwEscalated event,
      IntegrationMessage<Object> in) {
    return "event." + code;
  }
}
