package com.ebao.ls.srv.ctrl;

import java.util.ArrayList;
import java.util.List;

import com.ebao.pub.framework.GenericForm;

/**
 * <p>Title: MaturityPoolForm </p>		
 * <p>Description: This class Maturity Pool 's form class.</p>
 * <p>Copyright: Copyright (c) 2002	</p>
 * <p>Company: 	eBaoTech			</p>
 * <p>Create Time:		2005/04/11	</p> 
 * @author simen.li
 * @version 1.0
 */
public class MaturityPoolForm extends GenericForm {
  String policyNumber = "";
  String caseNumber = "";
  String lifeAssured = "";
  String idNumber = "";
  String batchNumber = "";
  List allMaturityLst = new ArrayList();
  String actionType = "";
  String itemId = "";
  String pageNo = "1";
  String pageSize = "10";

  /**
   * @return Returns the pageNo.
   */
  public String getPageNo() {
    return pageNo;
  }
  /**
   * @param pageNo The pageNo to set.
   */
  public void setPageNo(String pageNo) {
    this.pageNo = pageNo;
  }
  /**
   * @return Returns the pageSize.
   */
  public String getPageSize() {
    return pageSize;
  }
  /**
   * @param pageSize The pageSize to set.
   */
  public void setPageSize(String pageSize) {
    this.pageSize = pageSize;
  }
  /**
   * @return Returns the itemId.
   */
  public String getItemId() {
    return itemId;
  }
  /**
   * @param itemId The itemId to set.
   */
  public void setItemId(String itemId) {
    this.itemId = itemId;
  }
  /**
   * @return Returns the allMaturityLst.
   */
  public List getAllMaturityLst() {
    /*
     if (allMaturityLst.size()==0) {
     MaturityPoolInfoVO infoVO=new MaturityPoolInfoVO();
     infoVO.setBatchNumber("123456");
     infoVO.setCaseNumber("asd");
     infoVO.setIdNumber("1111111");
     infoVO.setLifeAssured("a");
     infoVO.setPolicyNumber("1qa");
     allMaturityLst.add(infoVO);
     }*/
    //
    return allMaturityLst;
  }
  /**
   * @param allMaturityLst The allMaturityLst to set.
   */
  public void setAllMaturityLst(List allMaturityLst) {
    this.allMaturityLst = allMaturityLst;
  }

  /**
   * @return Returns the batchNumber.
   */
  public String getBatchNumber() {
    return batchNumber;
  }
  /**
   * @param batchNumber The batchNumber to set.
   */
  public void setBatchNumber(String batchNumber) {
    this.batchNumber = batchNumber;
  }
  /**
   * @return Returns the caseNumber.
   */
  public String getCaseNumber() {
    return caseNumber;
  }
  /**
   * @param caseNumber The caseNumber to set.
   */
  public void setCaseNumber(String caseNumber) {
    this.caseNumber = caseNumber;
  }
  /**
   * @return Returns the idNumber.
   */
  public String getIdNumber() {
    return idNumber;
  }
  /**
   * @param idNumber The idNumber to set.
   */
  public void setIdNumber(String idNumber) {
    this.idNumber = idNumber;
  }
  /**
   * @return Returns the lifeAssured.
   */
  public String getLifeAssured() {
    return lifeAssured;
  }
  /**
   * @param lifeAssured The lifeAssured to set.
   */
  public void setLifeAssured(String lifeAssured) {
    this.lifeAssured = lifeAssured;
  }
  /**
   * @return Returns the policyNumber.
   */
  public String getPolicyNumber() {
    return policyNumber;
  }
  /**
   * @param policyNumber The policyNumber to set.
   */
  public void setPolicyNumber(String policyNumber) {
    this.policyNumber = policyNumber;
  }

  /**
   * @return Returns the actionType.
   */
  public String getActionType() {
    return actionType;
  }
  /**
   * @param actionType The actionType to set.
   */
  public void setActionType(String actionType) {
    this.actionType = actionType;
  }
}
