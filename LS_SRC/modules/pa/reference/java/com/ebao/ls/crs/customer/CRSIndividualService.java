package com.ebao.ls.crs.customer;

import com.ebao.ls.crs.vo.CRSIndividualVO;
import com.ebao.ls.foundation.service.GenericService;

public interface CRSIndividualService extends GenericService<CRSIndividualVO>{
	public final static String BEAN_DEFAULT = "crsIndividualService";
	
	public abstract CRSIndividualVO findByCertiCode(String certiCode);
	
	/**
	 * PCR 257607 330030 e-Approval ITR-1801051_CRS共同申報及盡職審查專案- 保全變更及CRS確認申報國作業
	 * @param crsId
	 * @param policyCode
	 * @return
	 */
	public abstract CRSIndividualVO findByCrsIdAndPolicyCode(Long crsId, String policyCode);
		
}
