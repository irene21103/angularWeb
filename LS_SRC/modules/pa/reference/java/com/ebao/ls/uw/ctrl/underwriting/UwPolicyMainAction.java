package com.ebao.ls.uw.ctrl.underwriting;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.foundation.app.sys.sysmgmt.ds.SysMgmtDSLocator;
import com.ebao.foundation.app.sys.sysmgmt.ds.UserVO;
import com.ebao.ls.arap.pub.ci.CashCI;
import com.ebao.ls.chl.service.AgentService;
import com.ebao.ls.cs.ci.CSCI;
import com.ebao.ls.pa.nb.bs.ProposalProcessService;
import com.ebao.ls.pa.nb.bs.rule.ProposalRuleResultService;
import com.ebao.ls.pa.pub.ci.PolicyCI;
import com.ebao.ls.pa.pub.service.PolicyService;
import com.ebao.ls.pa.pub.vo.BeneficiaryVO;
import com.ebao.ls.pa.pub.vo.CoverageAgentVO;
import com.ebao.ls.pa.pub.vo.CoverageInsuredVO;
import com.ebao.ls.pa.pub.vo.CoverageVO;
import com.ebao.ls.pa.pub.vo.PayerAccountVO;
import com.ebao.ls.pa.pub.vo.PolicyHolderVO;
import com.ebao.ls.pa.pub.vo.PolicyVO;
import com.ebao.ls.pa.pub.vo.UwRiFacExtendVO;
import com.ebao.ls.product.model.query.output.ProdBizCategorySub;
import com.ebao.ls.product.service.LifeProductService;
import com.ebao.ls.pty.ci.CustomerCI;
import com.ebao.ls.pty.vo.CustomerVO;
import com.ebao.ls.pub.CodeCst;
import com.ebao.ls.pub.workflow.WfContextLs;
import com.ebao.ls.uw.ctrl.UwGenericAction;
import com.ebao.ls.uw.ctrl.decision.UwProductDecisionViewForm;
import com.ebao.ls.uw.ctrl.pub.UwPolicyForm;
import com.ebao.ls.uw.data.query.UwQueryDao;
import com.ebao.ls.uw.ds.UwProcessService;
import com.ebao.ls.uw.ds.UwRiApplyService;
import com.ebao.ls.uw.ds.constant.Cst;
import com.ebao.ls.uw.ds.constant.UwSourceTypeConstants;
import com.ebao.ls.uw.ds.constant.UwStatusConstants;
import com.ebao.ls.uw.ds.sp.QueryUwDataSp;
import com.ebao.ls.uw.util.Utils;
import com.ebao.ls.uw.vo.UwExtraLoadingVO;
import com.ebao.ls.uw.vo.UwPolicyVO;
import com.ebao.ls.uw.vo.UwProductVO;
import com.ebao.pub.annotation.PAPubAPIUpdate;
import com.ebao.pub.annotation.PrdAPIUpdate;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.framework.ExceptionFactory;
import com.ebao.pub.framework.GenericException;
import com.ebao.pub.framework.internal.spring.DSProxy;
import com.ebao.pub.i18n.lang.CodeTable;
import com.ebao.pub.util.BeanUtils;

/**
 * <p>
 * Title:Underwriting
 * </p>
 * <p>
 * Description:
 * </p>
 * <p>
 * Copyright: Copyright (c) 2004
 * </p>
 * <p>
 * Company: eBaoTech Corporation
 * </p>
 * 
 * @author mingchun.shi
 * @since Jun 26, 2005
 * @version 1.0
 */
public class UwPolicyMainAction extends UwGenericAction {
  /**
   * Underwriting basic information(UwPolicyForm)
   */
  public static final String MAIN_FORM = "main_form";
  /**
   * Discount type of the proposal/policy
   */
  public static final String DISCOUNT_TYPE = "discount_type";
  /**
   * benefit list
   */
  public static final String PRODUCT_LIST = "product_list";
  /**
   * Life assured list
   */
  public static final String LIFE_ASSURED_LIST = "life_assured_list";
  /**
   * Life assured list
   */
  public static final String PROPOSAL_LIST = "proposal_list";
  /**
   * Life assured list
   */
  public static final String NOMINEE_LIST = "nominee_list";
  
    /**
     * Agent list/2015-06-25 add by sunny
     */
    public static final String AGENT_LIST = "agent_list";
    
    /**
     * 再保公司列表2015-06-25 add by sunny
     */
    public static final String RI_COMP_LIST = "ri_comp_list";

    /**
     * 身心障礙列表2015-06-25 add by sunny
     */
    public static final String DISABILITY_LIST = "disability_list";

    /**
     * 核保檢核綜合意見欄2015-06-29 add by sunny
     */
    public static final String COMMENT_OPTION_LIST = "comment_option_list";

    /**
     * is company holder
     */
  public static final String IS_COMPANY_HOLDER = "Y";
  
    /**
     * Underwriting endorsement code list
     */
    public static final String ENDORSEMENTCODE_LIST = "endorsementCode_list";
    
    /**
     * Underwriting condition code list
     */
    public static final String CONDITIONCODE_LIST = "conditionCode_list";
    
    /**
     * Underwriting exclusion code list
     */
    public static final String EXCLUSIONCODE_LIST = "exclusionCode_list";
  /**
   * Underwriting main page logic name
   */
  public static final String MAIN_PAGE = "detailInfo";
  /**
   * Policy Holder is company mark
   */
  public static final String COMPANY_HOLDER_MARK = "companyHolderMark";
  /**
   * Policy Main benefit first life assured
   */
  public static final String MAIN_BENEFIT_FIRST_INSURED = "mainBenefitFirstInsured";
  
  /**
   * add by simon.huang on 2015-07-24 增加是否為投資型商品保單
   */
  public static final String IS_ILP = "is_ilp";
  /**
   * Agent list/2015-08-26 add by peter
   */
  public static final String UWRIFAC_EXTEND_LIST = "uwRiFacExtendList";
  
  public static final String BEAN_DEFAULT = "/uw/uwPolicyMain";

  @Resource(name = UwRiApplyService.BEAN_DEFAULT)
  protected UwRiApplyService uwRiApplyService;
  
  /**
   * Underwriting main page rending and proposal status and uw status amendation
   * entry.
   * 
   * @param mapping The ActionMapping used to select this instance
   * @param form The optional ActionForm bean for this request (if any)
   * @param request HttpRequest
   * @param response HttpResponse
   * @return ActionForward ActionForward
   * @throws GenericException Application Exception
   */
  @Override
  @PrdAPIUpdate
  @PAPubAPIUpdate("loadPolicyByPolicyId")
  public ActionForward uwProcess(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws GenericException {
    // Long underwriteId = Long.valueOf(request.getParameter("underwriteId"));
    // update for workflow,by robert.xu on 2008.4.22
    Long underwriteId = null;
    WfContextLs wfLs = WfContextLs.getInstance();
    String taskId = (String) request
        .getAttribute("com.ebao.pub.workflow.taskId");
    if (taskId != null) {
      underwriteId = (Long) wfLs.getLocalVariable(taskId, "underWriteId");
    } else {
      underwriteId = Long.valueOf(com.ebao.ls.encode.encoder.Encode.encodeForHTML(request).getParameter("underwriteId"));
    }
    UwPolicyVO uwPolicyVO = getUwPolicyDS().findUwPolicy(underwriteId);
    
    //再保公司 表格
    Collection<UwRiFacExtendVO> uwRiFacExtendVoList =uwRiApplyService.getRiInfoByUnderwriteId(underwriteId);
    List<Map<String,Object>>getListRi = new ArrayList<Map<String,Object>>();
    Map<String,Object> riMap = null;
    		for(UwRiFacExtendVO uwRiFacExtendVo:uwRiFacExtendVoList){
    			riMap = new HashMap<String,Object>();
    			riMap.put("listId", uwRiFacExtendVo.getListId());
//    			riMap.put("reinsuredResult", uwRiFacExtendVo.getReinsuredResult());
//    			riMap.put("reinsuredDate", uwRiFacExtendVo.getReinsuredDate());
    			getListRi.add(riMap);
    		}
    		request.setAttribute(UWRIFAC_EXTEND_LIST, getListRi);	  
    		
    Long policyId = uwPolicyVO.getPolicyId();
    Long underwriterId = getUnderwriterId();
    String sourceType = uwPolicyVO.getUwSourceType();
    PolicyVO policyVO = policyService.loadPolicyByPolicyId(policyId);
    // to record the current status,for cancel to use
    if ("pool".equals(request.getParameter("from"))) {
      if (CodeCst.UW_STATUS__WAITING_UW.equals(uwPolicyVO.getUwStatus())
          || CodeCst.UW_STATUS__ESCALATED.equals(uwPolicyVO.getUwStatus())) {
        uwPolicyVO.setAssumpsit("0");
      } else {
        uwPolicyVO.setAssumpsit("1");
      }
    }
    // synchronize status with outside interface
    Date currentUserLocalTime = AppContext.getCurrentUserLocalTime();
    if (UwSourceTypeConstants.NEW_BIZ.equals(sourceType)) {
      // doProposalProcess(policyVO,CodeCst.PROPOSAL_STEP__MANUAL_UW);
      // update to change process with workflow engine. Only maintain proposal
      // status,don't maintain t_proposal_process. by robert.xu on 2008.5.6
      maintainProposalStatus(policyVO);
    } else if (Utils.isCsUw(sourceType) || Utils.isClaimUw(sourceType)) {
      // policyVO.setProposalLock("Y");
      // policyVO.setProcessorId(underwriterId);
      // policyVO.setProposalLockTime(currentUserLocalTime);
      // policyCI.updatePolicy(policyVO);
      uwProcessDS.updateReUwPolicyUwStatus(underwriteId, underwriterId,
          UwStatusConstants.IN_PROGRESS, sourceType, uwPolicyVO.getChangeId());
      if (Utils.isCsUw(sourceType)) {
        // update the cs-underwriting from CS_APP_STATUS__UW_DISPOSING to
        // CS_APP_STATUS__UW_IN_PROGRESS
        csCI.updateApplicationInfoByUW(uwPolicyVO.getChangeId(),
            Integer.valueOf(CodeCst.CS_APP_STATUS__UW_IN_PROGRESS),
            underwriterId);
      }
    }
    // // if the policy has some letters not settled,set the pending reason
    // DocumentAPI docApi = (DocumentAPI)
    // HubFactory.newApiInstance(DocumentAPI.class, getClass());
    // if (!docApi.hasIssuedSlq(policyId,
    // CodeCst.PROPOSAL_STEP__MANUAL_UW).booleanValue()) {
    // uwPolicyVO.setUwPending(Long.valueOf(CodeCst.PENDING_CODE__QUERY));
    // }
        /* 2015-06-25 mark by sunny 
        String lcaDateIndi = uwPolicyVO.getLcaDateIndi();
        if (StringUtils.isNullOrEmpty(lcaDateIndi)) {
          uwPolicyVO.setLcaDateIndi("Y");
        }
        String generateLcaIndi = uwPolicyVO.getGenerateLcaIndi();
        if (StringUtils.isNullOrEmpty(generateLcaIndi)) {
          uwPolicyVO.setGenerateLcaIndi("Y");
        }
        String consentGivenIndi = uwPolicyVO.getConsentGivenIndi();
        if (StringUtils.isNullOrEmpty(consentGivenIndi)) {
          uwPolicyVO.setConsentGivenIndi("N");
        }
        **/
    uwPolicyVO.setUwStatus(CodeCst.UW_STATUS__UW_IN_PROGRESS);
    uwPolicyVO.setUnderwriterId(underwriterId);
	//PCR-463250 綜合查詢-新契約資訊要新增顯示核保單位 2023/01/19 Add by Kathy
	UserVO userVO = SysMgmtDSLocator.getUserDS().getUserByUserID(uwPolicyVO.getUnderwriterId());
	uwPolicyVO.setUwDeptId(Long.parseLong(userVO == null ? null : userVO.getDeptId()));      
    uwPolicyVO.setLockTime(currentUserLocalTime);
    // Collection products = uwPolicyDS.findUwProductEntitis(underwriteId);
    // Long jobCate1 = getJobCate1(products);
    // uwPolicyVO.setJobCateId(jobCate1);
    getUwPolicyDS().updateUwPolicy(uwPolicyVO, false);
    UwPolicyForm uwPolicyForm = new UwPolicyForm();
    try {
      BeanUtils.copyProperties(uwPolicyForm, uwPolicyVO);
      // get agent_code by agent_id
            /* 2015-06-25 mark by sunny 
            AgentInfo agentInfo = agentService.load(uwPolicyVO.getServiceAgent());
            uwPolicyForm.setAgentCode(agentInfo.getAgentCode());
            */
    } catch (Exception e) {
      throw ExceptionFactory.parse(e);
    }
        /* 2015-06-25 mark by sunny 
        // set appearl reason desc
        if (uwPolicyForm.getAppealReason() != null
            && !uwPolicyForm.getAppealReason().equals("0")) {
          uwPolicyForm.setAppealReasonDesc(CodeTable.getCodeDesc("T_APPEAL_REASON",
              uwPolicyForm.getAppealReason()));
        }
        */
    // modify by hengyan 2009-11-16 for include:jsp error
        // uwPolicyForm
        // .setChannelStaffCode(policyVO.getChannelStaffCode() == null ? ""
        // : policyVO.getChannelStaffCode());
        // uwPolicyForm.setUwEscaUser(null);
        /* 2015-06-25 add by sunny*/
        // 通路
        uwPolicyForm.setSalesChannel(String.valueOf(policyVO.getChannelType()));

        CoverageVO coverageVO = policyVO.gitMasterCoverage();
        List coverageAgents = coverageVO.gitSortCoverageAgents();
        // TODO 業務員TopTargetAgent??
        request.setAttribute(AGENT_LIST, coverageAgents);
        if (coverageAgents != null && coverageAgents.size() > 0) {
            CoverageAgentVO coverageAgent = coverageVO.getCoverageAgents().get(
                            0);
            uwPolicyForm.setAgentOrgan(coverageAgent.getChannelCode());
        }
        uwPolicyForm.setUserId(AppContext.getCurrentUser().getUserId());
        // 繳別
        uwPolicyForm.setInitialType(coverageVO.getCurrentPremium().getPaymentFreq());

        // 首期序期繳費方式
        List<PayerAccountVO> payerAccounts = policyVO.getPayerAccounts();
        if ( payerAccounts != null && payerAccounts.size() > 0) {
            PayerAccountVO payeraccountVo = payerAccounts.get(0);
            uwPolicyForm.setPayMode(String.valueOf(payeraccountVo.getPaymentMethod()));
            uwPolicyForm.setPayNext(String.valueOf(payeraccountVo.getPaymentMethodNext()));
        }

        // 集彚編號
        uwPolicyForm.setPayTogetherCode(policyVO.getPayTogetherCode());
        
        // 一般懸帳
        uwPolicyForm.setSuspense(cashCI.getSuspenseByIdPremPurpose(
                        policyVO.getPolicyId(), CodeCst.PREM_PURPOSE__GENERAL));
        // 幣別
        uwPolicyForm.setMoneyId(String.valueOf(policyVO.getCurrency()));
        // 暫不列印
        String pendingIssueType = uwPolicyVO.getPendingIssueType();
        if (Cst.PENDING_ISSUE_TYPE_NOT_MAIL.equals(pendingIssueType)
                        || Cst.PENDING_ISSUE_TYPE_NOT_PRINT
                                        .equals(pendingIssueType)) {
            uwPolicyForm.setPendingIssueStatus("Y");
        } else {
            uwPolicyForm.setPendingIssueStatus("N");
        }

        // set comments information 2009-12-2
        initializeComment(uwPolicyForm, uwPolicyVO.getPolicyId());
        // set uwpolicy information
        request.setAttribute(UwPolicyMainAction.MAIN_FORM, uwPolicyForm);
        // retrieve discount type
        Collection insuredLists = getUwPolicyDS().findUwLifeInsuredEntitis(
            underwriteId);
        Collection benefits = getUwPolicyDS().findUwProductEntitis(underwriteId);
        Collection uwProductDecisionViewForms = getUwProductDecisionViewForms(benefits);
        request.setAttribute(UwPolicyMainAction.PRODUCT_LIST,
            uwProductDecisionViewForms);
        // 首期表定保費
        setPrem(uwPolicyForm, uwProductDecisionViewForms);
        // 首期應繳保費
        uwPolicyForm.setInstallPrem(policyVO.getInstallPrem());
        boolean companyCustomer = !customerCI.isPerson(uwPolicyVO.getApplicantId());
        String companyHolderMark = String.valueOf(companyCustomer);
        request.setAttribute(COMPANY_HOLDER_MARK, companyHolderMark);
        /*
         * Set life assured,proposal,nominee list has been integrated with party
         * module if interface(data transfer object) changed then we need to change
         * the corresponding jsp script in the jsp file
         */
    PolicyHolderVO holder = policyVO.getPolicyHolder();
    // set a flag to differ the holder which is IndivCustomer or organCompany
    String isOrganCompany = "N";
    if (!holder.getParty().isPerson()) {
      isOrganCompany = "Y";
    }
    // get the proposal list
    Collection proposalList = helper.getProposalList(holder, uwPolicyVO);
    // get the nominee list
    List<BeneficiaryVO> beneficiaryList = policyVO.getBeneficiaries();
    Collection<HashMap<String, Object>> nomineeList = helper
        .getNomineeList(beneficiaryList);
    Collection insuredListForms = helper.getInsuredListForms(uwPolicyVO, insuredLists);
    
    if (UwSourceTypeConstants.NEW_BIZ.equals(uwPolicyVO.getUwSourceType())) {
        
        //artf137256 若有加費則被保人的是否標準體為 N
        //add by simon.huang on 2015-07-27
        Collection<UwExtraLoadingVO> allExtra = getUwPolicyDS().findUwExtraLoadingEntitis(underwriteId);
        Map<Long,Integer> insuuredExtraCount = new HashMap<Long, Integer>();  
        for( UwExtraLoadingVO uwExtraLoading : allExtra){
            Integer count = insuuredExtraCount.get(uwExtraLoading.getInsuredId());
            if(count == null){
                count = new Integer(0);
            }
            count = count.intValue() + 1;
            insuuredExtraCount.put(uwExtraLoading.getInsuredId(),count);
        }
        for (Iterator iter = insuredListForms.iterator(); iter.hasNext();) {
            UwInsuredListDecisionViewForm uwInsuredDecisionViewForm = (UwInsuredListDecisionViewForm) iter.next();
            Long insuredId = uwInsuredDecisionViewForm.getInsuredId();
            if(insuuredExtraCount.get(insuredId)!=null){
                uwInsuredDecisionViewForm.setStandLife(CodeCst.YES_NO__NO); 
            }else{
                uwInsuredDecisionViewForm.setStandLife(CodeCst.YES_NO__YES); 
            }
        }
        //end artf137256
    }
    //artf137256 非投資型商品保單系統不允異動「投資型保單溢繳直接退費」欄位
    //add by simon.huang on 2015-07-27
    String isIlp = CodeCst.YES_NO__NO;
    for (Iterator iter = benefits.iterator(); iter.hasNext();) {
        UwProductVO uwProductVO = (UwProductVO) iter.next();
        Long productId = new Long( uwProductVO.getProductId().toString());
        uwProductVO.getProductVersionId();
        
       //要保書分類
        ProdBizCategorySub sub2 = policyService.getProposalProdCategory(productId); 
        if(ProdBizCategorySub.SUB_12_002.equals(sub2)){
            isIlp = CodeCst.YES_NO__YES;
            break;
        }
    }
    request.setAttribute(UwPolicyMainAction.IS_ILP, isIlp);
    //end artf137256
    
    // update in CRDB00381245 on Dec 31,2009
    CoverageInsuredVO coverageInsuredVO = coverageVO.getLifeInsured1();
    CustomerVO personVO = customerCI.getPerson(coverageInsuredVO.getInsured()
        .getPartyId());
    request.setAttribute(UwPolicyMainAction.MAIN_BENEFIT_FIRST_INSURED,
        personVO);
    request
        .setAttribute(UwPolicyMainAction.LIFE_ASSURED_LIST, insuredListForms);
    request.setAttribute(UwPolicyMainAction.PROPOSAL_LIST, proposalList);
    request.setAttribute(UwPolicyMainAction.NOMINEE_LIST, nomineeList);
    request.setAttribute(UwPolicyMainAction.IS_COMPANY_HOLDER, isOrganCompany);

        Collection disabilityList = helper.getDisabilityList(insuredLists,
                        insuredListForms);
        request.setAttribute(UwPolicyMainAction.DISABILITY_LIST, disabilityList);

        request.setAttribute(UwPolicyMainAction.RI_COMP_LIST,
                        helper.getRiCompList(underwriteId));

        request.setAttribute(UwPolicyMainAction.COMMENT_OPTION_LIST,
                        helper.getCommentOptionList(underwriteId,
                                        uwPolicyVO.getCommentVersionId()));
        /*2015-06-25 mark by Sunny
        try {
          // set condition code list
          request.setAttribute(UwPolicyMainAction.CONDITIONCODE_LIST, BeanUtils
              .copyCollection(UwConditionForm.class, getUwPolicyDS()
                  .findUwConditionEntitis(underwriteId)));
          // set endorsement code list
          request.setAttribute(UwPolicyMainAction.ENDORSEMENTCODE_LIST, BeanUtils
              .copyCollection(UwEndorsementForm.class, getUwPolicyDS()
                  .findUwEndorsementEntitis(underwriteId)));
          // set exclusion code list
          request.setAttribute(UwPolicyMainAction.EXCLUSIONCODE_LIST, BeanUtils
              .copyCollection(UwExclusionForm.class, getUwPolicyDS()
                  .findUwExclusionEntitis(underwriteId)));
        } catch (Exception ex) {
          throw ExceptionFactory.parse(ex);
        }
        */
    coverageVO.getProductId();

    // if (CodeCst.YES_NO__YES.equals(basicInfoVO.getApilpIndi())) {
    request.setAttribute("isApilp", "N");
    // }
    // add by hanzhong.yan for cq:GEL00030033 2007/8/10
    Long masterProductItemId = coverageVO.getItemId();
    int masterProductChargeTerm = QueryUwDataSp
        .getChargeTerm(masterProductItemId);
    request.setAttribute("masterProductChargeTerm",
        Integer.valueOf(masterProductChargeTerm));
    // end add
    // get all duplicate endorsement code
    UwQueryDao dao = new UwQueryDao();
    request.setAttribute("dupEndorsementList", dao.getDuplicateEndoCode());
    // add to control NBU process with workflow engine,by robert.xu on 2008.4.17
    if (UwSourceTypeConstants.NEW_BIZ.equals(uwPolicyVO.getUwSourceType())) {
      if (taskId != null) {
        // WfHelper.processManagerForDesplayMain(uwPolicyVO.getPolicyId(),
        // uwPolicyVO.getUwStatus(), preProposalStatus);
        // WfUtils.maintainVariable(policyId, uwPolicyVO.getUwStatus(),
        // preProposalStatus);
        try {
          ProposalProcessService service = DSProxy
              .newInstance(ProposalProcessService.class);
          service.claimTask(policyId, ProposalProcessService.TASK_UW);
        } catch (Exception e) {
          throw ExceptionFactory.parse(e);
        }
      }
    }
    // add end
    // add by jason.li for get outstanding issues in pending status; begin
    // begin set value for page
    String[] returnListName = new String[] { "uWIssuesList",
        "uWManualIssuesList" };
    int[] listTypeName = new int[] { CodeCst.PROPOSAL_RULE_TYPE__UW,
        CodeCst.PROPOSAL_RULE_TYPE__MANUAL_UW };
    for (int i = 0; i < returnListName.length; i++) {
      if (proposalRuleResultService.hasPendingIssue(policyId, listTypeName[i])) {
        request.setAttribute("existOutstandingIssue", CodeCst.YES_NO__YES);
        break;
      } else {
        request.setAttribute("existOutstandingIssue", CodeCst.YES_NO__NO);
      }
    }
    // end
    // navigate to underwriting main page
    return mapping.findForward(MAIN_PAGE);
  }

    /**
     * 計算首期保費
     * <p>Description : </p>
     * <p>Created By : Sunny Wu</p>
     * <p>Create Time : Jun 25, 2015</p>
     * @param uwPolicyForm
     * @param uwProductDecisionViewForms
     */
    private void setPrem(UwPolicyForm uwPolicyForm,
                    Collection uwProductDecisionViewForms) {
        BigDecimal totalStdPrem = new BigDecimal("0");
        BigDecimal totalDiscntedPrem = new BigDecimal("0");
        for (Iterator iter = uwProductDecisionViewForms.iterator(); iter.hasNext();) {
            //artf137256 modify by simon.huang on 2015-07-28
            UwProductDecisionViewForm uwProductForm = (UwProductDecisionViewForm) iter.next();
            totalStdPrem = totalStdPrem.add(uwProductForm.getStdPremAf());
            totalDiscntedPrem = totalDiscntedPrem.add(uwProductForm.getTotalPremAf()); // totalPrem = discntedPrem+extraPrem
        }
        uwPolicyForm.setTotalStdPrem(totalStdPrem);
        uwPolicyForm.setTotalDiscntedPrem(totalDiscntedPrem);
    }

    /**
       * initialize comment, draw these value from workflow
       * 
       * @param detailRegForm
       * @param policyId
       */
  private void initializeComment(UwPolicyForm uwPolicyForm, Long policyId) {
    String detailcomment = getProposalProcessService().getCurrentVariableValue(
        policyId, "comments", ProposalProcessService.TASK_DATAENTRY).toString();
    uwPolicyForm.setDetailRegComment(detailcomment);
    String veriComment = getProposalProcessService().getCurrentVariableValue(
        policyId, "comments", ProposalProcessService.TASK_VERI).toString();
    uwPolicyForm.setProofingComment(veriComment);
    String uwComment = getProposalProcessService().getCurrentVariableValue(
        policyId, "comments", ProposalProcessService.TASK_UW).toString();
    uwPolicyForm.setUwComment(uwComment);
    
  }

  protected void maintainProposalStatus(PolicyVO vo) throws GenericException {
    try {
      Integer nextStatus = Integer.valueOf(CodeCst.PROPOSAL_STATUS__UW);
      vo.setProposalStatus(nextStatus);
      policyCI.updatePolicy(vo);
    } catch (Exception e) {
      throw ExceptionFactory.parse(e);
    }
  }

  /**
   * Integration with party and other modules, translates UwProductVO into
   * UwProductDecisionViewForm with the supporting utility( CustomerVO) of party
   * module
   * 
   * @param productVOs Collection UwProductVO collection
   * @return Collection UwProductDecisionViewForm collection
   * @throws GenericException Application Exception
   */
  protected Collection getUwProductDecisionViewForms(Collection productVOs)
      throws GenericException {
    // similar decision lies in method getInsuredListDecisionViewForms
    Collection result = new ArrayList();
    Iterator iter = productVOs.iterator();
    while (iter.hasNext()) {
      UwProductDecisionViewForm uwProductDecisionViewForm = new UwProductDecisionViewForm();
      UwProductVO uwProductVO = (UwProductVO) iter.next();
      BeanUtils.copyProperties(uwProductDecisionViewForm, uwProductVO);
      CustomerVO customer = customerCI.getPerson(uwProductVO.getInsured1());
        
    StringBuffer sbf = new StringBuffer();
      // sbf.append(customer.getFirstName());
      sbf.append(CodeTable.getCodeDesc("V_PARTY", customer.getCustomerId()
          .toString()));
      Long insured2 = uwProductVO.getInsured2();
      if (insured2 != null) {
        sbf.append(",");
        customer = customerCI.getPerson(insured2);
        // sbf.append(customer.getFirstName());
        sbf.append(CodeTable.getCodeDesc("V_PARTY", customer.getCustomerId()
            .toString()));
      }
      String benefitName = CodeTable.getCodeDesc("V_PRODUCT_LIFE_3",
          uwProductVO.getProductId().toString());
      uwProductDecisionViewForm.setLifeAssuredName(sbf.toString());
      uwProductDecisionViewForm.setBenefitName(benefitName);
      if (uwProductDecisionViewForm.getPolicyFeeAn() != null) {
        uwProductDecisionViewForm.setTotalPremAn(uwProductDecisionViewForm
            .getStdPremAn().add(uwProductDecisionViewForm.getPolicyFeeAn()));
      }
      if (uwProductDecisionViewForm.getExtraPremAn() != null) {
        uwProductDecisionViewForm.setTotalPremAn(uwProductDecisionViewForm
            .getTotalPremAn().add(uwProductDecisionViewForm.getExtraPremAn()));
      }
      try {
        if (Integer.valueOf(CodeCst.PAY_MODE__INVEST_ACCOUNT_OFFER).equals(
            uwProductVO.getPayMode())
            || Integer.valueOf(CodeCst.PAY_MODE__INVEST_ACC_BID).equals(
                uwProductVO.getPayMode())) {
          uwProductDecisionViewForm.setTotalPremAn(null);
        }
      } catch (Exception ex) {
        throw ExceptionFactory.parse(ex);
      }
      result.add(uwProductDecisionViewForm);
    }
    return result;
  }

    @Resource(name = UwProcessService.BEAN_DEFAULT)
    protected UwProcessService uwProcessDS;

    public void setUwProcessDS(UwProcessService uwProcessDS) {
        this.uwProcessDS = uwProcessDS;
    }

    public UwProcessService getUwProcessDS() {
        return uwProcessDS;
    }

    @Resource(name = ProposalRuleResultService.BEAN_DEFAULT)
    protected ProposalRuleResultService proposalRuleResultService;

    @Resource(name = PolicyCI.BEAN_DEFAULT)
    protected PolicyCI policyCI;

    public PolicyCI getPolicyCI() {
        return policyCI;
    }

    public void setPolicyCI(PolicyCI policyCI) {
        this.policyCI = policyCI;
    }

    @Resource(name = CSCI.BEAN_DEFAULT)
    protected CSCI csCI;

    public CSCI getCsCI() {
        return csCI;
    }

    public void setCsCI(CSCI csCI) {
        this.csCI = csCI;
    }

    @Resource(name = AgentService.BEAN_DEFAULT)
    protected AgentService agentService;

    public AgentService getAgentService() {
        return agentService;
    }

    public void setAgentService(AgentService agentService) {
        this.agentService = agentService;
    }

    @Resource(name = UwPolicySubmitActionHelper.BEAN_DEFAULT)
    protected UwPolicySubmitActionHelper helper;

    public UwPolicySubmitActionHelper getHelper() {
        return helper;
    }

    public void setHelper(UwPolicySubmitActionHelper helper) {
        this.helper = helper;
    }

    @Resource(name = ProposalProcessService.BEAN_DEFAULT)
    protected ProposalProcessService proposalProcessService;

    public ProposalProcessService getProposalProcessService() {
        return proposalProcessService;
    }

    public void setProposalProcessService(
                    ProposalProcessService proposalProcessService) {
        this.proposalProcessService = proposalProcessService;
    }

    public ProposalRuleResultService getProposalRuleResultService() {
        return proposalRuleResultService;
    }

    public void setProposalRuleResultService(
                    ProposalRuleResultService proposalRuleResultService) {
        this.proposalRuleResultService = proposalRuleResultService;
    }

    @Resource(name = CustomerCI.BEAN_DEFAULT)
    private CustomerCI customerCI;

    /**
     * @return the customerCI
     */
    public CustomerCI getCustomerCI() {
        return customerCI;
    }

    /**
     * @param customerCI the customerCI to set
     */
    public void setCustomerCI(CustomerCI customerCI) {
        this.customerCI = customerCI;
    }

    @Resource(name = PolicyService.BEAN_DEFAULT)
    private PolicyService policyService;

    @Resource(name = CashCI.BEAN_DEFAULT)
    private CashCI cashCI;

    @Resource(name = LifeProductService.BEAN_DEFAULT)
    private LifeProductService lifeProductService;

}