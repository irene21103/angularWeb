package com.ebao.ls.crs.company.impl;

import javax.annotation.Resource;

import com.ebao.ls.crs.company.CRSEntityService;
import com.ebao.ls.crs.data.bo.CRSEntity;
import com.ebao.ls.crs.data.company.CRSEntityControlManListDAO;
import com.ebao.ls.crs.data.company.CRSEntityDAO;
import com.ebao.ls.crs.vo.CRSEntityVO;
import com.ebao.ls.foundation.service.GenericServiceImpl;

public class CRSEntityServiceImpl extends GenericServiceImpl<CRSEntityVO, CRSEntity, CRSEntityDAO>
		implements CRSEntityService {

	@Resource(name = CRSEntityControlManListDAO.BEAN_DEFAULT)
	private CRSEntityControlManListDAO controlManListDAO;

	@Override
	protected CRSEntityVO newEntityVO() {
		return new CRSEntityVO();
	}

	@Override
	public CRSEntityVO findByCertiCode(String certiCode) {
		CRSEntity entity = dao.findByCertiCode(certiCode);
		if(entity == null)
			return null;
		return this.convertToVO(entity);
	}

}
