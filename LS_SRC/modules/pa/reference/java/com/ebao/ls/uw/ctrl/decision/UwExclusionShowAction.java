package com.ebao.ls.uw.ctrl.decision;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.foundation.module.util.i18n.CodeTable;
import com.ebao.foundation.module.util.i18n.StringResource;
import com.ebao.ls.cs.commonflow.ctrl.letter.CsUWIssuesActionHelper;
import com.ebao.ls.cs.letter.ctrl.PosLetterHelper;
import com.ebao.ls.escape.helper.EscapeHelper;
import com.ebao.ls.letter.common.CommonLetterService;
import com.ebao.ls.notification.message.WSLetterUnit;
import com.ebao.ls.pa.nb.ctrl.letter.NbLetterHelper;
import com.ebao.ls.pa.nb.ctrl.letter.NbNotificationHelper;
import com.ebao.ls.pa.nb.util.NBUtils;
import com.ebao.ls.pa.pub.TableCst;
import com.ebao.ls.pa.pub.bs.PolicyHolderService;
import com.ebao.ls.pa.pub.bs.PolicyService;
import com.ebao.ls.pub.CodeCst;
import com.ebao.ls.pub.cst.Template;
import com.ebao.ls.uw.ctrl.UwGenericAction;
import com.ebao.ls.uw.ctrl.pub.UwActionHelper;
import com.ebao.ls.uw.ds.UwMedicalLetterService;
import com.ebao.ls.uw.ds.UwPolicyService;
import com.ebao.ls.uw.vo.UwExclusionVO;
import com.ebao.ls.uw.vo.UwPolicyVO;
import com.ebao.pub.annotation.PrdAPIUpdate;
import com.ebao.pub.framework.ExceptionFactory;
import com.ebao.pub.framework.GenericException;
import com.ebao.pub.util.StringUtils;
import com.ebao.pub.wincony.utils.CommonUtils;
//import com.ebao.ls.pa.pub.service.PolicyService;
//import com.ebao.ls.notification.extension.letter.nb.index.FmtUnb0100IndexVO;

/**
 * <p>Title: 核保作業</p>
 * <p>Description:保項批註顯示</p>
 * <p>Copyright: Copyright (c) 2015</p>
 * <p>Company: TGL Co., Ltd.</p>
 * <p>Create Time: Jul 9, 2015</p> 
 * @author 
 * <p>Update Time: Jul 9, 2015</p>
 * <p>Updater: simon.huang</p>
 * <p>Update Comments: </p>
 */
public class UwExclusionShowAction extends UwGenericAction {
	
	private Logger logger = Logger.getLogger(UwExclusionShowAction.class);

	public static final String BEAN_DEFAULT = "/uwExclusionShow";

	@Resource(name = UwPolicyService.BEAN_DEFAULT)
	protected UwPolicyService uwPolicyDS;

	@Resource(name = PolicyService.BEAN_DEFAULT)
	private PolicyService policyService;

	@Resource(name = PolicyHolderService.BEAN_DEFAULT)
	protected PolicyHolderService policyHolderService;

	@Resource(name = UwActionHelper.BEAN_DEFAULT)
	protected UwActionHelper uwActionHelper;

	@Resource(name = NbNotificationHelper.BEAN_DEFAULT)
	private NbNotificationHelper nbNotification;

	@Resource(name = UwMedicalLetterService.BEAN_DEFAULT)
	private UwMedicalLetterService uwMedicalLetterService;

	@Resource(name = NbNotificationHelper.BEAN_DEFAULT)
	private NbNotificationHelper nbNotificationHelper;

	@Resource(name = CommonLetterService.BEAN_DEFAULT)
	protected CommonLetterService commonLetterService;

	@Resource(name = NbLetterHelper.BEAN_DEFAULT)
	private NbLetterHelper nbLetterHelper;
	
	@Resource(name = PosLetterHelper.BEAN_DEFAULT)
	private PosLetterHelper posLetterHelper;

	public static String previewNewLetter = "previewNew";
	
	@Resource(name = CsUWIssuesActionHelper.BEAN_DEFAULT)
	private CsUWIssuesActionHelper csUWIssuesActionHelper;

	protected final static long TEMPLATE_ID = 20023L;

	/**
	 * @param mapping
	 * @param form
	 * @param request
	 * @param res
	 * @return
	 * @throws GenericException
	 */
	@Override
	@PrdAPIUpdate
	public ActionForward uwProcess(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws GenericException {
		try {
			UwProductExclusionForm uwProductExclusionForm = (UwProductExclusionForm) form;
			String isIframe = EscapeHelper.escapeHtml(request.getParameter("isIframe"));
			uwProductExclusionForm.setIsIframe(isIframe);
			String reqFrom = EscapeHelper.escapeHtml(request.getParameter("reqFrom"));
			uwProductExclusionForm.setCsUw(Boolean.parseBoolean(request.getParameter("isCsUw")));
			int bizCategoryId = CodeCst.BIZ_CATEGORY_UNB;
			String chgItemsDesc = null;
			if(uwProductExclusionForm.isCsUw() || org.apache.commons.lang.StringUtils.equals(reqFrom, "pos")){
				bizCategoryId = CodeCst.BIZ_CATEGORY_POS;
				UwPolicyVO uwPolicyVO = uwPolicyDS.findUwPolicy(uwProductExclusionForm.getUnderwriteId());
				chgItemsDesc = posLetterHelper.getAlterationItemsDesc(uwPolicyVO.getChangeId()); // 加保/復效/變更 
			}
			Map<String, String> exclusionCodeBindDesc = new LinkedHashMap<String, String>();
			String rowData[][] = CodeTable.getData(TableCst.T_EXCLUSION_CODE
							, "Version_Type is not null and category_id = " + bizCategoryId
							, null, "EXCLUSION_CODE asc", CommonUtils.getLangId()
							, "['EXCLUSION_CONTENT']", null);

			//
			String subAction = uwProductExclusionForm.getSubAction();
			if (previewNewLetter.equals(subAction)) {
				// 若沒有執行過新增，預覽列印內的資料帶不出來 
				boolean flag = checkUwExclusion(uwProductExclusionForm);
				if(!flag){
					String sDesc = StringResource.getStringData("ERR_" + 20510020126L, request);
					request.setAttribute("error", sDesc);
					return mapping.findForward("previewError");
				}
				
				Long policyId = uwProductExclusionForm.getPolicyId();
				Long insuredId = uwProductExclusionForm.getInsuredId();
				Long underwriteId = uwProductExclusionForm.getUnderwriteId();

				/**信函預覽**/
				try {
					logger.error("UwExclusionShowAction.previewNew.policyId="
							+policyId+",insuredId="+insuredId
							+",underwriteId="+underwriteId);
					WSLetterUnit unit = null;
					Long templateId = Template.TEMPLATE_20023.getTemplateId();
					// 保全核保 或 保全次標加費 
					if(uwProductExclusionForm.isCsUw() || org.apache.commons.lang.StringUtils.equals(reqFrom, "pos")){
						Long changeId = Long.valueOf(com.ebao.ls.encode.encoder.Encode.encodeForHTML(request).getParameter("changeId"));
						unit = csUWIssuesActionHelper.prepareExtensionVOPos0030PreView(policyId, changeId
										, underwriteId, insuredId);
						templateId = csUWIssuesActionHelper.TEMPLATE_ID__EXCLUSION;
					}else{
						unit = uwActionHelper.getFmtUnb0381Letter(policyId, underwriteId, insuredId);
					}
					logger.error("UwExclusionShowAction.previewNew.unit="+unit);
					
					commonLetterService.previewLetterSetupPDFHeader(request, response, templateId, unit);
				} catch (Exception e) {
					com.ebao.pub.util.Log.error(this.getClass(), e.getMessage());
					request.setAttribute("error", NBUtils.getTWMsg("MSG_1262038"));
					return mapping.findForward("previewError");
				}
			} else {

				for (int i = 0; i < rowData.length; i++) {
					String exclusionCode = rowData[i][0];
					String descLangTemplate = rowData[i][3];
					if(uwProductExclusionForm.isCsUw() || org.apache.commons.lang.StringUtils.equals(reqFrom, "pos")){
						descLangTemplate = StringUtils.replace(descLangTemplate, "{chgItemsDesc}", chgItemsDesc);
					}
					exclusionCodeBindDesc.put(exclusionCode, StringUtils.nullToEmpty(descLangTemplate));
				}
				// 設定 批註template資料
				request.setAttribute("exclusionCodeBindDesc", exclusionCodeBindDesc);

				//add by tgl151   pos-頁面進入
				if (reqFrom != null && reqFrom.equals("pos")) {
					String policyId = EscapeHelper.escapeHtml(request.getParameter("policyId"));
					String underwriteId = EscapeHelper.escapeHtml(request.getParameter("underwriteId"));
					String itemId = EscapeHelper.escapeHtml(request.getParameter("itemId"));
					String changeId = EscapeHelper.escapeHtml(request.getParameter("changeId"));
					String policyChgId = EscapeHelper.escapeHtml(request.getParameter("policyChgId"));

					Long[] underwriteIds = new Long[10];
					underwriteIds[0] = Long.parseLong(underwriteId);

					uwProductExclusionForm.setPolicyId(Long.parseLong(policyId));
					uwProductExclusionForm.setUnderwriterId(underwriteIds);
					EscapeHelper.escapeHtml(request).setAttribute("reqFrom", reqFrom);
					EscapeHelper.escapeHtml(request).setAttribute("changeId", changeId);
					EscapeHelper.escapeHtml(request).setAttribute("policyChgId", policyChgId);
					EscapeHelper.escapeHtml(request).setAttribute("itemId", itemId);
					//request.setAttribute(com.ebao.pub.Cst.ACTION_FORM,uwProductExclusionForm);//add by tgl151 
				}
			}
			return mapping.findForward("display");
		} catch (Exception ex) {
			throw ExceptionFactory.parse(ex);
		}
	}
	private boolean checkUwExclusion(UwProductExclusionForm form){
		boolean flag = true;
		Collection<UwExclusionVO> resultList = uwPolicyDS.
						findUwExclusionEntitis(form.getUnderwriteId());
		if(resultList == null || resultList.isEmpty()){
			flag = false;
		}
		return flag;
	}
}