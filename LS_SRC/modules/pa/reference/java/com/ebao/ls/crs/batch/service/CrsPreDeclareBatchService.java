package com.ebao.ls.crs.batch.service;


import java.util.Date;
import java.util.List;

import com.ebao.foundation.commons.exceptions.GenericException;
import com.ebao.ls.crs.batch.vo.CRSTempVO;
import com.ebao.ls.crs.vo.CRSPartyLogVO;
import com.ebao.ls.fatca.vo.FatcaDueDiligenceListVO;

public interface CrsPreDeclareBatchService {
    public final static String BEAN_DEFAULT = "crsPreDeclareBatchService";
    
    public final static String SURVEY_SYSTEM_TYPE_FATCA = "FATCA";
    public final static String SURVEY_SYSTEM_TYPE_CRS = "CRS";
    public final static String SURVEY_SYSTEM_TYPE_ALL = "CRS/FATCA";
    
    /**
	 * CRS LOG建檔與主檔提交
	 * @param voList
	 * @param userId 
	 * @param processDate
	 * @throws Exception
	 */
    public void createCrsRecord(String id, String flag, Long userId, Date processDate) throws Exception;
    
    
    /**
     * 從t_customer(自然人) 或 t_company_customer(法人) 撈取 party_id
     */
    public Long getPartyIdByCertiCode(String certiCode, String crsCode) throws Exception;
   
}
