package com.ebao.ls.liaRoc.batch;

import java.util.Date;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.scheduling.annotation.Scheduled;

import com.ebao.ls.cs.logger.ApplicationLogger;
import com.ebao.ls.cs.logger.helper.AppLogDataHelper;
import com.ebao.ls.liaRoc.ci.LiaRocUploadCommonService;
import com.ebao.ls.pa.nb.batch.NBSpringCorntabTrigger;
import com.ebao.pub.util.DateUtils;

/**
 * <p>Description : PCR-338437-針對當日完成預收覆核完成或預收輸入完成的保單，在16:00、20:00、21:30定時批次作業產生公會收件通報檔</p>
 * <p>Created By : 　Kathy Yeh</p>
 * <p>Create Time : Oct 18, 2019</p>
 */

public class LiaRocNbTransferBatchTrigger extends NBSpringCorntabTrigger {

	private Logger logger = Logger.getLogger(getClass());

	@Resource(name = LiaRocNbTransferBatch.BEAN_DEFAULT)
	private LiaRocNbTransferBatch liaRocNbTransferBatch;
	
	@Resource(name = LiaRocNbTransferBatch2020.BEAN_DEFAULT)
	private LiaRocNbTransferBatch2020 liaRocNbTransferBatch2020;

	@Resource(name = LiaRocUploadCommonService.BEAN_DEFAULT)
	private LiaRocUploadCommonService liaRocUploadCommonService;
	
	@Override
	@Scheduled(cron = "0 0 16,20 * * *") // 16:00 20:00 every day. 
	protected void execute() {

		try {

			if (super.isBatch()) {
				// 執行日小於新式公會通報生效日期(2021/07/01)，要執行舊式公會更正通報
				if (!liaRocUploadCommonService.isLiaRocV2020Process()){
					logger.info("新契約公會收件通報排程-Start");
					liaRocNbTransferBatch.mainProcess();
				}
				logger.info("新契約新式公會收件通報排程-Start");
				liaRocNbTransferBatch2020.mainProcess();
			}
		} catch (Exception e) {
			logger.error(e);
		}
	}

	//排程2 21:30
	//2023/02/15 因承保寫入明細執行時間21:30~21:55,後執行批前維護shutdown batch服務,導致送liaroc20最一批執行時間為2100
	//故增加一批20:30(提早寫入明細檔)
	@Scheduled(cron = "0 30 20,21 * * *") // 21:30 every day.
	protected void execute2() {
		
		//寫入新契約收件明細
		this.execute();
		
		//寫入新契約承保明細
		//PCR_454251
		//排程：21:30最後一批公會收件通報資料產生完後，接著產生新契約當日承保的承保公會通報資料。
		this.execute4();
	}

	//排程4 08:30
	@Scheduled(cron = "0 30 8 * * *") // 08:30 every day.
	protected void execute4() {
		//PCR_454251
		//因休假日可能會提早維護導致21:30批次未執行,增加排程08:30執行補送
		try {
			if (super.isBatch()) {
				liaRocNbTransferBatch2020.changeType10Proces();
			}
		} catch (Exception e) {
			logger.error(e);
		}
	}
	
	//排程3 08:00
	@Scheduled(cron = "0 0 8 * * *") // 08:00 every day.
	protected void execute3() {
		try {

			if (super.isBatch()) {
		    	
		    	ApplicationLogger.clear();
				ApplicationLogger.setSystemCode(AppLogDataHelper.SYSTEM_CODE_UNB);
				ApplicationLogger.setJobName("Undo後同天生效/撤件-隔天自動通報批次");
				ApplicationLogger.setPolicyCode("LiarocNbBatch2020");
				ApplicationLogger.setOptionId(DateUtils.date2String(new Date(), "yyyyMMdd HHmmss"));

				if (!liaRocUploadCommonService.isLiaRocV2020Process()){
					liaRocNbTransferBatch.changeType13Process();
					liaRocNbTransferBatch.changeType14Process();
				}
				liaRocNbTransferBatch2020.changeType13Process();
				liaRocNbTransferBatch2020.changeType14Process();

		        ApplicationLogger.flush();
			}
		} catch (Exception e) {
			logger.error(e);
			ApplicationLogger.clear();
		} 
	}

}
