package com.ebao.ls.uw.ctrl.qm;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;

import com.ebao.foundation.common.lang.exception.ExceptionUtils;
import com.ebao.foundation.core.logging.Log;
import com.ebao.foundation.module.util.beanutils.BeanUtils;
import com.ebao.ls.pa.nb.bs.KSystemService;
import com.ebao.ls.pa.pub.auth.PageButtonControlServiceImpl;
import com.ebao.ls.pty.ci.DeptCI;
import com.ebao.ls.pub.auth.AuthItemCst;
import com.ebao.ls.pub.cst.YesNo;
import com.ebao.ls.uw.ds.CSQmQueryService;
import com.ebao.ls.uw.ds.UwQmQueryService;
import com.ebao.ls.uw.vo.AbstractQualityManageVO;
import com.ebao.ls.uw.vo.CSQualityManageListVO;
import com.ebao.ls.uw.vo.QualityManageListVO;
import com.ebao.ls.ws.vo.unb.unb240.QmDataCIVO;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.framework.GenericAction;


public class QmMainAction extends GenericAction {

    @Resource(name = UwQmQueryService.BEAN_DEFAULT)
    private UwQmQueryService uwqqService;
    @Resource(name = CSQmQueryService.BEAN_DEFAULT)
    private CSQmQueryService csqqService;
    
    @Resource(name = DeptCI.BEAN_DEFAULT)
	private DeptCI deptCI;
    
    @Resource(name = PageButtonControlServiceImpl.BEAN_DEFAULT)
    private PageButtonControlServiceImpl pageButtonControlService;
    
    @Resource(name = KSystemService.BEAN_DEFAULT)
    private KSystemService kServ;
    
    protected static final String ACTION_SEARCH = "search";
    protected static final String ACTION_ADD = "add";
    protected static final String ACTION_DELETE_DIALOG = "deldlg";
    protected static final String ACTION_DELETE = "delete";
    protected static final String ACTION_WATCH = "watch";
    
    private static Log log = Log.getLogger(QmMainAction.class);
    
    @Override
    public ActionForward process(ActionMapping mapping, ActionForm form,
                    HttpServletRequest request, HttpServletResponse response)
                    throws Exception {
        request.setAttribute("qmQueryResult", null);
        UwQualityManageForm uwQualityManageForm = (UwQualityManageForm)form;
        String forward = "display";
        
        boolean isAddButtonRole = pageButtonControlService.hasAccessRight(AuthItemCst.AUTH_ITEM_ADD_BUTTON);
        boolean isDeleteButtonRole =  pageButtonControlService.hasAccessRight(AuthItemCst.AUTH_ITEM_DELETE_BUTTON);
        uwQualityManageForm.setIsAddButtonRole( isAddButtonRole );
        uwQualityManageForm.setIsDeleteButtonRole( isDeleteButtonRole );

        if(ACTION_DELETE_DIALOG.equals(uwQualityManageForm.getActionType())){
            forward = "deldlg";
        }
        if(ACTION_DELETE.equals(uwQualityManageForm.getActionType()) && isDeleteButtonRole ){
            Long listId = uwQualityManageForm.getQualityManageListVO().getListId();
            String desc = uwQualityManageForm.getQualityManageListVO().getDeleteReason();
            deleteQualityManageList(listId, desc);
            uwQualityManageForm.setActionType("search");
        }
        if(ACTION_SEARCH.equals(uwQualityManageForm.getActionType())){
        	if (!uwQualityManageForm.getCondQualityManageType().trim().equals("") || 
        		!uwQualityManageForm.getCondName().trim().equals("") ||
        		!uwQualityManageForm.getCondCertiCode().trim().equals("") ||
        		!uwQualityManageForm.getCondRegisterCode().trim().equals("")) {
	            List<QualityManageListVO> list = uwqqService.pagerQuery(uwQualityManageForm.getCondQualityManageType(),
	            														uwQualityManageForm.getPager(),
	                                                                    uwQualityManageForm.getCondName(),
	                                                                    uwQualityManageForm.getCondCertiCode(),
	                                                                    uwQualityManageForm.getCondRegisterCode());
	            
	            request.setAttribute("qmQueryList", list);
	            
	            if (list != null && list.size() > 0) {
	                request.setAttribute("qmQueryResult", true);
	            } else {
	                request.setAttribute("qmQueryResult", false);
	            }
        	}
            
        	//request.setAttribute("qmQueryResult", false);
            forward = "display";
        }
        if(ACTION_ADD.equals(uwQualityManageForm.getActionType()) && isAddButtonRole){
            if (uwQualityManageForm.getQualityManageListVO().getListId() != null) {
                QualityManageListVO qualityManageListVO = uwqqService.load(uwQualityManageForm.getQualityManageListVO().getListId());
                qualityManageListVO.setListId(null);
                qualityManageListVO.setInsertedBy(null);
                qualityManageListVO.setInsertTime(null);
                qualityManageListVO.setInsertTimestamp(null);
                qualityManageListVO.setUpdatedBy(null);
                qualityManageListVO.setUpdateTime(null);
                qualityManageListVO.setUpdateTimestamp(null);
                uwQualityManageForm.setQualityManageListVO(qualityManageListVO);
            }
            Integer bizCategory = deptCI.getBizCategoryByUserId(AppContext.getCurrentUser().getUserId());
            
            request.setAttribute("bizCategory", bizCategory);
            forward = "add";
        }
        if(ACTION_WATCH.equals(uwQualityManageForm.getActionType())){
            QualityManageListVO qualityManageListVO = uwqqService.load(uwQualityManageForm.getQualityManageListVO().getListId());
            uwQualityManageForm.setQualityManageListVO(qualityManageListVO);
            request.setAttribute("correctOrNot", "noChange");
            uwQualityManageForm.setActionType("add");
            forward = "add";
        }
        
        return mapping.findForward(forward);
    }
    
    private void deleteQualityManageList(Long listId, String desc){
        QualityManageListVO qualityManageListVO = uwqqService.load(new Long(listId));
        java.util.List<QmDataCIVO> qmDataList = new LinkedList<QmDataCIVO>();
        try {
            
            // Mark Primary Record as DELETE
            QmDataCIVO civo = new QmDataCIVO();
            BeanUtils.copyProperties(civo, qualityManageListVO);
            civo.setIsDelete(YesNo.YES_NO__YES);
            civo.setDeleteReason(desc);
            qmDataList.add(civo);
            boolean result = uwqqService.delete(listId, desc);
            if (uwqqService.isUnb() && !result) csqqService.delete(listId, desc);
            
            // IR 260448 -  Mark Related Records as DELETE
            List<AbstractQualityManageVO> subVOList = queryBySourceListId(listId, YesNo.YES_NO__NO);
            if(subVOList != null && subVOList.size() > 0) {
                for(AbstractQualityManageVO subVO : subVOList) {
                    QmDataCIVO subCIVO = new QmDataCIVO();
                    BeanUtils.copyProperties(subCIVO, subVO);
                    subCIVO.setIsDelete(YesNo.YES_NO__YES);
                    subCIVO.setDeleteReason(desc);
                    qmDataList.add(subCIVO);
                    deleteByListId(subVO, subVO.getListId(), desc);
                }
            }
            
            kServ.sendQualityManage(qmDataList);
            
        } catch (Exception ex) {
            log.error(String.format("[cause error when delete QM Lists to the K-SYS ] list_id := %d, delete_reason := %s", listId, desc));
            log.error(ExceptionUtils.getFullStackTrace(ex));
        }
    }
    
    private List<AbstractQualityManageVO> queryBySourceListId(Long sourceListId, String isDelete) {

        List<AbstractQualityManageVO> voList = new ArrayList<AbstractQualityManageVO>();

        Criterion cSoruceListId = Restrictions.eq("sourceListId", sourceListId);
        Criterion sIsDelete = Restrictions.eq("isDelete", isDelete);
        
        List<QualityManageListVO> uwVoList = uwqqService.find(cSoruceListId, sIsDelete);
        if(uwVoList != null && uwVoList.size() > 0) {
            voList.addAll(uwVoList);
        }
        
        List<CSQualityManageListVO> csVoList = csqqService.find(cSoruceListId, sIsDelete);
        if(csVoList != null && csVoList.size() > 0) {
            voList.addAll(csVoList);
        }
        
        return voList;
        
    }
    
    private void deleteByListId (AbstractQualityManageVO abstractVO, Long listId, String deleteReason) {
        if (abstractVO instanceof CSQualityManageListVO) {
            csqqService.delete(listId, deleteReason);
        } else {
            uwqqService.delete(listId, deleteReason);
        }
    }

}
