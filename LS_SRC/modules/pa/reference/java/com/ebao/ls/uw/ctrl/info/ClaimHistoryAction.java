package com.ebao.ls.uw.ctrl.info;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.ls.clm.ci.ClaimCI;
import com.ebao.ls.clm.ci.ClaimQueryInfoCIVO;
import com.ebao.ls.pa.nb.bs.ODSService;
import com.ebao.ls.pa.nb.ci.ValidatorService;
import com.ebao.ls.pa.pub.bs.PolicyService;
import com.ebao.ls.pa.pub.vo.InsuredVO;
import com.ebao.ls.pa.pub.vo.PolicyPersonVO;
import com.ebao.ls.product.service.LifeProductService;
import com.ebao.ls.pty.ci.CustomerCI;
import com.ebao.ls.pty.ci.EmployeeCI;
import com.ebao.ls.pty.data.bo.Customer;
import com.ebao.ls.pty.data.query.CustomerDao;
import com.ebao.ls.pub.CodeCst;
//import com.ebao.pub.framework.GenericAction;
import com.ebao.ls.qry.ds.QryBaseVO;
import com.ebao.ls.qry.ds.policy.ClaimInfoVO;
import com.ebao.ls.qry.ds.policy.PolicyDS;
import com.ebao.ls.uw.ds.UwInsureHistoryService;
import com.ebao.ls.ws.vo.clm.clm070.ClaimStatusVO;
import com.ebao.ls.ws.vo.clm.clm070.DiagnosisVO;
import com.ebao.ls.ws.vo.clm.clm070.PolicyDataVO;
import com.ebao.pub.framework.ExceptionFactory;
import com.ebao.pub.framework.GenericAction;

/**
 * talks to Domain Service Layer or forwards to another page according to
 * predefined business logic or rules.
 */

public class ClaimHistoryAction extends GenericAction {
	// public static final String BEAN_DEFAULT = "/uw/policy.Policy";
	public static final String BEAN_DEFAULT = "uwClaimHistory";

	public static final String CLAIM_INFO_LIST_1 = "claimInfoList_1";
	public static final String CLAIM_INFO_LIST_2 = "claimInfoList_2";
	public static final String POLICY_PERSONVO_LIST = "policyPersonvoList";
	public static final String CLAIM_INFO_STATUS = "claimInfoStatus";
	public static final String CLAIM_INSURED_NAME = "claimInsuredName";

	@Resource(name = UwInsureHistoryService.BEAN_DEFAULT)
	protected UwInsureHistoryService uwInsureHistoryService;

	@Resource(name = ClaimCI.BEAN_DEFAULT)
	public ClaimCI claimCI;
	
	@Resource(name = CustomerCI.BEAN_DEFAULT)
	public CustomerCI customerCI;

	public ClaimCI getClaimCI() {
		return claimCI;
	}

	@Resource(name = ODSService.BEAN_DEFAULT)
	protected ODSService paODSService;

	@Override
	public ActionForward process(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		try {
			PolicyForm aForm = (PolicyForm) form;
			// 2018/02/01 Add : YCSu
			// [artf410579](Internal Issue 207219)[UAT2]未產生TGL理賠歷史資料
			
			// 2018/02/27 Marked : YCSu
			// [artf410579](Internal Issue 225018)[UAT2][eBao]理賠歷史資訊--團險的保單號碼--不需出現連結,請移除連結！
//			String actionType = com.ebao.ls.encode.encoder.Encode.encodeForHTML(request).getParameter("actionType");
//			if(!StringUtils.isNullOrEmpty(actionType)) {
//				if(org.apache.commons.lang.StringUtils.equalsIgnoreCase("develop", actionType)) {
//					String errorMsg = StringResource.getStringData("MSG_1262934", request); //系統頁面程式開發中
//					
//					aForm.setRetMsg(errorMsg);
//					request.setAttribute("error", errorMsg);
//					// return mapping.findForward("error");
//				}				
//			}else {
//				request.setAttribute("error", "");
//			}			
			// String tabType = com.ebao.ls.encode.encoder.Encode.encodeForHTML(request).getParameter("tabType");
			Long policyId = (Long) request.getAttribute("policyId");
			// Long policyId = 505L;
			if (policyId == null) {
				policyId = Long.valueOf(com.ebao.ls.encode.encoder.Encode.encodeForHTML(request).getParameter("policyId"));
				request.setAttribute("policyId", policyId);
				aForm.setPolicyId(policyId);
			}

			QryBaseVO qryBaseVO = new QryBaseVO();
			qryBaseVO.setPolicyId(policyId);

			// aForm.setClaimInfo(qryPolicyDS.qryClaim(qryBaseVO));
			String insuredName = null;
			// List<ClaimInfoVO> claimInfo = (List<ClaimInfoVO>) aForm.getClaimInfo();
			
			// 2018/03/23 Modify : YCSu
			// 借用：[artf410579](Internal Issue 189705)[UAT2][eBao]保單0052755250 須臨分，進入再保畫面點臨分未帶出相對應的再保公司
			/* 2018/03/19 Meeting : 
			 * +Title AND 調 Data 的順序為 : 
			 * (1). 易保系統的理賠紀錄 : claimInfo_1
			 * (2). 舊系統理賠紀錄(部分導入) : personList
			 * (3). 團險系統的理賠紀錄 : claimInfo_2
			*/
			List<ClaimInfoVO> claimInfo_1 = new ArrayList<ClaimInfoVO>();
			List<ClaimInfoVO> claimInfo_2 = new ArrayList<ClaimInfoVO>();
			
			List<PolicyPersonVO> personList = new ArrayList<PolicyPersonVO>();
			// for(ClaimInfoVO claimInfoVO : claimInfo){
			// //get name language
			// insuredName = claimInfoVO.getCaseStatus();
			// String langId = AppContext.getCurrentUser().getLangId();
			// String caseStatusName = uwInsureHistoryService.getNameEngToLang(insuredName,
			// langId);
			// claimInfoVO.setCaseStatus(caseStatusName);
			// }

			// 保單找所有被保人;
			List<InsuredVO> claimInsured = uwInsureHistoryService.findInsuredById(String.valueOf(policyId));
			// 找所有被保人的所有理賠記錄保單;
			for (InsuredVO insuredVO : claimInsured) {
				
				List<ClaimQueryInfoCIVO> claimInfoList = new ArrayList<ClaimQueryInfoCIVO>();
				
				if( StringUtils.isNotEmpty( insuredVO.getCertiCode() ) ) {
					
					List<Customer> customerList = new CustomerDao().findIndivCustomerByTGLCondition( insuredVO.getCertiCode(),null,null);
					
					for( Customer customerVO : customerList  ) {
						
						if( customerVO != null && customerVO.getCustomerId() != null) {
							/* claimInfoh*/
							ClaimQueryInfoCIVO[] claimInfoh = getClaimCI().retrieveClaimHistroryByInsured( customerVO.getCustomerId()).getClaimInfos();
							
							if(claimInfoh != null) {
								for( ClaimQueryInfoCIVO vo:claimInfoh ) {
									vo.setOfficerName(insuredVO.getName());
									vo.setCertiCode(insuredVO.getCertiCode());
									claimInfoList.add(vo);
								}
							}
						}
					}
				}
				
				//以舊統一證號查詢理賠記錄
				if( StringUtils.isNotEmpty( insuredVO.getOldForeignerId() )) {
	
					List<Customer> customerList = new CustomerDao().findIndivCustomerByTGLCondition( insuredVO.getOldForeignerId(),null,null);
					for( Customer customerVO : customerList ) {

						if( customerVO != null && customerVO.getCustomerId() != null) {
						
							ClaimQueryInfoCIVO[] claimInfoh1 = getClaimCI().retrieveClaimHistroryByInsured( customerVO.getCustomerId()).getClaimInfos();
							if( claimInfoh1 != null) {
								for( ClaimQueryInfoCIVO vo : claimInfoh1 ) {
									vo.setOfficerName(insuredVO.getName());
									vo.setCertiCode(insuredVO.getOldForeignerId());
									claimInfoList.add(vo);
								}
							}
						}
					}
				}

				if (claimInfoList.size() != 0 ) {
					for ( ClaimQueryInfoCIVO claimCIVO : claimInfoList ) {
						ClaimInfoVO vo = new ClaimInfoVO();
						vo.setCaseId(claimCIVO.getCaseId());
						vo.setAccidentTime(claimCIVO.getAccidentTime());
						vo.setCaseStatus(claimCIVO.getCaseStatus());
						if (claimCIVO.getCaseNo() == null) {
							vo.setCaseNo("-");
						} else {
							vo.setCaseNo(claimCIVO.getCaseNo());
						}

						vo.setDiagnosisId(claimCIVO.getDiagnosisId());

						vo.setClaimType(claimCIVO.getClaimType());
						// vo.setOfficerName(claimCIVO.getOfficerName());
						vo.setOfficerName(insuredVO.getName());
						vo.setPolicyNo(claimCIVO.getPolicyNo());
						vo.setPaymentDates(claimCIVO.getPaymentDates());
						vo.setPaymentStatus(claimCIVO.getPaymentStatus());

						// 2016 09 04 Victor CASE_STATUS__SETTLED表示理賠完成，才需要顯示理賠金額 ;
						// 2017-12-13 Modify : YCSu
						// [artf410579](Internal Issue 215360)[UAT2]理賠歷史資訊中未顯示理賠金額
						// if(String.valueOf(CodeCst.CASE_STATUS__SETTLED).equals(claimCIVO.getCaseStatus())){
						vo.setPolicyAmounts(claimCIVO.getPolicyAmounts());
						// }
						vo.setStatusDate(claimCIVO.getStatusDate());
						vo.setNoficicationDate(claimCIVO.getNoficicationDate());
						vo.setProductNames(claimCIVO.getProductNames());
						vo.setPendingDocuments(claimCIVO.getPendingDocuments());
						//PCR-415346 新增受理理賠CERTI_CODE
						vo.setCertiCode(claimCIVO.getCertiCode());
						claimInfo_1.add(vo);
					}
				}
				
				/* 團險 旅平險*/
				// 系統調用接口將服務管理平台取得被保人的團險 旅平險理賠記錄查詢 ;
				List<ClaimQueryInfoCIVO> claimInfoODS = paODSService.getClaimByCertiCode(insuredVO.getCertiCode());
				if( StringUtils.isNotEmpty( insuredVO.getOldForeignerId() )) {
					claimInfoODS.addAll(paODSService.getClaimByCertiCode(insuredVO.getOldForeignerId()));
				}
				
				if (claimInfoODS != null) {
					for (ClaimQueryInfoCIVO claimCIVO : claimInfoODS) {

						ClaimInfoVO vo = new ClaimInfoVO();
						vo.setKsysIndi(CodeCst.YES_NO__YES);
						vo.setCaseId(claimCIVO.getCaseId());
						vo.setAccidentTime(claimCIVO.getAccidentTime());
						vo.setCaseStatus(claimCIVO.getCaseStatus());
						vo.setCaseNo(claimCIVO.getCaseNo());

						vo.setDiagnosisId(claimCIVO.getDiagnosisId());

						vo.setClaimType(claimCIVO.getClaimType());
						// vo.setOfficerName(claimCIVO.getOfficerName());
						vo.setOfficerName(claimCIVO.getOfficerName());
						vo.setPolicyNo(claimCIVO.getPolicyNo());
						vo.setPaymentDates(claimCIVO.getPaymentDates());
						vo.setPaymentStatus(claimCIVO.getPaymentStatus());

						// 2016 09 04 Victor CASE_STATUS__SETTLED表示理賠完成 才需要顯示理賠金額;
						// 2017/09/12 mark by simon 接口回傳狀態理賠/拒賠/退件，均代表結案，因此都顯示
						// if(String.valueOf(CodeCst.CASE_STATUS__SETTLED).equals(claimCIVO.getCaseStatus())){
						vo.setPolicyAmounts(claimCIVO.getPolicyAmounts());
						// }
						vo.setStatusDate(claimCIVO.getStatusDate());
						vo.setNoficicationDate(claimCIVO.getNoficicationDate());
						vo.setProductNames(claimCIVO.getProductNames());
						vo.setPendingDocuments(claimCIVO.getPendingDocuments());
						//PCR-415346 新增受理理賠CERTI_CODE 2021/07/06 add by Kathy
						vo.setCertiCode(claimCIVO.getCertiCode());
						if (claimCIVO.getCaseId() == null) {
							// 2016 0916 add by sunny in group policy not diagnosisId, use the field
							vo.setAuditDecision(claimCIVO.getAuditDecision());
						} else {
							vo.setAuditDecision("");
						}
						claimInfo_2.add(vo);
					}
				}
				
				List<ClaimStatusVO> claimStatusVoList = validatorService.getClaimStatusVoListByCertiCode(insuredVO.getCertiCode());
				if( StringUtils.isNotEmpty( insuredVO.getOldForeignerId() ) ) {
					claimStatusVoList.addAll( validatorService.getClaimStatusVoListByCertiCode( insuredVO.getOldForeignerId() ) );
				}
				
				if (claimStatusVoList != null) {
					//使用方法參照:ClaimCaseServiceImpl.doGroupClaim()
					for (ClaimStatusVO vo : claimStatusVoList) {
						
						String caseNo = vo.getCaseNo();
						String accidentName = vo.getAccidentName();
						Date accidentTime = vo.getAccidentTime();
						String certiCode = vo.getCertCode();
						String accidentReasonDesc = vo.getAccidentReasonDesc();
						String harm1Desc = vo.getHarm1Desc();
						String akindDesc = vo.getAkindDesc();
						List<DiagnosisVO> diagnosisVoList = vo.getDiagnosisList();
						StringBuffer accidentReasonDescSB = new StringBuffer();
						
						if( accidentReasonDesc != null && !"".equals(accidentReasonDesc)) {
							accidentReasonDescSB.append(accidentReasonDesc);
						}
						
						if( harm1Desc != null && !"".equals(harm1Desc) ) {
							if("".equals(accidentReasonDescSB.toString())) {
								accidentReasonDescSB.append(harm1Desc);
							}else {
								accidentReasonDescSB.append("/").append(harm1Desc);
							}
						}
						
						if( akindDesc != null && !"".equals(akindDesc) ) {
							if("".equals(accidentReasonDescSB.toString())) {
								accidentReasonDescSB.append(akindDesc);
							}else {
								accidentReasonDescSB.append("/").append(akindDesc);
							}
						}
						
						if( diagnosisVoList != null && diagnosisVoList.size() > 0) {
							StringBuffer diagnosisDest = new StringBuffer();
							
							for( DiagnosisVO dvo : diagnosisVoList) {
								diagnosisDest.append(dvo.getDiagnosisName()).append(",");
							}
							
							if(diagnosisDest.length() > 0) {
								diagnosisDest.deleteCharAt(diagnosisDest.length() - 1);
							}
							
							if( "".equals(accidentReasonDescSB.toString())) {
								accidentReasonDescSB.append(diagnosisDest.toString());
							}else {
								accidentReasonDescSB.append("/").append(diagnosisDest.toString());
							}
							
						}

						//理賠舊資料會在4/7會條件導入, 且已提供下列接口, 只是下列接口會含在eBao理賠的data, 建議舊資料使用下列接口, 
						//然後過篩在eBao理賠的data.(Policy_source<>’E’)
						//PolicySource è E:ebao , T:capsil, K:oracle
						String source = vo.getPolicySource();
						if ("E".equals(source) == false) {
							
							if (vo.getPolicyDataList() != null && vo.getPolicyDataList().size() > 0) {
								
								for (PolicyDataVO pvo : vo.getPolicyDataList()) {
									if (StringUtils.isNotEmpty(pvo.getPolicyCode())) {

											PolicyPersonVO policyPersonVO = new PolicyPersonVO();
											policyPersonVO.setCaseNo(caseNo);
											policyPersonVO.setName(accidentName);
											policyPersonVO.setLastClaimDate(accidentTime);
											policyPersonVO.setClaimPolicyCode(pvo.getPolicyCode());
											policyPersonVO.setAccidentReasonDesc(accidentReasonDescSB.toString());
											policyPersonVO.setCertiCode(certiCode);
											personList.add(policyPersonVO);
									}
								}
								
							} else {
								
								if (StringUtils.isNotEmpty(vo.getCaseNo())) {
									
										PolicyPersonVO policyPersonVO = new PolicyPersonVO();
										policyPersonVO.setCaseNo(caseNo);
										policyPersonVO.setName(accidentName);
										policyPersonVO.setLastClaimDate(accidentTime);
										policyPersonVO.setAccidentReasonDesc(accidentReasonDescSB.toString());
										policyPersonVO.setCertiCode(certiCode);
										personList.add(policyPersonVO);
								}
							}
						}
					}
				}
				
				/* 部分導入*/
				// 2018/03/14 Add : YCSu
				// [artf410579](Internal Issue 225067)[UAT2][eBao]理賠歷史資訊中未顯示KH保單理賠記錄！
				List<PolicyPersonVO> personSubList =policyDS.findHistoryPolicyByCertiCode(insuredVO.getCertiCode(), policyId); 
				if( StringUtils.isNotEmpty( insuredVO.getOldForeignerId() ) ) {
					personSubList.addAll( policyDS.findHistoryPolicyByCertiCode(insuredVO.getOldForeignerId(), policyId) );
				}
				
				if(personSubList != null && personSubList.size() > 0) {
					personList.addAll(personSubList);	
				}
				
				if (insuredVO.getName() != null && insuredName == null) {
					insuredName = insuredVO.getName();

				} else if (insuredVO.getName() != null) {
					insuredName += ", " + insuredVO.getName();
				} else {
					insuredName = "";
				}
			}
			if (claimInfo_1 != null) {
				/** 易保系統的理賠紀錄*/
				request.setAttribute(CLAIM_INFO_LIST_1, claimInfo_1);
			}
			if (personList != null) {
				/** 舊系統理賠紀錄 : 部分導入*/
				request.setAttribute(POLICY_PERSONVO_LIST, personList);				
			}
			if (claimInfo_2 != null) {
				/** 團險系統的理賠紀錄*/
				request.setAttribute(CLAIM_INFO_LIST_2, claimInfo_2);
			}	
			request.setAttribute(CLAIM_INFO_STATUS, "Y");
			request.setAttribute(CLAIM_INSURED_NAME, insuredName);			

			String forward = "display";
			return mapping.findForward(forward);
		} catch (Exception e) {
			throw ExceptionFactory.parse(e);
		}
	}

	@Resource(name = PolicyDS.BEAN_DEFAULT)
	private PolicyDS qryPolicyDS;

	@Resource(name = LifeProductService.BEAN_DEFAULT)
	private LifeProductService lifeProductService;
	
	@Resource(name = ValidatorService.BEAN_DEFAULT)
	private ValidatorService validatorService;

	public LifeProductService getLifeProductService() {
		return lifeProductService;
	}

	@Resource(name = EmployeeCI.BEAN_DEFAULT)
	private EmployeeCI employeeCI;

	@Resource(name = PolicyService.BEAN_DEFAULT)
	private PolicyService policyDS;

	@Resource(name = com.ebao.ls.pa.pub.service.PolicyService.BEAN_DEFAULT)
	com.ebao.ls.pa.pub.service.PolicyService policyService;

}
