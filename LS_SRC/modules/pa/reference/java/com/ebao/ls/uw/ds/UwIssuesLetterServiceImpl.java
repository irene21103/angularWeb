package com.ebao.ls.uw.ds;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;

import com.ebao.foundation.module.db.hibernate.HibernateSession3;
import com.ebao.foundation.module.util.i18n.CodeTable;
import com.ebao.ls.cmu.biz.vo.IssuesDocContentVO;
import com.ebao.ls.cmu.bs.outstandingissue.ManageLOIService;
import com.ebao.ls.cmu.bs.template.TemplateService;
import com.ebao.ls.cmu.pub.data.DocumentDao;
import com.ebao.ls.cmu.pub.data.DocumentRuleCodeDao;
import com.ebao.ls.cmu.pub.model.DocumentInput.DocumentPropsKey;
import com.ebao.ls.cmu.service.DocumentService;
import com.ebao.ls.foundation.dao.SQLBuilder;
import com.ebao.ls.foundation.dao.pager.PagerDaoHelper;
import com.ebao.ls.letter.common.CommonLetterService;
import com.ebao.ls.notification.extension.letter.nb.FmtUnb0120ExtensionVO;
import com.ebao.ls.notification.extension.letter.nb.FmtUnb0291ExtensionVO;
import com.ebao.ls.notification.extension.letter.nb.FmtUnb0370ExtensionVO;
import com.ebao.ls.notification.extension.letter.nb.index.FmtUnb0120IndexVO;
import com.ebao.ls.notification.extension.letter.nb.index.FmtUnb0291IndexVO;
import com.ebao.ls.notification.message.WSItemString;
import com.ebao.ls.notification.message.WSLetterUnit;
import com.ebao.ls.pa.nb.ci.ProposalCI;
import com.ebao.ls.pa.nb.ctrl.letter.NbLetterHelper;
import com.ebao.ls.pa.nb.ctrl.letter.NbNotificationHelper;
import com.ebao.ls.pa.nb.data.bo.DocumentRuleCode;
import com.ebao.ls.pa.nb.vo.ProposalRuleResultVO;
import com.ebao.ls.pa.pub.TableCst;
import com.ebao.ls.pa.pub.bs.InsuredService;
import com.ebao.ls.pa.pub.bs.PolicyService;
import com.ebao.ls.pa.pub.vo.InsuredVO;
import com.ebao.ls.pa.pub.vo.PolicyVO;
import com.ebao.ls.pa.pub.vo.UwAddInformLetterVO;
import com.ebao.ls.pa.pub.vo.UwSickFormLetterVO;
import com.ebao.ls.pty.ci.CompanyOrganCI;
import com.ebao.ls.pty.service.PartyService;
import com.ebao.ls.pub.CodeCst;
import com.ebao.ls.pub.cst.Template;
import com.ebao.ls.uw.ctrl.pub.UwActionHelper;
import com.ebao.pub.event.ods.FormattingMethod;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.framework.RTException;
import com.ebao.pub.util.DBean;
import com.ebao.pub.util.DateUtils;
import com.ebao.pub.web.pager.Pager;


public class UwIssuesLetterServiceImpl implements UwIssuesLetterService {
	/*  public static String nonuw = "Non-UW Issues:";
	  public static String uw = "UW Issues:";
	  public static String gender_M = "MR";
	  public static String gender_F = "MS";*/
	@Resource(name = PolicyService.BEAN_DEFAULT)
	private PolicyService policyService;

	@Resource(name = PartyService.BEAN_DEFAULT)
	private PartyService partyService;

	@Resource(name = DocumentService.BEAN_DEFAULT)
	private DocumentService documentService;

	@Resource(name = ManageLOIService.BEAN_DEFAULT)
	private ManageLOIService manageLOIDS;

	@Resource(name = ProposalCI.BEAN_DEFAULT)
	private ProposalCI proposalCI;

	@Resource(name = CompanyOrganCI.BEAN_DEFAULT)
	private CompanyOrganCI companyOrganCI;

	@Resource(name = DocumentDao.BEAN_DEFAULT)
	private DocumentDao documentDao;

	@Resource(name = CommonLetterService.BEAN_DEFAULT)
	private CommonLetterService commonLetterService;

	@Resource(name = UwSickFormLetterService.BEAN_DEFAULT)
	private UwSickFormLetterService uwSickFormLetterService;
	
	@Resource(name = UwSickFormItemService.BEAN_DEFAULT)
	private UwSickFormItemService uwSickFormItemService;

	@Resource(name = UwAddInformLetterService.BEAN_DEFAULT)
	private UwAddInformLetterService uwAddInformLetterService;

	@Resource(name = UwPolicyService.BEAN_DEFAULT)
	private UwPolicyService uwPolicyDS;
	
	@Resource(name = TemplateService.BEAN_DEFAULT)
	private TemplateService templateService;

    @Resource(name = DocumentRuleCodeDao.BEAN_DEFAULT)
    private DocumentRuleCodeDao documentRuleCodeDao;

	@Override
	public List<Map<String, Object>> queryAgents(Pager pager, Long policyId, String... producerRole) {
		SQLBuilder builder = new SQLBuilder();
		builder.write(" select a.agent_code from t_policy_producer_role r ");
		builder.write(" left join t_agent a on a.agent_id=r.producer_id ");
		builder.write(" where 1=1 ");
		builder.eq("policy_id", policyId);
		builder.in("producer_role", producerRole);
		return PagerDaoHelper.paging(HibernateSession3.currentSession(), builder, null);
	}

	@Override
	public Map<String, Object> getChannelInfo(Long policy) {
		SQLBuilder builder = new SQLBuilder();
		builder.write(" select o.channel_type, c.sales_channel_name , o2.channel_name ");
		builder.write(" , h.party_id,h.name,h.email,h.mobile_tel,m.policy_code");
		builder.write(" from t_contract_master m ");
		builder.write(" left join t_Channel_Org o on o.channel_id=m.channel_org_id ");
		builder.write(" left join t_sales_channel c on c.sales_channel_id=o.channel_type ");
		builder.write(" left join t_Channel_Org o2 on o2.channel_id=o.parent_id ");
		builder.write(" left join t_policy_holder h on m.policy_id=h.policy_id ");
		builder.write(" where 1=1 ");
		builder.eq("m.policy_id", policy);
		List<Map<String, Object>> list = PagerDaoHelper.paging(HibernateSession3.currentSession(), builder, null);
		if (list.size() != 1) {
			throw new RuntimeException("getChannelInfo(policy) not one row");
		}
		return list.get(0);
	}

	@Override
	public void storeOID(Long[] issuelistId, Long documentId, String langId) {
		if (issuelistId != null && issuelistId.length > 0) {
			Long organId = Long.valueOf(AppContext.getCurrentUser().getOrganId());
			organId = companyOrganCI.getCompanyIdByOrganId(organId);
			IssuesDocContentVO contentVO = null;
			for (Long element : issuelistId) {
				if (element.longValue() > 0) {
					contentVO = new IssuesDocContentVO();
					contentVO.setDocumentId(documentId);
					contentVO.setLangId(langId);
					contentVO.setOrganId(organId);
					contentVO.setToWhom(1L); // 1:proposer 2:agent
					Date localDate = AppContext.getCurrentUserLocalTime();
					Date lapseDate = DateUtils.addDay(localDate, 21);
					contentVO.setLapseDate(lapseDate);
					// ------------------ call service to get t_proposal_rule_result
					// data according to PK:listId
					contentVO.setIssueResultId(element);
					ProposalRuleResultVO ruleResult = proposalCI
									.loadProposalRuleResultById(element);
					if (ruleResult.getLetterContent() == null
									|| ruleResult.getLetterContent().length() == 0
									|| ruleResult.getLetterContent().equals("")) {
						contentVO.setDisplayContent(ruleResult.getViolatedDesc());
					} else {
						contentVO.setDisplayContent(ruleResult.getLetterContent());
					}
					contentVO.setTypeId(Long.valueOf(ruleResult.getRuleType().toString()));
					contentVO.setTypeName("Outstanding Issues"); // for user, they
					// needn't know the
					// nicety type name
					manageLOIDS.storeLOIContent(contentVO);
					
					//PCR-245552-新增UNB核保照會催辦單, 照會事項對應的照會代碼需保留在系統裡, 供FDW使用
					DocumentRuleCode docRuleCode = new DocumentRuleCode();
					ProposalRuleResultVO ruleResultVO = proposalCI.loadProposalRuleResultById(element);
					docRuleCode.setDocumentId(documentId);
					docRuleCode.setRuleResultId(element);
					if(ruleResultVO != null) {
						docRuleCode.setRuleCode(ruleResultVO.getRuleLetterCode());	
					}
					documentRuleCodeDao.save(docRuleCode);
				}
			} // end for
		}
	}

	@Override
	public void deleteOID(Long documentId) {
		DBean db = null;
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sql = "delete from t_outstanding_issues_doc where document_id = ? ";
		try {
			db = new DBean();
			db.connect();
			con = db.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, documentId);
			ps.execute();
		} catch (SQLException e) {
			throw new RTException(e);
		} catch (Exception e) {
			throw new RTException(e);
		} finally {
			DBean.closeAll(rs, ps, db);
		}

	}

	
	@Override
	@SuppressWarnings("unchecked")
	public List<Long> getSubDocList(
			FmtUnb0370ExtensionVO unb0370,
			List<Long> issuelistId, Long policyId,Long underwriteId) throws Exception {
		PolicyVO policyVO = policyService.load(policyId);
		String policyCode = policyVO.getPolicyNumber();
		List<Long> docIds = new ArrayList<Long>();
		/* 此次照會子照會 */
		Map<Long, Map<String,Object>> subLetterList = this.getSubLetterList(issuelistId, policyId);
		/* 此次照會疾病問卷**/
		Map<Long, Map<String,Object>>  subSickFormList = this.getUNBSickFormUnit(issuelistId, policyId);
		/* 隨函檢附 */
		List<String> fileNames = new ArrayList<String>();
		//被保險人問卷清單
		Map<String, List<String> > insuredBindSickForms = new LinkedHashMap<String, List<String>>();
	
		//依此次勾選的核保訊息依序產生「子照會」、「問卷」及依此次勾選核保訊息產生「隨函檢附」
		for(Long issueId : issuelistId) {
			
			if(subLetterList.containsKey(issueId)) {
				Map<String,Object> item = subLetterList.get(issueId);
				Integer type = (Integer)item.get("type");
				Long sourceId = (Long)item.get("sourceId");
				
				if (type == 3) {
					/* 加費 */
					Long templateId = Template.TEMPLATE_20024.getTemplateId();
					
					WSLetterUnit unit = uwActionHelper.getFmtUnb0441Letter(policyId, underwriteId);
					unit.setShortName("FMT_UNB_0441");
					
					Long documentId = commonLetterService.sendBatchLetter(policyId,
							templateId,
							unit, policyId, policyCode, null);
					docIds.add(documentId);
					
					//將documentId更新回核保加費檔
					uwPolicyDS.updateUwExtraPremDocumentId(underwriteId, documentId);
					
					//隨函檢附
					fileNames.add(templateService.getTemplateVO(templateId).getTemplateName());
					
				} else if (type == 4) {
					/* 批註 */
					Long templateId = Template.TEMPLATE_20023.getTemplateId();
					
					WSLetterUnit unit = uwActionHelper.getFmtUnb0381Letter(policyId, underwriteId, sourceId);
					unit.setShortName("FMT_UNB_0381");
				
					InsuredVO insuredVO = policyVO.gitInsured(sourceId);
					Map<DocumentPropsKey, Object> props = new HashMap<DocumentPropsKey, Object>();
					props.put(DocumentPropsKey.insuredCertiCode, insuredVO.getCertiCode());
					props.put(DocumentPropsKey.insuredName, insuredVO.getName());
					Long documentId = commonLetterService.sendBatchLetter(policyId,
							templateId,
							unit, policyId, policyCode, props);
					docIds.add(documentId);

					//將documentId更新回核保批註檔
					uwPolicyDS.updateUwExclusionDocumentId(underwriteId, insuredVO.getPartyId(), documentId);
				
					//隨函檢附
					fileNames.add(templateService.getTemplateVO(templateId).getTemplateName());
					
				} else if (type == 2) {
					/* 補充告知 */
					Long templateId = Template.TEMPLATE_20014.getTemplateId();
					
					
					UwAddInformLetterVO letterVO = this.uwAddInformLetterService.load(sourceId);
					WSLetterUnit unit = createAddInformFormLetterUnit(letterVO, policyId);
					unit.setShortName("FMT_UNB_0291");
				
					InsuredVO insuredVO = policyVO.gitInsured(letterVO.getSourceId());
					Map<DocumentPropsKey, Object> props = new HashMap<DocumentPropsKey, Object>();
					props.put(DocumentPropsKey.insuredCertiCode, insuredVO.getCertiCode());
					props.put(DocumentPropsKey.insuredName, insuredVO.getName());
					Long documentId = commonLetterService.sendBatchLetter(policyId,
							templateId,
							unit, policyId, policyCode, props);

					docIds.add(documentId);
					letterVO.setDocumentId(documentId);
					//將documentId更新回補充告知
					uwAddInformLetterService.updateDocumentId(sourceId, documentId);
					
					//隨函檢附
					fileNames.add(templateService.getTemplateVO(templateId).getTemplateName());
					
				}
				
				
			} else if(subSickFormList.containsKey(issueId)) {
				Map<String,Object> item = subSickFormList.get(issueId);

				//Long letterId = (Long)item.get("letterId");
				String certiCode = (String)item.get("certiCode");
				String name = (String)item.get("name");

				//未產生過問卷Unit才執行				
				if(insuredBindSickForms.get(certiCode) == null) {
					List<String> sickForms = this.getInsuredSickForms(issuelistId, subSickFormList, certiCode);
					List<Long> letterIdList = this.getInsuredSickLetterIds(issuelistId, subSickFormList, certiCode);
					
					insuredBindSickForms.put(certiCode, sickForms);

					//隨函檢附
					for(String sickFormItem : sickForms) {
						String fileName = CodeTable.getCodeDesc(TableCst.T_SICK_FORM, 
								sickFormItem, CodeCst.LANGUAGE__CHINESE_TRAD);
						fileNames.add(fileName);
					}
					
					WSLetterUnit sickFormUnit = createSickFormUnit(policyId,certiCode,name,sickForms);
					sickFormUnit.setShortName("FMT_UNB_0120");
				
					FmtUnb0120ExtensionVO vo = (FmtUnb0120ExtensionVO) sickFormUnit.getExtension();
					Map<DocumentPropsKey, Object> props = new HashMap<DocumentPropsKey, Object>();
					props.put(DocumentPropsKey.insuredCertiCode, vo.getCertiCode());
					props.put(DocumentPropsKey.insuredName, vo.getInsuredName());
					Long documentId = commonLetterService.sendBatchLetter(policyId, 20006L, sickFormUnit, policyId, policyCode, props);
					docIds.add(documentId);

					//將documentId更新回疾病問卷
					uwSickFormLetterService.updateDocumentId(letterIdList, documentId);
				}
			}
		}
		
		if(fileNames.isEmpty() == false){
			if( unb0370.getFileNames() == null){
				unb0370.setFileNames(new ArrayList<WSItemString>());
			}
			//前面的邏輯已設定掃描文件及人工上傳影像的隨函檢附文件，這邊的要用add(0,item)的方式向前增加
			for( int index = fileNames.size() - 1 ; index >= 0 ; index --){
				unb0370.getFileNames().add(0,new WSItemString(fileNames.get(index)));
			}
		}

		return docIds;
	}

	@Override
	public Map<Long,Map<String,Object>> getSubLetterList(List<Long> issuelistId, Long policyId) {
	
		Map<Long, Map<String,Object>>  result = new HashMap<Long, Map<String,Object>>();
		
		DBean db = null;
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		/* 已產生附照會的documentID */
		//Map<Integer, List<Long>> documentMap = new LinkedHashMap<Integer, List<Long>>();
		String sql = "select v.type,v.list_id,v.msg_id "
				+ " from V_UW_MSG_SUBDOC v left join t_proposal_rule_result a"
				+ " on v.MSG_ID = a.list_id where v.msg_id in ( ";
		int size = issuelistId.size();
		for (int i = 0; i < size; i++) {
			if (i > 0) {
				sql = sql + ",";
			}
			sql = sql + "?";
		}
		sql = sql + " )  ";

		try {
			db = new DBean();
			db.connect();
			con = db.getConnection();
			ps = con.prepareStatement(sql);
			for (int i = 0; i < size; i++) {
				ps.setLong(i + 1, issuelistId.get(i));
			}
			rs = ps.executeQuery();
			while (rs.next()) {
				int type = rs.getInt(1);
				Long sourceId = rs.getLong(2);
				Long issueId = rs.getLong(3);
				
				Map<String,Object> item = new HashMap<String, Object>();
				item.put("type", type);
				item.put("sourceId", sourceId); //批註/加費/補充告知 來源的list_id
			
				result.put(issueId, item);
			}

		} catch (SQLException e) {
			throw new RTException(e);
		} catch (Exception e) {
			throw new RTException(e);
		} finally {
			DBean.closeAll(rs, ps, db);
		}
		
		return result;
	}

	@Override
	public String getMasterDocStatus(Long msg_id) {
		DBean db = null;
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		/**已拆生附照會的documentID**/
		List<Long> subDocs = new ArrayList<Long>();
		Map<Integer, List<Long>> notDocumentMap = new HashMap<Integer, List<Long>>();
		String sql = "select status,print_time,formatting_method from t_outstanding_issues_doc oi, t_document d where oi.document_id = d.list_id and oi.issue_result_id = ? ";
		try {
			db = new DBean();
			db.connect();
			con = db.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, msg_id);
			rs = ps.executeQuery();
			if  (rs.next()) {
				String status = rs.getString(1);
				if ( status != null && CodeCst.DOCUMENT_STATUS__PRINTED.equals(status)) {
					java.util.Date printTime = new java.util.Date(rs.getDate(2).getTime());
					String formattingMethod = rs.getString(3);
					if ( FormattingMethod.BATCH.toString().equals(formattingMethod)) {
						if (org.apache.commons.lang3.time.DateUtils.truncatedCompareTo(printTime, AppContext.getCurrentUserLocalTime(), Calendar.DATE) == 0) {
							status = CodeCst.DOCUMENT_STATUS__TO_BE_PRINTED;
						}
					}
				}
				return status;
			}

		} catch (SQLException e) {
			throw new RTException(e);
		} catch (Exception e) {
			throw new RTException(e);
		} finally {
			DBean.closeAll(rs, ps, db);
		}
		return null;
	}

	
	private WSLetterUnit createAddInformFormLetterUnit(UwAddInformLetterVO letterVO, Long policyId) {
		Long templateId = 20014L;
		
		FmtUnb0291ExtensionVO vo = new FmtUnb0291ExtensionVO();
		
		nbNotificationHelper.initNBLetterExtensionVO(policyId, vo);
		nbNotificationHelper.resetInsuredData(letterVO.getSourceId(), vo);
		vo.setNoticeDate(AppContext.getCurrentUserLocalTime());
		vo.setTemplateId(templateId);
		vo.setBarcode1(CodeCst.BARCODE1_FMT_UNB_0291);
		vo.setBarcode2(vo.getPolicyCode());
		vo.setDocumentContent(stringToList(letterVO.getContent())); //照會內容 (文字區塊)
		nbNotificationHelper.setLetterContractorAtUW(vo, policyId);

		FmtUnb0291IndexVO index = new FmtUnb0291IndexVO(); 

		WSLetterUnit unit = new WSLetterUnit(vo, index);
		//通路寄送規則,採Email寄送
		NbLetterHelper.unbSendingMethod(unit);
		return unit;
	}

	private WSLetterUnit createSickFormLetterUnit(Long listId, Long policyId) {
		PolicyVO policyVO = policyService.load(policyId);
		Long templateId = 20006L;
		//TODO 建立照會
		FmtUnb0120ExtensionVO vo = new FmtUnb0120ExtensionVO();
		nbNotificationHelper.initNBLetterExtensionVO(policyId, vo);
		nbNotificationHelper.setLetterContractorAtUW(vo, policyId);
		vo.setNoticeDate(AppContext.getCurrentUserLocalTime());
		vo.setTemplateId(templateId);
		vo.setBarcode1(CodeCst.BARCODE1_FMT_UNB_0120);
		vo.setBarcode2(policyVO.getPolicyNumber());
		UwSickFormLetterVO letterVO = uwSickFormLetterService.load(listId);
		nbNotificationHelper.resetInsuredData(letterVO.getInsuredId(), vo);
		vo.setSickForms(uwSickFormItemService.findItemsByLetterId(listId));
		
		FmtUnb0120IndexVO index = new FmtUnb0120IndexVO(); 
		nbNotificationHelper.initIndexVO(index, vo, templateId);

		WSLetterUnit unit = new WSLetterUnit(vo, index);
		//通路寄送規則,採Email寄送
		NbLetterHelper.unbSendingMethod(unit);
		
		return unit;
	}

	@Override
	public List<WSItemString> getPosDetal(String documentId) {
		DBean db = null;
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sql = "SELECT r.letter_content FROM t_Proposal_Rule_Result r where r.list_id in" +
						"(select issue_result_id from t_outstanding_issues_doc where document_id =" +
						"(SELECT t.LIST_ID FROM t_Document t  where t.list_id = ?))" +
						"and r.rule_status = 1";
		List<WSItemString> posDetal = new ArrayList<WSItemString>();
		try {
			db = new DBean();
			db.connect();
			con = db.getConnection();
			ps = con.prepareStatement(sql);
			ps.setString(1, documentId);
			rs = ps.executeQuery();
			int prefix = 1;
			while (rs.next()) {
				String letterContentString = rs.getString(1);
				if (rs.getString(1).indexOf("隨函檢附") != -1) {

					letterContentString = rs.getString(1).substring(0, rs.getString(1).indexOf("隨函檢附"));
				}
				//2016/08/27 change by sunny 多筆要改格式
				//posDetal.add(new WSItemString(String.valueOf(prefix++),letterContentString));
				posDetal.addAll(WSItemString.getWSItemStringList(String.valueOf(prefix++), letterContentString));
			}
		} catch (SQLException e) {
			throw new RTException(e);
		} catch (Exception e) {
			throw new RTException(e);
		} finally {
			DBean.closeAll(rs, ps, db);
		}

		return posDetal;
	}

	@Override
	public List<Long> getGoupDocIds(Long docId) {
		return documentDao.queryGroupLetters(docId);
	}
	
	@Override
	public String getDocumentStatus(Long docId) {
		DBean db = null;
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
	
		String sql = " select status,print_time,formatting_method "
				+ " from t_document d "
				+ " where d.list_id = ? ";
		try {
			db = new DBean();
			db.connect();
			con = db.getConnection();
			ps = con.prepareStatement(sql);
			ps.setLong(1, docId);
			rs = ps.executeQuery();
			if  (rs.next()) {
				String status = rs.getString(1);
				if ( status != null && CodeCst.DOCUMENT_STATUS__PRINTED.equals(status)) {
					java.util.Date printTime = new java.util.Date(rs.getDate(2).getTime());
					String formattingMethod = rs.getString(3);
					if ( FormattingMethod.BATCH.toString().equals(formattingMethod)) {
						if (org.apache.commons.lang3.time.DateUtils.truncatedCompareTo(printTime, AppContext.getCurrentUserLocalTime(), Calendar.DATE) == 0) {
							status = CodeCst.DOCUMENT_STATUS__TO_BE_PRINTED;
						}
					}
				}
				return status;
			}

		} catch (SQLException e) {
			throw new RTException(e);
		} catch (Exception e) {
			throw new RTException(e);
		} finally {
			DBean.closeAll(rs, ps, db);
		}
		return null;
	}
	
	
	public java.util.List<WSItemString> stringToList(String contents){
        java.util.List<WSItemString> list = new ArrayList<WSItemString>();
        if(StringUtils.isEmpty(contents))
            return list;
        String[] inputArr =contents.split("\r\n");
        for(String word: inputArr) {
            list.add(new WSItemString(word));
        }
        return list;
    }
	
	@Override
	public List<WSLetterUnit> getSubUnitList(FmtUnb0370ExtensionVO unb0370,
			List<Long> issuelistId,
			Long policyId, Long underwriteId) throws Exception {
		List<WSLetterUnit> units = new ArrayList<WSLetterUnit>();
		/* 此次照會子照會 */
		Map<Long, Map<String,Object>> subLetterList = this.getSubLetterList(issuelistId, policyId);
		/* 此次照會疾病問卷**/
		Map<Long, Map<String,Object>> subSickFormList = this.getUNBSickFormUnit(issuelistId, policyId);
			
		/* 隨函檢附 */
		List<String> fileNames = new ArrayList<String>();
		//被保險人問卷清單
		Map<String, List<String> > insuredBindSickForms = new LinkedHashMap<String, List<String>>();
		
		//依此次勾選的核保訊息依序產生「子照會」、「問卷」及依此次勾選核保訊息產生「隨函檢附」
		for(Long issueId : issuelistId) {
			
			if(subLetterList.containsKey(issueId)) {
				Map<String,Object> item = subLetterList.get(issueId);
				Integer type = (Integer)item.get("type");
				Long sourceId = (Long)item.get("sourceId");
				
				if ( type == 3) {
					/* 加費 */
					WSLetterUnit unit = uwActionHelper.getFmtUnb0441Letter(policyId, underwriteId);
					unit.setShortName("FMT_UNB_0441");
					units.add(unit);
					
					//隨函檢附
					Long templateId = Template.TEMPLATE_20024.getTemplateId();
					fileNames.add(templateService.getTemplateVO(templateId).getTemplateName());
					
				} else if ( type == 4) {
					/* 批註 */
				    WSLetterUnit unit = uwActionHelper.getFmtUnb0381Letter(policyId, underwriteId, sourceId);
					unit.setShortName("FMT_UNB_0381");
				    units.add(unit);
					//隨函檢附
					Long templateId = Template.TEMPLATE_20023.getTemplateId();
					fileNames.add(templateService.getTemplateVO(templateId).getTemplateName());
					
				} else if ( type == 2) {
					
					UwAddInformLetterVO letterVO = this.uwAddInformLetterService.load(sourceId);
					WSLetterUnit unit = createAddInformFormLetterUnit(letterVO, policyId);
					unit.setShortName("FMT_UNB_0291");
					units.add(unit);
					
					//隨函檢附
					Long templateId = Template.TEMPLATE_20014.getTemplateId();
					fileNames.add(templateService.getTemplateVO(templateId).getTemplateName());
				}
			}  else if(subSickFormList.containsKey(issueId)) {
				Map<String,Object> item = subSickFormList.get(issueId);

				//Long letterId = (Long)item.get("letterId");
				String certiCode = (String)item.get("certiCode");
				String name = (String)item.get("name");

				//未產生過問卷Unit才執行				
				if(insuredBindSickForms.get(certiCode) == null) {
					List<String> sickForms = this.getInsuredSickForms(issuelistId, subSickFormList, certiCode);
				
					insuredBindSickForms.put(certiCode, sickForms);

					//隨函檢附
					for(String sickFormItem : sickForms) {
						String fileName = CodeTable.getCodeDesc(TableCst.T_SICK_FORM, 
								sickFormItem, CodeCst.LANGUAGE__CHINESE_TRAD);
						fileNames.add(fileName);
					}
					
					WSLetterUnit sickFormUnit = createSickFormUnit(policyId,certiCode,name,sickForms);
					sickFormUnit.setShortName("FMT_UNB_0120");
					
					units.add(sickFormUnit);
				}
			}
		}
	
		if(fileNames.isEmpty() == false){
			if( unb0370.getFileNames() == null){
				unb0370.setFileNames(new ArrayList<WSItemString>());
			}
			//前面的邏輯已設定掃描文件及人工上傳影像的隨函檢附文件，這邊的要用add(0,item)的方式向前增加
			for( int index = fileNames.size() - 1 ; index >= 0 ; index --){
				unb0370.getFileNames().add(0,new WSItemString(fileNames.get(index)));
			}
		}

		return units;
	}

	@Override
	public Map<Long,Map<String,Object>> getUNBSickFormUnit(List<Long> issuelistId, Long policyId) {
		
		Map<Long, Map<String,Object>> result = new HashMap<Long, Map<String,Object>>();
	
		DBean db = null;
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		String sql = "select distinct i.sick_form_item,l.insured_id,d.certi_code,d.name,l.list_id letter_id,i.msg_id "
				    + " from  t_uw_sick_form_item i, "
					+ " t_uw_sick_form_letter l ,t_insured_list d, t_proposal_rule_result a"
					+ " where i.sick_form_letter_id = l.list_id and l.insured_id = d.list_id "
					+ " and i.msg_id = a.list_id "
					+ " and i.msg_id in (";
		int size = issuelistId.size();
		
		for (int i = 0; i < size; i++) {

			if (i > 0) {
				sql = sql + ",";
			}
			sql = sql + "?";
		}
		sql = sql + " ) ";

		try {
			db = new DBean();
			db.connect();
			con = db.getConnection();
			ps = con.prepareStatement(sql);
			int index = 1 ;
			for (int i = 0; i < size; i++) {
				ps.setLong(index++, issuelistId.get(i));
			}
			rs = ps.executeQuery();
			
			while (rs.next()) {
				
				Long letterId = rs.getLong("LETTER_ID");
				String sickFormItem = rs.getString("SICK_FORM_ITEM");
				String certiCode = rs.getString("CERTI_CODE");
				String name = rs.getString("NAME");
				Long issueId = rs.getLong("MSG_ID");
				
				Map<String,Object> item = new HashMap<String, Object>();
				
				item.put("letterId", letterId);
				item.put("sickFormItem", sickFormItem);
				item.put("certiCode", certiCode);
				item.put("name", name);
				
				result.put(issueId, item);
				
			}
		} catch (SQLException e) {
			throw new RTException(e);
		} catch (Exception e) {
			throw new RTException(e);
		} finally {
			DBean.closeAll(rs, ps, db);
		}
		
		return result;

	}
	
	@Override
	public List<Object>  getSickFormUnit(List<Long> issuelistId, Long policyId) {
		
		List<Object> result = new ArrayList<Object>();
		List<WSLetterUnit> units = new ArrayList<WSLetterUnit>();
		Map<String,List<Long>> insuredBindLetterId = new LinkedHashMap<String, List<Long>>();
		List<String> fileNames = new ArrayList<String>(); //隨函檢附
		result.add(units);
		result.add(insuredBindLetterId);
		result.add(fileNames);
		
		DBean db = null;
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		String sql = "select distinct i.sick_form_item,l.insured_id,d.certi_code,d.name,l.list_id letter_id, "
					+ "   decode(a.rule_type,11,1,13,1,21,1,0) msg_level, " //--訊息等級 error在前
					+ "   decode(a.rule_status,1,1,2) msg_status, " //--待處理在前
					+ "   a.insert_time, a.rule_name, a.list_id"
				    + " from  t_uw_sick_form_item i, "
					+ " t_uw_sick_form_letter l ,t_insured_list d, t_proposal_rule_result a"
					+ " where i.sick_form_letter_id = l.list_id and l.insured_id = d.list_id "
					+ " and i.msg_id = a.list_id "
					+ " and i.msg_id in (";
		int size = issuelistId.size();
		
		for (int i = 0; i < size; i++) {

			if (i > 0) {
				sql = sql + ",";
			}
			sql = sql + "?";
		}
		sql = sql + " ) ";

		/**
		 * BSD :(1)若點重新校驗,屬於系統關閉者,狀態欄的文字改為｢系統關閉」。
		 *      (2)排序:先排錯誤訊息再排提示訊息,各自以日期時間遞減排列。(系統關閉及忽略排在最後面)
		 *  由UwIssueAction.queryIssue() 中的Collection.sort()方法移至此處理處理,
		 *  若調整需同步調整ProposalRuleResultDao..findRuleResultsByCols() 核保訊息的排序    
		 */
		sql += " order by msg_status, msg_level desc, a.insert_time desc,a.rule_name ,a.list_id desc";

		//PolicyVO policyVO = policyService.load(policyId);
		try {
			db = new DBean();
			db.connect();
			con = db.getConnection();
			ps = con.prepareStatement(sql);
			int index = 1 ;
			//ps.setString(index++, CodeCst.PROPOSAL_RULE_SOURCE_TYPE__SICK_FORM);
			for (int i = 0; i < size; i++) {
				ps.setLong(index++, issuelistId.get(i));
			}
			rs = ps.executeQuery();
			
			
			Map<String, List<String> > insuredBindSickForms = new LinkedHashMap<String, List<String>>();
			Map<String, String > insuredBindName = new LinkedHashMap<String, String>();
			while (rs.next()) {
				
				Long letterId = rs.getLong("LETTER_ID");
				String sickFormItem = rs.getString("SICK_FORM_ITEM");
				String certiCode = rs.getString("CERTI_CODE");
				String name = rs.getString("NAME");
				
				//被保險人問卷清單
				List<String>  sickForms =  insuredBindSickForms.get(certiCode);
				if(sickForms == null){
					 sickForms = new ArrayList<String>();
					 insuredBindSickForms.put(certiCode, sickForms);
				}
				if(sickForms.contains(sickFormItem) == false){
					sickForms.add(sickFormItem);

					//隨函檢附
					String fileName = CodeTable.getCodeDesc(TableCst.T_SICK_FORM, 
							sickFormItem, CodeCst.LANGUAGE__CHINESE_TRAD);
					if(fileNames.contains(fileName) == false){
						fileNames.add(fileName);
					}
				}

				//返回letterId作documentId更新用
				List<Long> leterIdList = insuredBindLetterId.get(certiCode);
				if(leterIdList == null){
					leterIdList = new ArrayList<Long>();
					insuredBindLetterId.put(certiCode, leterIdList);
				}
				
				if(leterIdList.contains(letterId) == false){
					leterIdList.add(letterId);
				}
				
				//被保險人id及姓名
				insuredBindName.put(certiCode, name);
			}
			
			
			for(String certiCode : insuredBindSickForms.keySet()){
				String name = insuredBindName.get(certiCode);
				List<String> sickForms = insuredBindSickForms.get(certiCode);
				units.add(createSickFormUnit(policyId,certiCode,name,sickForms));
			}
			
			return result;

		} catch (SQLException e) {
			throw new RTException(e);
		} catch (Exception e) {
			throw new RTException(e);
		} finally {
			DBean.closeAll(rs, ps, db);
		}
	}


	private WSLetterUnit createSickFormUnit(Long policyId, String certiCode,
			String name, List<String> sickForms) {
		//String policyCode = policyVO.getPolicyNumber();
		FmtUnb0120ExtensionVO vo = new FmtUnb0120ExtensionVO();
		nbNotificationHelper.initNBLetterExtensionVO(policyId, vo);
		vo.setNoticeDate(AppContext.getCurrentUserLocalTime());
		//vo.setPolicyCode(policyCode);
		//覆寫initNBLetterExtensionVO中的主被保人姓名及ID
		vo.setInsuredName(name);
		vo.setInsuredId(certiCode);
		
		vo.setCertiCode(certiCode);
		vo.setSickForms(sickForms);
		vo.setBarcode1(CodeCst.BARCODE1_FMT_UNB_0120);
		vo.setBarcode2(vo.getPolicyCode());
		vo.setTemplateId(20006L);
	
	    FmtUnb0120IndexVO index = new FmtUnb0120IndexVO();
	    nbNotificationHelper.initIndexVO(index, vo, 20006L);

		WSLetterUnit unit = new WSLetterUnit(vo, index);
		//通路寄送規則,採Email寄送
		NbLetterHelper.unbSendingMethod(unit);
		
		return unit;
	}

	@Resource(name = NbLetterHelper.BEAN_DEFAULT)
    private NbLetterHelper nbLetterHelper;
	
	@Resource(name = NbNotificationHelper.BEAN_DEFAULT)
	private NbNotificationHelper nbNotificationHelper;
	
	@Resource(name = UwActionHelper.BEAN_DEFAULT)
	private UwActionHelper uwActionHelper;
	
	@Resource(name = InsuredService.BEAN_DEFAULT)
	private InsuredService insuredService;


	private List<String> getInsuredSickForms(List<Long> issuelistId,  
			Map<Long,Map<String,Object>> subSickFormList, 
			String insuredCertiCode){

		List<String>  sickForms =  new ArrayList<String>();
		for(Long issueId : issuelistId) {
			Map<String,Object> item = subSickFormList.get(issueId);
			if(item != null) {
				String certiCode = StringUtils.trimToEmpty((String)item.get("certiCode"));
				String sickFormItem = (String)item.get("sickFormItem");
				if(certiCode.equals(insuredCertiCode)) {
					sickForms.add(sickFormItem);
				}
			}
		}

		return sickForms;
	}

	private List<Long> getInsuredSickLetterIds(List<Long> issuelistId,  
			Map<Long,Map<String,Object>> subSickFormList, 
			String insuredCertiCode){

		List<Long>  letterIdList =  new ArrayList<Long>();
		for(Long issueId : issuelistId) {
			Map<String,Object> item = subSickFormList.get(issueId);
			if(item != null) {
				String certiCode = StringUtils.trimToEmpty((String)item.get("certiCode"));
				Long letterId = (Long)item.get("letterId");
				if(certiCode.equals(insuredCertiCode)) {
					letterIdList.add(letterId);
				}
			}
		}
		return letterIdList;
	}
}
