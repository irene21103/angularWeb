package com.ebao.ls.uw.ctrl.pub;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

import com.ebao.pub.framework.GenericForm;

/**
 * <p>Title: </p>  
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2004 </p>
 * <p>Company: eBaoTech Corporation </p>
 * <p>Create Time: Fri Sep 03 17:54:57 GMT+08:00 2004 </p> 
 * @author Walter Huang
 * @version 1.0
 *
 */

public class UwRiFacForm extends GenericForm {

  private java.lang.Long uwListId = null;
  private java.lang.Long underwriteId = null;
  private java.lang.Long itemId = null;
  private java.lang.Long reinsurer = null;
  private java.math.BigDecimal cedingPercent = null;
  private java.util.Date lifeTimeLimit = null;

  /**
   * returns the value of the uwListId
   *
   * @return the uwListId
   */
  public java.lang.Long getUwListId() {
    return uwListId;
  }

  /**
   * sets the value of the uwListId
   *
   * @param uwListId the uwListId
   */
  public void setUwListId(java.lang.Long uwListId) {
    this.uwListId = uwListId;
  }
  /**
   * returns the value of the underwriteId
   *
   * @return the underwriteId
   */
  public java.lang.Long getUnderwriteId() {
    return underwriteId;
  }

  /**
   * sets the value of the underwriteId
   *
   * @param underwriteId the underwriteId
   */
  public void setUnderwriteId(java.lang.Long underwriteId) {
    this.underwriteId = underwriteId;
  }
  /**
   * returns the value of the itemId
   *
   * @return the itemId
   */
  public java.lang.Long getItemId() {
    return itemId;
  }

  /**
   * sets the value of the itemId
   *
   * @param itemId the itemId
   */
  public void setItemId(java.lang.Long itemId) {
    this.itemId = itemId;
  }
  /**
   * returns the value of the reinsurer
   *
   * @return the reinsurer
   */
  public java.lang.Long getReinsurer() {
    return reinsurer;
  }

  /**
   * sets the value of the reinsurer
   *
   * @param reinsurer the reinsurer
   */
  public void setReinsurer(java.lang.Long reinsurer) {
    this.reinsurer = reinsurer;
  }
  /**
   * returns the value of the cedingPercent
   *
   * @return the cedingPercent
   */
  public java.math.BigDecimal getCedingPercent() {
    return cedingPercent;
  }

  /**
   * sets the value of the cedingPercent
   *
   * @param cedingPercent the cedingPercent
   */
  public void setCedingPercent(java.math.BigDecimal cedingPercent) {
    this.cedingPercent = cedingPercent;
  }
  /**
   * returns the value of the lifeTimeLimit
   *
   * @return the lifeTimeLimit
   */
  public java.util.Date getLifeTimeLimit() {
    return lifeTimeLimit;
  }

  /**
   * sets the value of the lifeTimeLimit
   *
   * @param lifeTimeLimit the lifeTimeLimit
   */
  public void setLifeTimeLimit(java.util.Date lifeTimeLimit) {
    this.lifeTimeLimit = lifeTimeLimit;
  }

  // Contract methods

  /**
   * validate the submitted values 
   */
  public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {

    ActionErrors errors = new ActionErrors();

    return errors;

  }

  /**
   * This resets all the form values back to defaults, part of struts framework
   */
  public void reset(ActionMapping mapping, HttpServletRequest request) {

  }

  /**
   * returns the value of the UwRiFac
   *
   * @return the UwRiFacForm
   */
  public UwRiFacForm getValue() {
    UwRiFacForm value = new UwRiFacForm();
    value.setUwListId(uwListId);
    value.setUnderwriteId(underwriteId);
    value.setItemId(itemId);
    value.setReinsurer(reinsurer);
    value.setCedingPercent(cedingPercent);
    value.setLifeTimeLimit(lifeTimeLimit);
    return value;
  }
  /**
   * populates with the values of a UwRiFacForm
   *
   * @param UwRiFacForm the UwRiFacForm
   */
  public void populateWithValue(UwRiFacForm value) {
    uwListId = value.getUwListId();
    underwriteId = value.getUnderwriteId();
    itemId = value.getItemId();
    reinsurer = value.getReinsurer();
    cedingPercent = value.getCedingPercent();
    lifeTimeLimit = value.getLifeTimeLimit();
  }
  // Begin Additional Business Methods

  // End Additional Business Methods

}