package com.ebao.ls.uw.ctrl.bankAuth;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ebao.ls.arap.pub.StringUtils;
import com.ebao.ls.pa.nb.ctrl.detailreg.DetailRegAllInOneForm;

public class UwBankAuthQueryForm extends DetailRegAllInOneForm {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1393870016083561928L;

	/** 查詢準則.保單號 */
	protected String policyNo;
	
	/** 查詢準則.要保人身分證字號 */
	protected String holderCode= "";
	
	/** 查詢準則.授權人身分證字號 */
	protected String certiCode = "";
	
	/** 查詢準則.信用卡號 */
	protected String creditCardNo = "";

	/** 查詢準則.查詢類型 */
	protected String paymentRadio = "All";

	/** 查詢準則.銀行代號 */
	protected String bankCode = "";
	
	/** 查詢準則.授權銀行帳號 */
	protected String bankAccount = "";
	
	/** 查詢準則.幣別 */
	protected String currencyId = "";
	
	/** 查詢準則.核印狀態 */
	protected String approvalStatus = "";
	
	/** 查詢準則.建檔來源 */
	protected String sealSource = "";
	
	/** 查詢準則.核印回銷日期從 */
	protected String createDateStart = "";
	
	/** 查詢準則.核印回銷日期至 */
	protected String createDateEnd = "";
	
	/** 查詢準則.核印回銷日期從 */
	protected String bankReturnDateStart = "";
	
	/** 查詢準則.核印回銷日期至 */
	protected String bankReturnDateEnd = "";
	
	/** 查詢準則.授權人姓名 */
	protected String accName = "";
	
	/** 查詢結果.項目 */
	protected String[] listChecked;
	
	/** 查詢結果.幣別 */
	protected String[] listCurrencyId;
	
	/** 查詢結果.金融機構/發卡銀行 */
	protected String[] listBankCode;
	
	/** 查詢結果.帳號/信用卡卡號 */
	protected String[] listBankAccount;
	
	/** 查詢結果.有效期限(迄)年月 */
	protected String[] listExpireDate;
	
	/** 查詢結果.核印/驗證送件日期 */
	protected String[] listSendDate;
	
	/** 查詢結果.核印/驗證回覆日期 */
	protected String[] listBankReturnDate;
	
	/** 查詢結果.身分證字號 */
	protected String[] listCertiCode;
	
	/** 查詢結果.保單號 */
	protected String[] listPolicyNo;
	
	/** 查詢結果.建檔來源 */
	protected String[] listSealSource;
	
	/** 查詢結果.建檔日期 */
	protected String[] listCreateDate;
	
	/** 查詢結果.核印回銷日期 */
	protected String[] listRcptDate;
	
	/** 查詢結果.核印/驗證狀態 */
	protected String[] listApprovalStatus;
	
	/** 查詢結果.核印失敗原因 */
	protected String[] listFailureCode;
	
	/** 查詢結果.核印失敗原因 */
	protected String[] listFailureDesc;
	
	/** 查詢結果.驗證來源 */
	protected String[] listValidSource;
	
	/** 清單1 */
	protected List list1 = new ArrayList();
	
	/** 擴充資訊 */
	protected Map<String,Object> extension = new HashMap<String,Object>();
	
	/** 訊息狀態 */
	protected boolean resStatus = false;
	
	/** 回傳訊息 */
	protected String resMessage = "";
	
	/** 回傳訊息種類 */
	protected String resMessageType = "";
	
	/** 回傳腳本 */
	protected String resScript = "";
	
	/** 回傳導頁 */
	protected String resDirect = "";
	
	/** 請求類型*/
	protected String actionType = "";
	
	
	/** 取得 查詢準則.保單號 */
	public String getPolicyNo() {
		return policyNo;
	}
	
	/** 設定 查詢準則.保單號 */
	public void setPolicyNo(String policyNo) {
		this.policyNo = policyNo;
	}
	
	/** 取得 查詢準則.授權人身分證字號 */
	public String getCertiCode() {
		return certiCode;
	}
	
	/** 設定 查詢準則.授權人身分證字號 */
	public void setCertiCode(String certiCode) {
		this.certiCode = certiCode;
	}

	/** 取得 查詢準則.信用卡號 */
	public String getCreditCardNo() {
		return creditCardNo;
	}
	
	/** 設定 查詢準則.信用卡號 */
	public void setCreditCardNo(String creditCardNo) {
		this.creditCardNo = creditCardNo;
	}

	/** 取得 查詢準則.查詢類型*/
	public String getPaymentRadio() {
		return paymentRadio;
	}
	
	/** 設定' 查詢準則.查詢類型*/
	public void setPaymentRadio(String paymentRadio) {
		this.paymentRadio = paymentRadio;
	}

	
	/** 取得 查詢結果.項目 */
	public String[] getListChecked() {
		return listChecked;
	}
	
	/** 設定 查詢結果.項目 */
	public void setListChecked(String[] listChecked) {
		this.listChecked = listChecked;
	}

	/** 取得 查詢準則.銀行代號 */
	public String getBankCode() {
		return bankCode;
	}
	
	/** 設定 查詢準則.銀行代號 */
	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}
	
	/** 取得 查詢準則.授權銀行帳號 */
	public String getBankAccount() {
		return bankAccount;
	}
	
	/** 設定 查詢準則.授權銀行帳號 */
	public void setBankAccount(String bankAccount) {
		this.bankAccount = bankAccount;
	}
	
	/** 取得 查詢準則.幣別 */
	public String getCurrencyId() {
		return currencyId;
	}
	
	/** 設定 查詢準則.幣別 */
	public void setCurrencyId(String currencyId) {
		this.currencyId = currencyId;
	}
	
	/** 取得 查詢準則.核印/驗證狀態 */
	public String getApprovalStatus() {
		return approvalStatus;
	}
	
	/** 設定 查詢準則.核印/驗證狀態*/
	public void setApprovalStatus(String approvalStatus) {
		this.approvalStatus = approvalStatus;
	}
	
	/** 取得 查詢準則.建檔來源 */
	public String getSealSource() {
		return sealSource;
	}
	
	/** 設定 查詢準則.建檔來源 */
	public void setSealSource(String sealSource) {
		this.sealSource = sealSource;
	}
	
	/** 取得 查詢準則.核印回銷日期從 */
	public String getCreateDateStart() {
		return createDateStart;
	}
	
	/** 設定 查詢準則.建檔日期至 */
	public void setCreateDateStart(String createDateStart) {
		this.createDateStart = createDateStart;
	}
	
	/** 取得 查詢準則.建檔日期至  */
	public String getCreateDateEnd() {
		return createDateEnd;
	}
	
	/** 設定 查詢準則.建檔日期至 */
	public void setCreateDateEnd(String createDateEnd) {
		this.createDateEnd = createDateEnd;
	}
	
	/** 取得 查詢準則.核印回銷日期從 */
	public String getBankReturnDateStart() {
		return bankReturnDateStart;
	}
	/** 設定 查詢準則.核印回銷日期至 */
	public void setBankReturnDateStart(String bankReturnDateStart) {
		this.bankReturnDateStart = bankReturnDateStart;
	}
	/** 取得 查詢準則.核印回銷日期至 */
	public String getBankReturnDateEnd() {
		return bankReturnDateEnd;
	}

	public void setBankReturnDateEnd(String bankReturnDateEnd) {
		this.bankReturnDateEnd = bankReturnDateEnd;
	}
	
	/** 取得 查詢準則.授權人姓名 */
	public String getAccName() {
		return accName;
	}
	
	/** 設定 查詢準則.授權人姓名 */
	public void setAccName(String accName) {
		this.accName = accName;
	}
	
	/** 取得 查詢結果.幣別 */
	public String[] getListCurrencyId() {
		return listCurrencyId;
	}
	
	/** 設定 查詢結果.幣別 */
	public void setListCurrencyId(String[] listMoneyId) {
		this.listCurrencyId = listMoneyId;
	}
	
	/** 取得 查詢結果.金融機構/發卡銀行 */
	public String[] getListBankCode() {
		return listBankCode;
	}
	
	/** 設定 查詢結果.金融機構/發卡銀行 */
	public void setListBankCode(String[] listBankCode) {
		this.listBankCode = listBankCode;
	}
	
	/** 取得 查詢結果.帳號/信用卡卡號 */
	public String[] getListBankAccount() {
		return listBankAccount;
	}
	
	/** 設定 查詢結果.帳號/信用卡卡號 */
	public void setListBankAccount(String[] listBankAccount) {
		this.listBankAccount = listBankAccount;
	}

	/** 取得 查詢結果.有效期限(迄)年月 */
	public String[] getListExpireDate() {
		return listExpireDate;
	}

	/** 設定 查詢結果.有效期限(迄)年月 */	
	public void setListExpireDate(String[] listExpireDate) {
		this.listExpireDate = listExpireDate;
	}

	/** 取得 查詢結果.核印/驗證送件日期 */	
	public String[] getListSendDate() {
		return listSendDate;
	}

	/** 設定 查詢結果.核印/驗證送件日期 */	
	public void setListSendDate(String[] listSendDate) {
		this.listSendDate = listSendDate;
	}

	/** 取得 查詢結果.核印/驗證回覆日期 */	
	public String[] getListBankReturnDate() {
		return listBankReturnDate;
	}

	/** 設定 查詢結果.核印/驗證回覆日期 */	
	public void setListBankReturnDate(String[] listBankReturnDate) {
		this.listBankReturnDate = listBankReturnDate;
	}
	
	/** 取得 查詢結果.身分證字號 */
	public String[] getListCertiCode() {
		return listCertiCode;
	}
	
	/** 設定 查詢結果.身分證字號 */
	public void setListCertiCode(String[] listCertiCode) {
		this.listCertiCode = listCertiCode;
	}
	
	/** 取得 查詢結果.保單號 */
	public String[] getListPolicyNo() {
		return listPolicyNo;
	}
	
	/** 設定 查詢結果.保單號 */
	public void setListPolicyNo(String[] listPolicyNo) {
		this.listPolicyNo = listPolicyNo;
	}
	
	/** 取得 查詢結果.建檔來源 */
	public String[] getListSealSource() {
		return listSealSource;
	}
	
	/** 設定 查詢結果.建檔來源 */
	public void setListSealSource(String[] listSealSource) {
		this.listSealSource = listSealSource;
	}
	
	/** 取得 查詢結果.建檔日期 */
	public String[] getListCreateDate() {
		return listCreateDate;
	}
	
	/** 設定 查詢結果.建檔日期 */
	public void setListCreateDate(String[] listCreateDate) {
		this.listCreateDate = listCreateDate;
	}
	
	/** 取得 查詢結果.核印回銷日期 */
	public String[] getListRcptDate() {
		return listRcptDate;
	}
	
	/** 設定 查詢結果.核印回銷日期 */
	public void setListRcptDate(String[] listRcptDate) {
		this.listRcptDate = listRcptDate;
	}
	
	/** 取得 查詢結果.核印狀態 */
	public String[] getListApprovalStatus() {
		return listApprovalStatus;
	}
	
	/** 設定 查詢結果.核印狀態 */
	public void setListApprovalStatus(String[] listApprovalStatus) {
		this.listApprovalStatus = listApprovalStatus;
	}
	
	/** 取得 查詢結果.核印失敗原因 */
	public String[] getListFailureCode() {
		return listFailureCode;
	}
	
	/** 設定 查詢結果.核印失敗原因 */
	public void setListFailureCode(String[] listFailureCode) {
		this.listFailureCode = listFailureCode;
	}
	
	/** 取得 查詢結果.原因說明 */
	public String[] getListFailureDesc() {
		return listFailureDesc;
	}
	
	/** 設定 查詢結果.原因說明 */
	public void setListFailureDesc(String[] listFailureDesc) {
		this.listFailureDesc = listFailureDesc;
	}

	/** 取得 查詢結果.驗證來源 */
	public String[] getListValidSource() {
		return listValidSource;
	}

	/** 設定 查詢結果.驗證來源 */
	public void setListValidSource(String[] listValidSource) {
		this.listValidSource = listValidSource;
	}	
	
	/** 取得清單1 */
	public List getList1() {
		return list1;
	}
	
	/** 設定清單1 */
	public void setList1(List list1) {
		this.list1 = list1;
	}
	
	/** 取得擴充資訊 */
	public Map<String,Object> getExtension() {
		return extension;
	}
	
	/** 設定狀態 */
	public void setResStatus(boolean resStatus) {
		this.resStatus = resStatus;
	}
	
	/** 取得回傳訊息 */
	public String getResMessage() {
		return resMessage;
	}
	
	/** 設定回傳腳本 */
	public void setResMessage(String resMessage) {
		this.resMessage = resMessage;
	}
	
	/** 取得回傳訊息種類 */
	public String getResMessageType() {
		return resMessageType;
	}
	
	/** 設定回傳訊息種類 */
	public void setResMessageType(String resMessageType) {
		this.resMessageType = resMessageType;
	}
	
	/** 取得回傳腳本 */
	public String getResScript() {
		return resScript;
	}
	
	/** 設定回傳腳本 */
	public void setResScript(String resScript) {
		this.resScript = resScript;
	}
	
	/** 取得回傳導頁 */
	public String getResDirect() {
		return resDirect;
	}
	
	/** 設定回傳導頁 */
	public void setResDirect(String resDirect) {
		this.resDirect = resDirect;
	}
	
	public String getHolderCode() {
        return holderCode;
    }

    public void setHolderCode(String holderCode) {
        this.holderCode = holderCode;
    }

    public String getActionType() {
		return actionType;
	}

	public void setActionType(String actionType) {
		this.actionType = actionType;
	}

	/** 轉成Map */
	public Map<String,Object> toMap() {
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("policyNo",policyNo);
		map.put("bankCode",bankCode);
		map.put("bankAccount",bankAccount);
		map.put("currencyId",currencyId);
		map.put("approvalStatus",approvalStatus);
		map.put("sealSource",sealSource);
		map.put("createDateStart",createDateStart);
		map.put("createDateEnd",createDateEnd);
		map.put("bankReturnDateStart",bankReturnDateStart);
		map.put("bankReturnDateEnd",bankReturnDateEnd);
		map.put("accName",accName);
		map.put("certiCode",certiCode);
		map.put("creditCardNo",creditCardNo);
		map.put("listChecked",listChecked);
		map.put("listCurrencyId",listCurrencyId);
		map.put("listBankCode",listBankCode);
		map.put("listBankAccount",listBankAccount);
		map.put("listCertiCode",listCertiCode);
		map.put("listPolicyNo",listPolicyNo);
		map.put("listSealSource",listSealSource);
		map.put("listCreateDate",listCreateDate);
		map.put("listRcptDate",listRcptDate);
		map.put("listApprovalStatus",listApprovalStatus);
		map.put("listFailureCode",listFailureCode);
		map.put("listFailureDesc",listFailureDesc);
		map.put("listExpireDate",listExpireDate);
		map.put("listValidSource",listValidSource);
		
		return map;
	}
	
	/** 使用Map設定 */
	public void setByMap(Map<String,Object> map) {
		policyNo = StringUtils.O2S(map.get("policyNo"));
		bankCode = StringUtils.O2S(map.get("bankCode"));
		bankAccount = StringUtils.O2S(map.get("bankAccount"));
		currencyId = StringUtils.O2S(map.get("currencyId"));
		approvalStatus = StringUtils.O2S(map.get("approvalStatus"));
		sealSource = StringUtils.O2S(map.get("sealSource"));
		createDateStart = StringUtils.O2S(map.get("createDateStart"));
		createDateEnd = StringUtils.O2S(map.get("createDateEnd"));
		bankReturnDateStart = StringUtils.O2S(map.get("bankReturnDateStart"));
		bankReturnDateEnd = StringUtils.O2S(map.get("bankReturnDateEnd"));
		accName = StringUtils.O2S(map.get("accName"));
		certiCode = StringUtils.O2S(map.get("certiCode"));
		creditCardNo = StringUtils.O2S(map.get("creditCardNo"));
	}
	
	/** 清除資料 */
	public void clear() {
		list1 = new ArrayList();
		extension = new HashMap<String,Object>();
		if (!resStatus) {
			resMessage = "";
			resMessageType = "";
			resScript = "";
		}
	}
}