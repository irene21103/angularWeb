package com.ebao.ls.uw.ctrl.pub;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.ls.uw.ds.UwPolicyService;
import com.ebao.ls.uw.vo.UwPolicyVO;
import com.ebao.pub.framework.GenericAction;
import com.ebao.pub.framework.GenericException;
import com.ebao.pub.i18n.lang.CodeTable;

/**
 * <p>it implement asychornize method to implement
 * backward task. 
 * @author johnson.sun
 *
 */
public class LoadXmlAction extends GenericAction {

  private static final String XML_TITLE = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
  private static final String PENDING_CODE = "1";

  @Override
  public ActionForward process(ActionMapping mapping, ActionForm form,
      HttpServletRequest req, HttpServletResponse res) throws Exception {

    String actionType = req.getParameter("actionType");

    StringBuffer xml = new StringBuffer();

    if (PENDING_CODE.equals(actionType)) {
      getPendingInfo(req, xml);
    }

    res.setContentType("text/xml;charset=UTF-8");
    res.getOutputStream().write(xml.toString().getBytes("UTF-8"));
    res.getOutputStream().flush();

    return null;
  }

  /**
   * <P>requirement:When SLQ is generated,if the SLQ
   * has been published,we will set the pending reason to 'Query',
   * then we should update the information in uw main page,otherwise
   * it will still unchanged value which will make user confuse.
   * In the old implement,we should ask user to refresh the page.It's not
   * so convenient.
   * Now we implemnt asychornical method to update these data without any
   * refresh operation
   * @param req
   * @param xml
   * @throws GenericException
   */
  private void getPendingInfo(HttpServletRequest req, StringBuffer xml)
      throws GenericException {
    Long underwriteId = Long.valueOf(req.getParameter("underwriteId"));

    UwPolicyVO vo = uwPolicyDS.findUwPolicy(underwriteId);

    loadTitle(xml);
    xml.append("<pendingCode>");
    if (vo.getUwPending() != null) {
      xml.append(vo.getUwPending());
    } else {
      xml.append("");
    }
    xml.append("</pendingCode>");
    xml.append("<pendingCodeDesc>");
    if (vo.getUwPending() != null) {
      xml.append(CodeTable.getCodeDesc("T_PENDING_CODE",
          String.valueOf(vo.getUwPending())));
    }
    xml.append("</pendingCodeDesc>");
    loadTrail(xml);
  }

  private void loadTitle(StringBuffer xml) {
    xml.append(XML_TITLE);
    xml.append("<xml>");
  }

  private void loadTrail(StringBuffer xml) {
    xml.append("</xml>");
  }

  @Resource(name = UwPolicyService.BEAN_DEFAULT)
  private UwPolicyService uwPolicyDS;

  public void setUwPolicyDS(UwPolicyService uwPolicyDS) {
    this.uwPolicyDS = uwPolicyDS;
  }

}
