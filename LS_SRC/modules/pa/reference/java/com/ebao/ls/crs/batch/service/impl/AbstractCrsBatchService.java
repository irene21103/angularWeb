package com.ebao.ls.crs.batch.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;

import com.ebao.ls.chl.service.AgentService;
import com.ebao.ls.chl.service.ChannelOrgService;
import com.ebao.ls.chl.vo.ChannelOrgVO;
import com.ebao.ls.chl.vo.ScAgentDataVO;
import com.ebao.ls.chl.vo.ScServiceAgentDataVO;
import com.ebao.ls.chl.vo.ScServiceRepresentDataVO;
import com.ebao.ls.fatca.batch.data.FatcaDueDiligenceListDAO;
import com.ebao.ls.crs.batch.service.CrsMotherService;
import com.ebao.ls.fatca.data.bo.FatcaDueDiligenceList;
import com.ebao.ls.fatca.vo.FatcaDueDiligenceListVO;
import com.ebao.ls.foundation.service.GenericServiceImpl;
import com.ebao.ls.pub.cst.YesNo;
import com.ebao.ls.sc.vo.AgentChlVO;
import com.ebao.pub.batch.type.LogLevel;
import com.ebao.pub.batch.util.BatchLogUtils;

public abstract class AbstractCrsBatchService<T extends FatcaDueDiligenceListVO> extends GenericServiceImpl<FatcaDueDiligenceListVO, FatcaDueDiligenceList, FatcaDueDiligenceListDAO>{

    @Resource(name=AgentService.BEAN_DEFAULT)
    private AgentService agentService;
    
    @Resource(name=ChannelOrgService.BEAN_DEFAULT)
    private ChannelOrgService channelServ;
    
    @Resource(name=CrsMotherService.BEAN_DEFAULT)
    private CrsMotherService motherServ;
    
    protected void updateServiceAgent(FatcaDueDiligenceListVO vo){
        Long policyId = vo.getPolicyId();
        ScAgentDataVO agentDataVO = agentService.getAgentDataByPolicyId(policyId);
        List<ScServiceAgentDataVO> serviceAgentList = agentDataVO.getServiceAgentList();
        List<ScServiceRepresentDataVO> representAgentList = agentDataVO.getServiceRepresentList();
        if(CollectionUtils.isEmpty(serviceAgentList)){
            BatchLogUtils.addLog(LogLevel.WARN, null, String.format("[BR-CMN-FAT-033][同步FATCA盡職調查清單的服務員與綜合查詢一致] [查無服務員]policy_code = %s, certi_code =%s, policy_id = %d, party_id =%d" , vo.getPolicyCode(), vo.getTargetCertiCode(), vo.getPolicyId(), vo.getTargetId()));
            return;
        }
        ScServiceAgentDataVO serviceAgentVO = serviceAgentList.get(0);
        AgentChlVO agentchlVO = agentService.findAgentByAgentId(serviceAgentVO.getAgentId());
        if(agentchlVO == null)
            return;
        
        BatchLogUtils.addLog(LogLevel.WARN, null, "-1-serviceAgentList.size()="+serviceAgentList.size());
        Boolean IsAllDummy = Boolean.TRUE;
        for(ScServiceAgentDataVO svo:serviceAgentList){
        	BatchLogUtils.addLog(LogLevel.WARN, null, "0-findAgentByAgentId.getAgentId="+svo.getAgentId());
        	AgentChlVO agentChlVO =agentService.findAgentByAgentId(svo.getAgentId());
        	if(agentChlVO!=null){
        		BatchLogUtils.addLog(LogLevel.WARN, null, "agentChlVO!=nullagentChlVO.getAgentId()="+agentChlVO.getAgentId()+";agentChlVO.getIsDummy()="+agentChlVO.getIsDummy()+";");
        		if(YesNo.YES_NO__NO.equals(agentChlVO.getIsDummy())){
        			IsAllDummy = Boolean.FALSE;;
        		}
        	}
        }
        BatchLogUtils.addLog(LogLevel.WARN, null, "1-IsAllDummy="+IsAllDummy);
        
        vo.setServiceAgent(serviceAgentVO.getAgentId());
        if(agentchlVO.getBusinessCate() != null)
            vo.setAgentBizCate(Long.valueOf(agentchlVO.getBusinessCate()));
        vo.setAgentName(serviceAgentVO.getAgentName());
        vo.setAgentRegisterCode(serviceAgentVO.getAgentRegisterCode());
        
        Boolean toPCD = Boolean.FALSE;
        if(agentchlVO.getChannelOrgId() != null){
        	ChannelOrgVO channelVO = channelServ.findByChannelCode(serviceAgentVO.getAgentChannelCode());
            if(channelVO != null){
            	vo.setChannelOrg(channelVO.getChannelId());
                vo.setChannelCode(serviceAgentVO.getAgentChannelCode());
                vo.setChannelName(serviceAgentVO.getAgentChannelName());
                vo.setChannelType(serviceAgentVO.getAgentChannelType());
                //PCR-519543 部門跟人員都要虛擬且排除BRBD
                if(5l!=channelVO.getChannelType()&&6l!=channelVO.getChannelType()){
                	toPCD = YesNo.YES_NO__YES.equals(channelVO.getIsDummy()) && IsAllDummy;
                	vo.setAgentBizCate(YesNo.YES_NO__YES.equals(channelVO.getIsDummy())&& IsAllDummy ? 2L : 1L);
                }else{
                	//外部通路(BR/FID)-一律給服務單位處理，不歸入直營通路的虛擬單位PCD。
                	vo.setAgentBizCate(1L);
                }
                
            }
        }
        
        if(toPCD && CollectionUtils.isNotEmpty(representAgentList)) {//孤兒保單服務業務員
			ScServiceRepresentDataVO representVO = representAgentList.get(0);
			vo.setServiceAgent(representVO.getAgentId());
			vo.setAgentName(representVO.getRepresentName());
			vo.setAgentRegisterCode(representVO.getRepresentRegisterCode());
			vo.setAgentBizCate(2L);
        }
    }
    
    protected void updateServiceAgent(java.util.List<FatcaDueDiligenceListVO> voList){
        if(CollectionUtils.isEmpty(voList))
            return;
        
        for(FatcaDueDiligenceListVO vo : voList){
            updateServiceAgent(vo);
        }
    }
    
}