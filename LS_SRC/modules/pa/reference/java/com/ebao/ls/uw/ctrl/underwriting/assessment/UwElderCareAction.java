package com.ebao.ls.uw.ctrl.underwriting.assessment;

import static com.ebao.ls.pa.nb.bs.NbValidateRoleConfService.RULE_TYPE_1;
import static com.ebao.ls.pa.nb.util.Cst.UW_ELDER_CARE_TYPE_CALLOUT;
import static com.ebao.ls.pa.nb.util.Cst.UW_ELDER_CARE_TYPE_LIFE_SURVEY;
import static com.ebao.ls.pub.cst.AgeMethod.AGE_METHOD__ALAB;
import static com.ebao.ls.pub.cst.AgeMethod.AGE_METHOD__ANBD;
import static com.ebao.ls.pub.cst.YesNo.YES_NO__NA;
import static com.ebao.ls.pub.cst.YesNo.YES_NO__NO;
import static com.ebao.ls.pub.cst.YesNo.YES_NO__YES;
import static com.tgl.tools.web.enums.Action.SAVE;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.hibernate.criterion.Restrictions;

import com.ebao.ls.pa.nb.bs.NbValidateRoleConfService;
import com.ebao.ls.pa.nb.rule.ValidatorUtils;
import com.ebao.ls.pa.pub.bs.PolicyService;
import com.ebao.ls.pa.pub.vo.PolicyVO;
import com.ebao.ls.pub.util.BizUtils;
import com.ebao.ls.pub.util.HashMapBuilder;
import com.ebao.ls.uw.ci.UwPolicyCI;
import com.ebao.ls.uw.ci.vo.UwPolicyCIVO;
import com.ebao.ls.uw.ctrl.UwGenericAction;
import com.ebao.ls.uw.ctrl.underwriting.assessment.vo.UwElderCareTargetVO;
import com.ebao.ls.uw.ds.UwElderCareService;
import com.ebao.ls.uw.vo.UwElderCareTypeVO;
import com.ebao.ls.uw.vo.UwElderCareVO;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.framework.GenericException;
import com.ebao.pub.i18n.lang.StringResource;

import net.sf.json.JSONArray;

/**
 * 高齡投保核保評估Action-查詢
 *
 * @author Bruce Liao
 */
public class UwElderCareAction extends UwGenericAction {

    public static final String BEAN_DEFAULT = "/uw/policyUnderwriteElderCare";

    private final Logger logger = LogManager.getFormatterLogger();

    private PolicyVO policyVO;

    private UwPolicyCIVO uwPolicyCIVO;

    private final List<UwElderCareVO> allUwElderCareVOs = new ArrayList<>();

    @Resource(name = UwElderCareService.BEAN_DEFAULT)
    private UwElderCareService uwElderCareService;

    @Resource(name = NbValidateRoleConfService.BEAN_DEFAULT)
    private NbValidateRoleConfService paNbValidateRoleConfService;

    @Resource(name = PolicyService.BEAN_DEFAULT)
    private PolicyService paPolicyService;

    @Resource(name = UwPolicyCI.BEAN_DEFAULT)
    private UwPolicyCI uwPolicyCI;

    @Override
    public ActionForward uwProcess(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws GenericException {
        UwElderCareForm assessmentForm = (UwElderCareForm) form;

        if (SAVE == assessmentForm.getActionType()) {
            processSaveForm(assessmentForm);
        }

        // 初始資料
        initDatas(assessmentForm);

        // 評估對象
        assessmentForm.setTargetList(constructTargetList());

        // 因為有的人員不需要關懷提問，所以先行補上T_UW_ELDER_CARE
        createElderCareData(assessmentForm.getTargetList());

        return mapping.findForward("display");
    }

    private List<UwElderCareTargetVO> constructTargetList() {
        if (policyVO != null && uwPolicyCIVO != null) {
            return IterableUtils.toList(paNbValidateRoleConfService.getNbValidateRole(policyVO.getPolicyId(), RULE_TYPE_1)).stream()
                    .map(input -> {
                        UwElderCareTargetVO output = new UwElderCareTargetVO();
                        try {
                            BeanUtils.copyProperties(output, input);
                            return output;
                        } catch (IllegalAccessException | InvocationTargetException ex) {
							logger.error(StringUtils.EMPTY, ex);
                        }
                        return null;
                    })
                    .filter(Objects::nonNull)
                    .peek(this::setupOtherPropertyValue)
                    .collect(Collectors.toList());
        }
        return Collections.emptyList();
    }

    private void setupOtherPropertyValue(UwElderCareTargetVO vo) {
        if (Objects.nonNull(vo)) {
            String langId = AppContext.getCurrentUser().getLangId();

            allUwElderCareVOs.stream()
                    .filter(careVO -> StringUtils.equals(careVO.getCertiCode(), vo.getCertiCode()))
                    .forEach(careVO -> {
                        vo.setListId(careVO.getListId());
                        // 是否具有辨識能力評估結果(Y/N/W)
                        vo.setScaleResult(careVO.getScaleResult());
                        // 評估說明
                        vo.setScaleComment(careVO.getScaleComment());
                        // 確認高齡投保評估量表問項皆完成(Y/N)
                        vo.setAttchElderScale(careVO.getAttchElderScale());
                    });

			allUwElderCareVOs.stream()
					.filter(careVO -> StringUtils.equals(careVO.getCertiCode(), vo.getCertiCode()))
					.flatMap(careVO -> careVO.getUwElderCareTypeVOs().stream())
					.filter(careTypeVO -> ObjectUtils.equals(careTypeVO.getLastCmtFlg(), YES_NO__YES))
					.forEach(careTypeVO -> {
						// 高齡關懷提問最後會辦方式
						vo.setCareType(careTypeVO.getCareType());
					});

            // 投保年齡、足歲年齡
            Date applyDate = policyVO.getApplyDate();
            if (applyDate != null && vo.getBirthdate() != null) {
                // 投保年齡
                vo.setAgeInsurance(BizUtils.getAge(AGE_METHOD__ANBD, vo.getBirthdate(), applyDate));
                // 足歲年齡
                vo.setAgeFull(BizUtils.getAge(AGE_METHOD__ALAB, vo.getBirthdate(), applyDate));
            }

            // 是否評估對象-(投保年齡超過65歲(含)長者：同時考量高齡關懷提問、高齡投保評估量表)
            vo.setElderCareTarget(ValidatorUtils.isMathchElderAgeDefAge(vo.getBirthdate(), applyDate)
                    || ValidatorUtils.isMathchElderCaseUwAge(vo.getBirthdate(), applyDate));

            // 高齡投保核保評估-電訪：依電訪結案日期(遞增)、電訪任務編號排序(遞增)排序
            allUwElderCareVOs.stream()
                    .filter(careVO -> StringUtils.equals(careVO.getCertiCode(), vo.getCertiCode()))
                    .flatMap(careVO -> careVO.getUwElderCareTypeVOs().stream())
                    .filter(careTypeVO -> ObjectUtils.equals(careTypeVO.getCareType(), UW_ELDER_CARE_TYPE_CALLOUT))
                    .sorted(Comparator.comparing(UwElderCareTypeVO::getCareTaskDate).thenComparing(UwElderCareTypeVO::getCareTaskNo))
                    .forEach(vo::addCalloutVO);
            // 高齡投保核保評估-視訊生調：依更新日期(遞減)只取第一筆
            allUwElderCareVOs.stream()
                    .filter(careVO -> StringUtils.equals(careVO.getCertiCode(), vo.getCertiCode()))
                    .flatMap(careVO -> careVO.getUwElderCareTypeVOs().stream())
                    .filter(careTypeVO -> ObjectUtils.equals(careTypeVO.getCareType(), UW_ELDER_CARE_TYPE_LIFE_SURVEY))
                    .sorted(Comparator.comparing(UwElderCareTypeVO::getUpdateTimestamp).reversed())
                    .limit(1)
                    .forEach(vo::addLifesurveyVO);

            // 代碼欄位描述處理一定要放在最後面處理，否則會發生代碼與描述不一致的情形
            // 高齡關懷提問方式描述-1.電訪 2.視訊生調
			// 2023.1.4 PCR 500628 高齡投保件核保評估功能需求
			// 高齡關懷提問最後會辦方式 改成 高齡關懷提問會辦方式，顯示所有高齡關懷提問方式，若有多個以「、」間隔
			List<String> careTypeDescLst = new ArrayList<String>();
			if (CollectionUtils.isNotEmpty(vo.getCalloutVOs()))
				careTypeDescLst.add(StringResource.getStringData("MSG_1250933", langId));
			if (CollectionUtils.isNotEmpty(vo.getLifesurveyVOs()))
				careTypeDescLst.add(StringResource.getStringData("MSG_41", langId));

			if (CollectionUtils.isNotEmpty(careTypeDescLst)) {
				vo.setCareTypeDesc(StringUtils.join(careTypeDescLst, "、"));
            }

            // 評估結果描述(Y/N/W)
			HashMap<String, Object> msgs = HashMapBuilder.build(
                    YES_NO__YES, "MSG_46",
                    YES_NO__NO, "MSG_44",
                    YES_NO__NA, "MSG_4648"
            );
            if (ArrayUtils.contains(msgs.keySet().toArray(), vo.getScaleResult())) {
                vo.setScaleResultDesc(StringResource.getStringData(MapUtils.getString(msgs, vo.getScaleResult()), langId));
            }
        }
    }

    private void initDatas(UwElderCareForm assessmentForm) {
        // 保單VO
        policyVO = null;
        try {
            policyVO = paPolicyService.load(assessmentForm.getPolicyId());
        } catch (GenericException ex) {
			logger.error(StringUtils.EMPTY, ex);
        }
        // UWPolicyVO
        uwPolicyCIVO = null;
        try {
            uwPolicyCIVO = uwPolicyCI.getUwPolicyByUwId(assessmentForm.getUnderwriteId());
        } catch (GenericException ex) {
			logger.error(StringUtils.EMPTY, ex);
        }

        assessmentForm.setRoleSel(null);
        assessmentForm.setActionType(null);

        refreshDatas();
    }

    private void refreshDatas() {
        // 高齡投保核保評估資訊所有資訊
        allUwElderCareVOs.clear();
        try {
            allUwElderCareVOs.addAll(uwElderCareService.findElderCareDataById(policyVO.getPolicyId(), uwPolicyCIVO.getUnderwriteId()));
        } catch (Exception ex) {
			logger.error(StringUtils.EMPTY, ex);
        }
    }

    private void processSaveForm(UwElderCareForm assessmentForm) {
        String certiCodeSel = assessmentForm.getRoleSel();
        String submitData = assessmentForm.getSubmitData();
		// logger.error("processSaveForm().UwElderCareForm >> \n" + assessmentForm.toString());
		Map<String, Object> datas = convertFormSubmitData(submitData, certiCodeSel);
		if (StringUtils.isNotBlank(certiCodeSel)) {
			Set<Long> careListIds = new TreeSet<>();
			Set<Long> typeListIds = new TreeSet<>();
			Set<Long> dtlListIds = new TreeSet<>();
			Set<Long> dtlResetNListIds = new TreeSet<>();
			List<UwElderCareTargetVO> targetList = assessmentForm.getTargetList();
			List<Long> decisionCareType = (List<Long>) datas.get(String.format("decisionCareType_%s", certiCodeSel));
			if (null != decisionCareType) { // 有可能只有評估量表，沒有電訪也沒有生調
				// 儲存表單
				if (decisionCareType.contains(UW_ELDER_CARE_TYPE_CALLOUT)) {
					// 電訪
					targetList.stream()
							.filter(careTargetVO -> StringUtils.equals(careTargetVO.getCertiCode(), certiCodeSel))
							.flatMap(ctVO -> ctVO.getCalloutVOs().stream())
							.forEach(typeVO -> {
								careListIds.add(typeVO.getCareListId());
								typeListIds.add(typeVO.getListId());
								typeVO.getUwElderCareDtlVOs().forEach(dtlVO -> {
									dtlListIds.add(dtlVO.getListId());
								});
							});
				}
				if (decisionCareType.contains(UW_ELDER_CARE_TYPE_LIFE_SURVEY)) {
					// 生調
					targetList.stream()
							.filter(careTargetVO -> StringUtils.equals(careTargetVO.getCertiCode(), certiCodeSel))
							.flatMap(ctVO -> ctVO.getLifesurveyVOs().stream())
							.forEach(typeVO -> {
								careListIds.add(typeVO.getCareListId());
								typeListIds.add(typeVO.getListId());
								typeVO.getUwElderCareDtlVOs().forEach(dtlVO -> {
									dtlListIds.add(dtlVO.getListId());
								});
							});
				}
			}
			if (careListIds.isEmpty()) {
				// 如果沒有生調及電訪時改用證號進行問項更新
				targetList.stream()
						.filter(careTargetVO -> StringUtils.equals(careTargetVO.getCertiCode(), certiCodeSel))
						.map(UwElderCareTargetVO::getListId)
						.filter(Objects::nonNull)
						.forEachOrdered(careListIds::add);
			}
			uwElderCareService.updateCareDataByListIds(dtlResetNListIds, certiCodeSel, datas, careListIds, typeListIds, dtlListIds);
		}
    }

	private Map<String, Object> convertFormSubmitData(String submitData, String certiCodeSel) {
		Map<String, Object> data = new HashMap<>();
		String decisionCareTypeName = String.format("decisionCareType_%s", certiCodeSel);

		if (StringUtils.isNotBlank(submitData)) {
			JSONArray jsonArray = JSONArray.fromObject(submitData);
			int size = jsonArray.size();
			for (int i = 0; i < size; i++) {
				String name = StringUtils.trimToEmpty(jsonArray.optJSONObject(i).optString("name", StringUtils.EMPTY));
				String value = StringUtils.trimToEmpty(jsonArray.optJSONObject(i).optString("value", StringUtils.EMPTY));
				// 2023.2.3 PCR 500628 高齡投保件核保評估功能需求
				// 高齡關懷提問會辦方式，若是電訪和生調都有，那就會出現兩個 decisionCareType_身份證號
				if (decisionCareTypeName.equals(name)) {
					Object tempObj = data.get(name);
					List<Long> careTypeList;
					if (Objects.nonNull(tempObj))
						careTypeList = (List<Long>) tempObj;
					else
						careTypeList = new ArrayList<Long>();

					careTypeList.add(Long.valueOf(value));
					data.put(name, careTypeList);
				} else {
					data.put(name, value);
				}
			} // for
		}

        return data;
    }

    private Optional<UwElderCareVO> getUwElderCareVO(String certiCode) {
        return uwElderCareService.findByCriteria(
                Restrictions.eq("policyId", policyVO.getPolicyId()),
                Restrictions.eq("underwriteId", uwPolicyCIVO.getUnderwriteId()),
                Restrictions.eq("certiCode", certiCode)
        ).stream().findFirst();
    }

    private void createElderCareData(List<UwElderCareTargetVO> targetList) {
        // 還沒有T_UW_ELDER_CARE的人員預先建立資料
        targetList.stream()
                .filter(UwElderCareTargetVO::isElderCareTarget)
                .forEach(targetVO -> {
                    getUwElderCareVO(targetVO.getCertiCode()).orElseGet(() -> {
                        UwElderCareVO careVO = new UwElderCareVO();
                        careVO.setPolicyId(policyVO.getPolicyId());
                        careVO.setUnderwriteId(uwPolicyCIVO.getUnderwriteId());
                        careVO.setCertiCode(targetVO.getCertiCode());
                        return uwElderCareService.save(careVO);
                    });
                });
    }

}
