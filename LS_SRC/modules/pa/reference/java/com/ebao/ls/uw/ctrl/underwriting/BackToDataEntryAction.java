package com.ebao.ls.uw.ctrl.underwriting;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.ls.pa.nb.bs.ProposalProcessService;
import com.ebao.ls.pub.CodeCst;
import com.ebao.ls.uw.ctrl.UwGenericAction;
import com.ebao.ls.uw.ctrl.helper.LetterValidationHelper;
import com.ebao.ls.uw.ds.UwProcessService;
import com.ebao.ls.uw.ds.constant.ProposalStatusConstants;
import com.ebao.ls.uw.ds.constant.UwSourceTypeConstants;
import com.ebao.ls.uw.vo.UwPolicyVO;
import com.ebao.pub.framework.ExceptionFactory;
import com.ebao.pub.framework.GenericException;
import com.ebao.pub.framework.MultiWarning;

/**
 * Back to NBU data extry and synchronized data with NBU module
 * <p>
 * Title:Underwriting
 * </p>
 * <p>
 * Description:
 * </p>
 * <p>
 * Copyright: Copyright (c) 2004
 * </p>
 * <p>
 * Company: eBaoTech Corporation
 * </p>
 * 
 * @author jason.luo
 * @since 18.10.2004
 * @version 1.0
 */
public class BackToDataEntryAction extends UwGenericAction {
  /**
   * Next logic page after back to data entry process
   */
  public static final String NEXT_PAGE = "nbuDataEntryPage";
  public static final String BEAN_DEFAULT = "/uw/dataEntry";

  /**
   * Check before back to DataEntry page
   * 
   * @param mapping The ActionMapping used to select this instance
   * @param form The optional ActionForm bean for this request
   * @param request HttpRequest
   * @param response HttpResponse
   * @return MultiWarning MultiWarning
   * @throws Exception Exception
   * @author wenxuan.gu
   * @since 2008-12-23
   * @version 1.0
   */
  @Override
  public MultiWarning processWarning(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {
    MultiWarning warning = new MultiWarning();
    Long underwriteId = Long.valueOf(request.getParameter("underwriteId"));
    UwPolicyVO uwPolicyVO = this.getUwPolicyDS().findUwPolicy(underwriteId);
    Long policyId = uwPolicyVO.getPolicyId();
    pendingLetterCheck(warning, policyId);
    return warning;
  }

  /**
   * Cancel UwProposal underwriting
   * 
   * @param mapping The ActionMapping used to select this instance
   * @param form The optional ActionForm bean for this request (if any)
   * @param request HttpRequest
   * @param response HttpResponse
   * @return ActionForward ActionForward
   * @throws GenericException Application Exception
   */
  @Override
  public ActionForward uwProcess(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws GenericException {
    // UwProcessDS.exportUwResultIntoNBU(getUnderwriteId(request));
    Long underwriteId = Long.valueOf(request.getParameter("underwriteId"));
    UwPolicyVO uwPolicyVO = this.getUwPolicyDS().findUwPolicy(underwriteId);
    uwProcessDS.updateUwStatusAndDecisionAndProposalStatus(underwriteId, null,
        CodeCst.UW_STATUS__INVALID, null, null, true);
    Long policyId = uwPolicyVO.getPolicyId();
    uwProcessDS.changeProposalStatus(ProposalStatusConstants.ACTION_DATA_ENTRY,
        policyId, getUnderwriterId(), false);
    // add to control NBU process with workflow engine,by robert.xu on 2008.4.17
    if (UwSourceTypeConstants.NEW_BIZ.equals(uwPolicyVO.getUwSourceType())) {
      // WfHelper.processManagerForUwBackToDataEntry(policyId);
      // WfUtils.completeTask(policyId, CodeCst.UW_STATUS__UW_IN_PROGRESS,
      // CodeCst.PROPOSAL_STATUS__UW, WfUtils.UWTODATAENTRY);
      try {
        proposalProcessService.completeTask(policyId,
            CodeCst.UW_STATUS__UW_IN_PROGRESS, CodeCst.PROPOSAL_STATUS__UW,
            ProposalProcessService.UWTODATAENTRY);
      } catch (Exception e) {
        throw ExceptionFactory.parse(e);
      }
    }
    // add end
    return mapping.findForward(NEXT_PAGE);
  }

  /**
   * check CS/NBU's query letters
   * 
   * @param caseIdStr
   * @param response
   * @throws NumberFormatException
   * @throws GenericException
   */
  protected void pendingLetterCheck(MultiWarning multiWarning, Long policyId)
      throws NumberFormatException, GenericException {
    letterValidationHelper.checkPendingLetter4NbUw(multiWarning, policyId);
  }

  @Resource(name = UwProcessService.BEAN_DEFAULT)
  protected UwProcessService uwProcessDS;
  @Resource(name = ProposalProcessService.BEAN_DEFAULT)
  protected ProposalProcessService proposalProcessService;
  @Resource(name = LetterValidationHelper.BEAN_DEFAULT)
  private LetterValidationHelper letterValidationHelper;

  public void setUwProcessDS(UwProcessService uwProcessDS) {
    this.uwProcessDS = uwProcessDS;
  }

  public void setProposalProcessService(
      ProposalProcessService proposalProcessService) {
    this.proposalProcessService = proposalProcessService;
  }
}
