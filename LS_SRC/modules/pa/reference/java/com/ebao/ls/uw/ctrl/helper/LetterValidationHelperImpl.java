package com.ebao.ls.uw.ctrl.helper;

import javax.annotation.Resource;

import com.ebao.ls.cmu.pub.cst.CstCom;
import com.ebao.ls.cmu.service.DocumentService;
import com.ebao.pub.framework.GenericException;
import com.ebao.pub.framework.MultiWarning;

public class LetterValidationHelperImpl implements LetterValidationHelper {
  @Override
  public void checkPendingLetter4NbUw(MultiWarning multiWarning, Long policyId)
      throws GenericException {
    // NBU query letter
    if (documentService.hasPendingLettersForCertainTemplate(policyId,
        CstCom.NBU_QUERY_LETTER_TEMPLATE_ID) > 0) {
      multiWarning.addWarning("ERR_20710020015");
      multiWarning.setContinuable(false);
    }
    // Medical check letter
    if (documentService.hasPendingLettersForCertainTemplate(policyId,
        CstCom.MEDICAL_CHECK_LETTER_TEMPLATE_ID) > 0) {
      multiWarning.addWarning("ERR_20410020124");
      multiWarning.setContinuable(false);
    }
  }

  @Override
  public void checkPendingLetter4CsUw(MultiWarning multiWarning, Long changeId)
      throws GenericException {
    if (documentService.hasPendingLettersForCertainTemplate(changeId,
        CstCom.CS_QUERY_LETTER_TEMPLATE_ID) > 0) {
      multiWarning.addWarning("ERR_20710020015");
      multiWarning.setContinuable(false);
    }
  }

  @Resource(name = DocumentService.BEAN_DEFAULT)
  private DocumentService documentService;
}
