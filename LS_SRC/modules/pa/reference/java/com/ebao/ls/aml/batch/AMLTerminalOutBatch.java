package com.ebao.ls.aml.batch;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

import javax.sql.DataSource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

import com.ebao.ls.batch.BatchLogger;
import com.ebao.ls.batch.file.AbstractFileUploadBatchJob;
import com.ebao.ls.batch.file.filehelper.FileGenericHelper;
import com.ebao.ls.pub.batch.impl.DataSourceWrapper;
import com.ebao.ls.ws.ci.cmn.int1342.CmnPayUnpaid1342OutputBatch;
import com.ebao.pub.batch.help.BatchHelp;
import com.ebao.pub.batch.job.BaseUnpieceableJob;
import com.ebao.pub.batch.type.JobStatus;
import com.ebao.pub.util.EnvUtils;

public class AMLTerminalOutBatch  extends AbstractFileUploadBatchJob {
	
	public static final String BEAN_DEFAULT = "amlTerminalOutBatch";
	private static final String ROOT_PATH = EnvUtils.getSendToAMLSharePath();

	private static final String SERVICE_KEY = "INT1358";
	/** 系統別 */
	private static final String SYSTEM_CODE = "AML";
	/** 服務名稱 */
	private static final String SERVICE_NAME = "EBAO_CIFDATA";
	/** 輸出之檔名 */
	private static final String FILE_NAME = "INDIVIDUAL";
	/** 控制檔檔名 */
	private static final String CONTROL_FILE_NAME = "INDIVIDUAL";
	/** separate */
	private static final String SEPARATE_MARK = "|";
	private static final Pattern WHITESPACE_BLOCK = Pattern.compile("\\s+");
	
	private JdbcTemplate jdbcTemplate;
	private TransactionTemplate transactionTemplate;
	protected BatchLogger batchLogger = BatchLogger.getLogger(AMLTerminalOutBatch.class);

	//要保人
	private String aml_policy_holder_date_sql= "with uw AS ( "
											+ "  select * from ( "
											+ "    select "
											+ "      tup.underwrite_id, "
											+ "      tup.policy_id, "
											+ "      tup.last_underwriter_time, "
											+ "      row_number() over(partition by tup.policy_id order by tup.underwrite_id desc) row_number "
											+ "    FROM t_uw_policy tup "
											+ "      INNER JOIN v_contract_master tcm ON tcm.policy_id = tup.policy_id "
											+ "    where tup.last_underwriter_time >= ?-28  "
											+ "  ) d WHERE d.row_number=1 "
											+ ") "	
											+ "select distinct "
											+ "  a.CERTI_CODE || '_EBAO' cinNo, "
											+ "  '001' status, "
											+ "  to_char(a.apply_date,'YYYY-MM-DD') appDate, "
											+ "  to_char(?,'YYYY-MM-DD') openDate, "
											+ "  '' closeDate, "
											+ "  a.CERTI_CODE regId, "
											+ "  a.NAME name, "
											+ "  a.ENGLISH_NAME enName, "
											+ "  a.GENDER gender, "
											+ "  to_char(a.BIRTH_DATE,'YYYY-MM-DD') bierhdate, "
											+ "  a.COUNTRY ctrOfHome,"
											+ "  a.LIVED_OUTSIDE_OF_ROC ctrOfOth, "
											+ "  a.JOB_CODE prof, "
											+ "  a.NATIONALITY primNat, "
											+ "  a.NATIONALITY2 secNat, "
											+ "  'I' usage, "
											+ "  'CMD' Unit, "
											+ "  '' viewDate, "
											+ "  '' createdDate, "
											+ "  to_char(?,'YYYY-MM-DD') updatedDate, "
											+ "  'EBAO' EXT_SYS_CODE, "
											+ "  'N' EXT_IS_SCAN, "
											+ "  'N' EXT_IS_RISK, "
											+ "  'N' EXT_IS_TM, "
											+ "  'AMLFULL' EXT_USER_AD, "
											+ "  'FULL' EXT_EMPLOYEE_NAME "
											+ "from ("
											+ "  select tph.PARTY_TYPE, "
											+ "       DECODE(tph.ACTIVE_STATUS, '1', '0000', '0001') ACTIVE_STATUS, "
											+ "       tcm.APPLY_DATE, "
											+ "       tcm.VALIDATE_DATE, "
											+ "       tph.NAME, "
											+ "       tph.ENGLISH_NAME, "
											+ "       UPPER(TO_SINGLE_BYTE(NVL(tph.CERTI_CODE, 'EH_' || tph.LIST_ID))) CERTI_CODE, "
											+ "       case when "
											+ "         tjc.JOB_CODE is not NULL then "
											+ "           DECODE(tph.GENDER, 'N', 'X', tph.GENDER) "
											+ "         else "
											+ "           DECODE(tph.GENDER, 'N', 'U', tph.GENDER) "
											+ "       end GENDER, "
											+ "       tph.BIRTH_DATE, "
											+ "       tph.NATIONALITY, "
											+ "       tph.NATIONALITY2, "
											+ "       tnan.lived_outside_of_roc, "
											+ "       ta3.NATIONALITY COUNTRY, "
											+ "       tph.COMPANY_CATEGORY, "
											+ "       ta.nationality scale, "
											+ "       ta2.nationality seniority, "
											+ "       case when tnac.biz_address_indi = '1' then ta.nationality "
											+ "            when tnac.biz_address_indi = '2'  then ta2.nationality "
											+ "            else null "
											+ "       end grossRevn, "
											+ "       tjc.JOB_CODE, "
											+ "       tcm.POLICY_CODE "
											+ "  from v_Policy_Holder tph "
											+ "  join v_Contract_Master tcm "
											+ "    on tcm.POLICY_ID = tph.POLICY_ID "
											+ "  left join uw uw on tcm.policy_id=uw.policy_id "
											+ "  join t_Contract_Proposal prop on tcm.POLICY_ID = prop.POLICY_ID "
											+ "  left join t_Job_Category tjc "
											+ "    on tjc.JOB_CATE_ID = tph.JOB_CATE_ID "
											+ "  left join t_nb_agent_notify tnan "
											+ "    on tnan.policy_id = tcm.policy_id "
											+ "  left join t_nb_aml_company tnac "
											+ "    on tnac.policy_id = tcm.policy_id "
											+ "  left join t_address ta "
											+ "    on ta.address_id = tnac.reg_address_id "
											+ "  left join t_address ta2 "
											+ "    on ta2.address_id = tnac.biz_address_id "
											+ "  left join t_address ta3 "
											+ "    on ta3.address_id = tph.ADDRESS_ID "
											+ "  where tph.PARTY_TYPE = '1' and ((tcm.liability_state_date >= ?-28 and tcm.liability_state_date < ? and tcm.liability_state = 3) or (uw.LAST_UNDERWRITER_TIME >= ?-28 and uw.LAST_UNDERWRITER_TIME < ? and tcm.liability_state = 0 and prop.PROPOSAL_STATUS in('82','84','86'))) "
											+ "    and  not exists "
											+ "    ( "
											+ "      select b.certi_code from aml_temp_data b where b.certi_code = UPPER(TO_SINGLE_BYTE(tph.CERTI_CODE)) "
											+ "     ) "
											+ ") a order by cinNo, appDate desc "; 

	//受益人		
	private String aml_contract_bene_date_sql=	"with uw AS ( " +
												"  select * from ( " +
												"    select " +
												"      tup.underwrite_id, " +
												"      tup.policy_id, " +
												"      tup.last_underwriter_time, " +
												"      row_number() over(partition by tup.policy_id order by tup.underwrite_id desc) row_number " +
												"    FROM t_uw_policy tup " +
												"      INNER JOIN v_contract_master tcm ON tcm.policy_id = tup.policy_id " +
												"    where tup.last_underwriter_time >= ?-28  " +
												"  ) d WHERE d.row_number=1 " +
												") " +	
												"select distinct " + 
												"  a.CERTI_CODE || '_EBAO' cinNo, " + 
												"  '001' status, " + 
												"  to_char(a.apply_date,'YYYY-MM-DD') appDate, " + 
												"  to_char(?,'YYYY-MM-DD') openDate, " + 
												"  '' closeDate, " + 
												"  a.CERTI_CODE regId, " + 
												"  a.NAME name, " + 
												"  a.ENGLISH_NAME enName, " + 
												"  a.GENDER gender, " + 
												"  to_char(a.BIRTH_DATE,'YYYY-MM-DD') bierhdate, " + 
												"  a.COUNTRY ctrOfHome, " + 
												"  a.LIVED_OUTSIDE_OF_ROC ctrOfOth, " + 
												"  a.JOB_CODE prof, " + 
												"  a.NATIONALITY primNat, " + 
												"  a.NATIONALITY2 secNat, " + 
												"  'I' usage, " + 
												"  'CMD' Unit, " + 
												"  '' viewDate, " + 
												"  '' createdDate, " + 
												"  to_char(?,'YYYY-MM-DD') updatedDate, " + 
												"  'EBAO' EXT_SYS_CODE, " + 
												"  'N' EXT_IS_SCAN, " + 
												"  'N' EXT_IS_RISK, " + 
												"  'N' EXT_IS_TM, " + 
												"  'AMLFULL' EXT_USER_AD, " + 
												"  'FULL' EXT_EMPLOYEE_NAME " + 
												"from ( " + 
												"  select tpt.ORG_INDI PARTY_TYPE, " + 
												"       '0000' ACTIVE_STATUS,  " + 
												"       tcm.APPLY_DATE, " + 
												"       tcm.VALIDATE_DATE, " + 
												"       cb.NAME, " + 
												"       cb.ENGLISH_NAME, " + 
												"       UPPER(TO_SINGLE_BYTE(NVL(cb.CERTI_CODE, 'EB_' || cb.LIST_ID))) CERTI_CODE, " + 
												"       DECODE(cb.GENDER, 'N', 'U', cb.GENDER) GENDER,  " + 
												"       cb.BIRTH_DATE, " + 
												"       cb.NATIONALITY, " + 
												"       cb.NATIONALITY2,  " + 
												"       '' lived_outside_of_roc,  " + 
												"       ta.NATIONALITY COUNTRY,  " + 
												"       '' COMPANY_CATEGORY,   " + 
												"       '' scale,  " + 
												"       '' seniority,   " + 
												"       '' grossRevn,  " + 
												"       '' JOB_CODE,  " + 
												"       tcm.POLICY_CODE, " +  
												"       prop.proposal_status " + 
												"  from v_contract_bene cb " + 
												"  inner join v_Contract_Master tcm on tcm.POLICY_ID = cb.POLICY_ID " +
												"  left join uw uw on tcm.policy_id=uw.policy_id " +
												"  inner join t_Contract_Proposal prop on cb.POLICY_ID = prop.POLICY_ID    " + 
												"  left join t_Party tp on tp.PARTY_ID = cb.PARTY_ID " + 
												"  inner join t_Party_Type tpt on tp.PARTY_TYPE = tpt.PARTY_TYPE " + 
												"  left join T_ADDRESS ta on ta.address_id = cb.address_id " + 
												"  where tpt.PARTY_TYPE='1' and ((tcm.liability_state_date >= ?-28 and tcm.liability_state_date < ? and tcm.liability_state = 3) or (uw.LAST_UNDERWRITER_TIME >= ?-28 and uw.LAST_UNDERWRITER_TIME < ? and tcm.liability_state = 0 and prop.PROPOSAL_STATUS in('82','84','86'))) " + 
												"  and  not exists  " + 
												"    ( " + 
												"      select b.certi_code from aml_temp_data b where b.certi_code = UPPER(TO_SINGLE_BYTE(cb.CERTI_CODE)) " + 
												"    )    " + 
												") a order by cinNo, appDate desc "; 
	
	//代理人,受益人之代理人,要保人之代理人,被保人之代理人
	private String aml_lr_date_sql=	"with uw AS ( " +
									"  select * from ( " +
									"    select " +
									"      tup.underwrite_id, " +
									"      tup.policy_id, " +
									"      tup.last_underwriter_time, " +
									"      row_number() over(partition by tup.policy_id order by tup.underwrite_id desc) row_number " +
									"    FROM t_uw_policy tup " +
									"      INNER JOIN v_contract_master tcm ON tcm.policy_id = tup.policy_id " +
									"    where tup.last_underwriter_time >= ?-28  " +
									"  ) d WHERE d.row_number=1 " +
									") " +	
									"select distinct " + 
									"  a.CERTI_CODE || '_EBAO' cinNo, " + 
									"  '001' status, " + 
									"  to_char(a.apply_date,'YYYY-MM-DD') appDate, " + 
									"  to_char(?,'YYYY-MM-DD') openDate, " + 
									"  '' closeDate, " + 
									"  a.CERTI_CODE regId, " + 
									"  a.NAME name, " + 
									"  a.ENGLISH_NAME enName, " + 
									"  a.GENDER gender, " + 
									"  to_char(a.BIRTH_DATE,'YYYY-MM-DD') bierhdate, " + 
									"  a.COUNTRY ctrOfHome, " + 
									"  a.LIVED_OUTSIDE_OF_ROC ctrOfOth, " + 
									"  a.JOB_CODE prof, " + 
									"  a.NATIONALITY primNat, " + 
									"  a.NATIONALITY2 secNat, " + 
									"  'I' usage, " + 
									"  'CMD' Unit, " + 
									"  '' viewDate, " + 
									"  '' createdDate, " + 
									"  to_char(?,'YYYY-MM-DD') updatedDate, " + 
									"  'EBAO' EXT_SYS_CODE, " + 
									"  'N' EXT_IS_SCAN, " + 
									"  'N' EXT_IS_RISK, " + 
									"  'N' EXT_IS_TM, " + 
									"  'AMLFULL' EXT_USER_AD, " + 
									"  'FULL' EXT_EMPLOYEE_NAME " + 
									"from ( " + 
									"  select 'N' PARTY_TYPE, " + 
									"       '0000' ACTIVE_STATUS,  " + 
									"       tcm.APPLY_DATE, " + 
									"       tcm.VALIDATE_DATE, " + 
									"       lr.NAME, " + 
									"       lr.ENGLISH_NAME, " + 
									"       NVL(LR.CERTI_CODE, 'LR_' || LR.LIST_ID) CERTI_CODE, " + 
									"       LR.GENDER,  " + 
									"       lr.BIRTH_DATE, " + 
									"       lr.NATIONALITY, " + 
									"       lr.NATIONALITY2,  " + 
									"       '' lived_outside_of_roc,  " + 
									"       '' COUNTRY,  " + 
									"       '' COMPANY_CATEGORY,   " + 
									"       '' scale,  " + 
									"       '' seniority,   " + 
									"       '' grossRevn,  " + 
									"       '' JOB_CODE,  " + 
									"       tcm.POLICY_CODE, " + 
									"       prop.proposal_status " + 
									"  from T_LEGAL_REPRESENTATIVE lr " + 
									"  inner join v_Contract_Master tcm on tcm.POLICY_ID = lr.POLICY_ID " +
									"  left join uw uw on tcm.policy_id=uw.policy_id " +
									"  inner join t_Contract_Proposal prop on lr.POLICY_ID = prop.POLICY_ID   " + 
									"  left join t_Party tp on tp.PARTY_ID = lr.PARTY_ID " + 
									"  inner join t_Party_Type tpt on tp.PARTY_TYPE = tpt.PARTY_TYPE   " + 
									"  where tpt.PARTY_TYPE='1' and ((tcm.liability_state_date >= ?-28 and tcm.liability_state_date < ? and tcm.liability_state = 3) or (uw.LAST_UNDERWRITER_TIME >= ?-28 and uw.LAST_UNDERWRITER_TIME < ? and tcm.liability_state = 0 and prop.PROPOSAL_STATUS in('82','84','86'))) " + 
									"  and  not exists  " + 
									"    ( " + 
									"       select b.certi_code from aml_temp_data b where b.certi_code = LR.CERTI_CODE " + 
									"    )   " + 
									") a order by cinNo, appDate desc "; 

	//要保實質受益人
	private String aml_aml_benefical_owner_date_sql="with uw AS ( " +
													"  select * from ( " +
													"    select " +
													"      tup.underwrite_id, " +
													"      tup.policy_id, " +
													"      tup.last_underwriter_time, " +
													"      row_number() over(partition by tup.policy_id order by tup.underwrite_id desc) row_number " +
													"    FROM t_uw_policy tup " +
													"      INNER JOIN v_contract_master tcm ON tcm.policy_id = tup.policy_id " +
													"    where tup.last_underwriter_time >= ?-28  " +
													"  ) d WHERE d.row_number=1 " +
													") " +
													"select distinct " + 
													"  a.CERTI_CODE || '_EBAO' cinNo, " + 
													"  '001' status, " + 
													"  to_char(a.apply_date,'YYYY-MM-DD') appDate, " + 
													"  to_char(?,'YYYY-MM-DD') openDate, " + 
													"  '' closeDate, " + 
													"  a.CERTI_CODE regId, " + 
													"  a.NAME name, " + 
													"  a.ENGLISH_NAME enName, " + 
													"  a.GENDER gender, " + 
													"  to_char(a.BIRTH_DATE,'YYYY-MM-DD') bierhdate, " + 
													"  a.COUNTRY ctrOfHome, " + 
													"  a.LIVED_OUTSIDE_OF_ROC ctrOfOth, " + 
													"  a.JOB_CODE prof, " + 
													"  a.NATIONALITY primNat, " + 
													"  a.NATIONALITY2 secNat, " + 
													"  'I' usage, " + 
													"  'CMD' Unit, " + 
													"  '' viewDate, " + 
													"  '' createdDate, " + 
													"  to_char(?,'YYYY-MM-DD') updatedDate, " + 
													"  'EBAO' EXT_SYS_CODE, " + 
													"  'N' EXT_IS_SCAN, " + 
													"  'N' EXT_IS_RISK, " + 
													"  'N' EXT_IS_TM, " + 
													"  'AMLFULL' EXT_USER_AD, " + 
													"  'FULL' EXT_EMPLOYEE_NAME " + 
													"from ( " + 
													"  select  " + 
													"       case  " + 
													"         when nabo.IS_COMPANY is NULL then  " + 
													"         case  " + 
													"           when NAC.LIST_ID is NULL then  " + 
													"             'N'  " + 
													"           else  " + 
													"             'Y' " + 
													"         end  " + 
													"       else  " + 
													"         NABO.IS_COMPANY  " + 
													"       end PARTY_TYPE, " + 
													"       '0000' ACTIVE_STATUS,  " + 
													"       tcm.APPLY_DATE, " + 
													"       tcm.VALIDATE_DATE, " + 
													"       NABO.NAME, " + 
													"       '' ENGLISH_NAME, " + 
													"       NVL(NABO.IDENTITY_NO, 'NABO_' || NABO.LIST_ID) CERTI_CODE, " + 
													"       '' GENDER, " + 
													"       NABO.BIRTH_DATE, " + 
													"       NABO.NATIONALITY, " + 
													"       '' NATIONALITY2,  " + 
													"       '' lived_outside_of_roc,  " + 
													"       '' COUNTRY,  " + 
													"       '' COMPANY_CATEGORY,   " + 
													"       '' scale,  " + 
													"       '' seniority,   " + 
													"       '' grossRevn,  " + 
													"       '' JOB_CODE,  " + 
													"       tcm.POLICY_CODE , " + 
													"       prop.proposal_status " + 
													"  FROM T_NB_AML_BENEFICIAL_OWNER nabo " + 
													"  LEFT JOIN T_NB_AML_COMPANY NAC ON NAC.POLICY_ID = NABO.POLICY_ID " + 
													"  inner join v_Contract_Master tcm on tcm.POLICY_ID = NABO.POLICY_ID " +
													"  left join uw uw on tcm.policy_id=uw.policy_id " +
													"  inner join t_Contract_Proposal prop on NABO.POLICY_ID = prop.POLICY_ID  " + 
													"  left join t_Party tp on tp.PARTY_ID = NABO.PARTY_ID " + 
													"  inner join t_Party_Type tpt on tp.PARTY_TYPE = tpt.PARTY_TYPE  " + 
													"  where tpt.PARTY_TYPE='1' and ((tcm.liability_state_date >= ?-28 and tcm.liability_state_date < ? and tcm.liability_state = 3) or (uw.LAST_UNDERWRITER_TIME >= ?-28 and uw.LAST_UNDERWRITER_TIME < ? and tcm.liability_state = 0 and prop.PROPOSAL_STATUS in('82','84','86'))) " + 
													"  and not exists  " + 
													"    ( " + 
													"       select b.certi_code from aml_temp_data b where b.certi_code = NABO.IDENTITY_NO " + 
													"    )  " + 
													") a order by cinNo, appDate desc "; 
	//理賠實質受益人
	private String aml_claim_benefical_date_sql="with uw AS ( " +
												"  select * from ( " +
												"    select " +
												"      tup.underwrite_id, " +
												"      tup.policy_id, " +
												"      tup.last_underwriter_time, " +
												"      row_number() over(partition by tup.policy_id order by tup.underwrite_id desc) row_number " +
												"    FROM t_uw_policy tup " +
												"      INNER JOIN v_contract_master tcm ON tcm.policy_id = tup.policy_id " +
												"    where tup.last_underwriter_time >= ?-28  " +
												"  ) d WHERE d.row_number=1 " +
												") " +
												"select distinct " + 
												"  a.CERTI_CODE || '_EBAO' cinNo, " + 
												"  '001' status, " + 
												"  to_char(a.apply_date,'YYYY-MM-DD') appDate, " + 
												"  to_char(?,'YYYY-MM-DD') openDate, " + 
												"  '' closeDate, " + 
												"  a.CERTI_CODE regId, " + 
												"  a.NAME name, " + 
												"  a.ENGLISH_NAME enName, " + 
												"  a.GENDER gender, " + 
												"  to_char(a.BIRTH_DATE,'YYYY-MM-DD') bierhdate, " + 
												"  a.COUNTRY ctrOfHome, " + 
												"  a.LIVED_OUTSIDE_OF_ROC ctrOfOth, " + 
												"  a.JOB_CODE prof, " + 
												"  a.NATIONALITY primNat, " + 
												"  a.NATIONALITY2 secNat, " + 
												"  'I' usage, " + 
												"  'CMD' Unit, " + 
												"  '' viewDate, " + 
												"  '' createdDate, " + 
												"  to_char(?,'YYYY-MM-DD') updatedDate, " + 
												"  'EBAO' EXT_SYS_CODE, " + 
												"  'N' EXT_IS_SCAN, " + 
												"  'N' EXT_IS_RISK, " + 
												"  'N' EXT_IS_TM, " + 
												"  'AMLFULL' EXT_USER_AD, " + 
												"  'FULL' EXT_EMPLOYEE_NAME " + 
												"from ( " + 
												"  select 'N' PARTY_TYPE, " + 
												"       DECODE(TC.STATUS, '1', '0000', '2', '0000', '0001') ACTIVE_STATUS,  " + 
												"       tcm.APPLY_DATE, " + 
												"       tcm.VALIDATE_DATE, " + 
												"       TC.FIRST_NAME NAME, " + 
												"       TC.ENGLISH_NAME, " + 
												"       NVL(TC.CERTI_CODE, 'EP_' || TC.CUSTOMER_ID) CERTI_CODE, " + 
												"       DECODE(TC.GENDER, 'N', 'U', TC.GENDER) GENDER, " + 
												"       TC.BIRTHDAY BIRTH_DATE, " + 
												"       TC.NATIONALITY, " + 
												"       TC.NATIONALITY2,  " + 
												"       '' lived_outside_of_roc,  " + 
												"       ta.NATIONALITY COUNTRY,  " + 
												"       '' COMPANY_CATEGORY,   " + 
												"       '' scale,  " + 
												"       '' seniority,   " + 
												"       '' grossRevn,  " + 
												"       '' JOB_CODE,  " + 
												"       tcm.POLICY_CODE, " + 
												"       prop.proposal_status " + 
												"  FROM v_CONTRACT_BENE cb " + 
												"  inner join T_CUSTOMER tc on tc.customer_id =cb.party_id " + 
												"  inner join v_Contract_Master tcm on tcm.POLICY_ID = cb.POLICY_ID " + 
												"  left join uw uw on tcm.policy_id=uw.policy_id " +
												"  inner join t_Contract_Proposal prop on cb.POLICY_ID = prop.POLICY_ID   " + 
												"  inner join T_ADDRESS ta on ta.address_id = tc.address_id " + 
												"  left join t_Party tp on tp.PARTY_ID = cb.PARTY_ID " + 
												"  inner join t_Party_Type tpt on tp.PARTY_TYPE = tpt.PARTY_TYPE " + 
												"  where tpt.PARTY_TYPE='1' and TC.CUSTOMER_ID IS NOT NULL and ((tcm.liability_state_date >= ?-28 and tcm.liability_state_date < ? and tcm.liability_state = 3) or (uw.LAST_UNDERWRITER_TIME >= ?-28 and uw.LAST_UNDERWRITER_TIME < ? and tcm.liability_state = 0 and prop.PROPOSAL_STATUS in('82','84','86'))) " + 
												"    and  not exists  " + 
												"    ( " + 
												"      select b.certi_code from aml_temp_data b where b.certi_code = TC.CERTI_CODE " + 
												"    )  " + 
												") a order by cinNo, appDate desc "; ; 
	//被保人
	private String aml_benefit_insured_date_sql="with uw AS ( " +
												"  select * from ( " +
												"    select " +
												"      tup.underwrite_id, " +
												"      tup.policy_id, " +
												"      tup.last_underwriter_time, " +
												"      row_number() over(partition by tup.policy_id order by tup.underwrite_id desc) row_number " +
												"    FROM t_uw_policy tup " +
												"      INNER JOIN v_contract_master tcm ON tcm.policy_id = tup.policy_id " +
												"    where tup.last_underwriter_time >= ?-28  " +
												"  ) d WHERE d.row_number=1 " +
												") " +
												"select distinct " + 
												"  a.CERTI_CODE || '_EBAO' cinNo, " + 
												"  '001' status, " + 
												"  to_char(a.apply_date,'YYYY-MM-DD') appDate, " + 
												"  to_char(?,'YYYY-MM-DD') openDate, " + 
												"  '' closeDate, " + 
												"  a.CERTI_CODE regId, " + 
												"  a.NAME name, " + 
												"  a.ENGLISH_NAME enName, " + 
												"  a.GENDER gender, " + 
												"  to_char(a.BIRTH_DATE,'YYYY-MM-DD') bierhdate, " + 
												"  a.COUNTRY ctrOfHome, " + 
												"  a.LIVED_OUTSIDE_OF_ROC ctrOfOth, " + 
												"  a.JOB_CODE prof, " + 
												"  a.NATIONALITY primNat, " + 
												"  a.NATIONALITY2 secNat, " + 
												"  'I' usage, " + 
												"  'CMD' Unit, " + 
												"  '' viewDate, " + 
												"  '' createdDate, " + 
												"  to_char(?,'YYYY-MM-DD') updatedDate, " + 
												"  'EBAO' EXT_SYS_CODE, " + 
												"  'N' EXT_IS_SCAN, " + 
												"  'N' EXT_IS_RISK, " + 
												"  'N' EXT_IS_TM, " + 
												"  'AMLFULL' EXT_USER_AD, " + 
												"  'FULL' EXT_EMPLOYEE_NAME " + 
												"from ( " + 
												"  select tpt.ORG_INDI PARTY_TYPE, " + 
												"       DECODE(il.ACTIVE_STATUS, '1', '0000', '0001') ACTIVE_STATUS,  " + 
												"       tcm.APPLY_DATE, " + 
												"       tcm.VALIDATE_DATE, " + 
												"       il.NAME, " + 
												"       il.ENGLISH_NAME, " + 
												"       UPPER(TO_SINGLE_BYTE(NVL(il.CERTI_CODE,  il.LIST_ID))) CERTI_CODE, " + 
												"       DECODE(il.GENDER, 'N', 'U', il.GENDER) GENDER,  " + 
												"       il.BIRTH_DATE, " + 
												"       il.NATIONALITY, " + 
												"       il.NATIONALITY2,  " + 
												"       '' lived_outside_of_roc,  " + 
												"       ta.NATIONALITY COUNTRY,  " + 
												"       '' COMPANY_CATEGORY,   " + 
												"       '' scale,  " + 
												"       '' seniority,   " + 
												"       '' grossRevn,  " + 
												"       '' JOB_CODE,  " + 
												"       tcm.POLICY_CODE, " + 
												"       prop.proposal_status " + 
												"  from v_Benefit_Insured bi " + 
												"  inner join v_Insured_List il on il.LIST_ID = bi.INSURED_ID " + 
												"  inner join v_Contract_Master tcm on tcm.POLICY_ID = bi.POLICY_ID " +
												"  left join uw uw on tcm.policy_id=uw.policy_id " +
												"  inner join t_Contract_Proposal prop on bi.POLICY_ID = prop.POLICY_ID    " + 
												"  left join t_Party tp on tp.PARTY_ID = il.PARTY_ID " + 
												"  inner join t_Party_Type tpt on tp.PARTY_TYPE = tpt.PARTY_TYPE " + 
												"  left join T_ADDRESS ta on ta.address_id = il.address_id " + 
												"  where tpt.PARTY_TYPE='1' and ((tcm.liability_state_date >= ?-28 and tcm.liability_state_date < ? and tcm.liability_state = 3) or (uw.LAST_UNDERWRITER_TIME >= ?-28 and uw.LAST_UNDERWRITER_TIME < ? and tcm.liability_state = 0 and prop.PROPOSAL_STATUS in('82','84','86'))) " + 
												"  and  not exists  " + 
												"    ( " + 
												"      select b.certi_code from aml_temp_data b where b.certi_code = UPPER(TO_SINGLE_BYTE(il.CERTI_CODE)) " + 
												"    )  " + 
												") a order by cinNo, appDate desc ";  

	//單次繳款之第三人,重複性繳款之第三人
	private String aml_pa_trader_date_sql =  "with uw AS ( "
											+ "  select * from ( "
											+ "    select "
											+ "      tup.underwrite_id, "
											+ "      tup.policy_id, "
											+ "      tup.last_underwriter_time, "
											+ "      row_number() over(partition by tup.policy_id order by tup.underwrite_id desc) row_number "
											+ "    FROM t_uw_policy tup "
											+ "      INNER JOIN v_contract_master tcm ON tcm.policy_id = tup.policy_id "
											+ "    where tup.last_underwriter_time >= ?-28  "
											+ "  ) d WHERE d.row_number=1 "
											+ ") "	
											+ "select distinct "
											+ "  a.CERTI_CODE || '_EBAO' cinNo, "
											+ "  '001' status,"
											+ "  to_char(a.apply_date,'YYYY-MM-DD') appDate,"
											+ "  to_char(?,'YYYY-MM-DD') openDate,"
											+ "  '' closeDate,"
											+ "  a.CERTI_CODE regId,"
											+ "  a.NAME name,"
											+ "  a.ENGLISH_NAME enName,"
											+ "  a.GENDER gender,"
											+ "  to_char(a.BIRTH_DATE,'YYYY-MM-DD') bierhdate,"
											+ "  a.COUNTRY ctrOfHome,"
											+ "  a.LIVED_OUTSIDE_OF_ROC ctrOfOth,"
											+ "  a.JOB_CODE prof,"
											+ "  a.NATIONALITY primNat,"
											+ "  a.NATIONALITY2 secNat,"
											+ "  'I' usage,"
											+ "  'CMD' Unit,"
											+ "  '' viewDate,"
											+ "  '' createdDate,"
											+ "  to_char(?,'YYYY-MM-DD') updatedDate,"
											+ "  'EBAO' EXT_SYS_CODE,"
											+ "  'N' EXT_IS_SCAN,"
											+ "  'N' EXT_IS_RISK,"	
											+ "  'N' EXT_IS_TM,"
											+ "  'AMLFULL' EXT_USER_AD,"
											+ "  'FULL' EXT_EMPLOYEE_NAME "
											+ "from ("
											+ "  select 'N' PARTY_TYPE,"
											+ "       '0001' ACTIVE_STATUS, "
											+ "       tcm.APPLY_DATE,"
											+ "       tcm.VALIDATE_DATE,"
											+ "       t11.trader_name NAME,"
											+ "       '' ENGLISH_NAME,"
											+ "       t11.trader_certi_code CERTI_CODE,"
											+ "       '' GENDER, "
											+ "       '' BIRTH_DATE,"
											+ "       '' NATIONALITY,"
											+ "       '' NATIONALITY2, "
											+ "       '' lived_outside_of_roc, "
											+ "       '' COUNTRY, "
											+ "       '' COMPANY_CATEGORY,  "
											+ "       '' scale, "
											+ "       '' seniority,  "
											+ "       '' grossRevn, "
											+ "       '' JOB_CODE, "
											+ "       tcm.POLICY_CODE,"
											+ "       'RI44' relShip,"
											+ "       '' relCType,"
											+ "      NVL(t11.trader_certi_code, 'EP_' || t11.LIST_ID) || '_EBAO' relCinNo "
											+ "  FROM t_pa_trader t11 "
											+ "  inner join t_cash t21 on t11.fee_id = t21.fee_id "
											+ "  inner join v_Contract_Master tcm on tcm.POLICY_ID = t21.POLICY_ID "
											+ "  left join uw uw on tcm.policy_id=uw.policy_id " 
											+ "  inner join t_Contract_Proposal prop on t21.POLICY_ID = prop.POLICY_ID "
											+ "  where ((tcm.liability_state_date >= ?-28 and tcm.liability_state_date < ? and tcm.liability_state = 3) or (uw.LAST_UNDERWRITER_TIME >= ?-28 and uw.LAST_UNDERWRITER_TIME < ? and tcm.liability_state = 0 and prop.PROPOSAL_STATUS in('82','84','86'))) "
											+ "  and  not exists "
											+ "    ("
											+ "       select b.certi_code from aml_temp_data b where b.certi_code = t11.trader_certi_code"
											+ "    )  "
											+ ") a order by cinNo, appDate desc "; 
			
	//第三方受款人
	private String aml_third_payee_date_sql = "with uw AS ( "
											+ "  select * from ( "
											+ "    select "
											+ "      tup.underwrite_id, "
											+ "      tup.policy_id, "
											+ "      tup.last_underwriter_time, "
											+ "      row_number() over(partition by tup.policy_id order by tup.underwrite_id desc) row_number "
											+ "    FROM t_uw_policy tup "
											+ "      INNER JOIN v_contract_master tcm ON tcm.policy_id = tup.policy_id "
											+ "    where tup.last_underwriter_time >= ?-28  "
											+ "  ) d WHERE d.row_number=1 "
											+ ") "
											+ "select distinct "
											+ "  a.CERTI_CODE || '_EBAO' cinNo,"
											+ "  '001' status,"
											+ "  to_char(a.apply_date,'YYYY-MM-DD') appDate,"
											+ "  to_char(?,'YYYY-MM-DD') openDate,"
											+ "  '' closeDate,"
											+ "  a.CERTI_CODE regId,"
											+ "  a.NAME name,"
											+ "  a.ENGLISH_NAME enName,"
											+ "  a.GENDER gender,"
											+ "  to_char(a.BIRTH_DATE,'YYYY-MM-DD') bierhdate,"
											+ "  a.COUNTRY ctrOfHome,"
											+ "  a.LIVED_OUTSIDE_OF_ROC ctrOfOth,"
											+ "  a.JOB_CODE prof,"
											+ "  a.NATIONALITY primNat,"
											+ "  a.NATIONALITY2 secNat,"
											+ "  'I' usage,"
											+ "  'CMD' Unit,"
											+ "  '' viewDate,"
											+ "  '' createdDate,"
											+ "  to_char(?,'YYYY-MM-DD') updatedDate,"
											+ "  'EBAO' EXT_SYS_CODE,"
											+ "  'N' EXT_IS_SCAN,"
											+ "  'N' EXT_IS_RISK,"
											+ "  'N' EXT_IS_TM,"
											+ "  'AMLFULL' EXT_USER_AD,"
											+ "  'FULL' EXT_EMPLOYEE_NAME "
											+ "from ( "
											+ "  SELECT * "
											+ "  FROM "
											+ "  ("
											+ "    SELECT"
											+ "      DECODE(CPI.PARTY_TYPE,'2','Y','N') ORG_INDI,"
											+ "      CPI.CERTI_CODE,"
											+ "      '0000' ACTIVE_STATUS,"
											+ "      CPI.PAYEE_NAME NAME,"
											+ "      '' ENGLISH_NAME,"
											+ "      '' GENDER,"
											+ "      CPI.NATIONALITY,"
											+ "      CPI.NATIONALITY2,"
											+ "      '' COUNTRY,"
											+ "      CPI.BIRTHDAY BIRTH_DATE,"
											+ "      CM.POLICY_CODE,"
											+ "      '' lived_outside_of_roc, "
											+ "      '' scale, "
											+ "      '' seniority, "
											+ "      '' grossRevn,"
											+ "      DECODE(CPI.PARTY_TYPE,'2','RC','RI')||'46' RELSHIP,"
											+ "      '' JOB_CODE, "
											+ "      CM.apply_date "
											+ "    FROM T_CS_PAYEE_INFO CPI "
											+ "      INNER JOIN V_CONTRACT_MASTER CM ON CM.POLICY_ID=CPI.POLICY_ID "
											+ "      left join uw uw on CM.policy_id=uw.policy_id "
											+ "      inner join t_Contract_Proposal prop on CM.POLICY_ID = prop.POLICY_ID "
											+ "    WHERE ((tcm.liability_state_date >= ?-28 and tcm.liability_state_date < ? and tcm.liability_state = 3) or (uw.LAST_UNDERWRITER_TIME >= ?-28 and uw.LAST_UNDERWRITER_TIME < ? and tcm.liability_state = 0 and prop.PROPOSAL_STATUS in('82','84','86'))) "
											+ "      AND CPI.PARTY_TYPE = '1'"
											+ "      AND NOT EXISTS ("
											+ "        SELECT "//要保人
											+ "          PH.CERTI_CODE "
											+ "        FROM V_POLICY_HOLDER PH "
											+ "          INNER JOIN V_CONTRACT_MASTER CM2 on CM2.POLICY_ID=PH.POLICY_ID "
											+ "          left join uw uw on CM2.policy_id=uw.policy_id "
											+ "          inner join t_Contract_Proposal prop2 on CM2.POLICY_ID = prop2.POLICY_ID "
											+ "        WHERE ((tcm.liability_state_date >= ?-28 and tcm.liability_state_date < ? and tcm.liability_state = 3) or (uw.LAST_UNDERWRITER_TIME >= ?-28 and uw.LAST_UNDERWRITER_TIME < ? and tcm.liability_state = 0 and prop.PROPOSAL_STATUS in('82','84','86'))) "
											+ "          AND PH.CERTI_CODE = CPI.CERTI_CODE"
											+ "        UNION ALL"
											+ "        SELECT  IL.CERTI_CODE "//被保人
											+ "        FROM V_INSURED_LIST IL "
											+ "          INNER JOIN V_BENEFIT_INSURED BI on IL.POLICY_ID = BI.POLICY_ID "
											+ "          INNER JOIN V_CONTRACT_MASTER CM2 on CM2.POLICY_ID=IL.POLICY_ID "
											+ "          left join uw uw on CM2.policy_id=uw.policy_id "
											+ "          inner join t_Contract_Proposal prop2 on CM2.POLICY_ID = prop2.POLICY_ID "
											+ "        WHERE ((tcm.liability_state_date >= ?-28 and tcm.liability_state_date < ? and tcm.liability_state = 3) or (uw.LAST_UNDERWRITER_TIME >= ?-28 and uw.LAST_UNDERWRITER_TIME < ? and tcm.liability_state = 0 and prop.PROPOSAL_STATUS in('82','84','86'))) "
											+ "          AND IL.CERTI_CODE = CPI.CERTI_CODE "
											+ "      ) "
											+ "    UNION ALL "
											+ "    SELECT "//代理人
											+ "      'N' ORG_INDI,"
											+ "      CPI.LEGAL_AGENT_CERTI_CODE CERTI_CODE,"
											+ "      '0000' ACTIVE_STATUS,"
											+ "      CPI.LEGAL_AGENT_NAME NAME,"
											+ "      '' ENGLISH_NAME,"
											+ "      '' GENDER,"
											+ "      CPI.LEGAL_AGENT_NATION NATIONALITY,"
											+ "      CPI.LEGAL_AGENT_NATION2 NATIONALITY2,"
											+ "      '' COUNTRY,"
											+ "      CPI.LEGAL_AGENT_BIRTH BIRTH_DATE,"
											+ "      CM.POLICY_CODE,"
											+ "      '' lived_outside_of_roc, "
											+ "      '' scale, "
											+ "      '' seniority, "
											+ "      '' grossRevn,"
											+ "      'RI46' RELSHIP,"
											+ "      '' JOB_CODE, "
											+ "      CM.apply_date "
											+ "    FROM T_CS_PAYEE_INFO CPI "
											+ "      INNER JOIN  V_CONTRACT_MASTER CM ON CM.POLICY_ID=CPI.POLICY_ID "
											+ "      left join uw uw on CM.policy_id=uw.policy_id "
											+ "      inner join t_Contract_Proposal prop on CM.POLICY_ID = prop.POLICY_ID "
											+ "    WHERE ((tcm.liability_state_date >= ?-28 and tcm.liability_state_date < ? and tcm.liability_state = 3) or (uw.LAST_UNDERWRITER_TIME >= ?-28 and uw.LAST_UNDERWRITER_TIME < ? and tcm.liability_state = 0 and prop.PROPOSAL_STATUS in('82','84','86'))) "
											+ "      AND CPI.PARTY_TYPE='1' "
											+ "      AND CPI.LEGAL_AGENT_CERTI_CODE IS NOT NULL "
											+ "    ) c"
											+ "    where not exists "
											+ "    ("
											+ "      select b.certi_code from aml_temp_data b where b.certi_code = c.CERTI_CODE"
											+ "    )   "
											+ ") a order by cinNo, appDate desc ";
			
	//綜合查詢付款人
	private String aml_query_payee_date_sql=  "with uw AS ( "
											+ "  select * from ( " 
											+ "  select "
											+ "  tup.underwrite_id, " 
											+ "  tup.policy_id, " 
											+ "  tup.last_underwriter_time, " 
											+ "  row_number() over(partition by tup.policy_id order by tup.underwrite_id desc) row_number " 
											+ "  FROM t_uw_policy tup " 
											+ "  INNER JOIN v_contract_master tcm ON tcm.policy_id = tup.policy_id " 
											+ "  where tup.last_underwriter_time >= ?-28 "  
											+ "  ) d WHERE d.row_number=1 " 
											+ ") "   
											+ "  select distinct " 
											+ "  a.CERTI_CODE || '_EBAO' cinNo, " 
											+ "  '001' status, "
											+ "  to_char(a.apply_date,'YYYY-MM-DD') appDate, "
											+ "  to_char(?,'YYYY-MM-DD') openDate, "
											+ "  '' closeDate, "
											+ "  a.CERTI_CODE regId, "
											+ "  a.NAME name, "
											+ "  a.ENGLISH_NAME enName, "
											+ "  a.GENDER gender, "
											+ "  to_char(a.BIRTH_DATE,'YYYY-MM-DD') bierhdate, "
											+ "  a.COUNTRY ctrOfHome, "
											+ "  a.LIVED_OUTSIDE_OF_ROC ctrOfOth, "
											+ "  a.JOB_CODE prof, "
											+ "  a.NATIONALITY primNat, "
											+ "  a.NATIONALITY2 secNat, "
											+ "  'I' usage, "
											+ "  'CMD' Unit, "
											+ "  '' viewDate, "
											+ "  '' createdDate, "
											+ "  to_char(?,'YYYY-MM-DD') updatedDate, "
											+ "  'EBAO' EXT_SYS_CODE, "
											+ "  'N' EXT_IS_SCAN, "
											+ "  'N' EXT_IS_RISK, "  
											+ "  'N' EXT_IS_TM, "
											+ "  'AMLFULL' EXT_USER_AD, "
											+ "  'FULL' EXT_EMPLOYEE_NAME " 
											+ "from ( "
											+ "    select 'N' PARTY_TYPE, "
											+ "    '0001' ACTIVE_STATUS, " 
					       					+ "    tcm.APPLY_DATE, "
											+ "    tcm.VALIDATE_DATE, "
					       				    + "  vp.NAME, "
											+ "  '' ENGLISH_NAME, "
											+ "  UPPER(TO_SINGLE_BYTE(vp.certi_code)) CERTI_CODE, "
											+ "  DECODE(vp.GENDER, 'N', 'U', vp.GENDER) GENDER, " 
											+ "  vp.BIRTH_DATE, "
											+ "  '' NATIONALITY, "
											+ "  '' NATIONALITY2, " 
											+ "  '' lived_outside_of_roc, " 
											+ "  '' COUNTRY, " 
											+ "  '' COMPANY_CATEGORY, "  
											+ "  '' scale, " 
											+ "  '' seniority, "  
											+ "  '' grossRevn, " 
											+ "  '' JOB_CODE, " 
											+ "  tcm.POLICY_CODE, "
											+ "  'RI44' relShip, "
											+ "  '' relCType, "
											+ "  vp.certi_code relCinNo " 
											+ "  FROM v_payer vp " 
											+ "  inner join v_Contract_Master tcm on tcm.POLICY_ID = vp.POLICY_ID " 
											+ "  left join uw uw on tcm.policy_id=uw.policy_id "  
											+ "  inner join t_Contract_Proposal prop on vp.POLICY_ID = prop.POLICY_ID " 
											+ "  left join t_Party tp on tp.PARTY_ID = vp.PARTY_ID " 
											+ "  where tp.PARTY_TYPE='1' and ((tcm.liability_state_date >= ?-28 and tcm.liability_state_date < ? and tcm.liability_state = 3) or (uw.LAST_UNDERWRITER_TIME >= ?-28 and uw.LAST_UNDERWRITER_TIME < ? and tcm.liability_state = 0 and prop.PROPOSAL_STATUS in('82','84','86'))) " 
											+ "  and  not exists " 
											+ "  ( "
											+ "    select b.certi_code from aml_temp_data b where b.certi_code = UPPER(TO_SINGLE_BYTE(vp.certi_code)) "
											+ "  ) "  
											+ ") a order by cinNo, appDate desc ";  
	
	@Override
	public int mainProcess() throws Exception {
		batchLogger.info("job start");
		DataSource dataSource =new DataSourceWrapper();
		DataSourceTransactionManager transactionManager = new DataSourceTransactionManager(dataSource);
        jdbcTemplate = new JdbcTemplate(transactionManager.getDataSource());
        transactionTemplate = new TransactionTemplate(transactionManager);
		
		int rc = JobStatus.EXECUTE_SUCCESS;

		try {
			batchLogger.info("start transfer data ");

			
			init();

			// 不分批寫出
			Date processDate = BatchHelp.getProcessDate();
			String formatStringProcessDate = DateFormatUtils.format(processDate, "yyyyMMdd");
			//String processDate = "2018315";
			final List<String> lines = new ArrayList<String>();
			
			//要保人
			jdbcTemplate.query(aml_policy_holder_date_sql, new RowCallbackHandler() {
				private int rowNo = 0;
				private int columnCount = 0;

				@Override
				public void processRow(ResultSet rs) throws SQLException {
					if (rowNo == 0) {
						ResultSetMetaData rsmd = rs.getMetaData();
						columnCount = rsmd.getColumnCount();
					}

					StringBuffer sb = new StringBuffer();
					for(int i = 1; i <= columnCount; i++) {
						String str = StringUtils.defaultString(rs.getString(i));
						str = StringUtils.trim(str);
						sb.append(str).append(SEPARATE_MARK);
					}
					sb.deleteCharAt(sb.length() - 1);
					lines.add(sb.toString());
					
					rowNo++;
				}			
			}, processDate, processDate, processDate, processDate, processDate, processDate, processDate);
			

			//受益人
			jdbcTemplate.query(aml_contract_bene_date_sql, new RowCallbackHandler() {
				private int rowNo = 0;
				private int columnCount = 0;

				@Override
				public void processRow(ResultSet rs) throws SQLException {
					if (rowNo == 0) {
						ResultSetMetaData rsmd = rs.getMetaData();
						columnCount = rsmd.getColumnCount();
					}

					StringBuffer sb = new StringBuffer();
					for(int i = 1; i <= columnCount; i++) {
						String str = StringUtils.defaultString(rs.getString(i));
						str = StringUtils.trim(str);
						sb.append(str).append(SEPARATE_MARK);
					}
					sb.deleteCharAt(sb.length() - 1);
					lines.add(sb.toString());
					
					rowNo++;
				}			
			}, processDate, processDate, processDate, processDate, processDate, processDate, processDate );
			/*
			//代理人,受益人之代理人,要保人之代理人,被保人之代理人
			jdbcTemplate.query(aml_lr_date_sql, new RowCallbackHandler() {
				private int rowNo = 0;
				private int columnCount = 0;

				@Override
				public void processRow(ResultSet rs) throws SQLException {
					if (rowNo == 0) {
						ResultSetMetaData rsmd = rs.getMetaData();
						columnCount = rsmd.getColumnCount();
					}

					StringBuffer sb = new StringBuffer();
					for(int i = 1; i <= columnCount; i++) {
						String str = StringUtils.defaultString(rs.getString(i));
						str = StringUtils.trim(str);
						sb.append(str).append(SEPARATE_MARK);
					}
					sb.deleteCharAt(sb.length() - 1);
					lines.add(sb.toString());
					
					rowNo++;
				}			
			}, processDate, processDate, processDate, processDate, processDate, processDate, processDate);
			

			
			//要保實質受益人
			jdbcTemplate.query(aml_aml_benefical_owner_date_sql, new RowCallbackHandler() {
				private int rowNo = 0;
				private int columnCount = 0;

				@Override
				public void processRow(ResultSet rs) throws SQLException {
					if (rowNo == 0) {
						ResultSetMetaData rsmd = rs.getMetaData();
						columnCount = rsmd.getColumnCount();
					}

					StringBuffer sb = new StringBuffer();
					for(int i = 1; i <= columnCount; i++) {
						String str = StringUtils.defaultString(rs.getString(i));
						str = StringUtils.trim(str);
						sb.append(str).append(SEPARATE_MARK);
					}
					sb.deleteCharAt(sb.length() - 1);
					lines.add(sb.toString());
					
					rowNo++;
				}			
			}, processDate, processDate, processDate, processDate, processDate, processDate, processDate);
			
			//理賠實質受益人
			jdbcTemplate.query(aml_claim_benefical_date_sql, new RowCallbackHandler() {
				private int rowNo = 0;
				private int columnCount = 0;

				@Override
				public void processRow(ResultSet rs) throws SQLException {
					if (rowNo == 0) {
						ResultSetMetaData rsmd = rs.getMetaData();
						columnCount = rsmd.getColumnCount();
					}

					StringBuffer sb = new StringBuffer();
					for(int i = 1; i <= columnCount; i++) {
						String str = StringUtils.defaultString(rs.getString(i));
						str = StringUtils.trim(str);
						sb.append(str).append(SEPARATE_MARK);
					}
					sb.deleteCharAt(sb.length() - 1);
					lines.add(sb.toString());
					
					rowNo++;
				}			
			}, processDate, processDate, processDate, processDate, processDate, processDate, processDate);
			*/
			//被保人
			jdbcTemplate.query(aml_benefit_insured_date_sql, new RowCallbackHandler() {
				private int rowNo = 0;
				private int columnCount = 0;

				@Override
				public void processRow(ResultSet rs) throws SQLException {
					if (rowNo == 0) {
						ResultSetMetaData rsmd = rs.getMetaData();
						columnCount = rsmd.getColumnCount();
					}

					StringBuffer sb = new StringBuffer();
					for(int i = 1; i <= columnCount; i++) {
						String str = StringUtils.defaultString(rs.getString(i));
						str = StringUtils.trim(str);
						sb.append(str).append(SEPARATE_MARK);
					}
					sb.deleteCharAt(sb.length() - 1);
					lines.add(sb.toString());
					
					rowNo++;
				}			
			}, processDate, processDate, processDate, processDate, processDate, processDate, processDate);
			/*
			//單次繳款之第三人,重複性繳款之第三人
			jdbcTemplate.query(aml_pa_trader_date_sql, new RowCallbackHandler() {
				private int rowNo = 0;
				private int columnCount = 0;

				@Override
				public void processRow(ResultSet rs) throws SQLException {
					if (rowNo == 0) {
						ResultSetMetaData rsmd = rs.getMetaData();
						columnCount = rsmd.getColumnCount();
					}

					StringBuffer sb = new StringBuffer();
					if(StringUtils.defaultString(rs.getString(5))!=null&&StringUtils.defaultString(rs.getString(5)).length()!=8) {
						for(int i = 1; i <= columnCount; i++) {
							String str = StringUtils.defaultString(rs.getString(i));
							str = StringUtils.trim(str);
							sb.append(str).append(SEPARATE_MARK);
						}
						sb.deleteCharAt(sb.length() - 1);
						lines.add(sb.toString());
					}
					
					rowNo++;
				}			
			}, processDate, processDate, processDate, processDate, processDate, processDate, processDate);
			//第三方受款人
			jdbcTemplate.query(aml_third_payee_date_sql, new RowCallbackHandler() {
				private int rowNo = 0;
				private int columnCount = 0;

				@Override
				public void processRow(ResultSet rs) throws SQLException {
					if (rowNo == 0) {
						ResultSetMetaData rsmd = rs.getMetaData();
						columnCount = rsmd.getColumnCount();
					}

					StringBuffer sb = new StringBuffer();
					for(int i = 1; i <= columnCount; i++) {
						String str = StringUtils.defaultString(rs.getString(i));
						str = StringUtils.trim(str);
						sb.append(str).append(SEPARATE_MARK);
					}
					sb.deleteCharAt(sb.length() - 1);
					lines.add(sb.toString());
					
					rowNo++;
				}			
			}, processDate, processDate, processDate, processDate, processDate, processDate, processDate, processDate, processDate, processDate, processDate, processDate, processDate, processDate, processDate, processDate, processDate, processDate, processDate);
			*/
			
			//綜合查詢付款人
			jdbcTemplate.query(aml_query_payee_date_sql, new RowCallbackHandler() {
				private int rowNo = 0;
				private int columnCount = 0;

				@Override
				public void processRow(ResultSet rs) throws SQLException {
					if (rowNo == 0) {
						ResultSetMetaData rsmd = rs.getMetaData();
						columnCount = rsmd.getColumnCount();
					}

					StringBuffer sb = new StringBuffer();
					if(StringUtils.defaultString(rs.getString(5))!=null&&StringUtils.defaultString(rs.getString(5)).length()!=8) {
						for(int i = 1; i <= columnCount; i++) {
							String str = StringUtils.defaultString(rs.getString(i));
							str = StringUtils.trim(str);
							sb.append(str).append(SEPARATE_MARK);
						}
						sb.deleteCharAt(sb.length() - 1);
						lines.add(sb.toString());
					}
					
					rowNo++;
				}			
			}, processDate, processDate, processDate, processDate, processDate, processDate, processDate);
			batchLogger.info("data count:", lines.size());
			if(lines.size()>0) {
				
				// output all rows
				doUpload(FILE_NAME+"_"+formatStringProcessDate+".csv", lines, FileGenericHelper.ENCODING_UTF_8);
	
				// 控制檔, TRUE 會清空控制檔目錄下的資料
				doUploadControl(SYSTEM_CODE, SERVICE_NAME, CONTROL_FILE_NAME, FileGenericHelper.ENCODING_UTF_8, false);
			}
			
			
			batchLogger.info("batch job success");
		}catch(Exception e) {
			e.printStackTrace();
			batchLogger.error("ERROR:", e);
			rc = JobStatus.EXECUTE_FAILED;
		}		
		batchLogger.info("batch job end");
		return rc;
	}

	@Override
	protected String getRootPath() {
		//return ROOT_PATH;
		return "C:\\EFB\\EBAO_AML\\";
	}

	@Override
	protected String getServiceKey() {
		return SERVICE_KEY;
	}
}
