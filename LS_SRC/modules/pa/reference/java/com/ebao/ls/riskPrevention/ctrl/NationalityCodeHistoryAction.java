package com.ebao.ls.riskPrevention.ctrl;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.ls.riskPrevention.bs.NationalityCodeLogService;
import com.ebao.ls.riskPrevention.vo.NationalityCodeLogVO;
import com.ebao.pub.framework.GenericAction;

public class NationalityCodeHistoryAction extends GenericAction{
	
	@Resource(name = NationalityCodeLogService.BEAN_DEFAULT)
	NationalityCodeLogService nationalityCodeLogService;
	
	@Override
	public ActionForward process(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		NationalityCodeForm cForm = (NationalityCodeForm)form;
		switch(cForm.getAction()){
			case SEARCH:
				NationalityCodeLogVO vo = new NationalityCodeLogVO();
				vo.setNationalCode(cForm.getVo().getNationalCode());
				request.setAttribute("listHistory", nationalityCodeLogService.nationalQuery(vo, cForm.getPager(), true));
				break;
			default:
				break;
		}
		return mapping.findForward("success");
	}
}
