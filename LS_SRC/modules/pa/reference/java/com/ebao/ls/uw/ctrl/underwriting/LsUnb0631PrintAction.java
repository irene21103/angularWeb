package com.ebao.ls.uw.ctrl.underwriting;

import java.sql.Blob;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.ls.report.cms.data.ReportAsynExecution;
import com.ebao.ls.report.cms.ds.CMSDSDelegate;
import com.ebao.ls.report.viewer.util.HttpAttachmentFileType;
import com.ebao.ls.report.viewer.util.HttpResponseUtils;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.framework.GenericAction;

/**
 * <p>Title: ç·šä¸Šä¸‹è¼‰å¥‘å¯©è¡¨</p>
 * <p>Description: ç·šä¸Šä¸‹è¼‰å¥‘å¯©è¡¨</p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company: TGL Co., Ltd.</p>
 * <p>Create Time: Sep 2, 2016</p> 
 * @author 
 * <p>Update Time: Sep 2, 2016</p>
 * <p>Updater: Alex Cheng</p>
 * <p>Update Comments: </p>
 */
public class LsUnb0631PrintAction extends GenericAction{
	
	private static Logger logger = LogManager.getLogger(LsUnb0631PrintAction.class);

	@Override
	public ActionForward process(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		//ReportResult result = null; // å›žå‚³å€¼
//		CheckDetailForm checkDetailForm = (CheckDetailForm) form;
//		String userId = AppContext.getCurrentUser().getRealName();
//		String generateDateS = checkDetailForm.getGenerateDateStart();
//		String generateDateE = checkDetailForm.getGenerateDateEnd();
//		String checkPolicyDateS = checkDetailForm.getCheckPolicyDateStart();
//		String checkPolicyDateE = checkDetailForm.getCheckPolicyDateEnd();
//		String checkStatus = checkDetailForm.getChkStatus(); 
//		String checkType = checkDetailForm.getCheckType();
//		String internalId = checkDetailForm.getProdInternalId();
//		String strVersion = checkDetailForm.getProdVersion();
//		String userName= checkDetailForm.getOwnerId();
//		String policyCode= checkDetailForm.getPolicyCode();
//		
//		Map<String, String> reportParameters = new HashMap<String, String>();
//		reportParameters.put("1433", userId); //å ±è¡¨å�ƒæ•¸ guardian_user
//		reportParameters.put("1269", generateDateS);//å ±è¡¨å�ƒæ•¸ é©—å–®æŠ½æ¨£ç”¢ç”Ÿæ—¥æœŸ(èµ·)
//		reportParameters.put("1271", generateDateE);//å ±è¡¨å�ƒæ•¸ é©—å–®æŠ½æ¨£ç”¢ç”Ÿæ—¥æœŸ(è¿„)
//		reportParameters.put("1264", checkPolicyDateS);//å ±è¡¨å�ƒæ•¸ é©—å–®å®Œæˆ�æ—¥æœŸ(èµ·)
//		reportParameters.put("1270", checkPolicyDateE);//å ±è¡¨å�ƒæ•¸ é©—å–®å®Œæˆ�æ—¥æœŸ(è¿„)
//		reportParameters.put("1268", checkStatus);//å ±è¡¨å�ƒæ•¸ é©—å–®ç‹€æ…‹
//		reportParameters.put("1262", checkType);//å ±è¡¨å�ƒæ•¸ é©—å–®é¡žåž‹
//		reportParameters.put("1267", internalId);//å ±è¡¨å�ƒæ•¸ å•†å“�ä»£ç¢¼
//		reportParameters.put("1265", strVersion);//å ±è¡¨å�ƒæ•¸ å•†å“�ç‰ˆæœ¬
//		reportParameters.put("1266", userName);//å ±è¡¨å�ƒæ•¸ é©—å–®äººå“¡
//		reportParameters.put("1263", policyCode);//å ±è¡¨å�ƒæ•¸ é©—å–®äººå“¡
//
//		String folderId = "1221";    //è³‡æ–™å¤¾ ID
//        String printFormat = "PDF"; //å ±è¡¨è¼¸å‡ºæ ¼å¼�
//        //TODO:Generate Report
		// TODO 改成call CrystalReportWSCI.downloadFile 取得檔案
//        result = new ReportGenerator().generateReport( folderId, reportParameters, printFormat );
/*
		if (result != null) {
			if (result.isSuccess) {
			    logger.info("**** Result is Success :: " + result.isSuccess);
			    //TODO:Query Report Execution
				ReportAsynExecution rptAsynExec = CMSDSDelegate
		                        .loadReportAsynExecution(result.executionID);
				String fileFormat = rptAsynExec.getFileFormat();
                String fileName = rptAsynExec.getReportDef()
                        .getReportName();
			    Blob resultFile = rptAsynExec.getResultFile();
                HttpResponseUtils.wirteAttachment(resultFile
                        .getBinaryStream(), response,
                        HttpAttachmentFileType
                                .getFileTypeByFormatName(fileFormat),
                        fileName);
			} else {
			    logger.info("**** Result is Success :: " + result.isSuccess
						+ "\n errorType :: " + result.errorType + "\n message :: "
						+ result.message);
			}
		}
		*/
		return mapping.findForward("defFile");
	}

	

}
