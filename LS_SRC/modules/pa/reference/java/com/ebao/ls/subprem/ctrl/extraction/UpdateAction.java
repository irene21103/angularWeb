package com.ebao.ls.subprem.ctrl.extraction;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.UserTransaction;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.ls.subprem.bs.UpdateActionService;
import com.ebao.pub.framework.AppException;
import com.ebao.pub.framework.ExceptionFactory;
import com.ebao.pub.framework.GenericAction;
import com.ebao.pub.framework.GenericException;
import com.ebao.pub.util.Trans;
import com.ebao.pub.util.TransUtils;
import com.ebao.pub.util.Log;
import com.ebao.pub.util.json.DateUtils;

/**
 * <p>Title:     </p>
 * <p>Description:   </p>
 * <p>Copyright: Copyright (c) 2002 </p>
 * <p>Company:  eBaoTech   </p>
 * <p>Create Time:  </p>
 * @author
 * @version
 */

public class UpdateAction extends GenericAction {
  public UpdateAction() {
  }

  /**
   * Extract
   * @param ActionMapping mapping
   * @param ActionForm form
   * @param HttpServletRequest request
   * @param HttpServletResponse response
   */
  @Override
  public ActionForward process(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws GenericException {

    UpdateForm updateForm = (UpdateForm) form;
    UserTransaction userTrans = null;

    String policyId;
    String policyNumber;
    String policyNumberD;
    String policyHolderName;
    String policyHolderNameD;

    String payMethod;
    String dueDate;
    String exDueDate;
    String policyStatus;
    String forward = "extract";
    String eventType = "";
    String result = "-1"; //0:success  -1:fale
    String loadSource = "";
    String errorCode;

    try {
      policyNumber = updateForm.getPolicyNumber();
      policyNumberD = updateForm.getPolicyNumberD();
      policyHolderName = updateForm.getPolicyHolderName();
      policyHolderNameD = updateForm.getPolicyHolderNameD();

      payMethod = updateForm.getPayMethod();
      dueDate = updateForm.getDueDate();
      exDueDate = DateUtils.getTimeByCustomPattern(updateForm.getExDueDate(), "MMyyyy");
      policyStatus = updateForm.getPolicyStatus();
      policyId = updateForm.getPolicyId();
      loadSource = updateForm.getLoadSource();
      eventType = updateForm.getEventType();

      errorCode = "";

      //policyId = updateActionDS.getPolicyIdByPolicyCode(policyNumber);

      request.setAttribute("policyId", policyId);
      request.setAttribute("loadSource", loadSource);
      request.setAttribute("policyNumber", policyNumber);

      if (eventType.equals("extract")) {
        request.setAttribute("policyNumber", policyNumber);
        request.setAttribute("policyNumberD", policyNumberD);
        request.setAttribute("policyHolderName", policyHolderName);
        request.setAttribute("policyHolderNameD", policyHolderNameD);
        request.setAttribute("payMethod", payMethod);
        request.setAttribute("dueDate", dueDate);
        request.setAttribute("exDueDate", updateForm.getExDueDate());
        request.setAttribute("policyStatus", policyStatus);

        userTrans = Trans.getUserTransaction();
        userTrans.begin();
        try {
          //Extract by procedure
          String sPolicyId = updateActionDS
              .getPolicyIdByPolicyCode(policyNumber);
          updateActionDS.singleExtractByPlSql(Long.valueOf(sPolicyId),
              exDueDate);
          result = "0";
          userTrans.commit();
          Long.parseLong(result);
        } catch (AppException e) {
          Log.error(this.getClass(), "", e);
          TransUtils.rollback(userTrans);
          errorCode = Long.toString(e.getErrorCode());
        } catch (Exception e) {
          throw e;
        }
        request.setAttribute("errorCode", errorCode);
        request.setAttribute("result", result);
      } else if (eventType.equals("exit")) {
        forward = "exit";
      }
    } catch (Exception ex) {
      throw ExceptionFactory.parse(ex);
    }
    return mapping.findForward(forward);
  }

  @Resource(name = UpdateActionService.BEAN_DEFAULT)
  private UpdateActionService updateActionDS;
}
