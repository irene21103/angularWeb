package com.ebao.ls.riskPrevention.bs.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.ebao.ls.foundation.service.GenericServiceImpl;
import com.ebao.ls.riskPrevention.bs.RiskCustomerLogService;
import com.ebao.ls.riskPrevention.data.RiskCustomerLogDao;
import com.ebao.ls.riskPrevention.data.bo.RiskCustomerLog;
import com.ebao.ls.riskPrevention.vo.RiskCustomerLogVO;
import com.ebao.pub.util.BeanUtils;
import com.ebao.pub.web.pager.Pager;

public class RiskCustomerLogServiceImpl extends GenericServiceImpl<RiskCustomerLogVO, RiskCustomerLog, RiskCustomerLogDao> implements RiskCustomerLogService{

	@Resource(name= RiskCustomerLogDao.BEAN_DEFAULT)
	public void setDao(RiskCustomerLogDao dao){
		super.setDao(dao);
	}
	
	@Override
	protected RiskCustomerLogVO newEntityVO() {
		return new RiskCustomerLogVO();
	}

	@Override
	public List<Map<String, Object>> query(RiskCustomerLogVO vo, Pager pager) {
		RiskCustomerLog bo = new RiskCustomerLog();
		bo.copyFromVO(vo, true, false);
		return super.getDao().query(bo, pager);
	}
	
	public List<RiskCustomerLogVO> queryByCertiCode(String certiCode){
		RiskCustomerLog bo = new RiskCustomerLog();
		bo.setCertiCode(certiCode);
		List<RiskCustomerLog> results = super.getDao().queryVO(bo, RiskCustomerLogDao.ORDER_BY_UPDATE_DESC);
		List<RiskCustomerLogVO> data = new ArrayList<RiskCustomerLogVO>();
		if(results!=null){
			for(RiskCustomerLog result: results){
				RiskCustomerLogVO vo = new RiskCustomerLogVO();
				BeanUtils.copyProperties(vo, result);
				data.add(vo);
			}
		}
		return data;
	}
	
	/**
	 * <p>Description : 是否有人工調整記錄</p>
	 * <p>Created By : simon.huang</p>
	 * <p>Create Time : Sep 22, 2016</p>
	 * @param certiCode
	 * @return
	 * @throws Exception
	 */
	public boolean isManualAdjustHistory(String certiCode) throws Exception {
		return super.getDao().isManualAdjustHistory(certiCode);
	}

    @Override
    public RiskCustomerLogVO queryNewestDateByCertiCode(String certiCode) {
        RiskCustomerLog bo = new RiskCustomerLog();
        bo.setCertiCode(certiCode);
        List<RiskCustomerLog> boList = getDao().queryVO(bo, RiskCustomerLogDao.ORDER_BY_UPDATE_DESC);
        if(boList.size() > 0) {
            RiskCustomerLogVO vo = new RiskCustomerLogVO();
            boList.get(0).copyToVO(vo, true);
            return vo;
        }
        return null;
    }
    
    @Override
    public RiskCustomerLogVO pageQuery (String certiCode) throws Exception {
        RiskCustomerLog bo = new RiskCustomerLog();
        bo.setCertiCode(certiCode);
        List<RiskCustomerLog> boList = getDao().queryVO(bo, RiskCustomerLogDao.PAGE_DEMAND_VO);
        if(boList.size() > 0) {
            RiskCustomerLogVO vo = new RiskCustomerLogVO();
            boList.get(0).copyToVO(vo, true);
            return vo;
        }
        return null;
    }

    @Override
    public List<Map<String, Object>> pageQuery(RiskCustomerLogVO vo, Pager pager) {
        RiskCustomerLog bo = new RiskCustomerLog();
        bo.copyFromVO(vo, true, false);
        return super.getDao().query(bo, pager, RiskCustomerLogDao.PAGE_DEMAND_MAP);
    }

    @Override
    public RiskCustomerLogVO queryByListID(Long listId) {
        RiskCustomerLog bo = new RiskCustomerLog();
        bo.setListId(listId);
        List<RiskCustomerLog> boList = super.getDao().queryVO(bo, null, RiskCustomerLogDao.QUERY_BY_LIST_ID);
        if(boList.size() > 0) {
            RiskCustomerLogVO vo = new RiskCustomerLogVO();
            boList.get(0).copyToVO(vo, true);
            return vo;
        }
        return null;
    }

}
