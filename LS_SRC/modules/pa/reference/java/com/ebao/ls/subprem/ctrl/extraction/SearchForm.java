package com.ebao.ls.subprem.ctrl.extraction;

import com.ebao.pub.framework.GenericForm;

/**
 * <p>Title:     </p>
 * <p>Description:   </p>
 * <p>Copyright: Copyright (c) 2002 </p>
 * <p>Company:  eBaoTech   </p>
 * <p>Create Time:  </p>
 * @author
 * @version
 */

public class SearchForm extends GenericForm {

  String policyId;
  String policyNumber;
  String policyNumberD;
  String policyHolderName;
  String policyHolderNameD;
  String firstName;
  String lastName;
  String policyHolderId;
  String dueDate;
  String policyStatus;
  String payMethod;
  String partyIdType;
  String partyIdNumber;
  String dateOfBirthD;
  String genderD;
  String eventType;
  String tempStr;
  //ArrayList arList;

  public SearchForm() {
  }

  //PolicyNumber
  public String getPolicyNumber() {
    return policyNumber;
  }
  public void setPolicyNumber(String policyNumber) {
    this.policyNumber = policyNumber;
  }
  //PolicyNumberD
  public String getPolicyNumberD() {
    return policyNumberD;
  }
  public void setPolicyNumberD(String policyNumberD) {
    this.policyNumberD = policyNumberD;
  }
  //PolicyHolderName
  public String getPolicyHolderName() {
    return policyHolderName;
  }
  public void setPolicyHolderName(String policyHolderName) {
    this.policyHolderName = policyHolderName;
  }
  //PolicyHolderNameD
  public String getPolicyHolderNameD() {
    return policyHolderNameD;
  }
  public void setPolicyHolderNameD(String policyHolderNameD) {
    this.policyHolderNameD = policyHolderNameD;
  }
  //policyHolderId
  public String getPolicyHolderId() {
    return policyHolderId;
  }
  public void setPolicyHolderId(String policyHolderId) {
    this.policyHolderId = policyHolderId;
  }
  //dueDate
  public String getDueDate() {
    return dueDate;
  }
  public void setDueDate(String dueDate) {
    this.dueDate = dueDate;
  }
  //policyStatus
  public String getPolicyStatus() {
    return policyStatus;
  }
  public void setPolicyStatus(String policyStatus) {
    this.policyStatus = policyStatus;
  }
  //payMethod
  public String getPayMethod() {
    return payMethod;
  }
  public void setPayMethod(String payMethod) {
    this.payMethod = payMethod;
  }
  //PartyIdType
  public String getPartyIdType() {
    return partyIdType;
  }
  public void setpartyIdType(String partyIdType) {
    this.partyIdType = partyIdType;
  }
  //PartyIdNumber
  public String getPartyIdNumber() {
    return partyIdNumber;
  }
  public void setPartyIdNumber(String partyIdNumber) {
    this.partyIdNumber = partyIdNumber;
  }

  //PolicyId
  public String getPolicyId() {
    return policyId;
  }
  public void setPolicyId(String policyId) {
    this.policyId = policyId;
  }
  //dateOfBirthD
  public String getDateOfBirthD() {
    return dateOfBirthD;
  }
  public void setDateOfBirthD(String dateOfBirthD) {
    this.dateOfBirthD = dateOfBirthD;
  }
  //genderD
  public String getGenderD() {
    return genderD;
  }
  public void setGenderD(String genderD) {
    this.genderD = genderD;
  }
  //eventType
  public String getEventType() {
    return eventType;
  }
  public void setEventType(String eventType) {
    this.eventType = eventType;
  }
  //tempStr
  public String getTempStr() {
    return tempStr;
  }
  public void setTempStr(String tempStr) {
    this.tempStr = tempStr;
  }
  /*
   //list
   public ArrayList getList(){
   return arList;
   }
   public void  setList(ArrayList arList){
   this.arList = arList; 
   }
   */

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

}
