package com.ebao.ls.uw.ctrl.ri;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.ls.cmu.pub.model.DocumentInput.DocumentPropsKey;
import com.ebao.ls.notification.extension.letter.nb.FmtUnb0110ExtensionVO;
import com.ebao.ls.notification.extension.letter.nb.FmtUnb0110NewEnsureVO;
import com.ebao.ls.notification.extension.letter.nb.FmtUnb0110RiCompanyVO;
import com.ebao.ls.notification.extension.letter.nb.NbLetterExtensionVO;
import com.ebao.ls.notification.extension.letter.nb.index.FmtUnb0110IndexVO;
import com.ebao.ls.notification.message.WSLetterUnit;
import com.ebao.ls.pa.pub.vo.CoverageVO;
import com.ebao.ls.pa.nb.ctrl.letter.NbLetterHelper;
import com.ebao.ls.pa.nb.ctrl.letter.NbNotificationHelper;
import com.ebao.ls.pa.nb.util.NumericUtils;
import com.ebao.ls.pa.nb.vo.ProposalRuleResultVO;
import com.ebao.ls.pa.pub.data.bo.UwRiFacExtend;
import com.ebao.ls.pa.pub.data.bo.UwRiFacExtendAttachFile;
import com.ebao.ls.pa.pub.data.bo.UwRiFacExtendMedInfo;
import com.ebao.ls.pa.pub.service.PolicyService;
import com.ebao.ls.pa.pub.vo.CoverageInsuredVO;
import com.ebao.ls.pa.pub.vo.NbReinsurerRequestsVO;
import com.ebao.ls.pa.pub.vo.NbReinsurerSpecVO;
import com.ebao.ls.pa.pub.vo.PolicyVO;
import com.ebao.ls.product.service.LifeProductService;
import com.ebao.ls.pty.ds.ReinsurerService;
import com.ebao.ls.pub.cst.Billcard;
import com.ebao.ls.uw.ctrl.letter.UNBLetterAction;
import com.ebao.ls.uw.data.ReinsurerDelegate;
import com.ebao.ls.uw.data.TUwRiFacExtendAttachFileDelegate;
import com.ebao.ls.uw.data.TUwRiFacExtendDelegate;
import com.ebao.ls.uw.data.TUwRiFacExtendMedInfoDelegate;
import com.ebao.ls.uw.ds.UwPolicyService;
import com.ebao.ls.uw.ds.UwRiApplyService;
import com.ebao.ls.uw.vo.UwPolicyVO;
import com.ebao.ls.uw.vo.UwSurveyLetterVO;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.i18n.lang.CodeTable;
import com.ebao.pub.i18n.lang.StringResource;
import com.ebao.pub.para.Para;
import com.ebao.pub.util.BeanUtils;

import edu.emory.mathcs.backport.java.util.Arrays;

public class UwRiApplySaveAction extends UNBLetterAction {

	@Resource(name = TUwRiFacExtendDelegate.BEAN_DEFAULT)
	private TUwRiFacExtendDelegate uwRiFacExtendDao;

	@Resource(name = TUwRiFacExtendMedInfoDelegate.BEAN_DEFAULT)
	private TUwRiFacExtendMedInfoDelegate uwRiFacExtendMedInfoDao;

	@Resource(name = TUwRiFacExtendAttachFileDelegate.BEAN_DEFAULT)
	private TUwRiFacExtendAttachFileDelegate uwRiFacExtendAttachFileDao;

	@Resource(name = UwRiApplyService.BEAN_DEFAULT)
	protected UwRiApplyService uwRiApplyService;
	
	@Resource(name = PolicyService.BEAN_DEFAULT)
	protected PolicyService policyService;
	
	@Resource(name = UwPolicyService.BEAN_DEFAULT)
	protected UwPolicyService uwPolicyService;
	
	@Resource(name = ReinsurerService.BEAN_DEFAULT)
	protected ReinsurerService reinsurerService;
	
	@Resource(name = ReinsurerDelegate.BEAN_DEFAULT)
	protected ReinsurerDelegate reinsurerDelegate;
	
	@Resource(name = LifeProductService.BEAN_DEFAULT)
	protected LifeProductService lifeProductService;
	
	@Resource(name = NbLetterHelper.BEAN_DEFAULT)
	private NbLetterHelper nbLetterHelper;
	@Resource(name = NbNotificationHelper.BEAN_DEFAULT)
	private NbNotificationHelper nbNotification;
	private static final String CHECK_FLAG = new String(new char[]{'\u25A0'});
	private static final String NOT_CHECK_FLAG = new String(new char[]{'\u25A1'});

	private UwRiApplySaveAction() {
	}

	/**
	 * @param path
	 * @return
	 */

	@Override
	public ActionForward process(ActionMapping mapping, ActionForm form,
					HttpServletRequest request, HttpServletResponse response)
					throws Exception {
		String subAction = null;
		UwRiApplyForm uwRiApplyForm = (UwRiApplyForm) form;
		subAction = uwRiApplyForm.getSubAction();

		UwRiFacExtend bo = new UwRiFacExtend();
//		String getbCardId = uwRiApplyForm.getBillCardId();
//		request.setAttribute("billCardId", getbCardId);
		BeanUtils.copyProperties(bo, form);
		/**
		String decision = uwRiApplyForm.getUwDecision();
		if (decision != null) {
			if ("uwStandard".equals(decision)) {
				bo.setUwStandard("Y");
			} else if ("uwSecondary".equals(decision)) {
				bo.setUwSecondary("Y");
			} else if ("uwDelay".equals(decision)) {
				bo.setUwDelay("Y");
			} else {
				bo.setUwRefuse("Y");
			}
		}
		uwRiFacExtendDao.save(bo);
		Long listId = bo.getListId();

		//再保公司區塊
		String[] arrayReinsurCompany = request.getParameterValues("reinsurerFieldCompany");
		String[] arrayReinsurerName = request.getParameterValues("reinsurerFieldName");
		String[] arrayReinsurerEmail = request.getParameterValues("reinsurerFieldEmail");

		String[] arrayReinsurerFaxReg = request.getParameterValues("reinsurerFieldFaxReg");
		String[] arrayReinsurerFax = request.getParameterValues("reinsurerFieldFax");

		String[] arrayReinsurerTelReg = request.getParameterValues("reinsurerFieldTelReg");
		String[] arrayReinsurerTel = request.getParameterValues("reinsurerFieldTel");
		String[] arrayReinsurerTelExt = request.getParameterValues("reinsurerFieldTelExt");
		for (int i = 0; i < arrayReinsurCompany.length; i++) {
			NbReinsurerRequestsVO nbReinsurerRequestsVO = new NbReinsurerRequestsVO();
			nbReinsurerRequestsVO.setExtendId(listId);
			nbReinsurerRequestsVO.setReinsurerId(NumericUtils.parseLong(arrayReinsurCompany[i]));
			nbReinsurerRequestsVO.setContact(arrayReinsurerName[i]);
			nbReinsurerRequestsVO.setEmail(arrayReinsurerEmail[i]);
			nbReinsurerRequestsVO.setFaxReg(NumericUtils.parseLong(arrayReinsurerFaxReg[i]));
			nbReinsurerRequestsVO.setFax(NumericUtils.parseLong(arrayReinsurerFax[i]));
			nbReinsurerRequestsVO.setTelReg(NumericUtils.parseLong(arrayReinsurerTelReg[i]));
			nbReinsurerRequestsVO.setTel(NumericUtils.parseLong(arrayReinsurerTel[i]));
			nbReinsurerRequestsVO.setTelExt(NumericUtils.parseLong(arrayReinsurerTelExt[i]));
			uwRiApplyService.reinsurerRequestsSaveOrUpdate(nbReinsurerRequestsVO);

			NbReinsurerSpecVO nbReinsurerSpecVO = new NbReinsurerSpecVO();
			nbReinsurerSpecVO.setReinsurerId(NumericUtils.parseLong(arrayReinsurCompany[i]));
			nbReinsurerSpecVO.setContact(arrayReinsurerName[i]);
			nbReinsurerSpecVO.setEmail(arrayReinsurerEmail[i]);
			nbReinsurerSpecVO.setFaxReg(NumericUtils.parseLong(arrayReinsurerFaxReg[i]));
			nbReinsurerSpecVO.setFax(NumericUtils.parseLong(arrayReinsurerFax[i]));
			nbReinsurerSpecVO.setTelReg(NumericUtils.parseLong(arrayReinsurerTelReg[i]));
			nbReinsurerSpecVO.setTel(NumericUtils.parseLong(arrayReinsurerTel[i]));
			nbReinsurerSpecVO.setTelExt(NumericUtils.parseLong(arrayReinsurerTelExt[i]));
			uwRiApplyService.reinsurerSpecSaveOrUpdate(nbReinsurerSpecVO);
		}

		//醫療區塊
		if (uwRiApplyForm.getMedInfo() != null) {
			if (uwRiApplyForm.getMedInfo().length > 0) {
				bo.setMedInfoIndi("Y");

				List<String> medInfoList = Arrays.asList(uwRiApplyForm.getMedInfo());
				for (String medInfo : medInfoList) {
					UwRiFacExtendMedInfo uwRiFacExtendMedInfo = new UwRiFacExtendMedInfo();
					uwRiFacExtendMedInfo.setExtendId(listId);
					uwRiFacExtendMedInfo.setName(medInfo);
					uwRiFacExtendMedInfoDao.save(uwRiFacExtendMedInfo);
				}
			}
		}
		//附件區塊
		if (uwRiApplyForm.getAttachFile() != null) {
			if (uwRiApplyForm.getAttachFile().length > 0) {
				List<String> attachFileList = Arrays.asList(uwRiApplyForm.getAttachFile());
				for (String attachFile : attachFileList) {
					UwRiFacExtendAttachFile uwRiFacExtendAttachFile = new UwRiFacExtendAttachFile();
					uwRiFacExtendAttachFile.setExtendId(listId);
					uwRiFacExtendAttachFile.setValue(attachFile);
					uwRiFacExtendAttachFileDao.save(uwRiFacExtendAttachFile);
				}
			}
		}

		if (subAction.equals("save")) {
			request.setAttribute("retMsg", "success");
		} else if (subAction.equals("online") || subAction.equals("perview")) { 
			long underwritingId = uwRiApplyForm.getUnderwriteId();
			UwPolicyVO uwPolicyVO = uwPolicyService.findUwPolicy(underwritingId);
			PolicyVO policyVO = policyService.loadPolicyByPolicyId(uwPolicyVO.getPolicyId());
			FmtUnb0110ExtensionVO extensionVO = getFmtUnb110ExtensionVO(
				uwRiApplyForm, request, policyVO,
				arrayReinsurCompany, arrayReinsurerName, 
				arrayReinsurerEmail, arrayReinsurerFaxReg, 
				arrayReinsurerFax, arrayReinsurerTelReg, 
				arrayReinsurerTel, arrayReinsurerTelExt);
			
			Long policyId = policyVO.getPolicyId();
			Long templateId = Long.valueOf("20005");
			WSLetterUnit unit = nbLetterHelper.generalLetterUnit(response, policyId, templateId, 
							null, null, extensionVO);
			unit.setIndex(getIndexVO(policyVO,templateId));
			
			if (subAction.equals("online")) {
				Long documentId = commonLetterService.sendLetter(policyId, 
	                            templateId, 
	                            unit, 
	                            policyId, 
	                            uwPolicyVO.getPolicyCode(),
	                            new HashMap<DocumentPropsKey, Object>());
				request.setAttribute("retMsg", "success");
			} else {
				super.previewLetter(response, templateId, unit);
			}
			
		}
		**/
		return mapping.findForward("uwRiApply");
	}

	@Override
	protected NbLetterExtensionVO getExtension(ActionForm unbLetterForm) {
		return null;
	}
	
	private FmtUnb0110IndexVO getIndexVO(PolicyVO policyVO, Long templateId) {
		FmtUnb0110IndexVO index = new FmtUnb0110IndexVO(); 
		this.nbNotification.setIndex(index, policyVO, templateId);
		return index;
	}
	
	/**
	 * <p>Description : for Test(create DD xml sample)</p>
	 * <p>Created By : Sunny Wu</p>
	 * <p>Create Time : Aug 20, 2016</p>
	 * @param policyId
	 * @return
	*/
	public FmtUnb0110ExtensionVO getFmtUnb110ExtensionVO(
		UwRiApplyForm actionForm, ServletRequest request, PolicyVO policyVO,
		String[] arrayReinsurCompany, String[] arrayReinsurerName, 
		String[] arrayReinsurerEmail, String[] arrayReinsurerFaxReg, 
		String[] arrayReinsurerFax, String[] arrayReinsurerTelReg, 
		String[] arrayReinsurerTel, String[] arrayReinsurerTelExt) {
		String langId = AppContext.getCurrentUser().getLangId();
		FmtUnb0110ExtensionVO extensionVO = new FmtUnb0110ExtensionVO();
		/**
		// init
		nbNotification.setLetterContractorAtUW(extensionVO, policyVO.getPolicyId());
		extensionVO.setNoticeDate(AppContext.getCurrentUserLocalTime());
		
		String riType = 
			CHECK_FLAG+StringResource.getStringData("MSG_1252196", langId)+
			NOT_CHECK_FLAG+StringResource.getStringData("MSG_1252197", langId);
		if (StringUtils.trimToEmpty(actionForm.getFacCate()).equals("2")) {
			riType = 
				NOT_CHECK_FLAG+StringResource.getStringData("MSG_1252196", langId)+
				CHECK_FLAG+StringResource.getStringData("MSG_1252197", langId);
		}
		extensionVO.setRiType(riType);
		
		// 公會通報 todo
		//extensionVO.setIsLiaRocUpload(
		//	NOT_CHECK_FLAG+StringResource.getStringData("MSG_109482", langId)+
		//	CHECK_FLAG+StringResource.getStringData("MSG_206894", langId));
	    List<String> liaRocComps = new ArrayList<String>();
	    List<String> liaRocAmounts = new ArrayList<String>();
//	    liaRocComps.add("todo");
//	    liaRocAmounts.add("9999");
	    
	    //extensionVO.setLiaRocAmounts(liaRocAmounts);
	    //extensionVO.setLiaRocComps(liaRocComps);
	    
	    // 醫療 & 體檢
	    extensionVO.setMedicalNormal((StringUtils.trimToEmpty(actionForm.getMedStatus()).equals("1")) ? CHECK_FLAG : NOT_CHECK_FLAG);
	    extensionVO.setMedicalUnusual((StringUtils.trimToEmpty(actionForm.getMedStatus()).equals("1")) ? NOT_CHECK_FLAG : CHECK_FLAG);
	    extensionVO.setMedicalUnusualText(StringUtils.trimToEmpty(actionForm.getMedStatusDesc()));
	    
	    String[] medInfo = actionForm.getMedInfo();
	    extensionVO.setMedicalExt((chkMedInfoIsExist(medInfo, "ME")) ? CHECK_FLAG : NOT_CHECK_FLAG);
	    extensionVO.setCxr((chkMedInfoIsExist(medInfo, "CXR")) ? CHECK_FLAG : NOT_CHECK_FLAG);
	    extensionVO.setEkg((chkMedInfoIsExist(medInfo, "EKG")) ? CHECK_FLAG : NOT_CHECK_FLAG);
	    extensionVO.setResting((chkMedInfoIsExist(medInfo, "Resting")) ? CHECK_FLAG : NOT_CHECK_FLAG);
	    extensionVO.setMasters((chkMedInfoIsExist(medInfo, "Master")) ? CHECK_FLAG : NOT_CHECK_FLAG);
	    extensionVO.setBruceProtocol((chkMedInfoIsExist(medInfo, "BP")) ? CHECK_FLAG : NOT_CHECK_FLAG);
	    extensionVO.setCbc((chkMedInfoIsExist(medInfo, "CBC")) ? CHECK_FLAG : NOT_CHECK_FLAG);
	    extensionVO.setBiochemistry((chkMedInfoIsExist(medInfo, "BIO")) ? CHECK_FLAG : NOT_CHECK_FLAG);
	    extensionVO.setUltrasound((chkMedInfoIsExist(medInfo, "UltraS")) ? CHECK_FLAG : NOT_CHECK_FLAG);
	    extensionVO.setAbdomen((chkMedInfoIsExist(medInfo, "ABD")) ? CHECK_FLAG : NOT_CHECK_FLAG);
	    extensionVO.setHeat((chkMedInfoIsExist(medInfo, "Heart")) ? CHECK_FLAG : NOT_CHECK_FLAG);
	    
	    extensionVO.setCaseHistory((StringUtils.trimToEmpty(actionForm.getMedHistory()).equals("Y")) ? CHECK_FLAG : NOT_CHECK_FLAG);
	    extensionVO.setSickForm((StringUtils.trimToEmpty(actionForm.getMedQuestion()).equals("Y")) ? CHECK_FLAG : NOT_CHECK_FLAG);
	    extensionVO.setSickName(StringUtils.trimToEmpty(actionForm.getMedDisease()));
	    extensionVO.setOther((!StringUtils.trimToEmpty(actionForm.getMedOther()).equals("")) ? CHECK_FLAG : NOT_CHECK_FLAG);
	    extensionVO.setOtherNote(StringUtils.trimToEmpty(actionForm.getMedOther()));
	    
	    //List<String> uwFiles = new ArrayList<String>();
	    //uwFiles.add(CHECK_FLAG+"要保書");
	    //uwFiles.add(NOT_CHECK_FLAG+"財務問卷");
	    //extensionVO.setUwFiles(uwFiles);
	    
	    // 其它文件
	    //List<String> otherFiles = new ArrayList<String>();
//	    otherFiles.add("附件一");
//	    otherFiles.add("附件二");
	    //extensionVO.setOtherFiles(otherFiles);
	    
	    extensionVO.setStandLife((StringUtils.trimToEmpty(actionForm.getUwDecision()).equals("uwStandard")) ? CHECK_FLAG : NOT_CHECK_FLAG);
	    if ((actionForm.getUwDecision().equals("uwSecondary"))) {
	    	extensionVO.setNoStandLife(CHECK_FLAG);
	    	extensionVO.setExtraPremReason(StringUtils.trimToEmpty(actionForm.getUwReason()));
	    } else {
	    	extensionVO.setNoStandLife(NOT_CHECK_FLAG);
	    	extensionVO.setExtraPremReason("");
	    }
	    extensionVO.setPostpone((StringUtils.trimToEmpty(actionForm.getUwDecision()).equals("uwDelay")) ? CHECK_FLAG : NOT_CHECK_FLAG);
	    if ((actionForm.getUwDecision().equals("uwDeny"))) {
	    	extensionVO.setReject(CHECK_FLAG);
	    	extensionVO.setRejectReason(StringUtils.trimToEmpty(actionForm.getUwReason()));
	    } else {
	    	extensionVO.setReject(NOT_CHECK_FLAG);
	    	extensionVO.setRejectReason("");
	    }
	    
	    extensionVO.setNote(StringUtils.trimToEmpty(actionForm.getUwDescr()));
	    extensionVO.setIsOIU((policyVO.isOIU()) ? CHECK_FLAG : NOT_CHECK_FLAG);
	    extensionVO.setBarcode1(Billcard.BARCODE1_FMT_UNB_0110);
	    extensionVO.setBarcode2("");
	    
		/* 再保公司
	    List<FmtUnb0110RiCompanyVO> riCompanyList = new ArrayList<FmtUnb0110RiCompanyVO>();
	    for (int i = 0 ; i < arrayReinsurCompany.length ; i++) {
	    	
	    	List<CoverageVO> coverages = policyVO.getCoverages();
	    	for (CoverageVO coverageVO : coverages) {
	    		
	    		List<CoverageInsuredVO> insureds = coverageVO.getInsureds();
	    		for (CoverageInsuredVO insuredVO : insureds) {
	    			
	    			
	    			FmtUnb0110RiCompanyVO riCompanyVO = new  FmtUnb0110RiCompanyVO();
	    	    	
	    	    	// 再保公司資料
	    	    	riCompanyVO.setRiCompanyName(arrayReinsurerName[i]);
	    	    	riCompanyVO.setRiContact(arrayReinsurerName[i]);
	    	    	riCompanyVO.setRiEmail(arrayReinsurerEmail[i]);
	    	    	riCompanyVO.setRiFax(
	    	    		arrayReinsurerFaxReg[i]+
	    	    		((!arrayReinsurerFaxReg[i].equals("")) ? "" : "-") +
	    	    		arrayReinsurerFax[i]);
	    	    	riCompanyVO.setRiTel(
	    	    		arrayReinsurerTelReg[i]+
	    	    		((!arrayReinsurerTelReg[i].equals("")) ? "" : "-") +
	    	    		arrayReinsurerTel[i] +
	    	    		((!arrayReinsurerTelExt[i].equals("")) ? "" : "Ext#") +
	    	    		arrayReinsurerTelExt[i]);
	    	    	
	    	    	// 被保險人資料
	    	    	riCompanyVO.setName(insuredVO.getInsured().getName());
	    	    	riCompanyVO.setGender(insuredVO.getInsured().getGender());
	    	    	riCompanyVO.setBirthday(insuredVO.getInsured().getBirthDate().toString());
	    	    	riCompanyVO.setAge(insuredVO.getEntryAge().toString());
	    	    	riCompanyVO.setJob(insuredVO.getInsured().getJobClass().toString());
	    	    	// 呼叫RMS接口取得(指未含本件的保額)
	    	    	riCompanyVO.setIsEnsure(
	    	    		NOT_CHECK_FLAG+StringResource.getStringData("MSG_109482", langId)+
	    	    		CHECK_FLAG+StringResource.getStringData("MSG_206894", langId));
	    	    	
	    	    	riCompanyVO.setEnsureLifeAmount("99999");
	    	    	riCompanyVO.setEnsureDisabilityAmount("99999999");
	    	    	riCompanyVO.setEnsureHealthAmount("888889");
	    	    	riCompanyVO.setEnsureMedicalAmount("1000");
	    			
	    	    	// 理賠資料
	    	    	riCompanyVO.setIsClamInfo(
    					NOT_CHECK_FLAG+StringResource.getStringData("MSG_109482", langId)+
    					CHECK_FLAG+StringResource.getStringData("MSG_206894", langId));
	    		    List<String> clamDates = new ArrayList<String>();
//	    		    clamDates.add("20150101");
//	    		    clamDates.add("20110101");
	    		    List<String> clamReasons = new ArrayList<String>();
//	    		    clamReasons.add("車禍");
//	    		    clamReasons.add("疾病");
	    		    riCompanyVO.setClamDates(clamDates);
	    		    riCompanyVO.setClamReasons(clamReasons);
	    		    
	    		    // 保障內容
	    		    List<FmtUnb0110NewEnsureVO> newEnsureList = new ArrayList<FmtUnb0110NewEnsureVO>();
	    		    FmtUnb0110NewEnsureVO newEnsureVO = new  FmtUnb0110NewEnsureVO();
	    		    newEnsureVO.setInsuredName(insuredVO.getInsured().getName());
	    		    newEnsureVO.setItemName(CodeTable.getCodeDesc("V_PRODUCT_LIFE", coverageVO.getProductId().toString()));
	    		    BigDecimal amount = coverageVO.getCurrentPremium().getSumAssured();
	    		    newEnsureVO.setAmount(amount.toString());
	    		    newEnsureVO.setIsAdd(
	    				NOT_CHECK_FLAG+StringResource.getStringData("MSG_109482", langId)+
	    				CHECK_FLAG+StringResource.getStringData("MSG_206894", langId));
	    		    newEnsureList.add(newEnsureVO);
	    		    riCompanyVO.setNewEnsureList(newEnsureList);
	    		    
	    		    riCompanyVO.setTotalLifeAmount("9999");
	    		    riCompanyVO.setTotalDangerAmount("999999");
	    		    riCompanyVO.setTotalRiAmount("99999");
	    		    riCompanyList.add(riCompanyVO);
	    		}
	    		
	    	}
	    }
	    
	    extensionVO.setRiCompanyList(riCompanyList);
		 */
		return extensionVO;
	}
	
	private boolean chkMedInfoIsExist(String[] infos, String info) {
		boolean rtn = false; 
		if (infos != null) {
			for (int i = 0 ; i < infos.length ; i++) {
				if (infos[i].equals(info)) {
					rtn = true;
					break;
				}
			}
		}
		return rtn;
	}
	
}
