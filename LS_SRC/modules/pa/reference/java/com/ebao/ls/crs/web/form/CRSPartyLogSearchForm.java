package com.ebao.ls.crs.web.form;

import com.ebao.ls.crs.constant.CRSCodeCst;
import com.ebao.ls.crs.vo.CRSPartyLogVO;
import com.ebao.pub.web.pager.PagerFormSimpleImpl;

public class CRSPartyLogSearchForm extends PagerFormSimpleImpl{
	
	private static final long serialVersionUID = 5502761013420260841L;
	
	private String policyType;
	
	private String sysCode;
	
	private String certiCode;
	
	private String name;
	
	private String policyCode;
	
	private String crsType;
	
	private String actionType;
	
	private String inputSource = CRSCodeCst.INPUT_SOURCE__CMN;
	
	private Long logId; 
	
	private java.util.List<String> msgInfoList;
	
	public Long getLogId() {
		return logId;
	}

	public void setLogId(Long logId) {
		this.logId = logId;
	}

	public String getInputSource() {
		return inputSource;
	}

	public void setInputSource(String inputSource) {
		this.inputSource = inputSource;
	}

	
	
	public String getCertiCode() {
		return certiCode;
	}

	public void setCertiCode(String certiCode) {
		this.certiCode = certiCode;
	}

	public String getName() {
		return name;
	}

	
	public void setName(String name) {
		this.name = name;
	}

	public String getPolicyCode() {
		return policyCode;
	}

	public void setPolicyCode(String policyCode) {
		this.policyCode = policyCode;
	}

	public String getCrsType() {
		return crsType;
	}

	public void setCrsType(String crsType) {
		this.crsType = crsType;
	}

	public java.util.List<String> getMsgInfoList() {
		return msgInfoList;
	}

	public void setMsgInfoList(java.util.List<String> msgInfoList) {
		this.msgInfoList = msgInfoList;
	}

	private java.util.List<CRSPartyLogVO> resultVOList;

	public java.util.List<CRSPartyLogVO> getResultVOList() {
		return resultVOList;
	}

	public void setResultVOList(java.util.List<CRSPartyLogVO> resultVOList) {
		this.resultVOList = resultVOList;
	}

	public String getPolicyType() {
		return policyType;
	}

	public void setPolicyType(String policyType) {
		this.policyType = policyType;
	}

	public String getSysCode() {
		return sysCode;
	}

	public void setSysCode(String sysCode) {
		this.sysCode = sysCode;
	}
	
	public String getActionType() {
		return actionType;
	}

	public void setActionType(String actionType) {
		this.actionType = actionType;
	}

}
