package com.ebao.ls.uw.ctrl.search;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ebao.ls.escape.helper.EscapeHelper;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.ls.uw.ctrl.ReqParamNameConstants;
import com.ebao.pub.framework.GenericAction;

/**
 * <p>Title: GEL Underwriting</p>
 * <p>Description: Underwriting History search Action</p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: eBaoTech</p>
 * <p>Create Time: 2004/08/25</p>
 * @author yixing.lu
 * @version 1.0
 */

public class UwHistorySearchAction extends GenericAction {
  public UwHistorySearchAction() {
  }
  /**
   * query  historic policy information
   * @param mapping The ActionMapping used to select this instance
   * @param form The optional ActionForm bean for this request (if any)
   * @param request                   Http Request
   * @param response                  Http Response
   * @return ActionForward            ActionForward
   * @throws java.lang.Exception      ApplicationException
   */
  public ActionForward process(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {
    // get proposalNO
    String proposalNO = request.getParameter(ReqParamNameConstants.PROPOSALNO);
    // get policyCode
    String policyCode = request.getParameter(ReqParamNameConstants.POLICYCODE);
    UwHistorySearchForm searchForm = new UwHistorySearchForm();
    // set proposalNO
    searchForm.setApplyCode(proposalNO);
    // set policyCode
    searchForm.setPolicyCode(policyCode);
    // set the query result uw_history_list
    searchForm.setResultSearch((java.util.Collection) request
        .getAttribute("uw_history_list"));
    EscapeHelper.escapeHtml(request).setAttribute("searchForm", searchForm);
    //ActionForward the page  UnderwritingHistory.jsp
    return mapping.findForward("search");
  }

}
