package com.ebao.ls.liaRoc.ctrl;

public class UploadLiaRocVO {

    Long policyId;
    
    String policyCode;

    String liarocUploadType;        

    public Long getPolicyId() {
        return policyId;
    }

    public void setPolicyId(Long policyId) {
        this.policyId = policyId;
    }

    public String getPolicyCode() {
        return policyCode;
    }

    public void setPolicyCode(String policyCode) {
        this.policyCode = policyCode;
    }

    public String getLiarocUploadType() {
        return liarocUploadType;
    }

    public void setLiarocUploadType(String liarocUploadType) {
        this.liarocUploadType = liarocUploadType;
    }

}
