package com.ebao.ls.liaRoc.ajax;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.math.RandomUtils;
import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;

import com.ebao.foundation.module.db.hibernate.HibernateSession3;
import com.ebao.foundation.module.web.action.ExceptionInfoUtils;
import com.ebao.ls.cs.logger.ApplicationLogger;
import com.ebao.ls.cs.logger.helper.AppLogDataHelper;
import com.ebao.ls.liaRoc.LiaRocCst;
import com.ebao.ls.liaRoc.bo.LiaRocDownload2020VO;
import com.ebao.ls.liaRoc.bo.LiaRocDownloadDetail2020VO;
import com.ebao.ls.liaRoc.bo.LiaRocUploadCoverage2020;
import com.ebao.ls.liaRoc.bo.LiaRocUploadDetail2020VO;
import com.ebao.ls.liaRoc.ci.LiaRocDownloadWSCI;
import com.ebao.ls.liaRoc.ci.LiaRocUpload2020CI;
import com.ebao.ls.liaRoc.data.LiaRocUpload2020Dao;
import com.ebao.ls.liaRoc.data.LiaRocUploadCoverage2020Dao;
import com.ebao.ls.liaRoc.vo.LiaRocUpload2020SendVO;
import com.ebao.ls.liaRoc.vo.LiaRocUploadDetail2020WrapperVO;
import com.ebao.ls.pa.nb.ctrl.pub.ajax.AjaxBaseService;
import com.ebao.ls.pa.nb.util.NBUtils;
import com.ebao.ls.pa.nb.util.NumericUtils;
import com.ebao.ls.pa.pub.MsgCst;
import com.ebao.ls.pa.pub.TableCst;
import com.ebao.ls.pa.pub.bs.coverage.CoverageService;
import com.ebao.ls.pa.pub.vo.CoveragePremium;
import com.ebao.ls.pa.pub.vo.CoverageVO;
import com.ebao.ls.pa.pub.vo.PolicyVO;
import com.ebao.ls.product.model.query.output.LifeProduct;
import com.ebao.ls.product.service.LifeProductService;
import com.ebao.ls.pub.CodeCst;
import com.ebao.pub.util.BeanUtils;
import com.ebao.pub.util.DateUtils;
import com.ebao.pub.util.StringUtils;
import com.ebao.pub.util.json.EbaoStringUtils;

public class Liaroc20AjaxService extends AjaxBaseService {

	@Resource(name = LiaRocUpload2020CI.BEAN_DEFAULT)
	private LiaRocUpload2020CI liaRocUpload2020CI;

	@Resource(name = com.ebao.ls.pa.pub.bs.PolicyService.BEAN_DEFAULT)
	private com.ebao.ls.pa.pub.bs.PolicyService policyDS;

	@Resource(name = CoverageService.BEAN_DEFAULT)
	private CoverageService coverageService;

	@Resource(name = LifeProductService.BEAN_DEFAULT)
	private LifeProductService lifeProductService;

	@Resource(name = LiaRocUpload2020Dao.BEAN_DEFAULT)
	private LiaRocUpload2020Dao liaRocUpload2020Dao;

	@Resource(name = LiaRocDownloadWSCI.BEAN_DEFAULT)
	private LiaRocDownloadWSCI liaRocDownloadWSCI;

	@Resource(name = LiaRocUploadCoverage2020Dao.BEAN_DEFAULT)
	private LiaRocUploadCoverage2020Dao liaRocUploadCoverage2020Dao;

	/**
	 * 
	 * @param in
	 * @param output
	 * @throws Exception
	 */
	public void getUploadDetailList(Map<String, Object> in, Map<String, Object> output) throws Exception {
		
	
		String subTabType = (String) in.get("subTabType");

		if ("liaroc20ByPolicy".equals(subTabType)) {
			liaroc20ByPolicy(in, output);

		} else if ("liaroc20ByRemote".equals(subTabType)) {
			liaroc20ByRemote(in, output);
		}
		
		Map<String, Object> codeTableMap = new HashMap<String, Object>();
		codeTableMap.put("salesChannel", NBUtils.getTCodeData("V_LIAROC_SALES_CHANNEL"));
		codeTableMap.put("payType", NBUtils.getTCodeData("V_LIAROC_PAY_TYPE"));
		codeTableMap.put("liaRocChargeMode", NBUtils.getTCodeData("V_LIAROC_CHARGE_MODE"));
		codeTableMap.put("liaRocRelationToPh", NBUtils.getTCodeData("V_LIAROC_RELATION_TO_PH"));
		codeTableMap.put("proposalStatus", NBUtils.getTCodeData(TableCst.T_PROPOSAL_STATUS));
		
		output.put("codeTable", codeTableMap);
	
	}
	
	private static String SQL_QUERY_REPORT_LIST = "SELECT  "
			+ " (SELECT COUNT(1) FROM T_LIAROC_DELAY_REPORT_DETAIL B WHERE B.REPORT_ID = A.REPORT_ID AND B.PROCESS_DONE= 'Y') AS Y_CNT, "
			+ " (SELECT COUNT(1) FROM T_LIAROC_DELAY_REPORT_DETAIL B WHERE B.REPORT_ID = A.REPORT_ID AND B.PROCESS_DONE= 'N') AS N_CNT "
			+ " FROM T_LIAROC_DELAY_REPORT A where a.REPORT_ID = ? ";
	
	public void liaroc20DelayReportRenew(Map<String, Object> in, Map<String, Object> output) {
		String reportId = (String) in.get("reportId");
		if(StringUtils.isNullOrEmpty(reportId) == false) {
			try {
		    	SQLQuery query = HibernateSession3.currentSession().createSQLQuery(SQL_QUERY_REPORT_LIST);
		    	query.setParameter(0, new Long(reportId));
		    	query.addScalar("Y_CNT", Hibernate.LONG);
		    	query.addScalar("N_CNT", Hibernate.LONG);
		    	Object[] obj = (Object[])query.uniqueResult();
		    	output.put("Y_CNT",obj[0]);
		    	output.put("N_CNT",obj[1]);
		    	
		    	output.put(KEY_SUCCESS, SUCCESS);
			} catch (Exception e) {
				output.put(KEY_SUCCESS, FAIL);
				output.put(KEY_ERROR_MSG, ExceptionInfoUtils.getExceptionMsg(e));
			} 
		} else {
			output.put(KEY_SUCCESS, FAIL);
			output.put(KEY_ERROR_MSG, NBUtils.getTWMsg(MsgCst.MSG_102601));
		}
	}

	/**
	 * 依保單號碼查找保項資料
	 * 
	 * @param in
	 * @param output
	 * @throws Exception
	 */
	public void liaroc20ByPolicy(Map<String, Object> in, Map<String, Object> output) throws Exception {

		String policyCode = (String) in.get("policyCode");
		String liarocUploadType = (String) in.get("liarocUploadType");

		if (policyCode != null && !StringUtils.isNullOrEmpty(policyCode)) {

			Integer proposalStatus = policyDS.getProposalStatusByPolicyCode(policyCode);
			
			List<Map<String,Object>> returnList = new ArrayList<Map<String,Object>>();
			
			Long policyId = policyDS.getPolicyIdByPolicyCode(policyCode);

			if (policyId == null) {
				output.put(KEY_SUCCESS, FAIL);
				output.put(KEY_ERROR_MSG, NBUtils.getTWMsg(MsgCst.MSG_1253498));
				return;
			}
			LiaRocUpload2020SendVO sendVO = liaRocUpload2020CI.getUploadVOByPolicyId(policyId, liarocUploadType,
					LiaRocCst.LIAROC_UL_BIZ_SOURCE_UNB);

			List<LiaRocUploadDetail2020VO> details = (List<LiaRocUploadDetail2020VO>) sendVO.getDataList();
		
			for (LiaRocUploadDetail2020VO detail : details) {
				detail.setStatusEffectDate(new Date());
				Map<String,Object> map = org.apache.commons.beanutils.PropertyUtils.describe(detail);
				map.put("proposalStatus", proposalStatus);
				returnList.add(map);
			}
			
			output.put("details", returnList);

		} else {
			output.put(KEY_SUCCESS, FAIL);
			output.put(KEY_ERROR_MSG, NBUtils.getTWMsg(MsgCst.MSG_102601));
		}
	}

	/**
	 * 公會端記錄查詢
	 * 
	 * @param in
	 * @param output
	 * @throws Exception
	 */
	public void liaroc20ByRemote(Map<String, Object> in, Map<String, Object> output) throws Exception {

		String certiCode = (String) in.get("certiCode");
		String liarocUploadType = (String) in.get("liarocUploadType");
		
		if (certiCode != null && !StringUtils.isNullOrEmpty(certiCode)) {
			LiaRocDownload2020VO download2020VO = liaRocDownloadWSCI.downloadForFixedUpload(certiCode);
		
			List<Map<String,Object>> returnList = new ArrayList<Map<String,Object>>();
			
			Map<String,Integer> proposalStatusMap = new HashMap<String, Integer>();
			Map<String,Long> policyIdMap = new HashMap<String, Long>();
			if (download2020VO != null) {

				for (LiaRocDownloadDetail2020VO downloadDetailVO : download2020VO.getDownloadDetails()) {
					LiaRocUploadDetail2020VO uploadDetailVO = new LiaRocUploadDetail2020VO();
					BeanUtils.copyProperties(uploadDetailVO, downloadDetailVO);
					uploadDetailVO.setCheckStatus(LiaRocCst.CHECK_STATUS_UPLOAD);
					uploadDetailVO.setItemId(new Long(RandomUtils.nextInt() * -1));
					uploadDetailVO.setStatusEffectDate(new Date());
					
					//因upload/download vo中的liaroc開始的屬性大小寫不同，需在作一次設定
					Map<String, Object> mapping = org.apache.commons.beanutils.PropertyUtils.describe(downloadDetailVO);
					for(String key : mapping.keySet()) {
						if(key.indexOf("liaroc") != -1) {
							Object value = mapping.get(key);
							String newKey = key.replace("liaroc", "liaRoc");
							BeanUtils.setPropertyValue(uploadDetailVO, newKey, value);
						}
					}
					
					Integer proposalStatus = proposalStatusMap.get(uploadDetailVO.getPolicyCode());
					Long policyId = policyIdMap.get(uploadDetailVO.getPolicyCode());
					if(proposalStatus == null) {
						proposalStatus = policyDS.getProposalStatusByPolicyCode(uploadDetailVO.getPolicyCode());
						proposalStatusMap.put(uploadDetailVO.getPolicyCode(), proposalStatus);
						
						policyId = policyDS.getPolicyIdByPolicyCode(uploadDetailVO.getPolicyCode());
						policyIdMap.put(uploadDetailVO.getPolicyCode(), policyId);
					}

					if (policyId != null) {
						LiaRocUploadCoverage2020 liaRocUploadCoverage2020 = liaRocUploadCoverage2020Dao
								.findByPolicyIdLiaRocPolicyCode(policyId, uploadDetailVO.getLiaRocPolicyCode());
						if (liaRocUploadCoverage2020 != null) {
							uploadDetailVO.setItemId(liaRocUploadCoverage2020.getItemId());
						}
					} else {
						List<LiaRocUploadCoverage2020> rsList = liaRocUploadCoverage2020Dao
								.findByProperty("liarocPolicyCode", uploadDetailVO.getLiaRocPolicyCode());
						if (rsList != null && rsList.size() == 1) {
							//只有一筆才用item_id
							uploadDetailVO.setItemId(rsList.get(0).getItemId());
						}
					}

					Map<String,Object> map = org.apache.commons.beanutils.PropertyUtils.describe(uploadDetailVO);
					map.put("proposalStatus", proposalStatus);
					
					if(LiaRocCst.LIAROC_UL_TYPE_INFORCE.equals(liarocUploadType)) {
						if(NBUtils.in(downloadDetailVO.getLiarocType(), LiaRocCst.LIAROC_TYPE_INFORCE)) {
							returnList.add(map);
						}
					} else {
						if(NBUtils.in(downloadDetailVO.getLiarocType(), LiaRocCst.LIAROC_TYPE_RECEIVE)) {
							returnList.add(map);
						}
					}
				}	
			}
			output.put("details", returnList);

		} else {
			output.put(KEY_SUCCESS, FAIL);
			output.put(KEY_ERROR_MSG, NBUtils.getTWMsg(MsgCst.MSG_102601));
		}
	}

	/**
	 * 保額/單位/計劃 異動重新計算給付項目金額
	 */
	public void calcLiabilityAmount(Map<String, Object> in, Map<String, Object> output) {

		Long itemId = NumericUtils.parseLong(in.get("itemId"));
		String amount = (String) in.get("amount");

		if (itemId != null && !StringUtils.isNullOrEmpty(amount)) {

			CoverageVO coverageVO = coverageService.load(itemId);
			PolicyVO policyVO = policyDS.load(coverageVO.getPolicyId());

			LifeProduct lifeProduct = lifeProductService.getProductByVersionId(coverageVO.getProductVersionId());
			// 看商品設定決定amount屬於單位, 計畫別或是保額
			String unitFlag = lifeProduct.getProduct().getUnitFlag();
			CoveragePremium prem = coverageVO.getCurrentPremium();
			if (CodeCst.UNIT_FLAG_BY_UNIT.equals(unitFlag)) {
				prem.setUnit(new BigDecimal(amount));
			} else if (CodeCst.UNIT_FLAG_BY_BENEFIE_LEVEL.equals(unitFlag)) {
				if ("0".equals(amount)) {
					// DD 說明：元/單位/計劃別(核心將0轉B)
					prem.setBenefitLevel("B");
				} else {
					prem.setBenefitLevel(amount);
				}
			} else {
				prem.setSumAssured(new BigDecimal(amount));
			}

			try {
				LiaRocUploadDetail2020WrapperVO detailMapVO = liaRocUpload2020CI.generateUploadDetail(policyVO,
						coverageVO, LiaRocCst.LIAROC_UL_TYPE_RECEIVE);
				// 01~19
				output.put("detail", detailMapVO);

				output.put(KEY_SUCCESS, SUCCESS);
			} catch (Exception e) {
				output.put(KEY_SUCCESS, FAIL);
				output.put(KEY_ERROR_MSG, ExceptionInfoUtils.getExceptionMsg(e));
			}

		} else {
			output.put(KEY_SUCCESS, FAIL);
			output.put(KEY_ERROR_MSG, NBUtils.getTWMsg(MsgCst.MSG_102601));
		}
	}

	/**
	 * 新公會通報資料寫入
	 * 
	 * @param in
	 * @param output
	 * @throws Exception
	 */
	public void upload(Map<String, Object> in, Map<String, Object> output) throws Exception {

		@SuppressWarnings("unchecked")
		List<Map<String, Object>> details = (List<Map<String, Object>>) in.get("details");

		List<LiaRocUploadDetail2020VO> totalDetail2020List = new ArrayList<LiaRocUploadDetail2020VO>();

		if (details != null && details.isEmpty() == false) {

			ApplicationLogger.clear();
			ApplicationLogger.setSystemCode(AppLogDataHelper.SYSTEM_CODE_UNB);
			ApplicationLogger.setJobName("新式公會人工上傳");
			ApplicationLogger.setPolicyCode("Liaroc2020Upload");
			ApplicationLogger.setOptionId(DateUtils.date2String(new Date(), "yyyyMMdd HHmmss"));

			try {

				for (Map<String, Object> detail : details) {
					LiaRocUploadDetail2020VO detail2020VO = new LiaRocUploadDetail2020VO();
					/* 前端頁面欄位copy至form */
					EbaoStringUtils.setProperties(detail2020VO, detail);
					totalDetail2020List.add(detail2020VO);
				}

				// 明細檔的上傳分類區分收件/承保
				Map<String, List<LiaRocUploadDetail2020VO>> liaRocTypeMapList = NBUtils.toMapList(totalDetail2020List,
						"liaRocType");
				for (String liaRocType : liaRocTypeMapList.keySet()) {

					List<LiaRocUploadDetail2020VO> liaRocTypeDetailList = liaRocTypeMapList.get(liaRocType);

					// 主檔的上傳分類
					String mainLiarocType = LiaRocCst.LIAROC_UL_TYPE_RECEIVE;
					String descLiarocType = NBUtils.getTWMsg("MSG_1257654");
					
					if (LiaRocCst.INFORCE_LIAROC_TYPE_LIFE.equals(liaRocType)) {
						mainLiarocType = LiaRocCst.LIAROC_UL_TYPE_INFORCE;
						descLiarocType = NBUtils.getTWMsg("MSG_107547"); // 承保
					}

					// 區分主約保單號
					Map<String, List<LiaRocUploadDetail2020VO>> policyCodeMapList = NBUtils
							.toMapList(liaRocTypeDetailList, "policyCode");
					for (String policyCode : policyCodeMapList.keySet()) {

						List<LiaRocUploadDetail2020VO> policyDetailList = policyCodeMapList.get(policyCode);

						ApplicationLogger.addLoggerData(String.format("保單號：%s %s上傳 筆數：%s", policyCode, descLiarocType,
								policyDetailList.size()));

						// 收件/承保
						Long policyId = policyService.getPolicyIdByPolicyCode(policyCode);
						PolicyVO policyVO = null;
						if(policyId != null) {
							policyVO = policyService.load(policyId);	
						} else {
							//公會下載作上傳資料，可能會無法對應出保單號欄位AFINS， 進件可能會是受理編號或時間序
							policyVO = new PolicyVO();
							policyVO.setPolicyId(-1L);
							policyVO.setPolicyNumber(policyCode);
						}
						// 公會上傳主檔VO
						LiaRocUpload2020SendVO uploadVO = liaRocUpload2020CI.createLiarocUploadMaster(policyVO,
								mainLiarocType, LiaRocCst.LIAROC_UL_BIZ_SOURCE_MANUAL);
						
						//人工上傳設定902無需上傳
						uploadVO.setSend902Flag(LiaRocCst.LIAROC_UL_SEND_902_FLAG_2);
						uploadVO.setLiaRocUploadStatus902(LiaRocCst.LIAROC_UL_STATUS_SEND_NO_ITEM);
						//END
						
						uploadVO.setDataList(policyDetailList);
						
						liaRocUpload2020CI.saveUpload(uploadVO);

						liaRocUpload2020CI.restUpload(uploadVO);

						if (LiaRocCst.LIAROC_UL_STATUS_RETURN.equals(uploadVO.getLiaRocUploadStatus())) {
							// 發送完成且成功
							output.put(KEY_SUCCESS, SUCCESS);
						} else {
							if (LiaRocCst.LIAROC_UL_STATUS_SEND_FAIL.equals(uploadVO.getLiaRocUploadStatus())) {
								output.put(KEY_ERROR_MSG, "公會2.0  處理有誤無法取得回覆狀態");
							} else if (LiaRocCst.LIAROC_UL_STATUS_PROCESS_ERROR
									.equals(uploadVO.getLiaRocUploadStatus())) {
								output.put(KEY_ERROR_MSG, "公會2.0  連線有誤");
							} else {
								// 作業失敗
								output.put(KEY_ERROR_MSG, NBUtils.getTWMsg(MsgCst.MSG_1250722));
							}
							output.put(KEY_SUCCESS, FAIL);
						}
					}
				}
			} catch (Exception e) {
				ApplicationLogger.addLoggerData(ExceptionInfoUtils.getExceptionMsg(e));
			} finally {
				ApplicationLogger.flush();
			}
		}
	}

}
