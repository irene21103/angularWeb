package com.ebao.ls.uw.ctrl.underwriting;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.UserTransaction;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.ls.pa.nb.bs.ProposalProcessService;
import com.ebao.ls.pa.pub.ci.CoverageCI;
import com.ebao.ls.pa.pub.ci.PolicyCI;
import com.ebao.ls.pa.pub.service.PolicyService;
import com.ebao.ls.pa.pub.vo.CoverageVO;
import com.ebao.ls.pa.pub.vo.PolicyVO;
import com.ebao.ls.uw.ctrl.ReqParamNameConstants;
import com.ebao.ls.uw.ctrl.UwGenericAction;
import com.ebao.ls.uw.ds.UwProcessService;
import com.ebao.ls.uw.ds.UwRiApplyService;
import com.ebao.ls.uw.ds.constant.UwSourceTypeConstants;
import com.ebao.ls.uw.ds.sp.CompleteUWProcessSp;
import com.ebao.ls.uw.util.Utils;
import com.ebao.ls.uw.vo.UwCommentVO;
import com.ebao.ls.uw.vo.UwPolicyVO;
import com.ebao.pub.framework.ExceptionFactory;
import com.ebao.pub.framework.GenericException;
import com.ebao.pub.framework.internal.spring.DSProxy;
import com.ebao.pub.util.DateUtils;
import com.ebao.pub.util.Log;
import com.ebao.pub.util.Trans;
import com.ebao.pub.util.TransUtils;

/**
 * <p>
 * Title: GEL-UW
 * </p>
 * <p>
 * Description: Save selected proposal's status and returns to uw sharing pool
 * </p>
 * <p>
 * Copyright: Copyright (c) 2004
 * </p>
 * <p>
 * Company: ebaoTech Corporation
 * </p>
 * 
 * @author jason.luo
 * @version 1.0
 * @since 07.13.2004
 */
public class UwPolicySaveAndExitAction extends UwGenericAction {
  public static final String BEAN_DEFAULT = "/uw/uwPolicySave";

  /**
   * Main method of the action which making following changes to the system: 1.
   * save current policy status; 2. exit underwriting policy UI and return to
   * sharing pool UI.
   * 
   * @param mapping
   * @param form
   * @param request
   * @param response
   * @return
   * @throws GenericException
   */
  @Override
  public ActionForward uwProcess(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
                      throws GenericException {
      String NEXT_PAGE = "nextStep";
      try {
          Long underwriteId = Long.valueOf(request.getParameter("underwriteId"));
     
          // get the original underwriting policy
          UwPolicyVO uwPolicyVOFromDB = getUwPolicyDS().findUwPolicy(underwriteId);
          Collection products = getUwPolicyDS().findUwProductEntitis(
                          underwriteId);
          // 2015-07-09 chang by sunny 確認跟存檔要用同一個method存畫面資料
          helper.drawValueFromPage_TGL(request, uwPolicyVOFromDB, products);
          String uwSourceType = uwPolicyVOFromDB.getUwSourceType();
          // 2015-09-02 change by peter temp save 產生再保申請
          String[] riListId = request.getParameterValues("riListId");
          String[] riCompanyId = request.getParameterValues("riCompanyIds");
          String[] reinsuredResult = request.getParameterValues("reinsuredResult");
          String[] reinsuredDate = request.getParameterValues("reinsuredDate");
          if(riCompanyId!=null && riCompanyId.length>0){
          	for(int i=0; i<riListId.length; i++){
//          		uwRiApplyService.setRiCompanyInfo(riListId[i], reinsuredResult[i], reinsuredDate[i]);	
          	}
          }
          // 2015-07-02 Sunny 取得畫面上核保綜合意見欄結果
          List<UwCommentVO> optionList = helper
                          .getCommentLists(request);
          if (Log.isDebugEnabled(this.getClass())) {
              Log.info(UwPolicySaveAndExitAction.class,
                              "vo is " + uwPolicyVOFromDB.toString());
          }
          // update policy basic information
          // store policy info to avoid inputting repeatly
          UserTransaction userTransaction = Trans.getUserTransaction();
          try {
              userTransaction.begin();
              // 2015-07-02 Sunny 儲存畫面上核保綜合意見欄結果
              helper.saveUWComment(uwPolicyVOFromDB.getUnderwriteId(),
                              optionList);
              getUwPolicyDS().updateUwPolicy(uwPolicyVOFromDB, true);

              userTransaction.commit();
          } catch (Exception e) {
              TransUtils.rollback(userTransaction);
              throw ExceptionFactory.parse(e);
          }
          // remove company restrict for defect : GEL00036936 2007/12/13

          /** 呈核部分改到確認再做
            if ("Y".equalsIgnoreCase(uwPolicyVOFromDB.getManuEscIndi())
                            // && !StringUtils.isNullOrEmpty(uwEscaUser)
            && "Y".equals(request.getParameter("clickSaveExit"))) {
            UwPolicyVO uwPolicyFromDB = getUwPolicyDS().findUwPolicy(underwriteId);
            uwProcessDS.manualEscalteOnePolicy(uwPolicyFromDB);
            request.setAttribute("isEscalated", "Y");
            }
           */
          // when cs-uw,synchronize records
          if (Utils.isCsUw(uwPolicyVOFromDB.getUwSourceType())) {
              CompleteUWProcessSp.synUWInfo4CS(uwPolicyVOFromDB.getPolicyId(),
                              uwPolicyVOFromDB.getUnderwriteId(), uwPolicyVOFromDB.getChangeId(),
                              Integer.valueOf(1));
          }
          // add to control NBU process with workflow engine,by robert.xu on
          // 2008.4.17
          boolean isNewbiz = UwSourceTypeConstants.NEW_BIZ
                          .equals(uwSourceType);

        
          if (isNewbiz && "Y".equals(request.getParameter("clickSaveExit"))) {
              // WfHelper.processManagerForSaveOrCancel4UW(uwPolicyVOFromDB
              // .getPolicyId());
              try {
                  ProposalProcessService service = DSProxy
                                  .newInstance(ProposalProcessService.class);
                  service.maintainFlowVariable(uwPolicyVOFromDB.getPolicyId());
              } catch (Exception e) {
                  throw ExceptionFactory.parse(e);
              }
              NEXT_PAGE = "nbWorkflowPage";
          }
          // add end
          return mapping.findForward(NEXT_PAGE);
      } catch (Exception e) {
          throw ExceptionFactory.parse(e);
      }
  }

  @Resource(name = UwProcessService.BEAN_DEFAULT)
  private UwProcessService uwProcessDS;

  @Resource(name = UwPolicySubmitActionHelper.BEAN_DEFAULT)
  private UwPolicySubmitActionHelper helper;
  
  @Resource(name = PolicyCI.BEAN_DEFAULT)
  private PolicyCI policyCI;

  @Resource(name = CoverageCI.BEAN_DEFAULT)
  private CoverageCI coverageCI;

  @Resource(name = PolicyService.BEAN_DEFAULT)
  private PolicyService policyService;
  
  @Resource(name = UwRiApplyService.BEAN_DEFAULT)
  protected UwRiApplyService uwRiApplyService;
  
}

