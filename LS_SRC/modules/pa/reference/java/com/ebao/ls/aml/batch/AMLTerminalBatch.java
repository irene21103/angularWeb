package com.ebao.ls.aml.batch;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

import javax.sql.DataSource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

import com.ebao.ls.batch.BatchLogger;
import com.ebao.ls.batch.file.AbstractFileUploadBatchJob;
import com.ebao.ls.batch.file.filehelper.FileGenericHelper;
import com.ebao.ls.pub.batch.impl.DataSourceWrapper;
import com.ebao.ls.ws.ci.cmn.int1342.CmnPayUnpaid1342OutputBatch;
import com.ebao.pub.batch.help.BatchHelp;
import com.ebao.pub.batch.job.BaseUnpieceableJob;
import com.ebao.pub.batch.type.JobStatus;
import com.ebao.pub.util.EnvUtils;

public class AMLTerminalBatch  extends AbstractFileUploadBatchJob {
	
	public static final String BEAN_DEFAULT = "amlTerminalOutBatch";
	private static final String ROOT_PATH = EnvUtils.getSendToAMLSharePath();

	private static final String SERVICE_KEY = "INT1376";
	/** 系統別 */
	private static final String SYSTEM_CODE = "AML";
	/** 服務名稱 */
	private static final String SERVICE_NAME = "EBAO_CIFDATA";
	/** 輸出之檔名 */
	private static final String FILE_NAME = "";
	/** 控制檔檔名 */
	private static final String CONTROL_FILE_NAME = "";
	/** separate */
	private static final String SEPARATE_MARK = "|";
	private static final Pattern WHITESPACE_BLOCK = Pattern.compile("\\s+");
	
	private JdbcTemplate jdbcTemplate;
	private TransactionTemplate transactionTemplate;
	protected BatchLogger batchLogger = BatchLogger.getLogger(AMLTerminalBatch.class);
	private String delte_aml_temp_date_sql="delete from aml_temp_data";
	private String insert_aml_temp_date_sql="insert into aml_temp_data (certi_code) "  
											+"select UPPER(TO_SINGLE_BYTE(certi_code)) from (  "
											+ "       select "//--要保人
											+ "         ph2.certi_code "
											+ "       from v_policy_holder ph2"
											+ "         inner join v_contract_master cm2 on cm2.policy_id=ph2.policy_id"
											+ "         inner join t_Contract_Proposal prop on cm2.POLICY_ID = prop.POLICY_ID"
											+ "       where cm2.liability_state in (1,2) or (cm2.liability_state ='0' and prop.PROPOSAL_STATUS >'0' and prop.PROPOSAL_STATUS <'82')"
											+ "       union"
											+ "       select  il2.certi_code"//被保人
											+ "       from v_insured_list il2"
											+ "         inner join t_benefit_insured bi2 on il2.policy_id = bi2.policy_id"
											+ "         inner join v_contract_master cm2 on cm2.policy_id=il2.policy_id"
											+ "         inner join t_Contract_Proposal prop on cm2.POLICY_ID = prop.POLICY_ID"
											+ "       where cm2.liability_state in (1,2) or (cm2.liability_state ='0' and prop.PROPOSAL_STATUS >'0' and prop.PROPOSAL_STATUS <'82')"
											+ "       union"
											+ "       select  cb2.certi_code"//--受益人
											+ "       from v_contract_bene cb2"
											+ "         inner join v_contract_master cm2 on cb2.policy_id = cm2.policy_id"
											+ "         inner join t_Contract_Proposal prop on cm2.POLICY_ID = prop.POLICY_ID  "
											+ "       where cm2.liability_state in (1,2) or (cm2.liability_state ='0' and prop.PROPOSAL_STATUS >'0' and prop.PROPOSAL_STATUS <'82')"
											+ "       union "
											+ "       select tc.CERTI_CODE"//實質受益人
											+ "       from T_BENEFICIAL_OWNER bo"
											+ "         inner join T_CUSTOMER tc on bo.BO_PARTY_ID=tc.CUSTOMER_ID"
											+ "       where bo.IS_VALID='Y' "
											+ "       union "
											+ "       select lr.CERTI_CODE"//受益人之法定代理人(此撈法為撈取全部代理人)
											+ "       FROM T_LEGAL_REPRESENTATIVE lr"
											+ "         inner join v_contract_master cm2 on cm2.POLICY_ID=lr.POLICY_ID"
											+ "         inner join t_Contract_Proposal prop on cm2.POLICY_ID = prop.POLICY_ID  "
											+ "       where (cm2.liability_state in (1,2) or (cm2.liability_state ='0' and prop.PROPOSAL_STATUS >'0' and prop.PROPOSAL_STATUS <'82')) and lr.rel_type = '3'"
											+ "       union "
											+ "       select nabo.identity_no"//要保實質受益人
											+ "       from T_NB_AML_BENEFICIAL_OWNER nabo"
											+ "         LEFT JOIN T_NB_AML_COMPANY NAC ON NAC.POLICY_ID = NABO.POLICY_ID"
											+ "         inner join v_contract_master cm2 on cm2.POLICY_ID=nabo.POLICY_ID"
											+ "         inner join t_Contract_Proposal prop on cm2.POLICY_ID = prop.POLICY_ID  "
											+ "       where NAC.LIST_ID is NULL and (cm2.liability_state in (1,2) or (cm2.liability_state ='0' and prop.PROPOSAL_STATUS >'0' and prop.PROPOSAL_STATUS <'82'))  "
											+ "       union"
											+ "       select TC.CERTI_CODE"//理賠實質受益人
											+ "       FROM v_contract_bene cb"
											+ "         inner join T_CUSTOMER tc on tc.customer_id =cb.party_id"
											+ "         inner join v_contract_master cm2 on cm2.POLICY_ID=cb.POLICY_ID"
											+ "         inner join t_Contract_Proposal prop on cm2.POLICY_ID = prop.POLICY_ID  "
											+ "       where  TC.CUSTOMER_ID IS NOT NULL and (cm2.liability_state in (1,2) or (cm2.liability_state ='0' and prop.PROPOSAL_STATUS >'0' and prop.PROPOSAL_STATUS <'82'))"
											+ "       union"
											+ "       select vp2.certi_code"//綜合查詢付款人       
											+ "       from v_payer vp2"
											+ "         inner join v_contract_master cm2 on vp2.policy_id = cm2.policy_id"
											+ "         inner join t_Contract_Proposal prop on cm2.POLICY_ID = prop.POLICY_ID  "
											+ "       WHERE (cm2.liability_state in (1,2) or (cm2.liability_state ='0' and prop.PROPOSAL_STATUS >'0' and prop.PROPOSAL_STATUS <'82'))"
											+ "       union"
											+ "       SELECT t1.trader_certi_code "//繳費歷史查詢之繳款人
											+ "       FROM t_pa_trader t1"
											+ "         inner join t_cash t2 on t1.fee_id = t2.fee_id"
											+ "         inner join v_contract_master cm2 on t2.policy_id = cm2.policy_id"
											+ "         inner join t_Contract_Proposal prop on cm2.POLICY_ID = prop.POLICY_ID  "
											+ "       WHERE (cm2.liability_state in (1,2) or (cm2.liability_state ='0' and prop.PROPOSAL_STATUS >'0' and prop.PROPOSAL_STATUS <'82')) "
											+ "       union "//第三人受款人
										    + "   	  SELECT CPI.CERTI_CODE "
										    + "		  FROM T_CS_PAYEE_INFO CPI " 
								            + "			INNER JOIN V_CONTRACT_MASTER CM2 ON CM2.POLICY_ID=CPI.POLICY_ID " 
										    + "		    inner join t_Contract_Proposal prop on cm2.POLICY_ID = prop.POLICY_ID "  
										    + "		  WHERE (cm2.liability_state in (1,2) or (cm2.liability_state ='0' and prop.PROPOSAL_STATUS >'0' and prop.PROPOSAL_STATUS <'82')) "      
										    + "		  union "//第三人受款人代理人
										    + "		  SELECT CPI.LEGAL_AGENT_CERTI_CODE CERTI_CODE "
										    + "		  FROM T_CS_PAYEE_INFO CPI " 
										    + "	        INNER JOIN V_CONTRACT_MASTER CM2 ON CM2.POLICY_ID=CPI.POLICY_ID " 
										    + "         inner join t_Contract_Proposal prop on cm2.POLICY_ID = prop.POLICY_ID "  
										    + "		  WHERE CPI.LEGAL_AGENT_CERTI_CODE IS NOT NULL AND (cm2.liability_state in (1,2) or (cm2.liability_state ='0' and prop.PROPOSAL_STATUS >'0' and prop.PROPOSAL_STATUS <'82')) "      
											+ ")";
	
			
	@Override
	public int mainProcess() throws Exception {
		batchLogger.info("job start");
		DataSource dataSource =new DataSourceWrapper();
		DataSourceTransactionManager transactionManager = new DataSourceTransactionManager(dataSource);
        jdbcTemplate = new JdbcTemplate(transactionManager.getDataSource());
        transactionTemplate = new TransactionTemplate(transactionManager);
		
		int rc = JobStatus.EXECUTE_SUCCESS;

		try {
			batchLogger.info("start transfer data ");
			Object status=transactionTemplate.execute(new TransactionCallback<Object>() {
	            @Override
	            public Object doInTransaction(TransactionStatus status) {
	            	try {						
	            		
	            		jdbcTemplate.update(delte_aml_temp_date_sql);
						jdbcTemplate.update(insert_aml_temp_date_sql);
			            
	            	}catch(Exception exception) {
	            		batchLogger.error("ERROR:", exception.getMessage());
	            		exception.printStackTrace();
	            		return "fail";
	            	}
					
					return "success";
	            }
			});
			if(!status.toString().equals("success")) {
				rc = JobStatus.EXECUTE_FAILED;
			}
			
			//init();
		
			batchLogger.info("batch job success");
		}catch(Exception e) {
			e.printStackTrace();
			batchLogger.error("ERROR:", e);
			rc = JobStatus.EXECUTE_FAILED;
		}		
		batchLogger.info("batch job end");
		return rc;
	}

	@Override
	protected String getRootPath() {
		return ROOT_PATH;
		//return "C:\\EFB\\EBAO_AML\\";
	}

	@Override
	protected String getServiceKey() {
		return SERVICE_KEY;
	}
}
