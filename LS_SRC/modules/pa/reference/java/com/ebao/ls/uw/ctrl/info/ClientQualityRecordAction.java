package com.ebao.ls.uw.ctrl.info;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.ls.pa.pub.bs.PolicyService;
import com.ebao.ls.pa.pub.vo.PolicyVO;
import com.ebao.ls.uw.ds.UwQmQueryService;
import com.ebao.ls.uw.ds.constant.Cst;
import com.ebao.ls.uw.vo.QualityManageListVO;
import com.ebao.pub.framework.GenericAction;

public class ClientQualityRecordAction extends GenericAction {

    @Resource(name = QualityRecordActionHelper.BEAN_DEFAULT)
    private QualityRecordActionHelper qualityRecordActionHelper;
    
    @Resource(name = UwQmQueryService.BEAN_DEFAULT)
    private UwQmQueryService uwqqService;
    
	@Resource(name = PolicyService.BEAN_DEFAULT)
    protected PolicyService policyService;
	
    @Override
    public ActionForward process(ActionMapping mapping, ActionForm form,
                    HttpServletRequest request, HttpServletResponse response)
                    throws Exception {
        
        String policyId = request.getParameter("policyId").toString();
        Set<String> certiCodeSet = new HashSet<String>();
        String forward = "display";
        
        //初始化要保人資訊
        qualityRecordActionHelper.findPolicyHolderCertiCodeByPolicyId(certiCodeSet,policyId);
        
        //初始化被保險人資訊
        qualityRecordActionHelper.findInsuredCertiCodeByPolicyId(certiCodeSet,policyId);
        
        //初始化受益人資訊
        qualityRecordActionHelper.findBeneCertiCodeByPolicyId(certiCodeSet,policyId);

	   	if( !certiCodeSet.isEmpty() ) {	  
	        PolicyVO policyVO = policyService.load(Long.valueOf(policyId));
		   	Date date = policyVO.getApplyDate();

	   		String qmType = Cst.QUALITY_MANAGER_TYPE_CUSTOMER;
	        List<QualityManageListVO> list = uwqqService.findByCertiCodeAndDate(certiCodeSet, date, qmType);
	        request.setAttribute("qualityRecordList", list);
	   	}       
        return mapping.findForward(forward);
    }

}
