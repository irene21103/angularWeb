package com.ebao.ls.uw.ds;

import javax.annotation.Resource;

import com.ebao.ls.foundation.service.GenericServiceImpl;
import com.ebao.ls.uw.data.UwExclusionDao;
import com.ebao.ls.uw.data.bo.UwExclusion;
import com.ebao.ls.uw.vo.UwExclusionVO;


public class UwExclusionServiceImpl extends GenericServiceImpl<UwExclusionVO, UwExclusion, UwExclusionDao> implements UwExclusionService {

	public Long findByUnderwriteIdsMsgId(Long[] underwriteIds, Long msgId){
		Long underwriteId = uwExclusionDao.findByUnderwriteIdsMsgId(underwriteIds, msgId);
		return underwriteId;
	}
	@Override
	protected UwExclusionVO newEntityVO() {
		return null;
	}

	@Resource(name = UwExclusionDao.BEAN_DEFAULT)
	private UwExclusionDao uwExclusionDao;
}
