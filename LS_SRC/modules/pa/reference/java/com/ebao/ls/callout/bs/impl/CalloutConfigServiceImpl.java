package com.ebao.ls.callout.bs.impl;

import com.ebao.ls.callout.bs.CalloutConfigService;
import com.ebao.ls.callout.data.CalloutConfigDao;
import com.ebao.ls.callout.data.bo.CalloutConfig;
import com.ebao.ls.callout.vo.CalloutConfigVO;
import com.ebao.ls.foundation.service.GenericServiceImpl;
import com.ebao.pub.framework.GenericException;
import com.ebao.pub.web.pager.Pager;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;

public class CalloutConfigServiceImpl extends GenericServiceImpl<CalloutConfigVO, CalloutConfig, CalloutConfigDao> implements CalloutConfigService {

    @Resource(name = CalloutConfigDao.BEAN_DEFAULT)
    @Override
    public void setDao(CalloutConfigDao dao) {
        super.setDao(dao);
    }

    @Override
    public List<Map<String, Object>> query(String configType, Long displayOrder, Pager pager) {
        CalloutConfig config = new CalloutConfig();
        config.setConfigType(configType);
        config.setDisplayOrder(displayOrder);
        return dao.query(config, pager);
    }

    @Override
    public void saveOrUpdateCalloutConfigVO(CalloutConfigVO vo)
            throws GenericException {

        CalloutConfig calloutConfig;
        if (vo.getListId() == null) { // TODO: throw Exception
            calloutConfig = new CalloutConfig();
        } else {
            calloutConfig = dao.load(vo.getListId());
        }
        calloutConfig.setData(vo.getData());
        dao.saveorUpdate(calloutConfig);
    }

    @Override
    public void saveOrUpdateForNBU(CalloutConfigVO vo) throws GenericException {
        boolean nullUpdate = false;
        CalloutConfig calloutConfig;
        if (vo.getListId() == null) {
            BigDecimal bd = dao.checkConfigValue(vo.getConfigType(), vo.getChannelOrg(), vo.getSalesChannelType(), vo.getProductId());
            if (bd != null) {
                calloutConfig = dao.load(bd.longValue());
                nullUpdate = true;
            } else {
                calloutConfig = new CalloutConfig();
            }
        } else {
            calloutConfig = dao.load(vo.getListId());
            nullUpdate = true;
        }
        calloutConfig.copyFromVO(vo, nullUpdate, true);
        dao.saveorUpdate(calloutConfig);
    }

    @Override
    protected CalloutConfigVO newEntityVO() {
        return new CalloutConfigVO();
    }

    @Override
    public BigDecimal findChannelPercent(Long ChannelorgId, Long channelType) {
        return dao.findpercentage(ChannelorgId, channelType);
    }

    @Override
    public boolean findCalloutConfig(CalloutConfigVO vo) {
        CalloutConfig calloutConfig = new CalloutConfig();
        calloutConfig.copyFromVO(vo, true, true);
        return dao.query(calloutConfig, null).size() > 0;
    }

    @Override
    public CalloutConfigVO findMatchConfig(String configType, Long channelType,
            List<Long> channelOrgId) {

        CalloutConfig config = dao.findMatchConfig(configType, channelType, channelOrgId);
        if (config != null) {
            return convertToVO(config);
        }
        return null;

    }

    @Override
    public CalloutConfigVO findMatchConfig(Long productId, String configType, Long channelType,
            List<Long> channelOrgId) {
        CalloutConfig config = dao.findMatchConfig(productId, configType, channelType, channelOrgId);
        return convertToVO(config);
    }

    @Override
    public List<CalloutConfig> query(String configType, Long displayOrder) {
        return dao.findByCalloutConfig(configType, displayOrder);
    }

    @Override
    public Boolean existsProductSampleRate(Long productId, String configType, Long channelType) {
        return dao.existsProductSampleRate(productId, configType, channelType);
    }

}
