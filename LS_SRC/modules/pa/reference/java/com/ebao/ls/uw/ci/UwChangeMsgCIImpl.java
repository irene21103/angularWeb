package com.ebao.ls.uw.ci;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.ebao.foundation.common.lang.DateUtils;
import com.ebao.foundation.module.db.DBean;
import com.ebao.ls.arap.vo.SealRecordEddaVO;
import com.ebao.ls.callout.vo.CalloutTransVO;
import com.ebao.ls.cs.commonflow.ds.ApplicationService;
import com.ebao.ls.cs.logger.ApplicationLogger;
import com.ebao.ls.cs.model.CSApplicationInfo;
import com.ebao.ls.cs.service.PolicyChangeBusinessService;
import com.ebao.ls.nb.bo.NbEstateLoan;
import com.ebao.ls.notification.ci.NbEventCI;
import com.ebao.ls.pa.nb.bs.ProposalProcessService;
import com.ebao.ls.pa.nb.bs.rule.ProposalRuleResultService;
import com.ebao.ls.pa.nb.rule.ValidatorUtils;
import com.ebao.ls.pa.nb.util.LoanRole;
import com.ebao.ls.pa.nb.util.NBUtils;
import com.ebao.ls.pa.nb.util.UnbCst;
import com.ebao.ls.pa.nb.util.UnbRoleType;
import com.ebao.ls.pa.nb.vo.ProposalRuleResultVO;
import com.ebao.ls.pa.pub.model.PolicyChangeInfo;
import com.ebao.ls.pa.pub.model.PolicyInfo;
import com.ebao.ls.pa.pub.service.PolicyService;
import com.ebao.ls.pa.pub.vo.PolicyPersonVO;
import com.ebao.ls.pa.pub.vo.UwChangeMsgVO;
import com.ebao.ls.pub.CodeCst;

import static com.ebao.ls.pub.cst.Language.LANGUAGE__CHINESE_TRAD;

import com.ebao.ls.pub.cst.ProposalRuleMsg;
import com.ebao.ls.pub.cst.UwChangeCode;
import com.ebao.ls.pub.util.TGLDateUtil;
import com.ebao.ls.uw.ds.UwChangeMsgService;
import com.ebao.ls.uw.ds.UwPolicyService;
import com.ebao.pub.event.EventConstant;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.framework.GenericException;
import com.ebao.pub.framework.RTException;
import com.ebao.pub.i18n.lang.StringResource;
import com.ebao.pub.rms.method.StringResourceUtil;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;

import static com.ebao.ls.pub.cst.UwChangeCode.UW_CHANGE_CODE__AGENTCALLOUT;
import static com.ebao.pub.framework.AppContext.getCurrentUserLocalTime;
import static com.ebao.pub.i18n.lang.StringResource.getStringData;
import static java.util.Optional.ofNullable;


/**
 * Title:Underwriting <br>
 * Description: <br>
 * Copyright: Copyright (c) 2016 <br>
 * Company: eBaoTech Corporation <br>
 *
 * @author ChiaHui.Su
 * @since Time:July 11, 2016
 * @version 1.0
 */
public class UwChangeMsgCIImpl implements UwChangeMsgCI {

    private static Logger logger = Logger.getLogger(UwChangeMsgCIImpl.class);

	@Resource(name = UwChangeMsgService.BEAN_DEFAULT)
	private UwChangeMsgService uwChangeMsgDS;
	
	@Resource(name = ProposalProcessService.BEAN_DEFAULT)
	private ProposalProcessService proposalProcessService;
	
	@Resource(name = UwPolicyService.BEAN_DEFAULT)
	private UwPolicyService uwPolicyService;
	
	@Resource(name = PolicyService.BEAN_DEFAULT)
	private PolicyService policyService;

	@Resource(name = com.ebao.ls.pa.pub.bs.PolicyService.BEAN_DEFAULT)
	private com.ebao.ls.pa.pub.bs.PolicyService policyDS;
	
	@Resource(name = PolicyChangeBusinessService.BEAN_DEFAULT)
	private PolicyChangeBusinessService csService;
	
	@Resource(name = ApplicationService.BEAN_DEFAULT)
	private ApplicationService applicationService;
	
	@Resource(name = NbEventCI.BEAN_DEFAULT)
	private NbEventCI nbEventCI;

	@Resource(name = ProposalRuleResultService.BEAN_DEFAULT)
	protected ProposalRuleResultService proposalRuleResultService;

	private static int[] policyChangeLoanItemSerIds = UnbCst.POLICY_CHANGE_LOAN_SERVICE_ITEM__LOAN; // 保全契變借款相關項目:保單借款 
	private static int[] policyChangeLoanSurrenderSerIds = UnbCst.POLICY_CHANGE_LOAN_SERVICE_ITEM__SURRENDER; // 保全契變借款相關項目:保單解約
	private static int[] policyChangeLoanDecreaseSerIds = UnbCst.POLICY_CHANGE_LOAN_SERVICE_ITEM__DECREASE; // 保全契變借款相關項目:保單部分解約
	private static int[] policyChangeLoanReduceSerIds = UnbCst.POLICY_CHANGE_LOAN_SERVICE_ITEM__REDUCE; // 保全契變借款相關項目:減額減清
    
	/** PCR_462047_新契約系統需求(風險屬性評估表) 查找他件待核保或核保中保單且不存在核保中異動 */
	private String CHANGE_CODE_26_SQL = "with tmp as (select apply_time,policy_code from t_cs_application c join t_contract_master m on c.policy_id=m.policy_id where change_id = ?)  "+
			" select  ph.policy_id,tmp.apply_time,tmp.policy_code As pos_policy_code from t_policy_holder ph "+
			"  join t_contract_proposal cpl on ph.policy_id = cpl.policy_id "+
			"  join tmp on 1=1 "+
			" where ph.policy_id <> ? and cpl.proposal_status between 30 and 32  "+
			" and exists (select 1 from t_policy_holder ph2 where ph2.policy_id = ? and ph2.certi_code = ph.certi_code) "+    
			" and not exists (select 1 from t_uw_change_msg uwcm where uwcm.policy_id = ph.policy_id and uwcm.change_code = '23' and uwcm.biz_id = ? )" ;

	public UwChangeMsgService getUwChangeMsgDS() {
		return this.uwChangeMsgDS;
	}

	/**
	 * <p>Description : 
	 * PCR-155619-核印失敗修改保單續期繳費方式規則未含新契約件
	 * 一.保單待核保、核保中:核印失敗
	 *     (1)修改續期繳費方式=自行繳納,
	 *     (2)同時寫核保中異動資訊
	 * 二.保單待承保、有條件接受:核印失敗
	 *     (1)修改續期繳費方式=自行繳納,
	 *     (2)首期不能重算保費,僅重算續期保費.(註:修改續期繳費方式時,須同時重算續期保費.避免如綜合查詢等資訊異常. )
	 * </p>
	 * <p>Created By : Simon.Huang</p>
	 * <p>Create Time : 2018年2月20日</p>
	 * @param id
	 * @param type
	 * @return
	 */
	public void recordBankAuthFailUwChangeMsg(Long policyId) {
		
		//保單是否在核保作業中
		boolean isUwTask = proposalProcessService.isUwTask(policyId);

		if (isUwTask) {
			Long underwriteId = uwPolicyService.findLastUnderwriteIdbyPolicyId(policyId);
	
			//確認同一核保作業，是否有相同的核保中異動資料(且未確認)
			UwChangeMsgVO changeMsgVO = uwChangeMsgDS.findByUnderwriteId(underwriteId,
							CodeCst.UW_CHANGE_CODE__BANK_AUTH_FAIL, policyId);

			if(changeMsgVO == null){
				String policyCode = policyDS.getPolicyCodeByPolicyId(policyId);
				
				changeMsgVO = new UwChangeMsgVO();
				changeMsgVO.setPolicyId(policyId);
				changeMsgVO.setUnderwriteId(underwriteId);
				changeMsgVO.setChangeCode(CodeCst.UW_CHANGE_CODE__BANK_AUTH_FAIL);
				changeMsgVO.setChangeDate(AppContext.getCurrentUserLocalTime());
				changeMsgVO.setBizId(policyId);
				changeMsgVO.setBizCode(policyCode);
				uwChangeMsgDS.save(changeMsgVO);
			}
		}
	}

	/**
	 * <p>Description : 
	 * PCR-383857-信用卡驗證失敗修改保單續期繳費方式規則未含新契約件
	 * 一.保單待核保、核保中:驗證失敗
	 *     (1)同時寫核保中異動資訊
	 * 二.保單待承保、有條件接受:驗證失敗
	 *     (1)首期不能重算保費,僅重算續期保費.(註:修改續期繳費方式時,須同時重算續期保費.避免如綜合查詢等資訊異常. )
	 * </p>
	 * <p>Created By : Kathy.Yeh</p>
	 * <p>Create Time : 2020年11月27日</p>
	 * @param id
	 * @param type
	 * @return
	 */
	public void recordCreditCardFailUwChangeMsg(Long policyId) {
		
		//保單是否在核保作業中
		boolean isUwTask = proposalProcessService.isUwTask(policyId);

		if (isUwTask) {
			Long underwriteId = uwPolicyService.findLastUnderwriteIdbyPolicyId(policyId);
	
			//確認同一核保作業，是否有相同的核保中異動資料(且未確認)
			UwChangeMsgVO changeMsgVO = uwChangeMsgDS.findByUnderwriteId(underwriteId,
							CodeCst.UW_CHANGE_CODE__CREDIT_CARD_VALIDATE_ERROR, policyId);

			if(changeMsgVO == null){
				String policyCode = policyDS.getPolicyCodeByPolicyId(policyId);
				
				changeMsgVO = new UwChangeMsgVO();
				changeMsgVO.setPolicyId(policyId);
				changeMsgVO.setUnderwriteId(underwriteId);
				changeMsgVO.setChangeCode(CodeCst.UW_CHANGE_CODE__CREDIT_CARD_VALIDATE_ERROR);
				changeMsgVO.setChangeDate(AppContext.getCurrentUserLocalTime());
				changeMsgVO.setBizId(policyId);
				changeMsgVO.setBizCode(policyCode);
				uwChangeMsgDS.save(changeMsgVO);
			}
		}
	}
	
	/**
	 * process change msg by type
	 *
	 * @param id
	 * @param changeCode
	 * @param bizId
	 * @param bizCode
	 * @throws GenericException
	 */
	public void prcUwChangeMsgByType(String id, String changeCode, Long bizId, String bizCode)
			throws GenericException {
		
		//要發通知的保單
		List<Long> policyList = new ArrayList<Long>();
		
		if(CodeCst.UW_CHANGE_CODE__POS_POLICY_CHANGE_LOAN.equals(changeCode) ) {
			//已由保全接口查詢需通報保單,biz_code為待通知的保單policy_code,
			//biz_id為來源change_id
			Long policyId = policyService.getPolicyIdByPolicyNumber(bizCode);
			policyList.add(policyId);
			CSApplicationInfo csVO = csService.load(bizId);
			if ( csVO != null) {
				logger.debug("核保中異動  保全=" + bizId + "/" + bizCode);
				bizCode = policyService.load(csVO.getPolicyId()).getPolicyNumber();
			} 
		} else {
			//查詢與異動ID,相同的保單
			policyList = uwChangeMsgDS.getPolicyIdListByChangeCode(id, changeCode);	
		}
		
		/**RTC 113681 保單號碼有誤 => 傳入保全受理編號,需改為保單號碼*/
		if (CodeCst.UW_CHANGE_CODE__POS.equals(changeCode) 
				|| CodeCst.UW_CHANGE_CODE__POS_POLICY_CHANGE_FINANCE.equals(changeCode)						
				|| CodeCst.UW_CHANGE_CODE__POS_INCOME.equals(changeCode) 
				|| CodeCst.UW_CHANGE_CODE__RISK_TYPE_CHK.equals(changeCode)
				|| CodeCst.UW_CHANGE_CODE__ESTATE_LOAN.equals(changeCode)) {
			
			if(bizId != null) {
				
				boolean isFOA = false;
				//For FOA 保全受理
				if(bizCode != null) {
					Long policyId = policyService.getPolicyIdByPolicyNumber(bizCode);	
					if(policyId != null) {
						isFOA =true;
						logger.debug("核保中異動  FOA 保全受理=" + bizCode);
					}
				}
				
				if(isFOA == false) {
					CSApplicationInfo csVO = csService.load(bizId);
					if ( csVO != null) {
						logger.debug("核保中異動  保全=" + bizId + "/" + bizCode);
						bizCode = policyService.load(csVO.getPolicyId()).getPolicyNumber();
					} 
				}
			}

			logger.debug("核保中異動 certiCode=" + id  + ",policyCode=" + bizCode);
		}
		for (Long policyId : policyList) {
			//查詢與異動ID,相同的保單,是否在核保作業中
			boolean isUwTask = proposalProcessService.isUwTask(policyId);

			logger.debug("核保中異動 certiCode=" + id 
					+ ",policyId=" + policyId
					+ ",isUwTask=" + isUwTask);
			
			if (isUwTask) {
				Long underwriteId = uwPolicyService.findLastUnderwriteIdbyPolicyId(policyId);
				//確認同一核保作業，是否有相同的核保中異動資料(且未確認)
				
				UwChangeMsgVO changeMsgVO = uwChangeMsgDS.findByUnderwriteId(underwriteId,
						changeCode, bizId);

				if(changeMsgVO == null){
					changeMsgVO = new UwChangeMsgVO();
					changeMsgVO.setPolicyId(policyId);
					changeMsgVO.setUnderwriteId(underwriteId);
					changeMsgVO.setChangeCode(changeCode);
					changeMsgVO.setChangeDate(AppContext.getCurrentUserLocalTime());
					changeMsgVO.setBizId(bizId);
					changeMsgVO.setBizCode(bizCode);
					uwChangeMsgDS.save(changeMsgVO);
					
					logger.debug("核保中異動 certiCode=" + id + "add changeMsgVO"
							+ ",underwriteId="+underwriteId
							+ ",changeCode="+changeCode);
					
				}
			}
		}
	}

	@Override
	public void prcUwChangeRiskMsg(Long policyId, String certiCode, Date logDate) throws GenericException {
		List<Long> policyList = uwChangeMsgDS.getPolicyIdListByChangeCode(certiCode, "6");
		String policyCode = "";
		if ( policyId != null) {
			PolicyInfo policyVO = policyService.load(policyId);
		    policyCode = policyVO.getPolicyNumber(); 
		}
		for (Long pId : policyList) {
			if ( policyId == null || (!pId.equals(policyId))) {
				if (proposalProcessService.isUwTask(pId)) {
					UwChangeMsgVO changeMsgVO = new UwChangeMsgVO();
					changeMsgVO.setPolicyId(pId);
					changeMsgVO.setUnderwriteId(uwPolicyService.findLastUnderwriteIdbyPolicyId(pId));
					changeMsgVO.setChangeCode("6");
					changeMsgVO.setChangeDate(logDate);
					changeMsgVO.setBizId(policyId);
					changeMsgVO.setBizCode(policyCode);
					uwChangeMsgDS.save(changeMsgVO);
				}
			}
		}
	}

	@Override
	public void prcUwChangeMsgByEstateLoan(
					String uwChangeCode, Long policyId, String policyCode,
					String unbRoleType, String name, NbEstateLoan nbEstateLoan) {
		Long underwriteId = uwPolicyService.findLastUnderwriteIdbyPolicyId(policyId);
		//確認同一核保作業，是否有相同的核保中異動資料(且未確認)
		Long bizId = nbEstateLoan.getListId();
		UwChangeMsgVO changeMsgVO = uwChangeMsgDS.findByUnderwriteId(underwriteId,
						uwChangeCode, bizId);

		if (changeMsgVO == null) {
			changeMsgVO = new UwChangeMsgVO();
			changeMsgVO.setPolicyId(policyId);
			changeMsgVO.setUnderwriteId(underwriteId);
			changeMsgVO.setChangeCode(uwChangeCode);
			changeMsgVO.setChangeDate(AppContext.getCurrentUserLocalTime());
			changeMsgVO.setBizId(bizId);
			//保單角色/姓名/貸款角色/貸款身份證字號/貸款編號
			List<String> bizCode = new ArrayList<String>();
			bizCode.add(policyCode);
			bizCode.add(UnbRoleType.getRoleNameByRoleType(unbRoleType));
			bizCode.add(name);
			bizCode.add(LoanRole.getRoleNameByRoleType(nbEstateLoan.getLoanRole()));
			bizCode.add(nbEstateLoan.getCertiCode());
			bizCode.add(nbEstateLoan.getLoanAccount());
			changeMsgVO.setBizCode(StringUtils.join(bizCode.iterator(), "/"));
			uwChangeMsgDS.save(changeMsgVO);
		}
	}

	/**
	 * <p>Description : PCR-299807 儲存eDDA核保中異動&待承保接口資料。
	 * 記錄資料如下：
	 * <ul>
	 *     <li>changeCode = 9</li>
	 *     <li>bizId = sealRecordEddaVO.getTransSeqNo()</li>
	 *     <li>bizCode = sealRecordEddaVO.getCertiCode() + "/" + sealRecordEddaVO.getBankCode() + "/" + sealRecordEddaVO.getBankAccount()</li>
	 * </ul>
	 * </p>
	 * <p>Created By : Yvon Sun</p>
	 * <p>Create Time : Apr 3, 2019</p>
	 * @param policyId 保單ID
	 * @param sealRecordEddaVO PA提供之eDDA資料VO物件實例
	 */
	public void recordEddaAuthSucceedUwChangeMsg(Long policyId, SealRecordEddaVO sealRecordEddaVO) {
		Long underwriteId = uwPolicyService.findLastUnderwriteIdbyPolicyId(policyId);
		if (underwriteId != null) {
			UwChangeMsgVO changeMsgVO = new UwChangeMsgVO();
			changeMsgVO.setPolicyId(policyId);
			changeMsgVO.setUnderwriteId(underwriteId);
			changeMsgVO.setChangeCode(UwChangeCode.UW_CHANGE_CODE__EDDA);
			changeMsgVO.setChangeDate(AppContext.getCurrentUserLocalTime());
			changeMsgVO.setBizId(Long.valueOf(sealRecordEddaVO.getTransSeqNo()));
			changeMsgVO.setBizCode(sealRecordEddaVO.getCertiCode() + "/" + sealRecordEddaVO.getBankCode() + "/" + sealRecordEddaVO.getBankAccount());

			uwChangeMsgDS.save(changeMsgVO);
		}
	}
	
	/**
	 * <p>Description : 
	 * PCR-338436-配合招攬核保理賠辦法更修條文之系統需求
	 * 一.要保書狀態 = 待承保，不可生效，並發email通知核保人員
	 * </p>
	 * <p>Created By : Liam.Liu</p>
	 * <p>Create Time : 2019年10月31日</p>
	 * @param policyPersonVO
	 * @param policyChangeInfo
	 * @param applyCode
	 * @return
	 */
	public void doPolicyChangeProposalAccepted(PolicyPersonVO policyPersonVO, PolicyChangeInfo policyChangeInfo,
			String posPolicyCode) {

		// 2.發信給核保員
		Long nbUwPolicyId = (policyPersonVO.getPolicyId() != null ? policyPersonVO.getPolicyId() : Long.valueOf(0L)); // 新契約case's policyId
		String policyRoleType = UnbRoleType.getRoleNameByRoleType(policyPersonVO.getRoleType());
		String name = StringUtils.defaultIfEmpty(policyPersonVO.getName(), "");
		String applyDate = DateUtils.date2String(policyChangeInfo.getApplyTime(), "yyyy/MM/dd");
		
		String serviceName = ValidatorUtils.getDisplayStrByServiceId(policyChangeInfo.getServiceId());
		ProposalRuleResultVO ruleResultVO = genPolicySuspendInforceRuleResult(policyPersonVO, policyChangeInfo, posPolicyCode, nbUwPolicyId);
		
		if(ruleResultVO != null) {
			nbEventCI.sendUwNotifyForPosChange(nbUwPolicyId, policyRoleType, name, applyDate, serviceName, posPolicyCode);
		}
	}

	/**
	 * <p>Description :PCR-345349_實支實付醫療型保險復本理賠控款措施Phase2</p>
	 * <p>Created By  : Colin Hsu </p>
	 * <p>Create Time : 2019年12月6日 </p>
	 * @param policyId
	 * @param changeCode
	 * @param bizId
	 * @param bizCode
	 */
	public void prcUwChangeMsgByType4Pos(Long policyId, String changeCode, Long bizId, String bizCode){
		ApplicationLogger.addLoggerData("policyId = " + policyId + ", changeCode = " + changeCode
				+ ", bizId = " + bizId + ", bizCode = " + bizCode);
		Long underwriteId = uwPolicyService.findLastUnderwriteIdbyPolicyId(policyId);
		//UwChangeMsgVO changeMsgVO = uwChangeMsgDS.findByUnderwriteId(underwriteId, changeCode, bizId);
		ApplicationLogger.addLoggerData("underwriteId = " + underwriteId);
		
		if (underwriteId != null) {
			UwChangeMsgVO changeMsgVO = new UwChangeMsgVO();
			changeMsgVO.setPolicyId(policyId);
			changeMsgVO.setUnderwriteId(underwriteId);
			changeMsgVO.setChangeCode(changeCode);
			changeMsgVO.setChangeDate(AppContext.getCurrentUserLocalTime());
			changeMsgVO.setBizId(bizId);
			changeMsgVO.setBizCode(bizCode);
			uwChangeMsgDS.save(changeMsgVO);
			ApplicationLogger.addLoggerData("uwChangeMsgDS.save - success");
		}
	}
	
	/**
	 * 產生紀錄至暫存檔的核保照會訊息內容，以逗號分隔
	 */
	public String getUwChangeMsg(String changeCode, String bizCode) {
		if (StringUtils.isBlank(changeCode) || StringUtils.isBlank(bizCode)) {
			return null;
		}
		StringBuffer sb = new StringBuffer();
		sb.append(changeCode);
		sb.append(",");
		sb.append(bizCode);
		return sb.toString();
	}

	/**
	 * <p>Description : 產生一個核保訊息卡待承保保單不可生效</p>
	 * <p>Created By : Simon.Huang</p>
	 * <p>Create Time : 2020年1月16日</p>
	 * @param policyPersonVO 本張保單關係人
	 * @param policyChangeInfo 保全變更
	 * @param csPolicyCode 保全變更policyCode
	 * @param policyId 本張保單policyId
	 * @return
	 */
	
	public ProposalRuleResultVO genPolicySuspendInforceRuleResult(PolicyPersonVO policyPersonVO,
			PolicyChangeInfo policyChangeInfo, String csPolicyCode, Long policyId) {
		return genPolicySuspendInforceRuleResult(policyPersonVO.getName(), policyChangeInfo.getApplyTime(),
				policyPersonVO.getRoleType(), policyChangeInfo.getServiceId(), policyChangeInfo.getPolicyId(),
				csPolicyCode, policyId);
	}

	/**
	 * <p>Description : 產生一個核保訊息卡待承保保單不可生效</p>
	 * <p>Created By : Simon.Huang</p>
	 * <p>Create Time : 2020年1月16日</p>
	 * @param name 本張保單關係人姓名
	 * @param applyTime  保全受理日期
	 * @param unbRoleType 本張保單關係人
	 * @param serviceId  保全變更serviceId
	 * @param csPolicyId 保全變更policyId
	 * @param csPolicyCode 保全變更policyCode
	 * @param policyId 本張保單policyId
	 * @return
	 */
	public ProposalRuleResultVO genPolicySuspendInforceRuleResult(String name, Date applyTime, String unbRoleType,
			Integer serviceId,Long csPolicyId, String csPolicyCode, Long policyId) {
		
		Map<String, String> params = ValidatorUtils.getPolicySuspendInforceParams(name,
				applyTime, unbRoleType, serviceId,
				csPolicyCode);
		
		ProposalRuleResultVO ruleResultVO = proposalRuleResultService.genLoanRuleResult(policyId, params,
						ProposalRuleMsg.PROPOSAL_RULE_POLICY_SUSPEND_INFORCE_BY_POS,
						csPolicyId,
						CodeCst.PROPOSAL_RULE_SOURCE_TYPE__POS_CHANGE);
		
		if(ruleResultVO != null) {
			proposalProcessService.publishBatchTriggerEvent(policyId, EventConstant.POLICY__SUSPEND_INFORCE);
		}
		
		return ruleResultVO;
	}

	/**
	 * <p>Description : PCR-412145 依法令公司裁罰案已符合消極指標須限縮核保標準(phase 2)<br/>
	 * PROPOSAL_STATUS＝31、32（核保中），實際繳交保費之利害關係人不等於要保人、各被保險人及首/續授權人
	 * <ul><li>若同一保單同一人就不用再出，不同人要再顯示</li>
	 * <li>記錄資料如下：<ul>
	 *     <li>changeCode = 實際繳交保費人</li>
	 *     <li>changeDate = 保費入帳日期/時間(系統日期)</li>
	 *     <li>bizCode = <實際繳交保費人姓名>/<實際繳交保費人身分證字號></li>
	 * </ul></li>
	 * </p>
	 * <p>Created By : Charlotte Wang</p>
	 * <p>Create Time : Oct 12, 2020</p>
	 * @param policyId 保單ID
	 * @param name
	 * @param certiCode
	 */
	public void recordPaTraderUwChangeMsg(Long policyId, String name, String certiCode) {
		// 若同一保單同一人就不用再出，不同人要再顯示
		boolean isExists = false;
		UwChangeMsgVO uwChangeMsgVO = uwChangeMsgDS.findPaTraderUwChangeMsg(policyId, certiCode);
		if (null != uwChangeMsgVO)
			isExists = true;

		if (false == isExists) {
			Long underwriteId = uwPolicyService.findLastUnderwriteIdbyPolicyId(policyId);

			UwChangeMsgVO changeMsgVO = new UwChangeMsgVO();
			changeMsgVO.setPolicyId(policyId);
			changeMsgVO.setUnderwriteId(underwriteId);
			changeMsgVO.setChangeCode(UwChangeCode.UW_CHANGE_CODE__PA_TRADER);
			changeMsgVO.setChangeDate(AppContext.getCurrentUserLocalTime());
			changeMsgVO.setBizId(policyId);
			changeMsgVO.setBizCode(name + "/" + certiCode);

			uwChangeMsgDS.save(changeMsgVO);
		} // if (false == isExists)
	}

	/**
	 * <p>Description : PCR-414348 OIU保件實際繳款人ID檢核需求<br/>
	 * PROPOSAL_STATUS＝31、32（待核保、核保中），實際繳納人保費之人如符合以下任一條件須出警告訊息：
	 * <ol><li>保費繳款人關係=要保人或被保險人，但繳款人ID卻非要保人或被保險人ID 或</li>
	 * <li>保費繳款人姓名=要保人或被保險人姓名，但繳款人ID卻非要保人或被保險人ID 或</li>
	 * <li>保費負責人姓名=代表人姓名，但繳款人ID非要保人ID</li></ol>
	 * <ul>記錄資料如下：
	 *     <li>changeCode = OIU投保身分資格確認</li>
	 *     <li>changeDate = 保費入帳日期/時間(系統日期)</li>
	 *     <li>bizCode = {保單角色}/{姓名}/{繳款人ID}</li>
	 * </ul>
	 * </p>
	 * <p>Created By : Charlotte Wang</p>
	 * <p>Create Time : Mar 12, 2021</p>
	 * @param policyId 保單ID
	 * @param policyRoleName 保單角色
	 * @param name
	 * @param certiCode
	 */
	public void recordOiuPayerIDUwChangeMsg(Long policyId, String policyRoleName, String name, String certiCode) {
		Long underwriteId = uwPolicyService.findLastUnderwriteIdbyPolicyId(policyId);

		UwChangeMsgVO changeMsgVO = new UwChangeMsgVO();
		changeMsgVO.setPolicyId(policyId);
		changeMsgVO.setUnderwriteId(underwriteId);
		changeMsgVO.setChangeCode(UwChangeCode.UW_CHANGE_CODE__OIU_PAYER_VALIDATE);
		changeMsgVO.setChangeDate(AppContext.getCurrentUserLocalTime());
		changeMsgVO.setBizId(policyId);
		changeMsgVO.setBizCode(policyRoleName + "/" + name + "/" + certiCode);

		uwChangeMsgDS.save(changeMsgVO);
	}

    /**
     * <p>Description : PCR-344444 執行新契約保費入帳(首期)時，下載實際繳交保險費之人(繳款人證件號碼)公會通報資料及檢核<br/>
     * 1. 判斷繳款人證件號碼是否存在公會下載資料，若不存在，下載實際繳交保險費之人的公會通報資料<br/>
     * 2. 執行Policy.LiaRocDownload.Warning2規則校驗，若存在同業解約條件 <br/>
     * 2.1  PROPOSAL_STATUS＝31(待核保)、32（核保中）產生核保中異動訊息，寫一筆記錄至T_UW_CHANGE_MSG<br/>
	 * <ul>記錄資料如下：
	 *     <li>changeCode = 19-保費繳款人同業保單有近三個月解約資訊</li>
	 *     <li>changeDate = 保費入帳日期/時間(系統日期)</li>
	 *     <li>bizCode = {實際繳交保費人姓名}/{實際繳交保費人身分證字號>}</li>
	 * </ul>
     * 3. 公會下載失敗且PROPOSAL_STATUS＝30(待核保)產生核保中異動訊息，寫一筆記錄至T_UW_CHANGE_MSG<br/>
	 * <ul>記錄資料如下：
	 *     <li>changeCode = 20-保費繳款人尚未執行公會下載通報(同業保單解約資訊)</li>
	 *     <li>changeDate = 保費入帳日期/時間(系統日期)</li>
	 *     <li>bizCode = {實際繳交保費人姓名}/{實際繳交保費人身分證字號>}</li>
	 * </ul>
	 * </p>
	 * <p>Created By : Kathy.Yeh</p>
	 * <p>Create Time : April 28, 2020</p>
	 * @param policyId 保單ID
	 * @param name
	 * @param certiCode
	 * @param hasLiaRocTerminate 
	 */
	public void recordPaTraderLiaRocDownload(Long policyId, String name, String certiCode, boolean hasLiaRocTerminate) {
		//保單是否在核保作業中
		boolean isUwTask = proposalProcessService.isUwTask(policyId);

		if (isUwTask) {
			Long underwriteId = uwPolicyService.findLastUnderwriteIdbyPolicyId(policyId);
			
			if  (hasLiaRocTerminate) {
				//實際繳交保險費之人公會下載成功出核保中異動訊息
				UwChangeMsgVO changeMsgVO = uwChangeMsgDS.findByUnderwriteId(underwriteId,
								CodeCst.UW_CHANGE_CODE__PA_TRADER_LIAROC_TERMINATE, policyId);

				if(changeMsgVO == null){
					changeMsgVO = new UwChangeMsgVO();
					changeMsgVO.setPolicyId(policyId);
					changeMsgVO.setUnderwriteId(underwriteId);
					changeMsgVO.setChangeCode(CodeCst.UW_CHANGE_CODE__PA_TRADER_LIAROC_TERMINATE);
					changeMsgVO.setChangeDate(AppContext.getCurrentUserLocalTime());
					changeMsgVO.setBizId(policyId);
					changeMsgVO.setBizCode(name + "/" + certiCode);
					uwChangeMsgDS.save(changeMsgVO);
				}						
			}else{
				//實際繳交保險費之人公會下載失敗出核保中異動訊息
				UwChangeMsgVO changeMsgVO = uwChangeMsgDS.findByUnderwriteId(underwriteId,
								CodeCst.UW_CHANGE_CODE__PA_TRADER_LIAROC_DOWNLOAD_FAIL, policyId);

				if(changeMsgVO == null){
					changeMsgVO = new UwChangeMsgVO();
					changeMsgVO.setPolicyId(policyId);
					changeMsgVO.setUnderwriteId(underwriteId);
					changeMsgVO.setChangeCode(CodeCst.UW_CHANGE_CODE__PA_TRADER_LIAROC_DOWNLOAD_FAIL);
					changeMsgVO.setChangeDate(AppContext.getCurrentUserLocalTime());
					changeMsgVO.setBizId(policyId);
					changeMsgVO.setBizCode(name + "/" + certiCode);
					uwChangeMsgDS.save(changeMsgVO);
				}				
			}
		}
	}
	/**
	 * <p>Description : 474367_因應招攬及核保理賠辦法及其自律規範修正案<br/>
	 * 1.	實際繳納保費之人不等於本張保單各角色之人 且
	 * 2.	實際繳納保費之人投保年齡65歲(含)以上者 且
	 * 3.保單符合高齡投保件(核保高齡關懷提問)業務規則
	 * </p>
	 * <p>Created By : Kathy.Yeh</p>
	 * <p>Create Time : 2022/02/17</p>
	 * @param policyId 保單ID
	 * @param name 實際繳納保費之人姓名
	 * @param certiCode 實際繳納保費之人ID
	 * @param birthDate 實際繳納保費之人生日
	 * @param ruleType 規則{1..3}
	 * 1:實際繳納保費之人投保年齡65歲(含)以上者
	 * 2:繳款人法定代理人投保年齡大於等於65歲
	 * 3:實際繳納保費人<65歲，實際繳納保費人(以ID及姓名檢核)同本件保單受益人但出生日期不同
	 * @param relaction 實際繳納保費之人關係
	 */
	public void recordPaTraderElderCareUwChangeMsg(Long policyId, String name, String certiCode, Date birthDate, String ruleType, String relaction) {
		//保單是否在核保作業中
		boolean isUwTask = proposalProcessService.isUwTask(policyId);

		if (isUwTask) {
			Long underwriteId = uwPolicyService.findLastUnderwriteIdbyPolicyId(policyId);

			UwChangeMsgVO changeMsgVO = new UwChangeMsgVO();
			changeMsgVO.setPolicyId(policyId);
			changeMsgVO.setUnderwriteId(underwriteId);
			changeMsgVO.setChangeCode(UwChangeCode.UW_CHANGE_CODE__ELDER_CARE);
			changeMsgVO.setChangeDate(AppContext.getCurrentUserLocalTime());
			changeMsgVO.setBizId(policyId);
			switch (ruleType) {
			case "1":
				changeMsgVO.setBizCode("實際繳納保費之人：" + name + "/" + "身分證字號：" + certiCode + "/" + "生日："
						+ TGLDateUtil.format(birthDate) + "，65歲以上客戶須做高齡投保之相關審核作業");
				break;
			case "2":
				changeMsgVO.setBizCode("實際繳納保費人之法定代理人投保年齡65歲(含)以上：" + name + "/" + "身分證字號：" + certiCode + "/" + "生日："
						+ TGLDateUtil.format(birthDate) + "/繳款人與要/被保險人關係：" + relaction);
				break;
			case "3":
				changeMsgVO.setBizCode("實際繳納保費之人：" + name + "/" + "身分證字號：" + certiCode + "/" + "生日："
						+ TGLDateUtil.format(birthDate) + "，出生日期與受益人不同");
				break;
			}
			uwChangeMsgDS.save(changeMsgVO);
		}
	}

	
	/**
	 * <p>Description : PCR-PCR 448703 遠距投保 3.0<br/>
	 * PROPOSAL_STATUS＝31、32（待核保、核保中），產生核保中異動訊息：
	 * <ul>記錄資料如下：
	 *     <li>changeCode = 22-錄音錄影覆核結果</li>
	 *     <li>changeDate = 錄音錄影抽檢覆核結果寫入eBao核心的作業日期/時間</li>
	 *     <li>bizCode = {錄音錄影覆核結果}</li>
	 * </ul>
	 * </p>
	 * <p>Created By : Charlotte Wang</p>
	 * <p>Create Time : Jul 20, 2021</p>
	 * @param policyId 保單ID
	 * @param reviewResult 錄音錄影覆核結果
	 */
	public void recordRemoteReviewResultUwChangeMsg(Long policyId, String reviewResult) {
		Long underwriteId = uwPolicyService.findLastUnderwriteIdbyPolicyId(policyId);

		UwChangeMsgVO changeMsgVO = new UwChangeMsgVO();
		changeMsgVO.setPolicyId(policyId);
		changeMsgVO.setUnderwriteId(underwriteId);
		changeMsgVO.setChangeCode(UwChangeCode.UW_CHANGE_CODE__REMOTE_REVIEW_RESULT);
		changeMsgVO.setChangeDate(AppContext.getCurrentUserLocalTime());
		changeMsgVO.setBizId(policyId);
		changeMsgVO.setBizCode(reviewResult);

		uwChangeMsgDS.save(changeMsgVO);
	}
	
	/**
	 * <p>Description : PCR-477771  高齡P2 <br/>
	 * PROPOSAL_STATUS＝31、32（待核保、核保中）同一筆電訪重複回覆結果，產生核保中異動訊息
	 * <ul>記錄資料如下：
	 *     <li>changeCode = 25-電訪結果異動</li>
	 *     <li>changeDate = ECP回覆系統日</li>
	 *     <li>bizCode = 電訪任務編號%s電訪結果資訊異動 (%s帶入電訪任務編號)</li>
	 * </ul>
	 * </p>
	 * <p>Created By : Curious Wang</p>
	 * <p>Create Time : July 29, 2022</p>
	 * @param policyId 保單ID
	 * @param calloutTrans 電訪任務
	 */
	public void recordCalloutUwChangeMsg(Long policyId, CalloutTransVO calloutTransVO) {
		Long underwriteId = uwPolicyService.findLastUnderwriteIdbyPolicyId(policyId);
		//同一筆電訪重複回覆結果，產生核保中異動訊息
		UwChangeMsgVO changeMsgVO = uwChangeMsgDS.findByUnderwriteId(underwriteId,
						CodeCst.UW_CHANGE_CODE__CALLOUT_RESULT, Long.valueOf(calloutTransVO.getListId()));
		
		if( null == changeMsgVO ) {
			changeMsgVO = new UwChangeMsgVO();
			changeMsgVO.setPolicyId(policyId);
			changeMsgVO.setUnderwriteId(underwriteId);
			changeMsgVO.setChangeCode(UwChangeCode.UW_CHANGE_CODE__CALLOUT_RESULT);
			changeMsgVO.setChangeDate(AppContext.getCurrentUserLocalTime());
			changeMsgVO.setBizId(Long.valueOf(calloutTransVO.getListId()));
			changeMsgVO.setBizCode(String.format(StringResourceUtil.getStringData("MSG_1267117"),calloutTransVO.getCalloutNum()));
			uwChangeMsgDS.save(changeMsgVO);	
		}
		
	}

	@Override
	public void recordIlpRiskLevelUwChangeMsg(Long posPolicyId, Long changeId) {
		DBean db = null;
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer sql = new StringBuffer();
		
		if (NBUtils.isUATEnv()) {
			logger.error("[UNB DEBUG]recordIlpRiskLevelUwChangeMsg start");
		}
		try {
			if (1 == 0 || posPolicyId == null || changeId == null) {
				throw new NullPointerException();
			}
			sql.append(this.CHANGE_CODE_26_SQL);
			
			if (NBUtils.isUATEnv()) {
				logger.error("[UNB DEBUG]{posPolicyId=" + posPolicyId + ",changeId=" + posPolicyId + "}\n"
						+ CHANGE_CODE_26_SQL);
			}
			db = new DBean();
			db.connect();
			con = db.getConnection();
			ps = con.prepareStatement(sql.toString());
			int index = 1;
		
			ps.setLong(index++, changeId);
			ps.setLong(index++, posPolicyId);
			ps.setLong(index++, posPolicyId);
			ps.setLong(index++, changeId);
			rs = ps.executeQuery();
			
			List<UwChangeMsgVO> uwChangeMsgList = new ArrayList<UwChangeMsgVO>();
			while (rs.next()) {
				
				Long policyId = rs.getLong("policy_id");
				Date applyTime = rs.getDate("apply_time");
				String posPolicyCode = rs.getString("pos_policy_code");
				
				Long underwriteId = uwPolicyService.findLastUnderwriteIdbyPolicyId(policyId);
				
				UwChangeMsgVO uwChangeMsgVO = new UwChangeMsgVO();
				uwChangeMsgVO.setPolicyId(policyId);
				uwChangeMsgVO.setUnderwriteId(underwriteId);
				uwChangeMsgVO.setChangeCode(UwChangeCode.UW_CHANGE_CODE__RISK_FORM_CHANGE);
				uwChangeMsgVO.setChangeDate(applyTime);
				uwChangeMsgVO.setBizId(changeId);
				uwChangeMsgVO.setBizCode(posPolicyCode);
				
				uwChangeMsgList.add(uwChangeMsgVO);

				if (NBUtils.isUATEnv()) {
					logger.error("[UNB DEBUG]notify uwChangeMsg NB_POLICY_ID=" + policyId);
				}
			}
			
			for(UwChangeMsgVO uwChangeMsgVO : uwChangeMsgList) {
				uwChangeMsgDS.save(uwChangeMsgVO);
			}
		} catch (SQLException e) {
			throw new RTException(e);
		} catch (ClassNotFoundException e) {
			throw new RTException(e);
		} finally {
			DBean.closeAll(rs, ps, db);
		}

		if (NBUtils.isUATEnv()) {
			logger.error("[UNB DEBUG]recordIlpRiskLevelUwChangeMsg end");
		}
	}

	@Override
	public void recordElderAudioReplyUwChangeMsg(Long policyId) {
		//保單是否在核保作業中
		boolean isUwTask = proposalProcessService.isUwTask(policyId);

		if (isUwTask) {
			Long underwriteId = uwPolicyService.findLastUnderwriteIdbyPolicyId(policyId);
			
			//有更新已回覆之高齡銷售錄音覆審結果
			UwChangeMsgVO changeMsgVO = uwChangeMsgDS.findByUnderwriteId(underwriteId,
					UwChangeCode.UW_CHANGE_CODE__ELDER_AUDIO, policyId);

			if(changeMsgVO == null){
				changeMsgVO = new UwChangeMsgVO();
				changeMsgVO.setPolicyId(policyId);
				changeMsgVO.setUnderwriteId(underwriteId);
				changeMsgVO.setChangeCode(UwChangeCode.UW_CHANGE_CODE__ELDER_AUDIO);
				changeMsgVO.setChangeDate(AppContext.getCurrentUserLocalTime());
				changeMsgVO.setBizId(policyId);
				changeMsgVO.setBizCode(StringResourceUtil.getStringData("MSG_1267097"));// 核保中異動訊息:「有更新已回覆之高齡銷售錄音覆審結果」
				uwChangeMsgDS.save(changeMsgVO);						
			}
		}
	}

    @Override
    public void recordEcOffLineUnInsuredChangeMsg(Long policyId) {
        Optional.ofNullable(uwPolicyService.findLastUnderwriteIdbyPolicyId(policyId)).ifPresent(underwriteId -> {
            Optional.ofNullable(uwChangeMsgDS.findByUnderwriteId(underwriteId, UwChangeCode.UW_CHANGE_CODE__UNINSURED, policyId))
                    .orElseGet(() -> {
                        UwChangeMsgVO changeMsgVO = new UwChangeMsgVO();
                        changeMsgVO.setPolicyId(policyId);
                        changeMsgVO.setUnderwriteId(underwriteId);
                        changeMsgVO.setChangeCode(UwChangeCode.UW_CHANGE_CODE__UNINSURED);
                        changeMsgVO.setChangeDate(AppContext.getCurrentUserLocalTime());
                        changeMsgVO.setBizId(policyId);
                        // MSG_142 - 客戶申請取消投保，請取消本件(核保決定請選擇要保撤回)。
                        changeMsgVO.setBizCode(StringResource.getStringData("MSG_142", LANGUAGE__CHINESE_TRAD));
                        uwChangeMsgDS.save(changeMsgVO);
                        return null;
                    });
        });
    }

    @Override
    public boolean recordBatchSignAgentCalloutChangeMsg(Long policyId, Date createDate) {
        AtomicBoolean tof = new AtomicBoolean(false);

        // 保單是否在核保作業中
        boolean isUwTask = proposalProcessService.isUwTask(policyId);
        if (isUwTask) {
            ofNullable(uwPolicyService.findLastUnderwriteIdbyPolicyId(policyId)).ifPresent(underwriteId -> {
                ofNullable(uwChangeMsgDS.findByUnderwriteId(underwriteId, UW_CHANGE_CODE__AGENTCALLOUT, policyId))
                        .orElseGet(() -> {
                            UwChangeMsgVO changeMsgVO = new UwChangeMsgVO();
                            changeMsgVO.setPolicyId(policyId);
                            changeMsgVO.setUnderwriteId(underwriteId);
                            changeMsgVO.setChangeCode(UW_CHANGE_CODE__AGENTCALLOUT);
                            changeMsgVO.setChangeDate(createDate == null ? getCurrentUserLocalTime() : createDate);
                            changeMsgVO.setBizId(policyId);
                            // MSG_156 - 已更新是否由保經代公司對高齡、保費來源為解約/貸款/保單借款辦理電訪等作業
                            changeMsgVO.setBizCode(getStringData("MSG_156", LANGUAGE__CHINESE_TRAD));
                            try {
                                uwChangeMsgDS.save(changeMsgVO);
                                tof.set(true);
                            } catch (Exception ex) {
                                logger.error("", ex);
                            }
                            return null;
                        });
            });
        }

        return tof.get();
    }

	@Override
	public void recordIlpRiskLevelUwChangeMsg(Long policyId, String evaluateDate, String riskLevelDesc) {
		//保單是否在核保作業中
		boolean isUwTask = proposalProcessService.isUwTask(policyId);
		if (isUwTask) {
			Long underwriteId = uwPolicyService.findLastUnderwriteIdbyPolicyId(policyId);
			UwChangeMsgVO changeMsgVO = uwChangeMsgDS.findByUnderwriteId(underwriteId,
					UwChangeCode.UW_CHANGE_CODE__RISK_RENEW, policyId);
			if(changeMsgVO == null){
				changeMsgVO = new UwChangeMsgVO();
				changeMsgVO.setPolicyId(policyId);
				changeMsgVO.setUnderwriteId(underwriteId);
				changeMsgVO.setChangeCode(UwChangeCode.UW_CHANGE_CODE__RISK_RENEW);
				changeMsgVO.setChangeDate(AppContext.getCurrentUserLocalTime());
				changeMsgVO.setBizId(policyId);
				// 核保中異動訊息:「要保人｛重新評估日期｝於線上重新辦理客戶風險屬性評估－｛重新評估之風險屬性｝」
				changeMsgVO.setBizCode(NBUtils.replaceWithParams(StringResourceUtil.getStringData("MSG_1267435"),Arrays.asList(evaluateDate,riskLevelDesc)));
				uwChangeMsgDS.save(changeMsgVO);
			}
		}
	}

}
