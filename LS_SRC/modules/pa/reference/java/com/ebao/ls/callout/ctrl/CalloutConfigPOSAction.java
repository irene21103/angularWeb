package com.ebao.ls.callout.ctrl;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.UserTransaction;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.ls.callout.bs.CalloutConfigService;
import com.ebao.ls.callout.vo.CalloutConfigVO;
import com.ebao.pub.framework.ExceptionFactory;
import com.ebao.pub.framework.GenericAction;
import com.ebao.pub.util.Trans;
import com.ebao.pub.util.TransUtils;

public class CalloutConfigPOSAction extends GenericAction {

  private static final String ACTION_SAVE = "save";

  private static final String FORWARD_RESULT = "success";

  private static final String CONFIG_TYPE_OF_4 = "4"; // 4 POS抽樣比例

  @Resource(name = CalloutConfigService.BEAN_DEFAULT)
  private CalloutConfigService calloutConfigService;

  @Override
  public ActionForward process(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

    List<Map<String, Object>> gridList = calloutConfigService.query(CONFIG_TYPE_OF_4,null,null);
    CalloutConfigForm calloutConfigForm = (CalloutConfigForm) form;

    String saction = calloutConfigForm.getActionType();

    if (ACTION_SAVE.equals(saction)) {

      UserTransaction transaction = null;
      try {
        transaction = Trans.getUserTransaction();
        transaction.begin();
        List<CalloutConfigVO> listVO = calloutConfigForm.getGridList();
        for (int i = 0; i < listVO.size(); i++) {

          CalloutConfigVO calloutConfigVO = listVO.get(i);
          calloutConfigService.saveOrUpdateCalloutConfigVO(calloutConfigVO);
        }
        transaction.commit();
      } catch (Exception e1) {
        TransUtils.rollback(transaction);
        throw ExceptionFactory.parse(e1);
      }

      return mapping.findForward(ACTION_SAVE);

    }
    else {

      for (int index = 0; index < gridList.size(); index++) {
        CalloutConfigVO calloutConfigVO = new CalloutConfigVO();
        Long listId = ((BigDecimal) gridList.get(index).get("LIST_ID"))
            .longValue();
        String fileName = (String) gridList.get(index).get("FILE_NAME");
        String fileDesc = (String) gridList.get(index).get("FILE_DESC");
        BigDecimal data = (BigDecimal) gridList.get(index).get("DATA");
        BigDecimal display = (BigDecimal) gridList.get(index).get(
            "DISPLAY_ORDER");
        calloutConfigVO.setListId(listId);
        calloutConfigVO.setFileName(fileName);
        calloutConfigVO.setFileDesc(fileDesc);
        calloutConfigVO.setData(data);
        calloutConfigVO.setDisplayOrder(display != null ? display.longValue()
            : 0);
        calloutConfigForm.getGridList().add(calloutConfigVO);
      }

      request.setAttribute("gridList", gridList);
      return mapping.findForward(FORWARD_RESULT);
    }

  }

}
