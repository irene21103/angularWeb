package com.ebao.ls.callout.ci.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;

import com.ebao.foundation.core.logging.Log;
import com.ebao.foundation.module.util.i18n.CodeTable;
import com.ebao.ls.callout.ajax.CalloutTargetAjaxService;
import com.ebao.ls.callout.ajax.PolicyInfoAjaxService;
import com.ebao.ls.callout.bs.CalloutOnlineService;
import com.ebao.ls.callout.bs.CalloutQuizService;
import com.ebao.ls.callout.bs.CalloutTransService;
import com.ebao.ls.callout.ci.CalloutServiceCI;
import com.ebao.ls.callout.data.CalloutExceptionItemDao;
import com.ebao.ls.callout.data.CalloutReasonDao;
import com.ebao.ls.callout.data.bo.CalloutExceptionItem;
import com.ebao.ls.callout.data.bo.CalloutReason;
import com.ebao.ls.callout.vo.CalloutExceptionItemVO;
import com.ebao.ls.callout.vo.CalloutOnlineVO;
import com.ebao.ls.callout.vo.CalloutReasonVO;
import com.ebao.ls.callout.vo.CalloutTransVO;
import com.ebao.ls.callout.vo.EVoiceResultVO;
import com.ebao.ls.cmu.event.CalloutAdded;
import com.ebao.ls.cmu.event.ECPCalloutReply;
import com.ebao.ls.framework.AppException;
import com.ebao.ls.notification.ci.NbEventCI;
import com.ebao.ls.pa.nb.bs.ODSService;
import com.ebao.ls.pa.nb.bs.PendingIssueService;
import com.ebao.ls.pa.nb.ci.UnbProcessCI;
import com.ebao.ls.pa.nb.vo.PolicyIssuePendingLogVO;
import com.ebao.ls.pa.pub.bs.AgentNotifyService;
import com.ebao.ls.pa.pub.bs.PolicyService;
import com.ebao.ls.pa.pub.vo.AgentNotifyVO;
import com.ebao.ls.pa.pub.vo.PolicyVO;
import com.ebao.ls.pub.CodeCst;
import com.ebao.ls.pub.cst.CalloutTargetType;
import com.ebao.ls.ws.ci.evoice.EVoiceWSCI;
import com.ebao.ls.ws.vo.unb.unb360.CalloutTransReplyVOInfo;
import com.ebao.pub.event.EventService;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.framework.GenericException;
import com.ebao.pub.util.DBean;
import com.ebao.pub.util.json.DateUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tgl.tools.ListTool;

public class CalloutServiceCIImpl implements CalloutServiceCI {

	private final Log log = Log.getLogger(CalloutServiceCIImpl.class);

	private static final String CALLOUT_REASON_DISP_MAPPING = "SELECT distinct(r.code), r.code_order from t_callout_exception_item t LEFT JOIN t_callout_reason r ON t.callout_reason = r.code ";

	private static final String CALLOUT_TRANS_SQL_BY_CALLOUT_NUMBER = "SELECT t.* from t_callout_trans t WHERE t.callout_num = ? ORDER BY t.list_id DESC";

	private static final String CALLOUT_TRANS_SQL_BY_POLICY_ID_AND_CALLEE_ID = "SELECT t.* from t_callout_trans t WHERE t.policy_id = ? AND t.callee_certi_code = ? ORDER BY t.list_id DESC";

	@Resource(name = CalloutTransService.BEAN_DEFAULT)
	CalloutTransService calloutTransService;

	@Resource(name = CalloutOnlineService.BEAN_DEFAULT)
	CalloutOnlineService calloutOnlineService;

	@Resource(name = EVoiceWSCI.BEAN_DEFAULT)
	EVoiceWSCI eVoiceWSCI;

	@Resource(name = PolicyInfoAjaxService.BEAN_DEFAUL)
	PolicyInfoAjaxService policyInfoAjaxService;

	@Resource(name = CalloutTargetAjaxService.BEAN_DEFAULT)
	CalloutTargetAjaxService calloutTargetAjaxService;

	@Resource(name = EventService.BEAN_DEFAULT)
	private EventService eventService;

	@Resource(name = PendingIssueService.BEAN_DEFAULT)
	private PendingIssueService pendingServ;

	@Resource(name = PolicyService.BEAN_DEFAULT)
	private PolicyService policyServ;

	@Resource(name = ODSService.BEAN_DEFAULT)
	private ODSService odsServ;

	@Resource(name = CalloutReasonDao.BEAN_DEFAULT)
	private CalloutReasonDao calloutReasonDao;

	@Resource(name = CalloutExceptionItemDao.BEAN_DEFAULT)
	private CalloutExceptionItemDao calloutExceptionItemDao;
	
	@Resource(name = CalloutQuizService.BEAN_DEFAULT)
	private CalloutQuizService calloutQuizService;
	
	@Resource(name=UnbProcessCI.BEAN_DEFAULT)
	UnbProcessCI unbProcessCI;
	
	@Resource(name = NbEventCI.BEAN_DEFAULT)
	private NbEventCI nbEventCI;

	@Resource(name = AgentNotifyService.BEAN_DEFAULT)
	private AgentNotifyService agentNotifyService;
	
	@Override
	public List<Map<String, Object>> query(String code, String certiCode,
					Date startDate, Date endDate) {
		if (code == null || certiCode == null || startDate == null
						|| endDate == null)
			return null;
		else if (code.length() == 0 || certiCode.length() == 0)
			return null;
		else {
			CalloutTransVO queryVO = new CalloutTransVO();
			queryVO.setCalloutTargetType(code);
			queryVO.setCalleeCertiCode(certiCode);
			return calloutTransService
							.getCalloutRecordByCalleeCertiCodeWithDate(queryVO,
											startDate, endDate);
		}
	}

	@Override
	public boolean updateCalloutReply(String calloutNum, Date calloutDate,
					String callerName, String calloutResult, String memo,
					String subResult) throws Exception {
		CalloutTransVO vo = calloutTransService
						.findCalloutTransVOByCalloutNum(calloutNum);
		//若重複回覆要寫核保中異動
		if (vo.getCalloutResult() != null && CodeCst.CALLOUT_RESULT_DEFAULT != Integer.parseInt(vo.getCalloutResult())) {
			unbProcessCI.calloutRepeatReply(vo);
		}
		vo.setCalloutTime(calloutDate);
		vo.seteVoiceCaller(callerName);
		vo.setCalloutResult(calloutResult);
		vo.setCalloutResultMemo(memo);
		vo.setCalloutSubResult(subResult);
		calloutTransService.updateVOList(ListTool.toList(vo), true);

		java.util.List<String> calloutResultList = Arrays.asList(new String[] {
						"1", "2", "3", "4" });// #219623
		if (calloutResultList.contains(calloutResult)) {//#219623: 電訪成功需自動解除暫緩
			updateUnpending(vo.getPolicyId(), calloutResult);
		}
		
		//永豐件電訪異常需發MAIL通知
		if(CodeCst.CALLOUT_RESULT_ERROR == Integer.parseInt(calloutResult)) {
			PolicyVO policyVO = policyServ.load(vo.getPolicyId());
			//永豐EC件
			if(policyServ.isEcSinoPac(policyVO)) {
				nbEventCI.sendTglCalloutErrorNotification(policyVO.getPolicyNumber(), getClass().getSimpleName());
			}
		}

		return true;
	}

	/**
	 * 解除電訪的暫緩發單
	 */
	public void unlockCalloutPending(Long policyId, String calloutResult) {
		/* 解除電訪暫緩 */
		List<PolicyIssuePendingLogVO> pendingLogVOList = pendingServ.findPendingListByPolicyId(policyId);
		Long pendingId = null;
		if (CollectionUtils.isEmpty(pendingLogVOList))
			return;
		for (PolicyIssuePendingLogVO logVO : pendingLogVOList) {
			if (logVO.getPendingCause() != null
							&& logVO.getPendingCause().intValue() == 50) {// 電訪暫緩
				pendingId = logVO.getListId();

				Boolean isTriggerEvent = false;
				java.util.List<String> calloutResultList = Arrays.asList(new String[] { "1", "3", "4" });
				// 電訪成功，產生保單面頁PDF檔
				if (calloutResultList.contains(calloutResult)) {
					isTriggerEvent = true;
				}

				pendingServ.updateUnpending(policyId, pendingId, isTriggerEvent);
			}
		}

	}

	private void updateUnpending(Long policyId, String calloutResult) {
		Session session = com.ebao.foundation.module.db.hibernate.HibernateSession3
						.currentSession();
		this.unlockCalloutPending(policyId, calloutResult);
		session.flush();

		/* 電訪異常: 解除契約 */
		if ("2".equals(calloutResult)) {
			withdrawPolicy(policyId);
		}

	}

	private void withdrawPolicy(Long policyId) {
		// UNB 會辦 CSC 協助辦理契撤
	}

	@Override
	public CalloutTransVO queryTypeByPolicyIdAndChangeId(String policyId,
					String changeId, String type) {
		return calloutTransService.queryTypeByPolicyIdAndChangeID(policyId,
						changeId, type);
	}

	@Override
	public CalloutOnlineVO sendEcCalloutMission(CalloutTransVO vo) throws GenericException {
		Map<String, Object> in = new HashMap<String, Object>();
		in.put("policyId", vo.getPolicyId().toString());
		in.put("ajaxType", PolicyInfoAjaxService.FIND_POLICY_CODE);
		Map<String, Object> policyCodeInfo = policyInfoAjaxService
						.findPolicyCode(in);
		in.clear();

		String policyCode = policyCodeInfo.get("policyCode").toString();

		// 保單基本資料抓取
		in = new HashMap<String, Object>();
		in.put("policyCode", policyCode);
		in.put("changeId", null);
		in.put("ajaxType", PolicyInfoAjaxService.FIND_POLICY_INFO);
		Map<String, Object> policyInfo = policyInfoAjaxService
						.findPolicyInfo(in);
		in.clear();

		// 電訪對象資料抓取 (要保人)
		in = new HashMap<String, Object>();
		in.put("policyCode", policyCode);
		in.put("type", 1);
		in.put("ajaxType", CalloutTargetAjaxService.NEW_TARGET);
		Map<String, Object> calloutTargetInfo = calloutTargetAjaxService
						.getNewTargetInfo(in);
		in.clear();

		// CalloutTransVO vo = new CalloutTransVO();
		calloutTransVoDefaultValueUNB(vo);
		// 保單基本資料
		//vo.setPolicyId(policyId);
		vo.setHolderName(MapUtils.getString(policyInfo, "holderName"));
		vo.setHolderCertiCode(MapUtils.getString(policyInfo, "holderCertiCode"));
		vo.setMainInsuredName(MapUtils.getString(policyInfo, "mainInsuredName"));
		vo.setMainInsuredCertiCode(MapUtils.getString(policyInfo,
						"mainInsuredCertiCode"));

		vo.setChannelType(MapUtils.getLong(policyInfo, "channelType"));
		vo.setChannelOrgId(MapUtils.getLong(policyInfo, "channelOrgId"));
		vo.setAgentId(MapUtils.getLong(policyInfo, "agentId"));
		vo.setServiceAgent(MapUtils.getLong(policyInfo, "underwriteId"));

		vo.setTransCount(1);//#225019
		vo.setIssueDate(getMapDate(policyInfo, "issueDate"));
		vo.setValidateDate(getMapDate(policyInfo, "validateDate"));
		vo.setAcknowledgeDate(getMapDate(policyInfo, "acknowledgeDate"));

		vo.setDispatchDate(getMapDate(policyInfo, "dispatchDate"));
		vo.setSignResult(MapUtils.getString(policyInfo, "signResult"));
		vo.setLegalSignResult(MapUtils.getString(policyInfo, "legalSignResult"));
		vo.setRegDate(getMapDate(policyInfo, "regDate"));
		vo.setServiceAgent(MapUtils.getLong(policyInfo, "serviceAgent"));

		// 電訪對象 : 要保人
		vo.setCalloutTargetType("1");
		vo.setExpectedCalloutDate(getMapDate(calloutTargetInfo,
						"expectedCalloutDate"));
		vo.setExpectedCalloutTime(MapUtils.getString(calloutTargetInfo,
						"expectedCalloutTime"));
		//BC461 要保人可電訪時間其他
		AgentNotifyVO agentNotifyVO = agentNotifyService.load(vo.getPolicyId());
		if(null != agentNotifyVO && !com.ebao.pub.util.StringUtil.isNullOrEmpty(agentNotifyVO.getHolderTelTimeOthers())) {
			vo.setExpectedCalloutTimeOther(agentNotifyVO.getHolderTelTimeOthers());
		}
			
		vo.setCalleeCertiCode(MapUtils.getString(calloutTargetInfo,
						"calleeCertiCode"));
		vo.setCalleeName(MapUtils.getString(calloutTargetInfo, "calleeName"));
		vo.setCalleeBirthday(getMapDate(calloutTargetInfo, "calleeBirthday"));
		//vo.setMemo(null); // PCR 397786 網路投保接口修改 上層自動帶入memo內容
		vo.setOfficeTelReg(MapUtils
						.getString(calloutTargetInfo, "officeTelReg"));
		vo.setOfficeTel(MapUtils.getString(calloutTargetInfo, "officeTel"));
		vo.setOfficeTelExt(MapUtils
						.getString(calloutTargetInfo, "officeTelExt"));
		vo.setHomeTelReg(MapUtils.getString(calloutTargetInfo, "homeTelReg"));
		vo.setHomeTel(MapUtils.getString(calloutTargetInfo, "homeTel"));
		vo.setHomeTelExt(MapUtils.getString(calloutTargetInfo, "homeTelExt"));
		vo.setContactNum1(MapUtils.getString(calloutTargetInfo, "contactNum1"));
		vo.setContactNum2(MapUtils.getString(calloutTargetInfo, "contactNum2"));
		vo.setContactNum3(null);
		vo.setContactNum4(null);
		vo.setContactNum5(null);

		vo.setCalloutDocketNum("UNB-EC-01");
		// PCR355468　UNB修改電訪任務編碼規則
		String calloutIdentity = calloutTransService.getCalloutIdentity(vo, "UNB");

		EVoiceResultVO eVoiceResultVO = null;
		CalloutOnlineVO calloutOnlineVO = null;
		/* 儲存 t_callout_trans */
		try {
			vo = calloutTransService.saveVO(vo, calloutIdentity);
		} catch (Exception ex) {
			ObjectMapper mapper = new ObjectMapper();
			try {
				String prettyJSON = mapper.writerWithDefaultPrettyPrinter()
								.writeValueAsString(vo);
				log.error("[INT-CMN-180][ERROR]" + prettyJSON);
			} catch (JsonProcessingException e) {

			}
			throw new AppException(30710020012L);
		}
		/* 發送 eVoice */
		try {
			eVoiceResultVO = calloutTransService.onlineSendToEvoice(vo);
			eventService.publishEvent(new CalloutAdded(vo));
		} catch (com.ebao.foundation.commons.exceptions.AppException ex) {
			/* 核心至eVoice 問題*/
			calloutTransService.deleteByListId(vo.getListId());
			vo.setCalloutNum(null);//清除 T_CALLOUT_TRANS
			throw ex;
		}

		calloutOnlineVO = new CalloutOnlineVO();
		calloutOnlineVO.seteVoiceResult(eVoiceResultVO);
		calloutOnlineVO.setTargetType(CalloutTargetType.POLICY_HOLDER);
		calloutOnlineVO.setCalleeCertiCode(vo.getCalleeCertiCode());
		calloutOnlineVO.setCalleeName(vo.getCalleeName());
		calloutOnlineVO.setCalloutNum(vo.getCalloutNum());
		calloutOnlineVO.seteVoiceResult(eVoiceResultVO);
		return calloutOnlineVO;
	}

	public List<CalloutReasonVO> findCallReasonsWithExceptionItems() {
		List<CalloutReasonVO> result = new ArrayList<CalloutReasonVO>();

		List<CalloutReason> reasonList = calloutReasonDao.findCallReasons();
		List<CalloutExceptionItem> exceptionlist = calloutExceptionItemDao
						.findAvalibleExceptionItem();

		// 電訪原因
		for (CalloutReason reason : reasonList) {
			CalloutReasonVO vo = new CalloutReasonVO();
			reason.copyToVO(vo, Boolean.FALSE);

			List<CalloutExceptionItemVO> exceptionList = new ArrayList<CalloutExceptionItemVO>();
			// 異常表徵
			for (CalloutExceptionItem item : exceptionlist) {
				if (reason.getCode().equals(item.getCalloutReason())) {
					CalloutExceptionItemVO exceptionVO = new CalloutExceptionItemVO();
					item.copyToVO(exceptionVO, Boolean.FALSE);
					exceptionList.add(exceptionVO);
				}
			}

			if (!exceptionList.isEmpty()) {
				vo.setExceptionList(exceptionList);
			}
			result.add(vo);
		}
		return result;
	}

	public String getUniqueReasonCodeFrExceptionItem() {
		List<String> list = calloutExceptionItemDao.getCalloutReason();
		if (!list.isEmpty()) {
			StringBuilder sb = new StringBuilder();
			for (String str : list) {
				sb.append(str).append(",");
			}
			return sb.toString().substring(0, sb.length() - 1);
		}
		return "";
	}

	protected Date getMapDate(Map<String, Object> map, String key) {
		if (map != null) {
			Object o = map.get(key);
			if (o != null) {
				try {
					if (o instanceof Date) {
						return (Date) o;
					} else if (o instanceof String) {
						return DateUtils.parse(o.toString(), "yyyyMMdd");
					} else {
						return null;
					}
				} catch (Exception e) {
					return null;
				}
			} else {
				return null;
			}
		} else {
			return null;
		}
	}

	@Override
	public boolean createECPCalloutMission(CalloutTransReplyVOInfo info) {
		boolean checkIndi = Boolean.TRUE;
		Long policyId = null;
		policyId = policyServ.getPolicyIdByPolicyCode(info.getPolicyCode());
		if (policyId != null) {
			// translate CalloutReslut CalloutDetailResult code
			String calloutResult = CodeTable.getIdByCode("T_CALLOUT_RESULT_TYPE", info.getCalloutReslut());
			String calloutDetailResult = CodeTable.getIdByCode("T_CALLOUT_SUBRESULT_TYPE",
							info.getCalloutDetailResult());
			// create vo
			CalloutTransVO vo = new CalloutTransVO();
			// 電訪default value
			calloutTransVoDefaultValueUNB(vo);
			vo.setCalloutNum(info.getCalloutNum());
			vo.setPolicyId(policyId);
			vo.setCalloutTargetType(info.getCalloutTargetType());
			vo.setCalleeName(info.getCalleeName());
			vo.setCalleeCertiCode(info.getCalleeCertiCode());
			vo.setCalloutQuestionnaireStr(info.getCalloutQuestionnaire());
			vo.setCalloutResult(calloutResult);
			vo.setCalloutSubResult(calloutDetailResult);
			vo.setCalloutTime(info.getCalloutDateTime());
			vo.seteVoiceCaller(info.getCallerName());
			vo.setTransCount(info.getTransCount());
			vo.setCalloutResultMemo(info.getCalloutResultMemo());
			vo.setCalloutDocketNum(info.getCalloutDocketNum());
			// 因為沒有取電訪任務編號問題，所以直接call saveVO
			calloutTransService.saveVO(vo);
			// 更新任務列 或會辦資料
			eventService.publishEvent(new ECPCalloutReply(vo));
		} else {
			checkIndi = Boolean.FALSE;
		}
		return checkIndi;
	}

	protected void calloutTransVoDefaultValueUNB(CalloutTransVO vo) {
		// Copy form sendEcCalloutMission method
		vo.setLastTransDate(null);
		vo.setCalloutReqDept(AppContext.getCurrentUser().getDeptId());
		// 電訪內容
		// POS : EC進件 視同 新契約進件
		vo.setChangeId(null);
		vo.setCalloutTypesStr(null);
		vo.setCalloutTypeOther(null);
		vo.setParentPolicyId(null);
		vo.setCalloutAsksStr(null);
		vo.setTiedPolicy(null);

		vo.setCalloutReasonDesc(null);
		vo.setCalloutExceptionItem(null);
		vo.setCalloutExceptionDesc(null);
		vo.setSpecialConcernIndi("N");
		vo.setCoverageIndi("N");
		vo.setEmergencyIndi("N");

		vo.setRelation1(null);
		vo.setRelation2(null);
		// 電訪結果 : EC 電訪發起尚未有結果
		vo.setCalloutResult("0");
		vo.setCalloutSubResult(null);
		vo.setCalloutTime(null);

		vo.setHour(null);
		vo.setMinute(null);
		vo.setCallerParty(null);
		vo.setCallerExt(null);
		vo.setCalloutResultMemo(null);
		vo.seteVoiceCaller(null);

		// 電訪隱藏設定
		vo.setBatchIndi("N");
		vo.setCalloutFileName(null);
		vo.setCalloutImportantSender(null);
	}

	@Override
	public String findRemarkItems() {
		DBean db = new DBean();
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuilder sb = new StringBuilder();

		StringBuilder buf = new StringBuilder();
		buf.append(" SELECT R.CODE_ORDER, I.CODE_DISP, R.CODE_VALUE, I.CODE_VALUE ");
		buf.append("   FROM T_CALLOUT_REASON R                                    ");
		buf.append("   LEFT JOIN T_CALLOUT_EXCEPTION_ITEM I                       ");
		buf.append("     ON R.CODE = I.CALLOUT_REASON                             ");
		buf.append("  WHERE R.REMARK_ABLE = 'Y'                                   ");
		buf.append("     OR I.REMARK_ABLE = 'Y'                                   ");
		buf.append("  ORDER BY R.CODE_ORDER, I.CODE_ORDER                         ");

		try {
			db.connect();
			Connection con = db.getConnection();
			ps = con.prepareStatement(buf.toString());
			rs = ps.executeQuery();
			while (rs.next()) {
				String reasonCode = rs.getString("CODE_ORDER");
				String exceptionItemCode = rs.getString("CODE_DISP");
				if (StringUtils.isEmpty(exceptionItemCode)) {
					sb.append("\"" + reasonCode + "_remark" + "\"").append(",");
				} else {
					sb.append("\"" + reasonCode + "-" + exceptionItemCode + "_remark" + "\"").append(",");
				}
			}

			if (sb.length() > 0) {
				return sb.substring(0, sb.length() - 1);
			}
		} catch (SQLException e) {

		} catch (ClassNotFoundException e) {

		} finally {
			DBean.closeAll(rs, ps, db);
		}
		return "";
	}

	public List<String> getCalloutReasonDisplayMapping() {
		DBean db = new DBean();
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<String> list = new ArrayList<String>();
		try {
			db.connect();
			Connection con = db.getConnection();
			ps = con.prepareStatement(CalloutServiceCIImpl.CALLOUT_REASON_DISP_MAPPING);
			rs = ps.executeQuery();
			while (rs.next()) {
				String code = rs.getString("CODE");
				String order = rs.getString("CODE_ORDER");
				list.add("\"" + code + "-" + order + "\"");
			}

		} catch (SQLException e) {

		} catch (ClassNotFoundException e) {

		} finally {
			DBean.closeAll(rs, ps, db);
		}
		return list;
	}

	@Override
	public CalloutTransVO queryCalloutTransByCalloutNum(String calloutNum) throws Exception {
		CalloutTransVO vo = calloutTransService.findCalloutTransVOByCalloutNum(calloutNum);
		if (vo == null) {
			return new CalloutTransVO();
		}
		return vo;
	}
	
	@Override
	public  Boolean writeUwElderCare(List<com.ebao.ls.ws.vo.cmn.cmn190.CalloutQuiz> volist) throws Exception {
		return calloutQuizService.writeUwElderCare(volist);
	}
	
	@Override
	public void updateCalloutQuizList(List<com.ebao.ls.ws.vo.cmn.cmn190.CalloutQuiz> volist, String uwSourceType)
			throws Exception {
		calloutQuizService.updateOrInsertVOList(volist, uwSourceType);
	}
	
	@Override
	public CalloutTransVO queryCalloutTrans(Long policyId, String calleeCertiCode) throws Exception {
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("policyId", policyId);
		params.put("calleeCertiCode", calleeCertiCode);

		HashMap<String, Object> orders = new HashMap<String, Object>();
		orders.put("listId", "desc");

		List<CalloutTransVO> voList = calloutTransService.find(params, orders);

		if (voList.isEmpty()) {
			return new CalloutTransVO();
		} else {
			return voList.get(0);
		}
	}

}
