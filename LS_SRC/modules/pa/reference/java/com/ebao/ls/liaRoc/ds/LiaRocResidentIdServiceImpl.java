package com.ebao.ls.liaRoc.ds;

import com.ebao.ls.foundation.service.GenericServiceImpl;
import com.ebao.ls.liaRoc.bo.LiaRocResidentId;
import com.ebao.ls.liaRoc.bo.LiaRocResidentIdVO;
import com.ebao.ls.liaRoc.data.LiaRocResidentIdDao;

public class LiaRocResidentIdServiceImpl
		extends GenericServiceImpl<LiaRocResidentIdVO, LiaRocResidentId, LiaRocResidentIdDao<LiaRocResidentId>>
		implements LiaRocResidentIdService {

	@Override
	protected LiaRocResidentIdVO newEntityVO() {
		return new LiaRocResidentIdVO();
	}

}
