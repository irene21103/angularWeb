package com.ebao.ls.subprem.bs;

import javax.annotation.Resource;

import com.ebao.ls.pa.pub.service.PolicyService;
import com.ebao.ls.subprem.data.SubpremUpdateDao;
import com.ebao.pub.annotation.PAPubAPIUpdate;
import com.ebao.pub.framework.ExceptionFactory;
import com.ebao.pub.framework.GenericDS;
import com.ebao.pub.framework.GenericException;

/**
 * <p>
 * Title:
 * </p>
 * <p>
 * Description:
 * </p>
 * <p>
 * Copyright: Copyright (c) 2002
 * </p>
 * <p>
 * Company: eBaoTech
 * </p>
 * <p>
 * Create Time:
 * </p>
 * 
 * @author
 * @version
 */
public class UpdateActionServiceImpl extends GenericDS
    implements
      UpdateActionService {
  /**
   * Extract
   * 
   * @param policyNumber String
   * @param dueDate String
   */
  public void singleExtractByPlSql(Long policyId, String dueDate)
      throws GenericException {
//  第三個變數For批次使用所需的Batch_no，故在此先放空值  
    SubpremUpdateDao.execPlSql(policyId, dueDate);
  }

  /**
   * getPolicyIdByPolicyCode
   * 
   * @param policyCode String
   */
  @PAPubAPIUpdate("getPolicyIdByPolicyNumber")
  public String getPolicyIdByPolicyCode(String policyCode)
      throws GenericException {
    Long policyId = Long.valueOf(0);
    try {
      Long lPolicyId = policyService.getPolicyIdByPolicyNumber(policyCode);
      if (lPolicyId != null) {
        policyId = lPolicyId;
      }
    } catch (Exception e) {
      throw ExceptionFactory.parse(e);
    }
    return policyId.toString();
  }

  @Resource(name = PolicyService.BEAN_DEFAULT)
  private PolicyService policyService;
}
