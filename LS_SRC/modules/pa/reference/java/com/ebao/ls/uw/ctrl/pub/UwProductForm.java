package com.ebao.ls.uw.ctrl.pub;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionMapping;

import com.ebao.pub.framework.GenericForm;
import com.ebao.pub.util.BeanUtils;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004 </p>
 * <p>Company: eBaoTech Corporation </p>
 * <p>Create Time: Thu Sep 23 16:56:29 GMT+08:00 2004 </p>
 * @author Walter Huang
 * @version 1.0
 * 
 */

public class UwProductForm extends GenericForm {

  private java.lang.Long underwriteId = null;

  private java.lang.Long itemId = null;

  private java.lang.String initialType = "";

  private java.lang.String renewalType = "";

  private java.lang.String chargePeriod = "";

  private java.lang.Integer chargeYear = null;

  private java.lang.String coveragePeriod = "";

  private java.lang.Integer coverageYear = null;

  private java.util.Date validateDate = null;

  private java.util.Date actualValidate = null;

  private java.lang.String advantageIndi1 = "";

  private java.lang.String advantageIndi2 = "";

  private java.lang.String countWay = "";

  private java.lang.String policyPremSource = "";

  private java.math.BigDecimal saFactor = null;

  private java.lang.String benefitLevel = "";

  private java.lang.String reducedLevel = "";

  private java.lang.String recordStatus = "";

  private java.lang.String uwStatus = "";

  private java.lang.Integer decisionId = null;

  private java.lang.String decisionDesc = "";

  private java.lang.String reuwDesc = "";

  private java.lang.Long underwriterId = null;

  private java.util.Date underwriteTime = null;

  private java.lang.String renewCause = "";

  private java.lang.String renewDesc = "";

  private java.lang.String insuredStatus = "";

  private java.lang.Long jobCate1 = null;

  private java.lang.Integer jobClass1 = null;

  private java.lang.String smoking = "";

  private java.lang.Long jobCate2 = null;

  private java.lang.Long exceptValue = null;

  private java.lang.Long policyId = null;

  private java.lang.String discountType = "";

  private java.lang.String policyPremWaived = "";

  private java.math.BigDecimal stdPremBf = null;

  private java.math.BigDecimal discntPremBf = null;

  private java.math.BigDecimal discntedPremBf = null;

  private java.math.BigDecimal policyFeeBf = null;

  private java.math.BigDecimal extraPremBf = null;

  private java.math.BigDecimal stdPremAf = null;

  private java.math.BigDecimal discntPremAf = null;

  private java.math.BigDecimal discntedPremAf = null;

  private java.math.BigDecimal policyFeeAf = null;

  private java.math.BigDecimal grossPremAf = null;

  private java.math.BigDecimal extraPremAf = null;

  private java.math.BigDecimal totalPremAf = null;

  private java.math.BigDecimal totalPremAn = null;

  private java.math.BigDecimal stdPremAn = null;

  private java.math.BigDecimal discntPremAn = null;

  private java.math.BigDecimal discntedPremAn = null;

  private java.math.BigDecimal policyFeeAn = null;

  private java.math.BigDecimal extraPremAn = null;

  private java.math.BigDecimal nextStdPremBf = null;

  private java.math.BigDecimal nextDiscntPremBf = null;

  private java.math.BigDecimal nextDiscntedPremBf = null;

  private java.math.BigDecimal nextPolicyFeeBf = null;

  private java.math.BigDecimal nextExtraPremBf = null;

  private java.math.BigDecimal nextStdPremAf = null;

  private java.math.BigDecimal nextDiscntPremAf = null;

  private java.math.BigDecimal nextDiscntedPremAf = null;

  private java.math.BigDecimal nextPolicyFeeAf = null;

  private java.math.BigDecimal nextGrossPremAf = null;

  private java.math.BigDecimal nextExtraPremAf = null;

  private java.math.BigDecimal nextTotalPremAf = null;

  private java.math.BigDecimal nextStdPremAn = null;

  private java.math.BigDecimal nextDiscntPremAn = null;

  private java.math.BigDecimal nextDiscntedPremAn = null;

  private java.math.BigDecimal nextPolicyFeeAn = null;

  private java.math.BigDecimal nextExtraPremAn = null;

  private java.lang.Integer gibStatus = null;

  private java.math.BigDecimal amount = null;

  private java.math.BigDecimal reducedAmount = null;

  private java.math.BigDecimal unit = null;

  private java.math.BigDecimal reducedUnit = null;

  private java.lang.Integer uwChange = null;

  private java.lang.String renewDecision = "";

  private java.lang.String standLife1 = "";

  private java.lang.Integer finalEm1 = null;

  private java.lang.Integer emHealth1 = null;

  private java.lang.Integer jobClass2 = null;

  private java.lang.Integer finalEm2 = null;

  private java.lang.Integer emHealth2 = null;

  private java.lang.String smokingRelated = "";

  private java.lang.Integer uwChargeYear = null;

  private java.lang.Integer uwCoverageYear = null;

  private java.lang.String decisionReason = "";

  private java.lang.String decisionReasonDesc = "";

  private java.lang.String uwChargePeriod = "";

  private java.lang.String uwCoveragePeriod = "";

  private java.lang.String entityFund = "";

  private java.util.Date gurntStartDate = null;

  private java.lang.String gurntPerdType = "";

  private java.lang.Integer gurntPeriod = null;

  private java.lang.Integer investHorizon = null;

  private java.lang.String manualSa = "";

  private java.lang.Integer deferPeriod = null;

  private java.lang.Integer gibRejCode = null;

  private java.lang.String holidayIndi = "";

  private java.lang.Integer industryId1 = null;

  private java.lang.Integer industryId2 = null;

  private java.lang.String standLife2 = "";

  private java.lang.Integer emValue1 = null;

  private java.lang.Integer emValue2 = null;

  private java.lang.String fullDeclare = "";

  private java.lang.String hthFullDeclare = "";

  private java.lang.String surrValueIndi = "";

  private java.lang.Integer waitPeriod = null;

  private java.lang.Integer dividendOptTerm = null;

  private java.lang.Integer survivalOptTerm = null;

  private java.lang.Integer maturityOptTerm = null;

  private java.lang.String maturityOption = "";

  private java.math.BigDecimal waivAnulBenefit = null;

  private java.math.BigDecimal waivAnulPrem = null;

  private java.lang.Long suspendChgId = null;

  private java.util.Date lapseDate = null;

  private java.lang.Integer lapseCause = null;

  private java.lang.String mortgageStatus = "";

  private java.math.BigDecimal mortgageRate = null;

  private java.util.Date birthday2 = null;

  private java.util.Date bonusDueDate = null;

  private java.util.Date premChangeTime = null;

  private java.util.Date submissionDate = null;

  private java.math.BigDecimal ilpCalcSa = null;

  private java.lang.String insuredStatus2 = "";

  private java.lang.Long masterId = null;

  private java.lang.Integer productId = null;

  private java.lang.Integer dividendChoice = null;

  private java.util.Date applyDate = null;

  private java.util.Date endDate = null;

  private java.util.Date paidupDate = null;

  private java.lang.Integer liabilityState = null;

  private java.lang.Integer endCause = null;

  private java.lang.Long insured1 = null;

  private java.lang.Integer age1 = null;

  private java.lang.Integer relation1 = null;

  private java.lang.String job11 = "";

  private java.lang.String job12 = "";

  private java.lang.Long insured2 = null;

  private java.lang.Integer age2 = null;

  private java.lang.Integer relation2 = null;

  private java.lang.String job21 = "";

  private java.lang.String job22 = "";

  private java.lang.String payPeriod = "";

  private java.lang.Integer payYear = null;

  private java.lang.String endPeriod = "";

  private java.lang.Integer endYear = null;

  private java.lang.Integer payEnsure = null;

  private java.lang.String payType = "";

  private java.util.Date shortEndTime = null;

  private java.lang.String housekeeper = "";

  private java.math.BigDecimal payRate = null;

  private java.lang.String insuredCategory = "";

  private java.lang.Long insuredAmount = null;

  private java.lang.String startPay = "";

  private java.math.BigDecimal retiredRate = null;

  private java.lang.String gender1 = "";

  private java.lang.String gender2 = "";

  private java.util.Date updateTime = null;

  private java.lang.String suspend = "";

  private java.lang.Integer suspendCause = null;

  private java.lang.Integer ageMonth = null;

  private java.lang.Integer relatedMonth = null;

  private java.math.BigDecimal increaseRate = null;

  private java.lang.String increaseFreq = "";

  private java.lang.Integer increaseYear = null;

  private java.lang.Integer payMode = null;

  private java.math.BigDecimal expiryCashValue = null;

  private java.lang.String retired = "";

  private java.lang.String simpleCompound = "";

  private java.util.Date startPayDate = null;

  private java.util.Date insertTime = null;

  private java.util.Date payChangeTime = null;

  private java.math.BigDecimal bonusSa = null;

  private java.lang.String liveRange = "";

  private java.lang.String moveRange = "";

  private java.lang.Integer payNext = null;

  private java.math.BigDecimal latestBonusSa = null;

  private java.util.Date startInstDate = null;

  private java.math.BigDecimal originSa = null;

  private java.math.BigDecimal originBonusSa = null;

  private java.math.BigDecimal anniBalance = null;

  private java.lang.String fixIncrement = "";

  private java.math.BigDecimal cpfCost = null;

  private java.math.BigDecimal cashCost = null;

  private java.lang.String derivation = "";

  private java.math.BigDecimal latOriBonusSa = null;

  private java.lang.String riskCode = "";

  private java.math.BigDecimal exposureRate = null;

  private java.math.BigDecimal reinsRate = null;

  private java.lang.String medicalFlag = "";

  private java.math.BigDecimal nextAmount = null;

  private java.util.Date waiverStart = null;

  private java.util.Date waiverEnd = null;

  private java.lang.String autoPermntLapse = "";

  private java.util.Date permntLapseNoticeDate = null;

  private java.lang.Integer masterProduct = null;

  private java.util.Date birthday = null;

  private java.lang.String waiver = "";

  private java.math.BigDecimal waivedSa = null;

  private java.lang.Long issueAgent = null;

  private java.lang.String strategyCode = "";

  private java.lang.Integer mmYear = null;

  private java.lang.String loanType = "";

  private java.lang.String benPeriodType = "";

  private java.lang.Integer benefitPeriod = null;

  private java.lang.String facReinsurIndi = "";

  private java.lang.Long uwListId = null;

  private String decisionDesc2 = null;

  protected UwExtraLoadingForm[] uwextraloadings;

  protected UwLienForm[] uwliens;

  private UwProductDecisionForm uwproductdecision = null;

  private Date statusDate;

  private boolean jobDiff;

  private String underwriteJob;

  private boolean extraArithsAvailable;

  private String indxIndicator;

  private java.lang.Integer postponePeriod;

  private String productName ;
  
  public java.lang.Integer getPostponePeriod() {
    return postponePeriod;
  }

  public void setPostponePeriod(java.lang.Integer postponePeriod) {
    this.postponePeriod = postponePeriod;
  }

  public String getIndxIndicator() {
    return indxIndicator;
  }

  public void setIndxIndicator(String indxIndicator) {
    this.indxIndicator = indxIndicator;
  }

  public boolean isExtraArithsAvailable() {
    return extraArithsAvailable;
  }

  public void setExtraArithsAvailable(boolean extraArithsAvailable) {
    this.extraArithsAvailable = extraArithsAvailable;
  }

  public String getUnderwriteJob() {
    return underwriteJob;
  }

  public void setUnderwriteJob(String underwriteJob) {
    this.underwriteJob = underwriteJob;
  }

  public boolean isJobDiff() {
    return jobDiff;
  }

  public void setJobDiff(boolean jobDiff) {
    this.jobDiff = jobDiff;
  }

  /**
   * sets the value of the UwProductDecision
   * @param value
   */
  public void setUwProductDecisionForm(UwProductDecisionForm value) {
    uwproductdecision = value;
  }

  /**
   * returns the value of the UwProductDecision
   * 
   * @return the uwproductdecision
   */
  public UwProductDecisionForm getUwProductDecision() {
    if (uwproductdecision == null) {
      uwproductdecision = new UwProductDecisionForm();
      uwproductdecision.setUnderwriteId(getUnderwriteId());
      uwproductdecision.setItemId(getItemId());
      uwproductdecision.setDecisionId(getDecisionId());
      uwproductdecision.setUwStatus(getUwStatus());
      uwproductdecision.setDecisionDesc(getDecisionDesc());
      uwproductdecision.setRenewDecision(getRenewDecision());
      uwproductdecision.setReuwDesc(getReuwDesc());
      uwproductdecision.setSmoking(getSmoking());
      uwproductdecision.setSmokingRelated(getSmokingRelated());
      uwproductdecision.setJobCate1(getJobCate1());
      uwproductdecision.setJobClass2(getJobClass2());
      uwproductdecision.setJobCate2(getJobCate2());
      uwproductdecision.setJobClass1(getJobClass1());
      uwproductdecision.setInsuredStatus(getInsuredStatus());
      uwproductdecision.setInsuredStatus2(getInsuredStatus2());
      uwproductdecision.setFacReinsurIndi(getFacReinsurIndi());
      uwproductdecision.setDecisionReason(getDecisionReason());
      uwproductdecision.setUnderwriteTime(getUnderwriteTime());
      uwproductdecision.setUnderwriterId(getUnderwriterId());
      uwproductdecision.setDecisionReasonDesc(getDecisionReasonDesc());
      uwproductdecision.setUwListId(getUwListId());
      uwproductdecision.setDecisionDesc2(getDecisionDesc2());

    }

    return uwproductdecision;
  }

  private UwRestrictionForm uwrestriction = null;

  /**
   * sets the value of the UwRestriction
   * @param value
   */
  public void setUwRestrictionForm(UwRestrictionForm value) {
    uwrestriction = value;
  }

  /**
   * returns the value of the UwRestriction
   * 
   * @return the uwrestriction
   */
  public UwRestrictionForm getUwRestriction() {
    if (uwrestriction == null) {

      uwrestriction = new UwRestrictionForm();

      uwrestriction.setUnderwriteId(getUnderwriteId());
      uwrestriction.setItemId(getItemId());
      uwrestriction.setChargePeriod(getChargePeriod());
      uwrestriction.setChargeYear(getChargeYear());
      uwrestriction.setCoveragePeriod(getCoveragePeriod());
      uwrestriction.setCoverageYear(getCoverageYear());
      uwrestriction.setAmount(getAmount());
      uwrestriction.setReducedAmount(getReducedAmount());
      uwrestriction.setUnit(getUnit());
      uwrestriction.setReducedUnit(getReducedUnit());
      uwrestriction.setUwListId(getUwListId());

    }

    return uwrestriction;
  }

  /**
   * returns the value of the underwriteId
   * 
   * @return the underwriteId
   */
  public java.lang.Long getUnderwriteId() {
    return underwriteId;
  }

  /**
   * sets the value of the underwriteId
   * 
   * @param underwriteId
   *          the underwriteId
   */
  public void setUnderwriteId(java.lang.Long underwriteId) {
    this.underwriteId = underwriteId;
  }

  /**
   * returns the value of the itemId
   * 
   * @return the itemId
   */
  public java.lang.Long getItemId() {
    return itemId;
  }

  /**
   * sets the value of the itemId
   * 
   * @param itemId
   *          the itemId
   */
  public void setItemId(java.lang.Long itemId) {
    this.itemId = itemId;
  }

  /**
   * returns the value of the initialType
   * 
   * @return the initialType
   */
  public java.lang.String getInitialType() {
    return initialType;
  }

  /**
   * sets the value of the initialType
   * 
   * @param initialType
   *          the initialType
   */
  public void setInitialType(java.lang.String initialType) {
    this.initialType = initialType;
  }

  /**
   * returns the value of the renewalType
   * 
   * @return the renewalType
   */
  public java.lang.String getRenewalType() {
    return renewalType;
  }

  /**
   * sets the value of the renewalType
   * 
   * @param renewalType
   *          the renewalType
   */
  public void setRenewalType(java.lang.String renewalType) {
    this.renewalType = renewalType;
  }

  /**
   * returns the value of the chargePeriod
   * 
   * @return the chargePeriod
   */
  public java.lang.String getChargePeriod() {
    return chargePeriod;
  }

  /**
   * sets the value of the chargePeriod
   * 
   * @param chargePeriod
   *          the chargePeriod
   */
  public void setChargePeriod(java.lang.String chargePeriod) {
    this.chargePeriod = chargePeriod;
  }

  /**
   * returns the value of the chargeYear
   * 
   * @return the chargeYear
   */
  public java.lang.Integer getChargeYear() {
    return chargeYear;
  }

  /**
   * sets the value of the chargeYear
   * 
   * @param chargeYear
   *          the chargeYear
   */
  public void setChargeYear(java.lang.Integer chargeYear) {
    this.chargeYear = chargeYear;
  }

  /**
   * returns the value of the coveragePeriod
   * 
   * @return the coveragePeriod
   */
  public java.lang.String getCoveragePeriod() {
    return coveragePeriod;
  }

  /**
   * sets the value of the coveragePeriod
   * 
   * @param coveragePeriod
   *          the coveragePeriod
   */
  public void setCoveragePeriod(java.lang.String coveragePeriod) {
    this.coveragePeriod = coveragePeriod;
  }

  /**
   * returns the value of the coverageYear
   * 
   * @return the coverageYear
   */
  public java.lang.Integer getCoverageYear() {
    return coverageYear;
  }

  /**
   * sets the value of the coverageYear
   * 
   * @param coverageYear
   *          the coverageYear
   */
  public void setCoverageYear(java.lang.Integer coverageYear) {
    this.coverageYear = coverageYear;
  }

  /**
   * returns the value of the validateDate
   * 
   * @return the validateDate
   */
  public java.util.Date getValidateDate() {
    return validateDate;
  }

  /**
   * sets the value of the validateDate
   * 
   * @param validateDate
   *          the validateDate
   */
  public void setValidateDate(java.util.Date validateDate) {
    this.validateDate = validateDate;
  }

  /**
   * returns the value of the actualValidate
   * 
   * @return the actualValidate
   */
  public java.util.Date getActualValidate() {
    return actualValidate;
  }

  /**
   * sets the value of the actualValidate
   * 
   * @param actualValidate
   *          the actualValidate
   */
  public void setActualValidate(java.util.Date actualValidate) {
    this.actualValidate = actualValidate;
  }

  public java.lang.String getAdvantageIndi1() {
    return advantageIndi1;
  }

  public void setAdvantageIndi1(java.lang.String advantageIndi1) {
    this.advantageIndi1 = advantageIndi1;
  }

  public java.lang.String getAdvantageIndi2() {
    return advantageIndi2;
  }

  public void setAdvantageIndi2(java.lang.String advantageIndi2) {
    this.advantageIndi2 = advantageIndi2;
  }

  /**
   * returns the value of the countWay
   * 
   * @return the countWay
   */
  public java.lang.String getCountWay() {
    return countWay;
  }

  /**
   * sets the value of the countWay
   * 
   * @param countWay
   *          the countWay
   */
  public void setCountWay(java.lang.String countWay) {
    this.countWay = countWay;
  }

  /**
   * returns the value of the policyPremSource
   * 
   * @return the policyPremSource
   */
  public java.lang.String getPolicyPremSource() {
    return policyPremSource;
  }

  /**
   * sets the value of the policyPremSource
   * 
   * @param policyPremSource
   *          the policyPremSource
   */
  public void setPolicyPremSource(java.lang.String policyPremSource) {
    this.policyPremSource = policyPremSource;
  }

  /**
   * returns the value of the saFactor
   * 
   * @return the saFactor
   */
  public java.math.BigDecimal getSaFactor() {
    return saFactor;
  }

  /**
   * sets the value of the saFactor
   * 
   * @param saFactor
   *          the saFactor
   */
  public void setSaFactor(java.math.BigDecimal saFactor) {
    this.saFactor = saFactor;
  }

  /**
   * returns the value of the benefitLevel
   * 
   * @return the benefitLevel
   */
  public java.lang.String getBenefitLevel() {
    return benefitLevel;
  }

  /**
   * sets the value of the benefitLevel
   * 
   * @param benefitLevel
   *          the benefitLevel
   */
  public void setBenefitLevel(java.lang.String benefitLevel) {
    this.benefitLevel = benefitLevel;
  }

  /**
   * returns the value of the reducedLevel
   * 
   * @return the reducedLevel
   */
  public java.lang.String getReducedLevel() {
    return reducedLevel;
  }

  /**
   * sets the value of the reducedLevel
   * 
   * @param reducedLevel
   *          the reducedLevel
   */
  public void setReducedLevel(java.lang.String reducedLevel) {
    this.reducedLevel = reducedLevel;
  }

  /**
   * returns the value of the recordStatus
   * 
   * @return the recordStatus
   */
  public java.lang.String getRecordStatus() {
    return recordStatus;
  }

  /**
   * sets the value of the recordStatus
   * 
   * @param recordStatus
   *          the recordStatus
   */
  public void setRecordStatus(java.lang.String recordStatus) {
    this.recordStatus = recordStatus;
  }

  /**
   * returns the value of the uwStatus
   * 
   * @return the uwStatus
   */
  public java.lang.String getUwStatus() {
    return uwStatus;
  }

  /**
   * sets the value of the uwStatus
   * 
   * @param uwStatus
   *          the uwStatus
   */
  public void setUwStatus(java.lang.String uwStatus) {
    this.uwStatus = uwStatus;
  }

  /**
   * returns the value of the decisionId
   * 
   * @return the decisionId
   */
  public java.lang.Integer getDecisionId() {
    return decisionId;
  }

  /**
   * sets the value of the decisionId
   * 
   * @param decisionId
   *          the decisionId
   */
  public void setDecisionId(java.lang.Integer decisionId) {
    this.decisionId = decisionId;
  }

  /**
   * returns the value of the decisionDesc
   * 
   * @return the decisionDesc
   */
  public java.lang.String getDecisionDesc() {
    return decisionDesc;
  }

  /**
   * sets the value of the decisionDesc
   * 
   * @param decisionDesc
   *          the decisionDesc
   */
  public void setDecisionDesc(java.lang.String decisionDesc) {
    this.decisionDesc = decisionDesc;
  }

  /**
   * returns the value of the reuwDesc
   * 
   * @return the reuwDesc
   */
  public java.lang.String getReuwDesc() {
    return reuwDesc;
  }

  /**
   * sets the value of the reuwDesc
   * 
   * @param reuwDesc
   *          the reuwDesc
   */
  public void setReuwDesc(java.lang.String reuwDesc) {
    this.reuwDesc = reuwDesc;
  }

  /**
   * returns the value of the underwriterId
   * 
   * @return the underwriterId
   */
  public java.lang.Long getUnderwriterId() {
    return underwriterId;
  }

  /**
   * sets the value of the underwriterId
   * 
   * @param underwriterId
   *          the underwriterId
   */
  public void setUnderwriterId(java.lang.Long underwriterId) {
    this.underwriterId = underwriterId;
  }

  /**
   * returns the value of the underwriteTime
   * 
   * @return the underwriteTime
   */
  public java.util.Date getUnderwriteTime() {
    return underwriteTime;
  }

  /**
   * sets the value of the underwriteTime
   * 
   * @param underwriteTime
   *          the underwriteTime
   */
  public void setUnderwriteTime(java.util.Date underwriteTime) {
    this.underwriteTime = underwriteTime;
  }

  /**
   * returns the value of the renewCause
   * 
   * @return the renewCause
   */
  public java.lang.String getRenewCause() {
    return renewCause;
  }

  /**
   * sets the value of the renewCause
   * 
   * @param renewCause
   *          the renewCause
   */
  public void setRenewCause(java.lang.String renewCause) {
    this.renewCause = renewCause;
  }

  /**
   * returns the value of the renewDesc
   * 
   * @return the renewDesc
   */
  public java.lang.String getRenewDesc() {
    return renewDesc;
  }

  /**
   * sets the value of the renewDesc
   * 
   * @param renewDesc
   *          the renewDesc
   */
  public void setRenewDesc(java.lang.String renewDesc) {
    this.renewDesc = renewDesc;
  }

  /**
   * returns the value of the insuredStatus
   * 
   * @return the insuredStatus
   */
  public java.lang.String getInsuredStatus() {
    return insuredStatus;
  }

  /**
   * sets the value of the insuredStatus
   * 
   * @param insuredStatus
   *          the insuredStatus
   */
  public void setInsuredStatus(java.lang.String insuredStatus) {
    this.insuredStatus = insuredStatus;
  }

  /**
   * returns the value of the jobCate1
   * 
   * @return the jobCate1
   */
  public java.lang.Long getJobCate1() {
    return jobCate1;
  }

  /**
   * sets the value of the jobCate1
   * 
   * @param jobCate1
   *          the jobCate1
   */
  public void setJobCate1(java.lang.Long jobCate1) {
    this.jobCate1 = jobCate1;
  }

  /**
   * returns the value of the jobClass1
   * 
   * @return the jobClass1
   */
  public java.lang.Integer getJobClass1() {
    return jobClass1;
  }

  /**
   * sets the value of the jobClass1
   * 
   * @param jobClass1
   *          the jobClass1
   */
  public void setJobClass1(java.lang.Integer jobClass1) {
    this.jobClass1 = jobClass1;
  }

  /**
   * returns the value of the smoking
   * 
   * @return the smoking
   */
  public java.lang.String getSmoking() {
    return smoking;
  }

  /**
   * sets the value of the smoking
   * 
   * @param smoking
   *          the smoking
   */
  public void setSmoking(java.lang.String smoking) {
    this.smoking = smoking;
  }

  /**
   * returns the value of the jobCate2
   * 
   * @return the jobCate2
   */
  public java.lang.Long getJobCate2() {
    return jobCate2;
  }

  /**
   * sets the value of the jobCate2
   * 
   * @param jobCate2
   *          the jobCate2
   */
  public void setJobCate2(java.lang.Long jobCate2) {
    this.jobCate2 = jobCate2;
  }

  /**
   * returns the value of the exceptValue
   * 
   * @return the exceptValue
   */
  public java.lang.Long getExceptValue() {
    return exceptValue;
  }

  /**
   * sets the value of the exceptValue
   * 
   * @param exceptValue
   *          the exceptValue
   */
  public void setExceptValue(java.lang.Long exceptValue) {
    this.exceptValue = exceptValue;
  }

  /**
   * returns the value of the policyId
   * 
   * @return the policyId
   */
  public java.lang.Long getPolicyId() {
    return policyId;
  }

  /**
   * sets the value of the policyId
   * 
   * @param policyId
   *          the policyId
   */
  public void setPolicyId(java.lang.Long policyId) {
    this.policyId = policyId;
  }

  /**
   * returns the value of the discountType
   * 
   * @return the discountType
   */
  public java.lang.String getDiscountType() {
    return discountType;
  }

  /**
   * sets the value of the discountType
   * 
   * @param discountType
   *          the discountType
   */
  public void setDiscountType(java.lang.String discountType) {
    this.discountType = discountType;
  }

  /**
   * returns the value of the policyPremWaived
   * 
   * @return the policyPremWaived
   */
  public java.lang.String getPolicyPremWaived() {
    return policyPremWaived;
  }

  /**
   * sets the value of the policyPremWaived
   * 
   * @param policyPremWaived
   *          the policyPremWaived
   */
  public void setPolicyPremWaived(java.lang.String policyPremWaived) {
    this.policyPremWaived = policyPremWaived;
  }

  /**
   * returns the value of the stdPremBf
   * 
   * @return the stdPremBf
   */
  public java.math.BigDecimal getStdPremBf() {
    return stdPremBf;
  }

  /**
   * sets the value of the stdPremBf
   * 
   * @param stdPremBf
   *          the stdPremBf
   */
  public void setStdPremBf(java.math.BigDecimal stdPremBf) {
    this.stdPremBf = stdPremBf;
  }

  /**
   * returns the value of the discntPremBf
   * 
   * @return the discntPremBf
   */
  public java.math.BigDecimal getDiscntPremBf() {
    return discntPremBf;
  }

  /**
   * sets the value of the discntPremBf
   * 
   * @param discntPremBf
   *          the discntPremBf
   */
  public void setDiscntPremBf(java.math.BigDecimal discntPremBf) {
    this.discntPremBf = discntPremBf;
  }

  /**
   * returns the value of the discntedPremBf
   * 
   * @return the discntedPremBf
   */
  public java.math.BigDecimal getDiscntedPremBf() {
    return discntedPremBf;
  }

  /**
   * sets the value of the discntedPremBf
   * 
   * @param discntedPremBf
   *          the discntedPremBf
   */
  public void setDiscntedPremBf(java.math.BigDecimal discntedPremBf) {
    this.discntedPremBf = discntedPremBf;
  }

  /**
   * returns the value of the policyFeeBf
   * 
   * @return the policyFeeBf
   */
  public java.math.BigDecimal getPolicyFeeBf() {
    return policyFeeBf;
  }

  /**
   * sets the value of the policyFeeBf
   * 
   * @param policyFeeBf
   *          the policyFeeBf
   */
  public void setPolicyFeeBf(java.math.BigDecimal policyFeeBf) {
    this.policyFeeBf = policyFeeBf;
  }

  /**
   * returns the value of the extraPremBf
   * 
   * @return the extraPremBf
   */
  public java.math.BigDecimal getExtraPremBf() {
    return extraPremBf;
  }

  /**
   * sets the value of the extraPremBf
   * 
   * @param extraPremBf
   *          the extraPremBf
   */
  public void setExtraPremBf(java.math.BigDecimal extraPremBf) {
    this.extraPremBf = extraPremBf;
  }

  /**
   * returns the value of the stdPremAf
   * 
   * @return the stdPremAf
   */
  public java.math.BigDecimal getStdPremAf() {
    return stdPremAf;
  }

  /**
   * sets the value of the stdPremAf
   * 
   * @param stdPremAf
   *          the stdPremAf
   */
  public void setStdPremAf(java.math.BigDecimal stdPremAf) {
    this.stdPremAf = stdPremAf;
  }

  /**
   * returns the value of the discntPremAf
   * 
   * @return the discntPremAf
   */
  public java.math.BigDecimal getDiscntPremAf() {
    return discntPremAf;
  }

  /**
   * sets the value of the discntPremAf
   * 
   * @param discntPremAf
   *          the discntPremAf
   */
  public void setDiscntPremAf(java.math.BigDecimal discntPremAf) {
    this.discntPremAf = discntPremAf;
  }

  /**
   * returns the value of the discntedPremAf
   * 
   * @return the discntedPremAf
   */
  public java.math.BigDecimal getDiscntedPremAf() {
    return discntedPremAf;
  }

  /**
   * sets the value of the discntedPremAf
   * 
   * @param discntedPremAf
   *          the discntedPremAf
   */
  public void setDiscntedPremAf(java.math.BigDecimal discntedPremAf) {
    this.discntedPremAf = discntedPremAf;
  }

  /**
   * returns the value of the policyFeeAf
   * 
   * @return the policyFeeAf
   */
  public java.math.BigDecimal getPolicyFeeAf() {
    return policyFeeAf;
  }

  /**
   * sets the value of the policyFeeAf
   * 
   * @param policyFeeAf
   *          the policyFeeAf
   */
  public void setPolicyFeeAf(java.math.BigDecimal policyFeeAf) {
    this.policyFeeAf = policyFeeAf;
  }

  /**
   * returns the value of the grossPremAf
   * 
   * @return the grossPremAf
   */
  public java.math.BigDecimal getGrossPremAf() {
    return grossPremAf;
  }

  /**
   * sets the value of the grossPremAf
   * 
   * @param grossPremAf
   *          the grossPremAf
   */
  public void setGrossPremAf(java.math.BigDecimal grossPremAf) {
    this.grossPremAf = grossPremAf;
  }

  /**
   * returns the value of the extraPremAf
   * 
   * @return the extraPremAf
   */
  public java.math.BigDecimal getExtraPremAf() {
    return extraPremAf;
  }

  /**
   * sets the value of the extraPremAf
   * 
   * @param extraPremAf
   *          the extraPremAf
   */
  public void setExtraPremAf(java.math.BigDecimal extraPremAf) {
    this.extraPremAf = extraPremAf;
  }

  /**
   * returns the value of the totalPremAf
   * 
   * @return the totalPremAf
   */
  public java.math.BigDecimal getTotalPremAf() {
    return totalPremAf;
  }

  /**
   * sets the value of the totalPremAf
   * 
   * @param totalPremAf
   *          the totalPremAf
   */
  public void setTotalPremAf(java.math.BigDecimal totalPremAf) {
    this.totalPremAf = totalPremAf;
  }

  /**
   * returns the value of the stdPremAn
   * 
   * @return the stdPremAn
   */
  public java.math.BigDecimal getStdPremAn() {
    return stdPremAn;
  }

  /**
   * sets the value of the stdPremAn
   * 
   * @param stdPremAn
   *          the stdPremAn
   */
  public void setStdPremAn(java.math.BigDecimal stdPremAn) {
    this.stdPremAn = stdPremAn;
  }

  /**
   * returns the value of the discntPremAn
   * 
   * @return the discntPremAn
   */
  public java.math.BigDecimal getDiscntPremAn() {
    return discntPremAn;
  }

  /**
   * sets the value of the discntPremAn
   * 
   * @param discntPremAn
   *          the discntPremAn
   */
  public void setDiscntPremAn(java.math.BigDecimal discntPremAn) {
    this.discntPremAn = discntPremAn;
  }

  /**
   * returns the value of the discntedPremAn
   * 
   * @return the discntedPremAn
   */
  public java.math.BigDecimal getDiscntedPremAn() {
    return discntedPremAn;
  }

  /**
   * sets the value of the discntedPremAn
   * 
   * @param discntedPremAn
   *          the discntedPremAn
   */
  public void setDiscntedPremAn(java.math.BigDecimal discntedPremAn) {
    this.discntedPremAn = discntedPremAn;
  }

  /**
   * returns the value of the policyFeeAn
   * 
   * @return the policyFeeAn
   */
  public java.math.BigDecimal getPolicyFeeAn() {
    return policyFeeAn;
  }

  /**
   * sets the value of the policyFeeAn
   * 
   * @param policyFeeAn
   *          the policyFeeAn
   */
  public void setPolicyFeeAn(java.math.BigDecimal policyFeeAn) {
    this.policyFeeAn = policyFeeAn;
  }

  /**
   * returns the value of the extraPremAn
   * 
   * @return the extraPremAn
   */
  public java.math.BigDecimal getExtraPremAn() {
    return extraPremAn;
  }

  /**
   * sets the value of the extraPremAn
   * 
   * @param extraPremAn
   *          the extraPremAn
   */
  public void setExtraPremAn(java.math.BigDecimal extraPremAn) {
    this.extraPremAn = extraPremAn;
  }

  /**
   * returns the value of the nextStdPremBf
   * 
   * @return the nextStdPremBf
   */
  public java.math.BigDecimal getNextStdPremBf() {
    return nextStdPremBf;
  }

  /**
   * sets the value of the nextStdPremBf
   * 
   * @param nextStdPremBf
   *          the nextStdPremBf
   */
  public void setNextStdPremBf(java.math.BigDecimal nextStdPremBf) {
    this.nextStdPremBf = nextStdPremBf;
  }

  /**
   * returns the value of the nextDiscntPremBf
   * 
   * @return the nextDiscntPremBf
   */
  public java.math.BigDecimal getNextDiscntPremBf() {
    return nextDiscntPremBf;
  }

  /**
   * sets the value of the nextDiscntPremBf
   * 
   * @param nextDiscntPremBf
   *          the nextDiscntPremBf
   */
  public void setNextDiscntPremBf(java.math.BigDecimal nextDiscntPremBf) {
    this.nextDiscntPremBf = nextDiscntPremBf;
  }

  /**
   * returns the value of the nextDiscntedPremBf
   * 
   * @return the nextDiscntedPremBf
   */
  public java.math.BigDecimal getNextDiscntedPremBf() {
    return nextDiscntedPremBf;
  }

  /**
   * sets the value of the nextDiscntedPremBf
   * 
   * @param nextDiscntedPremBf
   *          the nextDiscntedPremBf
   */
  public void setNextDiscntedPremBf(java.math.BigDecimal nextDiscntedPremBf) {
    this.nextDiscntedPremBf = nextDiscntedPremBf;
  }

  /**
   * returns the value of the nextPolicyFeeBf
   * 
   * @return the nextPolicyFeeBf
   */
  public java.math.BigDecimal getNextPolicyFeeBf() {
    return nextPolicyFeeBf;
  }

  /**
   * sets the value of the nextPolicyFeeBf
   * 
   * @param nextPolicyFeeBf
   *          the nextPolicyFeeBf
   */
  public void setNextPolicyFeeBf(java.math.BigDecimal nextPolicyFeeBf) {
    this.nextPolicyFeeBf = nextPolicyFeeBf;
  }

  /**
   * returns the value of the nextExtraPremBf
   * 
   * @return the nextExtraPremBf
   */
  public java.math.BigDecimal getNextExtraPremBf() {
    return nextExtraPremBf;
  }

  /**
   * sets the value of the nextExtraPremBf
   * 
   * @param nextExtraPremBf
   *          the nextExtraPremBf
   */
  public void setNextExtraPremBf(java.math.BigDecimal nextExtraPremBf) {
    this.nextExtraPremBf = nextExtraPremBf;
  }

  /**
   * returns the value of the nextStdPremAf
   * 
   * @return the nextStdPremAf
   */
  public java.math.BigDecimal getNextStdPremAf() {
    return nextStdPremAf;
  }

  /**
   * sets the value of the nextStdPremAf
   * 
   * @param nextStdPremAf
   *          the nextStdPremAf
   */
  public void setNextStdPremAf(java.math.BigDecimal nextStdPremAf) {
    this.nextStdPremAf = nextStdPremAf;
  }

  /**
   * returns the value of the nextDiscntPremAf
   * 
   * @return the nextDiscntPremAf
   */
  public java.math.BigDecimal getNextDiscntPremAf() {
    return nextDiscntPremAf;
  }

  /**
   * sets the value of the nextDiscntPremAf
   * 
   * @param nextDiscntPremAf
   *          the nextDiscntPremAf
   */
  public void setNextDiscntPremAf(java.math.BigDecimal nextDiscntPremAf) {
    this.nextDiscntPremAf = nextDiscntPremAf;
  }

  /**
   * returns the value of the nextDiscntedPremAf
   * 
   * @return the nextDiscntedPremAf
   */
  public java.math.BigDecimal getNextDiscntedPremAf() {
    return nextDiscntedPremAf;
  }

  /**
   * sets the value of the nextDiscntedPremAf
   * 
   * @param nextDiscntedPremAf
   *          the nextDiscntedPremAf
   */
  public void setNextDiscntedPremAf(java.math.BigDecimal nextDiscntedPremAf) {
    this.nextDiscntedPremAf = nextDiscntedPremAf;
  }

  /**
   * returns the value of the nextPolicyFeeAf
   * 
   * @return the nextPolicyFeeAf
   */
  public java.math.BigDecimal getNextPolicyFeeAf() {
    return nextPolicyFeeAf;
  }

  /**
   * sets the value of the nextPolicyFeeAf
   * 
   * @param nextPolicyFeeAf
   *          the nextPolicyFeeAf
   */
  public void setNextPolicyFeeAf(java.math.BigDecimal nextPolicyFeeAf) {
    this.nextPolicyFeeAf = nextPolicyFeeAf;
  }

  /**
   * returns the value of the nextGrossPremAf
   * 
   * @return the nextGrossPremAf
   */
  public java.math.BigDecimal getNextGrossPremAf() {
    return nextGrossPremAf;
  }

  /**
   * sets the value of the nextGrossPremAf
   * 
   * @param nextGrossPremAf
   *          the nextGrossPremAf
   */
  public void setNextGrossPremAf(java.math.BigDecimal nextGrossPremAf) {
    this.nextGrossPremAf = nextGrossPremAf;
  }

  /**
   * returns the value of the nextExtraPremAf
   * 
   * @return the nextExtraPremAf
   */
  public java.math.BigDecimal getNextExtraPremAf() {
    return nextExtraPremAf;
  }

  /**
   * sets the value of the nextExtraPremAf
   * 
   * @param nextExtraPremAf
   *          the nextExtraPremAf
   */
  public void setNextExtraPremAf(java.math.BigDecimal nextExtraPremAf) {
    this.nextExtraPremAf = nextExtraPremAf;
  }

  /**
   * returns the value of the nextTotalPremAf
   * 
   * @return the nextTotalPremAf
   */
  public java.math.BigDecimal getNextTotalPremAf() {
    return nextTotalPremAf;
  }

  /**
   * sets the value of the nextTotalPremAf
   * 
   * @param nextTotalPremAf
   *          the nextTotalPremAf
   */
  public void setNextTotalPremAf(java.math.BigDecimal nextTotalPremAf) {
    this.nextTotalPremAf = nextTotalPremAf;
  }

  /**
   * returns the value of the nextStdPremAn
   * 
   * @return the nextStdPremAn
   */
  public java.math.BigDecimal getNextStdPremAn() {
    return nextStdPremAn;
  }

  /**
   * sets the value of the nextStdPremAn
   * 
   * @param nextStdPremAn
   *          the nextStdPremAn
   */
  public void setNextStdPremAn(java.math.BigDecimal nextStdPremAn) {
    this.nextStdPremAn = nextStdPremAn;
  }

  /**
   * returns the value of the nextDiscntPremAn
   * 
   * @return the nextDiscntPremAn
   */
  public java.math.BigDecimal getNextDiscntPremAn() {
    return nextDiscntPremAn;
  }

  /**
   * sets the value of the nextDiscntPremAn
   * 
   * @param nextDiscntPremAn
   *          the nextDiscntPremAn
   */
  public void setNextDiscntPremAn(java.math.BigDecimal nextDiscntPremAn) {
    this.nextDiscntPremAn = nextDiscntPremAn;
  }

  /**
   * returns the value of the nextDiscntedPremAn
   * 
   * @return the nextDiscntedPremAn
   */
  public java.math.BigDecimal getNextDiscntedPremAn() {
    return nextDiscntedPremAn;
  }

  /**
   * sets the value of the nextDiscntedPremAn
   * 
   * @param nextDiscntedPremAn
   *          the nextDiscntedPremAn
   */
  public void setNextDiscntedPremAn(java.math.BigDecimal nextDiscntedPremAn) {
    this.nextDiscntedPremAn = nextDiscntedPremAn;
  }

  /**
   * returns the value of the nextPolicyFeeAn
   * 
   * @return the nextPolicyFeeAn
   */
  public java.math.BigDecimal getNextPolicyFeeAn() {
    return nextPolicyFeeAn;
  }

  /**
   * sets the value of the nextPolicyFeeAn
   * 
   * @param nextPolicyFeeAn
   *          the nextPolicyFeeAn
   */
  public void setNextPolicyFeeAn(java.math.BigDecimal nextPolicyFeeAn) {
    this.nextPolicyFeeAn = nextPolicyFeeAn;
  }

  /**
   * returns the value of the nextExtraPremAn
   * 
   * @return the nextExtraPremAn
   */
  public java.math.BigDecimal getNextExtraPremAn() {
    return nextExtraPremAn;
  }

  /**
   * sets the value of the nextExtraPremAn
   * 
   * @param nextExtraPremAn
   *          the nextExtraPremAn
   */
  public void setNextExtraPremAn(java.math.BigDecimal nextExtraPremAn) {
    this.nextExtraPremAn = nextExtraPremAn;
  }

  /**
   * returns the value of the gibStatus
   * 
   * @return the gibStatus
   */
  public java.lang.Integer getGibStatus() {
    return gibStatus;
  }

  /**
   * sets the value of the gibStatus
   * 
   * @param gibStatus
   *          the gibStatus
   */
  public void setGibStatus(java.lang.Integer gibStatus) {
    this.gibStatus = gibStatus;
  }

  /**
   * returns the value of the amount
   * 
   * @return the amount
   */
  public java.math.BigDecimal getAmount() {
    return amount;
  }

  /**
   * sets the value of the amount
   * 
   * @param amount
   *          the amount
   */
  public void setAmount(java.math.BigDecimal amount) {
    this.amount = amount;
  }

  /**
   * returns the value of the reducedAmount
   * 
   * @return the reducedAmount
   */
  public java.math.BigDecimal getReducedAmount() {
    return reducedAmount;
  }

  /**
   * sets the value of the reducedAmount
   * 
   * @param reducedAmount
   *          the reducedAmount
   */
  public void setReducedAmount(java.math.BigDecimal reducedAmount) {
    this.reducedAmount = reducedAmount;
  }

  /**
   * returns the value of the unit
   * 
   * @return the unit
   */
  public java.math.BigDecimal getUnit() {
    return unit;
  }

  /**
   * sets the value of the unit
   * 
   * @param unit
   *          the unit
   */
  public void setUnit(java.math.BigDecimal unit) {
    this.unit = unit;
  }

  /**
   * returns the value of the reducedUnit
   * 
   * @return the reducedUnit
   */
  public java.math.BigDecimal getReducedUnit() {
    return reducedUnit;
  }

  /**
   * sets the value of the reducedUnit
   * 
   * @param reducedUnit
   *          the reducedUnit
   */
  public void setReducedUnit(java.math.BigDecimal reducedUnit) {
    this.reducedUnit = reducedUnit;
  }

  /**
   * returns the value of the uwChange
   * 
   * @return the uwChange
   */
  public java.lang.Integer getUwChange() {
    return uwChange;
  }

  /**
   * sets the value of the uwChange
   * 
   * @param uwChange
   *          the uwChange
   */
  public void setUwChange(java.lang.Integer uwChange) {
    this.uwChange = uwChange;
  }

  /**
   * returns the value of the renewDecision
   * 
   * @return the renewDecision
   */
  public java.lang.String getRenewDecision() {
    return renewDecision;
  }

  /**
   * sets the value of the renewDecision
   * 
   * @param renewDecision
   *          the renewDecision
   */
  public void setRenewDecision(java.lang.String renewDecision) {
    this.renewDecision = renewDecision;
  }

  /**
   * returns the value of the standLife1
   * 
   * @return the standLife1
   */
  public java.lang.String getStandLife1() {
    return standLife1;
  }

  /**
   * sets the value of the standLife1
   * 
   * @param standLife1
   *          the standLife1
   */
  public void setStandLife1(java.lang.String standLife1) {
    this.standLife1 = standLife1;
  }

  /**
   * returns the value of the finalEm1
   * 
   * @return the finalEm1
   */
  public java.lang.Integer getFinalEm1() {
    return finalEm1;
  }

  /**
   * sets the value of the finalEm1
   * 
   * @param finalEm1
   *          the finalEm1
   */
  public void setFinalEm1(java.lang.Integer finalEm1) {
    this.finalEm1 = finalEm1;
  }

  /**
   * returns the value of the emHealth1
   * 
   * @return the emHealth1
   */
  public java.lang.Integer getEmHealth1() {
    return emHealth1;
  }

  /**
   * sets the value of the emHealth1
   * 
   * @param emHealth1
   *          the emHealth1
   */
  public void setEmHealth1(java.lang.Integer emHealth1) {
    this.emHealth1 = emHealth1;
  }

  /**
   * returns the value of the jobClass2
   * 
   * @return the jobClass2
   */
  public java.lang.Integer getJobClass2() {
    return jobClass2;
  }

  /**
   * sets the value of the jobClass2
   * 
   * @param jobClass2
   *          the jobClass2
   */
  public void setJobClass2(java.lang.Integer jobClass2) {
    this.jobClass2 = jobClass2;
  }

  /**
   * returns the value of the finalEm2
   * 
   * @return the finalEm2
   */
  public java.lang.Integer getFinalEm2() {
    return finalEm2;
  }

  /**
   * sets the value of the finalEm2
   * 
   * @param finalEm2
   *          the finalEm2
   */
  public void setFinalEm2(java.lang.Integer finalEm2) {
    this.finalEm2 = finalEm2;
  }

  /**
   * returns the value of the emHealth2
   * 
   * @return the emHealth2
   */
  public java.lang.Integer getEmHealth2() {
    return emHealth2;
  }

  /**
   * sets the value of the emHealth2
   * 
   * @param emHealth2
   *          the emHealth2
   */
  public void setEmHealth2(java.lang.Integer emHealth2) {
    this.emHealth2 = emHealth2;
  }

  /**
   * returns the value of the smokingRelated
   * 
   * @return the smokingRelated
   */
  public java.lang.String getSmokingRelated() {
    return smokingRelated;
  }

  /**
   * sets the value of the smokingRelated
   * 
   * @param smokingRelated
   *          the smokingRelated
   */
  public void setSmokingRelated(java.lang.String smokingRelated) {
    this.smokingRelated = smokingRelated;
  }

  /**
   * returns the value of the uwChargeYear
   * 
   * @return the uwChargeYear
   */
  public java.lang.Integer getUwChargeYear() {
    return uwChargeYear;
  }

  /**
   * sets the value of the uwChargeYear
   * 
   * @param uwChargeYear
   *          the uwChargeYear
   */
  public void setUwChargeYear(java.lang.Integer uwChargeYear) {
    this.uwChargeYear = uwChargeYear;
  }

  /**
   * returns the value of the uwCoverageYear
   * 
   * @return the uwCoverageYear
   */
  public java.lang.Integer getUwCoverageYear() {
    return uwCoverageYear;
  }

  /**
   * sets the value of the uwCoverageYear
   * 
   * @param uwCoverageYear
   *          the uwCoverageYear
   */
  public void setUwCoverageYear(java.lang.Integer uwCoverageYear) {
    this.uwCoverageYear = uwCoverageYear;
  }

  /**
   * returns the value of the decisionReason
   * 
   * @return the decisionReason
   */
  public java.lang.String getDecisionReason() {
    return decisionReason;
  }

  /**
   * sets the value of the decisionReason
   * 
   * @param decisionReason
   *          the decisionReason
   */
  public void setDecisionReason(java.lang.String decisionReason) {
    this.decisionReason = decisionReason;
  }

  /**
   * returns the value of the decisionReasonDesc
   * 
   * @return the decisionReasonDesc
   */
  public java.lang.String getDecisionReasonDesc() {
    return decisionReasonDesc;
  }

  /**
   * sets the value of the decisionReasonDesc
   * 
   * @param decisionReasonDesc
   *          the decisionReasonDesc
   */
  public void setDecisionReasonDesc(java.lang.String decisionReasonDesc) {
    this.decisionReasonDesc = decisionReasonDesc;
  }

  /**
   * returns the value of the uwChargePeriod
   * 
   * @return the uwChargePeriod
   */
  public java.lang.String getUwChargePeriod() {
    return uwChargePeriod;
  }

  /**
   * sets the value of the uwChargePeriod
   * 
   * @param uwChargePeriod
   *          the uwChargePeriod
   */
  public void setUwChargePeriod(java.lang.String uwChargePeriod) {
    this.uwChargePeriod = uwChargePeriod;
  }

  /**
   * returns the value of the uwCoveragePeriod
   * 
   * @return the uwCoveragePeriod
   */
  public java.lang.String getUwCoveragePeriod() {
    return uwCoveragePeriod;
  }

  /**
   * sets the value of the uwCoveragePeriod
   * 
   * @param uwCoveragePeriod
   *          the uwCoveragePeriod
   */
  public void setUwCoveragePeriod(java.lang.String uwCoveragePeriod) {
    this.uwCoveragePeriod = uwCoveragePeriod;
  }

  /**
   * returns the value of the entityFund
   * 
   * @return the entityFund
   */
  public java.lang.String getEntityFund() {
    return entityFund;
  }

  /**
   * sets the value of the entityFund
   * 
   * @param entityFund
   *          the entityFund
   */
  public void setEntityFund(java.lang.String entityFund) {
    this.entityFund = entityFund;
  }

  /**
   * returns the value of the gurntStartDate
   * 
   * @return the gurntStartDate
   */
  public java.util.Date getGurntStartDate() {
    return gurntStartDate;
  }

  /**
   * sets the value of the gurntStartDate
   * 
   * @param gurntStartDate
   *          the gurntStartDate
   */
  public void setGurntStartDate(java.util.Date gurntStartDate) {
    this.gurntStartDate = gurntStartDate;
  }

  /**
   * returns the value of the gurntPerdType
   * 
   * @return the gurntPerdType
   */
  public java.lang.String getGurntPerdType() {
    return gurntPerdType;
  }

  /**
   * sets the value of the gurntPerdType
   * 
   * @param gurntPerdType
   *          the gurntPerdType
   */
  public void setGurntPerdType(java.lang.String gurntPerdType) {
    this.gurntPerdType = gurntPerdType;
  }

  /**
   * returns the value of the gurntPeriod
   * 
   * @return the gurntPeriod
   */
  public java.lang.Integer getGurntPeriod() {
    return gurntPeriod;
  }

  /**
   * sets the value of the gurntPeriod
   * 
   * @param gurntPeriod
   *          the gurntPeriod
   */
  public void setGurntPeriod(java.lang.Integer gurntPeriod) {
    this.gurntPeriod = gurntPeriod;
  }

  /**
   * returns the value of the investHorizon
   * 
   * @return the investHorizon
   */
  public java.lang.Integer getInvestHorizon() {
    return investHorizon;
  }

  /**
   * sets the value of the investHorizon
   * 
   * @param investHorizon
   *          the investHorizon
   */
  public void setInvestHorizon(java.lang.Integer investHorizon) {
    this.investHorizon = investHorizon;
  }

  /**
   * returns the value of the manualSa
   * 
   * @return the manualSa
   */
  public java.lang.String getManualSa() {
    return manualSa;
  }

  /**
   * sets the value of the manualSa
   * 
   * @param manualSa
   *          the manualSa
   */
  public void setManualSa(java.lang.String manualSa) {
    this.manualSa = manualSa;
  }

  /**
   * returns the value of the deferPeriod
   * 
   * @return the deferPeriod
   */
  public java.lang.Integer getDeferPeriod() {
    return deferPeriod;
  }

  /**
   * sets the value of the deferPeriod
   * 
   * @param deferPeriod
   *          the deferPeriod
   */
  public void setDeferPeriod(java.lang.Integer deferPeriod) {
    this.deferPeriod = deferPeriod;
  }

  /**
   * returns the value of the gibRejCode
   * 
   * @return the gibRejCode
   */
  public java.lang.Integer getGibRejCode() {
    return gibRejCode;
  }

  /**
   * sets the value of the gibRejCode
   * 
   * @param gibRejCode
   *          the gibRejCode
   */
  public void setGibRejCode(java.lang.Integer gibRejCode) {
    this.gibRejCode = gibRejCode;
  }

  /**
   * returns the value of the holidayIndi
   * 
   * @return the holidayIndi
   */
  public java.lang.String getHolidayIndi() {
    return holidayIndi;
  }

  /**
   * sets the value of the holidayIndi
   * 
   * @param holidayIndi
   *          the holidayIndi
   */
  public void setHolidayIndi(java.lang.String holidayIndi) {
    this.holidayIndi = holidayIndi;
  }

  /**
   * returns the value of the industryId1
   * 
   * @return the industryId1
   */
  public java.lang.Integer getIndustryId1() {
    return industryId1;
  }

  /**
   * sets the value of the industryId1
   * 
   * @param industryId1
   *          the industryId1
   */
  public void setIndustryId1(java.lang.Integer industryId1) {
    this.industryId1 = industryId1;
  }

  /**
   * returns the value of the industryId2
   * 
   * @return the industryId2
   */
  public java.lang.Integer getIndustryId2() {
    return industryId2;
  }

  /**
   * sets the value of the industryId2
   * 
   * @param industryId2
   *          the industryId2
   */
  public void setIndustryId2(java.lang.Integer industryId2) {
    this.industryId2 = industryId2;
  }

  /**
   * returns the value of the standLife2
   * 
   * @return the standLife2
   */
  public java.lang.String getStandLife2() {
    return standLife2;
  }

  /**
   * sets the value of the standLife2
   * 
   * @param standLife2
   *          the standLife2
   */
  public void setStandLife2(java.lang.String standLife2) {
    this.standLife2 = standLife2;
  }

  /**
   * returns the value of the emValue1
   * 
   * @return the emValue1
   */
  public java.lang.Integer getEmValue1() {
    return emValue1;
  }

  /**
   * sets the value of the emValue1
   * 
   * @param emValue1
   *          the emValue1
   */
  public void setEmValue1(java.lang.Integer emValue1) {
    this.emValue1 = emValue1;
  }

  /**
   * returns the value of the emValue2
   * 
   * @return the emValue2
   */
  public java.lang.Integer getEmValue2() {
    return emValue2;
  }

  /**
   * sets the value of the emValue2
   * 
   * @param emValue2
   *          the emValue2
   */
  public void setEmValue2(java.lang.Integer emValue2) {
    this.emValue2 = emValue2;
  }

  /**
   * returns the value of the fullDeclare
   * 
   * @return the fullDeclare
   */
  public java.lang.String getFullDeclare() {
    return fullDeclare;
  }

  /**
   * sets the value of the fullDeclare
   * 
   * @param fullDeclare
   *          the fullDeclare
   */
  public void setFullDeclare(java.lang.String fullDeclare) {
    this.fullDeclare = fullDeclare;
  }

  /**
   * returns the value of the hthFullDeclare
   * 
   * @return the hthFullDeclare
   */
  public java.lang.String getHthFullDeclare() {
    return hthFullDeclare;
  }

  /**
   * sets the value of the hthFullDeclare
   * 
   * @param hthFullDeclare
   *          the hthFullDeclare
   */
  public void setHthFullDeclare(java.lang.String hthFullDeclare) {
    this.hthFullDeclare = hthFullDeclare;
  }

  /**
   * returns the value of the surrValueIndi
   * 
   * @return the surrValueIndi
   */
  public java.lang.String getSurrValueIndi() {
    return surrValueIndi;
  }

  /**
   * sets the value of the surrValueIndi
   * 
   * @param surrValueIndi
   *          the surrValueIndi
   */
  public void setSurrValueIndi(java.lang.String surrValueIndi) {
    this.surrValueIndi = surrValueIndi;
  }

  /**
   * returns the value of the waitPeriod
   * 
   * @return the waitPeriod
   */
  public java.lang.Integer getWaitPeriod() {
    return waitPeriod;
  }

  /**
   * sets the value of the waitPeriod
   * 
   * @param waitPeriod
   *          the waitPeriod
   */
  public void setWaitPeriod(java.lang.Integer waitPeriod) {
    this.waitPeriod = waitPeriod;
  }

  /**
   * returns the value of the dividendOptTerm
   * 
   * @return the dividendOptTerm
   */
  public java.lang.Integer getDividendOptTerm() {
    return dividendOptTerm;
  }

  /**
   * sets the value of the dividendOptTerm
   * 
   * @param dividendOptTerm
   *          the dividendOptTerm
   */
  public void setDividendOptTerm(java.lang.Integer dividendOptTerm) {
    this.dividendOptTerm = dividendOptTerm;
  }

  /**
   * returns the value of the survivalOptTerm
   * 
   * @return the survivalOptTerm
   */
  public java.lang.Integer getSurvivalOptTerm() {
    return survivalOptTerm;
  }

  /**
   * sets the value of the survivalOptTerm
   * 
   * @param survivalOptTerm
   *          the survivalOptTerm
   */
  public void setSurvivalOptTerm(java.lang.Integer survivalOptTerm) {
    this.survivalOptTerm = survivalOptTerm;
  }

  /**
   * returns the value of the maturityOptTerm
   * 
   * @return the maturityOptTerm
   */
  public java.lang.Integer getMaturityOptTerm() {
    return maturityOptTerm;
  }

  /**
   * sets the value of the maturityOptTerm
   * 
   * @param maturityOptTerm
   *          the maturityOptTerm
   */
  public void setMaturityOptTerm(java.lang.Integer maturityOptTerm) {
    this.maturityOptTerm = maturityOptTerm;
  }

  /**
   * returns the value of the maturityOption
   * 
   * @return the maturityOption
   */
  public java.lang.String getMaturityOption() {
    return maturityOption;
  }

  /**
   * sets the value of the maturityOption
   * 
   * @param maturityOption
   *          the maturityOption
   */
  public void setMaturityOption(java.lang.String maturityOption) {
    this.maturityOption = maturityOption;
  }

  /**
   * returns the value of the waivAnulBenefit
   * 
   * @return the waivAnulBenefit
   */
  public java.math.BigDecimal getWaivAnulBenefit() {
    return waivAnulBenefit;
  }

  /**
   * sets the value of the waivAnulBenefit
   * 
   * @param waivAnulBenefit
   *          the waivAnulBenefit
   */
  public void setWaivAnulBenefit(java.math.BigDecimal waivAnulBenefit) {
    this.waivAnulBenefit = waivAnulBenefit;
  }

  /**
   * returns the value of the waivAnulPrem
   * 
   * @return the waivAnulPrem
   */
  public java.math.BigDecimal getWaivAnulPrem() {
    return waivAnulPrem;
  }

  /**
   * sets the value of the waivAnulPrem
   * 
   * @param waivAnulPrem
   *          the waivAnulPrem
   */
  public void setWaivAnulPrem(java.math.BigDecimal waivAnulPrem) {
    this.waivAnulPrem = waivAnulPrem;
  }

  /**
   * returns the value of the suspendChgId
   * 
   * @return the suspendChgId
   */
  public java.lang.Long getSuspendChgId() {
    return suspendChgId;
  }

  /**
   * sets the value of the suspendChgId
   * 
   * @param suspendChgId
   *          the suspendChgId
   */
  public void setSuspendChgId(java.lang.Long suspendChgId) {
    this.suspendChgId = suspendChgId;
  }

  /**
   * returns the value of the lapseDate
   * 
   * @return the lapseDate
   */
  public java.util.Date getLapseDate() {
    return lapseDate;
  }

  /**
   * sets the value of the lapseDate
   * 
   * @param lapseDate
   *          the lapseDate
   */
  public void setLapseDate(java.util.Date lapseDate) {
    this.lapseDate = lapseDate;
  }

  /**
   * returns the value of the lapseCause
   * 
   * @return the lapseCause
   */
  public java.lang.Integer getLapseCause() {
    return lapseCause;
  }

  /**
   * sets the value of the lapseCause
   * 
   * @param lapseCause
   *          the lapseCause
   */
  public void setLapseCause(java.lang.Integer lapseCause) {
    this.lapseCause = lapseCause;
  }

  /**
   * returns the value of the mortgageStatus
   * 
   * @return the mortgageStatus
   */
  public java.lang.String getMortgageStatus() {
    return mortgageStatus;
  }

  /**
   * sets the value of the mortgageStatus
   * 
   * @param mortgageStatus
   *          the mortgageStatus
   */
  public void setMortgageStatus(java.lang.String mortgageStatus) {
    this.mortgageStatus = mortgageStatus;
  }

  /**
   * returns the value of the mortgageRate
   * 
   * @return the mortgageRate
   */
  public java.math.BigDecimal getMortgageRate() {
    return mortgageRate;
  }

  /**
   * sets the value of the mortgageRate
   * 
   * @param mortgageRate
   *          the mortgageRate
   */
  public void setMortgageRate(java.math.BigDecimal mortgageRate) {
    this.mortgageRate = mortgageRate;
  }

  /**
   * returns the value of the birthday2
   * 
   * @return the birthday2
   */
  public java.util.Date getBirthday2() {
    return birthday2;
  }

  /**
   * sets the value of the birthday2
   * 
   * @param birthday2
   *          the birthday2
   */
  public void setBirthday2(java.util.Date birthday2) {
    this.birthday2 = birthday2;
  }

  /**
   * returns the value of the bonusDueDate
   * 
   * @return the bonusDueDate
   */
  public java.util.Date getBonusDueDate() {
    return bonusDueDate;
  }

  /**
   * sets the value of the bonusDueDate
   * 
   * @param bonusDueDate
   *          the bonusDueDate
   */
  public void setBonusDueDate(java.util.Date bonusDueDate) {
    this.bonusDueDate = bonusDueDate;
  }

  /**
   * returns the value of the premChangeTime
   * 
   * @return the premChangeTime
   */
  public java.util.Date getPremChangeTime() {
    return premChangeTime;
  }

  /**
   * sets the value of the premChangeTime
   * 
   * @param premChangeTime
   *          the premChangeTime
   */
  public void setPremChangeTime(java.util.Date premChangeTime) {
    this.premChangeTime = premChangeTime;
  }

  /**
   * returns the value of the submissionDate
   * 
   * @return the submissionDate
   */
  public java.util.Date getSubmissionDate() {
    return submissionDate;
  }

  /**
   * sets the value of the submissionDate
   * 
   * @param submissionDate
   *          the submissionDate
   */
  public void setSubmissionDate(java.util.Date submissionDate) {
    this.submissionDate = submissionDate;
  }

  /**
   * returns the value of the ilpCalcSa
   * 
   * @return the ilpCalcSa
   */
  public java.math.BigDecimal getIlpCalcSa() {
    return ilpCalcSa;
  }

  /**
   * sets the value of the ilpCalcSa
   * 
   * @param ilpCalcSa
   *          the ilpCalcSa
   */
  public void setIlpCalcSa(java.math.BigDecimal ilpCalcSa) {
    this.ilpCalcSa = ilpCalcSa;
  }

  /**
   * returns the value of the insuredStatus2
   * 
   * @return the insuredStatus2
   */
  public java.lang.String getInsuredStatus2() {
    return insuredStatus2;
  }

  /**
   * sets the value of the insuredStatus2
   * 
   * @param insuredStatus2
   *          the insuredStatus2
   */
  public void setInsuredStatus2(java.lang.String insuredStatus2) {
    this.insuredStatus2 = insuredStatus2;
  }

  /**
   * returns the value of the masterId
   * 
   * @return the masterId
   */
  public java.lang.Long getMasterId() {
    return masterId;
  }

  /**
   * sets the value of the masterId
   * 
   * @param masterId
   *          the masterId
   */
  public void setMasterId(java.lang.Long masterId) {
    this.masterId = masterId;
  }

  /**
   * returns the value of the productId
   * 
   * @return the productId
   */
  public java.lang.Integer getProductId() {
    return productId;
  }

  /**
   * sets the value of the productId
   * 
   * @param productId
   *          the productId
   */
  public void setProductId(java.lang.Integer productId) {
    this.productId = productId;
  }

  /**
   * returns the value of the dividendChoice
   * 
   * @return the dividendChoice
   */
  public java.lang.Integer getDividendChoice() {
    return dividendChoice;
  }

  /**
   * sets the value of the dividendChoice
   * 
   * @param dividendChoice
   *          the dividendChoice
   */
  public void setDividendChoice(java.lang.Integer dividendChoice) {
    this.dividendChoice = dividendChoice;
  }

  /**
   * returns the value of the applyDate
   * 
   * @return the applyDate
   */
  public java.util.Date getApplyDate() {
    return applyDate;
  }

  /**
   * sets the value of the applyDate
   * 
   * @param applyDate
   *          the applyDate
   */
  public void setApplyDate(java.util.Date applyDate) {
    this.applyDate = applyDate;
  }

  /**
   * returns the value of the endDate
   * 
   * @return the endDate
   */
  public java.util.Date getEndDate() {
    return endDate;
  }

  /**
   * sets the value of the endDate
   * 
   * @param endDate
   *          the endDate
   */
  public void setEndDate(java.util.Date endDate) {
    this.endDate = endDate;
  }

  /**
   * returns the value of the paidupDate
   * 
   * @return the paidupDate
   */
  public java.util.Date getPaidupDate() {
    return paidupDate;
  }

  /**
   * sets the value of the paidupDate
   * 
   * @param paidupDate
   *          the paidupDate
   */
  public void setPaidupDate(java.util.Date paidupDate) {
    this.paidupDate = paidupDate;
  }

  /**
   * returns the value of the liabilityState
   * 
   * @return the liabilityState
   */
  public java.lang.Integer getLiabilityState() {
    return liabilityState;
  }

  /**
   * sets the value of the liabilityState
   * 
   * @param liabilityState
   *          the liabilityState
   */
  public void setLiabilityState(java.lang.Integer liabilityState) {
    this.liabilityState = liabilityState;
  }

  /**
   * returns the value of the endCause
   * 
   * @return the endCause
   */
  public java.lang.Integer getEndCause() {
    return endCause;
  }

  /**
   * sets the value of the endCause
   * 
   * @param endCause
   *          the endCause
   */
  public void setEndCause(java.lang.Integer endCause) {
    this.endCause = endCause;
  }

  /**
   * returns the value of the insured1
   * 
   * @return the insured1
   */
  public java.lang.Long getInsured1() {
    return insured1;
  }

  /**
   * sets the value of the insured1
   * 
   * @param insured1
   *          the insured1
   */
  public void setInsured1(java.lang.Long insured1) {
    this.insured1 = insured1;
  }

  /**
   * returns the value of the age1
   * 
   * @return the age1
   */
  public java.lang.Integer getAge1() {
    return age1;
  }

  /**
   * sets the value of the age1
   * 
   * @param age1
   *          the age1
   */
  public void setAge1(java.lang.Integer age1) {
    this.age1 = age1;
  }

  /**
   * returns the value of the relation1
   * 
   * @return the relation1
   */
  public java.lang.Integer getRelation1() {
    return relation1;
  }

  /**
   * sets the value of the relation1
   * 
   * @param relation1
   *          the relation1
   */
  public void setRelation1(java.lang.Integer relation1) {
    this.relation1 = relation1;
  }

  /**
   * returns the value of the job11
   * 
   * @return the job11
   */
  public java.lang.String getJob11() {
    return job11;
  }

  /**
   * sets the value of the job11
   * 
   * @param job11
   *          the job11
   */
  public void setJob11(java.lang.String job11) {
    this.job11 = job11;
  }

  /**
   * returns the value of the job12
   * 
   * @return the job12
   */
  public java.lang.String getJob12() {
    return job12;
  }

  /**
   * sets the value of the job12
   * 
   * @param job12
   *          the job12
   */
  public void setJob12(java.lang.String job12) {
    this.job12 = job12;
  }

  /**
   * returns the value of the insured2
   * 
   * @return the insured2
   */
  public java.lang.Long getInsured2() {
    return insured2;
  }

  /**
   * sets the value of the insured2
   * 
   * @param insured2
   *          the insured2
   */
  public void setInsured2(java.lang.Long insured2) {
    this.insured2 = insured2;
  }

  /**
   * returns the value of the age2
   * 
   * @return the age2
   */
  public java.lang.Integer getAge2() {
    return age2;
  }

  /**
   * sets the value of the age2
   * 
   * @param age2
   *          the age2
   */
  public void setAge2(java.lang.Integer age2) {
    this.age2 = age2;
  }

  /**
   * returns the value of the relation2
   * 
   * @return the relation2
   */
  public java.lang.Integer getRelation2() {
    return relation2;
  }

  /**
   * sets the value of the relation2
   * 
   * @param relation2
   *          the relation2
   */
  public void setRelation2(java.lang.Integer relation2) {
    this.relation2 = relation2;
  }

  /**
   * returns the value of the job21
   * 
   * @return the job21
   */
  public java.lang.String getJob21() {
    return job21;
  }

  /**
   * sets the value of the job21
   * 
   * @param job21
   *          the job21
   */
  public void setJob21(java.lang.String job21) {
    this.job21 = job21;
  }

  /**
   * returns the value of the job22
   * 
   * @return the job22
   */
  public java.lang.String getJob22() {
    return job22;
  }

  /**
   * sets the value of the job22
   * 
   * @param job22
   *          the job22
   */
  public void setJob22(java.lang.String job22) {
    this.job22 = job22;
  }

  /**
   * returns the value of the payPeriod
   * 
   * @return the payPeriod
   */
  public java.lang.String getPayPeriod() {
    return payPeriod;
  }

  /**
   * sets the value of the payPeriod
   * 
   * @param payPeriod
   *          the payPeriod
   */
  public void setPayPeriod(java.lang.String payPeriod) {
    this.payPeriod = payPeriod;
  }

  /**
   * returns the value of the payYear
   * 
   * @return the payYear
   */
  public java.lang.Integer getPayYear() {
    return payYear;
  }

  /**
   * sets the value of the payYear
   * 
   * @param payYear
   *          the payYear
   */
  public void setPayYear(java.lang.Integer payYear) {
    this.payYear = payYear;
  }

  /**
   * returns the value of the endPeriod
   * 
   * @return the endPeriod
   */
  public java.lang.String getEndPeriod() {
    return endPeriod;
  }

  /**
   * sets the value of the endPeriod
   * 
   * @param endPeriod
   *          the endPeriod
   */
  public void setEndPeriod(java.lang.String endPeriod) {
    this.endPeriod = endPeriod;
  }

  /**
   * returns the value of the endYear
   * 
   * @return the endYear
   */
  public java.lang.Integer getEndYear() {
    return endYear;
  }

  /**
   * sets the value of the endYear
   * 
   * @param endYear
   *          the endYear
   */
  public void setEndYear(java.lang.Integer endYear) {
    this.endYear = endYear;
  }

  /**
   * returns the value of the payEnsure
   * 
   * @return the payEnsure
   */
  public java.lang.Integer getPayEnsure() {
    return payEnsure;
  }

  /**
   * sets the value of the payEnsure
   * 
   * @param payEnsure
   *          the payEnsure
   */
  public void setPayEnsure(java.lang.Integer payEnsure) {
    this.payEnsure = payEnsure;
  }

  /**
   * returns the value of the payType
   * 
   * @return the payType
   */
  public java.lang.String getPayType() {
    return payType;
  }

  /**
   * sets the value of the payType
   * 
   * @param payType
   *          the payType
   */
  public void setPayType(java.lang.String payType) {
    this.payType = payType;
  }

  /**
   * returns the value of the shortEndTime
   * 
   * @return the shortEndTime
   */
  public java.util.Date getShortEndTime() {
    return shortEndTime;
  }

  /**
   * sets the value of the shortEndTime
   * 
   * @param shortEndTime
   *          the shortEndTime
   */
  public void setShortEndTime(java.util.Date shortEndTime) {
    this.shortEndTime = shortEndTime;
  }

  /**
   * returns the value of the housekeeper
   * 
   * @return the housekeeper
   */
  public java.lang.String getHousekeeper() {
    return housekeeper;
  }

  /**
   * sets the value of the housekeeper
   * 
   * @param housekeeper
   *          the housekeeper
   */
  public void setHousekeeper(java.lang.String housekeeper) {
    this.housekeeper = housekeeper;
  }

  /**
   * returns the value of the payRate
   * 
   * @return the payRate
   */
  public java.math.BigDecimal getPayRate() {
    return payRate;
  }

  /**
   * sets the value of the payRate
   * 
   * @param payRate
   *          the payRate
   */
  public void setPayRate(java.math.BigDecimal payRate) {
    this.payRate = payRate;
  }

  /**
   * returns the value of the insuredCategory
   * 
   * @return the insuredCategory
   */
  public java.lang.String getInsuredCategory() {
    return insuredCategory;
  }

  /**
   * sets the value of the insuredCategory
   * 
   * @param insuredCategory
   *          the insuredCategory
   */
  public void setInsuredCategory(java.lang.String insuredCategory) {
    this.insuredCategory = insuredCategory;
  }

  /**
   * returns the value of the insuredAmount
   * 
   * @return the insuredAmount
   */
  public java.lang.Long getInsuredAmount() {
    return insuredAmount;
  }

  /**
   * sets the value of the insuredAmount
   * 
   * @param insuredAmount
   *          the insuredAmount
   */
  public void setInsuredAmount(java.lang.Long insuredAmount) {
    this.insuredAmount = insuredAmount;
  }

  /**
   * returns the value of the startPay
   * 
   * @return the startPay
   */
  public java.lang.String getStartPay() {
    return startPay;
  }

  /**
   * sets the value of the startPay
   * 
   * @param startPay
   *          the startPay
   */
  public void setStartPay(java.lang.String startPay) {
    this.startPay = startPay;
  }

  /**
   * returns the value of the retiredRate
   * 
   * @return the retiredRate
   */
  public java.math.BigDecimal getRetiredRate() {
    return retiredRate;
  }

  /**
   * sets the value of the retiredRate
   * 
   * @param retiredRate
   *          the retiredRate
   */
  public void setRetiredRate(java.math.BigDecimal retiredRate) {
    this.retiredRate = retiredRate;
  }

  /**
   * returns the value of the gender1
   * 
   * @return the gender1
   */
  public java.lang.String getGender1() {
    return gender1;
  }

  /**
   * sets the value of the gender1
   * 
   * @param gender1
   *          the gender1
   */
  public void setGender1(java.lang.String gender1) {
    this.gender1 = gender1;
  }

  /**
   * returns the value of the gender2
   * 
   * @return the gender2
   */
  public java.lang.String getGender2() {
    return gender2;
  }

  /**
   * sets the value of the gender2
   * 
   * @param gender2
   *          the gender2
   */
  public void setGender2(java.lang.String gender2) {
    this.gender2 = gender2;
  }

  /**
   * returns the value of the updateTime
   * 
   * @return the updateTime
   */
  public java.util.Date getUpdateTime() {
    return updateTime;
  }

  /**
   * sets the value of the updateTime
   * 
   * @param updateTime
   *          the updateTime
   */
  public void setUpdateTime(java.util.Date updateTime) {
    this.updateTime = updateTime;
  }

  /**
   * returns the value of the suspend
   * 
   * @return the suspend
   */
  public java.lang.String getSuspend() {
    return suspend;
  }

  /**
   * sets the value of the suspend
   * 
   * @param suspend
   *          the suspend
   */
  public void setSuspend(java.lang.String suspend) {
    this.suspend = suspend;
  }

  /**
   * returns the value of the suspendCause
   * 
   * @return the suspendCause
   */
  public java.lang.Integer getSuspendCause() {
    return suspendCause;
  }

  /**
   * sets the value of the suspendCause
   * 
   * @param suspendCause
   *          the suspendCause
   */
  public void setSuspendCause(java.lang.Integer suspendCause) {
    this.suspendCause = suspendCause;
  }

  /**
   * returns the value of the ageMonth
   * 
   * @return the ageMonth
   */
  public java.lang.Integer getAgeMonth() {
    return ageMonth;
  }

  /**
   * sets the value of the ageMonth
   * 
   * @param ageMonth
   *          the ageMonth
   */
  public void setAgeMonth(java.lang.Integer ageMonth) {
    this.ageMonth = ageMonth;
  }

  /**
   * returns the value of the relatedMonth
   * 
   * @return the relatedMonth
   */
  public java.lang.Integer getRelatedMonth() {
    return relatedMonth;
  }

  /**
   * sets the value of the relatedMonth
   * 
   * @param relatedMonth
   *          the relatedMonth
   */
  public void setRelatedMonth(java.lang.Integer relatedMonth) {
    this.relatedMonth = relatedMonth;
  }

  /**
   * returns the value of the increaseRate
   * 
   * @return the increaseRate
   */
  public java.math.BigDecimal getIncreaseRate() {
    return increaseRate;
  }

  /**
   * sets the value of the increaseRate
   * 
   * @param increaseRate
   *          the increaseRate
   */
  public void setIncreaseRate(java.math.BigDecimal increaseRate) {
    this.increaseRate = increaseRate;
  }

  /**
   * returns the value of the increaseFreq
   * 
   * @return the increaseFreq
   */
  public java.lang.String getIncreaseFreq() {
    return increaseFreq;
  }

  /**
   * sets the value of the increaseFreq
   * 
   * @param increaseFreq
   *          the increaseFreq
   */
  public void setIncreaseFreq(java.lang.String increaseFreq) {
    this.increaseFreq = increaseFreq;
  }

  /**
   * returns the value of the increaseYear
   * 
   * @return the increaseYear
   */
  public java.lang.Integer getIncreaseYear() {
    return increaseYear;
  }

  /**
   * sets the value of the increaseYear
   * 
   * @param increaseYear
   *          the increaseYear
   */
  public void setIncreaseYear(java.lang.Integer increaseYear) {
    this.increaseYear = increaseYear;
  }

  /**
   * returns the value of the payMode
   * 
   * @return the payMode
   */
  public java.lang.Integer getPayMode() {
    return payMode;
  }

  /**
   * sets the value of the payMode
   * 
   * @param payMode
   *          the payMode
   */
  public void setPayMode(java.lang.Integer payMode) {
    this.payMode = payMode;
  }

  /**
   * returns the value of the expiryCashValue
   * 
   * @return the expiryCashValue
   */
  public java.math.BigDecimal getExpiryCashValue() {
    return expiryCashValue;
  }

  /**
   * sets the value of the expiryCashValue
   * 
   * @param expiryCashValue
   *          the expiryCashValue
   */
  public void setExpiryCashValue(java.math.BigDecimal expiryCashValue) {
    this.expiryCashValue = expiryCashValue;
  }

  /**
   * returns the value of the retired
   * 
   * @return the retired
   */
  public java.lang.String getRetired() {
    return retired;
  }

  /**
   * sets the value of the retired
   * 
   * @param retired
   *          the retired
   */
  public void setRetired(java.lang.String retired) {
    this.retired = retired;
  }

  /**
   * returns the value of the simpleCompound
   * 
   * @return the simpleCompound
   */
  public java.lang.String getSimpleCompound() {
    return simpleCompound;
  }

  /**
   * sets the value of the simpleCompound
   * 
   * @param simpleCompound
   *          the simpleCompound
   */
  public void setSimpleCompound(java.lang.String simpleCompound) {
    this.simpleCompound = simpleCompound;
  }

  /**
   * returns the value of the startPayDate
   * 
   * @return the startPayDate
   */
  public java.util.Date getStartPayDate() {
    return startPayDate;
  }

  /**
   * sets the value of the startPayDate
   * 
   * @param startPayDate
   *          the startPayDate
   */
  public void setStartPayDate(java.util.Date startPayDate) {
    this.startPayDate = startPayDate;
  }

  /**
   * returns the value of the insertTime
   * 
   * @return the insertTime
   */
  public java.util.Date getInsertTime() {
    return insertTime;
  }

  /**
   * sets the value of the insertTime
   * 
   * @param insertTime
   *          the insertTime
   */
  public void setInsertTime(java.util.Date insertTime) {
    this.insertTime = insertTime;
  }

  /**
   * returns the value of the payChangeTime
   * 
   * @return the payChangeTime
   */
  public java.util.Date getPayChangeTime() {
    return payChangeTime;
  }

  /**
   * sets the value of the payChangeTime
   * 
   * @param payChangeTime
   *          the payChangeTime
   */
  public void setPayChangeTime(java.util.Date payChangeTime) {
    this.payChangeTime = payChangeTime;
  }

  /**
   * returns the value of the bonusSa
   * 
   * @return the bonusSa
   */
  public java.math.BigDecimal getBonusSa() {
    return bonusSa;
  }

  /**
   * sets the value of the bonusSa
   * 
   * @param bonusSa
   *          the bonusSa
   */
  public void setBonusSa(java.math.BigDecimal bonusSa) {
    this.bonusSa = bonusSa;
  }

  /**
   * returns the value of the liveRange
   * 
   * @return the liveRange
   */
  public java.lang.String getLiveRange() {
    return liveRange;
  }

  /**
   * sets the value of the liveRange
   * 
   * @param liveRange
   *          the liveRange
   */
  public void setLiveRange(java.lang.String liveRange) {
    this.liveRange = liveRange;
  }

  /**
   * returns the value of the moveRange
   * 
   * @return the moveRange
   */
  public java.lang.String getMoveRange() {
    return moveRange;
  }

  /**
   * sets the value of the moveRange
   * 
   * @param moveRange
   *          the moveRange
   */
  public void setMoveRange(java.lang.String moveRange) {
    this.moveRange = moveRange;
  }

  /**
   * returns the value of the payNext
   * 
   * @return the payNext
   */
  public java.lang.Integer getPayNext() {
    return payNext;
  }

  /**
   * sets the value of the payNext
   * 
   * @param payNext
   *          the payNext
   */
  public void setPayNext(java.lang.Integer payNext) {
    this.payNext = payNext;
  }

  /**
   * returns the value of the latestBonusSa
   * 
   * @return the latestBonusSa
   */
  public java.math.BigDecimal getLatestBonusSa() {
    return latestBonusSa;
  }

  /**
   * sets the value of the latestBonusSa
   * 
   * @param latestBonusSa
   *          the latestBonusSa
   */
  public void setLatestBonusSa(java.math.BigDecimal latestBonusSa) {
    this.latestBonusSa = latestBonusSa;
  }

  /**
   * returns the value of the startInstDate
   * 
   * @return the startInstDate
   */
  public java.util.Date getStartInstDate() {
    return startInstDate;
  }

  /**
   * sets the value of the startInstDate
   * 
   * @param startInstDate
   *          the startInstDate
   */
  public void setStartInstDate(java.util.Date startInstDate) {
    this.startInstDate = startInstDate;
  }

  /**
   * returns the value of the originSa
   * 
   * @return the originSa
   */
  public java.math.BigDecimal getOriginSa() {
    return originSa;
  }

  /**
   * sets the value of the originSa
   * 
   * @param originSa
   *          the originSa
   */
  public void setOriginSa(java.math.BigDecimal originSa) {
    this.originSa = originSa;
  }

  /**
   * returns the value of the originBonusSa
   * 
   * @return the originBonusSa
   */
  public java.math.BigDecimal getOriginBonusSa() {
    return originBonusSa;
  }

  /**
   * sets the value of the originBonusSa
   * 
   * @param originBonusSa
   *          the originBonusSa
   */
  public void setOriginBonusSa(java.math.BigDecimal originBonusSa) {
    this.originBonusSa = originBonusSa;
  }

  /**
   * returns the value of the anniBalance
   * 
   * @return the anniBalance
   */
  public java.math.BigDecimal getAnniBalance() {
    return anniBalance;
  }

  /**
   * sets the value of the anniBalance
   * 
   * @param anniBalance
   *          the anniBalance
   */
  public void setAnniBalance(java.math.BigDecimal anniBalance) {
    this.anniBalance = anniBalance;
  }

  /**
   * returns the value of the fixIncrement
   * 
   * @return the fixIncrement
   */
  public java.lang.String getFixIncrement() {
    return fixIncrement;
  }

  /**
   * sets the value of the fixIncrement
   * 
   * @param fixIncrement
   *          the fixIncrement
   */
  public void setFixIncrement(java.lang.String fixIncrement) {
    this.fixIncrement = fixIncrement;
  }

  /**
   * returns the value of the cpfCost
   * 
   * @return the cpfCost
   */
  public java.math.BigDecimal getCpfCost() {
    return cpfCost;
  }

  /**
   * sets the value of the cpfCost
   * 
   * @param cpfCost
   *          the cpfCost
   */
  public void setCpfCost(java.math.BigDecimal cpfCost) {
    this.cpfCost = cpfCost;
  }

  /**
   * returns the value of the cashCost
   * 
   * @return the cashCost
   */
  public java.math.BigDecimal getCashCost() {
    return cashCost;
  }

  /**
   * sets the value of the cashCost
   * 
   * @param cashCost
   *          the cashCost
   */
  public void setCashCost(java.math.BigDecimal cashCost) {
    this.cashCost = cashCost;
  }

  /**
   * returns the value of the derivation
   * 
   * @return the derivation
   */
  public java.lang.String getDerivation() {
    return derivation;
  }

  /**
   * sets the value of the derivation
   * 
   * @param derivation
   *          the derivation
   */
  public void setDerivation(java.lang.String derivation) {
    this.derivation = derivation;
  }

  /**
   * returns the value of the latOriBonusSa
   * 
   * @return the latOriBonusSa
   */
  public java.math.BigDecimal getLatOriBonusSa() {
    return latOriBonusSa;
  }

  /**
   * sets the value of the latOriBonusSa
   * 
   * @param latOriBonusSa
   *          the latOriBonusSa
   */
  public void setLatOriBonusSa(java.math.BigDecimal latOriBonusSa) {
    this.latOriBonusSa = latOriBonusSa;
  }

  /**
   * returns the value of the riskCode
   * 
   * @return the riskCode
   */
  public java.lang.String getRiskCode() {
    return riskCode;
  }

  /**
   * sets the value of the riskCode
   * 
   * @param riskCode
   *          the riskCode
   */
  public void setRiskCode(java.lang.String riskCode) {
    this.riskCode = riskCode;
  }

  /**
   * returns the value of the exposureRate
   * 
   * @return the exposureRate
   */
  public java.math.BigDecimal getExposureRate() {
    return exposureRate;
  }

  /**
   * sets the value of the exposureRate
   * 
   * @param exposureRate
   *          the exposureRate
   */
  public void setExposureRate(java.math.BigDecimal exposureRate) {
    this.exposureRate = exposureRate;
  }

  /**
   * returns the value of the reinsRate
   * 
   * @return the reinsRate
   */
  public java.math.BigDecimal getReinsRate() {
    return reinsRate;
  }

  /**
   * sets the value of the reinsRate
   * 
   * @param reinsRate
   *          the reinsRate
   */
  public void setReinsRate(java.math.BigDecimal reinsRate) {
    this.reinsRate = reinsRate;
  }

  /**
   * returns the value of the medicalFlag
   * 
   * @return the medicalFlag
   */
  public java.lang.String getMedicalFlag() {
    return medicalFlag;
  }

  /**
   * sets the value of the medicalFlag
   * 
   * @param medicalFlag
   *          the medicalFlag
   */
  public void setMedicalFlag(java.lang.String medicalFlag) {
    this.medicalFlag = medicalFlag;
  }

  /**
   * returns the value of the nextAmount
   * 
   * @return the nextAmount
   */
  public java.math.BigDecimal getNextAmount() {
    return nextAmount;
  }

  /**
   * sets the value of the nextAmount
   * 
   * @param nextAmount
   *          the nextAmount
   */
  public void setNextAmount(java.math.BigDecimal nextAmount) {
    this.nextAmount = nextAmount;
  }

  /**
   * returns the value of the waiverStart
   * 
   * @return the waiverStart
   */
  public java.util.Date getWaiverStart() {
    return waiverStart;
  }

  /**
   * sets the value of the waiverStart
   * 
   * @param waiverStart
   *          the waiverStart
   */
  public void setWaiverStart(java.util.Date waiverStart) {
    this.waiverStart = waiverStart;
  }

  /**
   * returns the value of the waiverEnd
   * 
   * @return the waiverEnd
   */
  public java.util.Date getWaiverEnd() {
    return waiverEnd;
  }

  /**
   * sets the value of the waiverEnd
   * 
   * @param waiverEnd
   *          the waiverEnd
   */
  public void setWaiverEnd(java.util.Date waiverEnd) {
    this.waiverEnd = waiverEnd;
  }

  /**
   * returns the value of the autoPermntLapse
   * 
   * @return the autoPermntLapse
   */
  public java.lang.String getAutoPermntLapse() {
    return autoPermntLapse;
  }

  /**
   * sets the value of the autoPermntLapse
   * 
   * @param autoPermntLapse
   *          the autoPermntLapse
   */
  public void setAutoPermntLapse(java.lang.String autoPermntLapse) {
    this.autoPermntLapse = autoPermntLapse;
  }

  /**
   * returns the value of the permntLapseNoticeDate
   * 
   * @return the permntLapseNoticeDate
   */
  public java.util.Date getPermntLapseNoticeDate() {
    return permntLapseNoticeDate;
  }

  /**
   * sets the value of the permntLapseNoticeDate
   * 
   * @param permntLapseNoticeDate
   *          the permntLapseNoticeDate
   */
  public void setPermntLapseNoticeDate(java.util.Date permntLapseNoticeDate) {
    this.permntLapseNoticeDate = permntLapseNoticeDate;
  }

  /**
   * returns the value of the masterProduct
   * 
   * @return the masterProduct
   */
  public java.lang.Integer getMasterProduct() {
    return masterProduct;
  }

  /**
   * sets the value of the masterProduct
   * 
   * @param masterProduct
   *          the masterProduct
   */
  public void setMasterProduct(java.lang.Integer masterProduct) {
    this.masterProduct = masterProduct;
  }

  /**
   * returns the value of the birthday
   * 
   * @return the birthday
   */
  public java.util.Date getBirthday() {
    return birthday;
  }

  /**
   * sets the value of the birthday
   * 
   * @param birthday
   *          the birthday
   */
  public void setBirthday(java.util.Date birthday) {
    this.birthday = birthday;
  }

  /**
   * returns the value of the waiver
   * 
   * @return the waiver
   */
  public java.lang.String getWaiver() {
    return waiver;
  }

  /**
   * sets the value of the waiver
   * 
   * @param waiver
   *          the waiver
   */
  public void setWaiver(java.lang.String waiver) {
    this.waiver = waiver;
  }

  /**
   * returns the value of the waivedSa
   * 
   * @return the waivedSa
   */
  public java.math.BigDecimal getWaivedSa() {
    return waivedSa;
  }

  /**
   * sets the value of the waivedSa
   * 
   * @param waivedSa
   *          the waivedSa
   */
  public void setWaivedSa(java.math.BigDecimal waivedSa) {
    this.waivedSa = waivedSa;
  }

  /**
   * returns the value of the issueAgent
   * 
   * @return the issueAgent
   */
  public java.lang.Long getIssueAgent() {
    return issueAgent;
  }

  /**
   * sets the value of the issueAgent
   * 
   * @param issueAgent
   *          the issueAgent
   */
  public void setIssueAgent(java.lang.Long issueAgent) {
    this.issueAgent = issueAgent;
  }

  /**
   * returns the value of the strategyCode
   * 
   * @return the strategyCode
   */
  public java.lang.String getStrategyCode() {
    return strategyCode;
  }

  /**
   * sets the value of the strategyCode
   * 
   * @param strategyCode
   *          the strategyCode
   */
  public void setStrategyCode(java.lang.String strategyCode) {
    this.strategyCode = strategyCode;
  }

  /**
   * returns the value of the mmYear
   * 
   * @return the mmYear
   */
  public java.lang.Integer getMmYear() {
    return mmYear;
  }

  /**
   * sets the value of the mmYear
   * 
   * @param mmYear
   *          the mmYear
   */
  public void setMmYear(java.lang.Integer mmYear) {
    this.mmYear = mmYear;
  }

  /**
   * returns the value of the loanType
   * 
   * @return the loanType
   */
  public java.lang.String getLoanType() {
    return loanType;
  }

  /**
   * sets the value of the loanType
   * 
   * @param loanType
   *          the loanType
   */
  public void setLoanType(java.lang.String loanType) {
    this.loanType = loanType;
  }

  /**
   * returns the value of the benPeriodType
   * 
   * @return the benPeriodType
   */
  public java.lang.String getBenPeriodType() {
    return benPeriodType;
  }

  /**
   * sets the value of the benPeriodType
   * 
   * @param benPeriodType
   *          the benPeriodType
   */
  public void setBenPeriodType(java.lang.String benPeriodType) {
    this.benPeriodType = benPeriodType;
  }

  /**
   * returns the value of the benefitPeriod
   * 
   * @return the benefitPeriod
   */
  public java.lang.Integer getBenefitPeriod() {
    return benefitPeriod;
  }

  /**
   * sets the value of the benefitPeriod
   * 
   * @param benefitPeriod
   *          the benefitPeriod
   */
  public void setBenefitPeriod(java.lang.Integer benefitPeriod) {
    this.benefitPeriod = benefitPeriod;
  }

  /**
   * returns the value of the facReinsurIndi
   * 
   * @return the facReinsurIndi
   */
  public java.lang.String getFacReinsurIndi() {
    return facReinsurIndi;
  }

  /**
   * sets the value of the facReinsurIndi
   * 
   * @param facReinsurIndi
   *          the facReinsurIndi
   */
  public void setFacReinsurIndi(java.lang.String facReinsurIndi) {
    this.facReinsurIndi = facReinsurIndi;
  }

  /**
   * returns the value of the uwListId
   * 
   * @return the uwListId
   */
  public java.lang.Long getUwListId() {
    return uwListId;
  }

  /**
   * sets the value of the uwListId
   * 
   * @param uwListId
   *          the uwListId
   */
  public void setUwListId(java.lang.Long uwListId) {
    this.uwListId = uwListId;
  }

  /**
   * This resets all the form values back to defaults, part of struts framework
   */
  @Override
  public void reset(ActionMapping mapping, HttpServletRequest request) {

    UwProductForm productForm = new UwProductForm();

    BeanUtils.copyProperties(this, productForm);

  }

  /**
   * sets the UwExtraLoading values attached to this UwProduct
   * @param formBeans
   */
  public void setUwextraloadings(UwExtraLoadingForm[] formBeans) {
    this.uwextraloadings = formBeans;
  }

  /**
   * get uw extra loading
   * @return
   */
  public UwExtraLoadingForm[] getUwextraloadings() {
    if (uwextraloadings != null && uwextraloadings.length != 0) {
      UwExtraLoadingForm[] copyOfUwExtraLoadings = new UwExtraLoadingForm[uwextraloadings.length];
      System.arraycopy(uwextraloadings, 0, copyOfUwExtraLoadings, 0,
          uwextraloadings.length);
      return copyOfUwExtraLoadings;
    }
    return null;
  }

  /**
   * sets the UwLien values attached to this UwProduct
   * @param formBeans
   */
  public void setUwliens(UwLienForm[] formBeans) {
    this.uwliens = formBeans;
  }

  /**
   * returns a <b>copy</b> of the contained Uwlien value array
   * 
   *@return a <b>copy</b> of the contained Uwlien value array
   */
  public UwLienForm[] getUwliens() {
    if (uwliens != null && uwliens.length != 0) {
      UwLienForm[] copyOfUwLiens = new UwLienForm[uwliens.length];
      System.arraycopy(uwliens, 0, copyOfUwLiens, 0, uwliens.length);
      return copyOfUwLiens;
    }
    return null;
  }

  // Begin Additional Business Methods
  private CustomerForm[] Insureds = null;

  /**
   * @return Returns the insureds.
   */
  public CustomerForm[] getInsureds() {
    return Insureds;
  }

  /**
   * @param insureds
   *          The insureds to set.
   */
  public void setInsureds(CustomerForm[] insureds) {
    Insureds = insureds;
  }

  // End Additional Business Methods

  /**
   * @return Returns the decisionDesc2.
   */
  public String getDecisionDesc2() {
    return decisionDesc2;
  }

  /**
   * @param decisionDesc2
   *          The decisionDesc2 to set.
   */
  public void setDecisionDesc2(String decisionDesc2) {
    this.decisionDesc2 = decisionDesc2;
  }

  /**
   * @return statusDate
   */
  public Date getStatusDate() {
    return statusDate;
  }

  /**
   * @param statusDate
   *          will be set statusDate
   */
  public void setStatusDate(Date dueDate) {
    this.statusDate = dueDate;
  }

  public java.math.BigDecimal getTotalPremAn() {
    return totalPremAn;
  }

  public void setTotalPremAn(java.math.BigDecimal totalPremAn) {
    this.totalPremAn = totalPremAn;
  }

public String getProductName() {
    return productName;
}

public void setProductName(String productName) {
    this.productName = productName;
}
}