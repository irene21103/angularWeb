package com.ebao.ls.crs.web.form;

import org.apache.commons.lang3.StringUtils;

import com.ebao.foundation.module.web.base.GenericForm;
import com.ebao.ls.crs.vo.CRSPartyLogVO;

public class CRSPartyLogForm extends GenericForm{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6227582275651448289L;
	
	private String actionType;
	
	private Long policyId;
	
	private Long partyId;
	
	private Long crsId;
	
	//0:個險, 1:團險
	private String policyType;
	
	private String inputSource;
	
	private String sysCode;
	
	//1:t_policy_holder, 2:t_insured_list, 3:t_contract_bene, 4:t_customer
	private String roleType;
	
	private Long logId;
	
	private Long changeId;
	
	private Long policyChgId;
	
	private java.util.Date currentUserLocalTime;
	
	//For jsp  binding variables.
	private CRSPartyLogVO crsPartyLogVO;
	
	private java.util.List<String> msgInfoList;
	
	private Boolean isReadOnly = Boolean.FALSE;
	
	private Boolean isExistsCRSDocument = Boolean.FALSE;
	
	private String partyRole;
	
	private String certiCode;
	
    //IR 358701 要保人變更，進入FATCA與CRS不應清除資料
	private String newRelation2PH;
	
    private String abandonInheritance;
    
    private String counterServices;

	public String getCertiCode() {
		return certiCode;
	}

	public void setCertiCode(String certiCode) {
		this.certiCode = certiCode;
	}

	public String getActionType() {
		return actionType;
	}

	public void setActionType(String actionType) {
		this.actionType = actionType;
	}

	public String getPolicyType() {
		return policyType;
	}

	public void setPolicyType(String policyType) {
		this.policyType = policyType;
	}

	public String getInputSource() {
		return inputSource;
	}

	public void setInputSource(String inputSource) {
		this.inputSource = inputSource;
	}

	public Long getLogId() {
		return logId;
	}

	public void setLogId(Long logId) {
		this.logId = logId;
	}

	public CRSPartyLogVO getCrsPartyLogVO() {
		if(crsPartyLogVO == null) {
			crsPartyLogVO = new CRSPartyLogVO();
		}
		return crsPartyLogVO;
	}

	public void setCrsPartyLogVO(CRSPartyLogVO crsPartyLogVO) {		
		this.crsPartyLogVO = crsPartyLogVO;
	}

	public java.util.List<String> getMsgInfoList() {
		return msgInfoList;
	}

	public void setMsgInfoList(java.util.List<String> msgInfoList) {
		this.msgInfoList = msgInfoList;
	}
	public void setMsgInfoList(String msgText) {
		if(StringUtils.isEmpty(msgText))
			return;
		this.msgInfoList = java.util.Arrays.asList(StringUtils.split(msgText, ","));
	}
	
	public void setMsgInfoList(String[] msgArray) {
		if(msgArray != null && msgArray.length > 0)
			this.msgInfoList = java.util.Arrays.asList(msgArray);
	}
	
	public void setMsgInfoList() {
		this.msgInfoList = new java.util.LinkedList<String>();
	}
	
	public Boolean getIsReadOnly() {
		return isReadOnly;
	}

	public void setIsReadOnly(Boolean isReadOnly) {
		this.isReadOnly = isReadOnly;
	}

	public String getSysCode() {
		return sysCode;
	}

	public void setSysCode(String sysCode) {
		this.sysCode = sysCode;
	}

	public Long getChangeId() {
		return changeId;
	}

	public void setChangeId(Long changeId) {
		this.changeId = changeId;
	}

	public Long getPolicyId() {
		return policyId;
	}

	public void setPolicyId(Long policyId) {
		this.policyId = policyId;
	}

	public Long getPartyId() {
		return partyId;
	}

	public void setPartyId(Long partyId) {
		this.partyId = partyId;
	}

	public String getRoleType() {
		return roleType;
	}

	public void setRoleType(String roleType) {
		this.roleType = roleType;
	}

	public String getPartyRole() {
		return partyRole;
	}

	public void setPartyRole(String partyRole) {
		this.partyRole = partyRole;
	}

	public Long getPolicyChgId() {
		return policyChgId;
	}

	public void setPolicyChgId(Long policyChgId) {
		this.policyChgId = policyChgId;
	}

	public Boolean getIsExistsCRSDocument() {
		return isExistsCRSDocument;
	}
	
	public Boolean isNotExistsCRSDocument() {
		return Boolean.FALSE.equals(isExistsCRSDocument);
	}
	public void setIsExistsCRSDocument(Boolean isExistsCRSDocument) {
		this.isExistsCRSDocument = isExistsCRSDocument;
	}

	public java.util.Date getCurrentUserLocalTime() {
		return currentUserLocalTime;
	}

	public void setCurrentUserLocalTime(java.util.Date currentUserLocalTime) {
		this.currentUserLocalTime = currentUserLocalTime;
	}

	public Long getCrsId() {
		return crsId;
	}

	public void setCrsId(Long crsId) {
		this.crsId = crsId;
	}

	public String getAbandonInheritance() {
		return abandonInheritance;
	}

	public void setAbandonInheritance(String abandonInheritance) {
		this.abandonInheritance = abandonInheritance;
	}

	public String getCounterServices() {
		return counterServices;
	}

	public void setCounterServices(String counterServices) {
		this.counterServices = counterServices;
	}

	public String getNewRelation2PH() {
		return newRelation2PH;
	}

	public void setNewRelation2PH(String newRelation2PH) {
		this.newRelation2PH = newRelation2PH;
	}

}
