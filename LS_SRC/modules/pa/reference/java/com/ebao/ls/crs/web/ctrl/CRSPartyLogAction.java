package com.ebao.ls.crs.web.ctrl;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.transaction.UserTransaction;

import org.apache.commons.collections.ListUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.web.util.WebUtils;

import com.ebao.foundation.core.logging.Log;
import com.ebao.foundation.module.util.beanutils.BeanUtils;
import com.ebao.foundation.module.web.action.GenericAction;
import com.ebao.ls.crs.ci.CRSPartyCI;
import com.ebao.ls.crs.constant.CRSCodeCst;
import com.ebao.ls.crs.constant.CRSCodeType;
import com.ebao.ls.crs.constant.CRSTinBizSource;
import com.ebao.ls.crs.exceptions.ValidateException;
import com.ebao.ls.crs.identity.CRSPartyLogService;
import com.ebao.ls.crs.vo.CRSPartyLogVO;
import com.ebao.ls.crs.vo.CRSTaxIDNumberLogVO;
import com.ebao.ls.crs.web.WebWarningMessage;
import com.ebao.ls.crs.web.form.CRSPartyLogForm;
import com.ebao.ls.cs.commonflow.ds.PolicyChangeAttributeService;
import com.ebao.ls.cs.commonflow.vo.AlterationItemVO;
import com.ebao.ls.cs.commonflow.vo.PolicyChangeAttributeVO;
import com.ebao.ls.cs.policyalteration.ctrl.trad.crs.CSValidationCrsService;
import com.ebao.ls.escape.helper.EscapeHelper;
import com.ebao.ls.pa.nb.util.NBUtils;
import com.ebao.ls.pa.pty.service.CertiCodeCI;
import com.ebao.ls.pa.pty.service.CertiCodeCheckResultVO;
import com.ebao.ls.pa.pub.CodeCst;
import com.ebao.ls.pty.ci.CustomerCI;
import com.ebao.ls.pty.vo.CustomerVO;
import com.ebao.ls.pub.cst.YesNo;
import com.ebao.pub.batch.type.LogLevel;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.framework.ExceptionFactory;
import com.ebao.pub.framework.MultiWarning;
import com.ebao.pub.i18n.lang.CodeTable;
import com.ebao.pub.i18n.lang.StringResource;
import com.ebao.pub.security.AppUser;
import com.ebao.pub.util.Trans;
import com.ebao.pub.util.TransUtils;

public class CRSPartyLogAction extends GenericAction {
	
	private static final Log log = Log.getLogger(CRSPartyLogAction.class);
	
	private static final String TO_CRS_COMPANY = "toCrsCompany";
	private static final String TO_CRS_PERSON = "toCrsPerson";

	private static final String ACTION_1_SUCCESS = "success";

	@Resource(name = CRSPartyLogService.BEAN_DEFAULT)
	private CRSPartyLogService crsPartyLogService;

	@Resource(name = CRSPartyCI.BEAN_DEFAULT)
	private CRSPartyCI partyCI;
	
	@Resource(name = CSValidationCrsService.BEAN_DEFAULT)
	private CSValidationCrsService cSValidationCrsService;
	
	@Resource(name = CertiCodeCI.BEAN_DEFAULT)
	private CertiCodeCI certiCodeCI;
	
	@Resource(name = PolicyChangeAttributeService.BEAN_DEFAULT)
	private PolicyChangeAttributeService policyChangeAttributeService; 
	
	@Resource(name = CustomerCI.BEAN_DEFAULT)
	private CustomerCI customerCI;
	
	@Override
	public ActionForward process(ActionMapping mapp, ActionForm actionForm, HttpServletRequest req,
			HttpServletResponse resp) throws Exception {
		
		CRSPartyLogForm inputForm = (CRSPartyLogForm) actionForm;
		AppUser user = AppContext.getCurrentUser();
		java.util.Date processDate = AppContext.getCurrentUserLocalTime();
		inputForm.setCurrentUserLocalTime(processDate); // 預設最大時間
        //IR 358701 要保人變更，進入FATCA與CRS不應清除資料
        HttpSession session = req.getSession();
		if(inputForm.getNewRelation2PH()!=null){
			EscapeHelper.escapeHtml(session).setAttribute("newRelation2PH", inputForm.getNewRelation2PH());
		}
		if(inputForm.getAbandonInheritance()!=null){
			EscapeHelper.escapeHtml(session).setAttribute("abandonInheritance", inputForm.getAbandonInheritance());
			EscapeHelper.escapeHtml(session).setAttribute("changeForm", "CRS");
		}
		if(inputForm.getCounterServices()!=null){
			EscapeHelper.escapeHtml(session).setAttribute("counterServices", inputForm.getCounterServices());
		}

		if (WebUtils.getSessionAttribute(req, CRSCodeCst.WEB_ACTION_TYPE__LOGIN_ACTION_TYPE) == null) {
			WebUtils.setSessionAttribute(req, CRSCodeCst.WEB_ACTION_TYPE__LOGIN_ACTION_TYPE, inputForm.getActionType());
		}
		/* read data */
		if (CRSCodeCst.WEB_ACTION_TYPE__READ_ONLY.equals(inputForm.getActionType())) { // 查詢CRS身份頁面-選擇查詢
			CRSPartyLogVO logVO = crsPartyLogService.load(inputForm.getLogId());
			inputForm.setCrsPartyLogVO(logVO);
			inputForm.setIsReadOnly(Boolean.TRUE);
			inputForm.setIsExistsCRSDocument(crsPartyLogService.isExistsCRSDocument(inputForm.getLogId()));
			return mapp.findForward(ACTION_1_SUCCESS);
		}
		List<String> inputSourceWithLogIdList = java.util.Arrays.asList(new String[] {
				CRSCodeCst.INPUT_SOURCE__CMN,
				CRSCodeCst.INPUT_SOURCE__LINK});
		if(CRSCodeCst.WEB_ACTION_TYPE__READ_AND_WRITE.equals(inputForm.getActionType())
				&& inputSourceWithLogIdList.contains(inputForm.getInputSource())
				&& inputForm.getLogId() != null) { // 查詢CRS身份頁面-選擇維護
			CRSPartyLogVO logVO = crsPartyLogService.load(inputForm.getLogId());
			inputForm.setCrsPartyLogVO(logVO);
			BeanUtils.copyProperties(inputForm, logVO);
			inputForm.setIsExistsCRSDocument(crsPartyLogService.isExistsCRSDocument(inputForm.getLogId()));
			return mapp.findForward(ACTION_1_SUCCESS);
		}
		if (CRSCodeCst.WEB_ACTION_TYPE__READ_CRS.equals(inputForm.getActionType())) { // CRS身分聲明書維護-查詢自我證明
			CRSPartyLogVO logVO = inputForm.getCrsPartyLogVO();
			req.setAttribute("partyLogId", String.valueOf(logVO.getLogId()));
			req.setAttribute("lockPage", YesNo.YES_NO__YES);
			String partyRole = CodeTable.getCodeDesc("V_CRS_PARTY_ROLE", logVO.getCrsType(), user.getLangId());
			/* 導頁回傳參數 */
			req.getSession().setAttribute("postBackMap", getPostBackMap(inputForm, CRSCodeCst.WEB_ACTION_TYPE__READ_ONLY));
			if (CRSCodeCst.CRS_IDENTITY_TYPE__MAN.equals(partyRole)) {
				return mapp.findForward(TO_CRS_PERSON);
			}
			return mapp.findForward(TO_CRS_COMPANY);
		}
		/* create the instance */
		if(CRSCodeCst.WEB_ACTION_TYPE__CREATE_USER.equals(inputForm.getActionType())
				&& CRSCodeCst.INPUT_SOURCE__CMN.equals(inputForm.getInputSource())) { // 查詢CRS身份頁面-新增CRS身份聲明
			String sysCode = CRSCodeCst.POLICY_TYPE__NORMAL.equals(inputForm.getPolicyType())
					? crsPartyLogService.getSYSCode(user.getUserId())
					: CRSCodeCst.SYS_CODE__GRP;
			inputForm.setSysCode(sysCode);
			CRSPartyLogVO logVO = new CRSPartyLogVO();
			BeanUtils.copyProperties(logVO, inputForm);
			// 335152_CRS共用作業調整，共用頁面新增時，若User有輸入值要帶入到CRS身分聲明書維護頁面。
			logVO.setCertiCode(StringUtils.defaultIfBlank(EscapeHelper.escapeHtml(req.getParameter("certiCode")), ""));
			logVO.setName(StringUtils.defaultIfBlank(EscapeHelper.escapeHtml(req.getParameter("name")), ""));
			logVO.setPolicyCode(StringUtils.defaultIfBlank(EscapeHelper.escapeHtml(req.getParameter("policyCode")), ""));
			inputForm.setCrsPartyLogVO(logVO);
			return mapp.findForward(ACTION_1_SUCCESS);
		}
		if(CRSCodeCst.WEB_ACTION_TYPE__CREATE_USER.equals(inputForm.getActionType())
				&& CRSCodeCst.INPUT_SOURCE__LINK.equals(inputForm.getInputSource()) 
				&& inputForm.getLogId() == null 
				&& inputForm.getChangeId() != null) {
			
			CRSPartyLogVO logVO = crsPartyLogService.initCRSPartyLog(inputForm.getPolicyId(), inputForm.getPartyId(),
					inputForm.getRoleType());
			BeanUtils.copyProperties(logVO, inputForm);
			inputForm.setCrsPartyLogVO(logVO);
			logVO.setCrsId(inputForm.getCrsId());
			logVO.setPolicyChangeId(inputForm.getPolicyChgId());
			return mapp.findForward(ACTION_1_SUCCESS);
		}
		
		/*write data*/
		UserTransaction ut = null;
		try {
			ut = Trans.getUserTransaction();
			ut.begin();
			CRSPartyLogVO logVO = inputForm.getCrsPartyLogVO();
			String forwardURL = ACTION_1_SUCCESS;
			if (CRSCodeCst.WEB_ACTION_TYPE__TEMP_SAVE.equals(inputForm.getActionType())) {
				WebWarningMessage warningMsg = this.validateSubmit(logVO, inputForm.getActionType());
				if (warningMsg.sizeErrorList() != 0) {
					inputForm.setMsgInfoList(warningMsg.toErrorList());
					return mapp.findForward(forwardURL);
				}

				this.saveOrUpdate(user.getUserId(), processDate, logVO, inputForm);
				List<String> savingMsgList = getSavingMsgList(logVO, this.getEbaoI18NStr("MSG_1263428"), user.getLangId()); // 資料已暫存
				List<String> msgInfoList = ListUtils.sum(warningMsg.toWarningList(), savingMsgList);
				inputForm.setMsgInfoList(msgInfoList);
				inputForm.setIsExistsCRSDocument(crsPartyLogService.isExistsCRSDocument(inputForm.getLogId()));
			} else if (CRSCodeCst.WEB_ACTION_TYPE__TEMP_SAVE_MAINTAIN.equals(inputForm.getActionType())) { // 暫存並維護自我證明
				this.saveOrUpdate(user.getUserId(), processDate, logVO, inputForm);
				/* 導頁回傳參數 */
				req.setAttribute("partyLogId", String.valueOf(logVO.getLogId()));
				req.setAttribute("lockPage", YesNo.YES_NO__NO);
				req.getSession().setAttribute("postBackMap", getPostBackMap(inputForm, CRSCodeCst.WEB_ACTION_TYPE__READ_AND_WRITE));
				forwardURL = TO_CRS_PERSON;
				if (CRSCodeCst.CRS_IDENTITY_TYPE__COMPANY.equals(logVO.getCrsCode())) {
					forwardURL = TO_CRS_COMPANY;
				}
			} 
			if (CRSCodeCst.WEB_ACTION_TYPE__COMPLETE.equals(inputForm.getActionType())) { // 提交
				WebWarningMessage warningMsg = this.validateSubmit(logVO, inputForm.getActionType());
				if (warningMsg.sizeErrorList() == 0) {
					/* 存檔作業 */
					this.saveOrUpdate(user.getUserId(), processDate, logVO, inputForm);
					inputForm.setIsExistsCRSDocument(crsPartyLogService.isExistsCRSDocument(inputForm.getLogId()));
					/* 提交作業 */
					partyCI.submitByLogId(logVO.getLogId());
					logVO = this.crsPartyLogService.load(logVO.getLogId());
					inputForm.setCrsPartyLogVO(logVO);
					List<String> savingMsgList = getSavingMsgList(logVO, "資料已提交", user.getLangId());
					List<String> msgList = ListUtils.sum(warningMsg.toWarningList(), savingMsgList);
					inputForm.setMsgInfoList(msgList);
					/* 提交後只能唯讀，不可重覆存檔 */
					inputForm.setIsReadOnly(Boolean.TRUE);
				} else {
					inputForm.setMsgInfoList(warningMsg.toErrorList());
				}
			}
			ut.commit();	
			return mapp.findForward(forwardURL);
		} catch (ValidateException validateEx) {
			TransUtils.rollback(ut);
			inputForm.setMsgInfoList(validateEx.getMsgInfoList());
			return mapp.findForward(ACTION_1_SUCCESS);
		} catch (Exception ex) {
			TransUtils.rollback(ut);
			throw ExceptionFactory.parse(ex);
		}
	}
	
	private void saveOrUpdate(Long userId, java.util.Date processDate, CRSPartyLogVO srcLogVO, Long changeId,
			String inputSource, String sysCode) throws Exception {
		CRSPartyLogVO destLogVO = srcLogVO;
		if (srcLogVO.getLogId() == null) { // 首次新建資料
			destLogVO.setInputSource(inputSource);
			destLogVO.setSysCode(sysCode);
		} else {
			destLogVO = this.crsPartyLogService.load(srcLogVO.getLogId());
			BeanUtils.copyProperties(destLogVO, srcLogVO);
		}
		
		// PCR-257607[Liam Liu]身分聲明為空 且 [聲明書簽屬狀態]非已簽署 且[稅務居住國家/地區]有任何值時，身份聲明情況固定為N-0
		if (destLogVO != null && StringUtils.isBlank(destLogVO.getCrsAnnounceStatus())
				&& !CRSCodeCst.CRS_DOC_SIGN_STATUS__SIGNOFF.equals(destLogVO.getDocSignStatus())
				&& CollectionUtils.isNotEmpty(this.checkTaxIDNumberResCountry(destLogVO))) {
			destLogVO.setCrsAnnounceStatus(CRSCodeCst.CRS_ANNOUNCE_STATUS__NOTCLAIMED_A);
		}
		
		// PCR-257607[Liam Liu][聲明書簽屬狀態]為02，身分聲明情況此欄位固定為Y。
		if(CRSCodeCst.CRS_DOC_SIGN_STATUS__SIGNOFF.equals(destLogVO.getDocSignStatus())) {
			destLogVO.setCrsAnnounceStatus(CRSCodeCst.CRS_ANNOUNCE_STATUS__CLAIMED);
		}
		
		if(destLogVO.getCrsType() == null) {
			destLogVO.setCrsType("");
		}
		crsPartyLogService.saveOrUpdate(userId, processDate, destLogVO, changeId, inputSource);
	}
	
	/**
	 * 依照傳入之CRSPartyLogVO使用PK反查CRSTaxIDNumberLog(T_CRS_RES_TIN_LOG)，
	 * 並根據Vo內的ResCountry[稅務居住國家/地區]進行List處理。
	 * 
	 * @param partyLogVo
	 * @return
	 */
	private List<CRSTaxIDNumberLogVO> checkTaxIDNumberResCountry(CRSPartyLogVO partyLogVo) {
		String bizSource1, bizSource2;
		if (CRSCodeCst.CRS_IDENTITY_TYPE__COMPANY.equals(partyLogVo.getCrsCode())) { 
			// 法人會有兩種source
			bizSource1 = CRSTinBizSource.BIZ_SOURCE_COMPANY;
			bizSource2 = CRSTinBizSource.BIZ_SOURCE_CTRL_MAN;
		} else { // 個人
			bizSource1 = CRSTinBizSource.BIZ_SOURCE_PERSON;
			bizSource2 = CRSTinBizSource.BIZ_SOURCE_PERSON;
		}
		
		// 若 Vo內的ResCountry[稅務居住國家/地區]沒值，則移出List
		List<CRSTaxIDNumberLogVO> rsList = partyCI.findByLogIdAndBizSource(partyLogVo.getLogId(), bizSource1, bizSource2);
		for (CRSTaxIDNumberLogVO crsTaxIDNumberLogVO : rsList) {
			if (StringUtils.isBlank(crsTaxIDNumberLogVO.getResCountry())) {
				rsList.remove(crsTaxIDNumberLogVO);
			}
		}
		return rsList; 
	}

	private void saveOrUpdate(Long userId, java.util.Date processDate, CRSPartyLogVO srcLogVO,
			CRSPartyLogForm inputForm) throws Exception {
		this.saveOrUpdate(userId, processDate, srcLogVO, inputForm.getChangeId(), inputForm.getInputSource(),
				inputForm.getSysCode());
		BeanUtils.copyProperties(inputForm, srcLogVO);// 畫面補值
	}
	
	private List<String> getSavingMsgList(CRSPartyLogVO logVO, String processCodeMsg, String langId) {
		String crsTypeDesc = CodeTable.getCodeDesc("T_CRS_PARTY_TYPE", logVO.getCrsType(), langId);
		WebWarningMessage msg = new WebWarningMessage("certiCode", "身份證字號/統一編號 = " + logVO.getCertiCode());
		msg.add("name", "姓名/公司名 = " + logVO.getName());
		if (StringUtils.isNotEmpty(logVO.getCrsType())) {
			msg.add("docSignStatus", String.format("文件簽署狀態 = %s-%s", logVO.getCrsType(), crsTypeDesc));
		}
		msg.add("processsCode", String.format("LogId = %d, %s", logVO.getLogId(), processCodeMsg));
		return msg.toMsgInfoList();
	}
	
	private WebWarningMessage validateSubmit(CRSPartyLogVO logVO, String actionType) {
		WebWarningMessage msg = new WebWarningMessage();
		Boolean pass = Boolean.TRUE;
		Boolean isCheckedOldForeignIdval = Boolean.FALSE;
		if (!CRSCodeCst.BUILD_TYPE__BUILDING.equals(logVO.getBuildType())) {
			msg.add(LogLevel.ERROR, "BUILD_TYPE", "建檔狀態不為[已暫存]");
			pass = Boolean.FALSE;
		}
		if (StringUtils.isEmpty(logVO.getDocSignStatus())) {
			msg.add(LogLevel.ERROR, "[DOC_SIGN_STATUS]", "[聲明書簽署狀態] 欄位尚未填寫");
			pass = Boolean.FALSE;
		}
		if (StringUtils.isNotEmpty(logVO.getDocSignStatus()) && logVO.getDocSignDate() == null) {
			msg.add(LogLevel.ERROR, "[DOC_SIGN_DATE]", "[狀態日期] 欄位尚未填寫");
			pass = Boolean.FALSE;
		}
		if (CRSCodeCst.CRS_DOC_SIGN_STATUS__SIGNOFF.equals(logVO.getDocSignStatus())
				&& logVO.getDeclarationDate() == null) {
			msg.add(LogLevel.ERROR, "[DECLARATION_DATE]", "[聲明書簽署狀態]=已簽署,但 [聲明日期 西元年/月/日] 欄位尚未填寫");
			pass = Boolean.FALSE;
		}
		if (StringUtils.isEmpty(logVO.getAgentAnnounce())) { // MSG-WRN-CRS-004 未填寫業務員聲明
			msg.add(LogLevel.ERROR, "[AGENT_ANNOUNCE]", "[業務員聲明] 欄位尚未填寫");
		}
		
		// 調整聲明書簽署日期空白規則：若聲明書簽署狀態非(已簽署02,不願簽署03,批次判定05,人工判定06) 此欄位應為空白
		if (StringUtils.isNotEmpty(logVO.getDocSignStatus())
				&& !NBUtils.in(logVO.getDocSignStatus(), CRSCodeCst.DOC_SIGN_DATE_BLANK_RULE)
				&& logVO.getDeclarationDate() != null && StringUtils.isNotEmpty(logVO.getDeclarationDate().toString())) {
			String showMsg = "當前[聲明書簽署狀態]為= %s，[聲明書簽署日期]應為空白";
			msg.add(LogLevel.ERROR, "[DOC_SIGN_DATE]", String.format(showMsg, logVO.getDocSignStatus()));
		}
		
		//PCR-507742 如果是pos系統進來，先查詢此次變更&&此ID是否有勾選【保戶確認無居留證】
		if (logVO.getSysCode().equals(CodeCst.SOURCE_TYPE__POS)) {
			CustomerVO customerVO = customerCI.getPersonbyCertiCode(logVO.getCertiCode());
			List<PolicyChangeAttributeVO> list185 = (List<PolicyChangeAttributeVO>) policyChangeAttributeService.getPolicyChangeAttributeList(logVO.getChangeId(), com.ebao.ls.pub.CodeCst.ATTR_SPEC__CHECK_OldForeignId);
			JSONParser parser = new JSONParser();
			if (customerVO != null) {
				for(PolicyChangeAttributeVO vo185 : list185){
					try {
						org.json.simple.JSONObject jsonObj = (org.json.simple.JSONObject) parser.parse(vo185.getAttributeValue());
						if (Long.toString(customerVO.getCustomerId()).equals(jsonObj.get("partyId"))) {
							if (com.ebao.ls.pub.CodeCst.YES_NO__YES.equals(jsonObj.get("statues"))) {
								isCheckedOldForeignIdval = true;
							}
						}
					} catch (ParseException e) {
	    				throw ExceptionFactory.parse(e);
	    			} 
				}
			}
		}
		
		//#366594 新增CRS驗證 certi_code
		if(StringUtils.isNotEmpty(logVO.getCertiCode()) && !isCheckedOldForeignIdval){
			String twCertiFmt =   "^[a-zA-Z][12]\\d+$";
			java.util.regex.Pattern pattern = java.util.regex.Pattern.compile(twCertiFmt);
			java.util.regex.Matcher match = pattern.matcher(logVO.getCertiCode());
			if(match.matches()) {
			CertiCodeCheckResultVO certiCodeVO = certiCodeCI.checkCertiCode(logVO.getCertiCode());
				if(!certiCodeVO.isValidated()){
					msg.add(LogLevel.ERROR, "[CERTI_CODE]", String.format("[身分證字號/統一編號] %s", getEbaoI18NStr("MSG_1265184")));
					pass = Boolean.FALSE;
				}
			}
		}
		if (!pass) {
			msg.add("[PASS]", "未建檔成功");
		}
		return msg;
	}

	/**
	 * CRS身分類別= CRS100 or CRS200 產生警告訊息 MSG-WRN-CRS-005
	 * 
	 * @param logVO [CRSPartyLogVO]
	 * @return resultString  [String]
	 */
	private String validateTempSaveMaintain(CRSPartyLogVO logVO) {

		String resultString = "";
		if (CRSCodeType.CODE_TYPE_PERSON__A.equals(logVO.getCrsType())
				|| CRSCodeType.CODE_TYPE_COMPANY__A.equals(logVO.getCrsType())) {
			resultString = "聲明書完成更新, 此CRS身分類別無需進行自我證明文件輸入";

		}
		return resultString;
	}
	
	/**
	 * 建立回傳參數。註:系統必須存在檔案
	 * @param logVO
	 * @param policyChgId
	 * @param actionType
	 * @return
	 */
	private java.util.Map<String,Object> getPostBackMap(CRSPartyLogForm inputForm, String actionType){
		java.util.Map<String,Object> postBackMap = new java.util.HashMap<String, Object>();
		postBackMap.put("sysCode", inputForm.getSysCode());
		postBackMap.put("inputSource", inputForm.getInputSource());
		postBackMap.put("logId", inputForm.getLogId());
		if(inputForm.getPolicyId() != null) {
			postBackMap.put("policyId", inputForm.getPolicyId());
		}
		if(inputForm.getChangeId() != null) {
			postBackMap.put("changeId", inputForm.getChangeId());
		}
		if(inputForm.getPolicyChgId() != null) {
			postBackMap.put("policyChgId",  inputForm.getPolicyChgId());
		}
		if(inputForm.getCertiCode() != null) {
			postBackMap.put("certiCode",  inputForm.getCertiCode());
		}
		postBackMap.put("actionType", actionType);
		return postBackMap;
	}

	@Override
	protected MultiWarning processWarning(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		CRSPartyLogForm inputForm = (CRSPartyLogForm) form;
		String actionType = inputForm.getActionType();
		CRSPartyLogVO logVO = inputForm.getCrsPartyLogVO();
		MultiWarning warning = new MultiWarning();
		AlterationItemVO alterationItemVO = new AlterationItemVO();
		
		List<String> verifyWarningList = java.util.Arrays.asList(new String[] { CRSCodeCst.WEB_ACTION_TYPE__COMPLETE,
				CRSCodeCst.WEB_ACTION_TYPE__TEMP_SAVE, CRSCodeCst.WEB_ACTION_TYPE__TEMP_SAVE_MAINTAIN });
		
		if (inputForm.getChangeId() != null && inputForm.getPolicyId() != null && inputForm.getPolicyChgId() != null) {
			if (inputForm.getCertiCode() == null){
				inputForm.setCertiCode(logVO.getCertiCode());
			}
			alterationItemVO.setMasterChgId(inputForm.getChangeId());
			alterationItemVO.setPolicyId(inputForm.getPolicyId());
			MultiWarning mw = cSValidationCrsService.validateCRSDiffCerti(alterationItemVO, form, false);
			if (mw != null) {
				warning.addWarning(mw);
				warning.setContinuable(false);
			}
			
			//暫存、暫存並維護自我證明、提交
			if(verifyWarningList.contains(actionType)){
				//PCR[350318] DEV[376275][Ian] e-Approval ITR-1903876_CRS 作業調整與通報需求(phase II) -保全變更CRS身份及保全輸入檢核
				//檢核當次建立CRS記錄的聲明人姓名與ID未與保單任一關係人(包含要保人、被保人、受益人、法定代理人)相同時 ，須顯示錯誤訊息
				mw = crsPartyLogService.validateCrsDeclarant(logVO);
				if(mw != null){
					warning.addWarning(mw);
					warning.setContinuable(Boolean.FALSE);
				}
			}
		}
		
		if (!verifyWarningList.contains(actionType)) {
			return warning;
		}
		
		// 1.提交
		if (CRSCodeCst.WEB_ACTION_TYPE__COMPLETE.equals(actionType)) {
			// MSG-WRN-CRS-001 提示身分聲明對應的未齊全資料，檢核CRS身份類別和檢附文件是否檢附齊全，未齊全要顯示[警告]訊息
			String valResultStr = crsPartyLogService.validateDocsByCrsType(logVO); 
			if (StringUtils.isNotBlank(valResultStr)) {
				warning.addWarning(valResultStr);
				warning.setContinuable(Boolean.TRUE);
			}
		}

		// 2.暫存並維護自我證明
		if (CRSCodeCst.WEB_ACTION_TYPE__TEMP_SAVE_MAINTAIN.equals(actionType)) {
			String valResultStr = this.validateTempSaveMaintain(logVO);
			if (StringUtils.isNotBlank(valResultStr)) {
				warning.addWarning(valResultStr);
				warning.setContinuable(Boolean.TRUE);
			}
		}
		
		return warning;
	}
	
	/**
	 * 取得eBao i18n的STR_ID對應之繁體中文字串
	 * 
	 * @param strId[String]
	 * @return String
	 */
	private String getEbaoI18NStr(String strId) {
		return StringResource.getStringData(strId, AppContext.getCurrentUser().getLangId());
	}
	
}
