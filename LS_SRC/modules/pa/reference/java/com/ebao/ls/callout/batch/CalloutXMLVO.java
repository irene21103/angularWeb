package com.ebao.ls.callout.batch;

import java.util.Map;

import com.ebao.foundation.commons.exceptions.GenericException;

public interface CalloutXMLVO {
    public abstract String parse(java.util.List<Map<String, Object>> mapList) throws GenericException;
}
