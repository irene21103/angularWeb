/**
 * <p>Title: MaturityPoolAction </p>		
 * <p>Description: This class Maturity Pool 's action class,
 *    it implement maturity pool search info.</p>
 * <p>Copyright: Copyright (c) 2002	</p>
 * <p>Company: 	eBaoTech			</p>
 * <p>Create Time:		2005/04/11	</p> 
 * @author simen.li
 * @version 1.0
 */
package com.ebao.ls.srv.ctrl;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.ls.pub.CodeCst;
import com.ebao.ls.srv.ds.SurvivalRepayService;
import com.ebao.pub.framework.GenericAction;

public class MaturityPoolAction extends GenericAction {

  /**
   * <p>Description: process some action </p>
   * @param mapping
   * @param form,
   * @param request
   * @param response 
   * @return ActionForward
   * @throws Exception
   * <P>Create time: 2005/4/11</P>
   */
  public ActionForward process(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {
    // TODO Auto-generated method stub

    MaturityPoolForm form1 = (MaturityPoolForm) form;

    String actionType = form1.getActionType();
    String policyNumber = form1.getPolicyNumber();
    String caseNumber = form1.getCaseNumber();
    String lifeAssured = form1.getLifeAssured();
    String idNumber = form1.getIdNumber();
    String batchNumber = form1.getBatchNumber();
    String pageNo = form1.getPageNo();
    Integer pageSize = Integer.valueOf(Integer.parseInt(form1.getPageSize()));

    /*
     
     */

    Integer first = Integer.valueOf((Integer.parseInt(form1.getPageNo()) - 1)
        * pageSize.intValue());
    Integer count;
    //require by mark,entry maturity update ,don't search result 
    // so clear this criteria || "".equals(actionType
    if ("search".equals(actionType)) {
      count = survivalRepayDS.findMaturityPoolInfoCount(policyNumber,
          caseNumber, lifeAssured, idNumber, batchNumber, Long
              .valueOf(CodeCst.LIABILITY__MATURITY));
      /*
       String policyNumber,
       String caseNumber, String lifeAsured, String idNumber,
       String batchNumber, Integer first ,Integer pageSize
       */
      List maturityPools = survivalRepayDS.findMaturityPoolInfo(policyNumber,
          caseNumber, lifeAssured, idNumber, batchNumber, first, pageSize);

      form1.setAllMaturityLst(maturityPools);
      request.setAttribute("pageSize", form1.getPageSize());
      request.setAttribute("pageNo", pageNo);
      request.setAttribute("recordCount", String.valueOf(count));
      request.setAttribute("maturityPoolInfos", maturityPools);
    }
    return mapping.findForward("maturityPool");

  }

  @Resource(name = SurvivalRepayService.BEAN_DEFAULT)
  private SurvivalRepayService survivalRepayDS;

}
