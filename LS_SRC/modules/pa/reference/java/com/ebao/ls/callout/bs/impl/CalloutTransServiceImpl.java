package com.ebao.ls.callout.bs.impl;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.collections4.CollectionUtils;

import com.ebao.foundation.common.lang.exception.ExceptionUtils;
import com.ebao.foundation.commons.exceptions.AppException;
import com.ebao.foundation.core.logging.Log;
import com.ebao.foundation.module.db.hibernate.HibernateSession3;
import com.ebao.ls.callout.bs.CalloutOnlineService;
import com.ebao.ls.callout.bs.CalloutTransService;
import com.ebao.ls.callout.data.CalloutAskRelDao;
import com.ebao.ls.callout.data.CalloutQuestionnaireDao;
import com.ebao.ls.callout.data.CalloutTransDao;
import com.ebao.ls.callout.data.CalloutTypeRelDao;
import com.ebao.ls.callout.data.bo.CalloutAskRel;
import com.ebao.ls.callout.data.bo.CalloutQuestionnaire;
import com.ebao.ls.callout.data.bo.CalloutTrans;
import com.ebao.ls.callout.data.bo.CalloutTypeRel;
import com.ebao.ls.callout.vo.CalloutOnlineVO;
import com.ebao.ls.callout.vo.CalloutQuestionnaireVO;
import com.ebao.ls.callout.vo.CalloutTransVO;
import com.ebao.ls.callout.vo.EVoiceResultVO;
import com.ebao.ls.cmu.event.CalloutAdded;
import com.ebao.ls.cmu.event.CalloutUpdated;
import com.ebao.ls.cs.ci.CSCI;
import com.ebao.ls.foundation.service.GenericServiceImpl;
import com.ebao.ls.pa.nb.util.UnbRoleType;
import com.ebao.ls.pa.pub.ci.PolicyCI;
import com.ebao.ls.pa.pub.service.PolicyService;
import com.ebao.ls.pty.ds.DeptService;
import com.ebao.ls.pty.ds.EmployeeService;
import com.ebao.ls.pty.vo.EmployeeVO;
import com.ebao.ls.pub.CodeCst;
import com.ebao.ls.pub.cst.CalloutTargetType;
import com.ebao.ls.pub.cst.NBInsuredCategory;
import com.ebao.ls.ws.ci.ecp.EcpWSCI;
import com.ebao.ls.ws.ci.evoice.EVoiceWSCI;
import com.ebao.ls.ws.vo.ecp.WsEbaoCalloutOnlineRqVO;
import com.ebao.ls.ws.vo.ecp.WsEbaoCalloutOnlineRsVO;
import com.ebao.pub.event.EventService;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.i18n.lang.CodeTable;
import com.ebao.pub.i18n.lang.StringResource;
import com.ebao.pub.util.StringUtils;
import com.ebao.pub.util.json.DateUtils;
import com.mongodb.util.Hash;

public class CalloutTransServiceImpl extends GenericServiceImpl<CalloutTransVO, CalloutTrans, CalloutTransDao> implements CalloutTransService {
	    
    @Resource(name = PolicyService.BEAN_DEFAULT)
    private PolicyService poiclicyservice;

    @Resource(name = PolicyCI.BEAN_DEFAULT)
    private PolicyCI policyci;
    
    @Resource(name = CSCI.BEAN_DEFAULT)
    private CSCI csci;

    @Resource(name = EventService.BEAN_DEFAULT)
    private EventService eventService;

    @Resource(name = CalloutTypeRelDao.BEAN_DEFAULT)
    private CalloutTypeRelDao calloutTypeRelDao;

    @Resource(name = CalloutAskRelDao.BEAN_DEFAULT)
    private CalloutAskRelDao calloutAskRelDao;
    
    @Resource(name = CalloutOnlineService.BEAN_DEFAULT)
    private CalloutOnlineService calloutOnlineService;
    
    @Resource(name = EVoiceWSCI.BEAN_DEFAULT)
    private EVoiceWSCI eVoiceWSCI;
    
    @Resource(name = EmployeeService.BEAN_DEFAULT)
    private EmployeeService empServ;
    
    @Resource(name = CalloutQuestionnaireDao.BEAN_DEFAULT)   
    private CalloutQuestionnaireDao calloutQuestionnaireDao;

    @Resource(name = DeptService.BEAN_DEFAULT)
    DeptService deptService;

    @Resource(name = EcpWSCI.BEAN_DEFAULT)
    private EcpWSCI ecpWSCI;
    
    private DecimalFormat df = new DecimalFormat("00000000");
    private DecimalFormat dfUNB = new DecimalFormat("00");
    
    private static Map<String, Integer> maxNumMap = new HashMap<String, Integer>();
    
    private final Log log = Log.getLogger(CalloutTransServiceImpl.class);
    
    private void syso(Object msg){
        log.info("[SYSOUT][UC-CMN-TPI-003]" +  msg);
    }
    
    private void syso(String layout, Object... msg){
        syso(String.format(layout, msg));
    }
    
    @Override
    protected CalloutTransVO newEntityVO() {
        return new CalloutTransVO();
    }
    
    /**
     * <p>Description : 取得電訪編號(synchronized) </p>
     * <p>Created By : Calvin Chen</p>
     * <p>Create Time : 2018年1月2日</p>
     * @param identity
     * @return
     */
	public String getCalloutNum(String identity) {
		return getCalloutNum(identity, false);
	}

	public String getCalloutNum(Long policyId, String identity,boolean isError) {
		// 非UNB走原邏輯
		if(!StringUtils.isNullOrEmpty(identity) && identity.indexOf("UNB") == -1) {
			return getCalloutNum(identity, isError);
		}
		return getUNBCalloutNum(policyId, identity, isError);
	}
	
	synchronized public String getUNBCalloutNum(Long policyId, String identity, boolean isError) {
		Integer currentNum;

		if (!maxNumMap.containsKey(identity) || isError) {
			currentNum = dao.queryMaxCalloutNum(policyId, identity);
		} else {
			currentNum = maxNumMap.get(identity);
		}
		currentNum += 1;
		maxNumMap.put(identity, currentNum);
		return String.format("%s%s", identity, dfUNB.format(currentNum));
	}

	synchronized public String getCalloutNum(String identity, boolean isError) {
		Integer currentNum ;
		
		if (!maxNumMap.containsKey(identity) || isError) {
			currentNum = dao.queryMaxCalloutNum(identity);
		} else {
			currentNum = maxNumMap.get(identity);
		}
		
		maxNumMap.put(identity, currentNum + 1);
		return String.format("%s%s", identity, df.format(currentNum + 1));
	}
    
    public List<CalloutTransVO> query(CalloutTransVO calloutTrans, Object... objects) {
        CalloutTrans bo = new CalloutTrans();
        bo.copyFromVO(calloutTrans, true, true);
        List<CalloutTransVO> list = super.convertToVOList(getDao().query(bo, objects));
        return queryAskRelsAndTypeRels(list);
    }
    
    public List<CalloutTransVO> queryPOSWithoutChangeID(CalloutTransVO calloutTrans) {
        CalloutTrans bo = new CalloutTrans();
        bo.copyFromVO(calloutTrans, true, true);
        List<CalloutTransVO> list = super.convertToVOList(getDao().query(bo, CalloutTransDao.SEARCH_CALLOUT_NUM_LIKE_POS));
        return queryAskRelsAndTypeRels(list);
    }

    @Override
    public List<Map<String, Object>> queryListMap(CalloutTransVO calloutTrans, Object... objects) {
        CalloutTrans bo = new CalloutTrans();
        bo.copyFromVO(calloutTrans, true, true);
        List<Map<String, Object>> list = super.getDao().queryListMap(bo, objects);
        List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
        boolean showParentPolucyId = false;
        for(Object object:objects){
            if(object instanceof String){
            	if(SHOW_PARENT_POLICYID.equals(object)){
            		showParentPolucyId = true;
            	}
            }
        }
        for (Map<String, Object> map : list) {
			//PCR 382493 離職業務員內控檢核 PHASE II
        	// 若為以下任一情境，無電訪對象紀錄(callout_target_type為NULL)
        	// 1.同[XXXXXX ]  保單電訪內容 :XXXXXX=  PARENT_POLICYID 有值  
        	// 2.[人工寄發權益通知函]  CALLOUT_IMPORTANT_SENDER_FLAG   = "Y"
        	// 3.不需電訪原因：01- 業務員仍在職 NO_CALLOUT_REASON = "01"
        	boolean ingnoreCallOut = false;
        	String sendFlag = MapUtils.getString(map, "CALLOUT_IMPORTANT_SENDER_FLAG"); 	//[人工寄發權益通知函]
        	String calloutReason = MapUtils.getString(map, "NO_CALLOUT_REASON"); 			//不需電訪原因
        	        	
        	if ( (sendFlag !=null && sendFlag.equals(CodeCst.YES_NO__YES))
            		||  (calloutReason !=null && calloutReason.equals("01") )
            		|| !StringUtils.isNullOrEmpty(MapUtils.getString(map, "PARENT_POLICYID", ""))){
        		ingnoreCallOut = true; 		//無電訪對象紀錄不需回傳 t_callout_trans 清單
        	}
			//有電訪對象紀錄需回傳 t_callout_trans 清單 
            if (!ingnoreCallOut || showParentPolucyId) {
            	Date calloutTime = (Date) map.get("CALLOUT_TIME");
                if (calloutTime != null) {
                    map.put("CALLOUT_TIME_HHMMSS", DateUtils.format(calloutTime, "HH:mm:SS"));
                }
                String typeRelStr = "";
                List<CalloutTypeRel> typeRels = calloutTypeRelDao.findByCalloutId(map.get("LIST_ID").toString());
                for (CalloutTypeRel typeRel : typeRels) {
                    typeRelStr += "," + typeRel.getTypeCode();
                }
                map.put("CALLOUT_TYPE_STR", typeRelStr);
                
                String calloutDocketNum = MapUtils.getString(map, "CALLOUT_DOCKET_NUM");
                String callerParty = map.get("CALLER_PARTY") != null ? map.get("CALLER_PARTY").toString() : null;
                Long empId = StringUtils.isNullOrEmpty(callerParty) ? null : Long.parseLong(callerParty);
                if(org.apache.commons.lang3.StringUtils.isNotEmpty(calloutDocketNum)){
                    String callerName = map.get("EVOICE_CALLER_NAME") == null ? "" : map.get("EVOICE_CALLER_NAME").toString();
                    map.put("CALLER_PARTY", callerName);
                } else if (empId != null) {
                    EmployeeVO empData  = empServ.getEmployeebyId(empId);
                    if(empData != null){
                    	String empInfo = String.format("%s/%s", empData.getRealName(), MapUtils.getString(map, "CALLER_EXT"));
                        String deptName = deptService.getDeptById(empData.getDeptId()).getDeptName();//<!-- PCR_382493 新增部門顯示-->
                        map.put("CALLER_PARTY", empInfo+"/"+deptName);
                    }
                }
                resultList.add(map);
            }
        }

        return resultList;
    }

    @Override
    public List<Map<String, Object>> findPolicyInfoByPolicyId(Long policyId) {
        return dao.findPolicyInfoByPolicyId(policyId);
    }
    
    @Override
    public List<Map<String, Object>> findLastTransDateBypolicyID(Long policyId) {
        return dao.findCalloutTransLastTransDate(policyId);
    }

    @Override
    public List<Map<String, Object>> findCalloutTransByCalloutNum(String calloutNum) {
        return dao.findCalloutTransByCalloutNum(calloutNum);
    }

    @Override
    @Deprecated
    public Integer getCurrentCalloutNumberIndex(String middleFix) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(AppContext.getCurrentUserLocalTime());
        String number = getDao().newCalloutNum(middleFix, cal);
        if (number != null && number.length() > 6) {
            number = number.substring(6);
        } else {
            number = "0";
        }
        Integer numberNow = (Integer.parseInt(number));
        return numberNow;
    }
    
    @Override
    @Deprecated
    public String getCurrentCalloutNumber(String middleFix, Integer index) {
        return getNextCalloutNum(AppContext.getCurrentUserLocalTime(), middleFix, index);
    }

    @Override
    public Integer getCalloutTransCount(Long policyId, Integer liabilityStatus) {
        List<Map<String, Object>> listMap = dao.findCalloutTransCountbyPolicy(policyId, liabilityStatus);
        if (listMap.size() > 0) {
            return ((BigDecimal) listMap.get(0).get("TRANS_COUNT")).intValue();
        } else {
            return 0;
        }
    }

    @Override
    public boolean getCalloutResult(Long policyId, Long changeId) {
        return getCalloutResult(policyId, changeId, null);
    }
    
    @Override
    public boolean getCalloutResult(Long policyId, Long changeId, String typeCode) {
        return dao.getCalloutResultbyPolicyIdchangeId(policyId, changeId,typeCode );
    }
    
    @Override
    public void deleteByListId(Long listId) {
        calloutTypeRelDao.deleteByCalloutId(listId);
        calloutAskRelDao.deleteByCalloutId(listId);
        super.remove(listId);
    }

    @Override
    public List<Map<String, Object>> getCalloutRecordByCalleeCertiCodeWithDate(
                    CalloutTransVO queryVO, Date startDate, Date endDate) {
        CalloutTrans bo = new CalloutTrans();
        bo.copyFromVO(queryVO, true, true);
        List<Map<String, Object>> results = super.getDao().getCalloutRecordByCalleeCertiCodeWithDate(bo, startDate, endDate);
        for( int i=0 ; i<results.size() ; i++){
            if(results.get(i).get("LIST_ID")!=null){
                results.get(i).put("CALLOUT_TYPE", calloutTypeRelDao.findByCalloutIdStr(results.get(i).get("LIST_ID").toString()));
            }
        }
        return results;
    }
    
    @Override
    public CalloutOnlineVO bindSaveVO(CalloutTransVO calloutTransVO,
                    Boolean toEvoice, Boolean isCRM) {
    	// PCR355468　UNB修改電訪任務編碼規則
        //Date now = AppContext.getCurrentUserLocalTime();
        //String calloutIdentity = getCalloutIdentity(now, (toEvoice ? "UNB" : "POS"));
    	
    	// 依UAT-issue516548新增電訪任務編號CRM中綴邏輯
    	String midFix;
    	if (toEvoice == true && isCRM == false){
    		midFix = "UNB";
    	} else if (toEvoice == false && isCRM == true){
    		midFix = "CRM";
    	} else {
    		midFix = "POS";
    	}
        String calloutIdentity = getCalloutIdentity(calloutTransVO, midFix);
        
        Long policyId = calloutTransVO.getPolicyId();
        if(!toEvoice){
            calloutTransVO.setTransCount(null);
        } 
        /* 儲存電訪交易資料表 */
        CalloutOnlineVO calloutVO = new CalloutOnlineVO();
        try{
            calloutTransVO = saveVO(calloutTransVO, calloutIdentity);
            calloutVO.setCalloutNum(calloutTransVO.getCalloutNum());
        }catch (Exception ex){
            String policyCode = policyci.findPolicyCodeByPolicyId(policyId);
            Map<String, String> message = new java.util.HashMap<String, String>();
            message.put("calleeCertiCode", calloutTransVO.getCalleeCertiCode());
            message.put("policyCode",  policyCode);
            AppException exception = new AppException(30710020013L);
            exception.addMessageParameter(message);
            syso("[ERROR][CalleeCertiCode := %s, PolicyCode := %s] %s", calloutTransVO.getCalleeCertiCode(), policyCode, ExceptionUtils.getFullStackTrace(ex) );
            throw exception;
        }
        
        if (!toEvoice) {
			// if (true) { // TODO DEV has no ECP env
            return calloutVO;
        }
        
        /* 發送電訪交易至eVoice */
        try{
            EVoiceResultVO eVoiceResultVO = onlineSendToEvoice(calloutTransVO);
            calloutVO.seteVoiceResult(eVoiceResultVO);
            eventService.publishEvent(new CalloutAdded(calloutTransVO));
        }catch(AppException ex){
            String layout = StringResource.getStringData("MSG_1262910", AppContext.getCurrentUser().getLangId());
            String infoMsg = String.format(layout, calloutTransVO.getCalleeName(), calloutTransVO.getCalleeCertiCode(), ex.getMessage());
            calloutVO.setEVoiceResult(CodeCst.CALLOUT_SENT_EVOICE_SYSTEM_ERROR, infoMsg);
        }
        
        return calloutVO;
    }

    
    @Override
    @Deprecated
    public java.util.List<CalloutOnlineVO> saveVOList(List<CalloutTransVO> calloutTransVOList, boolean isSendEVoice) throws Exception {
        Date now = AppContext.getCurrentUserLocalTime();
        String calloutIdentity = getCalloutIdentity(now, (isSendEVoice ? "UNB" : "POS"));
        java.util.List<CalloutOnlineVO> resultVOList = new java.util.LinkedList<CalloutOnlineVO>();
        if(CollectionUtils.isEmpty(calloutTransVOList))
            return resultVOList;
        /* 發送 eVoice, 填寫 t_callout_trans */
        for (CalloutTransVO vo : calloutTransVOList) {
            if (vo == null) 
                continue;
            if(!isSendEVoice){
                vo.setTransCount(null);
            }
            CalloutOnlineVO calloutVO = new CalloutOnlineVO();
            vo.setCalloutNum(getCalloutNum(calloutIdentity));
            vo = saveVO(vo);
            calloutVO.setCalloutNum(vo.getCalloutNum());
            if(isSendEVoice){
                try{
                    EVoiceResultVO eVoiceResultVO = onlineSendToEvoice(vo);
                    calloutVO.seteVoiceResult(eVoiceResultVO);
                    eventService.publishEvent(new CalloutAdded(vo));
                }catch(AppException ex){
                    calloutVO.setEVoiceResult(CodeCst.CALLOUT_SENT_EVOICE_SYSTEM_ERROR, ex.getMessage());
                }
            }
            resultVOList.add(calloutVO);
        }
        return resultVOList;
    }
    
    public EVoiceResultVO onlineSendToEvoice(CalloutTransVO vo) {
        WsEbaoCalloutOnlineRqVO rq = new WsEbaoCalloutOnlineRqVO();
        WsEbaoCalloutOnlineRsVO rs=null;
        EVoiceResultVO resultVO = null;
        String resultMsg = null;
        String resultDetail = null;
    	try {
        	rq = calloutOnlineService.prepareVO(vo);
        	syso("[INT-CMN-180][eBao -> eVoice] input params: callout_num = %s, policy_code = %s , CalleeCertiCode = %s ", vo.getCalloutNum(), rq.getPolicyCode(), rq.getCalleeCertiCode());
        	//PCR355468 新客服系統改call ECP 
        	//rs = eVoiceWSCI.calloutOnlineMission(rq);
        	rs = ecpWSCI.calloutOnlineMission(rq);
        	
        	if( rs == null)
        	    throw new AppException(20413020011L);//無效XML
        	
    	   resultVO = new EVoiceResultVO();
    	   resultMsg = rs.getCalloutTransResult();
    	   resultDetail = rs.getCalloutTransResultDetail();
    	   resultVO.setCalloutTransResult(resultMsg);
    	   resultVO.setCalloutTransResultDetail(resultDetail);
           syso("[INT-CMN-180][eBao -> eVoice] output result: resultMsg = %s, resultDetail = %s", resultMsg, resultDetail);
    	} catch (Exception e) {
    	    syso("[ERROR][INT-CMN-180][WebService][eBao -> eVoice] errorStack =\n" + ExceptionUtils.getFullStackTrace(e));
    	    syso("[ERROR][INT-CMN-180][WebService][eBao -> eVoice] returnMsg = %s , resultDetail = %s", resultMsg, resultDetail);
    	    //throw new AppException(30710010001L);//eVoice 連線異常
    		throw new AppException(30600220002L);//eVoice 連線異常
    	}
        syso("[WebService][eVoice -> eBao] returnMsg = %s , resultDetail = %s", resultMsg, resultDetail);
        if(resultVO !=null && !CodeCst.CALLOUT_SENT_EVOICE_SUCCESS.equals(resultVO.getCalloutTransResult())){
            Map<String, String> message = new java.util.HashMap<String, String>();
            message.put("errorCode", resultVO.getCalloutTransResult());
            message.put("errorMsg", resultVO.getCalloutTransResultDetail());
            //AppException exception = new AppException(30710020011L);
            AppException exception = new AppException(30600220001L);
            exception.addMessageParameter(message);
            throw exception;//eVoice 回傳錯誤訊息
        }
        
        return resultVO;
    }
    
    @Override
    public void updateVOList(List<CalloutTransVO> volist, boolean isSendEVoice) throws Exception {
        for (CalloutTransVO vo : volist) {
            if(vo!=null && vo.getListId()!=null){
                // 刪除多對多 詢問事項
                calloutAskRelDao.deleteByCalloutId(vo.getListId());
                // 刪除多對多 電訪類型
                calloutTypeRelDao.deleteByCalloutId(vo.getListId());
                CalloutTrans bo = dao.findUniqueByProperty("calloutNum", vo.getCalloutNum());
                bo.copyFromVO(vo, true, false);
                dao.saveorUpdate(bo);
                saveAskRels(vo);
                saveTypeRels(vo);
                if (isSendEVoice) {
                    eventService.publishEvent(new CalloutUpdated(vo));
                }
            }
        }
    }
    
    public void updateVO(CalloutTransVO vo, boolean isSendEVoice) throws Exception {
        if(vo == null || vo.getListId() == null)
            return ;
        // 刪除多對多 詢問事項
        calloutAskRelDao.deleteByCalloutId(vo.getListId());
        // 刪除多對多 電訪類型
        calloutTypeRelDao.deleteByCalloutId(vo.getListId());
        
        CalloutTrans bo = dao.findUniqueByProperty("calloutNum", vo.getCalloutNum());
        bo.copyFromVO(vo, true, true);
        dao.saveorUpdate(bo);
        saveAskRels(vo);
        saveTypeRels(vo);
        if (isSendEVoice) {
            eventService.publishEvent(new CalloutUpdated(vo));
        }
    }
    @Override
    public CalloutTransVO saveVO(CalloutTransVO vo) {
        org.hibernate.Session session = getDao().getSession();
        
        CalloutTrans data = new CalloutTrans();
        data.copyFromVO(vo, Boolean.TRUE, Boolean.TRUE);
        
        // 儲存電訪主表
        getDao().save(data);
        
        vo.setListId(data.getListId());
        
        // 儲存多對多 詢問事項
        saveAskRels(vo);
        // 儲存多對多 電訪類型
        saveTypeRels(vo);
        // 儲存一對多 電訪原因/異常表徵  
        List<CalloutQuestionnaireVO> list = vo.getQuestionnaireList();
		// 若 list == null,from ECP without remarks; 若list != null, from eBao
		if (!StringUtils.isEmpty(vo.getCalloutQuestionnaireStr())) {
			if (list == null || list.isEmpty()) {
				saveCalloutQuestionnaire(vo);
			} else {
				// 重押 CalloutTrans.CalloutQuestionnaireStr
				StringBuilder sb = new StringBuilder();
				for(CalloutQuestionnaireVO quesVO: list){
					sb.append(quesVO.getCalloutReasonCode());
					if(!StringUtils.isNullOrEmpty(quesVO.getExceptionItemCode())){
						sb.append("-").append(quesVO.getExceptionItemCode());
					}
					sb.append(";");
				}
				String calloutQuestionnaireStr = sb.toString().substring(0, sb.length()-1);
				data.setCalloutQuestionnaireStr(calloutQuestionnaireStr);
				getDao().saveorUpdate(data);
			}
		}

        session.flush();
        session.clear();
        
        CalloutTransVO newVO = new CalloutTransVO();
        data.copyToVO(newVO, Boolean.TRUE);
        return newVO;
    }
    
    private static final Integer MAX_TRY_NUM = 3;
    
    /**
     *  配合多 AP Instance (新增取號Retry機制)
     */
    synchronized public CalloutTransVO saveVO(CalloutTransVO vo, String calloutIdentity){

        Integer tryNum = 0;
        //UNB邏輯，每次先清空map
     	if(!StringUtils.isNullOrEmpty(calloutIdentity) && calloutIdentity.indexOf("UNB") != -1) {
     		 maxNumMap = new HashMap<String, Integer>();
     	}
        String policyCode = policyci.findPolicyCodeByPolicyId(vo.getPolicyId());
        Exception excepts = null;
        
        while(tryNum < MAX_TRY_NUM) {

            try {
            	
            	// 取電訪編號  PCR355468 UNB修改電訪任務編碼規則 
            	String calloutNum = getCalloutNum(vo.getPolicyId(),calloutIdentity, tryNum > 0 ? true : false);
                vo.setCalloutNum(calloutNum);
                // 設置每筆明細的 CalloutNum
				List<CalloutQuestionnaireVO> list = vo.getQuestionnaireList();
				if (list != null && !list.isEmpty()) {
					for (CalloutQuestionnaireVO quesVO : list) {
						quesVO.setCalloutNum(calloutNum);
					}
				}
            	return saveVO(vo);
            	
            } catch(Exception e) {
            	syso("[ERROR] Save Error. Try to refresh CalloutNum Sequence! Times:" + tryNum +"!");
            	HibernateSession3.clear();
            	excepts = e;
            	tryNum ++;
            	
            }
            
        }

        syso("[ERROR] Save Error. Try to refresh CalloutNum Sequence Failed! (TOO MANY TIMES)");

        Map<String, String> message = new java.util.HashMap<String, String>();
        message.put("calleeCertiCode", vo.getCalleeCertiCode());
        message.put("policyCode",  policyCode);
        AppException exception = new AppException(30710020013L);
        exception.addMessageParameter(message);
        exception.setStackTrace(excepts.getStackTrace());
        syso("[ERROR][CalleeCertiCode := %s, PolicyCode := %s] %s", vo.getCalleeCertiCode(), policyCode, ExceptionUtils.getFullStackTrace(excepts) );
        throw exception;
        
    }

    private void saveTypeRels(CalloutTransVO vo) {
        if (!StringUtils.isNullOrEmpty(vo.getCalloutTypesStr())) {
            String[] x = vo.getCalloutTypesStr().split(",");
            for (String string : x) {
                if (string.length() > 0) {
                    CalloutTypeRel type = new CalloutTypeRel();
                    type.setTypeCode(string);
                    type.setCalloutId(vo.getListId());
                    calloutTypeRelDao.save(type);
                }
            }
        }
    }

    private void saveAskRels(CalloutTransVO vo) {
        if (!StringUtils.isNullOrEmpty(vo.getCalloutAsksStr())) {
        	String askRadioItemStr = vo.getCalloutAskRadioAnsStr();
        	String[] askRadioItem = null;
        	if(null != askRadioItemStr && !askRadioItemStr.equals("")){
        		askRadioItem = askRadioItemStr.split(",");
        	}
            String[] x = vo.getCalloutAsksStr().split(",");
            for (String string : x) {
                if (string.length() > 0) {
                    CalloutAskRel ask = new CalloutAskRel();
                    ask.setAskCode(string);
                    ask.setCalloutId(vo.getListId());
                    //如果有Radio單選項
                    if(null != askRadioItem){
                    	for(String radioItem:askRadioItem){
                        	if(radioItem.contains("-")){
                        		String[] radiolist = radioItem.split("-");
                        		if(radiolist[0].equals(string)){
                        			ask.setReply(radiolist[1]);
                        		}
                        	}
                        }
                    }
                    calloutAskRelDao.save(ask);
                }
            }
        }
    }
    
	private void saveCalloutQuestionnaire(CalloutTransVO vo) {
		if (!StringUtils.isNullOrEmpty(vo.getCalloutQuestionnaireStr())) {
			String[] calloutQuestionnaireStrLst = vo.getCalloutQuestionnaireStr().split(";");
			String[] calloutQuestionnairelst = null;
			StringBuilder calloutReasonItem = new StringBuilder();
			StringBuilder execptionItem = new StringBuilder();
			for (String string : calloutQuestionnaireStrLst) {
				calloutReasonItem.setLength(0);
				execptionItem.setLength(0);
				if (string.length() > 0) {
					CalloutQuestionnaire calloutQuestionnaire = new CalloutQuestionnaire();
					calloutQuestionnaire.setCalloutNum(vo.getCalloutNum());
					calloutQuestionnairelst = string.split("-");
					calloutReasonItem.append(CodeTable.getCodeDesc("T_CALLOUT_REASON_CODE", calloutQuestionnairelst[0]));
					if (calloutQuestionnairelst.length >= 2) {
						calloutQuestionnaire.setExceptionItemCode(calloutQuestionnairelst[1]);
						calloutReasonItem.append("-");
						calloutReasonItem.append(CodeTable.getCodeDesc("T_CALLOUT_EXCEPTION_ITEM_CODE", string));
					}
					calloutQuestionnaire.setCalloutReasonCode(calloutQuestionnairelst[0]);
					calloutQuestionnaire.setReasonItem(calloutReasonItem.toString());
					calloutQuestionnaireDao.save(calloutQuestionnaire);
				}
			}
		}
	}
    
	@Override
	public List<CalloutQuestionnaireVO> getCalloutQuestionnaireByCalloutNum(
			String calloutNum) {
		List<CalloutQuestionnaire> boList = calloutQuestionnaireDao
				.findByProperty("calloutNum", calloutNum);
		List<CalloutQuestionnaireVO> voList = new ArrayList<CalloutQuestionnaireVO>();
		for (CalloutQuestionnaire bo : boList) {
			CalloutQuestionnaireVO vo = new CalloutQuestionnaireVO();
			bo.copyToVO(vo, true);
			voList.add(vo);
		}
		return voList;
	}
	
	/*
	 * 
	 * */
	public List<CalloutQuestionnaireVO> parseCalloutQuestionnaire(
			String calloutQuestionnaireStr) {
		List<CalloutQuestionnaireVO> resultList = null;
		if (!StringUtils.isNullOrEmpty(calloutQuestionnaireStr)) {
			resultList = new ArrayList<CalloutQuestionnaireVO>();
			// 電訪原因集合
			String[] calloutQuestionnaireStrLst = calloutQuestionnaireStr
					.split(";");
			String[] splitStrs = null;
			StringBuilder calloutReasonItem = new StringBuilder(); // eBao顯示的電訪原因跟異常表徵，用-分隔(CODE_ORDER)

			for (String string : calloutQuestionnaireStrLst) {
				calloutReasonItem.setLength(0);

				if (string.length() > 0) {
					CalloutQuestionnaireVO vo = new CalloutQuestionnaireVO();
					splitStrs = string.split("-");

					String reasonCode = splitStrs[0]; // 電訪原因
					String exceptionItemCode = null; // 異常項目

					// value Mapping 成畫面顯示的 Order
					calloutReasonItem.append(CodeTable.getCodeDesc(
							"T_CALLOUT_REASON_CODE", reasonCode));
					if (splitStrs.length >= 2) {
						exceptionItemCode = splitStrs[1];
						calloutReasonItem.append("-");
						calloutReasonItem
								.append(CodeTable.getCodeDesc(
										"T_CALLOUT_EXCEPTION_ITEM_CODE",
										string));
					}
					// 此時常未取得 calloutNum 
					// vo.setCalloutNum(calloutNum);
					vo.setCalloutReasonCode(reasonCode);
					vo.setExceptionItemCode(exceptionItemCode);
					vo.setReasonItem(calloutReasonItem.toString());

					resultList.add(vo);
				}

			}
		}

		return resultList;
	}
    @Deprecated
    /**
     * <p>Description : 為了避免存檔衝突，在Trigger裡取流水號</p>
     * <p>Created By : Calvin Chen</p>
     * <p>Create Time : Jan 4, 2017</p>
     * @param now
     * @param middleFix
     * @param numberNow
     * @return
     */
    private String getNextCalloutNum(Date now, String middleFix, Integer numberNow) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(now);
        return String.format("%s%s%s", ("" + (cal.get(Calendar.YEAR) - 1911)), middleFix, df.format(numberNow));
    }
    
    /**
     * <p>Description : 取得CalloutNum的前綴字串</p>
     * <p>Created By : Calvin Chen</p>
     * <p>Create Time : Jan 4, 2017</p>
     * @param now
     * @param middleFix
     * @return
     */
    @Override
    public String getCalloutIdentity(Date now, String middleFix) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(now);
        Integer twy = cal.get(Calendar.YEAR) - 1911;//BR-CMN-TPI-009
        return String.format("%03d%s", twy, middleFix);
    }
    
    /**
     * <p>Description : 取得CalloutNum的前綴字串，UNB修改電訪任務編碼規則</p>
     * <p>Created By : Curious Wang</p>
     * <p>Create Time : May 11, 2020</p>
     * @param CalloutTransVO
     * @param middleFix
     * @return
     */
    @Override
	public String getCalloutIdentity(CalloutTransVO vo, String middleFix) {
		if (!"UNB".equals(middleFix)) {
			return getCalloutIdentity(AppContext.getCurrentUserLocalTime(), middleFix);
		}
		// 保單號碼+部門別+角色(O-要保人；I-被保險人；L-法定代理人)+身分別(眷屬附約與主被保險人關係)
		String policyCode = policyci.findPolicyCodeByPolicyId(vo.getPolicyId());
		String roleType = "";
		if (CalloutTargetType.POLICY_HOLDER.equals(vo.getCalloutTargetType())) {
			roleType = UnbRoleType.POLICY_HOLDER.getRoleTypeENG();
		} else if (CalloutTargetType.LEGAL_REPRESENTATIVE.equals(vo.getCalloutTargetType())) {
			roleType = UnbRoleType.LEGAL_REPRESENTATIVE.getRoleTypeENG();
		} else if (CalloutTargetType.PAYEE.equals(vo.getCalloutTargetType())) {
			roleType = UnbRoleType.PAYER.getRoleTypeENG();
		} else if (CalloutTargetType.OTHERS.equals(vo.getCalloutTargetType())) {
			roleType = UnbRoleType.OTHERS.getRoleTypeENG();
		} else {
			roleType = UnbRoleType.INSURED.getRoleTypeENG();
		}
		return String.format("%s%s%s%s", policyCode, middleFix, roleType,
				getNBInsuredCategaryByCalloutTargetType(vo.getCalloutTargetType()));
	}
    
    private List<CalloutTransVO> queryAskRelsAndTypeRels(List<CalloutTransVO> list) {
        for (CalloutTransVO vo : list) {
            String typeRelStr = "";
            String askRelStr = "";
            String asksRadioAnsStr = "";
            List<CalloutTypeRel> typeRels = calloutTypeRelDao.findByCalloutId(vo.getListId());
            for (CalloutTypeRel typeRel : typeRels) {
                typeRelStr += "," + typeRel.getTypeCode();
            }
            List<CalloutAskRel> askRels = calloutAskRelDao.findByCalloutId(vo.getListId());
            for (CalloutAskRel askRel : askRels) {
            	if(null != askRel.getReply()){
            		String asksRadioAns = askRel.getAskCode() + "-" + askRel.getReply();
            		asksRadioAnsStr += "," + asksRadioAns;
            	}
                askRelStr += "," + askRel.getAskCode();
            }
            vo.setCalloutTypesStr(typeRelStr);
            vo.setCalloutAsksStr(askRelStr);
            vo.setCalloutAskRadioAnsStr(asksRadioAnsStr);
        }
        return list;
    }

    @Override
    public void updateCalloutImporterSender(String calloutNum, Date senderDate) {
        dao.updateCalloutImportantSender(calloutNum, senderDate);
    }

    @Override
    public CalloutTransVO findCalloutTransVOByCalloutNum(String calloutNum) {
        List<CalloutTrans> boList = dao.findCalloutTransBOByCalloutNum(calloutNum);
        if(boList.size()>0){
            CalloutTransVO vo = new CalloutTransVO();
            boList.get(0).copyToVO(vo, true);
            return vo;
        }
        return null;
    }

    @Override
    public CalloutTransVO queryTypeByPolicyIdAndChangeID(String policyId, String changeId, String type) {
        if(StringUtils.isNullOrEmpty(policyId)||StringUtils.isNullOrEmpty(changeId)||StringUtils.isNullOrEmpty(type)){
            return null;
        } else {
            CalloutTrans queryBO = new CalloutTrans();
            queryBO.setPolicyId(Long.parseLong(policyId));
            queryBO.setChangeId(Long.parseLong(changeId));
            queryBO.setCalloutTargetType(type);
            List<CalloutTrans> boList = super.getDao().query(queryBO, CalloutTransDao.SEARCH_CALLOUT_TARGET);
            List<CalloutTransVO> voList = queryAskRelsAndTypeRels(super.convertToVOList(boList));
            if(voList.size()>0){
                return voList.get(0);
            }
        }
        return null;
    }

    @Override
    public List<CalloutTransVO> queryListByPolicyIdAndChangeIdAndCalloutTargetType(Long policyId, Long changeId, String type) {
        if(policyId == null || changeId == null || StringUtils.isNullOrEmpty(type)){
            return null;
        } else {
            CalloutTrans queryBO = new CalloutTrans();
            queryBO.setPolicyId(policyId);
            queryBO.setChangeId(changeId);
            queryBO.setCalloutTargetType(type);
            List<CalloutTrans> boList = super.getDao().query(queryBO, CalloutTransDao.SEARCH_CALLOUT_TARGET);
            List<CalloutTransVO> voList = queryAskRelsAndTypeRels(super.convertToVOList(boList));
            if(voList.size()>0){
                return voList;
            }
        }
        return null;
    }

    @Override
    public boolean checkCalloutTransParentPolicyId(Long policyId, Long changeId) {
        if(policyId == null || changeId == null){
            return false;
        } else {
            CalloutTrans queryBO = new CalloutTrans();
            queryBO.setPolicyId(policyId);
            queryBO.setChangeId(changeId);
            List<CalloutTrans> boList = super.getDao().query(queryBO, CalloutTransDao.SEARCH_CALLOUT_PARENT_POLICYID);
            List<CalloutTransVO> voList = queryAskRelsAndTypeRels(super.convertToVOList(boList));
            if(voList.size()>0){
                return true;
            }
        }
        return false;
    }

    @Override
    public String getCalloutNumbersByCalloutListIds(String listIds) {
        return dao.getCalloutNumsByCalloutListIds(listIds);
    }
    
    @Override
    public String getCalloutNumByListId(Long id){
        return dao.getCalloutNumByListId(id);
    }
    /**
     * <p>Description : 取得檔案號基數 </p>
     * <p>Created By : Calvin Chen</p>
     * <p>Create Time : Feb 2, 2017</p>
     * <p> 非BD通路 : 9 - 12 </p>
     * <p> BD通路且為傳統型台幣 : 13 - 16 </p>
     * <p> BD通路且為非傳統型台幣符合被保人年齡、躉繳保費、年繳化保費其中一項 : 1 - 4 </p>
     * <p> BD通路且為非傳統型台幣不符合被保人年齡、躉繳保費、年繳化保費任何一項 : 5 - 8 </p>
     * @param policyId
     * @return
     */
	protected Integer getDocketNumType(Map<String, Object> map) {
		if(!map.isEmpty()) {
			 // BD 通路 
			if(CodeCst.YES_NO__NO.equals(map.get("BD_INDI").toString())) {
				// 9-12
				return 9;
			} else {
				// 傳統型台幣
				if(CodeCst.YES_NO__YES.equals(map.get("TRAN_PROD_INDI").toString()) 
						&& CodeCst.YES_NO__NO.equals(map.get("FORIEGN_MONEY_INDI").toString())) {
					// 13-16
					return 13;
				} else {
					// 1-8
					if(Integer.parseInt(MapUtils.getString(map, "ENTRY_AGE", "0")) >= 70){
						return 5;
					} else if(CodeCst.CHARGE_MODE__SINGLE.equals(map.get("RENEWAL_TYPE")) 
							&& Long.parseLong(map.get("STD_PREM_AN").toString())>1000000 ) {
						return 5;
					} else if(!CodeCst.CHARGE_MODE__SINGLE.equals(map.get("RENEWAL_TYPE")) 
							&& Long.parseLong(map.get("STD_PREM_AN").toString())>300000 ) {
						return 5;
					} else {
						return 1;
					}
				}
			}
		} else {
			return null;
		}
	}

	@Override
	public String getDocketNum(Long policyId, String specialConcernIndi, String categoryIndi) {
		Map<String, Object> map = dao.queryPolicyInfoForDocketNum(policyId);
		Integer no = getDocketNumType(map);
		if(CodeCst.LIABILITY_STATUS__NON_VALIDATE == Integer.parseInt(map.get("LIABILITY_STATE").toString())){
			return getBefDocketNum(no, specialConcernIndi, categoryIndi);
		} 
		boolean isCalloutSuccess = dao.isCalloutSuccessBefContractValidate(policyId);
	    int overHesitationDateIndi = csci.checkCoolingOffByPolicyId(policyId, AppContext.getCurrentUserLocalTime());
	    return getAfrDocketNum(no, isCalloutSuccess, overHesitationDateIndi);
	}
	
	/**
	 * <p>Description : 取得承保前問卷案號 </p>
	 * <p>Created By : Calvin Chen</p>
	 * <p>Create Time : Feb 3, 2017</p>
	 * @param no
	 * @param specialConcernIndi
	 * @param categoryIndi
	 * @return
	 */
	protected String getBefDocketNum(Integer no, String specialConcernIndi, String categoryIndi) {
		if (CodeCst.YES_NO__YES.equals(specialConcernIndi) && CodeCst.YES_NO__YES.equals(categoryIndi)) {
			no += 3;
		} else if (!CodeCst.YES_NO__YES.equals(specialConcernIndi) && CodeCst.YES_NO__YES.equals(categoryIndi)) {
			no += 2;
		} else if (CodeCst.YES_NO__YES.equals(specialConcernIndi) && !CodeCst.YES_NO__YES.equals(categoryIndi)) {
			no += 1;
		}
		return "UNB-Bef-"+String.format("%02d", no);
	}

	/**
	 * <p>Description : 取得承保後問卷案號 </p>
	 * <p>Created By : Calvin Chen</p>
	 * <p>Create Time : Feb 3, 2017</p>
	 * @param no
	 * @param isCalloutSuccess
	 * @param overHesitationDateIndi
	 * @return
	 */
	protected String getAfrDocketNum(Integer no, boolean isCalloutSuccess, int overHesitationDateIndi) {
		if (isCalloutSuccess && overHesitationDateIndi == 0) {
			no += 1;
		} else if (!isCalloutSuccess && overHesitationDateIndi == 1) {
			no += 2;
		} else if (!isCalloutSuccess && overHesitationDateIndi == 0) {
			no += 3;
		}
		return "UNB-Afr-"+String.format("%02d", no);
	}

    @Override
    public void deleteByCalloutNum(String calloutNumList) {
        dao.deleteBycalloutNum(calloutNumList);
    }

    /**
     * <p>Description : 取得電訪對象對應NB INSURED CATEGORY，非被保人回傳0</p>
     * <p>Created By : Curious Wang</p>
     * <p>Create Time : 2020/05/08 </p>
     * @param calloutTargetType
     */
    public String getNBInsuredCategaryByCalloutTargetType(String calloutTargetType) {
		if (CalloutTargetType.INSURED_SPOUSE.equals(calloutTargetType)){
			return NBInsuredCategory.NB_INSURED_CATEGORY_WIFE;
		}else if(CalloutTargetType.INSURED_CHILD_1.equals(calloutTargetType)) {
			return NBInsuredCategory.NB_INSURED_CATEGORY_CHILID_1;
		}else if(CalloutTargetType.INSURED_CHILD_2.equals(calloutTargetType)) {
			return NBInsuredCategory.NB_INSURED_CATEGORY_CHILID_2;
		}else if(CalloutTargetType.INSURED_CHILD_3.equals(calloutTargetType)) {
			return NBInsuredCategory.NB_INSURED_CATEGORY_CHILID_3;
		}else if(CalloutTargetType.INSURED_CHILD_4.equals(calloutTargetType)) {
			return NBInsuredCategory.NB_INSURED_CATEGORY_CHILID_4;
		}else if(CalloutTargetType.INSURED_CHILD_OTHERS.equals(calloutTargetType)) {
			return NBInsuredCategory.NB_INSURED_CATEGORY_CHILID_5;
		}
		return "0";
    }

	@Override
	public String getDocketNum(String policyCode) {
		int liabilityStatus = 0 ;
		liabilityStatus = policyci.getLiabilityStatusByPolicyCode(policyCode);
		if(CodeCst.LIABILITY_STATUS__NON_VALIDATE == liabilityStatus){
			return "UNB-Bef";
}
		return "UNB-Afr";
	}

	public Map<String, Object> findCalloutResultGroupByReasonCode(Long policyId) {
		List<Map<String, Object>> mapResultLst = dao.findCalloutResultGroupByReasonCode(policyId);
		Map<String, Object> result = new HashMap<String, Object>();
		if (!CollectionUtils.isEmpty(mapResultLst)) {
		    for (Map<String, Object> map : mapResultLst) {
				if (!map.isEmpty()) { //PCR_474367_電訪結果改取小類判斷
					if (!StringUtils.isNullOrEmpty(MapUtils.getString(map, "callout_reason"))
							&& !StringUtils.isNullOrEmpty(MapUtils.getString(map, "callout_result"))) {
						result.put(MapUtils.getString(map, "callout_reason"),
								Arrays.asList(MapUtils.getString(map, "callout_result").split(",")));
					}
				}
			}
		}
		return result;
	}
	
	@Override
	public List<CalloutTransVO> findByPolicyIdAndReasonAndResult4Unb(Long policyId, String calloutReason, Integer calloutResult) {
		return convertToVOList(dao.findByPolicyIdAndReasonAndResult4Unb(policyId, calloutReason, calloutResult));
	}
	
}
