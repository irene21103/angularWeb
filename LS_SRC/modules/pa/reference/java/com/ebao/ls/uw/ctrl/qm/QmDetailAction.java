package com.ebao.ls.uw.ctrl.qm;

import java.util.Calendar;
import java.util.LinkedList;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.foundation.common.lang.exception.ExceptionUtils;
import com.ebao.foundation.core.logging.Log;
import com.ebao.foundation.module.util.beanutils.BeanUtils;
import com.ebao.ls.pa.nb.bs.KSystemService;
import com.ebao.ls.pty.ci.DeptCI;
import com.ebao.ls.pub.CodeCst;
import com.ebao.ls.pub.cst.BizCategory;
import com.ebao.ls.uw.ci.UwChangeMsgCI;
import com.ebao.ls.uw.ds.UwQmQueryService;
import com.ebao.ls.uw.ds.constant.Cst;
import com.ebao.ls.uw.vo.QualityManageListVO;
import com.ebao.ls.ws.vo.unb.unb240.QmDataCIVO;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.framework.GenericAction;

public class QmDetailAction extends GenericAction {

    @Resource(name = UwQmQueryService.BEAN_DEFAULT)
    private UwQmQueryService uwqqService;

    @Resource(name = UwChangeMsgCI.BEAN_DEFAULT)
    private UwChangeMsgCI uwChangeMsgCI;

    @Resource(name = KSystemService.BEAN_DEFAULT)
    private KSystemService kServ;
    
    @Resource(name = DeptCI.BEAN_DEFAULT)
	private DeptCI deptCI;

    protected static final String ACTION_SAVE = "save";
    private final Log log = Log.getLogger(QmDetailAction.class);
    @Override
    public ActionForward process(ActionMapping mapping, ActionForm form,
                    HttpServletRequest request, HttpServletResponse response)
                    throws Exception {

        UwQualityManageForm uwQualityManageForm = (UwQualityManageForm) form;
        
        uwQualityManageForm.setSaveRtnMsg(null);
        String forward = "display";
        
	        if (ACTION_SAVE.equals(uwQualityManageForm.getActionType())) {
	            try {
	                QualityManageListVO qmVO = uwQualityManageForm
	                                .getQualityManageListVO();
	                qmVO = uwqqService.save(qmVO);
	            
	                String bizCode = String.valueOf(deptCI.getBizCategoryByUserId(AppContext.getCurrentUser().getUserId()));
	                //登錄者只有pos權限,故只新增pos類別資料,其餘皆多新增一筆pos類別資料
	                if(!(String.valueOf(BizCategory.BIZ_CATEGORY_POS).equals(bizCode))){ 
	                    // IR 260448 -  非 POS 建立的業務員資料，需要建一筆供POS使用
	                	if(!"6".equals(qmVO.getSource()) && "2".equals(qmVO.getQualityManageType())) {
	                	    QualityManageListVO qmVO2 = new QualityManageListVO();
	    	                BeanUtils.copyProperties(qmVO2, qmVO);
	                		qmVO2.setSourceListId(qmVO.getListId());
		                	qmVO2.setSource("6");
		                	qmVO2.setSourceDescs("POS");
		                	qmVO2.setListId(null);
		                	
		                	//POS類別資料迄日由原迄日加1年
		                	Calendar cal = Calendar.getInstance();
		                	cal.setTime( qmVO2.getEndDate() );
		                	cal.add(Calendar.YEAR, 1);
		                	qmVO2.setEndDate( cal.getTime() );
		                	
		                	qmVO2=uwqqService.save(qmVO2);
	                	}
	                }

	                String qmType = uwQualityManageForm.getQualityManageType();
	                if (Cst.QUALITY_MANAGER_TYPE_CUSTOMER.equals(qmType)) {
	                    uwChangeMsgCI.prcUwChangeMsgByType(qmVO.getCertiCode(),
	                                    CodeCst.UW_CHANGE_CODE__QM_TYPE_CUSTOMER,
	                                    qmVO.getListId(), qmVO.getCertiCode());
	                }
	                if (Cst.QUALITY_MANAGER_TYPE_AGENT.equals(qmType)) {
	                    uwChangeMsgCI.prcUwChangeMsgByType(qmVO.getRegisterCode(),
	                                    CodeCst.UW_CHANGE_CODE__QM_TYPE_AGENT,
	                                    qmVO.getListId(), qmVO.getRegisterCode());
	                }
	
	                /* 提交至 K-Middle */
	                if (qmVO != null) {
	                    java.util.List<QmDataCIVO> qmDataList = new LinkedList<QmDataCIVO>();
	                    QmDataCIVO vo = new QmDataCIVO();
	                    BeanUtils.copyProperties(vo, qmVO);
	                    vo.setSourceDesc(qmVO.getSourceDescs());
	                    qmDataList.add(vo);
	                    try {
	                        kServ.sendQualityManage(qmDataList);
	                    } catch (Exception ex) {
	                        log.error(String.format("[cause error when  Send QM Lists to the K-SYS ] qualityManageType = %s Certi_Code = %s ", qmVO.getQualityManageType(), qmVO.getCertiCode()));
	                        log.error(ExceptionUtils.getFullStackTrace(ex));
	                    }
	                }
	
	                uwQualityManageForm.setQualityManageListVO(null);
	                uwQualityManageForm.setSaveRtnMsg("MSG_214927");
	            } catch (Exception e) {
	                uwQualityManageForm.setSaveRtnMsg("MSG_1253292");
	            }
	            forward = "display";
	        }
    
        return mapping.findForward(forward);
    }
}
