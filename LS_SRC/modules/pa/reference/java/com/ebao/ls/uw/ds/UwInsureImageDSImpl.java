package com.ebao.ls.uw.ds;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.hibernate.tool.hbm2x.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ebao.ls.pa.pub.model.PolicyInfo;
import com.ebao.ls.pa.pub.service.PolicyService;
import com.ebao.ls.pub.CodeCst;
import com.ebao.ls.uw.data.UwInsureImageDelegate;
import com.ebao.ls.uw.ds.vo.UwHistoryInsureVO;
import com.ebao.ls.uw.ds.vo.UwInsureClientVO;
import com.ebao.pub.framework.GenericDS;
import com.ebao.pub.util.json.DateUtils;

/**
 * <p>Title: 查詢影像資料 實作UwInsureImageService </p>
 * <p>Description: Function Description Only vegetable</p>
 * <p>Copyright: Copyright (c) 2015</p>
 * <p>Company: TGL Co., Ltd.</p>
 * <p>Create Time: Aug 25, 2015</p> 
 * @author 
 * <p>Update Time: Aug 25, 2015</p>
 * <p>Updater: Alex Cheng</p>
 * <p>Update Comments: </p>
 */
public class UwInsureImageDSImpl extends GenericDS implements UwInsureImageService {
	private static final Logger logger = LoggerFactory.getLogger(UwInsureImageDSImpl.class);

	@Resource(name = UwInsureImageDelegate.BEAN_DEFAULT)
	private UwInsureImageDelegate uwInsureImageDao;

	@Resource(name = PolicyService.BEAN_DEFAULT)
	protected PolicyService policyService;

	/**
	* <p>Description : 查詢影像資料</p>
	* <p>Created By : Alex Cheng</p>
	* <p>Create Time : Aug 25, 2015</p>
	* <p>Update Time: Nov 6, 2020</p>
	* <p>Updater: Charlotte Wang</p>
	* <p>Update Comments: PCR 351294 未承保件個人資料保存期限逾未承保起五年不得處理及利用</p>
	* @param policyId
	* @param uwInsureClients
	*/
	public void findInsureImageByClient(String policyId, List<UwInsureClientVO> uwInsureClients) {
		if (CollectionUtils.isEmpty(uwInsureClients))
			return;

		// 2020.11.6 Charlotte  eBao個險排除「保單要保書狀態為未承保(82-拒絕、84-延期、86-撤件) 且 保單下未承保之核保決定日與當前件比 >= 5 年」
		SimpleDateFormat monthDayFormat = new SimpleDateFormat("MM/dd");
		SimpleDateFormat date2LongFormat = new SimpleDateFormat(DateUtils.datePatternYYMMDD);
		// 取得當前保單資訊
		PolicyInfo policyQ = policyService.load(Long.parseLong(policyId));
		Date curPolicyApplyDate = policyQ.getApplyDate(); // 本件要保書填寫日
		Calendar fiveYearsBeforeApplyDate = Calendar.getInstance();
		fiveYearsBeforeApplyDate.setTime(curPolicyApplyDate);
		String monthDaystr = monthDayFormat.format(fiveYearsBeforeApplyDate.getTime());
		fiveYearsBeforeApplyDate.add(Calendar.YEAR, -5);
		if ("02/29".equals(monthDaystr)) // 若遇到本件是潤年，則往前五年會變成三月一號，從寬認定為 2/28
			fiveYearsBeforeApplyDate.add(Calendar.DATE, -1);
		String fiveYearsBefore = date2LongFormat.format(fiveYearsBeforeApplyDate.getTime());
		logger.debug("5 years before apply date >> " + fiveYearsBefore);

		for (UwInsureClientVO uwInsureClientVO : uwInsureClients) {
			List<UwHistoryInsureVO> list = uwInsureImageDao.findInsureImageByCeritCode(uwInsureClientVO.getCertiType(), uwInsureClientVO.getCertiCode());
			if( StringUtils.isNotEmpty(uwInsureClientVO.getOldForeignerId())) {
				list.addAll(uwInsureImageDao.findInsureImageByCeritCode(uwInsureClientVO.getCertiType(), uwInsureClientVO.getOldForeignerId()));
			}
			
			List<UwHistoryInsureVO> filterList = new java.util.LinkedList<UwHistoryInsureVO>();
			Set<String> keySet = new HashSet<String>();

			List<Long> hasRemovedPolicyId = new ArrayList<Long>();
			for (Iterator<UwHistoryInsureVO> iter = list.listIterator(); iter.hasNext();) {
				UwHistoryInsureVO vo = iter.next();

				String voPrpslStatus = vo.getProposalStatus();
				if (String.valueOf(CodeCst.PROPOSAL_STATUS__DECLINED).equals(voPrpslStatus)
								|| String.valueOf(CodeCst.PROPOSAL_STATUS__POSTPONED).equals(voPrpslStatus)
								|| String.valueOf(CodeCst.PROPOSAL_STATUS__WITHDRAWN).equals(voPrpslStatus)) {
					if (hasRemovedPolicyId.contains(vo.getPolicyId())) {
						continue;
					}

					Date voUnderwriteDate = vo.getUnderwriteDate();
					logger.debug("Historial underwrite date >> " + voUnderwriteDate);
					String voUnderwriteDateStr = null;
					if (null != voUnderwriteDate)
						voUnderwriteDateStr = date2LongFormat.format(voUnderwriteDate);

					if (null == voUnderwriteDateStr || fiveYearsBefore.compareTo(voUnderwriteDateStr) >= 0) {
						hasRemovedPolicyId.add(vo.getPolicyId());
						continue;
					}
				}

				if ("1".equals(vo.getOrderType())) {
					String key = vo.getPolicyCode() + ":" + uwInsureClientVO.getCertiCode();
					keySet.add(key);
				} else {//orderType=2
					/* 要/被保險人不同人才顯示 */
					String key = vo.getPolicyCode() + ":" + uwInsureClientVO.getCertiCode();
					if (keySet.contains(key) == true) {
						continue;
					}
				}
				filterList.add(vo);
			} // for
			uwInsureClientVO.setHistoryInsureList(filterList);
		} // for

	}

}