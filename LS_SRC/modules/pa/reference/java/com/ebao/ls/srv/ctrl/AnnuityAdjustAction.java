package com.ebao.ls.srv.ctrl;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.UserTransaction;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.ls.pa.pub.ci.PolicyCI;
import com.ebao.ls.pa.pub.ref.api.prd.ProductService;
import com.ebao.ls.pa.pub.ref.vo.prd.ProductBasicVarietySimpleVO;
import com.ebao.ls.pa.pub.service.PolicyService;
import com.ebao.ls.pa.pub.vo.CoverageVO;
import com.ebao.ls.pa.pub.vo.PolicyVO;
import com.ebao.ls.prd.rl.bs.ExecResultVO;
import com.ebao.ls.prd.rl.bs.KeyDataVO;
import com.ebao.ls.prd.rl.bs.ResultsVO;
import com.ebao.ls.prd.rl.ci.RuleEngineCI;
import com.ebao.ls.pub.CodeCst;
import com.ebao.ls.srv.ds.SurvivalRepayService;
import com.ebao.ls.srv.ds.SurvivalRepaymentTool;
import com.ebao.ls.srv.ds.sp.SurvivalDSSp;
import com.ebao.ls.srv.ds.vo.AnnuityUpdateInfoVO;
import com.ebao.pub.annotation.PAPubAPIUpdate;
import com.ebao.pub.annotation.PrdAPIUpdate;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.framework.ExceptionFactory;
import com.ebao.pub.framework.GenericAction;
import com.ebao.pub.framework.GenericException;
import com.ebao.pub.framework.MultiWarning;
import com.ebao.pub.util.BeanUtils;
import com.ebao.pub.util.StringUtils;
import com.ebao.pub.util.Trans;
import com.ebao.pub.util.TransUtils;

/**
 * <p>
 * Title: AnnuityAdjustAction
 * </p>
 * <p>
 * Description: This class Annuity adjust's action class, it implement change
 * disbursement method, forward party ui add bank account popup payment query
 * ui.
 * </p>
 * <p>
 * Copyright: Copyright (c) 2002
 * </p>
 * <p>
 * Company: eBaoTech
 * </p>
 * <p>
 * Create Time: 2005/04/28
 * </p>
 * 
 * @author simen.li
 * @version 1.0
 */

public class AnnuityAdjustAction extends GenericAction {
  public static final String BEAN_DEFAULT = "/srv/AnnuityAdjust";

/**
   * @param mapping ActionMapping
   * @param form ActionForm
   * @param request HttpServletRequest
   * @param response HttpServletResponse
   * @return org.apache.struts.action.ActionForward
   * @throws com.ebao.pub.framework.GenericException
   * @roseuid 4124A54E015E
   * @author susan.qin 2004-08-26
   */
  @Override
  @PrdAPIUpdate
  @PAPubAPIUpdate("update for spring on survivalRepaymentTool")
  protected MultiWarning processWarning(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {
    MultiWarning mw = new MultiWarning();
    AnnuityAdjustForm form1 = (AnnuityAdjustForm) form;
    String actionType = null;
    String policyNumber = null;
    actionType = request.getParameter("actionType");
    policyNumber = request.getParameter("policyNumber");
    if (null == actionType) {
      actionType = form1.getActionType();
    }
    if (null == policyNumber) {
      policyNumber = form1.getPolicyCode();
    }
    if ("submit".equals(actionType)) {

      PolicyVO policyVO = survivalRepaymentTool
          .getPolicyVOByPolicyNumber(policyNumber);
      if (null == policyVO) {
        mw.addWarning("ERR_22210420006");
        mw.setContinuable(false);
        mw.setClosable(true);
        return mw;
      }
      if (policyVO.getRiskStatus().intValue() != CodeCst.LIABILITY_STATUS__IN_FORCE) {
        mw.addWarning("ERR_20910020013");
        mw.setContinuable(false);
        mw.setClosable(true);
        return mw;
      }
      CoverageVO policyProductVO = survivalRepaymentTool
          .getPolicyProductByPolicyId(policyVO.getPolicyId());
      Integer liabId = SurvivalDSSp.isSurvivalProduct(policyProductVO
          .getItemId());
      boolean isVaPolicy = policyCI.isVAPolicy(policyProductVO.getPolicyId());
      if (liabId.intValue() != CodeCst.LIABILITY__ANNUNITY && !isVaPolicy) {
        mw.addWarning("ERR_20910020012");
        mw.setContinuable(false);
        mw.setClosable(true);
        return mw;
      }
      if (isVaPolicy) {
        // String payEnsure = "0";
        // if (form1.getVaPayEnsure() != null
        // && !form1.getVaPayEnsure().equals("")) {
        // payEnsure = form1.getVaPayEnsure();
        // }
        // Need to check the VA related fields
        if (form1.getIsVaStart().equals("N")) {
          boolean allowAnnuFreqFlag = false;
          String freqMsg = "";
          boolean allowAnnuModeFlag = false;
          String modeMsg = "";
          // No need to check the coverage period and charge period
          List<String> allowAnnuFreq = getProductService().getProduct(
              policyProductVO.getProductId().longValue(),policyService.getActiveVersionDate(policyProductVO))
              .getAnnuityFrequencies();
          if (allowAnnuFreq != null && allowAnnuFreq.size() > 0) {
            if (allowAnnuFreq.contains(form1.getVaPayType())) {
              allowAnnuFreqFlag = true;
            } else {
              for (int i = 0; i < allowAnnuFreq.size(); i++) {
                freqMsg = freqMsg + allowAnnuFreq.get(i) + ",";
              }
              if (freqMsg.length() > 1) {
                freqMsg = freqMsg.substring(0, freqMsg.length() - 1);
              }
              freqMsg = "The input annuity installment frequency is not allowed for this product."
                  + "The product allowed annuity frequency is :" + freqMsg;
            }
          } else {
            freqMsg = "No annuity installment frequency is  allowed for this product.";
          }

          List<Integer> allowAnnuMode = getProductService().getProduct(
              policyProductVO.getProductId().longValue(),policyService.getActiveVersionDate(policyProductVO)).getAnnuityPayModes();
          if (allowAnnuMode != null && allowAnnuMode.size() > 0) {
            if (allowAnnuMode
                .contains(Integer.valueOf(form1.getCurVaPayMode()))) {
              allowAnnuModeFlag = true;
            } else {
              for (int i = 0; i < allowAnnuMode.size(); i++) {
                modeMsg = modeMsg + allowAnnuMode.get(i) + ",";
              }
              if (modeMsg.length() > 1) {
                modeMsg = modeMsg.substring(0, modeMsg.length() - 1);
              }
              modeMsg = "The input annuity pay mode  is not allowed for this product."
                  + "This product allowed annuity pay mode is:" + modeMsg;
            }
          } else {
            modeMsg = " No annuity pay mode  is  allowed for this product.";
          }
          //check VA basic information
          ProductBasicVarietySimpleVO criteria = new ProductBasicVarietySimpleVO();
          criteria.setCoveragePeriod(policyProductVO.getCoveragePeriod());
          criteria.setCoverageYear(policyProductVO.getCoverageYear());
          criteria.setChargePeriod(policyProductVO.getChargePeriod());
          criteria.setChargeYear(policyProductVO.getChargeYear());
          criteria.setPayPeriod(form1.getVaPayPeriod());
          criteria.setPayYear(Integer.valueOf(form1.getVaPayYear()));
          criteria.setEndPeriod(form1.getVaEndPeriod());
          criteria.setEndYear(Integer.valueOf(form1.getVaEndYear()));
          criteria.setPayEnsure(Integer.valueOf(form1.getVaPayEnsure()));
          criteria.setProductVersionDate(policyService.getActiveVersionDate(policyProductVO));
          boolean allowVaBasicInfoFlag = getProductService().checkTermLimit(
              policyProductVO.getProductId().longValue(), criteria);

          if (!allowAnnuFreqFlag) {
            mw.addWarning(freqMsg);

            mw.setContinuable(false);
            mw.setClosable(true);
            return mw;
          }
          if (!allowAnnuModeFlag) {
            mw.addWarning(modeMsg);
            mw.setContinuable(false);
            mw.setClosable(true);
            return mw;
          }
          if (!allowVaBasicInfoFlag) {
            mw.addWarning("ERR_20910020036");
            mw.setContinuable(false);
            mw.setClosable(true);
            return mw;
          }

        }
        if (form1.getIsGmwb().equals("Y") && form1.getIsGmwbStart().equals("N")) {
          boolean allowGmwbFreqFlag = false;
          String freqMsg = "";
          boolean allowGmwbModeFlag = false;
          String modeMsg = "";

          List<String> allowGmwbFreq = getProductService().getProduct(
              policyProductVO.getProductId().longValue(),policyService.getActiveVersionDate(policyProductVO))
              .getWithdrawFrequencies();// .getAnnuityFrequencies();
          if (allowGmwbFreq != null && allowGmwbFreq.size() > 0) {
            if (allowGmwbFreq.contains(form1.getGmwbPayType())) {
              allowGmwbFreqFlag = true;
            } else {
              for (int i = 0; i < allowGmwbFreq.size(); i++) {
                freqMsg = freqMsg + allowGmwbFreq.get(i) + ",";
              }
              if (freqMsg.length() > 1) {
                freqMsg = freqMsg.substring(0, freqMsg.length() - 1);
              }
              freqMsg = "The input withDraw installment frequency is not allowed for this product."
                  + "The product allowed withdraw frequency is :" + freqMsg;
            }
          } else {
            freqMsg = " No withDraw installment frequency is  allowed for this product.";
          }

          List<Integer> allowGmwbMode = getProductService().getProduct(
              policyProductVO.getProductId().longValue(),policyService.getActiveVersionDate(policyProductVO)).getWithdrawPayModes();
          if (allowGmwbMode != null && allowGmwbMode.size() > 0) {
            if (allowGmwbMode.contains(Integer.valueOf(form1
                .getCurGmwbPayMode()))) {
              allowGmwbModeFlag = true;
            } else {
              for (int i = 0; i < allowGmwbMode.size(); i++) {
                modeMsg = modeMsg + allowGmwbMode.get(i) + ",";
              }
              if (modeMsg.length() > 1) {
                modeMsg = modeMsg.substring(0, modeMsg.length() - 1);
              }
              modeMsg = "The input withdraw pay mode is not allowed for this product."
                  + "This product allowed withdraw mode is:" + modeMsg;
            }
          } else {
            modeMsg = "No withdraw pay mode is  allowed for this product.";
          }

          if (!allowGmwbFreqFlag) {
            mw.addWarning(freqMsg);
            mw.setContinuable(false);
            mw.setClosable(true);
            return mw;
          }
          if (!allowGmwbModeFlag) {
            mw.addWarning(modeMsg);
            mw.setContinuable(false);
            mw.setClosable(true);
            return mw;
          }
        }
        // If all check above passed, save VA info & do Rule Engine Validation.
        // The check rules below is only suitable for VA product
        if (mw == null || mw.isEmpty()) {
          UserTransaction trans = null;
          try {
            trans = Trans.getUserTransaction();
            trans.begin();
            AnnuityUpdateInfoVO annuityUpdateVO = new AnnuityUpdateInfoVO();
            BeanUtils.copyProperties(annuityUpdateVO, form1);
            survivalRepayDS.updateVaInfo(annuityUpdateVO);
            KeyDataVO keyDataVO = new KeyDataVO();
            keyDataVO.setPolicyId(policyProductVO.getPolicyId());
            keyDataVO.setItemId(policyProductVO.getItemId());
            keyDataVO.setInsuredId(policyProductVO.getInsureds().get(0)
                .getInsured().getPartyId());
            // Fire rule engine event.
            ResultsVO result = ruleEngineCI.fire("CSAnnuity", ""
                + AppContext.getCurrentUser().getUserId(), Boolean.TRUE,
                keyDataVO);
            ExecResultVO[] execVO = result.getResults();
            if (execVO != null && execVO.length > 0) {
              for (ExecResultVO element : execVO) {
                GenericException oneError = new GenericException(Long.valueOf(
                    element.getErrorCode()).longValue(), element.getMessage());
                mw.addWarning(oneError);
              }
              mw.setContinuable(false);
              mw.setClosable(true);
              // Rollback transaction update if has warning.
              TransUtils.rollback(trans);
            } else {
              trans.commit();
            }
          } catch (Exception e) {
            TransUtils.rollback(trans);
            throw ExceptionFactory.parse(e);
          }
        }
      }
    }
    return mw;
  }

  /**
   * <p>
   * Description: process some action
   * </p>
   * 
   * @param mapping
   * @param form,
   * @param request
   * @param response
   * @return ActionForward
   * @throws Exception <P>
   *           Create time: 2005/4/28
   *           </P>
   */
  @Override
  @PAPubAPIUpdate("update for survivalRepaymentTool")
  public ActionForward process(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {
    AnnuityAdjustForm form1 = (AnnuityAdjustForm) form;
    String actionType = null;
    String policyNumber = null;
    actionType = request.getParameter("actionType");
    policyNumber = request.getParameter("policyNumber");
    if (null == actionType) {
      actionType = form1.getActionType();
    }
    if (null == policyNumber) {
      policyNumber = form1.getPolicyCode();
    }
    if (!("".equals(policyNumber)) && null != policyNumber) {
      if ("searchByPolicyNumber".equals(actionType) || "".equals(actionType)) {
        AnnuityUpdateInfoVO annuityUpdateVO = survivalRepayDS
            .findAnnuityInfoByPolicyCode(policyNumber);
        BeanUtils.copyProperties(form1, annuityUpdateVO);
      } else if ("submit".equals(actionType)) {
        UserTransaction trans = null;
        try {
          PolicyVO policyVO = survivalRepaymentTool
              .getPolicyVOByPolicyNumber(policyNumber);
          boolean isVaPolicy = policyCI.isVAPolicy(policyVO.getPolicyId());
          trans = Trans.getUserTransaction();
          trans.begin();
          if (isVaPolicy) {
            // Update info in processWarning....
          } else {
            if (null == form1.getPlanId()
                || (form1.getPlanId().intValue() == 0)) {
              survivalRepayDS.updateAnnuityInfo(form1.getItemId(), form1
                  .getCurDisbursementMethod(), form1.getAccountId());
            } else {
              survivalRepayDS.updateAnnuityInfo(form1.getPlanId(), form1
                  .getCurDisbursementMethod(), form1.getAccountId(), form1
                  .getManual(), form1.getDischargeVoucher(), form1
                  .getCancelLastDischarge());
            }
          }
          trans.commit();
          return mapping.findForward("succeed");
        } catch (Exception e) {
          TransUtils.rollback(trans);
          throw ExceptionFactory.parse(e);
        }
      } else if ("addAccount".equals(actionType)) {
        if (!(null == form1.getPolicyCode() || "".equals(form1.getPolicyCode()))
            && form1.getPayeeId() != null
            && !StringUtils.isEmpty(form1.getPayeeId())) {
          String exitUrl = getExitURL(form1.getPolicyCode());
          String path = "/pty/toAccount.do?partyId=" + form1.getPayeeId()
              + "&invoker=SRV" + "&exitUrl=" + exitUrl;
          ActionForward actionForward = getActionForward(path);
          return actionForward;
        }
      } else if ("addGmwbAccount".equals(actionType)) {
        if (!(null == form1.getPolicyCode() || "".equals(form1.getPolicyCode()))
            && form1.getGmwbPayeeId() != null
            && !StringUtils.isEmpty(form1.getGmwbPayeeId())) {
          String exitUrl = getExitURL(form1.getPolicyCode());
          String path = "/pty/toAccount.do?partyId=" + form1.getGmwbPayeeId()
              + "&invoker=SRV" + "&exitUrl=" + exitUrl;
          ActionForward actionForward = getActionForward(path);
          return actionForward;
        }
      } else if ("addVaAccount".equals(actionType)) {
        if (!(null == form1.getPolicyCode() || "".equals(form1.getPolicyCode()))
            && form1.getVaPayeeId() != null
            && !StringUtils.isEmpty(form1.getVaPayeeId())) {
          String exitUrl = getExitURL(form1.getPolicyCode());
          String path = "/pty/toAccount.do?partyId=" + form1.getVaPayeeId()
              + "&invoker=SRV" + "&exitUrl=" + exitUrl;
          ActionForward actionForward = getActionForward(path);
          return actionForward;
        }
      }
    } else {
      clearFormValue(form);
    }
    return mapping.findForward("annuityAdjust");
  }

  /**
   * <p>
   * Description: handle url for call back by party ui.
   * </p>
   * 
   * @param param1
   * @return String url
   * @throws GenericException <p>
   *           Create time:2005/4/28
   *           </p>
   */
  private String getExitURL(String param1) throws GenericException {
    // String exitUrl = Env.URL_PREFIX +"/srv/AnnuityAdjust.do?policyNumber=" +
    // param1;
    String exitUrl = "/srv/AnnuityAdjust.do?policyNumber=" + param1;
    try {
      exitUrl = URLEncoder.encode(exitUrl, "UTF-8");
    } catch (UnsupportedEncodingException ex) {
      throw ExceptionFactory.parse(ex);
    }
    return exitUrl;
  }

  /**
   * <p>
   * get actionForward by spec url.
   * </p>
   * 
   * @param path
   * @return String url
   *         <p>
   *         Create time:2005/4/28
   *         </p>
   */
  private ActionForward getActionForward(String path) {
    ActionForward actionForward = new ActionForward();
    actionForward.setContextRelative(true);
    actionForward.setRedirect(false); // modified for GEL00040593
    actionForward.setPath(path);
    return actionForward;
  }

  /**
   * <p>
   * Description:clear form value.
   * </p>
   * 
   * @param form <p>
   *          Create time:2005/4/28
   *          </p>
   */
  private void clearFormValue(ActionForm form) {
    AnnuityAdjustForm form1 = (AnnuityAdjustForm) form;
    form1.setAccountInfos(null);
    form1.setVaAccountInfos(null);
    form1.setGmwbAccountInfos(null);
    form1.setAssignee("");
    form1.setBankAccount("");
    form1.setVaBankAccount("");
    form1.setGmwbBankAccount("");
    form1.setCancelLastDischarge("N");
    form1.setVaCancelLastDischarge("N");
    form1.setCheckedAccountId("");
    form1.setVaCheckedAccountId("");
    form1.setGmwbCheckedAccountId("");
    form1.setCurDisbursementMethod("");
    form1.setCurVaPayMode("");
    form1.setCurGmwbPayMode("");
    form1.setDisbursementMethod("");
    form1.setVaPayMode("");
    form1.setGmwbPayMode("");
    form1.setDischargeVoucher("N");
    form1.setVaDischargeVoucher("N");
    form1.setItemId("");
    form1.setLastDischargeDate("");
    form1.setVaLastDischargeDate("");
    form1.setManual("N");
    form1.setVaManual("N");
    form1.setNextAnnuityDate("");
    form1.setNextVaDate("");
    form1.setNextGmwbDate("");
    form1.setNextDischargeDate("");
    form1.setVaNextDischargeDate("");
    form1.setPayeeId("");
    form1.setVaPayeeId("");
    form1.setGmwbPayeeId("");
    form1.setVaPayeeIdNumber("");
    form1.setGmwbPayeeIdNumber("");
    form1.setPayeeIdNumber("");
    form1.setPayeeIdType("");
    form1.setVaPayeeIdType("");
    form1.setGmwbPayeeIdType("");
    form1.setPayeeName("");
    form1.setVaPayeeName("");
    form1.setGmwbPayeeName("");
    form1.setPaymentMethod("");
    form1.setVaPaymentMethod("");
    form1.setGmwbPaymentMethod("");
    form1.setPolicyCode("");
    form1.setPolicyHolder("");
    form1.setPolicyStatus("");
    // form1.setTPayDue(null);
    form1.setValidDate("");
    form1.setPlanId(Long.valueOf(0));
    form1.setVaPlanId(Long.valueOf(0));
    form1.setGmwbPlanId(Long.valueOf(0));
    form1.setIsGmwb("N");
    form1.setIsVa("N");
    form1.setIsVaStart("N");
    form1.setIsGmwbStart("N");
  }

  @Resource(name = SurvivalRepayService.BEAN_DEFAULT)
  private SurvivalRepayService survivalRepayDS;

  @Resource(name = PolicyCI.BEAN_DEFAULT)
  private PolicyCI policyCI;

  @Resource(name = RuleEngineCI.BEAN_DEFAULT)
  private RuleEngineCI ruleEngineCI;

  @Resource(name = ProductService.BEAN_DEFAULT)
  private ProductService productService;

  public ProductService getProductService() {
    return productService;
  }

  @Resource(name = SurvivalRepaymentTool.BEAN_DEFAULT)
  private SurvivalRepaymentTool survivalRepaymentTool;
  
  @Resource(name = PolicyService.BEAN_DEFAULT)
  PolicyService policyService;
}
