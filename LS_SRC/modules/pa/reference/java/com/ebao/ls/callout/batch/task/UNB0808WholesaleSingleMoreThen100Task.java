package com.ebao.ls.callout.batch.task;

import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.ApplicationContextAware;

import com.ebao.foundation.common.lang.DateUtils;
import com.ebao.foundation.common.lang.exception.ExceptionUtils;
import com.ebao.foundation.core.logging.Log;
import com.ebao.ls.callout.batch.AbstractCalloutTask;
import com.ebao.ls.callout.batch.CalloutConstants;
import com.ebao.ls.callout.batch.CalloutDataExportService;
import com.ebao.pub.batch.help.BatchHelp;
import com.ebao.pub.batch.type.JobStatus;
import com.ebao.pub.batch.type.LogLevel;
import com.ebao.pub.batch.util.BatchLogUtils;
import com.ebao.pub.util.TransUtils;

public class UNB0808WholesaleSingleMoreThen100Task extends AbstractCalloutTask implements ApplicationContextAware {
    
    public static final String BEAN_DEFAULT = "zeroEightZeroEightTask";
    
    private final Log log = Log.getLogger(UNB0808WholesaleSingleMoreThen100Task.class);
    
    public final static Long JOB_ID = 1332666L;
    
    private final Long DISPLAY_ORDER = 8L;

    private String CONFIG_TYPE = "1";// 080抽樣比例
    
    @Resource(name=CalloutDataExportService.BEAN_DEFAULT)
    private CalloutDataExportService exportServ;
    
    @Override
    public int mainProcess() throws Exception {
        List<Map<String, Object>> mapList = null;
        UserTransaction ut = TransUtils.getUserTransaction();
        try{
            ut.begin();
            /* 保單抽樣 */
            mapList = unbSampling(
                        "[BR-CMN-TFI-005][檔案080-8 躉繳單件保費>=100 萬]",
                        CalloutConstants.ZeroEightZeroEight, CONFIG_TYPE, DISPLAY_ORDER, "080-8", "080");
            /* 匯出抽樣保單 */
            exportServ.feedMap(mapList, CalloutConstants.ZeroEightZeroEight, BatchHelp.getProcessDate());
            ut.commit();
            if(CollectionUtils.isEmpty(mapList)){
                return JobStatus.EXECUTE_SUCCESS;
            }
        } catch (Exception ex) {
            try {
                ut.rollback();
            } catch (SystemException sysEx) {
                BatchLogUtils.addLog(LogLevel.ERROR, null,ExceptionUtils.getFullStackTrace(sysEx));
            }
            BatchLogUtils.addLog(LogLevel.ERROR, null,ExceptionUtils.getFullStackTrace(ex));
            return JobStatus.EXECUTE_FAILED;
        }
        
        
        return JobStatus.EXECUTE_SUCCESS;
    }

    public java.util.List<Map<String, Object>> getData(Date inputDate, String policyCode){
        java.util.List<java.util.Map<String,Object>> mapList  = new java.util.LinkedList<Map<String,Object>>();
        
        final java.sql.Date processDate = new java.sql.Date(inputDate.getTime());
        final java.sql.Date nextDate = new java.sql.Date(DateUtils.addDay(inputDate, 1).getTime());
        java.util.Date monthDate = DateUtils.truncateMonth(DateUtils.addMonth(inputDate,-1));
        final java.sql.Date monthSqlDate = new java.sql.Date(monthDate.getTime());
        
        String paraVal = com.ebao.pub.para.Para.getParaValue(3060010020L);
        java.util.Date paraValDate;
        try {
            paraValDate = org.apache.commons.lang3.time.DateUtils.parseDate(paraVal, "yyyyMMdd");
        } catch (ParseException e) {
            java.util.Calendar calendar = java.util.Calendar.getInstance();
            calendar.set(2018, 3, 1);
            paraValDate= calendar.getTime();
        }
        final java.sql.Date initDate = DateUtils.toSqlDate(paraValDate);
        
        StringBuffer sql = new StringBuffer();
        sql.append("select distinct mail.policy_id, ");
        sql.append("        mail.policy_code,  ");
        sql.append("        prd.internal_id,  ");
        sql.append("        cmprd.product_version_id, ");
        sql.append("        ph.name ph_name,  ");
        sql.append("       	ph.roman_name ph_roman_name, ");
        sql.append("        ph.certi_code ph_certi_code,  ");
        sql.append("        ph.gender ph_gender,  ");
        sql.append("        ph.BIRTH_DATE ph_birthday,  ");
        sql.append("        decode(ph.office_tel_reg, null, null, '(' || ph.office_tel_reg || ')') || ph.office_tel ||  ");
        sql.append("        decode(ph.office_tel_ext, null, null, '#' || ph.office_tel_ext) ph_office_tel,  ");
        sql.append("        decode(ph.home_tel_reg, null, null, '(' || ph.home_tel_reg || ')') || ph.home_tel ||  ");
        sql.append("        decode(ph.home_tel_ext, null, null, '#' || ph.home_tel_ext) ph_home_tel,  ");
        sql.append("        ph.MOBILE_TEL ph_phone1,  ");
        sql.append("        ph.MOBILE_TEL2 ph_phone2, ");
        sql.append("       (SELECT a.address_1 from t_address a WHERE a.address_id = ph.address_id) ph_home_addr,  ");
        sql.append("       lr.name lr_name,   ");
        sql.append("       lr.roman_name lr_roman_name,   ");
        sql.append("       lr.certi_code lr_certi_code,   ");
        sql.append("       lr.gender lr_gender,   ");
        sql.append("       lr.BIRTH_DATE lr_birthday,   ");
        sql.append("       lr.rel_to_ph_ins rel_to_ph,   ");  
        sql.append("       lr2.rel_to_ph_ins rel_to_ins,   ");
        sql.append("       decode(lrc.office_tel_reg, null, null, '(' || lrc.office_tel_reg || ')') || lrc.office_tel || decode(lrc.office_tel_ext, null, null, '#' || lrc.office_tel_ext) lr_office_tel,   ");
        sql.append("       decode(lrc.home_tel_reg, null, null, '(' || lrc.home_tel_reg || ')') || lrc.home_tel || decode(lrc.home_tel_ext, null, null, '#' ||  lrc.home_tel_ext) lr_home_tel,  ");
        sql.append("       lrc.MOBILE lr_phone1,  ");
        sql.append("       lrc.mobile2 lr_phone2,  ");
        sql.append("       cmprd.charge_period charge_period,  ");
        sql.append("       cmprd.charge_year charge_year,  ");
        sql.append("        belst.NAME i_name,  ");
        sql.append("        belst.CERTI_CODE i_certi_code,  ");
        sql.append("        be.ENTRY_AGE i_age,  ");
        sql.append("        mail.apply_date apply_date,  ");
        sql.append("        mail.validate_date validate_date,  ");
        sql.append("        mail.issue_date issue_date,  ");
        sql.append("        mail.submission_date submission_date,  ");
        sql.append("        mail.dispatch_date dispatch_date,  ");
        sql.append("        mail.acknowledge_date reply_date,  ");
        sql.append("       mail.receive_date receive_date,  ");
        sql.append("       mail.tracking_number tracking_number,  ");//PCR-415241 保單郵寄掛號/國際快捷/宅配號碼
        sql.append("        send_type,  ");
        sql.append("       pkg_pub_cmn_utils.f_get_ilp_charge_type(cmprd.PRODUCT_ID ) AS ilp_charge_type,  ");
        sql.append("        pkg_ls_pub_util.f_get_code_mutilang_desc('T_LIABILITY_STATUS', mail.liability_state, '311') state, ");
        sql.append("        pkg_ls_pub_util.f_get_code_mutilang_desc('T_MONEY', prd.base_money , '311') money_name, ");
        sql.append("        pkg_ls_sc_callout.f_get_cm_prod_rate(prd.base_money, mail.issue_date) exchange_rate,   ");
        sql.append("        pkg_ls_pub_util.f_get_code_mutilang_desc('V_NBU_CALLOUT_CHARGE_MODE', cmprd.RENEWAL_TYPE, '311') CHARGE_NAME, ");
        sql.append("        pkg_ls_sc_callout.f_get_annual_prem(mail.policy_id) annual_prem,  ");
        sql.append("        pkg_ls_sc_callout.f_get_annual_prem(mail.policy_id) * pkg_ls_sc_callout.f_get_cm_prod_rate(prd.base_money, mail.issue_date)  std_tw_prem,  ");
        sql.append("        nvl(cmprd.CUSTOMIZED_PREM,0) customized_prem,  ");
        sql.append("        pkg_ls_sc_callout.F_GET_CONTRACT_AMOUNT(CMPRD.ITEM_ID) AMOUNT,  ");
        sql.append("        pkg_ls_sc_callout.F_GET_CONTRACT_AMOUNT_UNIT(CMPRD.ITEM_ID) AMOUNT_UNIT,  ");
        sql.append("        prd.big_prodCate,    ");
        sql.append("        prd.small_prodcate,  ");
        sql.append("        pkg_ls_sc_callout.f_get_each_prem(mail.policy_id) each_prem, ");
        sql.append("    PKG_PUB_CMN_UTILS.f_is_purpose(mail.policy_id, '1') ensure_require,  ");
        sql.append("    PKG_PUB_CMN_UTILS.f_is_purpose(mail.policy_id, '2') retire_require,  ");
        sql.append("    PKG_PUB_CMN_UTILS.f_is_purpose(mail.policy_id, '3') child_fee,  ");
        sql.append("    PKG_PUB_CMN_UTILS.f_is_purpose(mail.policy_id, '4') asset,  ");
        sql.append("    PKG_PUB_CMN_UTILS.f_is_purpose(mail.policy_id, '5') house ");
        sql.append("   from (select *  ");
        sql.append("           from v_callout_mail m  ");
        sql.append("          where 1=1  ");
        if(StringUtils.isNotEmpty(policyCode)) {
        	sql.append(" and m.policy_code = :policy_code ");
        }
        sql.append("           and m.issue_date >= :init_date ");
        sql.append("           and m.dispatch_date >= :month_date ");
        sql.append("           and m.dispatch_date <= :process_date ");
        sql.append(" and not exists ");
        sql.append("          (select 1 ");
        sql.append("                   from t_callout_batch cb ");
        sql.append("                   join t_callout_config cfg ");
        sql.append("                     on cfg.list_id = cb.config_type ");
        sql.append("                  where cb.policy_code = m.policy_code ");
        sql.append("                    and cfg.config_type = 1 and display_order = 8) ");
        sql.append(" ) mail  ");
        sql.append("   join t_benefit_insured_log be on be.policy_id = mail.policy_id and be.ENTRY_AGE > 69 and be.ORDER_ID=1 and be.last_cmt_flg = 'Y' ");
        sql.append("   join t_contract_product_log cmprd on cmprd.policy_id=mail.policy_id and cmprd.ITEM_ID = be.ITEM_ID and cmprd.MASTER_ID is null and cmprd.renewal_type = 5 and cmprd.last_cmt_flg = 'Y'  ");
        sql.append("   join V_CALLOUT_PROD_LOAN prd on prd.product_id = cmprd.PRODUCT_ID  ");
        sql.append("   join t_insured_list_log belst on be.INSURED_ID = belst.list_id and belst.last_cmt_flg = 'Y' ");
        sql.append("   join t_policy_holder_log ph on ph.policy_id = mail.policy_id  and ph.last_cmt_flg = 'Y'  ");
        sql.append("  LEFT join t_legal_representative_log lr   ");
    	//要要保人關係
    	sql.append("  on lr.policy_id = mail.policy_id AND lr.rel_id = ph.list_id AND lr.rel_type=1 and  lr.last_cmt_flg = 'Y'  ");
    	sql.append("  LEFT join t_legal_representative_log lr2   ");
    	//要被保人關係
    	sql.append("  on lr2.policy_id = mail.policy_id AND lr2.rel_id = ph.list_id AND  lr2.rel_type=2 and lr2.last_cmt_flg = 'Y'  ");
        sql.append("  LEFT JOIN t_customer lrc  ");
        sql.append("  ON lr.party_id = lrc.customer_id ");
		sql.append("   where pkg_ls_sc_callout.f_get_annual_prem(mail.policy_id) * pkg_ls_sc_callout.f_get_cm_prod_rate(prd.base_money, mail.issue_date)  >= 1000000 ");
        sql.append("   order by mail.policy_code  ");
        
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("process_date", processDate);
        params.put("init_date", initDate);
        params.put("month_date", monthSqlDate);
        
        BatchLogUtils.addLog(LogLevel.INFO, null,
				String.format("process_date := %s, month_date := %s,  next_date := %s, policy_code :=%s ",
						DateUtils.date2String(processDate, "yyyy/MM/dd"),
						DateUtils.date2String(monthDate, "yyyy/MM/dd"),
						DateUtils.date2String(nextDate, "yyyy/MM/dd"),
						policyCode));
        if(StringUtils.isNotEmpty(policyCode)) {
        	params.put("policy_code", policyCode);
        }
        mapList = nt.queryForList(sql.toString(), params);
        return combineDataByPolicyCode(mapList);
    }
    
    @Override
    public String saveToTrans(Map<String, Object> map, Date processDate,
                    String fileName, String dept) {
        String calloutNum = savePolicyHolder(map, processDate, fileName, dept);
        return calloutNum;
    }
}
