package com.ebao.ls.uw.ctrl.search;

import java.util.Collection;
import java.util.Iterator;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.ls.uw.ds.UwPolicyService;
import com.ebao.ls.uw.vo.UwPolicyVO;
import com.ebao.pub.framework.GenericAction;

/**
 * <p>Title: GEL-UW</p>
 * <p>Description: get UnderwriteId action</p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: eBaoTech</p>
 * @author yixing.lu
 * @version 1.0
 * @since 08.23.2004
 */

public class UwIdQuery extends GenericAction {

  /**
   * query  policy's underwriteId
   * @param mapping The ActionMapping used to select this instance
   * @param form The optional ActionForm bean for this request (if any)
   * @param request                   Http Request
   * @param response                  Http Response
   * @return ActionForward            ActionForward
   * @throws java.lang.Exception      ApplicationException
   */
  @Override
  public ActionForward process(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {
    String changeId = request.getParameter("changId");
    String sourceType = request.getParameter("uwSourceType");
    Collection uwIdCollection = uwPolicyDS.queryUwDetail(changeId, sourceType);
    Long underwriteId = Long.valueOf(0);
    Long temp = Long.valueOf(0);

    if (uwIdCollection.isEmpty()) {
      uwIdCollection = new java.util.ArrayList();
    }
    Iterator uwIdIterator = uwIdCollection.iterator();
    while (uwIdIterator.hasNext()) {
      UwPolicyVO vo = (UwPolicyVO) uwIdIterator.next();
      underwriteId = vo.getUnderwriteId().longValue() > temp.longValue() ? vo
          .getUnderwriteId() : temp;
      temp = vo.getUnderwriteId();
    }
    request.setAttribute("underwriteId", underwriteId);
    return mapping.findForward("uwId");
  }

  @Resource(name = UwPolicyService.BEAN_DEFAULT)
  private UwPolicyService uwPolicyDS;

  public UwPolicyService getUwPolicyDS() {
    return uwPolicyDS;
  }

  public void setUwPolicyDS(UwPolicyService uwPolicyDS) {
    this.uwPolicyDS = uwPolicyDS;
  }
}
