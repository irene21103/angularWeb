package com.ebao.ls.uw.cmu.event;

import com.ebao.foundation.commons.annotation.TriggerPoint;
import com.ebao.ls.cmu.event.CsUwCompleted;
import com.ebao.pub.event.AbstractIntegrationListener;
import com.ebao.pub.integration.IntegrationMessage;

public class CsUwCompletedIntegrationListener
    extends
      AbstractIntegrationListener<CsUwCompleted> {
  final static String code = "uw.csuwCompleted";

  @Override
  @TriggerPoint(component = "Underwriting", description = "When CS underwriting is submitted successfully, the event is triggered.<br/>Menu navigation: Customer service > Customer service underwriting.<br/>", event = "direct:event."
      + code, id = "com.ebao.tp.ls." + code)
  protected String getEventCode(CsUwCompleted event,
      IntegrationMessage<Object> in) {
    return "event." + code;
  }
}
