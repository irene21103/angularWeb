package com.ebao.ls.uw.cmu.event;

import com.ebao.foundation.commons.annotation.TriggerPoint;
import com.ebao.ls.cmu.event.NbUwCompletedWithDeclined;
import com.ebao.pub.event.AbstractIntegrationListener;
import com.ebao.pub.integration.IntegrationMessage;

public class NbUwCompletedWithDeclinedIntegrationListener
    extends
      AbstractIntegrationListener<NbUwCompletedWithDeclined> {
  final static String code = "uw.nbuwCompleted.declined";

  @Override
  @TriggerPoint(component = "Underwriting", description = "During manual underwriting, if the underwriting decision is 'Declined' and has been submitted successfully,  the event is triggered.<br/>Triggers can be either of the following: <br/><ol><li>Manual underwriting is triggered in menu: New business > Worklist > Underwriting.</li><li>Auto-underwriting is triggered in <ul><li>New business > Worklist > Data entry </li><li>New business > Worklist > Verification</li></ul></li></ol>", event = "direct:event."
      + code, id = "com.ebao.tp.ls." + code)
  protected String getEventCode(NbUwCompletedWithDeclined event,
      IntegrationMessage<Object> in) {
    return "event." + code;
  }
}
