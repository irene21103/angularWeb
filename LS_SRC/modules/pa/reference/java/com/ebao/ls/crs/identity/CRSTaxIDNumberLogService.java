package com.ebao.ls.crs.identity;

import java.util.List;

import com.ebao.ls.crs.vo.CRSTaxIDNumberLogVO;
import com.ebao.ls.foundation.service.GenericService;

public interface CRSTaxIDNumberLogService extends GenericService<CRSTaxIDNumberLogVO> {
	
	public final static String BEAN_DEFAULT = "crsTaxIDNumberLogService";

	public List<CRSTaxIDNumberLogVO> findByPartyLogIdAndBizId(Long partyLogId, String bizSource, Long bizId);
	
	public List<CRSTaxIDNumberLogVO> findByPartyLogIdAndChangeId(Long partyLogId, String bizSource, Long changeId);

	public List<CRSTaxIDNumberLogVO> findByPartyLogIdAndBizIdCopy(Long partyLogId, String bizSource, Long bizId,
			Integer copy);

	public int getTaxMaxCreateOrder(Long partyLogId, Integer copy);

	/**
	 * 依照傳入之CRSPartyLogVO使用PK反查CRSTaxIDNumberLog(T_CRS_RES_TIN_LOG)，
	 * 因法人bizSource(BIZ_SOURCE)會有兩種，故bizSource使用為or
	 * 
	 * @param partyLogId	[Long]
	 * @param bizSource1	[String]
	 * @param bizSource2	[String]
	 * @return resultList	[List<CRSTaxIDNumberLogVO>]
	 */
	public List<CRSTaxIDNumberLogVO> findByLogIdAndBizSource(Long partyLogId, String bizSource1, String bizSource2);

}
