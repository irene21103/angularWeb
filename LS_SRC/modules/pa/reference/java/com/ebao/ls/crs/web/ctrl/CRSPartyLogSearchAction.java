package com.ebao.ls.crs.web.ctrl;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.foundation.core.logging.Log;
import com.ebao.foundation.module.util.beanutils.BeanUtils;
import com.ebao.foundation.module.util.i18n.CodeTable;
import com.ebao.ls.crs.ci.CRSPartyCI;
import com.ebao.ls.crs.constant.CRSCodeCst;
import com.ebao.ls.crs.data.bo.CRSPartyLog;
import com.ebao.ls.crs.identity.CRSPartyLogService;
import com.ebao.ls.crs.vo.CRSPartyLogVO;
import com.ebao.ls.crs.web.WebWarningMessage;
import com.ebao.ls.crs.web.form.CRSPartyLogSearchForm;
import com.ebao.ls.pa.nb.util.NBUtils;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.framework.GenericAction;
import com.ebao.pub.i18n.lang.StringResource;
import com.ebao.pub.security.AppUser;

public class CRSPartyLogSearchAction extends GenericAction {

	private static final Log log = Log.getLogger(CRSPartyLogSearchAction.class);
	
	private static final String MSG_I18N_STR_ID_REMOVE= "MSG_101986";
	private static final String MSG_I18N_STR_ID_REVOKE= "MSG_108808";

	private static final String ACTION_1_SUCCESS = "success";

	@Resource(name=CRSPartyLogService.BEAN_DEFAULT)
	private CRSPartyLogService logServ;

	@Resource(name = CRSPartyCI.BEAN_DEFAULT)
	private CRSPartyCI partyCI;
	
	@Override
	public ActionForward process(ActionMapping mapp, ActionForm actionForm, HttpServletRequest req, HttpServletResponse resp)
			throws Exception {
		CRSPartyLogSearchForm inputForm = (CRSPartyLogSearchForm)actionForm;
		if(StringUtils.isEmpty(inputForm.getActionType())) {
			req.getSession().removeAttribute(CRSCodeCst.WEB_ACTION_TYPE__LOGIN_ACTION_TYPE);
			inputForm = new CRSPartyLogSearchForm();
			return mapp.findForward(ACTION_1_SUCCESS);
		}
		
		if(CRSCodeCst.WEB_ACTION_TYPE__QUERY.equals(inputForm.getActionType())){
			/* 畫面查詢條件校驗 */
			String policyType = StringUtils.trim(inputForm.getPolicyType());
			String certiCode = StringUtils.trim(inputForm.getCertiCode());
			String name = StringUtils.trim(inputForm.getName());
			String policyCode = StringUtils.trim(inputForm.getPolicyCode());
			String crsType = StringUtils.trim(inputForm.getCrsType());
			
			java.util.List<String> msgInfoList  = validateInput(policyType, certiCode, name, policyCode, crsType);
			if(CollectionUtils.isNotEmpty(msgInfoList)) {
				inputForm.setMsgInfoList(msgInfoList);
				return mapp.findForward(ACTION_1_SUCCESS);
			}
			/* 查詢 CRS 歷史資訊 */
			java.util.List<CRSPartyLog>  resultList = this.query(policyType, certiCode, name, policyCode, crsType);
			if(CollectionUtils.isNotEmpty(resultList)) {
				inputForm.setTotalSize(CollectionUtils.size(resultList));
				inputForm.setResultVOList(getPaging(resultList, inputForm.getPageNo(), inputForm.getPageSize()));
			}else {
				WebWarningMessage msg = new WebWarningMessage("MSG-ERR-CRS-001","查詢不到CRS對象檔");
				inputForm.setMsgInfoList(msg.toMsgInfoList());
			}
		}
		if (CRSCodeCst.WEB_ACTION_TYPE__REMOVE.equals(inputForm.getActionType())) {
			CRSPartyLogVO logVO = logServ.load(inputForm.getLogId());
			if (!this.validateDocSignAndBuildType(logVO)) {
				String showMsg = "該筆資料不得進行 %s 處理 [聲明書簽署狀態 對應 建檔狀態] 規則";
				WebWarningMessage msg = new WebWarningMessage("MSG-ERR-CRS-003", String.format(showMsg, this.getEbaoI18NStr(MSG_I18N_STR_ID_REMOVE))); // 刪除
				inputForm.setMsgInfoList(msg.toMsgInfoList());
				return mapp.findForward(ACTION_1_SUCCESS);
			} else {
				this.logServ.markRemove(inputForm.getLogId());
				String removeMsg = "身份證字號/統一編號 = %s <br/>姓名/公司名 = %s <br/>文件簽署狀態=%s <br/>已標記刪除";
				String crsType = CodeTable.getCodeDesc("T_CRS_PARTY_TYPE", logVO.getCrsType());
				WebWarningMessage msg = new WebWarningMessage("MSG-INFO-CRS-001", String.format(removeMsg, logVO.getCertiCode(), logVO.getName(), crsType));
				inputForm.setMsgInfoList(msg.toMsgInfoList());
				inputForm = new CRSPartyLogSearchForm(); // 初始資料
			}
			return mapp.findForward(ACTION_1_SUCCESS);
		}
		if (CRSCodeCst.WEB_ACTION_TYPE__REVOKE.equals(inputForm.getActionType())) {
			CRSPartyLogVO logVO = logServ.load(inputForm.getLogId());
			if (!this.validateDocSignAndBuildType(logVO)) {
				String showMsg = "該筆資料不得進行 %s 處理 [聲明書簽署狀態 對應 建檔狀態] 規則";
				WebWarningMessage msg = new WebWarningMessage("MSG-ERR-CRS-003", String.format(showMsg, this.getEbaoI18NStr(MSG_I18N_STR_ID_REVOKE))); // 撤銷
				inputForm.setMsgInfoList(msg.toMsgInfoList());
			} else {
			
				/* (2)	CRS的更新都是先產生t_crs_party_log,再call PKG_LS_CRS_CI.p_log_2_crs更新t_crs_party,紅色星號=目前t_crs_party的資料(t_crs_party_log. LAST_CMT_FLG = ‘Y’)  */				
				CRSPartyLogVO desVO  = new CRSPartyLogVO();
				BeanUtils.copyProperties(desVO, logVO);
				desVO.setLogId(null);
				desVO.setBuildType(CRSCodeCst.BUILD_TYPE__CANCEL);
				logServ.save(desVO);
				
				partyCI.submitByLogId(desVO.getLogId());
				
				String revokeMsg = "身份證字號/統一編號 = %s <br/>姓名/公司名 = %s <br/>文件簽署狀態=%s <br/>已標記撤銷";
				String crsType = CodeTable.getCodeDesc("T_CRS_PARTY_TYPE", logVO.getCrsType());
				WebWarningMessage msg = new WebWarningMessage("MSG-INFO-CRS-001", String.format(revokeMsg, logVO.getCertiCode(), logVO.getName(), crsType));
				inputForm.setMsgInfoList(msg.toMsgInfoList());
				inputForm = new CRSPartyLogSearchForm(); // 初始資料
			}
			return mapp.findForward(ACTION_1_SUCCESS);
		}
		return mapp.findForward(ACTION_1_SUCCESS);
	}

	/**
	 * 針對 [聲明書簽署狀態 對應 建檔狀態 規則]作檢核
	 * 
	 * @param logVO [CRSPartyLogVO]
	 * @return isValidate [boolean]
	 */
	private boolean validateDocSignAndBuildType(CRSPartyLogVO logVO) {
		boolean isValidate = Boolean.TRUE;
		String buildType = NBUtils.trim(logVO.getBuildType()); // 建檔狀態
		String docSignStatus = NBUtils.trim(logVO.getDocSignStatus()); // 聲明書簽署狀態

		// 01-已通知未簽署  ||  (01-已建檔、05-新契約覆核註記)
		if (CRSCodeCst.CRS_DOC_SIGN_STATUS__NOTIFY.equals(docSignStatus)) {
			if (CRSCodeCst.BUILD_TYPE__COMPLETE.equals(buildType)) {isValidate = Boolean.FALSE;}
			if (CRSCodeCst.BUILD_TYPE__UNB_LOG.equals(buildType)) {isValidate = Boolean.FALSE;}
		}
		// 02-已簽署   ||  (04-已建檔、05-新契約覆核註記06-已註記)
		if (CRSCodeCst.CRS_DOC_SIGN_STATUS__SIGNOFF.equals(docSignStatus)) {
			if (CRSCodeCst.BUILD_TYPE__CANCEL.equals(buildType)) {isValidate = Boolean.FALSE;}
			if (CRSCodeCst.BUILD_TYPE__UNB_LOG.equals(buildType)) {isValidate = Boolean.FALSE;}
			if (CRSCodeCst.BUILD_TYPE__STAMPED.equals(buildType)) {isValidate = Boolean.FALSE;}
		}
		// 03-不願簽署    ||  (04-已建檔、05-新契約覆核註記、06-已註記)
		if (CRSCodeCst.CRS_DOC_SIGN_STATUS__NONE_WILLING.equals(docSignStatus)) {
			if (CRSCodeCst.BUILD_TYPE__CANCEL.equals(buildType)) {isValidate = Boolean.FALSE;}
			if (CRSCodeCst.BUILD_TYPE__UNB_LOG.equals(buildType)) {isValidate = Boolean.FALSE;}
			if (CRSCodeCst.BUILD_TYPE__STAMPED.equals(buildType)) {isValidate = Boolean.FALSE;}
		}
		// 04-未回覆 - 待人工判定  ||  (01-已建檔、04-已建檔、05-新契約覆核註記)
		if (CRSCodeCst.CRS_DOC_SIGN_STATUS__NONE_REPLY.equals(docSignStatus)) {
			if (CRSCodeCst.BUILD_TYPE__COMPLETE.equals(buildType)) {isValidate = Boolean.FALSE;}
			if (CRSCodeCst.BUILD_TYPE__CANCEL.equals(buildType)) {isValidate = Boolean.FALSE;}
			if (CRSCodeCst.BUILD_TYPE__UNB_LOG.equals(buildType)) {isValidate = Boolean.FALSE;}
		}
		// 05-批次判定  ||  (02-已暫存、03-已刪除、04-已建檔、05-新契約覆核註記、06-已註記)
		if (CRSCodeCst.CRS_DOC_SIGN_STATUS__BATCH_DETERMINE.equals(docSignStatus)) {
			if (CRSCodeCst.BUILD_TYPE__BUILDING.equals(buildType)) {isValidate = Boolean.FALSE;}
			if (CRSCodeCst.BUILD_TYPE__REMOVE.equals(buildType)) {isValidate = Boolean.FALSE;}
			if (CRSCodeCst.BUILD_TYPE__CANCEL.equals(buildType)) {isValidate = Boolean.FALSE;}
			if (CRSCodeCst.BUILD_TYPE__UNB_LOG.equals(buildType)) {isValidate = Boolean.FALSE;}
			if (CRSCodeCst.BUILD_TYPE__STAMPED.equals(buildType)) {isValidate = Boolean.FALSE;}
		}
		// 06-人工判定    ||  (04-已建檔、05-新契約覆核註記、06-已註記)
		if (CRSCodeCst.CRS_DOC_SIGN_STATUS__MANUAL_DETERMINE.equals(docSignStatus)) {
			if (CRSCodeCst.BUILD_TYPE__CANCEL.equals(buildType)) {isValidate = Boolean.FALSE;}
			if (CRSCodeCst.BUILD_TYPE__UNB_LOG.equals(buildType)) {isValidate = Boolean.FALSE;}
			if (CRSCodeCst.BUILD_TYPE__STAMPED.equals(buildType)) {isValidate = Boolean.FALSE;}
		}
		return isValidate;
	}

	private java.util.List<String> validateInput(String policyType, String certiCode, String name, String policyCode, String crsType){
		WebWarningMessage msg = new WebWarningMessage();
		if( StringUtils.isEmpty(crsType)  && StringUtils.isEmpty(policyType) && StringUtils.isEmpty(certiCode) && StringUtils.isEmpty(name) && StringUtils.isEmpty(policyCode)) {
			msg.add("MSG_1258463"); // 查詢至少擇一輸入
		}
		if( (StringUtils.isNotEmpty(crsType) || StringUtils.isNotEmpty(policyType)) && StringUtils.isEmpty(certiCode) && StringUtils.isEmpty(name) && StringUtils.isEmpty(policyCode) ) {
			msg.add("CERTI_CODE", "(1) " + this.getEbaoI18NStr("MSG_70") + "/" + this.getEbaoI18NStr("MSG_1250780")) // 身分證字號/統一編號
					.add("NAME", "(2) " + this.getEbaoI18NStr("MSG_1511") + "/公司名") // 姓名/公司名
					.add("POLICY_CODE", "(3) " + this.getEbaoI18NStr("MSG_30426")) // 保單號碼
					.add("PROCESS_CODE", "亦需至少擇一輸入");			

		}
		return msg.toMsgInfoList();
	}
	
	private java.util.List<CRSPartyLog> query(String policyType, String certiCode, String name, String policyCode,
			String crsType) {
		return this.logServ.queryPartyLog(policyType, certiCode, name, policyCode, crsType);
	}

	private java.util.List<CRSPartyLogVO> getPaging(java.util.List<CRSPartyLog> resultList, Integer pageNo, Integer pageSize){
		if(CollectionUtils.isEmpty(resultList))
			return null;
		int from = Math.max(0, (pageNo -1) * pageSize);
        int to = Math.min(resultList.size(), pageNo *  pageSize);
        AppUser user = AppContext.getCurrentUser();
        return logServ.parseUIRows(resultList.subList(from, to), user.getUserId());
	}
	
	/**
	 * 取得eBao i18n的STR_ID對應之繁體中文字串
	 * 
	 * @param strId[String]
	 * @return String
	 */
	private String getEbaoI18NStr(String strId) {
		return StringResource.getStringData(strId, AppContext.getCurrentUser().getLangId());
	}
}
