package com.ebao.ls.callout.ctrl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.foundation.module.util.beanutils.BeanUtils;
import com.ebao.ls.callout.bs.CalloutTransService;
import com.ebao.ls.escape.helper.EscapeHelper;
import com.ebao.ls.pa.pub.ci.PolicyCI;
import com.ebao.ls.pa.pub.service.PolicyService;
import com.ebao.ls.pty.ci.CustomerCI;
import com.ebao.ls.pty.ci.EmployeeCI;
import com.ebao.ls.pty.ds.CustomerService;
import com.ebao.ls.pty.ds.DeptService;
import com.ebao.ls.pty.ds.vo.PersonSearchConditionVO;
import com.ebao.ls.pty.service.PartyService;
import com.ebao.ls.pty.vo.CustomerVO;
import com.ebao.pub.Cst;
import com.ebao.pub.framework.GenericAction;
import com.ebao.pub.util.StringUtils;

public class CalloutOnlineCustomerDialogAction extends GenericAction {
    
    @Resource(name = CalloutTransService.BEAN_DEFAULT)
    private CalloutTransService calloutTransService;

    @Resource(name = DeptService.BEAN_DEFAULT)
    private DeptService deptDS;

    @Resource(name = PolicyService.BEAN_DEFAULT)
    private PolicyService policyService;

    @Resource(name = PartyService.BEAN_DEFAULT)
    private PartyService partyService;

    @Resource(name = EmployeeCI.BEAN_DEFAULT)
    private EmployeeCI employeeService;
    
    @Resource(name = PolicyCI.BEAN_DEFAULT)
    private PolicyCI policyCI;

    @Resource(name = CustomerCI.BEAN_DEFAULT)
    private CustomerCI customerCI;

    @Resource(name = CustomerService.BEAN_DEFAULT)
    private CustomerService customerDS;
    
    @Override
    public ActionForward process(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        
    	CalloutOnlineCustomerDialogForm cForm = (CalloutOnlineCustomerDialogForm) form;
        PersonSearchConditionVO personSearchConditionVO = (PersonSearchConditionVO) BeanUtils
                .getBean(PersonSearchConditionVO.class, cForm);
        
        String pageNo = "1";
        String pageSize = Cst.PAGE_SIZE + "";
        if (request.getParameter("pageNo") != null
                        && request.getParameter("pageSize") != null) {
            pageNo = EscapeHelper.escapeHtml(request).getParameter("pageNo"); /** Trust Boundary Violation */
            pageSize = EscapeHelper.escapeHtml(request).getParameter("pageSize");/** Trust Boundary Violation */
            personSearchConditionVO.setPageNo(Integer.valueOf(pageNo));
            personSearchConditionVO.setPageSize(Integer.valueOf(pageSize));
        }
        
        Map<String, Object> customerMap = new HashMap<String, Object>();
        Integer totalsize = 0;
        if(!StringUtils.isNullOrEmpty(personSearchConditionVO.getCertiCode())){
        	customerMap = customerCI.getPersonListAndSize(personSearchConditionVO);
        	cForm.setCustomerList((List<CustomerVO>) customerMap.get("RESULT_LIST"));
            totalsize = (Integer) customerMap.get("TOTAL_SIZE");
        }
        cForm.setPageNo(Integer.valueOf(pageNo));
        cForm.setPageSize(Integer.valueOf(pageSize));
        cForm.setTotalSize(totalsize);
        
        request.setAttribute("pageSize", pageSize);
        request.setAttribute("pageNo", pageNo);
        request.setAttribute("customerList", cForm.getCustomerList());
        request.setAttribute("certiCode", cForm.getCertiCode());
        request.setAttribute("totalSize", totalsize);
        return mapping.findForward("success");
    }
    
}
