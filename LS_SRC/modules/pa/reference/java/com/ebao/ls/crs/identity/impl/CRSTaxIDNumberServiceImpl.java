package com.ebao.ls.crs.identity.impl;

import com.ebao.ls.crs.data.bo.CRSTaxIDNumber;
import com.ebao.ls.crs.data.identity.CRSTaxIDNumberDAO;
import com.ebao.ls.crs.identity.CRSTaxIDNumberService;
import com.ebao.ls.crs.vo.CRSTaxIDNumberVO;
import com.ebao.ls.foundation.service.GenericServiceImpl;

public class CRSTaxIDNumberServiceImpl extends GenericServiceImpl<CRSTaxIDNumberVO, CRSTaxIDNumber, CRSTaxIDNumberDAO>
implements CRSTaxIDNumberService {

	@Override
	protected CRSTaxIDNumberVO newEntityVO() {
		return new CRSTaxIDNumberVO();
	}

}
