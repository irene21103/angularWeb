package com.ebao.ls.uw.ds;

import java.util.List;

import javax.annotation.Resource;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import com.ebao.ls.foundation.service.GenericServiceImpl;
import com.ebao.ls.pa.nb.bs.ProposalProcessService;
import com.ebao.ls.pa.pub.data.bo.UwChangeMsg;
import com.ebao.ls.pa.pub.vo.UwChangeMsgVO;
import com.ebao.ls.pub.CodeCst;
import com.ebao.ls.pub.cst.UwChangeCode;
import com.ebao.ls.uw.data.TUwChangeMsgDelegate;
import com.ebao.pub.framework.GenericException;
import com.ebao.pub.workflow.wfmodel.WfTaskInstance;

public class UwChangeMsgServiceImpl extends GenericServiceImpl<UwChangeMsgVO, UwChangeMsg, 
		TUwChangeMsgDelegate<UwChangeMsg>> implements UwChangeMsgService {


	@Resource(name = ProposalProcessService.BEAN_DEFAULT)
	protected ProposalProcessService proposalProcessService;
	

	@Override
	public UwChangeMsgVO save(UwChangeMsgVO entity) throws GenericException {

		//PCR 185207 + PCR 155619 核保任務列新增”核保中異動”欄位查詢及清單中顯示
		//新增時更新核保任務-核保中異動=Y
		if(entity.getListId() == null) {
			this.updateWFUwChangeMsg(entity.getPolicyId(), CodeCst.YES_NO__YES);	
		}
		
		return super.save(entity);
	}

	@Override
	public UwChangeMsgVO save(UwChangeMsgVO entity, boolean isHierarchy) throws GenericException {
	
		//PCR 185207 + PCR 155619 核保任務列新增”核保中異動”欄位查詢及清單中顯示
		//新增時更新核保任務-核保中異動=Y
		if(entity.getListId() == null) {
			this.updateWFUwChangeMsg(entity.getPolicyId(), CodeCst.YES_NO__YES);
		}
		
		return super.save(entity, true);
	}

	@Override
	protected UwChangeMsgVO newEntityVO() {
		return new UwChangeMsgVO();
	}
	
	@Override
	public List<UwChangeMsgVO> getListByPolicyId(Long policyId) {
		
		List<UwChangeMsgVO> result = dao.getListByPolicyId(policyId);
		return result;
	}
	
	@Override
	public List<UwChangeMsgVO> getListByPolicyId4Pos(Long policyId) {
		
		List<UwChangeMsgVO> result = dao.getListByPolicyId4Pos(policyId);
		return result;
	}
	

	@Override
	public List<UwChangeMsgVO> getListByUnderwriteId(Long underwriteId) {
		
		List<UwChangeMsgVO> result = dao.getListByUnderwriteId(underwriteId);
		return result;
	}
	
	@Override
	public List<Long> getPolicyIdListByChangeCode(String id, String type) {
		List<Long> result = dao.getPolicyIdListByChangeCode(id, type);
		return result;
	}

	@Override
	public boolean hasPendingChangeMsg(Long underwriteId) {

		Criterion condition1 = Restrictions.eq("underwriteId", underwriteId);
		/* 不為Y，待確認的異動訊息 */
		Criterion condition2 = Restrictions.ne("confirmIndi", CodeCst.YES_NO__YES); 

		List<UwChangeMsg> pendingChangeMsgList = dao.findByCriteria(Restrictions.and(condition1, condition2));

		if (pendingChangeMsgList.size() > 0) {
			return true;
		}
		return false;
	}

	@Override
	public UwChangeMsgVO findByUnderwriteId(Long underwriteId, String changeCode, Long bizId) {

		Criterion condition1 = Restrictions.eq("underwriteId", underwriteId);
		/* 不為Y，待確認的異動訊息 */
		Criterion condition2 = Restrictions.ne("confirmIndi", CodeCst.YES_NO__YES); 
		Criterion condition3 = Restrictions.eq("changeCode", changeCode);
		Criterion condition4 = Restrictions.eq("bizId", bizId);

		List<UwChangeMsg> pendingChangeMsgList = dao.findByCriteria(
				condition1, condition2, condition3, condition4 );

		if (pendingChangeMsgList.size() > 0) {
			return convertToVO(pendingChangeMsgList.get(0));
		}
		return null;
	}
	
	/**
	 * <p>Description : 更新核保任務列，核保中異動(Y/N) <br/>
	 * PCR 185207 + PCR 155619 核保任務列新增”核保中異動”欄位查詢及清單中顯示
	 * </p>
	 * <p>Created By : Simon.Huang</p>
	 * <p>Create Time : 2018年3月1日</p>
	 * @param policyId
	 * @param yesOrNo
	 */
	@Override
	public void updateWFUwChangeMsg(Long policyId, String yesOrNo) {
		WfTaskInstance wfTask = proposalProcessService.getCurrentTask(policyId);
		if ( wfTask != null) {
			if(!CodeCst.YES_NO__YES.equals(yesOrNo)) {
				yesOrNo = "";
			}
			proposalProcessService.updateWFVariable(wfTask, 
							ProposalProcessService.VARIABLE_NAME_UW_CHANGE_MSG, yesOrNo);
		}
	}

	/**
	 * <p>Description : PCR-412145 依法令公司裁罰案已符合消極指標須限縮核保標準(phase 2)<br/>
	 * 查詢某張保單某個身份證號是否出過「實際繳交保費人」的核保異動訊息
	 * </p>
	 * <p>Created By : Charlotte Wang</p>
	 * <p>Create Time : Oct 12, 2020</p>
	 * @param policyId
	 * @param certiCode
	 * @return
	 */
	@Override
	public UwChangeMsgVO findPaTraderUwChangeMsg(Long policyId, String certiCode) {
		Criterion condition1 = Restrictions.eq("policyId", policyId);
		Criterion condition3 = Restrictions.eq("changeCode", UwChangeCode.UW_CHANGE_CODE__PA_TRADER);
		Criterion condition4 = Restrictions.like("bizCode", certiCode, MatchMode.END);

		List<UwChangeMsg> paTraderChangeMsgList = dao.findByCriteria(condition1, condition3, condition4);

		if (paTraderChangeMsgList.size() > 0) {
			return convertToVO(paTraderChangeMsgList.get(0));
		}
		return null;
	}

}
