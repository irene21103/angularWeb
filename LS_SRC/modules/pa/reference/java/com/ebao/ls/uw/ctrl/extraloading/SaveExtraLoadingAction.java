package com.ebao.ls.uw.ctrl.extraloading;

import java.math.BigDecimal;
import java.util.Arrays;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.ls.pub.CodeCst;
import com.ebao.ls.uw.ctrl.ActionUtil;
import com.ebao.ls.uw.ctrl.UwGenericAction;
import com.ebao.ls.uw.ds.sp.CompleteUWProcessSp;
import com.ebao.ls.uw.util.Utils;
import com.ebao.ls.uw.vo.UwExtraLoadingVO;
import com.ebao.ls.uw.vo.UwPolicyVO;
import com.ebao.ls.uw.vo.UwProductVO;
import com.ebao.pub.framework.ExceptionFactory;
import com.ebao.pub.framework.GenericException;
import com.ebao.pub.framework.MultiWarning;

/**
 * <p>Title: GEL-UW</p>
 * <p>Description: extraloading submit action</p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: eBaoTech Corporation</p>
 * @author yixing.lu
 * @version 1.0
 */

public class SaveExtraLoadingAction extends UwGenericAction {

  public static final String BEAN_DEFAULT = "/uw/uwSaveExtra";
/**
   * @param mapping
   * @param form
   * @param request
   * @param response
   * added by hendry.xu to solve the same record in extra loading .
   */
  public MultiWarning processWarning(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {
    MultiWarning warning = new MultiWarning();

    Long preInsuredId = Long.valueOf(com.ebao.ls.encode.encoder.Encode.encodeForHTML(request).getParameter("preInsured"));
    String uwSourceType = com.ebao.ls.encode.encoder.Encode.encodeForHTML(request).getParameter("uwSourceType");
    boolean isNewbiz = false;
    if (CodeCst.UW_SOURCE_TYPE__NB.equals(uwSourceType)) {
      isNewbiz = true;
    }
    //	UwExtraLoadingVO extraLoadingVO = new UwExtraLoadingVO();
    extraLoadingActionHelper.processSameRecordWarning(request, warning,
        preInsuredId, isNewbiz);
    return warning;
  }
  /**
   * save extra loading action
   * @param mapping
   * @param form
   * @param req
   * @param res
   * @throws com.ebao.pub.framework.GenericException
   * @return
   */
  public ActionForward uwProcess(ActionMapping mapping, ActionForm form,
      HttpServletRequest req, HttpServletResponse res) throws GenericException {
    try {
      Long underwriteId = Long.valueOf(com.ebao.ls.encode.encoder.Encode.encodeForHTML(req).getParameter("underwriteId"));
      Long itemId = Long.valueOf(com.ebao.ls.encode.encoder.Encode.encodeForHTML(req).getParameter("itemIds"));
      Long insuredId = Long.valueOf(com.ebao.ls.encode.encoder.Encode.encodeForHTML(req).getParameter("insured"));
      UwExtraLoadingVO[] extraLoadingVOs;
      if (insuredId != null) {
        extraLoadingVOs = ActionUtil.calAndSaveExtraLoading(req, insuredId);
      } else {
        extraLoadingVOs = ActionUtil.calAndSaveExtraLoading(req, Long
            .valueOf(0));
      }

      UwProductVO productValue = extraLoadingActionHelper.initProductVO(req,
          underwriteId, itemId);
      productValue.setUwExtraLoadings(Arrays.asList(extraLoadingVOs));
      String gender = req.getParameter("gender");
      if (extraLoadingVOs != null) {
        for (int i = 0; i < extraLoadingVOs.length; i++) {
          if ((extraLoadingVOs[i].getEmValue() != null)
              && (extraLoadingVOs[i].getEmValue().intValue() >= 0)
              && ("1".equals(extraLoadingVOs[i].getExtraArith()))
              && (extraLoadingVOs[i].getExtraPara() == null || extraLoadingVOs[i]
                  .getExtraPara().compareTo(new BigDecimal(0)) == 0)) {
            extraLoadingActionHelper.doCalExRate(productValue, gender, i,
                extraLoadingVOs[i].getExtraArith(), insuredId, false);
            getUwPolicyDS().updateUwExtraLoading(
                productValue.getUwExtraLoadings().get(i));
          }
        }
      }

      // when cs-uw,synchronize records
      UwPolicyVO uwPolicyVO = getUwPolicyDS().findUwPolicy(underwriteId);
      if (Utils.isCsUw(uwPolicyVO.getUwSourceType())) {
        CompleteUWProcessSp.synUWInfo4CS(uwPolicyVO.getPolicyId(), uwPolicyVO
            .getUnderwriteId(), uwPolicyVO.getChangeId(), Integer.valueOf(2));
      }

      req.setAttribute("underwriteId", underwriteId);
      req.setAttribute("itemId", itemId);

    } catch (Exception e) {
      throw ExceptionFactory.parse(e);
    }
    return mapping.findForward("product");
  }

  @Resource(name = ExtraLoadingActionHelper.BEAN_DEFAULT)
  ExtraLoadingActionHelper extraLoadingActionHelper;
}