package com.ebao.ls.crs.batch.job;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.transaction.UserTransaction;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Session;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.ebao.foundation.common.lang.exception.ExceptionUtils;
import com.ebao.foundation.core.logging.Log;
import com.ebao.ls.batch.TGLBasePieceableJob;
import com.ebao.ls.crs.batch.service.CRSDueDiligenceListService;
import com.ebao.ls.crs.batch.vo.CRSTempVO;
import com.ebao.ls.crs.data.batch.CRSDueDiligenceListDAO;
import com.ebao.ls.crs.data.bo.CRSDueDiligenceList;
import com.ebao.ls.crs.vo.CRSDueDiligenceListVO;
import com.ebao.ls.cs.pub.helper.DateHelper;
import com.ebao.ls.fatca.constant.FatcaSurveyType;
import com.ebao.ls.pub.batch.impl.BatchJdbcTemplate;
import com.ebao.ls.pub.batch.impl.DataSourceWrapper;
import com.ebao.pub.batch.HibernateSession;
import com.ebao.pub.batch.help.BatchHelp;
import com.ebao.pub.batch.type.JobStatus;
import com.ebao.pub.util.Trans;
import com.ebao.pub.util.TransUtils;

/**
 * CRS高資產客戶經理人
 */
public class CrsHighAssetAgentBatchJob extends TGLBasePieceableJob implements ApplicationContextAware {
	
	public static final String BEAN_DEFAULT = "crsHighAssetAgentBatchJob";
	
	private final Log logger = Log.getLogger(CrsHighAssetAgentBatchJob.class);
	
	private static final boolean IS_DEBUG= false;
	
	@Resource(name = CRSDueDiligenceListService.BEAN_DEFAULT)
	private CRSDueDiligenceListService crsDueDiligenceListService;
	
	@Resource(name = CRSDueDiligenceListDAO.BEAN_DEFAULT)
    private CRSDueDiligenceListDAO crsDueDiligenceListDAO;

    private ApplicationContext ctx;
    
    private static BatchJdbcTemplate batchJdbcTemplate = new BatchJdbcTemplate(new DataSourceWrapper());
    
    private static NamedParameterJdbcTemplate namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(batchJdbcTemplate);
    
    private Date processDate;
    
    private String exeSql;

	public String getExeSql() {
		return exeSql;
	}

	public void setExeSql(String exeSql) {
		this.exeSql = exeSql;
	}

	private void log(String msg) {
		logger.info("[SYSOUT][CrsBenefitOwnerListBatchJob]" + msg);
		batchLogger.info(msg);
    }
	
	private void logWithId(String id, String msg) {
		
		if(!IS_DEBUG){
			return;
			}
		
		if (StringUtils.isNotBlank(id)) {
			msg = "[" + id + "] " + msg;
		}
		log(msg);
    }
	
    private void logWithLayout(String layout, Object... msg){
    	log(String.format(layout, msg));
    }
    
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		ctx=applicationContext;
	}

	@Override
	protected String getJobName() {
		return BatchHelp.getJobName();
	}
	
	/**
	 * 單筆測試用
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public int executeById(String id) throws Exception {
		preProcess();
		return execute(id);
	}

	@SuppressWarnings("unchecked")
	@Override
	protected Object[] prepareIds() throws Throwable {
	 
		StringBuffer sql = new StringBuffer();
		processDate = BatchHelp.getProcessDate();	
		//匯率取前一年度最後一天
	    Calendar calendar = Calendar.getInstance();
	    calendar.add(Calendar.YEAR, -1);
	    calendar.set(Calendar.MONTH, Calendar.DECEMBER);
	    calendar.set(Calendar.DATE, 31);
	    Date baseDate = calendar.getTime();
	    DateHelper.clearTime(baseDate);
	    
	    java.sql.Date processSqlDate = new java.sql.Date(baseDate.getTime());
		Map<String, Object> params = new HashMap<String, Object>();
        params.put("processDate",  processSqlDate);
        //PCR415240 調整條件經理客戶關係人調查條件:前一年度末帳戶所有人為個人且累計帳戶價值≥100萬美元(高資產客戶) 
	    sql.append(" select tfm1.party_id||'/'||tfm1.certi_code ");
		sql.append(" from ");
		sql.append(" ( ");
		sql.append(" select tfsp.account_owner_party_id party_id,tfsp.account_owner_certi_code certi_code, ");
		sql.append("        party_type, sum(policy_value*pkg_ls_crs_foreign_indi.f_get_us_exchage_rate(tfsp.money_id, :processDate)) policy_value "); 
		sql.append(" from t_fatca_survey_population tfsp, t_contract_master tcm, t_party party");
		sql.append(" where tfsp.policy_id=tcm.policy_id  ");
		sql.append(" and party.party_id=tfsp.account_owner_party_id and party_type=1  ");
		sql.append(" and (tfsp.policy_status in (1,2) or (tfsp.policy_status=3 and tcm.liability_state_date >= to_date(to_char(tfsp.ad_year||'0101'),'yyyymmdd') )) ");
		sql.append(" group by account_owner_party_id, account_owner_certi_code, party_type");
		sql.append(" having sum(policy_value*pkg_ls_crs_foreign_indi.f_get_us_exchage_rate(tfsp.money_id, :processDate)) >= 1000000");
		sql.append(" ) tfm1   ");	
		sql.append(" where  not exists (select 1 from t_crs_party crs ");
		sql.append("                    where  certi_code is not null ");
		sql.append("                    and    certi_code = tfm1.certi_code ");
		sql.append("                    and    crs.doc_sign_status ='02' ");
		sql.append("                    ) ");
		
		String sqlString = sql.toString();
	    log("prepareIds, SQL = " + sql);
	    List<String> itemList = namedParameterJdbcTemplate.queryForList(sqlString, params, String.class);
		logWithLayout("crsHighAssetAgentBatchJob total size = %,d " , itemList.size());
        return itemList.toArray();
	}
	
	@Override
	public int preProcess() throws Exception {

		StringBuffer sql = new StringBuffer();
		
		sql.append(" select * ");
		sql.append(" from ( ");
        sql.append(" select  tcm.policy_id, ");
        sql.append("         tcm.policy_code, ");
        sql.append("         tcm.liability_state policy_status,    ");
        sql.append("         tph.name target_name, ");
        sql.append("         tph.certi_code certi_code,      ");
        sql.append("         '01' role_type,     ");
        sql.append("         agt.agent_id service_agent,     ");
        sql.append("         agt.register_code agent_register_code,     ");
        sql.append("         agt.agent_name,     ");
        sql.append("         agt.business_cate agent_biz_cate,     ");
        sql.append("         org.channel_id channel_org,     ");
        sql.append("         org.channel_code,     ");
        sql.append("         org.channel_name,     ");
        sql.append("         decode(agt.business_cate, 2, 4, org.channel_type) channel_type,     ");
        sql.append("         rank() over (partition by tcm.service_agent order by tcm.policy_id desc) rank      ");
		sql.append(" from t_policy_holder tph, t_contract_master tcm,  ");
		sql.append("      t_agent agt, t_channel_org org   ");
		sql.append(" where tph.party_id =  :party_id ");
		sql.append(" and   tph.certi_code =  :certi_code ");
		sql.append(" and   tph.policy_id = tcm.policy_id ");
		sql.append(" and   (tcm.liability_state=1 or ");
		sql.append("       (tcm.liability_state in (2,3) and tcm.liability_state_date > to_date(to_char(sysdate,'yyyy')-1||'0101','yyyymmdd')))");
        sql.append(" and   service_agent = agt.agent_id ");
        sql.append(" and   agt.register_code <> 'BR01' ");
        sql.append(" and   agt.channel_org_id = org.channel_id ");
        sql.append(" and   decode(agt.business_cate, 2, 4, org.channel_type) in (1,2,4)");
        sql.append(" ) ");
        sql.append(" where rank = 1 ");
 
        this.setExeSql(sql.toString());
		return super.preProcess();
	}
	
	@Override
	protected int execute(String id) throws Exception {
		logWithId(id, "execute - start");
        
		String[] identityId = StringUtils.split(id, "/");
 
        String partyId     = identityId[0];
        String certiCode   = identityId[1];
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("party_id", partyId);
        params.put("certi_code", certiCode);
 
        List<Map<String,Object>> mapList= namedParameterJdbcTemplate.queryForList(this.getExeSql(),  params);
        if(CollectionUtils.isEmpty(mapList)) {
        	logWithId(id, "不符合處理條件，略過");
        	return JobStatus.EXECUTE_SUCCESS;
        }
        List<CRSTempVO> crsTempVOList = new LinkedList<CRSTempVO>();
        
        // 擷取查詢結果
        for (Map<String, Object> column : mapList) {
        	CRSTempVO crsTempVO = new CRSTempVO();
            
        	Long targetId = Long.valueOf(partyId);
        	String targetCertiCode = certiCode;
        	Long surveyId = MapUtils.getLong(column, "policy_id");
        	String targetName = StringUtils.trim(MapUtils.getString(column, "target_name",""));
        	String policyCode = StringUtils.trim(MapUtils.getString(column, "policy_code",""));
        	String roleType = StringUtils.trim(MapUtils.getString(column, "role_type",""));
        	
        	logWithId(id, "targetId = " + targetId + ", surveyId = " + surveyId
        			+ ", targetCertiCode = " + targetCertiCode + ",policyCode = " + policyCode + ", targetName = " + targetName );
        	
        	// 所有執行CRS高資產客戶經理人存入t_Crs_Due_Diligence_List
        	 CRSDueDiligenceListVO crsVO = new CRSDueDiligenceListVO ();
        	  
        	 crsVO.setTargetSurveyType(FatcaSurveyType.HIGH_ASSET_AGENT);
        	 crsVO.setPolicyId(surveyId);
         	 crsVO.setPolicyCode(policyCode);
         	 crsVO.setLiabilityStatus(MapUtils.getInteger(column, "policy_status"));
         	 crsVO.setPartyId(targetId);
        	 crsVO.setName(targetName);
        	 crsVO.setCertiCode(targetCertiCode);
        	 crsVO.setAgentId(MapUtils.getLong(column, "service_agent"));
         	 crsVO.setRegisterCode(StringUtils.trim(MapUtils.getString(column, "agent_register_code", "")));
         	 crsVO.setAgentName(StringUtils.trim(MapUtils.getString(column, "agent_name", "")));
         	 BigDecimal channelOrg = (BigDecimal) column.get("channel_org");
        	 if(channelOrg != null) {
        		crsVO.setChannelId(channelOrg.longValue());
        	 }
         	 crsVO.setChannelCode(StringUtils.trim(MapUtils.getString(column, "channel_code", "")));
        	 crsVO.setChannelName(StringUtils.trim(MapUtils.getString(column, "channel_name", "")));
        	 crsVO.setTargetSurveyDate(processDate);
        	 BigDecimal channelType = (BigDecimal)column.get("channel_type");
        	 if(channelType != null) {
        	 	crsVO.setChannelType(channelType.longValue());
        	 }
        	  
         	CRSDueDiligenceList resultCRSBO = crsDueDiligenceListDAO.searchCRSDueDiligenceList(targetId, surveyId, crsVO.getTargetSurveyType());
         	if (resultCRSBO == null) {
        		// 新紀錄
        		crsTempVO.setNewCRSRecord(true);
        	} else {
        		// 已有記錄
        		crsVO.setListId(resultCRSBO.getListId());
        		crsTempVO.setNewCRSRecord(false);
        	}
 
            crsTempVO.setCrsDueDiligenceListVO(crsVO);
            crsTempVOList.add(crsTempVO);
        }
        logWithId(id, "crsTempVOList.size() = " + crsTempVOList.size());
        
        UserTransaction trans = null;
        int successCount = 0;
        int errorCount = 0;
        for (CRSTempVO crsTempVO : crsTempVOList) {
        	try {
        		trans = TransUtils.getUserTransaction();
        		trans.begin();
        		CRSDueDiligenceListVO crsVO = crsTempVO.getCrsDueDiligenceListVO();
        		if(!crsVO.isEmpty() || crsVO != null){
        			// 更新業務員名單
        			crsDueDiligenceListService.updateCrsServiceAgent(crsVO);  
        			// 產生CRS報表數據
        			if (crsTempVO.isNewCRSRecord()) {
        				crsDueDiligenceListDAO.createOrUpdateVO(crsVO);
        			} else {
        				crsDueDiligenceListDAO.updateByVO(crsVO);
        			}
        			
        		}
                 
                successCount++;
                trans.commit();
                logWithId(id, "CRS建檔完成");
        	} catch (Exception ex) {
        		logWithId(id, "發生異常! 錯誤訊息 = " + ExceptionUtils.getFullStackTrace(ex));
        		errorCount++;
        		trans.rollback();
        	} finally {
        		trans = null;
        	}
        }
        logWithId(id, "successCount = " + successCount + ", errorCount = " + errorCount);
        logWithId(id, "execute - end");
        
        if (errorCount == 0) {
			return JobStatus.EXECUTE_SUCCESS;
		} else if (successCount > 0) {
			return JobStatus.EXECUTE_PARTIAL_SUCESS;
		} else {
			return JobStatus.EXECUTE_FAILED;
		}
	}
	
	/**
	 * 父類別的transation是以一片為單位，為了避免同片內一筆rollback時，把上一筆的變更也一同rollback，覆寫父類別的方法
	 */
	@Override
    public int exeSingleRecord(String id) throws Exception {
        long start = System.currentTimeMillis();
        
        UserTransaction trans = null;
        int ret = 0;
        try {
            trans = Trans.getUserTransaction();
            trans.begin();
        
            ret = this.execute(id);
            this.updateStatus(id, ret);
            if (ret != JobStatus.EXECUTE_SUCCESS) {
                batchLogger.error("執行不成功! ID:{0} 狀態:{1}", id, ret);
            }
            long stop = System.currentTimeMillis();
            if(enableStopwatch()){
                batchLogger.debug("ID:{0} 執行花費時間:{1} ms", id, stop - start);
            }
            trans.commit();
        }catch(Exception e){
            trans.rollback();
            batchLogger.error("執行不成功! ID:{0} errMsg:{1}", id, e.toString());
        }
        return ret;
    }
	
	/**
	 * 更新t_batch_job_record的分片紀錄執行結果
	 * @param id
	 * @param status
	 */
	private void updateStatus(String id, int status) {
        Long runId = BatchHelp.getRunId();

        Session s = HibernateSession.currentSession();
        String sql = "update t_batch_job_record set status=?, finish_time=sysdate where run_id = ? and identity=?";
        Connection con = s.connection();
        PreparedStatement ps = null;
        try {
            ps = con.prepareStatement(sql);
            ps.setInt(1, status);
            ps.setLong(2, runId);
            ps.setString(3, id);
            ps.execute();
        } catch (SQLException e) {
            //e.printStackTrace();
        	batchLogger.error("TGLBasePieceableJob.updateStatus error", e);
        } finally {
        	try {
        		if(ps!=null) {
        			ps.close();
        		} 		
        	} catch(Exception e) {
        		
        	}
        }
    }	
	
	public Date getProcessDate() {
		return processDate;
	}

	public void setProcessDate(Date processDate) {
		this.processDate = processDate;
	}
}