package com.ebao.ls.crs.batch.service.impl;

import java.io.StringWriter;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;

import com.ebao.ls.cmu.pub.data.DocumentDataDao;
import com.ebao.ls.cmu.pub.data.bo.DocumentData;
import com.ebao.ls.crs.batch.service.CrsFollowUpListService;
import com.ebao.foundation.commons.exceptions.GenericException;
import com.ebao.foundation.core.logging.Log;
import com.ebao.ls.fatca.batch.data.FatcaDueDiligenceListDAO;
import com.ebao.ls.crs.batch.service.impl.AbstractCrsBatchService;
import com.ebao.ls.crs.ci.CRSPartyCI;
import com.ebao.ls.crs.ci.CRSPartyReportVO;
import com.ebao.ls.crs.constant.CRSCodeCst;
import com.ebao.ls.crs.constant.CRSCodeType;
import com.ebao.ls.crs.data.batch.CrsRightNotifyDAO;
import com.ebao.ls.crs.data.bo.CrsRightNotify;
import com.ebao.ls.crs.data.identity.CRSPartyDAO;
import com.ebao.ls.crs.identity.CRSPartyLogService;
import com.ebao.ls.crs.identity.CRSPartyService;
import com.ebao.ls.crs.vo.CRSPartyLogVO;
import com.ebao.ls.crs.vo.CRSPartyVO;
import com.ebao.ls.cs.commonflow.data.TChangeDelegate;
import com.ebao.ls.cs.commonflow.data.bo.Change;
import com.ebao.ls.fatca.constant.FatcaDept;
import com.ebao.ls.fatca.constant.FatcaDocSignStatus;
import com.ebao.ls.fatca.constant.FatcaRole;
import com.ebao.ls.fatca.constant.FatcaSurveyType;
import com.ebao.ls.fatca.constant.FatcaUnCooperativeType;
import com.ebao.ls.fatca.data.bo.FatcaDueDiligenceList;
import com.ebao.ls.fatca.data.hist.FatcaRightNotifyDAO;
import com.ebao.ls.fatca.util.FatcaDBeanQuery;
import com.ebao.ls.fatca.util.FatcaJDBC;
import com.ebao.ls.fatca.vo.FatcaDueDiligenceListVO;
import com.ebao.ls.fatca.vo.FatcaIdentityVO;
import com.ebao.ls.fatca.vo.FatcaReportVO;
import com.ebao.ls.notification.event.clm.FmtClm0610Event;
import com.ebao.ls.notification.event.cs.FmtPos0350Event;
import com.ebao.ls.notification.event.cs.FmtPos0500Event;
import com.ebao.ls.notification.extension.event.clm.NotificationService;
import com.ebao.ls.pa.pty.service.CertiCodeCI;
import com.ebao.ls.pa.pub.bs.AddressService;
import com.ebao.ls.pa.pub.bs.BeneficiaryService;
import com.ebao.ls.pa.pub.bs.PolicyHolderService;
import com.ebao.ls.pa.pub.vo.BeneficiaryVO;
import com.ebao.ls.pa.pub.vo.PolicyHolderVO;
import com.ebao.ls.pty.ds.CustomerService;
import com.ebao.ls.pty.vo.AddressVO;
import com.ebao.ls.pty.vo.CompanyCustomerVO;
import com.ebao.ls.pty.vo.CustomerVO;
import com.ebao.ls.pub.CodeCst;
import com.ebao.ls.pub.cst.YesNo;
import com.ebao.ls.pub.util.ClobDao;
import com.ebao.ls.pub.util.data.bo.Clob;
import com.ebao.pub.batch.type.LogLevel;
import com.ebao.pub.batch.util.BatchLogUtils;
import com.ebao.pub.event.ApplicationEventVO;
import com.ebao.pub.event.EventService;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.framework.ExceptionFactory;
import com.ebao.pub.para.Para;
import com.ebao.pub.util.DateUtils;
import com.tgl.tools.gen.JaxbTool;

public class CrsFollowUpListServiceImpl 
		extends AbstractCrsBatchService<FatcaDueDiligenceListVO>
		implements CrsFollowUpListService {

	private final Log log =Log.getLogger(CrsFollowUpListServiceImpl.class);
	private final static Long FMT_POS_0500 = 30089L;
	private final static Long FMT_CLM_0610 = 30044L;
	private final static String nonResponseDays = Para.getParaValue(2061000051L);
	
	@Resource(name = EventService.BEAN_DEFAULT)
	private EventService eventService;
	  
	@Resource(name=CRSPartyService.BEAN_DEFAULT)
	private CRSPartyService partyService;
	
	@Resource(name = FatcaDueDiligenceListDAO.BEAN_DEFAULT)
    private FatcaDueDiligenceListDAO dao;
	
	@Resource(name = CrsRightNotifyDAO.BEAN_DEFAULT)
    private CrsRightNotifyDAO rightNotifyDao;
	
	@Resource(name = CRSPartyCI.BEAN_DEFAULT)
	private CRSPartyCI crsPartyCI;
	
	@Resource(name=CustomerService.BEAN_DEFAULT)
	private  CustomerService custServ;
	
	@Resource(name=AddressService.BEAN_DEFAULT)
    private AddressService addrServ;
	
	@Resource(name=DocumentDataDao.BEAN_DEFAULT)
	private DocumentDataDao documentDataDao;
	
	@Resource(name = CRSPartyLogService.BEAN_DEFAULT)
	private CRSPartyLogService crsPartyLogService;
	
	private void syso(Integer level,Long listId, Object msg){
     //   log.info("[CrsFollowUpListServiceImpl]" + "[Source ListID:"+listId+"]" + msg);        
        BatchLogUtils.addLog(level, null, "[CrsFollowUpListServiceImpl]" + "[Source ListID:"+listId+"]" + msg);
        //System.err.println("[CrsFollowUpListServiceImpl]" + "[Source ListID:"+listId+"]" + msg);
    }
	
	private String setMsg(String layout,Object... msg){
		return (String.format(layout, msg));
	}
        
        
	@Override
	public void sendCrsLetter(FatcaDueDiligenceList data, Long reportId,
			Date processDate, String targetSurveyType) throws GenericException {
		  
		/* 填寫資料至跟催名單 */
		  Long noData = 0L;
	  	  Long listId = new Long(data.getListID());  //source:t_fatca_due_diligence_list.list_id	  	  
	      FatcaDueDiligenceListVO vo = new FatcaDueDiligenceListVO();
	      data.copyToVO(vo, Boolean.FALSE);	       	      

	      java.util.List<String> benefitAgentList = Arrays.asList(new String[] {
	        		FatcaSurveyType.REAL_ANNUAL_BENEFICIARY_FOLLOW_UP_TARGET, 
	        		FatcaSurveyType.BENEFIT_OWNER_FOLLOW_UP_TARGET,
	        		FatcaSurveyType.RENEW_BENEFICIARY_FOLLOW_UP_TARGET});
	        
	        if(StringUtils.isNotEmpty(targetSurveyType) && benefitAgentList.contains(targetSurveyType)) {        	
	        	updateServiceAgent(vo);//更新業務員名單, 受益人類型跟催才需更新 
	        }	     
	      	      
	      vo.setListID(null);
	      vo.setTargetSurveyType(targetSurveyType); //跟催類型
	      vo.setSurveySystemType(CRSCodeCst.SURVEY_SYSTEM_TYPE_CRS );
	      vo.setNotifyDate(data.getTargetSurveyDate()); //資料來源的調查通知日期
	      vo.setTargetSurveyDate(processDate);	   //跟催批次日期   
	      FatcaDueDiligenceList entity = new FatcaDueDiligenceList();  //新產生之跟催資料
	      entity.copyFromVO(vo, Boolean.TRUE, Boolean.FALSE);
	      
	      String targetCertiCode = data.getTargetCertiCode() ;
	      String targetName = data.getTargetName() ;
	      String policyCode = data.getPolicyCode() ;
	      Long  policyId = data.getPolicyId() ;
	      String crsPartyCriterion = null ;
	      CRSPartyVO crsPartyVo = null ;
	      //根據CertiCode或Policy+Name找出 CRS Party資料
	      if (StringUtils.isNotEmpty(targetCertiCode)) {	     
	    	  crsPartyVo = crsPartyCI.findParty(targetCertiCode);
	    	  crsPartyCriterion = targetCertiCode ;
	      }else { 
	    	  if (StringUtils.isEmpty(policyCode)|| StringUtils.isEmpty(targetName)){
	    		  syso(LogLevel.WARN,listId,"targetCertiCode,targetName,policyCode are ALL empty!") ;
		    	  return ;   
	    	  }	    		  
	    	  crsPartyVo = crsPartyCI.findPartybyPolicyName(policyCode, targetName);
	    	  crsPartyCriterion = policyCode + "/" + targetName;
	      }
	      
	      if (crsPartyVo == null || crsPartyVo.isEmpty()){
	    	  syso(LogLevel.WARN,listId,"No match record in T_CRS_PARTY:"+crsPartyCriterion) ;
	    	  return ;  
	      } 
	      
	      String[] docSignStatusList = new String[]{CRSCodeCst.CRS_DOC_SIGN_STATUS__SIGNOFF, CRSCodeCst.CRS_DOC_SIGN_STATUS__NONE_WILLING};
	        /* 判斷對象不為『CRS 文件簽署狀態』=02,03 */
	      if(StringUtils.isNotEmpty(crsPartyVo.getDocSignStatus()) && Arrays.asList(docSignStatusList).contains(crsPartyVo.getDocSignStatus())){
	    	 syso(LogLevel.INFO,listId,"CRSDocSignStatus:"+crsPartyVo.getDocSignStatus()) ;   
	    	 return;
	      }
	      
	      //判斷同一人是否已進行過跟催。若fatca已先產生跟催 ，將類別由FATCA改為CRS/FATCA即可。
	      //IR353344  int chk = isfollowUpListExist(targetCertiCode,policyId,targetName,targetSurveyType,processDate);
	      int chk = isfollowUpListExist(data.getTargetId(),policyId,targetSurveyType,processDate);
	      if (chk == 1 ) {
	    	  syso(LogLevel.INFO,listId,setMsg("[TargetSurveyType done by FATCA][CRS產生權益通知函數據] TargetSurveyType= %s, policy_code = %s, certi_Code= %s ", targetSurveyType, data.getPolicyCode(), targetCertiCode)) ;
	      }else if (chk == 2 ){
	    	  syso(LogLevel.INFO,listId,setMsg("[TargetSurveyType already exist][CRS產生權益通知函數據] TargetSurveyType= %s, policy_code = %s, certi_Code= %s ", targetSurveyType, data.getPolicyCode(), targetCertiCode)) ;	    	  
	    	  return ;
	      }
	    	  
	      //檢查是否已發過權益通知函
	      /*  CrsRightNotify qryList = rightNotifyDao.quryByCrsId(crsPartyVo.getCrsId());		  	   	 
	      if  (qryList !=null && qryList.getListId() !=null ) {
	    		syso(LogLevel.INFO,listId,"Right Notice already sent. CRS_ID=" + crsPartyVo.getCrsId()) ;
		    	  return ;
	    	} */
	      
	      //判斷是個人或是公司，再決定由 T_CUSTOMER OR T_COMPANY_CUSTOMER 取address 	
	      AddressVO addressVO = null;
	        if(CodeCst.PARTY_TYPE__INDIV_CUSTOMER.equals(data.getTargetPartyType())){
	            CustomerVO customerVO = custServ.getIndivCustomer(data.getTargetId());
	            Long addressId = customerVO.getAddressId();
	            if(addressId != null) {
	            	addressVO = addrServ.find(addressId);
	            }
	        }else{
	            CompanyCustomerVO companyVO = custServ.getOrganCustomer(data.getTargetId());
	            Long addressId = companyVO.getAddressId();
	            if(addressId != null) {
	            	addressVO = addrServ.find(addressId);
	            }
	        }
	        
	      //依據list_id取出原盡職調查資料，判斷理賠資訊
	      FatcaDueDiligenceList record = dao.load(new Long(listId))  ;
	      if (record == null || record.getListID() == null)
	            return;
	      
	      if (FatcaSurveyType.CLAIM_PAYMENT_BENEFICIARY.equals(record.getTargetSurveyType()) ) {	    	  
	    	  reportId = record.getReportId();
	    	  if (reportId == null || reportId == 0L){
	    		   String msg = setMsg(String.format("[CRS產生權益通知函數據][ERROR-No report_id] policy_id = %d, policy_code= %s ,clob_id = %d ", data.getPolicyId(), data.getPolicyCode(), reportId));
	               syso(LogLevel.ERROR ,listId,msg) ;	            	
	            	return ; 
	    	   }
	    	  
	    	  //理賠受益人
	    	    Clob clmClob = ClobDao.load(new Long(reportId));
	            FatcaReportVO report = JaxbTool.toObject(clmClob.getContent(), FatcaReportVO.class);
	            Long caseId = report.getCaseId();
	            if (caseId==null){	  
	            	String msg = setMsg(String.format("[CRS產生權益通知函數據][ERROR-找不到理賠受理編號] policy_id = %d, policy_code= %s ,clob_id = %d ", data.getPolicyId(), data.getPolicyCode(), reportId));
	            	syso(LogLevel.ERROR ,listId,msg) ;	            	
	            	return ;
	            }
	            
	            FmtClm0610Event event = new FmtClm0610Event(ApplicationEventVO.newInstance(data.getPolicyId()));
	            event.setCaseId(caseId); 
	            event.setDeclarant(targetName);
		        event.setTargetCertiCode(targetCertiCode);
		        event.setPrintDate(processDate);				
		        event.setCrsId(crsPartyVo.getCrsId());
		        event.setPartyId(data.getTargetId());		        		        
		        eventService.publishEvent(event);
		        
		    	//若信函產生，根據document_Id串出clob_id回存
		        if (event.getDocumentId()!=null) {
		        	Long clobId = this.findClobIdByDocumentId(event.getDocumentId()) ;
		        	if (clobId.compareTo(noData) > 0){
		        		entity.setReportId(clobId); 	
		        	}		             
		          }	    	  	    	 
		      }
	      else if (CRSCodeCst.SYS_CODE__POS.equals(crsPartyVo.getSysCode())){	    	
	    	  FmtPos0500Event event = new FmtPos0500Event(ApplicationEventVO.newInstance(data.getPolicyId()));	            
	           event.setDeclarant(targetName);
	           event.setTargetCertiCode(targetCertiCode);
	           event.setPrintDate(processDate);				
	           event.setCrsId(crsPartyVo.getCrsId());
	           event.setPartyId(data.getTargetId());
	           eventService.publishEvent(event);
	           
	    	   //若信函產生，根據document_Id串出clob_id回存
	           if (event.getDocumentId()!=null) {
		        	Long clobId = this.findClobIdByDocumentId(event.getDocumentId()) ;
		        	if (clobId.compareTo(noData) > 0){
		        		entity.setReportId(clobId); 	
		        	}		             
		          }	 
	      }else {
	    	  syso(LogLevel.INFO,listId,"未產生CRS權益通知函.. SurveyType from:" + record.getTargetSurveyType() + " CRS Party sys_code:"+crsPartyVo.getSysCode()) ;		    	
	      }
	      
	      if (chk==0) {
	    	  //新增跟催紀錄 insert - t_fatca_due_diligence_list
		      dao.save(entity);
		      //BatchLogUtils.addLog(LogLevel.INFO, null, String.format("[crsFollowUpListBatchJob][CRS產出跟催名單數據][成功] list_id = %d, policy_id = %d, target_survey_type= %s ",entity.getListID() , entity.getPolicyId(), entity.getTargetSurveyType()));  
	      }
	      
	}
	
	//要保人融通件
	@Override
	public void sendCrsLetterPH(FatcaDueDiligenceList data, Date processDate) throws GenericException {		  
		/* 填寫資料至跟催名單 */
	  	  Long noData = 0L;
		  Long listId = new Long(data.getListID()); //source:t_proposal_rule_result.list_id
	      FatcaDueDiligenceListVO vo = new FatcaDueDiligenceListVO();
	      data.copyToVO(vo, Boolean.FALSE);
	      updateServiceAgent(vo);//取得最新服務業務員資訊      
	      vo.setListID(null);
	      vo.setSurveySystemType(CRSCodeCst.SURVEY_SYSTEM_TYPE_CRS ); 
	      vo.setTargetSurveyDate(processDate);	      
	      FatcaDueDiligenceList entity = new FatcaDueDiligenceList();  //新產生之跟催資料
	      entity.copyFromVO(vo, Boolean.TRUE, Boolean.FALSE);
	      
	      String targetCertiCode = data.getTargetCertiCode() ;
	      String targetName = data.getTargetName() ;
	      String policyCode = data.getPolicyCode() ;
	      Long  policyId = data.getPolicyId() ;
	      //要保人融通件，因無 CRS Party建檔資料
	      Long tmpCrsID = -1L;  	    
	      //PCR375449 CRS2 luisa start	      	      
	      CRSPartyLogVO logVO = this.getCRSPartyLogVO(vo, processDate); 
	      Long userId = AppContext.getCurrentUser().getUserId(); 
    	  // 建立CRSPartyLog
		  crsPartyLogService.saveOrUpdate(userId, processDate, logVO, logVO.getChangeId(), logVO.getInputSource());
		  CRSPartyLogVO qrylogVO = crsPartyLogService.
					findByChangeIdAndCertiCode(logVO.getChangeId(), targetCertiCode);
		  //要保人融通件-批次量不多，故每筆寫log
		  syso(LogLevel.INFO,listId,setMsg("[要保人融通件批次產生CRSPartyLog][CRS產生權益通知函數據] PartyLogId= %s, policy_code = %s, certi_Code= %s ", qrylogVO.getLogId(), data.getPolicyCode(), targetCertiCode)) ;
		  
		  // 提交CRS主檔
		  CRSPartyReportVO crsPartyReportVO = crsPartyCI.submitByLogId(qrylogVO.getLogId());
			if (crsPartyReportVO != null) {
				tmpCrsID = crsPartyReportVO.getParty().getCrsId();
			} else {	
				syso(LogLevel.WARN ,listId,setMsg("[要保人融通件批次產生CRSParty][CRS產生權益通知函數據] 提交CRS主檔有異常: PartyLogId= %s, policy_code = %s, certi_Code= %s ", qrylogVO.getLogId(), data.getPolicyCode(), targetCertiCode)) ;			
			}	   
 	      //PCR375449 CRS2 luisa end
	      //判斷同一人是否已進行過跟催。若fatca已先產生跟催 ，將類別由FATCA改為CRS/FATCA即可。
	      //IR353344 int chk = isfollowUpListExist(targetCertiCode,policyId,targetName,data.getTargetSurveyType(),processDate);
	      int chk = isfollowUpListExist(data.getTargetId(),policyId,data.getTargetSurveyType(),processDate);
	      if (chk == 1 ) {
	    	  syso(LogLevel.INFO,listId,setMsg("[TargetSurveyType done by FATCA][CRS產生權益通知函數據] TargetSurveyType= %s, policy_code = %s, certi_Code= %s ", data.getTargetSurveyType(), data.getPolicyCode(), targetCertiCode)) ;
	      }else if (chk == 2 ){
	    	  syso(LogLevel.INFO,listId,setMsg("[TargetSurveyType already exist][CRS產生權益通知函數據] TargetSurveyType= %s, policy_code = %s, certi_Code= %s ", data.getTargetSurveyType(), data.getPolicyCode(), targetCertiCode)) ;	    	  
	    	  return ;
	      }
	    	  
	      //檢查是否已發過權益通知函
	      /*CrsRightNotify qryList = rightNotifyDao.quryByCrsId(crsPartyVo.getCrsId());		  
	   	 
	      if  (qryList !=null && qryList.getListId() !=null ) {
	    		syso(LogLevel.INFO,listId,"Right Notice already sent. CRS_ID=" + crsPartyVo.getCrsId()) ;
		    	  return ;
	    	} */
	      
	      //判斷是個人或是公司，再決定由 T_CUSTOMER OR T_COMPANY_CUSTOMER 取address 	
	      AddressVO addressVO = null;
	      if(CodeCst.PARTY_TYPE__INDIV_CUSTOMER.equals(data.getTargetPartyType())){
	          CustomerVO customerVO = custServ.getIndivCustomer(data.getTargetId());
	          Long addressId = customerVO.getAddressId();
	          if(addressId != null) {
	           	 addressVO = addrServ.find(addressId);
	            }
	          
	       }else{
	          CompanyCustomerVO companyVO = custServ.getOrganCustomer(data.getTargetId());
	          Long addressId = companyVO.getAddressId();
	          if(addressId != null) {
	            	addressVO = addrServ.find(addressId);
	             }
	       }
	      	    	
	      FmtPos0500Event event = new FmtPos0500Event(ApplicationEventVO.newInstance(data.getPolicyId()));	            
	       event.setDeclarant(targetName);
	       event.setTargetCertiCode(targetCertiCode);
	       event.setPrintDate(processDate);				
	       event.setCrsId(tmpCrsID); //crs_id =-1
	       event.setPartyId(data.getTargetId());
	       eventService.publishEvent(event);
	       
		   //若信函產生，根據document_Id串出clob_id回存
	       if (event.getDocumentId()!=null) {
	        	Long clobId = this.findClobIdByDocumentId(event.getDocumentId()) ;
	        	if (clobId.compareTo(noData) > 0){
	        		entity.setReportId(clobId); 	
	        	}		             
	          }	 
	  
	      if (chk==0) {
	    	  //新增跟催紀錄 insert - t_fatca_due_diligence_list
		      dao.save(entity);
		     // BatchLogUtils.addLog(LogLevel.INFO, null, String.format("[crsFollowUpListBatchJob][CRS產出跟催名單數據][成功] list_id = %d, policy_id = %d, target_survey_type= %s ",entity.getListID() , entity.getPolicyId(), entity.getTargetSurveyType()));
	      }
	      
	}

	@Override
	public FatcaDueDiligenceList parseMap(Map<String, Object> map, String surveyType) throws GenericException {
    	FatcaDueDiligenceList data = new FatcaDueDiligenceList();
        data.setPolicyId(MapUtils.getLong(map, "POLICY_ID"));
        data.setPolicyCode(MapUtils.getString(map, "POLICY_CODE"));
        data.setPolicyStatus(MapUtils.getLong(map, "POLICY_STATUS"));
        data.setTargetId(MapUtils.getLong(map, "TARGET_ID"));
        data.setTargetName(MapUtils.getString(map, "TARGET_NAME"));
        data.setTargetCertiCode(MapUtils.getString(map, "TARGET_CERTI_CODE"));
        data.setTargetPartyType(MapUtils.getString(map,  "TARGET_PARTY_TYPE"));
        data.setTargetSurveyType(surveyType);
        data.setServiceAgent(MapUtils.getLong(map, "SERVICE_AGENT"));
        data.setAgentRegisterCode(MapUtils.getString(map, "AGENT_REGISTER_CODE"));
        data.setAgentName(MapUtils.getString(map, "AGENT_NAME"));
        BigDecimal agentBizCate = (BigDecimal) map.get("AGENT_BIZ_CATE");
        Long bizCate = null;
        if(agentBizCate != null) {
        	bizCate = agentBizCate.longValue();
        }
        data.setAgentBizCate(bizCate);
        BigDecimal channelOrg = (BigDecimal)  map.get("CHANNEL_ORG");
        Long channelId =null;
        if(channelOrg != null) {
        	channelId = channelOrg.longValue();
        }
        data.setChannelOrg(channelId);
        data.setChannelCode(MapUtils.getString(map, "CHANNEL_CODE"));
        data.setChannelName(MapUtils.getString(map, "CHANNEL_NAME"));
        BigDecimal channelType = (BigDecimal)map.get("CHANNEL_TYPE");
        Long channelTypeId = null;
        if(channelType != null)
        	channelTypeId = channelType.longValue();
        data.setChannelType(channelTypeId);
        if(map.containsKey("LIST_ID")){
            data.setListID(MapUtils.getLong(map, "LIST_ID"));
        }
        data.setSixIndi(MapUtils.getString(map, "SIX_INDI"));
        data.setAmlIndi(MapUtils.getString(map, "AML_INDI"));
        
		return data;
	}
	
	@Override
	public FatcaDueDiligenceList parseMapPH(Map<String, Object> map, String surveyType) throws GenericException {
    	FatcaDueDiligenceList data = new FatcaDueDiligenceList();
        data.setPolicyId(MapUtils.getLong(map, "POLICY_ID"));
        data.setPolicyCode(MapUtils.getString(map, "POLICY_CODE"));
        data.setPolicyStatus(MapUtils.getLong(map, "POLICY_STATUS"));
        data.setTargetId(MapUtils.getLong(map, "TARGET_ID"));
        data.setTargetName(MapUtils.getString(map, "TARGET_NAME"));
        data.setTargetCertiCode(MapUtils.getString(map, "TARGET_CERTI_CODE"));
        data.setTargetPartyType(MapUtils.getString(map,  "TARGET_PARTY_TYPE"));
        data.setTargetSurveyType(surveyType);
        
        if(map.containsKey("LIST_ID")){
            data.setListID(MapUtils.getLong(map, "LIST_ID"));
        }
		return data;
	}

	@Override
	protected FatcaDueDiligenceListVO newEntityVO() {
		return new FatcaDueDiligenceListVO();
	}
			
	private int isfollowUpListExist(Long partyId, Long policyId,
			String surveyType , Date processDate){
		int result = 0 ;
		
		//判斷是否同一人，fatca已產生跟催 ，將類別改為CRS/FATCA		IR 353344 modified
		List<FatcaDueDiligenceList> tmplist = dao.queryExistsFollowUpList(partyId, policyId, surveyType, processDate) ;
		if(tmplist!=null && tmplist.size() > 0 ){		
			//同一事件(surveyType)有進行過跟催，若無指定日期，取最近日期(targetSurveyDate)一筆資料查看
			FatcaDueDiligenceList record = tmplist.get(0);			
			
			if (CRSCodeCst.SURVEY_SYSTEM_TYPE_FATCA.equals(record.getSurveySystemType())) {								
				record.setSurveySystemType(CRSCodeCst.SURVEY_SYSTEM_TYPE_ALL);
                dao.saveorUpdate(record);
                result = 1 ;	                
			}else 
				result = 2 ;			 
		}
		//result- 0:新增跟催數據與發送權益通知   1:只發送權益通知不新增跟催數據   2:資料已存在，無須再次處理   
		return result ;
		
	}
	
	private Long findClobIdByDocumentId(Long documentId) throws GenericException {		
		DocumentData docData = documentDataDao.findByDocumentId(documentId).get(0);
		Long clobId = docData.getClobId();
		return clobId ;
	}
	
	//PCR375449-CRS 
	  private CRSPartyLogVO getCRSPartyLogVO(FatcaDueDiligenceListVO fatcaVO, Date processDate) {    	
	    	CRSPartyLogVO crsPartyLogVO = new CRSPartyLogVO();
			// CHANGE_ID
			Change tChange = new Change();
				tChange.setChangeSource(CodeCst.CHANGE_SOURCE__PARTY);
				tChange.setChangeStatus(CodeCst.CHANGE_STATUS__WAITING_ISSUE);
				// TODO: 暫時先用台北總公司
				tChange.setOrgId(101L);
			Long insertChangeId = TChangeDelegate.create(tChange);		
			crsPartyLogVO.setChangeId(insertChangeId);
			
			// LAST_CMT_FLG (預設值給N，不處理)
			// CERTI_CODE (調查對象ID)
			crsPartyLogVO.setCertiCode(fatcaVO.getTargetCertiCode());		
			/*
			if(StringUtils.isBlank(fatcaVO.getTargetCertiCode())) {
	            if(CodeCst.PARTY_TYPE__INDIV_CUSTOMER.equals(fatcaVO.getTargetPartyType())) {
	                CustomerVO customerVO = customerService.getIndivCustomer(fatcaVO.getTargetId());
	                if(customerVO != null) {
	                	crsPartyLogVO.setCertiCode(StringUtils.trim(customerVO.getCertiCode()));
	                } 
	            } else {
	                CompanyCustomerVO companyVO = customerService.getOrganCustomer(fatcaVO.getTargetId());
	                if(companyVO != null) {
	                	crsPartyLogVO.setCertiCode(StringUtils.trim(companyVO.getRegisterCode()));
	                }
	            }
	        } */

			// CERTI_TYPE
			crsPartyLogVO.setCertiType(fatcaVO.getTargetPartyType().equals(CodeCst.PARTY_TYPE__INDIV_CUSTOMER)? 
					Integer.valueOf(CertiCodeCI.CERTI_TYPE_TW_PID) : Integer.valueOf(CertiCodeCI.CERTI_TYPE_COMPANY_NO));
			
			// CRS_CODE 
			String partyType =fatcaVO.getTargetPartyType();
			crsPartyLogVO.setCrsCode(CodeCst.PARTY_TYPE__INDIV_CUSTOMER.equals(partyType)? 
					CRSCodeCst.CRS_IDENTITY_TYPE__MAN : CRSCodeCst.CRS_IDENTITY_TYPE__COMPANY);
			// NAME (調查對象姓名)
			crsPartyLogVO.setName(fatcaVO.getTargetName());
			// POLICY_CODE (調查對象的保單號碼)
			crsPartyLogVO.setPolicyCode(fatcaVO.getPolicyCode());
			// POLICY_TYPE (保單類型(個險:0,團險:1))
			crsPartyLogVO.setPolicyType(CRSCodeCst.POLICY_TYPE__NORMAL);
			// SYS_CODE (固定給保全)
			crsPartyLogVO.setSysCode(CRSCodeCst.SYS_CODE__POS);
			// CRS_TYPE (自動建檔時CRS身份類別不給值)
			// COUNTRY
			// DECLARATION_DATE
			// LEGAL_CERTI_CODE
			// LEGAL_NAME
			// AGENT_ANNOUNCE
			// DOC_SIGN_STATUS (01 已通知未簽署)
			crsPartyLogVO.setDocSignStatus(CRSCodeCst.CRS_DOC_SIGN_STATUS__NOTIFY);
			// DOC_SIGN_DATE (調查名單產生日期 = 批次業務日)
			crsPartyLogVO.setDocSignDate(processDate);
			// BUILD_TYPE 預設建檔中 
			// PCR257607  CRS身分聲明狀態 = [CRS000] 建檔狀態 = [已註記] 
			 crsPartyLogVO.setBuildType(CRSCodeCst.BUILD_TYPE__STAMPED);
			 crsPartyLogVO.setCrsType(CRSCodeType.CODE_TYPE_UNDEFINED__E); 
			// INPUT_SOURCE (BAT)
			crsPartyLogVO.setInputSource(CRSCodeCst.INPUT_SOURCE__BAT);		
			
			return crsPartyLogVO;
		}

}
