package com.ebao.ls.uw.ctrl.elderAudio;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.ebao.foundation.module.para.Para;
import com.ebao.ls.pub.CodeCstTgl;
import com.ebao.ls.uw.ctrl.letter.UNBLetterForm;

public class ElderAudioForm extends UNBLetterForm {

	private static final long serialVersionUID = 1829204133216875466L;

	// 取參數: 高齡客戶年齡定義
	private Integer elderAgeDef = Integer.valueOf(Para.getParaValue(CodeCstTgl.UNB_ELDER_AGE_DEF));

	private String selectedCertiCode;
	private Long underwriteId;
	private ElderAudioObject otherObj = new ElderAudioObject();
	private String audioNo;
	private String policyImageNames;
	private String[] selectedTaskList;

	public Integer getElderAgeDef() {
		return elderAgeDef;
	}

	public String getSelectedCertiCode() {
		return selectedCertiCode;
	}

	public void setSelectedCertiCode(String selectedCertiCode) {
		this.selectedCertiCode = selectedCertiCode;
	}

	public String getAudioNo() {
		return audioNo;
	}

	public void setAudioNo(String audioNo) {
		this.audioNo = audioNo;
	}

	public ElderAudioObject getOtherObj() {
		return otherObj;
	}

	public void setOtherObj(ElderAudioObject otherObj) {
		this.otherObj = otherObj;
	}

	public String getPolicyImageNames() {
		return policyImageNames;
	}

	public void setPolicyImageNames(String policyImageNames) {
		this.policyImageNames = policyImageNames;
	}

	public String[] getSelectedTaskList() {
		return selectedTaskList;
	}

	public void setSelectedTaskList(String[] selectedTaskList) {
		this.selectedTaskList = selectedTaskList;
	}

	/**
	 * @return the underwriteId
	 */
	public Long getUnderwriteId() {
		return underwriteId;
	}

	/**
	 * @param underwriteId the underwriteId to set
	 */
	public void setUnderwriteId(Long underwriteId) {
		this.underwriteId = underwriteId;
	}


	public String toString() {
		String properties = ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
		return properties;
	}

}
