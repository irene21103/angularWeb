package com.ebao.ls.crs.nb;

import java.util.List;

import com.ebao.foundation.commons.exceptions.GenericException;
import com.ebao.ls.crs.vo.NBcrsAttachmentVO;
import com.ebao.ls.crs.vo.NBcrsVO;
import com.ebao.ls.fatca.vo.FatcaIdentityAttachmentVO;
import com.ebao.ls.foundation.service.GenericService;

public interface NBcrsAttachmentService extends GenericService<NBcrsAttachmentVO> {
	public final static String BEAN_DEFAULT = "nBcrsAttachmentService";
	
	/**
	 * <p>Description : changeId區入核保作業查詢及覆核註記查詢</p>
	 * <p>Created By : Simon.Huang</p>
	 * <p>Create Time : 2018年12月18日</p>
	 * @param changeId
	 * @return
	 */
	public List<NBcrsAttachmentVO> findByChangeId(Long changeId) ;
	
	public abstract void saveOrUpdate(
					Long nbCrsId,
					Long changeId, java.util.List<FatcaIdentityAttachmentVO> attachmentVOList) throws GenericException;
}
