package com.ebao.ls.uw.ds;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.ebao.ls.foundation.service.GenericServiceImpl;
import com.ebao.ls.uw.data.UwSurveyLetterDelegate;
import com.ebao.ls.uw.data.bo.UwSurveyLetter;
import com.ebao.ls.uw.vo.UwSurveyLetterVO;
import com.ebao.pub.framework.GenericException;

/**
 * <p>Title: 核保頁面_生調交查選項列表</p>
 * <p>Description: 核保頁面_生調交查選項列表</p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company: TGL Co., Ltd.</p>
 * <p>Create Time: Apr 15, 2016</p> 
 * @author 
 * <p>Update Time: Apr 15, 2016</p>
 * <p>Updater: Alex Cheng</p>
 * <p>Update Comments: </p>
 */
public class UwSurveyLetterServiceImpl extends GenericServiceImpl<UwSurveyLetterVO, UwSurveyLetter, UwSurveyLetterDelegate> implements UwSurveyLetterService {

	@Resource(name = UwSurveyLetterDelegate.BEAN_DEFAULT)
    private UwSurveyLetterDelegate uwUwSurveyLetterDao;

	@Override
	  protected UwSurveyLetterVO newEntityVO() {
	    return new UwSurveyLetterVO();
	  }

	@Override
	public UwSurveyLetterVO findTempSaveUwSurveyLetter(Long underwriteId, Long policyId) throws GenericException {
		UwSurveyLetterVO vo = null;
		if(underwriteId != null && policyId != null){
			UwSurveyLetter bo = uwUwSurveyLetterDao.findTempSaveUwSurveyLetter(underwriteId, policyId);
			if(bo != null){
				vo = convertToVO(bo,true);
			}
		}
		return vo;
	}

    /**
     * <p>Description : 以policyId取得生調主檔記錄</p>
     * <p>Created By : Dania Lin</p>
     * <p>Create Time : Aug 3, 2021</p>
     * @param policyId
     * @return
     */  
	@Override
    public Long findSurveyCountByPolicyId(Long policyId) throws GenericException{
		return uwUwSurveyLetterDao.findSurveyCountByPolicyId(policyId);
	}

    /**
     * <p>Description : 交查員email設定資料</p>
     * <p>Created By : Dania Lin</p>
     * <p>Create Time : Nov 22, 2021</p>
     * @param surveyLetId, mail, name
     * @throws GenericException
     */
	public List<Map<String,String>> findSurveyAgentEmailBySurveyType(String surveyType) throws GenericException {
		return uwUwSurveyLetterDao.findSurveyAgentEmailBySurveyType(surveyType);
	}

}
