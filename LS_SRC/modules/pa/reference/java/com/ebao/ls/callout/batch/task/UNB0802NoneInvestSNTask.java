package com.ebao.ls.callout.batch.task;

import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.ApplicationContextAware;

import com.ebao.foundation.common.lang.exception.ExceptionUtils;
import com.ebao.foundation.core.logging.Log;
import com.ebao.ls.callout.batch.AbstractCalloutTask;
import com.ebao.ls.callout.batch.CalloutConstants;
import com.ebao.ls.callout.batch.CalloutDataExportService;
import com.ebao.ls.callout.bs.CalloutBatchService;
import com.ebao.ls.callout.data.CalloutConfigDao;
import com.ebao.pub.batch.help.BatchHelp;
import com.ebao.pub.batch.type.JobStatus;
import com.ebao.pub.batch.type.LogLevel;
import com.ebao.pub.batch.util.BatchLogUtils;
import com.ebao.pub.util.DateUtils;
import com.ebao.pub.util.TransUtils;

public class UNB0802NoneInvestSNTask extends AbstractCalloutTask implements ApplicationContextAware {
    
    public static final String BEAN_DEFAULT = "zeroEightZeroTwoTask";

    private final Long DISPLAY_ORDER = 2L;
    
    public final static Long JOB_ID = 1332323L;
    
    private String CONFIG_TYPE = "1";// 080抽樣比例

    private static final Log log = Log.getLogger(UNB0802NoneInvestSNTask.class);

    @Resource(name=CalloutDataExportService.BEAN_DEFAULT)
    private CalloutDataExportService exportServ;
    
    @Resource(name=CalloutBatchService.BEAN_DEFAULT)
    private CalloutBatchService batchServ;
    
    @Resource(name=CalloutConfigDao.BEAN_DEFAULT)
    private CalloutConfigDao configDao;
    
    
    @Override
    public int mainProcess() throws Exception {
        List<Map<String, Object>> mapList = null;
        UserTransaction ut = TransUtils.getUserTransaction();
        try{
            ut.begin();
            /* 保單抽樣 */
            mapList = unbSampling(
                        "[BR-CMN-TFI-005][檔案080-2 未連結結構債件之保單(非SN)]",
                        CalloutConstants.ZeroEightZeroTwo,CONFIG_TYPE, DISPLAY_ORDER, "080-2", "080");
            
            /* 匯出抽樣保單 */
            exportServ.feedMap(mapList, CalloutConstants.ZeroEightZeroTwo, BatchHelp.getProcessDate());
            ut.commit();
            if(CollectionUtils.isEmpty(mapList)){
                return JobStatus.EXECUTE_SUCCESS;
            }
        } catch (Exception ex) {
            try {
                ut.rollback();
            } catch (SystemException sysEx) {
                BatchLogUtils.addLog(LogLevel.ERROR, null,ExceptionUtils.getFullStackTrace(sysEx));
            }
            BatchLogUtils.addLog(LogLevel.ERROR, null,ExceptionUtils.getFullStackTrace(ex));
            return JobStatus.EXECUTE_FAILED;
        }
        return JobStatus.EXECUTE_SUCCESS;
    }

    public java.util.List<Map<String, Object>> getData(Date inputDate, String policyCode){
        java.util.List<java.util.Map<String,Object>> mapList  = new java.util.LinkedList<Map<String,Object>>();
        final java.sql.Date processDate = new java.sql.Date(inputDate.getTime());
        final java.sql.Date nextDate = new java.sql.Date(DateUtils.addDay(inputDate, 1).getTime());
        java.util.Date monthDate = DateUtils.truncateMonth(DateUtils.addMonth(inputDate,-1));
        final java.sql.Date monthSqlDate = new java.sql.Date(monthDate.getTime());
        String paraVal = com.ebao.pub.para.Para.getParaValue(3060010020L);
        java.util.Date paraValDate;
        try {
            paraValDate = org.apache.commons.lang3.time.DateUtils.parseDate(paraVal, "yyyyMMdd");
        } catch (ParseException e) {
            java.util.Calendar calendar = java.util.Calendar.getInstance();
            calendar.set(2018, 3, 1);
            paraValDate= calendar.getTime();
        }
        final java.sql.Date initDate = DateUtils.toSqlDate(paraValDate);
        
        StringBuffer sql = new StringBuffer();
        sql.append(" select distinct mail.policy_id, ");
        sql.append("        mail.policy_code, ");
        sql.append("        prod.internal_id, ");
        sql.append("        cmProd.product_version_id, ");
        sql.append("        ph.name ph_name, ph.certi_code ph_certi_code, ");
        sql.append("       	ph.roman_name ph_roman_name, ");
        sql.append("        ph.gender ph_gender, ");
        sql.append("        ph.BIRTH_DATE ph_birthday, ");
        sql.append("        decode(ph.office_tel_reg, null, null, '(' || ph.office_tel_reg || ')') || ph.office_tel || decode(ph.office_tel_ext, null,null,'#'||ph.office_tel_ext) ph_office_tel, ");
        sql.append("        decode(ph.home_tel_reg, null, null, '(' || ph.home_tel_reg || ')') || ph.home_tel || decode(ph.home_tel_ext, null, null , '#' || ph.home_tel_ext) ph_home_tel, ");
        sql.append("        ph.MOBILE_TEL ph_phone1, ");
        sql.append("        ph.MOBILE_TEL2 ph_phone2, ");
        sql.append("       	(SELECT a.address_1 from t_address a WHERE a.address_id = ph.address_id) ph_home_addr,  ");
        sql.append("       	lr.name lr_name,   ");
        sql.append("       	lr.roman_name lr_roman_name,   ");
        sql.append("       	lr.certi_code lr_certi_code,   ");
        sql.append("       	lr.gender lr_gender,   ");
        sql.append("       	lr.BIRTH_DATE lr_birthday,   ");
        sql.append("       	lr.rel_to_ph_ins rel_to_ph,   ");  
        sql.append("       	lr2.rel_to_ph_ins rel_to_ins,   ");
        sql.append("       	decode(lrc.office_tel_reg, null, null, '(' || lrc.office_tel_reg || ')') || lrc.office_tel || decode(lrc.office_tel_ext, null, null, '#' || lrc.office_tel_ext) lr_office_tel,   ");
        sql.append("       	decode(lrc.home_tel_reg, null, null, '(' || lrc.home_tel_reg || ')')  || decode(lrc.home_tel_ext, null, null, '#' ||  lrc.home_tel_ext) lr_home_tel,  ");
        sql.append("       	lrc.MOBILE lr_phone1,  ");
        sql.append("       	lrc.mobile2 lr_phone2,  ");
        sql.append("        cmProd.name i_name, ");
        sql.append("        cmProd.certi_code i_certi_code, ");
        sql.append("        cmProd.entry_age i_age, ");
        sql.append("        cmProd.APPLY_DATE apply_date,  ");
        sql.append("        cmProd.VALIDATE_DATE validate_date,  ");
        sql.append("        cmProd.ISSUE_DATE issue_date,  ");
        sql.append("        cmProd.charge_period charge_period,  ");
        sql.append("        cmProd.charge_year charge_year,  ");
        sql.append("        cmProd.submission_date submission_date,  ");
        sql.append("        mail.dispatch_date dispatch_date,  ");
        sql.append("        mail.acknowledge_date reply_date,  ");
        sql.append("       	mail.receive_date receive_date,  ");
        sql.append("        mail.tracking_number tracking_number,  ");//PCR-415241 保單郵寄掛號/國際快捷/宅配號碼
        sql.append("        send_type, ");
        sql.append("        PKG_PUB_CMN_UTILS.f_is_purpose(mail.policy_id, '1') ensure_require,   ");
        sql.append("        PKG_PUB_CMN_UTILS.f_is_purpose(mail.policy_id, '2') retire_require,    ");
        sql.append("        PKG_PUB_CMN_UTILS.f_is_purpose(mail.policy_id, '3') child_fee,  ");
        sql.append("        PKG_PUB_CMN_UTILS.f_is_purpose(mail.policy_id, '4') asset,  ");
        sql.append("        PKG_PUB_CMN_UTILS.f_is_purpose(mail.policy_id, '5') house,  ");
        sql.append("       	PKG_PUB_CMN_UTILS.f_is_purpose(mail.policy_id, '50') emp_benefit ,   ");
        sql.append("       	PKG_PUB_CMN_UTILS.f_is_purpose(mail.policy_id, '6') other_purpose,   ");
        sql.append("       	pkg_pub_cmn_utils.f_get_ilp_charge_type(cmProd.PRODUCT_ID ) AS ilp_charge_type,  ");
        sql.append("        pkg_ls_pub_util.f_get_code_mutilang_desc('T_LIABILITY_STATUS', mail.liability_state, '311') state, ");
        sql.append("        pkg_ls_sc_callout.f_get_cm_prod_rate(prod.base_money, mail.issue_date) exchange_rate, ");
        sql.append("        pkg_ls_pub_util.f_get_code_mutilang_desc('T_MONEY', prod.BASE_MONEY, '311') money_name, ");
        sql.append("        pkg_ls_pub_util.f_get_code_mutilang_desc('V_NBU_CALLOUT_CHARGE_MODE', cmProd.RENEWAL_TYPE, '311')  charge_name,   ");//PCR-415241 繳別
        sql.append("        cmProd.CUSTOMIZED_PREM, ");
        sql.append("        cmProd.amount, ");
        sql.append("        pkg_ls_pub_utils.f_get_code_mutilang_desc('T_PROD_BIZ_CATEGORY', '40031','311') big_prodCate, ");
        sql.append("        prod.small_prodcate, ");
        sql.append("        pkg_ls_sc_callout.f_get_each_prem(mail.policy_id) each_prem ");
        sql.append(" from ( select * from v_callout_v_not_struct m where 1=1  ");
        if(StringUtils.isNotEmpty(policyCode)) {
        	sql.append(" and m.policy_code = :policy_code ");
        }
        sql.append("           and m.issue_date >= :init_date ");
        sql.append("           and m.dispatch_date >= :month_date ");
        sql.append("           and m.dispatch_date <= :process_date ");
        sql.append(" and m.channel_type != 6 ");
        sql.append(" and not exists ");
        sql.append("          (select 1 ");
        sql.append("                   from t_callout_batch cb ");
        sql.append("                   join t_callout_config cfg ");
        sql.append("                     on cfg.list_id = cb.config_type ");
        sql.append("                  where cb.policy_code = m.policy_code ");
        sql.append("                    and cfg.config_type = 1 and display_order = 2) ");
        sql.append(" ) mail ");
        sql.append(" join V_CALLOUT_CM_PROD cmProd ");
        sql.append(" 	on ( cmProd.policy_id = mail.policy_id and cmProd.item_id = mail.item_id and cmProd.party_id = mail.insured_id) ");
        sql.append(" join V_callout_PROD_INVEST prod ");
        sql.append("  	 on prod.product_id = cmProd.product_id ");
        sql.append(" join t_policy_holder_log ph ");
        sql.append(" 	on  ph.policy_id = mail.policy_id  and ph.LAST_CMT_FLG = 'Y' ");
        sql.append("  LEFT join t_legal_representative_log lr   ");
    	//與要保人關係
    	sql.append("  on lr.policy_id = cmprod.policy_id AND lr.rel_id = ph.list_id AND lr.rel_type = 1 AND lr.last_cmt_flg = 'Y'  ");
    	sql.append("  LEFT join t_legal_representative_log lr2   ");
    	//與被保人關係
    	sql.append("  on lr2.policy_id = cmprod.policy_id AND lr2.rel_id =  ph.list_id  AND lr2.rel_type = 2 AND lr2.last_cmt_flg = 'Y'  ");
        sql.append(" left JOIN t_customer lrc  ");
        sql.append("  	ON lr.party_id = lrc.customer_id ");
        sql.append(" where not exists ");
        sql.append(" 		(	select * from t_callout_config cfg ");
        sql.append(" 			where mail.channel_org_id = cfg.channel_org and cfg.config_type = 2) ");
        //PCR 373468 抽件時需考慮1.1的排除規則（同1.1-(4)(5)），符合排除規則的保單不抽出
        //sql.append(addSqlWhereBy0801Of4("mail.policy_id", "ph.certi_code"));
        //sql.append(addSqlWhereBy0801Of5("mail.policy_id", "ph.certi_code", "prod.internal_id", "process_date"));
        
        //PCR 468327 調整排除規則 
        sql.append(addSqlWhereBy0802Of6("mail.policy_id"));
        sql.append(addSqlWhereBy0802Of7("mail.policy_id", "ph.certi_code", "prod.internal_id", "process_date"));
        
        sql.append(" order by mail.policy_code");
        
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("process_date", processDate);
        params.put("init_date", initDate);
        params.put("month_date", monthSqlDate);
        
        BatchLogUtils.addLog(LogLevel.INFO, null,
				String.format("process_date := %s, month_date := %s,  next_date := %s, policy_code :=%s ",
						DateUtils.date2String(processDate, "yyyy/MM/dd"),
						DateUtils.date2String(monthDate, "yyyy/MM/dd"),
						DateUtils.date2String(nextDate, "yyyy/MM/dd"),
						policyCode));
        if(StringUtils.isNotEmpty(policyCode)) {
        	params.put("policy_code", policyCode);
        }
        mapList = nt.queryForList(sql.toString(), params);
        return combineDataByPolicyCode(mapList);
    }

    @Override
    public String saveToTrans(Map<String, Object> map, Date processDate,
                    String fileName, String dept) {
        String calloutNum = savePolicyHolder(map, processDate, fileName, dept);
        return calloutNum;
    }
    
}
