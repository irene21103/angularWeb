package com.ebao.ls.riskPrevention.ctrl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.UserTransaction;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.foundation.module.db.Trans;
import com.ebao.ls.pty.ds.DeptService;
import com.ebao.ls.pub.auth.AuthItemCst;
import com.ebao.ls.riskPrevention.bs.NationalityCodeLogService;
import com.ebao.ls.riskPrevention.bs.NationalityCodeService;
import com.ebao.ls.riskPrevention.bs.impl.NationalityCodeAuthServiceImpl;
import com.ebao.ls.riskPrevention.vo.NationalityCodeLogVO;
import com.ebao.ls.riskPrevention.vo.NationalityCodeVO;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.framework.GenericAction;
import com.ebao.pub.util.BeanUtils;
import com.ebao.pub.util.TransUtils;
import com.ebao.pub.web.pager.Pager;

public class NationalityCodeNewAction extends GenericAction {

    @Resource(name = NationalityCodeService.BEAN_DEFAULT)
    NationalityCodeService nationalityCodeService;

    @Resource(name = NationalityCodeLogService.BEAN_DEFAULT)
    NationalityCodeLogService nationalityCodeLogService;

    @Resource(name = DeptService.BEAN_DEFAULT)
    DeptService deptService;

    @Resource(name = NationalityCodeAuthServiceImpl.BEAN_DEFAULT)
    NationalityCodeAuthServiceImpl pageAuthService;

    @SuppressWarnings("unchecked")
    @Override
    public ActionForward process(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        boolean auth = pageAuthService.hasAccessRight(AuthItemCst.AUTH_ITEM_PA_NATIONALITY_CODE);
        request.setAttribute("auth", auth);
        if (!auth) { // 未經授權者 不可使用此Action相關功能
            return mapping.findForward("success");
        }
        List<Map<String, Object>> voStatus = new ArrayList<Map<String, Object>>();
        NationalityCodeNewForm cForm = (NationalityCodeNewForm) form;
        switch (cForm.getAction()) {
            case SAVE:
                int valueInt = Integer.parseInt(nationalityCodeService.getCurrentNatationlCode())+1;
                for (NationalityCodeVO vo : cForm.getVoList()) {
                    if (vo != null) {
                        boolean syncResult = syncToDB(vo, cForm.getPager(), String.valueOf(valueInt));
                        Map<String, Object> currentMap = BeanUtils.getProperties(vo);
                        currentMap.put("singleStatus",  syncResult? "Y" : "N");
                        if(syncResult){
                            valueInt = valueInt + 1;
                        }
                        voStatus.add(currentMap);
                    }
                }
                request.setAttribute("showEnable", true);
                request.setAttribute("saveStatus", voStatus);
                break;
            default:
                break;
        }
        request.setAttribute("currentUser", AppContext.getCurrentUser().getUserName());
        request.setAttribute("currentTime", AppContext.getCurrentUserLocalTime());
        cForm.setVo(new NationalityCodeVO());
        cForm.setVoList(new ArrayList<NationalityCodeVO>());
        return mapping.findForward("success");
    }

    private Boolean syncToDB(NationalityCodeVO vo, Pager pager, String nationalCode) throws Exception {
        if (vo.getNationalName() != null && vo.getNationalRisk() != null) {
            UserTransaction ut = Trans.getUserTransaction();

            if (nationalityCodeService.multiSearch(vo, false).size() == 0) {
                vo.setNationalCode(nationalCode);
                NationalityCodeLogVO insertLog = new NationalityCodeLogVO();
                BeanUtils.copyProperties(insertLog, vo);
                insertLog.setUpdatedDeptID(AppContext.getCurrentUser().getDeptId());
                insertLog.setUpdatedDeptName(deptService.getDeptById(insertLog.getUpdatedDeptID()).getDeptName());
                insertLog.setInsertBy(AppContext.getCurrentUser().getUserId());
                insertLog.setUpdateBy(insertLog.getInsertBy());
                insertLog.setInsertTime(AppContext.getCurrentUserLocalTime());
                insertLog.setUpdateTime(insertLog.getInsertTime());
                insertLog.setInsertTimestamp(insertLog.getInsertTime());
                insertLog.setUpdateTimestamp(insertLog.getInsertTime());
                try {
                    ut.begin();
                    nationalityCodeService.insert(vo, pager);
                    nationalityCodeLogService.newLog(insertLog);
                    ut.commit();
                } catch (Exception e) {
                    TransUtils.rollback(ut);
                    return false;
                }
            } else {
                return false;
            }
        }
        return true;
    }
}
