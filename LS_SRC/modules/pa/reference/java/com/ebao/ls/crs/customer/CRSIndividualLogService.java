package com.ebao.ls.crs.customer;

import java.util.List;

import com.ebao.foundation.commons.exceptions.GenericException;
import com.ebao.ls.crs.vo.CRSIndividualLogVO;
import com.ebao.ls.foundation.service.GenericService;

public interface CRSIndividualLogService extends GenericService<CRSIndividualLogVO>{
	public final static String BEAN_DEFAULT = "crsIndividualLogService";

	public List<CRSIndividualLogVO> findByPartyLogId(Long partyLogId, boolean loadOtherTable);


	/**
	 * PCR 257607 330030 e-Approval ITR-1801051_CRS共同申報及盡職審查專案- 保全變更及CRS確認申報國作業
	 * @param changeId
	 * @return
	 */
	public List<CRSIndividualLogVO> findByChangeId(Long changeId);
	
	//public abstract CRSIndividualLogVO syncSave(CRSIndividualLogVO logVO) throws GenericException;
}
