package com.ebao.ls.uw.cmu.event;

import com.ebao.foundation.commons.annotation.TriggerPoint;
import com.ebao.ls.cmu.event.CsUwEscalated;
import com.ebao.pub.event.AbstractIntegrationListener;
import com.ebao.pub.integration.IntegrationMessage;

public class CsUwEscalatedIntegrationListener
    extends
      AbstractIntegrationListener<CsUwEscalated> {
  final static String code = "uw.csUwEscalated";

  @Override
  @TriggerPoint(component = "Underwriting", description = "During customer service underwriting, if the underwriter escalates the case successfully to senior staff, the event is triggered.<br/>Menu navigation: Customer service > Customer service underwriting.<br/>", event = "direct:event."
      + code, id = "com.ebao.tp.ls." + code)
  protected String getEventCode(CsUwEscalated event,
      IntegrationMessage<Object> in) {
    return "event." + code;
  }
}
