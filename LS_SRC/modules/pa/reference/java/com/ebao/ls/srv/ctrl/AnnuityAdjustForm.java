package com.ebao.ls.srv.ctrl;

import java.util.ArrayList;
import java.util.List;

import com.ebao.ls.srv.data.bo.PayDue;
import com.ebao.pub.framework.GenericForm;

/**
 * <p>Title: AnnuityAdjustForm </p>		
 * <p>Description: This class Annuity adjust's form class </p>
 * <p>Copyright: Copyright (c) 2002	</p>
 * <p>Company: 	eBaoTech			</p>
 * <p>Create Time:		2005/04/28	</p> 
 * @author simen.li
 * @version 1.0
 */
public class AnnuityAdjustForm extends GenericForm {

  private String policyCode = "";
  private String itemId = "";

  private String actionType = "";

  private String policyHolder = "";
  private String policyStatus = "";
  private String validDate = "";
  private String paymentMethod = "";
  private String nextAnnuityDate = "";
  private String disbursementMethod = "";
  private String curDisbursementMethod = "";

  private String bankAccount = "";
  private String assignee = "";
  private String payeeName = "";
  private String payeeIdType = "";
  private String payeeIdNumber = "";

  private String manual = "N";
  private String dischargeVoucher = "";
  private String nextDischargeDate = "";
  private String lastDischargeDate = "";
  private String cancelLastDischarge = "";

  private String accountId = "";

  private List accountInfos = new ArrayList();

  private PayDue payDue = null;

  private String VaPayeeId = "";

  private String gmwbPayeeId = "";

  private String payeeId = "";

  private String checkedAccountId = "";

  private Long planId = Long.valueOf(0);

  private String gmwbPaymentMethod = "";
  private String nextGmwbDate = "";
  private String gmwbBankAccount = "";
  /**
   * Type of Deferred period for GMWB
   */
  private String gmwbPayPeriod;
  /**
   * Deferred Period for GMWB
   */
  private String gmwbPayYear;
  /**
   * Type of GMWB installment period
   */
  private String gmwbEndPeriod;
  /**
   * Installment period for GMWB
   */
  private String gmwbEndYear;
  private String gmwbPayType;
  private String gmwbPayMode;
  private String gmwbPayeeName = "";
  private String gmwbPayeeIdType = "";
  private String gmwbPayeeIdNumber = "";
  private String curGmwbPayMode = "";
  private String gmwbManual = "N";
  private String gmwbAccountId = "";
  private String gmwbCheckedAccountId = "";
  private List gmwbAccountInfos = new ArrayList();
  private Long gmwbPlanId = Long.valueOf(0);
  private String gmwbDischargeVoucher = "";
  private String gmwbNextDischargeDate = "";
  private String gmwbLastDischargeDate = "";
  private String gmwbCancelLastDischarge = "";
  private String isGmwb = "N";

  private String vaPaymentMethod = "";
  private String nextVaDate = "";
  private String vaBankAccount = "";
  private String vaPayPeriod;
  private String vaPayYear;
  private String vaEndPeriod;
  private String vaEndYear;

  private String vaPayType;
  private String vaPayEnsure;
  private String vaPayMode;
  private String curVaPayMode = "";
  private String vaManual = "N";
  private String vaAccountId = "";
  private String vaCheckedAccountId = "";
  private List vaAccountInfos = new ArrayList();
  private String vaPayeeName = "";
  private String vaPayeeIdType = "";
  private String vaPayeeIdNumber = "";
  private Long vaPlanId = Long.valueOf(0);
  private String vaDischargeVoucher = "";
  private String vaNextDischargeDate = "";
  private String vaLastDischargeDate = "";
  private String vaCancelLastDischarge = "";
  private String isVa = "N";

  private String isVaStart = "N";
  private String isGmwbStart = "N";

  /**
   * @return Returns the planId.
   */
  public Long getPlanId() {
    return planId;
  }
  /**
   * @param planId The planId to set.
   */
  public void setPlanId(Long planId) {
    this.planId = planId;
  }

  /**
   * @return Returns the checkedAccountId.
   */
  public String getCheckedAccountId() {
    return checkedAccountId;
  }
  /**
   * @param checkedAccountId The checkedAccountId to set.
   */
  public void setCheckedAccountId(String checkedAccountId) {
    this.checkedAccountId = checkedAccountId;
  }
  /**
   * @return Returns the payeeId.
   */
  public String getPayeeId() {
    return payeeId;
  }
  /**
   * @param payeeId The payeeId to set.
   */
  public void setPayeeId(String payeeId) {
    this.payeeId = payeeId;
  }
  /**
   * @return Returns the tPayDue.
   */
  public PayDue getPayDue() {
    return payDue;
  }
  /**
   * @param payDue The tPayDue to set.
   */
  public void setPayDue(PayDue payDue) {
    this.payDue = payDue;
  }

  /**
   * @return Returns the accountId.
   */
  public String getAccountId() {
    return accountId;
  }
  /**
   * @param accountId The accountId to set.
   */
  public void setAccountId(String accountId) {
    this.accountId = accountId;
  }

  /**
   * @return Returns the itemId.
   */
  public String getItemId() {
    return itemId;
  }
  /**
   * @param itemId The itemId to set.
   */
  public void setItemId(String itemId) {
    this.itemId = itemId;
  }

  /**
   * @return Returns the accountInfos.
   */
  public List getAccountInfos() {
    //
    /*
     if (this.accountInfos.size()==0){
     PayeeAccountInfoVO accountInfo=new PayeeAccountInfoVO();
     accountInfo.setAccountHolder("aaa");
     accountInfo.setAccountId("1");
     accountInfo.setBankName("acc1");
     accountInfo.setAccountNo("111");
     accountInfos.add(accountInfo);
     }
     */
    return accountInfos;
  }
  /**
   * @param accountInfos The accountInfos to set.
   */
  public void setAccountInfos(List accountInfos) {
    this.accountInfos = accountInfos;
  }
  /**
   * @return Returns the actionType.
   */
  public String getActionType() {
    return actionType;
  }
  /**
   * @param actionType The actionType to set.
   */
  public void setActionType(String actionType) {
    this.actionType = actionType;
  }
  /**
   * @return Returns the assignee.
   */
  public String getAssignee() {
    return assignee;
  }
  /**
   * @param assignee The assignee to set.
   */
  public void setAssignee(String assignee) {
    this.assignee = assignee;
  }
  /**
   * @return Returns the bankAccount.
   */
  public String getBankAccount() {
    return bankAccount;
  }
  /**
   * @param bankAccount The bankAccount to set.
   */
  public void setBankAccount(String bankAccount) {
    this.bankAccount = bankAccount;
  }
  /**
   * @return Returns the cancelLastDischarge.
   */
  public String getCancelLastDischarge() {
    return cancelLastDischarge;
  }
  /**
   * @param cancelLastDischarge The cancelLastDischarge to set.
   */
  public void setCancelLastDischarge(String cancelLastDischarge) {
    this.cancelLastDischarge = cancelLastDischarge;
  }
  /**
   * @return Returns the disBursementMethod.
   */
  public String getDisbursementMethod() {
    return disbursementMethod;
  }
  /**
   * @param disBursementMethod The disBursementMethod to set.
   */
  public void setDisbursementMethod(String disBursementMethod) {
    this.disbursementMethod = disBursementMethod;
  }
  /**
   * @return Returns the dischargeVoucher.
   */
  public String getDischargeVoucher() {
    return dischargeVoucher;
  }
  /**
   * @param dischargeVoucher The dischargeVoucher to set.
   */
  public void setDischargeVoucher(String dischargeVoucher) {
    this.dischargeVoucher = dischargeVoucher;
  }
  /**
   * @return Returns the lastDischargeDate.
   */
  public String getLastDischargeDate() {
    return lastDischargeDate;
  }
  /**
   * @param lastDischargeDate The lastDischargeDate to set.
   */
  public void setLastDischargeDate(String lastDischargeDate) {
    this.lastDischargeDate = lastDischargeDate;
  }
  /**
   * @return Returns the manual.
   */
  public String getManual() {
    return manual;
  }
  /**
   * @param manual The manual to set.
   */
  public void setManual(String manual) {
    this.manual = manual;
  }
  /**
   * @return Returns the nextAnnuityDate.
   */
  public String getNextAnnuityDate() {
    return nextAnnuityDate;
  }
  /**
   * @param nextAnnuityDate The nextAnnuityDate to set.
   */
  public void setNextAnnuityDate(String nextAnnuityDate) {
    this.nextAnnuityDate = nextAnnuityDate;
  }
  /**
   * @return Returns the nextDischargeDate.
   */
  public String getNextDischargeDate() {
    return nextDischargeDate;
  }
  /**
   * @param nextDischargeDate The nextDischargeDate to set.
   */
  public void setNextDischargeDate(String nextDischargeDate) {
    this.nextDischargeDate = nextDischargeDate;
  }
  /**
   * @return Returns the payeeIdNumber.
   */
  public String getPayeeIdNumber() {
    return payeeIdNumber;
  }
  /**
   * @param payeeIdNumber The payeeIdNumber to set.
   */
  public void setPayeeIdNumber(String payeeIdNumber) {
    this.payeeIdNumber = payeeIdNumber;
  }
  /**
   * @return Returns the payeeIdType.
   */
  public String getPayeeIdType() {
    return payeeIdType;
  }
  /**
   * @param payeeIdType The payeeIdType to set.
   */
  public void setPayeeIdType(String payeeIdType) {
    this.payeeIdType = payeeIdType;
  }
  /**
   * @return Returns the payeeName.
   */
  public String getPayeeName() {
    return payeeName;
  }
  /**
   * @param payeeName The payeeName to set.
   */
  public void setPayeeName(String payeeName) {
    this.payeeName = payeeName;
  }
  /**
   * @return Returns the paymentMethod.
   */
  public String getPaymentMethod() {
    return paymentMethod;
  }
  /**
   * @param paymentMethod The paymentMethod to set.
   */
  public void setPaymentMethod(String paymentMethod) {
    this.paymentMethod = paymentMethod;
  }
  /**
   * @return Returns the policyCode.
   */
  public String getPolicyCode() {
    return policyCode;
  }
  /**
   * @param policyCode The policyCode to set.
   */
  public void setPolicyCode(String policyCode) {
    this.policyCode = policyCode;
  }
  /**
   * @return Returns the policyHolder.
   */
  public String getPolicyHolder() {
    return policyHolder;
  }
  /**
   * @param policyHolder The policyHolder to set.
   */
  public void setPolicyHolder(String policyHolder) {
    this.policyHolder = policyHolder;
  }
  /**
   * @return Returns the policyStatus.
   */
  public String getPolicyStatus() {
    return policyStatus;
  }
  /**
   * @param policyStatus The policyStatus to set.
   */
  public void setPolicyStatus(String policyStatus) {
    this.policyStatus = policyStatus;
  }
  /**
   * @return Returns the validDate.
   */
  public String getValidDate() {
    return validDate;
  }
  /**
   * @param validDate The validDate to set.
   */
  public void setValidDate(String validDate) {
    this.validDate = validDate;
  }

  /**
   * @return Returns the curDisbursementMethod.
   */
  public String getCurDisbursementMethod() {
    return curDisbursementMethod;
  }

  /**
   * @param curDisbursementMethod The curDisbursementMethod to set.
   */
  public void setCurDisbursementMethod(String curDisbursementMethod) {
    this.curDisbursementMethod = curDisbursementMethod;
  }
  public String getGmwbEndPeriod() {
    return gmwbEndPeriod;
  }
  public void setGmwbEndPeriod(String gmwbEndPeriod) {
    this.gmwbEndPeriod = gmwbEndPeriod;
  }
  public String getGmwbEndYear() {
    return gmwbEndYear;
  }
  public void setGmwbEndYear(String gmwbEndYear) {
    this.gmwbEndYear = gmwbEndYear;
  }
  public String getGmwbPayMode() {
    return gmwbPayMode;
  }
  public void setGmwbPayMode(String gmwbPayMode) {
    this.gmwbPayMode = gmwbPayMode;
  }
  public String getGmwbPayPeriod() {
    return gmwbPayPeriod;
  }
  public void setGmwbPayPeriod(String gmwbPayPeriod) {
    this.gmwbPayPeriod = gmwbPayPeriod;
  }
  public String getGmwbPayType() {
    return gmwbPayType;
  }
  public void setGmwbPayType(String gmwbPayType) {
    this.gmwbPayType = gmwbPayType;
  }
  public String getGmwbPayYear() {
    return gmwbPayYear;
  }
  public void setGmwbPayYear(String gmwbPayYear) {
    this.gmwbPayYear = gmwbPayYear;
  }
  public String getIsGmwb() {
    return isGmwb;
  }
  public void setIsGmwb(String isGmwb) {
    this.isGmwb = isGmwb;
  }
  public String getIsVa() {
    return isVa;
  }
  public void setIsVa(String isVa) {
    this.isVa = isVa;
  }
  public String getVaEndPeriod() {
    return vaEndPeriod;
  }
  public void setVaEndPeriod(String vaEndPeriod) {
    this.vaEndPeriod = vaEndPeriod;
  }
  public String getVaEndYear() {
    return vaEndYear;
  }
  public void setVaEndYear(String vaEndYear) {
    this.vaEndYear = vaEndYear;
  }
  public String getVaPayMode() {
    return vaPayMode;
  }
  public void setVaPayMode(String vaPayMode) {
    this.vaPayMode = vaPayMode;
  }
  public String getVaPayPeriod() {
    return vaPayPeriod;
  }
  public void setVaPayPeriod(String vaPayPeriod) {
    this.vaPayPeriod = vaPayPeriod;
  }
  public String getVaPayType() {
    return vaPayType;
  }
  public void setVaPayType(String vaPayType) {
    this.vaPayType = vaPayType;
  }
  public String getVaPayYear() {
    return vaPayYear;
  }
  public void setVaPayYear(String vaPayYear) {
    this.vaPayYear = vaPayYear;
  }
  public String getGmwbPayeeIdNumber() {
    return gmwbPayeeIdNumber;
  }
  public void setGmwbPayeeIdNumber(String gmwbPayeeIdNumber) {
    this.gmwbPayeeIdNumber = gmwbPayeeIdNumber;
  }
  public String getGmwbPayeeIdType() {
    return gmwbPayeeIdType;
  }
  public void setGmwbPayeeIdType(String gmwbPayeeIdType) {
    this.gmwbPayeeIdType = gmwbPayeeIdType;
  }
  public String getGmwbPayeeName() {
    return gmwbPayeeName;
  }
  public void setGmwbPayeeName(String gmwbPayeeName) {
    this.gmwbPayeeName = gmwbPayeeName;
  }
  public String getVaPayeeIdNumber() {
    return vaPayeeIdNumber;
  }
  public void setVaPayeeIdNumber(String vaPayeeIdNumber) {
    this.vaPayeeIdNumber = vaPayeeIdNumber;
  }
  public String getVaPayeeIdType() {
    return vaPayeeIdType;
  }
  public void setVaPayeeIdType(String vaPayeeIdType) {
    this.vaPayeeIdType = vaPayeeIdType;
  }
  public String getVaPayeeName() {
    return vaPayeeName;
  }
  public void setVaPayeeName(String vaPayeeName) {
    this.vaPayeeName = vaPayeeName;
  }
  public String getCurGmwbPayMode() {
    return curGmwbPayMode;
  }
  public void setCurGmwbPayMode(String curGmwbPayMode) {
    this.curGmwbPayMode = curGmwbPayMode;
  }
  public String getGmwbAccountId() {
    return gmwbAccountId;
  }
  public void setGmwbAccountId(String gmwbAccountId) {
    this.gmwbAccountId = gmwbAccountId;
  }
  public List getGmwbAccountInfos() {
    return gmwbAccountInfos;
  }
  public void setGmwbAccountInfos(List gmwbAccountInfos) {
    this.gmwbAccountInfos = gmwbAccountInfos;
  }
  public String getGmwbCheckedAccountId() {
    return gmwbCheckedAccountId;
  }
  public void setGmwbCheckedAccountId(String gmwbCheckedAccountId) {
    this.gmwbCheckedAccountId = gmwbCheckedAccountId;
  }
  public String getVaPayEnsure() {
    return vaPayEnsure;
  }
  public void setVaPayEnsure(String vaPayEnsure) {
    this.vaPayEnsure = vaPayEnsure;
  }
  public String getCurVaPayMode() {
    return curVaPayMode;
  }
  public void setCurVaPayMode(String curVaPayMode) {
    this.curVaPayMode = curVaPayMode;
  }
  public String getGmwbManual() {
    return gmwbManual;
  }
  public void setGmwbManual(String gmwbManual) {
    this.gmwbManual = gmwbManual;
  }
  public String getVaAccountId() {
    return vaAccountId;
  }
  public void setVaAccountId(String vaAccountId) {
    this.vaAccountId = vaAccountId;
  }
  public List getVaAccountInfos() {
    return vaAccountInfos;
  }
  public void setVaAccountInfos(List vaAccountInfos) {
    this.vaAccountInfos = vaAccountInfos;
  }
  public String getVaCheckedAccountId() {
    return vaCheckedAccountId;
  }
  public void setVaCheckedAccountId(String vaCheckedAccountId) {
    this.vaCheckedAccountId = vaCheckedAccountId;
  }
  public String getVaManual() {
    return vaManual;
  }
  public void setVaManual(String vaManual) {
    this.vaManual = vaManual;
  }
  public Long getGmwbPlanId() {
    return gmwbPlanId;
  }
  public void setGmwbPlanId(Long gmwbPlanId) {
    this.gmwbPlanId = gmwbPlanId;
  }
  public Long getVaPlanId() {
    return vaPlanId;
  }
  public void setVaPlanId(Long vaPlanId) {
    this.vaPlanId = vaPlanId;
  }
  public String getGmwbPayeeId() {
    return gmwbPayeeId;
  }
  public void setGmwbPayeeId(String gmwbPayeeId) {
    this.gmwbPayeeId = gmwbPayeeId;
  }
  public String getVaPayeeId() {
    return VaPayeeId;
  }
  public void setVaPayeeId(String vaPayeeId) {
    VaPayeeId = vaPayeeId;
  }
  public String getGmwbBankAccount() {
    return gmwbBankAccount;
  }
  public void setGmwbBankAccount(String gmwbBankAccount) {
    this.gmwbBankAccount = gmwbBankAccount;
  }
  public String getGmwbPaymentMethod() {
    return gmwbPaymentMethod;
  }
  public void setGmwbPaymentMethod(String gmwbPaymentMethod) {
    this.gmwbPaymentMethod = gmwbPaymentMethod;
  }
  public String getNextGmwbDate() {
    return nextGmwbDate;
  }
  public void setNextGmwbDate(String nextGmwbDate) {
    this.nextGmwbDate = nextGmwbDate;
  }
  public String getNextVaDate() {
    return nextVaDate;
  }
  public void setNextVaDate(String nextVaDate) {
    this.nextVaDate = nextVaDate;
  }
  public String getVaBankAccount() {
    return vaBankAccount;
  }
  public void setVaBankAccount(String vaBankAccount) {
    this.vaBankAccount = vaBankAccount;
  }
  public String getVaPaymentMethod() {
    return vaPaymentMethod;
  }
  public void setVaPaymentMethod(String vaPaymentMethod) {
    this.vaPaymentMethod = vaPaymentMethod;
  }
  public String getGmwbCancelLastDischarge() {
    return gmwbCancelLastDischarge;
  }
  public void setGmwbCancelLastDischarge(String gmwbCancelLastDischarge) {
    this.gmwbCancelLastDischarge = gmwbCancelLastDischarge;
  }
  public String getGmwbDischargeVoucher() {
    return gmwbDischargeVoucher;
  }
  public void setGmwbDischargeVoucher(String gmwbDischargeVoucher) {
    this.gmwbDischargeVoucher = gmwbDischargeVoucher;
  }
  public String getGmwbLastDischargeDate() {
    return gmwbLastDischargeDate;
  }
  public void setGmwbLastDischargeDate(String gmwbLastDischargeDate) {
    this.gmwbLastDischargeDate = gmwbLastDischargeDate;
  }
  public String getGmwbNextDischargeDate() {
    return gmwbNextDischargeDate;
  }
  public void setGmwbNextDischargeDate(String gmwbNextDischargeDate) {
    this.gmwbNextDischargeDate = gmwbNextDischargeDate;
  }
  public String getVaCancelLastDischarge() {
    return vaCancelLastDischarge;
  }
  public void setVaCancelLastDischarge(String vaCancelLastDischarge) {
    this.vaCancelLastDischarge = vaCancelLastDischarge;
  }
  public String getVaDischargeVoucher() {
    return vaDischargeVoucher;
  }
  public void setVaDischargeVoucher(String vaDischargeVoucher) {
    this.vaDischargeVoucher = vaDischargeVoucher;
  }
  public String getVaLastDischargeDate() {
    return vaLastDischargeDate;
  }
  public void setVaLastDischargeDate(String vaLastDischargeDate) {
    this.vaLastDischargeDate = vaLastDischargeDate;
  }
  public String getVaNextDischargeDate() {
    return vaNextDischargeDate;
  }
  public void setVaNextDischargeDate(String vaNextDischargeDate) {
    this.vaNextDischargeDate = vaNextDischargeDate;
  }
  public String getIsGmwbStart() {
    return isGmwbStart;
  }
  public void setIsGmwbStart(String isGmwbStart) {
    this.isGmwbStart = isGmwbStart;
  }
  public String getIsVaStart() {
    return isVaStart;
  }
  public void setIsVaStart(String isVaStart) {
    this.isVaStart = isVaStart;
  }
}
