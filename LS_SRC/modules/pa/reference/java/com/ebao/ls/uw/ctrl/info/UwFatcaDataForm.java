package com.ebao.ls.uw.ctrl.info;

import java.util.ArrayList;
import java.util.List;

import com.ebao.ls.uw.ds.vo.UwFatcaDataVO;
import com.ebao.pub.framework.GenericForm;

/**
 * <p>Title: 核保主頁面_FatcaDataForm</p>
 * <p>Description: 核保主頁面_FatcaDataForm</p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company: TGL Co., Ltd.</p>
 * <p>Create Time: Jun 13, 2016</p> 
 * @author 
 * <p>Update Time: Jun 13, 2016</p>
 * <p>Updater: Alex Cheng</p>
 * <p>Update Comments: </p>
 */
public class UwFatcaDataForm extends GenericForm {

	private static final long serialVersionUID = -2064326112699920716L;

	private Long policyId;

	private List<UwFatcaDataVO> fatcaDatas = new ArrayList<UwFatcaDataVO>();

	public Long getPolicyId() {
		return policyId;
	}

	public void setPolicyId(Long policyId) {
		this.policyId = policyId;
	}

	public List<UwFatcaDataVO> getFatcaDatas() {
		return fatcaDatas;
	}

	public void setFatcaDatas(List<UwFatcaDataVO> fatcaDatas) {
		this.fatcaDatas = fatcaDatas;
	}

}
