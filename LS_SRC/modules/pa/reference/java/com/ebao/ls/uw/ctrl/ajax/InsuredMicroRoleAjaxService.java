package com.ebao.ls.uw.ctrl.ajax;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.Resource;
import javax.transaction.UserTransaction;

import org.json.simple.parser.JSONParser;

import com.ebao.ls.pa.nb.bs.DisabilityCancelDetailService;
import com.ebao.ls.pa.nb.data.DisabilityCancelDetailDao;
import com.ebao.ls.pa.nb.util.NBUtils;
import com.ebao.ls.pa.pub.TableCst;
import com.ebao.ls.pa.pub.bs.NbInsMicroRoleService;
import com.ebao.ls.pa.pub.vo.NbInsMicroRoleVO;
import com.ebao.ls.pub.CodeCst;
import com.ebao.ls.pub.ajax.AjaxService;
import com.ebao.ls.uw.ctrl.underwriting.UwPolicySubmitActionHelper;
import com.ebao.ls.uw.data.TUwInsuredListDelegate;
import com.ebao.ls.uw.data.bo.UwLifeInsured;
import com.ebao.ls.uw.ds.UwInsuredDisabilityService;
import com.ebao.ls.uw.ds.UwPolicyService;
import com.ebao.pub.util.StringUtils;
import com.ebao.pub.util.Trans;
import com.ebao.pub.util.TransUtils;

import net.sf.json.JSONObject;

public class InsuredMicroRoleAjaxService implements
                AjaxService<Map<String, String>, Map<String, Object>> {
    @Resource(name = UwInsuredDisabilityService.BEAN_DEFAULT)
    private UwInsuredDisabilityService uwInsuredDisbilityDS;

    @Resource(name = UwPolicyService.BEAN_DEFAULT)
    private UwPolicyService uwPolicyDS;

	@Resource(name = DisabilityCancelDetailService.BEAN_DEFAULT)
	private DisabilityCancelDetailService disabilityCancelDetailService;

    @Resource(name = UwPolicySubmitActionHelper.BEAN_DEFAULT)
    protected UwPolicySubmitActionHelper uwHelper;
    
	@Resource(name = DisabilityCancelDetailDao.BEAN_DEFAULT)
	protected DisabilityCancelDetailDao disabilityCancelDetailDao;
	
	@Resource(name = NbInsMicroRoleService.BEAN_DEFAULT)
	protected NbInsMicroRoleService nbInsMicroRoleService;
	
    @Resource(name = TUwInsuredListDelegate.BEAN_DEFAULT)
    private TUwInsuredListDelegate uwInsuredListDao;

    @Override
    public Map<String, Object> execute(Map<String, String> in) {
        String type = in.get("type");

        UserTransaction trans = null;
        Map<String, Object> returnMap = new HashMap<String, Object>();
        try {

            if ("load".equals(type)) {
                Long insMicroRoleInsuredId = Long.parseLong(in.get("insMicroRoleInsuredId"));
                Long underwriteId = Long.parseLong(in.get("underwriteId"));
                Long policyId = Long.parseLong(in.get("policyId"));
                if( insMicroRoleInsuredId != null && policyId != null) {
                    List<NbInsMicroRoleVO> nbInsMicroRoleList = nbInsMicroRoleService.findByPolicyId(policyId);
                    nbInsMicroRoleList.sort(Comparator.comparing(NbInsMicroRoleVO::getMicroType)
                            				.thenComparing(Comparator.comparing(NbInsMicroRoleVO::getInsRoleType)));
                    
                	//取被保險人舊的主要微型身份設定資料
                    List<NbInsMicroRoleVO> oldNbMainInsMicroRoleVOList = nbInsMicroRoleList.stream().filter( 
                    		( NbInsMicroRoleVO vo ) -> CodeCst.YES_NO__YES.equals( vo.getMainRoleflag()) && vo.getInsuredId().equals(insMicroRoleInsuredId) ).collect(Collectors.toList());
                    if( !oldNbMainInsMicroRoleVOList.isEmpty() ) {
                    	
                    	NbInsMicroRoleVO oldNbMainInsMicroRoleVO = oldNbMainInsMicroRoleVOList.get(0);
                    	UwLifeInsured  uwLifeInsured = uwInsuredListDao.findByUnderwriteIdAndListId(underwriteId, insMicroRoleInsuredId);
                    	returnMap.put("oldNbMainInsMicroRoleVO", oldNbMainInsMicroRoleVO);
                    	returnMap.put("uwLifeInsured", uwLifeInsured);
                    }
                   
                    returnMap.put("nbInsMicroRoleList", nbInsMicroRoleList.stream().filter( ( NbInsMicroRoleVO vo ) -> insMicroRoleInsuredId.equals(vo.getInsuredId())
                    		).collect( Collectors.toList() ));
                    returnMap.put("codeTable", getCodeTableInfo());
                    returnMap.put("success", "1");
                }else {
                	  returnMap.put("success", "0");
                }

            } else if ("save".equals(type)) {
            	
                Long policyId = Long.parseLong(String.valueOf(in.get("policyId")));
            	Long underwriteId = Long.parseLong(String.valueOf(in.get("underwriteId")));
                Long insMicroRoleInsuredId = Long.parseLong(String.valueOf(in.get("insMicroRoleInsuredId")));//T_NB_INS_MICRO_ROLE.INSURED_ID
                Long listId = Long.parseLong( String.valueOf(in.get("listId"))); //T_NB_INS_MICRO_ROLE.LIST_ID

                if( policyId != null ) {
                	
                    trans = Trans.getUserTransaction();
                    trans.begin();
                    
                    List<NbInsMicroRoleVO> nbInsMicroRoleVOList = nbInsMicroRoleService.findByPolicyId(policyId);
                    NbInsMicroRoleVO oldNbMainInsMicroRoleVO = null;
                    NbInsMicroRoleVO newNbMainInsMicroRoleVO = null;
                    UwLifeInsured uwLifeInsured = null;
 
                    if( !nbInsMicroRoleVOList.isEmpty() &&  insMicroRoleInsuredId != null ) {
                    	//取被保險人舊的主要微型身份設定資料
                        List<NbInsMicroRoleVO> oldNbMainInsMicroRoleVOList = nbInsMicroRoleVOList.stream().filter( 
                        		( NbInsMicroRoleVO vo ) -> CodeCst.YES_NO__YES.equals( vo.getMainRoleflag()) && vo.getInsuredId().equals(insMicroRoleInsuredId) ).collect(Collectors.toList());
                        
                        //取被保險人新的微型身份設定
                        List<NbInsMicroRoleVO> newNbMainInsMicroRoleVOList = nbInsMicroRoleVOList.stream().filter( 
                        		( NbInsMicroRoleVO vo ) -> vo.getListId().equals(listId) && vo.getInsuredId().equals(insMicroRoleInsuredId) ).collect(Collectors.toList());
                       
                        if( !newNbMainInsMicroRoleVOList.isEmpty() && newNbMainInsMicroRoleVOList.size() == 1){
                        	newNbMainInsMicroRoleVO = newNbMainInsMicroRoleVOList.get(0);
                        }
                        
                        if( !oldNbMainInsMicroRoleVOList.isEmpty() && oldNbMainInsMicroRoleVOList.size() == 1) { //已有設定主要微型身份且只有一筆
                        	
                        	oldNbMainInsMicroRoleVO = oldNbMainInsMicroRoleVOList.get(0);
                        	//判斷新舊更新是否為不同一筆
                        	if( newNbMainInsMicroRoleVO != null && !newNbMainInsMicroRoleVO.getListId().equals(oldNbMainInsMicroRoleVO.getListId())) {

                        		//舊的設定資料需清空
                        		oldNbMainInsMicroRoleVO.setMainRoleflag(null);
                        		nbInsMicroRoleService.saveOrUpdate(oldNbMainInsMicroRoleVO);
                        		
                        	}
                        	
                        }
                        
                        //更新的主要微型身份設定為空值或不為Y都需更新
                        if( StringUtils.isNullOrEmpty(newNbMainInsMicroRoleVO.getMainRoleflag()) || 
                        		!CodeCst.YES_NO__YES.equals(newNbMainInsMicroRoleVO.getMainRoleflag())) {
                        	//直接更新主要微型設定
                    		newNbMainInsMicroRoleVO.setMainRoleflag(CodeCst.YES_NO__YES);
                    		nbInsMicroRoleService.saveOrUpdate(newNbMainInsMicroRoleVO);
                        }
                    
                    }
   
                    //主要微型身份為勾選身分類型09+身分關係本人時，更新身心障礙資料
                    if( newNbMainInsMicroRoleVO != null ) {

                        uwLifeInsured = uwInsuredListDao.findByUnderwriteIdAndListId(underwriteId, insMicroRoleInsuredId);
                        
                    	if(CodeCst.RELATION_TO_PHROLE_SELF.equals(newNbMainInsMicroRoleVO.getInsRoleType())
                    		&& CodeCst.PH_ROLE_CODE_NINE.equals(newNbMainInsMicroRoleVO.getMicroType() ) ) {
                    		
                            String insMicroRoleDisabilityCategory = in.get("insMicroRoleDisabilityCategory");
                            String insMicroRoledisabilityClass = in.get("insMicroRoledisabilityClass");
                            String insMicroRoleDisabilityTypes = in.get("insMicroRoleDisabilityTypes");
                            List<String> insMicroRoleDisabilityTypeList = Arrays.asList(in.get("insMicroRoleDisabilityTypes").split(","));
                            uwLifeInsured.setMicroDisabilityCategory(insMicroRoleDisabilityCategory);
                            uwLifeInsured.setMicroDisabilityClass(insMicroRoledisabilityClass);
                            uwLifeInsured.setMicroDisabilityTypeList(insMicroRoleDisabilityTypes);
                            
                            if ( insMicroRoleDisabilityTypeList.size() > 1) {
                            	String disabilityType = disabilityCancelDetailService.findCancelReason(insMicroRoleDisabilityTypeList);
                            	uwLifeInsured.setMicroDisabilityType(disabilityType);
                            } else if(insMicroRoleDisabilityTypeList.size() == 1) {
                            	uwLifeInsured.setMicroDisabilityType(insMicroRoleDisabilityTypeList.get(0));
                            } else {
                            	uwLifeInsured.setMicroDisabilityType(null);
                            }
                            
                    	}else {
                            uwLifeInsured.setMicroDisabilityCategory(null);
                            uwLifeInsured.setMicroDisabilityClass(null);
                            uwLifeInsured.setMicroDisabilityTypeList(null);
                        	uwLifeInsured.setMicroDisabilityType(null);
                    	}

                    	uwInsuredListDao.update(uwLifeInsured);
                    	
                    }
                    
                    trans.commit();
                    returnMap.put("success", "1");
                    returnMap.put("uwLifeInsured",uwLifeInsured);
                    returnMap.put("newNbMainInsMicroRoleVO",newNbMainInsMicroRoleVO);
                }


                
            } else if ("delete".equals(type)) {
            	
                Long policyId = Long.parseLong(String.valueOf(in.get("policyId")));
            	Long underwriteId = Long.parseLong(String.valueOf(in.get("underwriteId")));
                List<String> insuredListIdList = Arrays.asList(in.get("insuredListIds").split(","));

                List<NbInsMicroRoleVO> nbInsMicroRoleVOList = nbInsMicroRoleService.findByPolicyId(policyId);
            	//取被保險人舊的主要微型身份設定資料
                List<NbInsMicroRoleVO> oldNbMainInsMicroRoleVOList = nbInsMicroRoleVOList.stream().filter( 
                		( NbInsMicroRoleVO vo ) -> CodeCst.YES_NO__YES.equals( vo.getMainRoleflag()) && insuredListIdList.contains(String.valueOf(vo.getInsuredId())) ).collect(Collectors.toList());
                
                List<UwLifeInsured> uwLifeInsuredList = new ArrayList<UwLifeInsured>();
                
                for(String insuredId : insuredListIdList) {
                	uwLifeInsuredList.add( uwInsuredListDao.findByUnderwriteIdAndListId(underwriteId, Long.valueOf(insuredId)));
                }
                
                trans = Trans.getUserTransaction();
                trans.begin();
                
                for( NbInsMicroRoleVO  oldNbMainInsMicroRoleVO : oldNbMainInsMicroRoleVOList) {
                	oldNbMainInsMicroRoleVO.setMainRoleflag(null);
                	nbInsMicroRoleService.saveOrUpdate(oldNbMainInsMicroRoleVO);
                }

                for( UwLifeInsured uwLifeInsured:uwLifeInsuredList) {
                    uwLifeInsured.setMicroDisabilityCategory(null);
                    uwLifeInsured.setMicroDisabilityClass(null);
                    uwLifeInsured.setMicroDisabilityTypeList(null);
                	uwLifeInsured.setMicroDisabilityType(null);
                	uwInsuredListDao.update(uwLifeInsured);
                }

                trans.commit();
                returnMap.put("success", "1");
            } 
        } catch (Exception e) {
            if (trans != null) {
                TransUtils.rollback(trans);
            }
            returnMap.put("success", "0");
            returnMap.put("error", e.getMessage());

        }

        return returnMap;
    }


	
	private Map<String, JSONObject> getCodeTableInfo() throws Exception {
		Map<String, JSONObject> codeTableMap = new HashMap<String, JSONObject>();

		JSONParser parser = new JSONParser();

		codeTableMap.put("nbPhRole", JSONObject.fromObject(parser.parse(JSONObject.fromObject(NBUtils.getTCodeData( TableCst.T_NB_PH_ROLE )).toString())));
		codeTableMap.put("nbLaPhRole", JSONObject.fromObject(parser.parse(JSONObject.fromObject(NBUtils.getTCodeData( TableCst.T_NB_LA_PH_ROLE )).toString())));
		codeTableMap.put("disabilityType", JSONObject.fromObject(parser.parse(JSONObject.fromObject(NBUtils.getTCodeData( TableCst.T_DISABILITY_TYPE )).toString())));
		codeTableMap.put("disabilityClass", JSONObject.fromObject(parser.parse(JSONObject.fromObject(NBUtils.getTCodeData( TableCst.T_DISABILITY_CLASS )).toString())));

		return codeTableMap;
	}

}
