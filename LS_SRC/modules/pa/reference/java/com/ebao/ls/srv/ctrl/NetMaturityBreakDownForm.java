package com.ebao.ls.srv.ctrl;

import java.math.BigDecimal;

import com.ebao.ls.srv.vo.PayDueVO;
import com.ebao.pub.annotation.DataModeChgModifyPoint;
import com.ebao.pub.framework.GenericForm;
import com.ebao.pub.util.DateUtils;

/**
 * <p>Title: NetMaturityBreakDownForm </p>		
 * <p>Description: This class NetMaturityBreakDown 's form class.</p>
 * <p>Copyright: Copyright (c) 2002	</p>
 * <p>Company: 	eBaoTech			</p>
 * <p>Create Time:		2005/04/11	</p> 
 * @author simen.li
 * @version 1.0
 */
public class NetMaturityBreakDownForm extends GenericForm {

  private BigDecimal mb = new BigDecimal("0");
  private BigDecimal rb = new BigDecimal("0");
  private BigDecimal ib = new BigDecimal("0");
  private BigDecimal sm = new BigDecimal("0");
  private BigDecimal sb = new BigDecimal("0");

  private BigDecimal cb = new BigDecimal("0");

  private BigDecimal rs = new BigDecimal("0");
  private BigDecimal gs = new BigDecimal("0");
  private BigDecimal cs = new BigDecimal("0");
  private BigDecimal apa = new BigDecimal("0");
  private BigDecimal apl = new BigDecimal("0");
  private BigDecimal policyLoan = new BigDecimal("0");
  private BigDecimal studyLoan = new BigDecimal("0");
  private BigDecimal retainAmount = new BigDecimal("0");
  private BigDecimal netMB = new BigDecimal("0");
  private BigDecimal sbPaid = new BigDecimal("0");
  private BigDecimal repayPrem = new BigDecimal("0");
  private BigDecimal paidUpAddtion = new BigDecimal("0");

  @DataModeChgModifyPoint("change from bo to vo")
  private PayDueVO payDue = null;
  private Long itemId = null;

  private String policyNumber = "";

  private String productCode = "";

  public BigDecimal getPaidUpAddtion() {
    if (null != payDue) {
      return payDue.getPaidUpAddition();
    }
    return paidUpAddtion;
  }

  public void setPaidUpAddtion(BigDecimal paidUpAddtion) {
    this.paidUpAddtion = paidUpAddtion;
  }

  public BigDecimal getRepayPrem() {
    if (null != payDue) {
      return payDue.getRepayPrem();
    }
    return repayPrem;
  }

  public void setRepayPrem(BigDecimal repayPrem) {
    this.repayPrem = repayPrem;
  }
  public String getCaseNumber() {
    //return caseNumber;
    return payDue.getCaseNumber();
  }

  public String getEndDate() {
    //return endDate;
    return DateUtils.date2String(payDue.getPayDueDate());
  }

  public String getPolicyNumber() {
    return policyNumber;
  }
  public void setPolicyNumber(String policyNumber) {
    this.policyNumber = policyNumber;
  }
  public String getProductCode() {
    return productCode;
  }
  public void setProductCode(String productCode) {
    this.productCode = productCode;
  }
  /**
   * @return Returns the itemId.
   */
  public Long getItemId() {
    return itemId;
  }
  /**
   * @param itemId The itemId to set.
   */
  public void setItemId(Long itemId) {
    this.itemId = itemId;
  }
  /**
   * @return Returns the sbPaid.
   */
  public BigDecimal getSbPaid() {
    if (null != payDue) {
      return payDue.getSbPaidAmount();
    }
    return sbPaid;
  }
  /**
   * @param sbPaid The sbPaid to set.
   */
  public void setSbPaid(BigDecimal sbPaid) {
    this.sbPaid = sbPaid;
  }
  /**
   * @return Returns the payDue.
   */
  public PayDueVO getPayDue() {
    return payDue;
  }
  /**
   * @param payDue The payDue to set.
   */
  public void setPayDue(PayDueVO payDue) {
    this.payDue = payDue;
  }

  /**
   * @return Returns the retainAmount.
   */
  public BigDecimal getRetainAmount() {
    if (null != payDue) {
      return payDue.getRetainedAmount();
    }
    return retainAmount;
  }

  /**
   * @param retainAmount The retainAmount to set.
   */
  public void setRetainAmount(BigDecimal retainAmount) {
    this.retainAmount = retainAmount;
  }

  /**
   * @return Returns the apa.
   */
  public BigDecimal getApa() {
    if (null != payDue) {
      return payDue.getApaAmount();
    }
    return apa;
  }
  /**
   * @param apa The apa to set.
   */
  public void setApa(BigDecimal apa) {
    this.apa = apa;
  }
  /**
   * @return Returns the apl.
   */
  public BigDecimal getApl() {
    //		
    if (null != payDue) {
      return payDue.getAplAmount();
    }
    return apl;
  }
  /**
   * @param apl The apl to set.
   */
  public void setApl(BigDecimal apl) {
    this.apl = apl;
  }

  /**
   *  @return Returns the cs.
   */
  public BigDecimal getCs() {
    //		
    if (null != payDue) {
      return payDue.getCollectSuspense();
    }
    return cs;
  }
  /**
   * @param cs The cs to set.
   */
  public void setCs(BigDecimal cs) {
    this.cs = cs;
  }
  /**
   * @return Returns the gs.
   */
  public BigDecimal getGs() {
    //		
    if (null != payDue) {
      return payDue.getGeneralSuspense();
    }
    return gs;
  }
  /**
   * @param gs The gs to set.
   */
  public void setGs(BigDecimal gs) {
    this.gs = gs;
  }
  /**
   * @return Returns the ib.
   */
  public BigDecimal getIb() {
    //		
    if (null != payDue) {
      return payDue.getInterimBonus();
    }
    return ib;
  }
  /**
   * @param ib The ib to set.
   */
  public void setIb(BigDecimal ib) {
    this.ib = ib;
  }
  /**
   * @return Returns the mb.
   */
  public BigDecimal getMb() {
    //		
    if (null != payDue) {
      return payDue.getFeeAmount();
    }
    return mb;
  }
  /**
   * @param mb The mb to set.
   */
  public void setMb(BigDecimal mb) {
    this.mb = mb;
  }
  /**
   * @return Returns the netMB.
   */
  @DataModeChgModifyPoint("revmove payDue.getNetMaturityAmount(),in NetMaturityBreakDownAction has set the value")
  public BigDecimal getNetMB() {
    return netMB;
  }
  /**
   * @param netMB The netMB to set.
   */
  public void setNetMB(BigDecimal netMB) {
    this.netMB = netMB;
  }
  /**
   * @return Returns the policyLoan.
   */
  public BigDecimal getPolicyLoan() {
    if (null != payDue) {
      return payDue.getLoanAmount();
    }
    return policyLoan;
  }
  /**
   * @param policyLoan The policyLoan to set.
   */
  public void setPolicyLoan(BigDecimal policyLoan) {
    this.policyLoan = policyLoan;
  }
  /**
   * @return Returns the rb.
   */
  public BigDecimal getRb() {
    if (null != payDue) {
      return payDue.getTotalBonus();
    }
    return rb;
  }
  /**
   * @param rb The rb to set.
   */
  public void setRb(BigDecimal rb) {
    this.rb = rb;
  }
  /**
   * @return Returns the rs.
   */
  public BigDecimal getRs() {
    if (null != payDue) {
      return payDue.getRenewalSuspense();
    }
    return rs;
  }
  /**
   * @param rs The rs to set.
   */
  public void setRs(BigDecimal rs) {
    this.rs = rs;
  }
  /**
   * @return Returns the sb.
   */
  public BigDecimal getSb() {
    if (null != payDue) {
      return payDue.getSbAmount();
    }
    return sb;
  }
  /**
   * @param sb The sb to set.
   */
  public void setSb(BigDecimal sb) {
    this.sb = sb;
  }
  /**
   * @return Returns the sm.
   */
  public BigDecimal getSm() {
    if (null != payDue) {
      return payDue.getSpecialBonus();
    }
    return sm;
  }
  /**
   * @param sm The sm to set.
   */
  public void setSm(BigDecimal sm) {
    this.sm = sm;
  }
  /**
   * @return Returns the studyLoan.
   */
  public BigDecimal getStudyLoan() {
    if (null != payDue) {
      return payDue.getStudyLoan();
    }
    return studyLoan;
  }
  /**
   * @param studyLoan The studyLoan to set.
   */
  public void setStudyLoan(BigDecimal studyLoan) {
    this.studyLoan = studyLoan;
  }
  /**
   * @return Returns the cb.
   */
  public BigDecimal getCb() {
    if (null != payDue) {
      return payDue.getCashBonus();
    }
    return cb;
  }
  /**
   * @param cb The cb to set.
   */
  public void setCb(BigDecimal cb) {
    this.cb = cb;
  }
}
