package com.ebao.ls.bnu.ci;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;

import javax.annotation.Resource;

import com.ebao.ls.bnu.bs.BonusCalculateService;
import com.ebao.ls.bnu.bs.BonusInfoRepVO;
import com.ebao.ls.bnu.bs.BonusIntePrdVO;
import com.ebao.ls.bnu.bs.BonusInteSumIBVO;
import com.ebao.ls.bnu.bs.BonusInteVO;
import com.ebao.ls.bnu.bs.BonusQuotationVO;
import com.ebao.pub.framework.GenericException;
import com.ebao.pub.util.BeanUtils;
import com.ebao.pub.util.Log;
/**
 *
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: eBaoTech Corporation</p>
 * @author Sunrain.Han
 * @version 1.0
 */

public class BonusCalculateCIImpl implements BonusCalculateCI {

  /**
   *
   * @param validateDate
   * @return
   * @throws GenericException
   */

  public java.util.Date getBonusDueDate(java.util.Date validateDate)
      throws GenericException {
    return getBonusCalculateDS().getBonusDueDate(validateDate);
  }
  /**
   *
   * @param changeID
   * @param policyChgID
   * @param itemID
   * @return
   * @throws GenericException
   */
  public java.math.BigDecimal clearAccuRB(Long changeID, Long policyChgID,
      Long itemID) throws GenericException {
    return getBonusCalculateDS().clearAccuRB(changeID, policyChgID, itemID);
  }
  public java.math.BigDecimal clearAccuRBSurr(Long changeID, Long policyChgID,
      Long itemID) throws GenericException {
    return getBonusCalculateDS().clearAccuRBSurr(changeID, policyChgID, itemID);
  }
  /**
   *
   * @param itemID
   * @param drawBonus
   * @param moneyID
   * @param changeID
   * @param policyChgID
   * @param optID
   * @return
   * @throws GenericException
   */
  public int getPartialSurrender(Long itemID, java.math.BigDecimal drawBonus,
      Long moneyID, Long changeID, Long policyChgID, Long optID)
      throws GenericException {
    return getBonusCalculateDS().getPartialSurrender(itemID, drawBonus,
        moneyID, changeID, policyChgID, optID);
  }
  /**
   *
   * @param itemID
   * @param oldBonus
   * @param newBonus
   * @param bonusSA
   * @param moneyID
   * @param changeID
   * @param policyChgID
   * @param optID
   * @return
   * @throws GenericException
   */
  public int adjustBonusOfIcrSA(Long itemID, java.math.BigDecimal oldBonus,
      java.math.BigDecimal newBonus, java.math.BigDecimal bonusSA,
      Long moneyID, Long changeID, Long policyChgID, Long optID)
      throws GenericException {
    return getBonusCalculateDS().adjustBonusOfIcrSA(itemID, oldBonus, newBonus,
        bonusSA, moneyID, changeID, policyChgID, optID);
  }
  /**
   *
   * @param itemID
   * @param changeID
   * @param policyChgID
   * @throws GenericException
   */
  public void cancelPartialSurr(Long itemID, Long changeID, Long policyChgID)
      throws GenericException {
    getBonusCalculateDS().cancelPartialSurr(itemID, changeID, policyChgID);
  }
  /**
   *
   * @param itemID
   * @param changeID
   * @param policyChgID
   * @throws GenericException
   */
  public void cancelIncreaseSA(Long itemID, Long changeID, Long policyChgID)
      throws GenericException {
    getBonusCalculateDS().cancelIncreaseSA(itemID, changeID, policyChgID);
  }

  /**
   *
   * @param bonusDate
   * @return
   * @throws GenericException
   */

  public BonusIntePrdVO[] getIntegrationBonusRatePrd(Date bonusDate)
      throws GenericException {
    ArrayList bonusInteList;
    BonusIntePrdVO[] bonusInteCIVO;

    bonusInteList = getBonusCalculateDS().getIntegrationBonusRatePrd(bonusDate);
    if (bonusInteList == null)
      return null;

    try {

      bonusInteCIVO = (BonusIntePrdVO[]) BeanUtils.copyCollection(
          BonusIntePrdVO.class, bonusInteList).toArray(new BonusIntePrdVO[0]);
    } catch (Exception ex) {
      Log.error(this.getClass(), ex);
      return null;
    }
    // bonusInteArr= (BonusIntePrdCIVO[]) bonusInteList.toArray(new BonusIntePrdCIVO[0]);
    return bonusInteCIVO;
  }
  /**
   *
   * @param bonusDate
   * @param nricNo
   * @param nricType
   * @return
   * @throws GenericException
   */
  public BonusIntePrdVO[] getIntegrationBonusRate(java.util.Date bonusDate,
      java.lang.Long policyID) throws GenericException {
    ArrayList bonusInteList;
    BonusIntePrdVO[] bonusInteCIVO;
    bonusInteList = getBonusCalculateDS().getIntegrationBonusRate(bonusDate,
        policyID);
    try {
      bonusInteCIVO = (BonusIntePrdVO[]) BeanUtils.copyCollection(
          BonusIntePrdVO.class, bonusInteList).toArray(new BonusIntePrdVO[0]);
    } catch (Exception ex) {
      Log.error(this.getClass(), ex);
      return null;
    }

    return bonusInteCIVO;
  }
  /**
   *
   * @param policyID
   * @return
   * @throws GenericException
   */
  public BonusInteVO getIntegrationSumIB(Long policyID) throws GenericException {
    //   ArrayList  bonusInteList = new ArrayList();
    BonusIntePrdVO[] bonusInteCIVOArr;
    BonusIntePrdVO[] bonusInteVOArr;

    BonusInteSumIBVO bonusInteSumIBCIVO = new BonusInteSumIBVO();
    BonusInteSumIBVO bonusInteSumIBVO;
    BonusInteVO bonusInteVO;
    BonusInteVO bonusInteCIVO = new BonusInteVO();

    bonusInteVO = getBonusCalculateDS().getIntegrationSumIB(policyID);

    try {
      bonusInteSumIBVO = bonusInteVO.getBonusInteSumIBVO();
      BeanUtils.copyProperties(bonusInteSumIBCIVO, bonusInteSumIBVO);
    } catch (Exception ex) {
      Log.error(this.getClass(), ex);
      return null;
    }
    bonusInteVOArr = bonusInteVO.getBonusIntePrdVO();
    try {
      bonusInteCIVOArr = (BonusIntePrdVO[]) BeanUtils.copyArray(
          BonusIntePrdVO.class, bonusInteVOArr);
    } catch (Exception ex1) {
      Log.error(this.getClass(), ex1);
      return null;
    }
    bonusInteCIVO.setBonusIntePrdVO(bonusInteCIVOArr);
    bonusInteCIVO.setBonusInteSumIBVO(bonusInteSumIBCIVO);
    return bonusInteCIVO;

  }
  /**
   *
   * @param itemID
   * @param optionSB
   * @param optionCB
   * @param premOption
   * @param projectDate
   * @param projectValue
   * @throws GenericException
   */
  public void getProjectValue(Long itemID, String optionSB, String optionCB,
      String premOption, java.util.Date projectDate, String[] projectValue)
      throws GenericException {
    getBonusCalculateDS().getProjectValue(itemID, optionSB, optionCB,
        premOption, projectDate, projectValue);
  }
  /**
   *
   * @param itemID
   * @param optionSB
   * @param maturityValue
   * @throws GenericException
   */
  public void getMaturityValue(Long itemID, String optionSB,
      String[] maturityValue) throws GenericException {
    getBonusCalculateDS().getMaturityValue(itemID, optionSB, maturityValue);
  }
  /**
   *
   * @param policyID
   * @param itemID
   * @param calcDate
   * @return
   * @throws GenericException
   */
  public BonusInteSumIBVO getBonusInfoForInte(Long policyID, Long itemID,
      java.util.Date calcDate) throws GenericException {
    BonusInteSumIBVO bonusInteSumIBVO;
    BonusInteSumIBVO bonusInteSumIBCIVO = new BonusInteSumIBVO();
    bonusInteSumIBVO = getBonusCalculateDS().getBonusInfoForInte(policyID,
        itemID, calcDate);

    BeanUtils.copyProperties(bonusInteSumIBCIVO, bonusInteSumIBVO);

    return bonusInteSumIBCIVO;
  }
  /**
   *
   * @param itemID
   * @param changeID
   * @param policyChgID
   * @throws GenericException
   */
  public void cancelChangeCommenceDate(Long itemID, Long changeID,
      Long policyChgID) throws GenericException {
    getBonusCalculateDS().cancelChangeCommenceDate(itemID, changeID,
        policyChgID);
  }
  /**
   *
   * @param itemID
   * @param changeID
   * @param policyChgID
   * @param oldProductID
   * @throws GenericException
   */
  public void changeBenefit(Long itemID, Long changeID, Long policyChgID,
      Long oldProductID) throws GenericException {
    getBonusCalculateDS().changeBenefit(itemID, changeID, policyChgID,
        oldProductID);
  }
  /**
   *
   * @param itemID
   * @param changeID
   * @param policyChgID
   * @param oldProductID
   * @throws GenericException
   */
  public void cancelChangeBenefit(Long itemID, Long changeID, Long policyChgID,
      Long oldProductID) throws GenericException {
    getBonusCalculateDS().cancelChangeBenefit(itemID, changeID, policyChgID,
        oldProductID);
  }
  /**
   *
   * @param itemID
   * @param oldAmount
   * @param newAmount
   * @param changeID
   * @param policyChgID
   * @throws GenericException
   */
  public void calcSaPartSurr(java.lang.Long itemID,
      java.math.BigDecimal oldAmount, java.math.BigDecimal newAmount,
      java.lang.Long changeID, java.lang.Long policyChgID)
      throws GenericException {
    getBonusCalculateDS().calcSaPartSurr(itemID, oldAmount, newAmount,
        changeID, policyChgID);
  }
  /**
   *
   * @param itemID
   * @param changeID
   * @param policyChgID
   * @throws GenericException
   */
  public void cancelSaPartSurr(java.lang.Long changeID,
      java.lang.Long policyChgID, java.lang.Long itemID)
      throws GenericException {
    getBonusCalculateDS().cancelSaPartSurr(changeID, policyChgID, itemID);
  }
  /**
   *
   * @param itemID
   * @param changeID
   * @param policyChgID
   * @throws GenericException
   */
  public void cancelClearRB(java.lang.Long itemID, java.lang.Long changeID,
      java.lang.Long policyChgID) throws GenericException {
    getBonusCalculateDS().cancelClearRB(itemID, changeID, policyChgID);
  }
  /**
   *
   * @param itemID
   * @param calcDate
   * @throws GenericException
   */
  public BonusQuotationVO quotationCalcBonus(java.lang.Long itemID,
      java.util.Date calcDate, Date validateDate) throws GenericException {
    BonusQuotationVO bonusQuotationCIVO = new BonusQuotationVO();
    BonusQuotationVO bonusQuotationVO;
    bonusQuotationVO = getBonusCalculateDS().quotationCalcBonus(itemID,
        calcDate, validateDate);

    BeanUtils.copyProperties(bonusQuotationCIVO, bonusQuotationVO);

    return bonusQuotationCIVO;
  }
  
  

    @Override
    public BigDecimal calcUnallocBonus4Claim(Long itemID, String payPlanType,
                    Date eventDate) throws GenericException {

        return getBonusCalculateDS().calcUnallocBonus4Claim(itemID,
                        payPlanType, eventDate);
    }
    
    
@Override
    public BonusInfoRepVO getBonusInfo4Rep(Long policyId, Date anniDate)
                    throws GenericException {
        return getBonusCalculateDS().getBonusInfo4Rep(policyId, anniDate);
    }





@Resource(name = BonusCalculateService.BEAN_DEFAULT)
  private BonusCalculateService bonusCalculateDS;
  
  public BonusCalculateService getBonusCalculateDS() {
    return this.bonusCalculateDS;
  }

}