package com.ebao.ls.uw.ds;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;

import com.ebao.ls.foundation.service.GenericServiceImpl;
import com.ebao.ls.liaRoc.ci.LiaRocUploadCommonService;
import com.ebao.ls.pa.nb.util.NBUtils;
import com.ebao.ls.pa.pub.data.bo.UwNotintoRecv;
import com.ebao.ls.pa.pub.vo.InsuredVO;
import com.ebao.ls.pa.pub.vo.UwNotintoRecvVO;
import com.ebao.ls.uw.data.TUwNotintoRecvDelegate;
import com.ebao.pub.framework.GenericException;
import com.ebao.pub.util.BeanUtils;
public class UwNotintoRecvServiceImpl extends GenericServiceImpl<UwNotintoRecvVO, UwNotintoRecv, TUwNotintoRecvDelegate<UwNotintoRecv>> implements UwNotintoRecvService {

	@Resource(name = LiaRocUploadCommonService.BEAN_DEFAULT)
	private LiaRocUploadCommonService liaRocUploadCommonService;
	
	@Override
	protected UwNotintoRecvVO newEntityVO() {
		return new UwNotintoRecvVO();
	}

	@Override
	public List<UwNotintoRecvVO> getListByPolicyId(Long policyId) {
		
		List<UwNotintoRecvVO> result = null;
		
		// 執行日小於新式公會通報生效日期(2021/07/01)，要執行舊式公會通報
		if (liaRocUploadCommonService.isLiaRocV2020Process()) {
			result = dao.getListByPolicyIdByLiaRoc2020(policyId);
		}else{
			result = dao.getListByPolicyId(policyId);
		}

		return result;
	}
	
	@Override
	public List<UwNotintoRecvVO> getListByPolicyIdAfterApplyDate(Long policyId,Date afterDay) {
		List<UwNotintoRecvVO> result = dao.getListByPolicyIdAfterApplyDate(policyId, afterDay);
		return result;
	}
	
	@Override
	public Long saveUwNotintoRecv(UwNotintoRecvVO vo) throws GenericException {

	    UwNotintoRecv bo = new UwNotintoRecv();
	    BeanUtils.copyProperties(bo, vo);
	    if(vo.getSeqNo() != null){
	        UwNotintoRecv data = dao.findUniqueByProperty("seqNo", vo.getSeqNo());
	        if(data != null){
	            data.copyFromVO(vo, Boolean.TRUE, Boolean.TRUE);
	            dao.saveorUpdate(data);
	            return data.getListId();
	        }
	    }
	    dao.save(bo);
	    return bo.getListId();
	}
	
	public List<String[]> getListByCertiCode(String certiCode) {
		return dao.getListByCertiCode(certiCode);
	}
	
	/**
	 * <p>Description : 未進件之收件通報查詢</p>
	 * <p>Created By : Simon Huang</p>
	 * <p>Create Time : 2020年4月27日</p>
	 * @param policyId
	 * @param insureds
	 * @return
	 */
	public List<UwNotintoRecvVO> getNotIntoAFINSList(Long policyId, List<InsuredVO> insureds){
		/* 未進件之收件通報查詢 */
		List<UwNotintoRecvVO> allUwNotintoRecvList = new ArrayList<UwNotintoRecvVO>();
		List<UwNotintoRecvVO> uwNotintoRecvList = new ArrayList<UwNotintoRecvVO>();
		allUwNotintoRecvList = this.getListByPolicyId(policyId); //AFINS此保單被保險人ID通報記錄
	
		Map<String,List<String[]>> historyPolicyMap = new HashMap<String,List<String[]>>();
		
		for(InsuredVO insuredVO : insureds) {
			String certiCode = insuredVO.getCertiCode();
			historyPolicyMap.put(certiCode, this.getListByCertiCode(certiCode));
		}
		for (UwNotintoRecvVO vo : allUwNotintoRecvList) {
			String serialNum = vo.getSerialNum();
			String policyCode = vo.getPolicyCode();
			String uploadPolicyNo = vo.getUploadPolicyNo();
			String bizCode = "" ;  //若是新契約通報比對到，以新契約通報的保單號作是否進件保單號比對
			Boolean isExist = false;
			
			String displayPolicyCode = ""; //返回前端顯示用的保單號，依AFINS通報規則抓取
			if (StringUtils.isNotBlank(serialNum)) {
				displayPolicyCode = serialNum;
			} else if (StringUtils.isNotBlank(policyCode)) {
				displayPolicyCode = policyCode;
			} else if (StringUtils.isNotBlank(uploadPolicyNo)) {
				displayPolicyCode = uploadPolicyNo;
			}

			if(StringUtils.isNotEmpty( vo.getDynamicProperty("bizCode"))) {
				bizCode = vo.getDynamicProperty("bizCode");
			}

			if (historyPolicyMap.get(vo.getCeritCode()) != null) {
				List<String[]> historyPolicyList = historyPolicyMap.get(vo.getCeritCode());
				for (String[] array : historyPolicyList) {
					String liarocPolicyCode1 = array[0]; //已落地保單號
					String liarocPolicyCode2 = array[1]; //已落地保單號或受理編號
					String internalId = array[2];
					String liarocPolicySerialNum = array[3]; //受理編號

					//若是新契約通報，以新契約通報的保單號作是否進件保單號比對(bizCode)
					//比對AFINS上傳的受理編號或保單號，為已通報用的保單號則認為已進件(serialNum,policyCode)
					boolean isExistPolicyCode = NBUtils.in(bizCode, liarocPolicyCode1, liarocPolicyCode2)
									 || NBUtils.in(serialNum, liarocPolicyCode1, liarocPolicyCode2, liarocPolicySerialNum)
									 || NBUtils.in(policyCode, liarocPolicyCode1, liarocPolicyCode2);
					if (isExistPolicyCode 
									&& NBUtils.in(vo.getInternalId(), internalId)) {
						isExist = true;
						break;
					}
				}
			}
			if (!isExist) {// 動態顯示規則：收件通報資料的保單號碼，不存在核心系統通報公會的保單號碼資料，則需顯示。
				vo.setPolicyCode(displayPolicyCode);
				uwNotintoRecvList.add(vo);
			} 	
		}
		
		return uwNotintoRecvList;
	}
}
