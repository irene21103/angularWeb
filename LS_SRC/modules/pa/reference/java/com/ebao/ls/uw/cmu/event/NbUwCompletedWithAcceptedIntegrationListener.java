package com.ebao.ls.uw.cmu.event;

import com.ebao.foundation.commons.annotation.TriggerPoint;
import com.ebao.ls.cmu.event.NbUwCompletedWithAccepted;
import com.ebao.pub.event.AbstractIntegrationListener;
import com.ebao.pub.integration.IntegrationMessage;

public class NbUwCompletedWithAcceptedIntegrationListener
    extends
      AbstractIntegrationListener<NbUwCompletedWithAccepted> {
  final static String code = "uw.nbuwCompleted.accepted";

  @Override
  @TriggerPoint(component = "Underwriting", description = "When a proposal has been entered in system, it will go through auto-underwriting and manual underwriting if necessary.<br/>In any case, if the underwriting decision is 'Accepted', the event is triggered.<br/>Triggers can be either of the following: <br/><ol><li>Manual underwriting is triggered in menu: New business > Worklist > Underwriting.</li><li>Auto-underwriting is triggered in <ul><li>New business > Worklist > Data entry </li><li>New business > Worklist > Verification</li></ul></li></ol>", event = "direct:event."
      + code, id = "com.ebao.tp.ls." + code)
  protected String getEventCode(NbUwCompletedWithAccepted event,
      IntegrationMessage<Object> in) {
    return "event." + code;
  }
}
