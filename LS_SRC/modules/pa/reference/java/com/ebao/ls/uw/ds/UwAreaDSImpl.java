package com.ebao.ls.uw.ds;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import com.ebao.ls.foundation.service.GenericServiceImpl;
import com.ebao.ls.uw.data.TUwAreaDelegate;
import com.ebao.ls.uw.data.bo.UwArea;
import com.ebao.ls.uw.vo.UwAreaVO;

/**
 * <p>Title: Module Information Andy GOD</p>
 * <p>Description: Function Description Only vegetable</p>
 * <p>Copyright: Copyright (c) 2015</p>
 * <p>Company: TGL Co., Ltd.</p>
 * <p>Create Time: Aug 11, 2015</p> 
 * @author 
 * <p>Update Time: Aug 11, 2015</p>
 * <p>Updater: Alex Cheng</p>
 * <p>Update Comments: </p>
 */
public class UwAreaDSImpl extends GenericServiceImpl<UwAreaVO, UwArea, TUwAreaDelegate> implements UwAreaService{

  @Resource(name = TUwAreaDelegate.BEAN_DEFAULT)
  private TUwAreaDelegate uwAreaDao;

  @Override
  protected UwAreaVO newEntityVO() {
    return new UwAreaVO();
  }

  @Override
  public List<UwAreaVO> findUwAreaByEmpId(Long empId){
    List<UwArea> retList = uwAreaDao.findUwAreaByEmpId(empId);
    return convertToVOList(retList);
  }

    @Override
    public List<Long> searchIdsByChannelId(Long channelId) {
        List<UwArea> retList = uwAreaDao.findUwAreaByChannelId(channelId);
        List<Long> returnList = new ArrayList<Long>();
        for (UwArea area : retList) {
            returnList.add(area.getEmpId());
        }
        return returnList;
    }
}
