package com.ebao.ls.riskAggregation.ctrl;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.ls.riskAggregation.bs.RiskAggregationService;
import com.ebao.pub.framework.GenericAction;

public class ReCalRAInfoAction extends GenericAction {

  public static final String BEAN_DEFAULT = "/ra/calcRaInfo";

public ActionForward process(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {
    RaInfoForm raForm = (RaInfoForm) form;

    riskService.calcRA(raForm.getPolicyId());

    return mapping.findForward("display");
  }

  @Resource(name = RiskAggregationService.BEAN_DEFAULT)
  private RiskAggregationService riskService;

}
