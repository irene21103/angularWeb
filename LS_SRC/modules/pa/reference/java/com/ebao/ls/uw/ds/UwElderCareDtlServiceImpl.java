package com.ebao.ls.uw.ds;

import com.ebao.ls.foundation.service.GenericServiceImpl;
import com.ebao.ls.pa.pub.vo.VOGenerator;
import com.ebao.ls.uw.data.UwElderCareDtlDao;
import com.ebao.ls.uw.data.bo.UwElderCareDtl;
import com.ebao.ls.uw.vo.UwElderCareDtlVO;
import java.util.List;
import javax.annotation.Resource;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.SQLQuery;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;

/**
 * 高齡投保核保評估明細表服務實作
 *
 * @author Bruce Liao
 */
public class UwElderCareDtlServiceImpl extends GenericServiceImpl<UwElderCareDtlVO, UwElderCareDtl, UwElderCareDtlDao<UwElderCareDtl>> implements UwElderCareDtlService {

    protected final Logger logger = LogManager.getFormatterLogger();

    @Resource(name = VOGenerator.BEAN_DEFAULT)
    private VOGenerator voGenerator;

    @Override
    protected UwElderCareDtlVO newEntityVO() {
        return voGenerator.uwElderCareDtlVONewInstance();
    }

    @Override
    public List<UwElderCareDtlVO> findByCriteriaWithOrder(List<Order> orders, Criterion... criterion) {
        return convertToVOList(dao.findByCriteriaWithOrder(orders, criterion));
    }

    @Override
    public List<UwElderCareDtlVO> findByCriteria(Criterion... criterion) {
        return convertToVOList(dao.findByCriteria(criterion));
    }

    @Override
    public int updateCareDataToDefaultById(Long listId) {
        SQLQuery query = dao.getSession().createSQLQuery("UPDATE T_UW_ELDER_CARE_DTL SET RESULT_CODE = 'N' WHERE LIST_ID = :LIST_ID");
        query.setParameter("LIST_ID", listId);
        return query.executeUpdate();
    }

}
