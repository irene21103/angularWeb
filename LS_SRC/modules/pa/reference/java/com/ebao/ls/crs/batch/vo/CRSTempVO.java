package com.ebao.ls.crs.batch.vo;

import com.ebao.ls.crs.vo.CRSDueDiligenceListVO;
import com.ebao.ls.fatca.vo.FatcaDueDiligenceListVO;

public class CRSTempVO {
	
	private Long changeId;
	
	private Long crsId;
	
	private FatcaDueDiligenceListVO fatcaDueDiligenceListVO;
	
	private CRSDueDiligenceListVO crsDueDiligenceListVO;
	
	private String tempBatch;
	
	public String getTempBatch() {
		return tempBatch;
	}

	public void setTempBatch(String tempBatch) {
		this.tempBatch = tempBatch;
	}

	public CRSDueDiligenceListVO getCrsDueDiligenceListVO() {
		return crsDueDiligenceListVO;
	}

	public void setCrsDueDiligenceListVO(CRSDueDiligenceListVO crsDueDiligenceListVO) {
		this.crsDueDiligenceListVO = crsDueDiligenceListVO;
	}

	private boolean isNewRecord;

	public boolean isNewRecord() {
		return isNewRecord;
	}

	public void setNewRecord(boolean isNewRecord) {
		this.isNewRecord = isNewRecord;
	}

	public Long getChangeId() {
		return changeId;
	}

	public void setChangeId(Long changeId) {
		this.changeId = changeId;
	}

	public FatcaDueDiligenceListVO getFatcaDueDiligenceListVO() {
		return fatcaDueDiligenceListVO;
	}

	public void setFatcaDueDiligenceListVO(FatcaDueDiligenceListVO fatcaDueDiligenceListVO) {
		this.fatcaDueDiligenceListVO = fatcaDueDiligenceListVO;
	}
	
	private boolean isNewCRSRecord;

	public boolean isNewCRSRecord() {
		return isNewCRSRecord;
	}

	public void setNewCRSRecord(boolean isNewCRSRecord) {
		this.isNewCRSRecord = isNewCRSRecord;
	}

	public Long getCrsId() {
		return crsId;
	}

	public void setCrsId(Long crsId) {
		this.crsId = crsId;
	}

	
}
