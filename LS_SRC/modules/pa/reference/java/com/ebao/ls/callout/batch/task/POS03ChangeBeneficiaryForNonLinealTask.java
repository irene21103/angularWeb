package com.ebao.ls.callout.batch.task;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.transaction.UserTransaction;

import org.apache.commons.collections.MapUtils;
import org.springframework.context.ApplicationContextAware;

import com.ebao.foundation.commons.exceptions.ExceptionUtil;
import com.ebao.foundation.core.logging.Log;
import com.ebao.foundation.module.para.Para;
import com.ebao.ls.callout.batch.AbstractCalloutTask;
import com.ebao.ls.callout.batch.CalloutConstants;
import com.ebao.ls.callout.batch.CalloutDataExportService;
import com.ebao.ls.callout.data.bo.CalloutConfig;
import com.ebao.ls.cs.ci.TeleInterviewCI;
import com.ebao.ls.pub.cst.CalloutConfigType;
import com.ebao.ls.pub.cst.YesNo;
import com.ebao.pub.batch.help.BatchHelp;
import com.ebao.pub.batch.type.JobStatus;
import com.ebao.pub.batch.type.LogLevel;
import com.ebao.pub.batch.util.BatchLogUtils;
import com.ebao.pub.util.DateUtils;
import com.ebao.pub.util.TransUtils;

public class POS03ChangeBeneficiaryForNonLinealTask extends AbstractCalloutTask implements ApplicationContextAware {
    
    public static final String BEAN_DEFAULT = "posThreeTask";

    private final Log log = Log.getLogger(POS03ChangeBeneficiaryForNonLinealTask.class);
    
    private final Long DISPLAY_ORDER = 3L;

    private String CONFIG_TYPE = CalloutConfigType.CALLOUT_CONFIG_TYPE_POS;
    
    @Resource(name = TeleInterviewCI.BEAN_DEFAULT)
    private TeleInterviewCI pos;
    
    @Resource(name=CalloutDataExportService.BEAN_DEFAULT)
    private CalloutDataExportService exportServ;
    
    private void syso(Object msg){
        log.info("[BR-CMN-TFI-005][檔案POS-3 變更身故受益人非直系血親、配偶、法定繼承人]" + msg);
        BatchLogUtils.addLog(LogLevel.INFO, null, "[BR-CMN-TFI-005][檔案POS-3 變更身故受益人非直系血親、配偶、法定繼承人]" +msg);
    }
    
    private void syso(String layout, Object... msg){
        syso(String.format(layout, msg));
    }

    @Override
    public int mainProcess() throws Exception {
        String fileName = Para.getParaValue(CalloutConstants.POSThree);
        /* 是否重覆跑批 */
        java.util.Date processDate = BatchHelp.getProcessDate();
        if(isProcess(fileName, BatchHelp.getProcessDate())){
            syso("業務日期 = %s, 已處理完畢", com.ebao.pub.util.json.DateUtils.convertToMinguoDate(BatchHelp.getProcessDate()));
            return JobStatus.EXECUTE_SUCCESS;
        }
        /* 抽取母體資料 */
        CalloutConfig cfg = getConfig(CONFIG_TYPE, DISPLAY_ORDER);
        Long sampleType = cfg.getListId();
        if(sampleType == null)
            return JobStatus.EXECUTE_SUCCESS;
        
        List<Map<String, Object>> mapList = getData(processDate, null);
        mapList = cleanDuplicateBatch(mapList, sampleType, processDate);
        
        int totalSize = mapList != null  ? mapList.size()  : 0;
        syso("母體資料 List.size = " + totalSize);
        if(totalSize == 0)
            return JobStatus.EXECUTE_SUCCESS;
        
        /* 抽取樣本資料 */
        Integer sampleSize = readSample(mapList, CONFIG_TYPE, DISPLAY_ORDER);
        syso("資料抽樣筆數 = " + sampleSize);

        /* 儲存電訪紀錄 */
        UserTransaction ut = null;
        try{
            ut = TransUtils.getUserTransaction();
            ut.begin();
            save(mapList, BatchHelp.getProcessDate(), fileName, "POS","POS-3", CONFIG_TYPE, DISPLAY_ORDER);
            /* 匯出抽樣保單 */
            exportServ.feedMap(mapList, CalloutConstants.POSThree, BatchHelp.getProcessDate());
            ut.commit();
        }catch(Exception ex){
            TransUtils.rollback(ut);
            BatchLogUtils.addLog(LogLevel.ERROR, null, "[BR-CMN-TFI-005][檔案POS-3 變更身故受益人非直系血親、配偶、法定繼承人]\n" + ExceptionUtil.parse(ex));
            return JobStatus.EXECUTE_FAILED;
        }
        
        
        return JobStatus.EXECUTE_SUCCESS;
    }
    
    public java.util.List<Map<String, Object>> getData(Date inputDate, String policyCode){
        syso("業務日期= '%s'", com.ebao.pub.util.json.DateUtils.format( inputDate , "yyyy/MM/dd"));
        Date processDate = DateUtils.getNextWorkday(inputDate, -1);
        syso("前一工作日= '%s'", com.ebao.pub.util.json.DateUtils.format(  processDate, "yyyy/MM/dd"));
        List<Map<String, Object>> mapList = pos.getChangeBeneficiaryForNonLineal(processDate, processDate);
        return mapList;
    } 

    @Override
    public String saveToTrans(Map<String, Object> map, Date processDate,
                    String fileName, String dept) {
        String calloutNum = saveInsured(map, processDate, fileName, dept);
        return calloutNum;
    }
    
    /**
     * <p>Description : 抽取樣本資料(備註：以保單為單位)</p>
     * <p>Created By : Marvin Hsu</p>
     * <p>Create Time : Aug 29, 2018</p>
     * @param entireList
     * @param type
     * @param ConfigClass
     * @return 抽樣筆數
     */
    @Override
    public Integer readSample(List<Map<String, Object>> mapList, String type, Long DisplayOrder) {
        Map<String,Integer> sampleMap = new HashMap<String, Integer>();
        int sampleSize = 0;
        /* 抽樣筆數 */
        List<Map<String, Object>> getSampleRate = calloutConfigService.query(type, DisplayOrder, null);
        if (getSampleRate != null && getSampleRate.size() > 0) {
            BigDecimal sampleRate = (BigDecimal) getSampleRate.get(0).get("DATA"); // 拿取抽樣比例。
            BigDecimal sampleNumCalc = new BigDecimal(Float.valueOf(mapList.size()) * (sampleRate.floatValue() / 100f));// 計算需抽取。
            sampleNumCalc = sampleNumCalc.setScale(0, BigDecimal.ROUND_UP); // 先用無條件進位。
            sampleSize = sampleNumCalc.intValue();
        }
        /* 不足1件抽1件 */
        if (sampleSize < 1) {
        	sampleSize=1;
            return sampleSize;
        }
        
        /* 填滿值，且不能出現重覆狀態 */
        int bufferSize = 0;
        int mapSize = mapList.size();
        for (int now = 0; bufferSize < sampleSize && now < MAX_SAMPLE_ROUND; now++) {
            int rowNum = new java.util.Random().nextInt(mapSize);
            Map<String, Object> map = mapList.get(rowNum);
            String policyCode = MapUtils.getString(map, "POLICY_CODE");
            if(!sampleMap.containsKey(policyCode)){//無重覆抽取保單
                map.put("IS_CALLOUT",  YesNo.YES_NO__YES);
                sampleMap.put(policyCode, new Integer(rowNum));
                syso("抽出保單號碼 := %s, 流水號 :=%d ", policyCode, rowNum);
                bufferSize += 1;
            }
        }
        return bufferSize;
    }
}
