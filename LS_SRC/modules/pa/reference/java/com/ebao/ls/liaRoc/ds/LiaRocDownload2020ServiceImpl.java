package com.ebao.ls.liaRoc.ds;

import com.ebao.ls.foundation.service.GenericServiceImpl;
import com.ebao.ls.liaRoc.bo.LiaRocDownload2020;
import com.ebao.ls.liaRoc.bo.LiaRocDownload2020VO;
import com.ebao.ls.liaRoc.data.LiaRocDownload2020Dao;

public class LiaRocDownload2020ServiceImpl
		extends GenericServiceImpl<LiaRocDownload2020VO, LiaRocDownload2020, LiaRocDownload2020Dao<LiaRocDownload2020>>
		implements LiaRocDownload2020Service {

	@Override
	protected LiaRocDownload2020VO newEntityVO() {
		return new LiaRocDownload2020VO();
	}

}
