package com.ebao.ls.uw.ctrl.lifeSurvey;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.UserTransaction;

import org.apache.commons.lang.math.RandomUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.jfree.util.Log;

import com.ebao.ls.chl.service.AgentService;
import com.ebao.ls.chl.service.ChannelOrgService;
import com.ebao.ls.chl.vo.ChannelOrgVO;
import com.ebao.ls.cmu.pub.document.DocumentFormFile;
import com.ebao.ls.cmu.pub.model.DocumentInput.DocumentPropsKey;
import com.ebao.ls.notification.extension.letter.nb.FmtUnb0100ExtensionVO;
import com.ebao.ls.notification.extension.letter.nb.FmtUnb0100ObjectVO;
import com.ebao.ls.notification.extension.letter.nb.NbLetterExtensionVO;
import com.ebao.ls.notification.extension.letter.nb.index.FmtUnb0100IndexVO;
import com.ebao.ls.notification.message.WSItemString;
import com.ebao.ls.notification.message.WSLetterEmailInfo;
import com.ebao.ls.notification.message.WSLetterSendingMethod;
import com.ebao.ls.notification.message.WSLetterUnit;
import com.ebao.ls.pa.nb.bs.rule.ProposalRuleMsgService;
import com.ebao.ls.pa.nb.bs.rule.ProposalRuleResultService;
import com.ebao.ls.pa.nb.ci.ValidatorService;
import com.ebao.ls.pa.nb.ctrl.letter.NbLetterHelper;
import com.ebao.ls.pa.nb.ctrl.letter.NbNotificationHelper;
import com.ebao.ls.pa.nb.rule.ValidatorUtils;
import com.ebao.ls.pa.nb.util.Cst;
import com.ebao.ls.pa.nb.util.NBUtils;
import com.ebao.ls.pa.nb.util.UnbCst;
import com.ebao.ls.pa.nb.util.UnbRoleType;
import com.ebao.ls.pa.nb.vo.ProposalRuleMsgVO;
import com.ebao.ls.pa.nb.vo.ProposalRuleResultVO;
import com.ebao.ls.pa.pub.bs.AddressService;
import com.ebao.ls.pa.pub.bs.AgentNotifyService;
import com.ebao.ls.pa.pub.bs.InsuredService;
import com.ebao.ls.pa.pub.bs.NbPolicySpecialRuleService;
import com.ebao.ls.pa.pub.bs.NbQuestSettingService;
import com.ebao.ls.pa.pub.bs.PolicyService;
import com.ebao.ls.pa.pub.vo.AgentNotifyVO;
import com.ebao.ls.pa.pub.vo.CoverageAgentVO;
import com.ebao.ls.pa.pub.vo.CoverageVO;
import com.ebao.ls.pa.pub.vo.DetailRegPolicyVO;
import com.ebao.ls.pa.pub.vo.InsuredVO;
import com.ebao.ls.pa.pub.vo.NbQuestSettingVO;
import com.ebao.ls.pa.pub.vo.PolicyHolderVO;
import com.ebao.ls.pa.pub.vo.PolicyVO;
import com.ebao.ls.pa.pub.vo.UwCheckListStatusVO;
import com.ebao.ls.pty.ci.EmployeeCI;
import com.ebao.ls.pty.ds.DeptService;
import com.ebao.ls.pty.vo.AddressVO;
import com.ebao.ls.pty.vo.DeptVO;
import com.ebao.ls.pty.vo.EmployeeVO;
import com.ebao.ls.pub.CodeCst;
import com.ebao.ls.sc.vo.AgentChlVO;
import com.ebao.ls.uw.ci.UwPolicyCI;
import com.ebao.ls.uw.ctrl.elderAudio.ElderAudioObject;
import com.ebao.ls.uw.ctrl.helper.ElderAudioHelper;
import com.ebao.ls.uw.ctrl.letter.UNBLetterAction;
import com.ebao.ls.uw.ds.UwCheckListStatusService;
import com.ebao.ls.uw.ds.UwElderCareService;
import com.ebao.ls.uw.ds.UwLifeSurveyItemService;
import com.ebao.ls.uw.ds.UwPolicyService;
import com.ebao.ls.uw.ds.UwSurveyLetterService;
import com.ebao.ls.uw.vo.UwElderCareDtlVO;
import com.ebao.ls.uw.vo.UwElderCareTypeVO;
import com.ebao.ls.uw.vo.UwElderCareVO;
import com.ebao.ls.uw.vo.UwLifeSurveyItemVO;
import com.ebao.ls.uw.vo.UwPolicyVO;
import com.ebao.ls.uw.vo.UwSurveyAgentSentVO;
import com.ebao.ls.uw.vo.UwSurveyLetterVO;
import com.ebao.ls.uw.vo.UwSurveyListVO;
import com.ebao.ls.uw.vo.UwSurveyObjectVO;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.framework.ExceptionFactory;
import com.ebao.pub.framework.GenericException;
import com.ebao.pub.i18n.lang.StringResource;
import com.ebao.pub.para.Para;
import com.ebao.pub.util.StringUtils;
import com.ebao.pub.util.Trans;
import com.ebao.pub.util.TransUtils;

/**
 * <p>Title: 核保/生調交查</p>
 * <p>Description: 核保/生調交查</p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company: TGL Co., Ltd.</p>
 * <p>Create Time: Apr 16, 2016</p> 
 * @author 
 * <p>Update Time: Apr 16, 2016</p>
 * <p>Updater: Alex Cheng</p>
 * <p>Update Comments: </p>
 */
public class LifeSurveyLetterAction extends UNBLetterAction {
	private final com.ebao.foundation.core.logging.Log logger = com.ebao.foundation.core.logging.Log.getLogger(LifeSurveyLetterAction.class);

	public static final String BEAN_DEFAULT = "/uw/lifeSurveyLetter";

	protected final static String SUBACTION__INIT = "clearAdd";

	protected final static String SUBACTION__SAVE = "save";

	protected final static String SUBACTION__BATCH = "batch";

	protected final static String SUBACTION_PREVIEW = "perview";

	protected final static String SUBACTION_ONLINE = "online";

	private static final String CHECK_FLAG = new String(new char[] { '\u25A0' });

	private static final String NOT_CHECK_FLAG = new String(new char[] { '\u25A1' });

	// 高齡關懷提問
	private static final Integer _THE_ELDER_Q_CODE = 9;

	// 保費來源為解約、貸款或保單借款
	private static final Integer _MONEY_SOURCE_Q_CODE = 10;

	// 代替抽樣電訪或確認保費來源為是否為解約、貸款或保單借款
	private static final Integer _CALLOUT_MONEY_SOURCE_Q_CODE = 11;

	//一般生調
	private static final String SURVEY_TYPE_1 = "1"; 
	//內勤生調
	private static final String SURVEY_TYPE_2 = "2"; 
	//視訊生調
	private static final String SURVEY_TYPE_3 = "3"; 

	// 高離關懷提問的問題版次
	private static final String _THE_ELDER_CARE_QUEST_VERSION_CODE = "DEFAULT";

	// 高離關懷提問的問題題數
	private static final int _THE_ELDER_CARE_QUEST_NUM = 4;

	/**
	 * Action進入點
	 */
	@Override
	public ActionForward process(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws GenericException {
		String subAction = null;
		try {
			// 底下是原本的邏輯，不更動，原本做的邏輯還是要存在
			LifeSurveyForm aForm = (LifeSurveyForm) form;
			//logger.debug("process().LifeSurveyForm >> \n" + aForm.toString());
			aForm.setTemplateId(UnbCst.SURVIVAL_SURVERY_LETTER_TEMPLATE_ID);
			aForm.setCsUw(Boolean.valueOf(request.getParameter("isCsUw")));
			List<DocumentFormFile> uploadFileList = nbLetterHelper.getUploadFileList(request, aForm);

			//生調頁不卡控EC件的業務員1姓名/電話是否NULL(20221026 IrisLiu, PCR-499927-BC456_保障型平台微型保險)
			request.setAttribute("isEc", String.valueOf(policyService.isEc(aForm.getPolicyId())));

			subAction = aForm.getSubAction();
			String forward = "uwLifeSurvey";
			if (StringUtils.isNullOrEmpty(subAction) || SUBACTION__INIT.equals(subAction)) {
				this.queryInit(aForm, request);
				forward = "uwLifeSurvey";
				aForm.setRetMsg(null);
			} else {
				if (StringUtils.isNullOrEmpty(aForm.getSelectedObjectCertiCodes())) {
					request.setAttribute("error", "無選擇交查對象! ");
					return mapping.findForward("uwLifeSurvey");
				}

				// 取uw_survey_let_id:survey_role
				List<String> objectCertiCodes = Arrays.asList(aForm.getSelectedObjectCertiCodes().split(","));
				if (objectCertiCodes.size() == 0) {
					request.setAttribute("error", "無選擇交查對象! ");
					return mapping.findForward("uwLifeSurvey");
				}

				PolicyVO policyVO = policyService.load(aForm.getPolicyId());

				// 檢核 3.5 業務規則:「高齡投保件(核保高齡關懷提問)」(Y / N)
				boolean hasMatchElderCareStartDate = nbSpecialRuleService.isMatchElderCareStartDate(policyVO.getPolicyId());
				boolean hasElderScaleCoverage = paValidatorService.hasElderScaleCoverage(policyVO);
				boolean hasElderCareScaleRole = paValidatorService.hasElderCareScaleRole(policyVO);
				if (hasMatchElderCareStartDate && hasElderScaleCoverage && hasElderCareScaleRole) {
					aForm.setIsMeetElderCare(CodeCst.YES_NO__YES);
				}

				// 2022.12.16 PCR 500628 高齡投保件核保評估功能需求 規則同「高齡銷售錄音會辦」頁面的「會辦對象」
				Date applyDate = policyVO.getApplyDate();
				List<ElderAudioObject> elderAudioObjects = elderAudioHelper.getElderAudioObjects(policyVO.getPolicyId(), applyDate);
				Map<String, ElderAudioObject> elderObjsMap = new HashMap<String, ElderAudioObject>();
				for (ElderAudioObject elderAudioObject : elderAudioObjects) {
					elderObjsMap.put(elderAudioObject.getCertiCode(), elderAudioObject);
				} // for

				List<InsuredVO> insuredVOs = policyVO.gitSortInsuredList();
				Map<String, InsuredVO> insuredMap = new HashMap<String, InsuredVO>();
				for (InsuredVO vo : insuredVOs) {
					insuredMap.put(vo.getCertiCode(), vo);
				} // for

				List<String> selectedNameList = new ArrayList<String>();
				List<ElderAudioObject> selectedObjsList = new ArrayList<ElderAudioObject>(); // 由於後面會用到羅馬姓名，因此將 UnbRoleVO 物件的 policyRoleDesc 參數暫時用來存羅馬姓名

				// 改為使用身份證號
				for (String objectCertiCode : objectCertiCodes) {
					ElderAudioObject tempObj = elderObjsMap.get(objectCertiCode);
					selectedNameList.add(tempObj.getName());

					// 由於後面會用到羅馬姓名，因此將 UnbRoleVO 物件的 policyRoleDesc 參數暫時用來存羅馬姓名，此處先清成 empty string
					tempObj.setPoliicyRoleDesc(org.apache.commons.lang3.StringUtils.EMPTY);
					if (UnbRoleType.POLICY_HOLDER.getRoleType().equals(tempObj.getFirstUnbRole())) {
						PolicyHolderVO holderVO = policyVO.getPolicyHolder();
						// 當 pholderRomanName 有值時，則 insuredName 顯示「詳附件」。
						// 由於後面會用到羅馬姓名，因此將 UnbRoleVO 物件的 policyRoleDesc 參數暫時用來存羅馬姓名
						if (!StringUtils.isNullOrEmpty(holderVO.getRomanName())) {
							tempObj.setName("詳附件");
							tempObj.setPoliicyRoleDesc(holderVO.getRomanName());
						}
					} else if (UnbRoleType.INSURED.getRoleType().equals(tempObj.getFirstUnbRole())) {
						// 2020/7/3 Charlotte 因 user 需更改顯示資料，故當 insuredRomanName 有值時，則 insuredName 顯示「詳附件」。
						// 由於後面會用到羅馬姓名，因此將 UnbRoleVO 物件的 policyRoleDesc 參數暫時用來存羅馬姓名
						InsuredVO insured = insuredMap.get(tempObj.getCertiCode());
						if (null != insured) {
							if (!StringUtils.isNullOrEmpty(insured.getRomanName())) {
								tempObj.setName("詳附件");
								tempObj.setPoliicyRoleDesc(insured.getRomanName());
							}
						}
					} // 處理 要/被保險人的羅馬姓名

					selectedObjsList.add(tempObj);
				} // for

				// 取交查對象姓名-多人時，以「/」區隔
				String clientName = org.apache.commons.lang3.StringUtils.join(selectedNameList.toArray(), "/");

				FmtUnb0100ExtensionVO extensionVO = (FmtUnb0100ExtensionVO) this.getExtension(aForm, request, policyVO, selectedObjsList);

				Long policyId = aForm.getPolicyId();
				String policyCode = extensionVO.getPolicyCode();
				Long templateId = aForm.getTemplateId();
				Long changeId = aForm.getChangeId();

				WSLetterUnit unit = nbLetterHelper.generalLetterUnit(response, policyId, templateId, uploadFileList, aForm.getApplyImageList(), extensionVO);

				unit.setIndex(getIndexVO(policyVO, templateId));
				//1:email
				unit.setSendingMethod(WSLetterSendingMethod.EMAIL.getCode());

				//交查人員設定
				unit.getEmailReceivers().clear();

				// 交查員寄送資料
				List<UwSurveyAgentSentVO> agentSentLists = new ArrayList<UwSurveyAgentSentVO>();
				UwSurveyAgentSentVO agentSentVO = new UwSurveyAgentSentVO();
				// 2018/01/18 Modify : YCSu
				// [artf410579](Internal Issue 222505)[UAT2][eBao]保單0052568410生調在歐米加查無資料
				// 若沒有：(1). 郵遞區號, (2). E-Mail 設定，出Exception ，並且 send Email 的動作都不做。(Simon)
				boolean isExceptionInsureAddressPostCode = false;
				boolean isExceptionPCDAgentMailBox = false;

				//一般生調/視訊生調:
				if (SURVEY_TYPE_1.equals(aForm.getSurveyType()) || SURVEY_TYPE_3.equals(aForm.getSurveyType())) {
					if (!StringUtils.isNullOrEmpty(aForm.getRealSurveyZipCode())) {
						// 依郵遞區號找PCD 交查員
						Map<String,String> mailData = agentService.findPCDAgentEmailByPostCode(aForm.getRealSurveyZipCode());

						String mail = mailData.get("mail_box");
						String name = mailData.get("agent_name");

						if(!StringUtils.isNullOrEmpty(mail)) {
							if(StringUtils.isNullOrEmpty(name)) {
								name = mail;
							}
							//收件者類型: 1-正本  2-附本
							String receiveType = "1";
							agentSentVO.setUwSurveyLetId(aForm.getListId());
							agentSentVO.setReceiveType(receiveType);
							agentSentVO.setEmail(mail);
							agentSentVO.setName(name);
							
							agentSentLists.add(agentSentVO);
							
							String encryptCode = Para.getParaValue(UnbCst.UNB_LETTER_ENCRYPT_CODE);
							unit.getEmailReceivers().add(new WSLetterEmailInfo(mail, encryptCode, name));
						}else {
							isExceptionPCDAgentMailBox = true;
						}
					}else {
						isExceptionInsureAddressPostCode = true;
					}
				}

				//內勤生調 抓畫面
				if ( SURVEY_TYPE_2.equals(aForm.getSurveyType())) {
					isExceptionPCDAgentMailBox = false;
					isExceptionInsureAddressPostCode = false;
					String[] applyListAry = aForm.getApplyStaffList();

					if (applyListAry != null && applyListAry.length > 0) {
						for(String array : applyListAry) {
							String[] arr = array.split("-");

							if (arr.length == 2) {
								UwSurveyAgentSentVO agentSentType2VO = new UwSurveyAgentSentVO();

								String name = arr[0];
								String mail = arr[1];
								if(!StringUtils.isNullOrEmpty(mail)) {
									if(StringUtils.isNullOrEmpty(name)) {
										name = mail;
									}
									//收件者類型: 1-正本  2-附本
									String receiveType = "1";
									agentSentType2VO.setUwSurveyLetId(aForm.getListId());
									agentSentType2VO.setReceiveType(receiveType);
									agentSentType2VO.setEmail(mail);
									agentSentType2VO.setName(name);

									agentSentLists.add(agentSentType2VO);

									String encryptCode = Para.getParaValue(UnbCst.UNB_LETTER_ENCRYPT_CODE);
									unit.getEmailReceivers().add(new WSLetterEmailInfo(mail, encryptCode, name));
								}
							}
						} // for
					}
				}

				//所有生調結果皆需寄給核保員
				EmployeeVO empVO = employeeCI.getEmployeeByEmpId(AppContext.getCurrentUser().getUserId());
				UwSurveyAgentSentVO agentSentEmpVO = new UwSurveyAgentSentVO();
				String name = empVO.getName();
				String mail = empVO.getEmail();

				if(!StringUtils.isNullOrEmpty(mail)) {
					if(StringUtils.isNullOrEmpty(name)) {
						name = mail;
					}
				}
				//交查資料寄送附本給核保員(收件者類型: 1-正本  2-附本)
				String receiveType = "2";
				agentSentEmpVO.setUwSurveyLetId(aForm.getListId());
				agentSentEmpVO.setReceiveType(receiveType);
				agentSentEmpVO.setEmail(mail);
				agentSentEmpVO.setName(name);

				agentSentLists.add(agentSentEmpVO);

				/** 再保申請書、生調表 E-MAIL發放需加密*/
				// 2018/04/03 Add : YCSu
				// P35-再保申請書、生調表 E-MAIL發放需加密
				String encryptCode = Para.getParaValue(UnbCst.UNB_LETTER_ENCRYPT_CODE);
				unit.getEmailReceivers().add(new WSLetterEmailInfo(empVO.getEmail(), encryptCode, empVO.getRealName()));

				if (SUBACTION_PREVIEW.equals(subAction)) {
					super.previewLetter(request, response, templateId, unit);
					return mapping.findForward(FORWARD_WINDOW_CLOSE);
				} else {
					UserTransaction trans = Trans.getUserTransaction();
					try {
						trans.begin();
				        AddressVO addressVO = new AddressVO();
				        Long addressId = null;
	
				        addressVO.setPostCode(aForm.getRealSurveyZipCode());
				        addressVO.setAddress1(aForm.getRealSurveyAddress());
				        addressVO.setAddressFormat("1");
				        AddressVO address = addressService.saveOrUpdateAddress(addressVO);
				        addressId = address.getAddressId();

						// 暫存
						if (SUBACTION__SAVE.equals(subAction)) {
							UwSurveyLetterVO letter = getUwSurveyLetterVO(aForm, request, "0", selectedObjsList, agentSentLists, addressId);
							uwSurveyLetterService.save(letter, true);
							this.queryInit(aForm, request);
						}
						trans.commit();
					} catch (Exception e) {
						Log.error(LifeSurveyLetterAction.class, e);
						TransUtils.rollback(trans);
						throw e;
					}

					if (isExceptionInsureAddressPostCode == true) {
						// MSG_1262915: 此被保險人無郵遞區號
						String errorMsg = StringResource.getStringData("MSG_1262915", request);

						aForm.setRetMsg(errorMsg);
						request.setAttribute("error", errorMsg);
						return mapping.findForward("previewError");
					}

					if (isExceptionPCDAgentMailBox == true) {
						Map<String,String>  params = new HashMap<String,String>();

						// MSG_1262916 : 郵遞區號{postCode} 查無PCD服務代表Email
						params.put("postCode", aForm.getRealSurveyZipCode());
						String errorMsg = StringResource.getStringData("MSG_1262916", request, params);

						aForm.setRetMsg(errorMsg);
						request.setAttribute("error", errorMsg);
						return mapping.findForward("previewError");
					}

					try {
						trans.begin();
				        AddressVO addressVO = new AddressVO();
				        Long addressId = null;

				        addressVO.setPostCode(aForm.getRealSurveyZipCode());
				        addressVO.setAddress1(aForm.getRealSurveyAddress());
				        addressVO.setAddressFormat("1");
				        AddressVO address = addressService.saveOrUpdateAddress(addressVO);
				        addressId = address.getAddressId();

				        // 發放照會用
						Map<DocumentPropsKey, Object> props = new HashMap<DocumentPropsKey, Object>();
						if (null == selectedObjsList || selectedObjsList.size() == 0) {
							// 取要保人
							props.put(DocumentPropsKey.insuredCertiCode, policyVO.getPolicyHolder().getCertiCode());
							props.put(DocumentPropsKey.insuredName, policyVO.getPolicyHolder().getName());
						}else {
							props.put(DocumentPropsKey.insuredCertiCode, selectedObjsList.get(0).getCertiCode());
							props.put(DocumentPropsKey.insuredName, selectedObjsList.get(0).getName());
						}
						// 確認
						if (SUBACTION__BATCH.equals(subAction)) {
							UwSurveyLetterVO letter = getUwSurveyLetterVO(aForm, request, "2", selectedObjsList, agentSentLists, addressId);

							/* 核保問題MSG_ID */
							ProposalRuleResultVO ruleResultVO = this.saveProposalRuleResult(letter, clientName, policyId, changeId);
							letter.setMsgId(ruleResultVO.getListId());
							saveUwCheckList(aForm, clientName);
							/* batch發放照會*/
							Long documentId = commonLetterService.sendBatchLetter(policyId, templateId, unit, policyId, policyCode, props);
							letter.setDocumentId(documentId);
							uwSurveyLetterService.save(letter, true);

							// PCR 477771 視訊/遠距生調有勾選「高齡關懷提問」，要保書填寫日大於等於2022.10.01 
							// 符合3.5 業務規則:「高齡投保件(核保高齡關懷提問)」，則寫入1筆高齡投保核保評估主表 T_UW_ELDER_CARE等
							if (CodeCst.YES_NO__YES.equals(extensionVO.getHasElderQuestion()) && CodeCst.YES_NO__YES.equals(aForm.getIsMeetElderCare())) {
								this.saveUwElderCare(letter, extensionVO);
							}
							aForm.setRetMsg(BATCH_PRINT__SUCCESS);
						// online列印
						} else if (SUBACTION_ONLINE.equals(subAction)) {
							UwSurveyLetterVO letter = getUwSurveyLetterVO(aForm, request, "2", selectedObjsList, agentSentLists, addressId);
							/* 核保問題MSG_ID */
							ProposalRuleResultVO ruleResultVO = this.saveProposalRuleResult(letter, clientName, policyId, changeId);
							letter.setMsgId(ruleResultVO.getListId());
							saveUwCheckList(aForm, clientName);
							/* Online 發放照會*/
							Long documentId = commonLetterService.sendLetter(policyId, templateId, unit, policyId, policyCode, props);
							letter.setDocumentId(documentId);
							uwSurveyLetterService.save(letter, true);
							
							// PCR 477771 視訊/遠距生調有勾選「高齡關懷提問」，要保書填寫日大於等於2022.10.01 
							// 符合3.5 業務規則:「高齡投保件(核保高齡關懷提問)」，則寫入1筆高齡投保核保評估主表 T_UW_ELDER_CARE等
							if (CodeCst.YES_NO__YES.equals(extensionVO.getHasElderQuestion()) && CodeCst.YES_NO__YES.equals(aForm.getIsMeetElderCare())) {
								this.saveUwElderCare(letter, extensionVO);
							}
							aForm.setRetMsg(ONLINE_PRINT__SUCCESS);
						}
						trans.commit();
					} catch (Exception e) {
						Log.error(LifeSurveyLetterAction.class, e);
						TransUtils.rollback(trans);
						throw e;
					}
					this.queryInit(aForm, request);
					logger.error("process().queryInit >> \n" + aForm.toString());
				}

				forward = "uwLifeSurvey";
			}
			return mapping.findForward(forward);
		} catch (Exception e) {
			if (SUBACTION_PREVIEW.equals(subAction)) {
				com.ebao.pub.util.Log.error(this.getClass(), e.getMessage());
				request.setAttribute("error", "無法產生 PDF。");
				return mapping.findForward("previewError");
			}
			throw ExceptionFactory.parse(e);
		}
	}

	private FmtUnb0100IndexVO getIndexVO(PolicyVO policyVO, Long templateId) {
		FmtUnb0100IndexVO index = new FmtUnb0100IndexVO();
		this.nbNotification.setIndex(index, policyVO, templateId);
		return index;
	}

	/**
	 * <p>Description : 生承T_UW_SURVEY_LETTER跟T_UW_SURVEY_LIST</p>
	 * <p>Created By : Sunny Wu</p>
	 * <p>Create Time : Jun 25, 2016</p>
	 * @param aForm
	 * @param rMap
	 * @return
	 */
	private UwSurveyLetterVO getUwSurveyLetterVO(LifeSurveyForm aForm, HttpServletRequest request, String status, List<ElderAudioObject> selectedObjsList, List<UwSurveyAgentSentVO> agentSentLists, Long addressId) {
		UwSurveyLetterVO letter = new UwSurveyLetterVO();
		letter.setListId(aForm.getListId());
		letter.setUnderwriteId(aForm.getUnderwriteId());
		letter.setSourceType(selectedObjsList.get(0).getFirstUnbRole());
		letter.setSourceId(aForm.getPolicyId());

		letter.setStatus(status);
		letter.setAgentNameFirst(aForm.getAgent1Name());
		letter.setAgentMobileNumberFirst(aForm.getAgent1Number());
		letter.setAgentNameSecond(aForm.getAgent2Name());
		letter.setAgentMobileNumberSecond(aForm.getAgent2Number());
		letter.setAgentNameThird(aForm.getAgent3Name());
		letter.setAgentMobileNumberThird(aForm.getAgent3Number());
		
		letter.setSurveyType(aForm.getSurveyType());

		letter.setSurveyAddressId(addressId);
		letter.setSurveyZipcode(aForm.getRealSurveyZipCode());
		letter.setSurveyComment(aForm.getSurveyComment());
		letter.setPolicyId(aForm.getPolicyId());
		
		//status="0":暫存 , "2":確認(SUBACTION__BATCH)及online列印(SUBACTION_ONLINE)時皆需重新存入交查員寄送資料
		letter.setUwSurveyAgentSents(agentSentLists);
		
		List<UwSurveyListVO> list = letter.getUwSurveyLists();
		List<UwSurveyObjectVO> object = letter.getUwSurveyObjects();
		
		// t_uw_survey_list
		Integer[] itemAry = aForm.getLifeSurveyitem();
		if (itemAry != null && itemAry.length > 0) {
			for (Integer item : itemAry) {
				UwSurveyListVO surveyVO = new UwSurveyListVO();
				surveyVO.setUwSurveyLetId(aForm.getListId());
				
				surveyVO.setLifeSurveyItemId(new Long(item));
				UwLifeSurveyItemVO vo = uwLifeSurveyItemService.findUwLifeSurveyItemByLifeSurveyCode(item);
				if (CodeCst.YES_NO__YES.equals(vo.getRemarkIndi())) {
					String desc = request.getParameter("lifeSurveyDesc_" + item);
					surveyVO.setLifeSurveyDesc(desc);
				} else {
					surveyVO.setLifeSurveyDesc(vo.getLifeSurveyDesc());
				}
				list.add(surveyVO);
			}
		}

		// t_uw_survey_object
		// 2022.12.16 PCR 500628 高齡投保件核保評估功能需求 規則同「高齡銷售錄音會辦」頁面的「會辦對象」
		for (ElderAudioObject tempObj : selectedObjsList) {
			UwSurveyObjectVO surveyObjectVO = new UwSurveyObjectVO();
			surveyObjectVO.setUwSurveyLetId(aForm.getListId());
			surveyObjectVO.setSurveyObjectCertiCode(tempObj.getCertiCode());
			surveyObjectVO.setSurveyRole(tempObj.getFirstUnbRole());
			object.add(surveyObjectVO);
		}
		return letter;
	}

	/**
	 * <p>Description : 新增會辦狀態列</p>
	 * <p>Created By : Sunny Wu</p>
	 * <p>Create Time : Jun 25, 2016</p>
	 * @param aForm
	 */
	private void saveUwCheckList(LifeSurveyForm aForm, String clientName) {
		UwCheckListStatusVO uwCheckListStatusVO = new UwCheckListStatusVO();
		uwCheckListStatusVO.setPolicyId(aForm.getPolicyId());
		uwCheckListStatusVO.setUnderwriteId(aForm.getUnderwriteId());
		uwCheckListStatusVO.setApplyDate(AppContext.getCurrentUserLocalTime());
		uwCheckListStatusVO.setListType(CodeCst.UW_CHECK_LIST_STATUS_2);
		uwCheckListStatusVO.setClientName(clientName);
		uwCheckListStatusVO.setConfirmComplete(CodeCst.YES_NO__NO);

		/* 會辦狀態 */
		uwCheckListStatusService.save(uwCheckListStatusVO);
	}

	/**
	 * <p>Description : 新增一筆生調交查核保問題</p>
	 * <p>Created By : Alex Cheng</p>
	 * <p>Create Time : Apr 19, 2016</p>
	 * @param letterVO
	 * @param clientName
	 * @param policyId
	 * @return
	 */
	private ProposalRuleResultVO saveProposalRuleResult(UwSurveyLetterVO letterVO, String clientName, Long policyId, Long changeId) {
		ProposalRuleMsgVO msgVO = null;
		ProposalRuleResultVO vo = null;
		if (letterVO != null) {
			DetailRegPolicyVO detailRegPolicyVO = policyService.getDetailRegPolicyVO(policyId);
			PolicyVO policyVO = detailRegPolicyVO.getPolicyVO();
			vo = new ProposalRuleResultVO();
			msgVO = proposalRuleMsgService.findProposalRuleMsg(CodeCst.PROPOSAL_RULE_MSG_LIFE_SURVEY, AppContext.getCurrentUserLocalTime());

			if (msgVO != null) {
				boolean isOpqV2 = paValidatorService.isOpqV2(policyId);
				vo.setPolicyId(policyVO.getPolicyId());
				vo.setChangeId(changeId);
				vo.setRuleType(msgVO.getRuleType());
				vo.setRuleLetterCode(msgVO.getRuleCode());
				vo.setRuleStatus(CodeCst.PROPOSAL_RULE_STATUS__PENDING);
				vo.setRuleName(msgVO.getRuleName());

				vo.setPrintable(msgVO.getPrintable());

				String violatedDesc = NBUtils.getViolatedDescByProposalRuleMsgVO(msgVO, isOpqV2);
				String letterContent = NBUtils.getLetterContentByProposalRuleMsgVO(msgVO, isOpqV2);

				// 2022.12.16 PCR 500628 高齡投保件核保評估功能需求 規則同「高齡銷售錄音會辦」頁面的「會辦對象」
				violatedDesc = violatedDesc.replace("{1}", clientName);
				letterContent = letterContent.replace("{1}", clientName);

				vo.setViolatedDesc(violatedDesc);
				vo.setLetterContent(letterContent);

				vo = proposalRuleResultService.save(vo);
				vo = proposalRuleResultService.load(vo.getListId());
				letterVO.setMsgId(vo.getListId());
			}
		}
		return vo;
	}

	/**
	 * <p>Description : 組合生調交查頁面所需資料</p>
	 * <p>Created By : Alex Cheng</p>
	 * <p>Create Time : Apr 16, 2016</p>
	 * @param aForm
	 * @param request
	 */
	private void queryInit(LifeSurveyForm aForm, HttpServletRequest request) throws Exception {
		Long policyId = aForm.getPolicyId();
		Long underwriteId = aForm.getUnderwriteId();
		if (policyId == null && underwriteId == null) {
			throw new RuntimeException("NO policyId & underwriteId");
		}

		if (policyId == null) {
			UwPolicyVO uwPolicyVO = uwPolicyService.findUwPolicy(underwriteId);
			policyId = uwPolicyVO.getPolicyId();
		}

		PolicyVO policyVO = policyService.load(policyId);

		// 若此項有勾選，檢核所有勾選的交查對象皆須符合高齡投保(核保高齡關懷提問)規則(詳3.5業務規則)，
		// 錯誤顯示「{保單角色-姓名、保單角色-姓名...}非高齡客戶交查原因不可選擇高齡關懷提問」訊息，不可勾選。
		// {保單角色-姓名、保單角色-姓名...}為變數，若有多個中間「、」隔開。
		// 檢核 3.5 業務規則:「高齡投保件(核保高齡關懷提問)」(Y / N)
		boolean hasMatchElderCareStartDate = nbSpecialRuleService.isMatchElderCareStartDate(policyVO.getPolicyId());
		boolean hasElderScaleCoverage = paValidatorService.hasElderScaleCoverage(policyVO);
		boolean hasElderCareScaleRole = paValidatorService.hasElderCareScaleRole(policyVO);
		if (hasMatchElderCareStartDate && hasElderScaleCoverage && hasElderCareScaleRole) {
			aForm.setIsMeetElderCare(CodeCst.YES_NO__YES);
		}

		if (policyVO != null) {
			// 2022.12.16 PCR 500628 高齡投保件核保評估功能需求 規則同「高齡銷售錄音會辦」頁面的「會辦對象」
			// 高齡銷售錄音會辦對象順序：被保險人 要保人 首期授權人 續期授權人 實際繳交保費人 法定代理人 授權人之法定代理人
			// 保單角色的來源和高齡錄音會辦一樣，然後手工加工成原本生調的順序和欄位
			// 生調交查對象順序：要保人 被保險人 首期授權人 續期授權人 實際繳交保費人 法定代理人 授權人之法定代理人
			List<InsuredVO> insuredVOs = policyVO.gitSortInsuredList();
			Map<String, InsuredVO> insuredMap = new HashMap<String, InsuredVO>();
			if (insuredVOs != null) {
				for (InsuredVO vo : insuredVOs) {
					insuredMap.put(vo.getCertiCode(), vo);
				} // for
			} // if

			PolicyHolderVO holderVO = policyVO.getPolicyHolder();

			List<Map<String, Object>> surveyObjsList = new ArrayList<Map<String, Object>>();
			Map<String, Object> pholdMap = null;

			Date applyDate = policyVO.getApplyDate();
			List<ElderAudioObject> elderAudioObjects = elderAudioHelper.getElderAudioObjects(policyId, applyDate);
			for (ElderAudioObject elderAudioObject : elderAudioObjects) {
				if (UnbRoleType.POLICY_HOLDER.getRoleType().equals(elderAudioObject.getFirstUnbRole())) {
					pholdMap = new HashMap<String, Object>();

					pholdMap.put("policyRole", elderAudioObject.getFirstUnbRole());
					pholdMap.put("firstUnbRoleDesc", elderAudioObject.getFirstUnbRoleDesc());
					pholdMap.put("name", elderAudioObject.getName());
					pholdMap.put("certiCode", elderAudioObject.getCertiCode());
					pholdMap.put("birthDate", elderAudioObject.getBirthdate());
					pholdMap.put("entryAge", elderAudioObject.getAgeInsurance());
					pholdMap.put("gender", holderVO.getGender());

					AddressVO addrVo = holderVO.getAddress();
					if (null == addrVo) {
						addrVo = new AddressVO();
					}
					pholdMap.put("address", addrVo);

					// 先評估投保年齡是否為長者：同時考量高齡關懷提問、高齡投保評估量表
					// 因生日可能為空，只標註有生日且非長者
					Date birth = elderAudioObject.getBirthdate();
					if (null != birth &&
							(CodeCst.YES_NO__NO.equals(aForm.getIsMeetElderCare())
									|| (false == ValidatorUtils.isMathchElderAgeDefAge(birth, applyDate)
											|| false == ValidatorUtils.isMathchElderCaseUwAge(birth, applyDate)))) {
						pholdMap.put("isElder", CodeCst.YES_NO__NO);
					} else {
						pholdMap.put("isElder", CodeCst.YES_NO__NA); // 可能沒有生日，也可能是長者
					}
				} else {
					Map<String, Object> objectMap = new HashMap<String, Object>();
					objectMap.put("policyRole", elderAudioObject.getFirstUnbRole());
					objectMap.put("firstUnbRoleDesc", elderAudioObject.getFirstUnbRoleDesc());
					objectMap.put("name", elderAudioObject.getName());
					objectMap.put("certiCode", elderAudioObject.getCertiCode());
					objectMap.put("birthDate", elderAudioObject.getBirthdate());
					objectMap.put("entryAge", elderAudioObject.getAgeInsurance());

					if (UnbRoleType.INSURED.getRoleType().equals(elderAudioObject.getFirstUnbRole())) {
						// 被保險人
						InsuredVO insured = insuredMap.get(elderAudioObject.getCertiCode());
						if (null != insured) {
							objectMap.put("gender", insured.getGender());

							AddressVO addrVo = insured.getAddress();
							if (null == addrVo) {
								addrVo = new AddressVO();
							}
							objectMap.put("address", addrVo);
						}
					} else {
						// 其他角色沒有地址，直接塞空物件
						objectMap.put("address", new AddressVO());
					}

					// 先評估投保年齡是否為長者：同時考量高齡關懷提問、高齡投保評估量表
					// 因生日可能為空，只標註有生日且非長者
					Date birth = elderAudioObject.getBirthdate();
					if (null != birth &&
							(CodeCst.YES_NO__NO.equals(aForm.getIsMeetElderCare())
									|| (false == ValidatorUtils.isMathchElderAgeDefAge(birth, applyDate)
											|| false == ValidatorUtils.isMathchElderCaseUwAge(birth, applyDate)))) {
						objectMap.put("isElder", CodeCst.YES_NO__NO);
					} else {
						objectMap.put("isElder", CodeCst.YES_NO__NA); // 可能沒有生日，也可能是長者
					}

					surveyObjsList.add(objectMap);
				}
			} // for (ElderAudioObject elderAudioObject : elderAudioObjects)

			if (null != pholdMap) {
				// 要保人 <> 被保險人, 則將要保人放交查對象之第一個
				surveyObjsList.add(0, pholdMap);
			}

			aForm.setInsuredMaps(surveyObjsList);

			// 該角色若無地址，顯示保單要保人住所地址、郵遞區號於「生調地址」、「郵遞區號」
			aForm.setPhAddress(holderVO.getAddress().getAddress1());
			aForm.setPhZipCode(holderVO.getAddress().getPostCode());

			/* 生調交查選項列表 */
			aForm.setLifeSurveyItems(uwLifeSurveyItemService.getUwLifeSurveyItems());

			/* 業務員資料 */
			List<AgentChlVO> agentChls = new ArrayList<AgentChlVO>();
			CoverageVO coverage = policyVO.gitMasterCoverage();
			List<CoverageAgentVO> agentVOs = coverage.getCoverageAgents();

			if (agentVOs != null && agentVOs.size() > 0) {
				List<AgentNotifyVO> agentNotifyVoList = agentNotifyService.findByPolicyId(policyId);

				for (CoverageAgentVO agentVO : agentVOs) {
					/**agentId可能為空**/
					AgentChlVO agentChlVO = (agentVO.getAgentId() != null) ? agentService.findAgentByAgentId(agentVO.getAgentId()) : new AgentChlVO();
					agentChlVO.setAgentName(agentVO.getAgentName());

					//業報書有輸入業務員電話的話以此為優先顯示
					if( agentNotifyVoList != null && agentNotifyVoList.size() > 0) {
						String agentMobile = agentNotifyVoList.get(0).getAgentMobile();
						String agentMobile2 = agentNotifyVoList.get(0).getAgentMobile2();
						String agentMobile3 = agentNotifyVoList.get(0).getAgentMobile3();
						
						if( agentVO.getOrderId() == 1 && !"".equals(agentMobile) && agentMobile != null) {
							agentChlVO.setMobile(agentMobile);
						}else if( agentVO.getOrderId() == 2 && !"".equals(agentMobile2) && agentMobile2 != null) {
							agentChlVO.setMobile(agentMobile2);
						}else if( agentVO.getOrderId() == 3 && !"".equals(agentMobile3) && agentMobile3 != null) {
							agentChlVO.setMobile(agentMobile3);
						}
					}
					agentChls.add(agentChlVO);
				}
			} // if (agentVOs != null && agentVOs.size() > 0)
			aForm.setAgents(agentChls);
		} // if (policyVO != null)
	}

	@Override
	protected NbLetterExtensionVO getExtension(ActionForm actionForm) {
		return null;
	}

	protected NbLetterExtensionVO getExtension(ActionForm actionForm, ServletRequest request, PolicyVO policyVO, List<ElderAudioObject> selectedObjsList) {
		LifeSurveyForm aForm = (LifeSurveyForm) actionForm;

		FmtUnb0100ExtensionVO extensionVO = new FmtUnb0100ExtensionVO();
		extensionVO.setPolicyId(policyVO.getPolicyId().toString());
		extensionVO.setPolicyCode(policyVO.getPolicyNumber());

		/** surveyName:交查對象 **/
		List<FmtUnb0100ObjectVO> fmtUnb0100ObjectVOs = new ArrayList<FmtUnb0100ObjectVO>();

		for (ElderAudioObject tempObj : selectedObjsList) {
			if (UnbRoleType.POLICY_HOLDER.getRoleType().equals(tempObj.getFirstUnbRole())
					|| UnbRoleType.INSURED.getRoleType().equals(tempObj.getFirstUnbRole())) { // 處理 要/被保險人的羅馬姓名
				extensionVO.setCertiCode(tempObj.getCertiCode());
				extensionVO.setInsuredName(tempObj.getName());
				extensionVO.setInsuredRomanName(tempObj.getPoliicyRoleDesc());
			}

			FmtUnb0100ObjectVO objectVO = new FmtUnb0100ObjectVO();
			objectVO.setSurveyName(tempObj.getName());
			fmtUnb0100ObjectVOs.add(objectVO);
		} // for

		// 角色變多了，就不一定會選要 / 被保險人
		if (StringUtils.isNullOrEmpty(extensionVO.getCertiCode())) {
			ElderAudioObject tempObj = selectedObjsList.get(0);
			extensionVO.setCertiCode(tempObj.getCertiCode());
			extensionVO.setInsuredName(tempObj.getName());
		}

		extensionVO.setSurveyObjectList(fmtUnb0100ObjectVOs);

		/**交查單位**/
		Long userId = AppContext.getCurrentUser().getUserId();
		DeptVO deptVO = deptService.getDeptByUserId(userId);

		if (deptVO != null) {
			DeptVO companyDeptVO = deptService.getDeptByIdAndGrade(deptVO.getDeptId(), 3);
			extensionVO.setEmployeeDept(companyDeptVO == null ? deptVO.getDeptName() : companyDeptVO.getDeptName());
		}
		EmployeeVO employeeVO = employeeCI.getEmployeeByEmpId(userId);
		/**交查合保員.分機**/
		if (employeeVO != null) {
	        extensionVO.setEmployeeFax(employeeVO.getFax());
	        extensionVO.setEmployeeTel(employeeVO.getOfficeTel());
	        extensionVO.setEmployeeExt(employeeVO.getOfficeTelExt());
	        extensionVO.setEmployeeDept(employeeVO.getDeptName());
	        extensionVO.setEmployeeName(employeeVO.getRealName());
	        extensionVO.setEmployeeEmail(employeeVO.getEmail());
		}
		/**交查日期**/
		extensionVO.setSurveyDate(AppContext.getCurrentUserLocalTime());
		/**招攬擔位*/
		ChannelOrgVO channelOrgVO = channelOrgservice.getChannelOrg(policyVO.getChannelOrgId());
		if (channelOrgVO != null) {
			extensionVO.setDeptName(channelOrgVO.getChannelName());
		}
		/**業務員姓名.電話**/
		List<WSItemString> agentNames = new ArrayList<WSItemString>();
		if (!(StringUtils.isNullOrEmpty(aForm.getAgent1Name()) && StringUtils.isNullOrEmpty(aForm.getAgent1Number()))) {
			agentNames.add(new WSItemString(null, aForm.getAgent1Name() + "/" + aForm.getAgent1Number()));
		}
		if (!(StringUtils.isNullOrEmpty(aForm.getAgent2Name()) && StringUtils.isNullOrEmpty(aForm.getAgent2Number()))) {
			agentNames.add(new WSItemString(null, aForm.getAgent2Name() + "/" + aForm.getAgent2Number()));
		}
		if (!(StringUtils.isNullOrEmpty(aForm.getAgent3Name()) && StringUtils.isNullOrEmpty(aForm.getAgent3Number()))) {
			agentNames.add(new WSItemString(null, aForm.getAgent3Name() + "/" + aForm.getAgent3Number()));
		}

		extensionVO.setAgentNames(agentNames);
		/**組成交查原因*/
		List<WSItemString> lifeSurveyItems = new ArrayList<WSItemString>();
		List<Integer> items = new ArrayList<Integer>();
		for (Integer item : aForm.getLifeSurveyitem()) {
			items.add(item);
		}
		List<UwLifeSurveyItemVO> uwLifeSurveyItems = uwLifeSurveyItemService.getUwLifeSurveyItems();

		// PCR 474367 視訊生調且有勾選「高齡關懷提問」
		extensionVO.setHasElderQuestion(CodeCst.YES_NO__NO);
		// PCR 474367 視訊生調且有勾選「來源是否為解約、貸款或保單借款」
		extensionVO.setHasMoneySourceQuestion(CodeCst.YES_NO__NO);
		// 新契約客戶訪視表(FMT_UNB_0100生存調查表)第三頁區塊的顯示順序：
		// 高齡關懷提問相關附表
		// 來源是否為解約、貸款或保單借款相關附表
		//【附表】補充交查事項

		String dotStr = ".";
		for (UwLifeSurveyItemVO vo : uwLifeSurveyItems) {
			if (items.contains(vo.getLifeSurveyCode())) {
				/**畫面有勾選*/
				lifeSurveyItems.add(new WSItemString(null, CHECK_FLAG + vo.getLifeSurveyDesc()
						+ (CodeCst.YES_NO__YES.equals(vo.getRemarkIndi()) ? request.getParameter("lifeSurveyDesc_" + vo.getLifeSurveyCode()) : "")));

				// PCR 474367 視訊生調且有勾選「高齡關懷提問」
				if (_THE_ELDER_Q_CODE.equals(vo.getLifeSurveyCode())) {
					extensionVO.setHasElderQuestion(CodeCst.YES_NO__YES);

					// 設定高齡關懷問題
					List<WSItemString> elderQuestStrs = new ArrayList<WSItemString>();
					List<NbQuestSettingVO> questList = questSettingService.findByVersionCodeAndCategory(_THE_ELDER_CARE_QUEST_VERSION_CODE, Cst.QUEST_SETTING_CATEGORY_LIFE_SURVEY);
					int randomIndex;
					// 題庫可能少於設定的提問數，這樣就要全問
					int numOfQ = questList.size() > _THE_ELDER_CARE_QUEST_NUM ? _THE_ELDER_CARE_QUEST_NUM : questList.size();
					for (int i = 0; i < numOfQ;) {
						randomIndex = RandomUtils.nextInt(questList.size());
						NbQuestSettingVO randomQuestSetting = questList.get(randomIndex);
						questList.remove(randomIndex);

						WSItemString tmpObj = new WSItemString();
						// 迴圈在此處遞增
						tmpObj.setContext((++i) + dotStr + randomQuestSetting.getQuestDescVO().getQuestionDesc());

						elderQuestStrs.add(tmpObj);
					} // for

					extensionVO.setElderQuestions(elderQuestStrs);
				}
				// PCR 474367 視訊生調且有勾選「來源是否為解約、貸款或保單借款」
				if (_MONEY_SOURCE_Q_CODE.equals(vo.getLifeSurveyCode()) || _CALLOUT_MONEY_SOURCE_Q_CODE.equals(vo.getLifeSurveyCode())) {
					extensionVO.setHasMoneySourceQuestion(CodeCst.YES_NO__YES);
				}
			}
		}
		extensionVO.setSurveyReasons(lifeSurveyItems);
		/**IS OIU**/
		extensionVO.setIsOIU(nbLetterHelper.getIsOIU(policyVO));
		extensionVO.setBarcode1(CodeCst.BARCODE1_FMT_UNB_0100);
		extensionVO.setBarcode2(policyVO.getPolicyNumber());

		extensionVO.setSurveyType(aForm.getSurveyType());
		
		if ( StringUtils.isNullOrEmpty(aForm.getSurveyComment()) ) {
			extensionVO.setSurveyComment("無");
		} else {
			extensionVO.setSurveyComment("詳【附表】補充交查事項");
		}
		
		extensionVO.setSurveyCommentAppend(aForm.getSurveyComment());
		extensionVO.setSurveyType(aForm.getSurveyType());

		return extensionVO;
	}

	/**
	 * <p>Description : PCR 474367 視訊生調且有勾選「高齡關懷提問」需在高齡投保核保評估主表 T_UW_ELDER_CARE 新增 一筆資料</p>
	 * <p>Created By : Charlotte Wang</p>
	 * <p>Create Time : Feb 11, 2022</p>
	 * @param letter
	 */
	private void saveUwElderCare(UwSurveyLetterVO letter, FmtUnb0100ExtensionVO extensionVO) {
		List<WSItemString> elderQuestStrs = extensionVO.getElderQuestions();

		List<UwSurveyObjectVO> surveyObjs = letter.getUwSurveyObjects();
		for (UwSurveyObjectVO uwSurveyObjectVO : surveyObjs) {
			List<UwElderCareDtlVO> elderCareDtl = new ArrayList<UwElderCareDtlVO>();
			for (int i = 0; i < elderQuestStrs.size(); ++i) {
				WSItemString wsItemString = elderQuestStrs.get(i);
				UwElderCareDtlVO uwElderCareDtlVO = new UwElderCareDtlVO();
				String[] tmpArray = wsItemString.getContext().split("\\."); // 需要切掉題號
				uwElderCareDtlVO.setDisplayOrder(Long.valueOf(tmpArray[0]));
				uwElderCareDtlVO.setQuestionContent(tmpArray[1]);

				elderCareDtl.add(uwElderCareDtlVO);
			}

			UwElderCareTypeVO uwElderCareTypeVO = new UwElderCareTypeVO();
			uwElderCareTypeVO.setUwElderCareDtlVOs(elderCareDtl);
			uwElderCareTypeVO.setCareType(Cst.UW_ELDER_CARE_TYPE_LIFE_SURVEY);
			uwElderCareTypeVO.setCareTaskNo(String.valueOf(letter.getDocumentId()));
			uwElderCareTypeVO.setCareTaskDate(AppContext.getCurrentUserLocalTime());
			List<UwElderCareTypeVO> typeList = new ArrayList<UwElderCareTypeVO>();
			typeList.add(uwElderCareTypeVO);

			UwElderCareVO uwElderCareVO = new UwElderCareVO();
			uwElderCareVO.setPolicyId(letter.getPolicyId());
			uwElderCareVO.setCertiCode(uwSurveyObjectVO.getSurveyObjectCertiCode());
			uwElderCareVO.setUnderwriteId(letter.getUnderwriteId());
			uwElderCareVO.setUwElderCareTypeVOs(typeList);

			uwElderCareService.writeUwElderCare(uwElderCareVO);
		}
	}

	@Resource(name = UwLifeSurveyItemService.BEAN_DEFAULT)
	private UwLifeSurveyItemService uwLifeSurveyItemService;

	@Resource(name = UwSurveyLetterService.BEAN_DEFAULT)
	private UwSurveyLetterService uwSurveyLetterService;

	@Resource(name = PolicyService.BEAN_DEFAULT)
	private PolicyService policyService;

	@Resource(name = UwElderCareService.BEAN_DEFAULT)
	private UwElderCareService uwElderCareService;
	
	@Resource(name = InsuredService.BEAN_DEFAULT)
	private InsuredService insuredService;

	@Resource(name = UwPolicyService.BEAN_DEFAULT)
	private UwPolicyService uwPolicyService;

	@Resource(name = AgentService.BEAN_DEFAULT)
	private AgentService agentService;

	@Resource(name = UwCheckListStatusService.BEAN_DEFAULT)
	protected UwCheckListStatusService uwCheckListStatusService;

	@Resource(name = ProposalRuleMsgService.BEAN_DEFAULT)
	protected ProposalRuleMsgService proposalRuleMsgService;

	@Resource(name = ProposalRuleResultService.BEAN_DEFAULT)
	protected ProposalRuleResultService proposalRuleResultService;

	@Resource(name = NbLetterHelper.BEAN_DEFAULT)
	private NbLetterHelper nbLetterHelper;

	@Resource(name = NbNotificationHelper.BEAN_DEFAULT)
	private NbNotificationHelper nbNotification;

	@Resource(name = DeptService.BEAN_DEFAULT)
	private DeptService deptService;

	@Resource(name = UwPolicyCI.BEAN_DEFAULT)
	private UwPolicyCI uwPolicyCI;

	@Resource(name = ChannelOrgService.BEAN_DEFAULT)
	private ChannelOrgService channelOrgservice;

	@Resource(name = EmployeeCI.BEAN_DEFAULT)
	private EmployeeCI employeeCI;
	
	@Resource(name = AgentNotifyService.BEAN_DEFAULT)
	private AgentNotifyService agentNotifyService;

	@Resource(name = AddressService.BEAN_DEFAULT)
	private AddressService addressService;

	@Resource(name = NbQuestSettingService.BEAN_DEFAULT)
	private NbQuestSettingService questSettingService;

	@Resource(name = NbPolicySpecialRuleService.BEAN_DEFAULT)
	private NbPolicySpecialRuleService nbSpecialRuleService;

    @Resource(name = ValidatorService.BEAN_DEFAULT)
    ValidatorService paValidatorService;

	@Resource(name = ElderAudioHelper.BEAN_DEFAULT)
	private ElderAudioHelper elderAudioHelper;

}
