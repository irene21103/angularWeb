package com.ebao.ls.crs.batch.job;

import java.util.List;

import javax.annotation.Resource;
import javax.transaction.UserTransaction;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.ListUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.httpclient.util.DateUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.ebao.foundation.common.lang.exception.ExceptionUtils;
import com.ebao.foundation.core.logging.Log;
import com.ebao.ls.batch.TGLBasePieceableJob;
import com.ebao.ls.crs.batch.service.CrsFollowUpListService;
import com.ebao.ls.fatca.batch.data.FatcaDueDiligenceListDAO;
import com.ebao.ls.fatca.data.bo.FatcaDueDiligenceList;
import com.ebao.ls.pub.batch.impl.BatchJdbcTemplate;
import com.ebao.ls.pub.batch.impl.DataSourceWrapper;
import com.ebao.pub.batch.help.BatchHelp;
import com.ebao.pub.batch.type.JobStatus;
import com.ebao.pub.batch.type.LogLevel;
import com.ebao.pub.batch.util.BatchLogUtils;
import com.ebao.pub.para.Para;
import com.ebao.pub.util.DateUtils;
import com.ebao.pub.util.TransUtils;

public class CrsFollowUpListBatchJob extends TGLBasePieceableJob implements
		ApplicationContextAware {
    public final static String BEAN_DEFAULT = "crsFollowUpListBatchJob";
    
    @Resource(name = CrsFollowUpListService.BEAN_DEFAULT)
    private CrsFollowUpListService serv;
    
    @Resource(name = FatcaDueDiligenceListDAO.BEAN_DEFAULT)
    private FatcaDueDiligenceListDAO dao;

    private final Log log = Log.getLogger(CrsFollowUpListBatchJob.class);
    
    private ApplicationContext ctx;
    
    private static BatchJdbcTemplate t = new BatchJdbcTemplate(new DataSourceWrapper());
    
    private static NamedParameterJdbcTemplate nt = new NamedParameterJdbcTemplate(t);
    
    private void syso(Object msg){
        log.info("[SYSO][CrsFollowUpListBatchJob]" + msg);
    }
    
    @Override
    public void myPostProcess() throws Exception{
    	
    }
	@Override
	public void setApplicationContext(ApplicationContext arg0)
			throws BeansException {		
		this.ctx = arg0;

	}

	@Override
	protected String getJobName() {
		return BatchHelp.getJobName();
	}

	@Override
	@SuppressWarnings("unchecked")
	protected Object[] prepareIds() throws Throwable {
		java.util.Date processDate = BatchHelp.getProcessDate();	
		java.text.SimpleDateFormat format = new java.text.SimpleDateFormat("yyyyMMdd");
		//java.util.Date processDate = format.parse("20190930");		

    	int year = DateUtils.getYear(processDate);
    	java.util.Date junMonth = DateUtils.toDate(year+"0101", "yyyyMMdd");
    	java.sql.Date processSqlDate = new java.sql.Date(processDate.getTime());
    	java.sql.Date junMonthSqlDate = new java.sql.Date(junMonth.getTime());
    	java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
    	params.put("process_date", processSqlDate);    		   
    	params.put("jan_month", junMonthSqlDate);
    	//IR382042 -begin    	
    	int crsFollowUpDays = 55; //T_PARA_DEF.PARA_ID=2061000050 
    	int crsJudgedDays = 35;   //T_PARA_DEF.PARA_ID=2061000051
    	if (Para.getParaValue(2061000050L) != null ){
    		crsFollowUpDays = Integer.valueOf(Para.getParaValue(2061000050L));	
    	}
    	if (Para.getParaValue(2061000051L) != null ){
    		crsJudgedDays = Integer.valueOf(Para.getParaValue(2061000051L));	
    	}
    	params.put("followUpDays", crsFollowUpDays);
    	params.put("judgedDays", crsJudgedDays);
    	//IR382042 -end
		List<String> resultList = new java.util.LinkedList<String>();
		/* 抽取符合條件名單- 盡職調查跟催對象 */
		StringBuffer sql = new StringBuffer();
		/*IR382042:
		 *  1.加上FIRST_VALUE() OVER()語法  篩選同一對象，取最新日期資料
		 *  2.將原sql中Target_Certi_Code is null or not 拆成2段UNION 
		 * */
		//IR382042 sql.append(" select dueList.list_id || '/' ||DECODE(dueList.target_survey_type,  ");
		sql.append(" select DISTINCT (FIRST_VALUE(dueList.list_id || '/' ||DECODE(dueList.target_survey_type, "); //IR382042
		sql.append("              '02','08',   ");
		sql.append("              '03','14',   ");
		sql.append("              '15','16')   ");
		sql.append("    ) OVER (PARTITION BY dueList.Target_Certi_Code ORDER BY dueList.target_survey_date DESC )) "); //IR382042
		sql.append("   from t_fatca_due_diligence_list dueList   ");
		sql.append("  where 1 = 1   ");
		sql.append("   and dueList.target_survey_date > :jan_month   "); //當年1月產生的FATCA盡職調查名單對象
		sql.append("   and :process_date - :followUpDays >= dueList.target_survey_date   ");		
		sql.append("   and dueList.target_survey_type in ('02','03','15')  ");					
		sql.append("   and dueList.survey_system_type in ('CRS', 'CRS/FATCA')  ");
		sql.append("   AND dueList.target_certi_code is not NULL ");
		// 盡職調查跟催對象，排除已經做過的資料重複產生-->>
		sql.append("    and not exists   ");	     
		sql.append("      (select 1 from t_fatca_due_diligence_list lst   ");
		sql.append("        WHERE lst.target_certi_code = dueList.target_certi_code ");
//		sql.append("          where ((dueList.target_certi_code is not null and ");
//		sql.append("                 lst.target_certi_code = dueList.target_certi_code) ");
//		sql.append("                or dueList.target_certi_code is null )  ");
//		sql.append("            and lst.policy_id = dueList.policy_id  ");
//		sql.append("            and lst.target_name = dueList.target_name  ");
		sql.append("            and lst.survey_system_type in ('CRS', 'CRS/FATCA')  ");
		sql.append("            and lst.target_survey_type = DECODE(dueList.target_survey_type, ");
		sql.append("                        '02','08',    ");
		sql.append("                        '03','14',    ");
		sql.append("                        '15','16')   ");
							//尚未完成批次判定
		//sql.append("            and lst.TARGET_SURVEY_DATE + pkg_pub_para.f_get_para_value(2061000051) > :process_date )  ");		
		//    sql.append("            and lst.TARGET_SURVEY_DATE + :judgedDays > :process_date )  ");
		sql.append("            and (lst.TARGET_SURVEY_DATE + :judgedDays > :process_date   ");
		sql.append("                  or lst.Notify_Date = dueList.target_survey_date )  ");
		sql.append("          ) ");
		  // 已通知未簽暑日已達X天	-->>
		sql.append("   and exists  ");
		sql.append("       (select 1 from T_CRS_PARTY t  ");
                    //若certi_code為空，才加比對保單+姓名
		sql.append("         WHERE t.certi_code = dueList.TARGET_CERTI_CODE  ");
//        sql.append("         where ((dueList.TARGET_CERTI_CODE is null and     "); 		
//		sql.append("           	      t.policy_code  = dueList.POLICY_CODE and  ");
//		sql.append("           	      t.name = dueList.target_name )             ");
//		sql.append("           	   or (dueList.TARGET_CERTI_CODE is not null and   ");
//		sql.append("           	       t.certi_code = dueList.TARGET_CERTI_CODE )) ");
		sql.append("           and t.doc_sign_status = '01'   ");
		sql.append("           and t.build_type in ( '06' )  ");   //crs建檔紀錄為" 已註記"
		//sql.append("           and t.DOC_SIGN_DATE + pkg_pub_para.f_get_para_value(2061000050) <= :process_date ");
		sql.append("           and t.DOC_SIGN_DATE + :followUpDays <= :process_date ");
		sql.append("         )  ");		
				// 產生過權益通知書的(t_crs_right_notify)，不再產生  -->>
		sql.append("   and not exists  "); 
		sql.append("        (select 1 from T_CRS_PARTY t,    ");
		sql.append("         	            t_crs_right_notify nt  ");
		sql.append("          WHERE t.certi_code = dueList.TARGET_CERTI_CODE ");
//		sql.append("           where ((dueList.TARGET_CERTI_CODE is null and  ");
//		sql.append("             	      t.policy_code  = dueList.POLICY_CODE and  ");
//		sql.append("             	      t.name = dueList.target_name )  ");
//		sql.append("             	   or (dueList.TARGET_CERTI_CODE is not null and  ");
//		sql.append("            	        t.certi_code = dueList.TARGET_CERTI_CODE) )  ");
		sql.append("             and t.doc_sign_status = '01' ");
		sql.append("             and t.build_type in ('01','05','06') ");
		sql.append("             and nt.crs_id = t.crs_id ");
		sql.append("             and nt.update_time >= :process_date ");
		sql.append("           )   ");
	     //排除CRS主檔 02-已簽署, 03-不願簽署 
		sql.append("   and not exists   ");
		sql.append("         (select 1 from T_CRS_PARTY t ");
		sql.append("           WHERE t.certi_code = dueList.TARGET_CERTI_CODE ");
//		sql.append("           where ((dueList.TARGET_CERTI_CODE is null and  ");
//		sql.append("             	      t.policy_code  = dueList.POLICY_CODE and  ");
//		sql.append("             	      t.name = dueList.target_name )  ");
//		sql.append("             	   or (dueList.TARGET_CERTI_CODE is not null and  ");
//		sql.append("            	        t.certi_code = dueList.TARGET_CERTI_CODE) )  ");
		sql.append("             and t.doc_sign_status in ('02', '03')   ");
		sql.append("             and t.build_type not in ('03','04')  ");
		sql.append("           )  ");
//		sql.append("   and :process_date - pkg_pub_para.f_get_para_value(2061000050) >= dueList.target_survey_date   ");
//		sql.append("   and dueList.target_survey_type in ('02','03','15')  ");
//		//當年1月產生的FATCA盡職調查名單對象	
//		sql.append("   and dueList.target_survey_date > :jan_month   ");		
		sql.append("UNION  "); //若certi_code為空，比對保單+姓名
		sql.append("  select DISTINCT (FIRST_VALUE(dueList.list_id || '/' ||DECODE(dueList.target_survey_type,  ");  
	    sql.append("  		      '02','08',  ");
	    sql.append("              '03','14',  ");   
	    sql.append("              '15','16') )  ");
	    sql.append("              OVER (PARTITION BY dueList.policy_id, dueList.Target_Name ORDER BY dueList.Target_Survey_Date DESC ))  ");  
	    sql.append("   from t_fatca_due_diligence_list dueList  ");  
	    sql.append("   where 1 = 1  ");
	    sql.append("   and dueList.target_survey_date > :jan_month  "); //當年1月產生的FATCA盡職調查名單對象    
	    sql.append("   and :process_date - :followUpDays >= dueList.target_survey_date  ");   
	    sql.append("   and dueList.target_survey_type in ('02','03','15')  ");        
	    sql.append("   and dueList.survey_system_type in ('CRS', 'CRS/FATCA')  ");
	    sql.append("   AND dueList.target_certi_code is NULL ");
	    // 盡職調查跟催對象，排除已經做過的資料重複產生-->>
	    sql.append("     and not exists ");
	    sql.append("       (select 1 from t_fatca_due_diligence_list lst ");   
	    sql.append(" 	     where 1 = 1 ");
	    sql.append(" 	       and lst.policy_id = dueList.policy_id ");  
	    sql.append(" 	       and lst.target_name = dueList.target_name ");  
	    sql.append("           and lst.survey_system_type in ('CRS', 'CRS/FATCA')  ");
	    sql.append("           and lst.target_certi_code is NULL  ");
	    sql.append(" 	       and lst.target_survey_type = DECODE(dueList.target_survey_type, "); 
	    sql.append(" 	                            '02','08', "); 
	    sql.append(" 	                            '03','14', ");    
	    sql.append(" 	                            '15','16' ) ");   
	    //尚未完成批次判定
	    //   sql.append(" 	      and lst.TARGET_SURVEY_DATE + :judgedDays > :process_date ) ");
	    sql.append("            and (lst.TARGET_SURVEY_DATE + :judgedDays > :process_date   ");
		sql.append("                 or lst.Notify_Date = dueList.target_survey_date )  ");
		sql.append("          ) ");
	    // 已通知未簽暑日已達X天  -->>
	    sql.append(" 	     and exists "); 
	    sql.append(" 	           (select 1 from T_CRS_PARTY t ");                      
	    sql.append(" 	             where t.policy_code  = dueList.POLICY_CODE "); 
	    sql.append(" 	               and t.name = dueList.target_name "); 
	    sql.append("                   and t.CERTI_CODE IS NULL ");
	    sql.append(" 	               and t.doc_sign_status = '01' ");  
	    sql.append(" 	               and t.build_type in ( '06' ) ");  //crs建檔紀錄為" 已註記"
	    sql.append(" 	               and t.DOC_SIGN_DATE + :followUpDays <= :process_date "); 
	    sql.append(" 	             ) ");
	    // 產生過權益通知書的(t_crs_right_notify)，不再產生  -->>
	    sql.append("        and not exists   ");
	    sql.append(" 	            (select 1 from T_CRS_PARTY t, ");    
	    sql.append(" 	                          t_crs_right_notify nt  ");
	    sql.append(" 	               where t.policy_code  = dueList.POLICY_CODE "); 
	    sql.append(" 	                 and t.name = dueList.target_name "); 
	    sql.append("                     and t.CERTI_CODE IS NULL ");
	    sql.append(" 	                 and t.doc_sign_status = '01' ");     
	    sql.append(" 	                 and t.build_type in ('01','05','06') ");   
	    sql.append(" 	                 and nt.crs_id = t.crs_id ");           
	    sql.append(" 	                 and nt.update_time >= :process_date "); 
	    sql.append(" 	               ) ");
	    //排除CRS主檔 02-已簽署, 03-不願簽署 
	    sql.append(" 	       and not EXISTS "); 
	    sql.append(" 	             (select 1 from T_CRS_PARTY t "); 
	    sql.append(" 	               where t.policy_code  = dueList.POLICY_CODE "); 
	    sql.append(" 	                 and t.name = dueList.target_name ");
	    sql.append("                     and t.CERTI_CODE IS NULL ");
	    sql.append(" 	                 and t.doc_sign_status in ('02', '03') "); 
	    sql.append(" 	                 and t.build_type not in ('03','04') "); 
	    sql.append(" 	               ) "); 		

		List<String> itemList = nt.queryForList(sql.toString(), params, String.class);
        BatchLogUtils.addLog(LogLevel.INFO, null, String.format("[BR-CMN-CRS-029][CRS盡職調查跟催對象] list.size= %,d " , CollectionUtils.size(itemList)));
        resultList = ListUtils.union(resultList, itemList);
        
        /* 抽取符合條件名單-即期年金受益人(被保險人)跟催對象  05-->09  */
        sql.setLength(0);                
        //IR382042	sql.append(" select dueList.list_id || '/' || '09' ");
        	sql.append(" select DISTINCT (FIRST_VALUE(dueList.list_id || '/' || '09') ");  //IR382042
        	sql.append("        OVER (PARTITION BY dueList.Target_Certi_Code ORDER BY dueList.target_survey_date DESC )) ");  //IR382042
    		sql.append("   from t_fatca_due_diligence_list dueList   ");
    		sql.append("  where 1 = 1   ");
    		sql.append("   and :process_date - :followUpDays >= dueList.target_survey_date   ");
    		sql.append("   and duelist.target_survey_type = '05'  ");
    		sql.append("   and dueList.survey_system_type in ('CRS', 'CRS/FATCA')  ");
    		sql.append("   AND dueList.TARGET_CERTI_CODE IS NOT NULL ");
    		// 即期年金受益人(被保險人)跟催對象，排除已經做過的資料重複產生-->>
    		sql.append("    and not exists   ");	     
    		sql.append("        (select 1 from t_fatca_due_diligence_list lst   ");    
    		sql.append("          WHERE lst.target_certi_code = dueList.target_certi_code ");
//    		sql.append("          where ((dueList.target_certi_code is not null and ");
//    		sql.append("                 lst.target_certi_code = dueList.target_certi_code) ");
//    		sql.append("                or dueList.target_certi_code is null )  ");
//    		sql.append("            and lst.policy_id = dueList.policy_id  ");
//    		sql.append("            and lst.target_name = dueList.target_name  ");
    		sql.append("            and lst.survey_system_type in ('CRS', 'CRS/FATCA')  ");
    		sql.append("            and lst.target_survey_type = '09'  ");
					//尚未完成批次判定
    		//sql.append("            and lst.TARGET_SURVEY_DATE + pkg_pub_para.f_get_para_value(2061000051) > :process_date )  ");
    		//  sql.append("            and lst.TARGET_SURVEY_DATE + :judgedDays > :process_date )  ");
    		sql.append("            and (lst.TARGET_SURVEY_DATE + :judgedDays > :process_date   ");
    		sql.append("                  or lst.Notify_Date = dueList.target_survey_date )  ");
    		sql.append("          ) ");
    		  // 已通知未簽暑日已達X天	-->>
    		sql.append("   and exists  ");
    		sql.append("       (select 1 from T_CRS_PARTY t  ");
                         //若certi_code為空，才加比對保單+姓名
    		sql.append("         WHERE t.certi_code = dueList.TARGET_CERTI_CODE ");
//            sql.append("         where ((dueList.TARGET_CERTI_CODE is null and     ");     		
//    		sql.append("           	      t.policy_code  = dueList.POLICY_CODE and  ");
//    		sql.append("           	      t.name = dueList.target_name )             ");    		
//    		sql.append("           	   or (dueList.TARGET_CERTI_CODE is not null and   ");
//    		sql.append("           	       t.certi_code = dueList.TARGET_CERTI_CODE )) ");
    		sql.append("           and t.doc_sign_status = '01'   ");
    		sql.append("           and t.build_type in ( '06' )  ");   //crs建檔紀錄為" 已註記"
    		//sql.append("           and t.DOC_SIGN_DATE + pkg_pub_para.f_get_para_value(2061000050) <= :process_date ");
    		sql.append("           and t.DOC_SIGN_DATE + :followUpDays <= :process_date ");
    		sql.append("         )  ");		
    				// 產生過權益通知書的(t_crs_right_notify)，不再產生  -->>
    		sql.append("   and not exists  "); 
    		sql.append("        (select 1 from T_CRS_PARTY t,    ");
    		sql.append("         	            t_crs_right_notify nt  ");
    		sql.append("          WHERE t.certi_code = dueList.TARGET_CERTI_CODE ");
//    		sql.append("           where ((dueList.TARGET_CERTI_CODE is null and  ");
//    		sql.append("             	      t.policy_code  = dueList.POLICY_CODE and  ");
//    		sql.append("             	      t.name = dueList.target_name )  ");
//    		sql.append("             	   or (dueList.TARGET_CERTI_CODE is not null and  ");
//    		sql.append("            	        t.certi_code = dueList.TARGET_CERTI_CODE) )  ");
    		sql.append("             and t.doc_sign_status = '01'     ");
    		sql.append("             and t.build_type in ('01','05','06')   ");
    		sql.append("             and nt.crs_id = t.crs_id            ");
    		sql.append("             and nt.update_time >= :process_date ");
    		sql.append("           )   ");
    	     //排除CRS主檔 02-已簽署, 03-不願簽署 
    		sql.append("   and not exists   ");
    		sql.append("         (select 1 from T_CRS_PARTY t   ");
    		sql.append("           WHERE t.certi_code = dueList.TARGET_CERTI_CODE  ");
//    		sql.append("           where ((dueList.TARGET_CERTI_CODE is null and  ");
//    		sql.append("             	      t.policy_code  = dueList.POLICY_CODE and  ");
//    		sql.append("             	      t.name = dueList.target_name )  ");
//    		sql.append("             	   or (dueList.TARGET_CERTI_CODE is not null and  ");
//    		sql.append("            	        t.certi_code = dueList.TARGET_CERTI_CODE) )  ");
    		sql.append("             and t.doc_sign_status in ('02', '03')   ");
    		sql.append("             and t.build_type not in ('03','04')  ");
    		sql.append("           )  ");
//    		sql.append("   and :process_date - pkg_pub_para.f_get_para_value(2061000050) >= dueList.target_survey_date   ");
//    		sql.append("   and duelist.target_survey_type = '05'  ");
    		sql.append("  UNION  ");    //certi_code為空，比對保單+姓名
    		sql.append("     select DISTINCT (FIRST_VALUE(dueList.list_id || '/' || '09' ) ");
    		sql.append("            OVER (PARTITION BY dueList.policy_id, dueList.Target_Name ORDER BY dueList.Target_Survey_Date DESC ))  ");
    		sql.append("     from t_fatca_due_diligence_list dueList   ");
    		sql.append("    where 1 = 1   ");
    		sql.append("      and :process_date - :followUpDays >= dueList.target_survey_date    ");
    		sql.append("      and duelist.target_survey_type = '05'    ");
    		sql.append("      and dueList.survey_system_type in ('CRS', 'CRS/FATCA') ");
    		sql.append("      AND dueList.target_certi_code is NULL   ");
    				// 即期年金受益人(被保險人)跟催對象，排除已經做過的資料重複產生-->>  
    		sql.append("      and not exists   ");
    		sql.append("          (select 1 from t_fatca_due_diligence_list lst  ");
    		sql.append("            WHERE 1 = 1   "); 
    		sql.append("              and lst.policy_id = dueList.policy_id  ");
    		sql.append("              and lst.target_name = dueList.target_name  ");
    		sql.append("              and lst.TARGET_CERTI_CODE is NULL  ");
    		sql.append("              and lst.survey_system_type in ('CRS', 'CRS/FATCA') ");
    		sql.append("              and lst.target_survey_type = '09'  ");
    				//尚未完成批次判定                                                 
    		//  sql.append("              and lst.TARGET_SURVEY_DATE + :judgedDays > :process_date ) ");
    		sql.append("              and (lst.TARGET_SURVEY_DATE + :judgedDays > :process_date   ");
    		sql.append("                   or lst.Notify_Date = dueList.target_survey_date )  ");
    		sql.append("          ) ");
    				// 已通知未簽暑日已達X天  -->>                                     
    		sql.append("     and exists  ");
    		sql.append("         (select 1 from T_CRS_PARTY t  ");
    		sql.append("           where t.policy_code  = dueList.POLICY_CODE  ");
    		sql.append("             and t.name = dueList.target_name  ");
    		sql.append("             and t.CERTI_CODE IS NULL ");
    		sql.append("             and t.doc_sign_status = '01'  ");
    		sql.append("             and t.build_type in ( '06' )   ");
    		sql.append("             and t.DOC_SIGN_DATE + :followUpDays <= :process_date ");
    		sql.append("           )   ");
    			// 產生過權益通知書的(t_crs_right_notify)，不再產生  -->>      
    		sql.append("     and not exists   ");
    		sql.append("          (select 1 from T_CRS_PARTY t,  ");
    		sql.append("                        t_crs_right_notify nt   ");
    		sql.append("             where t.policy_code  = dueList.POLICY_CODE  ");
    		sql.append("               and t.name = dueList.target_name  ");
    		sql.append("               and t.CERTI_CODE IS NULL ");
    		sql.append("               and t.doc_sign_status = '01'  ");
    		sql.append("               and t.build_type in ('01','05','06')  ");
    		sql.append("               and nt.crs_id = t.crs_id    ");
    		sql.append("               and nt.update_time >= :process_date   ");
    		sql.append("                   )  ");
    			//排除CRS主檔 02-已簽署, 03-不願簽署                       
    		sql.append("        and not exists    ");
    		sql.append("             (select 1 from T_CRS_PARTY t     ");
    		sql.append("               where t.policy_code  = dueList.POLICY_CODE  ");
    		sql.append("                 and t.name = dueList.target_name          ");
    		sql.append("                 and t.CERTI_CODE IS NULL ");
    		sql.append("                 and t.doc_sign_status in ('02', '03')     ");
    		sql.append("                 and t.build_type not in ('03','04')       ");
    		sql.append("               )   "); 
     

        itemList = nt.queryForList(sql.toString(), params, String.class);
        BatchLogUtils.addLog(LogLevel.INFO, null, String.format("[BR-CMN-CRS-029][CRS即期年金受益人(被保險人)跟催對象] list.size= %,d " , CollectionUtils.size(itemList)));
        resultList = ListUtils.union(resultList, itemList);
        
        /* 抽取符合條件名單-保險金受益人跟催對象 06->10 */
        sql.setLength(0);
        //IR382042 sql.append(" select dueList.list_id || '/' || '10' ");
        	sql.append(" select DISTINCT (FIRST_VALUE( dueList.list_id || '/' || '10') ");   //IR382042
        	sql.append("        OVER (PARTITION BY dueList.Target_Certi_Code ORDER BY dueList.target_survey_date DESC )) ");   //IR382042
        	sql.append("   from t_fatca_due_diligence_list dueList   ");
        	sql.append("  where 1 = 1 ");    
        	sql.append("    and :process_date - :followUpDays >= dueList.target_survey_date   ");        
            sql.append("    and dueList.target_survey_type = '06'  ");  
    		sql.append("    and dueList.survey_system_type in ('CRS', 'CRS/FATCA')  ");
    		sql.append("    AND dueList.target_certi_code IS NOT NULL ");
    		// 保險金受益人跟催對象，排除已經做過的資料重複產生-->>
    		sql.append("    and not exists   ");	     
    		sql.append("        (select 1 from t_fatca_due_diligence_list lst   "); 
    		sql.append("          WHERE lst.target_certi_code = dueList.target_certi_code ");
//    		sql.append("          where ((dueList.target_certi_code is not null and ");
//    		sql.append("                 lst.target_certi_code = dueList.target_certi_code) ");
//    		sql.append("                or dueList.target_certi_code is null )  ");
//    		sql.append("            and lst.policy_id = dueList.policy_id  ");
//    		sql.append("            and lst.target_name = dueList.target_name  ");
    		sql.append("            and lst.survey_system_type in ('CRS', 'CRS/FATCA')  ");
    		sql.append("            and lst.target_survey_type = '10'  ");
				//尚未完成批次判定
    		//sql.append("            and lst.TARGET_SURVEY_DATE + pkg_pub_para.f_get_para_value(2061000051) > :process_date )  ");
    		//  sql.append("            and lst.TARGET_SURVEY_DATE + :judgedDays > :process_date )  ");
    		sql.append("            and (lst.TARGET_SURVEY_DATE + :judgedDays > :process_date   ");
    		sql.append("                  or lst.Notify_Date = dueList.target_survey_date )  ");
    		sql.append("          ) ");
    		  // 已通知未簽暑日已達X天	-->>
    		sql.append("   and exists  ");
    		sql.append("       (select 1 from T_CRS_PARTY t  ");
                        //若certi_code為空，才加比對保單+姓名
            sql.append("         WHERE t.certi_code = dueList.TARGET_CERTI_CODE  ");    
//            sql.append("         where ((dueList.TARGET_CERTI_CODE is null and     "); 
//    		sql.append("           	      t.policy_code  = dueList.POLICY_CODE and  ");
//    		sql.append("           	      t.name = dueList.target_name )             ");    		
//    		sql.append("           	   or (dueList.TARGET_CERTI_CODE is not null and   ");
//    		sql.append("           	       t.certi_code = dueList.TARGET_CERTI_CODE )) ");
    		sql.append("           and t.doc_sign_status = '01'   ");
    		sql.append("           and t.build_type in ( '06' )  ");   //crs建檔紀錄為" 已註記"
    		//sql.append("           and t.DOC_SIGN_DATE + pkg_pub_para.f_get_para_value(2061000050) <= :process_date ");
    		sql.append("           and t.DOC_SIGN_DATE + :followUpDays <= :process_date ");
    		sql.append("         )  ");		
    				// 產生過權益通知書的(t_crs_right_notify)，不再產生  -->>
    		sql.append("   and not exists  "); 
    		sql.append("        (select 1 from T_CRS_PARTY t,    ");
    		sql.append("         	            t_crs_right_notify nt  ");
    		sql.append("           WHERE t.certi_code = dueList.TARGET_CERTI_CODE ");
//    		sql.append("           where ((dueList.TARGET_CERTI_CODE is null and  ");
//    		sql.append("             	      t.policy_code  = dueList.POLICY_CODE and  ");
//    		sql.append("             	      t.name = dueList.target_name )  ");
//    		sql.append("             	   or (dueList.TARGET_CERTI_CODE is not null and  ");
//    		sql.append("            	        t.certi_code = dueList.TARGET_CERTI_CODE) )  ");
    		sql.append("             and t.doc_sign_status = '01'  ");
    		sql.append("             and t.build_type in ('01','05','06')   ");
    		sql.append("             and nt.crs_id = t.crs_id   ");
    		sql.append("             and nt.update_time >= :process_date ");
    		sql.append("           )   ");
    	     //排除CRS主檔 02-已簽署, 03-不願簽署 
    		sql.append("   and not exists   ");
    		sql.append("         (select 1 from T_CRS_PARTY t  ");
    		sql.append("           WHERE t.certi_code = dueList.TARGET_CERTI_CODE  ");
//    		sql.append("           where ((dueList.TARGET_CERTI_CODE is null and  ");
//    		sql.append("             	      t.policy_code  = dueList.POLICY_CODE and  ");
//    		sql.append("             	      t.name = dueList.target_name )  ");
//    		sql.append("             	   or (dueList.TARGET_CERTI_CODE is not null and  ");
//    		sql.append("            	        t.certi_code = dueList.TARGET_CERTI_CODE) )  ");
    		sql.append("             and t.doc_sign_status in ('02', '03')   ");
    		sql.append("             and t.build_type not in ('03','04')  ");
    		sql.append("           )  ");
//    		sql.append("   and :process_date - pkg_pub_para.f_get_para_value(2061000050) >= dueList.target_survey_date   ");        
//            sql.append("    and dueList.target_survey_type = '06'  ");          
//            sql.append("    and rownum < pkg_ls_crs_foreign_indi.f_get_bat_rownum  "); //限制一次批次筆數上線為5萬筆
    		 sql.append(" UNION ");//certi_code為空，改比對保單+姓名
    		 sql.append("	select DISTINCT (FIRST_VALUE( dueList.list_id || '/' || '10') ");
    		 sql.append("             OVER (PARTITION BY dueList.policy_id, dueList.Target_Name ORDER BY dueList.Target_Survey_Date DESC )) ");
    		 sql.append("      from t_fatca_due_diligence_list dueList    ");
    		 sql.append("     where 1 = 1          ");
    		 sql.append("       and dueList.target_survey_type = '06'       ");
    		 sql.append("       and dueList.survey_system_type in ('CRS', 'CRS/FATCA')             ");
    		 sql.append("       and :process_date - :followUpDays >= dueList.target_survey_date    ");
    		 sql.append("       AND dueList.target_certi_code IS NULL       ");
    		       // 保險金受益人跟催對象，排除已經做過的資料重複產生-->>    
    		 sql.append("       and not exists              ");
    		 sql.append("           (select 1 from t_fatca_due_diligence_list lst  ");
    		 sql.append("             WHERE 1 = 1    ");
    		 sql.append("           and lst.policy_id = dueList.policy_id      ");
    		 sql.append("           and lst.target_name = dueList.target_name  ");
    		 sql.append("           and lst.target_certi_code IS NULL   ");
    		 sql.append("           and lst.target_survey_type = '10' ");
    		 sql.append("           and lst.survey_system_type in ('CRS', 'CRS/FATCA')  "); 
    		       //尚未完成批次判定    
    		 //sql.append("           and lst.TARGET_SURVEY_DATE + :judgedDays > :process_date ) "); 
    		 sql.append("           and (lst.TARGET_SURVEY_DATE + :judgedDays > :process_date   ");
    	     sql.append("                 or lst.Notify_Date = dueList.target_survey_date )  ");
    		 sql.append("          ) ");
    		   // 已通知未簽暑日已達X天  -->> 
    		 sql.append("      and exists            ");
    		 sql.append("          (select 1 from T_CRS_PARTY t             ");
    		 sql.append("            where t.policy_code  = dueList.POLICY_CODE      ");
    		 sql.append("              and t.name = dueList.target_name     ");
    		 sql.append("              and t.CERTI_CODE IS NULL ");
    		 sql.append("              and t.doc_sign_status = '01'         ");
    		 sql.append("              and t.build_type in ( '06' ) "); //crs建檔紀錄為" 已註記" 
    		 sql.append("              and t.DOC_SIGN_DATE + :followUpDays <= :process_date  ");
    		 sql.append("            ) ");
    		// 產生過權益通知書的(t_crs_right_notify)，不再產生  -->>   
    		 sql.append("      and not exists              ");
    		 sql.append("          (select 1 from T_CRS_PARTY t,   ");
    		 sql.append("                    t_crs_right_notify nt           ");
    		 sql.append("            where t.policy_code  = dueList.POLICY_CODE ");
    		 sql.append("           and t.name = dueList.target_name         ");
    		 sql.append("           and t.CERTI_CODE IS NULL  ");
    		 sql.append("           and t.doc_sign_status = '01'             ");
    		 sql.append("           and t.build_type in ('01','05','06')     ");
    		 sql.append("           and nt.crs_id = t.crs_id   ");
    		 sql.append("           and nt.update_time >= :process_date      ");
    		 sql.append("         )     ");
    		 //排除CRS主檔 02-已簽署, 03-不願簽署  
    		 sql.append("      and not exists              ");
    		 sql.append("          (select 1 from T_CRS_PARTY t   ");
    		 sql.append("            where t.policy_code  = dueList.POLICY_CODE ");
    		 sql.append("              and t.name = dueList.target_name         ");
    		 sql.append("              and t.CERTI_CODE IS NULL ");
    		 sql.append("              and t.doc_sign_status in ('02', '03')    ");
    		 sql.append("              and t.build_type not in ('03','04')      ");
    		 sql.append("          )     ");
            
        itemList = nt.queryForList(sql.toString(), params, String.class);
        BatchLogUtils.addLog(LogLevel.INFO, null, String.format("[BR-CMN-CRS-029][CRS保險金受益人跟催對象] list.size=  %,d " , CollectionUtils.size(itemList)));
        resultList = ListUtils.union(resultList, itemList);
        
        /* 抽取符合條件名單-受益人變更跟催對象  04->13 */
        	sql.setLength(0);
        	//IR382042 sql.append(" select dueList.list_id || '/' || '13' ");
        	sql.append(" select DISTINCT (FIRST_VALUE(dueList.list_id || '/' || '13') ");  //IR382042
        	sql.append("        OVER (PARTITION BY dueList.Target_Certi_Code ORDER BY dueList.target_survey_date DESC )) ");  //IR382042
        	sql.append("   from t_fatca_due_diligence_list dueList   ");
        	sql.append("  where 1 = 1   ");
        	sql.append("    and :process_date - :followUpDays >= dueList.target_survey_date   ");   
    		sql.append("    and dueList.target_survey_type = '04'   ");    		
    		sql.append("    and dueList.survey_system_type in ('CRS', 'CRS/FATCA')  ");
    		sql.append("    AND dueList.Target_Certi_Code IS NOT NULL ");
    		// 受益人變更跟催對象，排除已經做過的資料重複產生-->>
    		sql.append("    and not exists   ");	     
    		sql.append("        (select 1 from t_fatca_due_diligence_list lst   ");    		
    		sql.append("          WHERE lst.target_certi_code = dueList.target_certi_code ");
//    		sql.append("          where ((dueList.target_certi_code is not null and ");
//    		sql.append("                 lst.target_certi_code = dueList.target_certi_code) ");
//    		sql.append("                or dueList.target_certi_code is null )  ");
//    		sql.append("            and lst.policy_id = dueList.policy_id  ");
//    		sql.append("            and lst.target_name = dueList.target_name  ");
    		sql.append("            and lst.survey_system_type in ('CRS', 'CRS/FATCA')  ");
    		sql.append("            and lst.target_survey_type = '13'  ");
				//尚未完成批次判定
    		//sql.append("            and lst.TARGET_SURVEY_DATE + pkg_pub_para.f_get_para_value(2061000051) > :process_date )  ");
    		//sql.append("            and lst.TARGET_SURVEY_DATE + :judgedDays > :process_date )  ");
    		sql.append("            and (lst.TARGET_SURVEY_DATE + :judgedDays > :process_date   ");
    		sql.append("                  or lst.Notify_Date = dueList.target_survey_date )  ");
    		sql.append("          ) ");
    		  // 已通知未簽暑日已達X天	-->>
    		sql.append("   and exists  ");
    		sql.append("       (select 1 from T_CRS_PARTY t  ");
                             //若certi_code為空，才加比對保單+姓名
            sql.append("         WHERE t.certi_code = dueList.TARGET_CERTI_CODE  ");   
//            sql.append("         where ((dueList.TARGET_CERTI_CODE is null and     ");   
//    		sql.append("           	      t.policy_code  = dueList.POLICY_CODE and  ");
//    		sql.append("           	      t.name = dueList.target_name )             ");    	
//    		sql.append("           	   or (dueList.TARGET_CERTI_CODE is not null and   ");
//    		sql.append("           	       t.certi_code = dueList.TARGET_CERTI_CODE )) ");
    		sql.append("           and t.doc_sign_status = '01'   ");
    		sql.append("           and t.build_type in ( '06' )  ");   //crs建檔紀錄為" 已註記"
    		//sql.append("           and t.DOC_SIGN_DATE + pkg_pub_para.f_get_para_value(2061000050) <= :process_date ");
    		sql.append("           and t.DOC_SIGN_DATE + :followUpDays <= :process_date ");
    		sql.append("         )  ");		
    				// 產生過權益通知書的(t_crs_right_notify)，不再產生  -->>
    		sql.append("   and not exists  "); 
    		sql.append("        (select 1 from T_CRS_PARTY t,    ");
    		sql.append("         	            t_crs_right_notify nt  ");
    		sql.append("          WHERE t.certi_code = dueList.TARGET_CERTI_CODE ");
//    		sql.append("           where ((dueList.TARGET_CERTI_CODE is null and  ");
//    		sql.append("             	      t.policy_code  = dueList.POLICY_CODE and  ");
//    		sql.append("             	      t.name = dueList.target_name )  ");
//    		sql.append("             	   or (dueList.TARGET_CERTI_CODE is not null and  ");
//    		sql.append("            	        t.certi_code = dueList.TARGET_CERTI_CODE) )  ");
    		sql.append("             and t.doc_sign_status = '01'     ");
    		sql.append("             and t.build_type in ('01','05','06')   ");
    		sql.append("             and nt.crs_id = t.crs_id            ");
    		sql.append("             and nt.update_time >= :process_date ");
    		sql.append("           )   ");
    	     //排除CRS主檔 02-已簽署, 03-不願簽署 
    		sql.append("   and not exists   ");
    		sql.append("         (select 1 from T_CRS_PARTY t                          ");
    		sql.append("           where t.certi_code = dueList.TARGET_CERTI_CODE ");
//    		sql.append("           where ((dueList.TARGET_CERTI_CODE is null and  ");
//    		sql.append("             	      t.policy_code  = dueList.POLICY_CODE and  ");
//    		sql.append("             	      t.name = dueList.target_name )  ");
//    		sql.append("             	   or (dueList.TARGET_CERTI_CODE is not null and  ");
//    		sql.append("            	        t.certi_code = dueList.TARGET_CERTI_CODE) )  ");
    		sql.append("             and t.doc_sign_status in ('02', '03')   ");
    		sql.append("             and t.build_type not in ('03','04')  ");
    		sql.append("           )  ");
//    		sql.append("   and :process_date - pkg_pub_para.f_get_para_value(2061000050) >= dueList.target_survey_date   ");   
//    		sql.append("    and dueList.target_survey_type = '04'   ");
    		sql.append(" UNION  "); //若certi_code為空，才加比對保單+姓名
    		sql.append(" select DISTINCT (FIRST_VALUE(dueList.list_id || '/' || '13') ");
    		sql.append("              OVER (PARTITION BY dueList.policy_id, dueList.Target_Name ORDER BY dueList.Target_Survey_Date DESC )) ");
    		sql.append("         from t_fatca_due_diligence_list dueList   ");
    		sql.append("        where 1 = 1  "); 
    		sql.append("        and :process_date - :followUpDays >= dueList.target_survey_date     ");  
    		sql.append("        and dueList.target_survey_type = '04'                  ");
    		sql.append("        and dueList.survey_system_type in ('CRS', 'CRS/FATCA') ");
    		sql.append("        AND dueList.Target_Certi_Code IS  NULL                 ");
    				// 受益人變更跟催對象，排除已經做過的資料重複產生-->>               
    		sql.append("        and not exists   ");
    		sql.append("            (select 1 from t_fatca_due_diligence_list lst      ");
    		sql.append("              where 1 = 1   ");
    		sql.append("                and lst.policy_id = dueList.policy_id          ");
    		sql.append("                and lst.target_name = dueList.target_name      ");
    		sql.append("                and lst.Target_Certi_Code IS  NULL   ");
    		sql.append("                and lst.survey_system_type in ('CRS', 'CRS/FATCA')  ");
    		sql.append("                and lst.target_survey_type = '13'  ");
    				//尚未完成批次判定                                                  
    		//  sql.append("                and lst.TARGET_SURVEY_DATE + :judgedDays > :process_date )  ");
    		sql.append("               and (lst.TARGET_SURVEY_DATE + :judgedDays > :process_date   ");
    		sql.append("                  or lst.Notify_Date = dueList.target_survey_date )  ");
    		sql.append("          ) ");
    				// 已通知未簽暑日已達X天  -->>                                  
    		sql.append("       and exists   ");
    		sql.append("           (select 1 from T_CRS_PARTY t  ");
    		sql.append("             where t.policy_code  = dueList.POLICY_CODE ");
    		sql.append("               and t.name = dueList.target_name         ");
    		sql.append("               and t.CERTI_CODE IS NULL "); 
    		sql.append("               and t.doc_sign_status = '01'             ");
    		sql.append("               and t.build_type in ( '06' )  "); //crs建檔紀錄為" 已註記" 
    		sql.append("               and t.DOC_SIGN_DATE + :followUpDays <= :process_date  ");
    		sql.append("             )  ");
    				// 產生過權益通知書的(t_crs_right_notify)，不再產生  -->>        
    		sql.append("       and not exists  ");
    		sql.append("            (select 1 from T_CRS_PARTY t,    ");
    		sql.append("                          t_crs_right_notify nt            ");
    		sql.append("               where t.policy_code  = dueList.POLICY_CODE  ");
    		sql.append("                 and t.name = dueList.target_name          ");
    		sql.append("                 and t.CERTI_CODE IS NULL ");
    		sql.append("                 and t.doc_sign_status = '01'              ");
    		sql.append("                 and t.build_type in ('01','05','06')      ");
    		sql.append("                 and nt.crs_id = t.crs_id                  ");
    		sql.append("                 and nt.update_time >= :process_date       ");
    		sql.append("               )   ");
    				//排除CRS主檔 02-已簽署, 03-不願簽署                            
    		sql.append("       and not exists   ");
    		sql.append("             (select 1 from T_CRS_PARTY t                  ");
    		sql.append("               where t.policy_code  = dueList.POLICY_CODE  ");
    		sql.append("                 and t.name = dueList.target_name          ");
    		sql.append("                 and t.CERTI_CODE IS NULL ");
    		sql.append("                 and t.doc_sign_status in ('02', '03')     ");
    		sql.append("                 and t.build_type not in ('03','04')       ");
    		sql.append("               )  ");
    

        itemList = nt.queryForList(sql.toString(), params, String.class);
        BatchLogUtils.addLog(LogLevel.INFO, null, String.format("[BR-CMN-CRS-029][CRS受益人變更跟催對象] list.size=  %,d " , CollectionUtils.size(itemList)));
        resultList = ListUtils.union(resultList, itemList);
        
        /* 抽取符合條件名單-理賠給付受益人跟催對象 07->11 */
        sql.setLength(0);
      //IR382042  sql.append(" select dueList.list_id || '/' || '11' ");
        sql.append(" select DISTINCT (FIRST_VALUE(dueList.list_id || '/' || '11') "); //IR382042
        sql.append(" OVER (PARTITION BY dueList.Target_Certi_Code ORDER BY dueList.target_survey_date DESC )) "); //IR382042
        sql.append("   from t_fatca_due_diligence_list dueList   ");
        sql.append("  where 1 = 1   ");        
        sql.append("   and :process_date - :followUpDays >= dueList.target_survey_date   ");   
        sql.append("   and duelist.target_survey_type = '07'  ");
        sql.append("   AND dueList.target_certi_code IS NOT NULL ");
           //理賠受理編號clob存在
        sql.append("   and  exists (select 1 from t_clob c where c.clob_id = duelist.REPORT_ID and c.content like '%caseId%')  ");
        sql.append("    and dueList.survey_system_type in ('CRS', 'CRS/FATCA')  ");
        // 理賠給付受益人跟催對象，排除已經做過的資料重複產生-->>
        sql.append("    and not exists   ");       
        sql.append("        (select 1 from t_fatca_due_diligence_list lst   ");        
        sql.append("          WHERE lst.target_certi_code = dueList.target_certi_code ");
//        sql.append("          where ((dueList.target_certi_code is not null and ");
//		sql.append("                 lst.target_certi_code = dueList.target_certi_code) ");
//		sql.append("                or dueList.target_certi_code is null )  ");
//		sql.append("            and lst.policy_id = dueList.policy_id  ");
//        sql.append("            and lst.target_name = dueList.target_name  ");
        sql.append("            and lst.survey_system_type in ('CRS', 'CRS/FATCA')  ");
        sql.append("            and lst.target_survey_type = '11'  ");
         //尚未完成批次判定
        //sql.append("            and lst.TARGET_SURVEY_DATE + pkg_pub_para.f_get_para_value(2061000051) > :process_date )  ");
        //  sql.append("            and lst.TARGET_SURVEY_DATE + :judgedDays > :process_date )  ");
        sql.append("            and (lst.TARGET_SURVEY_DATE + :judgedDays > :process_date   ");
		sql.append("                  or lst.Notify_Date = dueList.target_survey_date )  ");
		sql.append("          ) ");
         // 已通知未簽暑日已達X天  -->>
        sql.append("   and exists  ");
        sql.append("       (select 1 from T_CRS_PARTY t  ");
                   //若certi_code為空，才加比對保單+姓名
        sql.append("          WHERE t.certi_code = dueList.TARGET_CERTI_CODE  ");
//        sql.append("         where ((dueList.TARGET_CERTI_CODE is null and     ");
//        sql.append("                  t.policy_code  = dueList.POLICY_CODE and  ");
//        sql.append("                  t.name = dueList.target_name )             ");        
//        sql.append("           	   or (dueList.TARGET_CERTI_CODE is not null and   ");
//		sql.append("           	       t.certi_code = dueList.TARGET_CERTI_CODE )) ");
        sql.append("           and t.doc_sign_status = '01'   ");
        sql.append("           and t.build_type in ( '06' )  ");   //crs建檔紀錄為" 已註記"
        //sql.append("           and t.DOC_SIGN_DATE + pkg_pub_para.f_get_para_value(2061000050) <= :process_date ");
        sql.append("           and t.DOC_SIGN_DATE + :followUpDays <= :process_date ");
        sql.append("         )  ");   
           // 產生過權益通知書的(t_crs_right_notify)，不再產生  -->>
        sql.append("   and not exists  "); 
        sql.append("        (select 1 from T_CRS_PARTY t,    ");
        sql.append("                      t_crs_right_notify nt  ");
        sql.append("           WHERE t.certi_code = dueList.TARGET_CERTI_CODE ");
//        sql.append("           where ((dueList.TARGET_CERTI_CODE is null and  ");
//		sql.append("             	      t.policy_code  = dueList.POLICY_CODE and  ");
//		sql.append("             	      t.name = dueList.target_name )  ");
//		sql.append("             	   or (dueList.TARGET_CERTI_CODE is not null and  ");
//		sql.append("            	        t.certi_code = dueList.TARGET_CERTI_CODE) )  ");
        sql.append("             and t.doc_sign_status = '01'     ");
        sql.append("             and t.build_type in ('01','05','06')   ");
        sql.append("             and nt.crs_id = t.crs_id            ");
        sql.append("             and nt.update_time >= :process_date ");
        sql.append("           )   ");
          //排除CRS主檔 02-已簽署, 03-不願簽署 
        sql.append("   and not exists   ");
        sql.append("         (select 1 from T_CRS_PARTY t                          ");
        sql.append("           WHERE t.certi_code = dueList.TARGET_CERTI_CODE ");
//        sql.append("           where ((dueList.TARGET_CERTI_CODE is null and  ");
//		sql.append("             	      t.policy_code  = dueList.POLICY_CODE and  ");
//		sql.append("             	      t.name = dueList.target_name )  ");
//		sql.append("             	   or (dueList.TARGET_CERTI_CODE is not null and  ");
//		sql.append("            	        t.certi_code = dueList.TARGET_CERTI_CODE) )  ");
        sql.append("             and t.doc_sign_status in ('02', '03')   ");
        sql.append("             and t.build_type not in ('03','04')  ");
        sql.append("           )  ");
//        sql.append("   and :process_date - pkg_pub_para.f_get_para_value(2061000050) >= dueList.target_survey_date   ");   
//        sql.append("   and duelist.target_survey_type = '07'  ");
//           //理賠受理編號clob存在
//        sql.append("   and  exists (select 1 from t_clob c where c.clob_id = duelist.REPORT_ID and c.content like '%caseId%')  ");
        sql.append("  UNION  ");//若certi_code為空，才加比對保單+姓名
        sql.append("        select DISTINCT (FIRST_VALUE(dueList.list_id || '/' || '11') ");
        sql.append("                 OVER (PARTITION BY dueList.policy_id, dueList.Target_Name ORDER BY dueList.Target_Survey_Date DESC )) ");
        sql.append("          from t_fatca_due_diligence_list dueList   ");
        sql.append("         where 1 = 1 ");           
        sql.append("          and :process_date - :followUpDays >= dueList.target_survey_date ");     
        sql.append("          and duelist.target_survey_type = '07'  ");
        sql.append("          AND dueList.target_certi_code IS NULL ");
        sql.append("          and dueList.survey_system_type in ('CRS', 'CRS/FATCA')  ");
           //理賠受理編號clob存在             
        sql.append("          and  exists (select 1 from t_clob c where c.clob_id = duelist.REPORT_ID and c.content like '%caseId%')  ");       
          // 理賠給付受益人跟催對象，排除已經做過的資料重複產生-->>
        sql.append("           and not exists    ");
        sql.append("               (select 1 from t_fatca_due_diligence_list lst   ");
        sql.append("                 WHERE 1 = 1   ");
        sql.append("                   and lst.policy_id = dueList.policy_id   ");
        sql.append("                   and lst.target_name = dueList.target_name ");
        sql.append("                   and lst.target_certi_code IS NULL ");
        sql.append("                   and lst.survey_system_type in ('CRS', 'CRS/FATCA')  ");
        sql.append("                   and lst.target_survey_type = '11'  ");
          //尚未完成批次判定                                          
        //  sql.append("                   and lst.TARGET_SURVEY_DATE + :judgedDays > :process_date )     ");
        sql.append("               and (lst.TARGET_SURVEY_DATE + :judgedDays > :process_date   ");
		sql.append("                    or lst.Notify_Date = dueList.target_survey_date )  ");
		sql.append("             ) ");
        // 已通知未簽暑日已達X天  -->>                        
        sql.append("          and exists   ");
        sql.append("              (select 1 from T_CRS_PARTY t ");
        sql.append("                where t.policy_code  = dueList.POLICY_CODE ");
        sql.append("                  and t.name = dueList.target_name  ");
        sql.append("                  and t.CERTI_CODE IS NULL ");
        sql.append("                  and t.doc_sign_status = '01'  ");
        sql.append("                  and t.build_type in ( '06' ) ");      //crs建檔紀錄為" 已註記"     
        sql.append("                  and t.DOC_SIGN_DATE + :followUpDays <= :process_date ");
        sql.append("                )   ");   
        // 產生過權益通知書的(t_crs_right_notify)，不再產生  -->>    
        sql.append("          and not exists    ");
        sql.append("               (select 1 from T_CRS_PARTY t,                  ");
        sql.append("                             t_crs_right_notify nt            ");
        sql.append("                  where t.policy_code  = dueList.POLICY_CODE  ");
        sql.append("                    and t.name = dueList.target_name          ");
        sql.append("                    and t.CERTI_CODE IS NULL ");
        sql.append("                    and t.doc_sign_status = '01'              ");
        sql.append("                    and t.build_type in ('01','05','06')      ");
        sql.append("                    and nt.crs_id = t.crs_id                  ");
        sql.append("                    and nt.update_time >= :process_date       ");
        sql.append("                  )                                           ");
          //排除CRS主檔 02-已簽署, 03-不願簽署  
        sql.append("          and not exists                                      ");
        sql.append("                (select 1 from T_CRS_PARTY t                  ");
        sql.append("                  where t.policy_code  = dueList.POLICY_CODE  ");
        sql.append("                    and t.name = dueList.target_name          ");
        sql.append("                    and t.CERTI_CODE IS NULL ");
        sql.append("                    and t.doc_sign_status in ('02', '03')     ");
        sql.append("                    and t.build_type not in ('03','04')       ");
        sql.append("                  )    ");
        
        itemList = nt.queryForList(sql.toString(), params, String.class);
        BatchLogUtils.addLog(LogLevel.INFO, null, String.format("[BR-CMN-CRS-029][CRS理賠給付受益人跟催對象] list.size=  %,d " , CollectionUtils.size(itemList)));
        resultList = ListUtils.union(resultList, itemList);
        
        /* 抽取符合條件名單-要保人變更融通跟催對象  ->17 */
        sql.setLength(0);
        sql.append(" select a.list_id || '/' || '17' ");
        sql.append("  from t_proposal_rule_result a, ");
        sql.append("           t_change c,           ");
        sql.append("           T_POLICY_HOLDER_C h   ");
        sql.append("  where 1 = 1   ");
        sql.append("   and a.violated_desc like 'CRS關注險種%' ");
        sql.append("   and a.rule_status=5   ");
        sql.append("   and exists (select 1 from t_policy_change ");
        sql.append("               where master_chg_id = a.change_id ");
        sql.append("                 and service_id = 101 )      ");  //--101-更換要保人  
        sql.append("   and c.change_id = a.change_id  ");
        sql.append("   and c.CHANGE_STATUS =1       ");  // 所有變更項完成;
        sql.append("   and c.FIN_APP_TIME < :process_date +1 ");  //--變更完成日 <= 批次作業日 
        sql.append("   and h.policy_id = a.policy_id   ");
//PCR375449        sql.append("   and not exists (select 'x' from t_crs_party p ");
//PCR375449        sql.append("                   where p.certi_code =h.certi_code )  ");
        sql.append("   and not exists    ");
        sql.append("        (select 1 from t_fatca_due_diligence_list lst ");
        sql.append("          where lst.target_certi_code = h.certi_code ");        
        sql.append("             and lst.target_name = h.name  ");
        sql.append("             and lst.survey_system_type in ('CRS', 'CRS/FATCA')   ");
        sql.append("             and lst.target_survey_type = '17' ");
             //      --尚未完成批次判定之前，不重複執行
        //IR428348 --start
//IR428348-不重複產生          sql.append("             and lst.TARGET_SURVEY_DATE + pkg_pub_para.f_get_para_value(2061000051) > :process_date )  ");
        sql.append("         ) ");   //IR428348        
        //排除CRS主檔 02-已簽署, 03-不願簽署 
		sql.append("   and not exists   ");
		sql.append("         (select 1 from T_CRS_PARTY t       ");
		sql.append("           where t.certi_code = h.certi_code ");
		sql.append("             and t.name = h.name   ");
		sql.append("             and t.doc_sign_status in ('02', '03')   ");
		sql.append("             and t.build_type not in ('03','04')  ");
		sql.append("           )  ");
		//IR428348 --end
        itemList = nt.queryForList(sql.toString(), params, String.class);
                BatchLogUtils.addLog(LogLevel.INFO, null, String.format("[BR-CMN-CRS-029][CRS要保人變更融通跟催對象] list.size=  %,d " , CollectionUtils.size(itemList)));
                resultList = ListUtils.union(resultList, itemList);
      
		return resultList.toArray();
	}

	@Override
	protected int execute(String id) throws Exception {			
		String[] identityId = StringUtils.split(id, "/");
        String listId = identityId[0];
        String followupTarget = identityId[1];
        java.util.Date processDate = BatchHelp.getProcessDate();
        StringBuffer sql = new StringBuffer();
        
        if ("17".equals(followupTarget) ){
        	//要保人融通案件
        	sql.append(" select  a.policy_id, a.policy_code, a.list_id, ");
        	sql.append("          h.party_id as TARGET_ID, ");
        	sql.append("          h.certi_code as TARGET_CERTI_CODE, ");
        	sql.append("          h.name as TARGET_NAME, ");  
        	sql.append("          h.party_type as TARGET_PARTY_TYPE, ");
        	sql.append("          m.LIABILITY_STATE as POLICY_STATUS ");
        	sql.append(" from t_proposal_rule_result a, ");
        	sql.append("          T_POLICY_HOLDER h, ");
        	sql.append("          t_contract_master_c m ");
        	sql.append(" where a.list_id = :list_id ");
        	sql.append("   and h.policy_id = a.policy_id ");
        	sql.append("   and m.policy_id = a.policy_id  "); 
        	
            java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
            params.put("list_id", listId);
            List<java.util.Map<String,Object>> mapList = nt.queryForList(sql.toString(), params);
            
            if(CollectionUtils.isEmpty(mapList)) {
            	BatchLogUtils.addLog(LogLevel.ERROR, null, String.format("[BR-CMN-CRS-029][ERROR][SOURCE_DATA_NOT_FOUND-PH]  list_id := %s, followup_type := %s " , listId, followupTarget));            	
            	return JobStatus.EXECUTE_FAILED;
            }
            java.util.Map<String,Object> map = mapList.get(0); 
            FatcaDueDiligenceList data = serv.parseMapPH(map, followupTarget);
            UserTransaction ut = TransUtils.getUserTransaction();
            try {
    		    	ut.begin();
    				serv.sendCrsLetterPH(data, processDate); 
    				ut.commit();
    		}catch(Exception ex) {
    			TransUtils.rollback(ut);
    			BatchLogUtils.addLog(LogLevel.ERROR, null, String.format("[BR-CMN-CRS-029][ERROR][input data]:= %s \n %s", id, ExceptionUtils.getFullStackTrace(ex)));
    			return JobStatus.EXECUTE_FAILED;
    		}
    		return JobStatus.EXECUTE_SUCCESS;
        }else {
        	//一般盡職調查/受益人調查之跟催            
            FatcaDueDiligenceList sourcedata = dao.load(new Long(listId))  ;
            if (sourcedata == null) {
            	BatchLogUtils.addLog(LogLevel.ERROR, null, String.format("[BR-CMN-CRS-029][ERROR][SOURCE_DATA_NOT_FOUND1]  list_id := %s, followup_type := %s " , listId, followupTarget));
            	return JobStatus.EXECUTE_FAILED;
            }
            
            UserTransaction ut = TransUtils.getUserTransaction();
            try {
    					ut.begin();
    					serv.sendCrsLetter(sourcedata, sourcedata.getReportId(), processDate,followupTarget); 
    					ut.commit();
    		}catch(Exception ex) {
    			TransUtils.rollback(ut);
    			BatchLogUtils.addLog(LogLevel.ERROR, null, String.format("[BR-CMN-CRS-029][ERROR][input data]:= %s \n %s", id, ExceptionUtils.getFullStackTrace(ex)));
    			return JobStatus.EXECUTE_FAILED;
    		}
    		return JobStatus.EXECUTE_SUCCESS;
        }            
	}

	
}
