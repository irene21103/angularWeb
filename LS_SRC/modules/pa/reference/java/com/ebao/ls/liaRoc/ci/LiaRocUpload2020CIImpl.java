package com.ebao.ls.liaRoc.ci;

import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.beanutils.BeanPredicate;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.collections.PredicateUtils;
import org.apache.commons.lang.StringUtils;

import com.ebao.foundation.common.lang.exception.ExceptionUtils;
import com.ebao.foundation.commons.exceptions.ExceptionUtil;
import com.ebao.foundation.module.para.Para;
import com.ebao.foundation.module.util.i18n.CodeTable;
import com.ebao.foundation.module.web.action.ExceptionInfoUtils;
import com.ebao.ls.chl.service.AgentService;
import com.ebao.ls.chl.service.ChannelOrgService;
import com.ebao.ls.chl.vo.ChannelOrgInfo;
import com.ebao.ls.chl.vo.ChannelOrgVO;
import com.ebao.ls.cs.commonflow.data.bo.Application;
import com.ebao.ls.cs.logger.ApplicationLogger;
import com.ebao.ls.cs.logger.helper.AppLogDataHelper;
import com.ebao.ls.foundation.vo.GenericEntityVO;
import com.ebao.ls.liaRoc.LiaRocCst;
import com.ebao.ls.liaRoc.LiaRocCst.LiarocPolicyStatus;
import com.ebao.ls.liaRoc.LiaRocCst.LiarocType;
import com.ebao.ls.liaRoc.bo.LiaRocNbBatch;
import com.ebao.ls.liaRoc.bo.LiaRocUpload2020;
import com.ebao.ls.liaRoc.bo.LiaRocUpload2020VO;
import com.ebao.ls.liaRoc.bo.LiaRocUploadCoverage2020;
import com.ebao.ls.liaRoc.bo.LiaRocUploadDetail2020;
import com.ebao.ls.liaRoc.bo.LiaRocUploadDetail2020VO;
import com.ebao.ls.liaRoc.data.LiaRocNbBatchDao;
import com.ebao.ls.liaRoc.data.LiaRocUpload2020Dao;
import com.ebao.ls.liaRoc.data.LiaRocUploadCoverage2020Dao;
import com.ebao.ls.liaRoc.data.LiaRocUploadDetail2020Dao;
import com.ebao.ls.liaRoc.data.LiaRocUploadPolicyDao;
import com.ebao.ls.liaRoc.data.bo.QueryUploadDetailByParamsReq;
import com.ebao.ls.liaRoc.vo.LiaRocUpload2020SendVO;
import com.ebao.ls.liaRoc.vo.LiaRocUploadDetail2020WrapperVO;
import com.ebao.ls.pa.nb.ctrl.foa.FoaAcceptanceHelper;
import com.ebao.ls.pa.nb.util.NBUtils;
import com.ebao.ls.pa.pub.TableCst;
import com.ebao.ls.pa.pub.bs.coverage.CoverageService;
import com.ebao.ls.pa.pub.data.org.PolicyDao;
import com.ebao.ls.pa.pub.service.PolicyService;
import com.ebao.ls.pa.pub.vo.CoverageAgentVO;
import com.ebao.ls.pa.pub.vo.CoverageInsuredVO;
import com.ebao.ls.pa.pub.vo.CoveragePremium;
import com.ebao.ls.pa.pub.vo.CoverageVO;
import com.ebao.ls.pa.pub.vo.InsuredVO;
import com.ebao.ls.pa.pub.vo.PayPlanVO;
import com.ebao.ls.pa.pub.vo.PolicyHolderVO;
import com.ebao.ls.pa.pub.vo.PolicyVO;
import com.ebao.ls.pa.pub.vo.UwNotintoRecvVO;
import com.ebao.ls.prd.product.bs.productlife.ProductLifeDS;
import com.ebao.ls.prd.product.vo.ProductVO;
import com.ebao.ls.product.model.query.output.LifeProduct;
import com.ebao.ls.product.model.query.output.ProdBizCategory;
import com.ebao.ls.product.model.query.output.ProdBizCategorySub;
import com.ebao.ls.product.service.LifeProductCategoryService;
import com.ebao.ls.product.service.LifeProductService;
import com.ebao.ls.product.service.ProductVersionService;
import com.ebao.ls.pub.CodeCst;
import com.ebao.ls.pub.util.BizUtils;
import com.ebao.ls.pub.util.TGLDateUtil;
import com.ebao.ls.rstf.ci.liaroc20.Liaroc20CI;
import com.ebao.ls.rstf.ci.liaroc20.Liaroc20Cst;
import com.ebao.ls.rstf.ci.liaroc20.vo.internal.Liaroc20UploadRqInforceVO;
import com.ebao.ls.rstf.ci.liaroc20.vo.internal.Liaroc20UploadRqReceiveVO;
import com.ebao.ls.rstf.ci.liaroc20.vo.internal.Liaroc20UploadRsVO;
import com.ebao.ls.rstf.util.RstfClientUtils;
import com.ebao.ls.sc.service.foa.FoaAcceptanceCI;
import com.ebao.ls.sc.service.foa.vo.FoaPolicyVO;
import com.ebao.ls.sc.service.foa.vo.FoaPsAcceptanceVO;
import com.ebao.ls.sc.vo.AgentChlVO;
import com.ebao.ls.sc.vo.ScFoaAcceptanceVO;
import com.ebao.ls.sc.vo.ScFoaLiarocUploadVO;
import com.ebao.ls.sc.vo.ScFoaUnbProductVO;
import com.ebao.ls.uw.ds.UwNotintoRecvService;
import com.ebao.ls.ws.ci.esp.ESPWSServiceCI;
import com.ebao.ls.ws.vo.cust.liaRoc.LiaRocUploadProcessResultRootVO;
import com.ebao.pub.Cst;
import com.ebao.pub.fileresolver.writer.FixedCharLengthWriter;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.framework.ExceptionFactory;
import com.ebao.pub.framework.GenericException;
import com.ebao.pub.util.BeanUtils;
import com.ebao.pub.util.DBean;
import com.ebao.pub.util.DateUtils;
import com.ebao.pub.util.cache.Cache;
import com.ebao.pub.util.cache.CacheObjectNotFoundException;
import com.hazelcast.util.StringUtil;

public class LiaRocUpload2020CIImpl implements LiaRocUploadCI, LiaRocUpload2020CI {

	@Resource(name = PolicyService.BEAN_DEFAULT)
	private PolicyService policyService;

	@Resource(name = ESPWSServiceCI.BEAN_DEFAULT)
	private ESPWSServiceCI espWSServiceCI;

	@Resource(name = LiaRocUpload2020Dao.BEAN_DEFAULT)
	private LiaRocUpload2020Dao liaRocUpload2020Dao;

	@Resource(name = LiaRocUploadDetail2020Dao.BEAN_DEFAULT)
	private LiaRocUploadDetail2020Dao liaRocUploadDetail2020Dao;

	@Resource(name = "liaRocUploadRequestWriter")
	private FixedCharLengthWriter fixedLengthWriter;

	@Resource(name = com.ebao.ls.pa.pub.bs.PolicyService.BEAN_DEFAULT)
	private com.ebao.ls.pa.pub.bs.PolicyService policyDS;

	@Resource(name = com.ebao.ls.pa.pub.service.PolicyService.BEAN_DEFAULT)
	com.ebao.ls.pa.pub.service.PolicyService pubPolicyService;

	@Resource(name = ProductLifeDS.BEAN_DEFAULT)
	private ProductLifeDS productLifeDS;

	@Resource(name = LifeProductService.BEAN_DEFAULT)
	private LifeProductService lifeProductService;

	@Resource(name = PolicyDao.BEAN_DEFAULT)
	private PolicyDao policyDao;

	@Resource(name = FoaAcceptanceHelper.BEAN_DEFAULT)
	private FoaAcceptanceHelper foaAcceptanceHelper;

	@Resource(name = FoaAcceptanceCI.BEAN_DEFAULT)
	private FoaAcceptanceCI foaAcceptanceCI;

	@Resource(name = CoverageService.BEAN_DEFAULT)
	private CoverageService coverageService;

	@Resource(name = ChannelOrgService.BEAN_DEFAULT)
	private ChannelOrgService channelOrgService;

	@Resource(name = ProductVersionService.BEAN_DEFAULT)
	private ProductVersionService productVersionService;

	@Resource(name = AgentService.BEAN_DEFAULT)
	private AgentService agentService;

	@Resource(name = LiaRocUploadCoverage2020Dao.BEAN_DEFAULT)
	private LiaRocUploadCoverage2020Dao liaRocUploadCoverage2020Dao;

	@Resource(name = LiaRocUploadCommonService.BEAN_DEFAULT)
	private LiaRocUploadCommonService liaRocUploadCommonService;

	@Resource(name = LifeProductCategoryService.BEAN_DEFAULT)
	private LifeProductCategoryService lifeProductCategoryService;

	@Resource(name = UwNotintoRecvService.BEAN_DEFAULT)
	private UwNotintoRecvService uwNotintoRecvService;

	@Resource(name = LiaRocUploadPolicyDao.BEAN_DEFAULT)
	private LiaRocUploadPolicyDao liaRocUploadPolicyDao;

	@Resource(name = LiaRocNbBatchDao.BEAN_DEFAULT)
	private LiaRocNbBatchDao liaRocNbBatchDao;

	org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(this.getClass());
	
	@Resource(name = Liaroc20CI.BEAN_DEFAULT)
	protected Liaroc20CI liaroc20CI;

	/**
	 * <p>Description : 理賠公會通報</p>
	 * <p>Created By : Simon Huang</p>
	 * <p>Create Time : 2020年11月18日</p>
	 * @param policyId
	 * @param itemIdPolicyStatusMap<itemId, 通報保單狀況{50,51,52}>
	 * @param liaRocUploadType 收件1/承保2
	 * @param itemEffectStatusDate<itemId, 通報保單狀況生效日>
	 * @param itemApplyDate<itemId, 要保日期(一0七條理賠)>
	 */
	public void claimSaveUpload(Long policyId, Map<Long, String> itemIdPolicyStatusMap, String liaRocUploadType,
					Map<Long, Date> itemEffectStatusDate, Map<Long, Date> itemApplyDate) {
		@SuppressWarnings("deprecation")
		PolicyVO policyVO = policyService.loadPolicyByPolicyId(policyId);

		if (policyVO == null) {
			throw new IllegalArgumentException("Policy ID " + policyId + " does not exists in system.");
		}

		// make 主檔 vo
		LiaRocUpload2020SendVO uploadVO = new LiaRocUpload2020SendVO();
		uploadVO.setPolicyId(policyId);
		//change by sunny: 理賠用3
		uploadVO.setBizSource(LiaRocCst.LIAROC_UL_BIZ_SOURCE_CLM);
		uploadVO.setBizId(policyId);
		uploadVO.setBizCode(policyVO.getPolicyNumber());
		uploadVO.setLiaRocUploadType(liaRocUploadType);
		uploadVO.setInputUser(AppContext.getCurrentUser().getUserId());
		uploadVO.setLiaRocUploadStatus(LiaRocCst.LIAROC_UL_STATUS_WAITING);
		uploadVO.setPolicyCode(policyVO.getPolicyNumber());
		// get 明細資料 vo。
		// setup 通報明細資料
		List<LiaRocUploadDetail2020VO> details = new ArrayList<LiaRocUploadDetail2020VO>();
		for (CoverageVO coverage : policyVO.getCoverages()) {
			if (this.needUploadItem(coverage, liaRocUploadType) && itemIdPolicyStatusMap.get(coverage.getItemId()) != null) {
				LiaRocUploadDetail2020WrapperVO detailMapVO = this.generateUploadDetail(policyVO, coverage, liaRocUploadType);
				if (StringUtils.isNotEmpty(detailMapVO.getLiaRocProductType())) {
					detailMapVO.setLiaRocPolicyStatus(itemIdPolicyStatusMap.get(coverage.getItemId()));
					detailMapVO.setInternalId(getInternalIdByProuctId(coverage.getProductId()));
					Date effectStatusDate = itemEffectStatusDate.get(coverage.getItemId());
					Date applyDate = itemApplyDate.get(coverage.getItemId());
					detailMapVO.setStatusEffectDate(effectStatusDate);
					detailMapVO.setApplyDate(applyDate);
					detailMapVO.setBrbdType(LiaRocCst.LIAROC_BRBD_TYPE_00);
					details.add(detailMapVO);
				}
			}
		}
		uploadVO.setDataList(details);
		this.saveUpload(uploadVO);
	}

	@Override
	public LiaRocUpload2020SendVO getUploadVOByPolicyId(Long policyId, String liaRocUploadType, String bizSource) {

		PolicyVO policyVO = policyDS.retrieveById(policyId, false);

		if (policyVO == null) {
			throw new IllegalArgumentException("Policy ID " + policyId + " does not exists in system.");
		}

		LiaRocUpload2020SendVO uploadVO = createLiarocUploadMaster(policyVO, liaRocUploadType, bizSource);

		// get 明細資料 vo。
		List<GenericEntityVO> details = (List<GenericEntityVO>) this.getDetailList(policyVO, liaRocUploadType);
		uploadVO.setDataList(details);
		if (details.isEmpty()) {
			uploadVO.setLiaRocUploadStatus(LiaRocCst.LIAROC_UL_STATUS_SEND_NO_ITEM);
			uploadVO.setRequestTransUUid(LiaRocCst.LIAROC_NO_ITEM);
			uploadVO.setLiaRocUploadStatus902(LiaRocCst.LIAROC_UL_STATUS_SEND_NO_ITEM);
			uploadVO.setRequestTransUUid902(LiaRocCst.LIAROC_NO_ITEM);	
		}

		return uploadVO;

	}

	@Override
	public Long saveUpload(LiaRocUpload2020SendVO liaRocUploadVO) {

		List<? extends GenericEntityVO> dataList = liaRocUploadVO.getDataList();
		boolean isSaveDetail = true;
		if (dataList == null || dataList.size() == 0) {
			liaRocUploadVO.setLiaRocUploadStatus(LiaRocCst.LIAROC_UL_STATUS_SEND_NO_ITEM);
			liaRocUploadVO.setRequestTransUUid(LiaRocCst.LIAROC_NO_ITEM);
			liaRocUploadVO.setLiaRocUploadStatus902(LiaRocCst.LIAROC_UL_STATUS_SEND_NO_ITEM);
			liaRocUploadVO.setRequestTransUUid902(LiaRocCst.LIAROC_NO_ITEM);			
			isSaveDetail = false;
		} else {
			//PCR 188901 承保通報,依收件通報的保項通報號作承保通報
			if (LiaRocCst.LIAROC_UL_TYPE_INFORCE.equals(liaRocUploadVO.getLiaRocUploadType())) {
				for (GenericEntityVO genericEntityVO : dataList) {
					LiaRocUploadDetail2020VO detailVO = (LiaRocUploadDetail2020VO) genericEntityVO;
					String liarocPolicyCode = this.getLiaRocPolicyCode(liaRocUploadVO.getPolicyId(), detailVO.getItemId(), detailVO.getInternalId());
					detailVO.setLiaRocPolicyCode(liarocPolicyCode);
				}
			}
			//PCR 188901:新契約承保通報作業,於收件通報因會有AFINS以受理編號通報但新契約未輸入受理編號以保單號通報
			//承保通報需重新以新契約是否有輸入受理編號統一所有保項的通報編號,才不會有受理編號及保單號混合通報的情形
			if (LiaRocCst.LIAROC_UL_TYPE_INFORCE.equals(liaRocUploadVO.getLiaRocUploadType())
							&& LiaRocCst.LIAROC_UL_BIZ_SOURCE_UNB.equals(liaRocUploadVO.getBizSource())) {
				/* mark by Simon
				 * 2019/11/13 程式上線後，因核保中的保單承保通報與收件通報會不一致，UNB Joanne 決定不執行以下邏輯
				for (GenericEntityVO genericEntityVO : dataList) {
					LiaRocUploadDetailVO detailVO = (LiaRocUploadDetailVO) genericEntityVO;
					String bfLiarocPolicyCode = detailVO.getPolicyId();
					String afLiarocPolicyCode = this.syncNBInforceLiaRocPolicyCode(
									liaRocUploadVO.getPolicyId(), 
									detailVO.getItemId(),
									bfLiarocPolicyCode);
					detailVO.setPolicyId(afLiarocPolicyCode);
				}
				*/
				//新契約承保通報設定902需上傳
				if (liaRocUploadVO.getSend902Flag() == null){
					liaRocUploadVO.setSend902Flag(LiaRocCst.LIAROC_UL_SEND_902_FLAG_1);
				}
				if (liaRocUploadVO.getLiaRocUploadStatus902() == null){
					liaRocUploadVO.setLiaRocUploadStatus902(LiaRocCst.LIAROC_UL_STATUS_WAITING);
				}
				
			}
			//end PCR 188901
		}

		LiaRocUpload2020 liaRocUploadBO = new LiaRocUpload2020();
		BeanUtils.copyProperties(liaRocUploadBO, liaRocUploadVO);

		// 儲存主檔資料
		liaRocUpload2020Dao.save(liaRocUploadBO);

		liaRocUploadVO.setListId(liaRocUploadBO.getListId());

		if (isSaveDetail) {
			// 儲存明細資料
			this.saveUploadDetail(liaRocUploadVO, liaRocUploadVO.getDataList());
		}

		return liaRocUploadBO.getListId();

	}

	public void saveAcceptanceUploadPos(long aceptId) throws Exception {
		FoaPsAcceptanceVO vo = foaAcceptanceCI.findFoaCsCaseByCaseId(aceptId);
		saveAcceptanceUploadPos(vo);
	}

	private static String FOA_UPLOAD_POS = "saveAcceptanceUploadPos";

	private void saveAcceptanceUploadPos(FoaPsAcceptanceVO vo) throws Exception {

		ApplicationLogger.setSystemCode(AppLogDataHelper.SYSTEM_CODE_UNB);
		ApplicationLogger.setJobName(FOA_UPLOAD_POS);
		ApplicationLogger.setOptionId(DateUtils.date2String(new Date(), "yyyyMMdd HHmmss"));

		try {
			ScFoaAcceptanceVO acceptanceVO = vo.getAcceptanceVO();
			String policyCode = acceptanceVO.getPolicyCode();

			ApplicationLogger.setPolicyCode(policyCode);
			ApplicationLogger.addLoggerData("acceptanceId=" + acceptanceVO.getAceptId());

			PolicyVO fixPolicyVO = policyDS.retrieveByPolicyNumber(policyCode);
			Long channelOrgId = acceptanceVO.getSalesOrg();
			if (channelOrgId != null) {
				fixPolicyVO.setChannelOrgId(channelOrgId);
				fixPolicyVO.setChannelType(channelOrgService.load(channelOrgId).getChannelType());
			}
			fixPolicyVO.setSubmissionDate(acceptanceVO.getAceptDate());

			//PCR-337606 判斷FOA 會有重複送件問題，所以要檢核，第二筆壓 D 2020/09/09 Add by Kathy
			String liarocUploadStatus = LiaRocCst.LIAROC_UL_STATUS_WAITING;
			int count = countFoaLiarocUpload(vo.getAcceptanceVO().getAceptId(), LiaRocCst.LIAROC_UL_BIZ_SOURCE_FOA_POS);
			if (count > 0) {
				liarocUploadStatus = LiaRocCst.LIAROC_UL_STATUS_SEND_DELETE;
			}

			// 公會通報主檔
			LiaRocUpload2020SendVO info = new LiaRocUpload2020SendVO();
			/** 受理ID當key **/
			info.setBizId(vo.getAcceptanceVO().getAceptId());
			info.setBizCode(policyCode);
			info.setPolicyCode(policyCode);
			info.setPolicyId(fixPolicyVO.getPolicyId());
			info.setBizSource(LiaRocCst.LIAROC_UL_BIZ_SOURCE_FOA_POS);
			info.setLiaRocUploadType(LiaRocCst.LIAROC_UL_TYPE_RECEIVE);
			info.setInputUser(AppContext.getCurrentUser().getUserId());
			info.setLiaRocUploadStatus(liarocUploadStatus);
			ScFoaAcceptanceVO acceptVO = vo.getAcceptanceVO();
			info.setChannelOrgId(acceptVO.getDeliverAgentChannelOrgId()); //送件業務員單位

			info.setReceiveDate(acceptVO.getAceptDate());
			Long agentId = acceptVO.getDeliverAgent();
			if (agentId != null) {
				AgentChlVO agentVO = agentService.findAgentByAgentId(agentId);
				if (agentVO != null) {
					info.setRegisterCode(agentVO.getRegisterCode());
				}
			}
			//主約的繳別
			String initialType = fixPolicyVO.gitMasterCoverage().getCurrentPremium().getPaymentFreq();
			//清除原本所有險種資料,加入加保的CoverageVO
			List<CoverageVO> coverages = fixPolicyVO.getCoverages();
			coverages.clear();
			//取得 ITEM 最大值
			int itemOrder = policyDao.generateItemOrderByPolicyIdNoWaiver(fixPolicyVO.getPolicyId());

			//IR-254758-延遲通報表報(FOA POS收件通報,生效日與保障終止日因FOA填入資訊有限，以通報受理日作生效日，一年期作終止日通報)
			Calendar expireDate = Calendar.getInstance();
			expireDate.setTime(acceptanceVO.getAceptDate());
			expireDate.add(Calendar.YEAR, 1);

			List<ScFoaLiarocUploadVO> liarocList = vo.getScFoaLiarRocUploadList();
			for (ScFoaLiarocUploadVO scVo : liarocList) {

				CoverageVO coverage = new CoverageVO();
				coverage.setApplyDate(acceptVO.getApplyDate());
				coverage.setItemOrder(itemOrder++);
				coverage.setProductId(scVo.getProductId().intValue());
				coverage.setInceptionDate(acceptanceVO.getApplyDate());//IR-254758-受理日期作生效日
				coverage.setExpiryDate(expireDate.getTime()); //IR-254758-一年期作終止日通報
				//設定商品版本. 給附金額會用到
				coverage.setProductVersionId(productVersionService.getProductVersion(scVo.getProductId(), coverage.getApplyDate()).getVersionId());
				//設定保額/計畫/單位
				CoveragePremium currentPremium = coverage.getCurrentPremium();
				currentPremium.setSumAssured(scVo.getAmount());
				currentPremium.setUnit(scVo.getUnit());
				currentPremium.setBenefitLevel(scVo.getBenefitLevel());
				//TODO 給附金額會用到繳費年期
				coverage.setChargePeriod(CodeCst.CHARGE_PERIOD__CERTAIN_YEAR);
				coverage.setChargeYear(1);

				BigDecimal prem = scVo.getPremAmt();
				coverage.getCurrentPremium().setTotalPremAf(scVo.getPremAmt());
				if (prem != null && prem.compareTo(BigDecimal.ZERO) > 0) {
					//新增附約的角別會跟主約一樣					
					if (CodeCst.CHARGE_MODE__MONTHLY.equals(initialType)) {
						currentPremium.setStdPremAn(
										prem.multiply(new BigDecimal("6")));
					} else if (CodeCst.CHARGE_MODE__QTRLY.equals(initialType)) {
						currentPremium.setStdPremAn(
										prem.multiply(new BigDecimal("4")));
					} else if (CodeCst.CHARGE_MODE__HALF_YEARLY
									.equals(initialType)) {
						currentPremium.setStdPremAn(
										prem.multiply(new BigDecimal("2")));
					} else {
						currentPremium.setStdPremAn(prem);
					}
				}
				//設定被保人資訊
				InsuredVO insured = new InsuredVO();
				insured.setCertiCode(scVo.getCertiCode());
				insured.setName(scVo.getName());
				if (fixPolicyVO.getInsureds() != null) {
					for (InsuredVO insuredVO : fixPolicyVO.getInsureds()) {
						if (NBUtils.in(insuredVO.getCertiCode(), scVo.getCertiCode())) {
							insured = insuredVO;
							break;
						}
					}
				}

				CoverageInsuredVO coverInsured = new CoverageInsuredVO();
				coverInsured.setOrderId(1);
				coverInsured.setInsured(insured);
				List<CoverageInsuredVO> insureds = new ArrayList<CoverageInsuredVO>();
				insureds.add(coverInsured);
				coverage.setInsureds(insureds);
				coverages.add(coverage);
			}
			List<LiaRocUploadDetail2020VO> newDetails = (List<LiaRocUploadDetail2020VO>) this
							.getDetailList(fixPolicyVO, LiaRocCst.LIAROC_UL_TYPE_RECEIVE,
											true);

			for (LiaRocUploadDetail2020VO detailVO : newDetails) {
				detailVO.setStatusEffectDate(AppContext.getCurrentUserLocalTime());//IR-254758-POS 收件通報–延遲通報管理報表
				//detailBO.setValidateDate(acceptanceVO.getAceptDate());//IR-254758-受理日期作生效日
				//20180824 Email-要保書/契變書填寫日為生效日
				//like:T400019700
				//t_sc_foa_acceptance.apply_date=2018/08/22
				//t_contract_product.actual_validate_date=2018/08/22
				detailVO.setValidateDate(acceptanceVO.getApplyDate());
				detailVO.setApplyDate(acceptanceVO.getApplyDate());
				//20180824 Email-滿期日清空
				detailVO.setExpiredDate(null);//Email-
				//liaRocUploadDetailDao.save(detailBO);

			}
			info.setDataList(newDetails);

			//20180824 Email-POS收件通報作AFINS POS收件通報比對
			//POS 只用到policyVO.policyCode,所以直接傳入
			Long uploadListId = this.saveReceiveUploadData(fixPolicyVO, info);

			ApplicationLogger.addLoggerData("uploadId=" + uploadListId);

		} catch (Exception e) {
			logger.error(ExceptionUtils.getFullStackTrace(e));
			ApplicationLogger.addLoggerData(ExceptionUtils.getFullStackTrace(e));
			ApplicationLogger.setReturnCode(ApplicationLogger.RETURN_CODE_FAIL);
			try {
				ApplicationLogger.flush();
			} catch (Exception e1) {
				logger.error(e1);
			}
			throw e;
		} finally {
			try {
				ApplicationLogger.flush();
			} catch (Exception e) {
				logger.error(e);
			}
		}
	}

	/**
	 * <p>取得通報明細檔資料，同時會將保單及保項資料轉換為符合公會的資料格式</p>
	 * <p>Created By : Amy Hung</p>
	 * <p>Create Time : Mar 7, 2016</p>
	 * @param policyVO 保單 VO
	 * @param liaRocUploadType 通報種類: 1.收件, 2.承保, 3.107通報(理賠)
	 * @return
	 */
	public List<? extends GenericEntityVO> getDetailList(PolicyVO policyVO, String liaRocUploadType) {
		return getDetailList(policyVO, liaRocUploadType, false);
	}

	/**
	 * <p>取得通報明細檔資料，同時會將保單及保項資料轉換為符合公會的資料格式</p>
	 * <p>Created By : Amy Hung</p>
	 * <p>Create Time : Mar 7, 2016</p>
	 * @param policyVO 保單 VO
	 * @param liaRocUploadType 通報種類: 1.收件, 2.承保, 3.107通報(理賠)
	 * @return
	 */
	public List<? extends GenericEntityVO> getDetailList(PolicyVO policyVO, String liaRocUploadType, boolean isFoa) {

		List<LiaRocUploadDetail2020VO> details = new ArrayList<LiaRocUploadDetail2020VO>();

		// setup 通報明細資料
		for (CoverageVO coverage : policyVO.gitSortCoverageList()) {
			//保全新增附約中coverage.getLifeInsured1() is null會拋出null exception，因此不通報
			if(coverage.getLifeInsured1() != null) {
				if (this.needUploadItem(coverage, liaRocUploadType)) {
					LiaRocUploadDetail2020WrapperVO detailMapVO = this.generateUploadDetail(policyVO, coverage, liaRocUploadType, false);
					if (!StringUtil.isNullOrEmpty(detailMapVO.getLiaRocProductCategory())
									&& !StringUtil.isNullOrEmpty(detailMapVO.getLiaRocProductType())) {
						details.add(detailMapVO);
					}
				}
			} else {
				NBUtils.appLogger("無LifeInsured1 保項不通報,item_id="+ coverage.getItemId());
			}
		}

		return details;

	}

	/**
	 * <p>儲存通報明細檔資料</p>
	 * <p>若通報明細資料不是收件或承保的格式，可覆寫此方法，自行撰寫儲存明細資料的邏輯</p>
	 * <p>Created By : Amy Hung</p>
	 * <p>Create Time : Mar 7, 2016</p>
	 * @param liaRocUploadVO 通報主檔資料
	 * @param detailList 通報明細資料
	 */
	protected void saveUploadDetail(LiaRocUpload2020SendVO liaRocUploadVO, List<? extends GenericEntityVO> detailList) {

		for (GenericEntityVO detailVO : detailList) {

			LiaRocUploadDetail2020 detailBO = new LiaRocUploadDetail2020();
			BeanUtils.copyProperties(detailBO, detailVO);
			detailBO.setUploadListId(liaRocUploadVO.getListId());

			liaRocUploadDetail2020Dao.save(fixDetailNameLength(detailBO));

		}

	}

	@Override
	@SuppressWarnings("unchecked")
	public List<LiaRocUploadDetail2020WrapperVO> findUploadDetailDataByCriteria(List<String> bizSource, String liarocUploadType,
					String orderBy, boolean isOrderByAsc) {

		List<LiaRocUploadDetail2020> boResult = liaRocUploadDetail2020Dao.findUploadDetailDataByCriteria(bizSource,
						liarocUploadType, orderBy, isOrderByAsc);

		List<LiaRocUploadDetail2020WrapperVO> voList = new ArrayList<LiaRocUploadDetail2020WrapperVO>();
		voList = (List<LiaRocUploadDetail2020WrapperVO>) BeanUtils.copyCollection(LiaRocUploadDetail2020WrapperVO.class,
						boResult);

		return voList;

	}

	@Override
	public void updateUploadStatus(Map<Long, String> uploadStatus, String reqUUID, int serialNum, Date requestTime,
					Long uploadId) {
		liaRocUpload2020Dao.updateUploadStatus(uploadStatus, reqUUID, serialNum, requestTime);
	}

	@Override
	public List<LiaRocUpload2020SendVO> send(List<LiaRocUpload2020SendVO> uploadVOs, String uploadType, String uploadModule)
					throws GenericException {
		//待實作
		return null;
	}

	private Boolean isPOSMan(Long agentId) {
		if (agentId == null)
			return Boolean.FALSE;
		AgentChlVO agentChlVO = this.agentService.findAgentByAgentId(agentId);
		if (agentChlVO == null)
			return Boolean.FALSE;
		return Integer.valueOf("3").equals(agentChlVO.getBusinessCate());
	}

	/**
	 * see 受理編號107041306921
	 * CUST_APP_TIME(客戶申請日)	2018/03/26
	 * APPLY_TIME	(系統受理日)	2018/03/28
	 * 
	 * 保全收件通報的保單需要符合以下條件：
	 * i.	保全項目=新增附約（含豁免險）；
	 * ii.	保全狀態=錄入完成；
	 * iii.保全錄入完成日期=系統日期
	 * 依照送件單位處理如下 : 
	 * (1)	送件單位 : 空白或非簽約保經代。
	 * 比對未通報部份進行通報：收件通報系統(AFINS)與核心系統的資料比對【被保險人ID】+【系統受理日】+【險種代碼】，收件通報系統(AFINS)未通報者。
	 * 當比對相同時，視為“收件通報系統”已通報 ,核心系統就不須再通報。
	 * (2)	送件單位 : 直營通路AGY、STD、保全員。
	 * 比對未通報部份進行通報：收件通報系統(AFINS)與核心系統的資料比對【被保險人ID】+【客戶申請日】+【險種代碼】，收件通報系統(AFINS)未通報者。
	 * 當比對相同時，視為“收件通報系統”已通報 ,核心系統就不須再通報。
	 * (3)	送件單位 : 保經代代碼及BD，加保保項分類為：97-保經代傳真件、99-保經代壽險與傷害險。
	 * 比對未通報部份進行通報：收件通報系統(AFINS)與核心系統的資料比對【被保險人ID】+【客戶申請日】+【險種代碼】，收件通報系統(AFINS)未通報者。
	 * 當比對相同時，視為“收件通報系統”已通報 ,核心系統就不須再通報。
	 * (4)	送件單位 : 保經代代碼及BD，加保保項分類為：98-保經代健康險及傷害醫療險。
	 * 比對未通報部份進行通報：收件通報系統(AFINS)與核心系統的資料比對【被保險人ID】+【系統受理日】+【險種代碼】，收件通報系統(AFINS)未通報者。
	 * 當比對相同時，視為“收件通報系統”已通報 ,核心系統就不須再通報。
	 * c. 已完成收件通報件，系統自動判斷以下：
	 * i.	新契約通報件 : 要保書狀態如為拒絕、延期、撤件
	 * ii.	保全處理狀態 : Cancel、Rejected
	 * 該筆保單資料須再進行收件通報 以06-未承保取消件。
	 * (參考ATN-COR-UNB-BSD-005-PCR 承保作業-v0.2 BR-UNB-INF-002 儲存和更新承保資訊 描述)
	 * d. 新契約與保全通報資料，如被保險人身分證字號、生日錯誤，以12-鍵值欄位通報錯誤終止，並以15-通報更正重新通報。
	 * 
	 * 問題單241196是承保通報取值問題
	 * 收件通報【狀況生效日】&【契約生效日】應帶入【實際生效日】之欄位值非【生效日】之欄位值
	 */
	@Override
	public LiaRocUploadDetail2020WrapperVO generateCSUploadDetail(Application application, PolicyVO policy, CoverageVO coverage,
					String liaRocUploadType) {

		if (LiaRocCst.LIAROC_UL_TYPE_RECEIVE.equals(liaRocUploadType)) {
			/*
			//	 * CUST_APP_TIME(客戶申請日)	2018/03/26
			//	 * APPLY_TIME	(系統受理日)	2018/03/28	
			 */
			Long sendUnit = application.getSendUnit();
			ChannelOrgVO channelVO = sendUnit != null ? channelOrgService.getChannelOrg(sendUnit) : null;
			Boolean isPOSMan = isPOSMan(application.getSendPerson());

			//IR-275159-送件單位作保項分類
			if (channelVO != null) {
				if (NBUtils.in(channelVO.getChannelType().toString(),
								CodeCst.SALES_CHANNEL_TYPE__BD, CodeCst.SALES_CHANNEL_TYPE__BR)) {
					policy.setChannelOrgId(sendUnit);
					policy.setChannelType(channelVO.getChannelType());
				}
			}

			LiaRocUploadDetail2020WrapperVO wrapperVO = generateUploadDetail(policy, coverage, liaRocUploadType);

			if (channelVO == null) {
				//(1)	送件單位 : 空白或非簽約保經代。【系統受理日】
				wrapperVO.setValidateDate(application.getApplyTime());
			} else if (NBUtils.in(channelVO.getChannelType().toString(), CodeCst.SALES_CHANNEL_TYPE__BD, CodeCst.SALES_CHANNEL_TYPE__BR)) {
				Boolean isNotContractChannel = NBUtils.isNotContractUnit(channelVO.getChannelCode());
				if (isNotContractChannel) {
					//(1)	送件單位 : 空白或非簽約保經代。【系統受理日】
					wrapperVO.setValidateDate(application.getApplyTime());
				} else {

					if (NBUtils.in(wrapperVO.getBrbdType(), LiaRocCst.LIAROC_BRBD_TYPE_01, LiaRocCst.LIAROC_BRBD_TYPE_03)) {
						//(3)	送件單位 : 保經代代碼及BD，加保保項分類為：01-保經代傳真件、03-保經代壽險與傷害險。【客戶申請日】
						wrapperVO.setValidateDate(application.getCustAppTime());
					} else if (NBUtils.in(wrapperVO.getBrbdType(), LiaRocCst.LIAROC_BRBD_TYPE_02)) {
						//(4)	送件單位 : 保經代代碼及BD，加保保項分類為：02-保經代健康險及傷害醫療險。【系統受理日】		
						wrapperVO.setValidateDate(application.getApplyTime());
					} else {
						//其它
						wrapperVO.setValidateDate(application.getCustAppTime());
					}
				}
			} else if (NBUtils.in(channelVO.getChannelType().toString(),
							CodeCst.SALES_CHANNEL_TYPE__AGY, CodeCst.SALES_CHANNEL_TYPE__STD, CodeCst.SALES_CHANNEL_TYPE__HO)
							|| isPOSMan) {
				//(2)	送件單位 : 直營通路AGY、STD、HO、保全員。【客戶申請日】
				wrapperVO.setValidateDate(application.getCustAppTime());
			}

			wrapperVO.setApplyDate(wrapperVO.getValidateDate());

			//保單狀況生效日期，保全變更時，此欄位請填異動生效日期(系統日期)
			wrapperVO.setStatusEffectDate(AppContext.getCurrentUserLocalTime());

			return wrapperVO;
		} else {
			LiaRocUploadDetail2020WrapperVO wrapperVO = generateUploadDetail(policy, coverage, liaRocUploadType);

			//保全收件通報使用實際生效日
			if (coverage.getActualInceptionDate() != null) {
				wrapperVO.setValidateDate(coverage.getActualInceptionDate());
				//2018/06/29 Zoe email, BSD如下： 
				//保單狀況生效日期，保全變更時，此欄位請填異動生效日期(系統日期)
				wrapperVO.setStatusEffectDate(AppContext.getCurrentUserLocalTime());
			}
			return wrapperVO;
		}

	}

	public LiaRocUploadDetail2020WrapperVO generateUploadDetail(PolicyVO policy, CoverageVO coverage,
					String liaRocUploadType) {
		return generateUploadDetail(policy, coverage, liaRocUploadType, false);
	}

	/**
	 * <p>Description : </p>
	 * <p>Created By : Simon Huang</p>
	 * <p>Create Time : 2020年2月24日</p>
	 * @param policy
	 * @param coverage
	 * @param liaRocUploadType
	 * @param isAfinsLiarocPolicyCode 是否以AFINS，格式保項通報編號A01,A02
	 * @return
	 */
	public LiaRocUploadDetail2020WrapperVO generateUploadDetail(PolicyVO policy, CoverageVO coverage,
					String liaRocUploadType, boolean isAfinsLiarocPolicyCode) {

		LifeProduct lifeProduct = lifeProductService.getProductByVersionId(coverage.getProductVersionId());
		//取得險種的要保書分類
		ProdBizCategorySub sub = pubPolicyService.getProposalProdCategory(lifeProduct.getProduct().getProductId());
		//商品設定商品小類
		Map<ProdBizCategory, ProdBizCategorySub> prodBizCategory = lifeProductCategoryService.findProdBizCategories(lifeProduct.getProduct().getProductId());
		ProdBizCategorySub prodBizCategorySub = prodBizCategory.get(ProdBizCategory.MAIN_02);

		LiaRocUploadDetail2020WrapperVO detailVo = new LiaRocUploadDetail2020WrapperVO();

		detailVo.setItemId(coverage.getItemId());
		detailVo.setProductId(Long.valueOf(coverage.getProductId()));
		detailVo.setProductVersionId(Long.valueOf(coverage.getProductVersionId()));

		// 產壽險別
		detailVo.setLiaRocType("1".equals(liaRocUploadType) ? LiaRocCst.RECEIVE_LIAROC_TYPE_LIFE
						: LiaRocCst.INFORCE_LIAROC_TYPE_LIFE);

		// 保險公司代號
		detailVo.setLiaRocCompanyCode(Para.getParaValue(LiaRocCst.PARAM_LIAROC_TGL_COMPANY_CODE));
		Date birthday = null;
		String isGuardianAncmnt = null;

		PolicyHolderVO holder = policy.getPolicyHolder();

		// setup 被保險人資料
		InsuredVO masterInsured = policy.gitMasterInsured();
		if (coverage.getLifeInsured1() != null) {

			InsuredVO insured = coverage.getLifeInsured1().getInsured();
			if (masterInsured == null) {
				masterInsured = insured;
			}
			// 被保險人姓名
			String insuredName = insured.getName();
			String insuredRomanName = insured.getRomanName();
			if (!StringUtil.isNullOrEmpty(insuredRomanName)) {
				insuredName = insuredName + insuredRomanName;
			}
			// 取10個中文字	
			insuredName = substring(insuredName, 30);
			detailVo.setName(insuredName);

			// 被保險人身份證號
			detailVo.setCertiCode(insured.getCertiCode());
			birthday = insured.getBirthDate();
			// 被保險人出生年月日
			if (!NBUtils.isEmptyOrDummyDate(birthday)) {
				detailVo.setBirthday(birthday);
			}

			// 被保險人性別
			detailVo.setGender(liaRocUploadCommonService.getLiaRocTargetCodeValue("T_GENDER", insured.getGender(), "sex"));

			//被保人是否受有監護宣告
			isGuardianAncmnt = insured.getGuardianAncmnt();

			// 要保人與被保險人關係;
			detailVo.setLiaRocRelationToPh(liaRocUploadDetail2020Dao.getLiaRocRelationToPH(
							holder.getPartyType(),
							masterInsured.getRelationToPH(),
							insured.getInsuredCategory(),
							insured.getNbInsuredCategory()));
			
			//要/被保人CertiCode 相同時，關係＝01 本人
			if (insured.getCertiCode().equals(policy.getPolicyHolder().getCertiCode())){
				detailVo.setLiaRocRelationToPh("01"); //本人
			}

			// 給付金額的計算(非台幣需依要保書填寫日換匯至台幣)
			long insuredAge = 999L;
			if (insured.getBirthDate() != null && policy.getApplyDate() != null) {
				insuredAge = BizUtils.getAge(CodeCst.AGE_METHOD__ALAB, insured.getBirthDate(), policy.getApplyDate());
			}
			detailVo = this.calcLiabilityMoney(detailVo, coverage,
							lifeProduct.getProduct().getBaseMoney(), insuredAge, liaRocUploadType, isGuardianAncmnt);

		}

		// 來源別
		if (policy.isOIU()) {
			detailVo.setSourceType("1"); //OIU保單
		} else {
			detailVo.setSourceType("0"); //無
		}

		// 銷售通路別
		String salesChannel = "2"; //業務員
		if (NBUtils.isBRBD(String.valueOf(policy.getChannelType())) ||
						StringUtils.isEmpty(policy.getChannelType().toString())) {
			salesChannel = "3"; //保經、保代
		}
		if (CodeCst.SUBMIT_CHANNEL__EC.equals(policy.getSubmitChannel())) {
			salesChannel = "1"; //網路投保
		}
		detailVo.setSalesChannel(salesChannel);

		// 商品代碼(QLW-Q)
		String internalId = getInternalIdByProuctId(coverage.getProductId());

		String productVersion = "";
		if (coverage.getProductVersionId() != null) {
			productVersion = productVersionService.getProductVersion(coverage.getProductId().longValue(),
							coverage.getProductVersionId()).getProductVersion();
		} else {
			productVersion = productVersionService.getProductVersion(coverage.getProductId().longValue(),
							coverage.getApplyDate()).getProductVersion();
		}

		String productCode = internalId + "-" + productVersion;
		detailVo.setInternalId(productCode);

		// 保單分類
		String policyType = "1"; //個人
		detailVo.setLiaRocPolicyType(policyType);

		// 險種分類
		detailVo.setLiaRocProductCategory(this.getLiaRocProdCateCode(Long.valueOf(coverage.getProductId())));
		if (detailVo.getLiaRocProductCategory() == null)
			detailVo.setLiaRocProductCategory("");

		// 險種
		detailVo.setLiaRocProductType(this.getLiaRocProdCode(Long.valueOf(coverage.getProductId())));
		if (detailVo.getLiaRocProductType() == null)
			detailVo.setLiaRocProductType("");

		// 公、自費件
		detailVo.setPayType("0"); //無

		Date validateDate = policy.getApplyDate();

		boolean isMicro = lifeProductCategoryService.isMicro(coverage.getProductId().longValue());

		if (LiaRocCst.LIAROC_UL_TYPE_RECEIVE.equals(liaRocUploadType)) {
			//保經代分類
			Long salesChannelId = policy.getChannelType();
			String channelExtInt = policyDS.getChannelExInt(salesChannelId);

			if (CodeCst.YES_NO__YES.equals(channelExtInt) &&
					(!LiaRocCst.LIAROC_PRODUCT_CATEGORY_4.equals(detailVo.getLiaRocProductCategory()))) {
				//判斷是外部通路(保經代件)且非年金件;
				//01  保經代傳真件(97)
				//02  保經代健康險及傷害醫療險(98)
				//03  保經代壽險與傷害險(99)
				String isBrbdFax = policy.getIsBrbdFax();
				if ((CodeCst.YES_NO__YES).equals(isBrbdFax)) {
					//保經代傳真件(01);
					detailVo.setBrbdType(LiaRocCst.LIAROC_BRBD_TYPE_01);
				} else {
					Long productId = Long.valueOf(coverage.getProductId());
					boolean isDeathLiab = false;
					//BD/BR通路, 以商品ID判斷是否有設定在t_liaroc_product_conf
					//有存在-設定為03(保經代壽險與傷害險)
					//不存在-設定為02(保經代健康險及傷害醫療險)
					isDeathLiab = liaRocUpload2020Dao.getLiarocProductConf(productId);

					if (isDeathLiab) {
						//含死亡給付險種(03);
						detailVo.setBrbdType(LiaRocCst.LIAROC_BRBD_TYPE_03);
					} else {
						//健康/醫療險(02);
						detailVo.setBrbdType(LiaRocCst.LIAROC_BRBD_TYPE_02);
					}
				}
			} else {
				//00  無
				detailVo.setBrbdType(LiaRocCst.LIAROC_BRBD_TYPE_00);
			}
			
			//要保書填寫日
			//(1)內部通路：以「要保書填寫日」通報。;
			//(2)外部通路：保經代傳真件(01)及健康/醫療險(02)以「NB受理日期」通報：保經代分類=03-保經代壽險與傷害險以「要保書填寫日」通報；
			//(3)微型保單 收件通報以「通路受理日」通報;
			if (isMicro && !NBUtils.isEmptyOrDummyDate(policy.getSubmissionDate())) {
				validateDate = policy.getSubmissionDate();
			} else if(LiaRocCst.LIAROC_BRBD_TYPE_01.equals(detailVo.getBrbdType()) 
					|| LiaRocCst.LIAROC_BRBD_TYPE_02.equals(detailVo.getBrbdType())) {
				validateDate = policy.getReceiveDate();
			} else if(CodeCst.YES_NO__YES.equals(channelExtInt) 
					&& LiaRocCst.LIAROC_PRODUCT_CATEGORY_4.equals(detailVo.getLiaRocProductCategory())) {
				//2021/10/19 Fanny Email 保經代的年金險也由公司通報，請同直營通路以NB受理日進行通報
				validateDate = policy.getReceiveDate();
			}  else {
				validateDate = policy.getApplyDate();
			}
			
			if(validateDate == null) {
				validateDate = policy.getApplyDate();
			}			
			detailVo.setApplyDate(validateDate);
			
		} else if (LiaRocCst.LIAROC_UL_TYPE_INFORCE.equals(liaRocUploadType)) {
			validateDate = coverage.getInceptionDate();
		}
		
		//契約生效日：收件同通報的要保書填寫日期/承保通報生效日
		detailVo.setValidateDate(validateDate);

		//契約生效時分: detailVo.validateTime 固定0000

		// 契約滿期日
		//IR 186881 經與使用者討論後, 因公會偵錯"(契約滿期年-被保險人出生年)≦112。", 故若商品設定設商品為"終身"(滿期日=9999/12/31),
		//公會通報的契約滿期日期的年, 請以契約生效日期的年+112-被保險人年齡通報(含收件/承保)
		//IR 188586 因為保險年齡未滿半歲不算, 所以會有(契約滿期年-被保險人出生年)≦113 的錯誤
		//改為(生日年+112+(生效日之月/日 -1天 ))
		Date expiryDate = coverage.getExpiryDate();
		if (expiryDate != null && !NBUtils.isEmptyOrDummyDate(birthday)
						&& (DateUtils.getYear(expiryDate) - DateUtils.getYear(birthday)) > 112) {
			Date vDate = DateUtils.addDay(detailVo.getValidateDate(), -1);
			int addYear = DateUtils.getYear(birthday) + 112 - DateUtils.getYear(vDate);
			detailVo.setExpiredDate(DateUtils.addYear(vDate, addYear));
		} else {
			detailVo.setExpiredDate(expiryDate);
		}

		//契約滿期時分: detailVo.expiredTime 固定0000

		//保費(需作匯率轉換)
		if (coverage.getCurrentPremium().getTotalPremAf() != null) {
			BigDecimal prem = coverage.getCurrentPremium().getTotalPremAf();
			// 投資型商品、傳統利變年金以自定保費通報
			if (coverage.getCustomizedPrem() != null &&
							(lifeProduct.isInvestLink() || ProdBizCategorySub.SUB_02_SAN.equals(sub))) {
				prem = coverage.getCustomizedPrem();
			}

			if (CodeCst.MONEY__NTD != lifeProduct.getProduct().getBaseMoney()) {
				prem = liaRocUploadCommonService.transferToTwCurrency(lifeProduct.getProduct().getBaseMoney(),
								coverage.getApplyDate(), prem);
			}
			detailVo.setPrem(prem.longValue());
		} else {
			detailVo.setPrem(BigDecimal.ZERO.longValue());
		}

		// 保費繳別(transfer to 公會代碼)
		String initialType = coverage.getCurrentPremium().getPaymentFreq();
		if (StringUtil.isNullOrEmpty(initialType)) {
			initialType = "0"; //無
		}
		/*繳別為躉繳者，主約險種若為投資型商品 且 商品設定->服務規則->
		 *投資型規則的「可否單次追加投資」欄位=是，保費繳別通報6-彈性繳
		 *投資型規則的「可否單次追加投資」欄位=否，保費繳別通報1-躉繳
		 *繳別非躉繳者，依實際繳別通報(如.年半季月)
		 */
		if (ProdBizCategorySub.SUB_12_002.equals(sub) //投資型商品
						&& (lifeProduct.getProduct().getTopupPermit().equals(CodeCst.YES_NO__YES)) //可否單次追加投資
						&& (CodeCst.CHARGE_MODE__SINGLE.equals(initialType))) //繳別為躉繳
		{
			initialType = "6"; //彈性繳
		}
		detailVo.setLiaRocChargeMode(liaRocUploadCommonService.getLiaRocTargetCodeValue("T_CHARGE_MODE", initialType, "bamttype"));

		if (detailVo.getLiaRocChargeMode() == null) {
			detailVo.setLiaRocChargeMode("0");
		}

		//被保險人投保年齡
		CoverageInsuredVO coverageInsuredVO = coverage.getLifeInsured1();
		Integer entryAge = coverageInsuredVO.getEntryAge();
		//保費繳費年期
		boolean isOneYearRenew = coverageService.isOneYearRenew(detailVo.getProductVersionId());
		if (isOneYearRenew) {
			// 1年期商品通報1
			detailVo.setLiaRocChargeYear(1);
		} else {
			if (CodeCst.CHARGE_MODE__SINGLE.equals(initialType)) {
				// 躉繳商品通報0
				detailVo.setLiaRocChargeYear(BigDecimal.ZERO.intValue());
			} else {
				if (coverage.getChargeYear() == null) {
					detailVo.setLiaRocChargeYear(null);
				} else {
					//歲滿期商品=歲滿期限 - 投保年齡
					if (CodeCst.CHARGE_PERIOD__TO_CERTAIN_AGE.equals(coverage.getChargePeriod())) {
						detailVo.setLiaRocChargeYear(coverage.getChargeYear() - entryAge);
					} else {
						// 彈性繳
						if ("6".equals(initialType)) {
							if (LiaRocCst.LIAROC_PRODUCT_CATEGORY_4.equals(detailVo.getLiaRocProductCategory())) {
								PayPlanVO annPayPlan = coverage.getAnnuityPayPlan();
								PayPlanVO vAnnPayPlan = coverage.getVariableAnnuityPayPlan();
								PayPlanVO chkPayPlan = null;
								if (annPayPlan != null) {
									chkPayPlan = annPayPlan;
								} else if (vAnnPayPlan != null) {
									chkPayPlan = vAnnPayPlan;
								}
								//年金商品(公會設定：險種分類=4-年金保險)且年金開始給付日≠空白：年金開始給付日(年齡)-投保年齡
								if (chkPayPlan != null && chkPayPlan.getPayYear() != null && chkPayPlan.getPayYear() > 0) {
									detailVo.setLiaRocChargeYear(chkPayPlan.getPayYear() - entryAge);
								} else {
									detailVo.setLiaRocChargeYear(coverage.getChargeYear());
								}
							} else {
								//非年金商品: 保障終止日(年)-生效日(年)
								int intChargeYear = 0;
								if (detailVo.getExpiredDate() != null && detailVo.getValidateDate() != null){
									double yearAmount =  DateUtils.getYearAmount(detailVo.getValidateDate(),DateUtils.addDay(detailVo.getExpiredDate(), +1));
									intChargeYear = (int)yearAmount;
								}
								detailVo.setLiaRocChargeYear(intChargeYear);
							}
						} else {
							detailVo.setLiaRocChargeYear(coverage.getChargeYear());
						}
					}
				}
			}
		}

		// 保單狀況		
		detailVo.setLiaRocPolicyStatus(LiaRocCst.LIAROC_POLICY_STATUS__INFORCE);
		// 被保人有註記受監護宣告時，承保通報要通報50
		if (LiaRocCst.LIAROC_UL_TYPE_INFORCE.equals(liaRocUploadType)) {
			/*
			String liaRocPolicyStatus50 = liaRocUpload2020Dao.getProductLiaRocPolicyStatus50(detailVo.getCertiCode(),
					policy.getPolicyId()	,
					detailVo.getProductId(),
					detailVo.getProductVersionId());
			
			if (CodeCst.YES_NO__YES.equals(liaRocPolicyStatus50)) {
			*/
			if (CodeCst.YES_NO__YES.equals(isGuardianAncmnt)) {
				detailVo.setLiaRocPolicyStatus(LiaRocCst.LIAROC_POLICY_STATUS__107_INFORCE);
			}
		}

		/* 收件通報 */
		if (LiaRocCst.LIAROC_UL_TYPE_RECEIVE.equals(liaRocUploadType)) {
			// 保單狀況生效日：同收件通報的要保書填寫日期
			detailVo.setStatusEffectDate(detailVo.getApplyDate());

			/*
			 * 依2021/01/29 Fanny eMail 修改下列收件通報規則避免入公會偵錯 
			 * 1. 要保書填寫日如果大於契約滿期日，契約滿期日請放空白
			 * 2. 被保險人出生日期大於契約生效日期，契約生效日期請放空白 
			 * 3. 要保書填寫日大於契約生效日，契約生效日期請放空白
			 */
			if (detailVo.getApplyDate() != null && detailVo.getExpiredDate() != null) {
				if(detailVo.getApplyDate().getTime() > detailVo.getExpiredDate().getTime()) {
					detailVo.setExpiredDate(null);	
				}
			}
			boolean checkBirthday = detailVo.getBirthday() != null && detailVo.getValidateDate() != null
							&& (detailVo.getBirthday().getTime() > detailVo.getValidateDate().getTime());
			boolean checkApplyDate = detailVo.getApplyDate() != null && detailVo.getValidateDate() != null
							&& (detailVo.getApplyDate().getTime() > detailVo.getValidateDate().getTime());
			if (checkBirthday || checkApplyDate) {
				detailVo.setValidateDate(null);
			}
		} else if (LiaRocCst.LIAROC_UL_TYPE_INFORCE.equals(liaRocUploadType)) {
			//保單狀況生效日：承保日期
			if (policy.getIssueDate() != null
							&& policy.getRiskStatus() != null
							&& policy.getRiskStatus() == CodeCst.LIABILITY_STATUS__IN_FORCE) {
				detailVo.setStatusEffectDate(policy.getIssueDate());
			} else {
				//承保後UNDO執行生效06通報，無issue_date，抓取系統日送出
				detailVo.setStatusEffectDate(new Date());
			}

			//PCR_466194-保險存摺需上傳公會欄位
			//主附約:1=主約/顯示於主頁;2=主約/不顯示於主頁;3=附約
			if(coverage.getMasterId() == null) {
				detailVo.setConvenantType902(LiaRocCst.CONVENANT_TYPE_MASTER_SHOW);	
			} else {
				detailVo.setConvenantType902(LiaRocCst.CONVENANT_TYPE_RIDER);
			}
			//保險公司的產品名稱
			detailVo.setProductName902(lifeProduct.getProduct().getProductName());
			//保險公司自行定義的險種名稱
			String tglPlanName902 = CodeTable.getCodeDesc(TableCst.T_PROD_BIZ_CATEGORY, 
					String.valueOf(prodBizCategorySub.getCategoryId()));
			detailVo.setTglPlanName902(tglPlanName902); //商品小類名稱
			
			//電子保單/紙本保單:1=紙本保單,2=電子保單,3=紙本QR Code保單,4=電子QR Code保單
			if (policy.getElecType() != null &&
					policy.getElecType() == CodeCst.ELEC_TYPE__ELECTRONIC) {
				detailVo.setCelectronicType902(LiaRocCst.CELECTRONIC_TYPE_CELECTRONIC);
			}else {
				detailVo.setCelectronicType902(LiaRocCst.CELECTRONIC_TYPE_PAPER);
			}
			//END  PCR_466194-保險存摺
			
		}
		//保單狀況生效時分: detailVo.statusEffectTime 固定0000

		// 要保人姓名
		String holderName = policy.getPolicyHolder().getName();
		String holderRomanName = policy.getPolicyHolder().getRomanName();
		if (!StringUtil.isNullOrEmpty(holderRomanName)) {
			holderName = holderName + holderRomanName;
		}
		// 取10個中文字	
		holderName = substring(holderName, 30);
		detailVo.setPhName(holderName);

		// 要保人身份證號
		detailVo.setPhCertiCode(policy.getPolicyHolder().getCertiCode());

		// 要保人生日，要保人＝法人(關係=無)時，生日不需填列
		if ("00".equals(detailVo.getLiaRocRelationToPh())) {
			detailVo.setPhBirthday(null);
		}else {
			if (!NBUtils.isEmptyOrDummyDate(policy.getPolicyHolder().getBirthDate())) {
				detailVo.setPhBirthday(policy.getPolicyHolder().getBirthDate());
			}
		}
		
		//實際T_CONTRACT_PRODUCT.CHARGE_YEAR(繳費年期) for 比對規則使用
		if (coverage.getChargeYear() != null) {
			detailVo.setChargeYear(coverage.getChargeYear());
		}
		
		//生存保險金給付年齡/長照給付期間 for 比對規則使用
		if (coverage.getProposalTerm() != null ) {
			detailVo.setProposalTerm(coverage.getProposalTerm());
		}
		//主約保單號碼
		detailVo.setPolicyCode(policy.getPolicyNumber());

		// 通報公會保單號碼
		int itemOrder = (coverage.getItemOrder() == null ? 0 : coverage.getItemOrder());
		LiaRocUploadCoverage2020 uploadCoverage = null;
		if (coverage.getItemId() != null && coverage.getItemId().longValue() > 0L) {
			uploadCoverage = liaRocUploadCoverage2020Dao.findByPolicyIdItemId(policy.getPolicyId(), coverage.getItemId());
		}

		if (uploadCoverage != null) {
			//PCR 188901 已落地保項需以先前保項通報編號作通報
			detailVo.setLiaRocPolicyCode(uploadCoverage.getLiarocPolicyCode());
		} else {
			String liaRocPolicyCodePrefix = policy.getPolicyNumber();
			if (policy.getPolicyId() != null && policy.getPolicyId().longValue() > 0L) {
				//PCR 188901 落地保單舊件以保單號,新件以受理編號(無受理編號則以保單號)
				liaRocPolicyCodePrefix = liaRocUploadCommonService.getLiaRocPolicyCodePrefix(policy.getPolicyId());
			}

			String checkFmt = LiaRocCst.ITEM_ORDER_FMT_NORMAL;
			//LiaRocPolicyCode = 保單號碼+險種代碼+流水序號(001..002..003..)
			String planCode = this.productLifeDS.getProduct(coverage.getProductId().longValue()).getInternalId();
			detailVo.setLiaRocPolicyCode(liaRocPolicyCodePrefix + planCode + String.format(checkFmt, itemOrder));
		}

		//收件保額更正通報比對用
		if (coverage.getCurrentPremium() != null && coverage.getCurrentPremium().getSumAssured() != null
						&& coverage.getCurrentPremium().getSumAssured().intValue() > 0) {
			detailVo.setAmount(coverage.getCurrentPremium().getSumAssured().toString());
		}
		if (coverage.getCurrentPremium() != null && coverage.getCurrentPremium().getUnit() != null
						&& coverage.getCurrentPremium().getUnit().intValue() > 0) {
			detailVo.setAmount(coverage.getCurrentPremium().getUnit().toString());
		}
		if (coverage.getCurrentPremium() != null && coverage.getCurrentPremium().getBenefitLevel() != null) {
			String sBenefitLevel = coverage.getCurrentPremium().getBenefitLevel();
			detailVo.setAmount(sBenefitLevel);
		}
		
		
		return detailVo;

	}

	/**
	 * <p>Description : 公會通報給付金額轉換</p>
	 * 注意：呼叫此方法時，傳入之 detailVO 必須  assign 好要通報之保項的 item_id, product_id, product_version_id
	 * <p>Created By : Amy Hung</p>
	 * <p>Create Time : Aug 17, 2016</p>
	 * @param detailVO 通報明細 VO
	 * @param coverage
	 * @param sourceCurrency 保項幣別
	 * @param insuredAge
	 * @param liaRocUploadType
	 * @param isGuardianAncmnt
	 * @return 將傳立之 detailVO 填入需要通報的給付項目之給付金額後回傳
	 */
	public LiaRocUploadDetail2020WrapperVO calcLiabilityMoney(LiaRocUploadDetail2020WrapperVO detailVO,
					CoverageVO coverage, Integer sourceCurrency, Long insuredAge, String liaRocUploadType, String isGuardianAncmnt) {

		detailVO.setLiability01(0L);
		detailVO.setLiability02(0L);
		detailVO.setLiability03(0L);
		detailVO.setLiability04(0L);
		detailVO.setLiability05(0L);
		detailVO.setLiability06(0L);
		detailVO.setLiability07(0L);
		detailVO.setLiability08(0L);
		detailVO.setLiability09(0L);
		detailVO.setLiability10(0L);
		detailVO.setLiability11(0L);
		detailVO.setLiability12(0L);
		detailVO.setLiability13(0L);
		detailVO.setLiability14(0L);
		detailVO.setLiability15(0L);
		detailVO.setLiability16(0L);
		detailVO.setLiability17(0L);
		detailVO.setLiability18(0L);
		detailVO.setLiability19(0L);

		// 查詢這個商品要通報公會的理賠給付項目資料
		List<Map<String, Object>> liabs = liaRocUpload2020Dao.getProductLiaRocLiabs(detailVO.getProductId(),
						detailVO.getProductVersionId());

		Long amount = 0L;
		boolean hasLiabCode27 = false;

		if (liabs != null) {

			// 逐一的去計算各給付項目的給付金額
			for (Map<String, Object> liab : liabs) {

				amount = 0L;

				if (liab.get("LIAROC_LIA_ID") != null) {
					Long liarocLiaId = Long.valueOf(liab.get("LIAROC_LIA_ID").toString());

					BigDecimal liabAmount = BigDecimal.ZERO;

					if (LiaRocCst.LIAROC_LIAB_CODE_27.equals(liarocLiaId.toString())) {
						// 這裡不做給付項目-17  喪葬費用計算，移至下方做處理
					} else {
						liabAmount = calcLiabAmountFormula(coverage, liarocLiaId, false);
						if (liabAmount != null) {
							amount = liabAmount.longValue();

							//需轉為台幣
							if (CodeCst.MONEY__NTD != sourceCurrency) {
								BigDecimal twLiabAmount = liaRocUploadCommonService.transferToTwCurrency(sourceCurrency, coverage.getApplyDate(), liabAmount);
								amount = twLiabAmount.longValue();
							}
						}
					}
				}

				// 依不同的給付項目把計算結果 assign 到相應的欄位
				if (liab.get("LIAROC_LIA_ID") != null) {

					String liaRocLiabId = liab.get("LIAROC_LIA_ID").toString();
					//2020/02/24 因AFINS上傳保額過大,無法寫入通報明細檔,造成收件延遲通報,增加判斷過大保額強制寫入9999999999
					if (LiaRocCst.LIAB_MAXIMUM_AMOUNT < amount) {
						amount = LiaRocCst.LIAB_MAXIMUM_AMOUNT;
					}

					if (LiaRocCst.LIAROC_LIAB_CODE_11.equals(liaRocLiabId)) {
						detailVO.setLiability01(amount);
					} else if (LiaRocCst.LIAROC_LIAB_CODE_12.equals(liaRocLiabId)) {
						detailVO.setLiability02(amount);
					} else if (LiaRocCst.LIAROC_LIAB_CODE_13.equals(liaRocLiabId)) {
						detailVO.setLiability03(amount);
					} else if (LiaRocCst.LIAROC_LIAB_CODE_14.equals(liaRocLiabId)) {
						detailVO.setLiability04(amount);
					} else if (LiaRocCst.LIAROC_LIAB_CODE_15.equals(liaRocLiabId)) {
						detailVO.setLiability05(amount);
					} else if (LiaRocCst.LIAROC_LIAB_CODE_16.equals(liaRocLiabId)) {
						detailVO.setLiability06(amount);
					} else if (LiaRocCst.LIAROC_LIAB_CODE_17.equals(liaRocLiabId)) {
						detailVO.setLiability07(amount);
					} else if (LiaRocCst.LIAROC_LIAB_CODE_18.equals(liaRocLiabId)) {
						detailVO.setLiability08(amount);
					} else if (LiaRocCst.LIAROC_LIAB_CODE_19.equals(liaRocLiabId)) {
						detailVO.setLiability09(amount);
					} else if (LiaRocCst.LIAROC_LIAB_CODE_20.equals(liaRocLiabId)) {
						detailVO.setLiability10(amount);
					} else if (LiaRocCst.LIAROC_LIAB_CODE_21.equals(liaRocLiabId)) {
						detailVO.setLiability11(amount);
					} else if (LiaRocCst.LIAROC_LIAB_CODE_22.equals(liaRocLiabId)) {
						detailVO.setLiability12(amount);
					} else if (LiaRocCst.LIAROC_LIAB_CODE_23.equals(liaRocLiabId)) {
						detailVO.setLiability13(amount);
					} else if (LiaRocCst.LIAROC_LIAB_CODE_24.equals(liaRocLiabId)) {
						detailVO.setLiability14(amount);
					} else if (LiaRocCst.LIAROC_LIAB_CODE_25.equals(liaRocLiabId)) {
						detailVO.setLiability15(amount);
					} else if (LiaRocCst.LIAROC_LIAB_CODE_26.equals(liaRocLiabId)) {
						detailVO.setLiability16(amount);
					} else if (LiaRocCst.LIAROC_LIAB_CODE_27.equals(liaRocLiabId)) {
						detailVO.setLiability17(amount);
					} else if (LiaRocCst.LIAROC_LIAB_CODE_28.equals(liaRocLiabId)) {
						detailVO.setLiability18(amount);
					} else if (LiaRocCst.LIAROC_LIAB_CODE_29.equals(liaRocLiabId)) {
						detailVO.setLiability19(amount);
					}

				}
				// end.
			}
		}

		//給付項目-17 喪葬費用要by 被保人年齡< 15 歲或商品設定有設定LIAB_ID 2020喪葬費用計算通報保額
		BigDecimal liabAmount17 = BigDecimal.ZERO;
		liabAmount17 = calcLiabAmountFormulaByAge(coverage, Long.valueOf(LiaRocCst.LIAROC_LIAB_CODE_27), false, insuredAge, liaRocUploadType, isGuardianAncmnt);
		if (liabAmount17 != null) {
			detailVO.setLiability17(liabAmount17.longValue());
		}

		return detailVO;

	}

	/**
	 * 公會通報理賠給付項目金額計算 (不含給付項目-17 喪葬費用)
	 * @param coverage
	 * @param liarocLiaId
	 * @return
	 */

	public BigDecimal calcLiabAmountFormula(CoverageVO coverage, Long liarocLiaId, boolean isPos) {
		return liaRocUpload2020Dao.calcLiabAmountFormula(coverage, liarocLiaId, false);
	}

	/**
	 * 公會通報理賠給付項目金額計算 (只含給付項目-17 喪葬費用)
	 * @param coverage
	 * @param liarocLiaId
	 * @param insuredAge
	 * @param liaRocUploadType
	 * @param isGuardianAncmnt
	 * @return amount
	 */
	public BigDecimal calcLiabAmountFormulaByAge(CoverageVO coverage, Long liarocLiaId, boolean isPos, Long insuredAge, String liaRocUploadType, String isGuardianAncmnt) {
		return liaRocUpload2020Dao.calcLiabAmountFormulaByAge(coverage, liarocLiaId, false, insuredAge, liaRocUploadType, isGuardianAncmnt);
	}

	@Override
	public Long sendNB(Long policyId, String liaRocUploadType) {

		Long uploadId = null;
		if (LiaRocCst.LIAROC_UL_TYPE_RECEIVE.equals(liaRocUploadType)) {
			uploadId = sendNBReceiveNew(policyId);
		} else if (LiaRocCst.LIAROC_UL_TYPE_INFORCE.equals(liaRocUploadType)) {
			uploadId = sendNBInforce(policyId);
		}
		return uploadId;
	}

	//PCR-288932 mPos外部通路與一般進件收件通報拆開處理 2018/12/22 Add by Kathy
	@Override
	public Long sendmPosNB(Long policyId, String liaRocUploadType) {

		Long uploadId = null;
		if (LiaRocCst.LIAROC_UL_TYPE_RECEIVE.equals(liaRocUploadType)) {
			//2020/04/23 Joanne's eMail 保經代的99險種不進行通報，通報責任在保經代 by Kathy
			//uploadId = sendMPosReceiveNew(policyId);
			//PCR_454251_修改mPOS進件同紙本進件方式，執行全量通報並執行比對規則重覆不通報
			uploadId = sendNBReceiveNew(policyId);
		} else if (LiaRocCst.LIAROC_UL_TYPE_INFORCE.equals(liaRocUploadType)) {
			uploadId = sendNBInforce(policyId);
		}
		return uploadId;
	}

	private Long sendNBInforce(Long policyId) {

		Long uploadId = null;
		//OIU商品不需要收件通報與承保通報，且無須索引(下載)同業投保紀錄。TGL32/VICTOR CHANG;
		//EC(網路投保=>列TODO)不報
		PolicyVO policyVO = this.policyDS.retrieveById(policyId, false);
		if (policyVO.getRiskStatus() != 1) {
			//若有UNDO 保單為未生效不用報公會 
			return uploadId;
		}
		Date lastBatchDate = liaRocUploadCommonService.getLastNBBatchDate(policyId, LiaRocCst.LIAROC_NB_BATCH_CHANGE_TYPE_11);
		if (lastBatchDate != null
						&& DateUtils.date2String(lastBatchDate).equals(DateUtils.date2String(new Date()))) {
			//若同有承保UNDO後通報承保06的情況, 再次承保需判斷 是否有同日生效,有則不 執行承保通報,由SMART QUERY 產生報表後人工通報
			liaRocUploadCommonService.recordNBBatchData(policyId, LiaRocCst.LIAROC_NB_BATCH_CHANGE_TYPE_13);
			return uploadId;
		}

		List uploads = this.liaRocUpload2020Dao.queryByPolicyIdAndTypeAndSource(policyId, LiaRocCst.LIAROC_UL_TYPE_INFORCE, LiaRocCst.LIAROC_UL_BIZ_SOURCE_UNB);
		if (uploads == null || uploads.size() == 0) {
			/**非undo 直接報公會**/
			LiaRocUpload2020SendVO sendVO = checkLiaRocProduct(getUploadVOByPolicyId(policyId, LiaRocCst.LIAROC_UL_TYPE_INFORCE, LiaRocCst.LIAROC_UL_BIZ_SOURCE_UNB));

			uploadId = this.saveUpload(sendVO);
			return uploadId;
		} else {
			LiaRocUpload2020 oldUpload = (LiaRocUpload2020) uploads.get(0);
			LiaRocUpload2020VO oldUploadVO = new LiaRocUpload2020VO();
			BeanUtils.copyProperties(oldUploadVO, oldUpload);
			String status = oldUploadVO.getLiaRocUploadStatus();
			if (LiaRocCst.LIAROC_UL_STATUS_SEND.equals(status) || LiaRocCst.LIAROC_UL_STATUS_RETURN.equals(status)
							|| LiaRocCst.LIAROC_UL_STATUS_RETURN_ERROR.equals(status)) {
				/**通報資料已經送出,重新報一次**/
				// make 主檔 vo
				LiaRocUpload2020 liaRocUploadBO = new LiaRocUpload2020();
				liaRocUploadBO.setBizSource(LiaRocCst.LIAROC_UL_BIZ_SOURCE_UNB); // 新契約。
				liaRocUploadBO.setBizId(policyId);
				liaRocUploadBO.setBizCode(oldUploadVO.getBizCode());
				liaRocUploadBO.setPolicyCode(oldUploadVO.getPolicyCode());
				liaRocUploadBO.setPolicyId(policyId);
				liaRocUploadBO.setLiaRocUploadType(LiaRocCst.LIAROC_UL_TYPE_INFORCE);
				liaRocUploadBO.setInputUser(AppContext.getCurrentUser().getUserId());
				liaRocUploadBO.setLiaRocUploadStatus(LiaRocCst.LIAROC_UL_STATUS_WAITING);
				
				//新契約承保通報設定902需上傳
				liaRocUploadBO.setSend902Flag(LiaRocCst.LIAROC_UL_SEND_902_FLAG_1);
				liaRocUploadBO.setLiaRocUploadStatus902(LiaRocCst.LIAROC_UL_STATUS_WAITING);
				
				// 儲存主檔資料
				liaRocUpload2020Dao.save(liaRocUploadBO);

				uploadId = liaRocUploadBO.getListId();

				Map<String, LiaRocUploadDetail2020> detailMap = new HashMap<String, LiaRocUploadDetail2020>();
				List<LiaRocUploadDetail2020> details = liaRocUploadDetail2020Dao.findByUploadId(oldUploadVO.getListId());
				for (LiaRocUploadDetail2020 detail : details) {
					/* 01, 50 均為有效通報 */
					if (NBUtils.in(detail.getLiaRocPolicyStatus(),
									LiaRocCst.LIAROC_POLICY_STATUS__107_INFORCE, LiaRocCst.LIAROC_POLICY_STATUS__INFORCE)) {
						detailMap.put(detail.getLiaRocPolicyCode(), detail);
					}
				}
				// get 明細資料 vo。
				List<GenericEntityVO> newDetails = (List<GenericEntityVO>) this.getDetailList(policyService.loadPolicyByPolicyId(policyId), LiaRocCst.LIAROC_UL_TYPE_INFORCE);
				for (GenericEntityVO detailVO : newDetails) {
					LiaRocUploadDetail2020 detailBO = new LiaRocUploadDetail2020();
					BeanUtils.copyProperties(detailBO, detailVO);
					detailBO.setUploadListId(liaRocUploadBO.getListId());
					LiaRocUploadDetail2020 detail = detailMap.get(detailBO.getLiaRocPolicyCode());
					if (detail != null) {
						/** 已經通報 **/
						//2020/09/08 Simon 生日相同但卻通報12,01
						//detail.getBirthday() 為java.sql.Timestamp, detailBO.getBirthday()為java.util.Date,必須要用getTime()作比對
						boolean isBirthdayChange = false;
						if (detail.getBirthday() != null && detailBO.getBirthday() != null) {
							isBirthdayChange = !(detail.getBirthday().getTime() == detailBO.getBirthday().getTime());
						}

						if (!detail.getCertiCode().equals(detailBO.getCertiCode()) || isBirthdayChange) {
							/** 身分證字號或生日不一樣要先報12再重報 **/
							LiaRocUploadDetail2020 detailBO12 = createFixUploadDetail(detail, liaRocUploadBO.getListId(), LiaRocCst.LIAROC_POLICY_STATUS__ERROR_TERMINATION);
							//12-鍵值錯誤的通報狀態變更日與最新通報的更新日相同
							detailBO12.setStatusEffectDate(detailBO.getStatusEffectDate());
							liaRocUploadDetail2020Dao.save(detailBO12);
							liaRocUploadDetail2020Dao.save(detailBO);
						} else {
							/** 只修改資料 **/
							detailBO.setListId(liaRocUploadBO.getListId());
							liaRocUploadDetail2020Dao.save(detailBO);
						}
						/**移除Map中已處理的,剩下就是最後要刪除的保項**/
						detailMap.remove(detailBO.getLiaRocPolicyCode());
					} else {
						/**尚未通報**/
						liaRocUploadDetail2020Dao.save(detailBO);
					}

				}
				if (!detailMap.isEmpty()) {
					//PCR_466194_調整06(契約撤銷)為12-鍵值欄位通報錯誤終止
					/**比對完還存在表示已被刪除要新增一筆狀態為6(契約撤銷)**/
					for (String policyCode : detailMap.keySet()) {
						LiaRocUploadDetail2020 detailBO = new LiaRocUploadDetail2020();
						LiaRocUploadDetail2020 detail = detailMap.get(policyCode);
						BeanUtils.copyProperties(detailBO, detail);
						detailBO.setLiaRocPolicyStatus(LiaRocCst.LIAROC_POLICY_STATUS__ERROR_TERMINATION);
						detailBO.setUploadListId(oldUploadVO.getListId());
						detailBO.setListId(null);
						liaRocUploadDetail2020Dao.save(detailBO);
					}
				}

			} else if (LiaRocCst.LIAROC_UL_STATUS_WAITING.equals(status)) {
				/**待通報要修正原本資料**/
				List<LiaRocUploadDetail2020> details = liaRocUploadDetail2020Dao.findByUploadId(oldUploadVO.getListId());

				uploadId = oldUploadVO.getListId();

				for (LiaRocUploadDetail2020 detail : details) {
					liaRocUploadDetail2020Dao.remove(detail);
				}

				LiaRocUpload2020SendVO sendVO = checkLiaRocProduct(getUploadVOByPolicyId(policyId, LiaRocCst.LIAROC_UL_TYPE_INFORCE, LiaRocCst.LIAROC_UL_BIZ_SOURCE_UNB));
				uploadId = this.saveUpload(sendVO);
				return uploadId;
			} else {
				/**通報流程失敗重新發送**/
				//TODO 原本通報若有錯是否要註記已發送第二次
				uploadId = this.saveUpload(checkLiaRocProduct(getUploadVOByPolicyId(policyId, LiaRocCst.LIAROC_UL_TYPE_INFORCE, LiaRocCst.LIAROC_UL_BIZ_SOURCE_UNB)));
			}

			return uploadId;
		}
		//}

		//return null;
	}

	/**
	 * FOA NB受理改call此method
	 * @param foaPolicyVOs
	 */
	public void saveAcceptanceUploadNB(long caseId) throws Exception {

		FoaPolicyVO vo = foaAcceptanceCI.findFoaPolicyByCaseId(caseId);
		saveAcceptanceUploadNB(vo);
	}

	private static String FOA_UPLOAD_NB = "saveAcceptanceUploadNB";

	private void saveAcceptanceUploadNB(FoaPolicyVO vo) throws Exception {

		ApplicationLogger.setSystemCode(AppLogDataHelper.SYSTEM_CODE_UNB);
		ApplicationLogger.setJobName(FOA_UPLOAD_NB);
		ApplicationLogger.setOptionId(DateUtils.date2String(new Date(), "yyyyMMdd HHmmss"));
		try {
			PolicyVO policyVO = foaAcceptanceHelper.convert2PolicyVO(vo, false);
			ApplicationLogger.setPolicyCode(policyVO.getPolicyNumber());

			//PCR-337606 判斷FOA 會有重複送件問題，所以要檢核，第二筆壓 D 2020/09/09 Add by Kathy
			String liarocUploadStatus = LiaRocCst.LIAROC_UL_STATUS_WAITING;
			int count = countFoaLiarocUpload(vo.getUnbCaseVO().getListId(), LiaRocCst.LIAROC_UL_BIZ_SOURCE_FOA_UNB);
			if (count > 0) {
				liarocUploadStatus = LiaRocCst.LIAROC_UL_STATUS_SEND_DELETE;
			}

			// 公會通報主檔
			LiaRocUpload2020SendVO info = this.generateFoaUpload(policyVO, vo);
			info.setLiaRocUploadStatus(liarocUploadStatus);

			int itemOrder = 1;
			// 補上順序
			List<ScFoaUnbProductVO> prods = vo.getUnbProductList();
			int index = 0;
			for (CoverageVO coverage : policyVO.gitSortCoverageList()) {
				Date applyDate = policyVO.getApplyDate();
				coverage.setApplyDate(applyDate);
				coverage.setItemOrder(itemOrder++);
				LifeProduct lifeProduct = lifeProductService.getProductByVersionId(
								coverage.getProductId().longValue(),
								coverage.getProductVersionId());
				String ageMethod = lifeProduct.getProduct().getAgeMethod();
				// calc and set entry age and entry age month for insured1
				CoverageInsuredVO insured1 = coverage.getLifeInsured1();
				long entryAge1 = 0;
				long ageMonth1 = 0;
				InsuredVO insured = insured1.getInsured();
				if (!NBUtils.isEmptyOrDummyDate(insured.getBirthDate())
								&& !NBUtils.isEmptyOrDummyDate(applyDate)) {
					entryAge1 = BizUtils.getAge(ageMethod, insured1.getInsured()
									.getBirthDate(), applyDate);
					ageMonth1 = BizUtils.getAgeMonth(ageMethod, insured1.getInsured()
									.getBirthDate(), applyDate);
				}
				insured1.setEntryAge(Integer.valueOf(String.valueOf(entryAge1)));
				insured1.setEntryAgeMonth(Integer.valueOf(String.valueOf(ageMonth1)));
			}

			for (CoverageVO coverage : policyVO.gitSortCoverageList()) {
				//RTC-250475-FOA通報滿期日有誤,因上傳charge_preiod,無法由期限表中定義出來,收件通報先執行1年期避免收件通報逾時
				if (coverage.getCoveragePeriod() == null && coverage.getCoverageYear() == null) {
					coverage.setCoverageYear(1);
					coverage.setCoveragePeriod(CodeCst.COVERAGE_PERIOD__CERTAIN_YEA);
				}

				try {
					coverageService.calcDate2(policyVO, coverage);
				} catch (Exception e) {
					logger.error(ExceptionUtils.getFullStackTrace(e));
					ApplicationLogger.addLoggerData(ExceptionUtils.getFullStackTrace(e));
					//IR-254758-延遲通報表報(FOA POS收件通報,生效日與保障終止日因FOA填入資訊有限，以通報受理日作生效日，一年期作終止日通報)
					Calendar expireDate = Calendar.getInstance();
					expireDate.setTime(policyVO.getApplyDate());
					expireDate.add(Calendar.YEAR, 1);
					coverage.setExpiryDate(expireDate.getTime());
				}

				ApplicationLogger.addLoggerData("coverage=" + coverage.getProductId()
								+ ",expire_date=" + DateUtils.date2String(coverage.getExpiryDate(), "yyyy/MM/dd"));

				//BC416-XTC 新增長照給付年期(PROPOSAL_TEAM)設定 2021/03/02 Add by Kathy
				coverage.setProposalTerm( prods.get(index).getProposalTerm());					
				BigDecimal prem = prods.get(index).getPremAmt();
				coverage.getCurrentPremium().setTotalPremAf(prem);
				if (prem != null && prem.compareTo(BigDecimal.ZERO) > 0) {
					String initialType = coverage.getCurrentPremium()
									.getPaymentFreq();
					if (CodeCst.CHARGE_MODE__MONTHLY.equals(initialType)) {
						coverage.getCurrentPremium().setStdPremAn(
										prem.multiply(new BigDecimal("6")));
					} else if (CodeCst.CHARGE_MODE__QTRLY.equals(initialType)) {
						coverage.getCurrentPremium().setStdPremAn(
										prem.multiply(new BigDecimal("4")));
					} else if (CodeCst.CHARGE_MODE__HALF_YEARLY
									.equals(initialType)) {
						coverage.getCurrentPremium().setStdPremAn(
										prem.multiply(new BigDecimal("2")));
					} else {
						coverage.getCurrentPremium().setStdPremAn(prem);
					}
				}
				index ++;
			}

			List<GenericEntityVO> newDetails = (List<GenericEntityVO>) this
							.getDetailList(policyVO, LiaRocCst.LIAROC_UL_TYPE_RECEIVE,
											true);
			info.setDataList(newDetails);
			Long uploadId = this.saveReceiveUploadData(policyVO, info);

			ApplicationLogger.addLoggerData("uploadId=" + uploadId);
		} catch (Exception e) {
			logger.error(ExceptionUtils.getFullStackTrace(e));
			ApplicationLogger.addLoggerData(ExceptionUtils.getFullStackTrace(e));
			ApplicationLogger.setReturnCode(ApplicationLogger.RETURN_CODE_FAIL);
			try {
				ApplicationLogger.flush();
			} catch (Exception e1) {
				logger.error(e1);
			}
			throw e;
		} finally {
			try {
				ApplicationLogger.flush();
			} catch (Exception e) {
				logger.error(e);
			}
		}
	}

	/**
	 * <p>Description : 產生foa 公會上傳主檔資料</p>
	 * <p>Created By : Kathy.Yeh</p>
	 * <p>Create Time : 2020年09月03日</p>
	 * @param policyVO
	 * @param FoaPolicyVO
	 * @return LiaRocUploadSendVO 
	 */
	private LiaRocUpload2020SendVO generateFoaUpload(PolicyVO policyVO, FoaPolicyVO vo) throws Exception {

		ApplicationLogger.setPolicyCode(policyVO.getPolicyNumber());

		// 公會通報主檔
		LiaRocUpload2020SendVO info = new LiaRocUpload2020SendVO();
		info.setBizId(vo.getUnbCaseVO().getListId());
		ApplicationLogger.addLoggerData("unbCaseId=" + vo.getUnbCaseVO().getListId());
		/** 受理ID當key **/
		info.setBizCode(policyVO.getPolicyNumber());
		info.setPolicyCode(policyVO.getPolicyNumber());
		info.setPolicyId(policyVO.getPolicyId());
		info.setBizSource(LiaRocCst.LIAROC_UL_BIZ_SOURCE_FOA_UNB);
		info.setLiaRocUploadType(LiaRocCst.LIAROC_UL_TYPE_RECEIVE);
		info.setInputUser(AppContext.getCurrentUser().getUserId());
		ScFoaAcceptanceVO acceptVO = vo.getAcceptanceVO();
		info.setChannelOrgId(acceptVO.getDeliverAgentChannelOrgId());
		info.setReceiveDate(acceptVO.getAceptDate());
		List<CoverageAgentVO> agents = policyVO.gitMasterCoverage().getCoverageAgents();
		if (agents != null && agents.size() > 0) {
			info.setRegisterCode(agents.get(0).getRegisterCode());
		}

		return info;
	}

	/**
	 * ATN-COR-UNB-BSD-006-PCR 公會資料上下傳-_PCR_v1.0 Sign-Off
	 * BR-UNB-DWN-004獲取公會通報保單
	 * <pre>a. 新契約收件通報（除旅平險外）的保單需要同時符合以下條件：
	 * i. 內部通路的保單：
	 * (1)要保書狀態=待覆核。
	 * (2)比對未通報部份進行通報：收件通報系統(AFINS)與核心系統的資料比對
	 * 【保單號碼】 +【要保書填寫日】 +【險種代號】 +【被保險人 ID】，收件通報系統(AFINS)未通報者。
	 * ※當比對相同時，視為“收件通報系統”已通報 ,核心系統就不須再通報。
	 * ii.外部通路的保單
	 * (1)要保書狀態=待覆核。
	 * (2)死亡給付無關之險種則以收件通報項目第 36 項保經代分類= 98(保經代健康險及傷害醫療險)進行通報。
	 * (3)保經代傳真件：【外部通路受理作業-新件受理】頁面「保經代傳真件」欄位=「是」之保單，
	 * 則以收件通報項目第 36 項保經代分類=97(保經代傳真件)進行通報。
	 * (4)比對未通報部份進行通報：收件通報系統(AFINS)與核心系統的資料比對
	 * 【要保書填寫日】 +【險種代號】 +【被保險人 ID】，收件通報系統(AFINS)未通報者。
	 * ※當比對相同時，視為“收件通報系統”已通報 ,核心系統就不須再通報。
	 * iii.通報注意事項_收件通報項目 27 項(欄位名稱：要保書填寫日)：
	 * (1)內部通路：以「要保書填寫日」通報。
	 * (2)外部通路：保經代傳真件(97)及健康/醫療險(98)以「受理/收件確認日期」通報；
	 * 含死亡給付險種(99)以「要保書填寫日」</pre>
	 * <p>Created By : Simon.Huang</p>
	 * <p>Create Time : 2018年1月28日</p>
	 * @param policyId
	 */
	private Long sendNBReceiveNew(Long policyId) {

		Long uploadId = null;
		//EC(網路投保=>列TODO)不報
		PolicyVO policyVO = policyDS.retrieveById(policyId, false);

		//這次要傳的資料
		LiaRocUpload2020SendVO uploadVO = getUploadVOByPolicyId(policyId, LiaRocCst.LIAROC_UL_TYPE_RECEIVE,
						LiaRocCst.LIAROC_UL_BIZ_SOURCE_UNB);

		uploadId = this.saveReceiveUploadData(policyVO, uploadVO);
		//}
		return uploadId;
	}

	/**
	 * <pre>a. 新契約mPos收件通報（除旅平險外）的保單需要同時符合以下條件：
	 * i. 內部通路的保單：
	 * (1)要保書狀態=待覆核。
	 * (2)比對未通報部份進行通報：收件通報系統(AFINS)與核心系統的資料比對
	 * 【保單號碼】 +【要保書填寫日】 +【險種代號】 +【被保險人 ID】，收件通報系統(AFINS)未通報者。
	 * ※當比對相同時，視為“收件通報系統”已通報 ,核心系統就不須再通報。
	 * ii.外部通路的保單
	 * (1)要保書狀態=待覆核。
	 * (2)死亡給付無關之險種則以收件通報項目第 50項保經代分類= 02(保經代健康險及傷害醫療險)進行通報。
	 * (3)保經代傳真件：【外部通路受理作業-新件受理】頁面「保經代傳真件」欄位=「是」之保單，
	 * 則以收件通報項目第 50項保經代分類=01(保經代傳真件)進行通報。
	 * (4)比對未通報部份進行通報：收件通報系統(AFINS)與核心系統的資料比對
	 * 【要保書填寫日】 +【險種代號】 +【被保險人 ID】，收件通報系統(AFINS)未通報者。
	 * ※當比對相同時，視為“收件通報系統”已通報 ,核心系統就不須再通報。
	 * iii.通報注意事項_收件通報項目 49項(欄位名稱：要保書填寫日)：
	 * (1)內部通路：以「要保書填寫日」通報。
	 * (2)外部通路：保經代傳真件(01)及健康/醫療險(02)以「受理/收件確認日期」通報；
	 * mPos進件不寫入死亡給付險種(03)，由AFINS傳入死亡給付險種(03)做通報</pre>
	 * <p>Created By : Kathy Yeh</p>
	 * <p>Create Time : 2018年12月22日</p>
	 * @param policyId
	 * @deprecated PCR_454251_修改mPOS進件同紙本進件方式，執行全量通報並執行比對規則重覆不通報
	 */
	@Deprecated
	private Long sendMPosReceiveNew(Long policyId) {

		Long uploadId = null;
		//EC(網路投保=>列TODO)不報
		PolicyVO policyVO = policyDS.retrieveById(policyId, false);

		//這次要傳的資料
		LiaRocUpload2020SendVO uploadVO = getUploadVOByPolicyId(policyId, LiaRocCst.LIAROC_UL_TYPE_RECEIVE,
						LiaRocCst.LIAROC_UL_BIZ_SOURCE_UNB);
		
		/* 因會比對AFINS取原先通報號碼，若MPOS此次排除，取號檔會無此保項的通報號碼 (調整寫入明細檔後再更新狀態為5-mPOS排除件)
		List<GenericEntityVO> newDetails = new ArrayList<GenericEntityVO>();

		for (GenericEntityVO detailVO : uploadVO.getDataList()) {
			LiaRocUploadDetail2020 newDetailBO = new LiaRocUploadDetail2020();
			BeanUtils.copyProperties(newDetailBO, detailVO);
			//mPos進件之外部通路(BD,BR)保單不寫入死亡給付險種(03)，由AFINS傳入死亡給付險種(03)做通報
			if (LiaRocCst.LIAROC_BRBD_TYPE_03.equals(newDetailBO.getBrbdType())) {
				NBUtils.logger(this.getClass(), newDetailBO.getBrbdType()
								+ "->mPos ChannelType= BR/BD");
			} else {
				newDetails.add(detailVO);
			}
		}
		uploadVO.setDataList(newDetails);
		*/
		
		uploadId = this.saveReceiveUploadData(policyVO, uploadVO);

		List<LiaRocUploadDetail2020> details = liaRocUploadDetail2020Dao.findByUploadId(uploadId);
		for(LiaRocUploadDetail2020 detail : details) {
			//mPos進件之外部通路(BD,BR)保單不寫入死亡給付險種(03)，由AFINS傳入死亡給付險種(03)做通報
			if (LiaRocCst.LIAROC_BRBD_TYPE_03.equals(detail.getBrbdType())) {
				NBUtils.logger(this.getClass(), "->mPos skip ChannelType= BR/BD list_id=" + detail.getListId());
				detail.setCheckStatus(LiaRocCst.CHECK_STATUS_CANCEL_BY_MPOS);
				liaRocUploadDetail2020Dao.saveorUpdate(detail);
			}
		}

		return uploadId;
	}

	/**
	 * @deprecated : 改sendNBReceiveNew 處理，保留此處理邏輯作之後比對用
	 */
	@Deprecated
	private void sendNBReceive(Long policyId) {
		//OIU商品不需要收件通報與承保通報，且無須索引(下載)同業投保紀錄。TGL32/VICTOR CHANG;
		//EC(網路投保=>列TODO)不報
		PolicyVO policyVO = policyService.loadPolicyByPolicyId(policyId);
		if ((policyVO.isOIU()) == false) {

			//找出最後傳送的資料
			List<LiaRocUploadDetail2020> detailsOld = liaRocUploadDetail2020Dao.queryByPolicyLastUpload(policyId);
			//這次要傳的資料
			LiaRocUpload2020SendVO uploadVO = getUploadVOByPolicyId(policyId, LiaRocCst.LIAROC_UL_TYPE_RECEIVE, LiaRocCst.LIAROC_UL_BIZ_SOURCE_UNB);

			if (detailsOld != null && detailsOld.size() > 0) {
				//找出最後傳送的資料的主檔
				LiaRocUpload2020 uploadOld = liaRocUpload2020Dao.load(detailsOld.get(0).getUploadListId());
				//公會通報檔業務來源-FOA受理(UNB)
				if ((LiaRocCst.LIAROC_UL_BIZ_SOURCE_FOA_UNB).equals(uploadOld.getBizSource())) {

					// make 主檔 vo
					LiaRocUpload2020 liaRocUploadBO = new LiaRocUpload2020();
					liaRocUploadBO.setBizSource(LiaRocCst.LIAROC_UL_BIZ_SOURCE_UNB); // 新契約。
					liaRocUploadBO.setBizId(policyId);
					liaRocUploadBO.setBizCode(uploadVO.getBizCode());
					liaRocUploadBO.setPolicyCode(policyVO.getPolicyNumber());
					liaRocUploadBO.setPolicyId(policyId);
					liaRocUploadBO.setLiaRocUploadType(LiaRocCst.LIAROC_UL_TYPE_RECEIVE);
					liaRocUploadBO.setInputUser(AppContext.getCurrentUser().getUserId());
					liaRocUploadBO.setLiaRocUploadStatus(LiaRocCst.LIAROC_UL_STATUS_WAITING);
					liaRocUploadBO.setChannelOrgId(policyVO.getChannelOrgId());
					List<CoverageAgentVO> agents = policyVO.gitMasterCoverage().gitSortCoverageAgents();
					if (agents != null && agents.size() > 0) {
						liaRocUploadBO.setRegisterCode(agents.get(0).getRegisterCode());
					}
					// 儲存主檔資料
					liaRocUpload2020Dao.save(liaRocUploadBO);
					String status = uploadOld.getLiaRocUploadStatus();

					//若原本有為收件通報系統(AFINS),依通路比對通報檔, 若為FOA受理(UNB)通報用保單號碼+要保書填寫日,險種代碼比對, 已放送就不再發送;
					if ((LiaRocCst.LIAROC_UL_STATUS_SEND).equals(status)
									|| (LiaRocCst.LIAROC_UL_STATUS_RETURN).equals(status)
									|| LiaRocCst.LIAROC_UL_STATUS_RETURN_ERROR.equals(status)) {

						// get 明細資料 vo。
						Map<String, LiaRocUploadDetail2020> detailMap = new HashMap<String, LiaRocUploadDetail2020>();
						for (LiaRocUploadDetail2020 detail : detailsOld) {
							//  String statusEffectDate = String.valueOf(detail.getStatusEffectDate());
							String internalId = detail.getInternalId();
							String certiCode = detail.getCertiCode();
							//保單號碼+要保書填寫日,險種代碼比對
							detailMap.put(internalId + "@@" + certiCode, detail);

						}
						List<GenericEntityVO> newDetails = (List<GenericEntityVO>) this.getDetailList(policyService.loadPolicyByPolicyId(policyId), LiaRocCst.LIAROC_UL_TYPE_RECEIVE);

						for (GenericEntityVO detailVO : newDetails) {
							LiaRocUploadDetail2020 detailBO = new LiaRocUploadDetail2020();
							BeanUtils.copyProperties(detailBO, detailVO);
							detailBO.setUploadListId(liaRocUploadBO.getListId());

							String indexBo = detailBO.getInternalId() + "@@" + detailBO.getCertiCode();
							LiaRocUploadDetail2020 detail = detailMap.get(String.valueOf(indexBo));

							//要保書填寫日
							if ((detail != null) && (detail.getValidateDate()).equals(detailBO.getValidateDate())) {

								/** 已經通報 **/

								if (!detail.getBirthday().equals(detailBO.getBirthday())) {
									/** 生日不一樣要先報12再重報 **/
									LiaRocUploadDetail2020 detailBO12 = createFixUploadDetail(detail, liaRocUploadBO.getListId(), LiaRocCst.LIAROC_POLICY_STATUS__ERROR_TERMINATION);
									LiaRocUploadDetail2020 detailBO15 = createFixUploadDetail(detailBO, liaRocUploadBO.getListId(), LiaRocCst.LIAROC_POLICY_STATUS__FIX_ERROR);
									//12-鍵值錯誤的通報狀態變更日與最新通報的更新日相同
									detailBO12.setStatusEffectDate(detailBO15.getStatusEffectDate());
									liaRocUploadDetail2020Dao.save(detailBO12);
									liaRocUploadDetail2020Dao.save(detailBO15);
								} else {
									/** 只修改資料 **/
									detailBO.setListId(liaRocUploadBO.getListId());
									liaRocUploadDetail2020Dao.save(detailBO);
								}
								/**移除Map中已處理的,剩下就是最後要刪除的保項**/
								detailMap.remove(indexBo);
							} else {
								/**尚未通報**/
								//不必通報險種則不用寫明細檔
								if (isLiaRocProduct(detailBO)) {
									liaRocUploadDetail2020Dao.save(detailBO);
								}
							}

						}
						if (!detailMap.isEmpty()) {
							/**比對完還存在表示已被刪除要新增一筆狀態為6(契約撤銷)**/
							for (String policyCode : detailMap.keySet()) {
								LiaRocUploadDetail2020 detailBO = new LiaRocUploadDetail2020();
								LiaRocUploadDetail2020 detail = detailMap.get(policyCode);
								BeanUtils.copyProperties(detailBO, detail);
								detailBO.setLiaRocPolicyStatus(LiaRocCst.LIAROC_POLICY_STATUS__FREELOOK);
								detailBO.setUploadListId(uploadOld.getListId());
								detailBO.setListId(null);

								//不必通報險種則不用寫明細檔
								if (isLiaRocProduct(detailBO)) {
									liaRocUploadDetail2020Dao.save(detailBO);
								}

							}
						}

					} else if (LiaRocCst.LIAROC_UL_STATUS_WAITING.equals(status)) {
						/**待通報要修正原本資料**/
						List<LiaRocUploadDetail2020> details = liaRocUploadDetail2020Dao.findByUploadId(uploadOld.getListId());
						for (LiaRocUploadDetail2020 detail : details) {
							liaRocUploadDetail2020Dao.remove(detail);
						}
						// get 明細資料 vo。
						//#1835行
						List<GenericEntityVO> newDetails = (List<GenericEntityVO>) this.getDetailList(policyService.loadPolicyByPolicyId(policyId), LiaRocCst.LIAROC_UL_TYPE_RECEIVE);
						if (CollectionUtils.isNotEmpty(newDetails)) {
							for (GenericEntityVO detailVO : newDetails) {
								LiaRocUploadDetail2020 detailBO = new LiaRocUploadDetail2020();
								BeanUtils.copyProperties(detailBO, detailVO);
								detailBO.setUploadListId(liaRocUploadBO.getListId());
								liaRocUploadDetail2020Dao.save(detailBO);
							}
						}
					} else {
						/**通報流程失敗重新發送**/
						//TODO 雲本通報若有錯是否要註記已發送第二次
						this.saveUpload(checkLiaRocProduct(getUploadVOByPolicyId(policyId, LiaRocCst.LIAROC_UL_TYPE_RECEIVE, LiaRocCst.LIAROC_UL_BIZ_SOURCE_UNB)));
					}

				} else {
					this.saveUpload(checkLiaRocProduct(uploadVO));
				}

			} else {

				this.saveUpload(checkLiaRocProduct(uploadVO));
			}
		}
	}

	/**
	 * <p>Description : 提供給FOA受理，新契約受理通報共用存檔流程</p>
	 * <p>PCR_466194-兆豐公會比對規則_比對時，後者(如1KEY)保額空白或0時，不要作保額的12/15更正通報(不含來源為核保過程中的修正)</p>
	 * <p>Created By : Simon.Huang</p>
	 * <p>Create Time : 2018年2月5日</p>
	 * @param policyVO
	 * @param uploadVO
	 */
	public Long saveReceiveUploadData(PolicyVO policyVO, LiaRocUpload2020SendVO uploadVO) {

		List<GenericEntityVO> fixnewDetails = new ArrayList<GenericEntityVO>(uploadVO.getDataList());

		LiaRocUpload2020 oldLastUpload = null;
		Long policyId = policyVO.getPolicyId();
		if (policyId == null) {
			policyId = -1L;
		}
		String channelCode = null;
		
		//同被保險人，投保多筆相同險種，需依序取號
		Map<String,Set<String>> mapLiarocCode = new LinkedHashMap<String,Set<String>>(); //ID+ProdCode, Set<比對出的公會通報號碼>
		
		if (CollectionUtils.isNotEmpty(uploadVO.getDataList())) {
			
			if(policyVO.getChannelOrgId()!= null) {
				ChannelOrgVO channelOrgVO = channelOrgService.getChannelOrg(policyVO.getChannelOrgId());
				if(channelOrgVO != null) {
					channelCode = channelOrgVO.getChannelCode();
				}
			}
			//記錄比對重覆保項數量
			Map<String, List<String>> sameCodeMapList = new LinkedHashMap<String, List<String>>();
			List<UwNotintoRecvVO> allUwNotintoRecvList = new ArrayList<UwNotintoRecvVO>();
			//PCR-391319 通路= BDR時，讀取AFINS收件檔(T_UW_NOTINTO_RECV)取得受理編號做資料比對 2020/09/12 Add by Kathy
			String serialNum = policyVO.getSerialNum();
			String megaSerialNum = policyVO.getMegaSerialNum();
			boolean isD4B = NBUtils.isD4B(channelCode);
			// 保單號與受理編號相同，判斷為無受理編號
			boolean isEmptySerialNum = StringUtils.isEmpty(serialNum) && StringUtils.isEmpty(megaSerialNum) ;
			if (NBUtils.isBRBD(String.valueOf(policyVO.getChannelType()))
					&& (isEmptySerialNum || NBUtils.in(policyVO.getPolicyNumber(), serialNum, megaSerialNum))) {
				allUwNotintoRecvList = uwNotintoRecvService.getListByPolicyId(policyId); // AFINS此保單被保險人ID通報記錄
			}
			
			for (GenericEntityVO newDetailVO : uploadVO.getDataList()) {
				LiaRocUploadDetail2020 newDetailBO = new LiaRocUploadDetail2020();
				BeanUtils.copyProperties(newDetailBO, newDetailVO);

				//不必通報險種則不用寫明細檔
				if (isLiaRocProduct(newDetailBO) == false) {
					NBUtils.logger(this.getClass(), newDetailBO.getInternalId()
									+ "->isLiaRocProduct()=false"
									+ ",LiaRocProductCategory=" + newDetailBO.getLiaRocProductCategory()
									+ ",LiaRocProductType=" + newDetailBO.getLiaRocProductType());

					fixnewDetails.remove(newDetailVO);

					continue;
				}

				boolean isUnb = true;
				if (NBUtils.in(uploadVO.getBizSource(), LiaRocCst.LIAROC_UL_BIZ_SOURCE_AFINS_POS,
								LiaRocCst.LIAROC_UL_BIZ_SOURCE_FOA_POS,
								LiaRocCst.LIAROC_UL_BIZ_SOURCE_POS)) {
					isUnb = false;
				}

				//PCR-337603 比對保單狀態是否需要含06-撤件 2020/11/20 Add by Kathy				
				boolean isLiaRocPolicyStatus06 = false;
		
				//比對是否有重覆的收件通報資料
				String planCode = StringUtils.trimToEmpty(newDetailBO.getInternalId());
				if (planCode.indexOf("-") != -1) {
					planCode = planCode.substring(0, planCode.indexOf("-"));
				}
				
				if(isD4B) {
					if(megaSerialNum == null) {
						megaSerialNum = getAFINSSerialNum(policyVO, allUwNotintoRecvList, newDetailBO, planCode);
					}
				} else {
					if(serialNum == null) {
						serialNum = getAFINSSerialNum(policyVO, allUwNotintoRecvList, newDetailBO, planCode);
					}
				}
				
				
				List<LiaRocUploadDetail2020> lastUploadDetails = liaRocUploadDetail2020Dao.queryLastReceiveUploadDetail(
								uploadVO.getBizSource(),
								String.valueOf(policyVO.getChannelType()),
								policyVO.getPolicyNumber(),
								newDetailBO.getCertiCode(),
								planCode,
								newDetailBO.getValidateDate(),
								serialNum,
								megaSerialNum,
								LiaRocCst.IS_1KEY_TRUE,
								isLiaRocPolicyStatus06,
								newDetailBO.getProposalTerm(),
								newDetailBO.getChargeYear());


				NBUtils.logger(this.getClass(), "queryLastReceiveUploadDetail() params{"
								+ ",certiCode=" + newDetailBO.getCertiCode()
								+ ",internalId=" + planCode
								+ ",validateDate=" + DateUtils.date2String(newDetailBO.getValidateDate())
								+ ",serialNum=" + serialNum
								+ "},result_size=" + lastUploadDetails.size());
		
				//增加保額比對				
				String currentAmount = newDetailBO.getAmount() == null ? "" : newDetailBO.getAmount();
				if (StringUtils.isNotEmpty(currentAmount)) {

					Predicate predicate = new BeanPredicate("amount", PredicateUtils.equalPredicate(currentAmount));
					@SuppressWarnings("unchecked")
					List<LiaRocUploadDetail2020> amountUploadDetails = (List<LiaRocUploadDetail2020>) CollectionUtils
							.select(lastUploadDetails, predicate);
					if(amountUploadDetails != null && amountUploadDetails.size() > 0) {
						lastUploadDetails = amountUploadDetails;
					}
				}
				
				//移除本次已被使用的通報號碼
				lastUploadDetails = removeLiarocCode(lastUploadDetails, 
								newDetailBO.getCertiCode() + "@" + newDetailBO.getInternalId() ,
								mapLiarocCode, policyId, newDetailBO.getItemId());
				
				//有重覆送件的記錄
				if (CollectionUtils.isNotEmpty(lastUploadDetails)) {
					//修正AFINS通報  ID+XHR01,ID+XHR02,ID+XHR03, 但新契約通報 ID+XHR03,ID+XHR02,ID+XHR01
					//保項尚未寫入通報取號檔,因多個險種比對,改取最早通報的記錄作取號
					LiaRocUploadDetail2020 lastUploadDetail = lastUploadDetails.get(lastUploadDetails.size() - 1);
					
					String lastAmount = lastUploadDetail.getAmount() == null ? "" : lastUploadDetail.getAmount();

					//查明細的主檔，ID不同才查
					if (oldLastUpload == null ||
									!lastUploadDetail.getUploadListId().equals(oldLastUpload.getListId())) {
						oldLastUpload = liaRocUpload2020Dao.load(lastUploadDetail.getUploadListId());
					}

					//PCR 188901 受理編號作通報保單號
					newDetailVO.setDynamicProperty(LiaRocCst.LIAROC_DETAIL_LIST_ID, String.valueOf(lastUploadDetail.getListId()));
					newDetailVO.setDynamicProperty(LiaRocCst.LIAROC_POLICY_CODE, lastUploadDetail.getLiaRocPolicyCode());
					newDetailVO.setDynamicProperty(LiaRocCst.LIAROC_BIZ_CODE, lastUploadDetail.getPolicyCode());
					newDetailVO.setDynamicProperty(LiaRocCst.LIAROC_BIZ_SOURCE, oldLastUpload.getBizSource());
					
					addLiarocCode(lastUploadDetail.getLiaRocPolicyCode(), 
							newDetailBO.getCertiCode() + "@" + newDetailBO.getInternalId() ,
									mapLiarocCode);

					if (isUnb) {
						String bizCode = oldLastUpload.getBizCode();
						String bizSource = oldLastUpload.getBizSource();
						if (sameCodeMapList.get(bizSource + "-" + bizCode) == null) {
							//時間序號(detail.policy_code=detail.liaroc_policy_code 表示為時間序號)不進DB記錄通報序號
							if (lastUploadDetail.getLiaRocPolicyCode().equals(lastUploadDetail.getPolicyCode()) == false) {
								List<String> oldPolicyCodes = this.getPolicyListByBizCode(bizSource, bizCode);
								sameCodeMapList.put(bizSource + "-" + bizCode, oldPolicyCodes);
							}
						}
					}
					//END PCR-188901

					/* 公會通報狀態-待通報
					 *  0	待通報
					 *  1	通報發送完成
					 *  2	通報回傳完成
					 *  9	沒有險種需要通報
					 *  D	註記刪除不通報
					 *  E	通報流程發生未預期的錯誤
					 *  F	ESP端回傳接收失敗
					 *  N	ESP端回傳接收失敗
					 *  P	通報資料parse有錯
					 *  Q	通報佇列中
					 *  X	保項存在於回傳偵錯報表中
					 */
					String status = oldLastUpload.getLiaRocUploadStatus();

					//若原本有為收件通報系統(AFINS),依通路比對通報檔, 若為FOA受理(UNB)通報用保單號碼+要保書填寫日,險種代碼比對, 已放送就不再發送;
					if (LiaRocCst.LIAROC_UL_STATUS_SEND.equals(status)
									|| LiaRocCst.LIAROC_UL_STATUS_RETURN.equals(status)
							|| LiaRocCst.LIAROC_UL_STATUS_RETURN_ERROR.equals(status)) {

						// 2020/04/08 add FOA_UNB不作生日比對,由頁面生日修改時批次通報
						if (currentAmount.equals(lastAmount) || NBUtils.in(currentAmount, StringUtils.EMPTY, "0")) { // 新式公會收件通報,保額相同才排除不通報
							// IR-372064[Simon Huang]AFINS通報保項出偵錯，新契約此保項需通報(比對規則需排除出偵錯誤保項)
							if (LiaRocCst.CHECK_STATUS_UPLOAD_ERROR	.equals(lastUploadDetail.getCheckStatus()) == false) {

								NBUtils.logger(this.getClass(),
										"Skip by AFINS,BizSource=" + oldLastUpload.getBizSource());
								// 已通報此處不作通報
								// DEV-340097 增加邏輯：2：收件通報比對重覆不上傳,註解remove邏輯
								// fixnewDetails.remove(newDetailVO);
								markCancelByAFINS(policyVO, oldLastUpload, policyId, newDetailVO, lastUploadDetail);
							}
						} else {

							Date oldBirthday = lastUploadDetail.getBirthday();
							Date newBirthday = newDetailBO.getBirthday();

							boolean isSend12 = false;
							// IR-316003 保全若FOA保全收件通報生日為空則移除且不作12/15通報更新
							if (lastUploadDetail != null) {
								if ( oldBirthday != null) {
									if ( newBirthday != null 	&& !DateUtils.truncateDay(oldBirthday).equals(DateUtils.truncateDay(newBirthday))) {
										isSend12 = true;										
									}
								}else{
									// 因生日為key，故前次通報生日為空也要通報12，以避免公會上有重覆2筆資料 2020/08/10 Add by Kathy
									if ( oldBirthday == null && newBirthday != null ) {
										isSend12 = true;										
									}
								}
							}

							if (isSend12) {
								/** 生日不一樣要先報12再重報 **/
								/** IR-314914 新增一筆15(15-公會承保通報保單狀況 - 通報更正) */
								NBUtils.logger(this.getClass(),
										"Birthday is change " + ",oldBirthday=" + DateUtils.date2String(oldBirthday)
												+ ",newBirthday=" + DateUtils.date2String(newBirthday));
								fixnewDetails.remove(newDetailVO);
								LiaRocUploadDetail2020 detailBo12 = this.createFixUploadDetail(lastUploadDetail, null,
										LiaRocCst.LIAROC_POLICY_STATUS__ERROR_TERMINATION);
								LiaRocUploadDetail2020 detailBo15 = this.createFixUploadDetail(newDetailBO, null,
										LiaRocCst.LIAROC_POLICY_STATUS__FIX_ERROR);
								LiaRocUploadDetail2020VO detailVO12 = new LiaRocUploadDetail2020VO();
								LiaRocUploadDetail2020VO detailVO15 = new LiaRocUploadDetail2020VO();
								detailBo12.copyToVO(detailVO12, Boolean.TRUE);
								detailBo15.copyToVO(detailVO15, Boolean.TRUE);
								// PCR-338437 12-鍵值錯誤與15-通報更正的保單狀況生效日期=異動生效日期(系統日期)
								// 2019/10/25 modify by Kathy
								detailVO15.setStatusEffectDate(DateUtils.truncateDay(AppContext.getCurrentUserLocalTime()));
								fixnewDetails.add(detailVO12);
								fixnewDetails.add(detailVO15); // IR-314914

								// PCR 188901 受理編號作通報保單號
								detailVO12.setDynamicProperty(LiaRocCst.LIAROC_POLICY_CODE, lastUploadDetail.getLiaRocPolicyCode());
								detailVO12.setDynamicProperty(LiaRocCst.LIAROC_BIZ_SOURCE,oldLastUpload.getBizSource());
								detailVO12.setDynamicProperty(LiaRocCst.LIAROC_BIZ_CODE, lastUploadDetail.getPolicyCode());
								// PCR 188901 受理編號作通報保單號
								detailVO15.setDynamicProperty(LiaRocCst.LIAROC_POLICY_CODE,lastUploadDetail.getLiaRocPolicyCode());
								detailVO15.setDynamicProperty(LiaRocCst.LIAROC_BIZ_SOURCE,oldLastUpload.getBizSource());
								detailVO15.setDynamicProperty(LiaRocCst.LIAROC_BIZ_CODE, lastUploadDetail.getPolicyCode());
								
								markAFINSItemId(policyVO, oldLastUpload, policyId, detailVO15, lastUploadDetail);
							} else {

								if (currentAmount.equals(lastAmount) || NBUtils.in(currentAmount, StringUtils.EMPTY, "0")) { // 新式公會收件通報,保額相同才排除不通報
									NBUtils.logger(this.getClass(), "Skip by duplicate data");
									markCancelByAFINS(policyVO, oldLastUpload, policyId, newDetailVO, lastUploadDetail);
								} else {
									//PCR_454251 調整邏輯:保額不同作15更正通報
									((LiaRocUploadDetail2020VO) newDetailVO).setLiaRocPolicyStatus(LiaRocCst.LIAROC_POLICY_STATUS__FIX_ERROR);
									((LiaRocUploadDetail2020VO) newDetailVO).setStatusEffectDate(DateUtils.truncateDay(AppContext.getCurrentUserLocalTime()));
									markAFINSItemId(policyVO, oldLastUpload, policyId, newDetailVO, lastUploadDetail);
								}
							}
							/*
							 * sendNBReceive有此邏輯： 比對完還存在表示已被刪除要新增一筆狀態為6(契約撤銷)
							 * 但與Hebe溝通後因通報流程AFINS->FOA(BRBD)->UNB
							 * 三段收件通報,每段都是差異通報,無法系統比對出最後通報保項,不在先前通報的記錄中
							 * 若有此種情況，由人工自行上傳 06
							 */

						}
					} else if (LiaRocCst.LIAROC_UL_STATUS_WAITING.equals(status)
									|| LiaRocCst.LIAROC_UL_STATUS_QUEUE.equals(status)) {
						if (currentAmount.equals(lastAmount) || NBUtils.in(currentAmount, StringUtils.EMPTY, "0")) { //新式公會收件通報,保額相同才排除不通報
							//已通報此處不作通報
							NBUtils.logger(this.getClass(), "Skip by duplicate data,Status=" + status);
							markCancelByAFINS(policyVO, oldLastUpload, policyId, newDetailVO, lastUploadDetail);
						} else {
							//PCR_454251 調整邏輯:保額不同作15更正通報
							((LiaRocUploadDetail2020VO) newDetailVO).setLiaRocPolicyStatus(LiaRocCst.LIAROC_POLICY_STATUS__FIX_ERROR);				
							((LiaRocUploadDetail2020VO) newDetailVO).setStatusEffectDate(DateUtils.truncateDay(AppContext.getCurrentUserLocalTime()));
							markAFINSItemId(policyVO, oldLastUpload, policyId, newDetailVO, lastUploadDetail);
						}
					} else {

						/*通報流程失敗重新發送*/
						//TODO 原本通報若有錯是否要註記已發送第二次
					}
				}
			}

			//PCR 188901-以item_id查找保項的通報編號,並替換為真正要上傳的保項通報編號
			int countSameCode = 0;
			List<String> totalSameCodeList = new ArrayList<String>();
			for (List<String> sameCodeList : sameCodeMapList.values()) {
				for (String sameCode : sameCodeList) {
					if (totalSameCodeList.contains(sameCode) == false) {
						totalSameCodeList.add(sameCode);
					}
				}
			}

			if (NBUtils.in(uploadVO.getBizSource(), LiaRocCst.LIAROC_UL_BIZ_SOURCE_UNB)) {
				//將新契約 AFINS/FOA 未落地前的通報保單號寫入T_LIAROC_UPLOAD_COVERAGE_2020
				for (String oldPolicyCode : totalSameCodeList) {
					NBUtils.logger(this.getClass(), "syncLiarocUploadCoverageUNB(" + policyId + ", -1L, " + oldPolicyCode + ")");
					this.syncLiarocUploadCoverageUNB(policyId, -1L, oldPolicyCode);
				}
			}

			List<GenericEntityVO> afins12List = new ArrayList<GenericEntityVO>();
			
			for (GenericEntityVO genericEntityVO : fixnewDetails) {
				LiaRocUploadDetail2020VO detailVO = (LiaRocUploadDetail2020VO) genericEntityVO;
				String oldPolicyCode = detailVO.getDynamicProperty(LiaRocCst.LIAROC_POLICY_CODE);
				String oldBizSource = detailVO.getDynamicProperty(LiaRocCst.LIAROC_BIZ_SOURCE);
				String oldBizCode = detailVO.getDynamicProperty(LiaRocCst.LIAROC_BIZ_CODE);  //主約保單號碼

				if (oldPolicyCode == null) {//比對無重覆的通報記錄需重新設定保項通報號
					if (policyId.longValue() > 0) {
						//PCR 188901 落地保單舊件以保單號,新件以受理編號(無受理編號則以保單號)
						//落地保單case有保全FOA受理收件通報,新契約收件通報,保全收件通報
						oldPolicyCode = this.getLiaRocPolicyCode(policyId, detailVO.getItemId(), detailVO.getInternalId());
						NBUtils.logger(this.getClass(), "policyID > 0 ->getLiaRocPolicyCode(" + policyId + "," + detailVO.getItemId() + ")");
					} else {
						//未落地保單case有新契約 FOA受理收件通報
						countSameCode++;

						String planCode = StringUtils.trimToEmpty(detailVO.getInternalId());
						if (planCode.indexOf("-") != -1) {
							planCode = planCode.substring(0, planCode.indexOf("-"));
						}

						oldPolicyCode = policyVO.getPolicyNumber() + planCode + String.format("%03d", countSameCode + totalSameCodeList.size());
					}
				} else {
					if (NBUtils.in(oldBizSource, LiaRocCst.LIAROC_UL_BIZ_SOURCE_AFINS_POS,
									LiaRocCst.LIAROC_UL_BIZ_SOURCE_FOA_POS,
									LiaRocCst.LIAROC_UL_BIZ_SOURCE_POS)) {
						//item_id 綁定未落地保項通報編號
						this.syncLiarocUploadCoveragePOS(policyId, detailVO.getItemId(), oldPolicyCode);
						NBUtils.logger(this.getClass(), "syncLiarocUploadCoveragePOS(" + policyId + ", " + detailVO.getItemId() + ", "
										+ oldPolicyCode + ")");
					} else {

						if (policyId.longValue() > 0
										&& detailVO.getItemId() != null
										&& detailVO.getItemId().longValue() > 0
										&& oldPolicyCode.length() == 17
										&& NBUtils.in(oldBizSource, LiaRocCst.LIAROC_UL_BIZ_SOURCE_AFINS)
										&& oldPolicyCode.equals(oldBizCode)) {
							//落地保單通報且保項也落地,但前次通報保單號為17碼時間序號且為AFINS通報,重新取得編號記錄在t_liaroc_upload_coverage
							oldPolicyCode = this.getLiaRocPolicyCode(policyId, detailVO.getItemId(), detailVO.getInternalId());
							NBUtils.logger(this.getClass(), "時間序重新取號 ->getLiaRocPolicyCode(" + policyId + "," + detailVO.getItemId() + ")");
							
							if(NBUtils.in(detailVO.getCheckStatus(), LiaRocCst.CHECK_STATUS_UPLOAD)) { 
								//本次為可上傳但有AFINS時間序通報記錄,需作AFINS 12,本次作 15
								String detailListId = detailVO.getDynamicProperty(LiaRocCst.LIAROC_DETAIL_LIST_ID);
								if(detailListId != null) {
									//AFINS 12
									LiaRocUploadDetail2020 lastUploadDetail = liaRocUploadDetail2020Dao.load(Long.valueOf(detailListId));
									LiaRocUploadDetail2020 detailBo12 = this.createFixUploadDetail(lastUploadDetail, null,
											LiaRocCst.LIAROC_POLICY_STATUS__ERROR_TERMINATION);
									LiaRocUploadDetail2020VO detailVO12 = new LiaRocUploadDetail2020VO();
									detailBo12.copyToVO(detailVO12, Boolean.TRUE);
									detailBo12.setStatusEffectDate(DateUtils.truncateDay(AppContext.getCurrentUserLocalTime()));
									afins12List.add(detailVO12);
									//本次作 15
									detailVO.setLiaRocPolicyStatus(LiaRocCst.LIAROC_POLICY_STATUS__FIX_ERROR);
								}
							}
						} else {
							if (NBUtils.in(uploadVO.getBizSource(), LiaRocCst.LIAROC_UL_BIZ_SOURCE_UNB)) {
								//保單落地後需同保全一樣item_id 綁定未落地保項通報編號
								this.syncLiarocUploadCoveragePOS(policyId, detailVO.getItemId(), oldPolicyCode);
								NBUtils.logger(this.getClass(), "syncLiarocUploadCoveragePOS(" + policyId + ", " + detailVO.getItemId() + ", "
												+ oldPolicyCode + ")");
							}
						}
					}
				}

				NBUtils.logger(this.getClass(), "上傳保項編號=" + oldPolicyCode);
				detailVO.setLiaRocPolicyCode(oldPolicyCode);
				
				addLiarocCode(oldPolicyCode, 
								detailVO.getCertiCode() + "@" + detailVO.getInternalId() ,
								mapLiarocCode);
			}
			//END PCR 188901
			//＊＊＊替換有被移除及設定12-重報的上傳清單＊＊＊
			fixnewDetails.addAll(afins12List);
			uploadVO.setDataList(fixnewDetails);
		}

		return this.saveUpload(uploadVO);
	}

	/*
	 * PCR-391319 通路= BDR時，讀取AFINS收件檔(T_UW_NOTINTO_RECV)取得受理編號做資料比對 2020/09/12 Add by Kathy
	 */
	private String getAFINSSerialNum(PolicyVO policyVO, List<UwNotintoRecvVO> allUwNotintoRecvList,
			LiaRocUploadDetail2020 newDetailBO, String planCode) {
		
		String firstAfinsSerialNum = null;
		String amountAfinsSerialNum = null;
		for (UwNotintoRecvVO uwNotintoRecvVO : allUwNotintoRecvList) {
			String refBizCode = uwNotintoRecvVO.getDynamicProperty("bizCode");
			
			 //1 key送件，抓取AFINS的受理編號使用，需判斷未被引用或被引用是同張保單，不然會抓取到他張保單的受理編號導致取號到別張保單的號碼
			if (refBizCode == null || NBUtils.in(refBizCode, policyVO.getPolicyNumber())) {
				if (NBUtils.in(planCode, uwNotintoRecvVO.getInternalId())
						&& NBUtils.in(newDetailBO.getCertiCode(), uwNotintoRecvVO.getCeritCode())) {
					
					//因為會有mPOS先進件,AFINS後進,要再增加日期判斷,不然還是會取到他張的受理編號
					if(uwNotintoRecvVO.getApplyDate() != null && newDetailBO.getApplyDate() != null) {
						Date uploadApplyDate = DateUtils.truncateDay(newDetailBO.getApplyDate());
						if(uwNotintoRecvVO.getApplyDate().getTime() == uploadApplyDate.getTime()) {
							String afinsSerialNum = uwNotintoRecvVO.getSerialNum();
							if(afinsSerialNum != null && firstAfinsSerialNum == null) {
								firstAfinsSerialNum = afinsSerialNum;
							}
							
							//IR_470773 增加保額比對
							if(uwNotintoRecvVO.getAmount() != null && newDetailBO.getAmount() != null) {
								if(uwNotintoRecvVO.getAmount().equals(newDetailBO.getAmount())) {
									if(amountAfinsSerialNum == null) {
										amountAfinsSerialNum = afinsSerialNum;
									}
								}
							}
						}
					}
				}
			}
		}
		
		if(amountAfinsSerialNum != null) {
			return amountAfinsSerialNum;
		}
		
		return firstAfinsSerialNum;
	}
	
	
	/**
	 * 1KEY通報，比對到已通報，註記為不通報，若是比對到的是AFINS來源，則寫入主檔bizCode,policyId,policyCode及明細檔item_id
	 * @param policyVO
	 * @param oldLastUpload
	 * @param policyId
	 * @param newDetailVO
	 * @param lastUploadDetail
	 */
	private void markCancelByAFINS(PolicyVO policyVO, LiaRocUpload2020 oldLastUpload, Long policyId,
			GenericEntityVO newDetailVO, LiaRocUploadDetail2020 lastUploadDetail) {
		
		((LiaRocUploadDetail2020VO) newDetailVO).setLiaRocPolicyCode(lastUploadDetail.getLiaRocPolicyCode());
		((LiaRocUploadDetail2020VO) newDetailVO).setCheckStatus(LiaRocCst.CHECK_STATUS_CANCEL_BY_AFINS);
		((LiaRocUploadDetail2020VO) newDetailVO).setRefUploadListId(lastUploadDetail.getListId());
		
		markAFINSItemId(policyVO, oldLastUpload, policyId, newDetailVO, lastUploadDetail);
	}
	
	/**
	 * 案例: AFINS :01036661061(保額:300000)，01036661062(保額:1230000) <br/>
	 * 新契約:01036661061(保額:1230000)，01036661062(保額:300000) <br/>
	 * 編號同樣引用，但因保額不同會作通報，一併作itemId綁定
	 * @param policyVO
	 * @param oldLastUpload
	 * @param policyId
	 * @param newDetailVO
	 * @param lastUploadDetail
	 */
	private void markAFINSItemId(PolicyVO policyVO, LiaRocUpload2020 oldLastUpload, Long policyId,
			GenericEntityVO newDetailVO, LiaRocUploadDetail2020 lastUploadDetail) {
		
		((LiaRocUploadDetail2020VO) newDetailVO).setLiaRocPolicyCode(lastUploadDetail.getLiaRocPolicyCode());
		((LiaRocUploadDetail2020VO) newDetailVO).setRefUploadListId(lastUploadDetail.getListId());
		
		if(policyId > 0) { //已落地保單
			if(NBUtils.in(oldLastUpload.getBizSource(), LiaRocCst.LIAROC_UL_BIZ_SOURCE_AFINS,
					LiaRocCst.LIAROC_UL_BIZ_SOURCE_AFINS_POS,
					LiaRocCst.LIAROC_UL_BIZ_SOURCE_FOA_UNB,
					LiaRocCst.LIAROC_UL_BIZ_SOURCE_FOA_POS)) {
				
				oldLastUpload.setPolicyId(policyId);
				oldLastUpload.setBizCode(policyVO.getPolicyNumber());
				oldLastUpload.setPolicyCode(policyVO.getPolicyNumber());
				liaRocUpload2020Dao.save(oldLastUpload);
				
				lastUploadDetail.setItemId(((LiaRocUploadDetail2020VO) newDetailVO).getItemId());
				//POS查詢方法, 有替換LiaRocUploadDetail2020,因此一律load再更新
				LiaRocUploadDetail2020 entity = liaRocUploadDetail2020Dao.load(lastUploadDetail.getListId());
				entity.setItemId(((LiaRocUploadDetail2020VO) newDetailVO).getItemId());
				liaRocUploadDetail2020Dao.save(entity);
			}
		}
	}

	/**
	 * <p>Description : AFINS POS收件通報後保全FOA或保全收件通報呼叫
	 * 將AFINS POS,或FOA POS通報時產生的保項通報號碼與item_id綁定
	 * </p>
	 * <p>Created By : Simon.Huang</p>
	 * <p>Create Time : 2019年10月23日</p>
	 * @param policyId
	 * @param itemId
	 * @param liarocPolicyCode
	 */
	public void syncLiarocUploadCoveragePOS(Long policyId, Long itemId, String liarocPolicyCode) {

		if (itemId == null) {
			itemId = -1L;
		}

		Connection conn = null;
		CallableStatement stmt = null;
		DBean db = new DBean(true);
		try {

			db.connect();
			conn = db.getConnection();

			stmt = conn.prepareCall("{call PKG_LS_PM_NB_LIAROC_2020.P_SYNC_LIAROC_COVERAGE_POS(?, ?, ?)}");
			stmt.setLong(1, policyId);
			stmt.setLong(2, itemId);
			stmt.setString(3, liarocPolicyCode);

			stmt.execute();
		} catch (Exception e) {
			throw ExceptionFactory.parse(e);
		} finally {
			DBean.closeAll(null, stmt, db);
		}
	}

	/**
	 * <p>Description : AFINS POS收件通報後保全FOA或保全收件通報呼叫
	 * 將AFINS POS,或FOA POS通報時產生的保項通報號碼與item_id綁定
	 * </p>
	 * <p>Created By : Simon.Huang</p>
	 * <p>Create Time : 2019年10月23日</p>
	 * @param policyId
	 * @param itemId
	 * @param liarocPolicyCode
	 */
	public void syncLiarocUploadCoverageUNB(Long policyId, Long itemId, String liarocPolicyCode) {

		if (itemId == null) {
			itemId = -1L;
		}

		Connection conn = null;
		CallableStatement stmt = null;
		DBean db = new DBean(true);
		try {

			db.connect();
			conn = db.getConnection();

			stmt = conn.prepareCall("{call PKG_LS_PM_NB_LIAROC_2020.P_SYNC_LIAROC_COVERAGE_UNB(?, ?, ?)}");
			stmt.setLong(1, policyId);
			stmt.setLong(2, itemId);
			stmt.setString(3, liarocPolicyCode);

			stmt.execute();
		} catch (Exception e) {
			throw ExceptionFactory.parse(e);
		} finally {
			DBean.closeAll(null, stmt, db);
		}
	}

	@Override
	public Long sendNBRecCancel(Long policyId) {

		//RTC-223610-只要保單做拒保、延期、撤件就做收件通報06-未承保件取消
		Connection conn = null;
		CallableStatement stmt = null;
		DBean db = new DBean(true);
		try {

			db.connect();
			conn = db.getConnection();

			stmt = conn.prepareCall("{call PKG_LS_PM_NB_LIAROC_2020.P_NB_LIAROC_CANCEL_RECEIVE(?, ?)}");
			stmt.setLong(1, policyId);
			stmt.registerOutParameter(2, Types.DECIMAL);

			stmt.execute();
			BigDecimal uploadListId = stmt.getBigDecimal(2);

			return uploadListId.longValue();
		} catch (Exception e) {
			throw ExceptionFactory.parse(e);
		} finally {
			DBean.closeAll(null, stmt, db);
		}
	}

	//不必通報險種則不用寫明細檔
	private boolean isLiaRocProduct(LiaRocUploadDetail2020 detail) {

		if (StringUtil.isNullOrEmpty(detail.getLiaRocProductCategory())
						|| StringUtil.isNullOrEmpty(detail.getLiaRocProductType())) {
			return false;
		} else {
			return true;
		}
	}

	//檢查不必通報險種則不用寫明細檔
	private LiaRocUpload2020SendVO checkLiaRocProduct(LiaRocUpload2020SendVO vo) {
		List<? extends GenericEntityVO> oldDetails = vo.getDataList();
		List details = new ArrayList();
		for (GenericEntityVO detailVO : oldDetails) {
			LiaRocUploadDetail2020 detailBO = new LiaRocUploadDetail2020();
			BeanUtils.copyProperties(detailBO, detailVO);
			if (isLiaRocProduct(detailBO)) {
				details.add(detailVO);
			}
		}
		vo.setDataList(details);
		return vo;

	}

	/**
	 * <p>Description : 系統收到收件通報接口資料(包含新契約跟保全的資料)</p>
	 * <p>Created By : Victor Chang</p>
	 * <p>Create Time : Oct 4, 2016</p>
	 * @param policyId     
	 */
	@Override
	public Long sendAfinsNBReceive(UwNotintoRecvVO orgRecvVO) {

		UwNotintoRecvVO recvVO = (UwNotintoRecvVO) BeanUtils.cloneBean(orgRecvVO);

		//boolean unbFlag = false;
		//找出最後傳送的資料
		//String certiCode = vo.getCeritCode();
		String internalId = recvVO.getInternalId();
		//Date applyDate = vo.getApplyDate();
		String source = recvVO.getSource();

		boolean isUnb = false;

		//IR-367257-調整全通路通報保項，如AFINS已通報過之保項以ID、險種代號、保單號碼/受理編號為比對條件，不再重覆通報。
		//2021-01-19 調整已落地保單且mPOS進件, 因mPOS僅通報非03, AFINS只通報mPOS 03 
		//String submitChannel = "";
		//END IR-367257
		Long policyId = null;
		if ("POS".equals(recvVO.getDataType())) {
			isUnb = false;
			//POS來源一定要有保單號,沒有的話下面會報錯,
			policyId = policyDao.findPolicyIdByPolicyCode(orgRecvVO.getPolicyCode());
			orgRecvVO.setPolicyId(policyId);
		} else {
			isUnb = true;
			//t_uw_notinto_recv.seq_no=1989750(UNB來源,只有保單號,不為待承保,視為舊件)
			//t_uw_notinto_recv.seq_no=1954667(UNB來源,有受理編號無保單號,受理編號有對應的核心保單號,此件是舊件)
			//t_uw_notinto_recv.seq_no=1983645(UNB來源,有受理編號無保單號,受理編號有對應的核心保單號,但此件是新件)
			//seq_no=1954667,1983645案例相同但無法以受理編號來決定是否為新/舊件
			//只能處理seq_no=1989750的案例,保單號已存在核心且狀態不為待承保,視為保全件
			if (StringUtils.isNotBlank(orgRecvVO.getPolicyCode())) {
				policyId = policyDao.findPolicyIdByPolicyCode(orgRecvVO.getPolicyCode());
				orgRecvVO.setPolicyId(policyId);
				if (policyId != null) {
					//submitChannel = policyDao.findSubmitChannelByPolicyId(policyId);
					int liabilityState = policyDao.findLiabilityStatusByPolicyId(policyId);
					if (liabilityState != CodeCst.LIABILITY_STATUS__NON_VALIDATE) {
						//有效保單
						isUnb = false;
					} else {
						//待生效保單
						policyId = null;
						isUnb = true;
					}
				}
			}
		}

		String channelType = "";
		Long channelId = null;
		boolean isPolicyCode = false;

		String afinsPoliyCode = "";
		if (StringUtils.isNotBlank(recvVO.getPolicyCode())) {
			//AGY.STD才會有//有些BDR也會有
			isPolicyCode = true;
			afinsPoliyCode = recvVO.getPolicyCode();
		}
		
		if (isUnb && !StringUtils.isEmpty(recvVO.getSerialNum())) {//新契約才作受理編號上傳
			//有受理編號上來，強制下面流程均使用受理編號作policyCode作通報
			isPolicyCode = true;
			recvVO.setPolicyCode(recvVO.getSerialNum());
		}
		
		ChannelOrgInfo bdrChannelVO = null;
		if ("AGY".equals(recvVO.getSource())) {
			channelType = CodeCst.SALES_CHANNEL_TYPE__AGY;
		} else if ("STD".equals(source)) {
			channelType = CodeCst.SALES_CHANNEL_TYPE__STD;
		} else if ("BDR".equals(source)) {
			channelType = CodeCst.SALES_CHANNEL_TYPE__BR;
			if (StringUtils.isNotBlank(recvVO.getCompanyNo())) {
				channelId = liaRocUpload2020Dao.getChannelIdByCompanyNo(recvVO.getCompanyNo());
				if (channelId != null) {
					bdrChannelVO = channelOrgService.load(channelId);
					channelType = bdrChannelVO.getChannelType().toString();
				}
			}
		}

		LifeProduct lifeProduct = lifeProductService.getProductByInternalId(internalId);
		if (lifeProduct == null) {
			NBUtils.appLogger("無保項資料不作寫入 " + recvVO.getSeqNo() + "internalId=:=" + internalId);
			return null;
		}
		Long productId = lifeProduct.getProductId();
		// make 主檔 vo
		LiaRocUpload2020SendVO uploadVO = new LiaRocUpload2020SendVO();
		uploadVO.setBizSource(isUnb ? LiaRocCst.LIAROC_UL_BIZ_SOURCE_AFINS : LiaRocCst.LIAROC_UL_BIZ_SOURCE_AFINS_POS);
		uploadVO.setBizId(recvVO.getListId());
		if (isPolicyCode) {
			//AGY.STD才會有//有些BDR也會有
			if (StringUtils.isNotBlank(orgRecvVO.getPolicyCode())) {
				uploadVO.setBizCode(orgRecvVO.getPolicyCode());
			} else {
				uploadVO.setBizCode(recvVO.getPolicyCode());
			}
			uploadVO.setPolicyCode(uploadVO.getBizCode());
		} else {
			//BRBD 日期+時間+4碼序
			uploadVO.setBizCode(recvVO.getUploadPolicyNo());
		}
		uploadVO.setLiaRocUploadType(LiaRocCst.LIAROC_UL_TYPE_RECEIVE);
		uploadVO.setInputUser(AppContext.getCurrentUser().getUserId());
		uploadVO.setLiaRocUploadStatus(LiaRocCst.LIAROC_UL_STATUS_WAITING);

		uploadVO.setChannelOrgId(channelId);
		if (recvVO.getReceiveDate() != null) {
			try {
				Date receiveDate = TGLDateUtil.parseROCString("eeeMMddHHmmss", recvVO.getReceiveDate());
				uploadVO.setReceiveDate(receiveDate);
			} catch (ParseException e) {
				throw ExceptionUtil.parse(e);
			}
		}
		//PCR-243784 將AG登錄證字寫入公會主檔(t_liaroc_upload) add by Kathy
		uploadVO.setRegisterCode(recvVO.getRegisterCode());

		// setup 通報明細資料
		List<LiaRocUploadDetail2020VO> details = new ArrayList<LiaRocUploadDetail2020VO>();

		if (this.needUploadItem(lifeProduct.getProduct())
						&& StringUtils.isNotBlank(this.getLiaRocProdCateCode(productId))) {
			PolicyVO policy = new PolicyVO();
			policy.setApplyDate(recvVO.getApplyDate());
			policy.setPolicyId(policyId);
			policy.setChannelOrgId(channelId);
			policy.setChannelType(Long.parseLong(channelType));
			if (recvVO.getCurrency() != null && !StringUtil.isNullOrEmpty(recvVO.getCurrency().trim())) {
				policy.setCurrency(Integer.parseInt(recvVO.getCurrency()));
			}
			if (isPolicyCode) {
				//AGY.STD才會有
				policy.setPolicyNumber(recvVO.getPolicyCode());
			} else {
				//BRBD 日期+時間+4碼序
				policy.setPolicyNumber(recvVO.getUploadPolicyNo());
			}

			//需補上要保人, 不然與被保人關係判斷會有誤
			//PCR-243784 取得要保人ID modify by Kathy
			PolicyHolderVO policyHolder = new PolicyHolderVO();
			policyHolder.setCertiCode(recvVO.getPhCertiCode());
			policy.setPolicyHolder(policyHolder);

			CoverageVO coverage = new CoverageVO();
			coverage.setProductId(productId.intValue());
			coverage.setProductVersionId(lifeProductService.getProductVersion(productId, recvVO.getApplyDate()).getVersionId());
			CoveragePremium prem = new CoveragePremium();
			coverage.setCurrentPremium(prem);
			//繳費期間預設為年期
			coverage.setChargePeriod(CodeCst.CHARGE_PERIOD__CERTAIN_YEAR);
			//繳費年期預設為null  20210309 modify by Kathy
			coverage.setChargeYear(null);
			coverage.setApplyDate(recvVO.getApplyDate());

			if (isPolicyCode) {
				//AFINS 會傳入新增coverage item order
				int itemOrder = Integer.parseInt(recvVO.getCoverage());
				if (isUnb) {
					coverage.setItemOrder(itemOrder + 1);
				} else {

					//POS需看以報公會最後一筆 , 若沒有報公會的資料(DC件) 需找主檔的最大排序Order
					//int maxItemOrder = liaRocUploadDao.getMaxLiarocUploadItemOrder(recvVO.getPolicyCode(), "R");

					//2018/09/13 FIX取現行保項最大itemOrder加上AFINS傳入的itemOrder
					int maxItemOrder = 1;
					if (policyId != null) {
						maxItemOrder = policyDao.generateItemOrderByPolicyIdNoWaiver(policyId);
					}
					coverage.setItemOrder(maxItemOrder);//maxItemOrder已經加過1
				}
			}
			//看商品設定決定amount屬於單位, 計畫別或是保額
			String unitFlag = lifeProduct.getProduct().getUnitFlag();
			if (CodeCst.UNIT_FLAG_BY_UNIT.equals(unitFlag)) {
				prem.setUnit(new BigDecimal(recvVO.getAmount()));
			} else if (CodeCst.UNIT_FLAG_BY_BENEFIE_LEVEL.equals(unitFlag)) {
				if ("0".equals(recvVO.getAmount())) {
					//DD 說明：元/單位/計劃別(核心將0轉B)
					prem.setBenefitLevel("B");
				} else {
					prem.setBenefitLevel(recvVO.getAmount());
				}
			} else {
				prem.setSumAssured(new BigDecimal(recvVO.getAmount()));
			}

			if (StringUtils.isNotBlank(recvVO.getPrem())) {
				prem.setStdPremAf(new BigDecimal(recvVO.getPrem()));
				prem.setTotalPremAf(new BigDecimal(recvVO.getPrem()));
			}

			if (!StringUtil.isNullOrEmpty(recvVO.getPaidModx())) {
				int paidMod = Integer.parseInt(recvVO.getPaidModx()) - 1;
				if (paidMod > 0 && paidMod < 5) {
					coverage.setInitialChargeMode(String.valueOf(paidMod));
				} else {
					coverage.setInitialChargeMode(CodeCst.CHARGE_MODE__SINGLE);
				}
			}

			//被保人資料
			InsuredVO insured = new InsuredVO();
			insured.setCertiCode(recvVO.getCeritCode());

			insured.setName(recvVO.getName());
			if (!StringUtil.isNullOrEmpty(recvVO.getInsurantBDay())) {
				String birthday = recvVO.getInsurantBDay();
				if (birthday != null && birthday.length() == 6) {
					birthday = "0" + birthday;
				}

				insured.setBirthDate(TGLDateUtil.parseROCString(birthday
								.substring(0, 3)
								+ "/"
								+ birthday.substring(3, 5)
								+ "/" + birthday.substring(5, 7)));
			}
			CoverageInsuredVO coverInsured = new CoverageInsuredVO();
			coverInsured.setOrderId(1);
			coverInsured.setInsured(insured);
			List<CoverageInsuredVO> insureds = new ArrayList<CoverageInsuredVO>();
			insureds.add(coverInsured);
			coverage.setInsureds(insureds);

			LiaRocUploadDetail2020WrapperVO detailVO = generateUploadDetail(policy, coverage, LiaRocCst.LIAROC_UL_TYPE_RECEIVE, true);
			if (isPolicyCode == false) {
				//2018/06/28 Simon:未上傳PolicyCode以UploadPolicyNo作上傳policyNo,
				detailVO.setLiaRocPolicyCode(recvVO.getUploadPolicyNo());
			} else {
				if (policyId != null) { //POS 通報需記錄此次item_num取號
					/* PCR 188901 通報保單號調整以接口getLiarocPolicyCode(policyId,itemId)取得 */
					String liarocPolicyCode = this.getLiaRocPolicyCode(policyId, -1L, recvVO.getInternalId());
					detailVO.setLiaRocPolicyCode(liarocPolicyCode);
				}
			}
			
			if(StringUtils.isNotEmpty(afinsPoliyCode)) {
				uploadVO.setPolicyCode(afinsPoliyCode);
				detailVO.setPolicyCode(afinsPoliyCode);
			}
			
			detailVO.setStatusEffectDate(recvVO.getApplyDate());
			//AFINS 進件生效日一律使用要保書填寫日
			detailVO.setValidateDate(recvVO.getApplyDate());
			detailVO.setApplyDate(recvVO.getApplyDate());

			detailVO.setPhCertiCode(recvVO.getPhCertiCode());

			//IR-367257-調整全通路通報保項，如AFINS已通報過之保項以ID、險種代號、保單號碼/受理編號為比對條件，不再重覆通報。
			//mPos進件之外部通路(BD,BR)保單不寫入死亡給付險種(03)，由AFINS傳入死亡給付險種(03)做通報 (afins排除mPOS保經01,02)
			/* mark by PCR_454251_AFINS 進件執行落地保單比對
			if (CodeCst.SUBMIT_CHANNEL__MPOS.equals(submitChannel)) {
				if (NBUtils.in(LiaRocCst.LIAROC_BRBD_TYPE_01, LiaRocCst.LIAROC_BRBD_TYPE_02, detailvo.getBrbdType())) {
					NBUtils.logger(this.getClass(), detailvo.getBrbdType() + "->AFINS mPOS 不通報");
					detailvo.setCheckStatus(LiaRocCst.CHECK_STATUS_CANCEL_BY_AFINS);
					detailvo.setRefUploadListId(null);
				}
			}*/
			
			//PCR_454251_AFINS 進件執行落地保單比對
			if(isUnb) {
				String orgPolicyCode = orgRecvVO.getPolicyCode(); //保單號
				String orgSerialNum = orgRecvVO.getSerialNum();  //可能是受理編號或保單號
				List<String> policyCodes = new ArrayList<String>();
				List<LiaRocUploadDetail2020> lastUploadDetails = liaRocUploadDetail2020Dao.queryLastReceiveUploadDetail(
								LiaRocCst.LIAROC_UL_BIZ_SOURCE_AFINS,
								null, //channel_type
								detailVO.getPolicyCode(),
								detailVO.getCertiCode(),
								internalId,
								detailVO.getValidateDate(),
								orgSerialNum,
								null,
								LiaRocCst.IS_1KEY_TRUE,
								false, //isLiaRocPolicyStatus06,
								detailVO.getProposalTerm(),
								detailVO.getChargeYear());
				
				//本次通報明細保單號，不為時間序，才作保單號的過濾
				if(NBUtils.isAFINSDatetimeStr(detailVO.getPolicyCode()) == false) {
					
					//第一優先以保單號查詢,要保書狀態ybo 
					if(StringUtils.isNotBlank(orgPolicyCode) && NBUtils.isAFINSDatetimeStr(orgPolicyCode) == false) {
						int proposalStatus = policyDao.findProposalStatusByPolicyCode(orgPolicyCode);
						if(proposalStatus > 0 && proposalStatus < 80) {
							policyCodes.add(orgPolicyCode);
						}
					}
			
					//查找受理編號對應的保單號
					if(StringUtils.isNotBlank(orgSerialNum) && NBUtils.isAFINSDatetimeStr(orgSerialNum) == false) {
						//無保單資料再以受理編號查詢,要保書狀態
						if(policyCodes.size() == 0) {
							if(bdrChannelVO != null && NBUtils.isD4B(bdrChannelVO.getChannelCode())) {
								policyCodes = liaRocUploadPolicyDao.getPolicyCodesByMegaSerialNum(orgSerialNum);
							}
						}
						if(policyCodes.size() == 0) {
							policyCodes = liaRocUploadPolicyDao.getPolicyCodesBySerialNum(orgSerialNum);
						}
						if(policyCodes.size() == 0) {
							//無對應保單號,受理編號可能也是保單號,查詢要保書狀態
							int proposalStatus = policyDao.findProposalStatusByPolicyCode(orgSerialNum);
							if(proposalStatus > 0 && proposalStatus < 80) {
								policyCodes.add(orgSerialNum);
							}
						}
					}
				
					//有對應到未生效保單,執行保單號過濾
					if(policyCodes.size() > 0) {
						List<Predicate> anyPredicate = new ArrayList<Predicate>();
						for(String policyCode : policyCodes) {
							anyPredicate.add(PredicateUtils.equalPredicate(policyCode));
						}
						Predicate predicate = new BeanPredicate("policyCode", PredicateUtils.anyPredicate(anyPredicate));
						@SuppressWarnings("unchecked")
						List<LiaRocUploadDetail2020> policyCodeFilterDetails = (List<LiaRocUploadDetail2020>) CollectionUtils
								.select(lastUploadDetails, predicate);
						if(policyCodeFilterDetails != null && policyCodeFilterDetails.size() > 0) {
							lastUploadDetails = policyCodeFilterDetails;
						}
					}
				}
				
				//保額過濾				
				if(lastUploadDetails.size() > 0
						  && StringUtils.isNotEmpty(detailVO.getAmount())) {
					
					String currentAmount = detailVO.getAmount() == null ? "" : detailVO.getAmount();
					Predicate predicate = new BeanPredicate("amount", PredicateUtils.equalPredicate(currentAmount));
					@SuppressWarnings("unchecked")
					List<LiaRocUploadDetail2020> amountUploadDetails = (List<LiaRocUploadDetail2020>) CollectionUtils
							.select(lastUploadDetails, predicate);
					if(amountUploadDetails != null && amountUploadDetails.size() > 0) {
						lastUploadDetails = amountUploadDetails;
					}
				}
				
				//有比對到落地保單,本件不通報,並使用回寫落地保單item_id
				if(lastUploadDetails.size() > 0) {
					LiaRocUploadDetail2020 lastUploadDetail = lastUploadDetails.get(lastUploadDetails.size() - 1);	
					
					((LiaRocUploadDetail2020VO) detailVO).setLiaRocPolicyCode(lastUploadDetail.getLiaRocPolicyCode());
					((LiaRocUploadDetail2020VO) detailVO).setRefUploadListId(lastUploadDetail.getListId());
					((LiaRocUploadDetail2020VO) detailVO).setCheckStatus(LiaRocCst.CHECK_STATUS_CANCEL_BY_AFINS);
					((LiaRocUploadDetail2020VO) detailVO).setItemId(lastUploadDetail.getItemId());
					
					LiaRocUpload2020 oldLastUpload = liaRocUpload2020Dao.load(lastUploadDetail.getUploadListId());
					
					uploadVO.setPolicyId(oldLastUpload.getPolicyId());
					uploadVO.setBizCode(oldLastUpload.getBizCode());
					uploadVO.setPolicyCode(oldLastUpload.getPolicyCode());
					
				} else {
					//無落地保項符合,但保單已落地, 需依取號規則重新取號
					if(policyCodes.size() > 0) {
						Long tmpPolicyId = policyDao.findPolicyIdByPolicyCode(policyCodes.get(0));
						String tmpLiarocPolicyCode = getLiaRocPolicyCode(tmpPolicyId, new Long(-1), internalId);
						((LiaRocUploadDetail2020VO) detailVO).setLiaRocPolicyCode(tmpLiarocPolicyCode);
					}
				}
			}
			details.add(detailVO);
			uploadVO.setDataList(details);

		} else {
			uploadVO.setLiaRocUploadStatus(LiaRocCst.LIAROC_UL_STATUS_SEND_NO_ITEM);
			uploadVO.setRequestTransUUid("NO_ITEM");
		}

		Long uploadId = this.saveUpload(uploadVO);
		return uploadId;
	}

	/**
	 * PCR-188901
	 * <br/>取得保項通報用保單號,item_id查找t_liaroc_upload_coverage_2020
	 * <br/>1.已有通報保項號直接取用
	 * <br/>2.無通報保項項則依目前保項序號最大值+1 序編
	 * @param policyId
	 * @param itemId (若-1為AFINS POS或FOA POS傳入)
	 * @return
	 */
	public String getLiaRocPolicyCode(Long policyId, Long itemId, String planCode) {
		DBean db = new DBean();
		CallableStatement stmt = null;
		String liarocPolicyCode = "";
		if (itemId == null) {
			itemId = -1L;
		}
		try {
			db.connect();
			stmt = db.getConnection().prepareCall(
							"{? = call pkg_ls_cs_liaroc_bl_2020.f_get_item_policy_id(?, ?, ?) }");
			stmt.registerOutParameter(1, Types.VARCHAR);
			stmt.setLong(2, policyId);
			stmt.setLong(3, itemId);
			stmt.setString(4, planCode);
			stmt.execute();
			liarocPolicyCode = stmt.getString(1);
			return liarocPolicyCode;
		} catch (Exception ex) {
			throw ExceptionFactory.parse(ex);
		} finally {
			DBean.closeAll(null, stmt, db);
		}
	}

	//檢查明細檔人名長度不可超過10碼
	private LiaRocUploadDetail2020 fixDetailNameLength(LiaRocUploadDetail2020 bo) {
		String detailName = bo.getName();
		detailName = substring(detailName, 30);
		bo.setName(detailName);

		String holderName = bo.getPhName();
		holderName = substring(holderName, 30);
		bo.setPhName(holderName);

		return bo;

	}

	/**
	 * <p>Description : 截取字串的bytes_size(UTF-8 3 bytes,半形英數字為1 bytes)</p>
	 * <p>Created By : Simon.Huang</p>
	 * <p>Create Time : 2018年7月30日</p>
	 * @param str
	 * @param bytesize
	 * @return
	 */
	public static String substring(String str, int bytesize) {
		if (str != null) {
			StringBuffer sbf = new StringBuffer();
			int size = 0;
			while (bytesize > size && !com.ebao.pub.util.StringUtils.isNullOrEmpty(str)) {
				String sub = str.substring(0, 1);
				str = str.substring(1);
				size += sub.getBytes(Charset.forName("UTF-8")).length;
				if (size <= bytesize) {
					sbf.append(sub);
				}
			}
			return sbf.toString();
		}
		return str;
	}

	@Override
	public LiaRocUpload2020VO load(Long listId) {
		LiaRocUpload2020 bo = liaRocUpload2020Dao.load(listId);
		LiaRocUpload2020VO liaRocUploadVO = new LiaRocUpload2020VO();
		BeanUtils.copyProperties(liaRocUploadVO, bo);
		return liaRocUploadVO;
	}

	@Override
	public void uploadLiaRocUploadStatusToQ(List<String> bizSource,
					String liarocUploadType) {
		liaRocUpload2020Dao.updateStatusToQ(bizSource, liarocUploadType);

	}

	@Override
	public void uploadLiaRocUploadStatusTo9() {
		liaRocUpload2020Dao.updateStatusTo9();
	}

	private LiaRocUploadDetail2020 createFixUploadDetail(LiaRocUploadDetail2020 input, Long listId, String liaRocStatus) {
		LiaRocUploadDetail2020 detailBO = new LiaRocUploadDetail2020();
		BeanUtils.copyProperties(detailBO, input);
		detailBO.setLiaRocPolicyStatus(liaRocStatus);
		detailBO.setUploadListId(listId);
		detailBO.setListId(null);
		return detailBO;
	}

	private static String UNB_QUERY_BY_SAME_CODE_LIST = "select lud.liaroc_policy_code from t_liaroc_upload_2020 lu "
					+ " join t_liaroc_upload_detail_2020 lud on lu.list_id = lud.upload_list_id and lud.check_status = '1' "
					+ " where lu.biz_source = ? and lu.biz_code = ? order by lud.list_id";

	private List<String> getPolicyListByBizCode(String bizSource, String bizCode) {

		List<String> oldPolicyCodes = new ArrayList<String>();
		DBean db = null;
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			db = new DBean();
			db.connect();
			conn = db.getConnection();
			pstmt = conn.prepareStatement(UNB_QUERY_BY_SAME_CODE_LIST);
			pstmt.setString(1, bizSource);
			pstmt.setString(2, bizCode);

			rs = pstmt.executeQuery();

			while (rs.next()) {
				String oldPolicyCode = rs.getString("LIAROC_POLICY_CODE");
				if (oldPolicyCodes.contains(oldPolicyCode) == false) {
					oldPolicyCodes.add(oldPolicyCode);
				}
			}
		} catch (Exception e) {
			throw ExceptionFactory.parse(e);
		} finally {
			DBean.closeAll(null, pstmt, db);
		}
		return oldPolicyCodes;
	}

	/**
	 * PCR 188901:新契約承保通報作業,
	 * <br/>因於收件通報因會有AFINS以受理編號通報但新契約未輸入受理編號以保單號通報
	 * <br/>承保通報需重新以新契約是否有輸入受理編號統一所有保項的通報編號,才不會有受理編號及保單號混合通報的情形
	 * @param policyId 
	 * @param itemId
	 * @param liaRocPolicyCode  
	 * @return
	 */
	private String syncNBInforceLiaRocPolicyCode(Long policyId, Long itemId, String liaRocPolicyCode) {
		DBean db = new DBean();
		CallableStatement stmt = null;
		String liarocPolicyCode = "";
		try {
			db.connect();
			stmt = db.getConnection().prepareCall(
							"{call PKG_LS_PM_NB_LIAROC_2020.P_SYNC_NB_INFORCE_POLICY_CODE(?, ?, ?, ?) }");
			stmt.setLong(1, policyId);
			stmt.setLong(2, itemId);
			stmt.setString(3, liaRocPolicyCode);
			stmt.registerOutParameter(4, Types.VARCHAR);
			stmt.execute();
			liarocPolicyCode = stmt.getString(4);
			return liarocPolicyCode;
		} catch (Exception ex) {
			throw ExceptionFactory.parse(ex);
		} finally {
			DBean.closeAll(null, stmt, db);
		}
	}

	/**
	 *  新契約UNDO 後保單全量執行通報作業：
	 *  當承保後UNDO,以該筆保單的所有保項進行承保通報06-契約撤銷
	 *  當撤銷後UNDO,以該筆保單的所有保項進行收件通報01-有效
	 * @Author  : Simon Huang
	 * @Modify Date: 2019/07/07
	 * @Param liaRocUploadType (1:收件, 2:承保)
	 */
	@Override
	public Long sendNBUndo(Long policyId, String liaRocUploadType) {

		Long uploadListId = null;
		String changeType = "";
		//一律通報不作保項比對
		LiaRocUpload2020SendVO sendVO = this.getUploadVOByPolicyId(policyId,
						liaRocUploadType, LiaRocCst.LIAROC_UL_BIZ_SOURCE_UNB);

		for (GenericEntityVO genericEntityVO : sendVO.getDataList()) {
			LiaRocUploadDetail2020VO detailVO = (LiaRocUploadDetail2020VO) genericEntityVO;
			if (LiaRocCst.LIAROC_UL_TYPE_RECEIVE.equals(liaRocUploadType)) {

				detailVO.setLiaRocPolicyStatus(LiaRocCst.LIAROC_POLICY_STATUS__FIX_ERROR);
				//2021/10/28 Fanny Email:撤件後UNDO保單補15通報更正，保單狀況生效日應為資料修正的異動生效日期
				detailVO.setStatusEffectDate(new Date());
				//12-撤件UNDO後通報收件01 
				//PCR_454251_調整為通報15
				changeType = LiaRocCst.LIAROC_NB_BATCH_CHANGE_TYPE_12;
			} else {
				//PCR_466194_調整06(契約撤銷)為12-鍵值欄位通報錯誤終止
				detailVO.setLiaRocPolicyStatus(LiaRocCst.LIAROC_POLICY_STATUS__ERROR_TERMINATION);
				changeType = LiaRocCst.LIAROC_NB_BATCH_CHANGE_TYPE_11;
			}
		}
		uploadListId = this.saveUpload(sendVO);

		//寫入新契約通報異動檔作每日報表比對用
		liaRocUploadCommonService.recordNBBatchData(policyId, changeType);

		return uploadListId;
	}

	/**
	 *  PCR- 337606 FOA已送件，但新契約未收件之保單執行公會收件通報作業：
	 *  提供一個接口給通路(SC)在做保單號碼變更時將舊保單號碼通報06(終止)
	 *  foa 曾通報過公會01生效資料做06 終止通報
	 * @Author  : Vince Cheng
	 * @Modify Date: 2020/07/14
	 * @Param policyCode (foa 資料尚未落地只有保單號碼，無policy_id)
	 * @Param caseId (t_sc_foa_unb_case.list_id)
	 */
	@Override
	public Long sendFoaUndo(Long caseId) {
		Long uploadListId = null;

		FoaPolicyVO vo = foaAcceptanceCI.findFoaPolicyByCaseId(caseId);
		PolicyVO policyVO = foaAcceptanceHelper.convert2PolicyVO(vo, false);
		ApplicationLogger.setPolicyCode(policyVO.getPolicyNumber());

		//先取出foa 曾通報過公會資料
		List<LiaRocUploadDetail2020> lastUploadDetails = liaRocUploadDetail2020Dao.queryFoaUploadDetailByPolicyCode(caseId);
		//執行通報06-終止通報
		if (lastUploadDetails != null && lastUploadDetails.size() > 0) {
			try {
				// 公會通報主檔
				LiaRocUpload2020SendVO info = this.generateFoaUpload(policyVO, vo);
				info.setLiaRocUploadStatus(LiaRocCst.LIAROC_UL_STATUS_WAITING);

				List<LiaRocUploadDetail2020VO> fixnewDetails = new ArrayList<LiaRocUploadDetail2020VO>();

				for (LiaRocUploadDetail2020 uploadDetail : lastUploadDetails) {
					LiaRocUploadDetail2020 detailBO = new LiaRocUploadDetail2020();
					BeanUtils.copyProperties(detailBO, uploadDetail);
					detailBO.setLiaRocPolicyStatus(LiaRocCst.LIAROC_POLICY_STATUS__FREELOOK);
					detailBO.setStatusEffectDate(DateUtils.truncateDay(AppContext.getCurrentUserLocalTime()));
					detailBO.setListId(null);
					detailBO.setUploadListId(uploadListId);
					LiaRocUploadDetail2020VO detailVO06 = new LiaRocUploadDetail2020VO();
					detailBO.copyToVO(detailVO06, Boolean.TRUE);
					fixnewDetails.add(detailVO06);
				}

				info.setDataList(fixnewDetails);
				//寫入公會收件通報上傳主檔和明細檔
				uploadListId = this.saveUpload(info);

			} catch (Exception e) {
				logger.error(ExceptionUtils.getFullStackTrace(e));
				ApplicationLogger.addLoggerData(ExceptionUtils.getFullStackTrace(e));
				ApplicationLogger.setReturnCode(ApplicationLogger.RETURN_CODE_FAIL);
			} finally {
				try {
					ApplicationLogger.flush();
				} catch (Exception e) {
					logger.error(e);
				}
			}
		}
		return uploadListId;
	}

	/**
	 * <p>Description : 依保單資料產生公會上傳主檔(無上傳明細)</p>
	 * <p>Created By : Simon.Huang</p>
	 * <p>Create Time : 2019年10月30日</p>
	 * @param policyVO
	 * @param liaRocUploadType
	 * @param bizSource
	 * @return
	 */
	@Override
	public LiaRocUpload2020SendVO createLiarocUploadMaster(PolicyVO policyVO, String liaRocUploadType, String bizSource) {
		// make 主檔 vo
		LiaRocUpload2020SendVO uploadVO = new LiaRocUpload2020SendVO();
		uploadVO.setBizSource(bizSource);
		uploadVO.setBizId(policyVO.getPolicyId());
		uploadVO.setBizCode(policyVO.getPolicyNumber());
		uploadVO.setPolicyCode(policyVO.getPolicyNumber());
		uploadVO.setPolicyId(policyVO.getPolicyId());

		uploadVO.setLiaRocUploadType(liaRocUploadType);
		uploadVO.setLiaRocUploadStatus(LiaRocCst.LIAROC_UL_STATUS_WAITING);

		uploadVO.setChannelOrgId(policyVO.getChannelOrgId());
		uploadVO.setReceiveDate(policyVO.getSubmissionDate());
		if(policyVO.gitMasterCoverage() != null) {
			List<CoverageAgentVO> agents = policyVO.gitMasterCoverage().gitSortCoverageAgents();
			if (agents != null && agents.size() > 0) {
				uploadVO.setRegisterCode(agents.get(0).getRegisterCode());
			}
		}
		uploadVO.setInputUser(AppContext.getCurrentUser().getUserId());
		
		//新契約承保通報設定902需上傳 2023/01/17 Add by Kathy
		if (LiaRocCst.LIAROC_UL_TYPE_INFORCE.equals(liaRocUploadType)) {
			uploadVO.setSend902Flag(LiaRocCst.LIAROC_UL_SEND_902_FLAG_1);
			uploadVO.setLiaRocUploadStatus902(LiaRocCst.LIAROC_UL_STATUS_WAITING);			
		}
		
		return uploadVO;
	}

	/**
	 * <p>Description : PCR-338437-調整公會通報更正作業及比對規則<br/>
	 * 新契約-每日收件通報批次作業，依傳入保項產生通報明細檔</p>
	 * <p>Created By : Simon.Huang</p>
	 * <p>Create Time : 2019年11月22日</p>
	 * @param policy
	 * @param coverageList
	 * @param liaRocUploadType
	 * @return
	 */
	public List<LiaRocUploadDetail2020WrapperVO> generateNBBatchUploadDetail(PolicyVO policy, List<CoverageVO> coverageList,
					String liaRocUploadType, String changeType) {
		List<LiaRocUploadDetail2020WrapperVO> uploadDetailList = new ArrayList<LiaRocUploadDetail2020WrapperVO>();
		for (CoverageVO coverage : coverageList) {
			if (this.needUploadItem(coverage,liaRocUploadType)) {
				LiaRocUploadDetail2020WrapperVO uploadDetailVO = generateUploadDetail(policy, coverage, liaRocUploadType, false);
				//有做新增/變更險種/保額更正時再判斷是否曾通報過公會，其他變更項都以保單現況做通報 2021/03/09 Add by Kathy
				String liarocPolicyCode = null;
				boolean isLiaRocPolicyStatus06 = true;
				String planCode = StringUtils.trimToEmpty(uploadDetailVO.getInternalId());
				if (planCode.indexOf("-") != -1) {
					planCode = planCode.substring(0, planCode.indexOf("-"));
				}
				List<LiaRocUploadDetail2020> lastUploadDetails = filterByLiarocCoverage(liaRocUploadDetail2020Dao
						.queryLastReceiveUploadDetail(LiaRocCst.LIAROC_UL_BIZ_SOURCE_UNB, String.valueOf(policy.getChannelType()),
								policy.getPolicyNumber(), uploadDetailVO.getCertiCode(), planCode,
								uploadDetailVO.getValidateDate(), policy.getSerialNum(), policy.getMegaSerialNum(), LiaRocCst.IS_1KEY_FALSE,
								isLiaRocPolicyStatus06, uploadDetailVO.getProposalTerm(),
								uploadDetailVO.getChargeYear()), policy.getPolicyId());

				// 若此新增險種有曾通報過公會要以原有號碼做通報，不再重新取號 2020/11/20 Add by Kathy
				if (lastUploadDetails.size() != 0) {
					LiaRocUploadDetail2020 lastUploadDetail = lastUploadDetails.get(0);
					liarocPolicyCode = lastUploadDetail.getLiaRocPolicyCode();
				} else {
					liarocPolicyCode = this.getLiaRocPolicyCode(policy.getPolicyId(), coverage.getItemId(),
							uploadDetailVO.getInternalId());
				}

				uploadDetailVO.setLiaRocPolicyCode(liarocPolicyCode);
				uploadDetailList.add(uploadDetailVO);
			}
		}
		return uploadDetailList;
	}

	/**
	 * 判斷險種是否需通報(豁免險不必通報)
	 */
	@Override
	public boolean needUploadItem(CoverageVO coverage, String liaRocUploadType) {
		if (coverage.isWaiver()) {
			NBUtils.logger(getClass(), "needUploadItem(" + coverage.getItemId() + "," + coverage.getProductId() + "),Skip by isWaiver");
			return false;
		}
		//取得商品的公會通報-險種代碼, XX為無需通報(XGA/XGB)
		String liaRocProdCode = this.getLiaRocProdCode(coverage.getProductId().longValue());
		if (LiaRocCst.LIAROC_PORD_CODE_XX.equals(liaRocProdCode)) {
			NBUtils.logger(getClass(), "needUploadItem(" + coverage.getItemId() + "," + coverage.getProductId() + "),Skip by LIAROC_PORD_CODE_XX");
			return false;
		}

		if (coverage.getLifeInsured1() != null && coverage.getLifeInsured1().getInsured() != null) {
			InsuredVO insuredVO = coverage.getLifeInsured1().getInsured();
			//UAT-IR 423245收件通報中被保險人姓名非必填，承保通報時再檢查無記名
			if (NBUtils.isEmptyOrDummyName(insuredVO.getName())
							&& LiaRocCst.LIAROC_UL_TYPE_INFORCE.equals(liaRocUploadType)) {
				//IR-241196 comment, 2.無記名及原失效之附約資料,不需上傳公會.
				NBUtils.logger(getClass(), "needUploadItem(" + coverage.getItemId() + "," + coverage.getProductId() + "),Skip by isEmptyOrDummyName");
				return false;
			}
		}

		return true;
	}

	/**
	 * 判斷險種是否需通報(豁免險不必通報)
	 */
	@Override
	public boolean needUploadItem(ProductVO productVO) {
		if (CodeCst.YES_NO__YES.equals(productVO.getWaiver())) {
			return false;
		}

		//取得商品的公會通報-險種代碼, XX為無需通報(XGA/XGB)
		String liaRocProdCode = this.getLiaRocProdCode(productVO.getProductId());
		if (LiaRocCst.LIAROC_PORD_CODE_XX.equals(liaRocProdCode)) {
			return false;
		}

		return true;
	}

	/**
	 * <p>Description : IR-353680-調整AFINS傳送保項時保項編號以A01、A02...續編。	
	 * 新契約案件如險種已由核心系統自動通報，而AFINS是保單資料已進核心之後才有通報資料時，
	 * 應增加判斷AFINS的受理編號與核心系統的保單號碼相同時，保項號碼應以最大值往後編。不應產生保單號碼+保項重覆，但險種不相同之情形。
	 * </p>
	 * <p>Created By : Simon.Huang</p>
	 * <p>Create Time : 2020年2月4日</p>
	 * @param policyId
	 * @param certiCode
	 * @param internalId
	 * @param validateDate
	 * @param liaRocPolicyCode
	 * @param itemOrder
	 * @return
	 */
	private String generateAfinsPolicyCode(Long policyId, String certiCode, String internalId, Date validateDate,
					String liaRocPolicyCode, int itemOrder, String liaRocBrbdType) {

		String checkFmt = LiaRocCst.ITEM_ORDER_FMT_NORMAL;
		if (LiaRocCst.LIAROC_BRBD_TYPE_03.equals(liaRocBrbdType)) {
			checkFmt = LiaRocCst.ITEM_ORDER_FMT_AFINS_99;
		} else if (LiaRocCst.LIAROC_BRBD_TYPE_02.equals(liaRocBrbdType)) {
			checkFmt = LiaRocCst.ITEM_ORDER_FMT_AFINS_98;
		}

		String tmpPolicyCode = liaRocPolicyCode + String.format(checkFmt, itemOrder);
		int breakCnt = 0;

		//AFINS 未落地保單才作已通報保項判斷(新件保單)
		if (policyId == null || policyId.longValue() < 1) {
			List<LiaRocUploadDetail2020> resultList = liaRocUploadDetail2020Dao.queryAfinsUploadDetailByCertiCode(certiCode, validateDate);

			if (resultList != null && resultList.size() > 0) {
				try {
					List<String> historyPolicyCode = NBUtils.getPropertyStringList(resultList, "policyId");
					//若上傳公會保單號已重覆需重新編號
					while (historyPolicyCode.contains(tmpPolicyCode) == true
									&& breakCnt < 50) {

						//增加判斷若之前公會保單號相同且險種相同，不作重新編號，並以上次通報公會保單號作通報
						for (LiaRocUploadDetail2020 oldUploadDetail : resultList) {
							if (oldUploadDetail.getLiaRocPolicyCode().indexOf(liaRocPolicyCode) != -1
											&& NBUtils.in(oldUploadDetail.getInternalId(), internalId)) {
								return oldUploadDetail.getLiaRocPolicyCode();
							}
						}
						breakCnt++;
						itemOrder++;
						tmpPolicyCode = liaRocPolicyCode + String.format(checkFmt, itemOrder);
					}
				} catch (Exception e) {
					//do nothing
				}
			}
		}
		return tmpPolicyCode;
	}

	/**
	 * <p>Description : FOA 會有重複送件問題，所以要檢核，第二筆壓 D</p>
	 * <p>Created By : Simon Huang</p>
	 * <p>Create Time : 2020年9月9日</p>
	 * @param bizId
	 * @param bizSource
	 * @return
	 */
	private int countFoaLiarocUpload(long bizId, String bizSource) {
		DBean db = null;
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		int result = 0;

		try {

			StringBuffer sql = new StringBuffer();

			sql.append("select count(1) count from T_LIAROC_UPLOAD_2020 t where t.biz_source = ? and t.biz_id = ? ");

			db = new DBean();
			db.connect();
			conn = db.getConnection();
			pstmt = conn.prepareStatement(sql.toString());

			pstmt.setString(1, bizSource);
			pstmt.setLong(2, bizId);

			rs = pstmt.executeQuery();

			while (rs.next()) {
				result = rs.getInt("COUNT");
			}

		} catch (Exception e) {
			throw ExceptionFactory.parse(e);
		} finally {
			DBean.closeAll(rs, pstmt, db);
		}

		return result;
	}

	/**
	 * 針對同個被保險人，投保多保相同檢種的取號邏，將比對出舊件，此通報明細使用的通報取號記錄下來
	 * @param liarocPolicyCode
	 * @param compareKey
	 * @param mapLiarocCode
	 */
	private void addLiarocCode(String liarocPolicyCode, String compareKey, Map<String, Set<String>> mapLiarocCode) {
		
		Set<String> liarocCodeSet = mapLiarocCode.get(compareKey);
		if(liarocCodeSet == null) {
			liarocCodeSet = new HashSet<String>();
			mapLiarocCode.put(compareKey, liarocCodeSet);
		}
		liarocCodeSet.add(liarocPolicyCode);
	}

	/**
	 * 比對出的舊件，僅保留未被使用過的清單
	 * @param oldUploadDetails
	 * @param compareKey
	 * @param mapLiarocCode
	 * @return
	 */
	private List<LiaRocUploadDetail2020> removeLiarocCode(List<LiaRocUploadDetail2020> oldUploadDetails, 
					String compareKey, Map<String, Set<String>> mapLiarocCode, Long policyId, Long itemId) {
		Set<String> liarocCodeSet = mapLiarocCode.get(compareKey);
		if(liarocCodeSet == null) {
			liarocCodeSet = new HashSet<String>();
			mapLiarocCode.put(compareKey, liarocCodeSet);
			//liarocCodeSet 會由addLiarocCode()，放入已被使用的通報編號
		}
		List<LiaRocUploadDetail2020> newDetailList = new ArrayList<LiaRocUploadDetail2020>();
		
		for(LiaRocUploadDetail2020 oldDetail2020 : oldUploadDetails) {
			if(liarocCodeSet.contains(oldDetail2020.getLiaRocPolicyCode()) == false) {
				newDetailList.add(oldDetail2020);
			
				//已落地保單通報,detail有綁定item_id (可能是AFINS比對到後綁定的),則需與此次通報item_id一致
				if(policyId > 0 && oldDetail2020.getItemId() != null 
						&& oldDetail2020.getItemId().equals(itemId) == false) {
					newDetailList.remove(oldDetail2020);
				}
			}
		}
		return newDetailList;
	}
	  
    /**
     * 新增險種查找舊的通報記錄會有多筆，需排除取號檔itemID不為-1的資料(表示已被佔用)
     * @param lastUploadDetails
     * @param policyId
     * @return
     */
    private List<LiaRocUploadDetail2020> filterByLiarocCoverage(List<LiaRocUploadDetail2020> lastUploadDetails, Long policyId) {
    	List<LiaRocUploadDetail2020> filterLastUploadDetails = liaRocUploadCoverage2020Dao.filterByLiarocCoverage(lastUploadDetails, policyId);
	    return filterLastUploadDetails;
	}
    
	private String getInternalIdByProuctId(int productId) {
		return CodeTable.getCodeById("V_PRODUCT_LIFE_1", String.valueOf(productId));
	}

	@Override
	public void execute(com.ebao.ls.ws.vo.RqHeader rqHeader, LiaRocUploadProcessResultRootVO vo) {
		liaRocUploadCommonService.execute(rqHeader, vo);
	}

	@Override
	public String getLiaRocProdCateCode(Long productId) {
        String prodCateCode = null;
        String key = "liaRocProdCateCode_" + productId;
        try {
            prodCateCode = (String) (Cache.get(Cst.CACHE_PRODUCT_DEFINITION, key));
        } catch (CacheObjectNotFoundException e) {
        	prodCateCode =  liaRocUpload2020Dao.getLiaRocProdCateCode(productId);
            Cache.put(Cst.CACHE_PRODUCT_DEFINITION, key, prodCateCode);
        }
		return prodCateCode;
	}

	@Override
	public String getLiaRocProdCode(Long productI) {
		return liaRocUpload2020Dao.getLiaRocProdCode(productI);
	}
	
	public void restUpload(LiaRocUpload2020SendVO uploadVO) {
		
		String liaRocULType = uploadVO.getLiaRocUploadType();
		
		//新公會收件通報的保項資料
		List<Liaroc20UploadRqReceiveVO> dataRqReceiveList = new ArrayList<Liaroc20UploadRqReceiveVO>();
		List<Liaroc20UploadRqInforceVO> dataRqInforceList = new ArrayList<Liaroc20UploadRqInforceVO>();
		
		for (GenericEntityVO genericEntityVO : uploadVO.getDataList()) {
			LiaRocUploadDetail2020VO detailVO = (LiaRocUploadDetail2020VO) genericEntityVO;

			//wrapper才有convertUploadRqBaseVO()
			LiaRocUploadDetail2020WrapperVO wrapperVO = new LiaRocUploadDetail2020WrapperVO();
			BeanUtils.copyProperties(wrapperVO, detailVO, true);

			
			if (LiaRocCst.LIAROC_UL_TYPE_RECEIVE.equals(liaRocULType)) {
				Liaroc20UploadRqReceiveVO rqReceiveVO = (Liaroc20UploadRqReceiveVO) wrapperVO
						.convertUploadRqBaseVO(LiaRocCst.LIAROC_UL_TYPE_RECEIVE);
				dataRqReceiveList.add(rqReceiveVO);
				
			} else if (LiaRocCst.LIAROC_UL_TYPE_INFORCE.equals(liaRocULType)) {
				Liaroc20UploadRqInforceVO rqInforceVO = (Liaroc20UploadRqInforceVO) wrapperVO
						.convertUploadRqBaseVO(LiaRocCst.LIAROC_UL_TYPE_INFORCE);
				dataRqInforceList.add((Liaroc20UploadRqInforceVO) rqInforceVO);
				
			} 
		}

		Liaroc20UploadRsVO uploadRsVO = null;

		NBUtils.appLogger("[INFO]準備呼叫服務平台....");

		String statusIndi = LiaRocCst.LIAROC_UL_STATUS_WAITING;
		String rqUid = RstfClientUtils.genRqUid(Liaroc20Cst.TF_UPLOAD.getCode(), Thread.currentThread().getId());
	
		try {
			int maxCount = 4;
			for (int i = 0; i < maxCount; i++) {
				try {
					if (LiaRocCst.LIAROC_UL_TYPE_RECEIVE.equals(liaRocULType)) {
						String disableLiaroc20 = Para.getParaValue(LiaRocCst.PARAM_DISABLE_LIAROC20_RECEIVE);
						if(CodeCst.YES_NO__YES.equals(disableLiaroc20)) {
							uploadRsVO = new Liaroc20UploadRsVO();
							NBUtils.appLogger("[INFO]已關閉公會2.0通報..");
						} else {
							uploadRsVO = liaroc20CI.doLiaroc20UploadReceive(rqUid, Liaroc20Cst.MN_UNB.getCode(),
									dataRqReceiveList, Liaroc20Cst.PM_ASYNC.getCode());
						}
					} else if (LiaRocCst.LIAROC_UL_TYPE_INFORCE.equals(liaRocULType)) {
						String disableLiaroc20 = Para.getParaValue(LiaRocCst.PARAM_DISABLE_LIAROC20_INFORCE);
						if(CodeCst.YES_NO__YES.equals(disableLiaroc20)) {
							uploadRsVO = new Liaroc20UploadRsVO();
							NBUtils.appLogger("[INFO]已關閉公會2.0通報..");	
						} else {
							uploadRsVO = liaroc20CI.doLiaroc20UploadInforce(rqUid, Liaroc20Cst.MN_UNB.getCode(), dataRqInforceList);	
						}
					} else {
						NBUtils.appLogger("[ERROR]上傳失敗 無法解析LIAROC_UL_TYPE(收件/承保) ");
					}
					break; //成功發送後中斷for迴圈
					
					
				} catch (Exception e) {
					if (i >= (maxCount - 1)) {
						throw e;
					} else {
						String errMsg = ExceptionInfoUtils.getExceptionMsg(e);
						if (errMsg.indexOf("Connection timed out") != -1) {
							//發送失敗(可能是網路瞬斷)暫停10秒,retry
							NBUtils.appLogger("[ERROR]Connection timed out :次數 " + (i + 1));
							Thread.sleep(2000);
						} else {
							//其它錯誤將訊息往外拋
							throw e;
						}
					}
				}
			}

			if (uploadRsVO == null) {
				statusIndi = LiaRocCst.LIAROC_UL_STATUS_SEND_FAIL;
				NBUtils.appLogger("[INFO]呼叫服務平台發送通報有誤.. null");
			} else if (uploadRsVO != null && uploadRsVO.getError() != null) {
				statusIndi = LiaRocCst.LIAROC_UL_STATUS_SEND_FAIL;
				NBUtils.appLogger("[INFO]呼叫服務平台發送通報有誤.." + uploadRsVO.getError().getCode());
				NBUtils.appLogger("[INFO]呼叫服務平台發送通報有誤.." + uploadRsVO.getError().getMessage());
			} else {
				statusIndi = LiaRocCst.LIAROC_UL_STATUS_SEND;
				if (uploadRsVO.getSuccess() != null) {
					NBUtils.appLogger("[INFO]呼叫服務平台發送通報完成..");
				}
			}

		} catch (Exception e) {
			statusIndi = LiaRocCst.LIAROC_UL_STATUS_PROCESS_ERROR;
			NBUtils.appLogger(ExceptionInfoUtils.getExceptionMsg(e));
		}

		Map<Long, String> uploadStatus = new HashMap<Long, String>();
		uploadStatus.put(uploadVO.getListId(), statusIndi);

		// 將通報的狀態更新回 DB
		NBUtils.appLogger("[INFO]更新通報狀態.." + rqUid);
		this.updateUploadStatus(uploadStatus, rqUid, 1, new Date(), AppContext.getCurrentUser().getUserId());

		LiaRocUpload2020 uploadBO = liaRocUpload2020Dao.load(uploadVO.getListId());
		//REST 接口,即時通報,更新ResponseTransUUid
		if (LiaRocCst.LIAROC_UL_STATUS_SEND.equals(statusIndi)) {
			uploadBO.setLiaRocUploadStatus(LiaRocCst.LIAROC_UL_STATUS_RETURN);
			uploadBO.setResponseTransUUid(rqUid);
			uploadBO.setResponseTime(new Date());
			liaRocUpload2020Dao.save(uploadBO);
		} else {
			uploadBO.setLiaRocUploadStatus(statusIndi);
		}
		BeanUtils.copyProperties(uploadVO, uploadBO);
		return;
	}

	@Override
	public void updateLiaRoc902UploadStatusToQ() {
		//waiting->queue
		liaRocUpload2020Dao.update902Status(LiaRocCst.LIAROC_UL_STATUS_QUEUE);
		//若不含01,02,03,04,05,50的上傳資料，更新主檔902上傳狀態為no_item
		liaRocUpload2020Dao.update902StatusToNoItem();
		//依send_flag_902更新主檔902上傳狀態為no_item
		liaRocUpload2020Dao.updateStatusToNoItem();
	}

	@Override
	public void updateLiaRoc902UploadStatusToD() {
		//waiting->delete
		liaRocUpload2020Dao.update902Status(LiaRocCst.LIAROC_UL_STATUS_SEND_DELETE);
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<LiaRocUploadDetail2020WrapperVO> findUploadDetailData902() {
		List<LiaRocUploadDetail2020> boResult = liaRocUploadDetail2020Dao.findUploadDetailData902();
		List<LiaRocUploadDetail2020WrapperVO> voList = new ArrayList<LiaRocUploadDetail2020WrapperVO>();
		voList = (List<LiaRocUploadDetail2020WrapperVO>) BeanUtils.copyCollection(LiaRocUploadDetail2020WrapperVO.class,
						boResult);
		return voList;
	}

	@Override
	public void updateLiaRoc902UploadStatus(Map<Long, String> uploadStatus, String reqUUID, Date requestTime) {
		liaRocUpload2020Dao.updateLiaRoc902UploadStatus(uploadStatus, reqUUID, requestTime);
	}
	
	/**
	 * <p>Description : PCR_457819_電子保單<br/> 保單類型紙本<->電子保單轉換僅做保險存摺902通報</p>
	 * <p>Created By : Kathy.Yeh</p>
	 * <p>Create Time : 2022/05/11</p>
	 * @param policyId
	 */	
	@Override
	public void sendNBLiaRoc902(Long policyId) {

		PolicyVO policyVO = policyDS.retrieveById(policyId, false);

		// make 公會上傳主檔VO
		LiaRocUpload2020SendVO uploadVO = this.createLiarocUploadMaster(policyVO, LiaRocCst.LIAROC_UL_TYPE_INFORCE,
				LiaRocCst.LIAROC_UL_BIZ_SOURCE_UNB);
		
		//僅做902通報上傳
		uploadVO.setSend902Flag(LiaRocCst.LIAROC_UL_SEND_902_FLAG_3);
		uploadVO.setLiaRocUploadStatus902(LiaRocCst.LIAROC_UL_STATUS_WAITING);

		// setup 通報明細資料
		List<LiaRocUploadDetail2020VO> newDetails = new ArrayList<LiaRocUploadDetail2020VO>();
		for (CoverageVO coverage : policyVO.getCoverages()) {
			if (this.needUploadItem(coverage, LiaRocCst.LIAROC_UL_TYPE_INFORCE)) {
				LiaRocUploadDetail2020WrapperVO uploadDetailVO = generateUploadDetail(policyVO, coverage,
						LiaRocCst.LIAROC_UL_TYPE_INFORCE, false);
				if (uploadDetailVO.getLiaRocPolicyCode() != null) {
					newDetails.add(uploadDetailVO);
				}
			}
		}
		uploadVO.setDataList(newDetails);
		this.saveUpload(uploadVO);
	}

	@Override
	public Long sendAFinsUndo(UwNotintoRecvVO input) {

		Long result = 0L;

		UwNotintoRecvVO cloneInput = (UwNotintoRecvVO) BeanUtils.cloneBean(input);

		// 依照傳入的 vo 去撈公會上傳明細檔
		QueryUploadDetailByParamsReq req = new QueryUploadDetailByParamsReq();
		req.setLiarocType(LiarocType.RECEIVE);
		req.setCertiCode(cloneInput.getCeritCode());
		req.setInternalId(cloneInput.getInternalId());

		List<LiarocPolicyStatus> liarocPolicyStatus = new ArrayList<LiarocPolicyStatus>();
		liarocPolicyStatus.add(LiarocPolicyStatus._01_INFORCE);
		liarocPolicyStatus.add(LiarocPolicyStatus._15_FIX_ERROR);
		req.setLiarocPolicyStatus(liarocPolicyStatus);

		req.setApplyDate(cloneInput.getApplyDate());
		List<LiaRocUploadDetail2020> details = liaRocUploadDetail2020Dao.queryUploadDetailByParams(req);

		// 取出明細檔的所有 uploadListId
		List<String> uploadListIds = new ArrayList<String>();
		if (CollectionUtils.isNotEmpty(details)) {
			details.stream().forEach(item -> uploadListIds.add(item.getUploadListId().toString()));
		}
		
		
		// 公會上傳主檔map, key = listId (明細檔對應欄位是uploadListId)
		Map<String, LiaRocUpload2020> liaRocUpload2020Map = new HashMap<>();
		// 公會批次執行主檔map, key = upload2020ListId (明細檔對應欄位是uploadListId)
		Map<String, LiaRocNbBatch> nbBatchMap = new HashMap<>();
		
		if (CollectionUtils.isNotEmpty(uploadListIds)) {
			String[] uploadListIdArray = uploadListIds.toArray(new String[uploadListIds.size()]);

			// 找用明細檔的uploadListId去回找公會上傳主檔
			List<LiaRocUpload2020> upload2020List = liaRocUpload2020Dao.queryByListId(uploadListIdArray);
			if (CollectionUtils.isNotEmpty(upload2020List)) {
				upload2020List.stream().forEach(item -> liaRocUpload2020Map.put(item.getListId().toString(), item));
			}

			// 用上傳明細檔取出的 uploadListId 去抓公會批次執行主檔，用來判斷是否為1key件
			List<LiaRocNbBatch> nbBatchList = liaRocNbBatchDao.queryByUpload2020ListId(uploadListIdArray);
			if (CollectionUtils.isNotEmpty(nbBatchList)) {
				nbBatchList.stream().forEach(item -> nbBatchMap.put(item.getUpload2020ListId().toString(), item));
			}
		}

		// 跑邏輯整理，確認是不是真的要去 undo(1key件不用真的把取消上傳工會，只要留紀錄)
		LiaRocUploadDetail2020 newDetail = null;
		if (CollectionUtils.isNotEmpty(details)) {

			boolean is1key = false;
			LiaRocUploadDetail2020 lastDetail = null;
			for (LiaRocUploadDetail2020 detail : details) {
				
				// 公會上傳主檔
				LiaRocUpload2020 liaRocUpload2020 = liaRocUpload2020Map.get(detail.getUploadListId().toString());
				// 公會批次執行主檔
				LiaRocNbBatch nbBatch = nbBatchMap.get(detail.getUploadListId().toString());
				
				// 1key 件判斷，不管進件通路，只要有一筆是 1key 就為 true
				if (nbBatch != null && LiaRocCst.LIAROC_NB_BATCH_CHANGE_TYPE_01.equals(nbBatch.getChangeType())) {
					is1key = true;
				}

				
				boolean isAfins = LiaRocCst.LIAROC_UL_BIZ_SOURCE_AFINS.equals(liaRocUpload2020.getBizSource())
						|| LiaRocCst.LIAROC_UL_BIZ_SOURCE_AFINS_POS.equals(liaRocUpload2020.getBizSource());

				// 只取 AFINS 的件出來看，避免找到錯誤明細檔
				if (liaRocUpload2020 != null && isAfins) {
					
					if (lastDetail != null) {
						if (detail.getListId() > lastDetail.getListId()
								) {
							lastDetail = detail;
						}
					} else {
						lastDetail = detail;
					}
				}
			}

			LiaRocUploadDetail2020 detailBo = new LiaRocUploadDetail2020();
			BeanUtils.copyProperties(detailBo, lastDetail);
			// 因為是要新增資料，故清掉流水號讓它重長
			detailBo.setListId(null);
			detailBo.setLiaRocPolicyStatus(LiaRocCst.LIAROC_POLICY_STATUS__FREELOOK);
			
			// 上一筆永豐進件明細是 02 或 03 時，就也跟著不上傳
			if (LiaRocCst.CHECK_STATUS_CANCEL_BY_AFINS.equals(lastDetail.getCheckStatus())
					|| LiaRocCst.CHECK_STATUS_CANCEL_BY_SAMEKEY.equals(lastDetail.getCheckStatus())) {
				detailBo.setCheckStatus(lastDetail.getCheckStatus());
			} else {
				// 若為否時，判斷是否為1key，若是1key時仍然不上傳
				if (is1key) {
					// 已進件不取消，放入一筆不上傳的資料當作紀錄
					detailBo.setCheckStatus(LiaRocCst.CHECK_STATUS_CANCEL_BY_AFINS);
				} else {
					// 上傳06取消通報
					detailBo.setCheckStatus(LiaRocCst.CHECK_STATUS_UPLOAD);
				}
			}
			newDetail = detailBo;
		}

		// newDetails 有東西代表要落表，所以整理公會通報上傳主檔
		if (newDetail != null) {

			LiaRocUpload2020 upload2020 = liaRocUpload2020Map.get(newDetail.getUploadListId().toString());

			LiaRocUpload2020SendVO uploadVO = new LiaRocUpload2020SendVO();
			uploadVO.setBizSource(LiaRocCst.LIAROC_UL_UPLOAD_TYPE_POS.equals(cloneInput.getDataType())
					? LiaRocCst.LIAROC_UL_BIZ_SOURCE_AFINS_POS
					: LiaRocCst.LIAROC_UL_BIZ_SOURCE_AFINS);
			uploadVO.setBizId(upload2020.getBizId());
			uploadVO.setBizCode(upload2020.getBizCode());
			uploadVO.setPolicyCode(upload2020.getPolicyCode());
			uploadVO.setPolicyId(upload2020.getPolicyId());

			// 現行AfinsBatch呼叫sendAfinsNBReceive會把policyId塞回vo，並把vo回塞 table...所以比照辦理
			// 因為目前只會撈出一筆，所以這樣寫會正常，未來如果改多筆就要調整這邊
			input.setPolicyId(upload2020.getPolicyId());

			uploadVO.setLiaRocUploadType(upload2020.getLiaRocUploadType());
			uploadVO.setLiaRocUploadStatus(upload2020.getLiaRocUploadStatus());

			uploadVO.setChannelOrgId(upload2020.getChannelOrgId());
			uploadVO.setReceiveDate(upload2020.getReceiveDate());
			uploadVO.setRegisterCode(upload2020.getRegisterCode());
			uploadVO.setInputUser(AppContext.getCurrentUser().getUserId());

			List<LiaRocUploadDetail2020VO> detailVOs = new ArrayList<LiaRocUploadDetail2020VO>();
			if (upload2020.getListId().equals(newDetail.getUploadListId())) {
				LiaRocUploadDetail2020VO detailVO = new LiaRocUploadDetail2020VO();
				newDetail.copyToVO(detailVO, Boolean.TRUE);
				detailVOs.add(detailVO);
			}
			uploadVO.setDataList(detailVOs);

			result = this.saveUpload(uploadVO);
		}

		return result;
	}
}
