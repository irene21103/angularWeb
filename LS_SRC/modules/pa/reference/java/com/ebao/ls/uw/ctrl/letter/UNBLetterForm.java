package com.ebao.ls.uw.ctrl.letter;

import java.util.List;

import org.apache.struts.upload.FormFile;

import com.ebao.ls.cmu.pub.document.DocumentFormFile;
import com.ebao.pub.framework.GenericForm;

public class UNBLetterForm extends GenericForm {
    
    private static final long serialVersionUID = -3048267113513078476L;

    private long mainCategory;
    
    private long subCategory;
    
    private long templateId;
    
    private long documentId;
    
    

	private Long policyId;
    
    private String policyNo;
    
    private String applicationNo;
    
    private String documentContent;
    
    private String documentNo;
    
    private String createDate;
    
    private String actionType;

    private String forWordMode;
    
    private String retMsg;
    

	// TODO 預覽時送出去的 xml 資料，因為後端拿 pdf 的 ws 還沒 ready，先增加此欄位提供觀看送出的資訊是什麼
    // 之後整個串起來後這個欄位就可以刪掉了
    private String xmlData;
    
    private String channelId;

	private String agentId;
    
    private String channelIdText;
    
    private String agentIdText;
    
    private String channelIdDesc;

	private String agentIdDesc;
	
	private String receCetiDate;
	
	private String donePatchDate;
	
	private String cetiReason1;
	
	private String cetiReason2;
	
	private String cetiReason3;
	
	private String cetiReason4;
	
	private String cetiReason5;
	
	private String cetiReason6;
	
	private String cetiReason7;
	
	private String cetiReason8;

	public long getMainCategory() {
        return mainCategory;
    }

    public void setMainCategory(long mainCategory) {
        this.mainCategory = mainCategory;
    }

    public long getSubCategory() {
        return subCategory;
    }

    public void setSubCategory(long subCategory) {
        this.subCategory = subCategory;
    }

    public long getTemplateId() {
        return templateId;
    }

    public void setTemplateId(long templateId) {
        this.templateId = templateId;
    }

    public String getPolicyNo() {
        return policyNo;
    }

    public void setPolicyNo(String policyNo) {
        this.policyNo = policyNo;
    }

    public String getApplicationNo() {
        return applicationNo;
    }

    public void setApplicationNo(String applicationNo) {
        this.applicationNo = applicationNo;
    }

    public String getDocumentContent() {
        return documentContent;
    }

    public void setDocumentContent(String documentContent) {
        this.documentContent = documentContent;
    }

    public String getDocumentNo() {
        return documentNo;
    }

    public void setDocumentNo(String documentNo) {
        this.documentNo = documentNo;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }
    
    public String getActionType() {
        return actionType;
    }
    
    public void setActionType(String actionType) {
        this.actionType = actionType;
    }
    
	//File Config
	private FormFile[] formFiles;
	public FormFile[] getFormFiles() {
		//this.multipartRequestHandler.getFileElements();
		return formFiles;
	}
	
	

	public void setFormFiles(FormFile[] formFiles) {
		this.formFiles = formFiles;
	}

	private String uploadFileName;
	private List<DocumentFormFile> uploadFileList;
	private String[] applyImageList;
	

	
	public String[] getApplyImageList() {
		return applyImageList;
	}

	public void setApplyImageList(String[] applyImageList) {
		this.applyImageList = applyImageList;
	}

	public String getUploadFileName() {
		return uploadFileName;
	}

	public void setUploadFileName(String uploadFileName) {
		this.uploadFileName = uploadFileName;
	}

	public List<DocumentFormFile> getUploadFileList() {
		return uploadFileList;
	}

	public void setUploadFileList(List<DocumentFormFile> uploadFileList) {
		this.uploadFileList = uploadFileList;
	}



	public String getForWordMode() {
		return forWordMode;
	}

	public void setForWordMode(String forWordMode) {
		this.forWordMode = forWordMode;
	}

    public String getXmlData() {
        return xmlData;
    }

    public void setXmlData(String xmlData) {
        this.xmlData = xmlData;
    }

    public Long getPolicyId() {
        return policyId;
    }

    public void setPolicyId(Long policyId) {
        this.policyId = policyId;
    }
    public String getRetMsg() {
		return retMsg;
	}

	public void setRetMsg(String retMsg) {
		this.retMsg = retMsg;
	}
	
	public long getDocumentId() {
		return documentId;
	}

	public void setDocumentId(long documentId) {
		this.documentId = documentId;
	}
    
    public String getChannelId() {
		return channelId;
	}

	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}

	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	public String getChannelIdText() {
		return channelIdText;
	}

	public void setChannelIdText(String channelIdText) {
		this.channelIdText = channelIdText;
	}

	public String getAgentIdText() {
		return agentIdText;
	}

	public void setAgentIdText(String agentIdText) {
		this.agentIdText = agentIdText;
	}
    
    public String getChannelIdDesc() {
		return channelIdDesc;
	}

	public void setChannelIdDesc(String channelIdDesc) {
		this.channelIdDesc = channelIdDesc;
	}

	public String getAgentIdDesc() {
		return agentIdDesc;
	}

	public void setAgentIdDesc(String agentIdDesc) {
		this.agentIdDesc = agentIdDesc;
	}

	public String getReceCetiDate() {
		return receCetiDate;
	}

	public void setReceCetiDate(String receCetiDate) {
		this.receCetiDate = receCetiDate;
	}

	public String getDonePatchDate() {
		return donePatchDate;
	}

	public void setDonePatchDate(String donePatchDate) {
		this.donePatchDate = donePatchDate;
	}

	public String getCetiReason1() {
		return cetiReason1;
	}

	public void setCetiReason1(String cetiReason1) {
		this.cetiReason1 = cetiReason1;
	}

	public String getCetiReason2() {
		return cetiReason2;
	}

	public void setCetiReason2(String cetiReason2) {
		this.cetiReason2 = cetiReason2;
	}

	public String getCetiReason3() {
		return cetiReason3;
	}

	public void setCetiReason3(String cetiReason3) {
		this.cetiReason3 = cetiReason3;
	}

	public String getCetiReason4() {
		return cetiReason4;
	}

	public void setCetiReason4(String cetiReason4) {
		this.cetiReason4 = cetiReason4;
	}

	public String getCetiReason5() {
		return cetiReason5;
	}

	public void setCetiReason5(String cetiReason5) {
		this.cetiReason5 = cetiReason5;
	}

	public String getCetiReason6() {
		return cetiReason6;
	}

	public void setCetiReason6(String cetiReason6) {
		this.cetiReason6 = cetiReason6;
	}

	public String getCetiReason7() {
		return cetiReason7;
	}

	public void setCetiReason7(String cetiReason7) {
		this.cetiReason7 = cetiReason7;
	}
	
	public String getCetiReason8() {
		return cetiReason8;
	}

	public void setCetiReason8(String cetiReason8) {
		this.cetiReason8 = cetiReason8;
	}
}
