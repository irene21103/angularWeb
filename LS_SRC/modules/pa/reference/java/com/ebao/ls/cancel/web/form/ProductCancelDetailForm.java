package com.ebao.ls.cancel.web.form;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.ebao.foundation.module.web.base.GenericForm;

/**
 * 身心障礙填報-未承保原因記錄表
 *
 */
public class ProductCancelDetailForm extends GenericForm {
	private static final long serialVersionUID = 6227582275651448289L;

	long listId;

	long policyId;
	/**
	 * 來源別 UNB/POS
	 */
	String sourceType;

	long changeId;

	long policyChgId;
	/**
	 * 保單號
	 */
	String policyCode;
	/**
	 * 被保險人ID(豁免險無INSURED_ID)
	 */
	long insuredId;
	/**
	 * 被保險人身分證號
	 */
	String insuredCertiCode;
	/**
	 * 被保險人姓名
	 */
	String insuredName;
	/**
	 * 被保險人性別
	 */
	String insuredGender;
	/**
	 * 保項ID(豁免險無ITEM_ID)
	 */
	long itemId;
	/**
	 * 險種
	 */
	long productId;
	/**
	 * 險種-版本
	 */
	String productVersion;
	/**
	 * 險種-型別
	 */
	String productType;
	/**
	 * 保障年期類型
	 */
	String coveragePeriod;
	/**
	 * 保障年期
	 */
	String coverageYear;
	/**
	 * 身心障礙類別(當前被保險人狀態)
	 */
	String disabilityCategory;
	/**
	 * 身心障礙等級(當前被保險人狀態)
	 */
	String disabilityClass;
	/**
	 * 未承保原因類型
	 */
	String cancelReasonType1;
	/**
	 * 體況類型
	 */
	String cancelReasonType2;
	/**
	 * 未承保原因項目(單選對應T_DISABILITY_CANCEL_REASON, 多選對應T_DISABILITY_MAPPING)
	 */
	List<String> cancelReasonOption;
	/**
	 * 未承保原因項目(單選對應T_DISABILITY_CANCEL_REASON, 多選對應T_DISABILITY_MAPPING)
	 */
	String cancelReason;
	/**
	 * 主約幣別
	 */
	String moneyId;
	/**
	 * 保項生效日
	 */
	Date validateDate;
	/**
	 * 要保書狀態(寫入資料當下的狀態)
	 */
	String proposalStatus;
	/**
	 * 核保決定(寫入資料當下的狀態)
	 */
	String policyDecision;
	/**
	 * 核保決定日(寫入資料當下的狀態)
	 */
	String underwriteTime;
	/**
	 * 通路
	 */
	String channelType;
	/**
	 * 經代分類
	 */
	String agencyType;
	/**
	 * 業務員代號(服務業務員)
	 */
	String agentRegisterCode;
	/**
	 * 新制身心障礙類別:A01~A08為單一障礙類，若為多選，需依填報規則轉換為多重障礙代碼M03~MFF供填報。
	 */
	String disabilityCategoryNewRule;
	/**
	 * 被保險人投保年齡
	 */
	String insuredAge;

	List<Map<String, Object>> reason1List;

	List<Map<String, Object>> reason2List;

	List<Map<String, Object>> reason3List;

	List<Map<String, Object>> reason4List;
	
	List<Map<String, Object>> reason5List;
	
	//被保險人
	List<Map<String, Object>> masterInsured;
	
	String insuredUser;
	
	String items ;

	public long getListId() {
		return listId;
	}

	public void setListId(long listId) {
		this.listId = listId;
	}

	public long getPolicyId() {
		return policyId;
	}

	public void setPolicyId(long policyId) {
		this.policyId = policyId;
	}

	public String getSourceType() {
		return sourceType;
	}

	public void setSourceType(String sourceType) {
		this.sourceType = sourceType;
	}

	public long getChangeId() {
		return changeId;
	}

	public void setChangeId(long changeId) {
		this.changeId = changeId;
	}

	public long getPolicyChgId() {
		return policyChgId;
	}

	public void setPolicyChgId(long policyChgId) {
		this.policyChgId = policyChgId;
	}

	public String getPolicyCode() {
		return policyCode;
	}

	public void setPolicyCode(String policyCode) {
		this.policyCode = policyCode;
	}

	public long getInsuredId() {
		return insuredId;
	}

	public void setInsuredId(long insuredId) {
		this.insuredId = insuredId;
	}

	public String getInsuredCertiCode() {
		return insuredCertiCode;
	}

	public void setInsuredCertiCode(String insuredCertiCode) {
		this.insuredCertiCode = insuredCertiCode;
	}

	public String getInsuredName() {
		return insuredName;
	}

	public void setInsuredName(String insuredName) {
		this.insuredName = insuredName;
	}

	public String getInsuredGender() {
		return insuredGender;
	}

	public void setInsuredGender(String insuredGender) {
		this.insuredGender = insuredGender;
	}

	public long getItemId() {
		return itemId;
	}

	public void setItemId(long itemId) {
		this.itemId = itemId;
	}

	public long getProductId() {
		return productId;
	}

	public void setProductId(long productId) {
		this.productId = productId;
	}

	public String getProductVersion() {
		return productVersion;
	}

	public void setProductVersion(String productVersion) {
		this.productVersion = productVersion;
	}

	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	public String getCoveragePeriod() {
		return coveragePeriod;
	}

	public void setCoveragePeriod(String coveragePeriod) {
		this.coveragePeriod = coveragePeriod;
	}

	public String getCoverageYear() {
		return coverageYear;
	}

	public void setCoverageYear(String coverageYear) {
		this.coverageYear = coverageYear;
	}

	public String getDisabilityCategory() {
		return disabilityCategory;
	}

	public void setDisabilityCategory(String disabilityCategory) {
		this.disabilityCategory = disabilityCategory;
	}

	public String getDisabilityClass() {
		return disabilityClass;
	}

	public void setDisabilityClass(String disabilityClass) {
		this.disabilityClass = disabilityClass;
	}

	public String getCancelReasonType1() {
		return cancelReasonType1;
	}

	public void setCancelReasonType1(String cancelReasonType1) {
		this.cancelReasonType1 = cancelReasonType1;
	}

	public List<String> getCancelReasonOption() {
		return cancelReasonOption;
	}

	public void setCancelReasonOption(List<String> cancelReasonOption) {
		this.cancelReasonOption = cancelReasonOption;
	}

	public String getCancelReason() {
		return cancelReason;
	}

	public void setCancelReason(String cancelReason) {
		this.cancelReason = cancelReason;
	}

	public String getMoneyId() {
		return moneyId;
	}

	public void setMoneyId(String moneyId) {
		this.moneyId = moneyId;
	}

	public Date getValidateDate() {
		return validateDate;
	}

	public void setValidateDate(Date validateDate) {
		this.validateDate = validateDate;
	}

	public String getProposalStatus() {
		return proposalStatus;
	}

	public void setProposalStatus(String proposalStatus) {
		this.proposalStatus = proposalStatus;
	}

	public String getPolicyDecision() {
		return policyDecision;
	}

	public void setPolicyDecision(String policyDecision) {
		this.policyDecision = policyDecision;
	}

	public String getUnderwriteTime() {
		return underwriteTime;
	}

	public void setUnderwriteTime(String underwriteTime) {
		this.underwriteTime = underwriteTime;
	}

	public String getChannelType() {
		return channelType;
	}

	public void setChannelType(String channelType) {
		this.channelType = channelType;
	}

	public String getAgencyType() {
		return agencyType;
	}

	public void setAgencyType(String agencyType) {
		this.agencyType = agencyType;
	}

	public String getAgentRegisterCode() {
		return agentRegisterCode;
	}

	public void setAgentRegisterCode(String agentRegisterCode) {
		this.agentRegisterCode = agentRegisterCode;
	}

	public String getCancelReasonType2() {
		return cancelReasonType2;
	}

	public void setCancelReasonType2(String cancelReasonType2) {
		this.cancelReasonType2 = cancelReasonType2;
	}

	public String getDisabilityCategoryNewRule() {
		return disabilityCategoryNewRule;
	}

	public void setDisabilityCategoryNewRule(String disabilityCategoryNewRule) {
		this.disabilityCategoryNewRule = disabilityCategoryNewRule;
	}

	public String getInsuredAge() {
		return insuredAge;
	}

	public void setInsuredAge(String insuredAge) {
		this.insuredAge = insuredAge;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public List<Map<String, Object>> getReason1List() {
		return reason1List;
	}

	public void setReason1List(List<Map<String, Object>> reason1List) {
		this.reason1List = reason1List;
	}

	public List<Map<String, Object>> getReason2List() {
		return reason2List;
	}

	public void setReason2List(List<Map<String, Object>> reason2List) {
		this.reason2List = reason2List;
	}

	public List<Map<String, Object>> getReason3List() {
		return reason3List;
	}

	public void setReason3List(List<Map<String, Object>> reason3List) {
		this.reason3List = reason3List;
	}

	public List<Map<String, Object>> getReason4List() {
		return reason4List;
	}

	public void setReason4List(List<Map<String, Object>> reason4List) {
		this.reason4List = reason4List;
	}

	public List<Map<String, Object>> getMasterInsured() {
		return masterInsured;
	}

	public void setMasterInsured(List<Map<String, Object>> masterInsured) {
		this.masterInsured = masterInsured;
	}

	public String getItems() {
		return items;
	}

	public void setItems(String items) {
		this.items = items;
	}

	public String getInsuredUser() {
		return insuredUser;
	}

	public void setInsuredUser(String insuredUser) {
		this.insuredUser = insuredUser;
	}

	public List<Map<String, Object>> getReason5List() {
		return reason5List;
	}

	public void setReason5List(List<Map<String, Object>> reason5List) {
		this.reason5List = reason5List;
	}
}
