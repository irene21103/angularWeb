package com.ebao.ls.uw.ds;

import java.io.File;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

import com.ebao.foundation.module.util.i18n.CodeTable;
import com.ebao.ls.chl.service.AgentService;
import com.ebao.ls.chl.service.ChannelOrgService;
import com.ebao.ls.chl.vo.ChannelOrgVO;
import com.ebao.ls.cs.ci.PolicyQueryCompoundCI;
import com.ebao.ls.cs.common.format.AmountUnitBenefitLevelFormatter;
import com.ebao.ls.cs.commonflow.ds.cmnquery.CoverageDataVO;
import com.ebao.ls.cs.commonflow.ds.cmnquery.PolicyQueryService;
import com.ebao.ls.cs.logger.ApplicationLogger;
import com.ebao.ls.cs.logger.helper.AppLogDataHelper;
import com.ebao.ls.escape.helper.EscapeHelper;
import com.ebao.ls.foundation.service.GenericServiceImpl;
import com.ebao.ls.pa.nb.bs.rule.PolicyServiceHelper;
import com.ebao.ls.pa.nb.bs.rule.ProposalRuleMsgService;
import com.ebao.ls.pa.nb.bs.rule.ProposalRuleResultService;
import com.ebao.ls.pa.nb.ci.UnbProcessCI;
import com.ebao.ls.pa.nb.ctrl.letter.NbNotificationHelper;
import com.ebao.ls.pa.nb.util.NBUtils;
import com.ebao.ls.pa.nb.util.UnbCst;
import com.ebao.ls.pa.nb.util.UnbRoleType;
import com.ebao.ls.pa.nb.vo.ElderAudioExtInfoVO;
import com.ebao.ls.pa.nb.vo.ProposalRuleMsgVO;
import com.ebao.ls.pa.nb.vo.ProposalRuleResultVO;
import com.ebao.ls.pa.pub.bs.AgentNotifyPremSourceService;
import com.ebao.ls.pa.pub.bs.AgentNotifyPurposeService;
import com.ebao.ls.pa.pub.bs.AgentNotifyService;
import com.ebao.ls.pa.pub.bs.FinanceNotifyPremSourceService;
import com.ebao.ls.pa.pub.bs.FinanceNotifyService;
import com.ebao.ls.pa.pub.data.bo.Coverage;
import com.ebao.ls.pa.pub.data.bo.Policy;
import com.ebao.ls.pa.pub.data.org.PolicyDao;
import com.ebao.ls.pa.pub.model.CoverageAgentInfo;
import com.ebao.ls.pa.pub.model.CoverageInfo;
import com.ebao.ls.pa.pub.model.InsuredInfo;
import com.ebao.ls.pa.pub.model.PolicyInfo;
import com.ebao.ls.pa.pub.service.PolicyService;
import com.ebao.ls.pa.pub.vo.AgentNotifyPremSourceVO;
import com.ebao.ls.pa.pub.vo.AgentNotifyPurposeVO;
import com.ebao.ls.pa.pub.vo.AgentNotifyVO;
import com.ebao.ls.pa.pub.vo.FinanceNotifyPremSrcVO;
import com.ebao.ls.pa.pub.vo.FinanceNotifyVO;
import com.ebao.ls.pa.pub.vo.InsuredVO;
import com.ebao.ls.pa.pub.vo.PolicyVO;
import com.ebao.ls.pa.pub.vo.VOGenerator;
import com.ebao.ls.pa.rule.RuleRateTableCST;
import com.ebao.ls.product.service.LifeProductService;
import com.ebao.ls.pub.CodeCst;
import com.ebao.ls.pub.cst.ProposalRuleMsg;
import com.ebao.ls.pub.cst.ProposalRuleStatus;
import com.ebao.ls.pub.cst.ProposalRuleType;
import com.ebao.ls.rstf.ci.ctrs.CrmElderAudioTaskRstfCI;
import com.ebao.ls.rstf.ci.ctrs.vo.CrmElderAudioTaskInVO;
import com.ebao.ls.rstf.ci.ctrs.vo.CrmElderAudioTaskQuestionnaireAgentVO;
import com.ebao.ls.rstf.ci.ctrs.vo.CrmElderAudioTaskQuestionnaireVO;
import com.ebao.ls.rstf.ci.ctrs.vo.CrmReturnOutVO;
import com.ebao.ls.uw.ctrl.helper.ElderAudioHelper;
import com.ebao.ls.uw.data.UwElderAudioDao;
import com.ebao.ls.uw.data.UwElderAudioDtlDao;
import com.ebao.ls.uw.data.bo.UwElderAudio;
import com.ebao.ls.uw.data.bo.UwElderAudioDtl;
import com.ebao.ls.uw.vo.UwElderAudioDtlVO;
import com.ebao.ls.uw.vo.UwElderAudioVO;
import com.ebao.ls.ws.vo.unb.unb391.ElderAudioReplyResultVO;
import com.ebao.ls.ws.vo.unb.unb391.ElderAudioReplyVO;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.framework.GenericException;
import com.ebao.pub.framework.RTException;
import com.ebao.pub.i18n.lang.StringResource;
import com.ebao.pub.util.DBean;
import com.ebao.pub.util.DateUtils;
import com.ebao.pub.util.EnvUtils;

public class UwElderAudioServiceImpl extends GenericServiceImpl<UwElderAudioVO, UwElderAudio, UwElderAudioDao>
		implements UwElderAudioService {
	protected Logger logger = Logger.getLogger(this.getClass());
	private static String CODE_SUCCES = "0";
	private static String CODE_FAIL = "-1";
	private static String SENDSTATUS_FAIL = "F";
	private static String SENDSTATUS_REPEAT = "R";
	private static String SENDSTATUS_SUCCESS = "S";
	private static String RELATIVE_CRM = "elder_audio";
	private static String CRM_SHARE_ROOT;

	@PostConstruct
	private void init() {
		CRM_SHARE_ROOT=EnvUtils.getSendToCRMSharePath();//FT_CRM_PATH
		if (null == CRM_SHARE_ROOT || CRM_SHARE_ROOT.contains("null")) {
			logger.error( new NullPointerException("FT_CRM_PATH cannot be null, please check out env.properties").getMessage());
		}else {
			logger.info("CRM_SHARE_ROOT " + CRM_SHARE_ROOT);
		}
	}

	@Override
	protected UwElderAudioVO newEntityVO() {
		return new UwElderAudioVO();
	}

	@Override
	public UwElderAudioVO load(Serializable id) throws GenericException {
		UwElderAudioVO masterObjVo = super.load(id);
		if (null != masterObjVo) {
			masterObjVo.setUwElderAudioDtls(uwElderAudioDtlDao.findByTaskNum(masterObjVo.getTaskNum(), false));
		}
		return masterObjVo;
	}

	@Override
	public List<ElderAudioReplyResultVO> processRepliedTaskFromCRM(List<ElderAudioReplyVO> elderAudioReplyList, Integer totalNumber) {

		ApplicationLogger.setSystemCode(AppLogDataHelper.SYSTEM_CODE_UNB);
		ApplicationLogger.setOptionId(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMdd HHmmss")));
		ApplicationLogger.setJobName("processCRM");
		ApplicationLogger
				.addLoggerData("[INFO]process_CRM_begin (env:" + com.ebao.pub.util.EnvUtils.getEnvLabel() + ")");

		List<ElderAudioReplyResultVO> rsult = new ArrayList<>();
		for (ElderAudioReplyVO earVi : elderAudioReplyList) {
			// MSG1000 格式檢查錯誤
			String msg1000 = validate(earVi);
			if (!StringUtils.isBlank(msg1000)) {
				ApplicationLogger.addLoggerData("[WARN]" + earVi.getTaskNum() + "_1000_" + msg1000);
				rsult.add(genReplyResult(earVi.getTaskNum(), "1000", msg1000));
				continue;
			}
			// 若有此任務則傳回policyId，否則為-1
			long policyId = dao.getPolicyIdByTaskNum(earVi.getTaskNum());
			if (policyId < 0) {
				ApplicationLogger.addLoggerData("[WARN]" + earVi.getTaskNum() + "_2000_NoThisTaskNum");
				rsult.add(genReplyResult(earVi.getTaskNum(), "2000", NBUtils.getTWMsg("MSG_1267154")));//無此高齡銷售錄音審核任務
				continue;
			}
			PolicyInfo policyInfo = policyService.load(policyId);
			if (null == policyInfo) {
				rsult.add(genReplyResult(earVi.getTaskNum(), "2000", NBUtils.getTWMsg("ERR_20413020010")));//不存在的保單號碼
				continue;
			}
			Integer pStatus = policyInfo.getProposalStatus();
			if ((null != policyInfo.getIssueDate() && CodeCst.PROPOSAL_STATUS__IN_FORCE == pStatus)
					|| (CodeCst.PROPOSAL_STATUS__DECLINED == pStatus
					|| CodeCst.PROPOSAL_STATUS__POSTPONED == pStatus
					|| CodeCst.PROPOSAL_STATUS__WITHDRAWN == pStatus)) {
				// MSG2000 保單狀態=生效或未生效(要保書狀態=延期/拒保/撤件)，不可更新高齡銷售錄音覆審記錄
				ApplicationLogger
				.addLoggerData("[WARN]" + earVi.getTaskNum() + "_2000_NoChange_pStatus:" + pStatus);
				rsult.add(genReplyResult(earVi.getTaskNum(), "2000", NBUtils.getTWMsg("MSG_1267155")));//保單狀態為不可更新高齡銷售錄音覆審記錄
				continue;
			}
			// 是否已有同高齡銷售錄音審核任務記錄 有代表示是要更新
			UwElderAudioDtlVO dtlLast1 = uwElderAudioDtlDao.findFirstFitByTaskNum(earVi.getTaskNum(), true);
			boolean noAnyDtl = (null == dtlLast1 || null == dtlLast1.getTaskNum());
			// 接收並留存高齡銷售錄音審核任務記錄
			UwElderAudioDtlVO dtl = new UwElderAudioDtlVO();
			dtl.setTaskNum(earVi.getTaskNum());
			dtl.setAudioObjName(getFixedLengthStr(earVi.getAudioObjName(), 200));// (被)錄音者姓名
			dtl.setAudioDate(earVi.getAudioDate());
			dtl.setReviewResult(earVi.getReviewResult());
			dtl.setIssueComment(getFixedLengthStr(earVi.getIssueComment(), 500));// 異常內容說明
			dtl.setRealAudioNo(getFixedLengthStr(earVi.getRealAudioNo(), 200));// 實際錄音檔流水編號
			dtl.setReviewerName(getFixedLengthStr(earVi.getReviewerName(), 100));// 覆審人員姓名
			dtl.setReviewTime(earVi.getReviewTime());
			boolean isSuccess = writeElderAudioDtl(dtl);
			if (!isSuccess) {
				ApplicationLogger.addLoggerData("[ERR]" + earVi.getTaskNum() + "_saveUwElderAudioDtl("
						+ policyId + ")_pStatus:" + pStatus);
				// MSG40000
				rsult.add(genReplyResult(earVi.getTaskNum(), "4000", NBUtils.getTWMsg("MSG_1267156")));//處理發生儲存錯誤
				continue;
			}

			// 核保任務「高齡錄音回覆」=已回覆
			// 20220916補充規則，同一任務要第二筆才允許以下程序
			//   要保書狀態=待核保、核保中，產生核保中異動訊息
			//   要保書狀態=待承保
			//   執行UNDO（生效前UNDO）
			//   EMAIL通知核保員，若核保員無EMAIL或不在職，EMAIL改寄送核保主管之EMAIL
			try {
				unbProcessCI.checkElderAudioReply(policyId, noAnyDtl);
			} catch (Exception e) {
				String stacktrace = org.apache.commons.lang3.exception.ExceptionUtils.getStackTrace(e);
				NBUtils.logger(this.getClass(), "[ERR]" + earVi.getTaskNum() + "_checkElderAudioReply(" + policyId + ":"
						+ pStatus + ")_message:" + e.getMessage() + "\r\n" + stacktrace);
			}
			// 修正主檔統號姓名角色
			if(StringUtils.isNotBlank(earVi.getRoleCertiCode())
					||StringUtils.isNotBlank(earVi.getRoleType())
					||StringUtils.isNotBlank(earVi.getRoleName())) {
				UwElderAudioVO earTaskVO = dao.findByTaskNum(earVi.getTaskNum());
				StringBuffer sb=new StringBuffer();
				if(StringUtils.isNotBlank(earVi.getRoleCertiCode())) {
					earTaskVO.setCertiCode(earVi.getRoleCertiCode());
					sb.append(" CertiCode[" + earVi.getRoleCertiCode() + ":" + earTaskVO.getCertiCode() + "]");
				}
				if(StringUtils.isNotBlank(earVi.getRoleType())) {
					//要有此角色才可存
					UnbRoleType unrt = UnbRoleType.getByRoleTypeENG(earVi.getRoleType());
					if(null!=unrt) {
						earTaskVO.setRoleType(unrt.getRoleType());
						sb.append(" RoleType[" + earVi.getRoleType() + ":" + earTaskVO.getRoleType() + "]");
					}
				}
				if(StringUtils.isNotBlank(earVi.getRoleName())) {
					earTaskVO.setName(earVi.getRoleName());
					sb.append(" Name[" + earVi.getRoleName() + ":" + earTaskVO.getName() + "]");
				}
				save(earTaskVO);
				NBUtils.logger(this.getClass(),
						"[INFO]_updateElderAudio(TaskNum:" + earVi.getTaskNum() + ")_column[before:change]," + sb.toString());
			}
			// MSG0000
			rsult.add(genReplyResult(earVi.getTaskNum(), "0000", NBUtils.getTWMsg("MSG_1267001")));//處理完成
		}
		try {
			ApplicationLogger.flush();
		} catch (Exception e1) {
			logger.info("ApplicationLogger.flush() : " + e1.getMessage(), e1);
		}
		return rsult;
	}

	/**
	 * 擷取固定長(去尾)
	 * @param src
	 * @param limit
	 * @return substring(0, limit)
	 */
	private String getFixedLengthStr(String src, int limit) {
		if (null == src || src.isEmpty())
			return src;
		if (src.length() > limit) {
			return src.substring(0, limit);
		} else
			return src;
	}

	/**
	 * 產生處理結果
	 * 
	 * @param taskNum
	 * @param resultCode
	 * @param resultDetail
	 * @return
	 */
	private ElderAudioReplyResultVO genReplyResult(String taskNum, String resultCode, String resultDetail) {
		ElderAudioReplyResultVO esultVi = new ElderAudioReplyResultVO();
		esultVi.setTaskNum(taskNum);
		esultVi.setUpdateResult(resultCode);
		esultVi.setUpdateResultDetail(resultDetail);
		return esultVi;
	}

	/**
	 * MSG1000 格式檢查<BR>
	 * 目前只檢查reviewResult、
	 * 
	 * @param reply
	 * @return 格式檢查錯誤的中文
	 */
	private String validate(ElderAudioReplyVO reply) {
		StringBuffer memo = new StringBuffer();
		// reviewResult Y,N
		if (reply.getReviewResult().equals(CodeCst.YES_NO__NO) && StringUtils.isBlank(reply.getIssueComment())) {
			memo.append(NBUtils.getTWMsg("MSG_1267152"));//覆審結果為異常時需填註
		} else if (!reply.getReviewResult().matches("[YN]")) {
			memo.append(NBUtils.getTWMsg("MSG_1267153"));//覆審結果只允許為Y或N
		}
		return memo.toString();
	}

	/**
	 * 取得新產生的任務編號<BR>
	 * 透過taskNum的值進行
	 * 
	 * @param taskNum
	 * @param policyCode
	 * @param roleTypeENG 保單角色
	 * @return taskNum
	 */
	private String genTaskNum(String taskNum, String policyCode, UnbRoleType roleType) {
		if (StringUtils.isNotBlank(policyCode) && null != roleType) {// 要有POLICY_CODE
			// 保單角色英文碼3碼不足前補0
			String r3 = ("000" + roleType.getRoleTypeENG()).substring(("000" + roleType.getRoleTypeENG()).length() - 3);
			// 案號+保單角色英文碼3碼+流水號3碼
			if (StringUtils.isBlank(taskNum)) {// 新案
				taskNum = policyCode + r3 + "001";
			} else {// 流水號3碼+1
				String s3 = taskNum.substring(taskNum.length() - 3);
				int num = Integer.parseInt(s3) + 1;
				taskNum = policyCode + r3 + String.format("%03d", num);
			}
			return taskNum;
		}
		return null;
	}

	@Override
	public List<UwElderAudioVO> findAllTaskWithLastDtlByPolicyId(Long policyId) {
		List<UwElderAudioVO> uwEAVOs = dao.findByPolicyId(policyId);
		if (null == uwEAVOs || uwEAVOs.isEmpty())
			return null;
		else {
			uwEAVOs.forEach(uwEAVOi -> {
				UwElderAudioDtlVO dtlVO = uwElderAudioDtlDao.findFirstFitByTaskNum(uwEAVOi.getTaskNum(), true);
				if (null != dtlVO) {
					List<UwElderAudioDtlVO> dtls = new ArrayList<>(Arrays.asList(dtlVO));
					uwEAVOi.setUwElderAudioDtls(dtls);
				}
			});
			return uwEAVOs;
		}
	}

	@Override
	public UwElderAudioVO findTaskWithLastDtlByTaskNum(String taskNum) {
		UwElderAudioVO uwEAVO = dao.findByTaskNum(taskNum);
		if (null == uwEAVO)
			return null;
		else {
			UwElderAudioDtlVO dtlVO = uwElderAudioDtlDao.findFirstFitByTaskNum(taskNum, true);
			if (null != dtlVO) {
				List<UwElderAudioDtlVO> dtls = new ArrayList<>(Arrays.asList(dtlVO));
				uwEAVO.setUwElderAudioDtls(dtls);
			}
			return uwEAVO;
		}
	}

	@Override
	public boolean writeElderAudioTask(UwElderAudioVO uwElderAudioVO) {
		if (null == uwElderAudioVO)
			return false;
		try {
			uwElderAudioVO.setListId(null);// auto generate
			save(uwElderAudioVO);
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	@Override
	public List<Boolean> writeElderAudioTask(List<UwElderAudioVO> uwElderAudioVOList) {
		if (null == uwElderAudioVOList || uwElderAudioVOList.isEmpty())
			return new ArrayList<>(Arrays.asList(false));
		List<Boolean> rsultVo = new ArrayList<>();
		uwElderAudioVOList.forEach(ri -> {
			rsultVo.add(writeElderAudioTask(ri));
		});
		return rsultVo;
	}

	@Override
	public List<String> getExistElderAudioTasks(Long policyId) {
		return uwElderAudioDtlDao.getExistElderAudioTasks(policyId);
	}

	@Override
	public boolean writeElderAudioDtl(UwElderAudioDtlVO uwElderAudioDtlVO) {
		if (null == uwElderAudioDtlVO)
			return false;
		uwElderAudioDtlVO.setListId(null);// auto generate
		UwElderAudioDtl new_ead = new UwElderAudioDtl();
		new_ead.copyFromVO(uwElderAudioDtlVO, false, false);
		try {
			uwElderAudioDtlDao.save(new_ead);
		} catch (Exception e) {
			NBUtils.logger(this.getClass(), "[ERR]" + uwElderAudioDtlVO.getTaskNum() 
				+ "_writeElderAudioDtl_ExcepMsg:" + e.getMessage());
			return false;
		}
		return true;
	}

	@Override
	public List<Boolean> writeElderAudioDtl(List<UwElderAudioDtlVO> uwElderAudioDtlVOList) {
		if (null == uwElderAudioDtlVOList || uwElderAudioDtlVOList.isEmpty())
			return new ArrayList<>(Arrays.asList(false));
		List<Boolean> rsultVo = new ArrayList<>();
		uwElderAudioDtlVOList.forEach(ri -> {
			rsultVo.add(writeElderAudioDtl(ri));
		});
		return rsultVo;
	}

	@Override
	public String sendReviewTaskToCRM(List<UwElderAudioVO> tasks, ElderAudioExtInfoVO extInfo, boolean isAddOPQWhileCRMFail) {
		if (CollectionUtils.isEmpty(tasks)) {
			// 無高齡銷售錄音覆審任務
			return "";
		}
		List<String> output = new ArrayList<>();
		String lastTaskNum = dao.findMaxTaskNumByPolicyId(tasks.get(0).getPolicyId());
		Map<String, CrmReturnOutVO> crmRsult = new HashMap<>();
		String[] subPath = { extInfo.getNoticeYYYYMMDD().substring(0, 4), extInfo.getNoticeYYYYMMDD().substring(4, 6),
				extInfo.getNoticeYYYYMMDD().substring(6), extInfo.getPolicyCode() };
		// 檔案放置位置為 elder_audio/照會日期西元年YYYY/照會日期月MM/照會日期日DD/policyCode/certiCode_Num3碼/
		String policyPath = elderAudioHelper.genPath(RELATIVE_CRM + File.separator, subPath);
		
		// 20230420 PCR-517410, IrisLiu, CRM高齡銷售錄音審核任務新增傳入資訊(因資訊大多相同，故先處理各角色共用資訊，再依角色調整)
		CrmElderAudioTaskQuestionnaireVO crmElderAudioTaskQuestionnaireVO = new CrmElderAudioTaskQuestionnaireVO();
		try {
			crmElderAudioTaskQuestionnaireVO = setCrmElderAudioTaskQuestionnaireVO(tasks.get(0).getPolicyId());
		} catch (Exception e) {
			NBUtils.logger(this.getClass(), "[ERR]_sendReviewTaskToCRM_CrmElderAudioTaskQuestionnaireVO_ExcepMsg:" + e.getMessage());
			//MSG_1267489  產生提供給CRM的高齡銷售錄音審核資訊失敗，請洽資訊人員。
			return StringResource.getStringData("MSG_1267489", AppContext.getCurrentUser().getLangId());
		}
		
		for (UwElderAudioVO ti : tasks) {
			String tempPath = elderAudioHelper.genPath(policyPath, new String[] { ti.getTaskNum() });// certiCode_Num3碼
			String uploadPath = elderAudioHelper.genPath(tempPath, new String[] { ElderAudioHelper.UPLOAD_FOLDER });
			boolean doTask = true;
			UnbRoleType ut = UnbRoleType.getByRoleType(ti.getRoleType());
			ti.setTaskNum(genTaskNum(lastTaskNum, extInfo.getPolicyCode(), ut));
			CrmElderAudioTaskInVO taskVO = new CrmElderAudioTaskInVO();
			taskVO.setTaskNum(ti.getTaskNum());// 任務編號
			taskVO.setProcessingDate(DateUtils.date2String(ti.getConsultDate(), "yyyyMMddHHmmss") + "000");// 會辦日期時間YYYYMMDDHHmmss000
			taskVO.setPolicyRoleStringList(StringUtils.defaultString(ti.getCertiCode()),
							StringUtils.defaultString(ti.getName()), ut.getRoleTypeENG(), ut.getRoleName());// 角色清單{CertiCode、姓名、角色-代號、角色-中文}
			taskVO.setAudioDataStringList(ti.getAudioNoList());// 錄音檔清單
			taskVO.setPolicyNo(extInfo.getPolicyCode());// 保單號碼
			taskVO.setMasterProductCode(extInfo.getMasterProductCode());// 主約險種代碼
			taskVO.setMasterProductName(extInfo.getMasterProductName());// 主約險種名稱
			taskVO.setOwnerId(extInfo.getOwnerId());// 要保人-ID
			taskVO.setOwnerName(extInfo.getOwnerName());// 要保人-姓名
			
			// 20230420 PCR-517410, IrisLiu, 依所屬保單角色調整CRM高齡銷售錄音審核任務資訊
			updateCrmElderAudioTaskQuestionnaireVO(tasks.get(0).getPolicyId(), ut.getRoleType(), crmElderAudioTaskQuestionnaireVO);
			taskVO.setQuestionnaireData(crmElderAudioTaskQuestionnaireVO);

			List<String> fn = new ArrayList<>();//fileNameList 檔案名稱
			List<String> fs = new ArrayList<>();//fileSourceList 0:eBao影像 1:上傳補充檔案
			List<String> fp = new ArrayList<>();//filePathList 檔案交換區位置(相對位置)
			//image
			File images = new File(CRM_SHARE_ROOT + File.separator + tempPath);
			String[] imgFileNames= images.list();
			if(null != imgFileNames && imgFileNames.length>0) {
				List<String> fnOrder = Arrays.asList(imgFileNames);
				Collections.sort(fnOrder);
				String tempPathStr = tempPath.replaceAll("\\\\", "/").replaceAll("//", "/");
				for(String imgFN: fnOrder) {
					if(imgFN.contains(".tiff")) {
						fn.add(imgFN);
						fs.add("0");
						fp.add(tempPathStr);
					}
				}
			}
			
			//user-upload
			File uploads = new File(CRM_SHARE_ROOT + File.separator + uploadPath);
			String[] uploadFileNames= uploads.list();
			if(null != uploadFileNames && uploadFileNames.length>0) {
				List<String> fnOrder = Arrays.asList(uploadFileNames);
				Collections.sort(fnOrder);
				String uploadPathStr = uploadPath.replaceAll("\\\\", "/").replaceAll("//", "/");
				for(String upldFN: fnOrder) {
					fn.add(upldFN);
					fs.add("1");
					fp.add(uploadPathStr);
				}
			}
			if(fn.isEmpty()) {
				doTask = false;
				//無高齡銷售錄音覆審任務指定的檔案
				NBUtils.logger(this.getClass(), "[ERR]無指定的檔案:" + CRM_SHARE_ROOT + File.separator + tempPath + File.separator );
			}
			
			if (doTask) {
				taskVO.setFileDataStringList(fn, fs, fp);// 影像或檔案清單
				// call CRM restful-api
				try {
					CrmReturnOutVO crmReturnOutVO = callCrmRastApi(taskVO, 3);
					crmRsult.put(ti.getTaskNum(), crmReturnOutVO);
					if (CODE_SUCCES.equals(crmReturnOutVO.getCode())
							&& SENDSTATUS_SUCCESS.equals(crmReturnOutVO.getSendStatus())) {
						// 送CRM處理成功，同步taskNum
						lastTaskNum = crmReturnOutVO.getMsg();
						ti.setTaskNum(crmReturnOutVO.getMsg());
						
						//寫入高齡錄音任務主檔(T_UW_ELDER_AUDIO)
						if (!writeElderAudioTask(ti)) {
							NBUtils.logger(this.getClass(), "[ERR]writeElderAudioTask Fail:" + ti.toString());
						}
						unbProcessCI.genElderAudioCheckListAndUpdateUwTask(ti);
					} else {
						// CODE_FAIL,CODE_SUCCES&&SENDSTATUS_FAIL
						// BA決定CRM處理失敗時回字串為 失敗資料的姓名+”-“+音流水編號, 若有多筆”/”隔開
						output.add(StringUtils.defaultString(ti.getName(),"無姓名") + "-" + ti.getAudioNoList());
						if (isAddOPQWhileCRMFail) {
							// BSD-OPQ:與CRM系統連線失敗，「CRM系統連線失敗，請確認高齡銷售錄音覆審會辦作業。」
							this.writeRuleResultForCRM(tasks.get(0).getPolicyId());
						}
					}
				} catch (Exception e) {
					NBUtils.logger(this.getClass(),
							"[ERR]" + ti.getTaskNum() + "_sendReviewTaskToCRM_ExcepMsg:" + e.getMessage());
				}
			}
		}
		return String.join("/", output);// BA決定CRM處理失敗時回字串 若有多筆”/”隔開
	}

	/** 處理任務編號重複時要再呼叫的需求，傳送成功時於msg填入curTaskNum */
	private CrmReturnOutVO callCrmRastApi(CrmElderAudioTaskInVO taskVO, int repeatTimes) {
		CrmReturnOutVO crmReturnOutVO = new CrmReturnOutVO();
		String curTaskNum = taskVO.getTaskNum();
		try {
			for (int i = 0; i < repeatTimes; i++) {
				crmReturnOutVO = crmElderAudioTaskRstfCI.sendTask(taskVO);
				if (SENDSTATUS_REPEAT.equals(crmReturnOutVO.getSendStatus())) {
					//R 要重送 要改任務編號(案號+保單角色英文碼3碼不足前補0+流水號3碼)
					String imu = curTaskNum.substring(0, curTaskNum.length() - 3);
					// 流水號3碼+1
					String s3 = curTaskNum.substring(curTaskNum.length() - 3);
					int num = Integer.parseInt(s3) + 1;
					curTaskNum = imu + String.format("%03d", num);
					taskVO.setTaskNum(curTaskNum);
				} else {
					if (CODE_SUCCES.equals(crmReturnOutVO.getCode())
							&& SENDSTATUS_SUCCESS.equals(crmReturnOutVO.getSendStatus())) {
						crmReturnOutVO.setMsg(curTaskNum);//成功時msg填入curTaskNum
					}
					//CODE_FAIL,SENDSTATUS_FAIL: crmReturnOutVO內容不變
					break;
				}
			}
		} catch (Exception e) {
			NBUtils.logger(this.getClass(), "[ERR]" + curTaskNum + "_callCrmRastApi_ExcepMsg:" + e.getMessage());
			crmReturnOutVO.setCode(CODE_FAIL);
			crmReturnOutVO.setSendStatus(SENDSTATUS_FAIL);
			crmReturnOutVO.setMsg(e.getMessage());
		}
		return crmReturnOutVO;
	}

	/**
	 * 產生CRM校驗，OPQ核保訊息:CRM系統連線失敗，請確認高齡銷售錄音覆審會辦作業。
	 * @param policyId
	 * @return
	 */
	private void writeRuleResultForCRM(Long policyId) {
		boolean ruleExists = false;
		ProposalRuleMsgVO msgVO = proposalRuleMsgService.findProposalRuleMsg(
				ProposalRuleMsg.PROPOSAL_RULE_MSG_ELDER_AUDIO_CRM_CONNECT,
				policyService.load(policyId).getApplyDate());
		if (msgVO != null) {//RULE_NAME=ElderAudio.CRMConnect.Warning1
			List<ProposalRuleResultVO> existVos = proposalRuleResultService.findByPolicyIdAndRuleStatus(policyId,
					ProposalRuleStatus.PROPOSAL_RULE_STATUS__PENDING);
			// 如果存在相同RuleName與ViolatedDesc且待處理就不新增
			if (CollectionUtils.isNotEmpty(existVos)) {
				// find exists in existVos
				ruleExists = existVos.stream().filter(
						vi -> (ProposalRuleMsg.PROPOSAL_RULE_MSG_ELDER_AUDIO_CRM_CONNECT.equals(vi.getRuleName())))
						.findFirst().isPresent();
			}
			if (!ruleExists) {
				ProposalRuleResultVO ruleResult= new ProposalRuleResultVO();
				ruleResult.setPolicyId(policyId);
				ruleResult.setRuleStatus(CodeCst.PROPOSAL_RULE_STATUS__PENDING);
				ruleResult.setRuleName(ProposalRuleMsg.PROPOSAL_RULE_MSG_ELDER_AUDIO_CRM_CONNECT);
				ruleResult.setPrintable(CodeCst.YES_NO__NO);
				ruleResult.setViolatedDesc(msgVO.getMessage());
				ruleResult.setLetterContent(msgVO.getDocMessage());
				ruleResult.setRuleType(ProposalRuleType.PROPOSAL_RULE_TYPE__OPQ_NOT_SYS_CLOSE_MSG);
				proposalRuleResultService.save(ruleResult);
			}
		}
	}

	private static final String _Q_ELDER_AUDIO_REVIEW_REPLY_SQL = "Select ma.CONSULT_DATE, ma.TASK_NUM, ma.ROLE_TYPE, ma.FILE_NAME_LIST, ma.AUDIO_NO_LIST,"
			+ "       AUDIO_OBJ_NAME, AUDIO_DATE,"
			+ "       DECODE(REVIEW_RESULT, null, '', 'Y', '正常', '異常') REVIEW_RESULT,"
			+ "       ISSUE_COMMENT, REAL_AUDIO_NO, REVIEWER_NAME, REVIEW_TIME"
			+ "  From T_UW_ELDER_AUDIO ma"
			+ "  Left Join T_UW_ELDER_AUDIO_DTL dtl On ma.TASK_NUM = dtl.TASK_NUM"
			+ " Where ma.POLICY_ID = ?"
			+ " Order By ma.CONSULT_DATE Desc, ma.TASK_NUM Desc";

	/**
	 * <p>Description : PCR 455342 高齡投保銷售過程錄音錄影需求優化<br/>
	 * 高齡銷售錄音覆審結果：顯示所有高齡銷售錄音覆審會辦資料(含系統自動及人工啟動的資料)<br />
	 *       資料依會辦日期時間(由晚到早)排序顯示
	 * </p>
	 * <p>Created By : Charlotte Wang</p>
	 * <p>Create Time : Aug. 1, 2022</p>
	 * @param policyId
	 * @return
	 */
	public List<Map<String, Object>> findAllElderAudioReviewReplyByPolicyID(Long policyId) {
		List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
		if (policyId == null || StringUtils.isEmpty(Long.valueOf(policyId).toString())) {
			return resultList;
		}

		DBean dbBean = new DBean();
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			dbBean.connect();
			con = dbBean.getConnection();
			ps = con.prepareStatement(_Q_ELDER_AUDIO_REVIEW_REPLY_SQL);
			ps.setLong(1, policyId);

			rs = ps.executeQuery();

			ResultSetMetaData metaData = ps.getMetaData();
			int cols = metaData.getColumnCount();

			while (rs.next()) {
				Map<String, Object> row = new HashMap<String, Object>();
				for (int j = 1; j <= cols; j++) {
					row.put(metaData.getColumnName(j), rs.getObject(j));
				}
				resultList.add(row);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new RTException(e);
		} finally {
			DBean.closeAll(rs, ps, dbBean);
		}
		return resultList;
	}

	private static final String _Q_ELDER_AUDIO_REVIEW_AUDIO_NO_SQL = "Select CONSULT_DATE consultDate, TASK_NUM taskNum,"
			+ "       ROLE_TYPE roleType, FILE_NAME_LIST fileNameList, AUDIO_NO_LIST audioNoList"
			+ "  From T_UW_ELDER_AUDIO"
			+ " Where POLICY_ID = :policyId And CERTI_CODE = :certiCode And AUDIO_NO_LIST Like '%'|| :audioNo ||'%'";

	/**
	 * <p>Description : PCR 455342 高齡投保銷售過程錄音錄影需求優化<br/>
	 * 判斷「高齡客戶投保評估量表」相對應的角色若有輸入「錄音編號」且此角色是否已經會辦高齡銷售錄音審核
	 * </p>
	 * <p>Created By : Charlotte Wang</p>
	 * <p>Create Time : Aug. 1, 2022</p>
	 * @param policyId
	 * @return
	 */
	public List<UwElderAudioVO> findElderAudioReviewByPolicyIdCertiCodeAudioNo(Long policyId, String certiCode, String audioNo) {
		List<UwElderAudioVO> resultList = new ArrayList<UwElderAudioVO>();
		if (policyId == null || StringUtils.isEmpty(Long.valueOf(policyId).toString())) {
			return resultList;
		}
		Session session = com.ebao.foundation.module.db.hibernate.HibernateSession3.currentSession();

		SQLQuery query = (SQLQuery) session.createSQLQuery(_Q_ELDER_AUDIO_REVIEW_AUDIO_NO_SQL);
		query.setParameter("policyId", EscapeHelper.escapeHtml(policyId));
		query.setParameter("certiCode", EscapeHelper.escapeHtml(certiCode));
		query.setParameter("audioNo", EscapeHelper.escapeHtml(audioNo));

		query.setResultTransformer(Transformers.aliasToBean(UwElderAudioVO.class));
		query.addScalar("consultDate", Hibernate.DATE);
		query.addScalar("taskNum", Hibernate.STRING);
		query.addScalar("roleType", Hibernate.STRING);
		query.addScalar("fileNameList", Hibernate.STRING);
		query.addScalar("audioNoList", Hibernate.STRING);

		resultList = (List<UwElderAudioVO>) query.list();

		return resultList;
	}
	
	/**
	 * 設定CRM高齡銷售錄音審核任務傳入資訊
	 * 
	 * @param policyId
	 * @return
	 * @author Iris Liu, 2023/04/20, PCR-517410
	 * @throws Exception
	 */
	private CrmElderAudioTaskQuestionnaireVO setCrmElderAudioTaskQuestionnaireVO(Long policyId) throws Exception {

		PolicyInfo policyInfo = policyService.load(policyId);
		PolicyVO policyVO = policyDS.load(policyId);
		Policy policyBo = policyDao.load(policyId);
		List<Coverage> coverages = policyBo.getCoverages();
		CoverageInfo masterCoverageInfo = policyInfo.getMasterCoverage();
		CoverageDataVO masterCoverageDataVO = policyQueryCompoundCI.getCoverageCompoundVO(policyId).getCoverageDataVOList().get(0); // 主約資訊(比照綜合查詢處理的資料)

		// 以下處理提供給CRM的資訊---------------------------------------------------
		CrmElderAudioTaskQuestionnaireVO crmElderAudioTaskQuestionnaireVO = new CrmElderAudioTaskQuestionnaireVO();

		// 業務員列表
		List<CrmElderAudioTaskQuestionnaireAgentVO> crmAgentList = new ArrayList<CrmElderAudioTaskQuestionnaireAgentVO>();
		for (CoverageAgentInfo data : masterCoverageInfo.getCoverageAgents()) {
			CrmElderAudioTaskQuestionnaireAgentVO crmAgent = new CrmElderAudioTaskQuestionnaireAgentVO();
			crmAgent.setAgentName(data.getAgentName());
			crmAgent.setRegisterCode(data.getRegisterCode());
			crmAgentList.add(crmAgent);
		}
		crmElderAudioTaskQuestionnaireVO.setAgentList(crmAgentList);

		// 投保目的列表
		List<AgentNotifyPurposeVO> applyPurposeList = agentNotifyPurposeService.findByPolicyId(policyId);
		List<String> applyPurposeCodeList = applyPurposeList.stream().map(AgentNotifyPurposeVO::getApplyPrupose).collect(Collectors.toList());
		NBUtils.setCodeTableList(crmElderAudioTaskQuestionnaireVO, applyPurposeCodeList, "applyPurposeList", "T_NB_APPLY_PURPOSE", UnbCst.CODE_TABLE_TYPE__1);

		crmElderAudioTaskQuestionnaireVO.setChargePeriod(masterCoverageDataVO.getChargePeriod()); // 繳費期間類別(比照綜合查詢-險種資訊)
		crmElderAudioTaskQuestionnaireVO.setChargeYear(masterCoverageDataVO.getChargeYear()); // 繳費年期/年齡(比照綜合查詢-險種資訊)
		crmElderAudioTaskQuestionnaireVO.setCoveragePeriod(masterCoverageDataVO.getCoveragePeriod()); // 保險期限(比照綜合查詢-險種資訊)
		crmElderAudioTaskQuestionnaireVO.setCoverageYear(masterCoverageDataVO.getCoverageYear()); // 保障年期(比照綜合查詢-險種資訊)

		Integer chargeMode = Integer.parseInt(masterCoverageDataVO.getPaymentFreq());
		crmElderAudioTaskQuestionnaireVO.setChargeMode(chargeMode); // 繳別(比照綜合查詢-險種資訊)
		crmElderAudioTaskQuestionnaireVO.setChargeModeDesc(CodeTable.getCodeDesc("T_CHARGE_MODE", chargeMode, CodeCst.LANGUAGE__CHINESE_TRAD)); // 繳別說明(比照綜合查詢-險種資訊)

		crmElderAudioTaskQuestionnaireVO.setInstallPrem(nbNotificationHelper.getContractProposalInitialPremByPolicyId(policyId)); // 首期保費(為月繳顯示2個月保費)		
		
		// 期繳保費(整張保單的期繳保費) 
		//20230626 by Iris, Hebe告知躉繳不需顯示期繳保費
		if(Boolean.FALSE == CodeCst.CHARGE_MODE__SINGLE.equals(masterCoverageDataVO.getPaymentFreq())) {
			crmElderAudioTaskQuestionnaireVO.setChargePrem(policyQueryService.getSumTotalPremAf(coverages));
		}
		
		crmElderAudioTaskQuestionnaireVO.setMoneyId(policyInfo.getCurrency()); // 保單幣別代碼
		crmElderAudioTaskQuestionnaireVO.setMoneyDesc(CodeTable.getCodeDesc("T_MONEY", policyInfo.getCurrency(), CodeCst.LANGUAGE__CHINESE_TRAD)); // 保單幣別中文

		// 保額/單位數/計劃別類型、單位數(比照綜合查詢-險種資訊，但因綜合查詢會針對單位數做Format，故需replace逗號)
		Map<String, String> amountAndType = AmountUnitBenefitLevelFormatter.formatAmountAndType(masterCoverageInfo.getCurrentPremium());
		String amountType = StringUtils.trimToEmpty(amountAndType.get(AmountUnitBenefitLevelFormatter.AMOUNT_TYPE));
		String amount = StringUtils.trimToEmpty(amountAndType.get(AmountUnitBenefitLevelFormatter.AMOUNT_VALUE)).replace(",", "");
		crmElderAudioTaskQuestionnaireVO.setAmountType(amountType);
		crmElderAudioTaskQuestionnaireVO.setAmount(amount);

		// 本次所繳保費來源列表
		List<String> premSourceCodeList = new ArrayList<String>();
	    List<FinanceNotifyPremSrcVO> financeNotifyPremSrcVOList = financeNotifyPremSourceService.findByPolicyId(policyId);
	    //20230627 如果財告有保費來源用財告
	    if(null!=financeNotifyPremSrcVOList && financeNotifyPremSrcVOList.size()>0) {
	    	premSourceCodeList = financeNotifyPremSrcVOList.stream().map(FinanceNotifyPremSrcVO::getPremSource).collect(Collectors.toList());
			NBUtils.setCodeTableList(crmElderAudioTaskQuestionnaireVO, premSourceCodeList, "premSourceList", "T_NB_PREM_SOURCE", UnbCst.CODE_TABLE_TYPE__1);
	    } else {
	    	//財告沒資料就拿業報的
			List<AgentNotifyPremSourceVO> agentNotifyPremSourceList = agentNotifyPremSourceService.findByPolicyId(policyId);
		    if(null!=agentNotifyPremSourceList && agentNotifyPremSourceList.size()>0) {
				premSourceCodeList = agentNotifyPremSourceList.stream().map(AgentNotifyPremSourceVO::getPremSource).collect(Collectors.toList());
				NBUtils.setCodeTableList(crmElderAudioTaskQuestionnaireVO, premSourceCodeList, "premSourceList", "T_NB_PREM_SOURCE_AGENT_NOTIFY", UnbCst.CODE_TABLE_TYPE__1);
		    }
	    }
	    

		InsuredInfo insuredInfo = policyInfo.getInsureds().get(0);
		crmElderAudioTaskQuestionnaireVO.setDetailWorkFt(insuredInfo.getDetailWorkFt()); // 被保險人職業
		crmElderAudioTaskQuestionnaireVO.setInsuredName(insuredInfo.getName()); // 被保險人姓名
		crmElderAudioTaskQuestionnaireVO.setInsuredCertiCode(insuredInfo.getCertiCode()); // 被保險人身份證字號

		// 單位代碼、單位簡稱(外部單位帶第1層組織、其他帶第2層組織)
		Map<String, String> rmsChannelInfo = policyServiceHelper.getRMSChannelInfo(policyInfo.getChannelType(), policyInfo.getChannelOrgId());
		String channelCode = MapUtils.getString(rmsChannelInfo, RuleRateTableCST.FACTOR__CHANNEL_CODE);
		ChannelOrgVO channelOrgVO = channelOrgService.findByChannelCode(channelCode);
		crmElderAudioTaskQuestionnaireVO.setChannelCode(channelCode);
		crmElderAudioTaskQuestionnaireVO.setChannelName(channelOrgVO.getOrgAbbr());
		
		crmElderAudioTaskQuestionnaireVO.setExternalChannelCode(policyDS.getExternalChannelCode(policyInfo.getChannelType())); // 通路別代碼
		crmElderAudioTaskQuestionnaireVO.setExternalChannelName(
				CodeTable.getCodeDesc("T_SALES_CHANNEL", policyInfo.getChannelType(), CodeCst.LANGUAGE__CHINESE_TRAD)); // 通路別名稱

		Map<String, String> mainCategoryMap = policyDS.getMainCategoryByProductId(masterCoverageInfo.getProductId());
		crmElderAudioTaskQuestionnaireVO.setCategoryCode(mainCategoryMap.get("mainCategory")); // 商品類別代碼
		crmElderAudioTaskQuestionnaireVO.setCategoryName(mainCategoryMap.get("mainCategoryName")); // 商品類別名稱

		return crmElderAudioTaskQuestionnaireVO;

	}

	
	/**
	 * 照所屬保單角色，調整CRM高齡銷售錄音審核任務的傳入資訊
	 * @param policyId
	 * @param roleType
	 * @param crmElderAudioTaskQuestionnaireVO
	 * @author Iris Liu, 2023/04/20, PCR-517410
	 */
	private void updateCrmElderAudioTaskQuestionnaireVO(Long policyId, String roleType,
			CrmElderAudioTaskQuestionnaireVO crmElderAudioTaskQuestionnaireVO) {

		AgentNotifyVO agentNotifyVO = agentNotifyService.findByPolicyId(policyId).get(0); // 業務員核保報告書

		/*2023/06/19
		 * 1.	資產(含動產和不動產)： 改捉財告書的「(1)存款(萬元/美元) 」+ 「(2)股票等其他有價證劵(萬元/美元) 」+ 「不動產之市價及座落地點 總市價約(萬元/美元) 」
		 * 2.	負債：改捉財告書的「負債總額(萬元/美元)」
		 */
		List<FinanceNotifyVO> financeNotifyVOList = financeNotifyService.findByPolicyId(policyId);
		BigDecimal income = BigDecimal.ZERO;
		BigDecimal incomeOther = BigDecimal.ZERO;
		BigDecimal incomeFamily = BigDecimal.ZERO;
		BigDecimal netAssets =  BigDecimal.ZERO;
		BigDecimal debtAmount = BigDecimal.ZERO;
		FinanceNotifyVO financeNotifyVO = null;
		
		//篩選財告書資料
		for( FinanceNotifyVO vo: financeNotifyVOList) {
			if( roleType.equals(vo.getRelationHolderInsured()) ) {
				//被保人取主被保人的資料
				if( UnbRoleType.INSURED.getRoleType().equals(vo.getRelationHolderInsured())) {
					PolicyVO policyVO = policyDS.load(policyId);
					InsuredVO mainInsured = policyVO.getInsureds().stream().filter(insured -> insured.isMasterInsured()).findFirst().orElse(null);
					if(null!=mainInsured) {
						Long mainInsuredId = mainInsured.getListId();
						if(vo.getInsuredId().longValue()==mainInsuredId.longValue()) {
							financeNotifyVO = vo;
							break;
						}
					}
				} else {
					//要保人
					financeNotifyVO = vo;
					break;
				}
			}
		}
		
		//先取財告書資料
		if(null!=financeNotifyVO) {
			//個人年收入
			if(null!=financeNotifyVO.getIncome()) {
				income = NBUtils.multiply1Million(financeNotifyVO.getIncome());
			}
			//個人家庭收入
			if(null!=financeNotifyVO.getIncomeFamily()) {
				incomeFamily = NBUtils.multiply1Million(financeNotifyVO.getIncomeFamily());
			}
			//個人其他收入
			if(null!=financeNotifyVO.getIncomeOther()) {
				incomeOther = NBUtils.multiply1Million(financeNotifyVO.getIncomeOther());
			}
			//存款
			if(null!=financeNotifyVO.getBankDeposit()) {
				netAssets = netAssets.add(NBUtils.multiply1Million(financeNotifyVO.getBankDeposit()));
			}
			//股票等其他有價證劵
			if(null!=financeNotifyVO.getStock()) {
				netAssets = netAssets.add(NBUtils.multiply1Million(financeNotifyVO.getStock()));
			}
			//不動產之市價及座落地點  總市價
			if(null!=financeNotifyVO.getAssetsPrice()) {
				netAssets = netAssets.add(NBUtils.multiply1Million(financeNotifyVO.getAssetsPrice()));
			}
			//負債總額
			if(null!=financeNotifyVO.getLiability()) {
				debtAmount = NBUtils.multiply1Million(financeNotifyVO.getLiability());
			}
		}
		
		/*
		 * 2023/06/21
		 * 個人年收入、其他收入、家庭收入如果未在財告書取得有效資料(非空或非零)則拿業報書資料
		 */
		if(income.equals(BigDecimal.ZERO)) {
			if (UnbRoleType.INSURED.getRoleType().equals(roleType)) {
				income = agentNotifyVO.getInsuIncome();
			} else if (UnbRoleType.POLICY_HOLDER.getRoleType().equals(roleType)) {
				income = agentNotifyVO.getHolderIncome();
			}
		}

		if(incomeOther.equals(BigDecimal.ZERO)) {
			if (UnbRoleType.INSURED.getRoleType().equals(roleType)) {
				incomeOther = agentNotifyVO.getInsuIncomeOther();
			} else if (UnbRoleType.POLICY_HOLDER.getRoleType().equals(roleType)) {
				incomeOther = agentNotifyVO.getHolderIncomeOther();
			}
		}

		if(incomeFamily.equals(BigDecimal.ZERO)) {
			if (UnbRoleType.INSURED.getRoleType().equals(roleType)) {
				incomeFamily = agentNotifyVO.getInsuIncomeFamily();
			} else if (UnbRoleType.POLICY_HOLDER.getRoleType().equals(roleType)) {
				incomeFamily = agentNotifyVO.getHolderIncomeFamily();
			}
		}
		
		/*
		 * 2023/06/20
		 * 資產與負債來自財告書，財告書db紀錄台幣單位為萬元，需自行*10000
		 * */
		//要\被保人
		if(UnbRoleType.INSURED.getRoleType().equals(roleType) || UnbRoleType.POLICY_HOLDER.getRoleType().equals(roleType)) {
			crmElderAudioTaskQuestionnaireVO.setIncome(income); // 個人工作年收入(單位:元)
			crmElderAudioTaskQuestionnaireVO.setIncomeOther(incomeOther); // 個人其他收入(單位:元)
			crmElderAudioTaskQuestionnaireVO.setIncomeFamily(incomeFamily); // 家庭年收入(單位:元)
			crmElderAudioTaskQuestionnaireVO.setNetAssets(netAssets); // 資產(含動產和不動產)(單位:元)
			crmElderAudioTaskQuestionnaireVO.setDebtAmount(debtAmount); // 負債(單位:元)
		} else {
			crmElderAudioTaskQuestionnaireVO.setIncome(null);
			crmElderAudioTaskQuestionnaireVO.setIncomeOther(null);
			crmElderAudioTaskQuestionnaireVO.setIncomeFamily(null);
			crmElderAudioTaskQuestionnaireVO.setNetAssets(null);
			crmElderAudioTaskQuestionnaireVO.setDebtAmount(null);
		}

	}

	private static final String _Q_ELDER_AUDIO_AUDIO_NO_SQL = "Select ROLE_TYPE roleType, AUDIO_NO_LIST audioNoList"
			+ "  From T_UW_ELDER_AUDIO"
			+ " Where POLICY_ID = :policyId ";

	/**
	 * <p>Description : PCR 517410 FID新增合作銀行專案<br/>
	 *  以policyId查詢錄音會辦內的錄音編號
	 * </p>
	 * <p>Created By : Willy</p>
	 * <p>Create Time : Apr. 18, 2023</p>
	 * @param policyId
	 * @return
	 */
	public List<UwElderAudioVO> findElderAudioNoByPolicyId(Long policyId) {
		List<UwElderAudioVO> resultList = new ArrayList<UwElderAudioVO>();
		if (policyId == null || StringUtils.isEmpty(Long.valueOf(policyId).toString())) {
			return resultList;
		}
		Session session = com.ebao.foundation.module.db.hibernate.HibernateSession3.currentSession();

		SQLQuery query = (SQLQuery) session.createSQLQuery(_Q_ELDER_AUDIO_AUDIO_NO_SQL);
		query.setParameter("policyId", EscapeHelper.escapeHtml(policyId));

		query.setResultTransformer(Transformers.aliasToBean(UwElderAudioVO.class));
		query.addScalar("roleType", Hibernate.STRING);
		query.addScalar("audioNoList", Hibernate.STRING);

		resultList = (List<UwElderAudioVO>) query.list();

		return resultList;
	}

	@Resource(name = VOGenerator.BEAN_DEFAULT)
	private VOGenerator voGenerator;
	
	@Resource(name = UwElderAudioDtlDao.BEAN_DEFAULT)
	private UwElderAudioDtlDao uwElderAudioDtlDao;
	
	@Resource(name = UnbProcessCI.BEAN_DEFAULT)
	private UnbProcessCI unbProcessCI;
	
	@Resource(name = CrmElderAudioTaskRstfCI.BEAN_DEFAULT)
	private CrmElderAudioTaskRstfCI crmElderAudioTaskRstfCI;
	
	@Resource(name = PolicyService.BEAN_DEFAULT)
	PolicyService policyService;
	
	@Resource(name = ProposalRuleResultService.BEAN_DEFAULT)
	private ProposalRuleResultService proposalRuleResultService;
	
	@Resource(name = ProposalRuleMsgService.BEAN_DEFAULT)
	private ProposalRuleMsgService proposalRuleMsgService;
	
	@Resource(name = ElderAudioHelper.BEAN_DEFAULT)
	private ElderAudioHelper elderAudioHelper;
	

	@Resource(name = AgentNotifyService.BEAN_DEFAULT)
	private AgentNotifyService agentNotifyService;
	
	@Resource(name = AgentNotifyPurposeService.BEAN_DEFAULT)
	private AgentNotifyPurposeService agentNotifyPurposeService;
	
	@Resource(name = AgentNotifyPremSourceService.BEAN_DEFAULT)
	private AgentNotifyPremSourceService agentNotifyPremSourceService;
	
	@Resource(name = NbNotificationHelper.BEAN_DEFAULT)
	private NbNotificationHelper nbNotificationHelper;
	
    @Resource(name = PolicyDao.BEAN_PA_POLICY_VIEW)
    private PolicyDao<Policy> policyDao;
    
    @Resource(name = PolicyQueryService.BEAN_DEFAULT)
    private PolicyQueryService policyQueryService;
    
    @Resource(name = PolicyQueryCompoundCI.BEAN_DEFAULT)
    private PolicyQueryCompoundCI policyQueryCompoundCI;
    
    @Resource(name = LifeProductService.BEAN_DEFAULT)
	protected LifeProductService lifeProductService;
    
	@Resource(name = AgentService.BEAN_DEFAULT)
	private AgentService agentService;
	
	@Resource(name = com.ebao.ls.pa.pub.bs.PolicyService.BEAN_DEFAULT)
	private com.ebao.ls.pa.pub.bs.PolicyService policyDS;
	
    @Resource(name = PolicyServiceHelper.BEAN_DEFAULT)
    private PolicyServiceHelper policyServiceHelper;
   
	@Resource(name = ChannelOrgService.BEAN_DEFAULT)
	private ChannelOrgService channelOrgService;

	@Resource(name = FinanceNotifyService.BEAN_DEFAULT)
	private FinanceNotifyService financeNotifyService;
	
	@Resource(name = FinanceNotifyPremSourceService.BEAN_DEFAULT)
	protected FinanceNotifyPremSourceService financeNotifyPremSourceService;
}
