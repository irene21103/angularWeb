package com.ebao.ls.uw.ctrl.decision;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.UserTransaction;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.ls.escape.helper.EscapeHelper;
import com.ebao.ls.uw.ctrl.ActionUtil;
import com.ebao.ls.uw.ds.UwPolicyService;
import com.ebao.ls.uw.vo.UwRiFacVO;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.framework.AppException;
import com.ebao.pub.framework.ExceptionFactory;
import com.ebao.pub.framework.GenericAction;
import com.ebao.pub.util.Trans;
import com.ebao.pub.util.TransUtils;

/**
 * <p>
 * Title:
 * </p>
 * <p>
 * Description:
 * </p>
 * <p>
 * Copyright: Copyright (c) 2005
 * </p>
 * <p>
 * Company: eBaoTech Corporation
 * </p>
 * 
 * @author mingchun.shi
 * @version 1.0
 */

public class AddFacRequestAction extends GenericAction {

	public static final String BEAN_DEFAULT = "/uw/addFacRequest";

	/**
	 * add the apply fac request
	 * 
	 * @param mapping
	 * @param form
	 * @param req
	 * @param res
	 * @return
	 * @throws Exception
	 */
	@Override
	public ActionForward process(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse res)
			throws Exception {
		ApplyFacRequestForm requestForm = (ApplyFacRequestForm) form;
		UserTransaction ut = Trans.getUserTransaction();

		try {
			String[] itemIds = requestForm.getItemIds();
			
			// check duplicated record with status 'waiting for confirm'
			checkDuplicateRecord(itemIds);			
			
			String underwritingId = EscapeHelper.escapeHtml(request.getParameter("underwriteId"));
			
			EscapeHelper.escapeHtml(request).setAttribute("underwriteId", underwritingId);
			
			Date sysDate = AppContext.getCurrentUserLocalTime();

			ut.begin();
			for (String itemId : itemIds) {
				UwRiFacVO uwRiFacVO = new UwRiFacVO();
				uwRiFacVO.setItemId(Long.valueOf(itemId));
				uwRiFacVO.setUnderwritingId(Long.valueOf(underwritingId));
				uwRiFacVO.setRequestDate(sysDate);
				uwRiFacVO.setRequestMemo(requestForm.getFacRequest());
				uwRiFacVO.setStatus(0L);
				uwPolicyService.addFacRequest(uwRiFacVO);
			}
			ut.commit();

			ActionUtil.setFacRequestList(uwPolicyService, request, itemIds);
		} catch (Exception e) {
			TransUtils.rollback(ut);
			throw ExceptionFactory.parse(e);
		}
		return mapping.findForward("display");
	}

	@Resource(name = UwPolicyService.BEAN_DEFAULT)
	private UwPolicyService uwPolicyService;

	public UwPolicyService getUwPolicyService() {
		return uwPolicyService;
	}

	public void setUwPolicyService(UwPolicyService uwPolicyService) {
		this.uwPolicyService = uwPolicyService;
	}

	private void checkDuplicateRecord(String[] itemIdArr) throws AppException {
		if(itemIdArr != null && itemIdArr.length > 0) {
			for(int i=0; i<itemIdArr.length; i++) {
				List<UwRiFacVO> requestList = uwPolicyService.findByItemId(Long.parseLong(itemIdArr[i]));
				
				if(requestList != null && requestList.size() > 0) {
					for(int j=0, size=requestList.size(); j<size; j++) {
						UwRiFacVO uwRiFacVO = requestList.get(j);
						
						if(uwRiFacVO.getStatus() == UwPolicyService.FAC_REQ_STATUS_WAITING_FOR_CONFIRM) {
							throw new AppException(UwPolicyService.ERR_DUPLICATE_FAC_REQ);
						}
					}
				}
			}
		} 
	}
}
