package com.ebao.ls.uw.ctrl.pub;

import java.util.Date;

import com.ebao.pub.framework.GenericForm;

/**
 * <p>Title:核保作業-保項決定</p>
 * <p>Description: 保項批註、加費頁面共用顯示欄位</p>
 * <p>Copyright: Copyright (c) 2015</p>
 * <p>Company: TGL Co., Ltd.</p>
 * <p>Create Time: Jul 15, 2015</p> 
 * @author 
 * <p>Update Time: Jul 15, 2015</p>
 * <p>Updater: simon.huang</p>
 * <p>Update Comments: </p>
 */
public class UwProductGenericForm extends GenericForm {
    
    private Long underwriteId;

    private Long policyId;

    private Long insuredId;

    private Long itemIds;

    private Long productId;

    private String applyCode;

    private Date applyDate;

    private String policyCode;

    private String uwStatus;

    private String benefityType;

    private String uwSourceType;

    private Date validateDate;

    private String policyHolderName;

    private Date policyHolderBirthday;

    private Integer policyHolderAge;

    private String policyHolderGender;

    private UwProductForm uwProductForm;
    
    /** 是否為綜合查詢頁面進入Iframe */
	private String isIframe;


    // msg
    private String msg;

    /**
     * @return 傳回 underwriteId。
     */
    public Long getUnderwriteId() {
        return underwriteId;
    }

    /**
     * @param underwriteId 要設定的 underwriteId。
     */
    public void setUnderwriteId(Long underwriteId) {
        this.underwriteId = underwriteId;
    }

    /**
     * @return 傳回 policyId。
     */
    public Long getPolicyId() {
        return policyId;
    }

    /**
     * @param policyId 要設定的 policyId。
     */
    public void setPolicyId(Long policyId) {
        this.policyId = policyId;
    }

    /**
     * @return 傳回 insuredId。
     */
    public Long getInsuredId() {
        return insuredId;
    }

    /**
     * @param insuredId 要設定的 insuredId。
     */
    public void setInsuredId(Long insuredId) {
        this.insuredId = insuredId;
    }

    /**
     * @return 傳回 itemIds。
     */
    public Long getItemIds() {
        return itemIds;
    }

    /**
     * @param itemId 要設定的 itemIds。
     */
    public void setItemIds(Long itemIds) {
        this.itemIds = itemIds;
    }

    /**
     * @return 傳回 productId。
     */
    public Long getProductId() {
        return productId;
    }

    /**
     * @param productId 要設定的 productId。
     */
    public void setProductId(Long productId) {
        this.productId = productId;
    }

    /**
     * @return 傳回 applyCode。
     */
    public String getApplyCode() {
        return applyCode;
    }

    /**
     * @param applyCode 要設定的 applyCode。
     */
    public void setApplyCode(String applyCode) {
        this.applyCode = applyCode;
    }

    /**
     * @return 傳回 applyDate。
     */
    public Date getApplyDate() {
        return applyDate;
    }

    /**
     * @param applyDate 要設定的 applyDate。
     */
    public void setApplyDate(Date applyDate) {
        this.applyDate = applyDate;
    }

    /**
     * @return 傳回 policyCode。
     */
    public String getPolicyCode() {
        return policyCode;
    }

    /**
     * @param policyCode 要設定的 policyCode。
     */
    public void setPolicyCode(String policyCode) {
        this.policyCode = policyCode;
    }

    /**
     * @return 傳回 uwStatus。
     */
    public String getUwStatus() {
        return uwStatus;
    }

    /**
     * @param uwStatus 要設定的 uwStatus。
     */
    public void setUwStatus(String uwStatus) {
        this.uwStatus = uwStatus;
    }

    /**
     * @return 傳回 benefityType。
     */
    public String getBenefityType() {
        return benefityType;
    }

    /**
     * @param benefityType 要設定的 benefityType。
     */
    public void setBenefityType(String benefityType) {
        this.benefityType = benefityType;
    }

    /**
     * @return 傳回 uwSourceType。
     */
    public String getUwSourceType() {
        return uwSourceType;
    }

    /**
     * @param uwSourceType 要設定的 uwSourceType。
     */
    public void setUwSourceType(String uwSourceType) {
        this.uwSourceType = uwSourceType;
    }

    /**
     * @return 傳回 validateDate。
     */
    public Date getValidateDate() {
        return validateDate;
    }

    /**
     * @param validateDate 要設定的 validateDate。
     */
    public void setValidateDate(Date validateDate) {
        this.validateDate = validateDate;
    }

    /**
     * @return 傳回 policyHolderName。
     */
    public String getPolicyHolderName() {
        return policyHolderName;
    }

    /**
     * @param policyHolderName 要設定的 policyHolderName。
     */
    public void setPolicyHolderName(String policyHolderName) {
        this.policyHolderName = policyHolderName;
    }

    /**
     * @return 傳回 policyHolderBirthday。
     */
    public Date getPolicyHolderBirthday() {
        return policyHolderBirthday;
    }

    /**
     * @param policyHolderBirthday 要設定的 policyHolderBirthday。
     */
    public void setPolicyHolderBirthday(Date policyHolderBirthday) {
        this.policyHolderBirthday = policyHolderBirthday;
    }

    /**
     * @return 傳回 policyHolderAge。
     */
    public Integer getPolicyHolderAge() {
        return policyHolderAge;
    }

    /**
     * @param policyHolderAge 要設定的 policyHolderAge。
     */
    public void setPolicyHolderAge(Integer policyHolderAge) {
        this.policyHolderAge = policyHolderAge;
    }

    /**
     * @return 傳回 policyHolderGender。
     */
    public String getPolicyHolderGender() {
        return policyHolderGender;
    }

    /**
     * @param policyHolderGender 要設定的 policyHolderGender。
     */
    public void setPolicyHolderGender(String policyHolderGender) {
        this.policyHolderGender = policyHolderGender;
    }

    /**
     * @return 傳回 uwProductForm。
     */
    public UwProductForm getUwProductForm() {
        return uwProductForm;
    }

    /**
     * @param uwProductForm 要設定的 uwProductForm。
     */
    public void setUwProductForm(UwProductForm uwProductForm) {
        this.uwProductForm = uwProductForm;
    }

    /**
     * @return 傳回 msg。
     */
    public String getMsg() {
        return msg;
    }

    /**
     * @param msg 要設定的 msg。
     */
    public void setMsg(String msg) {
        this.msg = msg;
    }

	public String getIsIframe() {
		return isIframe;
	}

	public void setIsIframe(String isIframe) {
		this.isIframe = isIframe;
	}

}