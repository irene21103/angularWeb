package com.ebao.ls.uw.ctrl.info;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashMap;

import com.ebao.pub.framework.GenericForm;

/**
 * DisplayBeneficiaryAction etc.) for transfer between Action and Presentation
 * Layer.
 */
public class PolicyForm extends GenericForm {
  private Collection benefitInfo;
  private Collection csInfo;
  private Collection nbuInfo;
  private Collection loanAccountInfo;
  private Collection depositAccountInfo;
  private Collection uwInfo;
  private Collection claimInfo;
  private Collection cpfInfo;
  private Collection proposal;
  private Collection insured;
  private Collection nominee;
  private Collection trustee;
  private Collection assignee;
  private Collection payer;
  private Collection payee;
  // policy info
  private String applyCode;
  private Long branchId;
  private String productName;
  private String mainPlan;
  private String sa;
  private String applyDate;
  private String issueDate;
  private String validateDate;
  private String paidupDate;
  private String endDate;
  private String duration;
  private String policyDuration;
  private String liabilityState;
  private String proposalState;
  private String premStatus;
  private String endCause;
  private String terminateDate;
  private String suspend = "N";
  private String suspendCause;
  private String lapseDate;
  private String lapseCause;
  private String ppLapseDate;
  private String addressStatus;
  private String mailFailedTimes;
  private String pendingCause;
  private Long sendOutId;
  private String sendOutName;
  private String deliverType;
  private String sendOutDate;
  private String gibIndi;
  private String gibInviteDate;
  private String gibInviteSaInc;
  private String dcRepCollectionDdate;
  private String campaignCode;
  private String acknowledgeDate;
  private String reversalReason;
  private String serviceReceivedDate;
  private String apilpMaxLimit;
  private String apilpDesc;
  private String nbReversalReason;
  private String isUsed = "N";
  private String initAmount;
  private String balAmount;
  private String utilizedAmount;
  private String holidayIndi = "N";
  private String voucherTagPolicy;
  // financial info
  private String displayLoanValue = "Y"; // add by lucky for CQ:CRDB00281593
  private String discountType = "0";
  private String moneyId;
  private String tip;
  private String tipString;
  private String payMode;
  private String paymentFrequency;
  private String dueDate;
  private String renewalType;
  private String overduePremium;
  private String aplBalance;
  private String plBalance;
  private String slBalance;
  private String cbBalanceDate;
  private String sbBalanceDate;
  private String rbLastAllocDate;
  private String renewSuspense;
  private String csSuspense;
  private String generalSuspense;
  private String apaBalance;
  private String cbBalance;
  private String sbBalance;
  private String survivalOption;
  private Integer survivalOptTerm;
  private Integer dividendOptTerm;
  private Integer dividendChoice;
  private String lastPayDate;
  private String minAmountPreventLapse;
  private String minReinstate;
  private String fullReinstate;
  private String amountReceived;
  private String amountTurnInforce;
  private String netSV;
  private String netPV;
  private String tiv;
  private String lastCapitalisationDate;
  private String accumRB;
  private String netLoan;
  private String suspenseLocked;
  private Integer dunningLevel;
  private String policySection;
  private String trustPolicyIndi = "N";
  private String futChildIndi = "N";
  private String spouseChildIndi = "N";
  private String endCauseType;
  private String dispatchType;
  protected String stdBeneIndiDeath;
  protected String stdBeneIndiLife;
  private String deviationCode;
  private String deviationFreeText;
  private String notInforceReason;
  
  private BigDecimal aplUnCapitalizeInterestSys;
  private BigDecimal aplTransAmount;
  private BigDecimal aplCapitalizeInterest;
  private BigDecimal aplSum;
  
  private BigDecimal loanUnCapitalizeInterestSys;
  private BigDecimal loanTransAmount;
  private BigDecimal loanCapitalizeInterest;
  private BigDecimal loanSum;
  
  //BaseForm
  private HashMap resultMap;
  private Long policyId;
  private String policyCode;
  private String tableName;
  private Collection resultCollection;
  private String accessOrganIds;
  private String policyCurrency;
  private String actionType;
  
  //RetMsg
  private String retMsg; // 2018/02/01 Add : YCSu -- [artf410579](Internal Issue 207219)[UAT2]未產生TGL理賠歷史資料
  
  public String getNotInforceReason() {
    return notInforceReason;
  }

  public void setNotInforceReason(String notInforceReason) {
    this.notInforceReason = notInforceReason;
  }

  public String getFullReinstate() {
    return fullReinstate;
  }

  public void setFullReinstate(String fullReinstate) {
    this.fullReinstate = fullReinstate;
  }
  public String getDeviationCode() {
    return deviationCode;
  }

  public void setDeviationCode(String deviationCode) {
    this.deviationCode = deviationCode;
  }

  public String getDeviationFreeText() {
    return deviationFreeText;
  }

  public void setDeviationFreeText(String deviationFreeText) {
    this.deviationFreeText = deviationFreeText;
  }

  public void setStdBeneIndiDeath(String stdBeneIndiDeath) {
    this.stdBeneIndiDeath = stdBeneIndiDeath;
  }

  public String getStdBeneIndiDeath() {
    return this.stdBeneIndiDeath;
  }

  public void setStdBeneIndiLife(String stdBeneIndiLife) {
    this.stdBeneIndiLife = stdBeneIndiLife;
  }

  public String getStdBeneIndiLife() {
    return this.stdBeneIndiLife;
  }

  public void setFreeTextPolicyClauseDth(String freeTextPolicyClauseDth) {
    this.freeTextPolicyClauseDth = freeTextPolicyClauseDth;
  }

  public String getFreeTextPolicyClauseDth() {
    return this.freeTextPolicyClauseDth;
  }

  public void setFreeTextPolicyClauseLife(String freeTextPolicyClauseLife) {
    this.freeTextPolicyClauseLife = freeTextPolicyClauseLife;
  }

  public String getFreeTextPolicyClauseLife() {
    return this.freeTextPolicyClauseLife;
  }

  protected String freeTextPolicyClauseDth;
  protected String freeTextPolicyClauseLife;
  /**
   * sales channel staff code
   */
  private String channelStaffCode;

  public String getChannelStaffCode() {
    return channelStaffCode;
  }

  public void setChannelStaffCode(String channelStaffCode) {
    this.channelStaffCode = channelStaffCode;
  }

  public String getAccumRB() {
    return accumRB;
  }

  public String getAcknowledgeDate() {
    return acknowledgeDate;
  }

  public String getAddressStatus() {
    return addressStatus;
  }

  public String getAmountReceived() {
    return amountReceived;
  }

  public String getAmountTurnInforce() {
    return amountTurnInforce;
  }

  public String getApaBalance() {
    return apaBalance;
  }

  public String getAplBalance() {
    return aplBalance;
  }

  public String getApplyDate() {
    return applyDate;
  }

  public String getApplyCode() {
    return applyCode;
  }

  public Collection getAssignee() {
    return assignee;
  }

  public String getBalAmount() {
    return balAmount;
  }

  public Collection getBenefitInfo() {
    return benefitInfo;
  }

  public Long getBranchId() {
    return branchId;
  }

  public String getCampaignCode() {
    return campaignCode;
  }

  public String getCbBalance() {
    return cbBalance;
  }

  public Collection getClaimInfo() {
    return claimInfo;
  }

  public Collection getCpfInfo() {
    return cpfInfo;
  }

  public Collection getCsInfo() {
    return csInfo;
  }

  public String getCsSuspense() {
    return csSuspense;
  }

  public String getDcRepCollectionDdate() {
    return dcRepCollectionDdate;
  }

  public String getDeliverType() {
    return deliverType;
  }

  public String getDiscountType() {
    return discountType;
  }

  public Integer getDividendChoice() {
    return dividendChoice;
  }

  public Integer getDividendOptTerm() {
    return dividendOptTerm;
  }

  public String getDueDate() {
    return dueDate;
  }

  public String getDuration() {
    return duration;
  }

  public String getEndCause() {
    return endCause;
  }

  public String getEndDate() {
    return endDate;
  }

  public String getFutChildIndi() {
    return futChildIndi;
  }

  public String getGeneralSuspense() {
    return generalSuspense;
  }

  public String getGibIndi() {
    return gibIndi;
  }

  public String getGibInviteDate() {
    return gibInviteDate;
  }

  public String getGibInviteSaInc() {
    return gibInviteSaInc;
  }

  public String getInitAmount() {
    return initAmount;
  }

  public Collection getInsured() {
    return insured;
  }

  public String getIssueDate() {
    return issueDate;
  }

  public String getIsUsed() {
    return isUsed;
  }

  public String getLapseCause() {
    return lapseCause;
  }

  public String getLapseDate() {
    return lapseDate;
  }

  public String getLastCapitalisationDate() {
    return lastCapitalisationDate;
  }

  public String getLastPayDate() {
    return lastPayDate;
  }

  public String getLiabilityState() {
    return liabilityState;
  }

  public String getMailFailedTimes() {
    return mailFailedTimes;
  }

  public String getMainPlan() {
    return mainPlan;
  }

  public String getMinAmountPreventLapse() {
    return minAmountPreventLapse;
  }

  public String getMinReinstate() {
    return minReinstate;
  }

  public String getMoneyId() {
    return moneyId;
  }

  public String getNetPV() {
    return netPV;
  }

  public String getNetLoan() {
    return netLoan;
  }

  public Collection getNbuInfo() {
    return nbuInfo;
  }

  public String getNetSV() {
    return netSV;
  }

  public Collection getNominee() {
    return nominee;
  }

  public String getOverduePremium() {
    return overduePremium;
  }

  public String getPaidupDate() {
    return paidupDate;
  }

  public Collection getPayee() {
    return payee;
  }

  public Collection getPayer() {
    return payer;
  }

  public String getPaymentFrequency() {
    return paymentFrequency;
  }

  public String getPendingCause() {
    return pendingCause;
  }

  public String getPlBalance() {
    return plBalance;
  }

  public String getPolicySection() {
    return policySection;
  }

  public String getPpLapseDate() {
    return ppLapseDate;
  }

  public String getPremStatus() {
    return premStatus;
  }

  public Collection getProposal() {
    return proposal;
  }

  public String getRenewalType() {
    return renewalType;
  }

  public String getRenewSuspense() {
    return renewSuspense;
  }

  public String getReversalReason() {
    return reversalReason;
  }

  public String getSa() {
    return sa;
  }

  public String getSbBalance() {
    return sbBalance;
  }

  public String getSendOutDate() {
    return sendOutDate;
  }

  public Long getSendOutId() {
    return sendOutId;
  }

  public String getServiceReceivedDate() {
    return serviceReceivedDate;
  }

  public String getSlBalance() {
    return slBalance;
  }

  public String getSurvivalOption() {
    return survivalOption;
  }

  public Integer getSurvivalOptTerm() {
    return survivalOptTerm;
  }

  public String getSuspend() {
    return suspend;
  }

  public String getSuspendCause() {
    return suspendCause;
  }

  public String getTerminateDate() {
    return terminateDate;
  }

  public String getTip() {
    return tip;
  }

  public String getTipString() {
    return tipString;
  }

  public void setTipString(String tipString) {
    this.tipString = tipString;
  }

  public String getTiv() {
    return tiv;
  }

  public Collection getTrustee() {
    return trustee;
  }

  public String getTrustPolicyIndi() {
    return trustPolicyIndi;
  }

  public Collection getUwInfo() {
    return uwInfo;
  }

  public String getValidateDate() {
    return validateDate;
  }

  public void setAccumRB(String accumRB) {
    this.accumRB = accumRB;
  }

  public void setAcknowledgeDate(String acknowledgeDate) {
    this.acknowledgeDate = acknowledgeDate;
  }

  public void setAddressStatus(String addressStatus) {
    this.addressStatus = addressStatus;
  }

  public void setAmountReceived(String amountReceived) {
    this.amountReceived = amountReceived;
  }

  public void setAmountTurnInforce(String amountTurnInforce) {
    this.amountTurnInforce = amountTurnInforce;
  }

  public void setApaBalance(String apaBalance) {
    this.apaBalance = apaBalance;
  }

  public void setAplBalance(String aplBalance) {
    this.aplBalance = aplBalance;
  }

  public void setApplyCode(String applyCode) {
    this.applyCode = applyCode;
  }

  public void setApplyDate(String applyDate) {
    this.applyDate = applyDate;
  }

  public void setAssignee(Collection assignee) {
    this.assignee = assignee;
  }

  public void setBalAmount(String balAmount) {
    this.balAmount = balAmount;
  }

  public void setBenefitInfo(Collection benefitInfo) {
    this.benefitInfo = benefitInfo;
  }

  public void setBranchId(Long branchId) {
    this.branchId = branchId;
  }

  public void setCampaignCode(String campaignCode) {
    this.campaignCode = campaignCode;
  }

  public void setCbBalance(String cbBalance) {
    this.cbBalance = cbBalance;
  }

  public void setClaimInfo(Collection claimInfo) {
    this.claimInfo = claimInfo;
  }

  public void setCpfInfo(Collection cpfInfo) {
    this.cpfInfo = cpfInfo;
  }

  public void setCsInfo(Collection csInfo) {
    this.csInfo = csInfo;
  }

  public void setCsSuspense(String csSuspense) {
    this.csSuspense = csSuspense;
  }

  public void setDcRepCollectionDdate(String dcRepCollectionDdate) {
    this.dcRepCollectionDdate = dcRepCollectionDdate;
  }

  public void setDeliverType(String deliverType) {
    this.deliverType = deliverType;
  }

  public void setDiscountType(String discountType) {
    this.discountType = discountType;
  }

  public void setDividendChoice(Integer dividendChoice) {
    this.dividendChoice = dividendChoice;
  }

  public void setDividendOptTerm(Integer dividendOptTerm) {
    this.dividendOptTerm = dividendOptTerm;
  }

  public void setDueDate(String dueDate) {
    this.dueDate = dueDate;
  }

  public void setDuration(String duration) {
    this.duration = duration;
  }

  public void setEndCause(String endCause) {
    this.endCause = endCause;
  }

  public void setEndDate(String endDate) {
    this.endDate = endDate;
  }

  public void setFutChildIndi(String futChildIndi) {
    this.futChildIndi = futChildIndi;
  }

  public void setGeneralSuspense(String generalSuspense) {
    this.generalSuspense = generalSuspense;
  }

  public void setGibIndi(String gibIndi) {
    this.gibIndi = gibIndi;
  }

  public void setGibInviteDate(String gibInviteDate) {
    this.gibInviteDate = gibInviteDate;
  }

  public void setGibInviteSaInc(String gibInviteSaInc) {
    this.gibInviteSaInc = gibInviteSaInc;
  }

  public void setInitAmount(String initAmount) {
    this.initAmount = initAmount;
  }

  public void setInsured(Collection insured) {
    this.insured = insured;
  }

  public void setIssueDate(String issueDate) {
    this.issueDate = issueDate;
  }

  public void setIsUsed(String isUsed) {
    this.isUsed = isUsed;
  }

  public void setLapseCause(String lapseCause) {
    this.lapseCause = lapseCause;
  }

  public void setLapseDate(String lapseDate) {
    this.lapseDate = lapseDate;
  }

  public void setLastCapitalisationDate(String lastCapitalisationDate) {
    this.lastCapitalisationDate = lastCapitalisationDate;
  }

  public void setLastPayDate(String lastPayDate) {
    this.lastPayDate = lastPayDate;
  }

  public void setLiabilityState(String liabilityState) {
    this.liabilityState = liabilityState;
  }

  public void setMailFailedTimes(String mailFailedTimes) {
    this.mailFailedTimes = mailFailedTimes;
  }

  public void setMainPlan(String mainPlan) {
    this.mainPlan = mainPlan;
  }

  public void setMinAmountPreventLapse(String minAmountPreventLapse) {
    this.minAmountPreventLapse = minAmountPreventLapse;
  }

  public void setMinReinstate(String minReinstate) {
    this.minReinstate = minReinstate;
  }

  public void setMoneyId(String moneyId) {
    this.moneyId = moneyId;
  }

  public void setNbuInfo(Collection nbuInfo) {
    this.nbuInfo = nbuInfo;
  }

  public void setNetLoan(String netLoan) {
    this.netLoan = netLoan;
  }

  public void setNetPV(String netPV) {
    this.netPV = netPV;
  }

  public void setNetSV(String netSV) {
    this.netSV = netSV;
  }

  public void setNominee(Collection nominee) {
    this.nominee = nominee;
  }

  public void setOverduePremium(String overduePremium) {
    this.overduePremium = overduePremium;
  }

  public void setPaidupDate(String paidupDate) {
    this.paidupDate = paidupDate;
  }

  public void setPayee(Collection payee) {
    this.payee = payee;
  }

  public void setPayer(Collection payer) {
    this.payer = payer;
  }

  public void setPaymentFrequency(String paymentFrequency) {
    this.paymentFrequency = paymentFrequency;
  }

  public void setPendingCause(String pendingCause) {
    this.pendingCause = pendingCause;
  }

  public void setPlBalance(String plBalance) {
    this.plBalance = plBalance;
  }

  public void setPolicySection(String policySection) {
    this.policySection = policySection;
  }

  public void setPpLapseDate(String ppLapseDate) {
    this.ppLapseDate = ppLapseDate;
  }

  public void setPremStatus(String premStatus) {
    this.premStatus = premStatus;
  }

  public void setProposal(Collection proposal) {
    this.proposal = proposal;
  }

  public void setRenewalType(String renewalType) {
    this.renewalType = renewalType;
  }

  public void setRenewSuspense(String renewSuspense) {
    this.renewSuspense = renewSuspense;
  }

  public void setReversalReason(String reversalReason) {
    this.reversalReason = reversalReason;
  }

  public void setSa(String sa) {
    this.sa = sa;
  }

  public void setSbBalance(String sbBalance) {
    this.sbBalance = sbBalance;
  }

  public void setSendOutDate(String sendOutDate) {
    this.sendOutDate = sendOutDate;
  }

  public void setSendOutId(Long sendOutId) {
    this.sendOutId = sendOutId;
  }

  public void setServiceReceivedDate(String serviceReceivedDate) {
    this.serviceReceivedDate = serviceReceivedDate;
  }

  public void setSlBalance(String slBalance) {
    this.slBalance = slBalance;
  }

  public void setSurvivalOption(String survivalOption) {
    this.survivalOption = survivalOption;
  }

  public void setSurvivalOptTerm(Integer survivalOptTerm) {
    this.survivalOptTerm = survivalOptTerm;
  }

  public void setSuspend(String suspend) {
    this.suspend = suspend;
  }

  public void setSuspendCause(String suspendCause) {
    this.suspendCause = suspendCause;
  }

  public void setTerminateDate(String terminateDate) {
    this.terminateDate = terminateDate;
  }

  public void setTip(String tip) {
    this.tip = tip;
  }

  public void setTiv(String tiv) {
    this.tiv = tiv;
  }

  public void setTrustee(Collection trustee) {
    this.trustee = trustee;
  }

  public void setTrustPolicyIndi(String trustPolicyIndi) {
    this.trustPolicyIndi = trustPolicyIndi;
  }

  public void setUwInfo(Collection uwInfo) {
    this.uwInfo = uwInfo;
  }

  public void setValidateDate(String validateDate) {
    this.validateDate = validateDate;
  }

  public Collection getLoanAccountInfo() {
    return loanAccountInfo;
  }

  public void setLoanAccountInfo(Collection loanAccountInfo) {
    this.loanAccountInfo = loanAccountInfo;
  }

  public Collection getDepositAccountInfo() {
    return depositAccountInfo;
  }

  public void setDepositAccountInfo(Collection depositAccountInfo) {
    this.depositAccountInfo = depositAccountInfo;
  }

  public String getPayMode() {
    return payMode;
  }

  public void setPayMode(String payMode) {
    this.payMode = payMode;
  }

  public String getUtilizedAmount() {
    return utilizedAmount;
  }

  public void setUtilizedAmount(String utilizedAmount) {
    this.utilizedAmount = utilizedAmount;
  }

  public String getHolidayIndi() {
    return holidayIndi;
  }

  public void setHolidayIndi(String holidayIndi) {
    this.holidayIndi = holidayIndi;
  }

  public String getCbBalanceDate() {
    return cbBalanceDate;
  }

  public void setCbBalanceDate(String cbBalanceDate) {
    this.cbBalanceDate = cbBalanceDate;
  }

  public String getSbBalanceDate() {
    return sbBalanceDate;
  }

  public void setSbBalanceDate(String sbBalanceDate) {
    this.sbBalanceDate = sbBalanceDate;
  }

  public String getRbLastAllocDate() {
    return rbLastAllocDate;
  }

  public void setRbLastAllocDate(String rbLastAllocDate) {
    this.rbLastAllocDate = rbLastAllocDate;
  }

  public String getProposalState() {
    return proposalState;
  }

  public String getApilpDesc() {
    return apilpDesc;
  }

  public String getApilpMaxLimit() {
    return apilpMaxLimit;
  }

  public String getNbReversalReason() {
    return nbReversalReason;
  }

  public String getSendOutName() {
    return sendOutName;
  }

  public String getVoucherTagPolicy() {
    return voucherTagPolicy;
  }

  public String getProductName() {
    return productName;
  }

  public String getEndCauseType() {
    return endCauseType;
  }

  public void setProposalState(String proposalState) {
    this.proposalState = proposalState;
  }

  public void setApilpDesc(String apilpDesc) {
    this.apilpDesc = apilpDesc;
  }

  public void setApilpMaxLimit(String apilpMaxLimit) {
    this.apilpMaxLimit = apilpMaxLimit;
  }

  public void setNbReversalReason(String nbReversalReason) {
    this.nbReversalReason = nbReversalReason;
  }

  public void setSendOutName(String sendOutName) {
    this.sendOutName = sendOutName;
  }

  public void setVoucherTagPolicy(String voucherTagPolicy) {
    this.voucherTagPolicy = voucherTagPolicy;
  }

  public void setProductName(String productName) {
    this.productName = productName;
  }

  public void setEndCauseType(String endCauseType) {
    this.endCauseType = endCauseType;
  }

  public void setSuspenseLocked(String suspenseLocked) {
    this.suspenseLocked = suspenseLocked;
  }

  public String getSuspenseLocked() {
    return suspenseLocked;
  }

  public String getSpouseChildIndi() {
    return spouseChildIndi;
  }

  public void setSpouseChildIndi(String spouseChildIndi) {
    this.spouseChildIndi = spouseChildIndi;
  }

  public String getDisplayLoanValue() {
    return displayLoanValue;
  }

  public void setDisplayLoanValue(String displayLoanValue) {
    this.displayLoanValue = displayLoanValue;
  }

  public String getPolicyDuration() {
    return policyDuration;
  }

  public void setPolicyDuration(String policyDuration) {
    this.policyDuration = policyDuration;
  }

  public String getDispatchType() {
    return dispatchType;
  }

  public void setDispatchType(String dispatchType) {
    this.dispatchType = dispatchType;
  }

  public Integer getDunningLevel() {
    return dunningLevel;
  }

  public void setDunningLevel(Integer dunningLevel) {
    this.dunningLevel = dunningLevel;
  }

  public Collection dynamicTab1;

  public Collection getDynamicTab1() {
    return dynamicTab1;
  }

  public void setDynamicTab1(Collection dynamicTab1) {
    this.dynamicTab1 = dynamicTab1;
  }
	/**
	 * <p>Description :利息餘額(至系統日期) </p>
	 * <p>Created By : Kate Hsiao</p>
	 * <p>Create Time : Nov 19, 2015</p>
	 * @return
	 */
	public BigDecimal getAplUnCapitalizeInterestSys() {
		return aplUnCapitalizeInterestSys;
	}
	/**
	 * <p>Description :利息餘額(至系統日期) </p>
	 * <p>Created By : Kate Hsiao</p>
	 * <p>Create Time : Nov 19, 2015</p>
	 * @param aplUnCapitalizeInterestSys
	 */
	public void setAplUnCapitalizeInterestSys(BigDecimal aplUnCapitalizeInterestSys) {
		this.aplUnCapitalizeInterestSys = aplUnCapitalizeInterestSys;
	}
	/**
	 * <p>Description : 累計已還利息 </p>
	 * <p>Created By : Kate Hsiao</p>
	 * <p>Create Time : Nov 19, 2015</p>
	 * @return
	 */
	public BigDecimal getAplTransAmount() {
		return aplTransAmount;
	}
	/**
	 * <p>Description : 累計已還利息 </p>
	 * <p>Created By : Kate Hsiao</p>
	 * <p>Create Time : Nov 19, 2015</p>
	 * @param aplTransAmount
	 */
	public void setAplTransAmount(BigDecimal aplTransAmount) {
		this.aplTransAmount = aplTransAmount;
	}
	/**
	 * <p>Description :複利日滾本利息 </p>
	 * <p>Created By : Kate Hsiao</p>
	 * <p>Create Time : Nov 19, 2015</p>
	 * @return
	 */
	public BigDecimal getAplCapitalizeInterest() {
		return aplCapitalizeInterest;
	}
	/**
	 * <p>Description :複利日滾本利息 </p>
	 * <p>Created By : Kate Hsiao</p>
	 * <p>Create Time : Nov 19, 2015</p>
	 * @param aplCapitalizeInterest
	 */
	public void setAplCapitalizeInterest(BigDecimal aplCapitalizeInterest) {
		this.aplCapitalizeInterest = aplCapitalizeInterest;
	}
	/**
	 * <p>Description :利息餘額=aplUnCapitalizeInterestSys-aplCapitalizeInterest </p>
	 * <p>Created By : Kate Hsiao</p>
	 * <p>Create Time : Nov 19, 2015</p>
	 * @return
	 */
	public BigDecimal getAplSum() {
		return aplSum;
	}
	/**
	 * <p>Description :利息餘額=aplUnCapitalizeInterestSys-aplCapitalizeInterest </p>
	 * <p>Created By : Kate Hsiao</p>
	 * <p>Create Time : Nov 19, 2015</p>
	 * @param aplSum
	 */
	public void setAplSum(BigDecimal aplSum) {
		this.aplSum = aplSum;
	}
	/**
	 * <p>Description :利息餘額(至系統日期) </p>
	 * <p>Created By : Kate Hsiao</p>
	 * <p>Create Time : Nov 19, 2015</p>
	 * @return
	 */
	public BigDecimal getLoanUnCapitalizeInterestSys() {
		return loanUnCapitalizeInterestSys;
	}
	/**
	 * <p>Description :利息餘額(至系統日期) </p>
	 * <p>Created By : Kate Hsiao</p>
	 * <p>Create Time : Nov 19, 2015</p>
	 * @param loanUnCapitalizeInterestSys
	 */
	public void setLoanUnCapitalizeInterestSys(
			BigDecimal loanUnCapitalizeInterestSys) {
		this.loanUnCapitalizeInterestSys = loanUnCapitalizeInterestSys;
	}
	/**
	 * <p>Description :累計已還利息 </p>
	 * <p>Created By : Kate Hsiao</p>
	 * <p>Create Time : Nov 19, 2015</p>
	 * @return
	 */
	public BigDecimal getLoanTransAmount() {
		return loanTransAmount;
	}
	/**
	 * <p>Description :累計已還利息 </p>
	 * <p>Created By : Kate Hsiao</p>
	 * <p>Create Time : Nov 19, 2015</p>
	 * @param loanTransAmount
	 */
	public void setLoanTransAmount(BigDecimal loanTransAmount) {
		this.loanTransAmount = loanTransAmount;
	}
	/**
	 * <p>Description :複利日滾本利息 </p>
	 * <p>Created By : Kate Hsiao</p>
	 * <p>Create Time : Nov 19, 2015</p>
	 * @return
	 */
	public BigDecimal getLoanCapitalizeInterest() {
		return loanCapitalizeInterest;
	}
	/**
	 * <p>Description :複利日滾本利息 </p>
	 * <p>Created By : Kate Hsiao</p>
	 * <p>Create Time : Nov 19, 2015</p>
	 * @param loanCapitalizeInterest
	 */
	public void setLoanCapitalizeInterest(BigDecimal loanCapitalizeInterest) {
		this.loanCapitalizeInterest = loanCapitalizeInterest;
	}
	/**
	 * <p>Description :利息餘額=loanUnCapitalizeInterestSys-loanCapitalizeInterest </p>
	 * <p>Created By : Kate Hsiao</p>
	 * <p>Create Time : Nov 19, 2015</p>
	 * @return
	 */
	public BigDecimal getLoanSum() {
		return loanSum;
	}
	/**
	 * <p>Description :利息餘額=loanUnCapitalizeInterestSys-loanCapitalizeInterest </p>
	 * <p>Created By : Kate Hsiao</p>
	 * <p>Create Time : Nov 19, 2015</p>
	 * @param loanSum
	 */
	public void setLoanSum(BigDecimal loanSum) {
		this.loanSum = loanSum;
	}

    public HashMap getResultMap() {
        return resultMap;
    }

    public void setResultMap(HashMap resultMap) {
        this.resultMap = resultMap;
    }

    public Long getPolicyId() {
        return policyId;
    }

    public void setPolicyId(Long policyId) {
        this.policyId = policyId;
    }

    public String getPolicyCode() {
        return policyCode;
    }

    public void setPolicyCode(String policyCode) {
        this.policyCode = policyCode;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public Collection getResultCollection() {
        return resultCollection;
    }

    public void setResultCollection(Collection resultCollection) {
        this.resultCollection = resultCollection;
    }

    public String getAccessOrganIds() {
        return accessOrganIds;
    }

    public void setAccessOrganIds(String accessOrganIds) {
        this.accessOrganIds = accessOrganIds;
    }

    public String getPolicyCurrency() {
        return policyCurrency;
    }

    public void setPolicyCurrency(String policyCurrency) {
        this.policyCurrency = policyCurrency;
    }

    public String getActionType() {
        return actionType;
    }

    public void setActionType(String actionType) {
        this.actionType = actionType;
    }

	/**
	 * @return the retMsg
	 */
	public String getRetMsg() {
		return retMsg;
	}

	/**
	 * @param retMsg the retMsg to set
	 */
	public void setRetMsg(String retMsg) {
		this.retMsg = retMsg;
	}
	
}
