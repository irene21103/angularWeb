package com.ebao.ls.uw.ctrl.pub;

import com.ebao.pub.framework.GenericForm;

/**
 * 
 * <p>Title:Underwriting</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: eBaoTech Corporation</p>
 * @author Walter Huang
 * @since  Jun 27, 2005
 * @version 1.0
 */
public class UwProductDecisionForm extends GenericForm {

  private java.lang.Long underwriteId = null;

  private java.lang.Long itemId = null;

  private java.lang.Integer decisionId = null;

  private java.lang.String uwStatus = "";

  private java.lang.String decisionDesc = "";

  private java.lang.String renewDecision = "";

  private java.lang.String reuwDesc = "";

  private java.lang.String smoking = "";

  private java.lang.String smokingRelated = "";

  private java.lang.Long jobCate1 = null;

  private java.lang.Integer jobClass2 = null;

  private java.lang.Long jobCate2 = null;

  private java.lang.Integer jobClass1 = null;

  private java.lang.String insuredStatus = "";

  private java.lang.String insuredStatus2 = "";

  private java.lang.String facReinsurIndi = "";

  private java.lang.String decisionReason = "";

  private java.util.Date underwriteTime = null;

  private java.lang.Long underwriterId = null;

  private java.lang.String decisionReasonDesc = "";

  private java.lang.Long uwListId = null;

  private String decisionDesc2 = null;

  /**
   * returns the value of the underwriteId
   *
   * @return the underwriteId
   */
  public java.lang.Long getUnderwriteId() {
    return underwriteId;
  }

  /**
   * sets the value of the underwriteId
   *
   * @param underwriteId the underwriteId
   */
  public void setUnderwriteId(java.lang.Long underwriteId) {
    this.underwriteId = underwriteId;
  }

  /**
   * returns the value of the itemId
   *
   * @return the itemId
   */
  public java.lang.Long getItemId() {
    return itemId;
  }

  /**
   * sets the value of the itemId
   *
   * @param itemId the itemId
   */
  public void setItemId(java.lang.Long itemId) {
    this.itemId = itemId;
  }

  /**
   * returns the value of the decisionId
   *
   * @return the decisionId
   */
  public java.lang.Integer getDecisionId() {
    return decisionId;
  }

  /**
   * sets the value of the decisionId
   *
   * @param decisionId the decisionId
   */
  public void setDecisionId(java.lang.Integer decisionId) {
    this.decisionId = decisionId;
  }

  /**
   * returns the value of the uwStatus
   *
   * @return the uwStatus
   */
  public java.lang.String getUwStatus() {
    return uwStatus;
  }

  /**
   * sets the value of the uwStatus
   *
   * @param uwStatus the uwStatus
   */
  public void setUwStatus(java.lang.String uwStatus) {
    this.uwStatus = uwStatus;
  }

  /**
   * returns the value of the decisionDesc
   *
   * @return the decisionDesc
   */
  public java.lang.String getDecisionDesc() {
    return decisionDesc;
  }

  /**
   * sets the value of the decisionDesc
   *
   * @param decisionDesc the decisionDesc
   */
  public void setDecisionDesc(java.lang.String decisionDesc) {
    this.decisionDesc = decisionDesc;
  }

  /**
   * returns the value of the renewDecision
   *
   * @return the renewDecision
   */
  public java.lang.String getRenewDecision() {
    return renewDecision;
  }

  /**
   * sets the value of the renewDecision
   *
   * @param renewDecision the renewDecision
   */
  public void setRenewDecision(java.lang.String renewDecision) {
    this.renewDecision = renewDecision;
  }

  /**
   * returns the value of the reuwDesc
   *
   * @return the reuwDesc
   */
  public java.lang.String getReuwDesc() {
    return reuwDesc;
  }

  /**
   * sets the value of the reuwDesc
   *
   * @param reuwDesc the reuwDesc
   */
  public void setReuwDesc(java.lang.String reuwDesc) {
    this.reuwDesc = reuwDesc;
  }

  /**
   * returns the value of the smoking
   *
   * @return the smoking
   */
  public java.lang.String getSmoking() {
    return smoking;
  }

  /**
   * sets the value of the smoking
   *
   * @param smoking the smoking
   */
  public void setSmoking(java.lang.String smoking) {
    this.smoking = smoking;
  }

  /**
   * returns the value of the smokingRelated
   *
   * @return the smokingRelated
   */
  public java.lang.String getSmokingRelated() {
    return smokingRelated;
  }

  /**
   * sets the value of the smokingRelated
   *
   * @param smokingRelated the smokingRelated
   */
  public void setSmokingRelated(java.lang.String smokingRelated) {
    this.smokingRelated = smokingRelated;
  }

  /**
   * returns the value of the jobCate1
   *
   * @return the jobCate1
   */
  public java.lang.Long getJobCate1() {
    return jobCate1;
  }

  /**
   * sets the value of the jobCate1
   *
   * @param jobCate1 the jobCate1
   */
  public void setJobCate1(java.lang.Long jobCate1) {
    this.jobCate1 = jobCate1;
  }

  /**
   * returns the value of the jobClass2
   *
   * @return the jobClass2
   */
  public java.lang.Integer getJobClass2() {
    return jobClass2;
  }

  /**
   * sets the value of the jobClass2
   *
   * @param jobClass2 the jobClass2
   */
  public void setJobClass2(java.lang.Integer jobClass2) {
    this.jobClass2 = jobClass2;
  }

  /**
   * returns the value of the jobCate2
   *
   * @return the jobCate2
   */
  public java.lang.Long getJobCate2() {
    return jobCate2;
  }

  /**
   * sets the value of the jobCate2
   *
   * @param jobCate2 the jobCate2
   */
  public void setJobCate2(java.lang.Long jobCate2) {
    this.jobCate2 = jobCate2;
  }

  /**
   * returns the value of the jobClass1
   *
   * @return the jobClass1
   */
  public java.lang.Integer getJobClass1() {
    return jobClass1;
  }

  /**
   * sets the value of the jobClass1
   *
   * @param jobClass1 the jobClass1
   */
  public void setJobClass1(java.lang.Integer jobClass1) {
    this.jobClass1 = jobClass1;
  }

  /**
   * returns the value of the insuredStatus
   *
   * @return the insuredStatus
   */
  public java.lang.String getInsuredStatus() {
    return insuredStatus;
  }

  /**
   * sets the value of the insuredStatus
   *
   * @param insuredStatus the insuredStatus
   */
  public void setInsuredStatus(java.lang.String insuredStatus) {
    this.insuredStatus = insuredStatus;
  }

  /**
   * returns the value of the insuredStatus2
   *
   * @return the insuredStatus2
   */
  public java.lang.String getInsuredStatus2() {
    return insuredStatus2;
  }

  /**
   * sets the value of the insuredStatus2
   *
   * @param insuredStatus2 the insuredStatus2
   */
  public void setInsuredStatus2(java.lang.String insuredStatus2) {
    this.insuredStatus2 = insuredStatus2;
  }

  /**
   * returns the value of the facReinsurIndi
   *
   * @return the facReinsurIndi
   */
  public java.lang.String getFacReinsurIndi() {
    return facReinsurIndi;
  }

  /**
   * sets the value of the facReinsurIndi
   *
   * @param facReinsurIndi the facReinsurIndi
   */
  public void setFacReinsurIndi(java.lang.String facReinsurIndi) {
    this.facReinsurIndi = facReinsurIndi;
  }

  /**
   * returns the value of the decisionReason
   *
   * @return the decisionReason
   */
  public java.lang.String getDecisionReason() {
    return decisionReason;
  }

  /**
   * sets the value of the decisionReason
   *
   * @param decisionReason the decisionReason
   */
  public void setDecisionReason(java.lang.String decisionReason) {
    this.decisionReason = decisionReason;
  }

  /**
   * returns the value of the underwriteTime
   *
   * @return the underwriteTime
   */
  public java.util.Date getUnderwriteTime() {
    return underwriteTime;
  }

  /**
   * sets the value of the underwriteTime
   *
   * @param underwriteTime the underwriteTime
   */
  public void setUnderwriteTime(java.util.Date underwriteTime) {
    this.underwriteTime = underwriteTime;
  }

  /**
   * returns the value of the underwriterId
   *
   * @return the underwriterId
   */
  public java.lang.Long getUnderwriterId() {
    return underwriterId;
  }

  /**
   * sets the value of the underwriterId
   *
   * @param underwriterId the underwriterId
   */
  public void setUnderwriterId(java.lang.Long underwriterId) {
    this.underwriterId = underwriterId;
  }

  /**
   * returns the value of the decisionReasonDesc
   *
   * @return the decisionReasonDesc
   */
  public java.lang.String getDecisionReasonDesc() {
    return decisionReasonDesc;
  }

  /**
   * sets the value of the decisionReasonDesc
   *
   * @param decisionReasonDesc the decisionReasonDesc
   */
  public void setDecisionReasonDesc(java.lang.String decisionReasonDesc) {
    this.decisionReasonDesc = decisionReasonDesc;
  }

  /**
   * returns the value of the uwListId
   *
   * @return the uwListId
   */
  public java.lang.Long getUwListId() {
    return uwListId;
  }

  /**
   * sets the value of the uwListId
   *
   * @param uwListId the uwListId
   */
  public void setUwListId(java.lang.Long uwListId) {
    this.uwListId = uwListId;
  }

  /**
   * @return Returns the decisionDesc2.
   */
  public String getDecisionDesc2() {
    return decisionDesc2;
  }

  /**
   * @param decisionDesc2 The decisionDesc2 to set.
   */
  public void setDecisionDesc2(String decisionDesc2) {
    this.decisionDesc2 = decisionDesc2;
  }
}
