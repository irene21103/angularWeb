package com.ebao.ls.riskPrevention.bs.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.ebao.foundation.common.lang.exception.ExceptionUtils;
import com.ebao.foundation.core.logging.Log;
import com.ebao.ls.foundation.service.GenericServiceImpl;
import com.ebao.ls.riskPrevention.bs.RiskCustomerLogService;
import com.ebao.ls.riskPrevention.bs.RiskCustomerService;
import com.ebao.ls.riskPrevention.data.RiskCustomerDao;
import com.ebao.ls.riskPrevention.data.bo.RiskCustomer;
import com.ebao.ls.riskPrevention.vo.RiskCustomerVO;
import com.ebao.ls.ws.ci.aml.AmlQueryCI;
import com.ebao.pub.util.BeanUtils;
import com.ebao.pub.web.pager.Pager;

public class RiskCustomerServiceImpl extends
                GenericServiceImpl<RiskCustomerVO, RiskCustomer, RiskCustomerDao> implements RiskCustomerService {

    @Resource(name = RiskCustomerLogService.BEAN_DEFAULT)
    RiskCustomerLogService riskCustomerLogService;
    
    @Resource(name = AmlQueryCI.BEAN_DEFAULT)
    AmlQueryCI amlQueryCI;
    
    private Log log = Log.getLogger(RiskCustomerServiceImpl.class);

    @Resource(name = RiskCustomerDao.BEAN_DEFAULT)
    public void setDao(RiskCustomerDao dao) {
        super.setDao(dao);
    }

    @Override
    protected RiskCustomerVO newEntityVO() {
        return new RiskCustomerVO();
    }

    @Override
    public List<Map<String, Object>> query(RiskCustomerVO vo, Pager pager) {
        RiskCustomer bo = new RiskCustomer();
        bo.copyFromVO(vo, true, false);
        return super.getDao().query(bo, pager);
    }

    /***
     * <p>Description: 使用 CertiCode 查詢客戶風險資訊  </p>
     * @return 
     */
    public RiskCustomerVO queryByCertiCode(String certiCode) {
        if (certiCode == null) {
            return null;
        }
        RiskCustomer bo = new RiskCustomer();
        bo.setCertiCode(certiCode);
        List<RiskCustomer> boList = this.getDao().queryBO(bo, null);
        RiskCustomerVO data = null;
        if (boList != null && boList.size()>0) {
            data = new RiskCustomerVO();
            BeanUtils.copyProperties(data, boList.get(0));
        }
        return data;
    }

    public boolean isExistByCertiCode(String certiCode) {
        if (queryByCertiCode(certiCode) != null) {
            return true;
        }
        return false;
    }

    public void insert(RiskCustomerVO vo) {
        RiskCustomer bo = new RiskCustomer();
        bo.copyFromVO(vo, true, false);
        super.getDao().save(bo);
    }

    public void update(RiskCustomerVO vo) {
        RiskCustomer bo = new RiskCustomer();
        bo.copyFromVO(vo, true, false);
        super.getDao().update(bo);
    }
    
    public void saveOrUpdate(RiskCustomerVO vo) {
        RiskCustomer bo = this.getDao().findUniqueByProperty("certiCode", vo.getCertiCode());
        if(bo != null){
            bo.copyFromVO(vo, true, true);
            this.getDao().saveorUpdate(bo);
        } else {
            insert(vo);
        }
    }

    @Override
    public List<Map<String, Object>> prevQueryFromPolicyHolderByCertiCode(String certiCode) {
        return super.getDao().prevQueryByCertiCode(certiCode);
    }

    @Override
    public List<Map<String, Object>> prevQueryFromPolicyHolderByCertiCodeWithDate(String certiCode, Date validateDate) {
        return super.getDao().prevQueryByCertiCodeWithDate(certiCode, validateDate);
    }
    
    public String queryRiskLevelByCertiCode(String certiCode) {
        return super.getDao().queryRiskLevelByCertiCode(certiCode);
    }
    
    public String queryAMLRiskLevelByCertiCode(String certiCode) {
    	
    	try {
    		log.info("[RiskCustomerServiceImpl] Query AML Risk . Certi_Code : " + certiCode);
			String risk = amlQueryCI.getAmlCinoLastRisk(certiCode);
			log.info("[RiskCustomerServiceImpl] RiskLevel Result : " + risk == null ? "NULL" : risk);
			if (risk != null) {
				return risk;
			}
		} catch (Exception e) {
	       log.error("[RiskCustomerServiceImpl] Query AML Risk Error! Exception : " + ExceptionUtils.getFullStackTrace(e));
		}
    	return super.getDao().queryRiskLevelByCertiCode(certiCode);
    }

}
