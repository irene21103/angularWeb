package com.ebao.ls.uw.ctrl.info;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ebao.ls.image.vo.ImageVO;
import com.ebao.ls.uw.ds.vo.UwInsureClientVO;
import com.ebao.pub.framework.GenericForm;

/**
 * <p>Title: Module Information Andy GOD</p>
 * <p>Description: Function Description Only vegetable</p>
 * <p>Copyright: Copyright (c) 2015</p>
 * <p>Company: TGL Co., Ltd.</p>
 * <p>Create Time: Aug 20, 2015</p> 
 * @author 
 * <p>Update Time: Aug 20, 2015</p>
 * <p>Updater: Alex Cheng</p>
 * <p>Update Comments: </p>
 */
public class UwHistoryInsureForm extends GenericForm {

	private static final long serialVersionUID = -2064326112699920716L;

	private Long policyId;
	
	private String imagePolicyCode;
	
	private String imagePolicyId;

	private List<UwInsureClientVO> insureClientList = new ArrayList<UwInsureClientVO>();

	private List<ImageVO> imageList = new ArrayList<ImageVO>();
	
	private List<InsuredImageRecord> insureImageRecordList = new ArrayList<InsuredImageRecord>();
	
	private Map<Integer, String> imageMap = new HashMap<Integer, String>(); 
	

	public Long getPolicyId() {
		return policyId;
	}

	public void setPolicyId(Long policyId) {
		this.policyId = policyId;
	}

	public List<UwInsureClientVO> getInsureClientList() {
		return insureClientList;
	}

	public void setInsureClientList(List<UwInsureClientVO> insureClientList) {
		this.insureClientList = insureClientList;
	}

	public List<ImageVO> getImageList() {
		return imageList;
	}

	public void setImageList(List<ImageVO> imageList) {
		this.imageList = imageList;
	}

	public String getImagePolicyCode() {
		return imagePolicyCode;
	}

	public void setImagePolicyCode(String imagePolicyCode) {
		this.imagePolicyCode = imagePolicyCode;
	}

	public String getImagePolicyId() {
		return imagePolicyId;
	}

	public void setImagePolicyId(String imagePolicyId) {
		this.imagePolicyId = imagePolicyId;
	}

	public Map<Integer, String> getImageMap() {
		return imageMap;
	}

	public void setImageMap(Map<Integer, String> imageMap) {
		this.imageMap = imageMap;
	}

	public List<InsuredImageRecord> getInsureImageRecordList() {
		return insureImageRecordList;
	}

	public void setInsureImageRecordList(List<InsuredImageRecord> insureImageRecordList) {
		this.insureImageRecordList = insureImageRecordList;
	}

}
