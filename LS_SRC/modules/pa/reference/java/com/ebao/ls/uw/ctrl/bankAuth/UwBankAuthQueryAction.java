package com.ebao.ls.uw.ctrl.bankAuth;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.ls.arap.cash.ds.bankauth.BankAuthService;
import com.ebao.ls.arap.cash.ds.bankauth.SealRecordService;
import com.ebao.ls.escape.helper.EscapeHelper;/* Reflected XSS All Clients2018-09-11 */
import com.ebao.ls.pa.nb.ctrl.detailreg.DetailRegHelper;
import com.ebao.ls.pa.pub.bs.EddaService;
import com.ebao.ls.pa.pub.bs.PolicyBankAuthService;
import com.ebao.ls.pa.pub.bs.PolicyService;
import com.ebao.ls.pa.pub.data.bo.Policy;
import com.ebao.ls.pa.pub.data.org.PolicyDao;
import com.ebao.ls.pa.pub.vo.CoveragePremium;
import com.ebao.ls.pa.pub.vo.CoverageVO;
import com.ebao.ls.pa.pub.vo.EddaVO;
import com.ebao.ls.pa.pub.vo.PolicyBankAuthVO;
import com.ebao.ls.pa.pub.vo.PolicyVO;
import com.ebao.ls.pub.CodeCst;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.framework.GenericAction;
import com.ebao.pub.framework.GenericException;
import com.ebao.pub.i18n.lang.StringResource;

/**
 * <p>Title: 新契約</p>
 * <p>Description: 待承保件付款授權書資料維護</p>
 * <p>Copyright: Copyright (c) 2020</p>
 * <p>Company: TGL Co., Ltd.</p>
 * <p>Create Time: 2020年5月7日</p> 
 * @author 
 * <p>Update Time: 2020年5月7日</p>
 * <p>Updater: Jeremy Zhu</p>
 * <p>Update Comments: </p>
 */
public class UwBankAuthQueryAction extends GenericAction {
	
	public static final String BEAN_DEFAULT = "/uw/uwBankAuthQuery";

    @Resource(name = SealRecordService.BEAN_DEFAULT)
    private SealRecordService sealRecordDs;

    @Resource(name = PolicyService.BEAN_DEFAULT)
    private PolicyService policyService;
    
    @Resource(name = BankAuthService.BEAN_DEFAULT)
    private BankAuthService bankAuthService;
    
	@Resource(name = PolicyDao.BEAN_DEFAULT)
	private PolicyDao<Policy> policyDao;

	@Resource(name = PolicyBankAuthService.BEAN_DEFAULT)
	private PolicyBankAuthService policyBankAuthService;

	@Resource(name = DetailRegHelper.BEAN_DEFAULT)
	private DetailRegHelper detailRegHelper;
	
	@Resource(name = EddaService.BEAN_DEFAULT)
	protected EddaService paEddaService;
	
    /**
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return ActionForwrd
    */
    public ActionForward process(ActionMapping mapping, ActionForm form,
                    HttpServletRequest request, HttpServletResponse response)
                    throws GenericException {

        UwBankAuthQueryForm listForm = (UwBankAuthQueryForm) form;
        if (listForm == null || StringUtils.isEmpty(listForm.getPolicyNo())) {
        	return mapping.findForward("success");
        }
        
        String actionType = listForm.getActionType();
        String pageNo = EscapeHelper.escapeHtml(request.getParameter("pageNo"))/* Reflected XSS All Clients2018-09-11 0*/;
        String policyNo = listForm.getPolicyNo();
        Long policyId = null;
        //判斷是否符合待承保保單
		boolean flag = false;
        //判斷是否符合eDDA保單
		boolean eDDAFlag = false;
		if (StringUtils.isNotBlank(policyNo)) {
			Policy policy = policyDao.findByPolicyCode(policyNo);
			if (policy != null
					&& (policy.getProposalInfo().getProposalStatus().intValue() == CodeCst.PROPOSAL_STATUS__ACCEPTED
							|| policy.getProposalInfo().getProposalStatus()
									.intValue() == CodeCst.PROPOSAL_STATUS__CONDITIONAL)) {
				flag = true;
				policyId = policy.getPolicyId();
				listForm.setPolicyId(policyId.toString());
			}
			
			List<EddaVO> eddaList = paEddaService.findByPolicyId(policyId);
		    //判斷是否有檢附eDDA約定書
			if (eddaList.size() > 0 && CodeCst.YES_NO__YES.equals(eddaList.get(0).getEddaIndi())){
				eDDAFlag = true;
				policyId = policy.getPolicyId();
				listForm.setPolicyId(policyId.toString());
			}
		}
		if (!flag) {
            listForm.setResMessageType("alert");
            String langId = AppContext.getCurrentUser().getLangId();
            /*輸入保單號碼非待承保狀態，請確認*/
            String resMessage = StringResource.getStringData("MSG_1265288", langId);
            listForm.setResMessage(resMessage);
            request.setAttribute("actionForm", listForm); 
            return mapping.findForward("success");
		}

		if (eDDAFlag) {
            listForm.setResMessageType("alert");
            String langId = AppContext.getCurrentUser().getLangId();
            /*本件為eDDA保單，請undo回核保中處理*/
            String resMessage = StringResource.getStringData("MSG_1265530", langId);
            listForm.setResMessage(resMessage);
            request.setAttribute("actionForm", listForm); 
            return mapping.findForward("success");
		}
        
        Long pageNum = null;
        if (pageNo == null) {
            pageNum = new Long(1);
        } else {
            pageNum = Long.valueOf(pageNo);
        }

        int totalCount = sealRecordDs.queryForHistorySearch_Count(
                        listForm.getPolicyNo(), listForm.getHolderCode(),
                        listForm.getCertiCode(),listForm.getCreditCardNo(),listForm.getPaymentRadio());
        List<Map<String, Object>> voList = sealRecordDs.queryForHistorySearch(
                        listForm.getPolicyNo(), listForm.getHolderCode(),
                        listForm.getCertiCode(),listForm.getCreditCardNo(),listForm.getPaymentRadio(),
                        pageNum.intValue(),
                        (int) totalCount);

        listForm.setList1(voList);
        
        /*基本參數物件*/
        PolicyVO policyVO = policyService.load(policyId);
        
        //初始繳費方式的欄位資料
		CoverageVO coverageVO = policyVO.gitMasterCoverage();
		if (coverageVO != null) {

			CoveragePremium nextPremium = coverageVO.getNextPremium();
			CoveragePremium currentPremium = coverageVO.getCurrentPremium();
			if (currentPremium != null && currentPremium.getPaymentMethod() != null) {
				listForm.setPayMode(currentPremium.getPaymentMethod()
						.toString());
			}
			if (nextPremium != null && nextPremium.getPaymentMethod() != null) {
				/* 單一繳別或1年期商品,續期繳費方式為不可輸入,自動同首期繳費方式 */
				if (coverageVO.isSinglePay() == false) {
					listForm.setPayNext(nextPremium.getPaymentMethod()
							.toString());
				}
			}
		}
		
		//保險費墊繳,前端僅接收1-同意,2-不同意, 無關則不設定
		if (!CodeCst.YES_NO__NA.equals(policyVO.getAplPermit())) {
			listForm.setAplPermit(policyVO.getAplPermit());
		}
       
        //初始付款授權書的欄位資料
		List<PolicyBankAuthVO> bankAuthList = null;

		bankAuthList = policyBankAuthService.findByPolicyId(policyId);

		if (bankAuthList != null && bankAuthList.size() > 0) {
			detailRegHelper.initPolicyBankAuthForm(bankAuthList, listForm);
		}
        
        request.setAttribute("actionForm", listForm); // 輸出資料
        
        if ("modify".equals(actionType)) {
        	//跳轉到修改頁面
        	
        	return mapping.findForward("modify");
        }

        return mapping.findForward("success");
    }

}
