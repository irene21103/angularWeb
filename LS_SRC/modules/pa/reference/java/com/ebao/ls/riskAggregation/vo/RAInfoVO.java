package com.ebao.ls.riskAggregation.vo;

import java.util.List;

import com.ebao.pub.framework.GenericVO;

public class RAInfoVO extends GenericVO {

	private static final long serialVersionUID = 1L;

	private Long partyId;

	private String insureCetiCode;/* 契審表用 */

	private String insureName;/* 契審表用 */
	
	private String insuredCategory;/* 契審表用 */
	
	private String nbInsuredCategory;/* 契審表用 */

	private List<RiskInfoVO> riskInfoList;

	private List<RiskInfoVO> historyRiskInfoList;

	private List<AggrSaPremResultVO> aggrPremList;//BC479：新增334結果儲存至T_RMS_AGGR_SA_PREM_RESULT

	private List<PolicyHistoryVO> historyList;

	public Long getPartyId() {
		return partyId;
	}

	public void setPartyId(Long partyId) {
		this.partyId = partyId;
	}

	public List<RiskInfoVO> getRiskInfoList() {
		return riskInfoList;
	}

	public void setRiskInfoList(List<RiskInfoVO> riskInfoList) {
		this.riskInfoList = riskInfoList;
	}

	public List<RiskInfoVO> getHistoryRiskInfoList() {
		return historyRiskInfoList;
	}

	public void setHistoryRiskInfoList(List<RiskInfoVO> historyRiskInfoList) {
		this.historyRiskInfoList = historyRiskInfoList;
	}

	public List<AggrSaPremResultVO> getAggrPremList() {
		return aggrPremList;
	}

	public void setAggrPremList(List<AggrSaPremResultVO> aggrPremList) {
		this.aggrPremList = aggrPremList;
	}

	public List<PolicyHistoryVO> getHistoryList() {
		return historyList;
	}

	public void setHistoryList(List<PolicyHistoryVO> historyList) {
		this.historyList = historyList;
	}

	public String getInsureCetiCode() {
		return insureCetiCode;
	}

	public void setInsureCetiCode(String insureCetiCode) {
		this.insureCetiCode = insureCetiCode;
	}

	public String getInsureName() {
		return insureName;
	}

	public void setInsureName(String insureName) {
		this.insureName = insureName;
	}

	public String getInsuredCategory() {
		return insuredCategory;
	}

	public void setInsuredCategory(String insuredCategory) {
		this.insuredCategory = insuredCategory;
	}

	public String getNbInsuredCategory() {
		return nbInsuredCategory;
	}

	public void setNbInsuredCategory(String nbInsuredCategory) {
		this.nbInsuredCategory = nbInsuredCategory;
	}

}
