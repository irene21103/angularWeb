package com.ebao.ls.liaRoc.batch;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.transaction.UserTransaction;

import org.apache.commons.collections.CollectionUtils;

import com.ebao.foundation.module.db.hibernate.HibernateSession3;
import com.ebao.foundation.module.para.Para;
import com.ebao.foundation.module.web.action.ExceptionInfoUtils;
import com.ebao.ls.crs.constant.CRSSysCode;
import com.ebao.ls.cs.logger.ApplicationLogger;
import com.ebao.ls.cs.logger.helper.AppLogDataHelper;
import com.ebao.ls.liaRoc.LiaRocCst;
import com.ebao.ls.liaRoc.batch.vo.ResultMessage;
import com.ebao.ls.liaRoc.ci.LiaRocUpload1072020CI;
import com.ebao.ls.liaRoc.ci.LiaRocUpload2020CI;
import com.ebao.ls.liaRoc.ci.LiaRocUploadCommonService;
import com.ebao.ls.liaRoc.data.LiaRocUploadDetail2020Dao;
import com.ebao.ls.liaRoc.data.LiaRocUploadDetailDao;
import com.ebao.ls.liaRoc.vo.LiaRocUploadDetail2020WrapperVO;
import com.ebao.ls.liaRoc.vo.LiaRocUploadDetailBaseVO;
import com.ebao.ls.notification.ci.NbEventCI;
import com.ebao.ls.notification.ci.vo.RequestInfoVo;
import com.ebao.ls.notification.ci.vo.SendErrorNotificationResponseVo;
import com.ebao.ls.pa.nb.util.NBUtils;
import com.ebao.ls.pa.pub.bs.NbPolicySpecialRuleService;
import com.ebao.ls.pub.CodeCst;
import com.ebao.ls.pub.cst.NBPolicySpecialRuleType;
import com.ebao.ls.rstf.ci.liaroc20.Liaroc20CI;
import com.ebao.ls.rstf.ci.liaroc20.Liaroc20Cst;
import com.ebao.ls.rstf.ci.liaroc20.vo.internal.Liaroc20UploadRq902VO;
import com.ebao.ls.rstf.ci.liaroc20.vo.internal.Liaroc20UploadRs902VO;
import com.ebao.ls.rstf.util.RstfClientUtils;
import com.ebao.pub.batch.exe.BatchDB;
import com.ebao.pub.batch.help.BatchHelp;
import com.ebao.pub.batch.type.JobStatus;
import com.ebao.pub.util.DateUtils;
import com.ebao.pub.util.Trans;

/**
 * <h1>公會通報資料上傳 batch - 902</h1>
 * @since 2022/01/18<p>
 * @author Simon Huang
 * <p>Update Time: </p>
 * <p>Updater: </p>
 * <p>Update Comments: </p>
 */
public class LiaRocUpload902Batch {

	public static final String BEAN_DEFAULT = "liaRocUpload902Batch";

	@Resource(name = LiaRocUpload2020CI.BEAN_DEFAULT)
	protected LiaRocUpload2020CI liaRocUpload2020CI;

	@Resource(name = LiaRocUpload1072020CI.BEAN_DEFAULT)
	protected LiaRocUpload1072020CI liaRocUpload1072020CI;

	@Resource(name = LiaRocUploadDetailDao.BEAN_DEFAULT)
	protected LiaRocUploadDetailDao liaRocUploadDetailDao;

	@Resource(name = LiaRocUploadDetail2020Dao.BEAN_DEFAULT)
	protected LiaRocUploadDetail2020Dao liaRocUploadDetail2020Dao;

	@Resource(name = Liaroc20CI.BEAN_DEFAULT)
	protected Liaroc20CI liaroc20UplaodCI;

    @Resource(name = LiaRocUploadCommonService.BEAN_DEFAULT)
    private LiaRocUploadCommonService liaRocUploadCommonService;

    @Resource(name = NbPolicySpecialRuleService.BEAN_DEFAULT)
    private NbPolicySpecialRuleService nbPolicySpecialRuleService;

	@Resource(name = NbEventCI.BEAN_DEFAULT)
	private NbEventCI nbEventCI;
    
	// 公會2.0 ,902單次上傳最大筆數
	public static int LIAROC20_MAX_UPLOAD_902_SIZE = 500;

	public int mainProcess() throws Exception {

		ApplicationLogger.setSystemCode(AppLogDataHelper.SYSTEM_CODE_UNB);
		ApplicationLogger.setJobName("公會902通報排程");
		ApplicationLogger.setPolicyCode("LiarocUpload902");
		ApplicationLogger.setOptionId(DateUtils.date2String(new Date(), "yyyyMMdd HHmmss"));

		if (BatchDB.getSubId() != null) {
			ApplicationLogger.addLoggerData("[INFO]INFO]Batch Job Starting.. RunId=" + BatchHelp.getRunId());
		}

        List<String> errorList = new ArrayList<String>();
		int result = -1;
		try {
			boolean isStartSending = nbPolicySpecialRuleService.isSystemDateMatchRuleTypeStartDate(NBPolicySpecialRuleType.NB_LIAROC_902_SENDING_START_DATE);
			if(isStartSending) {
			// 承保跟通報batch取件後先將狀態壓成Ｑ, 避免動作中有資料寫入比對有誤
			liaRocUpload2020CI.updateLiaRoc902UploadStatusToQ();
			
			ResultMessage resultMessage = this.subProcess902();
			errorList.addAll(resultMessage.getErrorList());
			result = resultMessage.getExecuteResult();
			
			ApplicationLogger.addLoggerData("公會902 上傳 status=" + result);
			} else {
				ApplicationLogger.addLoggerData("公會902 尚未啟動上傳至公會");
				liaRocUpload2020CI.updateLiaRoc902UploadStatusToD();
			}

		} catch (Exception e) {
			ApplicationLogger.addLoggerData(ExceptionInfoUtils.getExceptionMsg(e));
			errorList.add(ExceptionInfoUtils.getExceptionMsg(e));
		}
	
		// PCR-507841_公會通報系統報錯新增系統警示
		if(CollectionUtils.isNotEmpty(errorList)) {
			
			// 取得要發的信箱設定
			List<String> params = Arrays.asList(
					Para.getParaValue(LiaRocCst.LIAROC_ALERT_EMAIL_ASD_TO),
					Para.getParaValue(LiaRocCst.LIAROC_ALERT_EMAIL_UNB_CC),
					Para.getParaValue(LiaRocCst.LIAROC_ALERT_EMAIL_POS_CC));
			
			List<String> emails = NBUtils.splitMultiple(params, ",");
			
			SendErrorNotificationResponseVo sendRes = nbEventCI.sendErrorNotification(
					errorList, emails, new RequestInfoVo(CRSSysCode.SYS_CODE__UNB, getClass().getSimpleName()));
			
			if (sendRes != null && sendRes.isSuccess()) {
				ApplicationLogger.addLoggerData("call nbEventCI.sendErrorNotification SUCCESS. EmailLaunchId:" + sendRes.getEmailLaunchId());
			} else {
				String errorMsg = sendRes != null ? sendRes.getErrorMessage() : "sendErrorNotification response is null";
				ApplicationLogger.addLoggerData("call nbEventCI.sendErrorNotification 發生錯誤.Error Message:" + errorMsg);
			}
		}
		
		ApplicationLogger.flush();

		return result;
	}

	protected List<LiaRocUploadDetail2020WrapperVO> getUpload902Details() {

		// 查詢新契約-承保的通報明細資料
		List<LiaRocUploadDetail2020WrapperVO> uploadList = liaRocUpload2020CI.findUploadDetailData902();

		return uploadList;
	}

	private ResultMessage subProcess902() throws Exception {
		// 批處理狀況預設為成功
		int executeResult = JobStatus.EXECUTE_SUCCESS;

        // PCR-507841_公會通報系統報錯新增系統警示
        ResultMessage result = new ResultMessage();
        List<String> errorList = new ArrayList<String>();
		
		// 記錄各通報主檔資料更新回 DB 的狀態
		Map<Long, String> uploadStatus = new HashMap<Long, String>();

		UserTransaction trans = null;
		try {

			List<? extends LiaRocUploadDetailBaseVO> liaRocUploadDetailBaseVOs = this.getUpload902Details();
			do {

				trans = Trans.getUserTransaction();
				trans.begin();

				List<LiaRocUploadDetailBaseVO> uploadList = new ArrayList<LiaRocUploadDetailBaseVO>();

				ApplicationLogger.addLoggerData("[INFO]上傳TotalDetail: " + liaRocUploadDetailBaseVOs.size());
				if (liaRocUploadDetailBaseVOs.size() > 0) {
					for (LiaRocUploadDetailBaseVO vo : liaRocUploadDetailBaseVOs) {
						uploadList.add(vo);

						// 每LIAROC20_MAX_UPLOAD_SIZE筆通報
						if (uploadList.size() >= LIAROC20_MAX_UPLOAD_902_SIZE) {
							// 取下一筆，若ulistid不一樣就先通報出去
							int index = liaRocUploadDetailBaseVOs.indexOf(vo);
							LiaRocUploadDetailBaseVO nextVO = null;
							if ((index + 1) < liaRocUploadDetailBaseVOs.size()) {
								nextVO = liaRocUploadDetailBaseVOs.get(index + 1);
							}

							if (nextVO != null && !nextVO.getUListId().equals(vo.getUListId())) {
								// 傳送通報資料
								ApplicationLogger.addLoggerData("[INFO]上傳SubDetail: " + uploadList.size());
								
								ResultMessage resultMessage = this.processUpload902(uploadList, uploadStatus);
								errorList.addAll(resultMessage.getErrorList());
								executeResult = resultMessage.getExecuteResult();
								
								
								uploadList.clear();
								uploadStatus.clear();
								HibernateSession3.currentSession().flush();
							}
						}
					}

					if (uploadList.size() > 0) {
						ApplicationLogger.addLoggerData("[INFO]上傳SubDetail: " + uploadList.size());
						// 傳送通報資料
						ResultMessage resultMessage = this.processUpload902(uploadList, uploadStatus);
						errorList.addAll(resultMessage.getErrorList());
						executeResult = resultMessage.getExecuteResult();
						
						HibernateSession3.currentSession().flush();
					}

				} else {
					ApplicationLogger.addLoggerData("[INFO]沒有要通報的資料");
				}

				trans.commit();

				uploadList.clear();
				uploadStatus.clear();
				if (liaRocUploadDetailBaseVOs.size() > 0) {
					liaRocUploadDetailBaseVOs.clear();
					liaRocUploadDetailBaseVOs = null;
					liaRocUploadDetailBaseVOs = this.getUpload902Details();
				}
			} while (liaRocUploadDetailBaseVOs.size() > 0);

			ApplicationLogger.addLoggerData("[INFO]Batch Job finished..");
		} catch (Throwable e) {
			trans.rollback();
			String message = ExceptionInfoUtils.getExceptionMsg(e);
			ApplicationLogger.addLoggerData("[ERROR]發生不可預期的錯誤, Error Message:" + message);
			executeResult = JobStatus.EXECUTE_FAILED;
			ApplicationLogger.setReturnCode(ApplicationLogger.RETURN_CODE_FAIL);
			
			errorList.add(ExceptionInfoUtils.getExceptionMsg(e));
		}

		result.setExecuteResult(executeResult);
		result.setErrorList(errorList);
		return result;
	}

	/**
	 * <p>Description : 取得通報主檔資料筆數</p>
	 * <p>Created By : Amy Hung</p>
	 * <p>Create Time : Mar 15, 2016</p>
	 * @param uploadList 通報明細資料
	 * @return 通報主檔資料筆數
	 */
	private int getUploadMasterCount(List<LiaRocUploadDetailBaseVO> uploadList) {

		List<Long> countedIds = new ArrayList<Long>();

		int count = 0;

		for (LiaRocUploadDetailBaseVO detail : uploadList) {
			if (!countedIds.contains(detail.getUListId())) {
				count++;
				countedIds.add(detail.getUListId());
			}
		}

		return count;
	}

	private ResultMessage processUpload902(List<LiaRocUploadDetailBaseVO> uploadList, Map<Long, String> uploadStatus) {

		// 預設為成功
		int executeResult = JobStatus.EXECUTE_SUCCESS;
		
        // PCR-507841_公會通報系統報錯新增系統警示
        ResultMessage result = new ResultMessage();
        List<String> errorList = new ArrayList<String>();

		// 記錄失敗的通報主檔 list_id
		ArrayList<Long> failUploadIds = new ArrayList<Long>();

		// 有資料要通報
		if (uploadList != null && uploadList.size() > 0) {

			Liaroc20UploadRs902VO uploadRsVO = null;

			int totalMasterCount = this.getUploadMasterCount(uploadList);
			
			String rqUid = RstfClientUtils.genRqUid(Liaroc20Cst.TF_BASICINFO_UPLOAD.getCode(),
					Thread.currentThread().getId());
			
			NBUtils.appLogger("[INFO]通報主檔資料筆數: " + totalMasterCount);
			NBUtils.appLogger("[INFO]通報明細資料筆數: " + uploadList.size());

			List<Liaroc20UploadRq902VO> dataRq902List = new ArrayList<Liaroc20UploadRq902VO>();

			for (LiaRocUploadDetailBaseVO uploadDetailVO : uploadList) {
				Liaroc20UploadRq902VO rq902VO = ((LiaRocUploadDetail2020WrapperVO) uploadDetailVO)
						.convertUploadRq902VO();
				if(rq902VO.getCelectronicType() != null) {
					dataRq902List.add(rq902VO);
				}
			}

			// @呼叫服務平台傳送通報資料
			if (executeResult == JobStatus.EXECUTE_SUCCESS) {

				// 失敗的明細資料筆數
				int failDetailCount = 0;

				try {

					NBUtils.appLogger("[INFO]準備呼叫服務平台....");

					// Call ESP...
					// 呼叫服務平台時，有可能它會回報失敗，則要將通報狀態設定為F
					// 以區分是 call ESP 時發生錯誤，還是 ESP 有接到，只是它回傳資料接收失敗
					int maxCount = 4;
					for (int i = 0; i < maxCount; i++) {
						try {
							String disableLiaroc20 = Para.getParaValue(LiaRocCst.PARAM_DISABLE_LIAROC20_RECEIVE);
							if (CodeCst.YES_NO__YES.equals(disableLiaroc20)) {
								uploadRsVO = new Liaroc20UploadRs902VO();
								NBUtils.appLogger("[INFO]已關閉公會2.0通報..");
							} else if(dataRq902List.size() > 0){
								uploadRsVO = liaroc20UplaodCI.doLiaroc20Upload902(rqUid, Liaroc20Cst.MN_UNB.getCode(),
										dataRq902List);
							}
							break; // 成功發送後中斷for迴圈
						} catch (Exception e) {
							if (i >= (maxCount - 1)) {
								throw e;
							} else {
								String errMsg = ExceptionInfoUtils.getExceptionMsg(e);
								if (errMsg.indexOf("Failed to access the WSDL") != -1) {
									// 發送失敗(可能是網路瞬斷)暫停10秒,retry
									NBUtils.appLogger("[ERROR]Failed to access the WSDL :次數 " + (i + 1));
									Thread.sleep(10000);
								} else {
									// 其它錯誤將訊息往外拋
									throw e;
								}
							}
						}
					}

					String statusIndi = LiaRocCst.LIAROC_UL_STATUS_SEND;

					if (uploadRsVO == null) {
						statusIndi = LiaRocCst.LIAROC_UL_STATUS_SEND_FAIL;
						NBUtils.appLogger("[INFO]呼叫服務平台發送通報有誤.. null");
						executeResult = JobStatus.EXECUTE_FAILED;
					}

					for (LiaRocUploadDetailBaseVO detail : uploadList) {
						if (!uploadStatus.containsKey(detail.getUListId())) {
							uploadStatus.put(detail.getUListId(), statusIndi);
						}
					}

					NBUtils.appLogger("[INFO]發送通報 - 主檔: " + (totalMasterCount - failUploadIds.size()) + " 筆, 明細: "
							+ (dataRq902List.size() - failDetailCount) + " 筆..");

				} catch (Exception e) {

					// 傳送到服務平台時發生錯誤，將通報狀態設定為(E:通報流程有誤)
					for (LiaRocUploadDetailBaseVO detail : uploadList) {
						if (!uploadStatus.containsKey(detail.getUListId())) {
							uploadStatus.put(detail.getUListId(), LiaRocCst.LIAROC_UL_STATUS_PROCESS_ERROR);
						}
					}
					NBUtils.appLogger("[ERROR]呼叫服務平台發送通報資料失敗，Error Message: " + e.getMessage());
					executeResult = JobStatus.EXECUTE_FAILED;
					
					errorList.add(ExceptionInfoUtils.getExceptionMsg(e));
				}

			}
			// end..

			// 將通報的狀態更新回 DB
			NBUtils.appLogger("[INFO]更新通報狀態.." + rqUid);
			liaRocUpload2020CI.updateLiaRoc902UploadStatus(uploadStatus, rqUid, new Date());

			// 回寫成功失敗結果
			if (uploadRsVO != null) {
				liaRocUploadCommonService.save902Response(rqUid, uploadRsVO);
			}
		} else {
			NBUtils.appLogger("[INFO]沒有要通報的資料..");
		}

		result.setErrorList(errorList);
		result.setExecuteResult(executeResult);
		
		return result;
	}
}
