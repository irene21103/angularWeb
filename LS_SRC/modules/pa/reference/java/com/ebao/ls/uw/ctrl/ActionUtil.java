package com.ebao.ls.uw.ctrl;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForward;

import com.ebao.ls.escape.helper.EscapeHelper;
import com.ebao.ls.pa.nb.util.NumericUtils;
import com.ebao.ls.pa.pub.ci.CoverageCI;
import com.ebao.ls.pa.pub.ref.api.prd.ProductService;
import com.ebao.ls.pa.pub.ref.vo.prd.ProductVO;
import com.ebao.ls.pa.pub.vo.CoverageVO;
import com.ebao.ls.pa.pub.vo.UnitCountWayCfgVO;
import com.ebao.ls.pub.CodeCst;
import com.ebao.ls.uw.ds.UwPolicyService;
import com.ebao.ls.uw.ds.vo.UwEmVO;
import com.ebao.ls.uw.vo.UwExtraLoadingVO;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.framework.AppException;
import com.ebao.pub.framework.ExceptionFactory;
import com.ebao.pub.framework.GenericException;
import com.ebao.pub.framework.internal.spring.DSProxy;
import com.ebao.pub.util.DateUtils;
import com.ebao.pub.util.Log;
import com.ebao.pub.util.StringUtils;

/*
 * <p>Title: GEL-UW</p>
 * <p>Description: Action Utility Class </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: eBaoTech Corporation.</p>
 * @author jason.luo
 * @version 1.0
 * @since 09.14.2004
 */

public class ActionUtil {
  // prevent initialization
  private ActionUtil() {
  }

  /**
   * Compares two date to determine which one is greater
   * 
   * @param date1 Date1
   * @param date2 Date2
   * @return boolean True - date1 is greater than date2 False - date1 is equals
   *         or less than date2
   */
  public static boolean compareDate(Date date1, Date date2) {
    date1 = DateUtils.truncateDay(date1);
    date2 = DateUtils.truncateDay(date2);
    if (date1.compareTo(date2) > 0) {
      return true;
    }
    return false;
  }

  /**
   * Get Underwrite Id
   * 
   * @param request HttpServletRequest
   * @return Underwrite Id
   */
  public static Long getUnderwriteId(HttpServletRequest request) {
    return Long.valueOf(com.ebao.ls.encode.encoder.Encode.encodeForHTML(request).getParameter(ReqParamNameConstants.POLICY_UNDERWRITEID));
  }

  /**
   * Get current underwriter's emp id
   * 
   * @return underwriter id
   * @throws GenericException Application Exception
   */
  public static Long getUnderwriterId() throws GenericException {
    Long underwriteId = Long.valueOf(AppContext.getCurrentUser().getUserId());
    if (underwriteId == null) {
      throw new AppException(20520110003L);
    }
    return underwriteId;
  }

  /**
   * @param underwriteId
   * @param roleType
   * @param originSource
   * @param partyId
   * @return
   * @throws GenericException
   */
  public static String getExitURL(Long underwriteId, String roleType,
      String originSource, Long partyId) throws GenericException {
    String exitUrl = null;
    // changed to fix defect GEL00037575. ning.qi 2008-01-09
    if ("commonQuery".equalsIgnoreCase(originSource)) {
      exitUrl = "/uw/showLastestRA.do?partyId=" + partyId;
    } else {
      exitUrl = "/uw/showRiskAggregation.do?underwriteId=" + underwriteId
          + "&roleType=" + roleType;
    }
    try {
      exitUrl = URLEncoder.encode(exitUrl, "UTF-8");
    } catch (UnsupportedEncodingException ex) {
      throw ExceptionFactory.parse(ex);
    }
    return exitUrl;
  }

  /**
   * @param path
   * @return
   */
  public static ActionForward getActionForward(String path) {
    ActionForward actionForward = new ActionForward();
    actionForward.setContextRelative(true);
    actionForward.setRedirect(true);
    actionForward.setPath(path);
    return actionForward;
  }

  /**
   * @param request
   * @param itemIds
   * @throws GenericException
   * @throws NumberFormatException
   */ 
  public static void setFacRequestList(UwPolicyService uwPolicyService, HttpServletRequest request,
			String[] itemIds) throws GenericException {
		List appFacRequstList = new ArrayList();
		// UwPolicyService uwPolicyDS =
		// DSProxy.newInstance(UwPolicyService.class);
		for (int i = 0; i < itemIds.length; i++) {
//			List requestList = uwPolicyService.findByItemId(Long
//					.parseLong(itemIds[i]));
			 List requestList = uwPolicyService.getApplyFacRequestList(Long
			 .valueOf(itemIds[i]));
			if (requestList != null && !requestList.isEmpty()) {
				appFacRequstList.addAll(requestList);
			}
		}
    request.setAttribute("applyFacList", appFacRequstList);
    EscapeHelper.escapeHtml(request).setAttribute("itemId", itemIds);
    if (request.getAttribute("uwSourceType") != null) {
      EscapeHelper.escapeHtml(request)
          .setAttribute("uwSourceType", EscapeHelper.escapeHtml(request).getAttribute("uwSourceType"));
    } else {
      EscapeHelper.escapeHtml(request)
          .setAttribute("uwSourceType", EscapeHelper.escapeHtml(request).getParameter("uwSourceType"));
    }
    if (request.getAttribute("decision") != null) {
      request.setAttribute("decisionId", request.getAttribute("decisionId"));
    } else {
      EscapeHelper.escapeHtml(request).setAttribute("decisionId", EscapeHelper.escapeHtml(request.getParameter("decisionId")));
    }
  }

  /**
   * @param req
   * @param isNewbiz
   * @param insuredId
   * @throws Exception
   * @throws AppException
   * @throws GenericException
   */
  public static UwExtraLoadingVO[] calAndSaveExtraLoading(
      HttpServletRequest req, Long preInsuredId) throws Exception {
    // try{
    String uwSourceType = EscapeHelper.escapeHtml(req.getParameter("uwSourceType"));
    boolean isNewbiz = false;
    if (CodeCst.UW_SOURCE_TYPE__NB.equals(uwSourceType)) {
      isNewbiz = true;
    }
    Long insuredId = null;
    if (req.getParameter("insured") != null) {
      insuredId = Long.valueOf(req.getParameter("insured"));
    } else {
      insuredId = Long.valueOf(0);
    }
    List extraLoadingList = getExtraLoadings(req, isNewbiz, preInsuredId);
    // if (isNewbiz) {
    // checkDuplicateLoadingType(extraLoadingList);
    // }
    UwExtraLoadingVO[] extraloadings = (UwExtraLoadingVO[]) extraLoadingList
        .toArray(new UwExtraLoadingVO[extraLoadingList.size()]);
    UwPolicyService uwPolicyDS = DSProxy.newInstance(UwPolicyService.class);
    // update the extraPrem
    uwPolicyDS.saveExtraPrem(extraloadings, getEmVO(req), preInsuredId);
    Collection rtnCol = uwPolicyDS.findUwExtraLoadingEntitis(Long.valueOf(req
        .getParameter("underwriteId")), Long.valueOf(req
        .getParameter("itemIds")), insuredId);
    return (UwExtraLoadingVO[]) rtnCol.toArray(new UwExtraLoadingVO[rtnCol
        .size()]);
    // }catch (Exception e) {
    // throw new AppException(20520120017L);
    // }
  }

  /**
   * get Emvo
   * 
   * @param req
   * @return
   * @throws Exception
   */
  private static UwEmVO getEmVO(HttpServletRequest req) throws Exception {
    Long underwriteId = Long.valueOf(req.getParameter("underwriteId"));
    Long itemId = Long.valueOf(req.getParameter("itemIds"));
    Long insuredId = null;
    if (req.getParameter("insured") != null) {
      insuredId = Long.valueOf(req.getParameter("insured"));
    } else {
      insuredId = Long.valueOf(0);
    }
    UwEmVO emVO = new UwEmVO();
    emVO.setUnderwriteId(underwriteId);
    emVO.setDecisionId(NumericUtils.parseInteger(req.getParameter("decisionId")));
    emVO.setItemId(itemId);
    // if healthFlag is true,U should update the em info on the product
    // if (healthFlag) {
    emVO.setFlag("Y");
    emVO.setInsured(insuredId);
    String advantageIND = req.getParameter("advantageIND");
    String emCI = req.getParameter("emCI");
    String emLife = req.getParameter("emLife");
    String emTPD = req.getParameter("emTPD");
    if ((advantageIND != null) && (advantageIND.length() > 0)) {
      emVO.setAdvantageIND(advantageIND);
    }
    if ((emCI != null) && (emCI.length() > 0)) {
      emVO.setEmCI(Integer.valueOf(emCI));
    }
    if ((emLife != null) && (emLife.length() > 0)) {
      emVO.setEmLife(Integer.valueOf(emLife));
    }
    if ((emTPD != null) && (emTPD.length() > 0)) {
      emVO.setEmTPD(Integer.valueOf(emTPD));
    }
    return emVO;
  }

  /**
   * get the extraLoadings
   * 
   * @param req
   * @return
   * @throws Exception
   */
  public static List getExtraLoadings(HttpServletRequest req, boolean isNewbiz,
      Long insuredId) {
    // assembly extraloading object from page parameters.
    List extraLoadingList = new ArrayList();
    String[] isOn = req.getParameterValues("isOn");
    if (isOn == null || isOn.length == 0) {
      return extraLoadingList;
    }
    String commenceDate = req.getParameter("commenceDate"); //險種生效日
    Long underwriteId = Long.valueOf(req.getParameter("underwriteId"));
    Long itemId = Long.valueOf(req.getParameter("itemIds"));
    if (Log.isDebugEnabled(ActionUtil.class)) {
    }
    String[] startDate = req.getParameterValues("startDate");  //險種生效日
    String[] endDate = req.getParameterValues("endDate");   //險種生效日+加費年度-1日
    String[] emValue = req.getParameterValues("emValue"); //EM 評分率
    
    
    /*
	1	保費乘以百分比(職加)
	2	保費乘以百分比(投資型職加)
	3	EM百分比或每千元保額加費金額(弱體加費)
	4	EM百分比或每千元保額加費金額(投資型弱體加費)
    */
    String[] extraArith = req.getParameterValues("extraArith"); //加費選項(計算方式)
    String[] extraPara = req.getParameterValues("extraPara");  //每1000保额
    String[] extraType = req.getParameterValues("extraType");  //加費類型(弱體/職加)
    String[] duration = req.getParameterValues("duration"); //加費年度
    String[] extraPrem = req.getParameterValues("extraPrem"); //加費金額
    String[] callIND = req.getParameterValues("callIND");
    String[] reason = req.getParameterValues("reason");
    String[] emLife = req.getParameterValues("emLife");
    String[] emTPD = req.getParameterValues("emTPD");
    String[] emCI = req.getParameterValues("emCI");
    String[] extraPeriodType = req.getParameterValues("extraPeriodType");
    if (isNewbiz) {
      int length = extraArith.length;
      startDate = new String[length];
      // endDate = new String[length];
      for (int i = 0; i < startDate.length; i++) {
        startDate[i] = commenceDate;
      }
    }
    UwExtraLoadingVO[] extraLoadingVOs = new UwExtraLoadingVO[isOn.length];
    for (int i = 0; i < isOn.length; i++) {
      UwExtraLoadingVO extraLoadingVO = new UwExtraLoadingVO();
      if (!"Y".equals(isOn[i])) {
        continue;
      }
      Date startDay = DateUtils.toDate(startDate[i]);
      Date endDay;
      if (isNewbiz) {
        // modified by hanzhong.yan for cq:GEL00029962 2007/9/7
        // endDay = DateUtils.addDay(DateUtils.addYear(startDay,
        // Integer.valueOf(duration[i]).intValue()), -1);
        if (Integer.valueOf(duration[i]).intValue() == 100)
          endDay = DateUtils.toDate("09/09/9999", "dd/MM/yyyy");
        else
          endDay = DateUtils.addDay(DateUtils.addYear(startDay, Integer
              .valueOf(duration[i]).intValue()), -1);
      } else {
        endDay = DateUtils.toDate(endDate[i]);
      }
      extraLoadingVO.setEndDate(endDay);
      extraLoadingVO.setStartDate(startDay);
      extraLoadingVO.setExtraArith(extraArith[i]);
      // modified by hanzhong.yan for cq:GEL00039090 2008/2/22
      // BigDecimal intExtraFactor = new BigDecimal(0);
      BigDecimal intExtraFactor = null;
      if (i < extraPara.length && !StringUtils.isNullOrEmpty(extraPara[i])) {
        intExtraFactor = new BigDecimal(extraPara[i]);
      }
      extraLoadingVO.setExtraPara(intExtraFactor);
      // Integer intEmValue = Integer.valueOf(0);
      if (!StringUtils.isNullOrEmpty(emValue[i])) {
        // intEmValue = Integer.valueOf(emValue[i]);
        extraLoadingVO.setEmValue(Integer.valueOf(emValue[i]));
      }
      extraLoadingVO.setExtraType(extraType[i]);
      if (extraPrem[i] != null && extraPrem[i].length() > 0) {
        extraLoadingVO.setExtraPrem(new BigDecimal(extraPrem[i]));
      }
      if (reason[i] != null) {
        extraLoadingVO.setReason(reason[i]);
      }
      if (callIND[i] != null && callIND[i].length() > 0) {
        extraLoadingVO.setReCalcIndi(callIND[i]);
      } else {
        extraLoadingVO.setReCalcIndi("N");
      }
      if (emLife[i] != null && emLife[i].length() > 0) {
        extraLoadingVO.setEmLife(Integer.valueOf(emLife[i]));
      }
      if (emTPD[i] != null && emTPD[i].length() > 0) {
        extraLoadingVO.setEmTpd(Integer.valueOf(emTPD[i]));
      }
      if (emCI[i] != null && emCI[i].length() > 0) {
        extraLoadingVO.setEmCi(Integer.valueOf(emCI[i]));
      }
      if (extraPeriodType[i] != null && extraPeriodType[i].length() > 0) {
          extraLoadingVO.setExtraPeriodType(extraPeriodType[i]);
      }
      extraLoadingVO.setUnderwriteId(underwriteId);
      extraLoadingVO.setItemId(itemId);
      extraLoadingVO.setPolicyId(Long.valueOf(req.getParameter("policyId")));
      extraLoadingVO.setUnderwriterId(Long.valueOf(AppContext.getCurrentUser()
          .getUserId()));
      extraLoadingVO.setInsuredId(insuredId);
      if (Log.isDebugEnabled(ActionUtil.class)) {
      }
      extraLoadingVOs[i] = extraLoadingVO;
      if (extraLoadingVO.getExtraArith() == null
          || extraLoadingVO.getExtraArith().trim().length() == 0) {
        continue;
      }
      extraLoadingList.add(extraLoadingVO);
    }
    return extraLoadingList;
  }

  /**
   * Translates standard life indicator from db schema value(1 or 0) to page
   * indicator value(Y or N)
   * 
   * @param stdLifeIndi standard life indicator db value
   * @return standard life indicator page value
   * @author jason.luo
   * @since 10.14.2004
   */
  public static String decodeStdLifeIndiFromDB(String stdLifeIndi) {
    if (null == stdLifeIndi || "".equals(stdLifeIndi)
        || "Y".equals(stdLifeIndi) || "1".equals(stdLifeIndi)) {
      return "Y";
    } else {
      return "N";
    }
  }

  public static Boolean getUnitIndicator(Long itemId, Long productId, Date productVersionDate)
      throws GenericException {
    ProductService productService = DSProxy.newInstance(ProductService.class);
    ProductVO productVO = productService.getProduct(productId,productVersionDate);
    String unitFlag = productVO.getUnitFlag();
    CoverageCI coverageCI = (CoverageCI) DSProxy
        .newInstanceByBeanName("paCoverageCI");
    CoverageVO coverage = coverageCI.retrieveByItemId(itemId);
    if (coverage.getCurrentPremium() != null) {
      UnitCountWayCfgVO cfgVO = coverageCI.getCountWayCfg(unitFlag, coverage
          .getCurrentPremium().getCountWay());
      if (cfgVO != null && "Y".equals(cfgVO.getUnitIndi())) {
        return Boolean.valueOf(true);
      }
    }
    return Boolean.valueOf(false);
  }
}
