package com.ebao.ls.liaRoc.ds;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.ebao.ls.liaRoc.bo.LiaRocDownload;
import com.ebao.ls.liaRoc.bo.LiaRocDownloadVO;
import com.ebao.ls.liaRoc.data.LiaRocDownloadDao;
import com.ebao.ls.pa.nb.util.NBUtils;
import com.ebao.ls.pa.pub.bs.InsuredService;
import com.ebao.ls.pa.pub.vo.InsuredVO;
import com.ebao.ls.uw.ds.vo.LiaRocPaidInjuryMedicalDetailVO;

public class LiaRocDownloadServiceImpl implements LiaRocDownloadService {

    @Resource(name = LiaRocDownloadDao.BEAN_DEFAULT)
    private LiaRocDownloadDao<LiaRocDownload> liaRocDownloadDao;
    
    @Resource(name = com.ebao.ls.pa.nb.bs.rule.CoverageInsuredService.BEAN_DEFAULT)
	public com.ebao.ls.pa.nb.bs.rule.CoverageInsuredService coverageInsuredService;
    
    @Resource(name = InsuredService.BEAN_DEFAULT)
	public InsuredService insuredService;
    
    @Override
    public void save(LiaRocDownloadVO liaRocDownloadVO) {
        LiaRocDownload liaRocDownload = new LiaRocDownload();
        liaRocDownload.copyFromVO(liaRocDownloadVO, true, true);
        liaRocDownloadDao.save(liaRocDownload);
    }

    /*
     * <b>已在LiarocDownloadCIProxy中分流 </b>
     */
	@Override
    public List<Map<String, Object>> findGroupLiaRocDetailData(String certiCode, String type, Long policyId) {

        // 查詢承保/收件之團險通報統計資料
    	List<Map<String, Object>> resultAll = new ArrayList<Map<String, Object>>() ;
    	// 要將查詢到之資料區分自費(Y)及公費(N)
        List<Map<String, Object>> resultN = liaRocDownloadDao.findGroupLiaRocDetailData(certiCode,type,policyId, "N");
        for (Map<String, Object> map:resultN){
        	map.put("NONSELFPAY", "Y");
        	map.put("SELFPAY", "N");
        	map.put("INDEX", type);
        }
        List<Map<String, Object>> resultY = liaRocDownloadDao.findGroupLiaRocDetailData(certiCode,type,policyId, "Y");
        for (Map<String, Object> map:resultY){
        	map.put("NONSELFPAY", "N");
        	map.put("SELFPAY", "Y");
        	map.put("INDEX", type);
        }
        
        resultAll.addAll(resultN);
        resultAll.addAll(resultY);
        
        return resultAll;

    }
	
	/*
     * <b>已在LiarocDownloadCIProxy中分流 </b>
     */
    @Override
    public Map<String, Long> getOtherTotalAnnPremByPolicyId(Long policyId) {
    	Map<String, Long> annPremMap = new HashMap<String, Long>();
    	List<InsuredVO> insuredVOs = insuredService.findByPolicyId(policyId);
    	for(InsuredVO insuredVO : insuredVOs) {
    		Long total = liaRocDownloadDao.queryIssueAnnuPrem(insuredVO.getCertiCode(), null, policyId)
    			+ liaRocDownloadDao.queryReceiveAnnuPrem(insuredVO.getCertiCode(), null, policyId);	
    		annPremMap.put(insuredVO.getPartyId().toString(), total);

    	}
    	return annPremMap;
    }

    /*
     * <b>已在LiarocDownloadCIProxy中分流 </b>
     */
    @Override
    public Map<String, Long> getCompTotalAnnPremByPolicyId(Long policyId) {
    	Map<String, Long> annPremMap = new HashMap<String, Long>();
    	List<InsuredVO> insuredVOs = insuredService.findByPolicyId(policyId);
    	Long policyChgId = null;
    	for(InsuredVO insuredVO : insuredVOs) {
    		BigDecimal annualPremOurCompany = coverageInsuredService.getAnnualPremOurCompany(insuredVO.getCertiCode(),policyId, policyChgId);
    		
    	   	//RTC-217681-以小數點後2位4捨5入至整數
    		annualPremOurCompany = NBUtils.specialScale(annualPremOurCompany, 2, 0, RoundingMode.HALF_UP);
    	
    		annPremMap.put(insuredVO.getPartyId().toString(), annualPremOurCompany.longValue());
    	}

        return  annPremMap;
    }
    
    /*
     * <b>已在LiarocDownloadCIProxy中分流 </b>
     */
	@Override
	public List<LiaRocPaidInjuryMedicalDetailVO> findLiarocPaidInjuryMedicalDetailInfo(Long policyId) {

		List<LiaRocPaidInjuryMedicalDetailVO> liaRocPaidInjuryMedicalDetailVoList = new ArrayList<LiaRocPaidInjuryMedicalDetailVO>();
		LiaRocPaidInjuryMedicalDetailVO vo = null;
		List<Map<String, Object>> detailMapList = null;
		
		List<Map<String, Object>> listMap = liaRocDownloadDao.getLiarocPaidInjuryMedicalDetailInfo(policyId);
		
		for(Map<String, Object> map :listMap) {
			
			String insuredName = (String) map.get("INSURED_NAME");
			String certiCode = (String) map.get("CERTI_CODE");
			
			if( liaRocPaidInjuryMedicalDetailVoList.isEmpty() ) { //第一筆資料
				
				vo = new LiaRocPaidInjuryMedicalDetailVO(); 
				vo.setCustomerName(insuredName);
				vo.setCertiCode(certiCode);
				detailMapList = new ArrayList<Map<String,Object>>();
				detailMapList.add(map);
				vo.setLiaRocPaidInjuryMedicalDetailListMap(detailMapList);
				liaRocPaidInjuryMedicalDetailVoList.add(vo);
				
			}else {
				
				boolean isExist = Boolean.FALSE;
				for( LiaRocPaidInjuryMedicalDetailVO lpmdVo : liaRocPaidInjuryMedicalDetailVoList) {
					if( insuredName.equals(lpmdVo.getCustomerName()) && certiCode.equals(lpmdVo.getCertiCode())) {	
						lpmdVo.getLiaRocPaidInjuryMedicalDetailListMap().add(map);
						isExist = Boolean.TRUE;
						break;
					}
				}
				
				if( !isExist ) {
					vo = new LiaRocPaidInjuryMedicalDetailVO(); 
					vo.setCustomerName(insuredName);
					vo.setCertiCode(certiCode);
					detailMapList = new ArrayList<Map<String,Object>>();
					detailMapList.add(map);
					vo.setLiaRocPaidInjuryMedicalDetailListMap(detailMapList);
					liaRocPaidInjuryMedicalDetailVoList.add(vo);
				}
				
			}
		}
		return liaRocPaidInjuryMedicalDetailVoList;
	}
}
