package com.ebao.ls.uw.ctrl.decision;

import com.ebao.pub.framework.GenericForm;

/**
 * 
 * <p>Title:Underwriting</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: eBaoTech Corporation</p>
 * @author mingchun.shi
 * @since  2005-7-11
 * @version 1.0
 */
public class ApplyFacRequestForm extends GenericForm {
  private String listId = null;
  private String enterStartDate = null;
  private String enterEndDate = null;
  private String facRequest = null;
  private String[] itemIds = null;
  /**
   * @return enterEndDate
   */
  public String getEnterEndDate() {
    return enterEndDate;
  }
  /**
   * @param enterEndDate will be set enterEndDate
   */
  public void setEnterEndDate(String enterEndDate) {
    this.enterEndDate = enterEndDate;
  }
  /**
   * @return enterStartDate
   */
  public String getEnterStartDate() {
    return enterStartDate;
  }
  /**
   * @param enterStartDate will be set enterStartDate
   */
  public void setEnterStartDate(String enterStartDate) {
    this.enterStartDate = enterStartDate;
  }
  /**
   * @return facRequest
   */
  public String getFacRequest() {
    return facRequest;
  }
  /**
   * @param facRequest will be set facRequest
   */
  public void setFacRequest(String facRequest) {
    this.facRequest = facRequest;
  }

  /**
   * @return itemId
   */
  public String[] getItemIds() {
    return itemIds;
  }
  /**
   * @param itemId will be set itemId
   */
  public void setItemIds(String[] itemId) {
    this.itemIds = itemId;
  }
  /**
   * @return listId
   */
  public String getListId() {
    return listId;
  }
  /**
   * @param listId will be set listId
   */
  public void setListId(String listId) {
    this.listId = listId;
  }

}
