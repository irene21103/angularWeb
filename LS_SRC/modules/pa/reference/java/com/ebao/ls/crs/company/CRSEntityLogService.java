package com.ebao.ls.crs.company;

import java.util.List;

import com.ebao.ls.crs.vo.CRSEntityLogVO;
import com.ebao.ls.foundation.service.GenericService;

public interface CRSEntityLogService extends GenericService<CRSEntityLogVO> {
	public final static String BEAN_DEFAULT = "crsEntityLogService";

	public List<CRSEntityLogVO> findByPartyLogId(Long partyLogId, boolean loadOtherTable);
	
    public List<CRSEntityLogVO> findByChangeId(Long changeId);

	//public abstract CRSEntityLogVO syncSave(CRSEntityLogVO logVO) throws GenericException;
}
