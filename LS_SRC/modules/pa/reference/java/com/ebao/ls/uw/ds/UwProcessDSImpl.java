package com.ebao.ls.uw.ds;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import com.ebao.foundation.app.sys.sysmgmt.ds.SysMgmtDSLocator;
import com.ebao.foundation.app.sys.sysmgmt.ds.UserVO;
import com.ebao.foundation.common.context.AppUserContext;
import com.ebao.foundation.module.db.hibernate.HibernateSession3;
import com.ebao.ls.chl.service.AgentService;
import com.ebao.ls.clm.model.ClaimUWInfo;
import com.ebao.ls.clm.service.ClaimService;
import com.ebao.ls.cmu.event.ClaimUwCompleted;
import com.ebao.ls.cmu.event.ClaimUwEscalated;
import com.ebao.ls.cmu.event.CsUwEscalated;
import com.ebao.ls.cmu.event.NbUwEscalated;
import com.ebao.ls.cmu.service.DocumentService;
import com.ebao.ls.crt.ci.ReversalCI;
import com.ebao.ls.cs.ci.CSCI;
import com.ebao.ls.cs.ci.vo.PolicyChangeCIVO;
import com.ebao.ls.cs.commonflow.data.TPolicyChangeDelegate;
import com.ebao.ls.cs.commonflow.data.bo.AlterationItem;
import com.ebao.ls.cs.commonflow.ds.ApplicationService;
import com.ebao.ls.cs.pub.bo.CommonDumper;
import com.ebao.ls.foundation.util.BeanConvertor;
import com.ebao.ls.leave.service.UserLeaveManagerService;
import com.ebao.ls.pa.nb.bs.DisabilityCancelDetailService;
import com.ebao.ls.pa.nb.bs.ProposalProcessService;
import com.ebao.ls.pa.nb.bs.impl.sp.ProposalServiceSp;
import com.ebao.ls.pa.nb.bs.rule.ProposalRuleResultService;
import com.ebao.ls.pa.nb.ci.ProposalCI;
import com.ebao.ls.pa.nb.util.NBUtils;
import com.ebao.ls.pa.nb.vo.ProposalRuleResultVO;
import com.ebao.ls.pa.pub.bs.InsuredDisabilityListService;
import com.ebao.ls.pa.pub.bs.InsuredService;
import com.ebao.ls.pa.pub.bs.PolicyExclusionService;
import com.ebao.ls.pa.pub.bs.PolicyHolderService;
import com.ebao.ls.pa.pub.bs.coverage.CoverageService;
import com.ebao.ls.pa.pub.ci.CoverageCI;
import com.ebao.ls.pa.pub.ci.PolicyCI;
import com.ebao.ls.pa.pub.ci.PolicyRoleCI;
import com.ebao.ls.pa.pub.data.bo.Insured;
import com.ebao.ls.pa.pub.data.bo.InsuredDisabilityIcf;
import com.ebao.ls.pa.pub.data.bo.InsuredDisabilityList;
import com.ebao.ls.pa.pub.data.bo.Policy;
import com.ebao.ls.pa.pub.data.bo.PolicyExclusionCX;
import com.ebao.ls.pa.pub.data.cx.GenericCXDao;
import com.ebao.ls.pa.pub.data.org.InsuredDao;
import com.ebao.ls.pa.pub.data.org.PolicyDao;
import com.ebao.ls.pa.pub.ref.api.prd.ProductService;
import com.ebao.ls.pa.pub.ref.vo.prd.ProductVO;
import com.ebao.ls.pa.pub.service.PolicyService;
import com.ebao.ls.pa.pub.vo.CoverageInsuredVO;
import com.ebao.ls.pa.pub.vo.CoverageVO;
import com.ebao.ls.pa.pub.vo.DisabilityInfoVO;
import com.ebao.ls.pa.pub.vo.ExtraPremVO;
import com.ebao.ls.pa.pub.vo.InsuredVO;
import com.ebao.ls.pa.pub.vo.LiabilityReduceVO;
import com.ebao.ls.pa.pub.vo.PayPlanVO;
import com.ebao.ls.pa.pub.vo.PolicyExclusionVO;
import com.ebao.ls.pa.pub.vo.PolicyVO;
import com.ebao.ls.pa.pub.vo.SingleTopupVO;
import com.ebao.ls.pty.ci.CustomerCI;
import com.ebao.ls.pty.ds.DeptService;
import com.ebao.ls.pub.CodeCst;
import com.ebao.ls.pub.util.BizUtils;
import com.ebao.ls.pub.workflow.WfContextLs;
import com.ebao.ls.riskAggregation.bs.RiskAggregationService;
import com.ebao.ls.riskPrevention.ci.RiskCustomerCI;
import com.ebao.ls.sc.vo.AgentChlVO;
import com.ebao.ls.uw.bo.UwDecisionConfig;
import com.ebao.ls.uw.data.TUwExtraPremDelegate;
import com.ebao.ls.uw.data.TUwInsuredDisabilityDelegate;
import com.ebao.ls.uw.data.TUwInsuredDisabilityIcfDelegate;
import com.ebao.ls.uw.data.TUwInsuredListDelegate;
import com.ebao.ls.uw.data.TUwPolicyDelegate;
import com.ebao.ls.uw.data.TUwProductDelegate;
import com.ebao.ls.uw.data.TUwReduceDelegate;
import com.ebao.ls.uw.data.bo.UwExtraLoading;
import com.ebao.ls.uw.data.bo.UwInsuredDisability;
import com.ebao.ls.uw.data.bo.UwInsuredDisabilityIcf;
import com.ebao.ls.uw.data.bo.UwLien;
import com.ebao.ls.uw.data.bo.UwLifeInsured;
import com.ebao.ls.uw.data.bo.UwPolicy;
import com.ebao.ls.uw.data.bo.UwProduct;
import com.ebao.ls.uw.data.query.UwQueryDao;
import com.ebao.ls.uw.ds.constant.CompleteUwProcessStatusConstants;
import com.ebao.ls.uw.ds.constant.ProposalStatusConstants;
import com.ebao.ls.uw.ds.constant.UwDecisionConstants;
import com.ebao.ls.uw.ds.constant.UwExceptionConstants;
import com.ebao.ls.uw.ds.constant.UwExtraLoadingTypeConstants;
import com.ebao.ls.uw.ds.constant.UwSourceTypeConstants;
import com.ebao.ls.uw.ds.constant.UwStatusConstants;
import com.ebao.ls.uw.ds.sp.CompleteUWProcessSp;
import com.ebao.ls.uw.ds.sp.DebitUWMedicalAgentSP;
import com.ebao.ls.uw.ds.vo.UwInsuredVO;
import com.ebao.ls.uw.esc.role.TransferLevelTypeValidate;
import com.ebao.ls.uw.util.Utils;
import com.ebao.ls.uw.vo.UwCommentVO;
import com.ebao.ls.uw.vo.UwConditionVO;
import com.ebao.ls.uw.vo.UwEndorsementVO;
import com.ebao.ls.uw.vo.UwExclusionVO;
import com.ebao.ls.uw.vo.UwExtraLoadingVO;
import com.ebao.ls.uw.vo.UwLienVO;
import com.ebao.ls.uw.vo.UwLifeInsuredVO;
import com.ebao.ls.uw.vo.UwPolicyVO;
import com.ebao.ls.uw.vo.UwProductVO;
import com.ebao.pub.Cst;
import com.ebao.pub.annotation.DataModeChgModifyPoint;
import com.ebao.pub.annotation.PAPubAPIUpdate;
import com.ebao.pub.event.ApplicationEvent;
import com.ebao.pub.event.ApplicationEventVO;
import com.ebao.pub.event.EventService;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.framework.AppException;
import com.ebao.pub.framework.ExceptionFactory;
import com.ebao.pub.framework.GenericException;
import com.ebao.pub.framework.SysException;
import com.ebao.pub.util.BeanUtils;
import com.ebao.pub.util.Log;
import com.ebao.pub.util.StringUtils;
import com.ebao.pub.workflow.wfmodel.WfTaskInstance;
import com.ebao.pub.workflow.worklist.web.action.WorkListConstant;

/**
 * Title:Underwriting Description: Copyright: Copyright (c) 2004 Company:
 * eBaoTech Corporation
 * 
 * @author walter.huang
 * @since Jun 28, 2005
 * @version 1.0
 */
public class UwProcessDSImpl implements UwProcessService {
	
	private Logger logger = Logger.getLogger(UwProcessDSImpl.class);
	
    protected static final String JETISSUEDECISION = "decision";

    protected static final String DOCUMENT = "document";

    private static final Long ESCALATED_TO_NEXT_LEVEL = Long.valueOf(15);

    private static final String ADD_HOC_CONDITION_0015 = "0015";

    protected static final int PRODUCT_OPTION_LIEN = 1;

    protected static final int PRODUCT_OPTION_RESTRICT_COVER = 2;

    protected static final int PRODUCT_OPTION_HEALTH = 3;

    protected static final int PRODUCT_OPTION_OCCUPATION = 4;

    protected static final int PRODUCT_OPTION_AVOCATION = 5;

    protected static final int PRODUCT_OPTION_RESIDENT = 6;

    protected static final int PRODUCT_OPTION_AVIATION = 7;

    protected static final int PRODUCT_OPTION_OTHERS = 8;

    private static final int PRODUCT_OPTION_LOADING_ALL = 9;

    /* 核保綜合意見欄保全版*/
    public static final long UW_COMMENT_VERSION_TYPE_CS = 4;

    @Resource(name = UwCommentService.BEAN_DEFAULT)
    private UwCommentService uwCommentDS;

    @Resource(name = UwInsuredDisabilityService.BEAN_DEFAULT)
    private UwInsuredDisabilityService uwInsuredDisbilityDS;

	@Resource(name = TUwInsuredDisabilityIcfDelegate.BEAN_DEFAULT)
	private TUwInsuredDisabilityIcfDelegate uwInsuredDisabilityIcfDao;
	

    /**
     * Process CS transaction
     * 
     * @param policyId Policy Id
     * @throws GenericException
     * @author jason.luo
     * @since 11.17.2004
     * @version 1.0
     */
    @PAPubAPIUpdate("update for loadPolicyByPolicyId")
    public void processCSTransaction(Long policyId, Long changeId,
                    Integer csTransactions[], Long uwProposerId, Long items[])
                    throws GenericException {
        if (null == policyId || null == changeId || null == csTransactions
                        || csTransactions.length == 0) {
            throw new IllegalArgumentException(
                            "PolicyId and changeId and csTransaction can't be null.");
        }
        // get policy info from t_contract_master by NB Interface
        PolicyVO policyVO = retrievePolicyValue(policyId);
        // get product info
        List<CoverageVO> policyProducts = retrieveProductValues(policyId);
        // generate uw policy & product and etc.
        UwPolicyVO policyValue = convertPolicyValue(policyVO,
                        UwSourceTypeConstants.CS, uwProposerId, policyProducts,
                        items);
        // add supplementary value
        policyValue.setChangeId(changeId);
        // get manual uw indicator from the latest uw
        String manualUPCIndicator = getUwQueryDao().getCSManualUPCIndicator(
                        policyId);
        policyValue.setManualUwIndi(manualUPCIndicator);
        // create underwriting policy
        //IR 363066 修正受理次標加費批註加上其他，進入核保後批註與照會單顯示問題
        HashMap<Integer, String> csTransactionMap = new HashMap<Integer, String>();
        for (Integer csTransaction : csTransactions) {
        	if(!csTransactionMap.containsKey(csTransaction)){
            UwPolicyVO uwPolicyVO = new UwPolicyVO();
            try {
                BeanUtils.copyProperties(uwPolicyVO, policyValue);
            } catch (Exception e) {
                throw ExceptionFactory.parse(e);
            }
            uwPolicyVO.setCsTransaction(csTransaction);
            uwPolicyVO.setUwSourceType(getCsSrcType(csTransaction));
			// 2017-05-01 Kate 當次標加費完成，進核保時使用原本的T_UW_EXTRA_PREM 帶回T_UW_EXTRA_PREM；
			// 原本的T_UW_exclusion 帶回T_UW_exclusion；
			if(csTransaction == CodeCst.SERVICE__CHANGE_EXTRA_PREMIUM){
				csTransactionMap.put(CodeCst.SERVICE__CHANGE_EXTRA_PREMIUM, "165");
				List<AlterationItem> alterationItemList = TPolicyChangeDelegate.findByMasterChgId(changeId);
				HashMap<Integer, String> serviceMap = new HashMap<Integer, String>();
				for (AlterationItem alterationItem : alterationItemList) {
					if(alterationItem.getServiceId() == CodeCst.SERVICE__CHANGE_EXTRA_PREMIUM
							&& alterationItem.getPolicyChgStatus() == CodeCst.POLICY_CHG_STATUS__COMPLETED){
						serviceMap.put(CodeCst.SERVICE__CHANGE_EXTRA_PREMIUM, "165");
						Long preUnderwriteId = uwPolicyDao.findUWIdByPolicyIdAndChangeId(policyId, changeId);
						reloadUwExtraLoading(preUnderwriteId, uwPolicyVO);
						reloadUwExclusion(preUnderwriteId, uwPolicyVO);
					}
				}
			}
            Long underwriteId = createUwPolicy(uwPolicyVO);
            com.ebao.ls.pa.pub.vo.PolicyVO policy = policyService
                            .loadPolicyByPolicyId(policyId);
            // 身心障礙 同步 T_UW_INSURED_DISABILITY
            Collection<UwLifeInsuredVO> uwInsuredList = uwPolicyDS.findUwLifeInsuredEntitis(underwriteId);
            this.createUwInsuredDisability(uwInsuredList);
            
            //風險累算時保全應提供policyChgId才能正確處理
            raService.calcRA(policyId, applicationService.getPolicyChgIdForCalcRA(changeId));
            raService.initPolicyRiskLevel(underwriteId, policy,
                            uwPolicyVO.getUwSourceType(),
                            CodeCst.PRODUCT_DECISION__ACCEPTED);
        }
        }
    }

    /**
     * Only when cs transaction = reinstatement,it's type will be cs-re
     * 
     * @param csTransactions
     * @param policyValue
     * @param i
     */
    protected String getCsSrcType(Integer csTransaction) {
        String uwSourceType = CodeCst.UW_SOURCE_TYPE__CS;
        if (CodeCst.SERVICE__REINSTATEMENT == csTransaction.intValue()
                        || CodeCst.SERVICE__SPECIAL_REVIVAL == csTransaction
                                        .intValue()) {
            uwSourceType = CodeCst.UW_SOURCE_TYPE__CS_RE;
        }
        return uwSourceType;
    }

    /**
     * Process claim transaction return the underwriteId
     * 
     * @param policyId Policy Id
     * @param changeId change Id
     * @param uwProposerId uwProposer Id
     * @return underwriteId *@throws GenericException Application Exception
     * @author yixing.lu
     * @since 19.04.2005
     * @version 1.0
     */
    @PAPubAPIUpdate("update for loadPolicyByPolicyId")
    public Long processClaimTransaction(Long policyId, Long changeId,
                    Long uwProposerId, String uwSourceType)
                    throws GenericException {
        if (null == policyId || null == changeId) {
            throw new IllegalArgumentException("PolicyId and changeId ");
        }
        // get uw policy info
        PolicyVO policyVO = retrievePolicyValue(policyId);
        // retrieve product info from t_contract_product by NB interface
        List<CoverageVO> policyProducts = retrieveProductValues(policyId);
        UwPolicyVO policyValue = convertPolicyValue(policyVO, uwSourceType,
                        uwProposerId, policyProducts, null);
        policyValue.setChangeId(changeId);
        policyValue.setCaseId(changeId);
        // just for trigger,not else
        policyValue.setCaseNo("Claim");
        // generate uw record
        Long underwriteId = createUwPolicy(policyValue);
        // calculate risk aggregation
        // UwRiskAggregationDSDelegate.calcualteRiskAggregation(underwriteId);
        com.ebao.ls.pa.pub.vo.PolicyVO policy = policyService
                        .loadPolicyByPolicyId(policyId);
        raService.calcRA(policyId);
        raService.initPolicyRiskLevel(underwriteId, policy, uwSourceType,
                        CodeCst.PRODUCT_DECISION__ACCEPTED);
        return underwriteId;
    }

    /**
     * get uw Status by policyId and changeId
     * 
     * @param policyId
     * @return
     * @throws GenericException
     */
    public String getUWStatusByPolicyIdAndChangeId(Long policyId, Long changeId)
                    throws GenericException {
        try {
            Criteria criteria = HibernateSession3.currentSession()
                            .createCriteria(
                                            UwPolicy.class);
            criteria.add(Restrictions.eq("policyId", policyId));
            criteria.add(Restrictions.eq("changeId", changeId));
            Collection<UwPolicy> queryResult = criteria.list();
            Iterator iter = queryResult.iterator();
            // even if these are some records which share with the same policyId
            // and changeId,
            // but due to the consistent of uw status of these records,we only
            // get one from one record
            if (iter.hasNext()) {
                UwPolicy data = (UwPolicy) iter.next();
                return data.getUwStatus();
            } else {
                return null;
            }
        } catch (Exception e) {
            throw ExceptionFactory.parse(e);
        }
    }

    /**
     * load ma/ms factor to uw product
     * 
     * @param product
     * @throws GenericException
     */
    protected void loadMAMSFactor(UwProductVO product) throws GenericException {
        /* to decide whether MS/MA factor existed in the page */
        boolean isMsFactor = false;
        boolean isMaFactor = false;
        ProductVO showVO = this.getProductService().getProductByVersionId(
                        Long.valueOf(product.getProductId().intValue()),
                        product.getProductVersionId());
        if (showVO.isMaFactor()) {
            isMaFactor = true;
        }
        if (showVO.isMsFactor()) {
            isMsFactor = true;
        }
        // set ma factor
        if (isMaFactor) {
            CoverageVO policyProductVO = coverageCI.retrieveByItemId(product
                            .getItemId());
            if (policyProductVO.getVulExt() != null
                            && policyProductVO.getVulExt().getSaFactor() != null) {
                product.setUwsafactor(policyProductVO.getVulExt().getSaFactor());
            } else {
                isMaFactor = false;
            }
        }
        // set ms factor
        if (isMsFactor) {
            CoverageVO coverage = coverageCI.retrieveByItemId(product
                            .getItemId());
            SingleTopupVO addInvestVO = coverage.getSingleTopup();
            if (addInvestVO != null && addInvestVO.getSaFactor() != null) {
                product.setUwmsfactor(addInvestVO.getSaFactor());
            } else {
                isMsFactor = false;
            }
        }
    }

    /**
     * Filled in UwPolicyVO with conditions,endorsements,exclusion,extra loading
     * and lien info.
     * 
     * @param policyId Policy Id
     * @param policyValue Policy Value
     * @param items it will has value when source type is cs
     * @throws GenericException Application Exception
     * @author jason.luo
     * @since 10.30.2004
     * @version 1.0
     */
    protected void loadPolicyRelatedInfo(Long policyId, UwPolicyVO policyValue,
                    List<CoverageVO> policyProducts, Long[] items)
                    throws GenericException {
        // copy product info to uwproductVO array
        UwProductVO[] productValues = convertProductValues(policyProducts,
                        items);
        try {
            // get endorsement condition and exclusion information
            loadCondExcluEndoInfo(policyValue);
            // get insured list info
            loadLifeInsureds(policyId, policyValue);
            for (UwProductVO productValue : productValues) {
                if (productValue == null) {
                    continue;
                }
                // modified by hendry.xu to meet the amend .
                // if
                // (!CodeCst.UW_SOURCE_TYPE__NB.equals(policyValue.getUwSourceType()))
                // {
                // load extra loading info & lien info
                loadExtraLoading(productValue);
                loadLien(productValue);
                // }
                // load ms/ma factor
                loadMAMSFactor(productValue);
                // these field to store restrict cover info
                productValue.setUwChargePeriod(productValue.getChargePeriod());
                productValue.setUwChargeYear(productValue.getChargeYear());
                productValue.setUwCoverageYear(productValue.getCoverageYear());
                productValue.setUwCoveragePeriod(productValue
                                .getCoveragePeriod());
                productValue.setReducedAmount(productValue.getAmount());
                productValue.setReducedUnit(productValue.getUnit());
                productValue.setReducedLevel(productValue.getBenefitLevel());
                // when newbiz,set the standarelife
                if (CodeCst.UW_SOURCE_TYPE__NB.equals(policyValue
                                .getUwSourceType())) {
                    List<UwLifeInsuredVO> uwInsuredVOs = policyValue.getUwLifeInsureds();
                    for (UwLifeInsuredVO uwInsuredVO : uwInsuredVOs) {
                        if (productValue.getInsured1().compareTo(uwInsuredVO.getInsuredId()) == 0) {
                            productValue.setStandLife1(uwInsuredVO.getStandLife());
                        } else {
                            productValue.setStandLife2(uwInsuredVO.getStandLife());
                        }
                    }
                }
                // load actualValidate
                Date actualValidate = productValue.getActualValidate();
                if (actualValidate == null) {
                    productValue.setActualValidate(productValue.getValidateDate());
                }
                // clear decision
                productValue.setDecisionId(null);
                productValue.setUwStatus(UwStatusConstants.WAITING);
                productValue.setRecordStatus("0");
                // when cs item has changed main plan,we should change policy's
                // benefit type too
                Long masterId = productValue.getMasterId();
                if (null == masterId
                                && CodeCst.UW_SOURCE_TYPE__CS
                                                .equals(policyValue
                                                                .getUwSourceType())) {
                    ProductVO csvo = this
                                    .getProductService()
                                    .getProductByVersionId(
                                                    Long.valueOf(productValue
                                                                    .getProductId()
                                                                    .intValue()),
                                                    productValue.getProductVersionId());
                    policyValue.setBenefitType(csvo.getBenefitType());
                }
            }
        } catch (Exception e) {
            throw ExceptionFactory.parse(e);
        }
        // added to setup the actual validate date in t_uw_policy ning.qi
        // GEL00042817
        for (CoverageVO policyProduct : policyProducts) {
            if (policyProduct.getMasterId() == null) {
                policyValue.setActualValidate(policyProduct
                                .getRiskCommenceDate());
            }
        }
        policyValue.setUwProducts(Arrays.asList(productValues));
    }

    /**
     * load all condition,exclusion,endorsement info
     * 
     * @param policyValue
     * @throws GenericException
     */
    protected void loadCondExcluEndoInfo(UwPolicyVO policyValue)
                    throws GenericException {
        // modified by hendry.xu to meet the amend .
        // if
        // (!UwSourceTypeConstants.NEW_BIZ.equals(policyValue.getUwSourceType()))
        // {
        loadConditions(policyValue);
        loadEndorsements(policyValue);
        loadExclusions(policyValue);
        // }else{
        // //it will be effective when user load decline benefit to FPMS
        // loadExclusions(policyValue, policyRoleApi);
        // }
    }

    /**
     * Load Life Insureds
     * 
     * @param policyId Policy Id
     * @param policyValue UwPolicyVO
     * @throws GenericException
     * @author jason.luo
     * @since 11.1.2004
     * @version 1.0
     */
    protected void loadLifeInsureds(Long policyId, UwPolicyVO policyValue)
                    throws GenericException {
    	// IR 245683, 保全新增附約, 新增被保險人需寫到核保被保險人檔(T_UW_INSURED_LIST)
    	boolean isCsUw = Utils.isCsUw(policyValue.getUwSourceType());
        List<InsuredVO> insureds = null;
        if(isCsUw){
        	try {
        		insureds = policyRoleCI.findInsuredsByPolicyId(policyId);
            } catch (Exception e) {
                throw ExceptionFactory.parse(e);
            }
        } else {
        	insureds = retrieveInsuredValues(policyId);
        }
        if (null != insureds && insureds.size() > 0) {
            UwLifeInsuredVO[] lifeInsureds = convertLifeInsuredValues(insureds);
            policyValue.setUwLifeInsureds(Arrays.asList(lifeInsureds));
       
        } else {
            throw new IllegalStateException("The proposal with id-" + policyId
                            + " doesn't has any insureds.");
        }
    }

    /**
     * Load Extra Loading info
     * 
     * @param policyProductCI ProductAPI
     * @param upvo UwProductVO
     * @throws GenericException Application Exception
     * @author jason.luo
     * @since 11.1.2004
     * @version 1.0
     */
    protected void loadExtraLoading(UwProductVO upvo) throws GenericException {
        try {
            List<ExtraPremVO> extraPrems = coverageCI.retrieveByItemId(
                            upvo.getItemId()).getExtraPrems();
            if (null != extraPrems && 0 < extraPrems.size()) {
                List<UwExtraLoadingVO> extraLoadings = new ArrayList<UwExtraLoadingVO>(
                                0);
                for (int i = 0; i < extraPrems.size(); i++) {
                    UwExtraLoadingVO loading = new UwExtraLoadingVO();
                    BeanUtils.copyProperties(loading, extraPrems.get(i));
                    extraLoadings.add(loading);
                }
                upvo.setUwExtraLoadings(extraLoadings);
            }
        } catch (Exception e) {
            throw ExceptionFactory.parse(e);
        }
    }

    /**
     * Load Lien info
     * 
     * @param policyProductCI ProductAPI
     * @param upvo UwProductVO
     * @throws GenericException Application Exception
     * @author jason.luo
     * @since 11.1.2004
     * @version 1.0
     */
    protected void loadLien(UwProductVO upvo) throws GenericException {
        try {
            List<LiabilityReduceVO> nbuLiens = coverageCI.retrieveByItemId(
                            upvo.getItemId()).getLiabilityReduces();
            if (null != nbuLiens && 0 < nbuLiens.size()) {
                List<UwLienVO> liens = new ArrayList<UwLienVO>(0);
                for (int i = 0; i < nbuLiens.size(); i++) {
                    UwLienVO loading = new UwLienVO();
                    BeanUtils.copyProperties(loading, nbuLiens.get(i));
                    liens.add(loading);
                }
                upvo.setUwLiens(liens);
            }
        } catch (Exception e) {
            throw ExceptionFactory.parse(e);
        }
    }

    /**
     * Attach condition information into policy if the one exists
     * 
     * @param policyValue UwPolicyVO
     * @param policyRoleApi PolicyRoleAPI
     * @throws GenericException Application Exception
     * @author jason.luo
     * @since 10.25.2004
     * @version 1.0
     */
    protected void loadEndorsements(UwPolicyVO policyValue)
                    throws GenericException {
        List<com.ebao.ls.pa.pub.vo.PolicyEndorsementVO> endos = policyCI
                        .findEndorsementsByPolicyId(policyValue.getPolicyId());
        try {
            if (null != endos && 0 < endos.size()) {
                List<UwEndorsementVO> endorsements = new ArrayList<UwEndorsementVO>(
                                0);
                for (int i = 0; i < endos.size(); i++) {
                    UwEndorsementVO vo = new UwEndorsementVO();
                    BeanUtils.copyProperties(vo, endos.get(i));
                    endorsements.add(vo);
                }
                policyValue.setUwEndorsements(endorsements);
            }
        } catch (Exception e) {
            throw ExceptionFactory.parse(e);
        }
    }

    /**
     * Attach condition information into policy if the one exists
     * 
     * @param policyValue UwPolicyVO
     * @param policyRoleApi PolicyRoleAPI
     * @throws GenericException Application Exception
     * @author jason.luo
     * @since 10.25.2004
     * @version 1.0
     */
    private void loadConditions(UwPolicyVO policyValue) throws GenericException {
        List<com.ebao.ls.pa.pub.vo.PolicyConditionVO> conds = policyCI
                        .findConditionsByPolicyId(policyValue.getPolicyId());
        try {
            if (null != conds && 0 < conds.size()) {
                List<UwConditionVO> conditions = new ArrayList<UwConditionVO>(0);
                for (int i = 0; i < conds.size(); i++) {
                    UwConditionVO vo = new UwConditionVO();
                    BeanUtils.copyProperties(vo, conds.get(i));
                    conditions.add(vo);
                }
                policyValue.setUwConditions(conditions);
            }
        } catch (Exception e) {
            throw ExceptionFactory.parse(e);
        }
    }

    /**
     * Attach exclusion information into policy if the one exists
     * 
     * @param policyValue UwPolicyVO
     * @param policyRoleApi PolicyRoleAPI
     * @throws GenericException Application Exception
     * @author jason.luo
     * @since 10.25.2004
     * @version 1.0
     */
    private void loadExclusions(UwPolicyVO policyValue) throws GenericException {
        List<com.ebao.ls.pa.pub.vo.PolicyExclusionVO> excls = policyCI
                        .findExclusionsByPolicyId(policyValue.getPolicyId());
        try {
            if (null != excls && 0 < excls.size()) {
                List<UwExclusionVO> exclusions = new ArrayList<UwExclusionVO>(0);
                for (int i = 0; i < excls.size(); i++) {
                    UwExclusionVO vo = new UwExclusionVO();
                    BeanUtils.copyProperties(vo, excls.get(i));
                    exclusions.add(vo);
                }
                policyValue.setUwExclusions(exclusions);
            }
        } catch (Exception e) {
            throw ExceptionFactory.parse(e);
        }
    }

    protected final String UW_TYPE_INDIVIDUAL = "1";

    /**
     * Process NB Transaction
     * 
     * @param policyId Policy Id
     * @param uwProposerId Underwriting Proposer Id
     * @param manualUwInd Manual Underwriting After UPC Indicator
     * @throws GenericException Application Exception
     * @author jason.luo
     * @version 1.0
     */
    // public String processNBTransaction(Long policyId, Long uwProposerId,
    // String manualUwInd) throws GenericException {
    // return processNBTransaction(policyId, uwProposerId, manualUwInd, false);
    // }
    /**
     * compare with previous version,it only handler with UW record no need return
     * value,but to meet interface's need,only return a meanless value
     * 
     * @param policyId
     * @param uwProposerId
     * @throws GenericException
     */
    @PAPubAPIUpdate("update for loadPolicyByPolicyId")
    public String processNBTransaction(Long policyId, Long uwProposerId,
    		String manualUwInd) throws GenericException {
    	// invalidate previous unfinished UW record
    	invalidPreUwRecord(policyId);
    	// get policy information
    	PolicyVO policyVO = policyService.loadPolicyByPolicyId(policyId);

    	List<CoverageVO> policyProducts = retrieveProductValues(policyId);

    	UwPolicy preUwPolicy = uwPolicyDS.findLastestUWPolicy(policyId, CodeCst.UW_SOURCE_TYPE__NB);

    	// convert policy&product's value to UW policy&product
    	UwPolicyVO uwPolicyVO = convertPolicyValue(policyVO,
    			UwSourceTypeConstants.NEW_BIZ, uwProposerId,
    			policyProducts, null);

    	// create UW record,included product info,risk aggregation info and etc.
    	Long underwriteId = createUwPolicy(uwPolicyVO);

    	raService.initPolicyRiskLevel(underwriteId, policyVO,
    			CodeCst.UW_SOURCE_TYPE__NB,
    			CodeCst.PRODUCT_DECISION__ACCEPTED);
    	// calculate risk aggregation
    	// UwRiskAggregationDSDelegate.calcualteRiskAggregation(underwriteId);
        
        uwPolicyVO = uwPolicyDS.findUwPolicy(underwriteId);

    	if (preUwPolicy != null && preUwPolicy.getUnderwriteId() != null) {

    		HibernateSession3.currentSession().flush();

    		/* set 保單核保決定 */
    		uwPolicyVO.setPolicyDecision(preUwPolicy.getPolicyDecision());
    		/* 陳核說明 (人工陳核) */
    		uwPolicyVO.setProposeDesc(preUwPolicy.getProposeDesc());
    		/* 核保特別說明 */
    		uwPolicyVO.setCheckNote(preUwPolicy.getCheckNote());
    		uwPolicyVO.setCancelDesc(preUwPolicy.getCancelDesc());
    		/* set 高保額退費 */
    		uwPolicyVO.setHiPremIndi(preUwPolicy.getHiPremIndi());
    		/* set 投資型保費溢繳保費直接退費 */
    		uwPolicyVO.setIlpRefundCustIndi(preUwPolicy.getIlpRefundCustIndi());
    		/* set 標準體承保 */
    		uwPolicyVO.setReportStandLife(preUwPolicy.getReportStandLife());
    		/* set 核保人員核保意見 */
    		uwPolicyVO.setReportNotes(preUwPolicy.getReportNotes());
    		/* 法人實質受益人辨識評估結果 */
    		uwPolicyVO.setLegalBeneEvalNotes(preUwPolicy.getLegalBeneEvalNotes());
    		/* 洗錢資恐高風險客戶核保審查意見 */
    		uwPolicyVO.setRiskReportNotes(preUwPolicy.getRiskReportNotes());
    		/* 覆核/主管核保意見(陳核回覆) */
    		uwPolicyVO.setUwNotes(preUwPolicy.getUwNotes());

    		/* set 暫不列印 */
    		uwPolicyVO.setPendingIssueType(preUwPolicy.getPendingIssueType());
    		/* set 暫不列印原因  */
    		uwPolicyVO.setPendingIssueCause(preUwPolicy.getPendingIssueCause());
    		/* set 暫不列印原因-其他說明 */
    		uwPolicyVO.setPendingIssueDesc(preUwPolicy.getPendingIssueDesc());


    		//IR-338842-回退後綜合意見欄版本不一致，不作選項寫入
			if (preUwPolicy.getCommentVersionId() != null &&
							uwPolicyVO.getCommentVersionId() != null &&
							preUwPolicy.getCommentVersionId().equals(uwPolicyVO.getCommentVersionId())) {
				//寫入上次核保作業的核保檢核綜合意見				
				Collection<UwCommentVO> preUwCommentList = uwCommentDS
								.getCommentResultByUnderwriteId(preUwPolicy.getUnderwriteId());
				uwCommentDS.saveResults(underwriteId, new ArrayList<UwCommentVO>(preUwCommentList));
			}

            // 寫入上次核保作業的高齡投保核保評估
            uwElderCareService.copyElderCareDataById(preUwPolicy.getUnderwriteId(), underwriteId);

    		Collection<UwLifeInsuredVO> preUwInsuredList = uwPolicyDS.findUwLifeInsuredEntitis(preUwPolicy.getUnderwriteId());
    		Map<String, UwLifeInsuredVO> preUwInsuredMap = NBUtils.toMap(preUwInsuredList, "insuredId");

    		Collection<UwLifeInsuredVO> uwInsuredList = uwPolicyDS.findUwLifeInsuredEntitis(underwriteId);
    		for (UwLifeInsuredVO uwInsuredVO : uwInsuredList) {

    			Long insuredId = uwInsuredVO.getInsuredId();
    			UwLifeInsuredVO preUwInusred = preUwInsuredMap.get(String.valueOf(insuredId));

    			uwInsuredVO.setMedicalExamIndi(preUwInusred.getMedicalExamIndi());
    			uwInsuredVO.setStandLife(preUwInusred.getStandLife());
    			uwInsuredVO.setAccuLtcSa(preUwInusred.getAccuLtcSa());
    			uwInsuredVO.setDisabilityType(preUwInusred.getDisabilityType());
    			uwInsuredVO.setDisabilityCategory(preUwInusred.getDisabilityCategory());
    			uwInsuredVO.setDisabilityClass(preUwInusred.getDisabilityClass());
    			uwInsuredVO.setImageIndi(preUwInusred.getImageIndi());
    			uwInsuredVO.setHoLifeSurveyIndi(preUwInusred.getHoLifeSurveyIndi());
    			uwInsuredVO.setGuardianAncmnt(preUwInusred.getGuardianAncmnt());
    			
    			//取undo前一筆身障類別資料
    			List<String> preDisabilityList = uwInsuredDisbilityDS.getDisabilityTypes(preUwInusred.getUwListId());
    			//存undo前一筆身障類別對應ICF編碼資料
    			Map<String,List<String>> preDisablityTypeIcfLevelMap = new HashMap<String,List<String>>();
    			//undo前一筆資料若有值需複製資料至新的核保資料
    			if (preDisabilityList != null && preDisabilityList.size() > 0) {
    				
	    			for( String preDisabilityType : preDisabilityList) {
	    				List<UwInsuredDisabilityIcf> uwInsuredDisabilityIcfList = uwInsuredDisabilityIcfDao.findByDisabilityCode(preUwInusred.getUwListId(), preDisabilityType);
	    				List<String> preDisabilityIcfLevelList = new ArrayList<String>();
	    				for( UwInsuredDisabilityIcf UwInsuredDisabilityIcf : uwInsuredDisabilityIcfList) {
	    					preDisabilityIcfLevelList.add(UwInsuredDisabilityIcf.getDisabilityLevel());
	    				}
	    				preDisablityTypeIcfLevelMap.put(preDisabilityType, preDisabilityIcfLevelList );
	    			}
    			
    				uwInsuredDisbilityDS.saveResultsForUndo(uwInsuredVO.getUwListId(), preDisablityTypeIcfLevelMap);
    			}
    			
    			//同步核保資料與被保險人相關資料(身障類別及ICF)
    			uwPolicyDS.updateUwLifeInsured(uwInsuredVO);
    		}

    		HibernateSession3.currentSession().flush();
    	}
    	
    	// check whether is manual UW
    	if (null != manualUwInd && "Y".equalsIgnoreCase(manualUwInd)) {
    		uwPolicyVO.setManualUwIndi(manualUwInd);
    	} else {
    		uwPolicyVO.setManualUwIndi("N");
    	}
    	handleWFInfo(uwPolicyVO);
    	uwPolicyDS.updateUwPolicy(uwPolicyVO, false);

    	return "0";
    }

    /**
     * if the previous task is UW,then the policy will be turned to UW in program
     * directly and it will be locked by previous operator
     * 
     * @param uwPolicyVO
     * @throws GenericException
     */
    @PAPubAPIUpdate("update for loadPolicyByPolicyId")
    private void handleWFInfo(UwPolicyVO uwPolicyVO) throws GenericException {
        WfTaskInstance task = proposalProcessService.getCurrentTask(uwPolicyVO
                        .getPolicyId());
        if (task != null) {
            if (ProposalProcessService.MANUALUW.equals(task.getName())) {
                uwPolicyVO.setUwStatus(CodeCst.UW_STATUS__UW_IN_PROGRESS);

                // add by simon.huang on 2015/11/14, 核保通過未生效前回退修改, 指定回退核保人員
                WfContextLs wfLs = WfContextLs.getInstance();
                Long assignToUser = (Long) wfLs.getLocalVariable(String.valueOf(task.getId()), ProposalProcessService.VARIABLE_NAME_ASSIGN_TO_USER);
                if (assignToUser != null) {
                    uwPolicyVO.setUnderwriterId(assignToUser);
                } else {
                    uwPolicyVO.setUnderwriterId(Long.parseLong(task.getActor()));
                }
        		//PCR-463250 綜合查詢-新契約資訊要新增顯示核保單位 2023/01/19 Add by Kathy
        		UserVO userVO = SysMgmtDSLocator.getUserDS().getUserByUserID(uwPolicyVO.getUnderwriterId());
        		uwPolicyVO.setUwDeptId(Long.parseLong(userVO == null ? null : userVO.getDeptId()));
        		
                // end 2015/11/14
                PolicyVO policyVO = policyService
                                .loadPolicyByPolicyId(uwPolicyVO
                                                .getPolicyId());
                policyVO.setProposalStatus(CodeCst.PROPOSAL_STATUS__UW);
                // policyVO.setProcessorId(Long.parseLong(task.getActor()));
                policyCI.updatePolicy(policyVO);
            }
        }
    }

    /**
     * @param policyId
     * @return
     * @throws GenericException
     */
    protected boolean isHICoversionPolicy(Long policyId)
                    throws GenericException {
        return false;
    }

    /**
     * check whether bad claim exists
     * 
     * @param policyId
     * @return
     */
    protected boolean isBadClaim(Long policyId) throws GenericException {
        return false;
    }

    /**
     * create uw records
     * 
     * @param policyId
     * @param uwProposerId
     * @param uwPolicyVO
     * @throws GenericException
     */
    protected Long createUwPolicy(UwPolicyVO uwPolicyVO)
                    throws GenericException {
        Long underwriteId = uwPolicyDS.createUwPolicy(uwPolicyVO);
        return underwriteId;
    }

    /**
     * Invalidate the latest unfinish underwriting status
     * 
     * @param policyId Policy Id
     * @throws GenericException Application Exception
     * @author jason.luo
     * @since 11.1.2004
     * @version 1.0
     */
    protected void invalidPreUwRecord(Long policyId) throws GenericException {
        Long underwriteId = uwPolicyDS.retrieveUwIdbyPolicyId(policyId, false);
        if (null != underwriteId) {
            updateUwStatus(underwriteId, UwStatusConstants.INVALID, false);
        }
    }

    /**
     * update uw status in all these related policies and products
     * 
     * @param underwriteId
     * @throws GenericException
     */
    private void updateUwStatus(Long underwriteId, String uwStatus,
                    boolean isCancleLock) throws GenericException {
        UwPolicy bo = uwPolicyDao.load(underwriteId);
        bo.setUwStatus(uwStatus);
        if (isCancleLock) {
            bo.setLockTime(null);
        }
        Collection products = uwPolicyDS.findUwProductEntitis(underwriteId);
        Iterator iter = products.iterator();
        while (iter.hasNext()) {
            UwProductVO vo = (UwProductVO) iter.next();
            vo.setUwStatus(uwStatus);
            uwPolicyDS.updateUwProduct(vo);
        }
    }

    /**
     * Retrieve Policy info from t_contract_master through NB interface
     * 
     * @param policyId Policy Id
     * @return PolicyCIVO
     * @throws GenericException Application Exception
     */
    @PAPubAPIUpdate("update for loadPolicyByPolicyId")
    protected PolicyVO retrievePolicyValue(Long policyId)
                    throws GenericException {
        try {
            PolicyVO contractPolicyValue = policyService
                            .loadPolicyByPolicyId(policyId);
            return contractPolicyValue;
        } catch (Exception e) {
            throw ExceptionFactory.parse(e);
        }
    }

    /**
     * Retrieve product info
     * 
     * @param PolicyId Policy Id
     * @return PolicyProductVOs
     * @throws GenericException Application Exception
     */
    protected List<CoverageVO> retrieveProductValues(Long policyId)
                    throws GenericException {
        try {
            List<CoverageVO> policyProduct = coverageCI
                            .findByPolicyId(policyId);
            return policyProduct;
        } catch (Exception e) {
            throw ExceptionFactory.parse(e);
        }
    }

    /**
     * coverts values of products to UwProductVOs
     * 
     * @param policyProducts
     * @return
     * @throws GenericException
     */
    protected UwProductVO[] convertProductValues(
                    List<CoverageVO> policyProducts,
                    Long[] items) throws GenericException {
        List<UwProductVO> productList = new ArrayList<UwProductVO>();
        UwProductVO value;
        try {
            HashMap<Long, Long> itemsMap = new HashMap<Long, Long>();
            // when items not null,put these items in HashMap
            if (items != null) {
                for (Long item : items) {
                    itemsMap.put(item, item);
                }
            }
            // add valid product to list
            if (policyProducts != null && policyProducts.size() != 0) {
                for (CoverageVO vo : policyProducts) {
                    // only when items[] is empty,in other words,source type is
                    // nb/claim,it should
                    // check liability status
                    if (itemsMap.size() == 0
                                    && (CodeCst.LIABILITY_STATUS__LAPSED == vo
                                                    .getRiskStatus()
                                                    .intValue() || CodeCst.LIABILITY_STATUS__TERMINATED == vo
                                                    .getRiskStatus().intValue())) {
                        continue;
                    }
                    // when source type is cs,it add these items in the array
                    if (itemsMap.size() > 0) {
                        if (!itemsMap.containsValue(vo.getItemId())) {
                            continue;
                        }
                    }
                    value = new UwProductVO();
                    coverageVO2UwProductVO(vo, value);
                    productList.add(value);
                }
            }
        } catch (Exception e) {
            throw ExceptionFactory.parse(e);
        }
        // copy result from list to array
        UwProductVO[] rtnProducts;
        try {
            rtnProducts = (UwProductVO[]) BeanUtils.copyCollection(
                            UwProductVO.class,
                            productList).toArray(
                            new UwProductVO[productList.size()]);
        } catch (Exception e) {
            throw ExceptionFactory.parse(e);
        }
        if (rtnProducts != null && rtnProducts.length > 0) {
            return rtnProducts;
        } else {
            return new UwProductVO[0];
        }
    }

    /**
     * coverts policy value to UwPolicyVO
     * 
     * @param policyValue
     * @return
     * @throws GenericException
     */
    protected UwPolicyVO convertPolicyValue(PolicyVO policyValue,
                    String UwSrcType, Long uwProposerId,
                    List<CoverageVO> policyProducts,
                    Long[] items) throws GenericException {
        UwPolicyVO uwPolicyVo = new UwPolicyVO();
        try {
        	
            BeanUtils.copyProperties(uwPolicyVo, policyValue, false);
        } catch (Exception ex) {
            throw ExceptionFactory.parse(ex);
        }
        
    	//保全變更不用帶入新契約暫緩發單相關欄位
        uwPolicyVo.setUwPending(null);
        uwPolicyVo.setPendingIssueCause(null);
        uwPolicyVo.setPendingIssueDesc(null);
        uwPolicyVo.setPendingIssueType(null);
        
        if (UwSourceTypeConstants.NEW_BIZ.equals(UwSrcType)) {
        	//TODO 查詢是否有核保中暫緩記錄
        	// loading pending cause
            if (null != policyValue.getPendingCause()) {
                uwPolicyVo.setUwPending(policyValue.getPendingCause());
            }
        } 
        
        // added at 11.21.2004
        uwPolicyVo.setApplicantId(policyValue.getPolicyHolder().getPartyId());
        uwPolicyVo.setManuEscIndi("N");
        // get reversal reason
        Long policyId = policyValue.getPolicyId();
        String reversalReason = reversalCI.getReversalReason(policyId);
        uwPolicyVo.setReversalReason(reversalReason);
        // when reversal,should set their indicator to Y,exclude jet issure
        // added at 8/4/2006
        Long preUnderwriteId = uwPolicyDS
                        .findLastUnderwriteIdbyPolicyId(uwPolicyVo
                                        .getPolicyId());
        if (preUnderwriteId != null) {
            uwPolicyVo.setGenerateLcaIndi("Y");
            uwPolicyVo.setLcaDateIndi("Y");
        }
        // set the t_uw_policy's appeal indicator
        if (policyValue.getAppealReason() != null) {
            uwPolicyVo.setAppealIndi("Y");
        }
        if (preUnderwriteId == null) {
            uwPolicyVo.setReUwInd("N");
        } else {
            uwPolicyVo.setReUwInd("Y");
        }
        // set uw source type
        uwPolicyVo.setUwSourceType(UwSrcType);
        uwPolicyVo.setUwType(UW_TYPE_INDIVIDUAL);
        uwPolicyVo.setUwStatus(UwStatusConstants.WAITING);
        uwPolicyVo.setProposerId(uwProposerId);
        // initialize insert time
        Date currentUserLocalTime = AppContext.getCurrentUserLocalTime();
        uwPolicyVo.setInsertTime(currentUserLocalTime);
        uwPolicyVo.setRegDate(currentUserLocalTime);

        Long serviceAgent = uwPolicyVo.getServiceAgent();
        if (null != serviceAgent) {
            AgentChlVO agentInfo = agentService.findAgentByAgentId(serviceAgent);
            uwPolicyVo.setIacGroup(agentInfo.getAsuType());
        }
        uwPolicyVo.setUwLevel(10l);
        uwPolicyVo.setUwCategory(0l);
        // initialize policy related info
        loadPolicyRelatedInfo(policyId, uwPolicyVo, policyProducts, items);

        uwPolicyVo.setPolicyCode(policyValue.getPolicyNumber());
        uwPolicyVo.setApplyCode(policyValue.getProposalNumber());
        uwPolicyVo.setValidateDate(policyValue.getInceptionDate());
        // 2015-07-01 add by Sunny add commentVersionID

        if (UwSourceTypeConstants.CS.equals(UwSrcType)) {
            uwPolicyVo.setCommentVersionId(UW_COMMENT_VERSION_TYPE_CS);
        } else {
            CoverageVO coverage = policyValue.gitMasterCoverage();
            uwPolicyVo.setCommentVersionId(uwCommentService.getCommentVersionId(
                            new Long(coverage.getProductId()), policyValue.isOIU(),
                            currentUserLocalTime));
        }

        // 2016/11/13 simon.huang,續保商品的生效日(於核保主頁面人工調整)
        uwPolicyVo.setRenewValidateDate(policyValue.getRenewValidateDate());
        // 2017/02/21 simon.huang,核保作業初始
        uwPolicyVo.setUwTransferFlow(CodeCst.UW_TRANS_FLOW_FIRSTER);
        return uwPolicyVo;
    }

	private void reloadUwExtraLoading(Long preUnderwriteId, UwPolicyVO uwPolicyVo){
		if(preUnderwriteId == null){
			return ;
		}
		try {
			// 2017/04/11 Kate 使用保項取得原本的T_UW_EXTRA_PREM 帶回T_UW_EXTRA_PREM； 
			// 而不是使用T_EXTRA_PREM帶回T_UW_EXTRA_PREM 
			for (UwProductVO uwProductVO : uwPolicyVo.getUwProducts()) {
				Long itemId = uwProductVO.getItemId();
				
				List<UwExtraLoading> extraLoadings = uwExtraPremDao.findByUnderwriteIdAndItemId(preUnderwriteId, itemId);
				if(extraLoadings != null && 0 < extraLoadings.size()){
					List<UwExtraLoadingVO> newExtraLoadings = new ArrayList<UwExtraLoadingVO>();
					for (int i = 0; i < extraLoadings.size(); i++) {
						UwExtraLoadingVO loading = new UwExtraLoadingVO();
						
						BeanUtils.copyProperties(loading, extraLoadings.get(i));
						newExtraLoadings.add(loading);
					}
					uwProductVO.setUwExtraLoadings(newExtraLoadings);
				}
				
			}
		} catch (Exception e) {
			throw ExceptionFactory.parse(e);
		}
	}
	
	private void reloadUwExclusion(Long preUnderwriteId, UwPolicyVO uwPolicyVo){
		if(preUnderwriteId == null){ 
			return ;
		}
		try {
			logger.error("UwProcessDSImpl.reloadUwExclusion preUnderwriteId="+preUnderwriteId);
			Collection<UwExclusionVO> uwExclusions = uwPolicyDS.findUwExclusionEntitis(preUnderwriteId);
			List<UwEndorsementVO> uwEndorsements = uwPolicyDS.findUwEndorsementByUnderwriteId(preUnderwriteId);
			logger.error("UwProcessDSImpl.reloadUwExclusion.uwExclusions="+uwExclusions);
			logger.error("UwProcessDSImpl.reloadUwExclusion.uwEndorsements="+uwEndorsements);
			
			List<UwExclusionVO> uwExclusionList = new ArrayList<UwExclusionVO>();
			List<UwEndorsementVO> uwEndorsementList = new ArrayList<UwEndorsementVO>();
			if(uwExclusions.size()>0){
				Iterator iter = uwExclusions.iterator();
				while (iter.hasNext()) {
					UwExclusionVO vo = (UwExclusionVO) iter.next();
					uwExclusionList.add(vo);
				}
			}
			 //IR 363066 修正受理次標加費批註加上其他，進入核保後批註與照會單顯示問題
			if(uwEndorsements!=null && uwEndorsements.size()>0){
				Iterator iter2 = uwEndorsements.iterator();
				while (iter2.hasNext()) {
					UwEndorsementVO vo = (UwEndorsementVO) iter2.next();
					uwEndorsementList.add(vo);
				}
			}
			logger.error("UwProcessDSImpl.reloadUwExclusion uwExclusionList"
					+",uwExclusionList="+uwExclusionList);
			logger.error("UwProcessDSImpl.reloadUwExclusion uwEndorsementList"
					+",uwEndorsementList="+uwEndorsements);
			uwPolicyVo.setUwExclusions(uwExclusionList);
			uwPolicyVo.setUwEndorsements(uwEndorsements);
		} catch (Exception e) {
			throw ExceptionFactory.parse(e);
		}
	}
    /**
     * Retrieve InsuredVOs with policyId through PolicyRoleAPI *
     * 
     * @param policyId PolicyId
     * @return InsuredVOs
     * @throws GenericException
     * @author jason.luo
     * @since 10.09.2004
     */
    protected List<InsuredVO> retrieveInsuredValues(Long policyId)
                    throws GenericException {
        try {
            List<InsuredVO> returnValue = policyRoleCI
                            .findValidInsuredsByPolicyId(policyId);
            return returnValue;
        } catch (Exception e) {
            throw ExceptionFactory.parse(e);
        }
    }

    /**
     * coverts values of InsuredVOs to UwLifeInsuredVOs
     * 
     * @param lifeInsureds InsuredVOs
     * @return UwLifeInsuredVOs
     * @throws GenericException
     * @author jason.luo
     * @since 10.09.2004
     */
    private UwLifeInsuredVO[] convertLifeInsuredValues(
                    List<InsuredVO> lifeInsureds) throws GenericException {
        if (null == lifeInsureds) {
            throw new IllegalStateException("InsuredVOs can't be null.");
        }
        UwLifeInsuredVO[] returnValue = new UwLifeInsuredVO[lifeInsureds.size()];
        for (int i = 0; i < lifeInsureds.size(); i++) {
            returnValue[i] = convertLifeInsuredValue(lifeInsureds.get(i));
        }
        return returnValue;
    }

    /**
     * coverts values of InsuredVO to UwLifeInsuredVO
     * 
     * @param lifeInsured
     * @return UwLifeInsuredVO
     * @throws GenericException
     * @author jason.luo
     * @since 10.09.2004
     */
    private UwLifeInsuredVO convertLifeInsuredValue(InsuredVO lifeInsured)
                    throws GenericException {
        if (null == lifeInsured) {
            throw new IllegalStateException("InsuredVO can't be null.");
        }
        UwLifeInsuredVO returnValue = new UwLifeInsuredVO();
        try {
            // underwriteId will be set when creating the proposal/policy
            returnValue.setListId(lifeInsured.getListId());
            returnValue.setPolicyId(lifeInsured.getPolicyId());
            returnValue.setInsuredId(lifeInsured.getPartyId());
            returnValue.setMedicalExamIndi(lifeInsured.getMedicalExamIndi());
            returnValue.setDisabilityType(lifeInsured.getDisabilityType());
            returnValue.setDisabilityCategory(lifeInsured.getDisabilityCategory());
            returnValue.setDisabilityClass(lifeInsured.getDisabilityClass());
            /*
             * if these are existed uw record for the policy,we must load the initial
             * flag from pervious record
             */
            returnValue.setStandLife(getPreStandardLifeIndi(lifeInsured));
            
        	//PCR_391319 新式公會-新增被保險人受監護宣告  20201030 by Simon
            returnValue.setGuardianAncmnt(lifeInsured.getGuardianAncmnt());
            return returnValue;
        } catch (Exception e) {
            throw ExceptionFactory.parse(e);
        }
    }

    /**
     * get standard life indicator from previous record,if no previous record,it
     * will try to get it from t_insured_list
     * 
     * @param lifeInsured
     * @param returnValue
     * @throws GenericException
     */
    private String getPreStandardLifeIndi(InsuredVO lifeInsured)
                    throws GenericException {
        // get latest record
        Long latestUnderwriteId = getUwQueryDao().findLatestUnderwriteId(
                        lifeInsured.getPolicyId());
        String standardLife = "Y";
        if (latestUnderwriteId != null) {
            List<UwProduct> productCol = uwProductDao
                            .findByUnderwriteId(latestUnderwriteId);
            Iterator iter = productCol.iterator();
            UwProduct productVO = null;
            // set standard life indicator
            while (iter.hasNext()) {
                productVO = (UwProduct) iter.next();
                if (productVO.getInsured1() != null
                                && productVO.getInsured1().equals(
                                                lifeInsured.getPartyId())) {
                    standardLife = productVO.getStandLife1();
                    break;
                } else {
                    if (productVO.getInsured2() != null
                                    && productVO.getInsured2().equals(
                                                    lifeInsured.getPartyId())) {
                        standardLife = productVO.getStandLife2();
                        break;
                    }
                }
            }
        } else {
            // for DC,no uw history be initialize when DC,so we has to retrieve
            // it from t_insurd_list
            standardLife = lifeInsured.getStandLifeIndi();
        }
        return standardLife;
    }

    private void completeClaimUW(ClaimUWInfo uwVO) throws GenericException {
        /*
         * com.ebao.ls.clm.ci.vo.ClaimUWVO clmVO = new
         * com.ebao.ls.clm.ci.vo.ClaimUWVO(); clmVO.setCaseId(uwVO.getCaseId());
         * clmVO.setPolicyId(uwVO.getPolicyId()); clmVO.setResult(uwVO.getResult());
         * clmVO.setUwId(uwVO.getUwId());
         */
        claimService.completeClaimUW(uwVO);
    }

    /**
     * To finish the underwriting process of the proposal or policy *
     * 
     * @param uwPolicyVO UwPolicyVO
     * @throws GenericException Application Exception
     * @author jason.luo
     * @since 09.30.2004
     */
    public int completeUnderwriting(UwPolicyVO uwPolicyVO)
                    throws GenericException {
        /*
         * To finish the underwriting process we need the following steps: 1.verify
         * that precondition is match; 2.verify that proposal matches uw
         * completeness criteria.
         */
        Long underwriteId = uwPolicyVO.getUnderwriteId();
        Long underwriterId = Long.valueOf(AppContext.getCurrentUser()
                        .getUserId());
        String uwSourceType = uwPolicyVO.getUwSourceType();
        boolean isNewbiz = UwSourceTypeConstants.NEW_BIZ.equals(uwSourceType);
        boolean isCs = Utils.isCsUw(uwSourceType);
        boolean isClaim = Utils.isClaimUw(uwSourceType);
        List<UwProduct> products = uwProductDao
                        .findByUnderwriteId(underwriteId);
        UwPolicy uwPolicyFromDB = setPolicyValue(uwPolicyVO, underwriteId,
                        underwriterId, products);
        Integer proposalStatus = uwPolicyFromDB.getProposalDecision();
        // if changeid is not null,find the cs all the uw records.
        Long changeId = uwPolicyFromDB.getChangeId();
        List uwpolicyVOs = null;
        List uwpolicies = null;
        // As requested by SG CS user, synchrozine stand_life indicator to
        // t_contract_product and t_insured_list
        // after CS UW complete. ning.qi GEL00041672 2008-04-12
        if (isCs) {
            Collection uwInsureds = uwPolicyDS
                            .findUwLifeInsuredEntitis(underwriteId);
            if (uwInsureds != null && uwInsureds.size() > 0) {
                Long policyId = uwPolicyVO.getPolicyId();
                List<InsuredVO> insuredVOs = policyRoleCI
                                .findInsuredsByPolicyId(policyId);
                for (Iterator uwItera = uwInsureds.iterator(); uwItera
                                .hasNext();) {
                    UwLifeInsuredVO uwLife = (UwLifeInsuredVO) uwItera.next();
                    if (insuredVOs != null) {
                        for (InsuredVO insuredVO : insuredVOs) {
                            if (uwLife.getInsuredId().equals(
                                            insuredVO.getPartyId())) {
                                insuredVO.setStandLifeIndi(uwLife
                                                .getStandLife());
                                // update only necessary
                                if ((insuredVO.getStandLifeIndi() == null && uwLife
                                                .getStandLife() != null)
                                                || (insuredVO.getStandLifeIndi() != null && uwLife
                                                                .getStandLife() == null)
                                                || (insuredVO.getStandLifeIndi() != null
                                                                && uwLife.getStandLife() != null && !insuredVO
                                                                .getStandLifeIndi()
                                                                .equals(uwLife.getStandLife()))) {
                                    insuredVO.setStandLifeIndi(uwLife
                                                    .getStandLife());
                                    PolicyChangeCIVO[] items = csCI
                                                    .getPolicyChangesByMasterChangeIdOrderByOrderIdDesc(changeId);
                                    Long policyChgId = items[0]
                                                    .getPolicyChgId();
                                    policyRoleCI.updateInsured(insuredVO,
                                                    changeId, policyChgId);
                                    // dump to log
                                    CommonDumper.completePolicyChange(changeId,
                                                    policyChgId);
                                }
                            }
                        }
                    }
                }
            }
        }
        // end GEL00041672
        // 2016/11/21 Kate.Hsiao,保全不執行原易保向上陳核的邏輯(加上 !isCs 排除保全)
        // 2016/11/10 ,add b simon.huang,新契約不執行原易保向上陳核的邏輯(加上 !isNewbiz 排除新契約)
        if (!isCs && !isNewbiz && (isPolicyNeedsEscalating(uwPolicyVO, underwriterId, null)
                        || uwPolicyFromDB.getManuEscIndi().equals("Y"))) {
            uwPolicyFromDB = uwPolicyDao.load(underwriteId);
            // determines whether the proposal needs manual escalating
            if (changeId != null && isCs) {
                uwpolicyVOs = uwPolicyDS.findUwPoliciesByChangeId(changeId);
                try {
                    uwpolicies = (List) BeanUtils.copyCollection(
                                    UwPolicy.class,
                                    uwpolicyVOs);
                } catch (Exception ex) {
                    throw ExceptionFactory.parse(ex);
                }
            }
            if (isClaim) {
                return processClaimEsacalation(underwriteId, uwPolicyFromDB);
            } else {
                if (uwPolicyFromDB.getManuEscIndi().equals("Y")) {
                    return manualEscalatePolicy(uwPolicyFromDB, uwpolicies);
                } else {
                    return autoEscalatePolicy(uwPolicyFromDB, uwpolicies);
                }
            }
        } else {
            // add by johnson 2006/05/28
            /* to update all related product's status to complete */
            updateProductStatus(products, CodeCst.UW_STATUS__FINISHED);
            uwPolicyFromDB = uwPolicyDao.load(underwriteId);
            uwPolicyFromDB.setUwStatus(UwStatusConstants.FINISHED);
    
            if (isNewbiz) {
                // export underwriting result into nbu module
                exportUwResultIntoNBU(uwPolicyFromDB.getUnderwriteId());
                // change proposal status it is a pre-condition of in-force
                int action = 0;
                switch (proposalStatus.intValue()) {
                    case 80:// accept
                        action = ProposalStatusConstants.ACTION_ACCEPT;
                        break;
                    case 81:// conditional accept
                        action = ProposalStatusConstants.ACTION_CONDITIONAL_ACCEPT;
                        break;
                    case 82:// declined
                        action = ProposalStatusConstants.ACTION_DECLINED;
                        break;
                    case 84:// postponed
                        action = ProposalStatusConstants.ACTION_POSTPONED;
                        break;
                    case 86:// withdrawn
                        action = ProposalStatusConstants.ACTION_WITHDRAW;
                        break;

                }
                changeProposalStatus(action, uwPolicyFromDB.getPolicyId(),
                                underwriterId, false);

                refundAndSendLetters(proposalStatus.intValue(),
                                uwPolicyFromDB.getPolicyId(), underwriteId);
                
                
                handleMaturityCancel(action, uwPolicyFromDB.getPolicyId());
                // cancelPreLCA(uwPolicyFromDB.getPolicyId());
                
                //UNB-BSD-007#BR-NB-UW-005 儲存和更新核保資訊
                cancelUNBLetter(proposalStatus, uwPolicyFromDB.getPolicyId(), underwriteId);
            } else if (isCs || isClaim) {
                Log.info(UwProcessDSImpl.class,
                                "[Finish UW] CS - Synchronize CS uw status.");
                
                uwPolicyFromDB.setUnderwriterId(underwriterId);
                uwPolicyFromDB.setUnderwriteTime(AppContext.getCurrentUserLocalTime());
                // CS synchronize interface
                if (isCs) {
                    csCI.updateApplicationInfoByUW(
                                    changeId,
                                    Integer.valueOf(CodeCst.CS_APP_STATUS__UW_COMPLETED),
                                    AppContext.getCurrentUser().getUserId());
                    Log.info(UwProcessDSImpl.class,
                                    "update csAppStatus to CS_UNDERWRITING_DIPOSING.");
                    /*Log.info(UwProcessDSImpl.class,
                                    "CsUwCompleted event, UnderwriteId:" + uwPolicyVO.getUnderwriteId());
                     2017-04-26 kate hsiao disabled CsUwCompleted event(此邏輯為基線的，現已經無使用了) 
                    // add by poppy
                    ApplicationEventVO eventVO = new ApplicationEventVO();
                    eventVO.setTransactionId(uwPolicyVO.getUnderwriteId());
                    eventVO.setUserId(AppUserContext.getCurrentUser()
                                    .getUserId());
                    eventVO.setPolicyId(uwPolicyVO.getPolicyId());
                    CsUwCompleted event = new CsUwCompleted(eventVO);
                    getEventService().publishEvent(event);
                    // end by poppy*/
                }
                // add by shenghai, when UW complete, tell claim : "I'm OK now,
                // you can go forward your process!"
                if (isClaim) {
                    ClaimUWInfo uwVO = new ClaimUWInfo();
                    uwVO.setPolicyId(uwPolicyFromDB.getPolicyId());
                    uwVO.setUwId(uwPolicyFromDB.getUnderwriteId());
                    uwVO.setCaseId(uwPolicyFromDB.getCaseId());
                    uwVO.setResult("success");
                    completeClaimUW(uwVO);
                    // add by poppy
                    ApplicationEventVO eventVO = new ApplicationEventVO();
                    eventVO.setTransactionId(uwPolicyVO.getUnderwriteId());
                    eventVO.setUserId(AppUserContext.getCurrentUser()
                                    .getUserId());
                    eventVO.setPolicyId(uwPolicyVO.getPolicyId());
                    ClaimUwCompleted event = new ClaimUwCompleted(eventVO);
                    getEventService().publishEvent(event);
                    // end by poppy
                }
                updateReUwPolicyUwStatus(underwriteId, underwriterId,
                                UwStatusConstants.FINISHED, uwSourceType,
                                changeId);
            }
            if (Log.isDebugEnabled(getClass())) {
            }
            if (isNewbiz) {
                return CompleteUwProcessStatusConstants.COMPLETE_UW_PROCESS_SUCCESS;
            } else if (isCs) {
                return CompleteUwProcessStatusConstants.COMPLETE_UW_CS_PROCESS_SUCCESS;
            }
            return 99;
        }
    }

    /**
     * 
     * <pre>Description : UNB-BSD-007#BR-NB-UW-005 儲存和更新核保資訊
     * 1.系統更新以下訊息：
     * a.如果保單核保結論=A核 保通過，則更新要保書狀態為核保通過。
     * b.如果保單核保決定=延 期，則更新要保書狀態為延期，同時更新狀態為未回覆的照會=已撤銷。
     * c.如果保單核保決定=謝絕承保，則更新要保書狀態為謝絕承保，同時更新狀態為未回覆的照會=已撤銷。
     * d.除以上保單核保決定外，則更新要保書狀態為取消，並將保單核保決定記錄為取消原因， 同時更新狀態為未回覆的照會=已撤銷。
     * e.如果核保來源=保全，則無需更新要保書狀態。
     * </pre>
     * <p>Created By : simon.huang</p>
     * <p>Create Time : Apr 15, 2017</p>
     * @param proposalStatus
     * @param policyId
     * @param underwriteId
     */
    private void cancelUNBLetter(Integer proposalStatus, Long policyId, Long underwriteId) {
    	switch (proposalStatus.intValue()) {
    		case 82:// declined
    		case 84:// postponed
    		case 86:// withdrawn

    			//更新狀態為未回覆的照會=已撤銷
    			ProposalServiceSp.withdrawLetterStatus(policyId);

    			//更新待處理核保訊息為=已撤銷
    			List<ProposalRuleResultVO> rulerResultList = proposalRuleResultService.loadRuleResults(
    					policyId);
    			for(ProposalRuleResultVO ruleResultVO : rulerResultList){
    				if(ruleResultVO.getRuleStatus() == null
    						|| ruleResultVO.getRuleStatus() == CodeCst.PROPOSAL_RULE_STATUS__PENDING ){

    					ruleResultVO.setRuleStatus(CodeCst.PROPOSAL_RULE_STATUS__WITHDRAW);
    					proposalRuleResultService.save(ruleResultVO);
    				}
    			}

    			break;
    		default:
    			break;
    	}
    }

	private void cancelPreLCA(Long policyId) {
        // 1:LCA
        documentService.cancelLetter(policyId, 1);
    }

    @Resource(name = EventService.BEAN_DEFAULT)
    private EventService eventService;

    public EventService getEventService() {
        return eventService;
    }

    protected int processClaimEsacalation(Long underwriteId,
                    UwPolicy uwPolicyFromDB) throws GenericException {
        uwPolicyFromDB.setUwStatus(UwStatusConstants.ESCALATED);
        Long uwEscaUser = uwPolicyFromDB.getUwEscaUser();
        uwPolicyFromDB.setUwEscaUser(uwPolicyFromDB.getUnderwriterId());
        // set the products escalate status
        updateProductsEscalteStatus(underwriteId);
        uwPolicyFromDB.setUwPending(ESCALATED_TO_NEXT_LEVEL);
        // add by poppy
        ApplicationEventVO eventVO = new ApplicationEventVO();
        eventVO.setTransactionId(underwriteId);
        eventVO.setUserId(AppUserContext.getCurrentUser().getUserId());
        eventVO.setPolicyId(uwPolicyFromDB.getPolicyId());
        ClaimUwEscalated event = new ClaimUwEscalated(eventVO);
        // end by poppy
        if (uwPolicyFromDB.getManuEscIndi().equals("Y")) {
            // check whethere the uw esaca user has the authority
            UwPolicyVO tempUwPolicyVO = new UwPolicyVO();
            BeanUtils.copyProperties(tempUwPolicyVO, uwPolicyFromDB);
            if (isPolicyNeedsEscalating(tempUwPolicyVO, uwEscaUser, null)) {
                throw new AppException(UwExceptionConstants.APP_UW_NO_RIGHT);
            }
            // manual escalation
            uwPolicyFromDB.setUnderwriterId(uwEscaUser);
            uwPolicyFromDB.setManuEscIndi("Y");
            uwPolicyFromDB.setUwDecision(null);
            getEventService().publishEvent(event);
            return CompleteUwProcessStatusConstants.PROPOSAL_NEED_MANUAL_ESCALATING;
        } else {
            // auto escalation
            uwPolicyFromDB.setManuEscIndi("N");
            uwPolicyFromDB.setUnderwriterId(null);
            uwPolicyFromDB.setUwDecision(null);
            getEventService().publishEvent(event);
            return CompleteUwProcessStatusConstants.PROPOSAL_BEEN_ESCALATED;
        }
    }

    @DataModeChgModifyPoint("remove uwPolicyVO.setProposalStatus,because uw policy no the field,the invoker will use ProposalDecision field")
    protected UwPolicy setPolicyValue(UwPolicyVO uwPolicyVO, Long underwriteId,
                    Long underwriterId, Collection products)
                    throws GenericException {
        // get the policy source type
        String uwSourceType = uwPolicyVO.getUwSourceType();
        boolean isClaim = Utils.isClaimUw(uwSourceType);
        /* when claim-uw,set the uw decsion to accept */
        if (isClaim) {
            Iterator iter = products.iterator();
            while (iter.hasNext()) {
                UwProduct u = (UwProduct) iter.next();
                u.setUwStatus(UwStatusConstants.FINISHED);
                u.setDecisionId(Integer
                                .valueOf(CodeCst.PRODUCT_DECISION__ACCEPTED));
            }
        }
        // check whether all the products has finished the decision
        /* mark by simon.huang on 2015-08-03 
         * 已由保單層執行核保決定並套用至保項層，無需作保項層核保決定檢核
        if (!isAllBenefitsUnderwritten(products)) {
            throw new AppException(
                            UwExceptionConstants.APP_PRODUCTS_NOT_UNDERWRITTEN);
        }
        */
        // get proposal status
        // 2015-07-11 change by sunny
        Integer proposalStatus = getProposalStatus_TGL(products,
                        uwPolicyVO.getPolicyDecision());
        // uwPolicyVO.setProposalStatus(Long.valueOf(proposalStatus.intValue()));
        uwPolicyVO.setProposalDecision(proposalStatus);
        // add by hendry
        if (isClaim) {
            uwPolicyVO.setUwDecision(Integer
                            .valueOf(CodeCst.PRODUCT_DECISION__ACCEPTED));
        } else {
            uwPolicyVO.setUwDecision(getuwDecision(proposalStatus));
        }
        setLCAIndsAndConsentGivenInd(uwPolicyVO);
        // get the policy
        UwPolicy uwPolicyFromDB = uwPolicyDao.load(underwriteId);
        BeanUtils.copyProperties(uwPolicyFromDB, uwPolicyVO, false);
        // updated for postponed and declined needn't lca date,defect
        // GEL00014658,by robert.xu on 2006.11.4
        if (ProposalStatusConstants.POSTPONED.equals(proposalStatus)
                        || ProposalStatusConstants.DECLINED
                                        .equals(proposalStatus)) {
            uwPolicyFromDB.setLcaDate(null);
        }
        return uwPolicyFromDB;
    }

    private Integer getuwDecision(Integer proposalStatus) {
        Integer uwDecison = Integer.valueOf(CodeCst.PRODUCT_DECISION__ACCEPTED);
        if (proposalStatus.intValue() == CodeCst.PROPOSAL_STATUS__ACCEPTED) {
            uwDecison = Integer.valueOf(CodeCst.PRODUCT_DECISION__ACCEPTED);
        } else if (proposalStatus.intValue() == CodeCst.PROPOSAL_STATUS__CONDITIONAL) {
            uwDecison = Integer.valueOf(CodeCst.PRODUCT_DECISION__CONDITION);
        } else if (proposalStatus.intValue() == CodeCst.PROPOSAL_STATUS__POSTPONED) {
            uwDecison = Integer.valueOf(CodeCst.PRODUCT_DECISION__POSTPONED);
        } else if (proposalStatus.intValue() == CodeCst.PROPOSAL_STATUS__DECLINED) {
            uwDecison = Integer.valueOf(CodeCst.PRODUCT_DECISION__DECLINED);
        }
        return uwDecison;
    }

    /**
     * refund and send letters
     * 
     * @param proposalStatus
     * @param policyId
     * @throws GenericException
     */
    protected void refundAndSendLetters(int proposalStatus, Long policyId,
                    Long underWriterId) throws GenericException {
        // check whether any insured's age above 57
        boolean isAllB57 = true;
        Collection insuredList = this.getUwPolicyDS().findUwLifeInsuredEntitis(
                        underWriterId);
        Iterator iter = insuredList.iterator();
        Date nowDate = AppContext.getCurrentUserLocalTime();
        while (iter.hasNext()) {
            UwLifeInsuredVO insuredVO = (UwLifeInsuredVO) iter.next();
            // update get age method , requirement need "ANB"(Age next birthday)
            // ,defect GEL00026059(request by #6525),by robert.xu on 2007.6.15
            try {
                long age = BizUtils.getAge(CodeCst.AGE_METHOD__ANB, customerCI
                                .getPerson(insuredVO.getInsuredId())
                                .getBirthday(), nowDate);
                if (isAllB57 && age >= 57) {
                    isAllB57 = false;
                    break;
                }
            } catch (Exception e) {
                Log.error(this.getClass(), e);
            }
        }
        if (CodeCst.PROPOSAL_STATUS__DECLINED == proposalStatus
                        || CodeCst.PROPOSAL_STATUS__POSTPONED == proposalStatus) {
            if (!isAllB57) {
                DebitUWMedicalAgentSP.debitUWMedicalAgent(policyId);
            }
        }
    }

    /**
     * @param upvo
     * @param policy
     * @param products
     * @return
     * @throws GenericException
     */
    protected int autoEscalatePolicy(UwPolicy policy, List<UwPolicy> policyVOs)
                    throws GenericException {
        Log.info(UwProcessDSImpl.class,
                        "[Finish UW] Escalte - uw status to escalated.");
        Long policyId = policy.getPolicyId();
        if (policyVOs != null && !policyVOs.isEmpty()) {
            Iterator iter = policyVOs.iterator();
            while (iter.hasNext()) {
                UwPolicy element = (UwPolicy) iter.next();
                updateOnePolicyEscalateStatus(element);
            }
        } else {
            // only in NB status ,we log the proposal status into the
            // t_proposal_process
            // updateProposalProcess(policy);
            updateOnePolicyEscalateStatus(policy);
        }
        updatePolicyInAutoEscalate(policyId, policy.getUwSourceType());
        // add by poppy
        ApplicationEventVO eventVO = new ApplicationEventVO();
        eventVO.setTransactionId(policy.getUnderwriteId());
        eventVO.setUserId(AppUserContext.getCurrentUser().getUserId());
        eventVO.setPolicyId(policy.getPolicyId());
        ApplicationEvent<ApplicationEventVO> event = null;
        String uwSourceType = policy.getUwSourceType();
        boolean isNewbiz = UwSourceTypeConstants.NEW_BIZ.equals(uwSourceType);
        boolean isCs = Utils.isCsUw(uwSourceType);
        if (isCs) {
            event = new CsUwEscalated(eventVO);
        } else if (isNewbiz) {
            event = new NbUwEscalated(eventVO);
        }
        getEventService().publishEvent(event);
        // end by poppy
        return CompleteUwProcessStatusConstants.PROPOSAL_BEEN_ESCALATED;
    }

    /**
     * @param policy
     * @throws GenericException
     */
    private void updateOnePolicyEscalateStatus(UwPolicy policy)
                    throws GenericException {
        // set the products escalate status
        Long underwriteId = policy.getUnderwriteId();
        updateProductsEscalteStatus(underwriteId);
        // Once the case has been escalated,
        // system to set Manual Escalation IND to "N" and "Underwriter to
        // escalate to " field to Null.
        policy.setUwStatus(UwStatusConstants.ESCALATED);
        Long currentUnderwriter = policy.getUnderwriterId();
        policy.setManuEscIndi("N");
        // added by hendry.xu to meet the auto escalate.
        policy
                        .setProposalDecision(ProposalStatusConstants.WAITING_FOR_UNDERWRITING);
        policy.setUwEscaUser(currentUnderwriter);
        // todo mingchun.shi 2005-11-2 must use codecst
        policy.setUwPending(ESCALATED_TO_NEXT_LEVEL);
        policy.setUnderwriterId(null);
        policy.setUwDecision(null);
        UwPolicyVO policyVO = new UwPolicyVO();
        BeanUtils.copyProperties(policyVO, policy);
        uwPolicyDS.updateUwPolicy(policyVO, true);
    }

    /**
     * @param policyId
     * @throws GenericException
     */
    @PAPubAPIUpdate("update for loadPolicyByPolicyId")
    private void updatePolicyInAutoEscalate(Long policyId, String uwType)
                    throws GenericException {
        if (CodeCst.UW_SOURCE_TYPE__NB.equals(uwType)) {
            PolicyVO policyVO = policyService.loadPolicyByPolicyId(policyId);
            // policyVO.setProcessorId(null);
            // policyVO.setProposalLock("N");
            policyVO.setProposalStatus(Integer
                            .valueOf(CodeCst.PROPOSAL_STATUS__WAIT_UW));
            policyCI.updatePolicy(policyVO);
        }
    }

    /**
     * manual escalate the policy to higer underwriter.
     * 
     * @param uwPolicyVO
     * @param underwriteId
     * @return
     * @throws GenericException
     */
    protected int manualEscalatePolicy(UwPolicy uwPolicyVO, List uwpolicyVOs)
                    throws GenericException {
        if (Log.isDebugEnabled(getClass())) {
        }
        if (uwpolicyVOs != null && !uwpolicyVOs.isEmpty()
                        && uwpolicyVOs.size() > 1) {
            Iterator iterator = uwpolicyVOs.iterator();
            while (iterator.hasNext()) {
                UwPolicy element = (UwPolicy) iterator.next();
                UwPolicyVO tempUwPolicyVO = new UwPolicyVO();
                BeanUtils.copyProperties(tempUwPolicyVO, element);
                manualEscalteOnePolicy(tempUwPolicyVO);
            }
        } else {
            UwPolicyVO tempUwPolicyVO = new UwPolicyVO();
            BeanUtils.copyProperties(tempUwPolicyVO, uwPolicyVO);
            manualEscalteOnePolicy(tempUwPolicyVO);
        }
        // add by poppy
        ApplicationEventVO eventVO = new ApplicationEventVO();
        eventVO.setTransactionId(uwPolicyVO.getUnderwriteId());
        eventVO.setUserId(AppUserContext.getCurrentUser().getUserId());
        eventVO.setPolicyId(uwPolicyVO.getPolicyId());
        ApplicationEvent<ApplicationEventVO> event = null;
        String uwSourceType = uwPolicyVO.getUwSourceType();
        boolean isNewbiz = UwSourceTypeConstants.NEW_BIZ.equals(uwSourceType);
        boolean isCs = Utils.isCsUw(uwSourceType);
        if (isCs) {
            event = new CsUwEscalated(eventVO);
        } else if (isNewbiz) {
            event = new NbUwEscalated(eventVO);
        }
        getEventService().publishEvent(event);
        // end by poppy
        return CompleteUwProcessStatusConstants.PROPOSAL_NEED_MANUAL_ESCALATING;
    }

    /**
     * @param uwPolicyVO
     * @param underwriteId
     * @param uwpolicyVOs
     * @throws GenericException
     * @throws AppException
     */
    @DataModeChgModifyPoint("change param from old bo to vo")
    public void manualEscalteOnePolicy(UwPolicyVO uwPolicyVO)
                    throws GenericException {
        // get the escalate to user
        Long uwEscaUser = uwPolicyVO.getUwEscaUser();
        if (isPolicyNeedsEscalating(uwPolicyVO, uwEscaUser, null)) {
            throw new AppException(UwExceptionConstants.APP_UW_NO_RIGHT);
        }
        Long underwriteId = uwPolicyVO.getUnderwriteId();
        // set the products escalate status
        updateProductsEscalteStatus(underwriteId);
        // Once the case has been escalated,
        // system to set Manual Escalation IND to "N" and "Underwriter to
        // escalate to " field to Null.
        Long currentUnderwriter = uwPolicyVO.getUnderwriterId();
        uwPolicyVO.setUwStatus(UwStatusConstants.IN_PROGRESS);
        uwPolicyVO.setUnderwriterId(uwEscaUser);
		//PCR-463250 綜合查詢-新契約資訊要新增顯示核保單位 2023/01/19 Add by Kathy
		UserVO userVO = SysMgmtDSLocator.getUserDS().getUserByUserID(uwPolicyVO.getUnderwriterId());
		uwPolicyVO.setUwDeptId(Long.parseLong(userVO == null ? null : userVO.getDeptId()));        
        uwPolicyVO.setManuEscIndi("Y");
        uwPolicyVO.setUwEscaUser(currentUnderwriter);
        // todo mingchun.shi 2005-11-2 user codecst
        uwPolicyVO.setUwPending(ESCALATED_TO_NEXT_LEVEL);
        uwPolicyVO.setUwDecision(null);
        // added by hendry.xu to meet the manual escalate
        uwPolicyVO
                        .setProposalDecision(ProposalStatusConstants.UNDERWRITING_IN_PROGRESS);
        // the Escalate process status update to t_proposal_process.
        // if (CodeCst.UW_SOURCE_TYPE__NB.equals(uwPolicyVO.getUwSourceType()))
        // {
        // ActionUtil.doProposalProcess(uwPolicyVO.getPolicyId(),
        // "Manual Escalation");
        // }
        uwPolicyDS.updateUwPolicy(uwPolicyVO, true);
        updatePolicyInManualEscalate(uwPolicyVO.getPolicyId(), uwEscaUser);
    }

    /**
     * update product's status to escalate
     * 
     * @param underwriteId
     * @throws GenericException
     */
    public void updateProductsEscalteStatus(Long underwriteId)
                    throws GenericException {
        // set benefits status
        List<UwProduct> products = uwProductDao
                        .findByUnderwriteId(underwriteId);
        Iterator iter = products.iterator();
        while (iter.hasNext()) {
            UwProduct benefit = (UwProduct) iter.next();
            benefit.setUwStatus(UwStatusConstants.ESCALATED);
            UwProductVO productVO = new UwProductVO();
            try {
                BeanUtils.copyProperties(productVO, benefit);
            } catch (Exception ex) {
                throw ExceptionFactory.parse(ex);
            }
            uwPolicyDS.updateUwProduct(productVO);
        }
    }

    /**
     * @param uwPolicyVO
     * @return
     * @throws GenericException
     */
    public void updatePolicyInManualEscalate(Long policyId, Long uwEscaUser)
                    throws GenericException {
        // PolicyVO policyVO = policyCI.retrievePolicyById(policyId);
        // policyVO.setProcessorId(uwEscaUser);
        // policyCI.updatePolicy(policyVO);
    }

    /**
     * Determines whether all benefits have been finished.
     * 
     * @param products UwProducts
     * @return boolean True - all benefits have been finished False - not all
     *         benefits have been finished yet
     * @throws GenericException Application Exception
     * @author jason.luo
     * @since 09.30.2004
     */
    protected boolean isAllBenefitsUnderwritten(Collection products)
                    throws GenericException {
        if (products == null || products.isEmpty()) {
            return true;
        }
        Iterator iter = products.iterator();
        while (iter.hasNext()) {
            UwProduct product = (UwProduct) iter.next();
            if (!UwStatusConstants.FINISHED.equals(product.getUwStatus())
                            && !UwStatusConstants.ESCALATED.equals(product
                                            .getUwStatus())) {
                return false;
            }
        }
        return true;
    }

    /**
     * Set Update LCA IND, Generate LCA IND and Consent Given IND
     * 
     * @param upvo UwPolicyVO
     * @throws GenericException Application Exception
     * @author jason.luo
     * @since 09.30.2004
     */
    // section 1.9.14 LCA/consent updating rules
    public void setLCAIndsAndConsentGivenInd(UwPolicyVO upvo)
                    throws GenericException {
        if ("Y".equals(upvo.getGenerateLcaIndi())) {
            if (!"Y".equals(upvo.getLcaDateIndi())) {
                throw new AppException(
                                UwExceptionConstants.APP_LCA_INDI_NOT_CONSISTENT);
            }
            upvo.setLcaDate(AppContext.getCurrentUserLocalTime());
        }
        if ("Y".equals(upvo.getLcaDateIndi())) {
            upvo.setLcaDate(AppContext.getCurrentUserLocalTime());
        }
    }

    /**
     * Determins whether needs to escalates the policy
     * 
     * @param uwPolicy id of the uw policy
     * @param empId empId of the underwriter
     * @author jason.luo
     * @throws Exception
     */
    @DataModeChgModifyPoint("change param from old bo to vo")
    @PAPubAPIUpdate("update for loadPolicyByPolicyId")
    protected boolean isPolicyNeedsEscalating(UwPolicyVO uwPolicy, Long empId,
                    List uwPolicyVOS) throws GenericException {
        boolean isNeedEscalate = false;
        // RE-initialize UW authority, because decision may have changed
        com.ebao.ls.pa.pub.vo.PolicyVO policy = policyService
                        .loadPolicyByPolicyId(uwPolicy.getPolicyId());
        Integer uwDecision = CodeCst.PRODUCT_DECISION__ACCEPTED;
        if (uwPolicy.getUwDecision() != null) {
            uwDecision = uwPolicy.getUwDecision();
        }
        // add to recalc ra if uw change the prem or sa
        // raService.calcRA(uwPolicy.getPolicyId());
        // added
        raService.initPolicyRiskLevel(uwPolicy.getUnderwriteId(), policy,
                        uwPolicy.getUwSourceType(), uwDecision);
        if (!raService.checkUserLimitWithPolicy(empId, uwPolicy.getPolicyId())) {
            isNeedEscalate = true;
        }
        return isNeedEscalate;
    }


    /**
     * Quashs underwriting (Mainly used by uw internal methods and unlock function
     * will use this one when try to unlock those policies been processed by other
     * underwriter)
     * 
     * @param uwId UnderwriteId
     * @param empId Current Underwriter
     * @param hintDesc Description of hint
     * @param isUpdateExternalStatus Update Status Mark
     * @throws GenericException Application Exception
     */
    // This method used for supports unlock underwriting policy
    public void quashUnderwriting(Long uwId, Long empId, String hintDesc,
                    boolean isUpdateExternalStatus) throws GenericException {
        try {
            UwPolicy policy = uwPolicyDao.load(uwId);
            // synchronize proposal status with external interface
            String uwSourceType = policy.getUwSourceType();
            /*
             * In previous design, there is a drawback:when user click make benefit
             * decision firstly, then start status will become uw in progress,but in
             * this scenario,the status after cancle should be waiting for uw. so Now
             * we will update assumpsit to 1 when staus is uw in progress otherwise
             * set it to 0
             */
            if (
            // (!UwSourceTypeConstants.NEW_BIZ.equals(uwSourceType)
            // || !(startStatus.intValue() == CodeCst.PROPOSAL_STATUS__UW)
            // )&&
            ("0".equals(policy.getAssumpsit()) || policy.getAssumpsit() == null || !isUpdateExternalStatus)) {
                policy.setUnderwriterId(null);
                policy.setLockTime(null);
                policy.setUwStatus(UwStatusConstants.WAITING);
                // reset benefits uw status and reserves underwriter for "Make
                // decision" UI
                Collection benefits = uwPolicyDS.findUwProductEntitis(uwId);
                Iterator iter = benefits.iterator();
                while (iter.hasNext()) {
                    UwProductVO benefit = (UwProductVO) iter.next();
                    // reset uwStatus to "Waiting for underwriting"
                    benefit.setUwStatus(UwStatusConstants.WAITING);
                    uwPolicyDS.updateUwProduct(benefit);
                }
            }
            if (UwSourceTypeConstants.NEW_BIZ.equals(uwSourceType)) {
                if (isUpdateExternalStatus) {
                    // add to get get prestatus it is saved when entry uw in
                    // UwPolicyMainAction with workflow engine,by robert.xu on
                    // 2008.5.6
                    Integer startStatus = null;
                    WfContextLs wfLs = WfContextLs.getInstance();
                    String curTaskId = wfLs.getCurrentTaskId("policyId",
                                    policy.getPolicyId(),
                                    ProposalProcessService.PROCESS_NAME,
                                    WorkListConstant.UIC_DATATYPE_LONG);
                    if (!StringUtils.isNullOrEmpty(curTaskId)) {
                        Integer preStatus = null;
                        Object obj = wfLs.getLocalVariable(curTaskId,
                                        "preProposalStatus");
                        if (obj instanceof Integer) {
                            preStatus = (Integer) obj;
                        } else if (obj instanceof Long) {
                            preStatus = Integer
                                            .valueOf(((Long) obj).toString());
                        }
                        if (preStatus != null) {
                            // back to original status
                            startStatus = preStatus;
                        }
                    }
                    // add end
                    if (startStatus != null
                                    && startStatus.intValue() == CodeCst.PROPOSAL_STATUS__UW) {
                        changeProposalStatus(
                                        ProposalStatusConstants.ACTION_CANCEL_UW_IN_PROGRESS,
                                        policy.getPolicyId(), empId, false);
                    } else {
                        changeProposalStatus(
                                        ProposalStatusConstants.ACTION_CANCEL,
                                        policy.getPolicyId(), empId, false);
                    }
                }
            } else if (Utils.isCsUw(uwSourceType)
                            || Utils.isClaimUw(uwSourceType)) {
                if ("0".equals(policy.getAssumpsit())) {
                    updateReUwPolicyUwStatus(uwId, empId,
                                    UwStatusConstants.WAITING,
                                    uwSourceType, policy.getChangeId());
                }
            }
            if (Utils.isCsUw(uwSourceType)) {
                csCI.updateApplicationInfoByUW(
                                policy.getChangeId(),
                                Integer.valueOf(CodeCst.CS_APP_STATUS__PEND_UW),
                                null);
            }
        } catch (Exception e) {
            throw ExceptionFactory.parse(e);
        }
    }

    /**
     * Quashs underwriting
     * 
     * @param uwId the underwrite Id of the policy
     * @param empId the emId of the underwriter
     * @param hintDesc the description of hint
     * @throws GenericException Application Exception
     * @author jason.luo
     * @since 09.30.2004
     */
    public void quashUnderwriting(Long uwId, Long empId, String hintDesc)
                    throws GenericException {
        quashUnderwriting(uwId, empId, hintDesc, true);
    }

    /**
     * Determines whether condition code has been imposed
     * 
     * @param conditions
     * @return True - imposed False - not
     * @author jason.luo
     * @since 11.11.2004
     * @version 1.0
     */
    protected boolean isConditionCodeImposed(Collection conditions) {
        return !(null == conditions || 0 == conditions.size());
    }

    /**
     * Determines whether 0015 has been imposed as the exclusion one
     * 
     * @param conditions Condition Codes(Should not be an empty one)
     * @return True - Only 0015 condition code imposed False - Multiply condition
     *         code imposed
     * @throws GenericException Application Exception
     * @author jason.luo
     * @since 11.11.2004
     * @version 1.0
     */
    protected boolean isOnlyAdHocConditionImposed(Collection conditions)
                    throws GenericException {
        Iterator iter = conditions.iterator();
        while (iter.hasNext()) {
            UwConditionVO condition = (UwConditionVO) iter.next();
            if (!ADD_HOC_CONDITION_0015.equals(condition.getConditionCode())) {
                return false;
            }
        }
        return true;
    }

    /**
     * Determines whether exclusion code has been imposed
     * 
     * @param exclusions Exclusion Codes
     * @return True - imposed False - not imposed
     * @author jason.luo
     * @since 11.11.2004
     * @version 1.0
     */
    protected boolean isExclusionCodeImposed(Collection exclusions) {
        return !(null == exclusions || 0 == exclusions.size());
    }

    /**
     * Determines whether all product were given the same uw decision
     * 
     * @param products Products
     * @param decisionId Uw decision
     * @return True - Products has the same decision False - Products has the
     *         different decision
     * @author jason.luo
     * @since 11.11.2004
     * @version 1.0
     */
    protected boolean isProductsSameDecision(Collection products, int decisionId) {
        if (null == products || 0 == products.size()) {
            return false;
        }
        Iterator iter = products.iterator();
        while (iter.hasNext()) {
            UwProduct product = (UwProduct) iter.next();
            if (null == product.getDecisionId()) {
                return false;
            }
            if (decisionId != product.getDecisionId().intValue()) {
                return false;
            }
        }
        return true;
    }

    /**
     * Determines whether assigned option has been imposed onto policy
     * 
     * @return True - imposed False - not imposed
     * @throws GenericException Application Exception
     * @author jason.luo
     * @since 11.11.2004
     * @version 1.0
     */
    protected boolean isPolicyOptionImposed(Collection products, int optionId)
                    throws GenericException {
        if (null == products || 0 == products.size()) {
            return false;
        }
        Iterator iter = products.iterator();
        while (iter.hasNext()) {
            UwProduct product = (UwProduct) iter.next();
            if (isProductOptionImposed(product, optionId)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Determines whether the loading option has been imposed
     * 
     * @return True - imposed False - not imposed
     * @author jason.luo
     * @since 11.11.2004
     * @version 1.0
     */
    public boolean isLoadingOptionImposed(Collection loadings,
                    String loadingOption) {
        Collection<UwExtraLoadingVO> c = BeanUtils.copyCollection(
                        UwExtraLoadingVO.class, loadings);
        Iterator iter = c.iterator();
        while (iter.hasNext()) {
            UwExtraLoadingVO loading = (UwExtraLoadingVO) iter.next();
            if (loadingOption.equals(loading.getExtraType())) {
                return true;
            }
        }
        return false;
    }

    /**
     * get origianl ms factor from t_add_invest
     * 
     * @param underwriteId
     * @param itemId
     * @param productId
     * @return
     * @throws GenericException
     */
    private BigDecimal getOriginalMSFactor(Long underwriteId, Long itemId,
                    Long productId) throws GenericException {
        boolean isMsFactor = false;
        BigDecimal msFactor = new BigDecimal(0);
        CoverageVO coverageVO = coverageCI.retrieveByItemId(itemId);
        ProductVO showVO = this.getProductService().getProductByVersionId(productId,
                        coverageVO.getProductVersionId());
        if (showVO.isMsFactor()) {
            isMsFactor = true;
        }
        // get ms factor
        if (isMsFactor) {
            SingleTopupVO addInvestVO = coverageVO.getSingleTopup();
            if (addInvestVO != null && addInvestVO.getSaFactor() != null) {
                msFactor = addInvestVO.getSaFactor();
            } else {
                isMsFactor = false;
            }
        }
        return msFactor;
    }

    public boolean isRestrictCoverImposed(UwProductVO productVO)
                    throws GenericException {
        // if charge year has reduced,then true
        if (productVO.getUwChargeYear() != null) {
            if (productVO.getUwChargeYear()
                            .compareTo(productVO.getChargeYear()) < 0) {
                return true;
            }
        }
        // if coverage year has reduced,then true
        if (productVO.getUwCoverageYear() != null) {
            if (productVO.getUwCoverageYear().compareTo(
                            productVO.getCoverageYear()) < 0) {
                return true;
            }
        }
        // if amount has reduced,then true
        if (productVO.getReducedAmount() != null
                        && productVO.getReducedAmount().compareTo(
                                        productVO.getAmount()) < 0) {
            return true;
        }
        // if ma factor has reduced,then true
        if (productVO.getUwsafactor() != null
                        && productVO.getSaFactor() != null) {
            if (productVO.getUwsafactor().compareTo(productVO.getSaFactor()) < 0) {
                return true;
            }
        }
        // if ms factor has reduced,then true
        BigDecimal originalMsFactor = getOriginalMSFactor(
                        productVO.getUnderwriteId(), productVO.getItemId(),
                        Long.valueOf(productVO.getProductId().intValue()));
        if (productVO.getUwmsfactor() != null && originalMsFactor != null) {
            if (productVO.getUwmsfactor().compareTo(originalMsFactor) < 0) {
                return true;
            }
        }
        // if unit has reduced,then true
        if (productVO.getReducedUnit() != null
                        && productVO.getReducedUnit().compareTo(
                                        productVO.getUnit()) < 0) {
            return true;
        }
        // if benefit level has reduced,then true
        if (!StringUtils.isNullOrEmpty(productVO.getReducedLevel())
                        && productVO.getReducedLevel().compareTo(
                                        productVO.getBenefitLevel()) < 0) {
            return true;
        }
        return false;
    }

    // @todo,this code should be incorporate with the code in
    // uwDecisionSaveAction
    private boolean isRestrictCoverImposed(UwProduct productVO)
                    throws GenericException {
        UwProductVO vo = new UwProductVO();
        productVO.copyToVO(vo, true);
        return isRestrictCoverImposed(vo);
    }

    /**
     * Determines whether lien has been imposed onto product
     * 
     * @param product Product
     * @return True - imposed False - not imposed
     * @throws GenericException Application Exception
     * @author jason.luo
     * @since 11.11.2004
     * @version 1.0
     */
    protected boolean isProductOptionImposed(UwProduct product, int optionId)
                    throws GenericException {
        List<UwExtraLoading> loadings;
        switch (optionId) {
            case PRODUCT_OPTION_LIEN:
                Collection liens = uwReduceDao.findByUnderwriteIdAndItemId(
                                product.getUnderwriteId(), product.getItemId());
                return !(null == liens || 0 == liens.size());
            case PRODUCT_OPTION_RESTRICT_COVER:
                if (isRestrictCoverImposed(product)) {
                    return true;
                }
                return false;
            case PRODUCT_OPTION_HEALTH:
                loadings = uwExtraPremDao.findByUnderwriteIdAndItemId(
                                product.getUnderwriteId(), product.getItemId());
                return isLoadingOptionImposed(loadings,
                                UwExtraLoadingTypeConstants.HEALTH);
            case PRODUCT_OPTION_OCCUPATION:
                loadings = uwExtraPremDao.findByUnderwriteIdAndItemId(
                                product.getUnderwriteId(), product.getItemId());
                return isLoadingOptionImposed(loadings,
                                UwExtraLoadingTypeConstants.OCCUPATION);
            case PRODUCT_OPTION_AVOCATION:
                loadings = uwExtraPremDao.findByUnderwriteIdAndItemId(
                                product.getUnderwriteId(), product.getItemId());
                return isLoadingOptionImposed(loadings,
                                UwExtraLoadingTypeConstants.AVOCATION);
            case PRODUCT_OPTION_RESIDENT:
                loadings = uwExtraPremDao.findByUnderwriteIdAndItemId(
                                product.getUnderwriteId(), product.getItemId());
                return isLoadingOptionImposed(loadings,
                                UwExtraLoadingTypeConstants.RESIDENTIAL);
            case PRODUCT_OPTION_AVIATION:
                loadings = uwExtraPremDao.findByUnderwriteIdAndItemId(
                                product.getUnderwriteId(), product.getItemId());
                return isLoadingOptionImposed(loadings,
                                UwExtraLoadingTypeConstants.AVIATION);
            case PRODUCT_OPTION_OTHERS:
                loadings = uwExtraPremDao.findByUnderwriteIdAndItemId(
                                product.getUnderwriteId(), product.getItemId());
                return isLoadingOptionImposed(loadings,
                                UwExtraLoadingTypeConstants.OTHER);
            case PRODUCT_OPTION_LOADING_ALL:
                loadings = uwExtraPremDao.findByUnderwriteIdAndItemId(
                                product.getUnderwriteId(), product.getItemId());
                return !(null == loadings || 0 == loadings.size());
            default:
                Log.info(UwProcessDSImpl.class,
                                "Product doesn't imposed item - "
                                                + optionId);
                return false;
        }
    }

    public Integer getProposalStatus_TGL(Collection copiedProducts,
                    String policyDecision)
                    throws GenericException {
        if (com.ebao.ls.uw.ds.constant.Cst.POLICY_DECISION_ACCEPT
                        .equals(policyDecision)) {
            // add by simon.huang on 2015-07-23
            // 核保結論下核保通過，不管是否有加費跟批註，proposal status均設定80
            return new Integer(CodeCst.PROPOSAL_STATUS__ACCEPTED);
        } else if (com.ebao.ls.uw.ds.constant.Cst.POLICY_DECISION_DECLINED
                        .equals(policyDecision)) {
            return new Integer(CodeCst.PROPOSAL_STATUS__DECLINED);
        } else if (com.ebao.ls.uw.ds.constant.Cst.POLICY_DECISION_POSTPONED
                        .equals(policyDecision)) {
            return new Integer(CodeCst.PROPOSAL_STATUS__POSTPONED);
        } else if (com.ebao.ls.uw.ds.constant.Cst.POLICY_DECISION_MISSING_DOC
                        .equals(policyDecision)
                        || com.ebao.ls.uw.ds.constant.Cst.POLICY_DECISION_WITHDRAW
                                        .equals(policyDecision)
                        || com.ebao.ls.uw.ds.constant.Cst.POLICY_DECISION_REJECT_EXCLUSION
                                        .equals(policyDecision)
                        || com.ebao.ls.uw.ds.constant.Cst.POLICY_DECISION_REJECT_EXTRA_PREM
                                        .equals(policyDecision)
                        || com.ebao.ls.uw.ds.constant.Cst.POLICY_DECISION_NOT_PAY
                                        .equals(policyDecision)
                        || com.ebao.ls.uw.ds.constant.Cst.POLICY_DECISION_NOT_MEDICAL_EXAM
                                        .equals(policyDecision)
                        || com.ebao.ls.uw.ds.constant.Cst.POLICY_DECISION_OTHER
                                        .equals(policyDecision)) {
            return new Integer(CodeCst.PROPOSAL_STATUS__WITHDRAWN);
        }
        return new Integer(0); // return N/A
    }

    /**
     * Get proposal status If underwriting decision is "Conditionally Accepted"
     * and no condition and/or exclusion and/or loading and/or lien and/or
     * restrict coverage is present for the case, raise GenericException
     * 
     * @param copiedProducts UwProducts
     * @param underwriteId UnderwriteId
     * @throws GenericException Application Exception
     * @author jason.luo
     * @since 09.30.2004
     */
    public Integer getProposalStatus(Collection copiedProducts,
                    Long underwriteId)
                    throws GenericException {
        // convert product decision
        try {
            for (Iterator iter = copiedProducts.iterator(); iter.hasNext();) {
                UwProduct element = (UwProduct) iter.next();
                int decisionId = element.getDecisionId().intValue();
                if (decisionId == CodeCst.PRODUCT_DECISION__ORIGINAL) {
                    Long itemId = element.getItemId();
                    Integer preDecisonId = Utils.findPreProductDecisonId(
                                    underwriteId,
                                    itemId);
                    if (preDecisonId != null) {
                        decisionId = preDecisonId.intValue();
                    } else {
                        decisionId = CodeCst.PRODUCT_DECISION__ACCEPTED;
                    }
                } else if (decisionId == CodeCst.PRODUCT_DECISION__STANDARD) {
                    decisionId = CodeCst.PRODUCT_DECISION__ACCEPTED;
                }
                element.setDecisionId(Integer.valueOf(decisionId));
            }
        } catch (Exception ex) {
            throw ExceptionFactory.parse(ex);
        }
        // proposal default status
        Integer proposalStatus = ProposalStatusConstants.ACCEPTED;
        // All declined
        if (isProductsSameDecision(copiedProducts,
                        UwDecisionConstants.DECLINATION)) {
            proposalStatus = ProposalStatusConstants.DECLINED;
        } else if (isProductsSameDecision(copiedProducts,
                        UwDecisionConstants.POSTPONE)) {
            // All postponed
            proposalStatus = ProposalStatusConstants.POSTPONED;
        } else {
            Collection exclusions = uwPolicyDS
                            .findUwExclusionEntitis(underwriteId);
            Collection conditions = uwPolicyDS
                            .findUwConditionEntitis(underwriteId);
            Iterator productIter = copiedProducts.iterator();
            while (productIter.hasNext()) {
                UwProduct product = (UwProduct) productIter.next();
                int decision = product.getDecisionId().intValue();
                if (UwDecisionConstants.CONDITIONAL_UNDERWRITTEN == decision) {
                    // if decision is conditinal accept,pls impose substandard
                    // term
                    if (!isExclusionCodeImposed(exclusions)
                                    && !isConditionCodeImposed(conditions)
                                    && !isProductOptionImposed(product,
                                                    PRODUCT_OPTION_LIEN)
                                    && !isProductOptionImposed(product,
                                                    PRODUCT_OPTION_RESTRICT_COVER)
                                    && !isSubstandExtraLoadingImposed(product)) {
                        throw new AppException(
                                        UwExceptionConstants.APP_SUBSTD_TERM_MISSING);
                    }
                }
            }
            if (isExclusionCodeImposed(exclusions)) {
                proposalStatus = ProposalStatusConstants.CONDITIONAL_ACCEPTED;
            } else if (isConditionCodeImposed(conditions)
                            && !isOnlyAdHocConditionImposed(conditions)) {
                proposalStatus = ProposalStatusConstants.CONDITIONAL_ACCEPTED;
            }
            // All condition
            else if (isProductsSameDecision(copiedProducts,
                            UwDecisionConstants.CONDITIONAL_UNDERWRITTEN)) {
                proposalStatus = ProposalStatusConstants.CONDITIONAL_ACCEPTED;
            }
            // Condition Or Decline Or Postponed
            else if (!isProductsSameDecision(copiedProducts,
                            UwDecisionConstants.STANDARD_CASE_UNDERWRITTEN)) {
                proposalStatus = ProposalStatusConstants.CONDITIONAL_ACCEPTED;
            }
            // Accept
            else {
                String currentCountry = AppContext.getCurrentCountry();
                if (Cst.GELS.equals(currentCountry)) {
                    if (isConditionCodeImposed(conditions)
                                    && isOnlyAdHocConditionImposed(conditions)
                                    && !isExclusionCodeImposed(exclusions)
                                    && !isPolicyOptionImposed(copiedProducts,
                                                    PRODUCT_OPTION_LOADING_ALL)) {
                        proposalStatus = ProposalStatusConstants.ACCEPTED;
                    }
                } else
                // if lien imposed then decision will be CA
                if (isPolicyOptionImposed(copiedProducts, PRODUCT_OPTION_LIEN)) {
                    proposalStatus = ProposalStatusConstants.CONDITIONAL_ACCEPTED;
                } else
                // if restrict coverage imposed then decision will be CA
                if (isPolicyOptionImposed(copiedProducts,
                                PRODUCT_OPTION_RESTRICT_COVER)) {
                    proposalStatus = ProposalStatusConstants.CONDITIONAL_ACCEPTED;
                } else {
                    // get Decision from config table
                    // endorsement code will not has any impact on the policy
                    // status
                    UwDecisionConfig decisionConfig = new UwDecisionConfig();
                    boolean policyOptionImposed = isPolicyOptionImposed(
                                    copiedProducts,
                                    PRODUCT_OPTION_HEALTH);
                    boolean policyOptionImposed2 = isPolicyOptionImposed(
                                    copiedProducts,
                                    PRODUCT_OPTION_OCCUPATION);
                    boolean policyOptionImposed3 = isPolicyOptionImposed(
                                    copiedProducts,
                                    PRODUCT_OPTION_AVOCATION);
                    boolean policyOptionImposed4 = isPolicyOptionImposed(
                                    copiedProducts,
                                    PRODUCT_OPTION_RESIDENT);
                    boolean policyOptionImposed5 = isPolicyOptionImposed(
                                    copiedProducts,
                                    PRODUCT_OPTION_AVIATION);
                    boolean policyOptionImposed6 = isPolicyOptionImposed(
                                    copiedProducts,
                                    PRODUCT_OPTION_OTHERS);
                    String[] loadings = decisionConfig.getExtraLodings(
                                    policyOptionImposed, policyOptionImposed2,
                                    policyOptionImposed3,
                                    policyOptionImposed4, policyOptionImposed5,
                                    policyOptionImposed6);
                    Integer decision = decisionConfig.findDecision(false,
                                    false, false,
                                    false, false, loadings, currentCountry);
                    if (null != decision) {
                        proposalStatus = decision;
                    }
                }
            }
        }
        return proposalStatus;
    }

    /**
     * check whether any substand extra loading has been imposed
     * 
     * @param product
     * @return
     * @throws GenericException
     */
    protected boolean isSubstandExtraLoadingImposed(UwProduct product)
                    throws GenericException {
        return isProductOptionImposed(product, PRODUCT_OPTION_HEALTH)
                        || isProductOptionImposed(product,
                                        PRODUCT_OPTION_OCCUPATION)
                        || isProductOptionImposed(product,
                                        PRODUCT_OPTION_AVOCATION)
                        || isProductOptionImposed(product,
                                        PRODUCT_OPTION_RESIDENT)
                        || isProductOptionImposed(product,
                                        PRODUCT_OPTION_AVIATION)
                        || isProductOptionImposed(product,
                                        PRODUCT_OPTION_OTHERS);
    }

    /**
     * Change proposal status according to the action
     * 
     * @param action Action Code[100-start, 200-suspend, 300-abort,
     *          400-activate,500-commit(submit, back to detail reg and re-expert
     *          uw using commit)]
     * @param policyId Policy Id
     * @param operatorId Id of the underwriter
     * @throws GenericException Application Exception
     */
    @PAPubAPIUpdate("update for loadPolicyByPolicyId")
    public void changeProposalStatus(int action, Long policyId,
                    Long operatorId,
                    boolean magnumAcceptInd) throws GenericException {
        try {
            com.ebao.ls.pa.pub.vo.PolicyVO policyVO = policyService
                            .loadPolicyByPolicyId(policyId);
            Integer policyStatus = policyVO.getProposalStatus();
            Long processorId = null;
            String decisionType = null; // decisionType 1-success, 2-conditional
            // success, 3-reject, 4-postpone
            String note = "";
            Integer curStatus = policyStatus;
            int manualUwAction = 0;
            boolean needDetailReg = false;
            boolean needUw = false;
            switch (action) {
                case ProposalStatusConstants.ACTION_START:// 31-->32
                    if (!ProposalStatusConstants.WAITING_FOR_UNDERWRITING
                                    .equals(policyStatus)) {
                        return;
                    }
                    manualUwAction = 100;
                    note = "Start underwriting.";
                    break;
                case ProposalStatusConstants.ACTION_CANCEL:// 32-->31
                    if (!ProposalStatusConstants.UNDERWRITING_IN_PROGRESS
                                    .equals(policyStatus)) {
                        return;
                    }
                    manualUwAction = 300;
                    note = "Cancel underwriting.";
                    break;
                case ProposalStatusConstants.ACTION_DATA_ENTRY:// 32-->10
                    if (!ProposalStatusConstants.UNDERWRITING_IN_PROGRESS
                                    .equals(policyStatus)) {
                        return;
                    }
                    manualUwAction = 500;
                    note = "Back to data entry.";
                    needDetailReg = true;
                    break;
                case ProposalStatusConstants.ACTION_MANUAL_ESCALATE:// 32-->31
                    if (!ProposalStatusConstants.UNDERWRITING_IN_PROGRESS
                                    .equals(policyStatus)) {
                        return;
                    }
                    manualUwAction = 300;
                    note = "Manual Escalated underwriting.";
                    break;
                case ProposalStatusConstants.ACTION_ESCALATED:// 32-->31
                    if (!ProposalStatusConstants.UNDERWRITING_IN_PROGRESS
                                    .equals(policyStatus)) {
                        return;
                    }
                    manualUwAction = 300;
                    note = "Escalated underwriting.";
                    break;
                case ProposalStatusConstants.ACTION_ACCEPT:// 32-->80
                    // added by hendry .xu to fix the defect of GEL00019076
                    if (!magnumAcceptInd
                                    && !ProposalStatusConstants.UNDERWRITING_IN_PROGRESS
                                                    .equals(policyStatus)) {
                        return;
                    }
                    manualUwAction = 500;
                    note = "Accepted underwriting.";
                    decisionType = "1";
                    break;
                case ProposalStatusConstants.ACTION_CONDITIONAL_ACCEPT:// 32--81
                    // added by hendry .xu to fix the defect of GEL00019076
                    if (!magnumAcceptInd
                                    && !ProposalStatusConstants.UNDERWRITING_IN_PROGRESS
                                                    .equals(policyStatus)) {
                        return;
                    }
                    manualUwAction = 500;
                    note = "Conditional accepted Underwriting.";
                    decisionType = "2";
                    break;
                case ProposalStatusConstants.ACTION_DECLINED:// 32--82
                    if (!ProposalStatusConstants.UNDERWRITING_IN_PROGRESS
                                    .equals(policyStatus)) {
                        return;
                    }
                    manualUwAction = 500;
                    note = "Declined underwriting.";
                    decisionType = "3";
                    break;
                case ProposalStatusConstants.ACTION_POSTPONED:// 32--84
                    if (!ProposalStatusConstants.UNDERWRITING_IN_PROGRESS
                                    .equals(policyStatus)) {
                        return;
                    }
                    manualUwAction = 500;
                    note = "Postponed underwriting.";
                    decisionType = "4";
                    break;
                case ProposalStatusConstants.ACTION_WITHDRAW:// 32--86
                    if (!ProposalStatusConstants.UNDERWRITING_IN_PROGRESS
                                    .equals(policyStatus)) {
                        return;
                    }
                    manualUwAction = 500;
                    note = "withdraw underwriting.";
                    decisionType = "5";
                    break;
                case ProposalStatusConstants.ACTION_ACTIVE: //
                    if (!ProposalStatusConstants.UNDERWRITING_IN_PROGRESS
                                    .equals(policyStatus)) {
                        return;
                    }
                    processorId = Long.valueOf(AppContext.getCurrentUser()
                                    .getUserId());
                    manualUwAction = 400;
                    break;
                // added to fix defect GEL00023549 ning.qi 2007-05-17
                case ProposalStatusConstants.ACTION_CANCEL_UW_IN_PROGRESS: // 32
                                                                           // --
                    // 32
                    if (!ProposalStatusConstants.UNDERWRITING_IN_PROGRESS
                                    .equals(policyStatus)) {
                        return;
                    }
                    manualUwAction = 200;
                    note = "Cancel underwriting.";
                    break;
            }
            /**
             * Action definition: 100: 31 - 32 200: 32 - 32(lock) 300: 32 - 31 400: 32
             * - 32(unlock) 500: 32 - other status needDetailReg: 32 - 20(back to data
             * entry) needUw:32 - 31(Re-Expert) decisionType:1 - 80 2 - 81 3 - 82 4 -
             * 84
             */
            proposalCI.processManualUw(manualUwAction, policyId, curStatus,
                            note,
                            needDetailReg, needUw, decisionType, operatorId,
                            processorId);
        } catch (GenericException ge) {
            throw new AppException(
                            UwExceptionConstants.APP_SYNC_NB_PROPOSAL_STATUS_FAILURE,
                            ge);
        }
    }

    /**
     * Export Underwriting result into NBU module
     * 
     * @param underwriteId Underwrite Id
     * @throws GenericException Application Exception
     * @author jason.luo
     * @since 10.18.2004
     */
    protected void exportUwResultIntoNBU(Long underwriteId)
                    throws GenericException {
        try {
            CompleteUWProcessSp.exportUwResultIntoNBU(underwriteId);
        } catch (GenericException ge) {
            throw new SysException(
                            UwExceptionConstants.APP_BACK_TO_DATA_ENTRY_FAILURE,
                            ge);
        }
    }

    /**
     * Update CS Application corresponding status
     * 
     * @param underwriteId Underwrite Id
     * @param underwriterId Underwriter Id
     * @param uwStatus Underwriting Status
     * @param appType Application Type
     * @throws GenericException Application Exception
     * @author jason.luo
     * @since 11.03.2004
     * @version 1.0
     */
    public void updateReUwPolicyUwStatus(Long underwriteId, Long underwriterId,
                    String uwStatus, String appType, Long changeId)
                    throws GenericException {
        getUwQueryDao().updateReUwPolicyUwStatus(underwriteId, underwriterId,
                        uwStatus, appType, changeId);
    }

    /**
     * update product's status
     * 
     * @param products
     * @param uwStatus
     * @throws GenericException
     */
    protected void updateProductStatus(List<UwProduct> products, String uwStatus)
                    throws GenericException {
        for (UwProduct uwProduct : products) {
            uwProduct.setUwStatus(uwStatus);
        }
    }

    /**
     * cancel maturity when policy is delcined or postponed
     * 
     * @param action
     * @param policyId
     * @throws GenericException
     */
    public void handleMaturityCancel(int action, Long policyId)
                    throws GenericException {
        if (action == ProposalStatusConstants.ACTION_DECLINED
                        || action == ProposalStatusConstants.ACTION_POSTPONED
                        || action == ProposalStatusConstants.ACTION_WITHDRAW) {
            CompleteUWProcessSp.maturityCancel(policyId);
        }
    }

    /**
     * This method is add by hanzhong.yan for defect GEL00040126 , this is used to
     * log some useful info for anlalising the odd defect
     * 
     * @author hanzhong.yan
     */
    public void logUsefulInfoForDebug(String info, Object obj) {
        String logHeader = "[Info for analising defect begin]-----";
        String logTail = "[Info for analising defect end]";
        String uwPolicySnapshot = obj != null ? "[" + obj.getClass().getName()
                        + " snapshot:" + describe(obj) + "]"
                        : "The object want to be logged is null.";
        StringBuffer buf = new StringBuffer(logHeader).append(info);
        buf.append("\r\n");
        buf.append(uwPolicySnapshot);
        buf.append("\r\n");
        buf.append(logTail);
    }

    /**
     * Common method write for print VO's snapshot
     * 
     * @author hanzhong.yan
     * @param obj
     * @return
     */
    public static Map describe(Object obj) {
        Map objSnapshot = new HashMap();
        Class clazz = obj.getClass();
        Field[] fields = clazz.getDeclaredFields();
        if (fields != null && fields.length > 0) {
            for (Field field : fields) {
                String fieldName = field.getName();
                objSnapshot.put(fieldName, getFieldValue(fieldName, obj, clazz));
            }
        }
        return objSnapshot;
    }

    private static Object getFieldValue(String fieldName, Object obj,
                    Class clazz) {
        String result = null;
        char[] fieldNameChars = fieldName.toCharArray();
        fieldNameChars[0] = Character.toUpperCase(fieldNameChars[0]);
        fieldName = new String(fieldNameChars);
        String methodName = "get" + fieldName;
        try {
            Method method = clazz.getDeclaredMethod(methodName, new Class[] {});
            Object dispatched = method.invoke(obj, new Object[] {});
            result = dispatched == null ? null : dispatched.toString();
        } catch (Exception e) {
            // nothing to do
        }
        return result;
    }

    /**
     * when exist coversion info,generate endorsement code
     * 
     * @param policyId
     * @param underwriteId
     * @throws GenericException
     */
    protected void generateEndorseMentCode4Hipolicy(Long policyId,
                    Long underwriteId) throws GenericException {}

    protected UwInsuredVO[] getInsuredByPolicyId(Long policyId,
                    Long underwriteId)
                    throws GenericException {
        Long unfinishedUnderwriteId = getUwQueryDao().getUnfinishedNBUW(
                        policyId);
        // if policy level,get indicator from t_customer,else get info from
        // t_uw_product
        if (unfinishedUnderwriteId != null && underwriteId != null
                        && Long.valueOf(0).compareTo(underwriteId) < 0) {
            Collection uwLifeInsureds = getUwPolicyDS()
                            .findUwLifeInsuredEntitis(
                                            underwriteId);
            UwLifeInsured[] uwLifeInsured = (UwLifeInsured[]) uwLifeInsureds
                            .toArray(new UwLifeInsured[uwLifeInsureds.size()]);
            UwInsuredVO[] insuredVOs = null;
            if (uwLifeInsured != null && uwLifeInsured.length > 0) {
                try {
                    insuredVOs = (UwInsuredVO[]) BeanUtils.copyArray(
                                    UwInsuredVO.class,
                                    uwLifeInsured);
                } catch (Exception ex) {
                    throw ExceptionFactory.parse(ex);
                }
            }
            return insuredVOs;
        } else {
            List<InsuredVO> insuredvos = policyRoleCI
                            .findValidInsuredsByPolicyId(policyId);
            UwInsuredVO[] insuredVOs = null;
            if (insuredvos != null && insuredvos.size() > 0) {
                try {
                    insuredVOs = (UwInsuredVO[]) BeanUtils.copyArray(
                                    UwInsuredVO.class,
                                    insuredvos);
                } catch (Exception ex) {
                    throw ExceptionFactory.parse(ex);
                }
            }
            return insuredVOs;
        }
    }

    private void coverageVO2UwProductVO(CoverageVO paVO, UwProductVO uwVO)
                    throws GenericException {
        uwVO.setItemId(paVO.getItemId());
        uwVO.setApplyDate(paVO.getApplyDate());
        uwVO.setAutoPermntLapse(paVO.getAutoPermntLapse());
        uwVO.setBenefitPeriod(paVO.getBenefitPeriod());
        uwVO.setBonusSa(paVO.getBonusSa());
        uwVO.setCashCost(paVO.getCashCost());
        uwVO.setChargePeriod(paVO.getChargePeriod());
        uwVO.setChargeYear(paVO.getChargeYear());
        uwVO.setCoveragePeriod(paVO.getCoveragePeriod());
        uwVO.setCoverageYear(paVO.getCoverageYear());
        uwVO.setCpfCost(paVO.getCpfCost());
        uwVO.setDecisionId(paVO.getDecisionId());
        uwVO.setDeferPeriod(paVO.getDeferPeriod());
        uwVO.setPayMode(paVO.getCurrentPremium().getPaymentMethod());
        uwVO.setDerivation(paVO.getDerivation());
        uwVO.setEndCause(paVO.getEndCause());
        uwVO.setEntityFund(paVO.getEntityFund());
        uwVO.setExpiryCashValue(paVO.getExpiryCashValue());
        uwVO.setGurntPerdType(paVO.getGurntPerdType());
        uwVO.setGurntPeriod(paVO.getGurntPeriod());
        uwVO.setGurntStartDate(paVO.getGurntStartDate());
        uwVO.setInsuredCategory(paVO.getInsuredCategory());
        uwVO.setIssueAgent(paVO.getIssueAgent());
        if (CodeCst.LIABILITY_STATUS__LAPSED == paVO.getRiskStatus().intValue()) {
            uwVO.setLapseCause(paVO.getRiskStatusCause());
            uwVO.setLapseDate(paVO.getRiskStatusDate());
        }
        uwVO.setLoanType(paVO.getLoanType());
        uwVO.setMasterId(paVO.getMasterId());
        uwVO.setMasterProduct(paVO.getMasterProduct());
        uwVO.setOriginSa(paVO.getOriginSa());
        uwVO.setPaidupDate(paVO.getPaidupDate());
        uwVO.setPermntLapseNoticeDate(paVO.getPermntLapseNoticeDate());
        uwVO.setPremChangeTime(paVO.getPremChangeTime());
        uwVO.setProductId(paVO.getProductId());
        uwVO.setProductVersionId(paVO.getProductVersionId());
        uwVO.setRenewDecision(paVO.getRenewDecision());
        uwVO.setRiskCommenceDate(paVO.getRiskCommenceDate());
        uwVO.setShortEndTime(paVO.getShortEndTime());
        uwVO.setSubmissionDate(paVO.getSubmissionDate());
        uwVO.setSuspend(paVO.getSuspend());
        uwVO.setSuspendCause(paVO.getSuspendCause());
        uwVO.setSuspendChgId(paVO.getSuspendChgId());
        uwVO.setWaitPeriod(paVO.getWaitPeriod());
        uwVO.setPolicyId(paVO.getPolicyId());
        uwVO.setLiabilityState(paVO.getRiskStatus());
        uwVO.setEndDate(paVO.getExpiryDate());
        if (CodeCst.LIABILITY_STATUS__LAPSED == paVO.getRiskStatus()) {
            uwVO.setLapseDate(paVO.getRiskStatusDate());
            uwVO.setLapseCause(paVO.getRiskStatusCause());
        } else if (CodeCst.LIABILITY_STATUS__TERMINATED == paVO.getRiskStatus()) {
            uwVO.setEndDate(paVO.getRiskStatusDate());
            uwVO.setEndCause(paVO.getRiskStatusCause());
        } else if (CodeCst.LIABILITY_STATUS__IN_FORCE == paVO.getRiskStatus()) {
            uwVO.setLapseDate(null);
            uwVO.setLapseCause(null);
            uwVO.setEndCause(null);
        }
        uwVO.setValidateDate(paVO.getInceptionDate());
        uwVO.setActualValidate(paVO.getActualInceptionDate());
        uwVO.setManualSa(paVO.getManualSaFlag());
        uwVO.setBenefitLevel(paVO.getCurrentPremium().getBenefitLevel());
        uwVO.setCountWay(paVO.getCurrentPremium().getCountWay());
        uwVO.setDiscntPremAf(paVO.getCurrentPremium().getDiscntPremAf());
        uwVO.setDiscntPremAn(paVO.getCurrentPremium().getDiscntPremAn());
        uwVO.setDiscntPremBf(paVO.getCurrentPremium().getDiscntPremBf());
        uwVO.setDiscntedPremAf(paVO.getCurrentPremium().getDiscntedPremAf());
        uwVO.setDiscntedPremAn(paVO.getCurrentPremium().getDiscntedPremAn());
        uwVO.setDiscntedPremBf(paVO.getCurrentPremium().getDiscntedPremBf());
        uwVO.setExtraPremAf(paVO.getCurrentPremium().getExtraPremAf());
        uwVO.setExtraPremAn(paVO.getCurrentPremium().getExtraPremAn());
        uwVO.setExtraPremBf(paVO.getCurrentPremium().getExtraPremBf());
        uwVO.setGrossPremAf(paVO.getCurrentPremium().getGrossPremAf());
        uwVO.setPolicyFeeAf(paVO.getCurrentPremium().getPolicyFeeAf());
        uwVO.setPolicyFeeAn(paVO.getCurrentPremium().getPolicyFeeAn());
        uwVO.setPolicyFeeBf(paVO.getCurrentPremium().getPolicyFeeBf());
        uwVO.setStdPremAf(paVO.getCurrentPremium().getStdPremAf());
        uwVO.setStdPremAn(paVO.getCurrentPremium().getStdPremAn());
        uwVO.setStdPremBf(paVO.getCurrentPremium().getStdPremBf());
        uwVO.setTotalPremAf(paVO.getCurrentPremium().getTotalPremAf());
        uwVO.setUnit(paVO.getCurrentPremium().getUnit());
        // for product sell by unit then amount may be null,but for uw_product
        // ,amount cannot be null
        if (paVO.getCurrentPremium().getSumAssured() != null) {
            uwVO.setAmount(paVO.getCurrentPremium().getSumAssured());
        } else {
            uwVO.setAmount(new BigDecimal(0));
        }
        uwVO.setInitialType(paVO.getCurrentPremium().getPaymentFreq());
        uwVO.setNextDiscntPremAf(paVO.getNextPremium().getDiscntPremAf());
        uwVO.setNextDiscntPremAn(paVO.getNextPremium().getDiscntPremAn());
        uwVO.setNextDiscntPremBf(paVO.getNextPremium().getDiscntPremBf());
        uwVO.setNextDiscntedPremAf(paVO.getNextPremium().getDiscntedPremAf());
        uwVO.setNextDiscntedPremAn(paVO.getNextPremium().getDiscntedPremAn());
        uwVO.setNextDiscntedPremBf(paVO.getNextPremium().getDiscntedPremBf());
        uwVO.setNextTotalPremAf(paVO.getNextPremium().getTotalPremAf());
        uwVO.setNextExtraPremAn(paVO.getNextPremium().getExtraPremAn());
        uwVO.setRenewalType(paVO.getNextPremium().getPaymentFreq());
        uwVO.setNextStdPremBf(paVO.getNextPremium().getStdPremBf());
        uwVO.setNextPolicyFeeBf(paVO.getNextPremium().getPolicyFeeBf());
        uwVO.setNextExtraPremBf(paVO.getNextPremium().getExtraPremBf());
        uwVO.setNextStdPremAf(paVO.getNextPremium().getStdPremAf());
        uwVO.setNextPolicyFeeAf(paVO.getNextPremium().getPolicyFeeAf());
        uwVO.setNextGrossPremAf(paVO.getNextPremium().getGrossPremAf());
        uwVO.setNextExtraPremAf(paVO.getNextPremium().getExtraPremAf());
        uwVO.setNextStdPremAn(paVO.getNextPremium().getStdPremAn());
        uwVO.setNextPolicyFeeAn(paVO.getNextPremium().getPolicyFeeAn());
        uwVO.setPayNext(paVO.getNextPremium().getPaymentMethod());
        uwVO.setNextAmount(paVO.getNextPremium().getSumAssured());
        uwVO.setReinsRate(paVO.getRiExt().getReinsRate());
        uwVO.setExposureRate(paVO.getRiExt().getExposureRate());
        uwVO.setWaivAnulBenefit(paVO.getWaiverExt().getWaivAnulBenefit());
        uwVO.setWaivAnulPrem(paVO.getWaiverExt().getWaivAnulPrem());
        uwVO.setWaivedSa(paVO.getWaiverExt().getWaivedSa());
        uwVO.setWaiver(paVO.getWaiverExt().getWaiver());
        uwVO.setWaiverEnd(paVO.getWaiverExt().getWaiverEnd());
        uwVO.setWaiverStart(paVO.getWaiverExt().getWaiverStart());
        uwVO.setSaFactor(paVO.getVulExt().getSaFactor());
        uwVO.setExceptValue(paVO.getVulExt().getExceptValue());
        uwVO.setFixIncrement(paVO.getVulExt().getFixIncrement());
        uwVO.setInvestHorizon(paVO.getVulExt().getInvestmentHorizon());
        uwVO.setStrategyCode(paVO.getVulExt().getInvestmentStrategyCode());
        uwVO.setAnniBalance(paVO.getVulExt().getAnniversaryBalance());
        uwVO.setIlpCalcSa(paVO.getVulExt().getIlpCalculatedSa());
        uwVO.setPolicyPremSource(paVO.getPolicyFeeSource());
        uwVO.setLiabilityState(paVO.getRiskStatus());
        if (CodeCst.LIABILITY_STATUS__TERMINATED == paVO.getRiskStatus()) {
            uwVO.setEndCause(paVO.getEndCause());
            uwVO.setEndDate(paVO.getRiskStatusDate());
        } else {
            uwVO.setEndDate(paVO.getExpiryDate());
        }
        if (CodeCst.LIABILITY_STATUS__LAPSED == paVO.getRiskStatus()) {
            uwVO.setLapseCause(paVO.getLapseCause());
            uwVO.setLapseDate(paVO.getRiskStatusDate());
        }
        // Bonus
        PayPlanVO bonusPay = paVO.getCashbonusPayPlan();
        if (bonusPay == null) {
            bonusPay = paVO.getReversionaryBonusPayPlan();
        }
        if (bonusPay != null) {
            uwVO.setBonusDueDate(bonusPay.getPayDueDate());
            if (bonusPay.getPayOption() != null) {
                uwVO.setDividendChoice(Integer.valueOf(bonusPay.getPayOption()));
            }
            uwVO.setDividendOptTerm(bonusPay.getPayOptionTerm());
        }
        // Annuity
        PayPlanVO abPay = paVO.getAnnuityPayPlan();
        if (abPay != null) {
            uwVO.setEndPeriod(abPay.getEndPeriod());
            uwVO.setEndYear(abPay.getEndYear());
            uwVO.setIncreaseFreq(abPay.getIncreaseFreq());
            uwVO.setIncreaseRate(abPay.getIncreaseRate());
            uwVO.setIncreaseYear(abPay.getIncreaseYear());
            uwVO.setPayPeriod(abPay.getPayPeriod());
            uwVO.setPayYear(abPay.getPayYear());
            uwVO.setSimpleCompound(abPay.getSimpleCompound());
            uwVO.setPayType(abPay.getPlanFreq());
            uwVO.setPayEnsure(abPay.getGurntPeriod());
            uwVO.setStartPayDate(abPay.getStartDate());
            if (abPay.getPayDueDate() != null) {
                uwVO.setStartPay(abPay.getPayDueDate().after(
                                abPay.getStartDate()) ? "Y"
                                : "N");
            } else {
                uwVO.setStartPay("N");
            }
        }
        // Survival Benefit
        PayPlanVO sbPay = paVO.getSurvivalBenefitPayPlan();
        if (sbPay != null) {
        	// IR185240 年金 無starDate
        	if(sbPay.getStartDate()!=null){
        		uwVO.setStartInstDate(sbPay.getStartDate());
        	}
            // uwVO.setSurvivalOption(sbPay.getPayOption());
            uwVO.setSurvivalOptTerm(sbPay.getPayOptionTerm());
            if (sbPay.getPayDueDate() != null && sbPay.getStartDate() !=null) {
                uwVO.setStartPay(sbPay.getPayDueDate().after(
                                sbPay.getStartDate()) ? "Y"
                                : "N");
            } else {
                uwVO.setStartPay("N");
            }
        }
        // Life Insured 1
        CoverageInsuredVO coverageInsured1 = paVO.getLifeInsured1();
        if (coverageInsured1 != null) {
            InsuredVO lifeInsured1 = coverageInsured1.getInsured();
            uwVO.setInsured1(lifeInsured1.getPartyId());
            uwVO.setAge1(coverageInsured1.getEntryAge());
            uwVO.setAgeMonth(coverageInsured1.getEntryAgeMonth());
            uwVO.setRelation1(lifeInsured1.getRelationToPH());
            uwVO.setJob11(lifeInsured1.getOccupation1());
            uwVO.setJob12(lifeInsured1.getOccupation2());
            uwVO.setJobClass1(coverageInsured1.getOccupClass());
            uwVO.setJobCate1(lifeInsured1.getOccupCate());
            uwVO.setGender1(lifeInsured1.getGender());
            uwVO.setRetired(lifeInsured1.getRetiredFlag());
            uwVO.setLiveRange(lifeInsured1.getLiveRange());
            uwVO.setMoveRange(lifeInsured1.getMoveRange());
            uwVO.setBirthday(lifeInsured1.getBirthDate());
            uwVO.setInsuredStatus(lifeInsured1.getPreferredLifeIndi());
            uwVO.setIndustryId1(lifeInsured1.getIndustryType());
            uwVO.setHousekeeper(lifeInsured1.getHousekeeper());
            uwVO.setSmoking(lifeInsured1.getSmokerStatus());
            uwVO.setStandLife1(lifeInsured1.getStandLifeIndi());
            uwVO.setEmValue1(lifeInsured1.getEmValue());
            uwVO.setMedicalFlag2(lifeInsured1.getMedicalExamIndi());
        }
        // Life Insured 2
        CoverageInsuredVO coverageInsured2 = paVO.getLifeInsured2();
        if (coverageInsured2 != null) {
            InsuredVO lifeInsured2 = coverageInsured2.getInsured();
            uwVO.setInsured2(lifeInsured2.getPartyId());
            uwVO.setBirthday2(lifeInsured2.getBirthDate());
            uwVO.setAge2(coverageInsured2.getEntryAge());
            uwVO.setRelatedMonth(coverageInsured2.getEntryAgeMonth());
            uwVO.setRelation2(lifeInsured2.getRelationToPH());
            uwVO.setJob21(lifeInsured2.getOccupation1());
            uwVO.setJob22(lifeInsured2.getOccupation2());
            uwVO.setJobClass2(coverageInsured2.getOccupClass());
            uwVO.setJobCate2(lifeInsured2.getOccupCate());
            uwVO.setGender2(lifeInsured2.getGender());
            uwVO.setIndustryId2(lifeInsured2.getIndustryType());
            uwVO.setStandLife2(lifeInsured2.getStandLifeIndi());
            uwVO.setEmValue2(lifeInsured2.getEmValue());
            uwVO.setInsuredStatus2(lifeInsured2.getPreferredLifeIndi());
            uwVO.setSmokingRelated(lifeInsured2.getSmokerStatus());
            uwVO.setMedicalFlag1(lifeInsured2.getMedicalExamIndi());
        }

        // 因預收輸入險種繳費/繳費年期類型/保障年期類別 有可能填空,由OPQ欄出檢核訊息
        // artf242733 調整因繳費年期類型及年期空白造成的java/plsql錯誤
        if (StringUtils.isNullOrEmpty(uwVO.getCountWay())) {
            String countWay = coverageService.getCountWayByProductId(uwVO.getProductId().longValue());
            uwVO.setCountWay(countWay);
        }
        if (StringUtils.isNullOrEmpty(uwVO.getInitialType())) {
            uwVO.setInitialType(CodeCst.CHARGE_MODE__NA);
        }
        if (StringUtils.isNullOrEmpty(uwVO.getChargePeriod())) {
            uwVO.setChargePeriod(CodeCst.CHARGE_PERIOD__NA);
        }
        if (StringUtils.isNullOrEmpty(uwVO.getCoveragePeriod())) {
            uwVO.setCoveragePeriod(CodeCst.COVERAGE_PERIOD__NA);
        }
    }

    /**
     * update underwriting status and underwriting decision
     * 
     * @param underwriteId
     * @throws GenericException
     */
    public void updateUwStatusAndDecisionAndProposalStatus(Long underwriteId,
                    Long underwriterId, String uwStatus, Integer decisionId,
                    Integer proposalStatus, boolean isCancelLock)
                    throws GenericException {
        UwPolicy uwPolicy = uwPolicyDao.load(underwriteId);
        uwPolicy.setUwStatus(uwStatus);
        uwPolicy.setUwDecision(decisionId);
        uwPolicy.setProposalDecision(proposalStatus);
        uwPolicy.setUnderwriterId(underwriterId);
        if (isCancelLock) {
            uwPolicy.setLockTime(null);
        }
        List<UwProduct> products = uwProductDao
                        .findByUnderwriteId(underwriteId);
        Iterator iter = products.iterator();
        while (iter.hasNext()) {
            UwProduct uwProduct = (UwProduct) iter.next();
            uwProduct.setDecisionId(decisionId);
            uwProduct.setUwStatus(uwStatus);
            uwProduct.setUnderwriterId(underwriterId);
        }
    }

    public void updateNBInfo(Long underwriteId, Long uwProposerId)
                    throws GenericException {
    	 
        /* 2017/07/24 發生以下錯誤,
         * com.ebao.foundation.core.RTException: convert error,
         * Unsupported Datatype:class java.util.Date-->class java.util.Date,
         * source:class java.sql.Date,propertyName:paidupDate
         * at com.ebao.foundation.module.util.beanutils.BeanUtils.convert0(BeanUtils.java:881)
         * 查看paidupDate的dstPropeertyType,srcPropertyType均為java.util.Data, 
         * 但 srcBean.get(key)取出java.sql.Date, 強制設定SupportSqlDateType=true,待複製結果restore原設定值
         * by simon.huang
         */
        boolean isSupportSqlDateType = BeanUtils.isSupportSqlDateType();
        BeanUtils.setSupportSqlDateType(true);
  	    NBUtils.logger(this.getClass(),"updateNBInfo( "+underwriteId+",  "+uwProposerId+") begin");
    	try{
            // load uw info 
    		
    		NBUtils.logger(this.getClass(),"uwPolicyDao.load("+underwriteId+") begin");
            UwPolicy uwPolicy = uwPolicyDao.load(underwriteId);
            NBUtils.logger(this.getClass(),"uwPolicyDao.load("+underwriteId+") end");
            
            Long policyId = uwPolicy.getPolicyId();
            // get policy information
            
            NBUtils.logger(this.getClass(),"policyService.loadPolicyByPolicyId("+policyId+") begin");
            PolicyVO policyVO = policyService.loadPolicyByPolicyId(policyId);
            NBUtils.logger(this.getClass(),"policyService.loadPolicyByPolicyId("+policyId+") end");
            
            NBUtils.logger(this.getClass(),"retrieveProductValues("+policyId+") begin");
            List<CoverageVO> policyProducts = retrieveProductValues(policyId);
            CoverageVO masterCoverage = policyVO.gitMasterCoverage();
            NBUtils.logger(this.getClass(),"retrieveProductValues("+policyId+") end");
            // convert policy&product's value to UW policy&product
           
            NBUtils.logger(this.getClass(),"convertPolicyValue("+policyId+") begin");
            UwPolicyVO newUwPolicyVO = convertPolicyValue(policyVO,
                            UwSourceTypeConstants.NEW_BIZ, uwProposerId,
                            policyProducts, null);
            NBUtils.logger(this.getClass(),"convertPolicyValue("+policyId+") end");
            
            // 陳核、核保決定綜合意見欄相關不可被覆蓋
            newUwPolicyVO.setUwTransferFlow(uwPolicy.getUwTransferFlow());
            newUwPolicyVO.setManuEscIndi(uwPolicy.getManuEscIndi());
            newUwPolicyVO.setUwStatus(uwPolicy.getUwStatus());
            newUwPolicyVO.setUwEscaUser(uwPolicy.getUwEscaUser());
            Date insertTime = uwPolicy.getInsertTime();
            Long commentVersionId = uwCommentService.getCommentVersionId(new Long(masterCoverage.getProductId()), policyVO.isOIU(), insertTime);
            newUwPolicyVO.setCommentVersionId(commentVersionId);
            newUwPolicyVO.setInsertTime(insertTime);
            newUwPolicyVO.setRegDate(uwPolicy.getRegDate());
            newUwPolicyVO.setScanDate(uwPolicy.getScanDate());

            BeanUtils.copyProperties(uwPolicy, newUwPolicyVO, false);

            // /* set the benefit type of master product into policy */
            for (UwProductVO newUwProduct : newUwPolicyVO.getUwProducts()) {
                if (newUwProduct.getMasterId() == null) {
                    Long productID = Long.valueOf(newUwProduct.getProductId()
                                    .longValue());
                    ProductVO basicInfo = this
                                    .getProductService()
                                    .getProductByVersionId(productID,
                                                    newUwProduct.getProductVersionId());
                    String benefitType = basicInfo.getBenefitType();
                    uwPolicy.setBenefitType(benefitType);
                }
            }
            // update T_WU_POLICY
            NBUtils.logger(this.getClass(),"uwPolicyDao.update() begin");
            uwPolicyDao.update(uwPolicy);
            NBUtils.logger(this.getClass(),"uwPolicyDao.update() end");
            // update T_UW_PRODUCT
            // put old uwProduct to map
            // set benefits status
            NBUtils.logger(this.getClass(),"uwProductDao.findByUnderwriteId("+underwriteId+") begin");
            List<UwProduct> products = uwProductDao
                            .findByUnderwriteId(underwriteId);
            NBUtils.logger(this.getClass(),"uwProductDao.findByUnderwriteId("+underwriteId+") end");
            
            Map<Long, UwProduct> uwProductMap = new HashMap<Long, UwProduct>();
            for (UwProduct uwProduct : products) {
                uwProductMap.put(uwProduct.getItemId(), uwProduct);
            }
            // check new UwPolicyVO need update or insert
            String discountType = newUwPolicyVO.getDiscountType();
            for (UwProductVO newUwProduct : newUwPolicyVO.getUwProducts()) {
                Long item_id = newUwProduct.getItemId();
                UwProduct uwProduct = uwProductMap.get(item_id);
                if (uwProductMap.containsKey(item_id)) {
                    BeanUtils.copyProperties(uwProduct, newUwProduct, false);
                    // update
                    uwProduct.setAmount(newUwProduct.getAmount() == null ? BigDecimal.ZERO : newUwProduct.getAmount());
                    uwProduct.setBenefitLevel(newUwProduct.getBenefitLevel());
                    uwProduct.setUnit(newUwProduct.getUnit() == null ? BigDecimal.ZERO : newUwProduct.getUnit());
                    uwProduct.setReducedAmount(uwProduct.getAmount());
                    uwProduct.setReducedUnit(uwProduct.getUnit());
                    uwProduct.setReducedLevel(uwProduct.getBenefitLevel());
                    uwProductDao.update(uwProduct);
                    // remove from map
                    uwProductMap.remove(item_id);
                } else {
                    newUwProduct.setUnderwriteId(underwriteId);
                    UwProduct vo = new UwProduct();
                    BeanUtils.copyProperties(vo, newUwProduct, false);
                    List<UwLienVO> uwLienVOs = newUwProduct.getUwLiens();
                    if (null != uwLienVOs && 0 < uwLienVOs.size()) {
                        List<UwLien> bos = (List<UwLien>) BeanUtils.copyCollection(
                                        UwLien.class,
                                        uwLienVOs);
                        vo.setUwLiens(bos);
                    }
                    List<UwExtraLoadingVO> uwExtraLoadingVOs = newUwProduct
                                    .getUwExtraLoadings();
                    if (null != uwExtraLoadingVOs && 0 < uwExtraLoadingVOs.size()) {
                        List<UwExtraLoading> bos = (List<UwExtraLoading>) BeanUtils
                                        .copyCollection(UwExtraLoading.class,
                                                        uwExtraLoadingVOs);
                        vo.setUwExtraLoadings(bos);
                    }
                    vo.setDiscountType(discountType);
                    // add
                    
                    NBUtils.logger(this.getClass(),"uwProductDao.create() begin");
                    uwProductDao.create(vo);
                    NBUtils.logger(this.getClass(),"uwProductDao.create() end");
                }
            }
            // delete uwPorduct
            for (Long item_id : uwProductMap.keySet()) {
                UwProduct uwProduct = uwProductMap.get(item_id);
                
                NBUtils.logger(this.getClass(),"uwProductDao.remove("+uwProduct.getUwListId()+") begin");
                uwProductDao.remove(uwProduct.getUwListId());
                NBUtils.logger(this.getClass(),"uwProductDao.remove("+uwProduct.getUwListId()+") end");
            }
            // update T_UW_INSURED_LIST
            Map<Long, UwLifeInsured> uwLifeInsuredMap = new HashMap<Long, UwLifeInsured>();
            
            NBUtils.logger(this.getClass(),"uwInsuredListDao.findByUnderwriteId("+underwriteId+") begin");
            List<UwLifeInsured> lifeInsureds = uwInsuredListDao
                            .findByUnderwriteId(underwriteId);
            NBUtils.logger(this.getClass(),"uwInsuredListDao.findByUnderwriteId("+underwriteId+") end");
            // put old uwInsuredList to map
            for (UwLifeInsured uwLifeInsured : lifeInsureds) {
                uwLifeInsuredMap.put(uwLifeInsured.getListId(), uwLifeInsured);
            }
            // check new UwPolicyVO need update or insert
            for (UwLifeInsuredVO newUwLifeInsured : newUwPolicyVO
                            .getUwLifeInsureds()) {
                Long list_id = newUwLifeInsured.getListId();
                UwLifeInsured uwLifeInsured = uwLifeInsuredMap.get(list_id);
                if (uwLifeInsuredMap.containsKey(list_id)) {
                    BeanUtils.copyProperties(uwLifeInsured, newUwLifeInsured, false);
                    // update
                	//PCR_391319 新式公會-新增被保險人受監護宣告  20201030 by Simon
                    //因copyProperties不作空值update,預設監護宣告由被保險人帶入可能是空白,需手動設定
                    uwLifeInsured.setGuardianAncmnt(newUwLifeInsured.getGuardianAncmnt());
                   
                    NBUtils.logger(this.getClass(),"uwInsuredListDao.update() begin");
                    uwInsuredListDao.update(uwLifeInsured);
                    NBUtils.logger(this.getClass(),"uwInsuredListDao.update() end");
                    // remove from map
                    uwLifeInsuredMap.remove(list_id);
                } else {
                    newUwLifeInsured.setUnderwriteId(underwriteId);
                    UwLifeInsured vo = new UwLifeInsured();
                    BeanUtils.copyProperties(vo, newUwLifeInsured, false);
                    // add
                	//PCR_391319 新式公會-新增被保險人受監護宣告  20201030 by Simon
                    //因copyProperties不作空值update,預設監護宣告由被保險人帶入可能是空白,需手動設定
                    vo.setGuardianAncmnt(newUwLifeInsured.getGuardianAncmnt());
                    
                    NBUtils.logger(this.getClass(),"uwInsuredListDao.create() begin");
                    uwInsuredListDao.create(vo);
                    NBUtils.logger(this.getClass(),"uwInsuredListDao.create() end");
                }
            }
            // delete uwLifeInsured
            for (Long list_id : uwLifeInsuredMap.keySet()) {
                UwLifeInsured uwLifeInsured = uwLifeInsuredMap.get(list_id);
                
                NBUtils.logger(this.getClass(),"uwInsuredListDao.remove("+uwLifeInsured.getUwListId()+") begin");
                uwInsuredListDao.remove(uwLifeInsured.getUwListId());
                NBUtils.logger(this.getClass(),"uwInsuredListDao.remove("+uwLifeInsured.getUwListId()+") end");
            }
            
    	}catch(Exception e){
    		throw new GenericException(GenericException.CODE_UNKNOW_SYS_ERROR ,e) ;
    	} finally {
    		//by simon.huang
            BeanUtils.setSupportSqlDateType(isSupportSqlDateType); 
            
            NBUtils.logger(this.getClass(),"updateNBInfo( "+underwriteId+",  "+uwProposerId+") end");
    	}
    }

    public void updateInsuredListDisability(UwLifeInsuredVO uwInsuredVO,boolean isNewbiz)
                    throws GenericException {
        InsuredVO insured = insuredService.load(uwInsuredVO.getListId());
        insured.setDisabilityType(uwInsuredVO.getDisabilityType());
        insured.setDisabilityCategory(uwInsuredVO.getDisabilityCategory());
        insured.setDisabilityClass(uwInsuredVO.getDisabilityClass());
        if(isNewbiz){
        	insuredService.save(insured);
        }else{
        	insuredService.saveAfterIssue(insured);
        }
        
    }

	public void cancelUnderwriting(java.lang.Long underwriteId)
				throws GenericException{
		UwPolicy uwPolicy = uwPolicyDao.load(underwriteId);
		uwPolicy.setUwStatus(UwStatusConstants.CANCELLATION);
		uwPolicy.setUnderwriterId(null);
		uwPolicyDao.update(uwPolicy);
		List<UwProduct> uwProducts = uwProductDao.findByUnderwriteId(underwriteId);
		if(uwProducts != null && uwProducts.size() > 0){
			for (UwProduct uwProduct : uwProducts) {
				uwProduct.setUwStatus(UwStatusConstants.CANCELLATION);
				uwProductDao.update(uwProduct);
			}
		}
	}
	public void updateUwStatus(java.lang.Long underwriteId, String uwStatus)
			throws GenericException{
	UwPolicy uwPolicy = uwPolicyDao.load(underwriteId);
	uwPolicy.setUwStatus(uwStatus);
	uwPolicyDao.update(uwPolicy);
	List<UwProduct> uwProducts = uwProductDao.findByUnderwriteId(underwriteId);
	if(uwProducts != null && uwProducts.size() > 0){
		for (UwProduct uwProduct : uwProducts) {
			uwProduct.setUwStatus(uwStatus);
			uwProductDao.update(uwProduct);
		}
	}
}
	
	/**
	 * IR 363066 修正受理次標加費批註加上其他，進入核保後批註與照會單顯示問題
	 * 保單有批註時，保全項非保全項完成，需帶回之前批註的資料
	 * @param preUnderwriteId
	 * @param uwPolicyVo
	 */
	private void reloadUwExclusionforCS(Long preUnderwriteId, UwPolicyVO uwPolicyVo){
		if(preUnderwriteId == null){ 
			return ;
		}
		try {
			logger.error("UwProcessDSImpl.reloadUwExclusion preUnderwriteId="+preUnderwriteId);
			Collection<UwExclusionVO> uwExclusions = uwPolicyDS.findUwExclusionEntitis(preUnderwriteId);
			List<UwEndorsementVO> uwEndorsements = uwPolicyDS.findUwEndorsementByUnderwriteId(preUnderwriteId);
			logger.error("UwProcessDSImpl.reloadUwExclusion.uwExclusions="+uwExclusions);
			logger.error("UwProcessDSImpl.reloadUwExclusion.uwEndorsements="+uwEndorsements);
			
			List<UwExclusionVO> uwExclusionList = new ArrayList<UwExclusionVO>();
			List<UwEndorsementVO> uwEndorsementList = new ArrayList<UwEndorsementVO>();
			if(uwExclusions.size()>0){
				Iterator iter = uwExclusions.iterator();
				while (iter.hasNext()) {
					UwExclusionVO vo = (UwExclusionVO) iter.next();
					vo.setMsgId(null);
					uwExclusionList.add(vo);
				}
			}
			
			if(uwEndorsements!=null && uwEndorsements.size()>0){
				Iterator iter2 = uwEndorsements.iterator();
				while (iter2.hasNext()) {
					UwEndorsementVO vo = (UwEndorsementVO) iter2.next();
					vo.setEndorsementId(null);
					 if(null == vo.getEndorsementVersion()) {
	                    	vo.setEndorsementVersion(2L);
	                    }
					uwEndorsementList.add(vo);
				}
			}
			logger.error("UwProcessDSImpl.reloadUwExclusion uwExclusionList"
					+",uwExclusionList="+uwExclusionList);
			logger.error("UwProcessDSImpl.reloadUwExclusion uwEndorsementList"
					+",uwEndorsementList="+uwEndorsements);
			uwPolicyVo.setUwExclusions(uwExclusionList);
			uwPolicyVo.setUwEndorsements(uwEndorsements);
		} catch (Exception e) {
			throw ExceptionFactory.parse(e);
		}
	}
	
    /**
     * IR 363066 修正受理次標加費批註加上其他，進入核保後批註與照會單顯示問題
     * @param uwPolicyVO
     * @return
     * @throws GenericException
     */
    private Long createUwPolicyforCS(UwPolicyVO uwPolicyVO) throws GenericException {
    	Long underwriteId  = null;
		UwPolicy uwPolicyBo = new UwPolicy();
		BeanUtils.copyProperties(uwPolicyBo, uwPolicyVO, false);
		Long policyId = uwPolicyVO.getPolicyId();
		// get discount type from t_uw_policy
		String discountType = uwPolicyVO.getDiscountType();
		/* copy product info from uwProductValues to uwproductEntities of bo */
		List<UwProductVO> uwproductValues = uwPolicyVO.getUwProducts();
		if (uwproductValues != null && uwproductValues.size() != 0) {
			UwProduct[] uwproductEntities = new UwProduct[uwproductValues.size()];
			for (int i = 0; i < uwproductValues.size(); i++) {
				UwProduct uwproductEntity = new UwProduct();
				BeanConvertor.convertBean(uwproductEntity, uwproductValues.get(i));
				/* load lien info */
				loadLienCS(uwproductValues.get(i).getUwLiens(), uwproductEntity);
				/* load extra loadings info */
				loadLoadingCS(uwproductValues.get(i).getUwExtraLoadings(), uwproductEntity);
				uwproductEntities[i] = uwproductEntity;
				/* set the benefit type of master product into policy */
				if (uwproductValues.get(i).getMasterId() == null) {
					Long productID = Long.valueOf(uwproductValues.get(i).getProductId().longValue());
					ProductVO basicInfo = this.getProductService().getProductByVersionId(productID,
							uwproductValues.get(i).getProductVersionId());
					String benefitType = basicInfo.getBenefitType();
					uwPolicyBo.setBenefitType(benefitType);
				}
				uwproductEntities[i].setDiscountType(discountType);
			}
			uwPolicyBo.setUwProducts(Arrays.asList(uwproductEntities));
		}
		
		/*
		 * copy product info from uwlifeinsuredValues to uwlifeinsuredEntities of bo
		 */
		List<UwLifeInsuredVO> uwlifeinsuredValues = uwPolicyVO.getUwLifeInsureds();
		if (uwlifeinsuredValues != null && uwlifeinsuredValues.size() != 0) {
			List<UwLifeInsured> uwlifeinsuredEntities = (List<UwLifeInsured>) BeanUtils
					.copyCollection(UwLifeInsured.class, uwlifeinsuredValues);
			uwPolicyBo.setUwLifeInsureds(uwlifeinsuredEntities);
		}
		
		underwriteId = uwPolicyDao.create(uwPolicyBo);
		
		List<UwProduct> uwproductValuesRp = uwPolicyBo.getUwProducts();
		if (uwproductValues != null) {
			for (UwProduct uwproductValue : uwproductValuesRp) {
				uwproductValue.setUnderwriteId(underwriteId);
				uwProductDao.create(uwproductValue);
				List<UwExtraLoading> uwextraloadingValues = uwproductValue.getUwExtraLoadings();
				if (uwextraloadingValues != null) {
					for (UwExtraLoading uwextraloadingValue : uwextraloadingValues) {
						UwExtraLoading loading = new UwExtraLoading();
						uwextraloadingValue.setUnderwriteId(underwriteId);
						BeanUtils.copyProperties(loading, uwextraloadingValue);
						uwExtraPremDao.create(loading);
					}
				}
				List<UwLien> uwlienValues = uwproductValue.getUwLiens();
				if (uwlienValues != null) {
					for (UwLien uwlienValue : uwlienValues) {
						UwLien uwlienEntity = new UwLien();
						uwlienValue.setUnderwriteId(underwriteId);
						BeanUtils.copyProperties(uwlienEntity, uwlienValue);
						uwReduceDao.create(uwlienEntity);
					}
				}
			}
		}
		
		List<UwLifeInsured> uwlifeinsuredValuesRp = uwPolicyBo.getUwLifeInsureds();
		if (uwlifeinsuredValues != null) {
			for (UwLifeInsured uwlifeinsuredValue : uwlifeinsuredValuesRp) {
				UwLifeInsured uwLifeInsured = new UwLifeInsured();
				uwlifeinsuredValue.setUnderwriteId(underwriteId);
				BeanUtils.copyProperties(uwLifeInsured, uwlifeinsuredValue);
				uwInsuredListDao.create(uwLifeInsured);
			}
		}
		 return underwriteId;
    }

	/**
	 * Attach loading info into product bo
	 *
	 * @param uwExtraLoadingVOs
	 * @param uwProduct
	 * @throws Exception
	 */
	protected void loadLoadingCS(List<UwExtraLoadingVO> uwExtraLoadingVOs, UwProduct uwProduct) {
		logger.info("UwPolicyDSImpl.loadLoading. = " + uwExtraLoadingVOs);
		if (null != uwExtraLoadingVOs && 0 < uwExtraLoadingVOs.size()) {
			List<UwExtraLoading> bos = (List<UwExtraLoading>) BeanUtils.copyCollection(UwExtraLoading.class,
					uwExtraLoadingVOs);
			uwProduct.setUwExtraLoadings(bos);

		}
	}

	/**
	 * Attach lien info into product bo
	 *
	 * @param uwLienVOs
	 * @param uwProduct
	 * @throws Exception
	 */
	protected void loadLienCS(List<UwLienVO> uwLienVOs, UwProduct uwProduct) {
		if (null != uwLienVOs && 0 < uwLienVOs.size()) {
			List<UwLien> bos = (List<UwLien>) BeanUtils.copyCollection(UwLien.class, uwLienVOs);
			uwProduct.setUwLiens(bos);
		}
	}
	
    /**
     * IR 363066 修正受理次標加費批註加上其他，進入核保後批註與照會單顯示問題
     * @param policyId
     * @param changeId
     * @param csTransactions
     * @param uwProposerId
     * @param items
     * @throws GenericException
     */
    public void initUwData(Long policyId, Long changeId,
            Integer csTransactions[], Long uwProposerId, Long items[])
            throws GenericException {
    	
        if (null == policyId || null == changeId || null == csTransactions
                || csTransactions.length == 0) {
        		throw new IllegalArgumentException(
                    "PolicyId and changeId and csTransaction can't be null.");
		}
		// get policy info from t_contract_master by NB Interface
		PolicyVO policyVO = retrievePolicyValue(policyId);
		// get product info
		List<CoverageVO> policyProducts = retrieveProductValues(policyId);
		// generate uw policy & product and etc.
		UwPolicyVO policyValue = convertPolicyValue(policyVO,
		                UwSourceTypeConstants.CS, uwProposerId, policyProducts,
		                items);
		// add supplementary value
		policyValue.setChangeId(changeId);
		// get manual uw indicator from the latest uw
		String manualUPCIndicator = getUwQueryDao().getCSManualUPCIndicator(
		                policyId);
		policyValue.setManualUwIndi(manualUPCIndicator);
		// create underwriting policy
		//IR 363066 修正受理次標加費批註加上其他，進入核保後批註與照會單顯示問題
		for (Integer csTransaction : csTransactions) {
		    UwPolicyVO uwPolicyVO = new UwPolicyVO();
		    try {
		        BeanUtils.copyProperties(uwPolicyVO, policyValue);
		    } catch (Exception e) {
		        throw ExceptionFactory.parse(e);
		    }
		    uwPolicyVO.setCsTransaction(csTransaction);
		    uwPolicyVO.setUwSourceType(getCsSrcType(csTransaction));
		
		    boolean hasExclusion = false; //保單無批註:false
			if(csTransaction == CodeCst.SERVICE__CHANGE_EXTRA_PREMIUM){
				List alterationItemList = TPolicyChangeDelegate.findByMasterChgId(changeId);
				HashMap<Integer, String> serviceMap = new HashMap<Integer, String>();
				for (int i = 0; i < alterationItemList.size(); i++) {
					AlterationItem alterationItem = (AlterationItem) alterationItemList.get(i);
					if(alterationItem.getServiceId() == CodeCst.SERVICE__CHANGE_EXTRA_PREMIUM
							&&!serviceMap.containsKey(alterationItem.getServiceId())){
						serviceMap.put(CodeCst.SERVICE__CHANGE_EXTRA_PREMIUM, "165");
						//IR 363066 修正受理次標加費批註加上其他，進入核保後批註與照會單顯示問題
						List<PolicyExclusionVO> policyExclusionVOBF = paPolicyExclusionService.getPolicyExclusionLogBFList(policyId, alterationItem.getPolicyChgId());
						if(policyExclusionVOBF!=null && policyExclusionVOBF.size()>0){
							hasExclusion =true;//保單有批註true
						}
						Long preUnderwriteId = uwPolicyDao.findPreUWIdByPolicyId(policyId);//找次標加費的前一筆
						reloadUwExtraLoading(preUnderwriteId, uwPolicyVO);
						if(hasExclusion){
							//保單有批註時，保全項非保全項完成，需帶回之前批註的資料
							reloadUwExclusionforCS(preUnderwriteId, uwPolicyVO);
						}
					}
				}
			}
			Long underwriteId  = null;
			if(hasExclusion){//保單有批註 true
				underwriteId = createUwPolicy(uwPolicyVO); //複製該批註
			}else{
				underwriteId= createUwPolicyforCS(uwPolicyVO);//創造新的uw
			}

		    // UwRiskAggregationDSDelegate.calcualteRiskAggregation(underwriteId);
		    com.ebao.ls.pa.pub.vo.PolicyVO policy = policyService
		                    .loadPolicyByPolicyId(policyId);
		    // 身心障礙 同步 T_UW_INSURED_DISABILITY
		    Collection<UwLifeInsuredVO> uwInsuredList = uwPolicyDS.findUwLifeInsuredEntitis(underwriteId);
		    this.createUwInsuredDisability(uwInsuredList);
		    
		    //風險累算時保全應提供policyChgId才能正確處理
		    raService.calcRA(policyId, applicationService.getPolicyChgIdForCalcRA(changeId));
		    raService.initPolicyRiskLevel(underwriteId, policy,
		                    uwPolicyVO.getUwSourceType(),
		                    CodeCst.PRODUCT_DECISION__ACCEPTED);
		}
    	
    }
    
    /**
     * 被保人身心障礙ICF編碼寫入核保資料
     * @param uwInsuredList
     * @throws GenericException
     */
	private void createUwInsuredDisability(Collection<UwLifeInsuredVO> uwInsuredList) throws GenericException {
		try {
			for (UwLifeInsuredVO uwInsuredVO : uwInsuredList) {
				Insured insured = (Insured) insuredDao.findByListId(uwInsuredVO.getListId());

				List<InsuredDisabilityList> disabilitys = insured.getInsuredDisabilityLists();
				for (InsuredDisabilityList dis : disabilitys) {

					//ICF編碼
					List<UwInsuredDisabilityIcf> uwIcfs = new ArrayList<UwInsuredDisabilityIcf>();
					for (InsuredDisabilityIcf icf : dis.getInsuredDisabilityIcfs()) {
						UwInsuredDisabilityIcf uwIcf = new UwInsuredDisabilityIcf();
						uwIcf.setDisabilityLevel(icf.getDisabilityLevel());
						uwIcfs.add(uwIcf);
					}

					//身心障礙類別
					UwInsuredDisability uwDis = new UwInsuredDisability();
					uwDis.setDisabilityType(dis.getDisabilityCode());
					uwDis.setUwInsuredId(uwInsuredVO.getUwListId());
					uwDis.addUwInsuredDisabilityIcfs(uwIcfs);
					uwInsuredDisabilityDao.create(uwDis);
				}
			}
		} catch (Exception e) {
			throw ExceptionFactory.parse(e);
		}
	}
    
    public List<Long> doCheckUwProducts(Long underwriteId, Long policyId)throws GenericException {
        List<UwProduct> uwProductList = uwProductDao.findByUnderwriteId(underwriteId);
        List<CoverageVO> policyProducts = retrieveProductValues(policyId);
        
        List<Long> existUwProdList = new ArrayList<Long>();
        List<Long> missingList = new ArrayList<Long>();
        if (uwProductList != null && uwProductList.size() > 0) {
            for (UwProduct uwProduct : uwProductList) {
            	existUwProdList.add(uwProduct.getItemId());
            }
        }
        
        // 若異動險種不在UWPRODUCT內, 就添加一件
		for (CoverageVO coverageVO : policyProducts) {
			if(coverageVO.getRiskStatus() == CodeCst.LIABILITY_STATUS__LAPSED || coverageVO.getRiskStatus() == CodeCst.LIABILITY_STATUS__TERMINATED){
				continue;
			}
			
			if (!existUwProdList.contains(coverageVO.getItemId())) {
				logger.warn("NEED ADD UW_PRODUCT item::" + coverageVO.getItemId());
				missingList.add(coverageVO.getItemId());
			}
		}
        
        return missingList;
    }
    
    public void doAddUwProducts(Long policyId, Long underwriteId, List<Long> addItemIdList)throws GenericException {
        if(!addItemIdList.isEmpty()){
        	long currUserId = AppContext.getCurrentUser().getUserId();
        	
        	PolicyVO policyVO = retrievePolicyValue(policyId);
        	List<CoverageVO> policyProducts = retrieveProductValues(policyId);
        	Long[] items = addItemIdList.toArray(new Long[addItemIdList.size()]);
        	
    		UwPolicyVO policyValue = convertPolicyValue(policyVO, UwSourceTypeConstants.CS, currUserId, policyProducts, items);
    		String discountType = policyValue.getDiscountType();
    		for(UwProductVO uwProductVO : policyValue.getUwProducts()){
    			uwProductVO.setUnderwriteId(underwriteId);
    			uwProductVO.setDiscountType(discountType);
    			
    			uwProductVO.setAmount(uwProductVO.getAmount() == null ? BigDecimal.ZERO : uwProductVO.getAmount());
    			uwProductVO.setBenefitLevel(uwProductVO.getBenefitLevel());
    			uwProductVO.setUnit(uwProductVO.getUnit() == null ? BigDecimal.ZERO : uwProductVO.getUnit());
    			uwProductVO.setReducedAmount(uwProductVO.getAmount());
    			uwProductVO.setReducedUnit(uwProductVO.getUnit());
    			uwProductVO.setReducedLevel(uwProductVO.getBenefitLevel());
    			
    			UwProduct uwproductValue = new UwProduct();
    			
                try {
                    BeanUtils.copyProperties(uwproductValue, uwProductVO);
                } catch (Exception e) {
                    throw ExceptionFactory.parse(e);
                }

                logger.warn(ToStringBuilder.reflectionToString(uwproductValue, ToStringStyle.MULTI_LINE_STYLE));
    			uwProductDao.create(uwproductValue);
    		}
        }
    }
    

    // spring bean for claim reference
    @Resource(name = ClaimService.BEAN_DEFAULT)
    private ClaimService claimService;

    @Resource(name = RiskAggregationService.BEAN_DEFAULT)
    private RiskAggregationService raService;

    @Resource(name = ProposalProcessService.BEAN_DEFAULT)
    private ProposalProcessService proposalProcessService;

    @Resource(name = PolicyCI.BEAN_DEFAULT)
    private PolicyCI policyCI;

    @Resource(name = CoverageCI.BEAN_DEFAULT)
    private CoverageCI coverageCI;

    @Resource(name = ReversalCI.BEAN_DEFAULT)
    private ReversalCI reversalCI;

    @Resource(name = DocumentService.BEAN_DEFAULT)
    private DocumentService documentService;

    @Resource(name = ProposalCI.BEAN_DEFAULT)
    private ProposalCI proposalCI;

    @Resource(name = CSCI.BEAN_DEFAULT)
    private CSCI csCI;

    @Resource(name = ApplicationService.BEAN_DEFAULT)
    private ApplicationService applicationService;
    
    @Resource(name = UwPolicyService.BEAN_DEFAULT)
    private UwPolicyService uwPolicyDS;

    public UwPolicyService getUwPolicyDS() {
        return uwPolicyDS;
    }

    @Resource(name = ProductService.BEAN_DEFAULT)
    private ProductService productService;

    public ProductService getProductService() {
        return productService;
    }

    @Resource(name = AgentService.BEAN_DEFAULT)
    private AgentService agentService;

    @Resource(name = PolicyRoleCI.BEAN_DEFAULT)
    private PolicyRoleCI policyRoleCI;

    @Resource(name = CustomerCI.BEAN_DEFAULT)
    private CustomerCI customerCI;

    @Resource(name = UwQueryDao.BEAN_DEFAULT)
    private UwQueryDao uwQueryDao;

    public UwQueryDao getUwQueryDao() {
        return uwQueryDao;
    }

    @Resource(name = TUwExtraPremDelegate.BEAN_DEFAULT)
    private TUwExtraPremDelegate uwExtraPremDao;

    @Resource(name = TUwReduceDelegate.BEAN_DEFAULT)
    private TUwReduceDelegate uwReduceDao;

    @Resource(name = TUwPolicyDelegate.BEAN_DEFAULT)
    private TUwPolicyDelegate uwPolicyDao;

    @Resource(name = TUwProductDelegate.BEAN_DEFAULT)
    private TUwProductDelegate uwProductDao;

    @Resource(name = TUwInsuredListDelegate.BEAN_DEFAULT)
    private TUwInsuredListDelegate uwInsuredListDao;

    @Resource(name = PolicyService.BEAN_DEFAULT)
    private PolicyService policyService;

    @Resource(name = UwCommentService.BEAN_DEFAULT)
    private UwCommentService uwCommentService;
    
    @Resource(name = UwElderCareService.BEAN_DEFAULT)
    private UwElderCareService uwElderCareService;

    @Resource(name = InsuredService.BEAN_DEFAULT)
    private InsuredService insuredService;

    @Resource(name = UwTransferService.BEAN_DEFAULT)
    private UwTransferService uwTransferService;

    @Resource(name = UserLeaveManagerService.BEAN_DEFAULT)
    private UserLeaveManagerService leaveManagerService;

    @Resource(name = TransferLevelTypeValidate.BEAN_DEFAULT)
    private TransferLevelTypeValidate transferLevelTypeValidate;

    @Resource(name = PolicyDao.BEAN_DEFAULT)
    private PolicyDao<Policy> policyDao;

    @Resource(name = CoverageService.BEAN_DEFAULT)
    protected CoverageService coverageService;

    @Resource(name = RiskCustomerCI.BEAN_DEFAULT)
    private RiskCustomerCI riskCustomerCI;

    @Resource(name = PolicyHolderService.BEAN_DEFAULT)
    private PolicyHolderService policyHolderService;

    @Resource(name = DeptService.BEAN_DEFAULT)
    private DeptService deptDS;

	@Resource(name = ProposalRuleResultService.BEAN_DEFAULT)
	private ProposalRuleResultService proposalRuleResultService;
	
    @Resource(name = TUwInsuredDisabilityDelegate.BEAN_DEFAULT)
    private TUwInsuredDisabilityDelegate uwInsuredDisabilityDao;

    @Resource(name = DisabilityCancelDetailService.BEAN_DEFAULT)
    private DisabilityCancelDetailService disabilityCancelDetailService;
    
    @Resource(name = InsuredDao.BEAN_DEFAULT)
    private InsuredDao insuredDao;
    
    @Resource(name = GenericCXDao.BEAN_POLICY_EXCLUSION_LOG_BF)
    private GenericCXDao<PolicyExclusionCX> paPolicyExclusionLogBFDao;
    
    @Resource(name = PolicyExclusionService.BEAN_DEFAULT)
    private PolicyExclusionService paPolicyExclusionService;
    
	@Resource(name = InsuredDisabilityListService.BEAN_DEFAULT)
	protected InsuredDisabilityListService insuredDisabilityListService;
	
    @Resource(name = UwInsuredDisabilityService.BEAN_DEFAULT)
    private UwInsuredDisabilityService uwInsuredDisabilityService;

	@SuppressWarnings("deprecation")
	@Override
	public void update(UwLifeInsuredVO vo) {
		UwLifeInsured entity = uwInsuredListDao.load(vo.getUwListId());
		BeanConvertor.convertBean(entity, vo, true);
		uwInsuredListDao.update(entity);
	}
	
	/**
	 * 同步核保被保人相關欄位
	 * @param icfForm
	 */
	@Override
	public void syncUwInsured(long underwriteid, long insuredListId, String disabilityClass) {
		//強制寫入DML+清緩存使下方重load時能正確取得
		HibernateSession3.flush();
		HibernateSession3.clear();
		
		UwLifeInsuredVO uwLife = uwPolicyDS.findUwLifeInsuredByListId(underwriteid, insuredListId);
		
		List<UwInsuredDisability> uwDisList = uwInsuredDisabilityService.findByUwInsuredId(uwLife.getUwListId());
		List<String> disList = new ArrayList<String>();
		for (UwInsuredDisability vo : uwDisList) {
			disList.add(vo.getDisabilityType());
		}
		
		UwLifeInsuredVO uwLifeInsuredVO = uwPolicyDS.findUwLifeInsuredByListId(underwriteid, insuredListId);
		DisabilityInfoVO disInfo = insuredDisabilityListService.findDisabilityInfoByStr(disList);
		
		if (!org.apache.commons.lang3.StringUtils.equals(uwLifeInsuredVO.getDisabilityType(), disInfo.getDisabilityType()) ||
			!org.apache.commons.lang3.StringUtils.equals(uwLifeInsuredVO.getDisabilityCategory(), disInfo.getDisabilityCategory()) ||
			!org.apache.commons.lang3.StringUtils.equals(uwLifeInsuredVO.getDisabilityClass(), disabilityClass)) {
			
			uwLifeInsuredVO.setDisabilityType(disInfo.getDisabilityType());
			uwLifeInsuredVO.setDisabilityCategory(disInfo.getDisabilityCategory());
			uwLifeInsuredVO.setDisabilityClass(disabilityClass);
			
			if ("000".equals(disInfo.getDisabilityType()) && "00".equals(disInfo.getDisabilityCategory())) {
				uwLifeInsuredVO.setDisabilityClass("0");
			}
			
			update(uwLifeInsuredVO);
		}
	}

}
