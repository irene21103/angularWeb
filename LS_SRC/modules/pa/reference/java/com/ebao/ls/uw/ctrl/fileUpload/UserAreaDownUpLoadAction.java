package com.ebao.ls.uw.ctrl.fileUpload;

import java.io.InputStream;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeMap;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;

import com.ebao.ls.leave.service.UserLeaveManagerService;
import com.ebao.ls.pa.nb.util.NBUtils;
import com.ebao.ls.pty.ci.UserInfoQueryService;
import com.ebao.ls.uw.ctrl.fileUpload.UserAreaDownUploadForm;
import com.ebao.ls.uw.data.TUserAreaConfDao;
import com.ebao.foundation.common.lang.StringUtils;
import com.ebao.pub.framework.GenericAction;

/**
 * PCR_41834 核保轄區設置作業調整及保留核保決定資料記錄
 * 
 * @author Alex Chang
 * @since 2020.12.10
 */
public class UserAreaDownUpLoadAction extends GenericAction{
	private static Logger logger = Logger.getLogger(UserAreaDownUpLoadAction.class);  

	public static String httpAttrProcessMsg= "processMsg";
	public static String httpAttrProcessResult= "processResult";
	public static String httpAttrFileErrMsg= "fileErrMsg";
	public static String httpAttrActionType= "actionType";
//	public static String metaDownloadData= "downloadUserArea";	
//	public static String metaUploadData= "uploadData_UserArea";	
//	public static String metaShowUploadPage= "showUpload_UserArea";	
//	public static String metaShowUploadResult_UserArea= "showUploadResult_UserArea"; 
	public static String httpAttrDownload= "downloadUserArea";	
	public static String httpAttrUpload= "uploadData_UserArea";	
	public static String httpAttrShowUploadPage= "showUpload_UserArea";	
	public static String httpAttrShowUploadResult= "showUploadResult_UserArea"; 

	@Resource(name = TUserAreaConfDao.BEAN_DEFAULT)
	private TUserAreaConfDao userAreaConfDao;
	
	@Resource(name = UserLeaveManagerService.BEAN_DEFAULT)
	private UserLeaveManagerService userLeaveManagerService;
	
	@Resource(name = UserInfoQueryService.BEAN_DEFAULT)
	private UserInfoQueryService userInfoQueryService;

	//PCR_41834 核保轄區設置作業調整及保留核保決定資料記錄 
	@Resource(name = UserAreaDownUploadUtil.BEAN_DEFAULT)
	private UserAreaDownUploadUtil userAreaDownUploadUtl;
	//END of PCR_41834 By Alex.Chang 12.28
	
	@Override
	public ActionForward process(ActionMapping mapping, ActionForm actionForm,
									HttpServletRequest request, HttpServletResponse response)
									throws Exception {
		UserAreaDownUploadForm form = (UserAreaDownUploadForm) actionForm;
		String findForward = "success";
//		String rgexExcelXlsx= "^(.*)\\.([xX][lL][sS][xX])$";
//		String rgexExcelXls= "^(.*)\\.([xX][lL][sS])$";
//		UserAreaDownUploadUtil userAreaDownUploadUtl= new UserAreaDownUploadUtil();
	
		String actionTypeStr = (String)request.getParameter(httpAttrActionType);

		actionTypeStr= (actionTypeStr== null? httpAttrShowUploadPage: actionTypeStr);
  		
		if(httpAttrShowUploadPage.equals(actionTypeStr)){
			request.setAttribute(httpAttrActionType, httpAttrShowUploadPage);
			findForward = httpAttrShowUploadPage;
		}
		else if(httpAttrUpload.equals(actionTypeStr)){
			StringBuffer processResultStr= new StringBuffer ();
			FormFile formFileObj = form.getUploadFile();
			String fileName = formFileObj.getFileName();
			boolean isXlsFile= true;
			String templX= userAreaDownUploadUtl.TEMPLATE_XLS_PROC_ERR_PER_ROW;	//"第{0}行: "+ "{1}<br/>";
//			String processMsg= null;
			String errCodeStr= null;
			int errCodeNum= 0;
			ArrayList<String> rowErrDataArray= new ArrayList<String>();
			ArrayList<String> errRowNumArray= new ArrayList<String>();
			
			logger.debug("###############"+ "\r\n"
					+ "["+ httpAttrUpload+ "]"+ "\r\n"
					+ "fileName["+ fileName+ "]"+ "\r\n"
					//+ "["+ ""+ "]"+ "\r\n"
					+ "");

			if ( !fileName.matches(userAreaDownUploadUtl.REGEXP_XLSX_FILE) ) isXlsFile= false;

			if ( !isXlsFile ) {
				request.setAttribute(httpAttrProcessMsg, userAreaDownUploadUtl.ERR_MSG_EXCEL_FILE_ONLY);	//"上傳檔案僅限xls檔");//限xls
			}
			else {
				processResultStr= new StringBuffer ();
				TreeMap<Integer, String> errDataMap= new TreeMap<Integer, String>();
				try {
					InputStream fileInputStream = formFileObj.getInputStream();
					HashMap<String, Object> resultMap= new HashMap<String, Object>();

					resultMap= userAreaDownUploadUtl.parseUploadData(fileInputStream);
					errCodeStr= (String)resultMap.get(userAreaDownUploadUtl.META_COL_ERR_CODE);
					errCodeNum= (StringUtils.isNullOrEmpty(errCodeStr)? 0: Integer.parseInt(errCodeStr)); 
					
					if (errCodeNum>= 0) {
						request.setAttribute(httpAttrProcessMsg, userAreaDownUploadUtl.MSG_FILE_UPLOAD_SUCCESS+ "");
						processResultStr.append(userAreaDownUploadUtl.MSG_UPLOAD_RESULT_PROMPT);	//"上傳資料處理結果:";
					}
					else {
						int totalErrQty= errRowNumArray.size();
						String[] errMsgPool= new String[totalErrQty];

						errDataMap= (TreeMap<Integer, String>)resultMap.get(userAreaDownUploadUtl.META_COL_ERR_DATA);
						for(int iCnt= 0; iCnt< rowErrDataArray.size(); iCnt++) {
							String tempStr= rowErrDataArray.get(iCnt);
							String rowNum= errRowNumArray.get(iCnt);
							if ( !StringUtils.isNullOrEmpty(tempStr))
								errMsgPool[iCnt]= MessageFormat.format(templX, rowNum, rowErrDataArray.get(iCnt) );
						}
						
						//組合錯誤訊息字串
						for(int iCnt= 0; iCnt< errMsgPool.length; iCnt++) 
							processResultStr.append(errMsgPool[iCnt]);
				
						for (int errRowNum: errDataMap.keySet()) {	
//						for (String errRowNum: errDataMap.keySet()) {	
							String tempStr= errDataMap.get(errRowNum);
							processResultStr.append(MessageFormat.format(templX, Integer.toString(errRowNum+ 1), tempStr) );
						}

						request.setAttribute(httpAttrProcessMsg, userAreaDownUploadUtl.MSG_FILE_UPLOAD_COMPLETE);	//"檔案上傳完成!!");
						processResultStr.insert(0, userAreaDownUploadUtl.MSG_TITLE_OF_PROC_RESULT);		//"錯誤的資料行:<br/>"
					}
					
					String procSummStr= (String)resultMap.get(userAreaDownUploadUtl.META_COL_PROC_SUMMARY);
					processResultStr.append( (procSummStr== null? "": "<br/>"+ procSummStr) );
					request.setAttribute(httpAttrProcessResult, processResultStr.toString());
				} catch (Exception e) {
					request.setAttribute(httpAttrProcessMsg, userAreaDownUploadUtl.MSG_FILE_UPLOAD_FAIL);
					String strTmp= NBUtils.getTWMsg("MSG_1262194");
					strTmp= strTmp.replaceAll("\\{functionName\\}", "");
					request.setAttribute(httpAttrFileErrMsg, strTmp+ "<br/>"+ e.getMessage() );
				} finally{
					if (formFileObj!= null) {formFileObj.destroy(); formFileObj= null; }
				}
				
			}

			request.setAttribute(httpAttrActionType, httpAttrShowUploadResult);
			findForward = httpAttrShowUploadResult;
		}
	
		return mapping.findForward(findForward);
	}
}
