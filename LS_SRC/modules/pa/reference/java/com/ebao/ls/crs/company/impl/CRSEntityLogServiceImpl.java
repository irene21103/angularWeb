package com.ebao.ls.crs.company.impl;

import java.util.List;

import javax.annotation.Resource;

import com.ebao.foundation.commons.exceptions.GenericException;
import com.ebao.ls.crs.company.CRSEntityLogService;
import com.ebao.ls.crs.data.bo.CRSEntityLog;
import com.ebao.ls.crs.data.bo.CRSPartyLog;
import com.ebao.ls.crs.data.company.CRSEntityControlManListLogDAO;
import com.ebao.ls.crs.data.company.CRSEntityLogDAO;
import com.ebao.ls.crs.data.identity.CRSPartyLogDAO;
import com.ebao.ls.crs.vo.CRSEntityLogVO;
import com.ebao.ls.crs.vo.CRSPartyLogVO;
import com.ebao.ls.foundation.service.GenericServiceImpl;
import com.ebao.ls.pub.cst.YesNo;
import com.ebao.ls.pub.util.HashMapBuilder;
import java.util.HashMap;
import java.util.Map;

public class CRSEntityLogServiceImpl extends GenericServiceImpl<CRSEntityLogVO, CRSEntityLog, CRSEntityLogDAO>
				implements CRSEntityLogService {

	@Resource(name = CRSEntityControlManListLogDAO.BEAN_DEFAULT)
	private CRSEntityControlManListLogDAO controlManListLogDAO;
	
	@Resource(name=CRSPartyLogDAO.BEAN_DEFAULT)
	private CRSPartyLogDAO partyLogDAO;
	
	@Override
	protected CRSEntityLogVO newEntityVO() {
		return new CRSEntityLogVO();
	}

	@Override
	public List<CRSEntityLogVO> findByPartyLogId(Long partyLogId, boolean loadOtherTable) {
		List<CRSEntityLog> entityLogList = this.dao.findByPartyLogId(partyLogId);
		return convertToVOList(entityLogList, loadOtherTable);
	}

	@Override
	public CRSEntityLogVO save(CRSEntityLogVO logVO) throws GenericException {
		CRSEntityLogVO entityLogVO = super.save(logVO);
		
		CRSPartyLog partyLog = null;
		CRSEntityLogVO reloadVO = this.load(logVO.getLogId());
		if(reloadVO.getCrsPartyLog() != null ) {
			partyLog = partyLogDAO.load(reloadVO.getCrsPartyLog().getLogId());
		}
		if(logVO.getSigDate() != null && partyLog != null) {
			partyLog.setIsEntityReady(YesNo.YES_NO__YES);
		}else {
			partyLog.setIsEntityReady(YesNo.YES_NO__NO);
		}
		if(partyLog != null) {
			partyLogDAO.saveorUpdate(partyLog);
			CRSPartyLogVO partyLogVO = new CRSPartyLogVO();
			partyLog.copyToVO(partyLogVO, Boolean.TRUE);
			entityLogVO.setCrsPartyLog(partyLogVO);
		}
		return entityLogVO;
	}

    @Override
    public List<CRSEntityLogVO> findByChangeId(Long changeId) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("changeId", HashMapBuilder.build("eq", changeId));
        return convertToVOList(dao.find(params));
    }


}
