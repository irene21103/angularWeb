package com.ebao.ls.uw.ctrl.leaveUwAuth;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.UserTransaction;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;

import com.ebao.ls.pa.pub.auth.UwAreaLeaveAuthServiceImpl;
import com.ebao.ls.pub.auth.AuthItemCst;
import com.ebao.ls.uw.ds.UwAreaService;
import com.ebao.ls.uw.ds.UwAuthLeaveService;
import com.ebao.ls.uw.vo.LeaveUwAuthVO;
import com.ebao.ls.uw.vo.UwAreaVO;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.framework.ExceptionFactory;
import com.ebao.pub.framework.GenericAction;
import com.ebao.pub.framework.MultiWarning;
import com.ebao.pub.util.StringUtils;
import com.ebao.pub.util.Trans;
import com.ebao.pub.util.TransUtils;
import com.ebao.ls.uw.ds.vo.LeaveAuthEdit;
import com.ebao.ls.leave.service.UserLeaveManagerService;
import com.ebao.ls.pub.leave.vo.DataentryVerifConfVO;
import com.ebao.ls.pub.leave.vo.DateStandardConfVO;
import com.ebao.ls.pub.leave.vo.UserAreaConfVO;
import com.ebao.ls.pub.leave.vo.UserLeaveMangeVO;
import com.ebao.ls.pub.leave.vo.VerifactionManagerConfVO;
import com.ebao.pub.sys.sysmgmt.usermgr.UserDS;
import com.ebao.foundation.app.sys.sysmgmt.ds.SysMgmtDSLocator;
import com.ebao.foundation.app.sys.sysmgmt.ds.UserVO;
/**
 * <p>Title: 休假管理及核保轄區設置-維護頁面</p>
 * <p>Description: Function Description Only vegetable</p>
 * <p>Copyright: Copyright (c) 2015</p>
 * <p>Company: TGL Co., Ltd.</p>
 * <p>Create Time: Aug 6, 2015</p> 
 * @author 
 * <p>Update Time: Aug 6, 2015</p>
 * <p>Updater: Alex Cheng</p>
 * <p>Update Comments: </p>
 */
public class LeaveAuthEditAction extends GenericAction {
	
	private static Logger logger = Logger.getLogger(LeaveAuthEditAction.class);  
	
  protected static final String ACTION_UPDATE = "update";
  protected static final String ACTION_SAVE = "save";
  
  @Resource(name = UwAreaService.BEAN_DEFAULT)
  private UwAreaService uwAreaService;
  
  @Resource(name = UwAuthLeaveService.BEAN_DEFAULT)
  private UwAuthLeaveService uwAuthLeaveService;
  
  
  
  
  @Resource(name = UwAreaLeaveAuthServiceImpl.BEAN_DEFAULT)
  private UwAreaLeaveAuthServiceImpl uwAreaLeaveAuthService;
  
  @Resource(name = UserLeaveManagerService.BEAN_DEFAULT)
  private UserLeaveManagerService userLeaveManagerService;
  
  
  @Override
  protected MultiWarning processWarning(ActionMapping mapping,
                  ActionForm form,
                  HttpServletRequest request, HttpServletResponse response)
                  throws Exception {
//      leaveAuthForm leaveAuthForm = (leaveAuthForm) form;
      MultiWarning mw = new MultiWarning();
      mw.setContinuable(false);
      return mw;
  }
  
  @Override
  public ActionForward process(ActionMapping mapping, ActionForm form, HttpServletRequest request,
          HttpServletResponse response) throws Exception {
    LeaveAuthForm leaveAuthForm = (LeaveAuthForm) form;
    String findForward = "success";
    
    String saction = leaveAuthForm.getActionType();
    LeaveAuthEdit leaveAuthEdit = null;
    Long empId = StringUtils.isNullOrEmpty(leaveAuthForm.getEditEmpId()) ? 
                    null: Long.valueOf(leaveAuthForm.getEditEmpId());
    boolean isEditRole = uwAreaLeaveAuthService.hasAccessRight(AuthItemCst.AUTH_ITEM_LEAVE_UW_AREA_PAGE);
    //預收與覆核人員設置作業權限
    boolean isDataEntryRole= uwAreaLeaveAuthService.hasAccessRight(AuthItemCst.AUTH_ITEM_DATA_ENTRY_VREIF);
    //覆核/主管設置作業權限
    boolean isVerifManagerRole= uwAreaLeaveAuthService.hasAccessRight(AuthItemCst.AUTH_ITEM_VERIF_MANAGER);
	//PCR_41834 核保轄區設置作業調整及保留核保決定資料記錄 @?@
    //轄區設置作業權限
    boolean isUserAreaRole= uwAreaLeaveAuthService.hasAccessRight(AuthItemCst.AUTH_ITEM_USER_AREA);
    boolean isUserAreaDownloadRole= uwAreaLeaveAuthService.hasAccessRight(AuthItemCst.AUTH_ITEM_USER_AREA_DOWNLOAD);
    boolean isUserAreaUploadRole= uwAreaLeaveAuthService.hasAccessRight(AuthItemCst.AUTH_ITEM_USER_AREA_UPLOAD);
	//END of PCR_41834 By Alex.Chang 12.21
    
    leaveAuthForm.setIsEditRole(isEditRole);
    leaveAuthForm.setIsDataEntryRole(isDataEntryRole);
    leaveAuthForm.setIsVerifManagerRole(isVerifManagerRole);
	//PCR_41834 核保轄區設置作業調整及保留核保決定資料記錄 
    leaveAuthForm.setIsUserAreaRole(isUserAreaRole);
    leaveAuthForm.setIsUserAreaDownloadRole(isUserAreaDownloadRole);
    leaveAuthForm.setIsUserAreaUploadRole(isUserAreaUploadRole);
	//END of PCR_41834 By Alex.Chang 12.21
    
    LeaveAuthEdit loginEmpData = userLeaveManagerService.findDeptByEmpId(Long.toString(AppContext.getCurrentUser().getUserId()));
    Long loginDeptId = loginEmpData.getSectionDeptId() != null ? loginEmpData.getSectionDeptId(): loginEmpData.getMasterDeptId();

    if(!StringUtils.isNullOrEmpty( saction)){
      if(LeaveAuthEditAction.ACTION_UPDATE.equals(saction)){
        Long leaveUwAuthListId = StringUtils.isNullOrEmpty(leaveAuthForm.getLeaveUwAuthListId()) ? 
                null: Long.valueOf(leaveAuthForm.getLeaveUwAuthListId());
//        Long empId = StringUtils.isNullOrEmpty(leaveAuthForm.getEditEmpId()) ? 
//                null: Long.valueOf(leaveAuthForm.getEditEmpId());
        
        UserVO userVO = UserDS.getUserById(empId);

        int managerType = -1 ;
        leaveAuthEdit = userLeaveManagerService.findAuthLeave(leaveUwAuthListId, empId, managerType);

        if(leaveAuthEdit.getOrgId() == null || leaveAuthEdit.getOrgId()<=0){
            leaveAuthEdit.setOrgId(Long.parseLong(userVO.getOrganId()));
        }
        if(leaveAuthEdit.getDeptId() == null || leaveAuthEdit.getDeptId()<=0){
            leaveAuthEdit.setDeptId(Long.parseLong(userVO.getDeptId()));
        }
      //  this.setDefDate(leaveAuthEdit);
        List<UserAreaConfVO> userAreaConfs = userLeaveManagerService.findAreaByEmpId(empId);
      //  leaveAuthEdit.setUserAreaConfs(userAreaConfs);
        
        leaveAuthForm.setLeaveAuthEdit(leaveAuthEdit);
      
 
            findForward = "leaveUser";
           // findForward = "success";
      
        
      }else if(LeaveAuthEditAction.ACTION_SAVE.equals(saction)){
//        Long empId = StringUtils.isNullOrEmpty(leaveAuthForm.getEditEmpId()) ? 
//                null: Long.valueOf(leaveAuthForm.getEditEmpId());
        leaveAuthEdit = leaveAuthForm.getLeaveAuthEdit();
        leaveAuthEdit.setMasterEmpId(
                StringUtils.isNullOrEmpty(leaveAuthForm.getMasterEmpId())?null:new Long(leaveAuthForm.getMasterEmpId()));
     //   this.setDefDate(leaveAuthEdit);
        UserLeaveMangeVO newVO = this.getUserLeaveMangeVO(leaveAuthEdit);
 //       List<UwAreaVO> uwAreaList = this.getUwAreas(leaveAuthEdit);
        UserTransaction ut = null;

        try {
            ut = Trans.getUserTransaction();
            ut.begin();
            if(newVO.getLeaveStartDate() ==null || newVO.getLeaveEndDate()==null){
                userLeaveManagerService.delUserLeaveData(newVO);
            }else{
                userLeaveManagerService.saveUserLeaveData(newVO);
            }
            
            ut.commit();

           // List<UwAreaVO> uwAreas = uwAreaService.findUwAreaByEmpId(empId);
          //  leaveAuthEdit.setUwAreas(uwAreas);
            leaveAuthForm.setLeaveAuthEdit(leaveAuthEdit);
            leaveAuthForm.setRetMsg("MSG_FMS_497");
            List<LeaveAuthEdit> leaveUwAuthList = null;
            if("1".equals(leaveAuthForm.getSearchType())){
              Long deptId = StringUtils.isNullOrEmpty(String.valueOf(leaveAuthEdit.getOrgId()))? 
                      null : new Long(leaveAuthEdit.getOrgId());
              Long sectionDeptId = StringUtils.isNullOrEmpty(String.valueOf(leaveAuthEdit.getDeptId()))? 
                      null : new Long(leaveAuthEdit.getDeptId());
              Long empName = StringUtils.isNullOrEmpty(leaveAuthForm.getEmpName())? 
                      null : new Long(leaveAuthForm.getEmpName());
              String empCode = StringUtils.isNullOrEmpty(leaveAuthForm.getEmpId())? 
                      null : leaveAuthForm.getEmpId();
              
              leaveUwAuthList = userLeaveManagerService.searchLeaveDataByEmp(deptId, sectionDeptId, empId, empCode, isEditRole, loginDeptId);
             
              Long empIdtmp =0L;
              for (Iterator<LeaveAuthEdit> iter=leaveUwAuthList.listIterator(); iter.hasNext();){
          
                  LeaveAuthEdit vo = new LeaveAuthEdit();
                  vo = iter.next();
                  
                  if(vo.getEmpId() != null){
                      if(empIdtmp.equals(vo.getEmpId())){
                          iter.remove();
                        }else{
                            empIdtmp = vo.getEmpId();
                        }
                  }
                  
              }    
            }else if("2".equals(leaveAuthForm.getSearchType())){
              Long channelType = StringUtils.isNullOrEmpty(leaveAuthForm.getChannelType())? 
                      null : new Long(leaveAuthForm.getChannelType());
              Long channelId = StringUtils.isNullOrEmpty(leaveAuthForm.getChannelId())? 
                      null : new Long(leaveAuthForm.getChannelId());
              leaveUwAuthList = userLeaveManagerService.searchLeaveDataByArea(channelType, channelId, isEditRole, loginDeptId);
            }
            request.setAttribute("leaveUwAuthList", leaveUwAuthList);
        } catch (Exception e) {
            TransUtils.rollback(ut);
            throw ExceptionFactory.parse(e);
        }
        findForward = "saveSuccess";
      }else if("dataentryVreif".equals(saction)){
//          Long empId = StringUtils.isNullOrEmpty(leaveAuthForm.getEditEmpId()) ? 
//                          null: Long.valueOf(leaveAuthForm.getEditEmpId());
          LeaveAuthEdit editData = new LeaveAuthEdit();
          DataentryVerifConfVO vo = new  DataentryVerifConfVO();
          vo = userLeaveManagerService.findDataentryVerifConfByEmpId(empId);
          if(vo != null ){
              editData.setUpdatedBy(vo.getUpdatedBy());
              editData.setUpdateTime(vo.getUpdateTime());
          }
           UserVO userVO = UserDS.getUserById(empId);
           if(vo.getOrgId() == null || vo.getOrgId()<=0){
               vo.setOrgId(Long.parseLong(userVO.getOrganId()));
           }
           if(vo.getDeptId() == null || vo.getDeptId()<=0){
               vo.setDeptId(Long.parseLong(userVO.getDeptId()));
           }
           if(vo.getEmpId() == null || vo.getEmpId()<=0){
               vo.setEmpId(empId);
           }
           //editData.setUpdatedBy(vo);
          // editData.setUpdateTime(updateTime);
           leaveAuthForm.setLeaveAuthEdit(editData);
           leaveAuthForm.setDataentryVerifConf(vo);
           findForward = "dataentryVreif";                  
      }else if("dataentryVreifSave".equals(saction)){
              
           DataentryVerifConfVO vo = new  DataentryVerifConfVO();
           vo = leaveAuthForm.getDataentryVerifConf();
           
                                       
           userLeaveManagerService.saveDataentryVerifData(vo);                      
  
           findForward = "saveSuccess";
      }else if("userArea".equals(saction)){
//          Long empId = StringUtils.isNullOrEmpty(leaveAuthForm.getEditEmpId()) ? 
//                          null: Long.valueOf(leaveAuthForm.getEditEmpId());
          LeaveAuthEdit editData = new LeaveAuthEdit();
          
          List<UserAreaConfVO> userAreaConfs = userLeaveManagerService.findAreaByEmpId(empId);
          editData.setUserAreaConfs(userAreaConfs);
          if(userAreaConfs != null && userAreaConfs.size() >0){
              editData.setUpdatedBy(userAreaConfs.get(0).getUpdatedBy());
              editData.setUpdateTime(userAreaConfs.get(0).getUpdateTime());
          }
           UserVO userVO = UserDS.getUserById(empId);
           if(editData.getOrgId() == null || editData.getOrgId()<=0){
               editData.setOrgId(Long.parseLong(userVO.getOrganId()));
           }
           if(editData.getDeptId() == null || editData.getDeptId()<=0){
               editData.setDeptId(Long.parseLong(userVO.getDeptId()));
           }
           if(editData.getEmpId() == null || editData.getEmpId()<=0){
               editData.setEmpId(empId);
           }
           //editData.setUpdatedBy(vo);
          // editData.setUpdateTime(updateTime);
           
           
           leaveAuthForm.setLeaveAuthEdit(editData);
           
           findForward = "userArea";                  
		//PCR_41834 核保轄區設置作業調整及保留核保決定資料記錄 
		}else if("showUpload_UserArea".equals(saction)){
			logger.debug("###############"+ "\r\n"
					+ "["+ "showUpload_UserArea"+ "]"+ "\r\n"
					//+ "["+ ""+ "]"+ "\r\n"
					+ "");
			leaveAuthEdit = leaveAuthForm.getLeaveAuthEdit();
			request.setAttribute("actionType", "showUpload_UserArea");
			findForward = "showUpload_UserArea";
		//END of PCR_41834 By Alex.Chang 12.09
      }else if("userAreaSave".equals(saction)){
           leaveAuthEdit = leaveAuthForm.getLeaveAuthEdit();    
           List<UserAreaConfVO> uwAreaList = this.getUserAreas(leaveAuthEdit);
           UserTransaction ut = null;

           try {
               ut = Trans.getUserTransaction();
               ut.begin();
               
               userLeaveManagerService.saveUserAreaData(uwAreaList,empId); 
               
               ut.commit();
                                       
           } catch (Exception e) {
               TransUtils.rollback(ut);
               throw ExceptionFactory.parse(e);
           }                    
  
           findForward = "saveSuccess";
      }else if("verifManager".equals(saction)){
//          Long empId = StringUtils.isNullOrEmpty(leaveAuthForm.getEditEmpId()) ? 
//                          null: Long.valueOf(leaveAuthForm.getEditEmpId());
          LeaveAuthEdit editData = new LeaveAuthEdit();
          
          List<VerifactionManagerConfVO> verifactionManages = userLeaveManagerService.findVerifactionManager(empId);
          editData.setVerifactionManagerConfs(verifactionManages);
          if(verifactionManages != null && verifactionManages.size() >0){
              editData.setUpdatedBy(verifactionManages.get(0).getUpdatedBy());
              editData.setUpdateTime(verifactionManages.get(0).getUpdateTime());
          }
           UserVO userVO = UserDS.getUserById(empId);
           if(editData.getOrgId() == null || editData.getOrgId()<=0){
               editData.setOrgId(Long.parseLong(userVO.getOrganId()));
           }
           if(editData.getDeptId() == null || editData.getDeptId()<=0){
               editData.setDeptId(Long.parseLong(userVO.getDeptId()));
           }
           if(editData.getEmpId() == null || editData.getEmpId()<=0){
               editData.setEmpId(empId);
           }
           //editData.setUpdatedBy(vo);
          // editData.setUpdateTime(updateTime);
           
           
           leaveAuthForm.setLeaveAuthEdit(editData);
           
           findForward = "verifManager";                  
      }else if("verifManagerSave".equals(saction)){
           leaveAuthEdit = leaveAuthForm.getLeaveAuthEdit();    
           List<VerifactionManagerConfVO> vmVos = this.getVerifManagers(leaveAuthEdit);
           UserTransaction ut = null;

           try {
               ut = Trans.getUserTransaction();
               ut.begin();
               
               userLeaveManagerService.saveVerifactionManagerData(vmVos,empId); 
               
               ut.commit();
                                       
           } catch (Exception e) {
               TransUtils.rollback(ut);
               throw ExceptionFactory.parse(e);
           }                    
  
           findForward = "saveSuccess";
      }
    }
    
    return mapping.findForward(findForward);
  }

  /**
   * <p>Description : 將頁面休假覆核資料組出LeaveAuthVO</p>
   * <p>Created By : Alex Cheng</p>
   * <p>Create Time : Aug 12, 2015</p>
   * @param leaveAuthEdit
   * @return LeaveUwAuthVO
   */
  private UserLeaveMangeVO getUserLeaveMangeVO(LeaveAuthEdit leaveAuthEdit){
      UserLeaveMangeVO userLeaveMangeVO = new UserLeaveMangeVO();
      userLeaveMangeVO.setListId(leaveAuthEdit.getListId());
      userLeaveMangeVO.setEmpId(leaveAuthEdit.getEmpId());
      userLeaveMangeVO.setLeaveStartDate(leaveAuthEdit.getStartDate());
      userLeaveMangeVO.setLeaveEndDate(leaveAuthEdit.getEndDate());
      userLeaveMangeVO.setStartDateType(leaveAuthEdit.getStartDateType());
      userLeaveMangeVO.setEndDateType(leaveAuthEdit.getEndDateType());
      userLeaveMangeVO.setDeptId(leaveAuthEdit.getDeptId());
      userLeaveMangeVO.setOrgId(leaveAuthEdit.getOrgId());
    return userLeaveMangeVO;
  }
  
  /**
   * <p>Description : 將頁面資料組出UwAreaVO集合</p>
   * <p>Created By : Alex Cheng</p>
   * <p>Create Time : Aug 12, 2015</p>
   * @param leaveAuthEdit
   * @return List<UwAreaVO>
   */
  private List<VerifactionManagerConfVO> getVerifManagers(LeaveAuthEdit leaveAuthEdit){
    List<VerifactionManagerConfVO> vmVos = new ArrayList<VerifactionManagerConfVO>();
    if(leaveAuthEdit != null && 
            leaveAuthEdit.getVerifactionManagers() != null && leaveAuthEdit.getVerifactionManagers().length > 0){
      String vmListId = "";
      String orgId = "";
      String deptId = "";
      String verifactionManager = "";
      String typeId = "";
      VerifactionManagerConfVO vo =new VerifactionManagerConfVO();
      for(int i = 0 ; i < leaveAuthEdit.getVerifactionManagers().length ; i++){
        vo =new VerifactionManagerConfVO();
        vmListId = leaveAuthEdit.getUwAreaListId()[i];
        orgId = leaveAuthEdit.getOrgIds()[i];
        deptId = leaveAuthEdit.getDeptIds()[i];
        typeId = leaveAuthEdit.getTypeIds()[i];
        verifactionManager = leaveAuthEdit.getVerifactionManagers()[i];
        if(!StringUtils.isNullOrEmpty(verifactionManager)){
          vo.setListId(StringUtils.isNullOrEmpty(vmListId)?null:new Long(vmListId));
          vo.setEmpId(leaveAuthEdit.getEmpId());
          vo.setDeptId(StringUtils.isNullOrEmpty(deptId)?null:new Long(deptId));
          vo.setOrgId(StringUtils.isNullOrEmpty(orgId)?null:new Long(orgId));
          vo.setTypeId(StringUtils.isNullOrEmpty(typeId)?null:new Long(typeId));
          vo.setVerifactionManager(StringUtils.isNullOrEmpty(verifactionManager)?null:new Long(verifactionManager));
          vo.setStartDate(AppContext.getCurrentUserLocalTime());
          vmVos.add(vo);
        }
      }
    }
    return vmVos;
  }
  
  
  
  /**
   * <p>Description : 將頁面資料組出UwAreaVO集合</p>
   * <p>Created By : Alex Cheng</p>
   * <p>Create Time : Aug 12, 2015</p>
   * @param leaveAuthEdit
   * @return List<UwAreaVO>
   */
  private List<UserAreaConfVO> getUserAreas(LeaveAuthEdit leaveAuthEdit){
    List<UserAreaConfVO> userAreas = new ArrayList<UserAreaConfVO>();
    if(leaveAuthEdit != null && 
            leaveAuthEdit.getChannelIds() != null && leaveAuthEdit.getChannelIds().length > 0){
      String areaListId = "";
      String channelId = "";
      String channelType = "";
      UserAreaConfVO vo =new UserAreaConfVO();
      for(int i = 0 ; i < leaveAuthEdit.getChannelIds().length ; i++){
        vo =new UserAreaConfVO();
        areaListId = leaveAuthEdit.getUwAreaListId()[i];
        channelId = leaveAuthEdit.getChannelIds()[i];
        channelType = leaveAuthEdit.getChannelTypes()[i];
        if(!StringUtils.isNullOrEmpty(channelId)){
          vo.setListId(StringUtils.isNullOrEmpty(areaListId)?null:new Long(areaListId));
          vo.setEmpId(leaveAuthEdit.getEmpId());
          vo.setDeptId(leaveAuthEdit.getDeptId());
          vo.setOrgId(leaveAuthEdit.getOrgId());
          vo.setChannelType(StringUtils.isNullOrEmpty(channelType)?null:new Long(channelType));
          vo.setChannelOrgId(StringUtils.isNullOrEmpty(channelId)?null:new Long(channelId));
          userAreas.add(vo);
        }
      }
    }
    return userAreas;
  }
  
  /**
   * <p>Description : 濾掉過期請假日期資料</p>
   * <p>Created By : Alex Cheng</p>
   * <p>Create Time : Aug 13, 2015</p>
   * @param leaveAuthEdit
   */
  private void setDefDate(LeaveAuthEdit leaveAuthEdit){
    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
    SimpleDateFormat sdf2 = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
    if(leaveAuthEdit.getEndDate() != null){
      try {
        Date endDate = leaveAuthEdit.getEndDate();
        String endDateStr = sdf.format(leaveAuthEdit.getEndDate());
        if(leaveAuthEdit.getEndDateType().equals("1")){
          endDateStr += " 12:00:00";
        }else{
          endDateStr += " 18:00:00";
        }
         Calendar nowCal = Calendar.getInstance();
         endDate = sdf2.parse(endDateStr);
         Calendar endCal = Calendar.getInstance();
         endCal.setTime(endDate);
         if(endCal.before(nowCal)){
           leaveAuthEdit.setStartDate(null);
           leaveAuthEdit.setStartDateType("1");
           leaveAuthEdit.setEndDate(null);
           leaveAuthEdit.setEndDateType("1");
         }
      } catch (ParseException e) {
//        e.printStackTrace();
      }
    }
  }
}
