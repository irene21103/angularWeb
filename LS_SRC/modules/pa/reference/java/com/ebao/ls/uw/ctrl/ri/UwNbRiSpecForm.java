package com.ebao.ls.uw.ctrl.ri;

import com.ebao.pub.framework.GenericForm;

public class UwNbRiSpecForm extends GenericForm {

	private static final long serialVersionUID = 4275711226246237617L;
	
	private String subAction;
	
	private Long reinsurerId;
	
	private String contact;
	
	private String email;
	
	private Integer faxReg;
	
	private Integer fax;
	
	private Integer telReg;
	
	private Integer tel;
	
	private Integer telExt;

	public String getSubAction() {
		return subAction;
	}

	public void setSubAction(String subAction) {
		this.subAction = subAction;
	}

	public Long getReinsurerId() {
		return reinsurerId;
	}

	public void setReinsurerId(Long reinsurerId) {
		this.reinsurerId = reinsurerId;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getFaxReg() {
		return faxReg;
	}

	public void setFaxReg(Integer faxReg) {
		this.faxReg = faxReg;
	}

	public Integer getFax() {
		return fax;
	}

	public void setFax(Integer fax) {
		this.fax = fax;
	}

	public Integer getTelReg() {
		return telReg;
	}

	public void setTelReg(Integer telReg) {
		this.telReg = telReg;
	}

	public Integer getTel() {
		return tel;
	}

	public void setTel(Integer tel) {
		this.tel = tel;
	}

	public Integer getTelExt() {
		return telExt;
	}

	public void setTelExt(Integer telExt) {
		this.telExt = telExt;
	}
	
	

}
