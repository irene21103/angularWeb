package com.ebao.ls.uw.ctrl.pub;

import com.ebao.pub.framework.GenericForm;

/**
 * 
 * <p>Title:Underwriting</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: eBaoTech Corporation</p>
 * @author Walter Huang
 * @since  Jun 27, 2005
 * @version 1.0
 */
public class UwLienForm extends GenericForm {

  private java.lang.Long underwriteId = null;

  private java.lang.Long itemId = null;

  private java.util.Date reduceStart = null;

  private java.util.Date reduceEnd = null;

  private java.math.BigDecimal reduceRate = null;

  private java.lang.String lienType = "";

  private java.lang.Integer emValue = null;

  private java.math.BigDecimal reducedAmount = null;

  private java.lang.Long uwListId = null;

  private String chargePeriod = null;

  private Long listId;

  private Long coverageTerm;

  public Long getListId() {
    return listId;
  }

  public void setListId(Long listId) {
    this.listId = listId;
  }

  /**
   * returns the value of the underwriteId
   *
   * @return the underwriteId
   */
  public java.lang.Long getUnderwriteId() {
    return underwriteId;
  }

  /**
   * sets the value of the underwriteId
   *
   * @param underwriteId the underwriteId
   */
  public void setUnderwriteId(java.lang.Long underwriteId) {
    this.underwriteId = underwriteId;
  }

  /**
   * returns the value of the itemId
   *
   * @return the itemId
   */
  public java.lang.Long getItemId() {
    return itemId;
  }

  /**
   * sets the value of the itemId
   *
   * @param itemId the itemId
   */
  public void setItemId(java.lang.Long itemId) {
    this.itemId = itemId;
  }

  /**
   * returns the value of the reduceStart
   *
   * @return the reduceStart
   */
  public java.util.Date getReduceStart() {
    return reduceStart;
  }

  /**
   * sets the value of the reduceStart
   *
   * @param reduceStart the reduceStart
   */
  public void setReduceStart(java.util.Date reduceStart) {
    this.reduceStart = reduceStart;
  }

  /**
   * returns the value of the reduceEnd
   *
   * @return the reduceEnd
   */
  public java.util.Date getReduceEnd() {
    return reduceEnd;
  }

  /**
   * sets the value of the reduceEnd
   *
   * @param reduceEnd the reduceEnd
   */
  public void setReduceEnd(java.util.Date reduceEnd) {
    this.reduceEnd = reduceEnd;
  }

  /**
   * returns the value of the reduceRate
   *
   * @return the reduceRate
   */
  public java.math.BigDecimal getReduceRate() {
    return reduceRate;
  }

  /**
   * sets the value of the reduceRate
   *
   * @param reduceRate the reduceRate
   */
  public void setReduceRate(java.math.BigDecimal reduceRate) {
    this.reduceRate = reduceRate;
  }

  /**
   * returns the value of the lienType
   *
   * @return the lienType
   */
  public java.lang.String getLienType() {
    return lienType;
  }

  /**
   * sets the value of the lienType
   *
   * @param lienType the lienType
   */
  public void setLienType(java.lang.String lienType) {
    this.lienType = lienType;
  }

  /**
   * returns the value of the emValue
   *
   * @return the emValue
   */
  public java.lang.Integer getEmValue() {
    return emValue;
  }

  /**
   * sets the value of the emValue
   *
   * @param emValue the emValue
   */
  public void setEmValue(java.lang.Integer emValue) {
    this.emValue = emValue;
  }

  /**
   * returns the value of the reducedAmount
   *
   * @return the reducedAmount
   */
  public java.math.BigDecimal getReducedAmount() {
    return reducedAmount;
  }

  /**
   * sets the value of the reducedAmount
   *
   * @param reducedAmount the reducedAmount
   */
  public void setReducedAmount(java.math.BigDecimal reducedAmount) {
    this.reducedAmount = reducedAmount;
  }

  /**
   * returns the value of the uwListId
   *
   * @return the uwListId
   */
  public java.lang.Long getUwListId() {
    return uwListId;
  }

  /**
   * sets the value of the uwListId
   *
   * @param uwListId the uwListId
   */
  public void setUwListId(java.lang.Long uwListId) {
    this.uwListId = uwListId;
  }

  private String applyCode;

  private String policyCode;

  private java.math.BigDecimal amount;

  private java.util.Date commencementDate;

  private Integer chargeYear;

  private Integer converYear;

  private java.lang.String coveragePeriod = "";

  private Integer currPolicyYear;

  private java.lang.Integer productId = null;

  private String uwSourceType;

  public String getApplyCode() {
    return applyCode;
  }

  public void setApplyCode(String applyCode) {
    this.applyCode = applyCode;
  }

  public String getPolicyCode() {
    return policyCode;
  }

  public void setPolicyCode(String policyCode) {
    this.policyCode = policyCode;
  }

  public Integer getConverYear() {
    return converYear;
  }

  public void setConverYear(Integer converYear) {
    this.converYear = converYear;
  }

  public java.math.BigDecimal getAmount() {
    return amount;
  }

  public void setAmount(java.math.BigDecimal amount) {
    this.amount = amount;
  }

  public Integer getChargeYear() {
    return chargeYear;
  }

  public void setChargeYear(Integer chargeYear) {
    this.chargeYear = chargeYear;
  }

  public java.util.Date getCommencementDate() {
    return commencementDate;
  }

  public void setCommencementDate(java.util.Date commencementDate) {
    this.commencementDate = commencementDate;
  }

  public Integer getCurrPolicyYear() {
    return currPolicyYear;
  }

  public void setCurrPolicyYear(Integer currPolicyYear) {
    this.currPolicyYear = currPolicyYear;
  }

  /**
   * returns the value of the productId
   *
   * @return the productId
   */
  public java.lang.Integer getProductId() {
    return productId;
  }

  /**
   * sets the value of the productId
   *
   * @param productId the productId
   */
  public void setProductId(java.lang.Integer productId) {
    this.productId = productId;
  }

  public String getUwSourceType() {
    return uwSourceType;
  }

  public void setUwSourceType(String uwSourceType) {
    this.uwSourceType = uwSourceType;
  }

  public String getLienYear() {
    if (getReduceEnd() != null && getReduceStart() != null) {
      return String.valueOf(com.ebao.pub.util.DateUtils.getYear(getReduceEnd())
          - (com.ebao.pub.util.DateUtils.getYear(getReduceStart())));
    }
    return null;
  }

  public void setLienYear(String lienYear) {
  }

  /**
   * @return Returns the chargePeriod.
   */
  public String getChargePeriod() {
    return chargePeriod;
  }

  /**
   * @param chargePeriod The chargePeriod to set.
   */
  public void setChargePeriod(String chargePeriod) {
    this.chargePeriod = chargePeriod;
  }

  /**
   * @return Returns the coveragePeriod.
   */
  public java.lang.String getCoveragePeriod() {
    return coveragePeriod;
  }

  /**
   * @param coveragePeriod The coveragePeriod to set.
   */
  public void setCoveragePeriod(java.lang.String coveragePeriod) {
    this.coveragePeriod = coveragePeriod;
  }

  public Long getCoverageTerm() {
    return coverageTerm;
  }

  public void setCoverageTerm(Long coverageTerm) {
    this.coverageTerm = coverageTerm;
  }
}
