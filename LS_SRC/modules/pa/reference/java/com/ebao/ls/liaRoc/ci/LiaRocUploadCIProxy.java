package com.ebao.ls.liaRoc.ci;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ebao.foundation.module.web.action.ExceptionInfoUtils;
import com.ebao.ls.pa.pub.vo.UwNotintoRecvVO;
import com.ebao.ls.ws.vo.cust.liaRoc.LiaRocUploadProcessResultRootVO;

public class LiaRocUploadCIProxy implements LiaRocUploadCI {

	@Resource(name = "liaRocUpload2019CI")
	private LiaRocUploadCI liaRocUpload2019CI;

	@Resource(name = "liaRocUpload2020CI")
	private LiaRocUploadCI liaRocUpload2020CI;

	@Resource(name = LiaRocUploadCommonService.BEAN_DEFAULT)
	private LiaRocUploadCommonService liaRocUploadCommonService;

	private static final Logger LOGGER = LoggerFactory.getLogger(LiaRocUploadCIProxy.class);
	
	// -------------------------可同時新舊一併處理---------------------------

	@Override
	public void saveAcceptanceUploadNB(long caseId) throws Exception {

		// 執行日小於新式公會通報生效日期(2021/07/01)，要執行舊式公會通報
		if (!liaRocUploadCommonService.isLiaRocV2020Process()) {
			liaRocUpload2019CI.saveAcceptanceUploadNB(caseId);
		}
		
		try {
			liaRocUpload2020CI.saveAcceptanceUploadNB(caseId);	
		}catch(Exception e) {
			LOGGER.error("[公會2020ERROR]" + ExceptionInfoUtils.getExceptionMsg(e));
		}
	}

	@Override
	public void saveAcceptanceUploadPos(long caseId) throws Exception {

		// 執行日小於新式公會通報生效日期(2021/07/01)，要執行舊式公會通報
		if (!liaRocUploadCommonService.isLiaRocV2020Process()) {
			liaRocUpload2019CI.saveAcceptanceUploadPos(caseId);
		}
		
		try {
			liaRocUpload2020CI.saveAcceptanceUploadPos(caseId);
		}catch(Exception e) {
			LOGGER.error("[公會2020ERROR]" + ExceptionInfoUtils.getExceptionMsg(e));
		}
	}

	@Override
	public Long sendNB(Long policyId, String liaRocUploadType) {

		Long listId = 0L;
		
		// 執行日小於新式公會通報生效日期(2021/07/01)，要執行舊式公會通報
		if (!liaRocUploadCommonService.isLiaRocV2020Process()) {
			listId = liaRocUpload2019CI.sendNB(policyId, liaRocUploadType);
		}
		
		try {
			listId = liaRocUpload2020CI.sendNB(policyId, liaRocUploadType);
		}catch(Exception e) {
			LOGGER.error("[公會2020ERROR]" + ExceptionInfoUtils.getExceptionMsg(e));
		}
		return listId;
	}

	@Override
	public Long sendmPosNB(Long policyId, String liaRocUploadType) {

		Long listId = 0L;
	
		// 執行日小於新式公會通報生效日期(2021/07/01)，要執行舊式公會通報
		if (!liaRocUploadCommonService.isLiaRocV2020Process()) {
			listId = liaRocUpload2019CI.sendmPosNB(policyId, liaRocUploadType);
		}

		try {
			listId = liaRocUpload2020CI.sendmPosNB(policyId, liaRocUploadType);
		}catch(Exception e) {
			LOGGER.error("[公會2020ERROR]" + ExceptionInfoUtils.getExceptionMsg(e));
		}
		return listId;
	}

	@Override
	public Long sendNBRecCancel(Long policyId) {

		Long listId = 0L;
		
		// 執行日小於新式公會通報生效日期(2021/07/01)，要執行舊式公會通報
		if (!liaRocUploadCommonService.isLiaRocV2020Process()) {
			listId = liaRocUpload2019CI.sendNBRecCancel(policyId);
		}
		
		try {
			listId = liaRocUpload2020CI.sendNBRecCancel(policyId);
		}catch(Exception e) {
			LOGGER.error("[公會2020ERROR]" + ExceptionInfoUtils.getExceptionMsg(e));
		}
		return listId;
	}

	@Override
	public Long sendAfinsNBReceive(UwNotintoRecvVO vo) {

		Long listId = 0L;
		
		// 執行日小於新式公會通報生效日期(2021/07/01)，要執行舊式公會通報
		if (!liaRocUploadCommonService.isLiaRocV2020Process()) {
			listId = liaRocUpload2019CI.sendAfinsNBReceive(vo);
		}

		try {
			listId = liaRocUpload2020CI.sendAfinsNBReceive(vo);
		}catch(Exception e) {
			LOGGER.error("[公會2020ERROR]" + ExceptionInfoUtils.getExceptionMsg(e));
		}
		return listId;
	}

	@Override
	public Long sendNBUndo(Long policyId, String liaRocUploadType) {

		Long listId = 0L;
		
		// 執行日小於新式公會通報生效日期(2021/07/01)，要執行舊式公會通報
		if (!liaRocUploadCommonService.isLiaRocV2020Process()) {
			listId = liaRocUpload2019CI.sendNBUndo(policyId, liaRocUploadType);
		}

		try {
			listId = liaRocUpload2020CI.sendNBUndo(policyId, liaRocUploadType);
		}catch(Exception e) {
			LOGGER.error("[公會2020ERROR]" + ExceptionInfoUtils.getExceptionMsg(e));
		}
		return listId;
	}

	@Override
	public Long sendFoaUndo(Long caseId) {

		Long listId = 0L;
		
		// 執行日小於新式公會通報生效日期(2021/07/01)，要執行舊式公會通報
		if (!liaRocUploadCommonService.isLiaRocV2020Process()) {
			listId = liaRocUpload2019CI.sendFoaUndo(caseId);
		}

		try {
			listId = liaRocUpload2020CI.sendFoaUndo(caseId);
		}catch(Exception e) {
			LOGGER.error("[公會2020ERROR]" + ExceptionInfoUtils.getExceptionMsg(e));
		}
		return listId;
	}

	@Override
	public void execute(com.ebao.ls.ws.vo.RqHeader rqHeader, LiaRocUploadProcessResultRootVO vo) {
		liaRocUploadCommonService.execute(rqHeader, vo);
	}

}
