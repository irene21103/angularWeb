package com.ebao.ls.liaRoc.batch;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.SequenceInputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.activation.DataHandler;
import javax.annotation.Resource;
import javax.transaction.UserTransaction;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.FileUtils;

import com.ebao.foundation.module.db.hibernate.HibernateSession3;
import com.ebao.foundation.module.para.Para;
import com.ebao.foundation.module.web.action.ExceptionInfoUtils;
import com.ebao.ls.batch.file.filehelper.FileGenericHelper;
import com.ebao.ls.crs.constant.CRSSysCode;
import com.ebao.ls.cs.logger.ApplicationLogger;
import com.ebao.ls.liaRoc.LiaRocCst;
import com.ebao.ls.liaRoc.batch.vo.ResultMessage;
import com.ebao.ls.liaRoc.bo.LiaRocUpload107DetailVO;
import com.ebao.ls.liaRoc.bo.LiaRocUploadDetailVO;
import com.ebao.ls.liaRoc.ci.LiaRocFileCI;
import com.ebao.ls.liaRoc.ci.LiaRocUpload1072020CI;
import com.ebao.ls.liaRoc.ci.LiaRocUpload2019CI;
import com.ebao.ls.liaRoc.ci.LiaRocUpload2020CI;
import com.ebao.ls.liaRoc.ci.LiaRocUploadCI;
import com.ebao.ls.liaRoc.ci.LiaRocUploadCommonService;
import com.ebao.ls.liaRoc.data.LiaRocUploadDetail2020Dao;
import com.ebao.ls.liaRoc.data.LiaRocUploadDetailDao;
import com.ebao.ls.liaRoc.vo.LiaRocUploadDetail1072020WrapperVO;
import com.ebao.ls.liaRoc.vo.LiaRocUploadDetail2020WrapperVO;
import com.ebao.ls.liaRoc.vo.LiaRocUploadDetailBaseVO;
import com.ebao.ls.notification.ci.NbEventCI;
import com.ebao.ls.notification.ci.vo.RequestInfoVo;
import com.ebao.ls.notification.ci.vo.SendErrorNotificationResponseVo;
import com.ebao.ls.pa.nb.util.NBUtils;
import com.ebao.ls.pub.CodeCst;
import com.ebao.ls.rstf.ci.liaroc20.Liaroc20CI;
import com.ebao.ls.rstf.ci.liaroc20.Liaroc20Cst;
import com.ebao.ls.rstf.ci.liaroc20.vo.internal.Liaroc20UploadRqBaseVO;
import com.ebao.ls.rstf.ci.liaroc20.vo.internal.Liaroc20UploadRqClm107VO;
import com.ebao.ls.rstf.ci.liaroc20.vo.internal.Liaroc20UploadRqInforceVO;
import com.ebao.ls.rstf.ci.liaroc20.vo.internal.Liaroc20UploadRqReceiveVO;
import com.ebao.ls.rstf.ci.liaroc20.vo.internal.Liaroc20UploadRsVO;
import com.ebao.ls.rstf.util.RstfClientUtils;
import com.ebao.ls.ws.ci.esp.ESPWSServiceCI;
import com.ebao.ls.ws.vo.InputStreamDataSoure;
import com.ebao.ls.ws.vo.client.esp.FileObject;
import com.ebao.ls.ws.vo.client.esp.RqHeader;
import com.ebao.ls.ws.vo.client.esp.StandardResponse;
import com.ebao.ls.ws.vo.cust.liaRoc.LiaRocUploadRequestRoot;
import com.ebao.pub.batch.job.BaseUnpieceableJob;
import com.ebao.pub.batch.type.JobStatus;
import com.ebao.pub.fileresolver.writer.FixedCharLengthWriter;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.util.DateUtils;
import com.ebao.pub.util.EnvUtils;
import com.ebao.pub.util.StringUtils;
import com.ebao.pub.util.Trans;

/**
 * <h1>公會通報資料上傳 batch - 父類別，提供共用的方法</h1><p>
 * @since 2016/03/10<p>
 * @author Amy Hung
 * <p>Update Time: </p>
 * <p>Updater: </p>
 * <p>Update Comments: </p>
*/
public abstract class LiaRocUploadBatch extends BaseUnpieceableJob {

	@Resource(name = LiaRocFileCI.BEAN_DEFAULT)
	protected LiaRocFileCI liaRocFileCI;

	@Resource(name = LiaRocUploadCI.BEAN_DEFAULT)
	protected LiaRocUploadCI liaRocUploadCI;

	@Resource(name = LiaRocUpload2019CI.BEAN_DEFAULT)
	protected LiaRocUpload2019CI liaRocUpload2019CI;

	@Resource(name = LiaRocUpload2020CI.BEAN_DEFAULT)
	protected LiaRocUpload2020CI liaRocUpload2020CI;

	@Resource(name = LiaRocUpload1072020CI.BEAN_DEFAULT)
	protected LiaRocUpload1072020CI liaRocUpload1072020CI;

	@Resource(name = LiaRocUploadDetailDao.BEAN_DEFAULT)
	protected LiaRocUploadDetailDao liaRocUploadDetailDao;

	@Resource(name = LiaRocUploadDetail2020Dao.BEAN_DEFAULT)
	protected LiaRocUploadDetail2020Dao liaRocUploadDetail2020Dao;

	@Resource(name = LiaRocUploadCommonService.BEAN_DEFAULT)
	protected LiaRocUploadCommonService liaRocUploadCommonService;

	@Resource(name = Liaroc20CI.BEAN_DEFAULT)
	protected Liaroc20CI liaroc20UplaodCI;

	@Resource(name = ESPWSServiceCI.BEAN_DEFAULT)
	private ESPWSServiceCI espWSServiceCI;
	
	@Resource(name = NbEventCI.BEAN_DEFAULT)
	private NbEventCI nbEventCI;

	private String requestFileName = null;

	private String prefixReqFileName = null;

	protected String charset = "big5";

	/**
	 * <p>Description : 通報種類(1:收件, 2:承保, 3:107通報)</p>
	 * <p>Created By : Amy Hung</p>
	 * <p>Create Time : May 14, 2016</p>
	 * @return
	 */
	protected abstract String getUploadType();

	/**
	 * <p>Description : 申報模組別(NB, POS, CLM)</p>
	 * <p>Created By : Amy Hung</p>
	 * <p>Create Time : May 14, 2016</p>
	 * @return
	 */
	protected abstract String getUploadModule();

	/**
	 * 用來將通報明細資料轉換成 txt 檔的 writer
	
	@Resource(name = "liaRocUploadRequestWriter")
	private FixedCharLengthWriter fixedLengthWriter;
	*/

	protected abstract FixedCharLengthWriter getFixedLengthWriter();

	/**
	 * <p>Description : 取得上傳通報資料檔檔名</p>
	 * <p>Created By : Amy Hung</p>
	 * <p>Create Time : Mar 11, 2016</p>
	 * @param type
	 * @return
	 */
	public String getUploadRequestFileName() {
		if (StringUtils.isNullOrEmpty(requestFileName)) {
			requestFileName = liaRocFileCI.getFileName(getPrefixReqFileName());
		}
		return requestFileName;
	}

	/**
	 * <p>Description : 將明細資料 VO 解析為 txt 格式的資料</p>
	 * <p>Created By : Amy Hung</p>
	 * <p>Create Time : Mar 12, 2016</p>
	 * @param detailList 明細資料 VO List
	 * @param failUploadIds 解析失敗的通報主檔 LIST_ID(當做回傳值)
	 * @return
	 * @throws UnsupportedEncodingException 
	 * @throws Exception
	 */
	public InputStream parseToText(List<LiaRocUploadDetailBaseVO> detailList, ArrayList<Long> failUploadIds)
					throws UnsupportedEncodingException {

		if (detailList == null || detailList.size() == 0)
			return null;

		Map<String, InputStream> detailStreams = new LinkedHashMap<String, InputStream>();
		InputStream singleStream = null;
		FixedCharLengthWriter fixedLengthWriter = getFixedLengthWriter();
		// 解析明細資料
		for (LiaRocUploadDetailBaseVO detail : detailList) {

			// 這筆明細資料被排除，因為它所屬的通報主檔(以保單為群組單位)，有明細檔資料解析有錯，
			// 因此該通報主檔下的明細資料一律都不通報，離開不處理
			if (failUploadIds.contains(detail.getUListId())) {
				NBUtils.appLogger("[INFO]此筆明細在排除通報的主檔清單中，不予處理 (LIST_ID:" + detail.getId() + " )");
				continue;
			}

			List<Map<String, Object>> maps = new ArrayList<Map<String, Object>>();
			Map<String, Object> map = null;
			boolean isParsePass = true;

			try {
				map = this.getMap(detail);
				maps.add(map);
			} catch (Exception e) {
				isParsePass = false;
				NBUtils.appLogger("[ERROR]通報明細資料 {LIST_ID:" + detail.getId()
								+ "} getMap() 執行失敗, Error Message: " + ExceptionInfoUtils.getExceptionMsg(e));

			}

			if (isParsePass) {

				try {

					fixedLengthWriter.setCharset(charset);
					singleStream = fixedLengthWriter.write(maps);

					// key 為:主檔 id + 明細檔 id
					detailStreams.put(detail.getUListId() + ":" + detail.getId(), singleStream);

				} catch (NullPointerException e) {
					isParsePass = false;
					NBUtils.appLogger("[ERROR]通報明細資料 {LIST_ID:" + detail.getId()
									+ "} 解析失敗, 請檢查是否存在 NULL 的欄位資料..");
				} catch (Exception e) {
					isParsePass = false;
					NBUtils.appLogger("[ERROR]通報明細資料 {LIST_ID:" + detail.getId() + "} 解析失敗, Error Message:" + e.getMessage());
				}

			}

			if (!isParsePass) {

				// 解析失敗，記錄此筆通報為失敗
				failUploadIds.add(detail.getUListId());

				NBUtils.appLogger("[INFO]列入排除發送通報主檔名單 {主檔 LIST_ID:" + detail.getUListId() + "}");

			}

		}
		// end.

		// 移除掉有錯的明細資料，將有錯的主檔下的所有明細資料移除掉
		if (failUploadIds.size() > 0) {
			for (Long failId : failUploadIds) {
				Iterator<String> itr = detailStreams.keySet().iterator();
				while (itr.hasNext()) {
					String[] key = itr.next().split(":");
					if (failId.equals(Long.valueOf(key[0]))) {
						itr.remove();
					}
				}
			}
		}
		// end.

		// 將多筆資料的 InputStream 合併為一個..
		Vector<InputStream> streamList = new Vector<InputStream>();
		int lastIndex = 0;
		for (Map.Entry<String, InputStream> entry : detailStreams.entrySet()) {

			streamList.add(entry.getValue());
			lastIndex++;

			if (lastIndex < detailStreams.size()) { // 最後一行不用加換行符號..
				streamList.add(new ByteArrayInputStream("\n".getBytes(charset)));
			}

		}

		if (streamList.size() > 0) {
			InputStream sequenceInputStream = new SequenceInputStream(streamList.elements());
			return sequenceInputStream;
		} else {
			return null;
		}

	}

	/**
	 * <p>Description : 儲存上傳檔</p>
	 * <p>Created By : Amy Hung</p>
	 * <p>Create Time : Mar 12, 2016</p>
	 * @param fileName
	 * @param inputStream
	 * @throws Exception
	 */
	public void saveUploadFile(String fileName, InputStream inputStream) throws Exception {

		File folder = new File(EnvUtils.getSendToInternalSharePath(), "liaRoc");

		File uploadFile = new File(folder, fileName + ".txt");

		if (uploadFile.getParentFile().exists() == false) {
			FileUtils.forceMkdir(uploadFile.getParentFile());
			// 2016-11-29 依長官指示加上 read write
			FileGenericHelper.setFolderReadWritable(uploadFile.getParentFile());
		}

		NBUtils.appLogger("[INFO]LOG檔案至=" + uploadFile.getAbsolutePath());

		OutputStream oStream = new FileOutputStream(uploadFile);

		byte[] buffer = new byte[8192];
		int bRead;

		while ((bRead = inputStream.read(buffer)) != -1) {
			oStream.write(buffer, 0, bRead);
		}

		inputStream.close();
		oStream.flush();
		oStream.close();

	}

	/**
	 * <p>Description : 呼叫服務平台，傳送公會通報資料</p>
	 * <p>Created By : Amy Hung</p>
	 * <p>Create Time : Mar 12, 2016</p>
	 * @param uploadList 傳送公會的通報明細資料 VO
	 * @param uploadFileName 公會通報檔名
	 * @param uploadStatus 公會通報主檔資料的處理狀態(回傳值)
	 * @return JobStatus.EXECUTE_SUCCESS/JobStatus.EXECUTE_FAILED
	 */
	@SuppressWarnings("deprecation")
	public int processUpload(List<LiaRocUploadDetailBaseVO> uploadList, String uploadFileName,
					Map<Long, String> uploadStatus) {

		// 預設為成功
		int executeResult = JobStatus.EXECUTE_SUCCESS;

		// 記錄失敗的通報主檔 list_id
		ArrayList<Long> failUploadIds = new ArrayList<Long>();

		// 有資料要通報
		if (uploadList != null && uploadList.size() > 0) {

			int totalMasterCount = this.getUploadMasterCount(uploadList);
			String requestUUID = null;

			NBUtils.appLogger("[INFO]通報主檔資料筆數: " + totalMasterCount);
			NBUtils.appLogger("[INFO]通報明細資料筆數: " + uploadList.size());

			InputStream inStream = null;

			// @ 解析通報資料
			try {

				NBUtils.appLogger("[INFO]解析通報資料..");

				// 把要通報的資料解析成 txt 格式
				inStream = this.parseToText(uploadList, failUploadIds);

				NBUtils.appLogger("[INFO]通報資料解析完成..");

			} catch (Exception e) {

				// 將所有通報主檔狀態設為 P
				for (LiaRocUploadDetailBaseVO detail : uploadList) {
					if (!uploadStatus.containsKey(detail.getUListId())) {
						uploadStatus.put(detail.getUListId(), LiaRocCst.LIAROC_UL_STATUS_PARSE_ERROR);
					}
				}

				NBUtils.appLogger("[ERROR]通報資料解析失敗，發生不可預期的錯誤, Error Message: " + e.getMessage());
				executeResult = JobStatus.EXECUTE_FAILED;

			}
			// end.

			// @ 資料解析正確，呼叫服務平台傳送通報資料
			if (executeResult == JobStatus.EXECUTE_SUCCESS) {

				// 失敗的明細資料筆數
				int failDetailCount = 0;

				// 有解析失敗的主檔ID
				if (failUploadIds.size() > 0) {
					for (Long id : failUploadIds) {
						if (!uploadStatus.containsKey(id)) {
							uploadStatus.put(id, LiaRocCst.LIAROC_UL_STATUS_PARSE_ERROR);
						}
					}
					failDetailCount = this.calUploadDetailCount(uploadList, failUploadIds);
					NBUtils.appLogger("[ERROR]警告：有解析失敗的通報明細資料..");
					NBUtils.appLogger("[ERROR]有 " + failUploadIds.size() + " 筆主檔(" + failDetailCount
									+ " 筆明細) 資料解析失敗..");

				}

				// 有要發送資料
				if (inStream != null) {

					LiaRocUploadRequestRoot requestVO = new LiaRocUploadRequestRoot();

					requestVO.setCreateTime(DateUtils.date2string(AppContext.getCurrentUserLocalTime(),
									"yyyyMMddHHmmss"));
					requestVO.setTotalCount(uploadList.size() - failDetailCount);
					requestVO.setUploadModule(getUploadModule());
					requestVO.setUploadType(getUploadType());
					requestVO.setUploadFileName(uploadFileName);

					try {
						// List<DataHandler> dataHandlers = new
						// ArrayList<DataHandler>();
						// DataHandler dataHandler = new DataHandler(new
						// InputStreamDataSoure(inStream));
						// dataHandlers.add(dataHandler);

						List<FileObject> files = new ArrayList<FileObject>();
						FileObject file = new FileObject();
						DataHandler dh = new DataHandler(new InputStreamDataSoure(inStream));
						file.setFile(dh);
						file.setFileName(dh.getName());
						files.add(file);

						NBUtils.appLogger("[INFO]準備呼叫服務平台....");

						RqHeader head = new RqHeader();

						// Call ESP...
						// 呼叫服務平台時，有可能它會回報失敗，則要將通報狀態設定為F
						// 以區分是 call ESP 時發生錯誤，還是 ESP 有接到，只是它回傳資料接收失敗
						StandardResponse rs = null;
						int maxCount = 4;
						for (int i = 0; i < maxCount; i++) {
							try {
								rs = espWSServiceCI.liaROCUpload(head, requestVO, files);
								break; //成功發送後中斷for迴圈
							} catch (Exception e) {
								if (i >= (maxCount - 1)) {
									throw e;
								} else {
									String errMsg = ExceptionInfoUtils.getExceptionMsg(e);
									if (errMsg.indexOf("Failed to access the WSDL") != -1) {
										//發送失敗(可能是網路瞬斷)暫停10秒,retry
										NBUtils.appLogger("[ERROR]Failed to access the WSDL :次數 " + (i + 1));
										Thread.sleep(10000);
									} else {
										//其它錯誤將訊息往外拋
										throw e;
									}
								}
							}
						}

						// RespXml resp =
						// commonWSCI.call(WebServiceName.ESP_LIAROC_UPLOAD_NB,
						// requestVO, dataHandlers);

						String statusIndi = LiaRocCst.LIAROC_UL_STATUS_SEND;

						if (rs != null
										&& !rs.getRsHeader().getReturnCode()
														.equals(LiaRocCst.LIAROC_UPLOAD_ESP_SUCCESSFUL_CODE)) {
							statusIndi = LiaRocCst.LIAROC_UL_STATUS_SEND_FAIL;
							NBUtils.appLogger("[INFO]呼叫服務平台發送通報有誤.." + rs.getRsHeader().getReturnCode());
							NBUtils.appLogger("[INFO]呼叫服務平台發送通報有誤.." + rs.getRsHeader().getReturnMsg());
							executeResult = JobStatus.EXECUTE_FAILED;
							/**寫入檔案**/
							try {
								this.saveUploadFile("LiaROC_" + uploadFileName + "_" + DateUtils.date2String(new Date()), inStream);
							} catch (Exception e) {
								NBUtils.appLogger("[ERROR]write file error" + e.getMessage());
							}
						} else {
							NBUtils.appLogger("[INFO]呼叫服務平台發送通報完成..");
						}

						requestUUID = (rs != null) ? rs.getRsHeader().getRqUID() : null;

						for (LiaRocUploadDetailBaseVO detail : uploadList) {
							if (!uploadStatus.containsKey(detail.getUListId())) {
								uploadStatus.put(detail.getUListId(), statusIndi);
							}
						}

						NBUtils.appLogger("[INFO]發送通報 - 主檔: " + (totalMasterCount - failUploadIds.size()) + " 筆, 明細: "
										+ (uploadList.size() - failDetailCount) + " 筆..");

					} catch (Exception e) {

						// 傳送到服務平台時發生錯誤，將通報狀態設定為(E:通報流程有誤)
						for (LiaRocUploadDetailBaseVO detail : uploadList) {
							if (!uploadStatus.containsKey(detail.getUListId())) {
								uploadStatus.put(detail.getUListId(), LiaRocCst.LIAROC_UL_STATUS_PROCESS_ERROR);
							}
						}

						NBUtils.appLogger("[ERROR]呼叫服務平台發送通報資料失敗，Error Message: " + e.getMessage());
						executeResult = JobStatus.EXECUTE_FAILED;
						/**寫入檔案**/
						try {
							this.saveUploadFile("LiaROC_" + uploadFileName + "_" + DateUtils.date2String(new Date()), inStream);
						} catch (Exception ex) {
							NBUtils.appLogger("[ERROR]write file error" + e.getMessage());
						}
					}

				}

			}
			// end..

			// 將通報的狀態更新回 DB
			int serialNum = Integer.parseInt(uploadFileName.replaceAll(getPrefixReqFileName(), ""));
			NBUtils.appLogger("[INFO]更新通報狀態.." + requestUUID + ":" + serialNum);
			liaRocUpload2019CI.updateUploadStatus(uploadStatus, requestUUID, serialNum,
							new Date(), AppContext.getCurrentUser().getUserId());

		} else {
			NBUtils.appLogger("[INFO]沒有要通報的資料..");
		}

		return executeResult;

	}

	/**
	 * <p>Description : 依主檔的 id list 判斷有多少明細資料筆數</p>
	 * <p>Created By : Amy Hung</p>
	 * <p>Create Time : Mar 15, 2016</p>
	 * @param uploadList 上傳公會通報明細資料
	 * @param uploadIds 要過濾的主檔資料 id list
	 * @return
	 */
	private int calUploadDetailCount(List<LiaRocUploadDetailBaseVO> uploadList, ArrayList<Long> uploadIds) {

		int count = 0;

		for (LiaRocUploadDetailBaseVO detail : uploadList) {
			if (uploadIds.contains(detail.getUListId())) {
				count += 1;
			}
		}

		return count;

	}

	/**
	 * <p>Description : 取得通報主檔資料筆數</p>
	 * <p>Created By : Amy Hung</p>
	 * <p>Create Time : Mar 15, 2016</p>
	 * @param uploadList 通報明細資料
	 * @return 通報主檔資料筆數
	 */
	private int getUploadMasterCount(List<LiaRocUploadDetailBaseVO> uploadList) {

		List<Long> countedIds = new ArrayList<Long>();

		int count = 0;

		for (LiaRocUploadDetailBaseVO detail : uploadList) {
			if (!countedIds.contains(detail.getUListId())) {
				count++;
				countedIds.add(detail.getUListId());
			}
		}

		return count;
	}

	/**
	 * <p>Description : 取得公會通報檔名前綴</p>
	 * <p>Created By : Amy Hung</p>
	 * <p>Create Time : Mar 15, 2016</p>
	 * @return
	 */
	public String getPrefixReqFileName() {
		// 公會通報檔檔名前綴設在 t_para_def 裡。
		if (StringUtils.isNullOrEmpty(prefixReqFileName)) {
			prefixReqFileName = Para.getParaValue(LiaRocCst.PARAM_LIAROC_UL_FILENAME_REQUEST);
		}
		return prefixReqFileName;
	}

	public Map<String, Object> getMap(Object obj) throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();
		BeanInfo info = null;
		if (LiaRocCst.LIAROC_UL_TYPE_107.equals(this.getUploadType())) {
			info = Introspector.getBeanInfo(LiaRocUpload107DetailVO.class);
		} else {
			info = Introspector.getBeanInfo(LiaRocUploadDetailVO.class);
		}

		SimpleDateFormat df2 = new SimpleDateFormat("yyyyMMdd");

		for (PropertyDescriptor pdr : info.getPropertyDescriptors()) {

			Method reader = pdr.getReadMethod();

			// 排除掉 getMap 方法。
			if (reader != null && !reader.getName().equals("getMap")) {

				String key = pdr.getName();
				Object value = reader.invoke(obj);

				// 如果該欄位為日期，將它轉成民國年
				if (value != null && value instanceof Date) {
					String mDate = convertTWDate((Date) value, df2);
					result.put(key, mDate);
				} else {
					result.put(key, value);
				}
			}
		}

		return result;
	}

	public String convertTWDate(Date date, SimpleDateFormat format) {

		if (date == null || format == null)
			return "";

		try {
			String pattern = format.toPattern();
			if (StringUtils.isNullOrEmpty(pattern)) {
				return "";
			}

			String spilitChar = pattern.replaceAll("Y", "").replaceAll("y", "")
							.replaceAll("M", "").replaceAll("m", "")
							.replaceAll("D", "").replaceAll("d", "")
							.replaceAll("H", "").replaceAll("h", "")
							.replaceAll("S", "").replaceAll("s", "");

			Calendar cal = Calendar.getInstance();
			cal.setTime(date);
			int year = cal.get(Calendar.YEAR);
			int month = cal.get(Calendar.MONTH) + 1;
			int day = cal.get(Calendar.DAY_OF_MONTH);

			// Rita 說 1911 年前不處理 
			if (year - 1911 < 0) {
				return null;
			}

			// convert to Minguo Year
			String formattedYear = String.format("%04d", year - 1911);
			String formattedMonth = String.format("%02d", month);
			String formattedDay = String.format("%02d", day);

			if (StringUtils.isNullOrEmpty(spilitChar)) {
				return formattedYear + formattedMonth + formattedDay;
			} else if (spilitChar.length() >= 2) {
				return formattedYear + spilitChar.charAt(0) + formattedMonth + spilitChar.charAt(1) + formattedDay;
			} else {
				return "";
			}

		} catch (Exception e) {
			return null;
		}
	}

	public ResultMessage processUpload2020(List<LiaRocUploadDetailBaseVO> uploadList, Map<Long, String> uploadStatus,
					String liaRocULType) {

		// 預設為成功
		int executeResult = JobStatus.EXECUTE_SUCCESS;
		
        // PCR-507841_公會通報系統報錯新增系統警示
        ResultMessage result = new ResultMessage();
        List<String> errorList = new ArrayList<String>();

		// 記錄失敗的通報主檔 list_id
		ArrayList<Long> failUploadIds = new ArrayList<Long>();

		// 有資料要通報
		if (uploadList != null && uploadList.size() > 0) {

			int totalMasterCount = this.getUploadMasterCount(uploadList);
			String rqUid = RstfClientUtils.genRqUid(Liaroc20Cst.TF_UPLOAD.getCode(), Thread.currentThread().getId());
			NBUtils.appLogger("[INFO]通報主檔資料筆數: " + totalMasterCount);
			NBUtils.appLogger("[INFO]通報明細資料筆數: " + uploadList.size());

			List<Liaroc20UploadRqReceiveVO> dataRqReceiveList = new ArrayList<Liaroc20UploadRqReceiveVO>();
			List<Liaroc20UploadRqInforceVO> dataRqInforceList = new ArrayList<Liaroc20UploadRqInforceVO>();
			List<Liaroc20UploadRqClm107VO> dataRqClm107List = new ArrayList<Liaroc20UploadRqClm107VO>();

			for (LiaRocUploadDetailBaseVO uploadDetailVO : uploadList) {

				if (LiaRocCst.LIAROC_UL_TYPE_RECEIVE.equals(liaRocULType)) {
					Liaroc20UploadRqBaseVO rqBaseVO = ((LiaRocUploadDetail2020WrapperVO) uploadDetailVO).convertUploadRqBaseVO(liaRocULType);
					dataRqReceiveList.add((Liaroc20UploadRqReceiveVO) rqBaseVO);
				} else if (LiaRocCst.LIAROC_UL_TYPE_INFORCE.equals(liaRocULType)) {
					Liaroc20UploadRqBaseVO rqBaseVO = ((LiaRocUploadDetail2020WrapperVO) uploadDetailVO).convertUploadRqBaseVO(liaRocULType);
					dataRqInforceList.add((Liaroc20UploadRqInforceVO) rqBaseVO);
				} else if (LiaRocCst.LIAROC_UL_TYPE_107.equals(liaRocULType)) {
					Liaroc20UploadRqBaseVO rqBaseVO = ((LiaRocUploadDetail1072020WrapperVO) uploadDetailVO).convertUploadRqBaseVO(liaRocULType);
					dataRqClm107List.add((Liaroc20UploadRqClm107VO) rqBaseVO);
				}
			}

			// @呼叫服務平台傳送通報資料
			if (executeResult == JobStatus.EXECUTE_SUCCESS) {

				// 失敗的明細資料筆數
				int failDetailCount = 0;

				try {

					Liaroc20UploadRsVO uploadRsVO = null;
					NBUtils.appLogger("[INFO]準備呼叫服務平台....");

					// Call ESP...
					// 呼叫服務平台時，有可能它會回報失敗，則要將通報狀態設定為F
					// 以區分是 call ESP 時發生錯誤，還是 ESP 有接到，只是它回傳資料接收失敗
					
					
					int maxCount = 4;
					for (int i = 0; i < maxCount; i++) {
						try {
							if (LiaRocCst.LIAROC_UL_TYPE_RECEIVE.equals(liaRocULType)) {
								String disableLiaroc20 = Para.getParaValue(LiaRocCst.PARAM_DISABLE_LIAROC20_RECEIVE);
								if(CodeCst.YES_NO__YES.equals(disableLiaroc20)) {
									uploadRsVO = new Liaroc20UploadRsVO();
									NBUtils.appLogger("[INFO]已關閉公會2.0通報..");
								} else {
									uploadRsVO = liaroc20UplaodCI.doLiaroc20UploadReceive(rqUid, Liaroc20Cst.MN_UNB.getCode(),
											dataRqReceiveList, Liaroc20Cst.PM_ASYNC.getCode());
								}
							} else if (LiaRocCst.LIAROC_UL_TYPE_INFORCE.equals(liaRocULType)) {
								String disableLiaroc20 = Para.getParaValue(LiaRocCst.PARAM_DISABLE_LIAROC20_INFORCE);
								if(CodeCst.YES_NO__YES.equals(disableLiaroc20)) {
									uploadRsVO = new Liaroc20UploadRsVO();
									NBUtils.appLogger("[INFO]已關閉公會2.0通報..");	
								} else {
									uploadRsVO = liaroc20UplaodCI.doLiaroc20UploadInforce(rqUid, Liaroc20Cst.MN_UNB.getCode(), dataRqInforceList);	
								}
							} else if (LiaRocCst.LIAROC_UL_TYPE_107.equals(liaRocULType)) {
								uploadRsVO = liaroc20UplaodCI.doLiaroc20UploadClm107(rqUid, Liaroc20Cst.MN_CLM.getCode(), dataRqClm107List);
							} else {
								ApplicationLogger.addLoggerData("[ERROR]上傳失敗 無法解析LIAROC_UL_TYPE(收件/承保/107) ");
							}
							break; //成功發送後中斷for迴圈
						} catch (Exception e) {
							if (i >= (maxCount - 1)) {
								throw e;
							} else {
								String errMsg = ExceptionInfoUtils.getExceptionMsg(e);
								if (errMsg.indexOf("Failed to access the WSDL") != -1) {
									//發送失敗(可能是網路瞬斷)暫停10秒,retry
									NBUtils.appLogger("[ERROR]Failed to access the WSDL :次數 " + (i + 1));
									Thread.sleep(10000);
								} else {
									//其它錯誤將訊息往外拋
									throw e;
								}
							}
						}
					}

					String statusIndi = LiaRocCst.LIAROC_UL_STATUS_SEND;

					if (uploadRsVO == null) {
						statusIndi = LiaRocCst.LIAROC_UL_STATUS_SEND_FAIL;
						NBUtils.appLogger("[INFO]呼叫服務平台發送通報有誤.. null");
						executeResult = JobStatus.EXECUTE_FAILED;
					} else if (uploadRsVO != null
									&& uploadRsVO.getError() != null) {
						statusIndi = LiaRocCst.LIAROC_UL_STATUS_SEND_FAIL;
						NBUtils.appLogger("[INFO]呼叫服務平台發送通報有誤.." + uploadRsVO.getError().getCode());
						NBUtils.appLogger("[INFO]呼叫服務平台發送通報有誤.." + uploadRsVO.getError().getMessage());
						executeResult = JobStatus.EXECUTE_FAILED;
					} else {
						if(uploadRsVO.getSuccess() != null) {
							NBUtils.appLogger("[INFO]呼叫服務平台發送通報完成..");	
						}
					}

					for (LiaRocUploadDetailBaseVO detail : uploadList) {
						if (!uploadStatus.containsKey(detail.getUListId())) {
							uploadStatus.put(detail.getUListId(), statusIndi);
						}
					}

					NBUtils.appLogger("[INFO]發送通報 - 主檔: " + (totalMasterCount - failUploadIds.size()) + " 筆, 明細: "
									+ (uploadList.size() - failDetailCount) + " 筆..");

				} catch (Exception e) {

					// 傳送到服務平台時發生錯誤，將通報狀態設定為(E:通報流程有誤)
					for (LiaRocUploadDetailBaseVO detail : uploadList) {
						if (!uploadStatus.containsKey(detail.getUListId())) {
							uploadStatus.put(detail.getUListId(), LiaRocCst.LIAROC_UL_STATUS_PROCESS_ERROR);
						}
					}
					NBUtils.appLogger("[ERROR]呼叫服務平台發送通報資料失敗，Error Message: " + e.getMessage());
					executeResult = JobStatus.EXECUTE_FAILED;
					
					errorList.add(ExceptionInfoUtils.getExceptionMsg(e));
				}

			}
			// end..

			// 將通報的狀態更新回 DB
			NBUtils.appLogger("[INFO]更新通報狀態.." + rqUid);
			String fileName = getUploadRequestFileName();
			int serialNum = Integer.parseInt(fileName.replaceAll(getPrefixReqFileName(), ""));
			liaRocUpload2020CI.updateUploadStatus(uploadStatus, rqUid, serialNum,
							new Date(), AppContext.getCurrentUser().getUserId());
		} else {
			NBUtils.appLogger("[INFO]沒有要通報的資料..");
		}

		result.setErrorList(errorList);
		result.setExecuteResult(executeResult);
		return result;

	}

	/* 2019 */
	/* ************************************************************************************************** */
	/* 2020 */

	protected abstract List<? extends LiaRocUploadDetailBaseVO> getUpload2020Details();

	//公會2.0,單次上傳最大筆數 
	public static int LIAROC20_MAX_UPLOAD_SIZE = 3000 ;
	
	
	/**
	 * <h1>批處理主程式<h1>
	 * <p>通報主檔資料是以保單為單位，通報明細資料是以保項為單位，因此一筆通報主檔資料會有多筆的通報明細資料;
	 * 上傳公會時，若該主檔下的任一筆明細資料解析發生錯誤，則該筆通報主檔狀態會是P(解析有錯)並且不會發送公會<p>
	 */
	protected int subProcess2020(String liaRocULType) throws Exception {
		//		LiaRocCst.LIAROC_UL_TYPE_RECEIVE
		// 批處理狀況預設為成功
		int executeResult = JobStatus.EXECUTE_SUCCESS;

		// 記錄各通報主檔資料更新回 DB 的狀態
		Map<Long, String> uploadStatus = new HashMap<Long, String>();

        List<String> errorList = new ArrayList<String>();
		
		UserTransaction trans = null;
		try {

			List<? extends LiaRocUploadDetailBaseVO> liaRocUploadDetailBaseVOs = this.getUpload2020Details();
			do { //IR-321585 PMC_UAT,增加通報查詢筆數限制
				trans = Trans.getUserTransaction();
				trans.begin();

				List<LiaRocUploadDetailBaseVO> uploadList = new ArrayList<LiaRocUploadDetailBaseVO>();

				ApplicationLogger.addLoggerData("[INFO]上傳TotalDetail: " + liaRocUploadDetailBaseVOs.size());
				if (liaRocUploadDetailBaseVOs.size() > 0) {
					for (LiaRocUploadDetailBaseVO vo : liaRocUploadDetailBaseVOs) {
						uploadList.add(vo);

						//每LIAROC20_MAX_UPLOAD_SIZE筆通報
						if (uploadList.size() >= LIAROC20_MAX_UPLOAD_SIZE) {
							//取下一筆，若ulistid不一樣就先通報出去
							int index = liaRocUploadDetailBaseVOs.indexOf(vo);
							LiaRocUploadDetailBaseVO nextVO = null;
							if ((index + 1) < liaRocUploadDetailBaseVOs.size()) {
								nextVO = liaRocUploadDetailBaseVOs.get(index + 1);
							}

							if (nextVO != null && !nextVO.getUListId().equals(vo.getUListId())) {
								// 傳送通報資料
								ApplicationLogger.addLoggerData("[INFO]上傳SubDetail: " + uploadList.size());
								ResultMessage resultMessage = this.processUpload2020(uploadList, uploadStatus, liaRocULType);
								errorList.addAll(resultMessage.getErrorList());
								executeResult = resultMessage.getExecuteResult();
								
								uploadList.clear();
								uploadStatus.clear();
								HibernateSession3.currentSession().flush();
							}
						}
					}

					if (uploadList.size() > 0) {
						ApplicationLogger.addLoggerData("[INFO]上傳SubDetail: " + uploadList.size());
						// 傳送通報資料
						ResultMessage resultMessage = this.processUpload2020(uploadList, uploadStatus, liaRocULType);
						errorList.addAll(resultMessage.getErrorList());
						executeResult = resultMessage.getExecuteResult();
						
						HibernateSession3.currentSession().flush();
					}

				} else {
					ApplicationLogger.addLoggerData("[INFO]沒有要通報的資料");
				}

				trans.commit();

				uploadList.clear();
				uploadStatus.clear();

				liaRocUploadDetailBaseVOs.clear();
				liaRocUploadDetailBaseVOs = null;

				liaRocUploadDetailBaseVOs = this.getUpload2020Details();

				//IR-321585 PMC_UAT,增加通報查詢筆數限制
			} while (liaRocUploadDetailBaseVOs.size() > 0);

			ApplicationLogger.addLoggerData("[INFO]Batch Job finished..");
		} catch (Throwable e) {
			trans.rollback();
			String message = ExceptionInfoUtils.getExceptionMsg(e);
			ApplicationLogger.addLoggerData("[ERROR]發生不可預期的錯誤, Error Message:" + message);
			executeResult = JobStatus.EXECUTE_FAILED;
			ApplicationLogger.setReturnCode(ApplicationLogger.RETURN_CODE_FAIL);

			errorList.add(message);
		}

		try {
			trans = Trans.getUserTransaction();
			trans.begin();
			ApplicationLogger.addLoggerData("[INFO]update no_item start..");
			//更新主檔為 NO_ITEM及通報資料狀態為9-無可通報保項(count明細檔check_status='1'為0 ，表示主檔為9-無可通報保項)
			liaRocUpload2020CI.uploadLiaRocUploadStatusTo9();
			ApplicationLogger.addLoggerData("[INFO]update no_item finished..");
			trans.commit();
		} catch (Throwable e) {
			trans.rollback();
			String message = ExceptionInfoUtils.getExceptionMsg(e);
			ApplicationLogger.addLoggerData("[ERROR]update no_item, Error Message:" + message);
			
			errorList.add(message);
		}

		// PCR-507841_公會通報系統報錯新增系統警示
		if(CollectionUtils.isNotEmpty(errorList)) {
			
			// 取得要發的信箱設定
			List<String> params = Arrays.asList(
					Para.getParaValue(LiaRocCst.LIAROC_ALERT_EMAIL_ASD_TO),
					Para.getParaValue(LiaRocCst.LIAROC_ALERT_EMAIL_UNB_CC),
					Para.getParaValue(LiaRocCst.LIAROC_ALERT_EMAIL_POS_CC));
			

			List<String> emails = NBUtils.splitMultiple(params, ",");
			
			SendErrorNotificationResponseVo result = nbEventCI.sendErrorNotification(
					errorList, emails, new RequestInfoVo(CRSSysCode.SYS_CODE__UNB, getClass().getSimpleName()));
			
			if (result != null && result.isSuccess()) {
				ApplicationLogger.addLoggerData("call nbEventCI.sendErrorNotification SUCCESS. EmailLaunchId:" + result.getEmailLaunchId());
			} else {
				String errorMsg = result != null ? result.getErrorMessage() : "sendErrorNotification response is null";
				ApplicationLogger.addLoggerData("call nbEventCI.sendErrorNotification 發生錯誤.Error Message:" + errorMsg);
			}
		}
		
		return executeResult;
	}
}
