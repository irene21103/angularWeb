package com.ebao.ls.liaRoc.ci;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ebao.ls.liaRoc.LiaRocCst;
import com.ebao.ls.liaRoc.bo.LiaRocDownload;
import com.ebao.ls.liaRoc.bo.LiaRocDownload2020;
import com.ebao.ls.liaRoc.bo.LiaRocDownload2020VO;
import com.ebao.ls.liaRoc.bo.LiaRocDownloadDetail;
import com.ebao.ls.liaRoc.bo.LiaRocDownloadDetail2020;
import com.ebao.ls.liaRoc.bo.LiaRocDownloadDetail2020VO;
import com.ebao.ls.liaRoc.bo.LiaRocDownloadDetailVO;
import com.ebao.ls.liaRoc.data.LiaRocDownload2020Dao;
import com.ebao.ls.liaRoc.data.LiaRocDownloadDao;
import com.ebao.ls.liaRoc.data.LiaRocDownloadDetail2020Dao;
import com.ebao.ls.liaRoc.ds.LiaRocDownload2020Service;
import com.ebao.ls.liaRoc.ds.LiaRocDownloadService;
import com.ebao.ls.pa.nb.bs.rule.vo.ProposalVO;
import com.ebao.ls.riskAggregation.data.bo.AggregationResult;
import com.ebao.ls.rstf.ci.liaroc20.Liaroc20Cst;
import com.ebao.ls.uw.ds.vo.LiaRocPaidInjuryMedicalDetailVO;
import com.ebao.pub.framework.GenericException;
import com.ebao.pub.util.BeanUtils;
import com.ebao.pub.util.DateUtils;

public class LiaRocDownloadCIProxy implements LiaRocDownloadCI, LiaRocDownloadWSCI {
	@Resource(name = LiaRocDownload2019CI.BEAN_2019_DEFAULT)
	private LiaRocDownload2019CI liaRocDownload2019CI;

	@Resource(name = LiaRocDownloadService.BEAN_DEFAULT)
	private LiaRocDownloadService liaRocDownloadService;

	@Resource(name = LiaRocDownload2020CI.BEAN_DEFAULT)
	private LiaRocDownload2020CI liaRocDownload2020CI;

	@Resource(name = LiaRocDownloadDao.BEAN_DEFAULT)
	private LiaRocDownloadDao<LiaRocDownload> liaRocDownloadDao;

	@Resource(name = LiaRocDownload2020Dao.BEAN_DEFAULT)
	private LiaRocDownload2020Dao<LiaRocDownload2020> liaRocDownload2020Dao;

	@Resource(name = LiaRocDownloadDetail2020Dao.BEAN_DEFAULT)
	private LiaRocDownloadDetail2020Dao liaRocDownloadDetail2020Dao;

	@Resource(name = LiaRocDownload2020Service.BEAN_DEFAULT)
	private LiaRocDownload2020Service liaRocDownload2020Service;

	private static final Logger logger = LoggerFactory.getLogger(LiaRocDownloadCIProxy.class);

	// -------------------------可同時新舊一併處理---------------------------
	@Override
	public boolean send(String moduleName, Set<String> ids) throws GenericException {
		return send(moduleName, null, ids);
	}

	@Override
	public boolean send(String moduleName, Long policyId, Set<String> ids) throws GenericException {
		if (liaRocDownload2020CI.isLiaRocV2020DownloadDate()) {

			List<String[]> sendIdnos = new ArrayList<String[]>();
			for (String idno : ids) {
				sendIdnos.add(new String[] { idno });
			}

			if (policyId == null) {
				policyId = (new Date().getTime() * -1);
			}

			List<LiaRocDownload2020VO> downloadList = liaRocDownload2020CI.download(moduleName,
					Liaroc20Cst.DOWNLOAD_ROLE_INSURED.getCode(), policyId, sendIdnos);

			if (downloadList == null || downloadList.size() == 0) {
				return false;
			} else if (LiaRocCst.LIAROC_DL_STATUS_ERROR.equals(downloadList.get(0).getLiarocDownloadStatus())) {
				return false;
			}
			return true;
		} else {
			return liaRocDownload2019CI.send(policyId, ids);
		}
	}

	@Override
	public boolean downloadWithOldForeignerId(String moduleName, Long policyId, List<String[]> sendIdnos) throws GenericException {
		if (liaRocDownload2020CI.isLiaRocV2020DownloadDate()) {

			if (policyId == null) {
				policyId = (new Date().getTime() * -1);
			}

			List<LiaRocDownload2020VO> downloadList = liaRocDownload2020CI.download(moduleName,
					Liaroc20Cst.DOWNLOAD_ROLE_INSURED.getCode(), policyId, sendIdnos);

			if (downloadList == null || downloadList.size() == 0) {
				return false;
			} else if (LiaRocCst.LIAROC_DL_STATUS_ERROR.equals(downloadList.get(0).getLiarocDownloadStatus())) {
				return false;
			}
			return true;
		} else {
			Set<String> ids = new HashSet<String>();
			for (String[] idnos : sendIdnos) {
				for (String idno : idnos) {
					ids.add(idno);
				}
			}
			return liaRocDownload2019CI.send(policyId, ids);
		}
	}

	@Override
	public boolean downloadWithOldForeignerIdAndCopyTerminate(String moduleName, Long policyId, List<String[]> sendIdnos)
			throws GenericException {
		if (liaRocDownload2020CI.isLiaRocV2020DownloadDate()) {

			if (policyId == null) {
				policyId = (new Date().getTime() * -1);
			}

			List<LiaRocDownload2020VO> downloadList = liaRocDownload2020CI.download(moduleName,
					Liaroc20Cst.DOWNLOAD_ROLE_INSURED.getCode(), policyId, sendIdnos);

			if (downloadList == null || downloadList.size() == 0) {
				return false;
			} else if (LiaRocCst.LIAROC_DL_STATUS_ERROR.equals(downloadList.get(0).getLiarocDownloadStatus())) {
				return false;
			}

			for (LiaRocDownload2020VO downloadVO : downloadList) {
				for (LiaRocDownloadDetail2020VO detailVO : downloadVO.getDownloadDetails()) {
					detailVO.setListId(null);
					detailVO.setDownloadListId(null);
				}
				downloadVO.setListId(null);
				downloadVO.setUsedKind(Liaroc20Cst.DOWNLOAD_KIND_TERMINATE_CHECK.getCode());
				liaRocDownload2020Service.save(downloadVO, true);
			}

			//要保人身份下載
			liaRocDownload2020CI.downloadTerminateCheck(Liaroc20Cst.MN_UNB.getCode(),
					Liaroc20Cst.DOWNLOAD_ROLE_HOLDER.getCode(), policyId, sendIdnos);

			return true;
		} else {
			Set<String> ids = new HashSet<String>();
			for (String[] idnos : sendIdnos) {
				for (String idno : idnos) {
					ids.add(idno);
				}
			}
			return liaRocDownload2019CI.send(policyId, ids);
		}
	}

	@Override
	public List<LiaRocDownloadDetail> findGroupingReceiveDetailList(String certiCode) throws GenericException {
		if (isLiaroc2020ByCertiCode(certiCode)) {
			String liarocType = "R";
			String orderBy = "list_id";
			boolean isOrderByAsc = true;

			List<LiaRocDownloadDetail2020> list = liaRocDownloadDetail2020Dao
					.findDownloadDetailDataByCriteria(certiCode, liarocType, orderBy, isOrderByAsc);

			List<LiaRocDownloadDetail> oldList = new ArrayList<LiaRocDownloadDetail>();
			for (LiaRocDownloadDetail2020 detail2020 : list) {
				LiaRocDownloadDetail oldDetail = new LiaRocDownloadDetail();
				BeanUtils.copyProperties(oldDetail, detail2020);
				oldDetail.setPolicyId(detail2020.getLiarocPolicyCode());
				oldList.add(oldDetail);
			}
			return oldList;
		} else {
			return liaRocDownload2019CI.findGroupingReceiveDetailList(certiCode);
		}
	}

	@Override
	public List<LiaRocDownloadDetail> findGroupingIssueDetailList(String certiCode) throws GenericException {
		if (isLiaroc2020ByCertiCode(certiCode)) {
			String liarocType = "L";
			String orderBy = "list_id";
			boolean isOrderByAsc = true;

			List<LiaRocDownloadDetail2020> list = liaRocDownloadDetail2020Dao
					.findDownloadDetailDataByCriteria(certiCode, liarocType, orderBy, isOrderByAsc);

			List<LiaRocDownloadDetail> oldList = new ArrayList<LiaRocDownloadDetail>();
			for (LiaRocDownloadDetail2020 detail2020 : list) {
				LiaRocDownloadDetail oldDetail = new LiaRocDownloadDetail();
				LiaRocDownload oldDownload = new LiaRocDownload();
				
				// 兩張表不一致, 先一個一個對copy, 讓資料相容於2019舊表給保全CSValidation用, 以免copyProperties異常
				oldDetail.setListId(detail2020.getListId());        
				
				oldDetail.setLiarocType(detail2020.getLiarocType());
				oldDetail.setLiarocCompanyCode(detail2020.getLiarocCompanyCode());
				oldDetail.setName(detail2020.getName());
				oldDetail.setCertiCode(detail2020.getCertiCode());
				oldDetail.setBirthday(detail2020.getBirthday());
				oldDetail.setGender(detail2020.getGender());				
				oldDetail.setPolicyId(detail2020.getLiarocPolicyCode());
				oldDetail.setLiarocPolicyType(detail2020.getLiarocPolicyType());
				oldDetail.setLiarocProductCategory(detail2020.getLiarocProductCategory());
				oldDetail.setLiarocProductType(detail2020.getLiarocProductType());
				oldDetail.setLiability01(detail2020.getLiability01());
				oldDetail.setLiability02(detail2020.getLiability02());
				oldDetail.setLiability03(detail2020.getLiability03());
				oldDetail.setLiability04(detail2020.getLiability04());
				oldDetail.setLiability05(detail2020.getLiability05());
				oldDetail.setLiability06(detail2020.getLiability06());
				oldDetail.setLiability07(detail2020.getLiability07());
				oldDetail.setLiability08(detail2020.getLiability08());
				oldDetail.setLiability09(detail2020.getLiability09());
				oldDetail.setLiability10(detail2020.getLiability10());
				oldDetail.setLiability11(detail2020.getLiability11());
				oldDetail.setLiability12(detail2020.getLiability12());
				oldDetail.setLiability13(detail2020.getLiability13());
				oldDetail.setLiability14(detail2020.getLiability14());
				oldDetail.setLiability15(detail2020.getLiability15());
				oldDetail.setLiability16(detail2020.getLiability16());
				
				oldDetail.setValidateDate(detail2020.getValidateDate());
				oldDetail.setExpiredDate(detail2020.getExpiredDate());
				oldDetail.setPrem(detail2020.getPrem());
				oldDetail.setLiarocChargeMode(detail2020.getLiarocChargeMode());
				oldDetail.setLiarocPolicyStatus(detail2020.getLiarocPolicyStatus());
				oldDetail.setStatusEffectDate(detail2020.getStatusEffectDate());
				oldDetail.setPhName(detail2020.getPhName());
				oldDetail.setPhCertiCode(detail2020.getPhCertiCode());
				oldDetail.setPhBirthday(detail2020.getPhBirthday());
				oldDetail.setLiarocRelationToPh(detail2020.getLiarocRelationToPh());
				oldDetail.setSendDate(detail2020.getSendDate()); 
				
				if(detail2020.getLiarocDownload() != null){
					oldDownload.setListId(detail2020.getLiarocDownload().getListId());
					oldDownload.setCertiCode(detail2020.getLiarocDownload().getCertiCode());
					//oldDownload.setTransUuid(detail2020.getLiarocDownload().getTransUuid()); // 2020表無此資料
					//oldDownload.setFileName(detail2020.getLiarocDownload().getFileName()); // 2020表無此資料
					oldDownload.setRequestTime(detail2020.getLiarocDownload().getRequestTime());
					oldDownload.setResponseTime(detail2020.getLiarocDownload().getResponseTime());
					oldDownload.setLiaRocDownloadStatus(detail2020.getLiarocDownload().getLiarocDownloadStatus());
					oldDownload.setNoDataFlag(detail2020.getLiarocDownload().getNoDataFlag());
					oldDownload.setPolicyId(detail2020.getLiarocDownload().getPolicyId());
					
				}
				oldDetail.setLiarocDownload(oldDownload);
				
				oldList.add(oldDetail);
			}
			return oldList;
		} else {
			return liaRocDownload2019CI.findGroupingIssueDetailList(certiCode);
		}
	}

	@Override
	public List<AggregationResult> calcRA306(ProposalVO proposal) {
		if (isLiaroc2020(proposal.getPolicyId())) {
			return liaRocDownload2020CI.calcRA306(proposal);
		} else {
			return liaRocDownload2019CI.calcRA306(proposal);
		}
	}

	@Override
	public List<AggregationResult> calcRA319(ProposalVO proposal) {
		if (isLiaroc2020(proposal.getPolicyId())) {
			return liaRocDownload2020CI.calcRA319(proposal);
		} else {
			return liaRocDownload2019CI.calcRA319(proposal);
		}
	}

	@Override
	public List<AggregationResult> calcRA325(ProposalVO proposal) {
		if (isLiaroc2020(proposal.getPolicyId())) {
			return liaRocDownload2020CI.calcRA325(proposal);
		} else {
			return liaRocDownload2019CI.calcRA325(proposal);
		}
	}

	@Override
	public List<AggregationResult> calcRA327(ProposalVO proposal) {
		if (isLiaroc2020(proposal.getPolicyId())) {
			return liaRocDownload2020CI.calcRA327(proposal);
		} else {
			return liaRocDownload2019CI.calcRA327(proposal);
		}
	}

	@Override
	public List<AggregationResult> calcRA330(ProposalVO proposal) {
		if (isLiaroc2020(proposal.getPolicyId())) {
			return liaRocDownload2020CI.calcRA330(proposal);
		} else {
			return liaRocDownload2019CI.calcRA330(proposal);
		}
	}

	@Override
	public List<AggregationResult> calcRA346(ProposalVO proposal) {
		if (isLiaroc2020(proposal.getPolicyId())) {
			return liaRocDownload2020CI.calcRA346(proposal);
		} else {
			return liaRocDownload2019CI.calcRA346(proposal);
		}
	}

	@Override
	public List<AggregationResult> calcRA369(ProposalVO proposal, Date dateBfEffectiveDate) {
		if (isLiaroc2020(proposal.getPolicyId())) {
			return liaRocDownload2020CI.calcRA369(proposal, dateBfEffectiveDate);
		} else {
			return liaRocDownload2019CI.calcRA369(proposal, dateBfEffectiveDate);
		}
	}

	@Override
	public List<AggregationResult> calcRA373(ProposalVO proposal) {
		if (isLiaroc2020(proposal.getPolicyId())) {
			return liaRocDownload2020CI.calcRA373(proposal);
		} else {
			return liaRocDownload2019CI.calcRA373(proposal);
		}
	}

	@Override
	public List<AggregationResult> calcRA384(ProposalVO proposal) {
		if (isLiaroc2020(proposal.getPolicyId())) {
			return liaRocDownload2020CI.calcRA384(proposal);
		} else {
			return liaRocDownload2019CI.calcRA384(proposal);
		}
	}

	@Override
	public List<AggregationResult> calcRA390(ProposalVO proposal) {
		if (isLiaroc2020(proposal.getPolicyId())) {
			return liaRocDownload2020CI.calcRA390(proposal);
		} else {
			return liaRocDownload2019CI.calcRA390(proposal);
		}
	}

	@Override
	public int getLiarocCaseNumByCertiCode(boolean isPOS, Long policyId, String certiCode) {
		if (isLiaroc2020(policyId, certiCode)) {
			return liaRocDownload2020Dao.getLiarocCaseNumByCertiCode(isPOS ? null : policyId, certiCode);
		} else {
			return liaRocDownload2019CI.getLiarocCaseNumByCertiCode(isPOS, policyId, certiCode);
		}
	}

	@Override
	public int countPolicyDownloadCount(Long policyId) {
		if (isLiaroc2020(policyId)) {
			return liaRocDownload2020CI.countPolicyDownloadCount(policyId);
		} else {
			return liaRocDownload2019CI.countPolicyDownloadCount(policyId);
		}
	}

	@Override
	public void saveLiaRocDoanloadLog(String certiCode, String name, String policyCode, String quereyFunction) {
		liaRocDownload2019CI.saveLiaRocDoanloadLog(certiCode, name, policyCode, quereyFunction);
	}

	@Override
	public Map<String, Long> getOtherTotalAnnPremByPolicyId(Long policyId) {
		if (isLiaroc2020(policyId)) {
			return liaRocDownload2020CI.getOtherTotalAnnPremByPolicyId(policyId);
		} else {
			return liaRocDownloadService.getOtherTotalAnnPremByPolicyId(policyId);
		}
	}

	@Override
	public Map<String, Long> getCompTotalAnnPremByPolicyId(Long policyId) {
		if (isLiaroc2020(policyId)) {
			return liaRocDownload2020CI.getCompTotalAnnPremByPolicyId(policyId);
		} else {
			return liaRocDownloadService.getCompTotalAnnPremByPolicyId(policyId);
		}
	}

	@Override
	public List<Map<String, Object>> findGroupingIssueDetailData(String certiCode, Long policyId) {
		if (isLiaroc2020(policyId, certiCode)) {
			return liaRocDownload2020Dao.findGroupingIssueDetailData(certiCode, policyId);
		} else {
			return liaRocDownload2019CI.findGroupingIssueDetailData(certiCode, policyId);
		}
	}

	@Override
	public List<Map<String, Object>> findGroupLiaRocDetailData(String certiCode, String type, Long policyId) {
		if (isLiaroc2020(policyId, certiCode)) {
			return liaRocDownload2020CI.findGroupLiaRocDetailData(certiCode, type, policyId);
		} else {
			return liaRocDownloadService.findGroupLiaRocDetailData(certiCode, type, policyId);
		}
	}

	@Override
	public List<Map<String, Object>> findGroupingReceiveDetailData(String certiCode, Long policyId) {
		if (isLiaroc2020(policyId, certiCode)) {
			return liaRocDownload2020Dao.findGroupingReceiveDetailData(certiCode, policyId);
		} else {
			return liaRocDownload2019CI.findGroupingReceiveDetailData(certiCode, policyId);
		}
	}

	@Override
	public Long queryIssueAnnuPrem(String certiCode, String companyCode, Long policyId) {
		if (isLiaroc2020(policyId, certiCode)) {
			return liaRocDownload2020Dao.queryIssueAnnuPrem(certiCode, companyCode, policyId);
		} else {
			return liaRocDownload2019CI.queryIssueAnnuPrem(certiCode, companyCode, policyId);
		}
	}

	@Override
	public Long queryReceiveAnnuPrem(String certiCode, String companyCode, Long policyId) {
		if (isLiaroc2020(policyId, certiCode)) {
			return liaRocDownload2020Dao.queryReceiveAnnuPrem(certiCode, companyCode, policyId);
		} else {
			return liaRocDownload2019CI.queryReceiveAnnuPrem(certiCode, companyCode, policyId);
		}
	}

	@Override
	public Integer queryDownloadRecordCount(String certiCode, Long policyId) {
		if (isLiaroc2020(policyId, certiCode)) {
			return liaRocDownload2020Dao.queryDownloadRecordCount(certiCode, policyId);
		} else {
			return liaRocDownload2019CI.queryDownloadRecordCount(certiCode, policyId);
		}
	}

	@Override
	public List<Map<String, Object>> findGroupingReceiveDetailLog(String certiCode, Long policyId) {
		if (isLiaroc2020(policyId, certiCode)) {
			return liaRocDownload2020Dao.findGroupingReceiveDetailLog(certiCode, policyId);
		} else {
			return liaRocDownload2019CI.findGroupingReceiveDetailLog(certiCode, policyId);
		}
	}

	@Override
	public List<Map<String, Object>> findGroupingIssueDetailLog(String certiCode, Long policyId) {
		if (isLiaroc2020(policyId, certiCode)) {
			return liaRocDownload2020Dao.findGroupingIssueDetailLog(certiCode, policyId);
		} else {
			return liaRocDownload2019CI.findGroupingIssueDetailLog(certiCode, policyId);
		}
	}

	@Override
	public List<Map<String, Object>> findTerminateDetailData(Long policyId) {

		if (hasTerminateLiaroc2020Download(policyId, null)) {
			//終止保單通報資訊
			//1.	2021/07/15 終止保單檢核僅檢核要保人身份
			List<Map<String, Object>> allList = new ArrayList<Map<String, Object>>();
			List<Map<String, Object>> holderList = liaRocDownload2020Dao.findTerminateIssueDetailData(policyId,
					Liaroc20Cst.DOWNLOAD_ROLE_HOLDER.getCode());
			allList.addAll(holderList);

			return allList;
		} else {
			return liaRocDownloadDao.findTerminateIssueDetailData(policyId);
		}
	}

	@Override
	public List<LiaRocPaidInjuryMedicalDetailVO> findLiarocPaidInjuryMedicalDetailInfo(Long policyId) {
		if (isLiaroc2020(policyId)) {
			return liaRocDownload2020CI.findLiarocPaidInjuryMedicalDetailInfo(policyId);
		} else {
			return liaRocDownloadService.findLiarocPaidInjuryMedicalDetailInfo(policyId);
		}
	}

	@Override
	public Long getLastDownloadListId(Long policyId, String certiCode) {
		if (liaRocDownload2020CI.isLiaRocV2020DownloadDate()) {
			return liaRocDownload2020Dao.getLastDownloadListId(policyId, certiCode);
		} else {
			return liaRocDownloadDao.getLastDownloadListId(policyId, certiCode);
		}
	}

	@Override
	public List<Map<String, Object>> findECTerminateDetailData(Long policyId, Date applyDate, String ecCertiCode) {

		if (hasTerminateLiaroc2020Download(policyId, ecCertiCode)) {
			//終止保單通報資訊
			//1.	2021/07/15 終止保單檢核僅檢核要保人身份
			List<Map<String, Object>> allList = new ArrayList<Map<String, Object>>();

			List<Map<String, Object>> holderList = liaRocDownload2020Dao.findECTerminateIssueDetailData(policyId,
					applyDate, ecCertiCode, Liaroc20Cst.DOWNLOAD_ROLE_HOLDER.getCode());
			allList.addAll(holderList);

			return allList;
		} else {
			return liaRocDownloadDao.findECTerminateIssueDetailData(policyId, applyDate, ecCertiCode);
		}
	}

	@Override
	public String queryDownloadRecordStatus(String policyStatus) {
		//T_LIAROC_CODE 查找不用分流
		return liaRocDownload2019CI.queryDownloadRecordStatus(policyStatus);
	}

	@Override
	public List<Map<String, Object>> findPayAsYouGoMedNInjDetailData(String certiCode, Long policyId) {
		if (isLiaroc2020(policyId, certiCode)) {
			return liaRocDownload2020Dao.findPayAsYouGoMedNInjDetailData(certiCode, policyId);
		} else {
			return liaRocDownload2019CI.findPayAsYouGoMedNInjDetailData(certiCode, policyId);
		}
	}

	/**
	 * <p>Description : 被保險人近三個月公會通報資料</p>
	 * <p>Created By : simon.huang</p>
	 * <p>Create Time : Jul 30, 2016</p>
	 * @param policyId
	 * @param ceriCode 被保險人身份證號
	 * @return 近三個月公會通報資料總數
	 */
	public List<String> getThreeMonthCount(Long policyId, String certiCode, Date applyDate) {
		if (isLiaroc2020(policyId, certiCode)) {
			return liaRocDownload2020Dao.getThreeMonthCompanyList(policyId, certiCode, applyDate);
		} else {
			return liaRocDownload2019CI.getThreeMonthCount(policyId, certiCode);
		}
	}

	@Override
	public int getNBLiaroc107LawCount(Long policyId, String certiCode, String law107EffectDate1,
			String law107EffectDate2) {
		if (isLiaroc2020(policyId, certiCode)) {
			Date applyDate = DateUtils.toDate(law107EffectDate1, "yyyyMMdd");
			return liaRocDownload2020Dao.getLiaroc107LawCount(policyId, certiCode, applyDate);
		} else {
			return liaRocDownload2019CI.getLiaroc107LawCount(policyId, certiCode, law107EffectDate1, law107EffectDate2);
		}
	}

	/**
	 *保全專用  PCR-426582 #437511 新版公會通報pos107條款調整
	 *  查詢被保人投保了幾項符合有喪葬費用保險金之商品
	 */
	@Override
	public int getPOSLiaroc107LawCount(Long policyId, String certiCode, String validateDateStr) {

		//		LiarocDownloadLogHelper.checkDownloadLogging(policyId, certiCode);
		//保全變更客戶申請日 ，若有值 ，只參考 同業保單狀況生效日大於等於保全變更生效n個月前往後所有日期區間
		Date validateDate = (validateDateStr != null) ? DateUtils.toDate(validateDateStr, "yyyyMMdd") : null;
		return liaRocDownload2020Dao.getPOSLiaroc107LawCount(policyId, certiCode, validateDate);

	}

	@Override
	public int getLiarocProdType16NumByCertiCode(Long policyId, String certiCode) {
		if (isLiaroc2020(policyId, certiCode)) {
			return liaRocDownload2020Dao.getLiarocProdType16NumByCertiCode(policyId, certiCode);
		} else {
			return liaRocDownload2019CI.getLiarocProdType16NumByCertiCode(policyId, certiCode);
		}
	}

	@Override
	public Map<String, Integer> getPayAsYouGoMedical(String certiCode, Long policyId) {
		if (isLiaroc2020(policyId, certiCode)) {
			return liaRocDownload2020Dao.getPayAsYouGoMedical(certiCode, policyId);
		} else {
			return liaRocDownload2019CI.getPayAsYouGoMedical(certiCode, policyId);
		}
	}

	@Override
	public BigDecimal queryRA327Amount(String certiCode, String liarocType, Long policyId) {
		if (isLiaroc2020(policyId, certiCode)) {
			return liaRocDownload2020Dao.queryRA327Amount(certiCode, liarocType, policyId);
		} else {
			return liaRocDownload2019CI.queryRA327Amount(certiCode, liarocType, policyId);
		}
	}

	@Override
	public List<LiaRocDownloadDetailVO> queryLiarocDownloadDetailList(String certiCode) {
		if (isLiaroc2020ByCertiCode(certiCode)) {

			List<LiaRocDownloadDetail2020VO> list = liaRocDownloadDetail2020Dao
					.queryDownloadDetailByCertiCode(certiCode);

			List<LiaRocDownloadDetailVO> oldList = new ArrayList<LiaRocDownloadDetailVO>();
			for (LiaRocDownloadDetail2020VO detail2020 : list) {
				LiaRocDownloadDetailVO oldDetail = new LiaRocDownloadDetailVO();
				BeanUtils.copyProperties(oldDetail, detail2020);
				oldDetail.setPolicyId(detail2020.getLiarocPolicyCode());
				oldList.add(oldDetail);
			}

			return oldList;
		} else {
			return liaRocDownload2019CI.queryLiarocDownloadDetailList(certiCode);
		}
	}
	
	private boolean isLiaroc2020(Long policyId, String certiCode) {
		//2022/07/25 移除查詢DB,均回傳使用新式公會邏輯
		return true;
	}

	private boolean isLiaroc2020(Long policyId) {
		//2022/07/25 移除查詢DB,均回傳使用新式公會邏輯
		return true;
	}

	private boolean isLiaroc2020ByCertiCode(String certiCode) {
		//2022/07/25 移除查詢DB,均回傳使用新式公會邏輯
		return true;
	}

	/**
	 * 是否有新式公會同業終止保單下載記錄
	 * @param policyId
	 * @return
	 */
	public boolean hasTerminateLiaroc2020Download(Long policyId, String certiCode) {
		boolean isLiaroc2020 = false;

		int count = liaRocDownload2020Dao.countTerminatePolicyCount(policyId, certiCode);

		if (count > 0) {
			isLiaroc2020 = true;
		}

		return isLiaroc2020;
	}

	@Override
	public LiaRocDownload2020VO downloadForFixedUpload(String certiCode) throws GenericException {
		//直接作新公會下載，不用判斷舊公會
		LiaRocDownload2020VO download2020VO  = liaRocDownload2020CI.downloadForFixedUpload(certiCode);
		return download2020VO;
	}

}
