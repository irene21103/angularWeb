package com.ebao.ls.riskPrevention.ctrl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.ls.pub.auth.AuthItemCst;
import com.ebao.ls.riskPrevention.bs.RiskCustomerLogService;
import com.ebao.ls.riskPrevention.bs.impl.RiskCustomerAuthServiceImpl;
import com.ebao.pub.framework.GenericAction;
import com.ebao.pub.util.StringUtils;

public class RiskCustomerAction extends GenericAction {

    @Resource(name = RiskCustomerLogService.BEAN_DEFAULT)
    RiskCustomerLogService riskCustomerLogService;

    @Resource(name = RiskCustomerAuthServiceImpl.BEAN_DEFAULT)
    RiskCustomerAuthServiceImpl pageAuthService;

    @Override
    public ActionForward process(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {

        RiskCustomerForm cForm = (RiskCustomerForm) form;

        request.setAttribute("mainPOPup", "Y".equals(request.getAttribute("mainPOPup")) ? "Y" : "N");
        request.setAttribute("detailPOPup", "Y".equals(request.getAttribute("detailPOPup")) ? "Y" : "N");

        request.setAttribute("detailSearch", false);
        request.setAttribute("isSearch", false);
        request.setAttribute("auth", pageAuthService.hasAccessRight(AuthItemCst.AUTH_ITEM_PA_RISK_CUSTOMER));
        switch (cForm.getAction()) {
            case LOAD:
                request.setAttribute("mainPOPup", "Y");
                request.setAttribute("detailPOPup", "Y");
            case SEARCH:
                if(!StringUtils.isNullOrEmpty(cForm.getLog().getCertiCode())){
                    List<Map<String, Object>> results = riskCustomerLogService.pageQuery(cForm.getLog(), cForm.getPager());
                    request.setAttribute("detailSearch", results.size() > 0 ? true : false);
                    request.setAttribute("isSearch", true);
                    request.setAttribute("list", results);
                }
                break;
            default:
                break;
        }
        return mapping.findForward("success");
    }
}
