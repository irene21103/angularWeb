package com.ebao.ls.liaRoc.batch.vo;

import java.util.List;

public class ResultMessage {

	private int executeResult;
	
	private List<String> errorList;

	public int getExecuteResult() {
		return executeResult;
	}

	public void setExecuteResult(int executeResult) {
		this.executeResult = executeResult;
	}

	public List<String> getErrorList() {
		return errorList;
	}

	public void setErrorList(List<String> errorList) {
		this.errorList = errorList;
	}
}
