package com.ebao.ls.liaRoc.ci;

import java.util.List;

import javax.annotation.Resource;

import com.ebao.ls.foundation.vo.GenericEntityVO;
import com.ebao.ls.liaRoc.bo.LiaRocUpload2020;
import com.ebao.ls.liaRoc.bo.LiaRocUpload107Detail2020;
import com.ebao.ls.liaRoc.bo.LiaRocUpload107Detail2020VO;
import com.ebao.ls.liaRoc.data.LiaRocUpload107Detail2020Dao;
import com.ebao.ls.liaRoc.data.LiaRocUpload2020Dao;
import com.ebao.ls.liaRoc.vo.LiaRocUpload2020SendVO;
import com.ebao.pub.util.BeanUtils;

public class LiaRocUpload1072020CIImpl implements LiaRocUpload1072020CI{

    @Resource(name = LiaRocUpload2020Dao.BEAN_DEFAULT)
    private LiaRocUpload2020Dao liaRocUpload2020Dao;

    @Resource(name = LiaRocUpload107Detail2020Dao.BEAN_DEFAULT)
    private LiaRocUpload107Detail2020Dao liaRocUpload107Detail2020Dao;

    @Override
    public void save107RocUpload(LiaRocUpload2020SendVO liaRocUpload2020SendVO) {
        LiaRocUpload2020 liaRocUploadBO = new LiaRocUpload2020();
        BeanUtils.copyProperties(liaRocUploadBO, liaRocUpload2020SendVO);

        // 儲存主檔資料
        liaRocUpload2020Dao.save(liaRocUploadBO);

        // 儲存明細資料
        for (GenericEntityVO detailVO : liaRocUpload2020SendVO.getDataList()) {

            LiaRocUpload107Detail2020 detailBO = new LiaRocUpload107Detail2020();
            BeanUtils.copyProperties(detailBO, detailVO);
            detailBO.setUploadListId(liaRocUploadBO.getListId());

            liaRocUpload107Detail2020Dao.save(detailBO);
        }
    }
    

    @Override
    public List<LiaRocUpload107Detail2020VO> findNotify107Detail() {
        return liaRocUpload107Detail2020Dao.findNotify107Detail();
    }

}
