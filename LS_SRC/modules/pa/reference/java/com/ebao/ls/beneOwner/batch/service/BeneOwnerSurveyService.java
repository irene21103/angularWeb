package com.ebao.ls.beneOwner.batch.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.ebao.ls.crs.batch.vo.CRSTempVO;
import com.ebao.ls.crs.vo.CRSDueDiligenceListVO;
import com.ebao.ls.crs.vo.CRSPartyLogVO;
import com.ebao.ls.cs.boaudit.vo.BeneficialOwnerSurveyVO;
import com.ebao.ls.fatca.vo.FatcaDueDiligenceListVO;
import com.ebao.ls.foundation.service.GenericService;
import com.ebao.ls.pa.pub.vo.BeneficiaryVO;
import com.ebao.pub.framework.GenericException;

public interface BeneOwnerSurveyService extends GenericService<BeneficialOwnerSurveyVO>{
	public final static String BEAN_DEFAULT = "beneOwnerSurveyService";
	
	public BeneficialOwnerSurveyVO queryBeneficialOwnerSurvey(Long partyId, Long policyId,
			String targetSurveyType, Date targetsurveyDate) throws GenericException;
	
	public List<Map<String, Object>> getBeneOwnerSurveyList(Date processDate);
	
	public List<Map<String, Object>> getBeneOwnerNoReplyList(Date processDate);
	
	public BeneficialOwnerSurveyVO createBeneficialOwnerSurvey(Map<String, Object> column, Date processDate, String targetSurveyType) throws GenericException;

    /**
     * 派發孤兒保單
     * @param voList
     * @throws Exception
     */
    public abstract void deliverOrphan(BeneficialOwnerSurveyVO vo) throws Exception;
    
    /**
     * 更新業務員
     * @param vo
     */
    public void updateServiceAgent(BeneficialOwnerSurveyVO vo) throws Exception;
    
    /**
     * save entity  
     * 
     * @param (GenericEntityVO)liarocCsInfoVO
     * @throws GenericException
     */
    public Long saveBeneOwnerSurvey(BeneficialOwnerSurveyVO beneOwnerSurveyVO) throws GenericException ;
 

}
