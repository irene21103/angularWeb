package com.ebao.ls.callout.batch.task;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.transaction.UserTransaction;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.jdbc.core.JdbcTemplate;

import com.ebao.foundation.common.context.AppUserContext;
import com.ebao.ls.callout.bs.CalloutTransService;
import com.ebao.ls.notification.event.nb.FmtUnb0140Event;
import com.ebao.ls.pub.batch.impl.DataSourceWrapper;
import com.ebao.pub.batch.help.BatchHelp;
import com.ebao.pub.batch.help.UnpieceableBatchProgram;
import com.ebao.pub.batch.job.BaseUnpieceableJob;
import com.ebao.pub.batch.type.JobStatus;
import com.ebao.pub.batch.type.LogLevel;
import com.ebao.pub.batch.util.BatchLogUtils;
import com.ebao.pub.event.ApplicationEventVO;
import com.ebao.pub.event.EventService;
import com.ebao.pub.util.TransUtils;
import com.ebao.pub.util.json.DateUtils;

/**
 * <p>Title:契約承保前電訪保單 </p>
 * <p>Description:
    系統抽取如下： 電訪結果=失敗 OR 拒絕電訪
    系統於保單狀況生效時晚上批次依投保商品產生「重要權益通知函」，請參考2.3 服務管理平台報表BSD：
    投資型保險重要權益通知函(承保前)（報表Index：UNB-120）
 * 
 * </p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company: TGL Co., Ltd.</p>
 * <p>Create Time: Oct 3, 2016</p> 
 * @author 
 * <p>Update Time: Oct 3, 2016</p>
 * <p>Updater: GrayKao</p>
 * <p>Update Comments: </p>
 */
public class FmtUnb0140Task extends BaseUnpieceableJob implements UnpieceableBatchProgram, ApplicationContextAware {
    public static final String BEAN_DEFAULT = "fmtUNB0140Task";
    
    @Resource(name = EventService.BEAN_DEFAULT)
    private EventService eventService;
    
    @Resource(name = CalloutTransService.BEAN_DEFAULT)
    private CalloutTransService calloutTransService;
    
    protected static final JdbcTemplate jdbcTemplate = new JdbcTemplate(new DataSourceWrapper());
    
    protected ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
    
    @Override
    public int mainProcess() throws Exception {
        
        List<Map<String, Object>> list = query();
        int size = CollectionUtils.isEmpty(list) ? 0 : list.size();
        String msg = String.format( "[FMT_UNB_0140][process_date = %s][電訪結果=失敗 OR 拒絕電訪] list.size= %d", DateUtils.format(BatchHelp.getProcessDate(), "yyyy/MM/dd"), size);
        BatchLogUtils.addLog(LogLevel.INFO, null, msg);
        UserTransaction ut = TransUtils.getUserTransaction();
        for (Map<String, Object> map : list) {
            try{
                if(map.get("CALLOUT_NUM")!=null){
                	ut.begin();
                    calloutTransService.updateCalloutImporterSender(map.get("CALLOUT_NUM").toString(), BatchHelp.getProcessDate());
                    handleOneRecord(map);
                    ut.commit();
                }
            }catch (Exception e ){
            	TransUtils.rollback(ut);
                msg = String.format( "[FMT_UNB_0140][process_date = %s]" + "Error Happen when PublishEvent or Update Callout_Important_Sender: %s", DateUtils.format(BatchHelp.getProcessDate(), "yyyy/MM/dd"), e);
                BatchLogUtils.addLog(LogLevel.ERROR, null, msg);
            }
        }
        return JobStatus.EXECUTE_SUCCESS;
    }

    private void handleOneRecord(Map<String,Object> map){
        
        ApplicationEventVO vo=new ApplicationEventVO();
        vo.setPolicyId(Long.parseLong(map.get("POLICY_ID").toString()));    //set 保單ID
        vo.setTransactionId(Long.parseLong(map.get("POLICY_ID").toString()));
        vo.setUserId(AppUserContext.getCurrentUser().getUserId());
        
        FmtUnb0140Event event=new FmtUnb0140Event(vo);
        event.setPrintDate(BatchHelp.getProcessDate()); //set 批次啟動時間
        event.setSendTo(MapUtils.getString(map, "CALLOUT_TARGET_TYPE"));
        event.setCalleeName(MapUtils.getString(map, "CALLEE_NAME"));
        event.setCalleeCertiCode(MapUtils.getString(map, "CALLEE_CERTI_CODE"));
        event.setCalleeRomanName(MapUtils.getString(map, "CALLEE_ROMAN_NAME"));
        String msg = String.format( "[FMT_UNB_0140][process_date = %s][電訪結果=失敗 OR 拒絕電訪] policy_id = %d", DateUtils.format(BatchHelp.getProcessDate(), "yyyy/MM/dd"),  vo.getPolicyId());
        BatchLogUtils.addLog(LogLevel.INFO, null, msg);
        
        eventService.publishEvent(event);
    }
    
    /**
     * <p>Description : 四個條件
     * 1:電訪結果=失敗 OR 拒絕電訪
     * 2:CALLOUT_IMPORTANT_SENDER is null
     * 3:檔案號:=??? 生效前電訪
     * 4:為 EVoice 回傳的結果  ->  要有檔案類別(Callout_File_Name)
     *  </p>
     * <p>Created By : GrayKao</p>
     * <p>Create Time : Oct 4, 2016</p>
     * @param pager
     * @return
     */
    public List<Map<String, Object>> query(){
        StringBuffer sql = new StringBuffer();    
        sql.append("select ct.CALLOUT_NUM, ct.POLICY_ID, ct.CALLOUT_TARGET_TYPE, ct.CALLEE_NAME, ct.CALLEE_CERTI_CODE, ct.CALLEE_ROMAN_NAME ");
        sql.append("  from (select ct.policy_id, max(ct.list_id) list_id ");
        sql.append("          from t_callout_trans ct ");
        sql.append("         where ct.batch_indi = 'N' ");
        sql.append("           and ct.callout_result <> 0 ");
        sql.append("           and ct.callout_docket_num like 'UNB-Bef%' ");
        sql.append("           and ct.callout_num like '%UNB%' ");
        sql.append("           and ct.callout_target_type in ('1','13','3') ");//PCR-500628 限電訪對象類型=要保人、主被保險人、法定代理人
        sql.append("         group by policy_id, callout_target_type) ctSelect ");
        sql.append("  join t_callout_trans ct ");
        sql.append("    on ct.policy_id = ctSelect.policy_id ");
        sql.append("   and ct.list_id = ctSelect.list_id ");
        sql.append("   and ct.batch_indi = 'N' ");
        sql.append("   and ct.callout_result in (3, 4) ");
        sql.append("   and ct.callout_important_sender is null ");
        sql.append("   and (ct.callout_reason is null or ct.callout_reason <> '11') ");
        sql.append("   and not exists (select 1 ");
        sql.append("   	     from t_callout_questionnaire cq ");
        sql.append("         where cq.callout_num = ct.callout_num ");
        sql.append("           and cq.callout_reason_code = '11') ");
        sql.append("  join t_contract_master cm ");
        sql.append("    on cm.policy_id = ct.policy_id ");
        sql.append("   and cm.liability_state in (1, 2) ");

        return jdbcTemplate.queryForList(sql.toString());
    }
}
