package com.ebao.ls.uw.ctrl.underwriting;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.UserTransaction;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ClassUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.foundation.app.sys.sysmgmt.ds.SysMgmtDSLocator;
import com.ebao.foundation.app.sys.sysmgmt.ds.UserVO;
import com.ebao.foundation.module.db.hibernate.HibernateSession3;
import com.ebao.foundation.module.db.jdbc.JdbcUtils;
import com.ebao.foundation.module.web.action.ExceptionInfoUtils;
import com.ebao.ls.arap.pub.ci.PremCI;
import com.ebao.ls.chl.service.AgentService;
import com.ebao.ls.chl.service.ChannelOrgService;
import com.ebao.ls.chl.vo.ChannelOrgVO;
import com.ebao.ls.cs.ci.CSCI;
import com.ebao.ls.cs.commonflow.ds.AlterationItemService;
import com.ebao.ls.cs.commonflow.ds.task.CsTaskTransService;
import com.ebao.ls.cs.commonflow.vo.AlterationItemVO;
import com.ebao.ls.cs.logger.ApplicationLogger;
import com.ebao.ls.cs.logger.helper.AppLogDataHelper;
import com.ebao.ls.pa.nb.bs.ProposalProcessService;
import com.ebao.ls.pa.nb.bs.ProposalService;
import com.ebao.ls.pa.nb.bs.impl.sp.ProposalServiceSp;
import com.ebao.ls.pa.nb.bs.rule.ProposalRuleResultService;
import com.ebao.ls.pa.nb.cmu.event.ProposalVerifiedValidateListener;
import com.ebao.ls.pa.nb.ctrl.detailreg.DetailRegHelper;
import com.ebao.ls.pa.nb.ctrl.helper.RemoteHelper;
import com.ebao.ls.pa.nb.util.NBUtils;
import com.ebao.ls.pa.pub.NbTaskCst;
import com.ebao.ls.pa.pub.bs.NbInsMicroRoleService;
import com.ebao.ls.pa.pub.bs.NbPolicySpecialRuleService;
import com.ebao.ls.pa.pub.bs.NbTaskService;
import com.ebao.ls.pa.pub.bs.ReviewService;
import com.ebao.ls.pa.pub.bs.coverage.CoverageService;
import com.ebao.ls.pa.pub.ci.PolicyCI;
import com.ebao.ls.pa.pub.service.PolicyService;
import com.ebao.ls.pa.pub.vo.BeneficiaryVO;
import com.ebao.ls.pa.pub.vo.CoverageAgentVO;
import com.ebao.ls.pa.pub.vo.CoverageInsuredVO;
import com.ebao.ls.pa.pub.vo.CoverageVO;
import com.ebao.ls.pa.pub.vo.NbInsMicroRoleVO;
import com.ebao.ls.pa.pub.vo.NbRiResultVO;
import com.ebao.ls.pa.pub.vo.PayerAccountVO;
import com.ebao.ls.pa.pub.vo.PolicyHolderVO;
import com.ebao.ls.pa.pub.vo.PolicyVO;
import com.ebao.ls.product.model.query.output.ProdBizCategorySub;
import com.ebao.ls.product.service.LifeProductCategoryService;
import com.ebao.ls.pub.CodeCst;
import com.ebao.ls.pub.cst.NBPolicySpecialRuleType;
import com.ebao.ls.pub.cst.ProposalStatus;
import com.ebao.ls.pub.workflow.WfContextLs;
import com.ebao.ls.uw.ctrl.UwGenericAction;
import com.ebao.ls.uw.ctrl.decision.UwProductDecisionViewForm;
import com.ebao.ls.uw.ctrl.pub.UwPolicyForm;
import com.ebao.ls.uw.data.TUwInsuredListDelegate;
import com.ebao.ls.uw.data.bo.UwLifeInsured;
import com.ebao.ls.uw.data.query.UwQueryDao;
import com.ebao.ls.uw.ds.UwPolicyService;
import com.ebao.ls.uw.ds.UwProcessService;
import com.ebao.ls.uw.ds.UwRiApplyService;
import com.ebao.ls.uw.ds.constant.UwSourceTypeConstants;
import com.ebao.ls.uw.ds.constant.UwStatusConstants;
import com.ebao.ls.uw.ds.sp.CompleteUWProcessSp;
import com.ebao.ls.uw.ds.sp.QueryUwDataSp;
import com.ebao.ls.uw.util.Utils;
import com.ebao.ls.uw.vo.UwExtraLoadingVO;
import com.ebao.ls.uw.vo.UwPolicyVO;
import com.ebao.ls.uw.vo.UwProductVO;
import com.ebao.pub.annotation.PAPubAPIUpdate;
import com.ebao.pub.annotation.PrdAPIUpdate;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.framework.AppException;
import com.ebao.pub.framework.ExceptionFactory;
import com.ebao.pub.framework.GenericException;
import com.ebao.pub.framework.internal.spring.DSProxy;
import com.ebao.pub.i18n.lang.CodeTable;
import com.ebao.pub.util.BeanUtils;
import com.ebao.pub.util.DBean;
import com.ebao.pub.util.DateUtils;
import com.ebao.pub.util.StringUtils;
import com.ebao.pub.util.Trans;
import com.ebao.pub.util.TransUtils;
import com.ebao.pub.workflow.jbpm.JbpmContextUtil;
import com.ebao.pub.workflow.worklist.web.action.WorkListConstant;

/**
 * <p> Title:Underwriting </p>
 * <p> Description: </p>
 * <p> Copyright: Copyright (c) 2004 </p>
 * <p> Company: eBaoTech Corporation </p>
 * 
 * @author mingchun.shi
 * @since Jun 26, 2005
 * @version 1.0
 */
public class UwPolicyMainFixAction extends UwGenericAction {

	private Logger logger = Logger.getLogger(UwPolicyMainFixAction.class);

	/**
	 * Underwriting basic information(UwPolicyForm)
	 */
	public static final String MAIN_FORM = "main_form";

	/**
	 * Discount type of the proposal/policy
	 */
	public static final String DISCOUNT_TYPE = "discount_type";

	/**
	 * benefit list
	 */
	public static final String PRODUCT_LIST = "product_list";

	/**
	 * benefit show list
	 */
	public static final String PRODUCT_SHOW_LIST = "product_show_list";

	/**
	 * Life assured list
	 */
	public static final String LIFE_ASSURED_LIST = "life_assured_list";

	/**
	 * Life assured list
	 */
	public static final String PROPOSAL_LIST = "proposal_list";

	/**
	 * Life assured list
	 */
	public static final String NOMINEE_LIST = "nominee_list";

	/**
	 * Agent list/2015-06-25 add by sunny
	 */
	public static final String AGENT_LIST = "agent_list";

	/**
	 * 再保公司列表2015-06-25 add by sunny
	 */
	public static final String RI_COMP_LIST = "ri_comp_list";

	/**
	 * 身心障礙列表2015-06-25 add by sunny
	 */
	public static final String DISABILITY_LIST = "disability_list";

	/**
	 * is company holder
	 */
	public static final String IS_COMPANY_HOLDER = "Y";

	/**
	 * Underwriting endorsement code list
	 */
	public static final String ENDORSEMENTCODE_LIST = "endorsementCode_list";

	/**
	 * Underwriting condition code list
	 */
	public static final String CONDITIONCODE_LIST = "conditionCode_list";

	/**
	 * Underwriting exclusion code list
	 */
	public static final String EXCLUSIONCODE_LIST = "exclusionCode_list";

	/**
	 * Underwriting main page logic name
	 */
	public static final String MAIN_PAGE = "detailInfo";

	/**
	 * Policy Holder is company mark
	 */
	public static final String COMPANY_HOLDER_MARK = "companyHolderMark";

	/**
	 * Policy Main benefit first life assured
	 */
	public static final String MAIN_BENEFIT_FIRST_INSURED = "mainBenefitFirstInsured";

	/**
	 * add by simon.huang on 2015-07-24 增加是否為投資型商品保單
	 */
	public static final String IS_ILP = "is_ilp";

	/**
	 * Agent list/2015-08-26 add by peter
	 */
	public static final String UWRIFAC_EXTEND_LIST = "uwRiFacExtendList";

	public static final String UWRIFAC_EXTEND_LIST_ALL = "uwRiFacExtendListAll";

	public static final String BEAN_DEFAULT = "/uw/uwPolicyMain";

	/**
	 * add by ChiaHui.Su on 2016-04-14 增加是否可修改保單生效日
	 */
	public static final String IS_ALT_VALIDATE = "N";

	/**
	 * add by ChiaHui.Su on 2016-06-04 增加核保流程控管
	 */
	public static final String UW_OPQ_CHECK_INDI = "UW_OPQ_CHECK_INDI";
	
	/**
	 * add by Sun on 2018-10-25 BVAU/BVAR商品
	 */
	public static final String IS_PRO_TG = "IS_PRO_TG";
	
	/**
	 * add by Sun on 2018-10-25 TVL/TVA/BVA商品
	 */
	public static final String IS_PRO_S1 = "IS_PRO_S1";
	
	/**
	 * Underwriting main page rending and proposal status and uw status amendation
	 * entry.
	 * 
	 * @param mapping The ActionMapping used to select this instance
	 * @param form The optional ActionForm bean for this request (if any)
	 * @param request HttpRequest
	 * @param response HttpResponse
	 * @return ActionForward ActionForward
	 * @throws GenericException Application Exception
	 */
    @Override
    @PrdAPIUpdate
    @PAPubAPIUpdate("loadPolicyByPolicyId")
    public ActionForward uwProcess(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws GenericException {
        Date currentUserLocalTime = AppContext.getCurrentUserLocalTime();
        Long currentUserId = AppContext.getCurrentUser().getUserId();
        String isIframe = request.getParameter("isIframe");
        boolean isCommonQuery = "true".equals(isIframe); //是否為綜合查詢來的

        // update for workflow,by robert.xu on 2008.4.22
        Long underwriteId;
        WfContextLs wfLs = WfContextLs.getInstance();
        String taskId = (String) request.getAttribute("com.ebao.pub.workflow.taskId");
        if (taskId != null && !isCommonQuery) {
            underwriteId = (Long) wfLs.getLocalVariable(taskId, "underWriteId");
        } else {
            underwriteId = Long.valueOf(com.ebao.ls.encode.encoder.Encode.encodeForHTML(request).getParameter("underwriteId"));
        }

        ApplicationLogger.setJobName(ClassUtils.getShortClassName(this.getClass()));
        ApplicationLogger.setOptionId(DateUtils.date2String(new Date(), "yyyyMMdd HHmmss"));

        ApplicationLogger.addLoggerData("uwPolicyDS.findUwPolicy(" + underwriteId + ") begin");
        UwPolicyVO uwPolicyVO = uwPolicyDS.findUwPolicy(underwriteId);
        ApplicationLogger.addLoggerData("uwPolicyDS.findUwPolicy(" + underwriteId + ") end");

        Long policyId = uwPolicyVO.getPolicyId();
        Long underwriterId = getUnderwriterId();
        String sourceType = uwPolicyVO.getUwSourceType();

        ApplicationLogger.setPolicyCode(uwPolicyVO.getPolicyCode());
        
        if (UwSourceTypeConstants.NEW_BIZ.equals(sourceType)) {
            ApplicationLogger.setSystemCode(AppLogDataHelper.SYSTEM_CODE_UNB);
            int proposalStatus = policyDS.getProposalStatusByPolicyCode(uwPolicyVO.getPolicyCode());
            //新契約核保主檔若已過核保狀態一律作查詢顯示用
            if(proposalStatus >= ProposalStatus.PROPOSAL_STATUS__ACCEPTED) {
            	isCommonQuery = true;
            }
            //新契約核保主檔，且不會查詢顯示用資料更新放這邊。
            if(isCommonQuery == false) {
                // 更新批次簽署資訊
                proposalService.syncBatchSignInfoFromChannelOrg(policyId);
            }
        } else {
            ApplicationLogger.setSystemCode(AppLogDataHelper.SYSTEM_CODE_POS);
        }

        /* 新契約核保因隨時會有繳費資料進來,需同時確認生效日是否有異動 */
        if (isCommonQuery == false) { //綜合查詢不可執行生效日同步
            if (UwSourceTypeConstants.NEW_BIZ.equals(sourceType)) {
                //需比對任務人員是否相同
                if (taskId == null) {
                    ApplicationLogger.addLoggerData("wfLs.getCurrentTaskId(" + policyId + ") begin");
                    taskId = wfLs.getCurrentTaskId(ProposalProcessService.VARIABLE_NAME_POLICY_ID,
                            policyId, ProposalProcessService.PROCESS_NAME,
                            WorkListConstant.UIC_DATATYPE_LONG);
                    ApplicationLogger.addLoggerData("wfLs.getCurrentTaskId(" + policyId + ") end");
                }

                ApplicationLogger.addLoggerData("wfLs.getTaskInstanceByTaskId(" + taskId + ") begin");
                String taskActorId = WfContextLs.getInstance().getTaskInstanceByTaskId(taskId).getActor();
                ApplicationLogger.addLoggerData("wfLs.getTaskInstanceByTaskId(" + taskId + ") end");

                if (!String.valueOf(currentUserId).equals(taskActorId)) {
                    //ERR_20611020022 非任務所有人不能核准，改他人核准需任務重分配！
                    throw new AppException(20611020022L);
                }

                ApplicationLogger.addLoggerData("ProposalServiceSp.getRiskCommenceDate() begin");
                Date newValidateDate = ProposalServiceSp.getRiskCommenceDate(policyId, underwriterId);
                ApplicationLogger.addLoggerData("ProposalServiceSp.getRiskCommenceDate() end");
                if (uwPolicyVO.getValidateDate().equals(newValidateDate) == false) {
                    // recalc commence_date
                    UserTransaction userTransaction2 = null;
                    try {
                        userTransaction2 = Trans.getUserTransaction();
                        userTransaction2.begin();

                        ApplicationLogger.addLoggerData("ProposalServiceSp.updateRiskCommenceDate() begin");
                        /* 核心表生效日 更新 */
                        ProposalServiceSp.updateRiskCommenceDate(policyId, newValidateDate);
                        ApplicationLogger.addLoggerData("ProposalServiceSp.updateRiskCommenceDate() end");

                        ApplicationLogger.addLoggerData("CompleteUWProcessSp.updateCommencedate() begin");
                        /* 核保作業檔生效日 更新 */
                        CompleteUWProcessSp.updateCommencedate(underwriteId, newValidateDate, newValidateDate, "N");

                        ApplicationLogger.addLoggerData("CompleteUWProcessSp.updateCommencedate() end");
                        userTransaction2.commit();

                        ApplicationLogger.addLoggerData("uwPolicyDS.findUwPolicy() again begin");
                        //生效日更新後斷開連線抓取新的資料
                        HibernateSession3.detachSession();
                        //若有系統關閉已發放核保訊息，可能會會更新照會單狀態，會執行一次workflow變數更新
                        //執行detachSession會造成jbpm底層session抓到舊的,先close讓底層重抓
                        JbpmContextUtil.closeJbpmContext();
                        uwPolicyVO = uwPolicyDS.findUwPolicy(underwriteId);

                        ApplicationLogger.addLoggerData("uwPolicyDS.findUwPolicy() again end");
                    } catch (Exception e) {
                        TransUtils.rollback(userTransaction2);

                        String message = ExceptionInfoUtils.getExceptionMsg(e);
                        int len = message.length();
                        if (len > 0) {
                            ApplicationLogger.addLoggerData("[ERROR]Exception => " + message.substring(0, (len > 2000 ? 2000 : len)));
                        } else {
                            ApplicationLogger.addLoggerData("[ERROR]Exception => " + e);
                        }

                        try {
                            ApplicationLogger.flush();
                        } catch (Exception e1) {
                            logger.warn(e.getMessage());
                        }
                        throw ExceptionFactory.parse(e);
                    }
                }
            }
        }

        PolicyVO policyVO;
        if (UwSourceTypeConstants.NEW_BIZ.equals(sourceType)) {
            ApplicationLogger.addLoggerData("policyService.getProposalStatusByPolicyCode() begin");
            int proposalStatus = policyService.getProposalStatusByPolicyCode(uwPolicyVO.getPolicyCode());
            ApplicationLogger.addLoggerData("policyService.getProposalStatusByPolicyCode() end");
            if (proposalStatus == CodeCst.PROPOSAL_STATUS__IN_FORCE) {
                //保要書狀態為已生效，查生效前的保單資料
                policyVO = this.loadBfInforcePolicy(policyId);
            } else {

                ApplicationLogger.addLoggerData("policyService.loadPolicyByPolicyId() begin");
                policyVO = policyService.loadPolicyByPolicyId(policyId);
                ApplicationLogger.addLoggerData("policyService.loadPolicyByPolicyId() end");
            }
        } else {
            //保全查T表資料
            ApplicationLogger.addLoggerData("policyService.loadPolicyByPolicyId() begin");
            policyVO = policyService.loadPolicyByPolicyId(policyId);
            ApplicationLogger.addLoggerData("policyService.loadPolicyByPolicyId() end");
        }

        if (isCommonQuery == false) { //綜合查詢不可執行
            if (UwSourceTypeConstants.NEW_BIZ.equals(sourceType)) {
                ApplicationLogger.addLoggerData("fixGarbageData() begin");
                uwPolicyVO = this.fixGarbageData(uwPolicyVO, policyVO);

                ApplicationLogger.addLoggerData("fixGarbageData() end");
            }
        }

        // RTC 240801 保全核保須判斷登入人員必須為此保全 CsTask.userId, 否則提示 "申請正由 {curr_operator}進行中"
        if (Utils.isCsUw(sourceType) && isCommonQuery == false) {

            ApplicationLogger.addLoggerData("csTaskTransService.getTheTaskOperatorUser(" + uwPolicyVO.getChangeId() + ") begin");
            Long csTransId = csTaskTransService.getTheTaskOperatorUser(uwPolicyVO.getChangeId());
            ApplicationLogger.addLoggerData("csTaskTransService.getTheTaskOperatorUser(" + uwPolicyVO.getChangeId() + ") end");
            if (csTransId != null && csTransId.longValue() != currentUserId.longValue()) {
                //ERR_20610020008 申請正由 {curr_operator}進行中
                GenericException appLockException = new AppException(20610020008L);
                Map<String, String> exceptParams = new HashMap<String, String>();
                exceptParams.put("curr_operator", CodeTable.getCodeDesc("T_EMPLOYEE", String.valueOf(csTransId)));
                appLockException.addMessageParameter(exceptParams);
                throw appLockException;
            }
        }

        // 2016-11-08 Kate Hsiao 當來源是保全核保時，須判斷登入人員是否為此保全受理的操作人員
        boolean isCsTransOperatorId = false;
        if (Utils.isCsUw(sourceType)) {
            if (StringUtils.isNullOrEmpty(isIframe) || "false".equals(isIframe)) {

                Long csTransId = csTaskTransService.getTheTaskOperatorUser(uwPolicyVO.getChangeId());
                if (ObjectUtils.equals(currentUserId, csTransId)) {
                    isCsTransOperatorId = true;
                }
                request.setAttribute("csTransId", csTransId);
            }
        }
        // 2016-06-04 ChiaHui 增加核保流程控管機制
        String opqCheckIndi = policyVO.getUwOpqCheckIndi();
        request.setAttribute(UW_OPQ_CHECK_INDI, opqCheckIndi);
        /* 2016/06/27 alex cheng 增加判斷綜合查詢模式不執行以下邏輯isIframe */
        if (!StringUtils.isNullOrEmpty(opqCheckIndi)
                && (StringUtils.isNullOrEmpty(isIframe)
                || isCsTransOperatorId)) {

            /* 若OPQ 檢核過久,重新啟動 */
            boolean isForceCheck = "Y".equals(request.getParameter("run_opq"));
            org.hibernate.Session s = com.ebao.foundation.module.db.hibernate.HibernateSession3.currentSession();

            /* 檢核中的狀態，但卻不在inBoundPolicyCode中，需要重新啟動檢核 */
            if (CodeCst.OPQ_CHECKING.equals(opqCheckIndi)) {
                if (ProposalVerifiedValidateListener.inBoundPolicyCode.contains(policyVO.getPolicyNumber()) == false) {
                    isForceCheck = true;
                }
            }

            ApplicationLogger.addLoggerData("isForceCheck=" + isForceCheck);
            request.setAttribute("isForceCheck", false);
            
            if(isCommonQuery == false) {
                if (CodeCst.OPQ_WAITING_CHECK.equals(opqCheckIndi) || isForceCheck) {
                    request.setAttribute("isForceCheck", true);
                }
            }
        }

        // to record the current status,for cancel to use
        if ("pool".equals(request.getParameter("from"))) {
            if (CodeCst.UW_STATUS__WAITING_UW.equals(uwPolicyVO.getUwStatus())
                    || CodeCst.UW_STATUS__ESCALATED.equals(uwPolicyVO.getUwStatus())) {
                uwPolicyVO.setAssumpsit("0");
            } else {
                uwPolicyVO.setAssumpsit("1");
            }
        }
        // synchronize status with outside interface

        /* 2016/06/27 alex cheng 增加判斷綜合查詢模式不執行以下邏輯isIframe */
        if (isCommonQuery == false) { //綜合查詢不可執行
            if (UwSourceTypeConstants.NEW_BIZ.equals(sourceType)) {
                // doProposalProcess(policyVO,CodeCst.PROPOSAL_STEP__MANUAL_UW);
                // update to change process with workflow engine. Only maintain
                // proposal
                // status,don't maintain t_proposal_process. by robert.xu on
                // 2008.5.6
                ApplicationLogger.addLoggerData("maintainProposalStatus() begin");
                maintainProposalStatus(policyVO);
                ApplicationLogger.addLoggerData("maintainProposalStatus() end");
            } else if ((Utils.isCsUw(sourceType) && isCsTransOperatorId) || Utils.isClaimUw(sourceType)) {
                uwProcessDS.updateReUwPolicyUwStatus(underwriteId, underwriterId, UwStatusConstants.IN_PROGRESS, sourceType, uwPolicyVO.getChangeId());
                if (Utils.isCsUw(sourceType)) {
                    // update the cs-underwriting from
                    // CS_APP_STATUS__UW_DISPOSING to
                    // CS_APP_STATUS__UW_IN_PROGRESS
                    ApplicationLogger.addLoggerData("csCI.updateApplicationInfoByUW() begin");
                    csCI.updateApplicationInfoByUW(uwPolicyVO.getChangeId(), CodeCst.CS_APP_STATUS__UW_IN_PROGRESS, underwriterId);
                    ApplicationLogger.addLoggerData("csCI.updateApplicationInfoByUW() end");
                }
            }
            // 2016-11-16 Kate Hsiao 當來源是保全時，只有此案件的核保者的能更新
            if (!Utils.isCsUw(sourceType) || (Utils.isCsUw(sourceType) && isCsTransOperatorId)) {
                uwPolicyVO.setUwStatus(CodeCst.UW_STATUS__UW_IN_PROGRESS);
                if (uwPolicyVO.getUnderwriterId() == null) {
                    // by simon.huang,核保員若為空值則初始核保員
                    uwPolicyVO.setUnderwriterId(underwriterId);
                }
        		//PCR-463250 綜合查詢-新契約資訊要新增顯示核保單位 2023/01/19 Add by Kathy
        		UserVO userVO = SysMgmtDSLocator.getUserDS().getUserByUserID(uwPolicyVO.getUnderwriterId());
        		uwPolicyVO.setUwDeptId(Long.parseLong(userVO == null ? null : userVO.getDeptId()));                 
                ApplicationLogger.addLoggerData("uwPolicyVO.setLockTime() begin");
                uwPolicyVO.setLockTime(currentUserLocalTime);
                uwPolicyDS.updateUwPolicy(uwPolicyVO, false);
                ApplicationLogger.addLoggerData("uwPolicyVO.setLockTime() end");
            }
        }

        UwPolicyForm uwPolicyForm = new UwPolicyForm();
        // 設定UwPolicyForm
        helper.constructUwPolicyForm(isIframe, uwPolicyForm, uwPolicyVO);
        // set uwpolicy information
        request.setAttribute(UwPolicyMainFixAction.MAIN_FORM, uwPolicyForm);

        // saleChannelName
        CoverageVO coverageVO = policyVO.gitMasterCoverage();
        List<CoverageAgentVO> coverageAgents = coverageVO.gitSortCoverageAgents();
        List<Map<String, Object>> agentList = new ArrayList<Map<String, Object>>();
        for (CoverageAgentVO vo : coverageAgents) {
            if (vo.getChannelCode() != null) {
                Map<String, Object> agentData;
                try {
                    agentData = org.apache.commons.beanutils.BeanUtils.describe(vo);
                    agentData.put("agentVipFlag", CodeCst.YES_NO__NO);
                    /* 設定AgentVIP/Target註記 */
                    try {
                        ApplicationLogger.addLoggerData("agentService.isTopAgent() begin");
                        if (policyService.isUNBTargetAgent(vo.getAgentId(), policyVO.getApplyDate(), policyVO.getChannelType())) {
                            agentData.put("agentVipFlag", CodeCst.YES_NO__YES);
                        }
                        ApplicationLogger.addLoggerData("agentService.isTopAgent() end");
                    } catch (Exception e) {
                        //查無資料不處理。
                    }

                    ApplicationLogger.addLoggerData("channelOrgService.findChannelIdByChannelCode() begin");
                    Long channelId = channelOrgService.findChannelIdByChannelCode(vo.getChannelCode());
                    ApplicationLogger.addLoggerData("channelOrgService.findChannelIdByChannelCode() end");
                    if (channelId != null) {
                        ApplicationLogger.addLoggerData("channelOrgService.getChannelOrg() begin");
                        ChannelOrgVO channelOrgVO = channelOrgService.getChannelOrg(channelId);
                        ApplicationLogger.addLoggerData("channelOrgService.getChannelOrg() end");
                        Long channelType = channelOrgVO.getChannelType();
                        agentData.put("salesChannelId", channelType);
                    }
                    agentList.add(agentData);
                } catch (Exception e) {
                    throw ExceptionFactory.parse(e);
                }
            }
        }
        request.setAttribute(AGENT_LIST, agentList);

        // 首期續期繳費方式
        List<PayerAccountVO> payerAccounts = policyVO.getPayerAccounts();

        //PCR-383857 取得付款授權書資料 2020/12/22 Add by Kathy
        if (CollectionUtils.isNotEmpty(payerAccounts)) {
            String foaCreditCardIndi = policyVO.getFoaCreditCardIndi();
            request.setAttribute("foaCreditCardIndi", foaCreditCardIndi);
        }

        ApplicationLogger.addLoggerData("uwPolicyDS.findUwLifeInsuredEntitis() begin");
        Collection insuredLists = uwPolicyDS.findUwLifeInsuredEntitis(underwriteId);
        ApplicationLogger.addLoggerData("uwPolicyDS.findUwProductEntitis() begin");
        Collection benefits = uwPolicyDS.findUwProductEntitis(underwriteId);
        ApplicationLogger.addLoggerData("getUwProductDecisionViewForms() begin");
        Collection<UwProductDecisionViewForm> uwProductDecisionViewForms = getUwProductDecisionViewForms(benefits);
        ApplicationLogger.addLoggerData("getUwProductDecisionViewForms() end");
        request.setAttribute(UwPolicyMainFixAction.PRODUCT_LIST, uwProductDecisionViewForms);

        PolicyHolderVO holder = policyVO.getPolicyHolder();
        // set a flag to differ the holder which is IndivCustomer or
        // organCompany
        String isOrganCompany = "N";
        if (!holder.getParty().isPerson()) {
            isOrganCompany = "Y";
        }
        // get the proposal list
        ApplicationLogger.addLoggerData("helper.getProposalList() begin");
        Collection proposalList = helper.getProposalList(holder, uwPolicyVO);
        ApplicationLogger.addLoggerData("helper.getProposalList() end");
        // get the nominee list
        List<BeneficiaryVO> beneficiaryList = policyVO.getBeneficiaries();
        Collection<HashMap<String, Object>> nomineeList = helper.getNomineeList(beneficiaryList);
        Collection insuredListForms = helper.getInsuredListForms(uwPolicyVO, insuredLists);
        if (UwSourceTypeConstants.NEW_BIZ.equals(uwPolicyVO.getUwSourceType())) {

            // artf137256 若有加費則被保人的是否標準體為 N
            // add by simon.huang on 2015-07-27
            ApplicationLogger.addLoggerData("uwPolicyDS.findUwExtraLoadingEntitis() begin");
            Collection<UwExtraLoadingVO> allExtra = uwPolicyDS.findUwExtraLoadingEntitis(underwriteId);
            ApplicationLogger.addLoggerData("uwPolicyDS.findUwExtraLoadingEntitis() end");
            Map<Long, Integer> insuuredExtraCount = new HashMap<Long, Integer>();
            for (UwExtraLoadingVO uwExtraLoading : allExtra) {
                Integer count = insuuredExtraCount.get(uwExtraLoading.getInsuredId());
                if (count == null) {
                    count = 0;
                }
                count = count + 1;
                insuuredExtraCount.put(uwExtraLoading.getInsuredId(), count);
            }
            for (Iterator iter = insuredListForms.iterator(); iter.hasNext();) {
                UwInsuredListDecisionViewForm uwInsuredDecisionViewForm = (UwInsuredListDecisionViewForm) iter.next();
                Long insuredId = uwInsuredDecisionViewForm.getInsuredId();
                if (insuuredExtraCount.get(insuredId) != null) {
                    uwInsuredDecisionViewForm.setStandLife(CodeCst.YES_NO__NO);
                } else {
                    uwInsuredDecisionViewForm.setStandLife(CodeCst.YES_NO__YES);
                }
            }
            // end artf137256

        }

        // artf137256 非投資型商品保單系統不允異動「投資型保單溢繳直接退費」欄位
        // add by simon.huang on 2015-07-27
        String isIlp = CodeCst.YES_NO__NO;

        // add by ChiaHui.Su on 2016-04-14
        String isAltValidate = CodeCst.YES_NO__NO;

        // 要保書分類
        ApplicationLogger.addLoggerData("policyService.getProposalProdCategory() begin");
        ProdBizCategorySub sub2 = policyService.getProposalProdCategory(coverageVO.getProductId().longValue());
        ApplicationLogger.addLoggerData("policyService.getProposalProdCategory() end");

        // 判斷是否為續保件，若是則保單生效日可修改 changedBy ChiaHui.Su 2016-06-13
        if (CodeCst.RENEW_INDI_RENEW.equals(coverageVO.getRenewIndi())) {
            isAltValidate = CodeCst.YES_NO__YES;
        }

        boolean isProductTypeS1 = categoryService.isProductTypeS1(coverageVO.getProductId().longValue());

        if (ProdBizCategorySub.SUB_12_002.equals(sub2)) {
            isIlp = CodeCst.YES_NO__YES;
        }

        request.setAttribute(UwPolicyMainFixAction.IS_ILP, isIlp);
        request.setAttribute(UwPolicyMainFixAction.IS_ALT_VALIDATE, isAltValidate);
        request.setAttribute(UwPolicyMainFixAction.IS_PRO_S1, isProductTypeS1);
        // end artf137256

        request.setAttribute(UwPolicyMainFixAction.LIFE_ASSURED_LIST, insuredListForms);
        request.setAttribute(UwPolicyMainFixAction.PROPOSAL_LIST, proposalList);
        request.setAttribute(UwPolicyMainFixAction.NOMINEE_LIST, nomineeList);
        request.setAttribute(UwPolicyMainFixAction.IS_COMPANY_HOLDER, isOrganCompany);

        ApplicationLogger.addLoggerData("helper.getDisabilityList() begin");
        Collection disabilityList = helper.getDisabilityList(insuredLists, insuredListForms);
        ApplicationLogger.addLoggerData("helper.getDisabilityList() end");
        request.setAttribute(UwPolicyMainFixAction.DISABILITY_LIST, disabilityList);

        ApplicationLogger.addLoggerData("helper.getRiCompList() begin");
        request.setAttribute(UwPolicyMainFixAction.RI_COMP_LIST, helper.getRiCompList(underwriteId));
        ApplicationLogger.addLoggerData("helper.getRiCompList() end");
        // if (CodeCst.YES_NO__YES.equals(basicInfoVO.getApilpIndi())) {
        request.setAttribute("isApilp", "N");
        // }
        // add by hanzhong.yan for cq:GEL00030033 2007/8/10
        Long masterProductItemId = coverageVO.getItemId();
        int masterProductChargeTerm = QueryUwDataSp.getChargeTerm(masterProductItemId);
        request.setAttribute("masterProductChargeTerm", masterProductChargeTerm);
        // end add
        // get all duplicate endorsement code
        UwQueryDao dao = new UwQueryDao();

        ApplicationLogger.addLoggerData("dao.getDuplicateEndoCode() begin");
        request.setAttribute("dupEndorsementList", dao.getDuplicateEndoCode());
        ApplicationLogger.addLoggerData("dao.getDuplicateEndoCode() end");
        /* 2016/06/27 alex cheng 增加判斷綜合查詢模式不執行以下邏輯isIframe */
        // add to control NBU process with workflow engine,by robert.xu on
        // 2008.4.17
        if (UwSourceTypeConstants.NEW_BIZ.equals(uwPolicyVO.getUwSourceType())
                && (StringUtils.isNullOrEmpty(isIframe) || "false".equals(isIframe))) {
            if (taskId != null) {
                try {
                    ProposalProcessService service = DSProxy.newInstance(ProposalProcessService.class);

                    ApplicationLogger.addLoggerData("service.claimTask() begin");
                    service.claimTask(policyId, ProposalProcessService.TASK_UW);
                    ApplicationLogger.addLoggerData("service.claimTask() end");
                } catch (Exception e) {
                    throw ExceptionFactory.parse(e);
                }
            }
        }
        // add end
        // add by jason.li for get outstanding issues in pending status; begin
        // begin set value for page
        String[] returnListName = new String[]{"uWIssuesList", "uWManualIssuesList"};
        int[] listTypeName = new int[]{CodeCst.PROPOSAL_RULE_TYPE__UW, CodeCst.PROPOSAL_RULE_TYPE__MANUAL_UW};
        for (int i = 0; i < returnListName.length; i++) {
            if (proposalRuleResultService.hasPendingIssue(policyId, listTypeName[i])) {
                request.setAttribute("existOutstandingIssue", CodeCst.YES_NO__YES);
                break;
            } else {
                request.setAttribute("existOutstandingIssue", CodeCst.YES_NO__NO);
            }
        }

        // find out if policy has suspend record.
        String suspendIndi = "N";
        if (uwPolicyService.checkIfSuspendExist(policyId)) {
            suspendIndi = "Y";
        }
        request.setAttribute("suspendIndi", suspendIndi);

        // 再保公司 表格
        List productShowList = new ArrayList();
        // change by Sunny: 因為再保需要險種, 需改table List<Map<String, Object>> getListRi = uwRiApplyService.getRIRequest(underwriteId);
        // 2017/12/19 Modify : YCsu
        // [artf410579](Internal Issue 189705)[UAT2][eBao][UAT2][eBao]保號：0052751760，主頁面再保資料有誤，請修正。
        ApplicationLogger.addLoggerData("uwRiApplyService.getRIResult() begin");
        List<NbRiResultVO> riResultVOList = uwRiApplyService.queryNbRiResultList(policyVO);
        ApplicationLogger.addLoggerData("uwRiApplyService.getRIResultAll() begin");
        List<Map<String, Object>> getListRiAll = uwRiApplyService.getRIResultAll(policyId, policyVO);
        ApplicationLogger.addLoggerData("uwRiApplyService.getRIResultAll() end");
        request.setAttribute(UWRIFAC_EXTEND_LIST, riResultVOList);
        request.setAttribute(UWRIFAC_EXTEND_LIST_ALL, getListRiAll);
        // end

        for (UwProductDecisionViewForm record : uwProductDecisionViewForms) {
            if (record.getBigAmountDiscnt() != null && Double.valueOf(record.getBigAmountDiscnt()) > 0) {
                productShowList.add(record);
            }
        }
        request.setAttribute(UwPolicyMainFixAction.PRODUCT_SHOW_LIST, productShowList);

        ApplicationLogger.addLoggerData("uwTransferHelper.getManualTransferUser() begin");
        List<Long> transferUserList = uwTransferHelper.getManualTransferUser(currentUserId, uwPolicyVO.getUwSourceType());
        ApplicationLogger.addLoggerData("uwTransferHelper.getManualTransferUser() end");
        request.setAttribute("transferUserList", transferUserList);

        //保單狀態
        request.setAttribute("liabilityState", policyVO.getRiskStatus());
        //要保書狀態
        request.setAttribute("proposalStatus", policyVO.getProposalStatus());

        try {
            if (UwSourceTypeConstants.NEW_BIZ.equals(uwPolicyVO.getUwSourceType())) {
                //新契約核保頁面效能已調整，不作debug log記錄
                ApplicationLogger.clear();
            } else {
                ApplicationLogger.flush();
            }

        } catch (Exception e1) {
            logger.warn(e1.getMessage());
        }

        List<CoverageVO> coverages = policyVO.getCoverages();

        List<String> itemIds = new ArrayList<String>();
        String waiverProdIdString = "";
        for (CoverageVO coverage : coverages) {
            if (coverage.isWaiver()) {
                waiverProdIdString = coverage.getProductId().toString();
            } else {
                itemIds.add(String.valueOf(coverage.getItemId()));
            }
        }

        request.setAttribute("itemIds", org.apache.commons.lang3.StringUtils.join(itemIds, ","));
        request.setAttribute("waiverProdId", waiverProdIdString);

        // PCR364900-新契約補全建檔 
        int countKeyingTask = nbTaskServiceCI.countKeyingTask(policyId, NbTaskCst.TASK_NAME_COMPLEMENT);	//"COMPLEMENT_TASK");
        request.setAttribute("countKeyingTask", countKeyingTask);

        //PCR_391319 新式公會-新增被保險人受監護宣告  20201030 by Simon
        //保單層受監護宣告,核保頁面受監護宣告可編輯
        String isGuardianAncmnt = CodeCst.YES_NO__NO;
        if (CodeCst.YES_NO__YES.equals(policyVO.getGuardianAncmnt())) {
            isGuardianAncmnt = CodeCst.YES_NO__YES;
        } else {
            if (isCommonQuery == false) { //綜合查詢不可執行
                for (Object insuredForm : insuredListForms) {
                    UwInsuredListDecisionViewForm uwInsuredListForm = (UwInsuredListDecisionViewForm) insuredForm;
                    //相容在途保單,若保單層受監護宣告為否或空白,所有被保險人Default=否,不可修改
                     //PCR-524169,保全核保，不參考保單層值 -start   
                    if (Utils.isCsUw(sourceType) ) { //POS邏輯
                        if (uwInsuredListForm.getGuardianAncmnt() == null) {
                     	   //當下值為空，才給預設N。若有值，則不變
                     	   uwInsuredListForm.setGuardianAncmnt(CodeCst.YES_NO__NO);
                        }                     
                     }else {	//UNB-原邏輯
                     	uwInsuredListForm.setGuardianAncmnt(CodeCst.YES_NO__NO);
                     } 
                   //PCR-524169,保全核保，不參考保單層值 -end
                }
            }
        }
        request.setAttribute("isGuardianAncmnt", isGuardianAncmnt);
        
        boolean isMicro = lifeProductCategoryService.isMicro(coverageVO.getProductId().longValue());
        request.setAttribute("isMicro", isMicro);
        
        List<NbInsMicroRoleVO> nbInsMicroRoleVOList = nbInsMicroRoleService.findByPolicyId(policyId);
        nbInsMicroRoleVOList.sort(Comparator.comparing(NbInsMicroRoleVO::getMicroType)
                				.thenComparing(Comparator.comparing(NbInsMicroRoleVO::getInsRoleType)));
        
        List<NbInsMicroRoleVO> mainInsMicroRoleVOList = nbInsMicroRoleVOList.stream().filter( 
        		( NbInsMicroRoleVO vo ) -> CodeCst.YES_NO__YES.equals( vo.getMainRoleflag()) ).collect(Collectors.toList());
        
        
        Set<Long> insuredIdSet = new HashSet<Long>();
        List<Map<String,Object>> insMicroRoleName = new ArrayList<Map<String,Object>>();
        List<Map<String,Object>> mainInsMicroRoleMapList = new ArrayList<Map<String,Object>>();
        if(!nbInsMicroRoleVOList.isEmpty() ) {
        	for( NbInsMicroRoleVO vo : nbInsMicroRoleVOList) {
        		insuredIdSet.add(vo.getInsuredId());
        	}
        	
        	for(Long insuredId:insuredIdSet) {
        		 for (Object insuredForm : insuredListForms) {
                     UwInsuredListDecisionViewForm uwInsuredListForm = (UwInsuredListDecisionViewForm) insuredForm;

                     if( insuredId.equals(uwInsuredListForm.getListId())) {
                         Map<String,Object> nameMap = new HashMap<String,Object>();
                         nameMap.put("insuredName", uwInsuredListForm.getLifeAssuredName());
                         nameMap.put("insuredId", insuredId);
                         insMicroRoleName.add(nameMap);
                         
                         for( NbInsMicroRoleVO vo: mainInsMicroRoleVOList) {
                        	 if( insuredId.equals(vo.getInsuredId()) ) {
                        		 
                                 UwLifeInsured uwLifeInsured = uwInsuredListDao.findByUnderwriteIdAndListId(uwInsuredListForm.getUnderwriteId(), uwInsuredListForm.getListId());
                        		 Map<String,Object> mainRoleMap = new HashMap<String,Object>();
                        		 mainRoleMap.put("listId", vo.getListId());
                        		 mainRoleMap.put("insuredId", vo.getInsuredId());
                        		 mainRoleMap.put("insuredName", uwInsuredListForm.getLifeAssuredName());
                        		 mainRoleMap.put("mainRoleFlag", vo.getMainRoleflag());
                        		 mainRoleMap.put("microType", vo.getMicroType());
                        		 mainRoleMap.put("insRoleType", vo.getInsRoleType());
                        		 
                        		 mainRoleMap.put("microDisabilityCategory", uwLifeInsured.getMicroDisabilityCategory());
                        		 mainRoleMap.put("microDisabilityType", uwLifeInsured.getMicroDisabilityType());
                        		 mainRoleMap.put("microDisabilityClass", uwLifeInsured.getMicroDisabilityClass());
                        		 
                        		 StringBuffer sb = new StringBuffer();
                        		 if( uwLifeInsured.getMicroDisabilityTypeList() != null) {
                        			 List<String> types = Arrays.asList(uwLifeInsured.getMicroDisabilityTypeList().split(","));
                            		 for (String type : types) {
                         				if (sb.length() > 0) {
                         					sb.append(",");
                         				} 
                         				sb.append(type + "-" + CodeTable.getCodeDesc("T_DISABILITY_TYPE", type, AppContext.getCurrentUser().getLangId()));
                         			}
                            		
                        		 }
                        		 mainRoleMap.put("microDisabilityTypeList", sb.toString() );

                        		 mainInsMicroRoleMapList.add(mainRoleMap);
                        	 }
                         }
                         

                     }
                 }
        	}
        }
        
        request.setAttribute("insMicroRoleNameList", insMicroRoleName );
        request.setAttribute("mainInsMicroRoleMapList", mainInsMicroRoleMapList );
        
        // navigate to underwriting main page
        return mapping.findForward(MAIN_PAGE);
    }

	/**
	 * <p>Description : 處理測試階段有刪除被豁免險揰邏輯未同時刪除豁免險造成核保主頁面null pointer excetion</p>
	 * <p>Created By : simon.huang</p>
	 * <p>Create Time : Jan 23, 2017</p>
	 * @param uwPolicyVO
	 * @param policyVO
	 * @return
	 */
	private UwPolicyVO fixGarbageData(UwPolicyVO uwPolicyVO, PolicyVO policyVO) {

		boolean isChange = false;

		// WPR 險種未同步刪除，進入核保頁面拋出錯誤
		UserTransaction userTransaction2 = null;
		try {
			userTransaction2 = Trans.getUserTransaction();
			userTransaction2.begin();

			List<CoverageVO> coverages = policyVO.getCoverages();
			Map<String, CoverageVO> itemBindCoverage = NBUtils.toMap(coverages, "itemId");

			Collection benefits = uwPolicyDS.findUwProductEntitis(uwPolicyVO.getUnderwriteId());

			for (Object benefit : benefits) {
				UwProductVO uwProductVO = (UwProductVO) benefit;
				Long itemId = uwProductVO.getItemId();
				if (itemBindCoverage.containsKey(String.valueOf(itemId)) == false) {
					uwPolicyService.removeUwProductByUwListId(uwProductVO.getUwListId());
					isChange = true;
				}
			}
			
			//BC436_因應保險法第107條條文修正案之相關配套措施需求  
			//承保作業-未滿15足歲需於承保作業執行公會下載及喪葬費用檢核
            //核保主頁面清除生效時寫入的NB_INFORCE_107_LAW_CHECK(for undo後重新生效)
			nbPolicySpecialRuleService.deleteByPolicyIdAndTypeId(uwPolicyVO.getPolicyId(),
						NBPolicySpecialRuleType.NB_INFORCE_107_LAW_CHECK);
			
			userTransaction2.commit();
		} catch (Exception e) {
			TransUtils.rollback(userTransaction2);
			throw ExceptionFactory.parse(e);
		}

		if (isChange) {
			uwPolicyVO = uwPolicyDS.findUwPolicy(uwPolicyVO.getUnderwriteId());
		}

		return uwPolicyVO;
	}

	protected void maintainProposalStatus(PolicyVO vo) throws GenericException {
		try {
			Integer nextStatus = Integer.valueOf(CodeCst.PROPOSAL_STATUS__UW);
			vo.setProposalStatus(nextStatus);
			policyCI.updatePolicy(vo);
		} catch (Exception e) {
			throw ExceptionFactory.parse(e);
		}
	}

	/**
	 * Integration with party and other modules, translates UwProductVO into
	 * UwProductDecisionViewForm with the supporting utility( CustomerVO) of party
	 * module
	 * 
	 * @param productVOs Collection UwProductVO collection
	 * @return Collection UwProductDecisionViewForm collection
	 * @throws GenericException Application Exception
	 */
	protected Collection getUwProductDecisionViewForms(Collection productVOs)
					throws GenericException {
		// similar decision lies in method getInsuredListDecisionViewForms
		Collection result = new ArrayList();
		Iterator iter = productVOs.iterator();
		while (iter.hasNext()) {
			UwProductDecisionViewForm uwProductDecisionViewForm = new UwProductDecisionViewForm();
			UwProductVO uwProductVO = (UwProductVO) iter.next();
			BeanUtils.copyProperties(uwProductDecisionViewForm, uwProductVO);
			/**取得被保人姓名**/
			CoverageVO coverageVO = coverageServ.load(uwProductVO.getItemId());
			StringBuffer sbf = new StringBuffer();
			boolean isFirst = true;
			// IR:130568
			if (coverageVO != null && coverageVO.getInsureds().size() > 0) {
				List<CoverageInsuredVO> voList = coverageVO.getInsureds();
				// for (CoverageInsuredVO vo : coverageVO.getInsureds())
				// {
				for (int i = 0; i < voList.size(); i++) {
					CoverageInsuredVO vo = voList.get(i);
					if (vo != null) {
						if (isFirst) {
							isFirst = false;
						} else {
							sbf.append(",");
						}
						sbf.append(policyService.getInsuredByInsuredId(vo.getInsuredId()).getName());
					}
				}
			}

			String benefitName = CodeTable.getCodeDesc("V_NB_PRODUCT_NAME_BY_VERSION",
							uwProductVO.getProductVersionId().toString());
			uwProductDecisionViewForm.setBenefitName(benefitName);

			uwProductDecisionViewForm.setLifeAssuredName(sbf.toString());

			if (uwProductDecisionViewForm.getPolicyFeeAn() != null) {
				uwProductDecisionViewForm.setTotalPremAn(uwProductDecisionViewForm
								.getStdPremAn().add(uwProductDecisionViewForm.getPolicyFeeAn()));
			}
			if (uwProductDecisionViewForm.getExtraPremAn() != null) {
				uwProductDecisionViewForm.setTotalPremAn(uwProductDecisionViewForm
								.getTotalPremAn().add(uwProductDecisionViewForm.getExtraPremAn()));
			}
			try {
				if (Integer.valueOf(CodeCst.PAY_MODE__INVEST_ACCOUNT_OFFER).equals(
								uwProductVO.getPayMode())
								|| Integer.valueOf(CodeCst.PAY_MODE__INVEST_ACC_BID).equals(
												uwProductVO.getPayMode())) {
					uwProductDecisionViewForm.setTotalPremAn(null);
				}
			} catch (Exception ex) {
				throw ExceptionFactory.parse(ex);
			}

			String bigAmountDiscnt = coverageServ.getLargeDiscountRate(uwProductVO.getPolicyId(), uwProductVO.getItemId());
			uwProductDecisionViewForm.setBigAmountDiscnt(bigAmountDiscnt);

			/* 主約查詢高保額折扣 */
			// if (uwProductVO.getMasterId() == null) {
			// String bigAmountDiscnt =
			// coverageServ.getLargeDiscountRate(uwProductVO.getPolicyId());
			// uwProductDecisionViewForm.setBigAmountDiscnt(bigAmountDiscnt);
			// }
			result.add(uwProductDecisionViewForm);
		}
		return result;
	}

	private PolicyVO loadBfInforcePolicy(Long policyId) {
		Map parmMap = new HashMap();
		parmMap.put("policyId", policyId);
		parmMap.put("serviceId", new Integer(CodeCst.SERVICE__IN_FORCE_POLICY));
		Map orderMap = new HashMap();
		orderMap.put("policyChgId", "desc");
		List<AlterationItemVO> alterationItemVOs = alterationItemService.find(parmMap, orderMap);
		PolicyVO policy = null;

		if (alterationItemVOs != null) {
			Long policyChgId = alterationItemVOs.get(0).getPolicyChgId();
			policy = policyDS.findBfByPolicyIdAndPolicyChgId(policyId, policyChgId);
		}
		return policy;
	}


	  /**
	   * calculate validate and relative information ,relculate benefit prem<br/>
	   * 執行以下： <br/>
	   * * 保項重新排序,pkg_ls_pm_nb_inforce.p_refresh_item_order
	   * * update_payer_account<br/>
	   * * 同步payer_account.pay_mode/pay_next回t_contract_product <br/>
	   * * 同步t_contract_master.submission_date/apply_date回t_contract_product  <br/>
	   * * update_prem_discnt_nb新契約保費折讓寫入   <br/>
	   * * update_age 年齡更新   <br/>
	   * * recl_commencedate_and_prem更新生效日/保障終止日/保費計算   <br/>
	   * * PKG_LS_PRD_RL_PDS_CI.p_update_policy_send_type (RTC 102929:回退修改增加執行保單寄發方式修改 ) <br/>
	   * @param policyId Long
	   * @throws GenericException
	   */
	  public void submitDetailReg(Long policyId) throws GenericException {
	    Connection conn = null;
	    CallableStatement stmt = null;
	    DBean db = new DBean(true);
	    
	    String logText = "PKG_LS_PM_NB.P_SUBMIT_DETAIL_REG(" + policyId + "," + AppContext.getCurrentUser().getUserId() + ")";
	    try {

	    	NBUtils.logger(ProposalServiceSp.class, logText + " begin");

	    	//效能問題將P_SUBMIT_DETAIL_REG 處理事項拆為多次執行，並記錄LOG確認慢的地方
	    	db.connect();
	    	conn = db.getConnection();

	    	NBUtils.logger(ProposalServiceSp.class, logText + " call  pkg_ls_prd_calc_para.p_clear_cache()");  
	    	stmt =  conn.prepareCall("{call  pkg_ls_prd_calc_para.p_clear_cache()}");
	    	stmt.executeUpdate();
	    	JdbcUtils.closeStatement(stmt);


	    	NBUtils.logger(ProposalServiceSp.class, logText + " call  pkg_ls_prd_rl_pds_ci.p_update_policy_send_type()");  
	    	stmt =  conn.prepareCall("{call  pkg_ls_prd_rl_pds_ci.p_update_policy_send_type(?)}");
	    	stmt.setLong(1, policyId.longValue());
	    	stmt.executeUpdate();
	    	JdbcUtils.closeStatement(stmt);

	    	NBUtils.logger(ProposalServiceSp.class, logText + " call  PKG_LS_PM_NB.P_STORE_EXTRA_VALUE2()");  
	    	stmt = conn.prepareCall("{call PKG_LS_PM_NB.P_STORE_EXTRA_VALUE2(?,?)}");
	    	stmt.setLong(1, policyId.longValue());
	    	stmt.setLong(2, AppContext.getCurrentUser().getUserId());
	    	stmt.executeUpdate();
	    	JdbcUtils.closeStatement(stmt);

	    	NBUtils.logger(ProposalServiceSp.class, logText + " call  PKG_LS_PM_NB.p_update_prem_discnt_nb()");  
	    	stmt = conn.prepareCall("{call PKG_LS_PM_NB.p_update_prem_discnt_nb(?)}");
	    	stmt.setLong(1, policyId.longValue());
	    	stmt.executeUpdate();
	    	JdbcUtils.closeStatement(stmt);

	    	NBUtils.logger(ProposalServiceSp.class, logText + " call  PKG_LS_PM_NB.P_STORE_EXTRA_VALUE3()");  
	    	stmt = conn.prepareCall("{call PKG_LS_PM_NB.P_STORE_EXTRA_VALUE3(?,?)}");
	    	stmt.setLong(1, policyId.longValue());
	    	stmt.setLong(2, AppContext.getCurrentUser().getUserId());
	    	stmt.executeUpdate();

	    } catch (Exception e) {
	      throw ExceptionFactory.parse(e);
	    } finally {
	      DBean.closeAll(null, stmt, db);
	      NBUtils.logger(ProposalServiceSp.class, logText + " end");
	    }
	  }
  
	@Resource(name = UwProcessService.BEAN_DEFAULT)
	protected UwProcessService uwProcessDS;

	public void setUwProcessDS(UwProcessService uwProcessDS) {
		this.uwProcessDS = uwProcessDS;
	}

	public UwProcessService getUwProcessDS() {
		return uwProcessDS;
	}

	@Resource(name = ProposalRuleResultService.BEAN_DEFAULT)
	protected ProposalRuleResultService proposalRuleResultService;

	@Resource(name = PolicyCI.BEAN_DEFAULT)
	protected PolicyCI policyCI;

	public PolicyCI getPolicyCI() {
		return policyCI;
	}

	public void setPolicyCI(PolicyCI policyCI) {
		this.policyCI = policyCI;
	}

	@Resource(name = CSCI.BEAN_DEFAULT)
	protected CSCI csCI;

	public CSCI getCsCI() {
		return csCI;
	}

	public void setCsCI(CSCI csCI) {
		this.csCI = csCI;
	}

	@Resource(name = AlterationItemService.BEAN_DEFAULT)
	private AlterationItemService alterationItemService;

	@Resource(name = AgentService.BEAN_DEFAULT)
	protected AgentService agentService;

	public AgentService getAgentService() {
		return agentService;
	}

	public void setAgentService(AgentService agentService) {
		this.agentService = agentService;
	}

	@Resource(name = UwPolicySubmitActionHelper.BEAN_DEFAULT)
	protected UwPolicySubmitActionHelper helper;

	public UwPolicySubmitActionHelper getHelper() {
		return helper;
	}

	public void setHelper(UwPolicySubmitActionHelper helper) {
		this.helper = helper;
	}

	@Resource(name = ProposalProcessService.BEAN_DEFAULT)
	protected ProposalProcessService proposalProcessService;

	public ProposalProcessService getProposalProcessService() {
		return proposalProcessService;
	}

	public void setProposalProcessService(
					ProposalProcessService proposalProcessService) {
		this.proposalProcessService = proposalProcessService;
	}

	public ProposalRuleResultService getProposalRuleResultService() {
		return proposalRuleResultService;
	}

	public void setProposalRuleResultService(
					ProposalRuleResultService proposalRuleResultService) {
		this.proposalRuleResultService = proposalRuleResultService;
	}

	@Resource(name = PolicyService.BEAN_DEFAULT)
	private PolicyService policyService;

	@Resource(name = com.ebao.ls.pa.pub.bs.PolicyService.BEAN_DEFAULT)
	private com.ebao.ls.pa.pub.bs.PolicyService policyDS;

	@Resource(name = ProposalService.BEAN_DEFAULT)
	private ProposalService proposalService;

	@Resource(name = UwPolicyService.BEAN_DEFAULT)
	private UwPolicyService uwPolicyService;

	@Resource(name = LifeProductCategoryService.BEAN_DEFAULT)
	protected LifeProductCategoryService lifeProductCategoryService;

	@Resource(name = CoverageService.BEAN_DEFAULT)
	private CoverageService coverageServ;

	@Resource(name = CsTaskTransService.BEAN_DEFAULT)
	private CsTaskTransService csTaskTransService;

	@Resource(name = UwRiApplyService.BEAN_DEFAULT)
	protected UwRiApplyService uwRiApplyService;

	@Resource(name = ChannelOrgService.BEAN_DEFAULT)
	private ChannelOrgService channelOrgService;
	
	@Resource(name = NbInsMicroRoleService.BEAN_DEFAULT)
	protected NbInsMicroRoleService nbInsMicroRoleService;
	
    @Resource(name = TUwInsuredListDelegate.BEAN_DEFAULT)
    private TUwInsuredListDelegate uwInsuredListDao;

	@Resource(name = ReviewService.BEAN_DEFAULT)
	protected ReviewService reviewService;

	@Resource(name = UwTransferHelper.BEAN_DEFAULT)
	private UwTransferHelper uwTransferHelper;

	@Resource(name = LifeProductCategoryService.BEAN_DEFAULT)
	private LifeProductCategoryService categoryService;

	@Resource(name = DetailRegHelper.BEAN_DEFAULT)
	protected DetailRegHelper detailRegHelper;
	
	@Resource(name = RemoteHelper.BEAN_DEFAULT)
	protected RemoteHelper remoteHelper;

	@Resource(name = PremCI.BEAN_DEFAULT)
	protected PremCI premCI;
	
	// PCR364900-新契約補全建檔 
	@Resource(name = NbTaskService.BEAN_DEFAULT)
	private NbTaskService nbTaskServiceCI;
	
	@Resource(name = NbPolicySpecialRuleService.BEAN_DEFAULT)
	private NbPolicySpecialRuleService nbPolicySpecialRuleService;

}
