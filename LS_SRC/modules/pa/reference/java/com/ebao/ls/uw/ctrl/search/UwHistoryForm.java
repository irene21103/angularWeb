package com.ebao.ls.uw.ctrl.search;

import java.util.Date;

import com.ebao.ls.uw.ctrl.pub.UwPolicyForm;

/**
 * <p>Title:GEL-UW </p>
 * <p>Description:Underwriting History Form </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: </p>
 * @author jason.luo
 * @version 1.0
 * @since 08.10.2004
 */

public class UwHistoryForm extends UwPolicyForm {

  //used for calculate reviewPeriod purpose
  //day in milliseconds
  public static final int DAY_IN_MILLISECONDS = 24 * 60 * 60 * 1000;

  //add by hanzhong.yan for cq:GEL00034610 2007/10/24
  public Date dueDate;

  /**
   * Default constructor
   */
  public UwHistoryForm() {

  }

  /**
   * returns a String representation of the condition codes,delimited by comma
   * @return String
   * @author jason.luo
   * <p>Create Time: 07.26.2004</p>
   */
  public String getConditionCodes() {
    if (null == uwconditions || uwconditions.length == 0) {
      return "";
    } else {
      StringBuffer sbf = new StringBuffer();
      int length = uwconditions.length;
      sbf.append(uwconditions[0].getConditionCode());
      for (int i = 1; i < length; i++) {
        sbf.append(",").append(uwconditions[i].getConditionCode());
      }
      return sbf.toString();
    }
  }
  /**
   * returns a String representation of the endowsment codes,delimited by comma
   * @return String
   * @author yixing.lu
   * <p>Create Time: 23.03.2005</p>
   */
  public String getEndowsmentsCodes() {
    if (null == uwendorsements || uwendorsements.length == 0) {
      return "";
    } else {
      StringBuffer sbf = new StringBuffer();
      int length = uwconditions.length;
      sbf.append(uwendorsements[0].getEndoCode());
      for (int i = 1; i < length; i++) {
        sbf.append(",").append(uwendorsements[i].getEndoCode());
      }
      return sbf.toString();
    }
  }
  /**
   * returns a String representation of the exclusioncode and review period
   * using the follwing format:
   *   exclusioncode1-review period1,exclusioncode2-review period2
   * @return String
   * @author jason.luo
   * <p>Create Time: 07.26.2004</p>
   */
  public String getExclusionCodes() {
    if (null == uwexclusions || uwexclusions.length == 0) {
      return "";
    } else {
      StringBuffer sbf = new StringBuffer();
      int length = uwexclusions.length;
      sbf.append(uwexclusions[0].getExclusionCode());
      uwexclusions[0].setCommencementDate(getValidateDate());
      if (null != uwexclusions[0].getReviewPeriod()
          && !"".equals(uwexclusions[0].getReviewPeriod())) {
        sbf.append("-");
      }
      sbf.append(uwexclusions[0].getReviewPeriod());

      for (int i = 1; i < length; i++) {
        sbf.append(",");
        sbf.append(uwexclusions[i].getExclusionCode());
        uwexclusions[i].setCommencementDate(getValidateDate());
        if (null != uwexclusions[i].getReviewPeriod()
            && !"".equals(uwexclusions[i].getReviewPeriod())) {
          sbf.append("-");
        }
        sbf.append(uwexclusions[i].getReviewPeriod());
      }
      return sbf.toString();
    }
  }

  public Date getDueDate() {
    return dueDate;
  }

  public void setDueDate(Date dueDate) {
    this.dueDate = dueDate;
  }

} //end of class
