package com.ebao.ls.uw.ds;

import java.util.List;

import javax.annotation.Resource;

import com.ebao.foundation.module.GenericException;
import com.ebao.ls.foundation.service.GenericServiceImpl;
import com.ebao.ls.pa.pub.data.bo.UwAddInformLetter;
import com.ebao.ls.pa.pub.vo.UwAddInformLetterVO;
import com.ebao.ls.uw.data.UwAddInformLetterDelegate;

/**
 * <p>Title: 核保頁面_被保人補充告知事項</p>
 * <p>Description: 核保頁面_被保人補充告知事項</p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company: TGL Co., Ltd.</p>
 * <p>Create Time: Jun 8, 2016</p> 
 * @author 
 * <p>Update Time: Jun 8, 2016</p>
 * <p>Updater: Sunny Wu</p>
 * <p>Update Comments: </p>
 */
public class UwAddInformLetterServiceImpl extends GenericServiceImpl<UwAddInformLetterVO, UwAddInformLetter, UwAddInformLetterDelegate> implements UwAddInformLetterService {
	
	@Resource(name = UwAddInformLetterDelegate.BEAN_DEFAULT)
    private UwAddInformLetterDelegate uwUwAddInformLetterDao;
	
	@Override
	  protected UwAddInformLetterVO newEntityVO() {
	    return new UwAddInformLetterVO();
	  }
	

	public void updateDocumentId(Long listId,Long documentId){
		if(listId == null || documentId == null){
			throw new GenericException("T_UW_ADDINFORM_LETTER.list_id or DOCUMENT_ID is null ! "); 
		}
		UwAddInformLetter bo = uwUwAddInformLetterDao.load(listId);
		bo.setDocumentId(documentId);
		
		uwUwAddInformLetterDao.saveorUpdate(bo);
	}

	@Override
	public UwAddInformLetterVO findByPolicyInsured(Long underwrtieId, int sourceType, Long insuredId) {
		UwAddInformLetter letter = uwUwAddInformLetterDao.findUwAddInformLetterByUnderwriteIdInsuredId(underwrtieId, sourceType, insuredId);
		if ( letter != null) {
			UwAddInformLetterVO vo = new UwAddInformLetterVO();
			letter.copyToVO(vo, true);
			return vo;
		}
		return null;
	}
	
	@Override
	public UwAddInformLetterVO findByPolicyInsured(Long underwrtieId, Long changeId, int sourceType, Long insuredId) {
		UwAddInformLetter letter = uwUwAddInformLetterDao.findByUnderwriteIdChangeIdInsuredId(underwrtieId, changeId, sourceType, insuredId);
		if ( letter != null) {
			UwAddInformLetterVO vo = new UwAddInformLetterVO();
			letter.copyToVO(vo, true);
			return vo;
		}
		return null;
	}
	
	/**
	 * <p>Description :使用T_PROPOSAL_RULE_RESULT.LIST_ID找UwAddInformLetterVO </p>
	 * <p>Created By : TGL144</p>
	 * <p>Create Time : Dec 19, 2016</p>
	 * @param msgId
	 * @return
	 */
	public UwAddInformLetterVO findByMsgId(Long msgId) {
		UwAddInformLetter letter = uwUwAddInformLetterDao.findByMsgId(msgId);
		if ( letter != null) {
			UwAddInformLetterVO vo = new UwAddInformLetterVO();
			letter.copyToVO(vo, true);
			return vo;
		}
		return null;
	}
	
	@Override
	public List<UwAddInformLetterVO> findByUnderwriteId(Long underwriteId) {
		List<UwAddInformLetter> boList = uwUwAddInformLetterDao.findByUnderwriteId(underwriteId);
		return this.convertToVOList(boList);
	}
}
