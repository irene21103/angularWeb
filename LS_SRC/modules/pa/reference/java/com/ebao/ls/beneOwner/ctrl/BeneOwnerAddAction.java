package com.ebao.ls.beneOwner.ctrl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.foundation.core.logging.Log;
import com.ebao.ls.cs.boaudit.ds.BeneficialOwnerService;
import com.ebao.ls.cs.boaudit.vo.BeneficialOwnerVO;
import com.ebao.ls.pty.ci.ContactCI;
import com.ebao.ls.pty.ci.CustomerCI;
import com.ebao.ls.pty.vo.AddressVO;
import com.ebao.ls.pty.vo.CompanyCustomerVO;
import com.ebao.ls.pty.vo.CustomerVO;
import com.ebao.ls.pub.CodeCst;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.framework.GenericAction;
import com.ebao.pub.i18n.lang.StringResource;

public class BeneOwnerAddAction extends GenericAction {

	private final Log log = Log.getLogger(BeneOwnerAddAction.class);

	@Resource(name = CustomerCI.BEAN_DEFAULT)
	private CustomerCI customerCI;

	@Resource(name = BeneficialOwnerService.BEAN_DEFAULT)
	private BeneficialOwnerService beneficialOwnerService;

	@Resource(name = ContactCI.BEAN_DEFAULT)
	private ContactCI contactCI;

	@Override
	public ActionForward process(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		// 因為如果開新子視窗，關掉後，母視窗submit 有processWarning，會被導到子視窗
		BeneOwnerAddForm boForm = (BeneOwnerAddForm) form;
		Long companyId = boForm.getCompanyId();
		if (companyId == null) {
			// 綜合查詢共用頁面
			String companyIdPara = request.getParameter("companyId");
			if (NumberUtils.isDigits(companyIdPara)) {
				boForm.setCompanyId(Long.valueOf(companyIdPara));
				String submitModeFromOpenPara = request.getParameter("submitModeFromOpen");
				if (StringUtils.isNotBlank(submitModeFromOpenPara)) {
					boForm.setSubmitModeFromOpen(submitModeFromOpenPara);
				}
			}
		} else{
			String saveData = request.getParameter("saveData");
			if ("Y".equals(saveData)){
				//存檔成功
				boForm.setSaveSuccess(StringResource.getStringData("MSG_1257228", AppContext.getCurrentUser().getLangId()));
			}
		}
		beneOwnerAddDisplayInit(boForm);
		boForm.setCheckBeneOwnerRuleMsg(beneficialOwnerService.getCheckBeneOwnerRuleStr(boForm.getCompanyId()));

		return mapping.findForward("success");
	}

	/**
	 * 利用parentId 查詢顯示於頁面上資訊
	 * 
	 * @return
	 */
	private void beneOwnerAddDisplayInit(BeneOwnerAddForm boForm) {
		// 頁面輸入-統一編(證)號
		Long selectCompanyId = boForm.getCompanyId();
		String submitModeFromOpen = boForm.getSubmitModeFromOpen();
		if (StringUtils.isBlank(submitModeFromOpen)) {
			boForm.setSubmitModeFromOpen("QUERY");
		}

		if (selectCompanyId == null) {
			boForm.setSubmitModeFromOpen("INSERT");
		}

		// 取得法人基本資料
		boolean hasCompany = getDisplayCompanyData(boForm, selectCompanyId);

		// 判斷是否有法人資料，如無法人資料，就不須查詢法人實質受益人清單
		if (!hasCompany) {
			return;
		}
		// 取得法人實質受益人明細
		boForm.setBeneOwnerList(getDisplayBeneOwnerList(selectCompanyId));
	}

	/**
	 * 利用法人partyId查詢法人實質受益人資料顯示dataTable
	 * 
	 * @param parentId
	 */
	private List<BeneOwnerForm> getDisplayBeneOwnerList(Long parentId) {
		List<BeneOwnerForm> resultList = new ArrayList<BeneOwnerForm>();
		if (parentId == null) {
			return resultList;
		}

		List<BeneficialOwnerVO> beneOwnerList = beneficialOwnerService.getBeneficialOwnerByParentId(parentId);
		for (BeneficialOwnerVO beneficialOwnerVO : beneOwnerList) {
			// 當不是有效的法人實質受益人，則不顯示
			if (!StringUtils.equals(CodeCst.BENEFICIARY_OWNER_TYPE__IS_VALID_VALID, beneficialOwnerVO.getIsValid())) {
				continue;
			}
			Long listId = beneficialOwnerVO.getListId();
			Long boPartyId = beneficialOwnerVO.getBoPartyId();
			BeneOwnerForm tempBeneOwnerForm = new BeneOwnerForm();
			String partyType = beneficialOwnerVO.getPartyType();
			// 法人 撈T_COMPANY_CUSTOMER
			if (CodeCst.BENEFICIARY_OWNER_TYPE__PARTY_TYPE_COMPANY.equals(partyType)) {
				tempBeneOwnerForm.setIsCompany(CodeCst.YES_NO__YES);
				CompanyCustomerVO companyCustomer = customerCI.getCompany(Long.valueOf(boPartyId));
				if (companyCustomer == null) {
					continue;
				}
				tempBeneOwnerForm.setBeneOwnerCode(companyCustomer.getRegisterCode());
				tempBeneOwnerForm.setBeneOwnerName(companyCustomer.getCompanyName());
				tempBeneOwnerForm.setBeneOwnerNationality(companyCustomer.getNationality());
				tempBeneOwnerForm.setBeneOwnerNationality2(companyCustomer.getNationality2());

				// 自然人 撈T_CUSTOMER
			} else if (CodeCst.BENEFICIARY_OWNER_TYPE__PARTY_TYPE_CUSTOMER.equals(partyType)) {
				tempBeneOwnerForm.setIsCompany(CodeCst.YES_NO__NO);
				CustomerVO customerVO = customerCI.getPerson(boPartyId);
				if (customerVO == null) {
					continue;
				}
				tempBeneOwnerForm.setBeneOwnerCode(customerVO.getCertiCode());
				tempBeneOwnerForm.setBeneOwnerName(customerVO.getFirstName());
				tempBeneOwnerForm.setBeneOwnerNationality(customerVO.getNationality());
				tempBeneOwnerForm.setBeneOwnerNationality2(customerVO.getNationality2());
				tempBeneOwnerForm.setBirthday(customerVO.getBirthday());
			} else {
				continue;
			}
			// list_id
			tempBeneOwnerForm.setListId(listId);
			// 實質受益人party_id
			tempBeneOwnerForm.setBoPartyId(boPartyId);
			// 實質控制權人
			tempBeneOwnerForm.setActualController(beneficialOwnerVO.getActualController());
			// 持有股份百分比
			tempBeneOwnerForm.setShares(beneficialOwnerVO.getShares());
			// 職稱
			tempBeneOwnerForm.setPosition(beneficialOwnerVO.getPosition());
			
			tempBeneOwnerForm.setbOUpdateBy(beneficialOwnerVO.getbOUpdateBy());

			tempBeneOwnerForm.setbOUpdateTime(beneficialOwnerVO.getbOUpdateTime());
			// 身分別
			tempBeneOwnerForm.setBeneOwnerType(beneficialOwnerVO.getType());
			
			resultList.add(tempBeneOwnerForm);
		}

		return resultList;
	}

	/**
	 * 取得法人資料
	 * 
	 * @param boForm
	 * @param selectCompanyId
	 * @return hasCompany 判斷是否有存在法人，如無存在後續都不處理
	 */
	private boolean getDisplayCompanyData(BeneOwnerAddForm boForm, Long selectCompanyId) {
		// 判斷是否有存在法人，如無存在後續都不處理
		boolean hasCompany = false;
		if (selectCompanyId == null) {
			return hasCompany;
		}
		// 取得法人資料
		// 法人
		CompanyCustomerVO companyCustomer = customerCI.getCompany(selectCompanyId);

		if (companyCustomer == null) {
			return hasCompany;
		}
		hasCompany = true;

		boForm.setRegisterCodeHidden(companyCustomer.getRegisterCode());

		// 統一編號
		boForm.setRegisterCode(companyCustomer.getRegisterCode());
		// 名稱
		boForm.setCompanyName(companyCustomer.getCompanyName());
		// 公家機關
		boForm.setIsGov(companyCustomer.getIsGov());
		// 機構類型
		boForm.setGovType(companyCustomer.getGovType());
		// 行業代碼
		boForm.setCompanyCategory(companyCustomer.getCompanyCategory());
		// 法人發行無記名股票情況
		boForm.setBearerIndi(companyCustomer.getBearerIndi());
		// 無記名發行股數佔總發行股數之(%)
		boForm.setBearerPercentage(companyCustomer.getBearerPercentage());
		// 註冊地
		Long regAddressId = companyCustomer.getRegAddressId();
		if (regAddressId != null) {
			AddressVO regAddressVO = new AddressVO();
			regAddressVO = contactCI.getAddressbyAddressId(regAddressId);
			if (regAddressVO != null) {
				boForm.setRegAddress(regAddressVO.getAddress1());
				boForm.setRegNationality(regAddressVO.getNationality());
				boForm.setRegPostCode(regAddressVO.getPostCode());
			}
		}
		// 註冊地
		Long bizAddressId = companyCustomer.getBizAddressId();
		AddressVO bizAddressVO = new AddressVO();
		if (bizAddressId != null) {
			bizAddressVO = contactCI.getAddressbyAddressId(bizAddressId);
			if (bizAddressVO != null) {
				boForm.setBizAddress(bizAddressVO.getAddress1());
				boForm.setBizNationality(bizAddressVO.getNationality());
				boForm.setBizPostCode(bizAddressVO.getPostCode());
			}
		}
		// 法人最後異動人員
		boForm.setCompanyUpdaterId(companyCustomer.getUpdaterId());
		// 法人最後異動日期
		boForm.setCompanyUpdateTime(companyCustomer.getUpdateTime());
		
		return hasCompany;
	}
}
