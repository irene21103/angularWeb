package com.ebao.ls.liaRoc.ci;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.Resource;

import com.ebao.foundation.module.web.action.ExceptionInfoUtils;
import com.ebao.ls.liaRoc.LiaRocCst;
import com.ebao.ls.liaRoc.bo.LiaRocDownload;
import com.ebao.ls.liaRoc.bo.LiaRocDownload2020;
import com.ebao.ls.liaRoc.bo.LiaRocDownloadDetail;
import com.ebao.ls.liaRoc.bo.LiaRocDownloadDetailVO;
import com.ebao.ls.liaRoc.bo.LiaRocDownloadVO;
import com.ebao.ls.liaRoc.data.LiaRocDownload2020Dao;
import com.ebao.ls.liaRoc.data.LiaRocDownloadDao;
import com.ebao.ls.liaRoc.data.LiaRocDownloadDetailDao;
import com.ebao.ls.liaRoc.data.LiaRocDownloadLogDao;
import com.ebao.ls.liaRoc.ds.LiaRocDownloadService;
import com.ebao.ls.pa.liaRoc.bo.LiaRocDownloadLog;
import com.ebao.ls.pa.nb.bs.rule.vo.InsuredVO;
import com.ebao.ls.pa.nb.bs.rule.vo.ProposalVO;
import com.ebao.ls.pa.nb.util.NBUtils;
import com.ebao.ls.pub.CodeCst;
import com.ebao.ls.riskAggregation.data.bo.AggregationResult;
import com.ebao.ls.uw.ds.vo.LiaRocPaidInjuryMedicalDetailVO;
import com.ebao.ls.ws.ci.esp.ESPWSServiceCI;
import com.ebao.ls.ws.vo.client.esp.RqHeader;
import com.ebao.ls.ws.vo.cust.liaRoc.LiaRocDetailInfoWSVO;
import com.ebao.ls.ws.vo.cust.liaRoc.LiaRocDownloadRequestRootVO;
import com.ebao.ls.ws.vo.cust.liaRoc.LiaRocDownloadResponseRootVO;
import com.ebao.ls.ws.vo.cust.liaRoc.LiaRocDownloadWSVO;
import com.ebao.pub.fileresolver.reader.FixedLengthReader;
import com.ebao.pub.fileresolver.writer.FixedLengthWriter;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.framework.GenericException;
import com.ebao.pub.util.BeanUtils;
import com.ebao.pub.util.StringUtils;

/**
 * <p>PCR_344444 新/舊公會下載，此處僅處理舊公會資料<br/>
 * LiaRocDownloadCIImpl(舊公會資料)<br/>
 * </p>
 * @author Simon.Huang
 *
 */
public class LiaRocDownloadCIImpl implements LiaRocDownload2019CI {

    @Resource(name = "liaRocDownloadRequestWriter")
    private FixedLengthWriter requestWriter;

    @Resource(name = "liaRocDownloadNotFoundReader")
    private FixedLengthReader notFoundReader;

    @Resource(name = "liaRocDownloadResultReader")
    private FixedLengthReader resultReader;

    @Resource(name = LiaRocFileCI.BEAN_DEFAULT)
    private LiaRocFileCI liaRocFileCI;

    @Resource(name = LiaRocDownloadService.BEAN_DEFAULT)
    private LiaRocDownloadService liaRocDownloadService;

    @Resource(name = ESPWSServiceCI.BEAN_DEFAULT)
    private ESPWSServiceCI espWSServiceCI;

    @Resource(name = LiaRocDownloadDao.BEAN_DEFAULT)
    private LiaRocDownloadDao<LiaRocDownload> liaRocDownloadDao;
    
    @Resource(name = LiaRocDownloadLogDao.BEAN_DEFAULT)
    private LiaRocDownloadLogDao<LiaRocDownloadLog> liaRocDownloadLogDao;

    @Resource(name = LiaRocDownloadDetailDao.BEAN_DEFAULT)
    private LiaRocDownloadDetailDao liaRocDownloadDetailDao;

    @Resource(name = LiaRocDownload2020Dao.BEAN_DEFAULT)
    private LiaRocDownload2020Dao<LiaRocDownload2020> liaRocDownload2020Dao;
    

    // for testing purpose
//    public static void main(String[] args) {
//
//        LiaRocDownloadCIImpl impl = new LiaRocDownloadCIImpl();
//        Set<String> ids = new LinkedHashSet<String>();
//        ids.add("A123456789");
//        ids.add("A123456788");
//        ids.add("B987654321");
//        impl.send(ids);
//
//    }

    @Override
    public List<LiaRocDownloadVO> sendAndReturn(Set<String> ids) throws GenericException {
        return this.doSendLiaRoc(null, ids);
    }

    @SuppressWarnings("unchecked")
    @Override
    public boolean send(Set<String> ids) throws GenericException {
        List<LiaRocDownloadVO> downloadResults = this.doSendLiaRoc(null, ids);
        if (downloadResults == null) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public boolean send(Long policyId, Set<String> ids) throws GenericException {

        List<LiaRocDownloadVO> downloadResults = this.doSendLiaRoc(policyId, ids);

        if (downloadResults == null) {
            return false;
        } else {
            return true;
        }

    }

    /**
     * <p>Description : 呼叫服務平台下載公會通報資料</p>
     * <p>Created By : Amy Hung</p>
     * <p>Create Time : Aug 19, 2016</p>
     * @param policyId
     * @param ids
     * @return 公會下載結果 List, 若為 null 表示呼叫公會發生錯誤，下載失敗，反之則為呼叫公會成功(但有可能查無資料，所以 list.size =0)
     */
    private List<LiaRocDownloadVO> doSendLiaRoc(Long policyId, Set<String> ids) {

        if (ids == null || ids.size() == 0) {
            new IllegalArgumentException("ids should not be null or empay");
        }

        List<LiaRocDownloadWSVO> resultList = null;
        List<LiaRocDownloadVO> returnResult = null; // 下載回傳結果

        String transId = "";
        Date requestTime = null;
        Date responseTime = null;
        boolean isSuccessful = true;

        // 將身份證號轉為呼叫WS的VO格式
        LiaRocDownloadRequestRootVO requestRootVO = new LiaRocDownloadRequestRootVO();
        Vector<LiaRocDownloadWSVO> wsIds = new Vector<LiaRocDownloadWSVO>();
        for (int i = 0; i < ids.size(); i++) {

            LiaRocDownloadWSVO reqVO = new LiaRocDownloadWSVO();
            LiaRocDetailInfoWSVO idVO = new LiaRocDetailInfoWSVO();

            idVO.setSn("1");

            //245800 身份證號超過10碼以前10碼傳至公會 modify by Kathy
            //idVO.setCertiCode((ids.toArray()[i].toString()));
            String certiCode = ids.toArray()[i].toString();

			if (certiCode.length() > 10) {

				certiCode = certiCode.substring(0, 10);
			}
			idVO.setCertiCode(certiCode);
            
            idVO.setNoDataFlag(null);
            reqVO.setSn(String.valueOf(i + 1));
            reqVO.getList().add(idVO);

            wsIds.add(reqVO);

        }
        requestRootVO.setList(wsIds);
        // end.
        
        Logger.getLogger(LiaRocDownloadCIImpl.class.getName()).log(Level.INFO,
                        "Call espWSServiceCI.liaROCDownload INPUT => "+  requestRootVO.toString());
        
        // 呼叫服務平台下載公會通報資料
        try {

            requestTime = AppContext.getCurrentUserLocalTime();

//            LiaRocDownloadResponseRootVO wsResult = commonWSCI.callEsp(WebServiceName.ESP_LIAROC_DOWNLOAD_NB,
//                            requestRootVO, null, null);
            
            LiaRocDownloadResponseRootVO wsResult = espWSServiceCI.liaROCDownload(new RqHeader(), requestRootVO);
            
            Logger.getLogger(LiaRocDownloadCIImpl.class.getName()).log(Level.INFO,
                            "Call espWSServiceCI.liaROCDownload OUTPUT => "+  wsResult);
            if(wsResult != null) {
            	  Logger.getLogger(LiaRocDownloadCIImpl.class.getName()).log(Level.INFO,
                                  "Call espWSServiceCI.liaROCDownload OUTPUT => "+  wsResult.getBody());
            	  Logger.getLogger(LiaRocDownloadCIImpl.class.getName()).log(Level.INFO,
                                  "Call espWSServiceCI.liaROCDownload OUTPUT => "+  wsResult.getBody().getList());
                  
                  resultList = wsResult.getBody().getList();
                  //resultList = this.mockLiaRocDownloadWSVO();

                  if (resultList == null) {
                      isSuccessful = false;
                      Logger.getLogger(LiaRocDownloadCIImpl.class.getName()).log(Level.INFO,
                                      "Call espWSServiceCI.liaROCDownload 成功，但卻拿到 null 的 list");
                  } else if(resultList.size() == 0 ) {
                      isSuccessful = false;
                      Logger.getLogger(LiaRocDownloadCIImpl.class.getName()).log(Level.INFO,
                                      "Call espWSServiceCI.liaROCDownload 成功，但卻拿到 size = 0 的 list");
                  }
            } else {
            	isSuccessful = false;
            }
            
            responseTime = AppContext.getCurrentUserLocalTime();

        } catch (Exception e) {
            // 呼叫發生錯誤
            isSuccessful = false;
            Logger.getLogger(LiaRocDownloadCIImpl.class.getName()).log(Level.INFO,
                            "Call espWSServiceCI.liaROCDownload encounter error, Error Msg:" + 
            ExceptionInfoUtils.getExceptionMsg(e));
        }
        // end.

        if (isSuccessful) {

            // 回來的資料還要做加工才能用
            for (LiaRocDownloadWSVO liaRocDownloadWSVO : resultList) {
                // 如果 detail 沒有身份證號的資料，表示查不到資料
                for (LiaRocDetailInfoWSVO detailInfo : liaRocDownloadWSVO.getList()) {
                    if (StringUtils.isNullOrEmpty(detailInfo.getCertiCode())) {
                        detailInfo.setNoDataFlag("Y");
                        for (LiaRocDownloadWSVO reqWSVO : wsIds) {
                            if (reqWSVO.getSn().equals(liaRocDownloadWSVO.getSn())) {
                                detailInfo.setCertiCode(reqWSVO.getList().get(0).getCertiCode());
                            }
                        }
                    }
                }
            }
            // end.

            returnResult = new ArrayList<LiaRocDownloadVO>();
            for (LiaRocDownloadWSVO reqWSVO : wsIds) {

                for (LiaRocDownloadWSVO resultWSVO : resultList) {

                    // 表示這個位置的查詢結果資料是該Id(被保人ID)的結果
                    if (reqWSVO.getSn().equals(resultWSVO.getSn())) {

                        // master table.
                        LiaRocDownloadVO liaRocDownloadVO = new LiaRocDownloadVO();
                        liaRocDownloadVO.setCertiCode(reqWSVO.getList().get(0).getCertiCode());
                        liaRocDownloadVO.setTransUuid(transId);
                        liaRocDownloadVO.setRequestTime(requestTime);
                        liaRocDownloadVO.setResponseTime(responseTime);
                        liaRocDownloadVO.setLiaRocDownloadStatus(LiaRocCst.LIAROC_DL_STATUS_SUCCESS);
                        liaRocDownloadVO.setNoDataFlag(CodeCst.YES_NO__NO);
                        liaRocDownloadVO.setFileName("");
                        liaRocDownloadVO.setDownloadDetails(new ArrayList<LiaRocDownloadDetailVO>());
                        liaRocDownloadVO.setPolicyId(policyId);

                        for (LiaRocDetailInfoWSVO resultInfo : resultWSVO.getList()) {

                            // 該ID查無資料
                            if (resultInfo.getNoDataFlag().equals("Y")) {
                                liaRocDownloadVO.setNoDataFlag(CodeCst.YES_NO__YES);
                            } else {
                                LiaRocDownloadDetailVO detailVO = new LiaRocDownloadDetailVO();
                                //PCR-243784 公會下傳回來ID若不足10碼TRIM空白 modify by Kathy
                                resultInfo.setCertiCode(org.apache.commons.lang.StringUtils.trimToEmpty(resultInfo.getCertiCode()));
                                resultInfo.setPhCertiCode(org.apache.commons.lang.StringUtils.trimToEmpty(resultInfo.getPhCertiCode()));
                                BeanUtils.copyProperties(detailVO, resultInfo);
                                
                                detailVO.setLiarocType(resultInfo.getLiaRocType());
                                detailVO.setLiarocCompanyCode(resultInfo.getLiaRocCompanyCode());
                                detailVO.setLiarocPolicyType(resultInfo.getLiaRocPolicyType());
                                detailVO.setLiarocProductCategory(resultInfo.getLiaRocProductCategory());
                                detailVO.setLiarocProductType(resultInfo.getLiaRocProductType());
                                detailVO.setLiarocChargeMode(resultInfo.getLiaRocChargeMode());
                                detailVO.setLiarocPolicyStatus(resultInfo.getLiaRocPolicyStatus());
                                detailVO.setLiarocRelationToPh(resultInfo.getLiaRocRelationToPh());
                                
                                liaRocDownloadVO.getDownloadDetails().add(detailVO);
                            }

                        }

                        liaRocDownloadService.save(liaRocDownloadVO);
                        returnResult.add(liaRocDownloadVO);

                        // 這筆ID處理完離開這層迴圈，繼續處理下一個ID的
                        break;

                    }

                }

            }

        } else {

            // 呼叫服務平台發生錯誤，把這些 id 寫入並且註記為下載失敗

            for (String id : ids) {
                LiaRocDownloadVO liaRocDownloadVO = new LiaRocDownloadVO();
                liaRocDownloadVO.setCertiCode(id);
                liaRocDownloadVO.setTransUuid(transId);
                liaRocDownloadVO.setRequestTime(requestTime);
                liaRocDownloadVO.setResponseTime(responseTime);
                liaRocDownloadVO.setLiaRocDownloadStatus(LiaRocCst.LIAROC_DL_STATUS_ERROR);
                liaRocDownloadVO.setNoDataFlag(CodeCst.YES_NO__YES);
                liaRocDownloadVO.setFileName("");
                liaRocDownloadVO.setDownloadDetails(new ArrayList<LiaRocDownloadDetailVO>());
                liaRocDownloadVO.setPolicyId(policyId);
                liaRocDownloadService.save(liaRocDownloadVO);
            }

            returnResult = null;

        }

        return returnResult;

    }

    /**
     * <p>Description : 測試用，用來模擬下載回傳的資料</p>
     * <p>Created By : Amy Hung</p>
     * <p>Create Time : May 21, 2016</p>
     * @return
     */
    public String mockRespXml() {

        StringBuilder builder = new StringBuilder();

        builder.append("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>");
        builder.append("<rs>");
        builder.append("  <rsHeader>");
        builder.append("    <total>2</total>");
        builder.append("    <success>1</success>");
        builder.append("    <fail>1</fail>");
        builder.append("  </rsHeader>");
        builder.append("  <rsBody>");
        builder.append("        <liaRocDownloadResponseRoot>");
        builder.append("            <header>");
        builder.append("                <fail>0</fail>");
        builder.append("                <success>10</success>");
        builder.append("                <total>10</total>");
        builder.append("            </header>");
        builder.append("            <body>");
        builder.append("    <id sn=\"1\">");
        builder.append("      <data sn=\"1\">");
        builder.append("        <cmptype>L</cmptype>");
        builder.append("        <cmpno>01</cmpno>");
        builder.append("        <name>張學有</name>");
        builder.append("        <idno>A12345789</idno>");
        builder.append("        <birdate>00530101</birdate>");
        builder.append("        <sex>1</sex>");
        builder.append("        <insno>360000001201</insno>");
        builder.append("        <class>1</class>");
        builder.append("        <inskind>1</inskind>");
        builder.append("        <insitem>01</insitem>");
        builder.append("        <itema>10</itema>");
        builder.append("        <itemb>20</itemb>");
        builder.append("        <itemc>30</itemc>");
        builder.append("        <itemd>40</itemd>");
        builder.append("        <iteme>50</iteme>");
        builder.append("        <itemf>60</itemf>");
        builder.append("        <itemg>70</itemg>");
        builder.append("        <itemh>80</itemh>");
        builder.append("        <itemi>90</itemi>");
        builder.append("        <itemj>100</itemj>");
        builder.append("        <itemk>110</itemk>");
        builder.append("        <iteml>120</iteml>");
        builder.append("        <itemm>130</itemm>");
        builder.append("        <itemn>140</itemn>");
        builder.append("        <itemo>150</itemo>");
        builder.append("        <itemp>160</itemp>");
        builder.append("        <valdate>01030101</valdate>");
        builder.append("        <ovrdate>01130101</ovrdate>");
        builder.append("        <prm>20000</prm>");
        builder.append("        <bamttype>1</bamttype>");
        builder.append("        <con>99</con>");
        builder.append("        <condate>01030101</condate>");
        builder.append("        <askname>張學量</askname>");
        builder.append("        <askidno>A123456781</askidno>");
        builder.append("        <askbirdate>00540301</askbirdate>");
        builder.append("        <asktype>05</asktype>");
        builder.append("        <adddate>01040329181500</adddate>");
        builder.append("        <message></message>");
        builder.append("        <askdate></askdate>");
        builder.append("        <kind></kind>");
        builder.append("        <instype></instype>");
        builder.append("        <source>12</source>");
        builder.append("      </data>");
        builder.append("      <data sn=\"2\">");
        builder.append("        <cmptype>L</cmptype>");
        builder.append("        <cmpno>02</cmpno>");
        builder.append("        <name>張學有</name>");
        builder.append("        <idno>A12345789</idno>");
        builder.append("        <birdate>00530101</birdate>");
        builder.append("        <sex>1</sex>");
        builder.append("        <insno>360000001201</insno>");
        builder.append("        <class>1</class>");
        builder.append("        <inskind>1</inskind>");
        builder.append("        <insitem>01</insitem>");
        builder.append("        <itema>1000</itema>");
        builder.append("        <itemb>2000</itemb>");
        builder.append("        <itemc>3000</itemc>");
        builder.append("        <itemd>4000</itemd>");
        builder.append("        <iteme>5000</iteme>");
        builder.append("        <itemf>6000</itemf>");
        builder.append("        <itemg>7000</itemg>");
        builder.append("        <itemh>8000</itemh>");
        builder.append("        <itemi>9000</itemi>");
        builder.append("        <itemj>10000</itemj>");
        builder.append("        <itemk>11000</itemk>");
        builder.append("        <iteml>12000</iteml>");
        builder.append("        <itemm>13000</itemm>");
        builder.append("        <itemn>14000</itemn>");
        builder.append("        <itemo>15000</itemo>");
        builder.append("        <itemp>16000</itemp>");
        builder.append("        <valdate>01050101</valdate>");
        builder.append("        <ovrdate>01050101</ovrdate>");
        builder.append("        <prm>3330000</prm>");
        builder.append("        <bamttype>1</bamttype>");
        builder.append("        <con>99</con>");
        builder.append("        <condate>01050101</condate>");
        builder.append("        <askname>張學量</askname>");
        builder.append("        <askidno>A123456781</askidno>");
        builder.append("        <askbirdate>01050101</askbirdate>");
        builder.append("        <asktype>05</asktype>");
        builder.append("        <adddate>01040329181500</adddate>");
        builder.append("        <message></message>");
        builder.append("        <askdate></askdate>");
        builder.append("        <kind></kind>");
        builder.append("        <instype></instype>");
        builder.append("        <source>12</source>");
        builder.append("      </data>");
        builder.append("    </id>");
        builder.append("    <id sn=\"2\">");
        builder.append("      <data sn=\"1\">");
        builder.append("        <message>查無資料</message>");
        builder.append("      </data>");
        builder.append("    </id>");

        builder.append("    <id sn=\"3\">");
        builder.append("      <data sn=\"1\">");
        builder.append("        <cmptype>L</cmptype>");
        builder.append("        <cmpno>01</cmpno>");
        builder.append("        <name>劉得華</name>");
        builder.append("        <idno>B987654321</idno>");
        builder.append("        <birdate>00401223</birdate>");
        builder.append("        <sex>1</sex>");
        builder.append("        <insno>111222333444</insno>");
        builder.append("        <class>1</class>");
        builder.append("        <inskind>1</inskind>");
        builder.append("        <insitem>01</insitem>");
        builder.append("        <itema>10</itema>");
        builder.append("        <itemb>20</itemb>");
        builder.append("        <itemc>30</itemc>");
        builder.append("        <itemd>40</itemd>");
        builder.append("        <iteme>50</iteme>");
        builder.append("        <itemf>60</itemf>");
        builder.append("        <itemg>70</itemg>");
        builder.append("        <itemh>80</itemh>");
        builder.append("        <itemi>90</itemi>");
        builder.append("        <itemj>100</itemj>");
        builder.append("        <itemk>110</itemk>");
        builder.append("        <iteml>120</iteml>");
        builder.append("        <itemm>130</itemm>");
        builder.append("        <itemn>140</itemn>");
        builder.append("        <itemo>150</itemo>");
        builder.append("        <itemp>160</itemp>");
        builder.append("        <valdate>01030101</valdate>");
        builder.append("        <ovrdate>01130101</ovrdate>");
        builder.append("        <prm>20000</prm>");
        builder.append("        <bamttype>1</bamttype>");
        builder.append("        <con>99</con>");
        builder.append("        <condate>01030101</condate>");
        builder.append("        <askname>劉得華</askname>");
        builder.append("        <askidno>A123456781</askidno>");
        builder.append("        <askbirdate>00540301</askbirdate>");
        builder.append("        <asktype>05</asktype>");
        builder.append("        <adddate>01040329181500</adddate>");
        builder.append("        <message></message>");
        builder.append("        <askdate></askdate>");
        builder.append("        <kind></kind>");
        builder.append("        <instype></instype>");
        builder.append("        <source>12</source>");
        builder.append("      </data>");
        builder.append("      <data sn=\"2\">");
        builder.append("        <cmptype>L</cmptype>");
        builder.append("        <cmpno>02</cmpno>");
        builder.append("        <name>劉得華</name>");
        builder.append("        <idno>B987654321</idno>");
        builder.append("        <birdate>00530101</birdate>");
        builder.append("        <sex>1</sex>");
        builder.append("        <insno>360000001201</insno>");
        builder.append("        <class>1</class>");
        builder.append("        <inskind>1</inskind>");
        builder.append("        <insitem>01</insitem>");
        builder.append("        <itema>1000</itema>");
        builder.append("        <itemb>2000</itemb>");
        builder.append("        <itemc>3000</itemc>");
        builder.append("        <itemd>4000</itemd>");
        builder.append("        <iteme>5000</iteme>");
        builder.append("        <itemf>6000</itemf>");
        builder.append("        <itemg>7000</itemg>");
        builder.append("        <itemh>8000</itemh>");
        builder.append("        <itemi>9000</itemi>");
        builder.append("        <itemj>10000</itemj>");
        builder.append("        <itemk>11000</itemk>");
        builder.append("        <iteml>12000</iteml>");
        builder.append("        <itemm>13000</itemm>");
        builder.append("        <itemn>14000</itemn>");
        builder.append("        <itemo>15000</itemo>");
        builder.append("        <itemp>16000</itemp>");
        builder.append("        <valdate>01050101</valdate>");
        builder.append("        <ovrdate>01050101</ovrdate>");
        builder.append("        <prm>3330000</prm>");
        builder.append("        <bamttype>1</bamttype>");
        builder.append("        <con>99</con>");
        builder.append("        <condate>01050101</condate>");
        builder.append("        <askname>劉得華</askname>");
        builder.append("        <askidno>A123456781</askidno>");
        builder.append("        <askbirdate>01050101</askbirdate>");
        builder.append("        <asktype>05</asktype>");
        builder.append("        <adddate>01040329181500</adddate>");
        builder.append("        <message></message>");
        builder.append("        <askdate></askdate>");
        builder.append("        <kind></kind>");
        builder.append("        <instype></instype>");
        builder.append("        <source>12</source>");
        builder.append("      </data>");
        builder.append("    </id>");
        builder.append("            </body>");
        builder.append("        </liaRocDownloadResponseRoot>");
        builder.append("  </rsBody>");
        builder.append("</rs>");

        return builder.toString();

    }

//    public List<LiaRocDownloadWSVO> mockLiaRocDownloadWSVO() {
//
//        ReqXml<LiaRocDownloadWSVO> rqBody = JaxbTool.<ReqXml<LiaRocDownloadWSVO>> toObject(ReqXml.class,
//                        this.mockRespXml());
//        List<LiaRocDownloadWSVO> list = rqBody.getRqBody().getList();
//
//        return list;
//
//    }

    @Override
    public List<AggregationResult> calcRA306(ProposalVO proposal) {
        return this.calcRiskAggrResult(LiaRocCst.RISK_AGGR_CALC_RA306, proposal, null);
    }

    @Override
    public List<AggregationResult> calcRA319(ProposalVO proposal) {
        return this.calcRiskAggrResult(LiaRocCst.RISK_AGGR_CALC_RA319, proposal, null);
    }

    @Override
    public List<AggregationResult> calcRA325(ProposalVO proposal) {
        return this.calcRiskAggrResult(LiaRocCst.RISK_AGGR_CALC_RA325, proposal, null);
    }

    @Override
    public List<AggregationResult> calcRA327(ProposalVO proposal) {
        return this.calcRiskAggrResult(LiaRocCst.RISK_AGGR_CALC_RA327, proposal, null);
    }

    @Override
    public List<AggregationResult> calcRA330(ProposalVO proposal) {
        return this.calcRiskAggrResult(LiaRocCst.RISK_AGGR_CALC_RA330, proposal, null);
    }

    @Override
    public List<AggregationResult> calcRA346(ProposalVO proposal) {
        return this.calcRiskAggrResult(LiaRocCst.RISK_AGGR_CALC_RA346, proposal, null);
    }

    @Override
    public List<AggregationResult> calcRA369(ProposalVO proposal, Date dateBfEffectiveDate) {
        return this.calcRiskAggrResult(LiaRocCst.RISK_AGGR_CALC_RA369, proposal, dateBfEffectiveDate);
    }

    @Override
    public List<AggregationResult> calcRA373(ProposalVO proposal) {
        return this.calcRiskAggrResult(LiaRocCst.RISK_AGGR_CALC_RA373, proposal, null);
    }

    @Override
    public List<AggregationResult> calcRA384(ProposalVO proposal) {
        return this.calcRiskAggrResult(LiaRocCst.RISK_AGGR_CALC_RA384, proposal, null);
    }
    
    @Override
    public List<AggregationResult> calcRA390(ProposalVO proposal) {
        return this.calcRiskAggrResult(LiaRocCst.RISK_AGGR_CALC_RA390, proposal, null);
    }

    /**
     * <p>Description : 風險累積計算共用接口，因為處理邏輯很像，所以寫成共用的</p>
     * <p>Created By : Amy Hung</p>
     * <p>Create Time : Apr 29, 2016</p>
     * @param riskType 傳入各風險累積接口相應的 RiskType
     * @param proposal ProposalVO
     * @param dateBfEffectiveDate 只有同業傷害醫療險346接口才會傳入值，其它傳入 null
     * @return
     */
    private List<AggregationResult> calcRiskAggrResult(Integer riskType, ProposalVO proposal, Date dateBfEffectiveDate) {

        List<AggregationResult> aggrList = new ArrayList<AggregationResult>();

        // 先下載更新公會資料
        try {

            Set<String> ids = new LinkedHashSet<String>();
            for (InsuredVO vo : proposal.getInsureds()) {
                ids.add(vo.getCertiCode());
            }

            // this.send(ids);

        } catch (Exception e) {
            // do nothing.
        }
        // end.

        // 逐一累計被保人的風險累積資料
        Long policyId = proposal.getPolicyChgId()== null?proposal.getPolicyId():null;
        for (InsuredVO vo : proposal.getInsureds()) {

            BigDecimal riskLAmmout = new BigDecimal(0);
            BigDecimal riskRAmmout = new BigDecimal(0);
            String certiCode = vo.getCertiCode();
            
            NBUtils.logger(this.getClass(),"calcRiskAggrResult riskType=" + riskType + ",certiCode=" + certiCode + " begin");
			
            switch (riskType) {
                case 77: //同業醫療住院日額306
                    riskLAmmout = liaRocDownloadDao.queryRA306Amount(certiCode,
                                    LiaRocCst.INFORCE_LIAROC_TYPE_LIFE, dateBfEffectiveDate, policyId);
                    riskRAmmout = liaRocDownloadDao.queryRA306Amount(certiCode,
                                    LiaRocCst.RECEIVE_LIAROC_TYPE_LIFE, dateBfEffectiveDate, policyId);
                    break;
                case 78: //同業壽險傷害險319
                    riskLAmmout = liaRocDownloadDao.queryRA319Amount(certiCode,
                                    LiaRocCst.INFORCE_LIAROC_TYPE_LIFE, policyId);
                    riskRAmmout = liaRocDownloadDao.queryRA319Amount(certiCode,
                                    LiaRocCst.RECEIVE_LIAROC_TYPE_LIFE, policyId);
                    break;
                case 79: //同業壽險傷害險325
                    riskLAmmout = liaRocDownloadDao.queryRA325Amount(certiCode,
                                    LiaRocCst.INFORCE_LIAROC_TYPE_LIFE, policyId);
                    riskRAmmout = liaRocDownloadDao.queryRA325Amount(certiCode,
                                    LiaRocCst.RECEIVE_LIAROC_TYPE_LIFE, policyId);
                    break;
                case 80: //同業壽險傷害險327
                    riskLAmmout = liaRocDownloadDao.queryRA327Amount(certiCode,
                                    LiaRocCst.INFORCE_LIAROC_TYPE_LIFE, policyId);
                    riskRAmmout = liaRocDownloadDao.queryRA327Amount(certiCode,
                                    LiaRocCst.RECEIVE_LIAROC_TYPE_LIFE, policyId);
                    break;
                case 81: //同業壽險傷害險330
                    riskLAmmout = liaRocDownloadDao.queryRA330Amount(certiCode,
                                    LiaRocCst.INFORCE_LIAROC_TYPE_LIFE, policyId);
                    riskRAmmout = liaRocDownloadDao.queryRA330Amount(certiCode,
                                    LiaRocCst.RECEIVE_LIAROC_TYPE_LIFE, policyId);
                    break;
                case 82: //同業傷害醫療險346
                    riskLAmmout = liaRocDownloadDao.queryRA346Amount(certiCode,
                                    LiaRocCst.INFORCE_LIAROC_TYPE_LIFE, policyId);
                    riskRAmmout = liaRocDownloadDao.queryRA346Amount(certiCode,
                                    LiaRocCst.RECEIVE_LIAROC_TYPE_LIFE, policyId);
                    break;
                case 83: //同業傷害微型險373
                    riskLAmmout = liaRocDownloadDao.queryRA373Amount(certiCode,
                                    LiaRocCst.INFORCE_LIAROC_TYPE_LIFE, policyId);
                    riskRAmmout = liaRocDownloadDao.queryRA373Amount(certiCode,
                                    LiaRocCst.RECEIVE_LIAROC_TYPE_LIFE, policyId);
                    break;
                case 121: //同業傷害微型險384
                    riskLAmmout = liaRocDownloadDao.queryRA384Amount(certiCode,
                                    LiaRocCst.INFORCE_LIAROC_TYPE_LIFE, policyId);
                    riskRAmmout = liaRocDownloadDao.queryRA384Amount(certiCode,
                                    LiaRocCst.RECEIVE_LIAROC_TYPE_LIFE, policyId);
                    break;
                case 135: //同業小額終老保險390
                    riskLAmmout = liaRocDownloadDao.queryRA390Amount(certiCode,
                                    LiaRocCst.INFORCE_LIAROC_TYPE_LIFE, policyId);
                    riskRAmmout = liaRocDownloadDao.queryRA390Amount(certiCode,
                                    LiaRocCst.RECEIVE_LIAROC_TYPE_LIFE, policyId);
                    break;     
            }
            
            riskLAmmout = ((riskLAmmout == null) ? BigDecimal.ZERO : riskLAmmout);
            riskRAmmout = ((riskRAmmout == null) ? BigDecimal.ZERO : riskRAmmout);

            NBUtils.logger(this.getClass(),"calcRiskAggrResult riskType=" + riskType + ",certiCode=" + certiCode + " end");
            NBUtils.logger(this.getClass(),"calcRiskAggrResult riskType=" + riskType + ",certiCode=" + certiCode 
            				+ ",承保累額=" + riskLAmmout + ",收件累額=" + riskRAmmout);
            
            // 承保
            AggregationResult aggrL = new AggregationResult();
            aggrL.setPolicyId(policyId);
            aggrL.setPartyId(vo.getPartyId());
            aggrL.setAggregationCategory(2);
            aggrL.setRiskType(riskType);
            aggrL.setRiskAmount(riskLAmmout);
            aggrL.setCertiCode(certiCode);
            aggrList.add(aggrL);

            // 收件
            AggregationResult aggrR = new AggregationResult();
            aggrR.setPolicyId(policyId);
            aggrR.setPartyId(vo.getPartyId());
            aggrR.setAggregationCategory(1);
            aggrR.setRiskType(riskType);
            aggrR.setRiskAmount(riskRAmmout);
            aggrR.setCertiCode(certiCode);
            aggrList.add(aggrR);

        }

        return aggrList;

    }

    public int getLiarocCaseNumByCertiCode(boolean isPOS,Long policyId, String certiCode) {
        return liaRocDownloadDao.getLiarocCaseNumByCertiCode(isPOS?null:policyId, certiCode);
    }

    @Override
    public List<LiaRocDownloadDetail> findGroupingReceiveDetailList(String certiCode) {
        String liarocType = "R";
        String orderBy = "list_id";
        boolean isOrderByAsc = true;

        return liaRocDownloadDetailDao.findDownloadDetailDataByCriteria(certiCode, liarocType, orderBy, isOrderByAsc);
    }

    @Override
    public List<LiaRocDownloadDetail> findGroupingIssueDetailList(String certiCode) {
        String liarocType = "L";
        String orderBy = "list_id";
        boolean isOrderByAsc = true;

        return liaRocDownloadDetailDao.findDownloadDetailDataByCriteria(certiCode, liarocType, orderBy, isOrderByAsc);
    }

    /**
     * <p>Description : 查詢保單公會下載次數</p>
     * <p>Created By : simon.huang</p>
     * <p>Create Time : Oct 20, 2017</p>
     * @param policyId
     * @return
     */
    public int countPolicyDownloadCount(Long policyId){
    	return liaRocDownloadDao.countPolicyDownloadCount(policyId);
    }

    @Override
    public void saveLiaRocDoanloadLog(String certiCode, String name,
    				String policyCode, String queryFunction) {
    	liaRocDownloadLogDao.saveLiaRocDoanloadLog(certiCode, name, policyCode, queryFunction);
    }

	@Override
	public Map<String, Long> getOtherTotalAnnPremByPolicyId(Long policyId) {
		return liaRocDownloadService.getOtherTotalAnnPremByPolicyId(policyId);
	}

	@Override
	public Map<String, Long> getCompTotalAnnPremByPolicyId(Long policyId) {
		return liaRocDownloadService.getCompTotalAnnPremByPolicyId(policyId);
	}

	@Override
	public List<Map<String, Object>> findGroupingIssueDetailData(String certiCode, Long policyId) {
		return liaRocDownloadDao.findGroupingIssueDetailData(certiCode, policyId);
	}

	@Override
	public List<Map<String, Object>> findGroupLiaRocDetailData(String certiCode, String type, Long policyId) {
		return liaRocDownloadService.findGroupLiaRocDetailData(certiCode, type, policyId);
	}

	@Override
	public List<Map<String, Object>> findGroupingReceiveDetailData(String certiCode, Long policyId) {
		return liaRocDownloadDao.findGroupingReceiveDetailData(certiCode, policyId);
	}

	@Override
	public Long queryIssueAnnuPrem(String certiCode, String companyCode, Long policyId) {
		return liaRocDownloadDao.queryIssueAnnuPrem(certiCode, companyCode, policyId);
	}

	@Override
	public Long queryReceiveAnnuPrem(String certiCode, String companyCode, Long policyId) {
		return liaRocDownloadDao.queryReceiveAnnuPrem(certiCode, companyCode, policyId);
	}

	@Override
	public Integer queryDownloadRecordCount(String certiCode, Long policyId) {
		return liaRocDownloadDao.queryDownloadRecordCount(certiCode, policyId);
	}

	@Override
	public List<Map<String, Object>> findGroupingReceiveDetailLog(String certiCode, Long policyId) {
		return liaRocDownloadDao.findGroupingReceiveDetailLog(certiCode, policyId);
	}

	@Override
	public List<Map<String, Object>> findGroupingIssueDetailLog(String certiCode, Long policyId) {
		return liaRocDownloadDao.findGroupingIssueDetailLog(certiCode, policyId);
	}

	@Override
	public List<Map<String, Object>> findTerminateDetailData(Long policyId) {
		//抓取code name呼叫原dao方法即可
		return liaRocDownloadDao.findTerminateIssueDetailData(policyId);
	}

	@Override
	public List<LiaRocPaidInjuryMedicalDetailVO> findLiarocPaidInjuryMedicalDetailInfo(Long policyId) {
		return liaRocDownloadService.findLiarocPaidInjuryMedicalDetailInfo(policyId);
	}

	@Override
	public List<Map<String, Object>> findECTerminateDetailData(Long policyId, Date applyDate, String ecCertiCode) {
		return liaRocDownloadDao.findECTerminateIssueDetailData(policyId, applyDate, ecCertiCode);
	}

	@Override
	public String queryDownloadRecordStatus(String policyStatus) {
		return liaRocDownloadDao.queryDownloadRecordStatus(policyStatus);
	}

	@Override
	public List<Map<String, Object>> findPayAsYouGoMedNInjDetailData(String certiCode, Long policyId) {
		return liaRocDownloadDao.findPayAsYouGoMedNInjDetailData(certiCode, policyId);
	}

	/**
	 * <p>Description : 被保險人近三個月公會通報資料</p>
	 * <p>Created By : simon.huang</p>
	 * <p>Create Time : Jul 30, 2016</p>
	 * @param ceriCode 被保險人身份證號
	 * @return 近三個月公會通報資料總數
	 */
	public List<String> getThreeMonthCount(Long policyId, String certiCode) {
		return liaRocDownloadDao.getThreeMonthCount(policyId, certiCode);
	}
	
	/**
	 * <p>Description : PCR-392013_BC398_:查詢被保人投保了幾項符合有喪葬費用保險金之商品</p>
	 * <p>Created By : Kathy.Yeh</p>
	 * <p>Create Time : 2020年6月30日</p>
	 * @param policyId
	 * @param certiCode
	 * @param law107EffectDate1
	 * @param law107EffectDate2
	 * @return
	 */
	public int getLiaroc107LawCount(Long policyId, String certiCode,String  law107EffectDate1, String law107EffectDate2) {
		return liaRocDownloadDao.getLiaroc107LawCount(policyId, certiCode, law107EffectDate1, law107EffectDate2);
	}

	@Override
	public int getLiarocProdType16NumByCertiCode(Long policyId, String certiCode) {
		return liaRocDownloadDao.getLiarocProdType16NumByCertiCode(policyId, certiCode);
	}

	@Override
	public Map<String, Integer> getPayAsYouGoMedical(String certiCode, Long policyId) {
		return liaRocDownloadDao.getPayAsYouGoMedical(certiCode, policyId);
	}

	@Override
	public BigDecimal queryRA327Amount(String certiCode, String liarocType, Long policyId) {
		return liaRocDownloadDao.queryRA327Amount(certiCode, liarocType, policyId);
	}

	@Override
	public List<LiaRocDownloadDetailVO> queryLiarocDownloadDetailList(String certiCode) {
		return liaRocDownloadDetailDao.queryDownloadDetailByCertiCode(certiCode);
	}	
}
