package com.ebao.ls.uw.ctrl.uwIssues;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.UserTransaction;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.ClassUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ebao.foundation.module.db.hibernate.HibernateSession3;
import com.ebao.foundation.module.web.action.ExceptionInfoUtils;
import com.ebao.ls.chl.service.ChannelOrgService;
import com.ebao.ls.chl.vo.ChannelOrgVO;
import com.ebao.ls.cmu.biz.vo.IssuesDocContentVO;
import com.ebao.ls.cmu.bo.pub.DocCriteriaVO;
import com.ebao.ls.cmu.bs.document.DocumentManagementService;
import com.ebao.ls.cmu.bs.document.IssuesDocVO;
import com.ebao.ls.cmu.pub.document.DocumentFormFile;
import com.ebao.ls.cmu.pub.document.FormFileWrapper;
import com.ebao.ls.cmu.pub.vo.DocumentVO;
import com.ebao.ls.cs.common.report.CSReportService;
import com.ebao.ls.cs.commonflow.ctrl.letter.CsUWIssuesActionHelper;
import com.ebao.ls.cs.commonflow.ctrl.letter.UWIssuesForm;
import com.ebao.ls.cs.commonflow.ds.ApplicationService;
import com.ebao.ls.cs.logger.ApplicationLogger;
import com.ebao.ls.cs.logger.helper.AppLogDataHelper;
import com.ebao.ls.escape.helper.EscapeHelper;
import com.ebao.ls.notification.ci.NbEventCI;
import com.ebao.ls.notification.extension.letter.nb.FmtUnb0370ExtensionVO;
import com.ebao.ls.notification.extension.letter.nb.FmtUnb0373ExtensionVO;
import com.ebao.ls.notification.extension.letter.nb.NbLetterExtensionVO;
import com.ebao.ls.notification.extension.letter.nb.index.FmtUnb0370IndexVO;
import com.ebao.ls.notification.extension.letter.nb.index.FmtUnb0373IndexVO;
import com.ebao.ls.notification.message.WSExtendLetterIndex;
import com.ebao.ls.notification.message.WSItemString;
import com.ebao.ls.notification.message.WSLetterSendingMethod;
import com.ebao.ls.notification.message.WSLetterUnit;
import com.ebao.ls.pa.nb.bs.rule.ProposalRuleResultService;
import com.ebao.ls.pa.nb.ci.ProposalCI;
import com.ebao.ls.pa.nb.ctrl.helper.RemoteHelper;
import com.ebao.ls.pa.nb.ctrl.letter.NbLetterHelper;
import com.ebao.ls.pa.nb.ctrl.letter.NbNotificationHelper;
import com.ebao.ls.pa.nb.ctrl.pub.ajax.AjaxBaseService;
import com.ebao.ls.pa.nb.util.UnbCst;
import com.ebao.ls.pa.nb.vo.ProposalRuleResultVO;
import com.ebao.ls.pa.pub.auth.UwIssuesAuthServiceImpl;
import com.ebao.ls.pa.pub.bs.PolicyService;
import com.ebao.ls.pa.pub.bs.coverage.CoverageService;
import com.ebao.ls.pa.pub.vo.CoverageAgentVO;
import com.ebao.ls.pa.pub.vo.CoverageVO;
import com.ebao.ls.pa.pub.vo.PolicyVO;
import com.ebao.ls.pa.pub.vo.UwSickFormItemVO;
import com.ebao.ls.pa.pub.vo.UwSickFormLetterVO;
import com.ebao.ls.pty.ci.DeptCI;
import com.ebao.ls.pub.CodeCst;
import com.ebao.ls.pub.auth.AuthItemCst;
import com.ebao.ls.pub.cst.Template;
import com.ebao.ls.uw.ctrl.letter.UNBLetterAction;
import com.ebao.ls.uw.ds.UwIssuesLetterService;
import com.ebao.ls.uw.ds.UwMedicalLetterService;
import com.ebao.ls.uw.ds.UwPolicyService;
import com.ebao.ls.uw.ds.UwSickFormItemService;
import com.ebao.ls.uw.ds.UwSickFormLetterService;
import com.ebao.ls.uw.ds.vo.UwIssueMedicalItemVO;
import com.ebao.ls.uw.ds.vo.UwProposalRuleResultVO;
import com.ebao.ls.uw.vo.UwPolicyVO;
import com.ebao.pub.event.ods.FormattingMethod;
import com.ebao.pub.fmt.dao.FmtCommonConstant;
import com.ebao.pub.fmt.util.FmtCommonCI;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.framework.ExceptionFactory;
import com.ebao.pub.framework.GenericException;
import com.ebao.pub.i18n.lang.CodeTable;
import com.ebao.pub.util.BeanUtils;
import com.ebao.pub.util.DateUtils;
import com.ebao.pub.util.StringUtils;
import com.ebao.pub.util.Trans;
import com.ebao.pub.util.TransUtils;
import com.ebao.pub.util.tiff.TIFConvertPDF;

import edu.emory.mathcs.backport.java.util.Collections;
import net.sf.json.JSONObject;

/**
 * <p>Title: 核保問題頁面Action</p>
 * <p>Description: 核保問題頁面Action</p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company: TGL Co., Ltd.</p>
 * <p>Create Time: Feb 19, 2016</p> 
 * @author 
 * <p>Update Time: Feb 19, 2016</p>
 * <p>Updater: Alex Cheng</p>
 * <p>Update Comments: 2016-11-29 Kate Hsiao 增加保全規則校驗邏輯 </p>
 */
public class UWIssueAction extends UNBLetterAction {
	public static final String BEAN_DEFAULT = "/uw/uwIssuesLog";

	protected final static String SUBACTION__QUERY_ISSUES = "queryIssues";
	
	protected final static String SUBACTION__BATCH_PRINT = "batchPrint";
	
	protected final static String SUBACTION__ONLINE_PRINT = "onlinePrint";
	
	protected final static String SUBACTION__PREVIEW_PRINT = "previewPrint";
	
	protected final static String BATCH_PRINT__SUCCESS = "MSG_1257540";
	
	protected final static String ONLINE_PRINT__SUCCESS = "MSG_1257541";
	
	protected final static String PRINT__ERROR = "MSG_1257542";
	
	/** FMT_UNB_0370 核保照會通知單  */
	protected final static long TEMPLATE_ID = Template.TEMPLATE_20021.getTemplateId();
	
	/** FMT_UNB_0373核保照會通知單-網路投保  */
	protected final static long TEMPLATE_ID_EC = Template.TEMPLATE_20039.getTemplateId();

	private static Map<String, String> msgSubDoc = new LinkedHashMap<String, String>();

	private static final Logger log = LoggerFactory.getLogger(UWIssueAction.class);
	
	static {
		msgSubDoc.put(CodeCst.PROPOSAL_RULE_MSG_UW_ADDINFORM_LETTER, "T_UW_ADDINFORM_LETTER");
		msgSubDoc.put(CodeCst.PROPOSAL_RULE_MSG_EXCLUSION_LETTER, "T_UW_EXCLUSION");
		msgSubDoc.put(CodeCst.PROPOSAL_RULE_MSG_EXTRA_PREM_LETTER, "T_UW_EXTRA_PREM");
		msgSubDoc.put(CodeCst.PROPOSAL_RULE_MSG_MEDICAL_LETTER, "T_UW_MEDICAL_LETTER");
		msgSubDoc.put(CodeCst.PROPOSAL_RULE_MSG_SICK_FORM, "T_UW_SICK_FORM_ITEM");
		msgSubDoc.put(CodeCst.PROPOSAL_RULE_MSG_INSURED_SICK, "T_UW_SICK_FORM_ITEM");
		msgSubDoc.put(CodeCst.PROPOSAL_RULE_MSG_LIFE_SURVEY, "T_UW_SURVEY_LET");
	}

	/**
	 * Action進入點
	 */
	@Override
	public ActionForward process(ActionMapping mapping, ActionForm form,
					HttpServletRequest request, HttpServletResponse response)
					throws GenericException {

		try {
			/* 取得是否有個案融通權限 */
			boolean isAccommodativeRole = uwIssuesAuthService.hasAccessRight(AuthItemCst.AUTH_ITEM_UW_ISSUES);
			IssuesForm aForm = (IssuesForm) form;
			String subAction = aForm.getSubAction();
			aForm.setIsAccommodativeRole(isAccommodativeRole);
			String forward = "uwissues";
			
			isCsUw(aForm, request);
			
			// ApplicationLogger Setting
	        ApplicationLogger.setJobName(ClassUtils.getShortClassName(this.getClass()));
	        ApplicationLogger.setOptionId(DateUtils.date2String(new Date(), "yyyyMMdd HHmmss"));
	        ApplicationLogger.setPolicyCode(aForm.getPolicyCode());
	        if (aForm.isCsUw()) {
                ApplicationLogger.setSystemCode(AppLogDataHelper.SYSTEM_CODE_POS);
	        } else {
                ApplicationLogger.setSystemCode(AppLogDataHelper.SYSTEM_CODE_UNB);
	        }
			
	        
			if(StringUtils.isNullOrEmpty(subAction) || SUBACTION__QUERY_ISSUES.equals(subAction)){
				this.queryIssues(aForm, request);
				forward = "uwissues";
				aForm.setRetMsg(null);
			}else if(SUBACTION__PREVIEW_PRINT.equals(subAction)){
				if(aForm.isCsUw()){
					UWIssuesForm csForm = new UWIssuesForm();
					BeanUtils.copyProperties(csForm, aForm);
					csUWIssuesActionHelper.preViewPrint(csForm, request, response);
				}else{
					UserTransaction trans = Trans.getUserTransaction();
					try {
						trans.begin();
						this.updateProposalRuleResult(aForm);
						this.preViewPrint(aForm, request, response);
						trans.commit();
					} catch (Exception e) {
						TransUtils.rollback(trans);
						log.error(ExceptionInfoUtils.getExceptionMsg(e));
						//無法產生預覽PDF。
						aForm.setRetMsg("MSG_1262038");
					}
				}
				
				return mapping.findForward(FORWARD_WINDOW_CLOSE);
			} else {
				UserTransaction trans = Trans.getUserTransaction();
				try {
					if(SUBACTION__BATCH_PRINT.equals(subAction)){
						trans.begin();
						this.updateProposalRuleResult(aForm);
						this.batchPrint(aForm, request,response);
						trans.commit();
						aForm.setRetMsg(BATCH_PRINT__SUCCESS);
						forward = "uwissues";
					}else if(SUBACTION__ONLINE_PRINT.equals(subAction)){
						trans.begin();
						this.updateProposalRuleResult(aForm);
						if(aForm.isCsUw()){
							UWIssuesForm csForm = new UWIssuesForm();
							BeanUtils.copyProperties(csForm, aForm);
							csUWIssuesActionHelper.onlinePrint(csForm, request);
						}else{
							this.onlinePrint(aForm, request, response);
						}
						aForm.setRetMsg(ONLINE_PRINT__SUCCESS);
						trans.commit();
						forward = "uwissues";
					}
				} catch (Exception e) {
					TransUtils.rollback(trans);
					log.error(ExceptionInfoUtils.getExceptionMsg(e));
					if(SUBACTION__ONLINE_PRINT.equals(subAction) 
							|| SUBACTION__BATCH_PRINT.equals(subAction)){
						aForm.setRetMsg(PRINT__ERROR);
					} else {
						throw ExceptionFactory.parse(e);	
					}
				}
				this.queryIssues(aForm, request);
			}
			
			return mapping.findForward(forward);
		} catch (Exception e) {
		    
		    String message = ExceptionInfoUtils.getExceptionMsg(e);
            int len = message.length();
            if (len > 0) {
                ApplicationLogger.addLoggerData("[ERROR]Exception => " + message.substring(0, (len > 2000 ? 2000 : len)));
            } else {
                ApplicationLogger.addLoggerData("[ERROR]Exception => " + e);
            }
		    
			throw ExceptionFactory.parse(e);
			
		} finally {
		    try {
                ApplicationLogger.flush();
            } catch (Exception e1) {
                log.warn(e1.getMessage());
            }
		}
	}

	/**
	 * <p>Description : 更新核保問題資料 </p>
	 * <p>Created By : Alex Cheng</p>
	 * <p>Create Time : Mar 13, 2016</p>
	 * @param issuesForm
	 */
	@SuppressWarnings("rawtypes")
	private void updateProposalRuleResult(IssuesForm issuesForm){
		
		String uwRuleObjStr = issuesForm.getUwRuleObjStr(); 
		
		if(!StringUtils.isNullOrEmpty(uwRuleObjStr)){
			Long checkListId = null;
			String letterContent = null;
			Map<String, Object> dataMap = AjaxBaseService.parseJSON2Map(uwRuleObjStr);
			for(Entry<String, Object> entry : dataMap.entrySet()) {
			    String key = entry.getKey();
			    Map value = (Map)entry.getValue();
			    checkListId = new Long(key);
				letterContent = MapUtils.getString(value, "letterContent", "");
				proposalRuleResultService.updateProposalRuleResult(checkListId, letterContent, null );
			}
		}
	}
	
	/**
	 * <p>Description : Batch發送</p>
	 * <p>Created By : Alex Cheng</p>
	 * <p>Create Time : Mar 8, 2016</p>
	 * @param aForm
	 * @param request
	 */
	private void batchPrint(IssuesForm aForm, HttpServletRequest request, HttpServletResponse response) throws Exception {
		 Long policyId = aForm.getPolicyId();
		 String policyCode = policyService.getPolicyCodeByPolicyId(policyId);
		 
		 //於prepareExtensionVO()中執行uwIssueId的排序
		 FmtUnb0370ExtensionVO vo = prepareExtensionVO(aForm, request);
		 
		 //依送件管道轉型成對應的VO(20221012 IrisLiu, PCR-499927-BC456_保障型平台微型保險)
		 vo = transExtensionVO(aForm, vo);
		 
		 List<DocumentFormFile> uploadFileList = nbLetterHelper.getUploadFileList(request, aForm);
		 // PCR-370827 e-Approval ITR-2000946_核保員由照會夾寄PDF檔,導致元鎂產製照會單失敗改善方案
		 uploadFileList = convertPdfToTiff(uploadFileList, vo.getTemplateId(), vo.getPolicyCode());
		 WSLetterUnit unit = nbLetterHelper.generalLetterUnit(   
						 response,
                         policyId, 
                         vo.getTemplateId(), 
                         uploadFileList,
                         aForm.getApplyImageList(),
                         vo);	
		 
		 //依送件管道於WSLetterUnit設定相關資料(20221012 IrisLiu, PCR-499927-BC456_保障型平台微型保險)
		 setIndexVO(aForm, vo, unit); //設定信函index
		 setSendInfo(aForm, unit); //設定信函寄送規則
			
		 /**取得今天的 batch 核保照會documentId**/
		 /** batch列印要先將舊的照會刪除**/
		 DocumentVO doc = getCurrentDocumentId(vo.getTemplateId(), policyId);
		 Long docId = (doc != null ? doc.getListId() : null);
		 if ( docId != null) {
			 /**清除 t_outstanding_issues_doc..**/
			 cleanOutstandingIssuesDoc(docId);
			 /**將batch發的附照會狀態改為刪除**/
			 deleteSubDocs(docId);			 
			 documentService.updateLetterStatus(docId, CodeCst.LETTER_STATUS_DELETED);
		 }
		 /**取得附加照會(重新產生所有附照會..)**/
		 List<Long> subDocs = null;
		 try {
			subDocs = uwIssuesLetterService.getSubDocList(vo,
					vo.getIssuelistId(),policyId,aForm.getUnderwriteId());
		 } catch (Exception e) {
			throw ExceptionFactory.parse(e);
		 }
		 if ( subDocs == null || subDocs.isEmpty()) {
			 docId = commonLetterService.sendBatchLetter(policyId, vo.getTemplateId(), unit, policyId, policyCode, null);
		 } else {
			 /**若有附件則要合併發送照會**/
			 docId = commonLetterService.sendBatchLetter(policyId, vo.getTemplateId(), unit, policyId, policyCode, null);
			 /**修改主照會XML**/
			 commonLetterService.updateBatchGroupLetter(docId, subDocs);
		 }
		 createOutstandingIssueDoc(vo.getIssuelistId(), docId);

	}

	/**將batch發的附照會狀態改為刪除**/
	private void deleteSubDocs(Long docId) {
		List<Long> ids = uwIssuesLetterService.getGoupDocIds(docId);
		if ( ids != null && !ids.isEmpty()) {
			commonLetterService.removeGroup(ids);
			for(Long id:ids) {
				if ( !id.equals(docId)) {
					documentService.updateLetterStatus(id, CodeCst.LETTER_STATUS_DELETED);
				}
			}
		}
	}

	/** 取得今天的 batch 核保照會documentId**/
	private DocumentVO getCurrentDocumentId(Long templateId, Long policyId) {
		DocCriteriaVO docCriteriaVO = new DocCriteriaVO();
		docCriteriaVO.setPolicyId(policyId);
		docCriteriaVO.setTemplateId(templateId.intValue());
		docCriteriaVO.setFormattingMethod(FormattingMethod.BATCH.toString());
		docCriteriaVO.setDocumentStatus(CodeCst.DOCUMENT_STATUS__PRINTED);
		
		List<DocumentVO> docs = documentManagementService.queryDocumentList(docCriteriaVO);
		Date today = DateUtils.truncateDay(AppContext.getCurrentUserLocalTime());
		for(DocumentVO doc : docs) {
			if ( doc.getPrintTime() != null && DateUtils.truncateDay(doc.getPrintTime()).equals(today) ) {
				return doc;
			}
		}
		return null;
	}
	

	/**
	 * <p>Description : Online發送</p>
	 * <p>Created By : Alex Cheng</p>
	 * <p>Create Time : Mar 13, 2016</p>
	 * @param aForm
	 * @param request
	 * @param response 
	 * @throws Exception 
	 */
	private void onlinePrint(IssuesForm aForm, HttpServletRequest request, HttpServletResponse response) throws Exception {
		 Long policyId = aForm.getPolicyId();
		 String policyCode = policyService.getPolicyCodeByPolicyId(policyId);
		 
		 //於prepareExtensionVO()中執行uwIssueId的排序
		 FmtUnb0370ExtensionVO vo = prepareExtensionVO(aForm, request);
		 
		 //依送件管道轉型成對應的VO(20221012 IrisLiu, PCR-499927-BC456_保障型平台微型保險)
		 vo = transExtensionVO(aForm, vo);		 
		 
		 List<DocumentFormFile> uploadFileList = nbLetterHelper.getUploadFileList(request, aForm);
		 // PCR-370827 e-Approval ITR-2000946_核保員由照會夾寄PDF檔,導致元鎂產製照會單失敗改善方案
		 uploadFileList = convertPdfToTiff(uploadFileList, vo.getTemplateId(), vo.getPolicyCode());
		 WSLetterUnit unit = nbLetterHelper.generalLetterUnit(   
						 response,
                         policyId, 
                         vo.getTemplateId(), 
                         uploadFileList,
                         aForm.getApplyImageList(),
                         vo);

		 //依送件管道於WSLetterUnit設定相關資料(20221012 IrisLiu, PCR-499927-BC456_保障型平台微型保險)
		 setIndexVO(aForm, vo, unit); //設定信函index
		 setSendInfo(aForm, unit); //設定信函寄送規則
		 
		 /**取得今天的 batch 核保照會documentId**/
		 /** batch列印要先將舊的照會刪除**/
		 DocumentVO doc = getCurrentDocumentId(vo.getTemplateId(), policyId);
		 Long docId = (doc != null ? doc.getListId() : null);
		 if (docId != null) {
			 /**清除 t_outstanding_issues_doc..**/
			 cleanOutstandingIssuesDoc(docId);
			 /**將已發的附照會狀態改為6:已取消**/
			 deleteSubDocs(docId);
			 documentService.updateLetterStatus(docId, CodeCst.LETTER_STATUS_DELETED);
		 }
		 /**取得附加照會**/
		 List<Long> subDocs;
		 try {
			subDocs = uwIssuesLetterService.getSubDocList(vo,
					vo.getIssuelistId(),policyId,aForm.getUnderwriteId());
		 } catch (Exception e) {
			throw ExceptionFactory.parse(e);
		 }
		 
		 if ( subDocs == null || subDocs.isEmpty()) {
			 docId = commonLetterService.sendLetter(policyId, vo.getTemplateId(), unit, policyId, policyCode, null);
		 } else {
			 /**需等組成group letter在一起發送*/
			 docId = commonLetterService.sendBatchLetter(policyId, vo.getTemplateId(), unit, policyId, policyCode, null);
			 /**batch 照會轉online發放照會**/
			 commonLetterService.sendGroupLetter(docId, subDocs);
			 
		 }
		 createOutstandingIssueDoc(vo.getIssuelistId(), docId);

		 if(doc != null){
			 //2017/08/13 by simon.huang
			 //RTC 185271-核保照會通知單batch打印後修改催辦日及催辦到期日,再online發送日期會被reset
			 //調整由修改後的日期回押至online打印的照會
			 if(documentService.isFmtUnb0373(doc.getTemplateId().longValue())) {
				 //20221012 IrisLiu, PCR-499927-BC456_保障型平台微型保險
				 //核保照會通知單-網路投保(FMT_UNB_0373)無催辦，以修改後的照會到期日回壓至online發送的照會單
				 documentManagementService.updateDocumentReminderDate(docId, doc.getReplyDueDate(), null);
				 //on-line 產生FMT-UNB-0373照會單同時做UNB0373簡訊發送 2022/11/03 add by Kathy
				 nbEventCI.sendECUnb0373Sms2Cust(policyId, vo.getPolicyCode(),  vo.getNoticeDate(), doc.getReplyDueDate());
			 }else {
				 documentManagementService.updateDocumentReminderDate(docId,
						 doc.getReminderStartDate(), doc.getReminderEndDate());
			 }
		 }else {
			 //on-line 產生FMT-UNB-0373照會單同時做UNB0373簡訊發送 2022/11/03 add by Kathy
			 if(documentService.isFmtUnb0373(vo.getTemplateId())) {
		        //照會到期日(寫入t_document時才會計算照會到期日)
				Date noticeDate = AppContext.getCurrentUserLocalTime();
				//FMT_UNB_0373核保照會通知單-網路投保  計算照會到期日
				Date replyDueDate = documentService.getReplyDueDate(Template.TEMPLATE_20039.getTemplateId(), noticeDate);
				nbEventCI.sendECUnb0373Sms2Cust(policyId, vo.getPolicyCode(),  vo.getNoticeDate(),replyDueDate);
			 }
		 }
	}
	

	
	/**新增T_CREATE_OUTSTANDING_ISSUE_DOC**/
	private void createOutstandingIssueDoc(List<Long> issuelistId, Long docId) {
		Long[] issueListArray = new Long[issuelistId.size()];
		issueListArray = issuelistId.toArray(issueListArray);
		uwIssuesLetterService.storeOID(issueListArray, docId, AppContext.getCurrentUser().getLangId());
	}
	
	/**刪除T_CREATE_OUTSTANDING_ISSUE_DOC**/
	private void cleanOutstandingIssuesDoc(Long docId) {
		uwIssuesLetterService.deleteOID(docId);
	}

	/**
	 * <p>Description : 預覽照會單</p>
	 * <p>Created By : Alex Cheng</p>
	 * <p>Create Time : Mar 13, 2016</p>
	 * @param aForm
	 * @param request
	 * @param response
	 * @throws IOException
	 * @throws Exception
	 */
	private void preViewPrint(IssuesForm aForm, HttpServletRequest request, HttpServletResponse response) throws IOException, Exception {
		 Long policyId = aForm.getPolicyId();
		 
		 //於prepareExtensionVO()中執行uwIssueId的排序
		 FmtUnb0370ExtensionVO vo = prepareExtensionVO(aForm, request);
		 
		 //依送件管道轉型成對應的VO(20221012 IrisLiu, PCR-499927-BC456_保障型平台微型保險)
		 vo = transExtensionVO(aForm, vo);
		 
		 List<DocumentFormFile> uploadFileList = nbLetterHelper.getUploadFileList(request, aForm);
		 // PCR-370827 e-Approval ITR-2000946_核保員由照會夾寄PDF檔,導致元鎂產製照會單失敗改善方案
		 uploadFileList = convertPdfToTiff(uploadFileList, vo.getTemplateId(), vo.getPolicyCode());
		 WSLetterUnit unit = nbLetterHelper.generalLetterUnit(   
						 response,
                         policyId, 
                         vo.getTemplateId(), 
                         uploadFileList,
                         aForm.getApplyImageList(),
                         vo);
		 
		 //依送件管道於WSLetterUnit設定相關資料(20221012 IrisLiu, PCR-499927-BC456_保障型平台微型保險)
		 setIndexVO(aForm, vo, unit); //設定信函index
		 setSendInfo(aForm, unit); //設定信函寄送規則
		 
		 List<Long> issuelistId = vo.getIssuelistId();
		
		 /*取得附加照會 (因隨函檢附為add(0,item),所以將 附加照會放在疾病問卷之後才處理 */
		 List<WSLetterUnit> subUnits = uwIssuesLetterService.getSubUnitList(vo, issuelistId, policyId,aForm.getUnderwriteId());
		 WSLetterUnit[] units = new WSLetterUnit[subUnits.size() + 1];
		 units[0] = unit;
		 for(int i = 0 ; i < subUnits.size() ; i++ ) {
			 units[1+i] = subUnits.get(i);
		 }
		 super.previewLetter(request, response, vo.getTemplateId(), units);
		 

	}
	
	
	private boolean checkDate(Date dbDate, Date localDate ){
		Date aDate = DateUtils.truncateDay(dbDate);
		Date bDate = DateUtils.truncateDay(localDate);
		boolean retBln = aDate.compareTo(bDate) == 0;
		return retBln;
	}
	
	/**
	 * <p>Description : 取消已Batch發送，未發放的信函 </p>
	 * <p>Created By : Alex Cheng</p>
	 * <p>Create Time : Mar 14, 2016</p>
	 * @param issueListIds
	 */
	private void cancelUWIssuesLetter(Long[] issueListIds){
		if(issueListIds != null ){
			for(Long listId : issueListIds){
				ProposalRuleResultVO ruleResultVO = proposalRuleResultService.load(listId);
				List<IssuesDocContentVO> issuesDocContentVOs = documentManagementService.findIssuesDocContentByRuleResultId(listId);
				if(issuesDocContentVOs != null){
					for(IssuesDocContentVO issuesDocContentVO : issuesDocContentVOs){
						if(issuesDocContentVO.getDocumentId() == null){
							return ;
						}else{
							DocumentVO documentVO = documentService.load(issuesDocContentVO.getDocumentId());
							if((CodeCst.DOCUMENT_STATUS__PUBLISHED.equals(documentVO.getStatus()) || CodeCst.DOCUMENT_STATUS__ISSUED.equals(documentVO.getStatus())) &&
											checkDate(documentVO.getInsertTime(), AppContext.getCurrentUserLocalTime())){
								documentVO.setStatus(CodeCst.DOCUMENT_STATUS__DELETED);
								documentService.saveorUpdate(documentVO);
								
							}
						}
					}
				}
			}
		}
	}

    /**
     * Description : 核保問題頁面查詢<br>
     * Created By : Alex Cheng<br>
     * Create Time : Mar 8, 2016<br>
     *
     * @param aForm
     * @param request
     */
    private void queryIssues(IssuesForm aForm, HttpServletRequest request) throws GenericException {
        String curStep = com.ebao.ls.encode.encoder.Encode.encodeForHTML(request).getParameter("curStep");
        String isIframe = com.ebao.ls.encode.encoder.Encode.encodeForHTML(request).getParameter("isIframe");        aForm.setIsIframe(isIframe);
        if (curStep == null) {
            curStep = "uw";
        }
        com.ebao.ls.encode.encoder.Encode.encodeForHTML(request).setAttribute("curStep", curStep);
        Long policyId = null;
        try {
            policyId = Long.valueOf(com.ebao.ls.encode.encoder.Encode.encodeForHTML(request).getParameter("policyId"));
        } catch (NumberFormatException ex) {
            log.error("", ex);
        }
        if (policyId == null) {
            policyId = (Long) request.getAttribute("policyId");
        }
        aForm.setPolicyId(policyId);
        request.setAttribute("policyId", policyId);
        // -------get policy Basic info
        PolicyVO policy = policyService.load(policyId);

        if (policy != null) {
            aForm.setPolicyCode(policy.getPolicyNumber());
            aForm.setLiabilityState(policy.getRiskStatus());
            aForm.setProposalStatus(policy.getProposalStatus());
            aForm.setMoneyId(policy.getCurrency());
            aForm.setApplyCode(policy.getProposalNumber());
            aForm.setApplyDate(DateUtils.date2String(policy.getApplyDate()));
            aForm.setSubmitChannel(policy.getSubmitChannel());

            // 批次簽署
            aForm.setBatchSignRange(policy.getProposalInfo().getBatchSignRange());
            aForm.setElderCalloutFlag(policyService.isBrBdElderCalloutFlag(policy));
            String remoteIndi = policy.getProposalInfo().getRemoteIndi();
            String remoteCallout = policy.getProposalInfo().getRemoteCallout();
            List<String> certiCodeList = remoteHelper.findRemotePolicyCertiCodeList(policyId);
            aForm.setRemoteIndi(remoteHelper.converterRemoteIndi(remoteIndi));//遠距投保
            aForm.setRemoteOverseas(remoteHelper.getDisplayRemoteOverseas(policyId, remoteIndi)); // 境外遠距投保
            aForm.setRemoteCallout(remoteHelper.getDisplayRemoteCallout(remoteIndi, remoteCallout));//錄音錄影覆核抽檢件
            aForm.setRemoteCheck(remoteHelper.getDisplayRemoteCheck(policyId, remoteIndi, certiCodeList));//身分驗證結果

            // ------ coverage
            CoverageVO coverage = coverageService.findMasterCoverageByPolicyId(policy.getPolicyId());
            if (coverage != null) {
                String productName = policyDS.getCoverageProudctName(coverage);

                aForm.setInitialType(coverage.getCurrentPremium().getPaymentFreq());
                aForm.setProductName(productName);

                // ------agent
                List<CoverageAgentVO> voList = coverage.gitSortCoverageAgents();
                if (voList != null && voList.size() >= 1) {
                    aForm.setAgentName(voList.get(0).getAgentName());
                    if (voList.get(0).getChannelCode() != null) { //抓第一筆業務員資料
                        String channelCode = voList.get(0).getChannelCode();
                        ChannelOrgVO channelOrgVO = channelOrgService.findByChannelCode(channelCode);
                        if (channelOrgVO != null) {
                            aForm.setSalesChannel(channelOrgVO.getChannelType());  //設定通路別
                            aForm.setAgentOrgan(channelOrgVO.getChannelId() == null ? "" : channelOrgVO.getChannelId().toString());   //設定單位代號
                        }
                    }

                }
            }
            aForm.setHolderCertiCode(policy.getPolicyHolder().getCertiCode());
        }

        /* issuesLtrList 取得 照會信函列表 */
        List<IssuesDocVO> uWLetterList = null;
        /* 核保照會中規則校驗結果 */
        List<UwProposalRuleResultVO> issuesList = null;
        //PCR 415727 新增保全規則校驗作業架構 - 新增保全規則校驗結果
        List<UwProposalRuleResultVO> posRuleIssuesList = null;
        /* 取得人工新增核保問題列表 */
        Integer[] listTypeName = null;
        List<UwProposalRuleResultVO> uWManualIssuesList = null;
        /* 取得 體檢照會列表 */
        List medicalIssueList = null;

        if (aForm.isCsUw()) {// 來源是保全核保 
            String applyCode = applicationService.getApplication(aForm.getChangeId()).getApplyCode();
            ApplicationLogger.addLoggerData("csUWIssuesActionHelper.getUWLetterList(" + policyId + ", " + applyCode + ") begin");
            uWLetterList = csUWIssuesActionHelper.getUWLetterList(policyId, applyCode);
            csUWLetterListAdj(uWLetterList);
            ApplicationLogger.addLoggerData("csUWIssuesActionHelper.getUWLetterList(" + policyId + ", " + applyCode + ") end");

            ApplicationLogger.addLoggerData("csUWIssuesActionHelper.getIssuesList(" + policyId + ", " + aForm.getChangeId() + ") begin");
            issuesList = csUWIssuesActionHelper.getIssuesList(policyId, aForm.getChangeId());
            ApplicationLogger.addLoggerData("csUWIssuesActionHelper.getIssuesList(" + policyId + ", " + aForm.getChangeId() + ") end");

            //PCR 415727 新增保全規則校驗作業架構 - 新增保全規則校驗結果
            ApplicationLogger.addLoggerData("csUWIssuesActionHelper.getPosRuleIssuesList(" + policyId + ", " + aForm.getChangeId() + ") begin");
            posRuleIssuesList = csUWIssuesActionHelper.getPosRuleIssuesList(policyId, aForm.getChangeId());
            ApplicationLogger.addLoggerData("csUWIssuesActionHelper.getPosRuleIssuesList(" + policyId + ", " + aForm.getChangeId() + ") end");

            ApplicationLogger.addLoggerData("csUWIssuesActionHelper.getManualIssuesList(" + policyId + ", " + aForm.getChangeId() + ") begin");
            uWManualIssuesList = csUWIssuesActionHelper.getManualIssuesList(policyId, aForm.getChangeId());
            ApplicationLogger.addLoggerData("csUWIssuesActionHelper.getManualIssuesList(" + policyId + ", " + aForm.getChangeId() + ") end");

            ApplicationLogger.addLoggerData("csUWIssuesActionHelper.getMedicalIssueList(" + aForm.getChangeId() + ") begin");
            medicalIssueList = csUWIssuesActionHelper.getMedicalIssueList(aForm.getChangeId());
            ApplicationLogger.addLoggerData("csUWIssuesActionHelper.getMedicalIssueList(" + aForm.getChangeId() + ") end");
        } else {
            Long underwriteId = aForm.getUnderwriteId();
            //2017/05/06 by simon.huang 特別處理新契約加費/批註核保事項，主照會狀態已作廢同步核保信息狀態為註銷;
            ApplicationLogger.addLoggerData("syncAbortedOutstandingIssueDoc(" + policyId + ", " + underwriteId + ") begin");
            syncAbortedOutstandingIssueDoc(policyId, underwriteId);
            ApplicationLogger.addLoggerData("syncAbortedOutstandingIssueDoc(" + policyId + ", " + underwriteId + ") end");

            //2017/06/07 by simon.huang 特別處理新契約疾病問卷OPQ關連至疾病letter資料表
            ApplicationLogger.addLoggerData("syncInsuredNoticesSickTemp(" + policyId + ", " + underwriteId + ") begin");
            syncInsuredNoticesSickTemp(policyId, underwriteId);
            ApplicationLogger.addLoggerData("syncInsuredNoticesSickTemp(" + policyId + ", " + underwriteId + ") end");

            ApplicationLogger.addLoggerData("getUWLetterList(" + policyId + ", request) begin");
            uWLetterList = this.getUWLetterList(policyId, request);
            ////核保信函列表內，「核保照會通知單-網路投保(FMT_UNB_0373)」需顯示「照會到期日」(20221025 IrisLiu,PCR-499927-BC456_保障型平台微型保險)
            documentService.replaceReminderStartDateToReplyDueDate(policyId, uWLetterList);
            ApplicationLogger.addLoggerData("getUWLetterList(" + policyId + ", request) end");

            ApplicationLogger.addLoggerData("proposalRuleResultService.findRuleResultsByTypes(" + policyId + ", UnbCst.uWIssuesRuleTypeList) begin");
            issuesList = proposalRuleResultService.findRuleResultsByTypes(policyId, UnbCst.uWIssuesRuleTypeList);
            if (issuesList != null && !issuesList.isEmpty()) {
                for (UwProposalRuleResultVO issue : issuesList) {
                    ApplicationLogger.addLoggerData(" [RuleResults] RuleName: " + issue.getRuleName() + " - RuleStatus:" + issue.getRuleStatus());
                }
            }
            ApplicationLogger.addLoggerData("proposalRuleResultService.findRuleResultsByTypes(" + policyId + ", UnbCst.uWIssuesRuleTypeList) end");

            ApplicationLogger.addLoggerData("proposalRuleResultService.findRuleResultsByTypes(" + policyId + ", CodeCst.PROPOSAL_RULE_TYPE__MANUAL_MSG) begin");
            listTypeName = new Integer[]{CodeCst.PROPOSAL_RULE_TYPE__MANUAL_MSG};
            uWManualIssuesList = proposalRuleResultService.findRuleResultsByTypes(policyId, listTypeName);
            ApplicationLogger.addLoggerData("proposalRuleResultService.findRuleResultsByTypes(" + policyId + ", CodeCst.PROPOSAL_RULE_TYPE__MANUAL_MSG) end");

            ApplicationLogger.addLoggerData("getMedicalIssueList(" + policyId + ", " + underwriteId + ", request) begin");
            medicalIssueList = this.getMedicalIssueList(policyId, underwriteId, request);
            ApplicationLogger.addLoggerData("getMedicalIssueList(" + policyId + ", " + underwriteId + ", request) end");
        }

        /* 依UpdateTime排序 */
        Collections.sort(uWManualIssuesList, (Comparator<UwProposalRuleResultVO>) (UwProposalRuleResultVO o1, UwProposalRuleResultVO o2) -> o1.getUpdateTime().compareTo(o2.getUpdateTime()));

        request.setAttribute("issuesLtrList", uWLetterList);
        request.setAttribute("posRuleIssuesList", posRuleIssuesList);
        request.setAttribute("uWIssuesList", issuesList);
        request.setAttribute("uWManualIssuesList", uWManualIssuesList);
        request.setAttribute("medicalIssueList", medicalIssueList);
        request.setAttribute("fmtUnb0373ValidatorMap", setFmtUnb0373ValidatorMap());
    }

	/**
	 * <p>Description : 核保信息已不存在副照會用的子表中,需判斷核保信息是否發放過，若發放過且均為已作廢，則作系統自動關閉，避免再次發放查無子表資料</p>
	 * <p>Created By : simon.huang</p>
	 * <p>Create Time : May 8, 2017</p>
	 * @param policyId
	 * @param underwriteId
	 */
	private void syncAbortedOutstandingIssueDoc(Long policyId,Long underwriteId) {
		
		
		List<ProposalRuleResultVO> ruleResultList = new ArrayList<ProposalRuleResultVO>();
		
		for(String ruleName : msgSubDoc.keySet()){
			//找出待處理的核保訊息(加費/批註)
			ruleResultList.addAll(proposalRuleResultService.findAllPendingList(
					policyId, 
					ruleName, 
					null, 
					null));
		}
		
		for(ProposalRuleResultVO ruleResult : ruleResultList){
			
			String tableName = msgSubDoc.get(ruleResult.getRuleName());
			Long msgId = ruleResult.getListId();
			if(tableName == null){
				continue;
			}
			
			List<Long> msgIdList = uwPolicyService.findSubDocMsgByMsgId(
					tableName, underwriteId, msgId);
			
			//msgId不在加費(批註)檔中
			if(msgIdList.size() == 0){
				//是否有未作廢的照會，沒有則關閉
				List<IssuesDocContentVO> issueDocList = documentManagementService.
							findDocuemtStatusByIssueResultId(ruleResult.getListId());
				
				if(issueDocList.size() > 0){//發放過，判斷是否有未作廢的照會
					
					boolean isAutoAbort = true;
					
					//全部都是作廢的就關閉此核保訊息(因若再次發放也無法對到子表的資料)
					for(IssuesDocContentVO issueDoc : issueDocList){
						String stauts = issueDoc.getStatus();
						if(!CodeCst.DOCUMENT_STATUS__ABORTED.equals(stauts) 
								&& !CodeCst.DOCUMENT_STATUS__DELETED.equals(stauts) ){
							isAutoAbort = false;
							break;
						}
					}
					
					if(isAutoAbort){
						//執行系統關閉
						ruleResult.setRuleStatus(CodeCst.PROPOSAL_RULE_STATUS__SYSTEMCLOSED);
						proposalRuleResultService.save(ruleResult);
						
						HibernateSession3.currentSession().flush();
					}
				}
			}
		}
	}

	/**
	 * <p>Description : </p>
	 * <p>Created By : simon.huang</p>
	 * <p>Create Time : Jun 7, 2017</p>
	 * @param policyId
	 * @param underwriteId
	 */
	private void syncInsuredNoticesSickTemp(Long policyId, Long underwriteId) {
	
		//未出未被設定letterId的問卷資料，新增letter並回寫letterId至sourceId
		List<ProposalRuleResultVO> resultList = proposalRuleResultService.find(
				Restrictions.eq("policyId", policyId),
				Restrictions.eq("sourceType", CodeCst.PROPOSAL_RULE_SOURCE_TYPE__SICK_FORM),
				Restrictions.isNull("sourceId"),
				Restrictions.isNotNull("notes"));
		
		for(ProposalRuleResultVO ruleResultVO : resultList){
			JSONObject json = JSONObject.fromObject(ruleResultVO.getNotes());
			Long insuredId = json.getLong("insuredId");
			
			
			List<UwSickFormLetterVO> letterList = uwSickFormLetterService.findByPolicyInsured(underwriteId, insuredId, "0");
			UwSickFormLetterVO letter = null;
			if(letterList.size() > 0){
				letter = letterList.get(0);
				letter.setDocumentId(null);
				letter = uwSickFormLetterService.save(letter);
				//刪除舊有問卷項目寫入新的
				uwSickFormItemService.deleteByLetterId(letter.getListId());
			} else {
				letter =  new UwSickFormLetterVO();
				letter.setUnderwriteId(underwriteId);
				letter.setInsuredId(insuredId);
				letter.setStatus("0");
				letter = uwSickFormLetterService.save(letter);
			}
			
			for( Object sickFormId : json.getJSONArray("sickForms").toArray()){
				/* 新增疾病問卷項目 */
				UwSickFormItemVO vo = new UwSickFormItemVO();
				vo.setSickFormLetterId(letter.getListId());
				vo.setSickFormItem((String)sickFormId);
				/* 回寫msg_id至 sick_form_item */
				vo.setMsgId(ruleResultVO.getListId());
				vo = uwSickFormItemService.save(vo);
			}
			//回寫letterId至sourceId
			ruleResultVO.setSourceId(letter.getListId());
			proposalRuleResultService.save(ruleResultVO);
		}
		
	}
	/**
	 * <p>Description : 體檢照會列表</p>
	 * <p>Created By : Alex Cheng</p>
	 * <p>Create Time : Mar 17, 2016</p>
	 * @param underwriteId
	 * @param request
	 * @return
	 */
	protected List<UwIssueMedicalItemVO> getMedicalIssueList(
				Long policyId, Long underwriteId,
					HttpServletRequest request){
		List<UwIssueMedicalItemVO> retList = uwMedicalLetterService.getMedicalIssueListByUnderwriteId(policyId, underwriteId);
		Iterator<UwIssueMedicalItemVO> it = retList.iterator();
		while(it.hasNext()) {
			UwIssueMedicalItemVO vo = it.next();
			
			if ( uwMedicalLetterService.STATUS_DEFAULT.equals(vo.getMedicalLetterStatus())) {
				it.remove();
			} else {
				String itemCode = vo.getMedicalItem();
				if ( vo.getMedicalItem().indexOf(",") < 0) {
					String desc = CodeTable.getCodeDesc("T_STD_MEDICAL_FEE", itemCode, request);
					vo.setMedicalItem(itemCode+","+desc);
				}
			}
			
		}
		return retList;
	}

	
	/**
	 * <p>Description : 取得 照會信函列表</p>
	 * <p>Created By : Alex Cheng</p>
	 * <p>Create Time : Feb 20, 2016</p>
	 * @param policyId
	 * @param request
	 * @return
	 */
	protected List<IssuesDocVO> getUWLetterList(Long policyId,
					HttpServletRequest request) {
		List<IssuesDocVO> issuesDocVos= documentManagementService
						.getUWLetterListByPolicyId(policyId);
		return issuesDocVos;
	}

	
	/**
	 * <p>Description : 整理頁面送出信函需要的資料 </p>
	 * <p>Created By : Alex Cheng</p>
	 * <p>Create Time : Mar 9, 2016</p>
	 * @param issuesForm
	 * @param request
	 * @return
	 */
	protected FmtUnb0370ExtensionVO prepareExtensionVO(IssuesForm issuesForm, HttpServletRequest request) {
		Long policyId = issuesForm.getPolicyId();
		
		//EC件改寄送「核保照會通知單-網路投保(FMT_UNB_0373)」(20221004 IrisLiu, PCR-499927-BC456_保障型平台微型保險)
		Long templateId = TEMPLATE_ID;
		if(policyService.isEc(issuesForm.getPolicyId())) {
			templateId = TEMPLATE_ID_EC;
		}
		
		FmtUnb0370ExtensionVO vo = new FmtUnb0370ExtensionVO();
		nbNotificationHelper.initNBLetterExtensionVO(policyId,vo);
		nbNotificationHelper.setLetterContractorAtUW(vo,policyId);

		Date noticeDate = AppContext.getCurrentUserLocalTime();
		//催辦日(預設同回覆到期日)
		//BC353 PCR-263273 有固定回覆日時催辦日改用催辦到期日(SpecialReplyDueDate) 2018/10/12 add by Kathy
    	Date specialReplyDueDate = null;
        Map<String, Object> specialDaysMap =  new HashMap<String, Object>();
        specialDaysMap = documentService.getDocSpecialReplyDueDate(policyId, templateId, noticeDate);  
		
		if (specialDaysMap != null) {
	        specialReplyDueDate = (Date) specialDaysMap.get("specialReplyDueDate");
		}

		vo.setNoticeDate(noticeDate);
		vo.setTemplateId(templateId);
		if (specialReplyDueDate  != null) { 
			vo.setRemindStartDate(specialReplyDueDate);			
		}else{
			Date remindStartDate = commonLetterService.getRemaindStartDate(templateId, noticeDate);
			vo.setRemindStartDate(remindStartDate);
		}	

		vo.setHotline(nbNotificationHelper.getHotline());
		vo.setBarcode1(CodeCst.BARCODE1_FMT_UNB_0370);
		vo.setBarcode2(vo.getPolicyCode());
		vo.setSerialNum(vo.getSerialNum());
		List<WSItemString> contents = new ArrayList<WSItemString>();
		List<Long> contentIds = new ArrayList<Long>();
		int num = 1;
		
		//系統核保訊息9(OPQ+RMS)
		if(issuesForm.getuWIssuesCheckListId() != null){
			//重新排序uwIssueListId#RTC-147426 comment41第2點
			List<Long> sortIssueListId = proposalRuleResultService.sortUwIssueListId(policyId,
					issuesForm.getuWIssuesCheckListId());

			for (long element : sortIssueListId) {
				if (element > 0) {
					ProposalRuleResultVO ruleResult = proposalCI
							.loadProposalRuleResultById(element);
					if (ruleResult.getLetterContent() == null
							|| ruleResult.getLetterContent().length() == 0
							|| ruleResult.getLetterContent().equals("")) {
						contents.addAll(WSItemString.getWSItemDotStringList(String.valueOf(num++), ruleResult.getViolatedDesc()));

					} else {
						contents.addAll(WSItemString.getWSItemDotStringList(String.valueOf(num++), ruleResult.getLetterContent()));
					}
					contentIds.add(ruleResult.getListId());
				}
			}
		}
		
		//人工照會訊息
		if(issuesForm.getuWManualIssuesCheckListId() != null){
			for (long element : issuesForm.getuWManualIssuesCheckListId()) {
				if (element > 0) {
					ProposalRuleResultVO ruleResult = proposalCI
							.loadProposalRuleResultById(element);
					if (ruleResult.getLetterContent() == null
							|| ruleResult.getLetterContent().length() == 0
							|| ruleResult.getLetterContent().equals("")) {
						contents.addAll(WSItemString.getWSItemDotStringList(String.valueOf(num++), ruleResult.getViolatedDesc()));
					} else {
						contents.addAll(WSItemString.getWSItemDotStringList(String.valueOf(num++), ruleResult.getLetterContent()));
					}
					contentIds.add(ruleResult.getListId());
				}
			}
		}
		vo.setIssueContents(contents);
		vo.setIssuelistId(contentIds);
		return vo;
	}

	/**<pre>
     * PCR-499927-BC456_保障型平台微型保險
	 * 依送件管道將FmtUnb0370ExtensionVO轉成對應的子類別VO
	 * </pre>
	 * @param issuesForm
	 * @param vo
	 * @return
	 * @throws Exception
	 * @author IrisLiu 2022/10/12
	 */
	private FmtUnb0370ExtensionVO transExtensionVO(IssuesForm issuesForm, FmtUnb0370ExtensionVO fmtUnb0370VO) throws Exception {
		 
		 //若為EC件，需轉型為FmtUnb0373ExtensionVO以設定擴充欄位
		 if(policyService.isEc(issuesForm.getPolicyId())) {
			 
			 //不可用eBao底層的BeanUtils.copyProperties，避免轉換VO內的List<Long>參數時出錯
			 FmtUnb0373ExtensionVO fmtUnb0373VO = new FmtUnb0373ExtensionVO();
			 org.apache.commons.beanutils.BeanUtils.copyProperties(fmtUnb0373VO, fmtUnb0370VO);
			 
			 //照會到期日(寫入t_document時才會計算照會到期日，故參考com.ebao.ls.letter.common.CommonLetterServiceImpl.addReminderAndDueDate()計算)
			 Date replyDueDate = documentService.getReplyDueDate(fmtUnb0373VO.getTemplateId(), fmtUnb0373VO.getNoticeDate());
			 
			 fmtUnb0373VO.setReplyDueDate(replyDueDate); //照會到期日
			 fmtUnb0373VO.setProdName(nbNotificationHelper.getFullProdName(issuesForm.getPolicyId())); //商品名稱
			 fmtUnb0373VO.setServiceTel(fmtCommentCi.getFmtCommon(FmtCommonConstant.NB_LETTER_SERVICE_TEL)); //客服專線
			 fmtUnb0373VO.setHotline(nbNotificationHelper.getHotlineToCust()); //免付費專線
			 fmtUnb0373VO.setDocUploadURL(nbNotificationHelper.getUploadURLToCust()); //文件上傳網址
			 fmtUnb0373VO.setRocReplyDueDate(nbNotificationHelper.convertFullRocDate(replyDueDate)); //照會到期日(民國年月日)
			 fmtUnb0370VO = fmtUnb0373VO;

		 }
			 
		 return fmtUnb0370VO;
	}

	/**
	 * <pre>
     * PCR-499927-BC456_保障型平台微型保險
	 * 依送件管道初始&回傳對應的信函indexVO
	 * </pre>
	 * @param issuesForm
	 * @param vo
	 * @return
	 * @author IrisLiu 2022/10/04
	 */
	private void setIndexVO(IssuesForm issuesForm, NbLetterExtensionVO vo, WSLetterUnit unit) {
		
		WSExtendLetterIndex index;
		
		if(policyService.isEc(issuesForm.getPolicyId())) {
			index = new FmtUnb0373IndexVO();
		}else {
			index = new FmtUnb0370IndexVO();
		}
		
		nbNotificationHelper.initIndexVO(index, vo, vo.getTemplateId());
		unit.setIndex(index);
		
	}

	/**
	 * <pre>
     * PCR-499927-BC456_保障型平台微型保險
	 * 依送件管道，設定對應的寄送規則
	 * </pre>
	 * @param issuesForm
	 * @param unit
	 * @author IrisLiu 2022/10/14
	 */
	private void setSendInfo(IssuesForm issuesForm, WSLetterUnit unit) {
		
		//通路寄送規則,採Email寄送
		NbLetterHelper.unbSendingMethod(unit);
		 
		//若為EC件，調整寄送規則(0370寄給業務員、0373改寄給要保人)
		//此處不設定emailReceivers，底層DocumentServiceImpl.prepareDocumentInput()會依照T_TEMPLATE.的RULE_NO設定寄送對象、mail、密碼
		if(policyService.isEc(issuesForm.getPolicyId())) {
			//採email寄送
			unit.setIsToAgent(CodeCst.YES_NO__NO);
			unit.setSendingMethod(WSLetterSendingMethod.EMAIL.getCode());
		}
		
	}
    
    /**
     * <pre>
     * PCR-499927-BC456_保障型平台微型保險
     * 設定供前端畫面檢核信函是否為「FMT_UNB_0373(核保照會通知單-網路投保)」的map
     * </pre>
     * @param policyId
     * @return
     * @author IrisLiu 20221024
     */
    private Map<String, Object> setFmtUnb0373ValidatorMap() {
        Map<String, Object> fmtUnb0373ValidatorMap = new HashMap<String, Object>();
        fmtUnb0373ValidatorMap.put("templateId", TEMPLATE_ID_EC);
        fmtUnb0373ValidatorMap.put("errorMsg", "MSG_1267235"); //不可早於系統日，請確認。
        return fmtUnb0373ValidatorMap;
    }
	
	/**
	 * <p>Description :判斷來源是否為保全核保 </p>
	 * <p>Created By : TGL144</p>
	 * <p>Create Time : Nov 29, 2016</p>
	 * @param issuesForm
	 * @param request
	 */
	private void isCsUw(IssuesForm issuesForm, HttpServletRequest request){
		UwPolicyVO uwPolicyVO = uwPolicyService.findUwPolicy(issuesForm.getUnderwriteId());
		if(uwPolicyVO.getChangeId()!=null){
			issuesForm.setCsUw(true);
			issuesForm.setChangeId(uwPolicyVO.getChangeId());
			request.setAttribute("isCsUw",true);
		}else{
			issuesForm.setCsUw(false);
		}
		
		//判斷是否為核保員/或簽核主管
		Integer uwTransferFlow = uwPolicyVO.getUwTransferFlow();
		if(uwTransferFlow != null && uwTransferFlow == CodeCst.UW_TRANS_FLOW_FIRSTER){
			issuesForm.setIsUnderwriter(true);
		} else {
			issuesForm.setIsUnderwriter(false);
		}
		
		Integer bizCategory = deptCI.getBizCategoryByUserId(AppContext.getCurrentUser().getUserId());
		request.setAttribute("bizCategory", (bizCategory!=null?bizCategory.toString():""));
	}
	
	// IR224172-顯示pos照會到期日
	private void csUWLetterListAdj(List<IssuesDocVO> uWLetterList) {
        for (IssuesDocVO doc:uWLetterList) {
            doc.setReminderStartDate(doc.getReplyDueDate());
        }
    }

	/**
	 * PCR-370827 e-Approval ITR-2000946_核保員由照會夾寄PDF檔,導致元鎂產製照會單失敗改善方案
	 */
	private List<DocumentFormFile> convertPdfToTiff(List<DocumentFormFile> uploadFileList, long templateId, String policyCode) throws GenericException {
		List<DocumentFormFile> documentFormFileList = new LinkedList<DocumentFormFile>();
		String saveFilePath = commonLetterService.getFilePathNb(templateId);
		log.debug("convertPdfToTiff saveFilePath=" + saveFilePath);
		int fileOrder = 1;
		for(DocumentFormFile upFile : uploadFileList) {
			if( upFile.getFormFile() != null && upFile.getImageName() == null ) { //排除已上傳過的檔案未刪除
				FormFile file = upFile.getFormFile();
				String fileName = file.getFileName();
				String ext = fileName.substring(fileName.lastIndexOf(".") + 1, fileName.length());
				if(!StringUtils.isNullOrEmpty(ext)) {
					ext = ext.toUpperCase();
				}
				// 只針對 PDF 檔做處理，非 PDF 檔一律原封不動塞進要回傳的 formFileList
				if("PDF".equals(ext)) {
					try {
//						List<ImageOutputStream> imageOutputStreams = IcePdfUtil.pdfToTiff(file.getInputStream());
//						int fileOrder = 1;
//						for(ImageOutputStream ios : imageOutputStreams){
//							// imageOutputStreams -> TIFF
//							try{
//								String folderName = saveFilePath;
//								String imageName = policyCode + "_" + fileOrder + "_" + System.currentTimeMillis() + ".tif";
//								String fullPath = folderName + imageName;
//								byte[] by = IcePdfUtil.readImageToByte(ios);
//								log.debug("convertPdfToTiff fullPath=" + fullPath);
//								FileOutputStream fos = new FileOutputStream(fullPath);
//								fos.write(by);
//								fileOrder++;
//								fos.close();
//								FormFileWrapper formFileWrapper = new FormFileWrapper(new File(fullPath));
//								DocumentFormFile copiedDocumentFormFile = copyDocumentFormFile(upFile, formFileWrapper, fileOrder);
//								documentFormFileList.add(copiedDocumentFormFile);
//							}catch(Exception e){
//								log.error(ExceptionInfoUtils.getExceptionMsg(e));
//							}finally{
//								if(ios != null){
//									ios.flush();
//									ios.close();
//								}
//							}
//							
//						}
						// 2022.05.27 調整為單一TIFF檔案輸出(單檔多頁)
						String folderName = saveFilePath;
						String imageName = policyCode + "_" + templateId + "_" + System.currentTimeMillis() + ".tif";
						String fullPath = folderName + imageName;
						TIFConvertPDF.convert(file.getFileData(), fullPath);
						FormFileWrapper formFileWrapper = new FormFileWrapper(new File(fullPath));
						DocumentFormFile copiedDocumentFormFile = copyDocumentFormFile(upFile, formFileWrapper, fileOrder);
						documentFormFileList.add(copiedDocumentFormFile);
					} catch (Exception e) {
						log.error(ExceptionInfoUtils.getExceptionMsg(e));
					}
				} else {
					// 非 PDF 檔一律原封不動塞進要回傳的 formFileList
					upFile.setOrderNum(new Long(fileOrder));
					documentFormFileList.add(upFile);
				}
			} else {
				upFile.setOrderNum(new Long(fileOrder));
				documentFormFileList.add(upFile);
			}
			fileOrder++;
		}
		return documentFormFileList;
	}

	/**
	 * PCR-370827 e-Approval ITR-2000946_核保員由照會夾寄PDF檔,導致元鎂產製照會單失敗改善方案
	 * @param originalDocumentFormFile
	 * @param formFileWrapper
	 * @param fileOrder
	 * @return DocumentFormFile
	 */
	private DocumentFormFile copyDocumentFormFile(DocumentFormFile originalDocumentFormFile, FormFileWrapper formFileWrapper, int fileOrder) {
		DocumentFormFile copiedDocumentFormFile = new DocumentFormFile();
		copiedDocumentFormFile.setArchivePath(originalDocumentFormFile.getArchivePath());
		copiedDocumentFormFile.setFileName(originalDocumentFormFile.getFileName());
		copiedDocumentFormFile.setFormFile(formFileWrapper);
		copiedDocumentFormFile.setImageIds(originalDocumentFormFile.getImageIds());
		copiedDocumentFormFile.setImageName(originalDocumentFormFile.getImageName());
		copiedDocumentFormFile.setOrderNum(new Long(fileOrder));
		return copiedDocumentFormFile;
	}

    @Resource(name = PolicyService.BEAN_DEFAULT)
    private PolicyService policyService;

    @Resource(name = ProposalRuleResultService.BEAN_DEFAULT)
    private ProposalRuleResultService proposalRuleResultService;

    @Resource(name = CoverageService.BEAN_DEFAULT)
    private CoverageService coverageService;

    @Resource(name = DocumentManagementService.BEAN_DEFAULT)
    private DocumentManagementService documentManagementService;

    @Resource(name = UwIssuesAuthServiceImpl.BEAN_DEFAULT)
    private UwIssuesAuthServiceImpl uwIssuesAuthService;

    @Resource(name = ProposalCI.BEAN_DEFAULT)
    private ProposalCI proposalCI;

    @Resource(name = DeptCI.BEAN_DEFAULT)
    private DeptCI deptCI;

    @Resource(name = UwMedicalLetterService.BEAN_DEFAULT)
    private UwMedicalLetterService uwMedicalLetterService;

    @Resource(name = com.ebao.ls.pa.pub.service.PolicyService.BEAN_DEFAULT)
    com.ebao.ls.pa.pub.service.PolicyService policyDS;

    @Resource(name = NbNotificationHelper.BEAN_DEFAULT)
    private NbNotificationHelper nbNotificationHelper;

    @Resource(name = UwIssuesLetterService.BEAN_DEFAULT)
    private UwIssuesLetterService uwIssuesLetterService;

    @Resource(name = UwPolicyService.BEAN_DEFAULT)
    private UwPolicyService uwPolicyService;

    @Resource(name = CsUWIssuesActionHelper.BEAN_DEFAULT)
    private CsUWIssuesActionHelper csUWIssuesActionHelper;

    @Resource(name = RemoteHelper.BEAN_DEFAULT)
    protected RemoteHelper remoteHelper;

    @Resource(name = ApplicationService.BEAN_DEFAULT)
    private ApplicationService applicationService;

    @Resource(name = UwSickFormItemService.BEAN_DEFAULT)
    private UwSickFormItemService uwSickFormItemService;

    @Resource(name = UwSickFormLetterService.BEAN_DEFAULT)
    private UwSickFormLetterService uwSickFormLetterService;

    @Resource(name = ChannelOrgService.BEAN_DEFAULT)
    protected ChannelOrgService channelOrgService;

	@Resource(name = FmtCommonCI.BEAN_DEFAULT)
	private FmtCommonCI fmtCommentCi;
	
	@Resource(name = CSReportService.BEAN_DEFAULT)
	private CSReportService cSReportService;
	
    @Resource(name = NbEventCI.BEAN_DEFAULT)
    protected NbEventCI nbEventCI;
	
    @Override
    protected NbLetterExtensionVO getExtension(ActionForm unbLetterForm) {
        return null;
    }

}
