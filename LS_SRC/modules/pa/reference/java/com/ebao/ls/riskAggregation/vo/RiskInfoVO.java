package com.ebao.ls.riskAggregation.vo;

import java.math.BigDecimal;

import com.ebao.pub.framework.GenericVO;

public class RiskInfoVO extends GenericVO {

  /** * 
   */
  private static final long serialVersionUID = 1L;
  private Integer riskType;

  //define 8 category amount firstly
  private BigDecimal riskAmount0 = new BigDecimal(0);
  private BigDecimal riskAmount1 = new BigDecimal(0);
  private BigDecimal riskAmount2 = new BigDecimal(0);
  private BigDecimal riskAmount3 = new BigDecimal(0);
  private BigDecimal riskAmount4 = new BigDecimal(0);
  private BigDecimal riskAmount5 = new BigDecimal(0);
  private BigDecimal riskAmount6 = new BigDecimal(0);
  private BigDecimal riskAmount7 = new BigDecimal(0);
  private BigDecimal riskAmount8 = new BigDecimal(0);

  public Integer getRiskType() {
    return riskType;
  }
  public void setRiskType(Integer riskType) {
    this.riskType = riskType;
  }

  public BigDecimal getRiskAmount0() {
    return riskAmount0;
  }
  public void setRiskAmount0(BigDecimal riskAmount0) {
    this.riskAmount0 = riskAmount0;
  }
  public BigDecimal getRiskAmount1() {
    return riskAmount1;
  }
  public void setRiskAmount1(BigDecimal riskAmount1) {
    this.riskAmount1 = riskAmount1;
  }
  public BigDecimal getRiskAmount2() {
    return riskAmount2;
  }
  public void setRiskAmount2(BigDecimal riskAmount2) {
    this.riskAmount2 = riskAmount2;
  }
  public BigDecimal getRiskAmount3() {
    return riskAmount3;
  }
  public void setRiskAmount3(BigDecimal riskAmount3) {
    this.riskAmount3 = riskAmount3;
  }
  public BigDecimal getRiskAmount4() {
    return riskAmount4;
  }
  public void setRiskAmount4(BigDecimal riskAmount4) {
    this.riskAmount4 = riskAmount4;
  }
  public BigDecimal getRiskAmount5() {
    return riskAmount5;
  }
  public void setRiskAmount5(BigDecimal riskAmount5) {
    this.riskAmount5 = riskAmount5;
  }
  public BigDecimal getRiskAmount6() {
    return riskAmount6;
  }
  public void setRiskAmount6(BigDecimal riskAmount6) {
    this.riskAmount6 = riskAmount6;
  }
  public BigDecimal getRiskAmount7() {
    return riskAmount7;
  }
  public void setRiskAmount7(BigDecimal riskAmount7) {
    this.riskAmount7 = riskAmount7;
  }
  public BigDecimal getRiskAmount8() {
    return riskAmount8;
  }
  public void setRiskAmount8(BigDecimal riskAmount8) {
    this.riskAmount8 = riskAmount8;
  }

}
