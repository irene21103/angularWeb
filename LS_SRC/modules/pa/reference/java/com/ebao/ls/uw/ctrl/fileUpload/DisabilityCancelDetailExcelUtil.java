package com.ebao.ls.uw.ctrl.fileUpload;

import java.io.OutputStream;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.ebao.foundation.module.util.i18n.CodeTable;
import com.ebao.ls.pa.nb.util.NBUtils;
import com.ebao.pub.framework.AppContext;

/**
 * <p>Description : 身心障礙查詢/下載</p>
 * <p>PCR_444271&455705_預收輸入覆核補全建檔優化需求</p>
 * <p>Created By : Vince Cheng</p>
 * <p>Create Time : 2021.10.04</p>
 */
public class DisabilityCancelDetailExcelUtil {
	
	private static Logger logger = Logger.getLogger(DisabilityCancelDetailExcelUtil.class);
	
	public static final String BEAN_DEFAULT = "uwDisabilityCancelDetailExcelUtil";
	public static final String EXCEL_SHEET_NAME = "T_DISABILITY_CANCEL_DETAIL";
	public static final String EXCEL_FILE_EXTENSION = ".xlsx";
	public static final Column[] EXCEL_COLUMN_ORDER = new Column[] {
		Column.POLICY_CODE, 
		Column.INSURED_CERTI_CODE, 
		Column.INSURED_NAME, 
		Column.INSURED_GENDER, 
		Column.PRODUCT_VERSION_TYPE, 
		Column.COVERAGE_PERIOD, 
		Column.COVERAGE_YEAR, 
		Column.DISABILITY_TYPE_DESC, 
		Column.DISABILITY_CLASS, 
		Column.CANCEL_REASON_TYPE1, 
		Column.CANCEL_REASON 
	};
	
    public OutputStream processExcel(OutputStream outputStream, List<Map<String, Object>> dataListMap) throws Exception {
    	NBUtils.logger(this.getClass(), "processExcel() begin");
    	
		if (CollectionUtils.isNotEmpty(dataListMap)) {
			XSSFWorkbook workbook = new XSSFWorkbook();
			this.processWorkbook(workbook, dataListMap);
			workbook.write(outputStream);
			outputStream.flush();
		} else {
			NBUtils.logger(this.getClass(), "processExcel() data isEmpty");
		}

		NBUtils.logger(this.getClass(), "processExcel() end");

		return outputStream;
	}
    
	public void processWorkbook(XSSFWorkbook workbook, List<Map<String, Object>> dataListMap) {
		if (workbook!=null && CollectionUtils.isNotEmpty(dataListMap)) {
			XSSFSheet sheet = workbook.createSheet(EXCEL_SHEET_NAME);//create sheet
			
			int rowIdx = 0;
			this.processHeadRow(sheet, rowIdx);
		    
		    for(int dataIdx=0; dataIdx<dataListMap.size(); dataIdx++) {
		    	rowIdx++;
		    	
		    	this.processBodyRow(sheet, rowIdx, dataListMap.get(dataIdx));
		    }
		}
	}
	
	public void processHeadRow(XSSFSheet sheet, int rowIdx) {
		if (sheet != null) {
		    XSSFRow headRow = sheet.createRow(rowIdx);//create head row
		    for(int cellIdx=0; cellIdx<EXCEL_COLUMN_ORDER.length; cellIdx++ ) {
		    	Column column = EXCEL_COLUMN_ORDER[cellIdx];
		    	
		    	headRow.createCell(cellIdx).setCellValue(NBUtils.getTWMsg(column.getDesc()));
		    	
		    	sheet.setColumnWidth(cellIdx, column.getWidth());
		    }
		}
	}
	
	public void processBodyRow(XSSFSheet sheet, int rowIdx, Map<String, Object> dataMap) {
		if (sheet!=null && MapUtils.isNotEmpty(dataMap)) {
			XSSFRow bodyRow = sheet.createRow(rowIdx);//create body row
			
			for(int cellIdx=0; cellIdx<EXCEL_COLUMN_ORDER.length; cellIdx++) {
				XSSFCell cell = bodyRow.createCell(cellIdx);//create cell
				
				this.processCell(cell, cellIdx, dataMap);
			}
		}
	}
	
	public void processCell(XSSFCell cell, int cellIdx, Map<String, Object> dataMap) {
		if (cell!=null && MapUtils.isNotEmpty(dataMap)) {
			String cellValue = null;
			Column column = EXCEL_COLUMN_ORDER[cellIdx];
			
			cellValue = MapUtils.getString(dataMap, column.getName(), StringUtils.EMPTY);//VALUE
			if (StringUtils.isNotBlank(cellValue)) {
				if (StringUtils.isNotBlank(column.getCodeTableName())) {
					if (Column.DISABILITY_CLASS == column) {//因descStyle="CODE_AND_DESC"，故特殊處理
						cellValue = cellValue + "-" + CodeTable.getCodeDesc(column.getCodeTableName(), cellValue, AppContext.getCurrentUser().getLangId());//CODE_AND_DESC
					} else {
						cellValue = CodeTable.getCodeDesc(column.getCodeTableName(), cellValue, AppContext.getCurrentUser().getLangId());//DESC
					}
				}
			}
			
			cell.setCellType(CellType.STRING);
			cell.setCellValue(cellValue);
		}
	}
	
	public enum Column {
		POLICY_CODE("POLICY_CODE", "MSG_101742", 5*600, null),//保單號碼
		INSURED_CERTI_CODE("INSURED_CERTI_CODE", "MSG_108330", 5*600, null),//被保險人ID
		INSURED_NAME("INSURED_NAME", "MSG_9530", 6*600, null),//被保險人姓名
		INSURED_GENDER("INSURED_GENDER", "MSG_211947", 6*600, "T_GENDER"),//被保險人性別
		PRODUCT_VERSION_TYPE("ProductVersionType", "MSG_20190412611", 7*600, null),//險種-版本-型別
		COVERAGE_PERIOD("COVERAGE_PERIOD", "MSG_205940", 7*600, "T_COVERAGE_PERIOD"),//保障年期類型
		COVERAGE_YEAR("COVERAGE_YEAR", "MSG_223424", 4*600, null),//保障年期
		DISABILITY_TYPE_DESC("DISABILITY_TYPE_DESC", "MSG_1250819", 42*600, null),//身心障礙類別
		DISABILITY_CLASS("DISABILITY_CLASS", "MSG_1250821", 6*600, "T_DISABILITY_CLASS"),//身心障礙等級
		CANCEL_REASON_TYPE1("CANCEL_REASON_TYPE1", "MSG_1264649", 7*600, "T_DISABILITY_CANCEL_TYPE1"),//未承保原因類別
		CANCEL_REASON("CANCEL_REASON", "MSG_1264650", 7*600, null);//未承保原因項目
		
		private String name;
		private String desc;
		private Integer width;//1中文字寬600，視為1單位
		private String codeTableName;

		Column(String name, String desc, Integer width, String codeTableName) {
			this.name = name;
			this.desc = desc;
			this.width = width;
			this.codeTableName = codeTableName;
		}

		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getDesc() {
			return desc;
		}
		public void setDesc(String desc) {
			this.desc = desc;
		}
		public Integer getWidth() {
			return width;
		}
		public void setWidth(Integer width) {
			this.width = width;
		}
		public String getCodeTableName() {
			return codeTableName;
		}
		public void setCodeTableName(String codeTableName) {
			this.codeTableName = codeTableName;
		}
	}
	
}
