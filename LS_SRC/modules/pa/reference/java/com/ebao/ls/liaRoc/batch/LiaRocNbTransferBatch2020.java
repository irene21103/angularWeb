package com.ebao.ls.liaRoc.batch;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.transaction.UserTransaction;

import org.apache.commons.collections.CollectionUtils;

import com.ebao.foundation.module.util.i18n.CodeTable;
import com.ebao.foundation.module.web.action.ExceptionInfoUtils;
import com.ebao.ls.crs.constant.CRSSysCode;
import com.ebao.ls.cs.logger.ApplicationLogger;
import com.ebao.ls.cs.logger.helper.AppLogDataHelper;
import com.ebao.ls.foundation.vo.GenericEntityVO;
import com.ebao.ls.liaRoc.LiaRocCst;
import com.ebao.ls.liaRoc.batch.vo.ResultMessage;
import com.ebao.ls.liaRoc.bo.LiaRocNbBatchVO;
import com.ebao.ls.liaRoc.bo.LiaRocUpload2020;
import com.ebao.ls.liaRoc.bo.LiaRocUploadCoverage2020;
import com.ebao.ls.liaRoc.bo.LiaRocUploadDetail2020;
import com.ebao.ls.liaRoc.bo.LiaRocUploadDetail2020VO;
import com.ebao.ls.liaRoc.ci.LiaRocNbBatchService;
import com.ebao.ls.liaRoc.ci.LiaRocUpload2020CI;
import com.ebao.ls.liaRoc.data.LiaRocNbBatchDao;
import com.ebao.ls.liaRoc.data.LiaRocUpload2020Dao;
import com.ebao.ls.liaRoc.data.LiaRocUploadCoverage2020Dao;
import com.ebao.ls.liaRoc.data.LiaRocUploadDetail2020Dao;
import com.ebao.ls.liaRoc.vo.LiaRocUpload2020SendVO;
import com.ebao.ls.liaRoc.vo.LiaRocUploadDetail2020WrapperVO;
import com.ebao.ls.liaRoc.vo.LiaRocUploadDetailBaseVO;
import com.ebao.ls.notification.ci.NbEventCI;
import com.ebao.ls.notification.ci.vo.RequestInfoVo;
import com.ebao.ls.notification.ci.vo.SendErrorNotificationResponseVo;
import com.ebao.ls.pa.nb.util.NBUtils;
import com.ebao.ls.pa.pub.bs.coverage.CoverageService;
import com.ebao.ls.pa.pub.vo.CoverageVO;
import com.ebao.ls.pa.pub.vo.InsuredVO;
import com.ebao.ls.pa.pub.vo.PolicyVO;
import com.ebao.ls.pub.CodeCst;
import com.ebao.pub.batch.exe.BatchDB;
import com.ebao.pub.batch.help.BatchHelp;
import com.ebao.pub.batch.type.JobStatus;
import com.ebao.pub.fileresolver.writer.FixedCharLengthWriter;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.para.Para;
import com.ebao.pub.util.BeanUtils;
import com.ebao.pub.util.DateUtils;
import com.ebao.pub.util.StringUtils;
import com.ebao.pub.util.Trans;

/**
 * <h1>新契約預收輸入新式公會通報資料上傳 batch - 收件</h1><p>
 * @since 2020/11/13<p>
 * @author Kathy.Yeh
 * <p>Update Time: </p>
 * <p>Updater: </p>
 * <p>Update Comments: </p>
*/
public class LiaRocNbTransferBatch2020 extends LiaRocUploadBatch {
	
    @Resource(name = LiaRocNbBatchService.BEAN_DEFAULT)
    private LiaRocNbBatchService liaRocNbBatchService;
    
    @Resource(name = LiaRocUpload2020CI.BEAN_DEFAULT)
   private LiaRocUpload2020CI liaRocUpload2020CI;
	
    @Resource(name = LiaRocNbBatchDao.BEAN_DEFAULT)
    private LiaRocNbBatchDao liaRocNbBatchDao;
    
	@Resource(name = LiaRocUploadDetail2020Dao.BEAN_DEFAULT)
	private LiaRocUploadDetail2020Dao liaRocUploadDetail2020Dao;
	
	@Resource(name = CoverageService.BEAN_DEFAULT)
	protected CoverageService coverageService;
	
	@Resource(name = LiaRocUploadReceiveBatch.BEAN_DEFAULT)
	protected LiaRocUploadReceiveBatch liaRocUploadReceiveBatch;

	@Resource(name = com.ebao.ls.pa.pub.bs.PolicyService.BEAN_DEFAULT)
	private com.ebao.ls.pa.pub.bs.PolicyService policyDS;
 
	@Resource(name = LiaRocUpload2020Dao.BEAN_DEFAULT)
	private LiaRocUpload2020Dao liaRocUpload2020Dao;
	
	@Resource(name = LiaRocUploadCoverage2020Dao.BEAN_DEFAULT)
	private LiaRocUploadCoverage2020Dao liaRocUploadCoverage2020Dao;
	
	@Resource(name = NbEventCI.BEAN_DEFAULT)
	private NbEventCI nbEventCI;
	
	public static final String BEAN_DEFAULT = "liaRocNbTransferBatch2020";

    @Override
    protected String getUploadType() {
        return LiaRocCst.LIAROC_UL_TYPE_RECEIVE;
    }

    @Override
    protected String getUploadModule() {
        return LiaRocCst.LIAROC_UL_UPLOAD_TYPE_NB;
    }

    @Override
    public int mainProcess() throws Exception {
    	
    	ApplicationLogger.clear();
		ApplicationLogger.setSystemCode(AppLogDataHelper.SYSTEM_CODE_UNB);
		ApplicationLogger.setJobName("新契約預收輸入新式公會收件通報2020");
		ApplicationLogger.setPolicyCode("LiarocNbBatch2020");
		ApplicationLogger.setOptionId(DateUtils.date2String(new Date(), "yyyyMMdd HHmmss"));

		if (BatchDB.getSubId() != null) {
			ApplicationLogger.addLoggerData("[INFO]Batch Job Starting.. RunId=" + BatchHelp.getRunId());
		}

	
		//處理預收輸入尚未做公會收件通報的資料(1 key)
		ResultMessage result01 = this.changeType01Process();
		
		//做ID/生日變更通報12(找出曾通報過明細)/15(保單現況)後再依序執行保額異動/新/刪/修等通報
		ResultMessage result02 = this.changeType02Process();
				
		//PCR-391319 保額/單位/計劃別有異動通報15
		ResultMessage result06 = this.changeType06Process();
		
		//刪除險種通報06
		ResultMessage result04 = this.changeType04Process();
		
		//新增險種通報15
		ResultMessage result03 = this.changeType03Process();

		//變更險種，舊險種通報06，新險種通報15
		ResultMessage result05 = this.changeType05Process();
		
		//13-承保UNDO後通報承保06，當天又承保
		ResultMessage result13 = this.changeType13Process();
		
		//14-撤件UNDO後通報收件01，當天又撤件
		ResultMessage result14 = this.changeType14Process();
		
		
		// PCR-507841 取出所有 process 的 errorMessage，統一發信
		List<String> errorList = new ArrayList<String>();
		errorList.addAll(result01.getErrorList());
		errorList.addAll(result02.getErrorList());
		errorList.addAll(result03.getErrorList());
		errorList.addAll(result04.getErrorList());
		errorList.addAll(result05.getErrorList());
		errorList.addAll(result06.getErrorList());
		errorList.addAll(result13.getErrorList());
		errorList.addAll(result14.getErrorList());
		
		if(CollectionUtils.isNotEmpty(errorList)) {

			// 取得要發的信箱設定
			List<String> params = Arrays.asList(
					Para.getParaValue(LiaRocCst.LIAROC_ALERT_EMAIL_ASD_TO),
					Para.getParaValue(LiaRocCst.LIAROC_ALERT_EMAIL_UNB_CC),
					Para.getParaValue(LiaRocCst.LIAROC_ALERT_EMAIL_POS_CC));

			List<String> emails = NBUtils.splitMultiple(params, ",");
			
			SendErrorNotificationResponseVo result = nbEventCI.sendErrorNotification(
					errorList, emails, new RequestInfoVo(CRSSysCode.SYS_CODE__UNB, getClass().getSimpleName()));
			
			if (result != null && result.isSuccess()) {
				ApplicationLogger.addLoggerData("call nbEventCI.sendErrorNotification SUCCESS. EmailLaunchId:" + result.getEmailLaunchId());
			} else {
				String errorMsg = result != null ? result.getErrorMessage() : "sendErrorNotification response is null";
				ApplicationLogger.addLoggerData("call nbEventCI.sendErrorNotification 發生錯誤.Error Message:" + errorMsg);
			}
		}
		
		
        ApplicationLogger.flush();

		if ((result01.getExecuteResult() == JobStatus.EXECUTE_FAILED) || 
			(result02.getExecuteResult() == JobStatus.EXECUTE_FAILED) ||
			(result03.getExecuteResult() == JobStatus.EXECUTE_FAILED) ||
			(result04.getExecuteResult() == JobStatus.EXECUTE_FAILED) ||
			(result05.getExecuteResult() == JobStatus.EXECUTE_FAILED) ||
			(result06.getExecuteResult() == JobStatus.EXECUTE_FAILED) || 
			(result13.getExecuteResult() == JobStatus.EXECUTE_FAILED) || 
			(result14.getExecuteResult() == JobStatus.EXECUTE_FAILED) ) {
			return com.ebao.pub.batch.type.JobStatus.EXECUTE_PARTIAL_SUCESS;
		}

		return com.ebao.pub.batch.type.JobStatus.EXECUTE_SUCCESS;
    }

	/**
     * 處理預收輸入尚未做新式公會收件通報的資料(1 key)，通報01
     */
    protected ResultMessage changeType01Process() throws Exception {
        // 批處理狀況預設為成功
        int executeResult = JobStatus.EXECUTE_SUCCESS;
        
        // PCR-507841_公會通報系統報錯新增系統警示
        ResultMessage result = new ResultMessage();
        List<String> errorList = new ArrayList<String>();
        
    	// 依序處理控制檔
		int errorCount = 0;
		int succCount = 0;
		String changeType = LiaRocCst.LIAROC_NB_BATCH_CHANGE_TYPE_01;
        UserTransaction trans = null;

		
		//取得預收輸入尚未做公會收件通報的資料
		List<LiaRocNbBatchVO> nbBatchVOs = liaRocNbBatchService.getLiaRocNbBatchData2020(changeType);

    	for(LiaRocNbBatchVO nbBatchVO : nbBatchVOs) {
    		try {
	    		trans = Trans.getUserTransaction();
				trans.begin();

	    		if ((CodeCst.SUBMIT_CHANNEL__MPOS.equals(nbBatchVO.getSubmitChannel())) 
	    		    && (NBUtils.isBRBD(nbBatchVO.getChannelType().toString()))) {
	    			// mpos外部通路(BR,BD)進件時不申報死亡給付險種(99)，由Afins進件做申報
	    			try{
	    				
		    			Long uploadId = liaRocUpload2020CI.sendmPosNB(nbBatchVO.getPolicyId(),LiaRocCst.LIAROC_UL_TYPE_RECEIVE);
		    			if (uploadId != null) {
		    				nbBatchVO.setCheckStatus2020(LiaRocCst.LIAROC_NB_BATCH_CHECK_STATUS_YES);
		    				nbBatchVO.setUpload2020ListId(uploadId);
		    				liaRocNbBatchService.save(nbBatchVO);
		    				succCount++;
		    			}
	    			}catch (Throwable e) {
	    				nbBatchVO.setCheckStatus2020(LiaRocCst.LIAROC_NB_BATCH_CHECK_STATUS_ERROR);
	    				liaRocNbBatchService.save(nbBatchVO);
	    				errorCount++;
	    				String message = ExceptionInfoUtils.getExceptionMsg(e);
	    	        	ApplicationLogger.addLoggerData("[ERROR]2020 call liaRocUploadCI.sendmPosNB 發生不可預期的錯誤, "
	    	        			+ "PolicyId:" + nbBatchVO.getPolicyId() 
								+ "Error Message:" + message);
	    	        	
						errorList.add("[ERROR]2020 call liaRocUploadCI.sendmPosNB 發生不可預期的錯誤;PolicyId:" + nbBatchVO.getPolicyId() + ";" + message);
	    			}
	    		}else{
	                // 一般NB收件通報
	    			try {
		                Long uploadId = liaRocUpload2020CI.sendNB(nbBatchVO.getPolicyId(),LiaRocCst.LIAROC_UL_TYPE_RECEIVE);
		    			if (uploadId != null) {
		    				nbBatchVO.setCheckStatus2020(LiaRocCst.LIAROC_NB_BATCH_CHECK_STATUS_YES);
		    				nbBatchVO.setUpload2020ListId(uploadId);
		    				liaRocNbBatchService.save(nbBatchVO);
		    				succCount++;
		    			}
	    			}catch (Throwable e) {
	    				nbBatchVO.setCheckStatus2020(LiaRocCst.LIAROC_NB_BATCH_CHECK_STATUS_ERROR);
	    				liaRocNbBatchService.save(nbBatchVO);
	    				errorCount++;
	    	        	String message = ExceptionInfoUtils.getExceptionMsg(e);
	    	        	ApplicationLogger.addLoggerData("[ERROR]2020 call liaRocUploadCI.sendNB 發生不可預期的錯誤, "
	    	        			+ "PolicyId:" + nbBatchVO.getPolicyId() 
	    	        			+ "Error Message:" + message);
	    	        	
						errorList.add("[ERROR]2020 call liaRocUploadCI.sendNB 發生不可預期的錯誤;PolicyId:" + nbBatchVO.getPolicyId() + ";" + message);
	    			}
	    		}
	    		trans.commit();
			} catch (Throwable e) {
	        	trans.rollback();
	        	String message = ExceptionInfoUtils.getExceptionMsg(e);
	        	ApplicationLogger.addLoggerData("[ERROR]2020 changeType01Process發生不可預期的錯誤, Error Message:" + message);
	            executeResult = JobStatus.EXECUTE_FAILED;
	            ApplicationLogger.setReturnCode(ApplicationLogger.RETURN_CODE_FAIL);

				errorList.add("[ERROR]2020 changeType01Process發生不可預期的錯誤;PolicyId:" + nbBatchVO.getPolicyId() + ";" + message);
			}
        }

		ApplicationLogger.addLoggerData("[INFO] 預收輸入批次寫入新式公會收件上傳檔(1Key)" 
				+ ";寫檔處理,成功=" + succCount + ",失敗=" + errorCount);

		
		result.setExecuteResult(executeResult);
		result.setErrorList(errorList);
		
		return result;
    }

    /**
     * 處理被保人ID/生日變更產生通報12/15新式公會收件通報的資料
     */
    protected ResultMessage changeType02Process() throws Exception {
		
        // 批處理狀況預設為成功
        int executeResult = JobStatus.EXECUTE_SUCCESS;
    	
        // PCR-507841_公會通報系統報錯新增系統警示
        ResultMessage result = new ResultMessage();
        List<String> errorList = new ArrayList<String>();
        
    	// 依序處理控制檔
		int errorCount = 0;
		int succCount = 0;
		String changeType = LiaRocCst.LIAROC_NB_BATCH_CHANGE_TYPE_02;
        UserTransaction trans = null;

		//取得被保人ID/生日變更資料
		List<LiaRocNbBatchVO> nbBatchVOs = liaRocNbBatchService.getLiaRocNbBatchData2020(changeType);

    	for(LiaRocNbBatchVO nbBatchVO : nbBatchVOs) {

    		try {
	    		trans = Trans.getUserTransaction();
				trans.begin();
				
				PolicyVO policyVO = policyDS.retrieveById(nbBatchVO.getPolicyId(), false);
				
				//比對ID不同或是生日不同才要做12/15更正通報
				if (!nbBatchVO.getCertiCodeBf().equals(nbBatchVO.getCertiCodeAf()) ||
						!nbBatchVO.getBirthDateBf().equals(nbBatchVO.getBirthDateAf())) {
					
					//同一批次處理若有變更險種+ID變更，要先找出變更前險種對應的取號檔將itemId = -1讓變更後的險種可以重新取號
					//2021/08/09 Add by Kathy
					List<LiaRocNbBatchVO> nbBatchVO05s = 
							liaRocNbBatchService.getLiaRocNbBatchData2020ByChangeType(nbBatchVO.getPolicyId(),LiaRocCst.LIAROC_NB_BATCH_CHANGE_TYPE_05);
			    	for(LiaRocNbBatchVO nbBatchVO05 : nbBatchVO05s) {
						//變更前險種的通報公會保單號碼找出對應的t_liaroc_upload_coverage_2020
						LiaRocUploadCoverage2020 uploadCoverage = null;
						
						if (nbBatchVO.getPolicyId()!= null && nbBatchVO.getPolicyId().longValue() > 0L) {
							uploadCoverage = liaRocUploadCoverage2020Dao.findByPolicyIdItemId(
											nbBatchVO05.getPolicyId(), nbBatchVO05.getChangeItemId());
						}
						if (uploadCoverage != null) {
							//將變更前的保項item_id = -1 做註銷
							uploadCoverage.setItemId(-1L);
							liaRocUploadCoverage2020Dao.saveorUpdate(uploadCoverage);
						}
			    	}
					
					// make 公會上傳主檔VO
					LiaRocUpload2020SendVO uploadVO = liaRocUpload2020CI.createLiarocUploadMaster(policyVO,
							LiaRocCst.LIAROC_UL_TYPE_RECEIVE, LiaRocCst.LIAROC_UL_BIZ_SOURCE_UNB);
					
					List<LiaRocUploadDetail2020VO> fixnewDetails = new ArrayList<LiaRocUploadDetail2020VO>();

					boolean isStatus06 = false;
					// 以變更前被保人ID/生日 取出最新一筆公會收件通報資料
					List<LiaRocUploadDetail2020> lastUploadDetails = liaRocUploadDetail2020Dao
							.queryLastReceiveUploadDetailByCertiCode(String.valueOf(nbBatchVO.getChannelType()),
									policyVO.getPolicyNumber(), nbBatchVO.getCertiCodeBf(), nbBatchVO.getApplyDate(),
									policyVO.getSerialNum(), null, isStatus06,nbBatchVO.getBirthDateBf());

					// 確認有曾做過公會收件通報，執行通報12-鍵值欄位通報錯誤終止
					if (CollectionUtils.isNotEmpty(lastUploadDetails)) {

						for (LiaRocUploadDetail2020 liaRocUploadDetail : lastUploadDetails) {

							LiaRocUploadDetail2020 detailBo12 = this.createFixUploadDetail(liaRocUploadDetail, null,
									LiaRocCst.LIAROC_POLICY_STATUS__ERROR_TERMINATION);

							LiaRocUploadDetail2020VO detailVO12 = new LiaRocUploadDetail2020VO();
							detailBo12.copyToVO(detailVO12, Boolean.TRUE);
							detailVO12.setStatusEffectDate(DateUtils.truncateDay(AppContext.getCurrentUserLocalTime()));
							detailVO12.setCheckStatus(LiaRocCst.CHECK_STATUS_UPLOAD);
							detailVO12.setRefUploadListId(null);
							
							fixnewDetails.add(detailVO12);

						}
						// 取得變更後被保險人 partyId 產生15-更正通報
						for (InsuredVO insuredVO : policyVO.getInsureds()) {
							if (nbBatchVO.getCertiCodeAf().equals(insuredVO.getCertiCode())){
								List<CoverageVO> coverageList = policyVO.gitCoveragesByInsured(insuredVO.getListId());
								LiaRocUploadDetail2020VO detailVO15 = new LiaRocUploadDetail2020VO();
								//取得公會上傳明細檔
								List<LiaRocUploadDetail2020WrapperVO> genUploadDetails = liaRocUpload2020CI.generateNBBatchUploadDetail(
										policyVO, coverageList, LiaRocCst.LIAROC_UL_TYPE_RECEIVE, changeType);

								if (CollectionUtils.isNotEmpty(genUploadDetails)) {
							        for (int i = 0; i < genUploadDetails.size(); i++) {
										detailVO15 = genUploadDetails.get(i);
										detailVO15.setLiaRocPolicyStatus(LiaRocCst.LIAROC_POLICY_STATUS__FIX_ERROR);
										detailVO15.setStatusEffectDate(DateUtils.truncateDay(AppContext.getCurrentUserLocalTime()));
										//IR-376932 取出上筆公會通報資料後把通報狀態check_status = 1-可上傳，清空RefUploadListId
										detailVO15.setCheckStatus(LiaRocCst.CHECK_STATUS_UPLOAD);			
										detailVO15.setRefUploadListId(null);
										fixnewDetails.add(detailVO15);
									}
								}
							}
						}
					}
					uploadVO.setDataList(fixnewDetails);
					// 寫入公會收件通報上傳主檔和明細檔
					liaRocUpload2020CI.saveUpload(uploadVO);

					if (uploadVO.getListId() != null) {
						nbBatchVO.setCheckStatus2020(LiaRocCst.LIAROC_NB_BATCH_CHECK_STATUS_YES);
						nbBatchVO.setUpload2020ListId(uploadVO.getListId());
						liaRocNbBatchService.save(nbBatchVO);
						succCount++;
					} else {
						errorCount++;
					}
				}else{
					//變更前、變更後ID若一樣就不做收件通報處理
					nbBatchVO.setCheckStatus2020(LiaRocCst.LIAROC_NB_BATCH_CHECK_STATUS_Z);
					liaRocNbBatchService.save(nbBatchVO);
					succCount++;
				}
				//做完ID/生日變更寫入12/15後，將同一批同一保單同一ID有新/刪/修/保額異動之變更項註記成'Z'不處理，避免重覆通報
				List<String> certiCode = new ArrayList<String>();
				certiCode.add(nbBatchVO.getCertiCodeBf());
				certiCode.add(nbBatchVO.getCertiCodeAf());
				liaRocNbBatchDao.updateCheckStatusToZ(nbBatchVO.getPolicyId(), certiCode);
				trans.commit();
    		} catch (Throwable e) {
	        	trans.rollback();
	        	String message = ExceptionInfoUtils.getExceptionMsg(e);
	        	ApplicationLogger.addLoggerData("[ERROR]2020 changeType02Process發生不可預期的錯誤, Error Message:" + message);
	            executeResult = JobStatus.EXECUTE_FAILED;
	            ApplicationLogger.setReturnCode(ApplicationLogger.RETURN_CODE_FAIL);
	            
				errorList.add("[ERROR]2020 changeType02Process發生不可預期的錯誤;PolicyId:" + nbBatchVO.getPolicyId() + ";" + message);
			} 
    	}

		ApplicationLogger.addLoggerData("[INFO] 預收輸入批次寫入新式公會收件上傳檔(ID/生日變更)" 
				+ ";寫檔處理,成功=" + succCount + ",失敗=" + errorCount);

		result.setExecuteResult(executeResult);
		result.setErrorList(errorList);
		return result;
    }

    /**
     * 處理新增險種需產生通報15新式公會收件通報的資料
     */
    protected ResultMessage changeType03Process() throws Exception {

        // 批處理狀況預設為成功
        int executeResult = JobStatus.EXECUTE_SUCCESS;

        // PCR-507841_公會通報系統報錯新增系統警示
        ResultMessage result = new ResultMessage();
        List<String> errorList = new ArrayList<String>();
        
    	// 依序處理控制檔
		int errorCount = 0;
		int succCount = 0;
		String changeType = LiaRocCst.LIAROC_NB_BATCH_CHANGE_TYPE_03;
        UserTransaction trans = null;
        
		//取得新增險種未處理資料
		List<LiaRocNbBatchVO> nbBatchVOs = liaRocNbBatchService.getLiaRocNbBatchData2020(changeType);
		//資料重整為 Map<ruleName,List<policyId>>
		Map<String, List<LiaRocNbBatchVO>> nbBatchMap = NBUtils.toMapList(nbBatchVOs, "policyId");
    	
		 //by policyId group
		 for(String policyId : nbBatchMap.keySet() ) {
			 try{
	    		trans = Trans.getUserTransaction();
				trans.begin();
	
				PolicyVO policyVO = policyDS.retrieveById(Long.parseLong(policyId), false);
				//make 公會上傳主檔VO
				LiaRocUpload2020SendVO uploadVO = liaRocUpload2020CI.createLiarocUploadMaster
						(policyVO, LiaRocCst.LIAROC_UL_TYPE_RECEIVE, LiaRocCst.LIAROC_UL_BIZ_SOURCE_UNB);
	
				List<LiaRocUploadDetail2020VO> newDetails = new ArrayList<LiaRocUploadDetail2020VO>();
				
				for (LiaRocNbBatchVO nbBatchVO : nbBatchMap.get(policyId)) {
					List<CoverageVO> coverageList = new ArrayList<CoverageVO>();
					CoverageVO coverageVO = coverageService.load(nbBatchVO.getChangeItemId());
					if (coverageVO.getPolicyId() > 0) {
						coverageList.add(coverageVO);
						//以目前保項對應的被保險人CERTI_CODE去查是否曾有通報過公會資料
						InsuredVO insuredVO = coverageVO.getLifeInsured1().getInsured();
						
						LiaRocUploadDetail2020VO detailVO01 = new LiaRocUploadDetail2020VO();
						// 取得公會上傳明細檔
						List<LiaRocUploadDetail2020WrapperVO> genUploadDetails = liaRocUpload2020CI
								.generateNBBatchUploadDetail(policyVO, coverageList, LiaRocCst.LIAROC_UL_TYPE_RECEIVE,
										changeType);

						// 執行通報15-通報更正
						if (CollectionUtils.isNotEmpty(genUploadDetails)) {
							detailVO01 = genUploadDetails.get(0);
							detailVO01.setLiaRocPolicyStatus(LiaRocCst.LIAROC_POLICY_STATUS__FIX_ERROR);

							// PCR-439788 保單狀況生效日 : 以異動生效日通報
							if (nbBatchVO.getSystemDate() != null) {
								detailVO01.setStatusEffectDate(nbBatchVO.getSystemDate());
							}
						}

						boolean isUnb = true;
						// PCR-337603 比對保單狀態是否需要含06-撤件 2020/11/20 Add by Kathy
						boolean isLiaRocPolicyStatus06 = true;

						// 以新增險種取出最近一筆有無公會收件通報資料
						// 因多險種，新增險種時查找先前通報記錄需排除通報取號已存在的(若有刪除的記錄要延用上次刪除的取號檔)
						List<LiaRocUploadDetail2020> lastUploadDetails = filterByLiarocCoverage(
								liaRocUploadDetail2020Dao.queryLastReceiveUploadDetail(LiaRocCst.LIAROC_UL_BIZ_SOURCE_UNB,
										String.valueOf(nbBatchVO.getChannelType()), policyVO.getPolicyNumber(),
										insuredVO.getCertiCode(), nbBatchVO.getInternalIdBf(),
										nbBatchVO.getApplyDate(), policyVO.getSerialNum(), policyVO.getMegaSerialNum(), LiaRocCst.IS_1KEY_FALSE,
										isLiaRocPolicyStatus06, nbBatchVO.getProposalTerm(), nbBatchVO.getChargeYear()),
								nbBatchVO.getPolicyId());

						// 已通報此處不作通報
						if (lastUploadDetails.size() != 0) {
							LiaRocUploadDetail2020 lastUploadDetail = lastUploadDetails.get(0);
							// 上一筆通報狀態不為06-撤件
							if (!LiaRocCst.LIAROC_POLICY_STATUS__FREELOOK
									.equals(lastUploadDetail.getLiaRocPolicyStatus())) {
								// 更新狀態為2-不通報，它件已通報
								detailVO01.setCheckStatus(LiaRocCst.CHECK_STATUS_CANCEL_BY_AFINS);
								detailVO01.setRefUploadListId(lastUploadDetail.getListId());
								detailVO01.setLiaRocPolicyCode(lastUploadDetail.getLiaRocPolicyCode());
							}
						}

						// 更新公會通報保單取號檔(T_LIAROC_UPLOAD_COVERAGE_2020)
						LiaRocUploadCoverage2020 uploadCoverage = null;
						if (nbBatchVO.getPolicyId() != null && nbBatchVO.getPolicyId().longValue() > 0L) {
							uploadCoverage = liaRocUploadCoverage2020Dao.findByPolicyIdLiaRocPolicyCode(
									nbBatchVO.getPolicyId(), detailVO01.getLiaRocPolicyCode());
						}
						
						//不為AFINS時間序才回寫取號檔
						if(NBUtils.isAFINSDatetimeStr(detailVO01.getLiaRocPolicyCode()) == false) {
							if (uploadCoverage != null) {
								// 將本次保項編號(item_id)回寫T_LIAROC_UPLOAD_COVERAGE_2020.ITEM_ID
								uploadCoverage.setItemId(nbBatchVO.getChangeItemId());
								liaRocUploadCoverage2020Dao.saveorUpdate(uploadCoverage);
							}else if (uploadCoverage == null) {
								//將本次通報公會保單號碼回寫取號檔(T_LIAROC_UPLOAD_COVERAGE_2020)
								liaRocUpload2020CI.syncLiarocUploadCoveragePOS(nbBatchVO.getPolicyId(), nbBatchVO.getChangeItemId(), detailVO01.getLiaRocPolicyCode());
							}
						}
					
						if (detailVO01.getLiaRocPolicyCode() != null) {
							newDetails.add(detailVO01);
							succCount++;
						} else {
							errorCount++;
						}
						uploadVO.setDataList(newDetails);
					}
				}
				//寫入公會收件通報上傳主檔和明細檔
		    	liaRocUpload2020CI.saveUpload(uploadVO);
				
				for(LiaRocNbBatchVO nbBatchVO : nbBatchMap.get(policyId)) {
					nbBatchVO.setCheckStatus2020(LiaRocCst.LIAROC_NB_BATCH_CHECK_STATUS_YES);
					if (uploadVO.getListId() != null) {
						nbBatchVO.setUpload2020ListId(uploadVO.getListId());
					}
					liaRocNbBatchService.save(nbBatchVO);
				}
				trans.commit();
			} catch (Throwable e) {
	        	trans.rollback();
	        	String message = ExceptionInfoUtils.getExceptionMsg(e);
	        	ApplicationLogger.addLoggerData("[ERROR]2020 changeType03Process發生不可預期的錯誤, Error Message:" + message);
	            executeResult = JobStatus.EXECUTE_FAILED;
	            ApplicationLogger.setReturnCode(ApplicationLogger.RETURN_CODE_FAIL);

				errorList.add("[ERROR]2020 changeType03Process發生不可預期的錯誤;PolicyId:" + policyId + ";" + message);
			} 
    	}

		ApplicationLogger.addLoggerData("[INFO] 預收輸入批次寫入新式公會收件上傳檔(新增險種)" 
				+ ";寫檔處理,成功=" + succCount + ",無資料筆數=" + errorCount);

		result.setExecuteResult(executeResult);
		result.setErrorList(errorList);
		return result;
    }
    
    
    /**
     * 處理刪除險種需產生通報06新式公會收件通報的資料
     */
    protected ResultMessage changeType04Process() throws Exception {

        // 批處理狀況預設為成功
        int executeResult = JobStatus.EXECUTE_SUCCESS;

        // PCR-507841_公會通報系統報錯新增系統警示
        ResultMessage result = new ResultMessage();
        List<String> errorList = new ArrayList<String>();
        
    	// 依序處理控制檔
		int errorCount = 0;
		int succCount = 0;
		String changeType = LiaRocCst.LIAROC_NB_BATCH_CHANGE_TYPE_04;
        UserTransaction trans = null;
        
		//取得刪除險種未處理資料
		List<LiaRocNbBatchVO> nbBatchVOs = liaRocNbBatchService.getLiaRocNbBatchData2020(changeType);
		//資料重整為 Map<ruleName,List<policyId>>
		Map<String, List<LiaRocNbBatchVO>> nbBatchMap = NBUtils.toMapList(nbBatchVOs, "policyId");
    	
		 for(String policyId : nbBatchMap.keySet() ) {
			 try{
	    		trans = Trans.getUserTransaction();
				trans.begin();
				
				boolean isUnb = true;
		
				//PCR-337603 比對保單狀態是否需要含06-撤件 2020/11/20 Add by Kathy
				boolean isLiaRocPolicyStatus06 = false;
				
				PolicyVO policyVO = policyDS.retrieveById(Long.parseLong(policyId), false);
				//make 公會上傳主檔VO
				LiaRocUpload2020SendVO uploadVO = liaRocUpload2020CI.createLiarocUploadMaster
						(policyVO, LiaRocCst.LIAROC_UL_TYPE_RECEIVE, LiaRocCst.LIAROC_UL_BIZ_SOURCE_UNB);
	
				List<LiaRocUploadDetail2020VO> fixnewDetails = new ArrayList<LiaRocUploadDetail2020VO>();
				
		    	for(LiaRocNbBatchVO nbBatchVO : nbBatchMap.get(policyId)) {
					//以刪除險種取出最近一筆公會收件通報資料
		    		//因多險種，增加比對已通報保項的通報取號檔
					List<LiaRocUploadDetail2020> lastUploadDetails = selectByLiarocPolicyCode(liaRocUploadDetail2020Dao.queryLastReceiveUploadDetail(
									LiaRocCst.LIAROC_UL_BIZ_SOURCE_UNB, 
									String.valueOf(nbBatchVO.getChannelType()),
									policyVO.getPolicyNumber(), 
									nbBatchVO.getCertiCodeBf(), 
									nbBatchVO.getInternalIdBf(), 
									nbBatchVO.getApplyDate(), 
									policyVO.getSerialNum(),
									policyVO.getMegaSerialNum(),
									LiaRocCst.IS_1KEY_FALSE,
									isLiaRocPolicyStatus06,
									nbBatchVO.getProposalTerm(),
									nbBatchVO.getChargeYear()), nbBatchVO);
					
					boolean isStatus06 = true;
					//以刪除險種的被保人ID取出有無做過15-更正通報
					//因多險種，增加比對已通報保項的通報取號檔
					List<LiaRocUploadDetail2020> lastUpload15Details = selectByLiarocPolicyCode(liaRocUploadDetail2020Dao.queryLastReceiveUploadDetailByCertiCode(
									String.valueOf(nbBatchVO.getChannelType()),
									policyVO.getPolicyNumber(), 
									nbBatchVO.getCertiCodeBf(),
									nbBatchVO.getApplyDate(), 
									policyVO.getSerialNum(),
									nbBatchVO.getInternalIdBf(),
									isStatus06,
									null), nbBatchVO);
				
					//執行通報06-終止通報
					if (CollectionUtils.isNotEmpty(lastUploadDetails)) {
						LiaRocUploadDetail2020 lastUploadDetail = lastUploadDetails.get(0);
						LiaRocUploadDetail2020 detailBo06 = this.createFixUploadDetail(lastUploadDetail, null, LiaRocCst.LIAROC_POLICY_STATUS__FREELOOK);
						LiaRocUploadDetail2020VO detailVO06 = new LiaRocUploadDetail2020VO();
						detailBo06.copyToVO(detailVO06, Boolean.TRUE);
						
						if (CollectionUtils.isNotEmpty(lastUpload15Details)) {
							LiaRocUploadDetail2020 lastUpload15Detail = lastUpload15Details.get(0);
							//在做06通報前再去確認是否有做過ID/生日變更通報15，若有以新ID/新生日做06-終止通報
							if (!StringUtils.isNullOrEmpty(detailVO06.getCertiCode())  &&
									!StringUtils.isNullOrEmpty(lastUpload15Detail.getCertiCode())) {
								if (detailVO06.getCertiCode() != lastUpload15Detail.getCertiCode()){
									detailVO06.setCertiCode(lastUpload15Detail.getCertiCode());
								}								
							}
							if (detailVO06.getBirthday() != null && lastUpload15Detail.getBirthday()  != null){
								if (detailVO06.getBirthday().compareTo(lastUpload15Detail.getBirthday()) != 0){
									detailVO06.setBirthday(lastUpload15Detail.getBirthday());
								}								
							}
						}

						detailVO06.setStatusEffectDate(DateUtils.truncateDay(AppContext.getCurrentUserLocalTime()));
						fixnewDetails.add(detailVO06);
						succCount++;
					}else{
						errorCount++;
					}
		
					uploadVO.setDataList(fixnewDetails);
				}
				//寫入公會收件通報上傳主檔和明細檔
		    	liaRocUpload2020CI.saveUpload(uploadVO);
				
				for(LiaRocNbBatchVO nbBatchVO : nbBatchMap.get(policyId)) {
					nbBatchVO.setCheckStatus2020(LiaRocCst.LIAROC_NB_BATCH_CHECK_STATUS_YES);
					if (uploadVO.getListId() != null) {
						nbBatchVO.setUpload2020ListId(uploadVO.getListId());
					}
					liaRocNbBatchService.save(nbBatchVO);
					
					//刪除險種用item_id找出對應的t_liaroc_upload_coverage
					LiaRocUploadCoverage2020 uploadCoverage = null;
					if (nbBatchVO.getChangeItemId()!= null && nbBatchVO.getChangeItemId().longValue() > 0L) {
						uploadCoverage = liaRocUploadCoverage2020Dao.findByPolicyIdItemId(nbBatchVO.getPolicyId(), nbBatchVO.getChangeItemId());
					}
					if (uploadCoverage != null) {
						//將刪除的保項item_id = -1 做註銷
						uploadCoverage.setItemId(-1L);
						liaRocUploadCoverage2020Dao.saveorUpdate(uploadCoverage);
					}
				}
				trans.commit();
			} catch (Throwable e) {
	        	trans.rollback();
	        	String message = ExceptionInfoUtils.getExceptionMsg(e);
	        	ApplicationLogger.addLoggerData("[ERROR]2020 changeType04Process發生不可預期的錯誤, Error Message:" + message);
	            executeResult = JobStatus.EXECUTE_FAILED;
	            ApplicationLogger.setReturnCode(ApplicationLogger.RETURN_CODE_FAIL);
	            
				errorList.add("[ERROR]2020 changeType04Process發生不可預期的錯誤;PolicyId:" + policyId + ";" + message);
			} 
    	}

		ApplicationLogger.addLoggerData("[INFO] 預收輸入批次寫入新式公會收件上傳檔(刪除險種)" 
				+ ";寫檔處理,成功=" + succCount + ",無資料筆數=" + errorCount);

		result.setExecuteResult(executeResult);
		result.setErrorList(errorList);
		return result;
    }
    
    /**
     * 過濾該保項存在通報取號檔且舊的通報明細檔得通報號碼與取檔檔的號碼相同的資料。
     * (因多險種，故在變更查找舊的通報明細，增加比對已通報保項的通報取號檔)
     * @param lastUploadDetails
     * @param uploadCoverage
     * @return
     */
    private List<LiaRocUploadDetail2020> selectByLiarocPolicyCode(List<LiaRocUploadDetail2020> lastUploadDetails, LiaRocNbBatchVO nbBatchVO) {
    	List<LiaRocUploadDetail2020> fixLastUploadDetails = new ArrayList<LiaRocUploadDetail2020>();
    	
		LiaRocUploadCoverage2020 uploadCoverage = null;
		if (nbBatchVO.getChangeItemId()!= null && nbBatchVO.getChangeItemId().longValue() > 0L) {
			uploadCoverage = liaRocUploadCoverage2020Dao.findByPolicyIdItemId(nbBatchVO.getPolicyId(), 
					nbBatchVO.getChangeItemId());
		}
	    if(uploadCoverage != null) {
	    	for(LiaRocUploadDetail2020 detail2020 : lastUploadDetails) {
	    		if(NBUtils.in(uploadCoverage.getLiarocPolicyCode(), detail2020.getLiaRocPolicyCode())) {
	    			fixLastUploadDetails.add(detail2020);
	    		}
	    	}
	    	if(fixLastUploadDetails.size() > 0) {
	    		return fixLastUploadDetails;
	    	}
	    }
	    return lastUploadDetails;
	}
    
    /**
     * 新增險種查找舊的通報記錄會有多筆，需排除取號檔itemID不為-1的資料(表示已被佔用)
     * @param lastUploadDetails
     * @param policyId
     * @return
     */
    private List<LiaRocUploadDetail2020> filterByLiarocCoverage(List<LiaRocUploadDetail2020> lastUploadDetails, Long policyId) {
    	List<LiaRocUploadDetail2020> filterLastUploadDetails = liaRocUploadCoverage2020Dao.filterByLiarocCoverage(lastUploadDetails, policyId);
	    return filterLastUploadDetails;
	}
    
	/**
     * 處理變更險種需產生通報15-更正通報/06終止新式公會收件通報的資料
     */
    protected ResultMessage changeType05Process() throws Exception {

        // 批處理狀況預設為成功
        int executeResult = JobStatus.EXECUTE_SUCCESS;

        // PCR-507841_公會通報系統報錯新增系統警示
        ResultMessage result = new ResultMessage();
        List<String> errorList = new ArrayList<String>();
        
    	// 依序處理控制檔
		int errorCount = 0;
		int succCount = 0;
		String changeType = LiaRocCst.LIAROC_NB_BATCH_CHANGE_TYPE_05;
        UserTransaction trans = null;
        
		//取得變更險種未處理資料
		List<LiaRocNbBatchVO> nbBatchVOs = liaRocNbBatchService.getLiaRocNbBatchData2020(changeType);
		//資料重整為 Map<ruleName,List<policyId>>
		Map<String, List<LiaRocNbBatchVO>> nbBatchMap = NBUtils.toMapList(nbBatchVOs, "policyId");
    	
		 //by policyId group
		 for(String policyId : nbBatchMap.keySet() ) {
			 try{
	    		trans = Trans.getUserTransaction();
				trans.begin();
	
				PolicyVO policyVO = policyDS.retrieveById(Long.parseLong(policyId), false);
				//make 公會上傳主檔VO
				LiaRocUpload2020SendVO uploadVO = liaRocUpload2020CI.createLiarocUploadMaster
						(policyVO, LiaRocCst.LIAROC_UL_TYPE_RECEIVE, LiaRocCst.LIAROC_UL_BIZ_SOURCE_UNB);

				//PCR-337603 比對保單狀態是否需要含06-撤件 2020/11/20 Add by Kathy
				boolean isLiaRocPolicyStatus06 = false;
				
				List<LiaRocUploadDetail2020VO> newDetails = new ArrayList<LiaRocUploadDetail2020VO>();
	
		    	for(LiaRocNbBatchVO nbBatchVO : nbBatchMap.get(policyId)) {
					String oldUploadKey = null;
					String newUploadKey = null;
					LiaRocUploadDetail2020WrapperVO detailVO06 = new LiaRocUploadDetail2020WrapperVO();		
					LiaRocUploadDetail2020VO detailVO01 = new LiaRocUploadDetail2020VO();

					//取得保項明細檔
		    		List<CoverageVO>coverageList  = new ArrayList<CoverageVO>();
		    		CoverageVO coverageVO = coverageService.load(nbBatchVO.getChangeItemId());
					coverageList.add(coverageVO);
					//以目前保項對應的被保險人CERTI_CODE去查是否曾有通報過公會資料
					InsuredVO insuredVO = coverageVO.getLifeInsured1().getInsured();
					
		    		//以變更前險種取出最近一筆公會收件通報資料
					//因多險種，增加比對已通報保項的通報取號檔
					List<LiaRocUploadDetail2020> lastUploadDetails = selectByLiarocPolicyCode(liaRocUploadDetail2020Dao.queryLastReceiveUploadDetail(
									LiaRocCst.LIAROC_UL_BIZ_SOURCE_UNB, 
									String.valueOf(nbBatchVO.getChannelType()),
									policyVO.getPolicyNumber(), 
									insuredVO.getCertiCode(), 
									nbBatchVO.getInternalIdBf(), 
									nbBatchVO.getApplyDate(), 
									policyVO.getSerialNum(),
									policyVO.getMegaSerialNum(),
									LiaRocCst.IS_1KEY_FALSE,
									isLiaRocPolicyStatus06,
									nbBatchVO.getProposalTerm(),
									nbBatchVO.getChargeYear()), nbBatchVO);	

					boolean isStatus06 = true;
					//以變更前險種代碼+ID取出有無做過15-更正通報
					//因多險種，增加比對已通報保項的通報取號檔
					List<LiaRocUploadDetail2020> lastUpload15Details = selectByLiarocPolicyCode(liaRocUploadDetail2020Dao.queryLastReceiveUploadDetailByCertiCode(
									String.valueOf(nbBatchVO.getChannelType()),
									policyVO.getPolicyNumber(), 
									insuredVO.getCertiCode(),
									nbBatchVO.getApplyDate(), 
									policyVO.getSerialNum(),
									nbBatchVO.getInternalIdBf(),
									isStatus06,
									null), nbBatchVO);		

					if (CollectionUtils.isNotEmpty(lastUploadDetails)) {
						LiaRocUploadDetail2020 lastUploadDetail = lastUploadDetails.get(0);
						LiaRocUploadDetail2020 detailBo06 = this.createFixUploadDetail(lastUploadDetail, null, LiaRocCst.LIAROC_POLICY_STATUS__FREELOOK);
						detailBo06.copyToVO(detailVO06, Boolean.TRUE);
						if (CollectionUtils.isNotEmpty(lastUpload15Details)) {
							LiaRocUploadDetail2020 lastUpload15Detail = lastUpload15Details.get(0);
							//在做06通報前再去確認是否有做過ID/生日變更通報15，若有以新ID/新生日做06-終止通報
							if (!StringUtils.isNullOrEmpty(detailVO06.getCertiCode())  &&
									!StringUtils.isNullOrEmpty(lastUpload15Detail.getCertiCode())) {
								if (detailVO06.getCertiCode().equals(lastUpload15Detail.getCertiCode())== false){
									detailVO06.setCertiCode(lastUpload15Detail.getCertiCode());
								}								
							}
							if (detailVO06.getBirthday() != null && lastUpload15Detail.getBirthday()  != null){
								if (detailVO06.getBirthday().compareTo(lastUpload15Detail.getBirthday()) != 0){
									detailVO06.setBirthday(lastUpload15Detail.getBirthday());
								}								
							}
						}
						oldUploadKey = liaRocUploadReceiveBatch.getUpload2020Key(detailVO06);
						//變更前險種的通報公會保單號碼找出對應的t_liaroc_upload_coverage
						LiaRocUploadCoverage2020 uploadCoverage = null;
						
						if (nbBatchVO.getPolicyId()!= null && nbBatchVO.getPolicyId().longValue() > 0L) {
							uploadCoverage = liaRocUploadCoverage2020Dao.findByPolicyIdLiaRocPolicyCode(
											nbBatchVO.getPolicyId(), detailVO06.getLiaRocPolicyCode());
						}
						if (uploadCoverage != null) {
							//將變更前的保項item_id = -1 做註銷
							uploadCoverage.setItemId(-1L);
							liaRocUploadCoverage2020Dao.saveorUpdate(uploadCoverage);
						}
					}		    		
				
					//以變更後險種取得公會上傳明細檔
					List<LiaRocUploadDetail2020WrapperVO> genUploadDetails = liaRocUpload2020CI.generateNBBatchUploadDetail(
							policyVO, coverageList, LiaRocCst.LIAROC_UL_TYPE_RECEIVE, changeType);
					
					//執行通報15-通報更正
					if (CollectionUtils.isNotEmpty(genUploadDetails)) {
						newUploadKey = liaRocUploadReceiveBatch.getUpload2020Key(genUploadDetails.get(0));
						detailVO01 = genUploadDetails.get(0);
						detailVO01.setLiaRocPolicyStatus(LiaRocCst.LIAROC_POLICY_STATUS__FIX_ERROR);
					}	
					
					//PCR-337603 比對保單狀態是否需要含06-撤件 2020/11/20 Add by Kathy
					isLiaRocPolicyStatus06 = true; 					
		    		//以變更後險種取出最近一筆公會收件通報資料
					LiaRocUploadDetail2020 lastUploadDetail = this.queryLastReceiveUploadDetail(
									LiaRocCst.LIAROC_UL_BIZ_SOURCE_UNB, 
									String.valueOf(nbBatchVO.getChannelType()),
									policyVO.getPolicyNumber(), 
									insuredVO.getCertiCode(), 
									nbBatchVO.getInternalIdAf(), 
									nbBatchVO.getApplyDate(), 
									policyVO.getSerialNum(),
									policyVO.getMegaSerialNum(),
									LiaRocCst.IS_1KEY_FALSE,
									isLiaRocPolicyStatus06,
									nbBatchVO.getProposalTerm(),
									nbBatchVO.getChargeYear(),
									nbBatchVO.getPolicyId());

					//已通報此處不作通報
					if (lastUploadDetail != null) {
						//上一筆通報狀態不為06-撤件
						if (!LiaRocCst.LIAROC_POLICY_STATUS__FREELOOK.equals(lastUploadDetail.getLiaRocPolicyStatus())){
							//更新狀態為2-不通報，它件已通報
							detailVO01.setCheckStatus(LiaRocCst.CHECK_STATUS_CANCEL_BY_AFINS);
							detailVO01.setRefUploadListId(lastUploadDetail.getListId());
							detailVO01.setLiaRocPolicyCode(lastUploadDetail.getLiaRocPolicyCode());							
						}
					}
					//更新公會通報保單取號檔(T_LIAROC_UPLOAD_COVERAGE_2020)
					LiaRocUploadCoverage2020 uploadCoverage = null;
					if (nbBatchVO.getPolicyId() != null && nbBatchVO.getPolicyId().longValue() > 0L) {
						uploadCoverage = liaRocUploadCoverage2020Dao.findByPolicyIdLiaRocPolicyCode(
										nbBatchVO.getPolicyId(), detailVO01.getLiaRocPolicyCode());
					}
					
					//不為AFINS時間序才回寫取號檔
					if(NBUtils.isAFINSDatetimeStr(detailVO01.getLiaRocPolicyCode()) == false) {
						if (uploadCoverage != null && uploadCoverage.getItemId() < 0) { //已被佔用不更新
							//將這次新增item_id 回壓對應的t_liaroc_upload_coverage.item_id
							uploadCoverage.setItemId(nbBatchVO.getChangeItemId());
							liaRocUploadCoverage2020Dao.saveorUpdate(uploadCoverage);
						}else if (uploadCoverage == null) {
							//將本次通報公會保單號碼回寫取號檔(T_LIAROC_UPLOAD_COVERAGE_2020)
							liaRocUpload2020CI.syncLiarocUploadCoveragePOS(nbBatchVO.getPolicyId(), nbBatchVO.getChangeItemId(), detailVO01.getLiaRocPolicyCode());
						}
					}
				
					//若新舊險種若通報公會的key相同
					if (NBUtils.in(oldUploadKey, newUploadKey)) {
						//更新舊險種通報06-終止明細檔狀態為3-不通報,上傳重覆件(key值重覆)
						detailVO06.setCheckStatus(LiaRocCst.CHECK_STATUS_CANCEL_BY_SAMEKEY);
					}
					
					//執行新險種通報15-更正通報，舊險種通報06-終止
					if (detailVO01.getLiaRocPolicyCode() != null) {
						// IR-359765 新增被保險人附約(含眷屬)如跳出視窗點選確認時應以變更的日期通報要保書填寫日
						if (nbBatchVO.getSystemDate() != null) {
							detailVO01.setStatusEffectDate(nbBatchVO.getSystemDate());
						}
						//變更前險種和變更後險種一樣時，註記通報明細檔狀態為3-不通報,上傳重覆件(key值重覆)
						if (NBUtils.in(nbBatchVO.getInternalIdBf(), nbBatchVO.getInternalIdAf())) {
							detailVO06.setCheckStatus(LiaRocCst.CHECK_STATUS_CANCEL_BY_SAMEKEY);
							detailVO01.setCheckStatus(LiaRocCst.CHECK_STATUS_CANCEL_BY_SAMEKEY);
						}							
						
						if (detailVO06.getLiaRocPolicyCode() != null) {
							newDetails.add(detailVO06);
						} 
						newDetails.add(detailVO01);
						succCount++;
					} else {
						errorCount++;
					}
				}
		    	uploadVO.setDataList(newDetails);
				//寫入公會收件通報上傳主檔和明細檔
		    	liaRocUpload2020CI.saveUpload(uploadVO);
				
				for(LiaRocNbBatchVO nbBatchVO : nbBatchMap.get(policyId)) {
					nbBatchVO.setCheckStatus2020(LiaRocCst.LIAROC_NB_BATCH_CHECK_STATUS_YES);
					if (uploadVO.getListId() != null) {
						nbBatchVO.setUpload2020ListId(uploadVO.getListId());
					}
					liaRocNbBatchService.save(nbBatchVO);
				}
				trans.commit();
			} catch (Throwable e) {
	        	trans.rollback();
	        	String message = ExceptionInfoUtils.getExceptionMsg(e);
	        	ApplicationLogger.addLoggerData("[ERROR]2020 changeType05Process發生不可預期的錯誤, Error Message:" + message);
	            executeResult = JobStatus.EXECUTE_FAILED;
	            ApplicationLogger.setReturnCode(ApplicationLogger.RETURN_CODE_FAIL);
	            
				errorList.add("[ERROR]2020 changeType05Process發生不可預期的錯誤;PolicyId:" + policyId + ";" + message);
			} 
    	}

		ApplicationLogger.addLoggerData("[INFO] 預收輸入批次寫入新式公會收件上傳檔(變更險種)" 
				+ ";寫檔處理,成功=" + succCount + ",無資料筆數=" + errorCount);

		result.setExecuteResult(executeResult);
		result.setErrorList(errorList);
		return result;
    }

    /**
     * 處理保額/單位/計劃別變更需產生通報15-更正通報的資料
     */
    protected ResultMessage changeType06Process() throws Exception {

        // 批處理狀況預設為成功
        int executeResult = JobStatus.EXECUTE_SUCCESS;

        // PCR-507841_公會通報系統報錯新增系統警示
        ResultMessage result = new ResultMessage();
        List<String> errorList = new ArrayList<String>();
        
    	// 依序處理控制檔
		int errorCount = 0;
		int succCount = 0;
		String changeType = LiaRocCst.LIAROC_NB_BATCH_CHANGE_TYPE_06;
        UserTransaction trans = null;
        
		//取得保額變更未處理資料
		List<LiaRocNbBatchVO> nbBatchVOs = liaRocNbBatchService.getLiaRocNbBatchData2020(changeType);
		//資料重整為 Map<ruleName,List<policyId>>
		Map<String, List<LiaRocNbBatchVO>> nbBatchMap = NBUtils.toMapList(nbBatchVOs, "policyId");
    	
		 //by policyId group
		 for(String policyId : nbBatchMap.keySet() ) {
			 try{
	    		trans = Trans.getUserTransaction();
				trans.begin();
	
				PolicyVO policyVO = policyDS.retrieveById(Long.parseLong(policyId), false);
				boolean isUnb = true;
				
				//PCR-337603 比對保單狀態是否需要含06-撤件 2020/11/20 Add by Kathy
				boolean isLiaRocPolicyStatus06 = false;
				
				//make 公會上傳主檔VO
				LiaRocUpload2020SendVO uploadVO = liaRocUpload2020CI.createLiarocUploadMaster
						(policyVO, LiaRocCst.LIAROC_UL_TYPE_RECEIVE, LiaRocCst.LIAROC_UL_BIZ_SOURCE_UNB);
	
				List<LiaRocUploadDetail2020VO> newDetails = new ArrayList<LiaRocUploadDetail2020VO>();
				
		    	for(LiaRocNbBatchVO nbBatchVO : nbBatchMap.get(policyId)) {
					List<CoverageVO>coverageList  = new ArrayList<CoverageVO>();
		    		CoverageVO coverageVO = coverageService.load(nbBatchVO.getChangeItemId());
					coverageList.add(coverageVO);
					
					//PolicyId = -1 表示有做過險種變更或刪除，已對應不到所屬保項資料，不做保額異動通報
					if (coverageVO.getPolicyId() > 0){
						//執行通報15-通報更正
						LiaRocUploadDetail2020VO detailVO01 = new LiaRocUploadDetail2020VO();
						//取得公會上傳明細檔
						List<LiaRocUploadDetail2020WrapperVO> genUploadDetails = liaRocUpload2020CI.generateNBBatchUploadDetail(
								policyVO, coverageList, LiaRocCst.LIAROC_UL_TYPE_RECEIVE,changeType);

						if (CollectionUtils.isNotEmpty(genUploadDetails)) {
							detailVO01 = genUploadDetails.get(0);
							detailVO01.setLiaRocPolicyStatus(LiaRocCst.LIAROC_POLICY_STATUS__FIX_ERROR);

							//IR-359765 新增被保險人附約(含眷屬)如跳出視窗點選確認時應以變更的日期通報要保書填寫日
							if (nbBatchVO.getSystemDate() != null) {
								detailVO01.setStatusEffectDate(nbBatchVO.getSystemDate());
							}
						}					
						//通報保額異動先找出AFINS 通報無生日資料，若有要將無生日那筆做12-鍵值錯誤，避免保額更正通報15在新公會上有2筆收件資料
						LiaRocUploadDetail2020WrapperVO detailVO12 = new LiaRocUploadDetail2020WrapperVO();		
						String internalId = CodeTable.getCodeById("V_PRODUCT_LIFE_1", String.valueOf(coverageVO.getProductId()));
						
						//以目前保項對應的被保險人CERTI_CODE去查是否曾有通報過公會資料
						InsuredVO insuredVO = coverageVO.getLifeInsured1().getInsured();
						
			    		//取出最近一筆公會收件通報資料
						//因多險種，增加比對已通報保項的通報取號檔
						List<LiaRocUploadDetail2020> lastUploadDetails = selectByLiarocPolicyCode(liaRocUploadDetail2020Dao.queryLastReceiveUploadDetail(
										LiaRocCst.LIAROC_UL_BIZ_SOURCE_UNB, 
										String.valueOf(nbBatchVO.getChannelType()),
										policyVO.getPolicyNumber(), 
										insuredVO.getCertiCode(), 
										internalId,
										nbBatchVO.getApplyDate(), 
										policyVO.getSerialNum(),
										policyVO.getMegaSerialNum(),
										LiaRocCst.IS_1KEY_FALSE,
										isLiaRocPolicyStatus06,
										nbBatchVO.getProposalTerm(),
										nbBatchVO.getChargeYear()), nbBatchVO);	

						boolean isStatus06 = true;
						//以變更前險種代碼+ID取出有無做過15-更正通報
						//因多險種，增加比對已通報保項的通報取號檔
						List<LiaRocUploadDetail2020> lastUpload15Details = selectByLiarocPolicyCode(liaRocUploadDetail2020Dao.queryLastReceiveUploadDetailByCertiCode(
										String.valueOf(nbBatchVO.getChannelType()),
										policyVO.getPolicyNumber(), 
										nbBatchVO.getCertiCodeBf(),
										nbBatchVO.getApplyDate(), 
										policyVO.getSerialNum(),
										internalId,
										isStatus06,
										null), nbBatchVO);		

						if (CollectionUtils.isNotEmpty(lastUploadDetails)) {
							LiaRocUploadDetail2020 lastUploadDetail = lastUploadDetails.get(0);
							if (lastUploadDetail.getBirthday() == null && detailVO01.getBirthday() != null) {
								LiaRocUploadDetail2020 detailBo12 = this.createFixUploadDetail(lastUploadDetail, null,
										LiaRocCst.LIAROC_POLICY_STATUS__ERROR_TERMINATION);
								detailBo12.copyToVO(detailVO12, Boolean.TRUE);
								if (CollectionUtils.isNotEmpty(lastUpload15Details)) {
									LiaRocUploadDetail2020 lastUpload15Detail = lastUpload15Details.get(0);
									// 在做06通報前再去確認是否有做過ID/生日變更通報15，若有以新ID/新生日做06-終止通報
									if (!StringUtils.isNullOrEmpty(detailVO12.getCertiCode())
											&& !StringUtils.isNullOrEmpty(lastUpload15Detail.getCertiCode())) {
										if (detailVO12.getCertiCode()
												.equals(lastUpload15Detail.getCertiCode()) == false) {
											detailVO12.setCertiCode(lastUpload15Detail.getCertiCode());
										}
									}
									if (detailVO12.getBirthday() != null && lastUpload15Detail.getBirthday() != null) {
										if (detailVO12.getBirthday().compareTo(lastUpload15Detail.getBirthday()) != 0) {
											detailVO12.setBirthday(lastUpload15Detail.getBirthday());
										}
									}
								}
							}
						}
						if (detailVO12.getLiaRocPolicyCode() != null){
							newDetails.add(detailVO12);							
						}
						
						if (detailVO01.getLiaRocPolicyCode() != null){
							newDetails.add(detailVO01);
							succCount++;
						}else{
							errorCount++;
						}
			
						uploadVO.setDataList(newDetails);

						//寫入公會收件通報上傳主檔和明細檔
				    	liaRocUpload2020CI.saveUpload(uploadVO);
						
						//舊公會不用做保額更正通報，由新公會程式回壓狀態’Z'不處理
						nbBatchVO.setCheckStatus(LiaRocCst.LIAROC_NB_BATCH_CHECK_STATUS_Z);
						nbBatchVO.setCheckStatus2020(LiaRocCst.LIAROC_NB_BATCH_CHECK_STATUS_YES);
						if (uploadVO.getListId() != null) {
							nbBatchVO.setUpload2020ListId(uploadVO.getListId());
						}						
					}else {
						//保額異動找不到保項時可能已做刪除險種故回壓狀態’Z'不處理，避免被一直被拿出來執行 2023/04/17 Add by Kathy
						nbBatchVO.setCheckStatus(LiaRocCst.LIAROC_NB_BATCH_CHECK_STATUS_Z);
						nbBatchVO.setCheckStatus2020(LiaRocCst.LIAROC_NB_BATCH_CHECK_STATUS_Z);
					}
					liaRocNbBatchService.save(nbBatchVO);					
				}
				
				trans.commit();
			} catch (Throwable e) {
	        	trans.rollback();
	        	String message = ExceptionInfoUtils.getExceptionMsg(e);
	        	ApplicationLogger.addLoggerData("[ERROR]2020 changeType06Process發生不可預期的錯誤, Error Message:" + message);
	            executeResult = JobStatus.EXECUTE_FAILED;
	            ApplicationLogger.setReturnCode(ApplicationLogger.RETURN_CODE_FAIL);
	            
				errorList.add("[ERROR]2020 changeType06Process發生不可預期的錯誤;PolicyId:" + policyId + ";" + message);
			} 
    	}

		ApplicationLogger.addLoggerData("[INFO] 預收輸入批次寫入新式公會收件上傳檔(保額異動)" 
				+ ";寫檔處理,成功=" + succCount + ",無資料筆數=" + errorCount);

		result.setExecuteResult(executeResult);
		result.setErrorList(errorList);
		return result;
    }

    public ResultMessage changeType13Process() throws Exception {

		// 批處理狀況預設為成功
		int executeResult = JobStatus.EXECUTE_SUCCESS;

        // PCR-507841_公會通報系統報錯新增系統警示
        ResultMessage result = new ResultMessage();
        List<String> errorList = new ArrayList<String>();
        
		// 依序處理控制檔
		int errorCount = 0;
		int succCount = 0;
		String changeType = LiaRocCst.LIAROC_NB_BATCH_CHANGE_TYPE_13;
		UserTransaction trans = null;

		//13-承保UNDO後通報承保06，當天又承保
		List<LiaRocNbBatchVO> nbBatchVOs = liaRocNbBatchService.getLiaRocNbBatchData2020(changeType);

		Date today = DateUtils.truncateDay(new Date());//去除時分秒

		for (LiaRocNbBatchVO nbBatchVO : nbBatchVOs) {

			//僅處理今天0時前的資料
			if (today.getTime() > nbBatchVO.getInsertTimestamp().getTime()) {

				try {
					trans = Trans.getUserTransaction();
					trans.begin();
					//生效狀態判斷
					int liabilityState = policyDS.getLiabilityStatusByPolicyId(nbBatchVO.getPolicyId());

					if (CodeCst.LIABILITY_STATUS__IN_FORCE == liabilityState) {
						//執行全量承保通報
						Long uploadId = liaRocUpload2020CI.sendNB(nbBatchVO.getPolicyId(),
								LiaRocCst.LIAROC_UL_TYPE_INFORCE);

						if (uploadId != null) {
							succCount++;
							nbBatchVO.setCheckStatus2020(LiaRocCst.LIAROC_NB_BATCH_CHECK_STATUS_YES);
						} else {
							errorCount++;
							nbBatchVO.setCheckStatus2020(LiaRocCst.LIAROC_NB_BATCH_CHECK_STATUS_ERROR);
						}
						nbBatchVO.setUpload2020ListId(uploadId);
					} else {
						succCount++;
						nbBatchVO.setCheckStatus(LiaRocCst.LIAROC_NB_BATCH_CHECK_STATUS_Z);
					}

					liaRocNbBatchService.save(nbBatchVO);

					trans.commit();
				} catch (Throwable e) {
					trans.rollback();
					String message = ExceptionInfoUtils.getExceptionMsg(e);
					ApplicationLogger.addLoggerData("[ERROR]2020 changeType13Process發生不可預期的錯誤, Error Message:" + message);
					executeResult = JobStatus.EXECUTE_FAILED;
					ApplicationLogger.setReturnCode(ApplicationLogger.RETURN_CODE_FAIL);
					
					errorList.add("[ERROR]2020 changeType13Process發生不可預期的錯誤;PolicyId:" + nbBatchVO.getPolicyId() + ";" + message);
				}
			}
		}

		ApplicationLogger
				.addLoggerData("[INFO] 承保UNDO後通報承保06，當天又承保，隔天執行2020承保通報" + ";寫檔處理,成功=" + succCount + ",失敗=" + errorCount);

		result.setExecuteResult(executeResult);
		result.setErrorList(errorList);
		return result;
	}

	public ResultMessage changeType14Process() throws Exception {

		// 批處理狀況預設為成功
		int executeResult = JobStatus.EXECUTE_SUCCESS;

        // PCR-507841_公會通報系統報錯新增系統警示
        ResultMessage result = new ResultMessage();
        List<String> errorList = new ArrayList<String>();
        
		// 依序處理控制檔
		int errorCount = 0;
		int succCount = 0;
		String changeType = LiaRocCst.LIAROC_NB_BATCH_CHANGE_TYPE_14;
		UserTransaction trans = null;

		//14-撤件UNDO後通報收件01，當天又撤件
		List<LiaRocNbBatchVO> nbBatchVOs = liaRocNbBatchService.getLiaRocNbBatchData2020(changeType);

		Date today = DateUtils.truncateDay(new Date());//去除時分秒

		for (LiaRocNbBatchVO nbBatchVO : nbBatchVOs) {

			//僅處理今天0時前的資料
			if (today.getTime() > nbBatchVO.getInsertTimestamp().getTime()) {

				try {

					String policyCode = policyDS.getPolicyCodeByPolicyId(nbBatchVO.getPolicyId());
					int proposalStatus = policyDS.getProposalStatusByPolicyCode(policyCode);

					trans = Trans.getUserTransaction();
					trans.begin();
					if (proposalStatus == CodeCst.PROPOSAL_STATUS__DECLINED
							|| proposalStatus == CodeCst.PROPOSAL_STATUS__POSTPONED
							|| proposalStatus == CodeCst.PROPOSAL_STATUS__WITHDRAWN) {
						//執行全量收件通報06
						LiaRocUpload2020SendVO sendVO = liaRocUpload2020CI.getUploadVOByPolicyId(
								nbBatchVO.getPolicyId(), LiaRocCst.LIAROC_UL_TYPE_RECEIVE,
								LiaRocCst.LIAROC_UL_BIZ_SOURCE_UNB);

						List<? extends GenericEntityVO> details = sendVO.getDataList();
						for (GenericEntityVO entityVO : details) {
							LiaRocUploadDetail2020VO detailVO = (LiaRocUploadDetail2020VO) entityVO;
							detailVO.setLiaRocPolicyStatus(LiaRocCst.LIAROC_POLICY_STATUS__FREELOOK);
						}
						Long uploadId = liaRocUpload2020CI.saveUpload(sendVO);
						if (uploadId != null) {
							succCount++;
							nbBatchVO.setCheckStatus2020(LiaRocCst.LIAROC_NB_BATCH_CHECK_STATUS_YES);
						} else {
							errorCount++;
							nbBatchVO.setCheckStatus2020(LiaRocCst.LIAROC_NB_BATCH_CHECK_STATUS_ERROR);
						}

						nbBatchVO.setUpload2020ListId(uploadId);
					} else {
						succCount++;
						nbBatchVO.setCheckStatus(LiaRocCst.LIAROC_NB_BATCH_CHECK_STATUS_Z);
					}

					liaRocNbBatchService.save(nbBatchVO);

					trans.commit();
				} catch (Throwable e) {
					trans.rollback();
					String message = ExceptionInfoUtils.getExceptionMsg(e);
					ApplicationLogger.addLoggerData("[ERROR]2020 changeType14Process發生不可預期的錯誤, Error Message:" + message);
					executeResult = JobStatus.EXECUTE_FAILED;
					ApplicationLogger.setReturnCode(ApplicationLogger.RETURN_CODE_FAIL);
					
					errorList.add("[ERROR]2020 changeType14Process發生不可預期的錯誤;PolicyId:" + nbBatchVO.getPolicyId() + ";" + message);
				}
			}
		}

		ApplicationLogger.addLoggerData(
				"[INFO] 撤件UNDO後通報收件01，當天又撤件，隔天執行2020收件06通報" + ";寫檔處理,成功=" + succCount + ",失敗=" + errorCount);

		result.setExecuteResult(executeResult);
		result.setErrorList(errorList);
		return result;
	}

	@Override
	protected FixedCharLengthWriter getFixedLengthWriter() {
		// TODO Auto-generated method stub
		return null;
	}
	
	private LiaRocUploadDetail2020 createFixUploadDetail(LiaRocUploadDetail2020 input, Long listId, String liaRocStatus) {
		LiaRocUploadDetail2020 detailBO = new LiaRocUploadDetail2020();
		BeanUtils.copyProperties(detailBO, input);
		detailBO.setLiaRocPolicyStatus(liaRocStatus);
		detailBO.setUploadListId(listId);
		detailBO.setListId(null);
		return detailBO;
	}

	private LiaRocUploadDetail2020  queryLastReceiveUploadDetail(String bizSource, String channelType, String policyCode,
			String certiCode, String internalId,Date applyDate, String serialNum, String megaSerialNum, boolean is1Key ,boolean isLiaRocPolicyStatus06,
			Integer proposalTerm, Integer chargeYear, Long policyId) {
		
		List<LiaRocUploadDetail2020>  lastDetailVO01s = new ArrayList<LiaRocUploadDetail2020>();
		LiaRocUploadDetail2020  lastDetailVO01               = null;
		//以傳入險種取出最近一筆公會收件通報資料
		lastDetailVO01s = filterByLiarocCoverage(liaRocUploadDetail2020Dao.queryLastReceiveUploadDetail(
						bizSource, 
						channelType,
						policyCode, 
						certiCode, 
						internalId, 
						applyDate, 
						serialNum,
						megaSerialNum,
						is1Key,
						isLiaRocPolicyStatus06,
						proposalTerm,
						chargeYear), policyId) ;
		LiaRocUpload2020 oldLastUpload = null;					
		//有重覆送件的記錄
		if (CollectionUtils.isNotEmpty(lastDetailVO01s)) {
			lastDetailVO01 = lastDetailVO01s.get(0);
			//查明細的主檔，ID不同才查
			oldLastUpload = liaRocUpload2020Dao.load(lastDetailVO01.getUploadListId());
			String status = oldLastUpload.getLiaRocUploadStatus();

			//若原本有為收件通報系統(AFINS),依通路比對通報檔, 若為FOA受理(UNB)通報用保單號碼+要保書填寫日,險種代碼比對, 已放送就不再發送;
			//判斷最後一筆通報01-有效才是重覆送件
			if (LiaRocCst.LIAROC_UL_STATUS_SEND.equals(status)
							|| LiaRocCst.LIAROC_UL_STATUS_RETURN.equals(status)
							|| LiaRocCst.LIAROC_UL_STATUS_RETURN_ERROR.equals(status)
											&& (lastDetailVO01.getLiaRocPolicyStatus().equals(LiaRocCst.LIAROC_POLICY_STATUS__INFORCE) == true)) {
				//取得曾通報過的detail
				return lastDetailVO01;
			} else {
				 lastDetailVO01 = null;
			}
		}
		return lastDetailVO01;
	}

	@Override
	protected List<? extends LiaRocUploadDetailBaseVO> getUpload2020Details() {
		return new ArrayList<LiaRocUploadDetailBaseVO>();
	}
	
	/**
	 * PCR_454251
	 * 排程：21:30最後一批公會收件通報資料產生完後，接著產生新契約當日承保的承保公會通報資料。
	 * @return
	 * @throws Exception
	 */
	public int changeType10Proces() throws Exception{
	  	ApplicationLogger.clear();
	  	ApplicationLogger.setSystemCode(AppLogDataHelper.SYSTEM_CODE_UNB);
	  	ApplicationLogger.setJobName("新契約預收輸入新式公會承保通報2020");
	  	ApplicationLogger.setPolicyCode("LiarocNbBatch2020");
	  	ApplicationLogger.setOptionId(DateUtils.date2String(new Date(), "yyyyMMdd HHmmss"));

	  	if (BatchDB.getSubId() != null) {
	  		ApplicationLogger.addLoggerData("[INFO]Batch Job Starting.. RunId=" + BatchHelp.getRunId());
	  	}

		// 批處理狀況預設為成功
		int executeResult = JobStatus.EXECUTE_SUCCESS;

		// 依序處理控制檔
		int errorCount = 0;
		int succCount = 0;
		String changeType = LiaRocCst.LIAROC_NB_BATCH_CHANGE_TYPE_10;
		UserTransaction trans = null;

		//10-承保通報
		List<LiaRocNbBatchVO> nbBatchVOs = liaRocNbBatchService.getLiaRocNbBatchData2020(changeType);

		List<Long> policyList = new ArrayList<Long>();
		
		for (LiaRocNbBatchVO nbBatchVO : nbBatchVOs) {

			try {
				trans = Trans.getUserTransaction();
				trans.begin();
				//生效狀態判斷
				int liabilityState = policyDS.getLiabilityStatusByPolicyId(nbBatchVO.getPolicyId());
				if (CodeCst.LIABILITY_STATUS__IN_FORCE == liabilityState) {
					if(policyList.contains(nbBatchVO.getPolicyId())){
						succCount++;
						nbBatchVO.setCheckStatus2020(LiaRocCst.LIAROC_NB_BATCH_CHECK_STATUS_Z);
						ApplicationLogger.addLoggerData("不通報，重覆執行件 policyId=" + nbBatchVO.getPolicyId());
					} else {
						
						//執行承保通報
						Long uploadId = liaRocUpload2020CI.sendNB(nbBatchVO.getPolicyId(),
								LiaRocCst.LIAROC_UL_TYPE_INFORCE);

						if (uploadId != null) {
							succCount++;
							nbBatchVO.setCheckStatus2020(LiaRocCst.LIAROC_NB_BATCH_CHECK_STATUS_YES);
							ApplicationLogger.addLoggerData("成功 policyId=" + nbBatchVO.getPolicyId());
						} else {
							errorCount++;
							ApplicationLogger.addLoggerData("失敗 policyId=" + nbBatchVO.getPolicyId());
						}
						nbBatchVO.setUpload2020ListId(uploadId);
						
						policyList.add(nbBatchVO.getPolicyId());
					}
				} else {
					succCount++;
					nbBatchVO.setCheckStatus(LiaRocCst.LIAROC_NB_BATCH_CHECK_STATUS_Z);
				}
				liaRocNbBatchService.save(nbBatchVO);

				trans.commit();
			} catch (Throwable e) {
				trans.rollback();
				String message = ExceptionInfoUtils.getExceptionMsg(e);
				ApplicationLogger.addLoggerData("[ERROR]2020 changeType10Process發生不可預期的錯誤, Error Message:" + message);
				executeResult = JobStatus.EXECUTE_FAILED;
				ApplicationLogger.setReturnCode(ApplicationLogger.RETURN_CODE_FAIL);
			}
		}

		ApplicationLogger
				.addLoggerData("[INFO] 承保通報;寫檔處理,成功=" + succCount + ",失敗=" + errorCount);

		ApplicationLogger.flush();
		return executeResult;
	}
}
