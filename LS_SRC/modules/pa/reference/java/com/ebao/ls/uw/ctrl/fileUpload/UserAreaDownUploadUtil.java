package com.ebao.ls.uw.ctrl.fileUpload;

import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.TreeMap;

import javax.annotation.Resource;
import javax.transaction.UserTransaction;

import org.apache.commons.lang.ClassUtils;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.ebao.foundation.common.lang.StringUtils;
import com.ebao.foundation.module.db.DBean;
import com.ebao.ls.cs.logger.ApplicationLogger;
import com.ebao.ls.cs.logger.helper.AppLogDataHelper;
import com.ebao.ls.leave.service.UserLeaveManagerService;
import com.ebao.ls.pa.nb.util.NBUtils;
import com.ebao.ls.pty.ci.UserInfoQueryService;
import com.ebao.ls.uw.data.TUserAreaConfDao;
import com.ebao.pub.util.DateUtils;

/**
 * <p>Description : 轄區設置資料上傳/下載</p>
 * <p>PCR_41834 核保轄區設置作業調整及保留核保決定資料記錄 </p>
 * <p>Created By : Alex Chang</p>
 * <p>Create Time : 2020.12.14</p>
 * @return
 */
public class UserAreaDownUploadUtil {
	public static final String BEAN_DEFAULT = "uwUserAreaDownUploadUtil";
	private static Logger logger = Logger.getLogger(UserAreaDownUploadUtil.class);
	
	public static final int UPLOAD_EXCEL_ROW_QTY_PER_BATCH= 3;	//檢查內容(上傳的  Excel)時, 每個批次筆數
	public static final String REGEXP_XLSX_FILE= "^(.*)\\.([xX][lL][sS][xX])$"; 
	public static final String REGEXP_XLS_FILE= "^(.*)\\.([xX][lL][sS])$"; 

	public static final String EXCEL_SHEET_NAME= "UserArea";
	public static final String[] EXCEL_HEADER_COL_NAME= new String[] {"EXTERNAL_CHANNEL_CODE", "CHANNEL_CODE", "SUB_CHANNEL_CODE"
    																		, "CHANNEL_NAME", "USER_NAME", "REAL_NAME"};
    public static final String META_CHANNEL_CODE= "CHANNEL_CODE"; 
    public static final String META_SUB_CHANNEL_CODE= "SUB_CHANNEL_CODE"; 
    public static final String META_USER_NAME= "USER_NAME"; 

	public static String ERR_MSG_EMPTY_SUB_CHANNEL_CODE= "Channel_Code 或 Sub_Channel_Code不可為空";
	public static String ERR_MSG_EMPTY_CHANNEL_CODE= "Channel_Code 或 Sub_Channel_Code不可為空";
	public static String ERR_MSG_EMPTY_USER_NAME= "User_Name不可為空";
	public static String ERR_MSG_SUB_CHANNEL_CODE_NOT_EXIST= NBUtils.getTWMsg("MSG_1265847");	//"Sub_Channel_Code 不存在";
	public static String ERR_MSG_CHANNEL_CODE_NOT_EXIST= NBUtils.getTWMsg("MSG_1265847");	//"Channel_Code 不存在";
	public static String ERR_MSG_USER_NAME_NOT_EXIST= NBUtils.getTWMsg("MSG_1265848");	//"User_Name 不存在";
	public static String ERR_MSG_EXCEL_FILE_ONLY= NBUtils.getTWMsg("MSG_1265857");	//"上傳檔案僅限xls檔";
	public static String ERR_MSG_INNER_CHANNEL_GRADE_NOT_2nd_Level= "內部通路資料,只能上傳 Channel_Grade=2 及 通訊處(Channel_Grade=3)資料!";
	public static String ERR_MSG_EXCEL_CELL_MUST_STRING= "Excel 欄位儲存格格式必需為'文字'";	//Excel 欄位儲存格格式必需為'文字'
	
	public final String TEMPLATE_PROC_RESULT= NBUtils.getTWMsg("MSG_1265851");	//"success row:{0}, failure row:{1}, total row:{2}"; 
	public final String TEMPLATE_XLS_PROC_ERR_PER_ROW= "第{0}行: "+ "{1}<br/>"; 

	public final String MSG_FILE_UPLOAD_SUCCESS= "檔案上傳成功!!";
	public final String MSG_FILE_UPLOAD_FAIL= NBUtils.getTWMsg("MSG_1259478");	//"檔案上傳失敗!!";
	public final String MSG_FILE_UPLOAD_COMPLETE= NBUtils.getTWMsg("MSG_1265854");	//上傳檔案格式錯誤;
	public final String MSG_UPLOAD_RESULT_PROMPT= "上傳資料處理結果:";
	public final String MSG_TITLE_OF_PROC_RESULT= "錯誤的資料行:<br/>";
	
	public final String META_COL_PROC_SUMMARY= "processSummary";
	public final String META_COL_ERR_CODE= "errCode";
	public final String META_COL_ERR_DATA= "errData";
	public final String META_COL_ERR_ROW_NUM= "errRowNum";
	
	@Resource(name = TUserAreaConfDao.BEAN_DEFAULT)
	private TUserAreaConfDao userAreaConfDao;

	@Resource(name = UserLeaveManagerService.BEAN_DEFAULT)
	private UserLeaveManagerService userLeaveManagerService;

	@Resource(name = UserInfoQueryService.BEAN_DEFAULT)
	private UserInfoQueryService userInfoQueryService;
	
	  
	  /**
     * <p>Description : 讀取所有轄區設置</p>
     * <p>PCR_41834 核保轄區設置作業調整及保留核保決定資料記錄 </p>
     * <p>Created By : Alex Chang</p>
     * <p>Create Time : 2020.12.14</p>
     * @return
     */
    public ArrayList<ArrayList<String>> dumpUserAreaConf() throws Exception{
    	ArrayList<ArrayList<String>> dumpResultList= new ArrayList<ArrayList<String>>();
		DBean db = new DBean();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			db.connect();
			Connection con = db.getConnection();
			String sql =  "";
			
			//Total:14629	
			sql= " Select Channel_Code || Sub_Channel_Code AS Sort_Col "
			        + " , Chl_Parent_ID, Channel_Name, Channel_Code, Sub_Channel_Code "
			        + " , External_Channel_Code, Is_Extenal, Channel_Grade, Sub_Channel_Grade "
			        + " , User_Name USER_NAME, REAL_NAME REAL_NAME, Emp_Id, Inserted_By "
			    + " From ( "
			        + " Select co.Parent_ID Chl_Parent_ID, co.Channel_Name "
			                    + " , NVL(co2.Channel_Code, co.Channel_Code) AS Channel_Code, co.Channel_Code AS Sub_Channel_Code "
			                + " , sc.External_Channel_Code, sc.Is_Extenal "
			                + " , NVL(co2.Channel_Grade, co.Channel_Grade) AS Channel_Grade, co.Channel_Grade AS Sub_Channel_Grade "
			                + " , emp.User_Name USER_NAME, emp.REAL_NAME REAL_NAME, emp.Emp_Id, uac.Inserted_By "
			                + " From v_nb_user_area_channel co "
			                    + " Left Join v_nb_user_area_channel co2 On co2.CHANNEL_Id= co.Parent_Id "
			                    + " Left Join T_Sales_CHANNEL sc On sc.Sales_CHANNEL_Id= co.CHANNEL_Type  "
			                    + " Left Join T_USER_AREA_CONF uac On co.Channel_Id= uac.Channel_Org_Id "
			                    + " Left Join T_Employee emp On uac.Emp_Id= emp.Emp_Id "
			                + " Where sc.Is_Extenal in ('Y', 'y') And co.Channel_Grade in ('1', '2') "
			                    + " And ((co2.Status = 2 And co.Status = 2  ) OR (co2.Status= 2 And co.Status is NULL ) "
			                                                                + " OR (co2.Status is NULL And co.Status= 2) ) "
			        + " Union All "
			        + " Select co.Parent_ID Chl_Parent_ID, co.Channel_Name, co.Channel_Code AS Channel_Code, null AS Sub_Channel_Code "
			                + " , sc.External_Channel_Code, sc.Is_Extenal, co.Channel_Grade AS Channel_Grade, null AS Sub_Channel_Grade "
			                + " , emp.User_Name USER_NAME, emp.REAL_NAME REAL_NAME, emp.Emp_Id, uac.Inserted_By "
			                + " From v_nb_user_area_channel co "
			                    + " Left Join v_nb_user_area_channel co2 On co2.CHANNEL_Id= co.Parent_Id "
			                    + " Left Join T_Sales_CHANNEL sc On sc.Sales_CHANNEL_Id= co.CHANNEL_Type  "
			                    + " Left Join T_USER_AREA_CONF uac On co.Channel_Id= uac.Channel_Org_Id "
			                    + " Left Join T_Employee emp On uac.Emp_Id= emp.Emp_Id "
			                + " Where sc.Is_Extenal in ('N', 'n') And co.Channel_Grade = '2' "
			        + " Union All "
			        + " Select co.Parent_ID Chl_Parent_ID, co.Communicate_Name AS Channel_Name "
			                    + " , NVL(co2.Channel_Code, co.Channel_Code) AS Channel_Code, co.Channel_Code AS Sub_Channel_Code "
			                + " , sc.External_Channel_Code, sc.Is_Extenal "
			                + " , NVL(co2.Channel_Grade, co.Channel_Grade) AS Channel_Grade, co.Channel_Grade AS Sub_Channel_Grade "
			                + " , emp.User_Name USER_NAME, emp.REAL_NAME REAL_NAME, emp.Emp_Id, uac.Inserted_By "
			                + " From v_nb_user_area_channel co "
			                    + " Left Join v_nb_user_area_channel co2 On co2.CHANNEL_Id= co.Parent_Id "
			                    + " Left Join T_Sales_CHANNEL sc On sc.Sales_CHANNEL_Id= co.CHANNEL_Type  "
			                    + " Left Join T_USER_AREA_CONF uac On co.Channel_Id= uac.Channel_Org_Id "
			                    + " Left Join T_Employee emp On uac.Emp_Id= emp.Emp_Id "
			                + " Where co.communicate_id is not null "
			    + " ) "
			    + " Order By Sort_Col ";
			sql= ""+ " Select Channel_Code || Sub_Channel_Code AS Sort_Col "
			        + " , Channel_ID, Chl_Parent_ID, Channel_Name, Communicate_ID, Channel_Code, Sub_Channel_Code "
			        + " , External_Channel_Code, Is_Extenal, Channel_Grade, Sub_Channel_Grade "
			        + " , User_Name USER_NAME, REAL_NAME REAL_NAME, Emp_Id, Inserted_By "
			    + " From ( "
			        + " Select co.Channel_ID, co.Parent_ID Chl_Parent_ID, co.Channel_Name, co.Communicate_ID "
			                    + " , NVL(co2.Channel_Code, co.Channel_Code) AS Channel_Code, co.Channel_Code AS Sub_Channel_Code "
			                + " , sc.External_Channel_Code, sc.Is_Extenal "
			                + " , NVL(co2.Channel_Grade, co.Channel_Grade) AS Channel_Grade, co.Channel_Grade AS Sub_Channel_Grade "
			                + " , emp.User_Name USER_NAME, emp.REAL_NAME REAL_NAME, emp.Emp_Id, uac.Inserted_By "
			                + " From v_nb_user_area_channel co "
			                    + " Left Join v_nb_user_area_channel co2 On co2.CHANNEL_Id= co.Parent_Id "
			                    + " Left Join T_Sales_CHANNEL sc On sc.Sales_CHANNEL_Id= co.CHANNEL_Type "
			                    + " Left Join T_USER_AREA_CONF uac On co.Channel_Id= uac.Channel_Org_Id "
			                    + " Left Join T_Employee emp On uac.Emp_Id= emp.Emp_Id "
			                + " Where sc.Is_Extenal in ('Y', 'y') And co.Channel_Grade in ('1', '2') "
			                    + " And ((co2.Status = 2 And co.Status = 2  ) OR (co2.Status= 2 And co.Status is NULL ) "
			                                                            + " OR (co2.Status is NULL And co.Status= 2) ) "
			        + " Union All "
			        + " Select co.Channel_ID, co.Parent_ID Chl_Parent_ID, co.Channel_Name, co.Communicate_ID "
			                + " , co.Channel_Code AS Channel_Code, null AS Sub_Channel_Code "
			                + " , sc.External_Channel_Code, sc.Is_Extenal, co.Channel_Grade AS Channel_Grade, null AS Sub_Channel_Grade "
			                + " , emp.User_Name USER_NAME, emp.REAL_NAME REAL_NAME, emp.Emp_Id, uac.Inserted_By "
			                + " From v_nb_user_area_channel co "
			                    + " Left Join v_nb_user_area_channel co2 On co2.CHANNEL_Id= co.Parent_Id "
			                    + " Left Join T_Sales_CHANNEL sc On sc.Sales_CHANNEL_Id= co.CHANNEL_Type "
			                    + " Left Join T_USER_AREA_CONF uac On co.Channel_Id= uac.Channel_Org_Id "
			                    + " Left Join T_Employee emp On uac.Emp_Id= emp.Emp_Id "
			                + " Where sc.Is_Extenal in ('N', 'n') And co.Channel_Grade = '2' "
			        + " Union All "
			        + " Select co.Channel_ID, co.Parent_ID Chl_Parent_ID, co.Communicate_Name AS Channel_Name, co.Communicate_ID "
			                    + " , NVL(co2.Channel_Code, co.Channel_Code) AS Channel_Code, co.Channel_Code AS Sub_Channel_Code "
			                + " , sc.External_Channel_Code, sc.Is_Extenal "
			                + " , NVL(co2.Channel_Grade, co.Channel_Grade) AS Channel_Grade, co.Channel_Grade AS Sub_Channel_Grade "
			                + " , emp.User_Name USER_NAME, emp.REAL_NAME REAL_NAME, emp.Emp_Id, uac.Inserted_By "
			                + " From v_nb_user_area_channel co "
			                    + " Left Join v_nb_user_area_channel co2 On co2.CHANNEL_Id= co.Parent_Id "
			                    + " Left Join T_Sales_CHANNEL sc On sc.Sales_CHANNEL_Id= co.CHANNEL_Type "
			                    + " Left Join T_USER_AREA_CONF uac On co.Channel_Id= uac.Channel_Org_Id "
			                    + " Left Join T_Employee emp On uac.Emp_Id= emp.Emp_Id "
			                + " Where co.communicate_id is not null "
			    + " ) "
			    + " Order By Sort_Col ";
			
			ps = con.prepareStatement(sql);
			rs= ps.executeQuery();
			int rowQty= 0;
			if (rs.next()) {
				do{
					rowQty++;
//					if (rowQty> 14999) break;	//14594
					
					ArrayList<String> tempRow= new ArrayList<String> ();
					boolean isUserDisabled= false;
					com.ebao.foundation.app.sys.sysmgmt.ds.UserVO voUser= null;
					String userName= null;
					String realName= null;
					String channelCode= "", subChannelCode= "", communicateId= "";
					
					Long empId= rs.getLong("Emp_Id");
					if (empId!= null){
						voUser= userInfoQueryService.getUserByUserId(empId);
						if (voUser== null) isUserDisabled= true; 
						else isUserDisabled= voUser.getDisable();
					}
					else isUserDisabled= true;
					
					//員工不存在 or 不在職 的 User 不輸出到Excel, 但 通路資料還是要輸出
					if ( isUserDisabled ) {
//			    		logger.warn(""+ "\r\n"
//			    				+ "Emploee/User (ID:["+ empId+ "]) disabled or not exist!!"+ "\r\n"
//			    				+ "");
						userName= "";
						realName= "";
					}
					else {
						userName= rs.getString("User_Name");
						realName= rs.getString("Real_Name");
					}
					
					Long parentChannelId= rs.getLong("Chl_Parent_ID");
					String isExternalStr= rs.getString("Is_Extenal");
					isExternalStr= (isExternalStr== null? "": isExternalStr);
					String channelGrade= rs.getString("Channel_Grade");
					channelGrade= (channelGrade== null? "": channelGrade);
					channelCode		= rs.getString("Channel_Code");
					subChannelCode = rs.getString("SUB_Channel_Code");
					communicateId= rs.getString("Communicate_ID");

					//內部通路
					if ("N".equals(isExternalStr)) {
						if (!StringUtils.isNullOrEmpty(communicateId)) {
							//有 Communicate Id時,代表此筆為通訊處資料
						}
						else //內部通路只顯示第2階(Channel Grade=2) 
							subChannelCode	= "";
					}
					
					//外部通路
					if ("Y".equals(isExternalStr)  && (parentChannelId== null || parentChannelId== 0) ) {
						//第一階資料, 要去除 SQL 產生過程中, 冗餘的 Sub_Channel_Code
						subChannelCode	= "";
					}
					
					channelCode		= (channelCode== null? "": channelCode);
					subChannelCode 	= (subChannelCode== null? "": subChannelCode);
					communicateId		= (communicateId== null? "": communicateId);
					userName		= (userName== null? "": userName);
					realName		= (realName== null? "": realName);
					
					tempRow.add( rs.getString("External_Channel_Code"));
					tempRow.add( channelCode);		//rs.getString("Channel_Code"));
					tempRow.add( subChannelCode);	//rs.getString("SUB_Channel_Code"));
					tempRow.add( rs.getString("Channel_Name"));
					
					tempRow.add( userName);	//rs.getString("User_Name"));
					tempRow.add( realName);	//rs.getString("Real_Name"));
					dumpResultList.add(tempRow);
				} while (rs.next());
			}
			else {
				//No Record exist!!
			}
			
		}catch (Exception e) {
			throw e;
		} finally {
			DBean.closeAll(rs, ps, db);
		}
	    return dumpResultList;
    }

    /**
     * <p>Description : 讀取所有轄區設置</p>
     * <p>PCR_41834 核保轄區設置作業調整及保留核保決定資料記錄 </p>
     * <p>Created By : Alex Chang</p>
     * <p>Create Time : 2020.12.14</p>
     * @return
     */
   public OutputStream genUserAreaExcelData(OutputStream osOutExcel) throws Exception {
		XSSFWorkbook xssfBook= new XSSFWorkbook();
	    ArrayList<ArrayList<String>> lstDataSet= new ArrayList<ArrayList<String>>();
	    
	    try {
			String clsName= ClassUtils.getShortClassName(this.getClass());
			String jobName= NBUtils.getTWMsg("MSG_1251825");
			clsName= (clsName.length()> 20? clsName.substring(0, 20): clsName);
			jobName= (jobName.length()> 128/3? jobName.substring(0, 128/3): jobName);

			ApplicationLogger.setSystemCode(AppLogDataHelper.SYSTEM_CODE_UNB);
	 		ApplicationLogger.setPolicyCode(clsName );
			ApplicationLogger.setJobName(jobName );	//休假管理及核保轄區設置
			ApplicationLogger.setOptionId(DateUtils.date2String(new Date(), "yyyyMMdd HHmmss"));
			ApplicationLogger.addLoggerData(com.ebao.pub.util.EnvUtils.getEnvLabel());
			ApplicationLogger.addLoggerData("Excel Download begin");

			NBUtils.logger(this.getClass(), "genUserAreaExcelData [[Generate download User_Area Excel Data ]] begin");
    	    //讀取 T_User_Area_Conf table data
	    	lstDataSet= dumpUserAreaConf();
    		
			setSheetData(xssfBook, EXCEL_SHEET_NAME, lstDataSet);
			xssfBook.write(osOutExcel);
			osOutExcel.flush();
			NBUtils.logger(this.getClass(), "genUserAreaExcelData [[Generate download User_Area Excel Data ]] end");

			ApplicationLogger.addLoggerData("Excel Download end");
	     	ApplicationLogger.flush();  
    	}
    	catch(Exception e) {
    		throw e;
    	}
    	return osOutExcel;
    }
    
	public void setSheetData(XSSFWorkbook xssfBook, String sheetName
										, ArrayList<ArrayList<String>> lstDataSet) throws Exception{
		int rowCnt= 0;
		sheetName= (sheetName== null? " ": sheetName);
		XSSFSheet shtTmp = xssfBook.createSheet(sheetName);
		
		shtTmp.setColumnWidth(0, 3000);
		shtTmp.setColumnWidth(1, 5000);
		shtTmp.setColumnWidth(2, 5000);
		shtTmp.setColumnWidth(3, 16000);
		shtTmp.setColumnWidth(4, 3000);
		shtTmp.setColumnWidth(5, 5000);
		    
	    XSSFRow xssfRowTmp = shtTmp.createRow(rowCnt);

	    for(int iColCnt= 0; iColCnt< EXCEL_HEADER_COL_NAME.length; iColCnt++ ) 
		    xssfRowTmp.createCell(iColCnt).setCellValue(EXCEL_HEADER_COL_NAME[iColCnt]);

	    for(int rowCnt2= 0; rowCnt2< lstDataSet.size(); rowCnt2++) {
	    	rowCnt++;
	    	ArrayList<String> alRow= lstDataSet.get(rowCnt2);
	    	
	    	setXlsRow(shtTmp.createRow(rowCnt), EXCEL_HEADER_COL_NAME.length, alRow);
	    }
	}
	
	public void setXlsRow(XSSFRow xssfRow, int iColQty, ArrayList<String> alRowData) throws Exception{
		for(int iColCnt= 0; iColCnt< iColQty; iColCnt++) {
			XSSFCell xssfCellTemp= xssfRow.createCell(iColCnt);
			xssfCellTemp.setCellType(CellType.STRING);
			xssfCellTemp.setCellValue(alRowData.get(iColCnt));
			xssfCellTemp.setCellType(CellType.STRING);
		}
	}
	
	public  void setSheetData_Xls(HSSFWorkbook workBook) throws Exception{
		int iRowCnt= 0;
		HSSFSheet shtTmp = workBook.createSheet();
		
		shtTmp.setColumnWidth(0, 3000);
		shtTmp.setColumnWidth(1, 3000);
		shtTmp.setColumnWidth(2, 9000);
		    
	    HSSFRow hssfRowTmp = shtTmp.createRow(iRowCnt);
	    hssfRowTmp.createCell(0).setCellValue("EXTERNAL_CHANNEL_CODE");
	    hssfRowTmp.createCell(1).setCellValue("CHANNEL_CODE");         
	    hssfRowTmp.createCell(2).setCellValue("SUB_CHANNEL_CODE");     
	    hssfRowTmp.createCell(3).setCellValue("CHANNEL_NAME");         
	    hssfRowTmp.createCell(4).setCellValue("USER_NAME");            
	    hssfRowTmp.createCell(5).setCellValue("REAL_NAME");            
	}

	/**
	 * <p>Description : 分析上傳 Excel 檔案內容</p>
	 * <p>PCR_41834 核保轄區設置作業調整及保留核保決定資料記錄 </p>
	 * <p>Created By : Alex Chang</p>
	 * <p>Create Time : 2020.12.14</p>
	 * @param empId
	 * @return
	 */
	public HashMap<String, Object> parseUploadData(InputStream fileInputStream) throws Exception {
		TreeMap<Integer, String> errDataMap= new TreeMap<Integer, String>();
		int successRowCnt= 0;
		int failRowCnt= 0;
		int errNum_RowN= 0, errCodeNum= 0;
		String errCodeStr= "0";
		String templMsg= TEMPLATE_PROC_RESULT;
		
		HashMap<String, Object> mapRtnResult= new HashMap<String, Object>();
		HashMap<String, HashMap<String, String>> mapDataTable= new HashMap<String, HashMap<String, String>>();
//		HSSFWorkbook xssfBook= null;
//		HSSFSheet xssfSheet= null;
		XSSFWorkbook xssfBook= null;
		XSSFSheet xssfSheet= null;
		SimpleDateFormat sdfYYYYMMDD_HMS=new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date startTime_Upload= new Date();
		
		String clsName= ClassUtils.getShortClassName(this.getClass());
		String jobName= NBUtils.getTWMsg("MSG_1251825");
		clsName= (clsName.length()> 20? clsName.substring(0, 20): clsName);
		jobName= (jobName.length()> 128/3? jobName.substring(0, 128/3): jobName);

		ApplicationLogger.setSystemCode(AppLogDataHelper.SYSTEM_CODE_UNB);
 		ApplicationLogger.setPolicyCode(clsName );
		ApplicationLogger.setJobName(jobName );	//休假管理及核保轄區設置
		ApplicationLogger.setOptionId(DateUtils.date2String(new Date(), "yyyyMMdd HHmmss"));
		ApplicationLogger.addLoggerData(com.ebao.pub.util.EnvUtils.getEnvLabel());
		ApplicationLogger.addLoggerData("Excel Upload begin");
		
		startTime_Upload= new Date();
		NBUtils.logger(this.getClass(), "parseUploadData [[Parse upload User_Area Excel Data ]] begin");
		logger.debug(""+ "\r\n"
				+ "parseUploadData [[Parse upload User_Area Excel Data ]] begin"+ "\r\n"
				//+ "["+ ""+ "]"+ "\r\n"
				+ "");
		try {
//    		xssfBook= new HSSFWorkbook(fileInputStream);
    		xssfBook= new XSSFWorkbook(fileInputStream);
    		xssfSheet= xssfBook.getSheetAt(0);
    		int xlsRowQty= xssfSheet.getPhysicalNumberOfRows();

    		Date startTime_format= new Date();
    		NBUtils.logger(this.getClass(), "parseUploadData [[Verify upload User_Area Data format]] begin");
    		logger.debug(""+ "\r\n"
    				+ "parseUploadData [[Verify upload User_Area Data format]] begin"+ "\r\n"
    				//+ "["+ ""+ "]"+ "\r\n"
    				+ "");
			//Verify Data format
    		mapRtnResult= verifyDataFormat(xssfBook, mapDataTable);
//    		mapRtnResult= userAreaDownUploadUtil.verifyDataFormat(xssfBook, mapDataTable);
    		NBUtils.logger(this.getClass(), "parseUploadData [[Verify upload User_Area Data format]] end"+ "\r\n"
    				+ "startTime["+ sdfYYYYMMDD_HMS.format(startTime_format)+ "]");
    		logger.debug(""+ "\r\n"
    				+ "parseUploadData [[Verify upload User_Area Data format]] end"+ "\r\n"
    				+ "startTime["+ sdfYYYYMMDD_HMS.format(startTime_format)+ "]"+ "\r\n"
    				//+ "["+ ""+ "]"+ "\r\n"
    				+ "");
    		
//    		logger.debug(""+ "\r\n"
//    				+ "############### After Parse-Data result is:\r\n"
//					+ "Success row qty:["+ (mapDataTable== null? 0: mapDataTable.size())+ "]"+ "\r\n"
//					//+ "["+ ""+ "]"+ "\r\n"
//					+ "");
    		
    		Date startTime_Data_Validity= new Date();
    		NBUtils.logger(this.getClass(), "parseUploadData [[Check upload User_Area Data Validity]] begin");
    		logger.debug(""+ "\r\n"
    				+ "parseUploadData [[Check upload User_Area Data Validity]] begin"+ "\r\n"
    				//+ "["+ ""+ "]"+ "\r\n"
    				+ "");
    		//Truncate Table Data
			//Check Data Exist?
			//Insert Data
    		HashMap<String, Object> mapRtnTmp= checkDataValidity(mapDataTable);
			errCodeStr= (String)mapRtnTmp.get(META_COL_ERR_CODE);
			errCodeNum= Integer.parseInt(errCodeStr);
			if (errCodeNum< 0) {
				TreeMap<Integer, String> errDataTmp= (TreeMap<Integer, String>) mapRtnResult.get(META_COL_ERR_DATA);
				TreeMap<Integer, String> errDataTmp2= (TreeMap<Integer, String>) mapRtnTmp.get(META_COL_ERR_DATA);
				if (errDataTmp== null) errDataTmp= new TreeMap<Integer, String>();
				
				if (errDataTmp2!= null && errDataTmp2.size()> 0) {
					for(int rowNumXls: errDataTmp2.keySet()) {
//					for(String rowNumXls: errDataTmp2.keySet()) {
						errDataTmp.put(rowNumXls, errDataTmp2.get(rowNumXls));
					}
					mapRtnResult.put(META_COL_ERR_DATA, (TreeMap<Integer, String>)errDataTmp.clone() );
				}
				mapRtnResult.put(META_COL_ERR_CODE, errCodeStr);
			}
    		NBUtils.logger(this.getClass(), "parseUploadData [[Check upload User_Area Data Validity]] end"+ "\r\n"
    				+ "startTime["+ sdfYYYYMMDD_HMS.format(startTime_Data_Validity)+ "]");
    		logger.debug(""+ "\r\n"
    				+ "parseUploadData [[Check upload User_Area Data Validity]] end"+ "\r\n"
    				+ "startTime["+ sdfYYYYMMDD_HMS.format(startTime_Data_Validity)+ "]"+ "\r\n"
    				//+ "["+ ""+ "]"+ "\r\n"
    				+ "");
			
    		errCodeStr= (String) mapRtnResult.get(META_COL_ERR_CODE );
    		errDataMap= (TreeMap<Integer, String>)mapRtnResult.get(META_COL_ERR_DATA);
    		failRowCnt= (errDataMap== null? 0+ 0: errDataMap.size() );
    		if (xlsRowQty<= 1) successRowCnt= 0;
    		else successRowCnt= (xlsRowQty- 1)- failRowCnt;
    		
//    		logger.debug(""+ "\r\n"
//    				+ "############### After Parse-Data result is:\r\n"
//					+ "Error Code:["+ errCodeStr+ "]"+ "\r\n"
//					+ "Total row qty in Excel:["+ xlsRowQty+ "]"+ "\r\n"
//					+ "Success row qty:["+ successRowCnt+ "]"+ "\r\n"
//					+ "Failure row qty:["+ failRowCnt+ "]"+ "\r\n"
//					//+ "["+ ""+ "]"+ "\r\n"
//					+ "");
			mapRtnResult.put(META_COL_PROC_SUMMARY, MessageFormat.format(templMsg
								, successRowCnt, failRowCnt, (xlsRowQty<= 0? xlsRowQty: xlsRowQty- 1)) );

			NBUtils.logger(this.getClass(), "parseUploadData [[Parse upload User_Area Excel Data ]] end"+ "\r\n"
					+ "startTime["+ sdfYYYYMMDD_HMS.format(startTime_Upload)+ "]");
			logger.debug(""+ "\r\n"
					+ "parseUploadData [[Parse upload User_Area Excel Data ]] end"+ "\r\n"
					+ "startTime["+ sdfYYYYMMDD_HMS.format(startTime_Upload)+ "]"+ "\r\n"
					//+ "["+ ""+ "]"+ "\r\n"
					+ "");
			
		}
		catch (Exception e) {
			throw e;
		}
		finally {
			ApplicationLogger.addLoggerData("Excel Upload end");
	     	ApplicationLogger.flush();  
		}
		return mapRtnResult;
	}
	
	/**
	 * 驗證 Excel每個  Cell資料內容的正確性
	 * @param xssfBook
	 * @param mapDataTable
	 * @return
	 * @throws Exception
	 */
	public HashMap<String, Object> verifyDataFormat(XSSFWorkbook xssfBook
							, HashMap<String, HashMap<String, String>> mapDataTable) throws Exception {
		TreeMap<Integer, String> errDataMap= new TreeMap<Integer, String>();
		int successRowCnt= 0;
		int failRowCnt= 0;
		int errNum_RowN= 0, errCode= 0;
		
		HashMap<String, Object> mapRtnResult= new HashMap<String, Object>();
		HashMap<String, String> tempRowMap= new HashMap<String, String>();
//		HSSFSheet xssfSheet= null;
//		HSSFRow xssfRowN= null;
		XSSFSheet xssfSheet= null;
		XSSFRow xssfRowN= null;

		try {
			xssfSheet= xssfBook.getSheetAt(0);
    		int iRowQty= xssfSheet.getPhysicalNumberOfRows();
    		
    		if (iRowQty<= 0) { errNum_RowN= 1; iRowQty= 0;}//內容為空時 
    		else if (iRowQty< 2) errNum_RowN= 1;//內容只有表頭行時
    		
    		for (int iRowCnt= 1; iRowCnt< iRowQty; iRowCnt++) {
//	    		HSSFCell hssfCellTmp= null;
	    		XSSFCell hssfCellTmp= null;
	    		String errData_RowN= "";

	    		errNum_RowN= 0;
    			String rowNumStr= Integer.toString(iRowCnt);
    			xssfRowN= xssfSheet.getRow(iRowCnt);
    			
	    		int iCellQty= xssfRowN.getPhysicalNumberOfCells();
	    		String channelCode= null, subChannelCode= null, userName= null;
	    		try {	
		    		//CHANNEL_CODE
		    		hssfCellTmp= xssfRowN.getCell(1);
		    		channelCode= (hssfCellTmp== null? null: (String) hssfCellTmp.getStringCellValue() );
		    		//SUB_CHANNEL_CODE
		    		hssfCellTmp= xssfRowN.getCell(2);
		    		subChannelCode= (hssfCellTmp== null? null: (String) hssfCellTmp.getStringCellValue() );
		    		//USER_NAME
		    		hssfCellTmp= xssfRowN.getCell(4);
		    		userName= (hssfCellTmp== null? null: (String) hssfCellTmp.getStringCellValue() );
	    		}
	    		catch (Exception e) {
	    			errNum_RowN= -1;
	    			errData_RowN+= ","+ ERR_MSG_EXCEL_CELL_MUST_STRING;	//Excel 欄位儲存格格式必需為'文字'
	    		}

//	    		logger.debug("############### Row Num["+ iRowCnt+ "]:\r\n"
//						+ "channelCode["+ channelCode+ "] "
//						+ "subChannelCode["+ subChannelCode+ "] "
//						+ "userName["+ userName+ "]"+ "\r\n"
//						//+ "["+ ""+ "]"+ "\r\n"
//						+ "");
	    		
	    		if (errNum_RowN>= 0) {
		    		//檢查資料格式
		    		if (StringUtils.isNullOrEmpty(channelCode) ) {//Channel_Code && Sub_Channel_Code不可為空 
		    			errNum_RowN= -1;
		    			if ( StringUtils.isNullOrEmpty(subChannelCode))
		    				errData_RowN+= ","+ ERR_MSG_EMPTY_SUB_CHANNEL_CODE;	//"Channel_Code && Sub_Channel_Code不可為空";
		    			else errData_RowN+= ","+ ERR_MSG_EMPTY_CHANNEL_CODE;	//"Channel_Code && Sub_Channel_Code不可為空";
		    		}
	
		    		if (StringUtils.isNullOrEmpty(userName) ) {//User_Name不可為空 
		    			errNum_RowN= -1;
		    			errData_RowN+= ","+ ERR_MSG_EMPTY_USER_NAME;	//"User_Name不可為空";
		    		}
	    		}
	    		
	    		if (errNum_RowN== 0) {
	    			successRowCnt++;
	    			tempRowMap= new HashMap<String, String>();
	    			tempRowMap.put(META_CHANNEL_CODE, channelCode);
	    			tempRowMap.put(META_SUB_CHANNEL_CODE, subChannelCode);
	    			tempRowMap.put(META_USER_NAME, userName);
	    			mapDataTable.put(rowNumStr, (HashMap<String, String>)tempRowMap.clone());
	    		}
	    		else {
	    			if (errCode== 0) errCode= errNum_RowN;
	    			failRowCnt++;
	    			errData_RowN= ( StringUtils.isNullOrEmpty(errData_RowN)? "": errData_RowN.substring(1) );
	    			errDataMap.put(iRowCnt, errData_RowN);
	    		}
	    		
    		}
			mapRtnResult.put(META_COL_ERR_CODE, Integer.toString(errCode) );
			if (errCode< 0) {
				mapRtnResult.put(META_COL_ERR_DATA, errDataMap);
			}

		}
		catch (Exception e) {
			throw e;
		}
		
		return mapRtnResult;
	}

	/**
	 * <p>Description : 檢核 Excel資料合法性並儲存</p>
	 * <p>PCR_41834 核保轄區設置作業調整及保留核保決定資料記錄 </p>
	 * <p>Created By : Alex Chang</p>
	 * <p>Create Time : 2020.12.15</p>
	 * @param mapDataTable
	 * @return
	 */
	public HashMap<String, Object> checkDataValidity(
						HashMap<String, HashMap<String, String>> mapDataTable) throws Exception {
		TreeMap<Integer, String> errDataMap= new TreeMap<Integer, String>();
		int successRowCnt= 0;
		int failRowCnt= 0;
		int errNum_RowN= 0, errCode= 0;

		SimpleDateFormat sdfYYYYMMDD_HMS=new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		DBean db = new DBean();
		PreparedStatement ps = null;
		ResultSet rs = null;
		//檢查上傳的資料是否存在的 SQL
		String templSql_CheckData= " Select emp.xlsRow_Num, chl.CHANNEL_CODE, chl.CHANNEL_ID, chl.Channel_Type "
	             + " , chl.Channel_Grade, chl.Is_Extenal, chl.Communicate_Id, emp.Emp_Id, emp.User_Name, emp.Dept_Id "
	             + " , emp.Organ_Id, ''{3}'' Is_SubChannel, ''{4}'' ChannelCode_Xls , ''{5}'' Sub_ChannelCode_Xls , ''{6}'' User_Name "
	        + " From (Select c.xlsRow_Num, d.Emp_Id, c.User_Name, d.Dept_Id, d.Organ_Id "
	                + " From (Select ''{0}'' xlsRow_Num, ''{1}'' User_Name From Dual) c "
	                    + " Left Join (Select ''{0}'' xlsRow_Num, Emp_Id, Dept_Id, Organ_Id "
	                                    + " From T_EMPLOYEE Where ROWNUM= 1 And User_Name= ''{1}'') d "
	                           + " On c.xlsRow_Num= d.xlsRow_Num ) emp "
	             + " Inner Join (Select a.xlsRow_Num, a.CHANNEL_CODE, b.CHANNEL_ID, b.Channel_Type, b.Channel_Grade, b.Is_Extenal, b.COMMUNICATE_ID "
	                            + " From (Select ''{0}'' xlsRow_Num, ''{2}'' CHANNEL_CODE From Dual) a "
	                                + " Left Join (Select ''{0}'' xlsRow_Num, co.Channel_Id, co.Channel_Type "
	                                            + " , co.Channel_Grade, sc.Is_Extenal, CO.COMMUNICATE_ID "
	                                            + " From v_nb_user_area_channel co "
	                                                + " Left Join T_Sales_CHANNEL sc On sc.Sales_CHANNEL_Id= co.Channel_Type "
	                                            + " Where ROWNUM= 1 And CHANNEL_CODE= ''{2}'') b "
	                                       + " On a.xlsRow_Num= b.xlsRow_Num) chl "
	                    + " On emp.xlsRow_Num= chl.xlsRow_Num ";
		
		HashMap<String, Object> mapRtnResult= new HashMap<String, Object>();
		HashMap<String, String> tempRowMap= new HashMap<String, String>();
		HashMap<String, String[]> validRowData= new HashMap<String, String[]>();
		try {
			db.connect();
			Connection con = db.getConnection();
			StringBuffer sqlTmpBuff= new StringBuffer();
			int rowCnt= 0, rowBatchCnt= 0;
			int rowQtyPerBatch= UPLOAD_EXCEL_ROW_QTY_PER_BATCH;

			Date startTime_CheckDataValidity= new Date();
    		NBUtils.logger(this.getClass(), "checkDataValidity [[Check Data Validity]] begin");
    		logger.debug(""+ "\r\n"
    				+ "checkDataValidity [[Check Data Validity]] begin"+ "\r\n"
    				//+ "["+ ""+ "]"+ "\r\n"
    				+ "");
			
			for(String rowNumStr: mapDataTable.keySet()) {
				boolean isSubChannel= true;
				String tmpStr= null;
				
				if (rowBatchCnt== 0) sqlTmpBuff= new StringBuffer();
				rowCnt++;
				rowBatchCnt++;
				HashMap<String, String> rowDataMap= mapDataTable.get(rowNumStr);
				String channelCodeXls= rowDataMap.get(META_CHANNEL_CODE);
				String subChannelCodeXls= rowDataMap.get(META_SUB_CHANNEL_CODE);
				String userNameXls= rowDataMap.get(META_USER_NAME);
				
				channelCodeXls= (channelCodeXls== null? "": channelCodeXls);
				subChannelCodeXls= (subChannelCodeXls== null? "": subChannelCodeXls);
				userNameXls= (userNameXls== null? "": userNameXls);

				if (StringUtils.isNullOrEmpty(subChannelCodeXls)) {
					isSubChannel= false;
					tmpStr= MessageFormat.format(templSql_CheckData, rowNumStr, userNameXls, channelCodeXls
													, (isSubChannel? "Y": "N"), channelCodeXls, subChannelCodeXls, userNameXls );
				}
				else {
					isSubChannel= true;
					tmpStr= MessageFormat.format(templSql_CheckData, rowNumStr, userNameXls, subChannelCodeXls
												, (isSubChannel? "Y": "N"), channelCodeXls, subChannelCodeXls, userNameXls );
				}

				sqlTmpBuff.append( (sqlTmpBuff.length()> 0? "\r\n"+ "Union All "+ "\r\n": "") )
							.append(tmpStr);
				
				if ( rowBatchCnt< rowQtyPerBatch  ) {
					if (rowCnt< mapDataTable.keySet().size())
						continue;
					else {
						//Latest Batch
					}
				}
				else rowBatchCnt= 0;

//	    		logger.debug(" \r\n"
//						+ "!!! Exec Batch"+ "\r\n"
//						+ "rowCnt["+ rowCnt+ "]"+ "\r\n"
//						+ "rowBatchCnt["+ rowBatchCnt+ "]"+ "\r\n"
//						+ "["+ sqlTmpBuff.toString()+ "]"+ "\r\n"
//						//+ "["+ ""+ "]"+ "\r\n"
//						+ "");
				
				Date startTime_Qry= new Date();
	    		logger.debug(""+ "\r\n"
	    				+ "checkDataValidity [[Check Data Validity]] query 'upload parameters' Validity START"+ "\r\n"
	    				+ "SQL["+ sqlTmpBuff.toString()+ "]"+ "\r\n"
	    				//+ "["+ ""+ "]"+ "\r\n"
	    				+ "");
				ps = con.prepareStatement(sqlTmpBuff.toString());
				rs= ps.executeQuery();
	    		logger.debug(""+ "\r\n"
	    				+ "checkDataValidity [[Check Data Validity]] query 'upload parameters' Validity complete"+ "\r\n"
	    				+ "startTime ["+ sdfYYYYMMDD_HMS.format( startTime_Qry)+ "]"+ "\r\n"
	    				//+ "["+ ""+ "]"+ "\r\n"
	    				+ "");
	
				while (rs.next()){
		    		String errData_RowN= "";
	    			String tmpStr2= null;
					boolean isPassed= true;
					errNum_RowN= 0;
	
	    			String rowNumStr_Xls= rs.getString("xlsRow_Num");
					String channelId= rs.getString("Channel_Id");
					String communicateId= rs.getString("Communicate_Id");
					String channelType= rs.getString("Channel_Type");
					String deptId= rs.getString("Dept_Id");
					String empId= rs.getString("Emp_Id");
					String organId= rs.getString("Organ_Id");
					String isSubChannelRec= rs.getString("Is_SubChannel");
					String channelGrade= rs.getString("Channel_Grade");
					String isExternal= rs.getString("Is_Extenal");
					String channelCode= rs.getString("ChannelCode_Xls");
					String subChannelCode= rs.getString("Sub_ChannelCode_Xls");
					String userName= rs.getString("User_Name");
					int rowNumXls= Integer.parseInt(rowNumStr_Xls);
	
//					logger.debug(" \r\n"
//							+ "rowNumStr_Xls["+ rowNumStr_Xls+ "]"+ "\r\n"
//							+ "channelId["+ channelId+ "]"+ "\r\n"
//							+ "channelType["+ channelType+ "]"+ "\r\n"
//							+ "deptId["+ deptId+ "]"+ "\r\n"
//							+ "empId["+ empId+ "]"+ "\r\n"
//							+ "organId["+ organId+ "]"+ "\r\n"
//							+ "isSubChannelRec["+ isSubChannelRec+ "]"+ "\r\n"
//							//+ "["+ ""+ "]"+ "\r\n"
//							+ "");
					
					if (channelId== null) {//Channel_Code or Sub_Channel_Code 不存在
						//Dara not exist! user_name or channel_code(sub_channel_code) error!!
		    			errNum_RowN= -2;
		    			if ("Y".equals( isSubChannelRec) ) {
		    				tmpStr2= MessageFormat.format(ERR_MSG_SUB_CHANNEL_CODE_NOT_EXIST, subChannelCode);	
		    				errData_RowN+= ","+ tmpStr2;
		    			}
		    			else {
		    				tmpStr2= MessageFormat.format(ERR_MSG_CHANNEL_CODE_NOT_EXIST, channelCode);	
		    				errData_RowN+= ","+ tmpStr2;
	//		    				errData_RowN+= ","+ ERR_MSG_CHANNEL_CODE_NOT_EXIST;	//"Channel_Code 不存在";
		    			}
						isPassed= false;
					}
					
					//若是"內部通路"(IS_Extenal='N')時, 若 channel_Grade!=2 且 違反 "communicate_Id存在 且 channel_Grade= 3"條件 就報錯
					if ("N".equals(isExternal) && !"2".equals(channelGrade)  
								&& !(!StringUtils.isNullOrEmpty(communicateId) && "3".equals(channelGrade)) ) {
		    			errNum_RowN= -2;
	    				tmpStr2= MessageFormat.format(ERR_MSG_INNER_CHANNEL_GRADE_NOT_2nd_Level, "");	
	    				errData_RowN+= ","+ tmpStr2;
						isPassed= false;
					}
					
					if (empId== null) {
		    			errNum_RowN= -2;
	    				tmpStr2= MessageFormat.format(ERR_MSG_USER_NAME_NOT_EXIST, userName);	
	    				errData_RowN+= ","+ tmpStr2;
						isPassed= false;
					}
					else {//Check employee is disabled or not!?
						boolean isUserDisabled= false;
						com.ebao.foundation.app.sys.sysmgmt.ds.UserVO userVO = userInfoQueryService.getUserByUserId(Long.parseLong(empId) );
						if (userVO != null) isUserDisabled= userVO.getDisable();
						else isUserDisabled= true;
						
						if (isUserDisabled) {	//Disabled User/Employee!	
			    			errNum_RowN= -2;
		    				tmpStr2= MessageFormat.format(ERR_MSG_USER_NAME_NOT_EXIST, userName);	
		    				errData_RowN+= ","+ tmpStr2;
							isPassed= false;
						}
					}
	
					if (isPassed) {//Normal User/Employee! 
						//Data exist!
						String[] tmpAry= new String[] {channelId, channelType, deptId, empId, organId};
						validRowData.put(rowNumStr_Xls, tmpAry.clone());
					}
					else {
	    				errData_RowN= ( StringUtils.isNullOrEmpty(errData_RowN)? "": errData_RowN.substring(1) );
		    			errDataMap.put(rowNumXls, errData_RowN);
						failRowCnt++;
					}
					
					if (errCode== 0) errCode= errNum_RowN;
				}	//END of while(rs.next...
				
				if (rs!= null) {rs.close(); rs= null;}
				if (ps!= null) {ps.close(); ps= null;}
			}
    		NBUtils.logger(this.getClass(), "checkDataValidity [[Check Data Validity]] end"+ "\r\n"
    				+ "startTime ["+ sdfYYYYMMDD_HMS.format( startTime_CheckDataValidity)+ "]");
    		logger.debug(""+ "\r\n"
    				+ "checkDataValidity [[Check Data Validity]] end"+ "\r\n"
    				+ "startTime ["+ sdfYYYYMMDD_HMS.format( startTime_CheckDataValidity)+ "]"+ "\r\n"
    				//+ "["+ ""+ "]"+ "\r\n"
    				+ "");

			Date startTime_Save_to_DB= new Date();
    		NBUtils.logger(this.getClass(), "checkDataValidity [[Save to DB]] begin");
    		logger.debug(""+ "\r\n"
    				+ "checkDataValidity [[Save to DB]] begin"+ "\r\n"
    				//+ "["+ ""+ "]"+ "\r\n"
    				+ "");
			//Save validated data row
			if (validRowData!= null && validRowData.size()> 0) {
				//Delete all row in T_User_Area_Conf table
				userLeaveManagerService.removeUserAreaConf();

				//Insert it!!
				HashMap<String, String> rtnErrMap= userLeaveManagerService.saveUserAreaConfs(validRowData);
				
				if (rtnErrMap== null || rtnErrMap.size()== 0) {
		    		logger.debug("############### \r\n"
							+ "Import excel data success(qyt:["+ validRowData.size()+ "])!!"+ "\r\n"
							//+ "["+ ""+ "]"+ "\r\n"
							+ "");
				}
			}
    		NBUtils.logger(this.getClass(), "checkDataValidity [[Save to DB]] end"+ "\r\n"
    				+ "startTime ["+ sdfYYYYMMDD_HMS.format( startTime_Save_to_DB)+ "]");
    		logger.debug(""+ "\r\n"
    				+ "checkDataValidity [[Save to DB]] end"+ "\r\n"
    				+ "startTime ["+ sdfYYYYMMDD_HMS.format( startTime_Save_to_DB)+ "]"+ "\r\n"
    				//+ "["+ ""+ "]"+ "\r\n"
    				+ "");
			
			mapRtnResult.put(META_COL_ERR_CODE, Integer.toString(errCode) );
			if (errDataMap!= null && errDataMap.size()> 0)
				mapRtnResult.put(META_COL_ERR_DATA, (TreeMap<Integer, String>)errDataMap.clone() );

		}
		catch (Exception e) {
			throw e;
		}
		
		return mapRtnResult;
	}

}
