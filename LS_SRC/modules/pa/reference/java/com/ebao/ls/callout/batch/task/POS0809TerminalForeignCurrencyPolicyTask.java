package com.ebao.ls.callout.batch.task;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.transaction.UserTransaction;

import org.apache.commons.collections.MapUtils;
import org.springframework.context.ApplicationContextAware;

import com.ebao.foundation.commons.exceptions.ExceptionUtil;
import com.ebao.foundation.core.logging.Log;
import com.ebao.foundation.module.para.Para;
import com.ebao.ls.callout.batch.AbstractCalloutTask;
import com.ebao.ls.callout.batch.CalloutConstants;
import com.ebao.ls.callout.batch.CalloutDataExportService;
import com.ebao.ls.callout.data.bo.CalloutConfig;
import com.ebao.ls.cs.ci.TeleInterviewCI;
import com.ebao.ls.cs.commonflow.ds.cmnquery.PolicyQueryService;
import com.ebao.ls.pa.pub.bs.PolicyService;
import com.ebao.ls.pa.pub.vo.CoverageVO;
import com.ebao.ls.pa.pub.vo.PolicyVO;
import com.ebao.ls.pub.cst.CalloutConfigType;
import com.ebao.pub.batch.help.BatchHelp;
import com.ebao.pub.batch.type.JobStatus;
import com.ebao.pub.batch.type.LogLevel;
import com.ebao.pub.batch.util.BatchLogUtils;
import com.ebao.pub.util.TransUtils;

public class POS0809TerminalForeignCurrencyPolicyTask extends AbstractCalloutTask implements  ApplicationContextAware {
    
    public static final String BEAN_DEFAULT = "zeroEightZeroNineTask";

    private final Log log = Log.getLogger(POS0809TerminalForeignCurrencyPolicyTask.class);
    
    private final Long DISPLAY_ORDER = 9L;

    private String CONFIG_TYPE = CalloutConfigType.CALLOUT_CONFIG_TYPE_080;
    
    @Resource(name=CalloutDataExportService.BEAN_DEFAULT)
    private CalloutDataExportService exportServ;
    
    @Resource(name = TeleInterviewCI.BEAN_DEFAULT)
    private TeleInterviewCI pos;

    @Resource(name = PolicyService.BEAN_DEFAULT)
    private PolicyService paPolicyService;

    @Resource(name = PolicyQueryService.BEAN_DEFAULT)
    private PolicyQueryService policyQueryService;
        
    private void syso(Object msg){
        log.info("[BR-CMN-TFI-005][檔案080-9 外幣保單執行解約]" +msg);
        BatchLogUtils.addLog(LogLevel.INFO, null, "[BR-CMN-TFI-005][檔案080-9 外幣保單執行解約]" +msg);
    }
    
    private void syso(String layout, Object... msg){
        syso(String.format(layout, msg));
    }
    
    @Override
    public int mainProcess() throws Exception {
        String fileName = Para.getParaValue(CalloutConstants.ZeroEightZeroNine);
        CalloutConfig cfg = getConfig(CONFIG_TYPE, DISPLAY_ORDER);
        Long sampleType = cfg.getListId();
        if(sampleType == null)
            return JobStatus.EXECUTE_SUCCESS;
        
        /* 抽取母體資料 */
        List<Map<String, Object>> mapList = getData(BatchHelp.getProcessDate(), null);
        mapList = posCleanDuplicateBatch(mapList, sampleType);
        
        int totalSize = mapList != null  ? mapList.size()  : 0;
        syso("母體資料 List.size = " + totalSize);
        if(totalSize == 0)
            return JobStatus.EXECUTE_SUCCESS;
        
        /* 抽取樣本資料*/
        Integer sampleSize = readSample(mapList, CONFIG_TYPE, DISPLAY_ORDER);
        syso("資料抽樣筆數 = " + sampleSize);
        
        /* 儲存電訪紀錄 */
        UserTransaction ut = null;
        try{
            ut = TransUtils.getUserTransaction();
            ut.begin();
            save(mapList, BatchHelp.getProcessDate(), fileName, "080", "080-09", CONFIG_TYPE, DISPLAY_ORDER);
            /* 匯出抽樣保單 */
            exportServ.feedMap(mapList, CalloutConstants.ZeroEightZeroNine, BatchHelp.getProcessDate());
            ut.commit();
        }catch(Exception ex){
            TransUtils.rollback(ut);
            BatchLogUtils.addLog(LogLevel.ERROR, null, "[BR-CMN-TFI-005][檔案080-9 外幣保單執行解約]\n" + ExceptionUtil.parse(ex));
            return JobStatus.EXECUTE_FAILED;
        }
        return JobStatus.EXECUTE_SUCCESS;
    }
    
    public java.util.List<Map<String, Object>> getData(Date inputDate, String policyCode){
        Calendar cal=Calendar.getInstance();
        cal.setTime(inputDate);
        List<Map<String, Object>> mapList = pos.getTerminalForeignCurrencyPolicy(cal.getTime(), cal.getTime());
        for(Map<String, Object> map :mapList){
            syso("抽樣保單號碼 = " + MapUtils.getString(map, "POLICY_CODE"));
    		PolicyVO policyVO = paPolicyService.load(MapUtils.getLong(map, "POLICY_ID")); 
    		CoverageVO masterCoverageVO = policyVO.gitMasterCoverage();
    		map.put("MASTER_NEW_PRODUCT_CODE", "");
			map.put("MASTER_PRODUCT_NAME", "");
    		if(masterCoverageVO != null){
    			map.put("MASTER_NEW_PRODUCT_CODE", policyQueryService.getNewProductCodeVersion(masterCoverageVO));
    			map.put("MASTER_PRODUCT_NAME", policyQueryService.getProductName(masterCoverageVO.getProductVersionId()));
    		}
        }
        return mapList;
    }
    
    @Override
    public String saveToTrans(Map<String, Object> map, Date processDate,
                    String fileName, String dept) {
        String calloutNum = savePolicyHolder(map, processDate, fileName, dept);
        return calloutNum;
    }    
   
}
