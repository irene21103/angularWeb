package com.ebao.ls.uw.ctrl.search;

import com.ebao.pub.framework.GenericForm;

/**
 * 
 * <p>Title:Underwriting</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: eBaoTech Corporation</p>
 * @author mingchun.shi
 * @since  Jun 27, 2005
 * @version 1.0
 */
public class UwHistorySearchForm extends GenericForm {

  private java.lang.String uwAppTypeCode = "";

  private java.lang.String policyCode = "";

  private java.lang.String applyCode = "";

  private java.lang.String displayStatus = "";

  private java.util.Collection resultSearch;

  /**
   * returns the value of the applyCode
   *
   * @return the applyCode
   */
  public String getApplyCode() {
    return applyCode;
  }

  /**
   * sets the value of the applyCode
   *
   * @param applyCode the applyCode
   */
  public void setApplyCode(String applyCode) {
    this.applyCode = applyCode;
  }

  /**
   * returns the value of the policyCode
   *
   * @return the policyCode
   */

  public String getPolicyCode() {
    return policyCode;
  }

  /**
   * set policyCode
   * @param policyCode
   */
  public void setPolicyCode(String policyCode) {
    this.policyCode = policyCode;
  }

  /**
   * returns the value of the resultSearch
   *
   * @return the resultSearch
   */

  public java.util.Collection getResultSearch() {
    return resultSearch;
  }

  /**
   * sets the value of the resultSearch
   *
   * @param resultSearch the resultSearch
   */

  public void setResultSearch(java.util.Collection resultSearch) {
    this.resultSearch = resultSearch;
  }

  /**
   * returns the value of the uwAppTypeCode
   *
   * @return the uwAppTypeCode
   */

  public String getUwAppTypeCode() {
    return uwAppTypeCode;
  }

  /**
   * sets the value of the uwAppTypeCode
   *
   * @param uwAppTypeCode the uwAppTypeCode
   */

  public void setUwAppTypeCode(String uwAppTypeCode) {
    this.uwAppTypeCode = uwAppTypeCode;
  }

  /**
   * returns the value of the displayStatus
   *
   * @return the displayStatus
   */

  public String getDisplayStatus() {
    return displayStatus;
  }

  /**
   * sets the value of the displayStatus
   *
   * @param displayStatus the displayStatus
   */

  public void setDisplayStatus(String displayStatus) {
    this.displayStatus = displayStatus;
  }
}
