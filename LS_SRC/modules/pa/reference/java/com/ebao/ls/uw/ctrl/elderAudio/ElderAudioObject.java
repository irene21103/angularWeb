package com.ebao.ls.uw.ctrl.elderAudio;

import java.io.Serializable;
import java.lang.reflect.Field;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.ebao.foundation.module.para.Para;
import com.ebao.ls.pa.nb.vo.UnbRoleVO;
import com.ebao.ls.pub.CodeCstTgl;
import com.ebao.pub.util.DateUtils;

public class ElderAudioObject extends UnbRoleVO implements Serializable {
	private static final long serialVersionUID = 4172144169273360962L;

	// 取參數: 高齡客戶年齡定義
	private Integer elderAgeDef = Integer.valueOf(Para.getParaValue(CodeCstTgl.UNB_ELDER_AGE_DEF));

	// 保險年齡
	private Long ageInsurance;
	// 是否符合高齡資格
	private boolean isTargetObject = false;
	// 畫面上的生日
	private String birthDateStr;

	public Long getAgeInsurance() {
		return ageInsurance;
	}

	public void setAgeInsurance(Long ageInsurance) {
		this.ageInsurance = ageInsurance;
		if (elderAgeDef <= ageInsurance)
			setTargetObject(true);
	}

	public boolean isTargetObject() {
		return isTargetObject;
	}

	public void setTargetObject(boolean isTargetObject) {
		this.isTargetObject = isTargetObject;
	}

	public String getBirthDateStr() {
		return birthDateStr;
	}

	public void setBirthDateStr(String birthDateStr) {
		this.birthDateStr = birthDateStr;
		if (StringUtils.isNotBlank(birthDateStr)) {
			setBirthdate(DateUtils.toDate(birthDateStr));
		}
	}

	public boolean isEmpty() {
		for (Field field : this.getClass().getDeclaredFields()) {
			try {
				field.setAccessible(true);
				if (field.get(this) != null) {
					return false;
				}
			} catch (Exception e) {
				System.out.println("Exception occured in processing");
			}
		}
		return true;
	}

	public String toString() {
		String properties = ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
		return properties;
	}

}
