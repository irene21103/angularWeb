package com.ebao.ls.uw.ds;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.activation.DataHandler;
import javax.annotation.Resource;
import javax.imageio.stream.ImageOutputStream;
import javax.transaction.UserTransaction;

import org.apache.commons.collections4.MapUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ebao.foundation.module.db.hibernate.HibernateSession3;
import com.ebao.foundation.module.util.i18n.CodeTable;
import com.ebao.foundation.module.util.i18n.StringResource;
import com.ebao.ls.callout.data.bo.UwWorkSheetImage;
import com.ebao.ls.clm.ci.ClaimCI;
import com.ebao.ls.clm.ci.ClaimQueryInfoCIVO;
import com.ebao.ls.foundation.service.GenericServiceImpl;
import com.ebao.ls.image.bs.ImageScanVO;
import com.ebao.ls.image.ci.ImageCI;
import com.ebao.ls.image.vo.ImageVO;
import com.ebao.ls.liaRoc.LiaRocCst;
import com.ebao.ls.liaRoc.ci.LiaRocDownload2020CI;
import com.ebao.ls.liaRoc.ci.LiaRocDownloadCI;
import com.ebao.ls.pa.nb.bs.ODSService;
import com.ebao.ls.pa.nb.bs.rule.ProposalRuleMsgService;
import com.ebao.ls.pa.nb.bs.rule.ProposalRuleResultService;
import com.ebao.ls.pa.nb.ci.ValidatorService;
import com.ebao.ls.pa.nb.util.Cst;
import com.ebao.ls.pa.nb.util.NBUtils;
import com.ebao.ls.pa.nb.vo.ProposalRuleResultVO;
import com.ebao.ls.pa.pub.bs.InsuredService;
import com.ebao.ls.pa.pub.ci.PolicyCI;
import com.ebao.ls.pa.pub.service.PolicyService;
import com.ebao.ls.pa.pub.vo.CoverageVO;
import com.ebao.ls.pa.pub.vo.InsuredVO;
import com.ebao.ls.pa.pub.vo.PolicyHolderVO;
import com.ebao.ls.pa.pub.vo.PolicyVO;
import com.ebao.ls.pa.pub.vo.UwNotintoRecvVO;
import com.ebao.ls.pty.ci.CustomerCI;
import com.ebao.ls.pty.data.bo.Customer;
import com.ebao.ls.pty.data.query.CustomerDao;
import com.ebao.ls.pub.CodeCst;
import com.ebao.ls.pub.cst.ProposalRuleMsg;
import com.ebao.ls.pub.data.InsuredVOComparator;
import com.ebao.ls.qry.ds.policy.ClaimInfoVO;
import com.ebao.ls.riskAggregation.bs.AggrSaPremResultService;
import com.ebao.ls.riskAggregation.vo.AggrSaPremResultVO;
import com.ebao.ls.riskAggregation.vo.RAInfoVO;
import com.ebao.ls.riskAggregation.vo.RiskInfoVO;
import com.ebao.ls.riskPrevention.ci.RiskCustomerLogCI;
import com.ebao.ls.riskPrevention.vo.RiskCustomerLogQueryVO;
import com.ebao.ls.uw.bo.NbClaimHistory;
import com.ebao.ls.uw.bo.NbInsureHistory;
import com.ebao.ls.uw.bo.NbLiaRocHistory;
import com.ebao.ls.uw.bo.NbPosHistory;
import com.ebao.ls.uw.bo.NbRiskCustomerHistory;
import com.ebao.ls.uw.bo.NbRiskHistory;
import com.ebao.ls.uw.ctrl.info.QualityRecordActionHelper;
import com.ebao.ls.uw.data.UwInsureHistoryDelegate;
import com.ebao.ls.uw.data.UwWorkSheetDao;
import com.ebao.ls.uw.data.UwWorkSheetDelegate;
import com.ebao.ls.uw.data.UwWorkSheetImageDelegate;
import com.ebao.ls.uw.data.bo.UwWorkSheet;
import com.ebao.ls.uw.ds.vo.LiaRocInfoForm;
import com.ebao.ls.uw.ds.vo.LiaRocInfoForm.CompanyDetailVO;
import com.ebao.ls.uw.ds.vo.LiaRocInfoForm.DetailVO;
import com.ebao.ls.uw.ds.vo.UwHistoryInsureVO;
import com.ebao.ls.uw.ds.vo.UwInsureClientVO;
import com.ebao.ls.uw.vo.UwTransferVO;
import com.ebao.ls.uw.vo.UwWorkSheetVO;
import com.ebao.ls.ws.ci.cr.CrystalReportWSCI;
import com.ebao.ls.ws.cr.vo.KernelParameter;
import com.ebao.ls.ws.vo.clm.clm070.ClaimStatusVO;
import com.ebao.ls.ws.vo.clm.clm070.DiagnosisVO;
import com.ebao.ls.ws.vo.clm.clm070.PolicyDataVO;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.framework.GenericException;
import com.ebao.pub.framework.RTException;
import com.ebao.pub.para.Para;
import com.ebao.pub.util.DateUtils;
import com.ebao.pub.util.EnvUtils;
import com.ebao.pub.util.IcePdfUtil;
import com.ebao.pub.util.StringUtils;
import com.ebao.pub.util.Trans;
import com.ebao.pub.util.TransUtils;

import edu.emory.mathcs.backport.java.util.Collections;

/**
 * <p>Title: 契審表 列印紀錄 ServiceImpl</p>
 * <p>Description: 契審表 列印紀錄 ServiceImpl</p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company: TGL Co., Ltd.</p>
 * <p>Create Time: Aug 3, 2016</p> 
 * @author 
 * <p>Update Time: Aug 3, 2016</p>
 * <p>Updater: Alex Cheng</p>
 * <p>Update Comments: </p>
 */
public class UwWorkSheetServiceImpl extends GenericServiceImpl<UwWorkSheetVO, UwWorkSheet, UwWorkSheetDelegate> implements UwWorkSheetService {

	private static final Logger log = LoggerFactory
			.getLogger(UwWorkSheetServiceImpl.class);

	final String CHANGE_MANUAL = "調整風險";

	final String CHANGE_FAMOUS = "高知名度";

	@Resource(name = CrystalReportWSCI.BEAN_DEFAULT)
	private CrystalReportWSCI crReportWSCI;

	@Resource(name = UwWorkSheetDao.BEAN_DEFAULT)
	private UwWorkSheetDao uwWorkSheetDao;

	@Resource(name = UwWorkSheetImageDelegate.BEAN_DEFAULT)
	private UwWorkSheetImageDelegate uwWorkSheetImageDao;

	@Resource(name = UwInsureHistoryService.BEAN_DEFAULT)
	private UwInsureHistoryService uwInsureHistoryService;

	@Resource(name = InsuredService.BEAN_DEFAULT)
	private InsuredService insuredService;

	@Resource(name = PolicyCI.BEAN_DEFAULT)
	private PolicyCI policyCI;

	@Resource(name = ClaimCI.BEAN_DEFAULT)
	private ClaimCI claimCI;

	@Resource(name = RiskCustomerLogCI.BEAN_DEFAULT)
	private RiskCustomerLogCI riskCustomerLogCI;

	@Resource(name = ODSService.BEAN_DEFAULT)
	private ODSService paODSService;

	@Resource(name = QualityRecordActionHelper.BEAN_DEFAULT)
	private QualityRecordActionHelper qualityRecordActionHelper;

	@Resource(name = LiaRocDownloadCI.BEAN_DEFAULT)
	private LiaRocDownloadCI liaRocDownloadCI;

	@Resource(name = PolicyService.BEAN_DEFAULT)
	private PolicyService policyService;

	@Resource(name = UwNotintoRecvService.BEAN_DEFAULT)
	private UwNotintoRecvService uwNotintoRecvService;

	@Resource(name = ImageCI.BEAN_DEFAULT)
	private ImageCI imageCI;

	@Resource(name = ProposalRuleMsgService.BEAN_DEFAULT)
	private ProposalRuleMsgService proposalRuleMsgService;

	@Resource(name = ProposalRuleResultService.BEAN_DEFAULT)
	private ProposalRuleResultService proposalRuleResultService;

	@Resource(name = com.ebao.ls.pa.nb.bs.rule.CoverageInsuredService.BEAN_DEFAULT)
	public com.ebao.ls.pa.nb.bs.rule.CoverageInsuredService coverageInsuredService;

	@Resource(name = AggrSaPremResultService.BEAN_DEFAULT)
	private AggrSaPremResultService aggrSaPremResultService;

    @Resource(name = UwTransferService.BEAN_DEFAULT)
    private UwTransferService uwTransferService;

	@Resource(name = LiaRocDownload2020CI.BEAN_DEFAULT)
	private LiaRocDownload2020CI liaRocDownload2020CI;

	@Resource(name = UwInsureHistoryDelegate.BEAN_DEFAULT)
    private UwInsureHistoryDelegate uwInsureHistoryDao;
	
	@Resource(name = com.ebao.ls.pa.pub.bs.PolicyService.BEAN_DEFAULT)
	private com.ebao.ls.pa.pub.bs.PolicyService policyDS;
	
	@Resource(name = CustomerCI.BEAN_DEFAULT)
	private CustomerCI customerCI;
	
	@Resource(name = ValidatorService.BEAN_DEFAULT)
	private ValidatorService validatorService;
	
	/**
	 * <p>Description : 清除新契約契審表暫存資料</p>
	 * <p>Created By : Alex Cheng</p>
	 * <p>Create Time : Sep 4, 2016</p>
	 * @param policyId
	 * @param underwriteId
	 * @returnT
	 * @throws Exception
	 */
	public boolean deleteUwWorkSheetTempTable(Long policyId, Long underwriteId) throws Exception {
		boolean retBln = false;
		NBUtils.logger(this.getClass(), "deleteUwWorkSheetTempTable() start");
		retBln = uwWorkSheetDao.deleteNbInsureHistoryData(policyId);
		NBUtils.logger(this.getClass(), "deleteNbInsureHistoryData() done");
		retBln = uwWorkSheetDao.deleteNbClaimHistoryData(policyId);
		NBUtils.logger(this.getClass(), "deleteNbClaimHistoryData() done");
		retBln = uwWorkSheetDao.deleteNbPosHistoryData(underwriteId);
		NBUtils.logger(this.getClass(), "deleteNbPosHistoryData() done");
		retBln = uwWorkSheetDao.deleteNbRiskHistoryData(policyId);
		NBUtils.logger(this.getClass(), "deleteNbRiskHistoryData() done");
		retBln = uwWorkSheetDao.deleteNbRiskCustomerHistoryData(policyId);
		NBUtils.logger(this.getClass(), "deleteNbRiskCustomerHistoryData() done");
		retBln = uwWorkSheetDao.deleteNbLiarocData(policyId);
		NBUtils.logger(this.getClass(), "deleteNbLiarocData() done");
		NBUtils.logger(this.getClass(), "deleteUwWorkSheetTempTable() end");
		return retBln;
	}

	/**
	 * <p>Description : 新增契審表 投保歷史資訊暫存資料表</p>
	 * <p>Created By : Alex Cheng</p>
	 * <p>Create Time : Sep 4, 2016</p>
	 * @return
	 */
	@Override
	public boolean insertNbInsureHistoryData(Long policyId) throws Exception {

		boolean isGroupDownlaodOK = true;

		if (policyId != null) {

			List<UwInsureClientVO> insureClientList = new ArrayList<UwInsureClientVO>();

			isGroupDownlaodOK = uwInsureHistoryService.findUwInsuredHistoryRecord(
					insureClientList, String.valueOf(policyId));

			for (UwInsureClientVO uwInsureClientVO : insureClientList) {
				for (UwHistoryInsureVO uwHistoryInsureVO : uwInsureClientVO.getHistoryInsureList()) {
					NbInsureHistory bo = new NbInsureHistory();
					bo.setPolicyId(policyId);
					bo.setHistoryName(uwInsureClientVO.getName());
					bo.setHistoryCertiCode(uwInsureClientVO.getCertiCode());
					bo.setHistoryPolicyCode(uwHistoryInsureVO.getPolicyCode());
					bo.setValidateDate(uwHistoryInsureVO.getValidateDate());
					bo.setLiabilityName(uwHistoryInsureVO.getLiabilityName());
					bo.setHolderName(uwHistoryInsureVO.getHolderName());
					bo.setInsutrdName(uwHistoryInsureVO.getInsuredName());
					bo.setInternalId(uwHistoryInsureVO.getInternalId());
					bo.setAmount(uwHistoryInsureVO.getAmountName());
					bo.setMoneyCode(CodeTable.getCodeDesc("T_MONEY", uwHistoryInsureVO.getMoneyId(), AppContext.getCurrentUser().getLangId()));
					bo.setExtraprem(uwHistoryInsureVO.getExtraPrem());
					bo.setExclusion(uwHistoryInsureVO.getExclusion());
					bo.setMedical(uwHistoryInsureVO.getMedical());
					bo.setInterView(uwHistoryInsureVO.getInterView());
					bo.setSurvival(uwHistoryInsureVO.getSurvival());
					bo.setImage(uwHistoryInsureVO.getImageIndi());
					bo.setOrderType(Integer.valueOf(uwInsureClientVO.getOrderType()));
					bo.setEmpyPay(uwHistoryInsureVO.getEmpyPay());
					bo.setInitialInstalmentAmountAn(uwHistoryInsureVO.getFirstYearAmount());
					bo.setServicesBenefitLevel(uwHistoryInsureVO.getServicesBenefitLevel());
					bo.setOldForeignerId(uwInsureClientVO.getOldForeignerId());
			
					uwWorkSheetDao.insertNbInsureHistoryData(bo);
				}
			}
		}

		return isGroupDownlaodOK;
	}

	/**
	 * <p>Description : 新增契審表 理賠歷史資訊暫存資料表</p>
	 * <p>Created By : Alex Cheng</p>
	 * <p>Create Time : Sep 4, 2016</p>
	 * @param policyId
	 * @return
	 */
	@Override
	public boolean insertNbClaimHistoryData(Long policyId) throws Exception {
		boolean retBln = false;
		if (policyId != null) {
			List<ClaimInfoVO> claimInfo = new ArrayList<ClaimInfoVO>();
			List<InsuredVO> claimInsured = uwInsureHistoryService.findInsuredById(String.valueOf(policyId));
			//	      找所有被保人的所有理賠記錄保單;
			for (InsuredVO insuredVO : claimInsured) {
				
				if(!StringUtils.isNullOrEmpty(insuredVO.getCertiCode())) {
					List<Customer> customerList = new CustomerDao().findIndivCustomerByTGLCondition( insuredVO.getCertiCode(),null,null );
					for( Customer customerVO : customerList ) {
						if (customerVO != null && customerVO.getCustomerId() != null){
							ClaimQueryInfoCIVO[] claimInfoh = claimCI.retrieveClaimHistroryByInsured(customerVO.getCustomerId()).getClaimInfos();
							if (claimInfoh != null) {
								for (ClaimQueryInfoCIVO claimCIVO : claimInfoh) {
									ClaimInfoVO vo = new ClaimInfoVO();
									this.insertClaimInfo(claimCIVO, vo);
									vo.setOfficerName(insuredVO.getName());
									vo.setCertiCode(insuredVO.getCertiCode());
									/* CASE_STATUS__SETTLED表示理賠完成，才需要顯示理賠金額 */
									//if (String.valueOf(CodeCst.CASE_STATUS__SETTLED).equals(claimCIVO.getCaseStatus())) {
									vo.setPolicyAmounts(claimCIVO.getPolicyAmounts());
									//}
									claimInfo.add(vo);
								}
							}
						}
					}
					
					// 系統調用接口將服務管理平台取得被保人的團險 旅平險理賠記錄查詢 ;
					List<ClaimQueryInfoCIVO> claimInfoODS = paODSService.getClaimByCertiCode(insuredVO.getCertiCode());
					if (claimInfoODS != null) {
						for (ClaimQueryInfoCIVO claimCIVO : claimInfoODS) {
							ClaimInfoVO vo = new ClaimInfoVO();
							this.insertClaimInfo(claimCIVO, vo);
							vo.setOfficerName(claimCIVO.getOfficerName());
							vo.setCertiCode(insuredVO.getCertiCode());
							vo.setPolicyAmounts(claimCIVO.getPolicyAmounts());

							if (claimCIVO.getCaseId() == null) {
								// 2016 0916 add by sunny in group policy not
								// diagnosisId, use the field
								vo.setAuditDecision(claimCIVO.getAuditDecision());
							} else {
								vo.setAuditDecision("");
							}
							
							vo.setDiagnosisStr(claimCIVO.getAuditDecision()); //NB_CLAIM_HISTORY需塞的值
							
							claimInfo.add(vo);
						}
					}
				}
				
				// PCR-415346 有舊式統一證號時要用舊式統一證號讀理賠檔 2021/07/06 add by Kathy
				if (!StringUtils.isNullOrEmpty(insuredVO.getOldForeignerId())) {
					List<Customer> customerList = new CustomerDao().findIndivCustomerByTGLCondition( insuredVO.getOldForeignerId(),null,null);

					for( Customer customerVO : customerList ) {
						if (customerVO != null && customerVO.getCustomerId() != null){
							ClaimQueryInfoCIVO[] claimInfoOldfh = claimCI.retrieveClaimHistroryByInsured(customerVO.getCustomerId()).getClaimInfos();

							if (claimInfoOldfh != null) {
								for (ClaimQueryInfoCIVO claimCIVO : claimInfoOldfh) {
									ClaimInfoVO vo = new ClaimInfoVO();
									this.insertClaimInfo(claimCIVO, vo);
									vo.setOfficerName(insuredVO.getName());
									vo.setCertiCode(insuredVO.getOldForeignerId());
									/* CASE_STATUS__SETTLED表示理賠完成，才需要顯示理賠金額 */
									//if (String.valueOf(CodeCst.CASE_STATUS__SETTLED).equals(claimCIVO.getCaseStatus())) {
									vo.setPolicyAmounts(claimCIVO.getPolicyAmounts());
									//}
									claimInfo.add(vo);
								}
							}
						}
					}
					
					// 系統調用接口將服務管理平台取得被保人舊式統一證號的團險 旅平險理賠記錄查詢 ;
					List<ClaimQueryInfoCIVO> claimInfoOldfODS = paODSService.getClaimByCertiCode(insuredVO.getOldForeignerId());
					if (claimInfoOldfODS != null) {
						for (ClaimQueryInfoCIVO claimCIVO : claimInfoOldfODS) {
							ClaimInfoVO vo = new ClaimInfoVO();
							this.insertClaimInfo(claimCIVO, vo);
							vo.setOfficerName(claimCIVO.getOfficerName());
							vo.setCertiCode(insuredVO.getCertiCode());
							vo.setPolicyAmounts(claimCIVO.getPolicyAmounts());

							if (claimCIVO.getCaseId() == null) {
								// 2016 0916 add by sunny in group policy not
								// diagnosisId, use the field
								vo.setAuditDecision(claimCIVO.getAuditDecision());
							} else {
								vo.setAuditDecision("");
							}
							
							vo.setDiagnosisStr(claimCIVO.getAuditDecision()); //NB_CLAIM_HISTORY需塞的值
							
							claimInfo.add(vo);
						}
					}
				}
				
				//舊系統理賠記錄
				List<ClaimStatusVO> claimStatusVoList = validatorService.getClaimStatusVoListByCertiCode(insuredVO.getCertiCode());
				if( !StringUtils.isNullOrEmpty(insuredVO.getOldForeignerId() ) ) {
					claimStatusVoList.addAll( validatorService.getClaimStatusVoListByCertiCode( insuredVO.getOldForeignerId() ) );
				}
				
				if (claimStatusVoList != null) {
					//使用方法參照:ClaimCaseServiceImpl.doGroupClaim()
					for (ClaimStatusVO vo : claimStatusVoList) {
						
						String caseNo = vo.getCaseNo();
						String accidentName = vo.getAccidentName();
						Date accidentTime = vo.getAccidentTime();
						String certiCode = vo.getCertCode();
						String accidentReasonDesc = vo.getAccidentReasonDesc();
						String harm1Desc = vo.getHarm1Desc();
						String akindDesc = vo.getAkindDesc();
						List<DiagnosisVO> diagnosisVoList = vo.getDiagnosisList();
						StringBuffer accidentReasonDescSB = new StringBuffer();
						
						if( accidentReasonDesc != null && !"".equals(accidentReasonDesc)) {
							accidentReasonDescSB.append(accidentReasonDesc);
						}
						
						if( harm1Desc != null && !"".equals(harm1Desc) ) {
							if("".equals(accidentReasonDescSB.toString())) {
								accidentReasonDescSB.append(harm1Desc);
							}else {
								accidentReasonDescSB.append("/").append(harm1Desc);
							}
						}
						
						if( akindDesc != null && !"".equals(akindDesc) ) {
							if("".equals(accidentReasonDescSB.toString())) {
								accidentReasonDescSB.append(akindDesc);
							}else {
								accidentReasonDescSB.append("/").append(akindDesc);
							}
						}
						
						if( diagnosisVoList != null && diagnosisVoList.size() > 0) {
							StringBuffer diagnosisDest = new StringBuffer();
							
							for( DiagnosisVO dvo : diagnosisVoList) {
								diagnosisDest.append(dvo.getDiagnosisName()).append(",");
							}
							
							if(diagnosisDest.length() > 0) {
								diagnosisDest.deleteCharAt(diagnosisDest.length() - 1);
							}
							
							if( "".equals(accidentReasonDescSB.toString())) {
								accidentReasonDescSB.append(diagnosisDest.toString());
							}else {
								accidentReasonDescSB.append("/").append(diagnosisDest.toString());
							}
							
						}

						//理賠舊資料會在4/7會條件導入, 且已提供下列接口, 只是下列接口會含在eBao理賠的data, 建議舊資料使用下列接口, 
						//然後過篩在eBao理賠的data.(Policy_source<>’E’)
						//PolicySource è E:ebao , T:capsil, K:oracle
						String source = vo.getPolicySource();
						if ("E".equals(source) == false) {
							
							if (vo.getPolicyDataList() != null && vo.getPolicyDataList().size() > 0) {
								
								for (PolicyDataVO pvo : vo.getPolicyDataList()) {
									if ( !StringUtils.isNullOrEmpty(pvo.getPolicyCode())) {
										ClaimInfoVO claimInfoVo = new ClaimInfoVO();
										claimInfoVo.setCaseNo( caseNo );
										claimInfoVo.setOfficerName( accidentName );
										claimInfoVo.setPolicyNo( pvo.getPolicyCode() );
										claimInfoVo.setAccidentTime(accidentTime);
										claimInfoVo.setCertiCode(certiCode);
										claimInfoVo.setDiagnosisStr(accidentReasonDescSB.toString());
										claimInfo.add(claimInfoVo);
									}
								}
								
							} else {
								
								if (!StringUtils.isNullOrEmpty(vo.getCaseNo())) {
									
									ClaimInfoVO claimInfoVo = new ClaimInfoVO();
									claimInfoVo.setCaseNo(caseNo);
									claimInfoVo.setOfficerName(accidentName);
									claimInfoVo.setAccidentTime(accidentTime);
									claimInfoVo.setDiagnosisStr(accidentReasonDescSB.toString());
									claimInfoVo.setCertiCode(certiCode);
									claimInfo.add(claimInfoVo);
									
								}
							}
						}
					}
				}
				
			}
			
			if (claimInfo != null && claimInfo.size() > 0) {
				for (ClaimInfoVO vo : claimInfo) {
					NbClaimHistory bo = new NbClaimHistory();
					bo.setPolicyId(policyId);
					bo.setCaseNo(vo.getCaseNo());
					bo.setHistoryPolicyCode(vo.getPolicyNo());
					bo.setOfficerName(vo.getOfficerName());
					bo.setAccidentTime(vo.getAccidentTime());
					bo.setDiagnosisId(vo.getDiagnosisId());
					bo.setCaseStatus(vo.getCaseStatus());
					bo.setPolicyAmounts(vo.getPolicyAmounts());
					bo.setCertiCode(vo.getCertiCode());
					bo.setDiagnosisStr(vo.getDiagnosisStr());
					uwWorkSheetDao.insertNbClaimHistoryData(bo);
				}
			}
			retBln = true;
		}
		return retBln;
	}
	/**
	 * <p>Description : 整合寫入理賠受理資訊共用處理</p>
	 * <p>Created By : Kathy Yeh</p>
	 * <p>Create Time : July 6, 2021</p>
	 * @param ClaimQueryInfoCIVO
	 * @param ClaimInfoVO
	 * @return
	 */
	private void insertClaimInfo(ClaimQueryInfoCIVO claimCIVO, ClaimInfoVO vo ) throws Exception {
		vo.setCaseId(claimCIVO.getCaseId());
		vo.setAccidentTime(claimCIVO.getAccidentTime());
		vo.setCaseStatus(claimCIVO.getCaseStatus());
		vo.setCaseNo(claimCIVO.getCaseNo());
		vo.setDiagnosisId(claimCIVO.getDiagnosisId());
		vo.setClaimType(claimCIVO.getClaimType());
		vo.setPolicyNo(claimCIVO.getPolicyNo());
		vo.setPaymentDates(claimCIVO.getPaymentDates());
		vo.setPaymentStatus(claimCIVO.getPaymentStatus());
		vo.setStatusDate(claimCIVO.getStatusDate());
		vo.setNoficicationDate(claimCIVO.getNoficicationDate());
		vo.setProductNames(claimCIVO.getProductNames());
		vo.setPendingDocuments(claimCIVO.getPendingDocuments());
	}
	
	/**
	 * <p>Description : 新增契審表暫存保全變更資訊</p>
	 * <p>Created By : Alex Cheng</p>
	 * <p>Create Time : Sep 4, 2016</p>
	 * @param policyId
	 * @param underwriteId
	 * @return
	 */
	@Override
	@SuppressWarnings("unchecked")
	public boolean insertNbPosHistoryData(Long policyId, Long underwriteId) throws Exception {
		boolean retBln = false;
		if (policyId != null && underwriteId != null) {
			List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
			List<InsuredVO> insuredVOList = qualityRecordActionHelper.findPartyIdOfInsuredByPolicyId(String.valueOf(policyId));
			for (InsuredVO o : insuredVOList) {
				Long[] partyIdArray = new Long[] { o.getPartyId() };
				List<Map<String, Object>> queryList = qualityRecordActionHelper.findInfoVOByPartyId(partyIdArray, AppContext.getCurrentUser().getLangId());
				for (int i = 0; i < queryList.size(); i++) {
					String tmp = queryList.get(i).get("policyCode").toString();
					long policyIdVal = policyCI.findPolicyIdByPolicyCode(tmp);
					queryList.get(i).put("policyId", policyIdVal);

				}

				Map<String, Object> queryMap = new HashMap<String, Object>();
				queryMap.put("name", o.getName());
				queryMap.put("queryList", queryList);

				resultList.add(queryMap);
			}
			if (resultList != null && resultList.size() > 0) {
				for (Map<String, Object> voMap : resultList) {
					if (voMap.get("queryList") != null) {
						for (Map<String, Object> map : (List<Map<String, Object>>) voMap.get("queryList")) {
							NbPosHistory bo = new NbPosHistory();
							bo.setUnderwriteId(underwriteId);
							bo.setInsuredName(MapUtils.getString(voMap, "name", null));
							bo.setHistoryPolicyCode(MapUtils.getString(map, "policyCode", null));
							bo.setChangeType(MapUtils.getString(map, "changeId", null));
							bo.setTransactionName(MapUtils.getString(map, "transactionName", null));
							bo.setApplyDate(map.get("applyTime") == null ? null : (Date) map.get("applyTime"));
							bo.setValidateDate(map.get("validateDate") == null ? null : (Date) map.get("validateDate"));
							bo.setLastHandlerId(MapUtils.getLong(map, "lastHandlerName", null));
							bo.setPolicyChgStatus(MapUtils.getString(map, "policyChgStatus", null));
							uwWorkSheetDao.insertNbPosHistoryData(bo);
						}
					}
				}
			}
			retBln = true;
		}
		return retBln;
	}

	/**
	 * <p>Description : 新增契審表風險累計資料暫存表</p>
	 * <p>Created By : Alex Cheng</p>
	 * <p>Create Time : Sep 4, 2016</p>
	 * @param policyId
	 * @return
	 */
	@Override
	public boolean insertNbRiskHistoryData(Long policyId) throws Exception {
		boolean retBln = false;
		if (policyId != null) {
			List<RAInfoVO> raInfos = new ArrayList<RAInfoVO>();
			List<InsuredVO> insureds = insuredService.findByPolicyId(policyId);
			java.util.Collections.sort(insureds, new InsuredVOComparator());    
			for (InsuredVO insuredVO : insureds) {
				RAInfoVO raInfo = new RAInfoVO();
				raInfo.setPartyId(insuredVO.getPartyId());
				raInfo.setInsureCetiCode(insuredVO.getCertiCode());
				raInfo.setInsureName(insuredVO.getName());
				raInfo.setInsuredCategory(insuredVO.getInsuredCategory());
				raInfo.setNbInsuredCategory(insuredVO.getNbInsuredCategory());
				raInfos.add(raInfo);
			}

			for (RAInfoVO infoVO : raInfos) {
				/* get policy risk info 本次 */
				Map<Integer, BigDecimal> riskInfo = insuredService.getPolicyRiskInfo(policyId, infoVO.getInsureCetiCode());
				Iterator<Map.Entry<Integer, BigDecimal>> infoEntry = riskInfo.entrySet().iterator();
				List<RiskInfoVO> riskVOs = new ArrayList<RiskInfoVO>();

				while (infoEntry.hasNext()) {
					Map.Entry<Integer, BigDecimal> info = infoEntry.next();
					RiskInfoVO vo = new RiskInfoVO();
					vo.setRiskType(info.getKey());/* 風險類型 */
					vo.setRiskAmount0(info.getValue()); /* 風險保額 */

					riskVOs.add(vo);
				}
				if (riskVOs.size() == 0) {
					RiskInfoVO vo = new RiskInfoVO();
					riskVOs.add(vo);
				}
				infoVO.setRiskInfoList(riskVOs);

				//get history policy risk info 各類別
				Map<Integer, Map<Integer, BigDecimal>> historyRiskInfo = insuredService.getHistoryPolicyRiskInfo(policyId, infoVO.getInsureCetiCode());
				Iterator<Map.Entry<Integer, Map<Integer, BigDecimal>>> historyInfoEntry = historyRiskInfo.entrySet().iterator();
				List<RiskInfoVO> historyRiskVOs = new ArrayList<RiskInfoVO>();
				while (historyInfoEntry.hasNext()) {
					Map.Entry<Integer, Map<Integer, BigDecimal>> info = historyInfoEntry.next();
					Map<Integer, BigDecimal> categoryInfo = info.getValue();
					RiskInfoVO vo = new RiskInfoVO();
					vo.setRiskType(info.getKey());
					vo.setRiskAmount0(categoryInfo.get(CodeCst.AGGREGATION_CATEGORY_APPLYING));
					vo.setRiskAmount1(categoryInfo.get(CodeCst.AGGREGATION_CATEGORY_EFFECTIVE));
					vo.setRiskAmount2(categoryInfo.get(CodeCst.AGGREGATION_CATEGORY_LAPSED));
					vo.setRiskAmount3(categoryInfo.get(CodeCst.AGGREGATION_CATEGORY_TOTAL));
					historyRiskVOs.add(vo);
				}
				if (historyRiskVOs.size() == 0) {
					RiskInfoVO vo = new RiskInfoVO();
					historyRiskVOs.add(vo);
				}
				infoVO.setHistoryRiskInfoList(historyRiskVOs);

				//get 334結果T_RMS_AGGR_SA_PREM_RESULT
				List<AggrSaPremResultVO> aggrPremList = aggrSaPremResultService.findByRuleNameAndPolicyIdAndPartyId(AggrSaPremResultService.RULE_NAME_334, policyId, infoVO.getPartyId());
				infoVO.setAggrPremList(aggrPremList);
			}
			if (raInfos != null && raInfos.size() > 0) {
				for (RAInfoVO infoVO : raInfos) {
					/* 寫入本次資訊 */
					if (infoVO.getRiskInfoList() != null && infoVO.getRiskInfoList().size() > 0) {
						String accumulationType = "1";
						for (RiskInfoVO vo : infoVO.getRiskInfoList()) {
							NbRiskHistory bo = new NbRiskHistory();
							bo.setAccumulationType(accumulationType);
							bo.setOrderInsure(this.getOrderInsure(infoVO.getInsuredCategory(), infoVO.getNbInsuredCategory()));
							bo.setPolicyId(policyId);
							bo.setInsureCetiCode(infoVO.getInsureCetiCode());
							bo.setInsureName(infoVO.getInsureName());

							if (vo.getRiskType() != null) {
								bo.setRiskType(vo.getRiskType());
								bo.setRiskDesc(CodeTable.getCodeDesc("T_AGGREGATION_RISK_TYPE", vo.getRiskType(), AppContext.getCurrentUser().getLangId()));
								bo.setRiskAmount(vo.getRiskAmount0());
							}

							uwWorkSheetDao.insertNbRiskHistoryData(bo);
						}
					}
					/* 寫入各類別資訊 */
					if (infoVO.getHistoryRiskInfoList() != null && infoVO.getHistoryRiskInfoList().size() > 0) {
						String accumulationType = "2";
						for (RiskInfoVO vo : infoVO.getHistoryRiskInfoList()) {
							NbRiskHistory bo = new NbRiskHistory();
							bo.setAccumulationType(accumulationType);
							bo.setOrderInsure(this.getOrderInsure(infoVO.getInsuredCategory(), infoVO.getNbInsuredCategory()));
							bo.setPolicyId(policyId);
							bo.setInsureCetiCode(infoVO.getInsureCetiCode());
							bo.setInsureName(infoVO.getInsureName());
							if (vo.getRiskType() != null) {
								bo.setRiskType(vo.getRiskType());
								bo.setRiskDesc(CodeTable.getCodeDesc("T_AGGREGATION_RISK_TYPE",
										vo.getRiskType(), AppContext.getCurrentUser().getLangId()));
								bo.setRiskApplying(vo.getRiskAmount0());
								bo.setRiskEffective(vo.getRiskAmount1());
								//寫入停效資訊
								bo.setRiskSuspend(vo.getRiskAmount2());
								bo.setRiskTotal(vo.getRiskAmount3());
							}

							uwWorkSheetDao.insertNbRiskHistoryData(bo);
						}
					}
					/* 寫入累積年繳化 */
					NbRiskHistory bo = new NbRiskHistory();
					String accumulationType = "3";
					bo.setAccumulationType(accumulationType);
					bo.setOrderInsure(this.getOrderInsure(infoVO.getInsuredCategory(), infoVO.getNbInsuredCategory()));
					bo.setPolicyId(policyId);
					bo.setInsureCetiCode(infoVO.getInsureCetiCode());
					bo.setInsureName(infoVO.getInsureName());
					Long total = liaRocDownloadCI.queryIssueAnnuPrem(infoVO.getInsureCetiCode(), null, policyId)
							+ liaRocDownloadCI.queryReceiveAnnuPrem(infoVO.getInsureCetiCode(), null, policyId);
					//同業(含承保/收件通報)
					BigDecimal riskApplying = new BigDecimal(total);
					//本公司年繳保費(含生效/未生效件)
					Long policyChgId = null;
					BigDecimal riskEffective = coverageInsuredService.getAnnualPremOurCompany(
							infoVO.getInsureCetiCode(),
							policyId, policyChgId);
					if (riskEffective == null) {
						riskEffective = BigDecimal.ZERO;
					}
					//累計
					BigDecimal riskTotal = riskApplying.add(riskEffective);

				 	//RTC-217681-以小數點後2位4捨5入至整數
					riskApplying = NBUtils.specialScale(riskApplying, 2, 0, RoundingMode.HALF_UP);
					riskEffective = NBUtils.specialScale(riskEffective, 2, 0, RoundingMode.HALF_UP);
					riskTotal = NBUtils.specialScale(riskTotal, 2, 0, RoundingMode.HALF_UP);
		    	
					bo.setRiskApplying(riskApplying);
					bo.setRiskEffective(riskEffective);
					bo.setRiskTotal(riskTotal);
					
					uwWorkSheetDao.insertNbRiskHistoryData(bo);

					/* 寫入334結果T_RMS_AGGR_SA_PREM_RESULT */
					infoVO.getAggrPremList().forEach(aggrPrem -> {
						NbRiskHistory aggr = new NbRiskHistory();
						aggr.setPolicyId(policyId);
						aggr.setInsureCetiCode(infoVO.getInsureCetiCode());
						aggr.setInsureName(infoVO.getInsureName());
						aggr.setAccumulationType("4");
						aggr.setRiskDesc(aggrPrem.getSameCateBenefit());
						aggr.setRiskTotal(NBUtils.specialScale(aggrPrem.getSumPrem(), 2, 0, RoundingMode.HALF_UP));
						aggr.setOrderInsure(this.getOrderInsure(infoVO.getInsuredCategory(), infoVO.getNbInsuredCategory()));
						uwWorkSheetDao.insertNbRiskHistoryData(aggr);
					});
				}
				retBln = true;
			}
		}
		return retBln;
	}

	/**
	 * <p>Description : 新增契審表_資恐風險等級_人工調整項目歷史列表暫存檔</p>
	 * <p>Created By : Alex Cheng</p>
	 * <p>Create Time : Sep 4, 2016</p>
	 * @param policyId
	 * @return
	 */
	@Override
	public boolean insertNbRiskCustomerHistoryData(Long policyId) throws Exception {
		boolean retBln = false;
		if (policyId != null && policyId > 0) {
			PolicyHolderVO policyHolderVO = uwInsureHistoryService.findPolicyHolderById(policyId.toString());
			if (policyHolderVO != null &&
					!StringUtils.isNullOrEmpty(policyHolderVO.getCertiCode()) &&
					!StringUtils.isNullOrEmpty(policyHolderVO.getCertiType())) {
				List<RiskCustomerLogQueryVO> riskList = riskCustomerLogCI.queryCustomerLogsList(policyHolderVO.getCertiCode());
				RiskCustomerLogQueryVO newVo = null;
				for (RiskCustomerLogQueryVO vo : riskList) {
					if (!StringUtils.isNullOrEmpty(vo.getRiskRemark())) {
						if (newVo != null) {
							/* 風險數值修改 */
							if ((newVo.getRiskManual() != null && !newVo.getRiskManual().equals(vo.getRiskManual())) ||
									(newVo.getRiskManual() == null && vo.getRiskManual() != null)) {
								NbRiskCustomerHistory bo = new NbRiskCustomerHistory();
								bo.setPolicyId(policyId);
								bo.setHolderCetiCode(policyHolderVO.getCertiCode());
								bo.setArtificialItem(CHANGE_MANUAL);
								bo.setUpdateDate(newVo.getUpdateTime());
								uwWorkSheetDao.insertNbRiskCustomerHistoryData(bo);
							}
							/* 知名人士修改 */
							if ((newVo.getRiskFamous() != null && !newVo.getRiskFamous().equals(vo.getRiskFamous())) ||
									(newVo.getRiskFamous() == null && vo.getRiskFamous() != null)) {
								NbRiskCustomerHistory bo = new NbRiskCustomerHistory();
								bo.setPolicyId(policyId);
								bo.setHolderCetiCode(policyHolderVO.getCertiCode());
								bo.setArtificialItem(CHANGE_FAMOUS);
								bo.setUpdateDate(newVo.getUpdateTime());
								uwWorkSheetDao.insertNbRiskCustomerHistoryData(bo);
							}
						}
					}
					newVo = vo;
				}
			}
			retBln = true;
		}

		return retBln;
	}

	/**
	 * <p>Description : 新增契審表_公會通報暫存資料</p>
	 * <p>Created By : Alex Cheng</p>
	 * <p>Create Time : Sep 4, 2016</p>
	 * @param policyId
	 * @return
	 */
	@SuppressWarnings("deprecation")
	@Override
	public boolean insertNbLiarocData(Long policyId) throws Exception {
		boolean retBln = false;
		if (policyId != null && policyId > 0) {
			PolicyVO policy = policyService.loadPolicyByPolicyId(policyId);
			if (policy != null) {
				List<LiaRocInfoForm> issueForms = new ArrayList<LiaRocInfoForm>();
				List<LiaRocInfoForm> receiveForms = new ArrayList<LiaRocInfoForm>();
				LiaRocInfoForm notIntoForm = new LiaRocInfoForm();

				// 承保資料查詢;
				issueForms = this.queryDetailDetail(policy, "2");
				boolean isEmptyIssue = this.insertLiaDocTempTable(issueForms, policy, "1");

				// 收件資料查詢;
				receiveForms = this.queryDetailDetail(policy, "1");
				boolean isEmptyReceive = this.insertLiaDocTempTable(receiveForms, policy, "2");

				boolean isEmptyNoneReceive = false;
				/* 未進件之收件通報查詢 */
				List<UwNotintoRecvVO> uwNotintoRecvList = new ArrayList<UwNotintoRecvVO>();
				uwNotintoRecvList = uwNotintoRecvService.getNotIntoAFINSList(policyId, policy.getInsureds());
				notIntoForm.setUwNotintoRecvList(uwNotintoRecvList);
				if (notIntoForm.getUwNotintoRecvList() != null && notIntoForm.getUwNotintoRecvList().size() > 0) {
					
					for (UwNotintoRecvVO vo : notIntoForm.getUwNotintoRecvList()) {
						NbLiaRocHistory bo = new NbLiaRocHistory();
						bo.setPolicyId(policy.getPolicyId());
						bo.setLiaRocType("3"); //未進件
						bo.setInsureCetiCode(vo.getCeritCode());
						bo.setInsureName(vo.getName());
						bo.setNotIntoSource(vo.getSource());
						
						bo.setNotIntoPolicyCode(vo.getPolicyCode() + "/"
										+ org.apache.commons.lang3.StringUtils.defaultString(vo.getCompanyNo()));
						bo.setNotIntoApplyDate(vo.getApplyDate());
						bo.setNotIntoPosAdd("POS".equals(vo.getDataType()) ? CodeCst.YES_NO__YES : "");
						bo.setProductType(vo.getInternalId());
						bo.setNotIntoAmount(vo.getAmount());
						bo.setBlockType("3");
						uwWorkSheetDao.insertNbLiarocData(bo);
					}
				} else {
					NbLiaRocHistory bo = new NbLiaRocHistory();
					bo.setPolicyId(policy.getPolicyId());
					bo.setLiaRocType("3"); //未進件
					bo.setBlockType("3");
					uwWorkSheetDao.insertNbLiarocData(bo);

					isEmptyNoneReceive = true;
				}

				retBln = true;

				//均無通報資料刪除公會暫緩檔
				if (isEmptyIssue && isEmptyReceive && isEmptyNoneReceive) {
					HibernateSession3.currentSession().flush();
					uwWorkSheetDao.deleteNbLiarocData(policyId);
				}
			}

		}
		return retBln;
	}

	/**
	 * <p>Description : 將承保/收件資料寫入暫存TABLE</p>
	 * <p>Created By : Alex Cheng</p>
	 * <p>Create Time : Sep 8, 2016</p>
	 * @param issueForms
	 * @param policy
	 * @param liaRocType
	 */
	private boolean insertLiaDocTempTable(List<LiaRocInfoForm> forms, PolicyVO policy, String liaRocType) {

		boolean isEmpty = true;

		if (forms != null && forms.size() > 0) {
			/* 不同被保險人 */
			for (LiaRocInfoForm form : forms) {
				/* 承保/收件資料 */
				if (form.getCompanyDetailList() != null && form.getCompanyDetailList().size() > 0) {

					for (CompanyDetailVO vo : form.getCompanyDetailList()) {
						String companyCode = vo.getCompanyCode();
						String companyName = "";
						if ("N".equals(vo.getLiarocType()) || "r".equals(vo.getLiarocType())) {
							companyName = CodeTable.getCodeDesc("V_LIAROC_GENERAL_CODE", companyCode, "311");
						} else {
							companyName = CodeTable.getCodeDesc("V_LIAROC_COMPANY_CODE", companyCode, "311");
						}
						if (companyName.isEmpty()) {
							companyName = companyCode;
						}

						if (vo.getDetails() != null && vo.getDetails().size() > 0) {
							for (DetailVO detailVo : vo.getDetails()) {
								NbLiaRocHistory bo = new NbLiaRocHistory();
								bo.setPolicyId(policy.getPolicyId());
								bo.setLiaRocType(liaRocType);
								bo.setInsureCetiCode(form.getCertiCode());
								bo.setInsureName(form.getName());
								bo.setAnnPremium(vo.getAnnPremium());
								bo.setCompanyName(companyName);
								bo.setOrderInsure(this.getOrderInsure(form.getInsuredCategory(), form.getNbInsuredCategory()));
								/**change by sunny 險種說明應該是險種的中文**/
								// 險種代號
								bo.setProductType(detailVo.getProductDesc());
								// 險種說明
								String productTypeDesc = "";
								if (detailVo.getProductType() != null) {
									productTypeDesc = CodeTable.getCodeDesc("V_LIAROC_PRODUCT_TYPE", detailVo.getProductType(), "311");
								}
								bo.setProductDesc(productTypeDesc);

								bo.setValidateDate(detailVo.getValidateDate());
								bo.setSendDate(detailVo.getSendDate());
								bo.setLiability01(detailVo.getLiability01());
								bo.setLiability02(detailVo.getLiability02());
								bo.setLiability12(detailVo.getLiability12());
								bo.setLiability08(detailVo.getLiability08());
								bo.setLiability06(detailVo.getLiability06());
								/* PCR 343102 Add Eddy 新增累計給付項目=醫療限額自負額(LIABILITY_07)*/
								bo.setLiability07(detailVo.getLiability07());
								/* PCR 344444 Add Kathy 新增累計給付項目=喪葬費用(LIABILITY_17)*/
								/*公會通報資訊呈現是新公會還是舊公會下載資料判斷 */
								if (liaRocDownload2020CI.hasLiaroc20(policy.getPolicyId())) {
									bo.setLiability17(detailVo.getLiability17().toString());
								}else{
									//舊公會無喪葬費用，寫入NA
									bo.setLiability17("NA");
								}									
								/* PCR 415346 Add Kathy 新增舊式統一證號*/
								bo.setOldForeignerId(form.getOldForeignerId());
								bo.setBlockType("1");
								uwWorkSheetDao.insertNbLiarocData(bo);

								isEmpty = false;
							}
						} else {

							NbLiaRocHistory bo = new NbLiaRocHistory();
							bo.setPolicyId(policy.getPolicyId());
							bo.setLiaRocType(liaRocType);
							bo.setInsureCetiCode(form.getCertiCode());
							bo.setInsureName(form.getName());
							bo.setAnnPremium(vo.getAnnPremium());
							bo.setCompanyName(companyName);
							bo.setOrderInsure(this.getOrderInsure(form.getInsuredCategory(), form.getNbInsuredCategory()));
							/* PCR 415346 Add Kathy 新增舊式統一證號*/
							bo.setOldForeignerId(form.getOldForeignerId());
							bo.setBlockType("1");
							uwWorkSheetDao.insertNbLiarocData(bo);

						}
					}
				}
				/* 承保保額總計 */
				if (form.getTotalDetailVOs() != null && form.getTotalDetailVOs().size() > 0) {
					for (DetailVO detailVo : form.getTotalDetailVOs()) {
						NbLiaRocHistory bo = new NbLiaRocHistory();
						bo.setPolicyId(policy.getPolicyId());
						bo.setLiaRocType(liaRocType);
						bo.setInsureCetiCode(form.getCertiCode());
						bo.setInsureName(form.getName());
						bo.setOrderInsure(this.getOrderInsure(form.getInsuredCategory(), form.getNbInsuredCategory()));
						/**change by sunny 險種說明應該是險種的中文**/
						// 險種代號
						bo.setProductType(detailVo.getProductDesc());
						// 險種說明
						String productTypeDesc = "";
						if (detailVo.getProductType() != null) {
							productTypeDesc = CodeTable.getCodeDesc("V_LIAROC_PRODUCT_TYPE", detailVo.getProductType(), "311");
						}
						bo.setProductDesc(productTypeDesc);
						bo.setLiability01(detailVo.getLiability01());
						bo.setLiability02(detailVo.getLiability02());
						bo.setLiability12(detailVo.getLiability12());
						bo.setLiability08(detailVo.getLiability08());
						bo.setLiability06(detailVo.getLiability06());
						/* PCR 343102 Add Eddy 新增累計給付項目=醫療限額自負額(LIABILITY_07)*/
						bo.setLiability07(detailVo.getLiability07());
						/* PCR 344444 Add Kathy 新增累計給付項目=喪葬費用(LIABILITY_17)*/
						/*公會通報資訊呈現是新公會還是舊公會下載資料判斷 */
						if (liaRocDownload2020CI.hasLiaroc20(policy.getPolicyId())) {
							bo.setLiability17(detailVo.getLiability17().toString());
						}else{
							//舊公會無喪葬費用，寫入NA
							bo.setLiability17("NA");
						}								
						/* PCR 415346 Add Kathy 新增舊式統一證號*/
						bo.setOldForeignerId(form.getOldForeignerId());
						bo.setBlockType("2");
						uwWorkSheetDao.insertNbLiarocData(bo);

						isEmpty = false;
					}
				} else {
					NbLiaRocHistory bo = new NbLiaRocHistory();
					bo.setPolicyId(policy.getPolicyId());
					bo.setLiaRocType(liaRocType);
					bo.setInsureCetiCode(form.getCertiCode());
					bo.setInsureName(form.getName());
					bo.setOrderInsure(this.getOrderInsure(form.getInsuredCategory(), form.getNbInsuredCategory()));
					/* PCR 415346 Add Kathy 新增舊式統一證號*/
					bo.setOldForeignerId(form.getOldForeignerId());
					bo.setBlockType("2");
					uwWorkSheetDao.insertNbLiarocData(bo);
				}

				/* 年繳化保費總計 */
				if (form.getTotalAnnPremium() != null) {
					NbLiaRocHistory bo = new NbLiaRocHistory();
					bo.setPolicyId(policy.getPolicyId());
					bo.setLiaRocType(liaRocType);
					bo.setInsureCetiCode(form.getCertiCode());
					bo.setInsureName(form.getName());
					bo.setOrderInsure(this.getOrderInsure(form.getInsuredCategory(), form.getNbInsuredCategory()));
					bo.setLiability01(form.getTotalAnnPremium());
					/* PCR 415346 Add Kathy 新增舊式統一證號*/
					bo.setOldForeignerId(form.getOldForeignerId());
					bo.setBlockType("3");
					uwWorkSheetDao.insertNbLiarocData(bo);
				}
			}
		}

		return isEmpty;
	}

	/**
	 * <p>Description : 更新最後一版註記為N </p>
	 * <p>Created By : Alex Cheng</p>
	 * <p>Create Time : Aug 3, 2016</p>
	 * @param listId
	 * @param policyId
	 * @throws GenericException
	 */
	@Override
	public void updateLastFlagByListIdPolicyId(Long listId, Long policyId) throws GenericException {
		dao.updateLastFlagByListIdPolicyId(listId, policyId);
	}

	/**
	 * <p>Description : 設定刪除註記為Y listId與policyId不可為NULL </p>
	 * <p>Created By : Alex Cheng</p>
	 * <p>Create Time : Aug 3, 2016</p>
	 * @param listId
	 * @param policyId
	 * @throws GenericException
	 */
	@Override
	public void deleteIndiByListIdPolicyId(Long listId, Long policyId) throws GenericException {
		dao.deleteIndiByListIdPolicyId(listId, policyId);
	}

	/**
	 * <p>Description : 查詢未刪除的契審表 DESC </p>
	 * <p>Created By : Alex Cheng</p>
	 * <p>Create Time : Aug 3, 2016</p>
	 * @param policyId
	 * @param lastFlag
	 * @return
	 * @throws GenericException
	 */
	@Override
	public List<UwWorkSheet> queryLastByPolicyId(Long policyId, String lastFlag) throws GenericException {
		return dao.queryLastByPolicyId(policyId, lastFlag);
	}

	/**
	 * <p>Description : ONLINE_REPORT</p>
	 * <p>Created By : Alex Cheng</p>
	 * <p>Create Time : Sep 9, 2016</p>
	 * @param policyId
	 * @param underwriteId
	 * @param titleStr
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	@Override
	public DataHandler genUnb0631Report(String policyId, String underwriteId, String titleStr, String userId) throws Exception {
		NBUtils.logger(this.getClass(), "3.1 genUnb0631Report in...");
		List<KernelParameter> parameters = new ArrayList<KernelParameter>();
		KernelParameter par = new KernelParameter();
		par.setKey("titleName");//String
		par.setIsArray(CrystalReportWSCI.IS_NOT_ARRAY);// 0=N,1=Y
		par.setValue(titleStr);//全球人壽保險股份有限公司
		par.setType(CrystalReportWSCI.STRING);
		parameters.add(par);
		par = new KernelParameter();
		par.setKey("i_underwriteId");//integer(long)
		par.setIsArray(CrystalReportWSCI.IS_NOT_ARRAY);// 0=N,1=Y
		par.setValue(underwriteId);//1481
		par.setType(CrystalReportWSCI.INTEGER);
		parameters.add(par);
		par = new KernelParameter();
		par.setKey("i_policyId");//integer(long)
		par.setIsArray(CrystalReportWSCI.IS_NOT_ARRAY);// 0=N,1=Y
		par.setValue(policyId);//1871
		par.setType(CrystalReportWSCI.INTEGER);
		parameters.add(par);
		par = new KernelParameter();
		par.setKey("p_userid");//string(long)
		par.setIsArray(CrystalReportWSCI.IS_NOT_ARRAY);// 0=N,1=Y
		par.setValue(userId);//1020160
		par.setType(CrystalReportWSCI.STRING);
		parameters.add(par);
		NBUtils.logger(this.getClass(), "3.2 genUnb0631Report CALL crystalReportWSCI.downloadFile Start ...");
		NBUtils.logger(this.getClass(), "3.3 genUnb0631Report downloadFile parameter: titleName:" + titleStr + ",i_underwriteId:" + underwriteId + ",i_policyId:" + policyId + ",p_userid:" + userId);
		DataHandler dataHandler = crReportWSCI.downloadFile("LS_UNB_0631", "pdf", parameters);
		NBUtils.logger(this.getClass(), "3.4 genUnb0631Report CALL crystalReportWSCI.downloadFile End ...");
		return dataHandler;
	}

	/**
	 * <p>Description : 寫入契審表列印資訊</p>
	 * <p>Created By : Alex Cheng</p>
	 * <p>Create Time : Sep 9, 2016</p>
	 * @param policyId
	 * @param underwriteId
	 * @param dataHandler
	 * @throws Exception
	 */
	@Override
	public void updateToNbWorkSheet(Long policyId, Long underwriteId, DataHandler dataHandler) throws Exception {
		NBUtils.logger(this.getClass(), "4.1 updateToNbWorkSheet Start ");
		String lastFlag = CodeCst.YES_NO__YES;
		if (dataHandler.getInputStream() != null) {
			String policyCode = policyCI.getPolicyCodeByPolicyId(policyId);

			NBUtils.logger(this.getClass(), "genPDFFile...Start Path:" + EnvUtils.getSendToInternalSharePath());
			IcePdfUtil.genPdfFile(policyCode, dataHandler.getInputStream(), EnvUtils.getSendToInternalSharePath());
			NBUtils.logger(this.getClass(), "genPDFFile...End");
			/* PDF轉換TIF檔 */
			List<ImageOutputStream> tifArrays = IcePdfUtil.pdfToTif(policyCode, dataHandler.getInputStream());
			//			List<byte[]> tifArrays = this.pdfToTif2(dataHandler.getInputStream());
			NBUtils.logger(this.getClass(), "4.2 updateToNbWorkSheet : tifArrays size=" + (tifArrays == null ? "is null!" : tifArrays.size()));

			List<UwWorkSheet> sheets = dao.queryLastByPolicyId(policyId, "N");

			String capsilStatus = policyService.getCapsilStatus(policyId);
			/* 找有沒有第一版資料(且不為在途件，則產生第一版契審表) */
			if (StringUtils.isNullOrEmpty(capsilStatus) &&
					(sheets == null || sheets.size() == 0)) {
				NBUtils.logger(this.getClass(), "4.3 updateToNbWorkSheet UwWorkSheet first is null ");
				/* 沒有第一版時，設定lastFlag=N */
				lastFlag = CodeCst.YES_NO__NO;
			} else {
				NBUtils.logger(this.getClass(), "4.3 updateToNbWorkSheet UwWorkSheet first is not null ");
				/* 已經有第一版資料的話，再找有沒有第二版資料 */
				sheets = dao.queryLastByPolicyId(policyId, "Y");
				/* 有查到第二版資料時，要先刪除原本第二版資料 */
				NBUtils.logger(this.getClass(), "4.4 updateToNbWorkSheet UwWorkSheet delete second data start!");
				if (sheets != null && sheets.size() > 0) {
					for (UwWorkSheet sheet : sheets) {
						List<UwWorkSheetImage> imageList = uwWorkSheetImageDao.findByWorkSheetId(sheet.getListId());
						if (imageList != null) {
							List<Long> imageIds = new ArrayList<Long>();
							for (UwWorkSheetImage image : imageList) {
								imageIds.add(image.getImageId());
								uwWorkSheetImageDao.remove(image);
							}
							if (imageIds != null && imageIds.size() > 0) {
								/* 刪除T_IMAGE */
								try {
									imageCI.imageRemove(imageIds);
								} catch (Exception e) {
									NBUtils.logger(this.getClass(), "imageIds does not exits" + imageIds.get(0));
								}
							}
						}
						dao.deleteIndiByListIdPolicyId(sheet.getListId(), policyId);
					}
				}
				NBUtils.logger(this.getClass(), "4.4 updateToNbWorkSheet UwWorkSheet delete second data end!");
			}
			NBUtils.logger(this.getClass(), "4.5 updateToNbWorkSheet UwWorkSheet insert new second data start!");
			/* 新增T_IMAGE */
			List<Long> imageIds = new ArrayList<Long>();
			imageIds = this.createImage(policyId, tifArrays, lastFlag);
			//			imageIds = this.createImage2(policyId, tifArrays);

			/* 寫入契審表 列印紀錄 */
			UwWorkSheet newSheet = new UwWorkSheet();
			newSheet.setPolicyId(policyId);
			newSheet.setUwderwriteId(underwriteId);
			newSheet.setLastFlag(lastFlag);
			dao.save(newSheet);
			NBUtils.logger(this.getClass(), "4.7 updateToNbWorkSheet UwWorkSheet insert new data =" + newSheet.getListId());
			/* 寫入契審表 列印紀錄影像關聯 */
			for (Long imageId : imageIds) {
				UwWorkSheetImage imageObj = new UwWorkSheetImage();
				imageObj.setWorkSheetId(newSheet.getListId());
				imageObj.setImageId(imageId);
				uwWorkSheetImageDao.save(imageObj);
				NBUtils.logger(this.getClass(), "4.8 updateToNbWorkSheet UwWorkSheetImage insert new data WorkSheetId=" + imageObj.getWorkSheetId() + ",ImageId=" + imageObj.getImageId());
			}
			NBUtils.logger(this.getClass(), "4.9 updateToNbWorkSheet UwWorkSheet insert new second data end!");
		} else {
			NBUtils.logger(this.getClass(), "4.2 updateToNbWorkSheet dataHandler.getInputStream() is null ");
		}
	}

	@Override
	protected UwWorkSheetVO newEntityVO() {
		return new UwWorkSheetVO();
	}

	/**
	 * <p>Description : 取得被保險人排序</p>
	 * <p>Created By : Alex Cheng</p>
	 * <p>Create Time : Sep 6, 2016</p>
	 * @param insuredCategory
	 * @param nbInsuredCategory
	 * @return
	 */
	private Integer getOrderInsure(String insuredCategory, String nbInsuredCategory) {
		if (StringUtils.isNullOrEmpty(insuredCategory) || CodeCst.INSURED_CATGORY_NA.equals(insuredCategory)) {
			insuredCategory = "9";
		}
		if (StringUtils.isNullOrEmpty(nbInsuredCategory)) {
			nbInsuredCategory = "9";
		}
		int orderInsure = (Integer.parseInt(insuredCategory) * 1000) + Integer.parseInt(nbInsuredCategory);
		return orderInsure;
	}

	/**
	 * <p>Description : 承保通報資料查詢(將Amy原先處理方法移至契審表service 共用)</p>
	 * <p>Created By : Amy Hung</p>
	 * <p>Create Time : Apr 12, 2016</p>
	 * @param request
	 */
	public List<LiaRocInfoForm> queryDetailDetail(PolicyVO policy, String downloadType) {
		List<LiaRocInfoForm> infoForms = new ArrayList<LiaRocInfoForm>();
		Set<String> certiCodeSet = new HashSet<String>();
		Long policyId = policy.getPolicyId();

		// 逐一查詢被保人的公會通報資料;
		for (CoverageVO coverage : policy.gitSortCoverageList()) {

			String certiCode = coverage.getLifeInsured1().getInsured().getCertiCode();
			String oldForeignerId = coverage.getLifeInsured1().getInsured().getOldForeignerId();

			String insuredCategory = coverage.getLifeInsured1().getInsured().getInsuredCategory();
			String nbInsuredCategory = coverage.getLifeInsured1().getInsured().getNbInsuredCategory();

			if (!certiCodeSet.contains(certiCode)) {

				certiCodeSet.add(certiCode);
				LiaRocInfoForm infoForm = new LiaRocInfoForm();
				infoForm.setDownloadType(downloadType);
				infoForm.setTotalAnnPremium(0L);
				// 客戶險種資料區塊 VO;
				infoForm.setCompanyDetailList(new ArrayList<LiaRocInfoForm.CompanyDetailVO>());
				// [收件/承保]保額總計區塊 VO;
				infoForm.setTotalDetailVOs(new ArrayList<LiaRocInfoForm.DetailVO>());

				infoForm.setName(coverage.getLifeInsured1().getInsured().getName());
				infoForm.setCertiCode(certiCode);
				infoForm.setOldForeignerId(oldForeignerId);
				infoForm.setInsuredCategory(insuredCategory);
				infoForm.setNbInsuredCategory(nbInsuredCategory);
				// 公會通報承保資料查詢;
				List<Map<String, Object>> detail = null;

				// 1:收件,2:承保;
				if (LiaRocCst.LIAROC_UL_TYPE_RECEIVE.equals(downloadType)) {
					detail = liaRocDownloadCI.findGroupingReceiveDetailData(certiCode, policyId);
				} else {
					detail = liaRocDownloadCI.findGroupingIssueDetailData(certiCode, policyId);
				}

				if (detail.size() > 0) {

					for (Map<String, Object> map : detail) {

						LiaRocInfoForm.CompanyDetailVO companyDetail = null;
						
						//02-台灣人壽,02-兆豐產物 (公司代碼重覆需再區分壽險及產險)
						String mapLiarocType = map.get("LIAROC_TYPE").toString();
						if (LiaRocCst.INFORCE_LIAROC_TYPE_LIFE.equals(mapLiarocType) || LiaRocCst.RECEIVE_LIAROC_TYPE_LIFE.equals(mapLiarocType)) {
							mapLiarocType = "L";
						} else{
							mapLiarocType = "N";
						}
						
						// 判斷是否同個被保人+公司的資料已存在，若已存在則取出;
						for (CompanyDetailVO vo : infoForm.getCompanyDetailList()) {
							
							//02-台灣人壽,02-兆豐產物 (公司代碼重覆需再區分壽險及產險)
							String companyLiarocType = vo.getLiarocType();
							if (LiaRocCst.INFORCE_LIAROC_TYPE_LIFE.equals(companyLiarocType) || LiaRocCst.RECEIVE_LIAROC_TYPE_LIFE.equals(companyLiarocType)) {
								companyLiarocType = "L";
							} else{
								companyLiarocType = "N";
							}
							
							//02-台灣人壽,02-兆豐產物 (公司代碼重覆需在區分壽險及產險)
							if (vo.getCertiCode().equals(map.get("CERTI_CODE"))
									&& vo.getCompanyCode().equals(map.get("LIAROC_COMPANY_CODE"))
									&& companyLiarocType.equals(mapLiarocType)) {
								companyDetail = vo;
							}
						}

						if (companyDetail == null) {
							companyDetail = infoForm.new CompanyDetailVO();
							companyDetail.setDetails(new ArrayList<LiaRocInfoForm.DetailVO>());
							companyDetail.setName(coverage.getLifeInsured1().getInsured().getName());
							companyDetail.setCertiCode(String.valueOf(map.get("CERTI_CODE")));
							companyDetail.setCompanyCode(String.valueOf(map.get("LIAROC_COMPANY_CODE")));
							String liarocType = String.valueOf(map.get("LIAROC_TYPE"));
							companyDetail.setLiarocType(liarocType);

							if (LiaRocCst.INFORCE_LIAROC_TYPE_LIFE.equals(mapLiarocType) || LiaRocCst.RECEIVE_LIAROC_TYPE_LIFE.equals(mapLiarocType)) {
								// 年繳化保費小計;
								if (infoForm.getDownloadType().equals("1")) {   //收件
									companyDetail.setAnnPremium(liaRocDownloadCI.queryReceiveAnnuPrem(
											String.valueOf(certiCode),
											String.valueOf(map.get("LIAROC_COMPANY_CODE"))
											, policyId));
								} else {
									companyDetail.setAnnPremium(liaRocDownloadCI.queryIssueAnnuPrem(
											String.valueOf(certiCode),
											String.valueOf(map.get("LIAROC_COMPANY_CODE"))
											, policyId));
								}
							} else {
								companyDetail.setAnnPremium(0L);
							}

							infoForm.getCompanyDetailList().add(companyDetail);
						}

						// 險種明細資料;
						LiaRocInfoForm.DetailVO detailVO = infoForm.new DetailVO();
						detailVO.setLiability01(Long.valueOf(String.valueOf(map.get("LIABILITY_01"))));
						detailVO.setLiability02(Long.valueOf(String.valueOf(map.get("LIABILITY_02"))));
						detailVO.setLiability06(Long.valueOf(String.valueOf(map.get("LIABILITY_06"))));
						detailVO.setLiability07(Long.valueOf(String.valueOf(map.get("LIABILITY_07"))));
						detailVO.setLiability08(Long.valueOf(String.valueOf(map.get("LIABILITY_08"))));
						detailVO.setLiability12(Long.valueOf(String.valueOf(map.get("LIABILITY_12"))));
						detailVO.setLiability17(Long.valueOf(String.valueOf(map.get("LIABILITY_17")))); //新式公會才有喪葬費用
						detailVO.setProductDesc(String.valueOf(map.get("PRODUCT_DESC")));
						detailVO.setProductType(String.valueOf(map.get("PRODUCT_TYPE")));
						if (map.get("SEND_DATE") != null) {
							detailVO.setSendDate(DateUtils.toDate(map.get("SEND_DATE").toString(), "yyyy/MM/dd"));
						}
						if (map.get("VALIDATE_DATE") != null) {
							detailVO.setValidateDate(DateUtils.toDate(map.get("VALIDATE_DATE").toString(), "yyyy/MM/dd"));
						}
						companyDetail.getDetails().add(detailVO);

						// 承保保額總計累加;
						DetailVO totalDetailVO = null;
						for (DetailVO vo : infoForm.getTotalDetailVOs()) {
							if (vo.getProductType().equals(String.valueOf(map.get("PRODUCT_TYPE")))
									&& vo.getProductDesc().equals(String.valueOf(map.get("PRODUCT_DESC")))) {
								totalDetailVO = vo;
							}
						}

						if (totalDetailVO == null) {
							totalDetailVO = infoForm.new DetailVO();
							totalDetailVO.setProductType(String.valueOf(map.get("PRODUCT_TYPE")));
							totalDetailVO.setProductDesc(String.valueOf(map.get("PRODUCT_DESC")));
							infoForm.getTotalDetailVOs().add(totalDetailVO);
						}

						totalDetailVO.setLiability01(totalDetailVO.getLiability01()
								+ Long.valueOf(String.valueOf(map.get("LIABILITY_01"))));

						totalDetailVO.setLiability02(totalDetailVO.getLiability02()
								+ Long.valueOf(String.valueOf(map.get("LIABILITY_02"))));

						totalDetailVO.setLiability06(totalDetailVO.getLiability06()
								+ Long.valueOf(String.valueOf(map.get("LIABILITY_06"))));
						
						totalDetailVO.setLiability07(totalDetailVO.getLiability07()
								+ Long.valueOf(String.valueOf(map.get("LIABILITY_07"))));

						totalDetailVO.setLiability08(totalDetailVO.getLiability08()
								+ Long.valueOf(String.valueOf(map.get("LIABILITY_08"))));

						totalDetailVO.setLiability12(totalDetailVO.getLiability12()
								+ Long.valueOf(String.valueOf(map.get("LIABILITY_12"))));

						totalDetailVO.setLiability17(totalDetailVO.getLiability17()
								+ Long.valueOf(String.valueOf(map.get("LIABILITY_17"))));						
						// end.

					}

				} else {

					// 這個被保人查無資料，判斷是根本沒有去下載過，還是有下載但是沒資料;
					LiaRocInfoForm.CompanyDetailVO companyDetail = infoForm.new CompanyDetailVO();
					companyDetail.setName(coverage.getLifeInsured1().getInsured().getName());
					companyDetail.setCertiCode(certiCode);
					companyDetail.setAnnPremium(0L);

					//查詢下載次數
					Integer count = liaRocDownloadCI.queryDownloadRecordCount(certiCode, policyId);
					if (count == 0) {
						//此被保人尚未下載過公會通報資料!
						companyDetail.setEmptyMsg(StringResource.getStringData("MSG_1258191", AppContext.getCurrentUser().getLangId()));
					} else {
						//查無公會通報資料!
						companyDetail.setEmptyMsg(StringResource.getStringData("MSG_1258192", AppContext.getCurrentUser().getLangId()));
					}

					infoForm.getCompanyDetailList().add(companyDetail);

				}
				// 年繳化保費總計累加 ;
				for (CompanyDetailVO vo : infoForm.getCompanyDetailList()) {
					infoForm.setTotalAnnPremium(infoForm.getTotalAnnPremium() + vo.getAnnPremium());
				}
				infoForms.add(infoForm);
				liaRocDownloadCI.saveLiaRocDoanloadLog(certiCode,
						coverage.getLifeInsured1().getInsured().getName(),
						policy.getPolicyNumber(),
						LiaRocCst.DL_LOG_MSG_UW_INFO);
			}
		}
		return infoForms;
	}
	
	/**
	 * <p>Description : 終止保單通報資訊</p>
	 * <p>Created By : Jeremy Zhu</p>
	 * <p>Create Time : Jan 15, 2020</p>
	 * @param request
	 */
	public LiaRocInfoForm queryTerminateDetail(PolicyVO policy) {
		
		Set<String> certiCodeSet = new HashSet<String>();
		Long policyId = policy.getPolicyId();
		LiaRocInfoForm infoForm = new LiaRocInfoForm();
		// 客戶險種資料區塊 VO;
		infoForm.setCompanyDetailList(new ArrayList<LiaRocInfoForm.CompanyDetailVO>());

		// 公會終止保單通報資訊;
		List<Map<String, Object>> detail = liaRocDownloadCI.findTerminateDetailData(policyId);

		if (detail != null && detail.size() > 0) {

			LiaRocInfoForm.CompanyDetailVO companyDetail = null;
			for (Map<String, Object> map : detail) {
				
				//顯示發查公會的CERTI_CODE
				String certiCode = String.valueOf(map.get("ROLE_CERTI_CODE"));
				String  oldForeignerId= String.valueOf(map.get("OLD_FOREIGNER_ID"));
				if (oldForeignerId == "null"){
					oldForeignerId = "";
				}
				
				if (!certiCodeSet.contains(certiCode)) {
					companyDetail = infoForm.new CompanyDetailVO();
					companyDetail.setDetails(new ArrayList<LiaRocInfoForm.DetailVO>());
					companyDetail.setName(String.valueOf(map.get("NAME")));
					companyDetail.setCertiCode(certiCode);
					companyDetail.setOldForeignerId(oldForeignerId);
					companyDetail.setRoleDesc(String.valueOf(map.get("ROLE_DESC")));
					
					certiCodeSet.add(companyDetail.getCertiCode());
					infoForm.getCompanyDetailList().add(companyDetail);
				}

				// 險種明細資料;
				LiaRocInfoForm.DetailVO detailVO = infoForm.new DetailVO();
				detailVO.setCompanyName(String.valueOf(map.get("COMPANY_NAME")));
				detailVO.setRoleType(String.valueOf(map.get("ROLE_TYPE")));
				detailVO.setPolicyCode(String.valueOf(map.get("POLICY_CODE")));
				detailVO.setProductDesc(String.valueOf(map.get("PRODUCT_DESC")));
				detailVO.setProductType(String.valueOf(map.get("PRODUCT_TYPE")));
				detailVO.setValidateDate(DateUtils.toDate(map.get("VALIDATE_DATE").toString(), "yyyy/MM/dd"));
				detailVO.setLiarocPolicyStatus(String.valueOf(map.get("LIAROC_POLICY_STATUS")));
				companyDetail.getDetails().add(detailVO);

				// end.
			}
		}

		return infoForm;
	}

	/**
	 * <p>Description : 將TIF檔寫入T_IMAGE</p>
	 * <p>Created By : Alex Cheng</p>
	 * <p>Create Time : Sep 10, 2016</p>
	 * @param policyId
	 * @param tifArrays
	 * @param lastFlag
	 * @return
	 */
	@SuppressWarnings("deprecation")
	private List<Long> createImage(Long policyId, List<ImageOutputStream> tifArrays, String lastFlag) {
		NBUtils.logger(this.getClass(), "4.6.1 createImage  start!");
		List<Long> imageIds = new ArrayList<Long>();
		PolicyVO policy = policyService.loadPolicyByPolicyId(policyId);
		int pageNo = 1;
		Date nowDate = AppContext.getCurrentUserLocalTime();
		for (ImageOutputStream tif : tifArrays) {
			ImageScanVO vo = new ImageScanVO();

			byte[] buff = IcePdfUtil.readImageToByte(tif);
			vo.setImageFormat("image/tif");
			vo.setImageData(buff);
			//            if(CodeCst.YES_NO__YES.equals(lastFlag)){
			//            	vo.setCardCode("UNBN041"); 
			//            }else{
			//            	vo.setCardCode("UNBN040"); 
			//            }
			vo.setCardCode("UNBN040");
			vo.setFileCode(policy.getPolicyNumber()); //policyCode
			vo.setSignature("N");
			vo.setSeqNumber(Integer.valueOf(pageNo++)); // PageNo
			vo.setHeadId(AppContext.getCurrentUser().getOrganId()); //
			vo.setOrganId(AppContext.getCurrentUser().getOrganId()); //
			vo.setEmpId(AppContext.getCurrentUser().getUserId());
			vo.setProcessStatus("0"); //
			vo.setImageTypeId(Cst.UNB_WORK_SHEET_CARD_CODE_ID__FIRST);
			vo.setMstrMainFileType("1"); //
			vo.setScanTime(nowDate);
			vo.setOrgCode(String.valueOf(AppContext.getCurrentUser().getOrganId()));
			vo.setPriority("N");//
			vo.setPolicyId(policyId);
			/**因無實體文件, 寫入影像系統時不適用批次號碼及箱號帶NA**/
			vo.setBatchArea("NA");
			vo.setBatchDate("NA");
			vo.setBatchDocType("NA");
			vo.setBatchDepType("NA");
			vo.setBoxNo("NA");
			// add by chris for TGL

			vo.setDeptId(String.valueOf(AppContext.getCurrentUser().getDeptId()));
			Long imageId = imageCI.imageUpload(vo);
			NBUtils.logger(this.getClass(), "4.6.2 createImage imageId=" + imageId);
			imageIds.add(imageId);
		}

		NBUtils.logger(this.getClass(), "4.6.3 createImage end! imageIds.size=" + imageIds.size());
		return imageIds;
	}

	/**
	 * <p>Description : 將TIF檔寫入T_IMAGE</p>
	 * <p>Created By : Alex Cheng</p>
	 * <p>Create Time : Sep 10, 2016</p>
	 * @param policyId
	 * @param tifArrays
	 * @return
	 */
	@SuppressWarnings({ "deprecation", "unused" })
	private List<Long> createImage2(Long policyId, List<byte[]> tifArrays) {
		NBUtils.logger(this.getClass(), "4.6.1 createImage2  start!");
		List<Long> imageIds = new ArrayList<Long>();
		PolicyVO policy = policyService.loadPolicyByPolicyId(policyId);
		int pageNo = 1;
		Date nowDate = AppContext.getCurrentUserLocalTime();
		for (byte[] buff : tifArrays) {
			ImageScanVO vo = new ImageScanVO();
			NBUtils.logger(this.getClass(), "4.6.1.1 createImage2  buff.size=" + buff.length);
			vo.setImageFormat("image/tif");
			vo.setImageData(buff);
			vo.setCardCode("UNBN040");
			vo.setFileCode(policy.getPolicyNumber()); //policyCode
			vo.setSignature("N");
			vo.setSeqNumber(Integer.valueOf(pageNo++)); // PageNo
			vo.setHeadId(AppContext.getCurrentUser().getOrganId()); //
			vo.setOrganId(AppContext.getCurrentUser().getOrganId()); //
			vo.setEmpId(AppContext.getCurrentUser().getUserId());
			vo.setProcessStatus("0"); //
			vo.setImageTypeId(Cst.UNB_WORK_SHEET_CARD_CODE_ID__FIRST);
			vo.setMstrMainFileType("1"); //
			vo.setScanTime(nowDate);
			vo.setOrgCode(String.valueOf(AppContext.getCurrentUser().getOrganId()));
			vo.setPriority("N");//
			vo.setPolicyId(policyId);
			/**因無實體文件, 寫入影像系統時不適用批次號碼及箱號帶NA**/
			vo.setBatchArea("NA");
			vo.setBatchDate("NA");
			vo.setBatchDocType("NA");
			vo.setBatchDepType("NA");
			vo.setBoxNo("NA");
			// add by chris for TGL

			vo.setDeptId(String.valueOf(AppContext.getCurrentUser().getDeptId()));
			Long imageId = imageCI.imageUpload(vo);
			NBUtils.logger(this.getClass(), "4.6.2 createImage2 imageId=" + imageId);
			imageIds.add(imageId);
		}

		NBUtils.logger(this.getClass(), "4.6.3 createImage2 end! imageIds.size=" + imageIds.size());
		return imageIds;
	}

	/**
	 * <p>Description : 取得契審表對應的IMAGEID </p>
	 * <p>Created By : Alex Cheng</p>
	 * <p>Create Time : Oct 25, 2016</p>
	 * @param policyId
	 * @return
	 * @throws Exception
	 */
	public String getUnb0631ImageIds(Long policyId) throws Exception {
		StringBuffer imageIds = new StringBuffer("");
		List<Long> policyList = new ArrayList<Long>();
		policyList.add(policyId);
		List<String> cardCodeList = new ArrayList<String>();
		cardCodeList.add("UNBN040");
		//		cardCodeList.add("UNBN041");
		//		cardCodeList.add("UNBN042");
		//		cardCodeList.add("UNBN043");
		List<ImageVO> imageVos = imageCI.queryImageByPolicy(policyList, cardCodeList, false);
		if (imageVos != null && imageVos.size() > 0) {
			Collections.sort(imageVos, new Comparator<ImageVO>() {
				public int compare(ImageVO o1, ImageVO o2) {
					return o1.getImageId().compareTo(o2.getImageId());
				}
			});
			for (ImageVO vo : imageVos) {
				if (imageIds.length() > 0) {
					imageIds.append(",");
				}
				imageIds.append(vo.getImageId().toString());
			}
		}
		return imageIds.toString();
	}

	@SuppressWarnings("unused")
	public ResultType insertUwWorkSheetHistoryData(Long policyId, Long underwriteId) {

		UserTransaction ut = null;
		try {
			ut = Trans.getUserTransaction();
			ut.begin();
			NBUtils.logger(this.getClass(), "1 this.genUnb0631Report...insertUwWorkSheetHistoryData start");
			
			/* 1 清除契審表暫存TABLE資料 */
			boolean delTempData = this.deleteUwWorkSheetTempTable(policyId, underwriteId);
			/* 2 寫入契審表暫存TABLE資料 */
			/* 新增契審表 投保歷史資訊暫存資料 */
			boolean isGroupListDownlaod = this.insertNbInsureHistoryData(policyId);
			NBUtils.logger(this.getClass(), "insertNbInsureHistoryData() done");
			/* 新增契審表 理賠歷史資訊暫存資料表 */
			boolean nbClaimHistory = this.insertNbClaimHistoryData(policyId);
			NBUtils.logger(this.getClass(), "insertNbClaimHistoryData() done");
			/* 新增契審表暫存保全變更資訊 */
			boolean nbPosHistory = this.insertNbPosHistoryData(policyId, underwriteId);
			NBUtils.logger(this.getClass(), "insertNbPosHistoryData() done");
			/* 新增契審表風險累計資料暫存表 */
			boolean nbRiskHistory = this.insertNbRiskHistoryData(policyId);
			NBUtils.logger(this.getClass(), "insertNbRiskHistoryData() done");
			/* 新增契審表_公會通報暫存資料 */
			boolean nbLiarocHistory = this.insertNbLiarocData(policyId);
			NBUtils.logger(this.getClass(), "insertNbLiarocData() done");
			/* 新增契審表_資恐風險等級_人工調整項目歷史列表暫存檔 */
			boolean nbRiskCustomerHistory = this.insertNbRiskCustomerHistoryData(policyId);
			NBUtils.logger(this.getClass(), "insertNbRiskCustomerHistoryData() done");
			ut.commit();

			NBUtils.logger(this.getClass(), "2 this.genUnb0631Report...insertUwWorkSheetHistoryData end");

			if (isGroupListDownlaod == true) {
				return ResultType.SUCCESS;
			} else {
				return ResultType.GROUP_LIST_DOWNLOAD_FAIL;
			}
		} catch (Exception ex) {
			/* 契審表生成暫存錯誤 */
			TransUtils.rollback(ut);

			log.error(NBUtils.getTWMsg(ResultType.TEMPLATE_EXCEPTION.getMsgId()) + ":", ex);

			return ResultType.TEMPLATE_EXCEPTION;
		}
	}

	@Override
	public ResultType generateWorkSheet(Long policyId, Long underwriteId) {

		ResultType result = ResultType.FAILURE;

		/* 契審表報表須傳入的參數 */
		/* MSG_1250494-全球人壽保險股份有限公司 */
		String titleStr = NBUtils.getTWMsg("MSG_1250494");
		String userId = Long.toString(AppContext.getCurrentUser().getUserId());

		if (policyId != null && underwriteId != null) {
		    
            try {
                // 更新覆核人員 Update SubmitTime For Crystal Report
                this.updateLatestUwTransferSubmitTime(underwriteId, AppContext.getCurrentUserLocalTime());
            } catch (Exception e) {
                /* 契審表報表產出錯誤*/
                result = ResultType.EXCEPTION;
                log.error(NBUtils.getTWMsg(result.getMsgId()) + ":", e);
                return result;
            }
		    
			DataHandler dataHandler = null;

			result = insertUwWorkSheetHistoryData(policyId, underwriteId);

			if (ResultType.GROUP_LIST_DOWNLOAD_FAIL.equals(result) == true) {
				//團險下載失敗
				List<UwWorkSheet> sheets = this.queryLastByPolicyId(policyId, CodeCst.YES_NO__NO);
				boolean isFirstGenerate = (sheets == null || sheets.size() == 0);
				if (isFirstGenerate == false) {
					//不為第一次下載，若團險下載失敗，中斷處理	
					return ResultType.GROUP_LIST_DOWNLOAD_FAIL;
				}
			} else if (ResultType.SUCCESS.equals(result) == false) {
				return result;
			}

			try {
				NBUtils.logger(this.getClass(), "3 this.genUnb0631Report...");
				/* 3 genUNB0631Service取得 */
				dataHandler = this.genUnb0631Report(String.valueOf(policyId),
						String.valueOf(underwriteId), titleStr, userId);

				if (dataHandler == null) {
					throw new Exception("genUnb0631Report return dataHandler is null!");
				}
				/*
				DataSource dataSource = new FileDataSource(new File("E:\\00000245630.pdf"));
				dataHandler = new DataHandler(dataSource);
				*/
				NBUtils.logger(this.getClass(), "4 this.updateToNbWorkSheet...");
				/* 4 寫入T_NB_WORK_SHEET */
				this.updateToNbWorkSheet(policyId, underwriteId, dataHandler);
				NBUtils.logger(this.getClass(), "5 this.deleteUwWorkSheetTempTable...");
				/* 5 清除契審表暫存TABLE資料 */
				//boolean delTempData = this.deleteUwWorkSheetTempTable(policyId, underwriteId);

				if (dataHandler.getInputStream() == null) {
					/* 契審表報表產出錯誤*/
					result = ResultType.FAILURE;
				} else {
					/* 契審表產出成功! */
					result = ResultType.SUCCESS;
				}
				NBUtils.logger(this.getClass(), "6 this...END");

			} catch (Exception ex) {
				/* 契審表報表產出錯誤*/
				result = ResultType.EXCEPTION;
				log.error(NBUtils.getTWMsg(result.getMsgId()) + ":", ex);
			}
		}
		
		if (!ResultType.SUCCESS.equals(result)) {
            try {
                // 契審表錯誤更新回 NULL
                this.updateLatestUwTransferSubmitTime(underwriteId, null);
            } catch (Exception e) {
                /* 契審表報表產出錯誤*/
                result = ResultType.EXCEPTION;
                log.error(NBUtils.getTWMsg(result.getMsgId()) + ":", e);
            }
		}
		
		return result;
	}
	
	private void updateLatestUwTransferSubmitTime(Long underwriteId, Date submitDate) throws Exception {
        UwTransferVO lastestVO = uwTransferService.findLastestWorkUwTransfer(underwriteId);
        if (lastestVO != null) {
            if (CodeCst.UW_TRANS_FLOW_FIRSTER != lastestVO.getFlowId()) {
                lastestVO.setSubmitTime(submitDate);
                UserTransaction ut = TransUtils.getUserTransaction();
                try {
                    ut.begin();
                    NBUtils.logger(this.getClass(), "uwTransferService.save begin");
                    uwTransferService.save(lastestVO);
                    NBUtils.logger(this.getClass(), "uwTransferService.save end");
                    ut.commit();
                } catch (Exception e) {
                    TransUtils.rollback(ut);
                    throw e;
                }
            }
        }
	}

	@Override
	public ResultType generateFirstWorkSheet(Long policyId) throws RTException {

		List<UwWorkSheet> sheets = this.queryLastByPolicyId(policyId, CodeCst.YES_NO__NO);

		/* 找有沒有第一版資料 */
		if (sheets == null || sheets.size() == 0) {
			/* 沒有第一版時，執行 validateRule 產生契審表*/

			/* 契審表報表須傳入的參數 */
			Long underwriteId = null;

			int count = 0;

			//契審表需要underwriteId,執行等待查詢,最多等待3分鐘
			while (underwriteId == null) {
				if (count >= 36) {
					break;
				}

				underwriteId = uwPolicyService.findLastUnderwriteIdbyPolicyId(policyId);
				if (underwriteId == null) {
					count++;
					try {
						Thread.sleep(5000);
					} catch (InterruptedException e) {
						throw new RTException(e);
					}
				}
			}

			if (policyId != null && underwriteId != null) {
				String param = Para.getParaValue(com.ebao.ls.pub.CodeCstTgl.OPQ_CHECK_UW_WORK_SHEET);
				if (CodeCst.YES_NO__YES.equals(param)) {
					ResultType result = this.generateUWWorkSheet(policyId, underwriteId);
					return result;
				}
			}
		}

		return ResultType.NONE;
	}

	@Override
	public ResultType generateUWWorkSheet(Long policyId, Long underwriteId) {

		ResultType result = this.generateWorkSheet(policyId, underwriteId);

		List<ProposalRuleResultVO> ruleResultList = proposalRuleResultService.findAllPendingList(policyId,
				ProposalRuleMsg.PROPOSAL_RULE_MSG_GROUPLIST_DOWNLOAD_ERROR,
				null,
				null);

		if (ResultType.GROUP_LIST_DOWNLOAD_FAIL.equals(result)) {
			if (ruleResultList.size() == 0) {

				Date now = AppContext.getCurrentUserLocalTime();
				ProposalRuleResultVO resultVO;
				try {
					boolean isOpqV2 = validatorService.isOpqV2(policyId);
					resultVO = proposalRuleMsgService.genProposalRuleResultVO(ProposalRuleMsg.PROPOSAL_RULE_MSG_GROUPLIST_DOWNLOAD_ERROR, now, null, isOpqV2);
				} catch (Exception e) {
					throw new RTException(e);
				}

				resultVO.setPolicyId(policyId);
				resultVO.setRuleStatus(CodeCst.PROPOSAL_RULE_STATUS__PENDING);
				resultVO.setRuleType(CodeCst.PROPOSAL_RULE_TYPE__OPQ_NOT_SYS_CLOSE_MSG);

				proposalRuleResultService.save(resultVO);
			} else {
				for (ProposalRuleResultVO ruleResult : ruleResultList) {
					ruleResult.setRuleStatus(CodeCst.PROPOSAL_RULE_STATUS__SYSTEMCLOSED);
					proposalRuleResultService.update(ruleResult);
				}
			}
		}

		return result;
	}

	@Resource(name = UwPolicyService.BEAN_DEFAULT)
	private UwPolicyService uwPolicyService;

	//	/**
	//	 * <p>Description : PDF轉換為TIF(一頁一個TIF檔)</p>
	//	 * <p>Created By : Alex Cheng</p>
	//	 * <p>Create Time : Sep 10, 2016</p>
	//	 * @param output
	//	 * @return
	//	 * @throws Exception 
	//	 */
	//	private List<byte[]> pdfToTif2(InputStream input) throws Exception{
	//		NBUtils.logger(this.getClass(), "4.2.1 pdfToTif2 Start...");
	//		List<byte[]> images = IcePdfUtil.pdfToTiff(input);
	//		NBUtils.logger(this.getClass(), "4.2.2 pdfToTif2 genFile...");
	////		PdfUtil.genFile(images, "/R4E2E/EFB/EBAO_INTERNAL");
	////		NBUtils.logger(this.getClass(), "4.2.3 pdfToTif End...");
	//		return images;
	//	}

}
