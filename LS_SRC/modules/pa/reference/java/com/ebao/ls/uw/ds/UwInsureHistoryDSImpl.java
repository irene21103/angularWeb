package com.ebao.ls.uw.ds;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ebao.ls.pa.nb.bs.ODSService;
import com.ebao.ls.pa.pub.bs.BeneficiaryService;
import com.ebao.ls.pa.pub.bs.InsuredService;
import com.ebao.ls.pa.pub.bs.PolicyService;
import com.ebao.ls.pa.pub.bs.impl.BeneficiaryServiceImpl;
import com.ebao.ls.pa.pub.bs.impl.InsuredServiceImpl;
import com.ebao.ls.pa.pub.bs.impl.PolicyServiceImpl;
import com.ebao.ls.pa.pub.vo.BeneficiaryVO;
import com.ebao.ls.pa.pub.vo.InsuredVO;
import com.ebao.ls.pa.pub.vo.PolicyHolderVO;
import com.ebao.ls.pa.pub.vo.PolicyVO;
import com.ebao.ls.pub.CodeCst;
import com.ebao.ls.pub.data.InsuredVOComparator;
import com.ebao.ls.uw.data.UwInsureHistoryDelegate;
import com.ebao.ls.uw.ds.vo.UwFatcaDataVO;
import com.ebao.ls.uw.ds.vo.UwFinanceNotifyVO;
import com.ebao.ls.uw.ds.vo.UwHistoryInsureVO;
import com.ebao.ls.uw.ds.vo.UwInsureClientVO;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.framework.GenericDS;
import com.ebao.pub.i18n.lang.StringResource;
import com.ebao.pub.util.StringUtils;

/**
 * <p>Title: 查詢投保歷史資料 實作UwInsureHistoryService</p>
 * <p>Description: Function Description Only vegetable</p>
 * <p>Copyright: Copyright (c) 2015</p>
 * <p>Company: TGL Co., Ltd.</p>
 * <p>Create Time: Aug 20, 2015</p> 
 * @author 
 * <p>Update Time: Aug 20, 2015</p>
 * <p>Updater: Alex Cheng</p>
 * <p>Update Comments: </p>
 */
public class UwInsureHistoryDSImpl extends GenericDS implements UwInsureHistoryService {
	private static final Logger logger = LoggerFactory.getLogger(UwInsureHistoryDSImpl.class);

    @Resource(name = PolicyService.BEAN_DEFAULT)
    private PolicyServiceImpl policyServiceImpl;

    @Resource(name = InsuredService.BEAN_DEFAULT)
    private InsuredServiceImpl insuredServiceImpl;

    @Resource(name = BeneficiaryService.BEAN_DEFAULT)
    private BeneficiaryServiceImpl beneficiaryServiceImpl;

    @Resource(name = UwInsureHistoryDelegate.BEAN_DEFAULT)
    private UwInsureHistoryDelegate uwInsureHistoryDao;
    
	@Resource(name = UwPolicyService.BEAN_DEFAULT)
	private UwPolicyService uwPolicyDS;

    @Override
    public PolicyHolderVO findPolicyHolderById(String policyId) {
        PolicyVO policy = policyServiceImpl.load(Long.valueOf(policyId));
        return policy.getPolicyHolder();
    }

    @Override
    public List<InsuredVO> findInsuredById(String policyId) {
        List<InsuredVO> queryList = insuredServiceImpl.findByPolicyId(Long
                        .valueOf(policyId));
        return queryList;
    }

    @Override
    public List<BeneficiaryVO> findBeneById(String policyId) {
        List<BeneficiaryVO> queryList = beneficiaryServiceImpl
                        .findByPolicyId(Long.valueOf(policyId));
        return queryList;
    }

    /**
     * <p>Description : 查詢FATCA_DATA</p>
     * <p>Created By : Alex Cheng</p>
     * <p>Create Time : Jun 13, 2016</p>
     * @param policyId
     * @return
     */
    @Override
    public List<UwFatcaDataVO> findFatcaDatas(Long policyId) {
        List<UwFatcaDataVO> queryList = uwInsureHistoryDao
                        .findFatcaDatas(policyId);
        return queryList;
    }

	/**
	 * <p>Description : 查詢投保歷史資料</p>
	 * <p>Created By : Alex Cheng</p>
	 * <p>Create Time : Aug 21, 2015</p>
	 * <p>Update Time: Sep 46, 2020</p>
	 * <p>Updater: Charlotte Wang</p>
	 * <p>Update Comments: PCR 351294 未承保件個人資料保存期限逾未承保起五年不得處理及利用</p>
	 * @param uwInsureClients
	 * @param policyId
	 * @param curPolicyApplyDate 頁面上當前保單的要保書填寫日
	 * @return boolean 團險下載是否成功
	 */
	private boolean findInsureHistoryByClient(List<UwInsureClientVO> uwInsureClients, Long policyId, Date curPolicyApplyDate) {
    	boolean isWorkable = true; //判斷團險下載是否成功

    	String policyCode = policyServiceImpl.getPolicyCodeByPolicyId(policyId);
    	if (uwInsureClients != null && uwInsureClients.size() > 0) {
    		String langId = AppContext.getCurrentUser().getLangId();
    		String yun = StringResource.getStringData("MSG_216294", langId); // 元
    		String danway = StringResource.getStringData("MSG_113300", langId); // 單位
    		String project = StringResource.getStringData("MSG_NB_000132",langId); // 計畫

    		for (UwInsureClientVO uwInsureClientVO : uwInsureClients) {
				List<UwHistoryInsureVO> list = uwInsureHistoryDao.findInsureHistoryByCeritCode(uwInsureClientVO.getCertiType(), uwInsureClientVO.getCertiCode(), curPolicyApplyDate);

				if( !StringUtils.isNullOrEmpty(uwInsureClientVO.getOldForeignerId()) ) { //以舊式統一證號查詢保單歷史資料
					list.addAll(  uwInsureHistoryDao.findInsureHistoryByCeritCode(uwInsureClientVO.getOldForeignerId(), uwInsureClientVO.getOldForeignerId(), curPolicyApplyDate) );
				}
    			/* 2016/11/17 simon.huang:RTC-96052 要/被保險人同一人不需呈現 */
    			List<UwHistoryInsureVO> filterList = new ArrayList<UwHistoryInsureVO>();
    			Set<String> keySet = new HashSet<String>();
				for (Iterator<UwHistoryInsureVO> iter = list.listIterator(); iter.hasNext();) {
    				UwHistoryInsureVO vo = iter.next();
					// 過濾掉當前在核保的這筆保單號碼
					if (policyCode.equals(vo.getPolicyCode())) {
						continue;
					}

    				if ("1".equals(vo.getOrderType())) {
						String key = vo.getPolicyCode() + ":" + vo.getInsuredCertiCode();
    					keySet.add(key);

    					filterList.add(vo);
    				} else {
    					/* 要/被保險人不同人才顯示 */
						String key = vo.getPolicyCode() + ":" + vo.getHolderCertiCode();
    					if (keySet.contains(key) == false) {
    						filterList.add(vo);
    					}
    				}
				} // for
    			/* end 過濾要/被保險人同一人 */

				Long firstPolicyId = 0L;

				for (Iterator<UwHistoryInsureVO> iter = filterList.listIterator(); iter.hasNext();) {
					UwHistoryInsureVO vo = iter.next();

    				Long policyIdVal = vo.getPolicyId();
    				String amount = vo.getAmount();
    				String unit = vo.getUnit();
    				String benefitLevel = vo.getBenefitLevel();

    				if ("0".equals(vo.getLiabilityState())) {
						String strId = "COD_T_PROPOSAL_STATUS_" + vo.getProposalStatus();
						String liabilityName = uwInsureHistoryDao.getStrDataByStrId(strId, langId);
    					vo.setLiabilityName(liabilityName);
    				} else {
						String strId = "COD_T_LIABILITY_STATUS_" + vo.getLiabilityState();
						String liabilityName = uwInsureHistoryDao.getStrDataByStrId(strId, langId);
    					vo.setLiabilityName(liabilityName);
    				}

					if (StringUtils.isNumber(amount) && !"0".equals(amount)) {
						vo.setAmountName(String.format("%,d", Integer.parseInt(amount)) + yun);
					} else if (StringUtils.isNullOrEmpty(benefitLevel) == false && !"0".equals(benefitLevel)) {
    					vo.setAmountName(project + benefitLevel);
					} else if (StringUtils.isNullOrEmpty(unit) == false && !"0".equals(unit)) {
    					vo.setAmountName(unit + danway); //RTC 171044 調整為XX單位
    				}

    				//TODO 首年度
    				vo.setFirstYearAmount(vo.getFirstYearAmount());
    				String waiver = vo.getWaiver();
    				if (firstPolicyId != null && !firstPolicyId.equals(policyIdVal)) {
						if (CodeCst.YES_NO__YES.equals(waiver)) {
    						vo.setAmountName(null);
    						vo.setMoneyId(null);
    						vo.setExtraPrem(null);
    						vo.setExclusion(null);
    						vo.setMedical(null);
    						vo.setSurvival(null);
    						vo.setInterView(null);
    						vo.setImageIndi(null);
    						firstPolicyId = policyIdVal;
    					}
    				} else {
						if (CodeCst.YES_NO__YES.equals(waiver)) {
    						iter.remove();
    					}
    				}
				} // for

    			/* 團險資料查訊 */
    			try {
					filterList.addAll(paODSService.getGroupListByCertiCode(uwInsureClientVO.getCertiCode(), curPolicyApplyDate));
    			} catch (Throwable e) {
    				isWorkable = false; //團險接口有誤
    			}

    			uwInsureClientVO.setHistoryInsureList(filterList);
			} //for (UwInsureClientVO uwInsureClientVO : uwInsureClients)
    	}
    	return isWorkable;
    }

    @Resource(name = ODSService.BEAN_DEFAULT)
    protected ODSService paODSService;

    @Override
    public String getNameEngToLang(String insuredName, String langId) {

        String transName = uwInsureHistoryDao.getNameEngToLang(insuredName,
                        langId);
        return transName;
    }

    @Override
    public String getStrDataByStrId(String strId, String langId) {

        String transName = uwInsureHistoryDao.getStrDataByStrId(strId, langId);
        return transName;
    }

    @Override
    public void findSignatureHistoryByClient(
                    List<UwInsureClientVO> uwInsureClients) {
        if (uwInsureClients != null && uwInsureClients.size() > 0) {
            for (UwInsureClientVO uwInsureClientVO : uwInsureClients) {
                List<UwHistoryInsureVO> list = uwInsureHistoryDao
                                .findSignatureHistoryByCeritCode(
                                                uwInsureClientVO.getCertiType(),
                                                uwInsureClientVO.getCertiCode());

                uwInsureClientVO.setHistoryInsureList(list);
            }
        }
    }
    
    	
    /**
	 * <p>Description : 新契約核保及契審表共用投保歷史記錄查詢</p>
	 * <p>Created By : simon.huang</p>
	 * <p>Create Time : Jul 28, 2017</p>
	 * @param insureClientList 傳入需設定的空List
	 * @param policyId
	 * @return
	 */
    @Override
	public boolean findUwInsuredHistoryRecord(List<UwInsureClientVO> insureClientList, String policyId) {
		PolicyVO policy = policyServiceImpl.load(Long.valueOf(policyId));
		PolicyHolderVO policyHolderVO = policy.getPolicyHolder();

		// @:整理有多少人要查詢投保歷史資料
		List<InsuredVO> insureds = this.findInsuredById(policyId);
		java.util.Collections.sort(insureds, new InsuredVOComparator());
		// 找出這張保單的被保人資料，去查這些被保人的投保歷史資料
		for (InsuredVO o : insureds) {

		    if (o != null && !StringUtils.isNullOrEmpty(o.getCertiCode())
		                    && !StringUtils.isNullOrEmpty(o.getCertiType())) {

		        // 判斷有沒有重覆的身份證號，因為有時候相同的被保人會投保多個保項
		        boolean flag = true;
		        for (UwInsureClientVO uwInsureClientVO : insureClientList) {
		            if (uwInsureClientVO.getCertiCode().equals(o.getCertiCode())) {
		                flag = false;
		            }
		            if (!flag)
		                break;
		        }

		        if (flag) {
		            UwInsureClientVO tmpVo = new UwInsureClientVO();
		            tmpVo.setCertiCode(o.getCertiCode());
		            tmpVo.setCertiType(o.getCertiType());
		            tmpVo.setName(o.getName());
		            tmpVo.setOrderType("1");
		            tmpVo.setOldForeignerId(o.getOldForeignerId());
		            insureClientList.add(tmpVo);
		        }

		    }
		    
		}

		// 用要保人資料去查這些要保人的投保歷史資料
		if (policyHolderVO != null && !StringUtils.isNullOrEmpty(policyHolderVO.getCertiCode())
		                && !StringUtils.isNullOrEmpty(policyHolderVO.getCertiType())) {

		    // 判斷要保人有沒有在查詢清單裡，如果沒有才加入(因為有可能這個要保人同時又是被保人，上面就已經加過了)
		    boolean flag = true;
		    for (UwInsureClientVO uwInsureClientVO : insureClientList) {
		        if (uwInsureClientVO.getCertiCode().equals(policyHolderVO.getCertiCode())) {
		            flag = false;
		        }
		        if (!flag)
		            break;
		    }

		    if (flag) {
		    	UwInsureClientVO vo = new UwInsureClientVO();
		        vo.setCertiCode(policyHolderVO.getCertiCode());
		        vo.setCertiType(policyHolderVO.getCertiType());
		        vo.setName(policyHolderVO.getName());
		        vo.setOrderType("2");
		        vo.setOldForeignerId(policyHolderVO.getOldForeignerId());
		        insureClientList.add(vo);
		    }

		}

		/* 2016/10/21 amy: 以下 BSD 並沒有說要用受益人去查，跟 BA 討論此部份先註解掉
		for (BeneficiaryVO o : uwInsureHistoryService.findBeneById(policyId)) {
		    if (o != null && !StringUtils.isNullOrEmpty(o.getCertiCode()) && o.getCertiType() != null
		                    && o.getCertiType() > 0) {
		        boolean flag = true;
		        for (UwInsureClientVO uwInsureClientVO : insureClientList) {
		            if (uwInsureClientVO.getCertiCode().equals(o.getCertiCode())) {
		                flag = false;
		            }
		            if (!flag)
		                break;
		        }
		        if (flag) {
		            UwInsureClientVO tmpVo = new UwInsureClientVO();
		            tmpVo.setCertiCode(o.getCertiCode());
		            tmpVo.setCertiType(o.getCertiType().toString());
		            tmpVo.setName(o.getName());
		            tmpVo.setOrderType("3");
		            insureClientList.add(tmpVo);
		        }
		    }
		}*/

		// end.整理有多少人要準備查詢投保歷史資料
		 boolean isGroupDownlaodOK = false;
		// @:把整理好要查的人傳入，得到這些人的投保歷史資料
		if (insureClientList.size() > 0) {
			isGroupDownlaodOK = this.findInsureHistoryByClient(insureClientList, new Long(policyId), policy.getApplyDate());

			// 過濾掉當前在核保的這筆保單號碼
			Iterator<UwInsureClientVO> iter = insureClientList.listIterator();
			while (iter.hasNext()) {
				UwInsureClientVO clientVO = iter.next();

				Iterator<UwHistoryInsureVO> iter2 = clientVO.getHistoryInsureList().listIterator();
				while (iter2.hasNext()) {
					UwHistoryInsureVO insureVO = iter2.next();
					if (insureVO != null && insureVO.getPolicyId() != null) {
						if (policyId.equals(insureVO.getPolicyId().toString())) {
							iter2.remove();
						}
					}
				} // while
			} // while
		}
		
		return isGroupDownlaodOK;
	}

	@Override
	public void findUwFinanceNotiFy(List<UwInsureClientVO> insureClientList, String policyId) {
		
		PolicyHolderVO policyHolderVO = this.findPolicyHolderById(policyId);
		List<InsuredVO> insureds = this.findInsuredById(policyId);
		java.util.Collections.sort(insureds, new InsuredVOComparator());
		Set<String> certiCodeSet = new HashSet<String>();
		
		for (InsuredVO o : insureds) {

		    if (o != null && !StringUtils.isNullOrEmpty(o.getCertiCode())
		                    && !StringUtils.isNullOrEmpty(o.getCertiType())) {
		        // 判斷有沒有重覆的身份證號，因為有時候相同的被保人會投保多個保項
		    	if (!certiCodeSet.contains(o.getCertiCode())) {
		    		
		    		certiCodeSet.add(o.getCertiCode());
		    		UwInsureClientVO tmpVo = new UwInsureClientVO();
		    		tmpVo.setCertiCode(o.getCertiCode());
		    		tmpVo.setCertiType(o.getCertiType());
		    		tmpVo.setName(o.getName());
		    		tmpVo.setOrderType("1");
		    		tmpVo.setOldForeignerId(o.getOldForeignerId());
		    		insureClientList.add(tmpVo);
		    	}

		    }
		}

		// 用要保人資料去查這些要保人的投保歷史資料
		if (policyHolderVO != null && !StringUtils.isNullOrEmpty(policyHolderVO.getCertiCode())
		                && !StringUtils.isNullOrEmpty(policyHolderVO.getCertiType())) {
		    // 判斷要保人有沒有在查詢清單裡，如果沒有才加入(因為有可能這個要保人同時又是被保人，上面就已經加過了)
			if (!certiCodeSet.contains(policyHolderVO.getCertiCode())) {
			
				certiCodeSet.add(policyHolderVO.getCertiCode());
		    	UwInsureClientVO vo = new UwInsureClientVO();
		        vo.setCertiCode(policyHolderVO.getCertiCode());
		        vo.setCertiType(policyHolderVO.getCertiType());
		        vo.setName(policyHolderVO.getName());
		        vo.setOrderType("2");
		        vo.setOldForeignerId(policyHolderVO.getOldForeignerId());
		        insureClientList.add(vo);
		        
			}
		}
		
		this.findFinanceInfoRecord(insureClientList, Long.valueOf(policyId));
		
	}
	
	
	private void findFinanceInfoRecord(List<UwInsureClientVO> uwInsureClients, final Long policyId) {

		List<Map<String, Object>> list = uwPolicyDS.getFinanceInfoDetail(policyId);

		for (UwInsureClientVO uwInsureClientVO : uwInsureClients) {

			List<UwFinanceNotifyVO> voList = uwInsureClientVO.getFinanceNotifyList();

			for (Map<String, Object> map : list) {

				BigDecimal detPolicyId = (BigDecimal) map.get("POLICY_ID");
				String detPolicyCode = (String) map.get("POLICY_CODE");
				String identity = (String) map.get("POLICY_IDENTITY");
				// identity有可能合并为“主被保險人要保人”
				if (identity != null && identity.length() > 3) {
					identity = identity.replace(
							StringResource.getStringData("MSG_100182", AppContext.getCurrentUser().getLangId()), "");
				}
				String name = (String) map.get("CUSTOMER_NAME");
				String certiCode = (String) map.get("CERTI_CODE");
				String oldForeignerId = (String) map.get("OLD_FOREIGNER_ID");
				java.sql.Timestamp applyDate = (java.sql.Timestamp) map.get("APPLY_DATE");
				java.sql.Timestamp updateTime = (java.sql.Timestamp) map.get("UPDATE_TIME");
				String proposalStatus = (String) map.get("PROPOSAL_STATUS");
				String proposalStatusDesc = (String) map.get("PROPOSAL_STATUS_DESC");
				BigDecimal anIncome = (BigDecimal) map.get("AN_INCOME");
				BigDecimal anIncomeOther = (BigDecimal) map.get("AN_INCOME_OTHER");
				BigDecimal anIncomeFamily = (BigDecimal) map.get("AN_INCOME_FAMILY");
				BigDecimal fnIncome = (BigDecimal) map.get("FN_INCOME");
				BigDecimal fnIncomeOther = (BigDecimal) map.get("FN_INCOME_OTHER");
				BigDecimal fnIncomeFamily = (BigDecimal) map.get("FN_INCOME_FAMILY");

				if (anIncome == null && anIncomeOther == null && anIncomeFamily == null 
						&& fnIncome == null	&& fnIncomeOther == null && fnIncomeFamily == null) {
					continue;
				}
				if ( (!StringUtils.isNullOrEmpty(certiCode) &&
						(certiCode.equals(uwInsureClientVO.getCertiCode()) ) || certiCode.equals(uwInsureClientVO.getOldForeignerId()) ) ||
					 (!StringUtils.isNullOrEmpty(oldForeignerId) && 
						(oldForeignerId.equals(uwInsureClientVO.getOldForeignerId()) || oldForeignerId.equals(uwInsureClientVO.getCertiCode())) ) ) {
					if (detPolicyCode != null) {

						UwFinanceNotifyVO finVO = new UwFinanceNotifyVO();
						finVO.setPolicyId(detPolicyId.longValue());
						finVO.setPolicyCode(detPolicyCode);
						finVO.setIdentity(identity);
						setFinVOIdentityOrder(finVO);
						finVO.setCertiCode(certiCode);
						finVO.setOldForeignerId(oldForeignerId);
						finVO.setCustomerName(name);
						finVO.setApplyDate(new Date(applyDate.getTime()));
						if (updateTime != null) {
							finVO.setUpdateTime(new Date(updateTime.getTime()));
						}
						finVO.setProposalStatus(proposalStatus);
						finVO.setProposalStatusDesc(proposalStatusDesc);
						if (anIncome != null) {
							finVO.setAnIncome(String.format("%,d", anIncome.longValue()));
						}
						if (anIncomeOther != null) {
							finVO.setAnIncomeOther(String.format("%,d", anIncomeOther.longValue()));
						}
						if (anIncomeFamily != null) {
							finVO.setAnIncomeFamily(String.format("%,d", anIncomeFamily.longValue()));
						}
						if (fnIncome != null) {
							finVO.setFnIncome(String.format("%,d", fnIncome.longValue()));
						}
						if (fnIncomeOther != null) {
							finVO.setFnIncomeOther(String.format("%,d", fnIncomeOther.longValue()));
						}
						if (fnIncomeFamily != null) {
							finVO.setFnIncomeFamily(String.format("%,d", fnIncomeFamily.longValue()));
						}
						voList.add(finVO);

					}
				}
			}

			Collections.sort(voList, new Comparator<UwFinanceNotifyVO>() {

				@Override
				public int compare(UwFinanceNotifyVO o1, UwFinanceNotifyVO o2) {
					int o1Sort = 0;
					int o2Sort = 0;
					// 本件保單置頂
					if (!o1.getPolicyId().equals(o2.getPolicyId())) {
						if (policyId.equals(o1.getPolicyId())) {
							o1Sort = 0;
							o2Sort = 1;
						}
						if (policyId.equals(o2.getPolicyId())) {
							o1Sort = 1;
							o2Sort = 0;
						}
					}
					int compare = o1Sort - o2Sort;
					if (compare == 0) {
						// 1. 按照保單要保書填寫日期降序排列
						compare = o2.getApplyDate().compareTo(o1.getApplyDate());
					}
					if (compare == 0) {
						// 2. 保單號碼由小到大
						compare = o1.getPolicyCode().compareTo(o2.getPolicyCode());
					}
					if (compare == 0) {
						// 3. 角色排序
						compare = o1.getIdentityOrder().compareTo(o2.getIdentityOrder());
					}
					return compare;
				}
			});

		}
	}
	
	
	/**
	 * 角色的欄位，排列順序為主被保險人、被保險人、要保人
	 * @param finVO
	 */
	private void setFinVOIdentityOrder(UwFinanceNotifyVO finVO) {
		if (finVO != null && !StringUtils.isNullOrEmpty(finVO.getIdentity())) {
			if (StringResource.getStringData("MSG_1252242", AppContext.getCurrentUser().getLangId())
					.equals(finVO.getIdentity())) { //主被保險人
				finVO.setIdentityOrder(2);
			} else if (StringResource.getStringData("MSG_105703", AppContext.getCurrentUser().getLangId())
					.equals(finVO.getIdentity())) { //被保險人
				finVO.setIdentityOrder(3);
			}else if (StringResource.getStringData("MSG_100182", AppContext.getCurrentUser().getLangId())
					.equals(finVO.getIdentity())) { //要保人
				finVO.setIdentityOrder(1);
			}

		}
	}

}