package com.ebao.ls.uw.ctrl.letter;

import java.util.List;

import com.ebao.ls.pa.pub.vo.InsuredVO;
import com.ebao.ls.pa.pub.vo.PolicyHolderVO;
import com.ebao.pub.framework.GenericForm;

public class AddInformLetterForm extends GenericForm {

	/**
	 * <code>serialVersionUID</code> 的註解
	 */
	private static final long serialVersionUID = 1202986722981718885L;
	
	private Long ListId;

	public Long getListId() {
		return ListId;
	}

	public void setListId(Long listId) {
		ListId = listId;
	}
	
	private Long msgId;


	public Long getMsgId() {
		return msgId;
	}

	public void setMsgId(Long msgId) {
		this.msgId = msgId;
	}

	private Long policyId;

	private Long underwriteId;
	
	private String name;
	
	private String subAction;
	
	private List<InsuredVO> insuredVOs;
	
	private List<Boolean> isLockInsureds;
	
	private int sourceType;
	
	private Long sourceId;
	
	private String documentContent;

	
	public String getDocumentContent() {
		return documentContent;
	}

	public void setDocumentContent(String documentContent) {
		this.documentContent = documentContent;
	}

	private String retMsg;
	
	private Boolean submitSuccess = new Boolean(false);
	
	
	private PolicyHolderVO holderVO;
	
	public PolicyHolderVO getHolderVO() {
		return holderVO;
	}

	public void setHolderVO(PolicyHolderVO holderVO) {
		this.holderVO = holderVO;
	}

	public Long getPolicyId() {
		return policyId;
	}

	public void setPolicyId(Long policyId) {
		this.policyId = policyId;
	}

	public Long getUnderwriteId() {
		return underwriteId;
	}

	public void setUnderwriteId(Long underwriteId) {
		this.underwriteId = underwriteId;
	}
	

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRetMsg() {
		return retMsg;
	}

	public void setRetMsg(String retMsg) {
		this.retMsg = retMsg;
	}

	public List<InsuredVO> getInsuredVOs() {
		return insuredVOs;
	}

	public void setInsuredVOs(List<InsuredVO> insuredVOs) {
		this.insuredVOs = insuredVOs;
	}


	public int getSourceType() {
		return sourceType;
	}

	public void setSourceType(int sourceType) {
		this.sourceType = sourceType;
	}

	public Long getSourceId() {
		return sourceId;
	}

	public void setSourceId(Long sourceId) {
		this.sourceId = sourceId;
	}

	public String getSubAction() {
		return subAction;
	}

	public void setSubAction(String subAction) {
		this.subAction = subAction;
	}

	
	public List<Boolean> getIsLockInsureds() {
		return isLockInsureds;
	}

	public void setIsLockInsureds(List<Boolean> isLockInsureds) {
		this.isLockInsureds = isLockInsureds;
	}


	public Boolean getSubmitSuccess() {
		return submitSuccess;
	}

	public void setSubmitSuccess(Boolean submitSuccess) {
		this.submitSuccess = submitSuccess;
	}

}
