package com.ebao.ls.uw.ctrl.extraloading;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.ls.pa.pub.ci.CoverageCI;
import com.ebao.ls.pa.pub.ref.api.prd.ProductService;
import com.ebao.ls.pa.pub.service.PolicyService;
import com.ebao.ls.pa.pub.vo.CoverageVO;
import com.ebao.ls.pub.CodeCst;
import com.ebao.ls.uw.ctrl.ActionUtil;
import com.ebao.ls.uw.ctrl.UwGenericAction;
import com.ebao.ls.uw.ctrl.pub.CustomerForm;
import com.ebao.ls.uw.ctrl.pub.UwExtraLoadingForm;
import com.ebao.ls.uw.ctrl.pub.UwProductForm;
import com.ebao.ls.uw.ds.sp.CompleteUWProcessSp;
import com.ebao.ls.uw.ds.sp.QueryUwDataSp;
import com.ebao.ls.uw.util.Utils;
import com.ebao.ls.uw.vo.UwExtraLoadingVO;
import com.ebao.ls.uw.vo.UwPolicyVO;
import com.ebao.ls.uw.vo.UwProductVO;
import com.ebao.pub.framework.ExceptionFactory;
import com.ebao.pub.framework.GenericException;
import com.ebao.pub.framework.MultiWarning;
import com.ebao.pub.util.BeanUtils;
import com.ebao.pub.util.DateUtils;

/**
 * <p>
 * Title:Underwriting
 * </p>
 * <p>
 * Description:
 * </p>
 * <p>
 * Copyright: Copyright (c) 2004
 * </p>
 * <p>
 * Company: eBaoTech Corporation
 * </p>
 * 
 * @author mingchun.shi
 * @since Jun 27, 2005
 * @version 1.0
 */
public class ReloadLoadingAction extends UwGenericAction {
  /**
   * @param mapping
   * @param form
   * @param request
   * @param response added by hendry.xu to solve the same record in extra
   *          loading .
   */
  @Override
  public MultiWarning processWarning(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {
    MultiWarning warning = new MultiWarning();
    Long preInsuredId = Long.valueOf(request.getParameter("preInsured"));
    String uwSourceType = request.getParameter("uwSourceType");
    boolean isNewbiz = false;
    if (CodeCst.UW_SOURCE_TYPE__NB.equals(uwSourceType)) {
      isNewbiz = true;
    }
    // UwExtraLoadingVO extraLoadingVO = new UwExtraLoadingVO();
    extraLoadingActionHelper.processSameRecordWarning(request, warning,
        preInsuredId, isNewbiz);
    return warning;
  }

  /**
   * reloading action
   * 
   * @param mapping
   * @param form
   * @param req
   * @param res
   * @return
   * @throws GenericException
   */
  @Override
  public ActionForward uwProcess(ActionMapping mapping, ActionForm form,
      HttpServletRequest req, HttpServletResponse res) throws GenericException {
    try {
      // get the value from page
      Long underwriteId = Long.valueOf(com.ebao.ls.encode.encoder.Encode.encodeForHTML(req).getParameter("underwriteId"));
      Long itemId = Long.valueOf(com.ebao.ls.encode.encoder.Encode.encodeForHTML(req).getParameter("itemIds"));
      Long insuredId = Long.valueOf(com.ebao.ls.encode.encoder.Encode.encodeForHTML(req).getParameter("insured"));
      Long preInsuredId = Long.valueOf(com.ebao.ls.encode.encoder.Encode.encodeForHTML(req).getParameter("preInsured"));
      // calculate the extra prem
      UwExtraLoadingVO[] extraLoadingVOs = ActionUtil.calAndSaveExtraLoading(
          req, preInsuredId);
      UwProductVO productValue = extraLoadingActionHelper.initProductVO(req,
          underwriteId, itemId);
      productValue.setUwExtraLoadings(Arrays.asList(extraLoadingVOs));
      String gender = req.getParameter("gender");
      for (int i = 0; i < extraLoadingVOs.length; i++) {
        if ((extraLoadingVOs[i].getEmValue() != null)
            && (extraLoadingVOs[i].getEmValue().intValue() >= 0)
            && ("1".equals(extraLoadingVOs[i].getExtraArith()))
            && (extraLoadingVOs[i].getExtraPara() == null || extraLoadingVOs[i]
                .getExtraPara().compareTo(new BigDecimal(0)) == 0)) {
          extraLoadingActionHelper.doCalExRate(productValue, gender, i,
              extraLoadingVOs[i].getExtraArith(), insuredId, false);
          getUwPolicyDS().updateUwExtraLoading(
              productValue.getUwExtraLoadings().get(i));
        }
      }
      // when cs-uw,synchronize records
      UwPolicyVO uwPolicyVO = getUwPolicyDS().findUwPolicy(underwriteId);
      if (Utils.isCsUw(uwPolicyVO.getUwSourceType())) {
        CompleteUWProcessSp.synUWInfo4CS(uwPolicyVO.getPolicyId(),
            uwPolicyVO.getUnderwriteId(), uwPolicyVO.getChangeId(),
            Integer.valueOf(2));
      }
      Collection insureds = getUwPolicyDS().findUwLifeInsuredEntitis(
          Long.valueOf(req.getParameter("underwriteId")),
          Long.valueOf(req.getParameter("itemIds")));
      req.setAttribute("insured", insuredId);
      Collection customerFormCollection = BeanUtils.copyCollection(
          CustomerForm.class, insureds);
      CustomerForm[] insured = (CustomerForm[]) customerFormCollection
          .toArray(new CustomerForm[customerFormCollection.size()]);
      Long policyId = productValue.getPolicyId();
      String uwSourceType = req.getParameter("uwSourceType");
      if (!CodeCst.UW_SOURCE_TYPE__NB.equals(uwSourceType)) {
        setDueDateList(policyId, req);
      }
      req.setAttribute("insureds", insured);
      req.setAttribute("action_form", decompose(productValue, req));
    } catch (Exception e) {
      throw ExceptionFactory.parse(e);
    }
    return mapping.findForward("extra");
  }

  /**
   * get due date list
   * 
   * @param policyId
   * @param request
   * @throws Exception
   */
  public void setDueDateList(Long policyId, HttpServletRequest request)
      throws Exception {
    CoverageVO policyProductVO = coverageCI.getMainBenefitByPolicyId(policyId);
    Integer mainProductId = policyProductVO.getProductId();
    Date validateDate = policyProductVO.getInceptionDate();
    String chargeType = policyProductVO.getCurrentPremium().getPaymentFreq();
    Date paidUpDate = policyProductVO.getPaidupDate();
    boolean isIlp = this.getProductService()
        .getProduct(Long.valueOf(mainProductId.intValue()),policyService.getActiveVersionDate(policyProductVO)).isIlp();
    List dueDateList = Utils.getAllDueDate(validateDate, chargeType, isIlp,
        paidUpDate);
    String dueDates = "";
    for (Iterator iter = dueDateList.iterator(); iter.hasNext();) {
      String element = (String) iter.next();
      dueDates = "'" + element + "'" + "," + dueDates;
    }
    if (dueDates.endsWith(",")) {
      dueDates = dueDates.substring(0, dueDates.length() - 1);
    }
    String dateArray = "[" + dueDates + "]";
    request.setAttribute("dateArray", dateArray);
    // added by hendry.xu to GEL00024917
    String paidUpDateStr = DateUtils.date2String(paidUpDate);
    paidUpDateStr = "'" + paidUpDateStr + "'";
    request.setAttribute("paidUpDate", paidUpDateStr);
  }

  /**
   * get the UwExtraLoadingInfoForm value
   * 
   * @param productValue
   * @param req
   * @return
   * @throws Exception
   */
  public UwExtraLoadingInfoForm decompose(UwProductVO productValue,
      HttpServletRequest req) throws Exception {
    UwExtraLoadingInfoForm extraForm = new UwExtraLoadingInfoForm();
    extraForm.setApplyCode(req.getParameter("applyCode"));
    extraForm.setPolicyCode(req.getParameter("policyCode"));
    extraForm.setBenefityType(req.getParameter("benefitType"));
    extraForm.setUwStatus(req.getParameter("uwStatus"));
    try {
      if ("true".equals(req.getParameter("isLoading"))) {
        extraForm.setUwProductForm(getUwProductForm(req));
      } else {
        BeanUtils.copyProperties(extraForm.getUwProductForm(), productValue);
      }
    } catch (Exception e) {
      throw ExceptionFactory.parse(e);
    }
    String decisionId = req.getParameter("decisionId");
    if (decisionId != null && decisionId.trim().length() > 0) {
      extraForm.getUwProductForm().setDecisionId(
          Integer.valueOf(decisionId.trim()));
    } else {
      extraForm.getUwProductForm().setDecisionId(Integer.valueOf(2));
    }
    // get the default charge period
    int chargeYear = QueryUwDataSp.getChargeTerm(productValue.getItemId());
    extraForm.getUwProductForm().setChargeYear(Integer.valueOf(chargeYear));
    extraForm.setDuration(chargeYear + "");
    List<UwExtraLoadingVO> extraLoadingVOs = productValue.getUwExtraLoadings();
    if (extraLoadingVOs != null) {
      for (int i = 0; i < extraLoadingVOs.size(); i++) {
        UwExtraLoadingVO extraLoading = extraLoadingVOs.get(i);
        UwExtraLoadingForm extraLoadingForm = new UwExtraLoadingForm();
        if (extraLoading != null) {
          BeanUtils.copyProperties(extraLoadingForm, extraLoading);
          if (!"".equals(extraLoading.getExtraType())) {
            if (extraLoading.getExtraType() != null) {
              switch (extraLoading.getExtraType().charAt(0)) {
              case 'A':
                extraForm.getHealth().add(extraLoadingForm);
                extraForm.setHealthFlag("Y");
                break;
              case 'B':
                extraForm.getOccupation().add(extraLoadingForm);
                extraForm.setOccupationFlag("Y");
                break;
              case 'C':
                extraForm.getAvocation().add(extraLoadingForm);
                extraForm.setAvocationFlag("Y");
                break;
              case 'D':
                extraForm.getResidential().add(extraLoadingForm);
                extraForm.setResidentialFlag("Y");
                break;
              case 'E':
                extraForm.getAviation().add(extraLoadingForm);
                extraForm.setAviationFlag("Y");
                break;
              case 'F':
                extraForm.getOther().add(extraLoadingForm);
                extraForm.setOtherFlag("Y");
                break;
              }
            }
          }
        }
      }
    }
    if (extraForm.getHealth().size() == 0) {
      extraForm.getHealth().add(new UwExtraLoadingForm());
    }
    if (extraForm.getOccupation().size() == 0) {
      extraForm.getOccupation().add(new UwExtraLoadingForm());
    }
    if (extraForm.getAvocation().size() == 0) {
      extraForm.getAvocation().add(new UwExtraLoadingForm());
    }
    if (extraForm.getResidential().size() == 0) {
      extraForm.getResidential().add(new UwExtraLoadingForm());
    }
    if (extraForm.getAviation().size() == 0) {
      extraForm.getAviation().add(new UwExtraLoadingForm());
    }
    if (extraForm.getOther().size() == 0) {
      extraForm.getOther().add(new UwExtraLoadingForm());
    }
    return extraForm;
  }

  /**
   * get the UwProductForm value
   * 
   * @param req HttpServletRequest
   * @throws Exception
   * @return UwProductForm
   */
  public UwProductForm getUwProductForm(HttpServletRequest req)
      throws Exception {
    UwProductForm productForm = new UwProductForm();
    Long underwriteId = Long.valueOf(req.getParameter("underwriteId"));
    Long itemId = Long.valueOf(req.getParameter("itemIds"));
    UwProductVO pvo = getUwPolicyDS().findUwProduct(underwriteId, itemId);
    productForm.setInitialType(pvo.getInitialType());
    productForm.setCountWay(pvo.getCountWay());
    productForm.setUnderwriteId(Long.valueOf(req.getParameter("underwriteId")));
    productForm.setAge1(pvo.getAge1() != null ? pvo.getAge1() : null);
    productForm.setAge2(pvo.getAge2() != null ? pvo.getAge2() : null);
    Long insurdedId = Long.valueOf(0);
    if ((req.getParameter("insured") != null)
        && (req.getParameter("insured").length() > 0)) {
      insurdedId = Long.valueOf(req.getParameter("insured"));
    }
    boolean firstInsured = insurdedId.longValue() == pvo.getInsured1()
        .longValue() ? true : false;
    productForm.setItemId(Long.valueOf(req.getParameter("itemIds")));
    if ((req.getParameter("policyId") != null)
        && (req.getParameter("policyId").length() > 0)) {
      productForm.setPolicyId(Long.valueOf(req.getParameter("policyId")));
    }
    if (firstInsured) {
      productForm.setInsured1(insurdedId);
    } else {
      productForm.setInsured2(insurdedId);
    }
    if ((req.getParameter("coverAgeYear") != null)
        && (req.getParameter("coverAgeYear").length() > 0)) {
      productForm.setCoverageYear(Integer.valueOf(req
          .getParameter("coverAgeYear")));
    }
    if ((req.getParameter("chargeYear") != null)
        && (req.getParameter("chargeYear").length() > 0)) {
      productForm
          .setChargeYear(Integer.valueOf(req.getParameter("chargeYear")));
    }
    if ((req.getParameter("productId") != null)
        && (req.getParameter("productId").length() > 0)) {
      productForm.setProductId(Integer.valueOf(req.getParameter("productId")));
    }
    if ((req.getParameter("underwriterId") != null)
        && (req.getParameter("underwriterId").length() > 0)) {
      productForm.setUnderwriterId(Long.valueOf(req
          .getParameter("underwriterId")));
    }
    if ((req.getParameter("commenceDate") != null)
        && (req.getParameter("commenceDate").length() > 0)) {
      productForm.setValidateDate(DateUtils.toDate(req
          .getParameter("commenceDate")));
    }
    String advantageIND = req.getParameter("advantageIND");
    if ((advantageIND != null) && (advantageIND.length() > 0)) {
      if (firstInsured) {
        productForm.setAdvantageIndi1(advantageIND);
      } else {
        productForm.setAdvantageIndi2(advantageIND);
      }
    }
    return productForm;
  }

  @Resource(name = CoverageCI.BEAN_DEFAULT)
  private CoverageCI coverageCI;

  public CoverageCI getCoverageCI() {
    return coverageCI;
  }

  public void setCoverageCI(CoverageCI coverageCI) {
    this.coverageCI = coverageCI;
  }

  @Resource(name = ProductService.BEAN_DEFAULT)
  private ProductService productService;

  public ProductService getProductService() {
    return productService;
  }

  public void setProductService(ProductService productService) {
    this.productService = productService;
  }
  
  @Resource(name = ExtraLoadingActionHelper.BEAN_DEFAULT)
  ExtraLoadingActionHelper extraLoadingActionHelper;
  
  @Resource(name = PolicyService.BEAN_DEFAULT)
  PolicyService policyService;
}