package com.ebao.ls.liaRoc.ctrl;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.UserTransaction;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.streaming.SXSSFCell;
import org.apache.poi.xssf.streaming.SXSSFRow;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;
import org.hibernate.Session;

import com.ebao.foundation.module.db.DBean;
import com.ebao.foundation.module.db.hibernate.HibernateSession3;
import com.ebao.foundation.module.web.action.ExceptionInfoUtils;
import com.ebao.ls.cs.logger.ApplicationLogger;
import com.ebao.ls.foundation.dao.SQLBuilder;
import com.ebao.ls.foundation.dao.SQLBuilder.IgnoreEmpty;
import com.ebao.ls.foundation.dao.SQLBuilder.IgnoreNull;
import com.ebao.ls.foundation.dao.pager.PagerDaoHelper;
import com.ebao.ls.liaRoc.LiaRocCst;
import com.ebao.ls.pa.nb.ctrl.fileUpload.ProposalRuleCodeExcelUtil;
import com.ebao.ls.pa.nb.util.NBUtils;
import com.ebao.pub.framework.ExceptionFactory;
import com.ebao.pub.framework.GenericAction;
import com.ebao.pub.framework.RTException;
import com.ebao.pub.i18n.lang.CodeTable;
import com.ebao.pub.util.DateUtils;
import com.ebao.pub.util.Trans;
import com.tgl.tools.jaxb.adapter.date.TaiwanDateAdapter;

/**
 *  <h1>公會延遲報表上傳</h1><p>
 * @author Simon.Huang
 * @since 2022/05/19<p>
 */
public class UploadLiaRocDelayReportAction extends GenericAction {

	protected static final String ACTION_DOWNLOAD = "downloadData";
	protected static final String ACTION_UPLOAD = "uploadData";
	protected static final String ACTION_DELETE = "deleteData";
	
	private TaiwanDateAdapter twDateParser = new TaiwanDateAdapter();
	
	@Resource(name = ProposalRuleCodeExcelUtil.BEAN_DEFAULT)
	private ProposalRuleCodeExcelUtil proposalRuleCodeExcelUtil;

	private String SQL_GET_REPORT_ID = "select S_LIAROC_DELAY_REPORT__REPORT_.nextval as REPORT_ID from dual ";

	private String SQL_INSERT_REPORT = "insert into t_liaroc_delay_report (REPORT_ID, REPORT_NAME, LIAROC_TYPE) values (?, ?, ?)";

	private String SQL_INSERT_REPORT_DETAIL = "insert into t_liaroc_delay_report_detail "
			+ " (list_id, report_id, in_row_num, in_liaroc_type_ch, in_liaroc_company_ch, in_ins_name, in_ins_certi_code, in_ins_birthday, in_ins_sex_ch, in_liaroc_policy_code, in_liaroc_policy_type_ch, in_liaroc_product_category_ch, in_liaroc_product_type_ch, in_status_effect_date, in_relation_to_ph_ch, in_send_time, in_policy_code, in_sales_channel_ch, in_plan_code, in_apply_date, in_brbd_type_ch, in_validate_date, in_send_days, in_liaroc_type, in_liaroc_company, in_ins_sex, in_liaroc_policy_type, in_liaroc_product_category, in_liaroc_product_type, in_relation_to_ph, in_sales_channel, in_brbd_type)"
			+ " values (S_LIAROC_DELAY_REPORT_DETAIL__.nextval, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ";

	private String SQL_QUERY_REPORT_LIST = "SELECT A.REPORT_ID,A.REPORT_NAME,A.LIAROC_TYPE ,A.INSERT_TIMESTAMP, "
			+ " (SELECT COUNT(1) FROM T_LIAROC_DELAY_REPORT_DETAIL B WHERE B.REPORT_ID = A.REPORT_ID AND B.PROCESS_DONE= 'Y') AS Y_CNT, "
			+ " (SELECT COUNT(1) FROM T_LIAROC_DELAY_REPORT_DETAIL B WHERE B.REPORT_ID = A.REPORT_ID AND B.PROCESS_DONE= 'N') AS N_CNT, "
			+ " (SELECT COUNT(1) AS W FROM T_LIAROC_DELAY_REPORT_DETAIL B WHERE B.REPORT_ID = A.REPORT_ID ) AS DETAIL_CNT "
			+ " FROM T_LIAROC_DELAY_REPORT A ORDER BY REPORT_ID ";
	
	private String SQL_QUERY_REPORT = "SELECT A.* FROM T_LIAROC_DELAY_REPORT A WHERE a.REPORT_ID = ?  ";
	
	private String SQL_QUERY_REPORT_DETAIL = "SELECT A.REPORT_NAME,A.LIAROC_TYPE ,A.INSERT_TIMESTAMP,B.*,  "
			+ "to_char(IN_INS_BIRTHDAY, 'yyyy-mm-dd') AS IN_INS_BIRTHDAY_STR,"
			+ "to_char(IN_STATUS_EFFECT_DATE, 'yyyy-mm-dd') AS IN_STATUS_EFFECT_DATE_STR,"
			+ "to_char(OUT_APPLY_DATE, 'yyyy-mm-dd') AS OUT_APPLY_DATE_STR,"
			+ "to_char(OUT_FOA_REG_DATE, 'yyyy-mm-dd') AS OUT_FOA_REG_DATE_STR,"
			+ "to_char(OUT_FOA_RECEIVE_DATE, 'yyyy-mm-dd') AS OUT_FOA_RECEIVE_DATE_STR,"
			+ "to_char(OUT_CHANNEL_RECEIVE_DATE, 'yyyy-mm-dd') AS OUT_CHANNEL_RECEIVE_DATE_STR "
			+ " FROM T_LIAROC_DELAY_REPORT A JOIN T_LIAROC_DELAY_REPORT_DETAIL B ON B.REPORT_ID = A.REPORT_ID "
			+ " WHERE a.REPORT_ID = ? ORDER BY IN_ROW_NUM ";
	
	private String SQL_DELETE_REPORT = "DELETE T_LIAROC_DELAY_REPORT A WHERE a.REPORT_ID = ?  ";
	
	private String SQL_DELETE_REPORT_DETAIL = "DELETE FROM T_LIAROC_DELAY_REPORT_DETAIL A WHERE a.REPORT_ID = ?  ";
	/** 每500筆commit 1次 */
	private final static int COMMIT_SIZE = 500;

	@Override
	public ActionForward process(ActionMapping mapping, ActionForm actionForm, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		UploadLiaRocDelayReportForm form = (UploadLiaRocDelayReportForm) actionForm;

		if (ACTION_DOWNLOAD.equals(form.getActionType())) {
			this.processBeforeDownload(form, response);
			this.processDownload(form, response);
			return null;
		} else if (ACTION_UPLOAD.equals(form.getActionType())) {
			this.processUploadFile(form, request);
		} else if (ACTION_DELETE.equals(form.getActionType())) {
			this.processDeleteFile(form, request);
		}

		this.queryReportList(request, form);

		return mapping.findForward("success");
	}

	private void processBeforeDownload(UploadLiaRocDelayReportForm form, HttpServletResponse response) throws Exception {
		DBean dbean = null;
		Connection conn = null;
		UserTransaction trans = null;
		PreparedStatement stmt = null;
		try {
			trans = Trans.getUserTransaction();
			trans.begin();
			dbean = new DBean();
			dbean.connect();
			conn = dbean.getConnection();
			stmt = conn.prepareCall("{call PKG_LS_LIAROC_DELAY_REPORT.P_EXECUTE_REPORT(?)}");
			stmt.setLong(1, form.getReportId());
			stmt.execute();
			trans.commit();
		} catch (Exception e) {
			if (trans != null) {
				trans.rollback();
			}
		} finally {
			if (dbean != null) {
				DBean.closeAll(null, stmt, dbean);
			}
		}
	}

	private void processDeleteFile(UploadLiaRocDelayReportForm form, HttpServletRequest request) throws Exception {

		DBean dbean = null;
		Connection conn = null;
		UserTransaction trans = null;
		PreparedStatement stmt = null;
		
		List<String> errorMsg = new ArrayList<String>();
		try {
			trans = Trans.getUserTransaction();
			trans.begin();

			dbean = new DBean();
			dbean.connect();
			conn = dbean.getConnection();
			

			stmt = conn.prepareStatement(SQL_DELETE_REPORT_DETAIL);
			stmt.setLong(1, form.getReportId());
			stmt.execute();
			stmt.close();
			
			stmt = conn.prepareStatement(SQL_DELETE_REPORT);
			stmt.setLong(1, form.getReportId());
			stmt.execute();
			
			trans.commit();
			request.setAttribute("resultMsg", NBUtils.getTWMsg("MSG_1250720"));
			
		} catch (Exception e) {
			if (trans != null) {
				trans.rollback();
			}
			if(errorMsg.isEmpty() == false) {
				request.setAttribute("resultMsg", ExceptionInfoUtils.getExceptionMsg(e));
			}
		} finally {
			if (dbean != null) {
				DBean.closeAll(null, stmt, dbean);
			}
		}
	
	}

	/**	
	 * <p>Description: 處理下傳檔案 </p>
	 * <p>Create Time: 2022/05/20</p>
	 * @author Simon Huang
	 * @param form
	 * @param request 
	 */
	private void processDownload(UploadLiaRocDelayReportForm form, HttpServletResponse response) throws Exception {

		OutputStream outputStream = null;
		SXSSFWorkbook workbook = null;//https://poi.apache.org/components/spreadsheet/

		Map<String,Object> reportData = queryReport(form.getReportId());
		String fileName = "查無報表" + ProposalRuleCodeExcelUtil.EXCEL_FILE_EXTENSION__XLSX;
		if(reportData != null) {
			fileName = reportData.get("REPORT_NAME") + "_" ;
			if(LiaRocCst.RECEIVE_LIAROC_TYPE_LIFE.equals(reportData.get("LIAROC_TYPE"))) {
				fileName+= NBUtils.getTWMsg("MSG_1257654") ;
			} else {
				fileName+= NBUtils.getTWMsg("MSG_107547") ;
			}
			fileName += ProposalRuleCodeExcelUtil.EXCEL_FILE_EXTENSION__XLSX ;	
		}
		
		try {
			outputStream = response.getOutputStream();
			workbook = new SXSSFWorkbook();
			response.setContentType(ProposalRuleCodeExcelUtil.EXCEL_CONTENT_TYPE);
			response.setHeader(ProposalRuleCodeExcelUtil.EXCEL_CONTENT_DISPOSITION,
					"attachment;filename=\"" + URLEncoder.encode(fileName,"UTF-8") + "\"");
			
			SXSSFSheet sheet = workbook.createSheet("sheet1");//create sheet
			
			CellStyle cellStyle  = workbook.createCellStyle();          
			DataFormat format = workbook.createDataFormat();
			cellStyle.setDataFormat(format.getFormat("yyyy-MM-dd HH:mm:ss"));
			
			int rowIdx = 0;
			int cellIdx = 0;
			if (sheet != null) {
				SXSSFRow headRow = sheet.createRow(rowIdx++);//create head row
				for (Column column : Column.values()) {
					SXSSFCell cell = headRow.createCell(cellIdx++);//create head cell
					cell.setCellType(CellType.STRING);
					cell.setCellValue(column.getDesc());
					sheet.setColumnWidth(cellIdx, column.getWidth());
				}
				
				if(reportData != null) {
					List<Map<String, Object>> detailList = queryReportDetailList(form.getReportId());

					for(Map<String,Object> rowData : detailList) {
						SXSSFRow xlsRow = sheet.createRow(rowIdx++);//create data row
						cellIdx = 0;
						for (Column column : Column.values()) {
							
							SXSSFCell cell = xlsRow.createCell(cellIdx++);//create data cell
							
							Object value = rowData.get(column.getColName()) ;
							if(value != null) {
								if(value instanceof Date) {
									cell.setCellStyle(cellStyle);
									cell.setCellValue((Date)value);	
								} else {
									cell.setCellValue(value.toString());	
								}
							} else {
								cell.setCellValue("");
							}
							
							sheet.setColumnWidth(cellIdx, column.getWidth());
						}
					}
				}
			}
			workbook.write(outputStream);
			outputStream.flush();
		} catch (Exception e) {
			ApplicationLogger.addLoggerData("[ERROR]Exception => " + ExceptionUtils.getStackTrace(e));
			throw ExceptionFactory.parse(e);
		} finally {
			if (outputStream != null) {
				outputStream.close();
			}
			if (workbook != null) {
				workbook.close();
			}
		}
	}

	/**	
	 * <p>Description: 處理上傳檔案 </p>
	 * <p>Create Time: 2022/05/20</p>
	 * @author Simon Huang
	 * @param form
	 * @param request 
	 */
	private void processUploadFile(UploadLiaRocDelayReportForm form, HttpServletRequest request) throws Exception {

		DBean dbean = null;
		Connection conn = null;
		UserTransaction trans = null;
		PreparedStatement stmt = null;
		
		List<String> errorMsg = new ArrayList<String>();
		try {
			boolean isFileEmpty = false;
			boolean isFileError = false;
			FormFile formFile = form.getUploadFile();

			isFileEmpty = proposalRuleCodeExcelUtil.checkFileEmpty(formFile);
			if (!isFileEmpty)
				isFileError = proposalRuleCodeExcelUtil.checkFileFormat(formFile);

			if (!isFileEmpty && !isFileError) {
				InputStream inputStream = formFile.getInputStream();
				Workbook workbook = WorkbookFactory.create(inputStream);
				Sheet sheet = workbook.getSheetAt(0);

				isFileEmpty = proposalRuleCodeExcelUtil.checkSheetEmpty(sheet);

				if (!isFileEmpty) {

					int count = 0;

					trans = Trans.getUserTransaction();
					trans.begin();

					dbean = new DBean();
					dbean.connect();
					conn = dbean.getConnection();
					Long reportId = insertReport(conn, form.getReportName(), form.getLiaRocType());

					stmt = conn.prepareStatement(SQL_INSERT_REPORT_DETAIL);

					for (int rowIdx = 1; rowIdx <= sheet.getLastRowNum(); rowIdx++) {

						Row row = sheet.getRow(rowIdx);

						int index = 1;

						//設定LIST_ID,ROW_NUM
						stmt.setLong(index++, reportId);//report_id, 
						stmt.setLong(index++, rowIdx);//in_row_num, 

						//設定Excel轉入資料
						List<String> cellValues = new ArrayList<String>();
						boolean isEmptyRow = false;
						for (int cellIdx = 0; cellIdx < row.getLastCellNum(); cellIdx++) {
							Cell cell = row.getCell(cellIdx);
							
							String cellValue = proposalRuleCodeExcelUtil.processUploadCellStringValue(cell);
							
							if(cellIdx == 0 && StringUtils.isEmpty(cellValue)) {
								isEmptyRow = true;
								break;
							}
								
							if (StringUtils.isEmpty(cellValue)) {
								stmt.setNull(index++, Types.NULL);
							} else {
								if (cellIdx == 12) {
									//通報日期
									//2022-01-05T13:02:04.269+08:00
									try {
									
										Date date = DateUtils.toDate(cellValue.substring(0, 23), "yyyy-MM-dd'T'hh:mm:ss.S");
										Timestamp timstamp = new Timestamp(date.getTime());
										stmt.setTimestamp(index++, timstamp);
									} catch(Exception e) {
										errorMsg.add("第" + rowIdx + "列,通報日期[" + cellValue + "]格式有誤 !");
									}
								} else if (cellIdx == 4 || cellIdx == 10 || cellIdx == 16 || cellIdx == 18) {
									//被保人出生日期
									//保單狀況生效日期
									//要保書填寫日期
									//契約生效日期
									Date date = parseCellDate(errorMsg, cellValue);
									if(date == null) {
										stmt.setNull(index++, Types.NULL);
									} else {
										java.sql.Date sqlDate = new java.sql.Date(date.getTime());
										stmt.setDate(index++, sqlDate);
									}
								} else if (cellIdx == 19) {//IN_SEND_DAYS
									stmt.setLong(index++, Long.valueOf(cellValue));
								} else {
									stmt.setString(index++, cellValue);
								}
							}
							cellValues.add(cellValue);
						}

						if(isEmptyRow == true) {
							continue;
						}
						stmt.setString(index++, form.getLiaRocType());//in_liaroc_type, 
						stmt.setString(index++, "64");

						//設定相關中文轉代碼
						String inInsSex = cellValues.get(5);//in_ins_sex, 
						String inLiarocPolicyType = cellValues.get(7);//in_liaroc_policy_type, 
						String inLiarocProductCategory = cellValues.get(8);//in_liaroc_product_category, 
						String inLiarocProductType = cellValues.get(9);//in_liaroc_product_type, 
						String inRelationToPh = cellValues.get(11);//in_relation_to_ph, 
						String inSalesChannel = cellValues.get(14);//in_sales_channel, 
						String inBrbdType = cellValues.get(17);//in_brbd_type

						stmt.setString(index++, getDelayReportCode(errorMsg,"GENDER", inInsSex));
						stmt.setString(index++, getDelayReportCode(errorMsg,"LIAROC_POLICY_TYPE", inLiarocPolicyType));
						stmt.setString(index++, getDelayReportCode(errorMsg,"LIAROC_PRODUCT_CATEGORY", inLiarocProductCategory));
						stmt.setString(index++, getDelayReportCode(errorMsg,"LIAROC_PRODUCT_TYPE", inLiarocProductType));
						stmt.setString(index++, getDelayReportCode(errorMsg,"LIAROC_RELATION_TO_PH", inRelationToPh));
						stmt.setString(index++, getDelayReportCode(errorMsg,"LIAROC_SALES_CHANNEL", inSalesChannel));
						stmt.setString(index++, getDelayReportCode(errorMsg,"LIAROC_BRBD_TYPE", inBrbdType));
						
						if(errorMsg.isEmpty()) {
							stmt.addBatch();
							if (count % COMMIT_SIZE == 0) {
								stmt.executeBatch();
							}
						}
					}

					if(errorMsg.isEmpty()) {
						stmt.executeBatch();
						trans.commit();
					} else {
						trans.rollback();
					}
				}
			}
			
			if (isFileError) {
				request.setAttribute("resultMsg", NBUtils.getTWMsg("MSG_1251005"));//文件須為EXCEL格式
			} else if (isFileEmpty) {
				request.setAttribute("resultMsg", NBUtils.getTWMsg("MSG_1265856"));//選擇檔案不可為空白，請確認
			}
		} catch (Exception e) {
			if (trans != null) {
				trans.rollback();
			}
			//throw e;
			errorMsg.add(ExceptionInfoUtils.getExceptionMsg(e));
		} finally {
			if (dbean != null) {
				DBean.closeAll(null, stmt, dbean);
			}
		}
		
		if(errorMsg.isEmpty() == false) {
			request.setAttribute("resultMsg", StringUtils.join(errorMsg, "\n"));
		}
	}


	private Date parseCellDate(List<String> errorMsg,String cellValue) {
		Date date = null;
		if(StringUtils.isNotEmpty(cellValue)) {
			String strDate = cellValue.replaceAll("-", "").replaceAll("/", "");
			if(strDate.length() == 8) {
				if(strDate.indexOf("0") == 0) {
					//8碼民國年
					try {
						date = twDateParser.unmarshal(strDate);
					} catch (Exception e) {
						date = null;
					}
				} else {
					date = DateUtils.toDate(strDate, "yyyyMMdd");
				}
			}
		}
		
		if(date == null) {
			String error = "日期欄位無法析解格式=" + cellValue;
			if(errorMsg.contains(error) == false) {
				errorMsg.add(error);
			}
		}
		return date;
	}

	private String getDelayReportCode(List<String> errorMsg, String relCode, String relValue) {
		String value = "";
		if (StringUtils.isNotEmpty(relValue)) {
			value = CodeTable.getCodeById("V_LIAROC_DELAY_REPORT_CODE", relCode + "-" + relValue);
			if (StringUtils.isEmpty(value)) {
				String error = "查無對應資料=" + relCode + "-" + relValue;
				if(errorMsg.contains(error) == false) {
					errorMsg.add(error);
				}
			} else if ("NULL".equals(value)) {
				value = "";
			}
		}
		return value;
	}

	private Map<String, Object> queryReport(Long reportId) {
		if(reportId != null) {
			SQLBuilder builder = new SQLBuilder(new StringBuffer(), IgnoreNull.TRUE, IgnoreEmpty.TRUE);
			builder.write(SQL_QUERY_REPORT);
			builder.getParams().add(reportId);
			Session s = HibernateSession3.currentSession();
			List<Map<String, Object>> reportList = PagerDaoHelper.paging(s, builder, null);
			if(reportList.size() > 0) {
				return reportList.get(0);
			}
		}
		return null;
	}
	
	private void queryReportList(HttpServletRequest request, UploadLiaRocDelayReportForm form) {
		SQLBuilder builder = new SQLBuilder(new StringBuffer(), IgnoreNull.TRUE, IgnoreEmpty.TRUE);
		builder.write(SQL_QUERY_REPORT_LIST);
		Session s = HibernateSession3.currentSession();
		List<Map<String, Object>> reportList = PagerDaoHelper.paging(s, builder, form.getPager());
		request.setAttribute("reportList", reportList);
	}
	
	private List<Map<String, Object>> queryReportDetailList(Long reportId) {
		SQLBuilder builder = new SQLBuilder(new StringBuffer(), IgnoreNull.TRUE, IgnoreEmpty.TRUE);
		builder.write(SQL_QUERY_REPORT_DETAIL);
		builder.getParams().add(reportId);
		Session s = HibernateSession3.currentSession();
		List<Map<String, Object>> detailList = PagerDaoHelper.paging(s, builder, null); //no count;
		return detailList ;
	}
	
	private Long insertReport(Connection conn, String reportName, String liarocType) {

		Long returnValue = null;

		PreparedStatement ps = null;
		StringBuffer sql = new StringBuffer();

		try {
			Long reportId = this.getReportId(conn);

			sql.append(SQL_INSERT_REPORT);

			ps = conn.prepareStatement(sql.toString());

			ps.setLong(1, reportId);
			ps.setString(2, reportName);
			ps.setString(3, liarocType);

			int rows = ps.executeUpdate();
			if (rows > 0) {
				returnValue = reportId;
			}
		} catch (SQLException e) {
			throw new RTException(e);
		}
		return returnValue;
	}

	private Long getReportId(Connection conn) {
		Long reportId = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer sql = new StringBuffer();
		try {

			sql.append(SQL_GET_REPORT_ID);

			ps = conn.prepareStatement(sql.toString());
			rs = ps.executeQuery();
			while (rs.next()) {
				reportId = rs.getLong("REPORT_ID");
				break;
			}
			rs.close();
			ps.close();
		} catch (SQLException e) {
			throw new RTException(e);
		}
		return reportId;
	}
	
	public enum Column {
		COLUMN010("季別","REPORT_NAME",2*600),
		COLUMN020("ROW_INDEX","IN_ROW_NUM",2*600),
		COLUMN030("報表分類","LIAROC_TYPE",3*600),
		COLUMN040("產壽業別","IN_LIAROC_TYPE_CH",2*600),
		COLUMN050("公司名稱","IN_LIAROC_COMPANY_CH",3*600),
		COLUMN060("被保險人姓名","IN_INS_NAME",5*600),
		COLUMN070("被保險人身分證號碼","IN_INS_CERTI_CODE",5*600),
		COLUMN080("被保險人出生日期","IN_INS_BIRTHDAY_STR",5*600),
		COLUMN090("被保險人性別","IN_INS_SEX_CH",3*600),
		COLUMN100("保單號碼","IN_LIAROC_POLICY_CODE",5*600),
		COLUMN110("保單分類","IN_LIAROC_POLICY_TYPE_CH",3*600),
		COLUMN120("險種分類","IN_LIAROC_PRODUCT_CATEGORY_CH",3*600),
		COLUMN130("險種","IN_LIAROC_PRODUCT_TYPE_CH",5*600),
		COLUMN140("保單狀況生效日期","IN_STATUS_EFFECT_DATE_STR",3*600),
		COLUMN150("通報時間","IN_SEND_TIME",10*600),
		COLUMN160("要保人與被保險人關係","IN_RELATION_TO_PH_CH",10*600),
		COLUMN170("UPLOAD_LIST_ID","OUT_UPLOAD_LIST_ID",3*600),
		COLUMN180("ITEM_ID","OUT_ITEM_ID",3*600),
		COLUMN190("POLICY_CHG_ID","OUT_POLICY_CHG_ID",3*600),
		COLUMN200("CHANGE_ID","OUT_CHANGE_ID",3*600),
		COLUMN210("通報模組","OUT_BIZ_SOURCE",2*600),
		COLUMN220("通報模組說明","OUT_BIZ_SOURCE_CH",10*600),
		COLUMN230("要保書填寫日","OUT_APPLY_DATE_STR",10*600),
		COLUMN240("要保書狀態","OUT_PROPOSAL_STATUS_CH",10*600),
		COLUMN250("FOA填寫日","OUT_FOA_REG_DATE_STR",10*600),
		COLUMN260("FOA受理日","OUT_FOA_RECEIVE_DATE_STR",10*600),
		COLUMN270("FOA送件日","OUT_FOA_SEND_DATE",10*600),
		COLUMN280("FOA受理人姓名","OUT_FOA_ACEPT_USER_NAME",10*600),
		COLUMN290("通路受理日","OUT_CHANNEL_RECEIVE_DATE_STR",10*600),
		COLUMN300("受理收件確認日","OUT_CHANNEL_CONFIRM_DATE",10*600),
		COLUMN310("通報保單號碼","OUT_POLICY_CODE",10*600),
		COLUMN320("受理編號","OUT_SERIAL_NUM",10*600),
		COLUMN330("險種代號","OUT_PLAN_CODE",10*600),
		COLUMN340("通路","OUT_CHANNEL_TYPE",10*600),
		COLUMN350("業務員單位代號","OUT_CHANNEL_CODE",10*600),
		COLUMN360("業務員單位名稱","OUT_CHANNEL_NAME",10*600),
		COLUMN370("業務員1登錄證號","OUT_REGISTER_CODE_1",10*600),
		COLUMN380("業務員1姓名","OUT_AGENT_NAME_1",10*600),
		COLUMN390("業務員2登錄證號","OUT_REGISTER_CODE_2",10*600),
		COLUMN400("業務員2姓名","OUT_AGENT_NAME_2",10*600),
		COLUMN410("通報公會日期時間","OUT_SEND_TIME",10*600),
		COLUMN420("RQUID","OUT_RQUID",10*600),
		COLUMN430("RQUID_TIME","OUT_RQUID_TIME",10*600),
		COLUMN440("有重覆通報","OUT_IS_DUPLICATED",10*600),
		COLUMN450("目前同要被保險人","OUT_IS_SAME_HOLDER",10*600),
		COLUMN460("要保人ID","OUT_HOLDER_CERTI_CODE",10*600),
		COLUMN470("目前保項狀態","OUT_ITEM_LIABILITY_STATE_CH",10*600),
		COLUMN480("送件單位名稱","OUT_SEND_CHANNEL_NAME",10*600),
		COLUMN490("送件單位代號","OUT_SEND_CHANNEL_CODE",10*600),
		COLUMN500("送件方式","OUT_SEND_TYPE_DESC",10*600),
		COLUMN510("受理方式","OUT_ACEPT_TYPE_DESC",10*600),
		COLUMN520("承辦人員","OUT_ACEPT_USER_NAME",10*600),
		COLUMN530("有UNDO作業","OUT_IS_UNDO",5*600),
		COLUMN540("UNDO日期","OUT_UNDO_DATE",10*600),
		COLUMN550("有公會偵錯","OUT_HAS_LIAROC_ERROR",5*600),
		COLUMN560("通報代碼有1215","OUT_HAS_1215",5*600),
		COLUMN570("備註","NOTE",10*600);
		
		private String colName;
		private String desc;
		private Integer width;//1中文字寬600，視為1單位

		Column(String desc, String colName, Integer width) {
			this.colName = colName;
			this.desc = desc;
			this.width = width;
		}

		public String getColName() {
			return colName;
		}
		public void setColName(String name) {
			this.colName = name;
		}
		public String getDesc() {
			return desc;
		}
		public void setDesc(String desc) {
			this.desc = desc;
		}
		public Integer getWidth() {
			return width;
		}
		public void setWidth(Integer width) {
			this.width = width;
		}
	}
	
}
