package com.ebao.ls.crs.customer.impl;

import java.util.List;

import javax.annotation.Resource;

import com.ebao.foundation.commons.exceptions.GenericException;
import com.ebao.ls.crs.customer.CRSIndividualLogService;
import com.ebao.ls.crs.data.bo.CRSIndividualLog;
import com.ebao.ls.crs.data.bo.CRSPartyLog;
import com.ebao.ls.crs.data.customer.CRSIndividualLogDAO;
import com.ebao.ls.crs.data.identity.CRSPartyLogDAO;
import com.ebao.ls.crs.identity.CRSPartyLogService;
import com.ebao.ls.crs.vo.CRSIndividualLogVO;
import com.ebao.ls.crs.vo.CRSPartyLogVO;
import com.ebao.ls.foundation.service.GenericServiceImpl;
import com.ebao.ls.pty.ci.EmployeeCI;
import com.ebao.ls.pub.cst.YesNo;

public class CRSIndividualLogServiceImpl
		extends GenericServiceImpl<CRSIndividualLogVO, CRSIndividualLog, CRSIndividualLogDAO>
		implements CRSIndividualLogService {
	
	@Resource(name=CRSPartyLogDAO.BEAN_DEFAULT)
	private CRSPartyLogDAO partyLogDAO;
	
	@Resource(name = CRSPartyLogService.BEAN_DEFAULT)
	private CRSPartyLogService crsPartyLogService;
	
	@Override
	protected CRSIndividualLogVO newEntityVO() {
		return new CRSIndividualLogVO();
	}

	@Override
	public List<CRSIndividualLogVO> findByPartyLogId(Long partyLogId, boolean loadOtherTable) {
		List<CRSIndividualLog> individualLogList = this.dao.findByPartyLogId(partyLogId);
		return convertToVOList(individualLogList, loadOtherTable);
	}

	@Override
	public CRSIndividualLogVO save(CRSIndividualLogVO logVO) throws GenericException {
		CRSIndividualLogVO individualVO = super.save(logVO);
		
		CRSPartyLog partyLog = null;
		CRSIndividualLogVO reloadVO = this.load(individualVO.getLogId());
		if(reloadVO.getCrsPartyLog() != null ) {
			partyLog = partyLogDAO.load(reloadVO.getCrsPartyLog().getLogId());
		}
		if(logVO.getSigDate() != null && partyLog != null) {
			partyLog.setIsIndividualReady(YesNo.YES_NO__YES);
		}else {
			partyLog.setIsIndividualReady(YesNo.YES_NO__NO);
		}
		if(partyLog != null) {
			partyLogDAO.saveorUpdate(partyLog);
			CRSPartyLogVO partyLogVO = new CRSPartyLogVO();
			partyLog.copyToVO(partyLogVO, Boolean.TRUE);
			individualVO.setCrsPartyLog(partyLogVO);
		}
		return individualVO;
	}

	/**
	 * PCR 257607 330030 e-Approval ITR-1801051_CRS共同申報及盡職審查專案- 保全變更及CRS確認申報國作業
	 */
	@Override
	public List<CRSIndividualLogVO> findByChangeId(Long changeId ) {
		List<CRSIndividualLog> individualLogList = this.dao.findByChangeId(changeId);
		return convertToVOList(individualLogList);
	}
	

}
