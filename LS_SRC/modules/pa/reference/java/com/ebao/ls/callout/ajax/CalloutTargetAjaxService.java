package com.ebao.ls.callout.ajax;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;

import com.ebao.foundation.module.util.i18n.CodeTable;
import com.ebao.ls.callout.bs.CalloutOnlineService;
import com.ebao.ls.callout.data.CalloutTransDao;
import com.ebao.ls.cs.commonflow.ds.ApplicationService;
import com.ebao.ls.cs.commonflow.vo.ApplicationVO;
import com.ebao.ls.pa.nb.bs.rule.ProposalRuleMsgService;
import com.ebao.ls.pa.nb.util.NBUtils;
import com.ebao.ls.pa.nb.vo.ProposalRuleMsgVO;
import com.ebao.ls.pa.pty.service.CertiCodeCI;
import com.ebao.ls.pa.pub.bs.LegalRepresentativeService;
import com.ebao.ls.pa.pub.data.bo.AgentNotify;
import com.ebao.ls.pa.pub.data.org.AgentNotifyDao;
import com.ebao.ls.pa.pub.model.InsuredInfo;
import com.ebao.ls.pa.pub.model.PolicyHolderInfo;
import com.ebao.ls.pa.pub.model.PolicyInfo;
import com.ebao.ls.pa.pub.service.PolicyService;
import com.ebao.ls.pa.pub.vo.LegalRepresentativeVO;
import com.ebao.ls.pub.CodeCst;
import com.ebao.ls.pub.ajax.AjaxService;
import com.ebao.ls.pub.cst.InsuredCategory;
import com.ebao.ls.pub.cst.NBInsuredCategory;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.i18n.lang.StringResource;
import com.ebao.pub.util.DateUtils;

public class CalloutTargetAjaxService implements AjaxService<Map<String, Object>, Map<String, Object>> {

	private static final String MEMO = "memo";

	private static final String RESULT = "result";

	private static final String CONTACT_NUM6 = "contactNum6";

	private static final String CONTACT_NUM5 = "contactNum5";

	private static final String CONTACT_NUM4 = "contactNum4";

	private static final String CONTACT_NUM3 = "contactNum3";

	private static final String CONTACT_NUM2 = "contactNum2";

	private static final String CONTACT_NUM1 = "contactNum1";

	private static final String EXPECTED_CALLOUT_TIME_OTHER = "expectedCalloutTimeOther";

	private static final String EXPECTED_CALLOUT_TIME = "expectedCalloutTime";

	private static final int HOLDER = 1;

	private static final int INSURED = 2;

	private static final int LEGAL_REPRESENTATIVE = 3;

	private static final int POLICY_BENEFICIARY = 4;

	private static final int ORG_POLICY_HOLDER = 5;

	private static final int OTHER = 6;

	private static final int SPOUSE = 7;

	private static final int MAIN_INSURED = 13;

	private static final int PAYER = 14;

	private static final int AUTHORIZER = 15;

	private static final int CHILD5 = 12;

	private static final int CHILD4 = 11;

	private static final int CHILD3 = 10;

	private static final int CHILD2 = 9;

	private static final int CHILD1 = 8;

	public static final String BEAN_DEFAULT = "calloutTargetAjax";

	public static final String NEW_TARGET = "newTarget";

	public static final String EXIST_TARGET = "existTarget";

	public static final String CHECK_CERTI_CODE = "checkCertiCode";

	private static final String CHECK_CALLOUT_REASON = "checkCalloutReason";

	@Resource(name = PolicyService.BEAN_DEFAULT)
	private PolicyService policyService;

	@Resource(name = CalloutTransDao.BEAN_DEFAULT)
	private CalloutTransDao calloutTransDao;

	@Resource(name = AgentNotifyDao.BEAN_DEFAULT)
	private AgentNotifyDao<AgentNotify> agentNotifyDao;

	@Resource(name = LegalRepresentativeService.BEAN_DEFAULT)
	private LegalRepresentativeService legalRepresentativeService;
	
	@Resource(name = ApplicationService.BEAN_DEFAULT)
	private ApplicationService applicationService;

	@Resource(name = CertiCodeCI.BEAN_DEFAULT)
	private CertiCodeCI certiCodeCI;

	@Resource(name = ProposalRuleMsgService.BEAN_DEFAULT)
	protected ProposalRuleMsgService proposalRuleMsgService;

	@Resource(name = CalloutOnlineService.BEAN_DEFAULT)
	protected CalloutOnlineService calloutOnlineService;

	@Override
	public Map<String, Object> execute(Map<String, Object> in) {
		String ajaxType = (String) in.get("ajaxType");
		if (NEW_TARGET.equals(ajaxType)) {
			return getNewTargetInfo(in);
		} else if (EXIST_TARGET.equals(ajaxType)) {
			return getExistTarget(in);
		} else if (CHECK_CERTI_CODE.equals(ajaxType)) {
			return checkCertiCode(in);
		} else if (CHECK_CALLOUT_REASON.equals(ajaxType)) {
			return checkCalloutReason(in);
		} else {
			return new HashMap<String, Object>();
		}
	}

	public Map<String, Object> getExistTarget(Map<String, Object> in) {
		Map<String, Object> outMap = new HashMap<String, Object>();
		try {
			if ("".equals(in.get("callerId"))) {
				outMap.put("callerName", "");
			} else {
				outMap.put("callerName", CodeTable.getCodeById("V_USER", in.get("callerId").toString()));
			}
			if ("".equals(in.get("type"))) {
				return outMap;
			}
			Long policyid = policyService.getPolicyIdByPolicyNumber((String) in.get("policyCode"));
			PolicyInfo policy = policyService.load(policyid);
			Integer type = Integer.parseInt((String) in.get("type"));
			switch (type) {
			case 1:
				if (policy != null && policy.getPolicyHolder() != null
						&& policy.getPolicyHolder().getAddress() != null) {
					outMap.put("address", policy.getPolicyHolder().getAddress().getAddress1() == null ? ""
							: policy.getPolicyHolder().getAddress().getAddress1());
				} else {
					outMap.put("address", "");
				}
				break;
			case 2:
				List<InsuredInfo> insuredInfos = policy.getInsureds();
				for (InsuredInfo insured : insuredInfos) {
					if (insured != null && insured.getInsuredCategory().equals(InsuredCategory.INSURED_CATGORY_SELF)) {
						if (insured.getAddress() != null) {
							outMap.put("address", insured.getAddress().getAddress1() == null ? ""
									: insured.getAddress().getAddress1());
						} else {
							outMap.put("address", "");
						}
					}
				}
				break;
			case 3:
				outMap.put("address", "");
				break;
			default:
				outMap.put("address", "");
				break;
			}
		} catch (Exception e) {
			outMap.put("address", "");
		}
		return outMap;
	}

	/**
	 * 設定業報書電訪連絡電話1/2/3/4 對應要/被保人
	 * 
	 * @param agentNotify
	 * @param type
	 * @return
	 */
	private Map<String, Object> getAgentNotify(AgentNotify agentNotify, Integer type) {

		Map<String, Object> outMap = new HashMap<String, Object>();

		if (agentNotify == null) {
			outMap.put(MEMO, "");// 備註 = 要/被保險人聯絡電話2
			outMap.put(CONTACT_NUM3, "");// 聯絡電話1 = 要/被保險人手機1
			outMap.put(CONTACT_NUM4, "");// 聯絡電話2 = 要/被保險人手機2
			outMap.put(CONTACT_NUM5, "");// 聯絡電話3 = 要/被保險人聯絡電話1
			outMap.put(CONTACT_NUM6, "");// 聯絡電話4 = 要/被保險人聯絡電話2
			return outMap;
		}
		StringBuilder sb = null;
		switch (type) {
		case HOLDER: // 要保人
			// 聯絡電話1 = 要/被保險人手機1
			outMap.put(CONTACT_NUM3, agentNotify.getHolderMobile() == null ? "" : agentNotify.getHolderMobile());
			// 聯絡電話2 = 要/被保險人手機2
			outMap.put(CONTACT_NUM4, agentNotify.getHolderMobile2() == null ? "" : agentNotify.getHolderMobile2());
			// 聯絡電話3 = 要/被保險人聯絡電話1
			sb = new StringBuilder();
			sb.append(agentNotify.getHolderTel() == null ? ""
					: StringUtils.stripToEmpty(agentNotify.getHolderTelReg()) + agentNotify.getHolderTel());
			if (!StringUtils.isBlank(agentNotify.getHolderTelExt())) {
				sb.append("#").append(StringUtils.stripToEmpty(agentNotify.getHolderTelExt()));
			}
			outMap.put(CONTACT_NUM5, sb.toString());
			// 聯絡電話4 = 要/被保險人聯絡電話2
			sb = new StringBuilder();
			sb.append(agentNotify.getHolderTel2() == null ? ""
					: StringUtils.stripToEmpty(agentNotify.getHolderTel2Reg()) + agentNotify.getHolderTel2());
			if (!StringUtils.isBlank(agentNotify.getHolderTel2Ext())) {
				sb.append("#").append(StringUtils.stripToEmpty(agentNotify.getHolderTel2Ext()));
			}
			outMap.put(CONTACT_NUM6, sb.toString());
			outMap.put(EXPECTED_CALLOUT_TIME,
					agentNotify.getHolderTelTime() == null ? "" : agentNotify.getHolderTelTime()); // 指定電訪時段
			outMap.put(EXPECTED_CALLOUT_TIME_OTHER,
					agentNotify.getHolderTelTimeOthers() == null ? "" : agentNotify.getHolderTelTimeOthers()); // 指定電訪時段-其它
			break;
		case MAIN_INSURED: // 主被保險人
			// 聯絡電話1 = 要/被保險人手機1
			outMap.put(CONTACT_NUM3, agentNotify.getHolderMobile() == null ? "" : agentNotify.getInsuMobile());
			// 聯絡電話2 = 要/被保險人手機2
			outMap.put(CONTACT_NUM4, agentNotify.getHolderMobile2() == null ? "" : agentNotify.getInsuMobile2());
			// 聯絡電話3 = 要/被保險人聯絡電話1
			sb = new StringBuilder();
			sb.append(agentNotify.getInsuTel() == null ? ""
					: StringUtils.stripToEmpty(agentNotify.getHolderTelReg()) + agentNotify.getInsuTel());
			if (!StringUtils.isBlank(agentNotify.getInsuTelExt())) {
				sb.append("#").append(StringUtils.stripToEmpty(agentNotify.getInsuTelExt()));
			}
			outMap.put(CONTACT_NUM5, sb.toString());
			// 聯絡電話4 = 要/被保險人聯絡電話2
			sb = new StringBuilder();
			sb.append(agentNotify.getInsuTel2() == null ? ""
					: StringUtils.stripToEmpty(agentNotify.getInsuTel2Reg()) + agentNotify.getInsuTel2());
			if (!StringUtils.isBlank(agentNotify.getInsuTel2Ext())) {
				sb.append("#").append(StringUtils.stripToEmpty(agentNotify.getInsuTel2Ext()));
			}
			outMap.put(CONTACT_NUM6, sb.toString());
			outMap.put(EXPECTED_CALLOUT_TIME, agentNotify.getInsuTelTime() == null ? "" : agentNotify.getInsuTelTime()); // 指定電訪時段
			outMap.put(EXPECTED_CALLOUT_TIME_OTHER,
					agentNotify.getInsuTelTimeOthers() == null ? "" : agentNotify.getInsuTelTimeOthers()); // 指定電訪時段-其它

			break;
		case LEGAL_REPRESENTATIVE: // 法定代理人
		case POLICY_BENEFICIARY: // 受款人
		case ORG_POLICY_HOLDER: // 原要保人
		case AUTHORIZER: // 授權人
		case OTHER: // 其他
			outMap.put(CONTACT_NUM3, agentNotify.getLegalTel() == null ? "" : agentNotify.getLegalTel());// 聯絡電話1
			break;
		default:
			outMap.put(CONTACT_NUM3, "");
			outMap.put(CONTACT_NUM4, "");
			outMap.put(CONTACT_NUM5, "");
			outMap.put(CONTACT_NUM6, "");
		}

		return outMap;
	}

	public Map<String, Object> getNewTargetInfo(Map<String, Object> in) {
		try {
			Map<String, Object> outMap = new HashMap<String, Object>();

			Long policyId = policyService.getPolicyIdByPolicyNumber(MapUtils.getString(in, "policyCode"));

			if ("".equals(in.get("type"))) {
				return outMap;
			}
			boolean isFromNB = MapUtils.getBooleanValue(in, "isFromNB");

			Integer type = MapUtils.getInteger(in, "type");

			PolicyInfo policy = policyService.load(policyId);
			if (policy == null) {
				return outMap;
			}
			
			// POS-電訪頁面
			Date calloutDate = null;
			if(!isFromNB){
				// 保全輸入用客戶申請日
				String changeId = MapUtils.getString(in, "changeId");
				if (StringUtils.isNotEmpty(changeId)) {
					ApplicationVO applicationVO = applicationService.getApplication(Long.valueOf(changeId));
					calloutDate = applicationVO.getCustAppTime(); // 客戶申請日
				}else{
					// 電訪輸入用電訪日期
					calloutDate = DateUtils.toDate((String)in.get("calloutDate"));
				}
			}
			
			AgentNotify agentNotify = agentNotifyDao.load(policyId); // T_NB_AGENT_NOTIFY
			outMap = getAgentNotify(agentNotify, type);

			switch (type) {
			case HOLDER: // 要保人
				return emptyMapNull(getHolder(outMap, policy.getPolicyHolder(), isFromNB, calloutDate, 
						agentNotify != null ? agentNotify.getHolderTelTime() : null));

			case INSURED: // 被保險人
				// PCR 377520 ESP新增對象類型
			case SPOUSE: // 配偶
			case CHILD1: // 子女一
			case CHILD2: // 子女二
			case CHILD3: // 子女三
			case CHILD4: // 子女四
			case CHILD5: // 子女五(含以上)
			case MAIN_INSURED: // 主被保險人
				List<InsuredInfo> insuredInfos = policy.getInsureds();
				if (isFromNB) {
					for (InsuredInfo insured : insuredInfos) {
						// PCR 377520 依所選被保人帶入對應的資訊
						if (type == MAIN_INSURED) {
							// 若主被保險人=要保人,提示User選擇要保人
							if (InsuredCategory.INSURED_CATGORY_SELF.equals(insured.getInsuredCategory())) {
								if (CodeCst.RELATION_TO_PH_SELF == insured.getRelationToPH()) {
									outMap.put(RESULT, false);
									outMap.put("warnCause", "RelationToPH_SELF");
									return outMap;
								} else {
									// 主被保人
									outMap.put(RESULT, true);
									return emptyMapNull(getInsuredInfo(outMap, insured, isFromNB, calloutDate, 
											agentNotify != null ? agentNotify.getInsuTelTime() : null));
								}
							}
						} else if ((type == SPOUSE
								&& NBInsuredCategory.NB_INSURED_CATEGORY_WIFE.equals(insured.getNbInsuredCategory()))
								|| (type == CHILD1 && NBInsuredCategory.NB_INSURED_CATEGORY_CHILID_1
										.equals(insured.getNbInsuredCategory()))
								|| (type == CHILD2 && NBInsuredCategory.NB_INSURED_CATEGORY_CHILID_2
										.equals(insured.getNbInsuredCategory()))
								|| (type == CHILD3 && NBInsuredCategory.NB_INSURED_CATEGORY_CHILID_3
										.equals(insured.getNbInsuredCategory()))
								|| (type == CHILD4 && NBInsuredCategory.NB_INSURED_CATEGORY_CHILID_4
										.equals(insured.getNbInsuredCategory()))
								|| (type == CHILD5 && NBInsuredCategory.NB_INSURED_CATEGORY_CHILID_5
										.equals(insured.getNbInsuredCategory()))) {
							outMap.put(RESULT, true);
							return emptyMapNull(getInsuredInfo(outMap, insured, isFromNB, calloutDate, null));
						}
					}
					outMap.put(RESULT, true);
					return emptyMapNull(getInsuredInfo(outMap, null, isFromNB, calloutDate, null));
				} else {
					// 取出眾多被保人判斷最主要的欄位 InsuredCategory = "1".
					for (InsuredInfo insured : insuredInfos) {
						if (insured.getInsuredCategory().equals(InsuredCategory.INSURED_CATGORY_SELF)) {
							if (insured.getRelationToPH().equals(CodeCst.RELATION_TO_PH_SELF)) {
								outMap.put(RESULT, false);
								outMap.put("warnCause", "RelationToPH_SELF");
								return outMap;
							}
							outMap.put(RESULT, true);
							return emptyMapNull(getInsuredInfo(outMap, insured, isFromNB, calloutDate,
									agentNotify != null ? agentNotify.getInsuTelTime() : null));
						}
					}
				}

				return emptyMapNull(
						getInsuredInfo(outMap, null, isFromNB, calloutDate, agentNotify != null ? agentNotify.getInsuTelTime() : null));
			case PAYER: // 付款人
				outMap.put(RESULT, true);
				return emptyMapNull(outMap);
			case LEGAL_REPRESENTATIVE: // 法定代理人
			case POLICY_BENEFICIARY: // 受款人
			case ORG_POLICY_HOLDER: // 原要保人
			case AUTHORIZER: // 授權人
			case OTHER: // 其他
				outMap.put("relation1", ""); // 法代與要保人關係
				outMap.put("relation2", ""); // 法代與被保人關係
				outMap.put("expectedCalloutDate", ""); // 指定電訪日期 不用帶出
				outMap.put(EXPECTED_CALLOUT_TIME, ""); // 指定電訪時間

				outMap.put("calleeCertiCode", ""); // 身份證字號
				outMap.put("calleeName", ""); // 姓名
				outMap.put("calleeBirthday", ""); // 出生日期
				outMap.put(MEMO, ""); // 備註

				outMap.put("officeTelReg", ""); // 聯絡電話日區碼.
				outMap.put("officeTel", ""); // 聯絡電話日電話碼.
				outMap.put("officeTelExt", ""); // 聯絡電話日電話分機.
				outMap.put("homeTelReg", ""); // 聯絡電話夜區碼.
				outMap.put("homeTel", ""); // 聯絡電話夜電話碼.
				outMap.put("homeTelExt", ""); // 聯絡電話夜電話分機.
				outMap.put(CONTACT_NUM1, ""); // 手機1
				outMap.put(CONTACT_NUM2, ""); // 手機2
				outMap.put("address", "");

				outMap.put(RESULT, true);
				if (isFromNB) {
					// 設定 "姓名", "出生日期", "身份證字號"
					getLegalInfo(outMap, policy);
					// 設定 「與要/被保險人關係」的中文說明
					getRelationText(outMap, policy);
				}
				return emptyMapNull(outMap);
			default:
				break;
			}
		} catch (Exception e) {
			return new HashMap<String, Object>();
		}
		return new HashMap<String, Object>();
	}

	private Map<String, Object> getLegalInfo(Map<String, Object> outMap, PolicyInfo policy) {
		// 新契約件法定代理人只能約定/輸入同一個人
		List<LegalRepresentativeVO> list = legalRepresentativeService.findByPolicyId(policy.getPolicyId());
		if (!list.isEmpty()) {
			LegalRepresentativeVO vo = list.get(0);
			outMap.put("calleeBirthday",
					vo.getBirthDate() == null ? "" : DateUtils.date2String(vo.getBirthDate(), "yyyyMMdd")); // 出生日期
			outMap.put("calleeCertiCode", vo.getCertiCode()); // 身份證字號
			outMap.put("calleeName", vo.getName()); // 姓名
			outMap.put("calleeRomanName", StringUtils.defaultString(vo.getRomanName(), ""));// 姓名-羅馬拼音

		}
		return outMap;
	}

	private void getRelationText(Map<String, Object> outMap, PolicyInfo policy) {
		// 法代與要保人關係 - 若要保人實際年齡未滿20歲(要保書填寫日－生日)，顯示預收輸入「法定代理人」區塊的「與要/被保險人關係」的中文說明
		// PCR 433312 配合民法修正 112年1月1日未成年人由未滿 20足歲改為未滿 18足歲
		Date holderBirthday = policy.getPolicyHolder().getBirthDate();
		Date applyDate = policy.getApplyDate();
		boolean isGrownUp = NBUtils.isGrownUp(holderBirthday, applyDate);
		List<LegalRepresentativeVO> list = legalRepresentativeService.findByPolicyId(policy.getPolicyId());

		if (false == isGrownUp) {
			for (LegalRepresentativeVO vo : list) {
				if ("1".equals(vo.getRelToPhIns())) {
					outMap.put("relation1", CodeTable.getCodeDesc("V_NEW_REL_TO_PH_INS", vo.getNewRelToPhIns()));
					break;
				}
			}
		}
		// 法代與被保險人關係 -
		// 2.若各被保險人任一實際年齡未滿20歲(要保書填寫日－生日)或是否有監護宣告=Y，顯示預收輸入「法定代理人」區塊的「與要/被保險人關係」的中文說明
		List<InsuredInfo> insList = policy.getInsureds();
		for (InsuredInfo info : insList) {
			Date insuredBirthday = info.getBirthDate();
			String grdn = info.getGuardianAncmnt();
			int insuredAge = DateUtils.getAge(insuredBirthday, applyDate);
			if ((CodeCst.YES_NO__YES.equals(grdn)) || (insuredAge < 20)) {
				for (LegalRepresentativeVO vo : list) {
					if ("2".equals(vo.getRelToPhIns())) {
						outMap.put("relation2", CodeTable.getCodeDesc("V_NEW_REL_TO_PH_INS", vo.getNewRelToPhIns()));
						break;
					}
				}
			}
		}
	}

	private Map<String, Object> getInsuredInfo(Map<String, Object> outMap, InsuredInfo insured, boolean isFromNB, 
			Date calloutDate, String expectedCalloutTime) {
		if (insured == null) {
			outMap.put("relation1", "");
			outMap.put("relation2", "");
			outMap.put("expectedCalloutDate", "");
			outMap.put(EXPECTED_CALLOUT_TIME, "");
			outMap.put("calleeCertiCode", "");
			outMap.put("calleeName", "");
			outMap.put("calleeBirthday", "");
			outMap.put(MEMO, "");
			outMap.put("officeTelReg", "");
			outMap.put("officeTel", "");
			outMap.put("officeTelExt", "");
			outMap.put("homeTelReg", "");
			outMap.put("homeTel", "");
			outMap.put("homeTelExt", "");
			outMap.put(CONTACT_NUM1, "");
			outMap.put(CONTACT_NUM2, "");
			outMap.put("address", "");
			return outMap;
		}
		outMap.put(RESULT, true);
		// outMap.put("calloutTargetType", ""); //電訪對象類型
		outMap.put("relation1", ""); // 法代與要保人關係
		outMap.put("relation2", ""); // 法代與被保人關係
		outMap.put("expectedCalloutDate", ""); // 指定電訪日期 不用帶出
		outMap.put(EXPECTED_CALLOUT_TIME, StringUtils.defaultString(expectedCalloutTime, "")); // 指定電訪時間
		outMap.put("calleeCertiCode", StringUtils.defaultString(insured.getCertiCode(), "")); // 身份證字號

		outMap.put("calleeName", StringUtils.defaultString(insured.getName(), "")); // 姓名
		outMap.put("calleeRomanName", StringUtils.defaultString(insured.getRomanName(), ""));// 姓名-羅馬拼音
		outMap.put("calleeBirthday",
				insured.getBirthDate() == null ? "" : DateUtils.date2String(insured.getBirthDate(), "yyyyMMdd")); // 出生日期
		outMap.put(MEMO, ""); // 備註
		outMap.put("officeTelReg", StringUtils.defaultString(insured.getOfficeTelReg(), "")); // 聯絡電話日區碼.
		outMap.put("officeTel", StringUtils.defaultString(insured.getOfficeTel(), "")); // 聯絡電話日電話碼.
		outMap.put("officeTelExt", StringUtils.defaultString(insured.getOfficeTelExt(), "")); // 聯絡電話日電話分機.
		outMap.put("homeTelReg", StringUtils.defaultString(insured.getHomeTelReg(), "")); // 聯絡電話夜區碼.
		outMap.put("homeTel", StringUtils.defaultString(insured.getHomeTel(), "")); // 聯絡電話夜電話碼.
		outMap.put("homeTelExt", StringUtils.defaultString(insured.getHomeTelExt(), "")); // 聯絡電話夜電話分機.
		outMap.put(CONTACT_NUM1, StringUtils.defaultString(insured.getMobileTelephone(), "")); // 手機1
		outMap.put(CONTACT_NUM2, StringUtils.defaultString(insured.getMobileTel2(), "")); // 手機2
		outMap.put("address",
				insured.getAddress() == null ? "" : StringUtils.defaultString(insured.getAddress().getAddress1(), ""));
		checkValue(outMap, insured.getPolicyId(), insured.getBirthDate(), isFromNB, calloutDate);
		return outMap;
	}

	private Map<String, Object> emptyMapNull(Map<String, Object> map) {
		Set<String> set = map.keySet();
		for (String string : set) {
			if (map.get(string) == null) {
				map.put(string, "");
			}
		}
		return map;
	}

	private Map<String, Object> getHolder(Map<String, Object> outMap, PolicyHolderInfo holdervo, boolean isFromNB, 
			Date calloutDate, String expectedCalloutTime) {
		if (holdervo == null) {
			return outMap;
		}
		outMap.put(RESULT, true);
		// outMap.put("calloutTargetType", ""); //電訪對象類型
		outMap.put("relation1", ""); // 法代與要保人關係
		outMap.put("relation2", ""); // 法代與被保人關係
		outMap.put("expectedCalloutDate", ""); // 指定電訪日期
		outMap.put(EXPECTED_CALLOUT_TIME, StringUtils.defaultString(expectedCalloutTime, "")); // 指定電訪時間
		outMap.put("calleeCertiCode", StringUtils.defaultString(holdervo.getCertiCode(), "")); // 身份證字號
		outMap.put("calleeName", StringUtils.defaultString(holdervo.getName(), "")); // 姓名
		outMap.put("calleeRomanName", StringUtils.defaultString(holdervo.getRomanName(), ""));// 姓名-羅馬拼音
		outMap.put("calleeBirthday",
				holdervo.getBirthDate() == null ? "" : DateUtils.date2String(holdervo.getBirthDate(), "yyyyMMdd")); // 出生日期
		outMap.put(MEMO, ""); // 備註
		outMap.put("officeTelReg", StringUtils.defaultString(holdervo.getOfficeTelReg(), "")); // 聯絡電話日區碼.
		outMap.put("officeTel", StringUtils.defaultString(holdervo.getOfficeTel(), "")); // 聯絡電話日電話碼.
		outMap.put("officeTelExt", StringUtils.defaultString(holdervo.getOfficeTelExt(), "")); // 聯絡電話日電話分機.
		outMap.put("homeTelReg", StringUtils.defaultString(holdervo.getHomeTelReg(), "")); // 聯絡電話夜區碼.
		outMap.put("homeTel", StringUtils.defaultString(holdervo.getHomeTel(), "")); // 聯絡電話夜電話碼.
		outMap.put("homeTelExt", StringUtils.defaultString(holdervo.getHomeTelExt(), "")); // 聯絡電話夜電話分機.
		outMap.put(CONTACT_NUM1, StringUtils.defaultString(holdervo.getMobileTelephone(), "")); // 手機1
		outMap.put(CONTACT_NUM2, StringUtils.defaultString(holdervo.getMobileTel2(), "")); // 手機2
		outMap.put("address", holdervo.getAddress() == null ? ""
				: StringUtils.defaultString(holdervo.getAddress().getAddress1(), ""));
		// PCR355468 新客服系統增加電訪對象羅馬拼音
		outMap.put("calleeRomanName", StringUtils.defaultString(holdervo.getRomanName(), ""));
		// BC425 業報書聯絡電話

		checkValue(outMap, holdervo.getPolicyId(), holdervo.getBirthDate(), isFromNB, calloutDate);
		return outMap;
	}

	private void checkValue(Map<String, Object> outMap, Long policyId, Date birthDay, boolean isFromNB, Date calloutDate) {
		StringBuilder sb = new StringBuilder();

		String msg1 = checkCalleeAge(birthDay, isFromNB, calloutDate, policyId);
		String msg2 = checkExpectedCalloutTime((String) outMap.get(EXPECTED_CALLOUT_TIME_OTHER));

		if (!StringUtils.isEmpty(msg1)) {
			sb.append(msg1).append("\n");
		}
		if (!StringUtils.isEmpty(msg2)) {
			sb.append(msg2).append("\n");
		}

		if (sb.length() > 0) {
			outMap.put("alertMsg", sb.toString());
		}

	}

	// 若選擇電訪對象實際年齡未滿法定成年(要保書填寫日－生日)，彈出視窗顯示警告訊息「選擇電訪對象為未成年人，請確認」。
	private String checkCalleeAge(Date birthday, boolean isFromNB, Date calloutDate, Long policyId) {
		if (birthday != null) {
			// UNB
			if(isFromNB){
				PolicyInfo policy = policyService.load(policyId);
				if (!NBUtils.isGrownUp(birthday, policy.getApplyDate())) {
					return StringResource.getStringData("MSG_1265456", AppContext.getCurrentUser().getLangId());
				}
			}else{
				// POS
				if (calloutDate != null) {
					if (!NBUtils.isGrownUp(birthday, calloutDate)) {
						return StringResource.getStringData("MSG_1265456", AppContext.getCurrentUser().getLangId());
					}
				}
			}
		}
		return "";
	}

	// 電訪對象若為要保人或主被保險人，若自動帶出的「指定電訪時段-其他」有值，則出提醒訊息：有其他指定電訪時段，請輸入預約電訪時間。
	private String checkExpectedCalloutTime(String expectedCalloutTimeOther) {
		if (!StringUtils.isEmpty(expectedCalloutTimeOther)) {
			return StringResource.getStringData("MSG_1265457", AppContext.getCurrentUser().getLangId());
		}
		return "";
	}

	// 2022.12.30 PCR 500628 高齡投保件核保評估功能需求
	// 檢核身分證字號的編碼邏輯，如不正確出警告訊息：身分證字號錯誤，請修正。
	private Map<String, Object> checkCertiCode(Map<String, Object> in) {
		Map<String, Object> outMap = new HashMap<String, Object>();
		String certiCode = (String) in.get("certiCode");
		outMap.put("checkCertiCodeResult", certiCodeCI.checkCertiCode(certiCode).isValidated());

		return outMap;
	}

	// 2023.1.4 PCR 500628 高齡投保件核保評估功能需求
	// 檢核勾選的電訪原因大類及小類是否完整正確(規則同Policy.CalloutResult.Warning1/ Policy.CalloutResult.Error1註)。
	// 應檢核的核保訊息，若已存在應電訪的原因及電訪結果小類的電訪任務，排除不須檢核
	private Map<String, Object> checkCalloutReason(Map<String, Object> in) {
		Map<String, Object> outMap = new HashMap<String, Object>();

		Object tmpObj1 = in.get("policyId");
		Object tmpObj2 = in.get("calloutQuestionnaireStr");
		if (null != tmpObj1 && null != tmpObj2) {
			Long policyId = Long.valueOf((String) tmpObj1);
			Map<String, String> neededCallout = calloutOnlineService.findNeededCalloutReasonCodeByPolicyId(policyId);

			String calloutQuestionnaireStr = (String) tmpObj2;
			String[] calloutReasons = calloutQuestionnaireStr.split(";");
			for (int i = 0; i < calloutReasons.length; i++) {
				String itemStr = neededCallout.get(calloutReasons[i]);
				if (null != itemStr) // 本次有勾選
					neededCallout.remove(calloutReasons[i]);
			} // for

			if (false == neededCallout.isEmpty()) {
				Set<String> itemLst = new HashSet<String>();
				itemLst.addAll(neededCallout.values()); // 不同 callout_reason 可能有相同的 item
				ProposalRuleMsgVO msgVO = proposalRuleMsgService.findProposalRuleMsg(CodeCst.PROPOSAL_RULE_MSG_POLICY_CALLOUT_RESULT_WARNING1, AppContext.getCurrentUserLocalTime());
				if (msgVO != null) {
					String resultMsg = msgVO.getMessage();
					resultMsg = resultMsg.replace("{0}", StringUtils.join(neededCallout.values(), "／"));

					outMap.put("resultMsg", resultMsg);
				}
			}
		}

		return outMap;
	}

}
