package com.ebao.ls.uw.ctrl.ri;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.Factory;
import org.apache.commons.collections.ListUtils;

import com.ebao.ls.cmu.pub.document.DocumentFormFile;
import com.ebao.ls.uw.ctrl.letter.UNBLetterForm;


/**
 * Title: Policy Management <br>
 * Description: Detail Reg action form <br>
 * Copyright: Copyright (c) 2004 <br>
 * Company: eBaoTech Corporation <br>
 * Create Time: 2008/11/06 <br>
 * 
 * @author lucky.xie
 * @version 1.0
 */
public class UwRiApplyForm  extends UNBLetterForm {
	
	private static final long serialVersionUID = 1530244615588903924L;

	
	private List<Map<String, Object>> insureds;
	
	// 再保公司清單 (固定清單)
	private List<Map<String, Object>> reinsurerList;
	
    /* 以下為  subAction : Online、Preview、Save 操作的目標資料 (被保人身分證字號、臨分/再保、核保ID) */
	private String subAction;
	private Long deleteInsured;
	private Long underwriteId;
	private UwRiApplyInsuredForm operateInsured = new UwRiApplyInsuredForm();
	
	/* 影像上傳 JSP */
	@SuppressWarnings("unchecked")
	private List<DocumentFormFile> uploadFileList = ListUtils.lazyList(
            new ArrayList<DocumentFormFile>(), new Factory() {
                @Override
                public DocumentFormFile create() {
                    return new DocumentFormFile();
                }
            });;
  private String[] applyImageList;
	private String[] attachFile;
	private String attachOther;
	
	// 頁面控制
  private String isSave;
	
	public List<Map<String, Object>> getInsureds() {
		return insureds;
	}
	
	public void setInsureds(List<Map<String, Object>> insureds) {
		this.insureds = insureds;
	}
	
	public List<Map<String, Object>> getReinsurerList() {
		return reinsurerList;
	}
	
	public void setReinsurerList(List<Map<String, Object>> reinsurerList) {
		this.reinsurerList = reinsurerList;
	}
	
	public String getSubAction() {
		return subAction;
	}
	
	public void setSubAction(String subAction) {
		this.subAction = subAction;
	}
	
	public Long getUnderwriteId() {
		return underwriteId;
	}
	
	public void setUnderwriteId(Long underwriteId) {
		this.underwriteId = underwriteId;
	}
	
	public UwRiApplyInsuredForm getOperateInsured() {
		return operateInsured;
	}
	
	public void setOperateInsured(UwRiApplyInsuredForm operateInsured) {
		this.operateInsured = operateInsured;
	}
	
	public List<DocumentFormFile> getUploadFileList() {
		return uploadFileList;
	}
	
	public void setUploadFileList(List<DocumentFormFile> uploadFileList) {
		this.uploadFileList = uploadFileList;
	}
  
  public String[] getApplyImageList() {
    return applyImageList;
  }

  public void setApplyImageList(String[] applyImageList) {
    this.applyImageList = applyImageList;
  }
	
	public String[] getAttachFile() {
		return attachFile;
	}

  public void setAttachFile(String[] attachFile) {
		this.attachFile = attachFile;
	}
	
	public String getAttachOther() {
		return attachOther;
	}
	
	public void setAttachOther(String attachOther) {
		this.attachOther = attachOther;
	}

  public Long getDeleteInsured() {
    return deleteInsured;
  }

  public void setDeleteInsured(Long deleteInsured) {
    this.deleteInsured = deleteInsured;
  }

  public String getIsSave() {
    return isSave;
  }

  public void setIsSave(String isSave) {
    this.isSave = isSave;
  }
	
}
