package com.ebao.ls.uw.ctrl.letter;

import com.ebao.pub.framework.GenericForm;

public class MedicalExamForm extends GenericForm {
  private String subAction;
  private String curStep;
  private String tabType;

  private Long policyId;
  private String policyPartyId;
  private String[] selectedItem;
  private String[] newItemName;
  private String[] newItemComment;
  private String[] physicalExamPreparation;
  private Long[] selectedHospital;
  private String locations;
  private String sourceHospitals;
  public String addresseeName;
  public String address;
  
public Long addressId;
  
  private String retMsg;

    /**20150812 add by sunny Fort TGL**/
    private String[] medicalReason;

    private String[] physicalExamPreparationNotice;

    private String medicalReasonOther;

    private String isContractHospital;

    private String isPaySelf;

    private Long insurant;
    
    /**20160303 add by victor chang TGL**/
    private String  otherReason;
    private String[] medicalItem;
    private String isHospitalTxt;
    private String hospitalTxt;
    private Long underwriteId;
    private Long documentId;
    private Long mListId;
    private Long insuredId;
    private Long msgId; 
    private String notice;
    private String status;
    private String[] reasonCode;
    private String isContraHospital;
    private String insuredName;
  public String getSubAction() {
    return subAction;
  }
  public void setSubAction(String subAction) {
    this.subAction = subAction;
  }
  public Long getPolicyId() {
    return policyId;
  }
  public void setPolicyId(Long policyId) {
    this.policyId = policyId;
  }

  public String[] getNewItemName() {
    return newItemName;
  }
  public void setNewItemName(String[] newItemName) {
    this.newItemName = newItemName;
  }
  public String[] getNewItemComment() {
    return newItemComment;
  }
  public void setNewItemComment(String[] newItemComment) {
    this.newItemComment = newItemComment;
  }


  public String getCurStep() {
    return curStep;
  }
  public void setCurStep(String curStep) {
    this.curStep = curStep;
  }
  public String getTabType() {
    return tabType;
  }
  public void setTabType(String tabType) {
    this.tabType = tabType;
  }
public String[] getPhysicalExamPreparation() {
	return physicalExamPreparation;
}
public void setPhysicalExamPreparation(String[] physicalExamPreparation) {
	this.physicalExamPreparation = physicalExamPreparation;
}
public String getPolicyPartyId() {
	return policyPartyId;
}
public void setPolicyPartyId(String policyPartyId) {
	this.policyPartyId = policyPartyId;
}
public String getLocations() {
	return locations;
}
public void setLocations(String locations) {
	this.locations = locations;
}
public String getSourceHospitals() {
	return sourceHospitals;
}
public void setSourceHospitals(String sourceHospitals) {
	this.sourceHospitals = sourceHospitals;
}
public String getAddresseeName() {
	return addresseeName;
}
public void setAddresseeName(String addresseeName) {
	this.addresseeName = addresseeName;
}
public String getAddress() {
	return address;
}
public void setAddress(String address) {
	this.address = address;
}

public Long getAddressId() {
	return addressId;
}
public void setAddressId(Long addressId) {
	this.addressId = addressId;
}
public String[] getSelectedItem() {
	return selectedItem;
}
public void setSelectedItem(String[] selectedItem) {
	this.selectedItem = selectedItem;
}
public Long[] getSelectedHospital() {
	return selectedHospital;
}
public void setSelectedHospital(Long[] selectedHospital) {
	this.selectedHospital = selectedHospital;
}

    public String[] getMedicalReason() {
        return medicalReason;
    }

    public void setMedicalReason(String[] medicalReason) {
        this.medicalReason = medicalReason;
    }

    public String getMedicalReasonOther() {
        return medicalReasonOther;
    }

    public void setMedicalReasonOther(String medicalReasonOther) {
        this.medicalReasonOther = medicalReasonOther;
    }

    public String getIsContractHospital() {
        return isContractHospital;
    }

    public void setIsContractHospital(String isContractHospital) {
        this.isContractHospital = isContractHospital;
    }

    public String getIsPaySelf() {
        return isPaySelf;
    }

    public void setIsPaySelf(String isPaySelf) {
        this.isPaySelf = isPaySelf;
    }

    public Long getInsurant() {
        return insurant;
    }

    public void setInsurant(Long insurant) {
        this.insurant = insurant;
    }

    public String[] getPhysicalExamPreparationNotice() {
        return physicalExamPreparationNotice;
    }

    public void setPhysicalExamPreparationNotice(
                    String[] physicalExamPreparationNotice) {
        this.physicalExamPreparationNotice = physicalExamPreparationNotice;
    }
    public String getOtherReason() {
        return otherReason;
    }
    public void setOtherReason(String otherReason) {
        this.otherReason = otherReason;
    }
    public String[] getMedicalItem() {
        return medicalItem;
    }
    public void setMedicalItem(String[] medicalItem) {
        this.medicalItem = medicalItem;
    }
    public String getIsHospitalTxt() {
        return isHospitalTxt;
    }
    public void setIsHospitalTxt(String isHospitalTxt) {
        this.isHospitalTxt = isHospitalTxt;
    }
    public String getHospitalTxt() {
        return hospitalTxt;
    }
    public void setHospitalTxt(String hospitalTxt) {
        this.hospitalTxt = hospitalTxt;
    }
 
    public Long getDocumentId() {
        return documentId;
    }
    public void setDocumentId(Long documentId) {
        this.documentId = documentId;
    }
 
    public Long getInsuredId() {
        return insuredId;
    }
    public void setInsuredId(Long insuredId) {
        this.insuredId = insuredId;
    }
    public Long getUnderwriteId() {
        return underwriteId;
    }
    public void setUnderwriteId(Long underwriteId) {
        this.underwriteId = underwriteId;
    }
    public Long getMsgId() {
        return msgId;
    }
    public void setMsgId(Long msgId) {
        this.msgId = msgId;
    }
    public String getNotice() {
        return notice;
    }
    public void setNotice(String notice) {
        this.notice = notice;
    }
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public String[] getReasonCode() {
        return reasonCode;
    }
    public void setReasonCode(String[] reasonCode) {
        this.reasonCode = reasonCode;
    }
    public String getIsContraHospital() {
        return isContraHospital;
    }
    public void setIsContraHospital(String isContraHospital) {
        this.isContraHospital = isContraHospital;
    }
    public Long getmListId() {
        return mListId;
    }
    public void setmListId(Long mListId) {
        this.mListId = mListId;
    }
    public String getInsuredName() {
        return insuredName;
    }
    public void setInsuredName(String insuredName) {
        this.insuredName = insuredName;
    }
    public String getRetMsg() {
    	return retMsg;
    }
    public void setRetMsg(String retMsg) {
    	this.retMsg = retMsg;
    }


}
