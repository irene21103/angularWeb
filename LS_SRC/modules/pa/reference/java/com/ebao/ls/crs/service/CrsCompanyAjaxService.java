package com.ebao.ls.crs.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.ebao.foundation.module.db.hibernate.HibernateSession3;
import com.ebao.ls.crs.constant.CRSTinBizSource;
import com.ebao.ls.crs.vo.CRSEntityControlManListLogVO;
import com.ebao.ls.crs.vo.CRSEntityLogVO;
import com.ebao.ls.crs.vo.CRSPartyLogVO;
import com.ebao.ls.crs.vo.CRSTaxIDNumberLogVO;
import com.ebao.ls.pa.nb.util.NBUtils;
import com.ebao.ls.pa.nb.util.NumericUtils;
import com.ebao.ls.pa.pub.MsgCst;
import com.ebao.ls.pa.pub.NbTaskCst;
import com.ebao.pub.util.StringUtils;
import com.ebao.pub.util.json.EbaoStringUtils;

/**
 * <p>Title: CRS 聲明書-PCR_264035&263803_CRS專案</p>
 * <p>Description: 自我證明-法人</p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: TGL Co., Ltd.</p>
 * <p>Create Time: 2018/11/08</p> 
 * @author 
 * <p>Update Time: 2018/11/08</p>
 * <p>Updater: simon.huang</p>
 * <p>Update Comments: </p>
 */
public class CrsCompanyAjaxService extends CrsAjaxService {

	public static Logger logger = Logger.getLogger(CrsCompanyAjaxService.class);

	/**
	 * <p>Description :自我證明-法人 </p>
	 * <p>Created By : Simon.Huang</p>
	 * <p>Create Time : 2018年11月9日</p>
	 * @param in
	 * @param output
	 * @throws Exception
	 */
	public void init(Map<String, Object> in, Map<String, Object> output) throws Exception {

		Long partyLogId = NumericUtils.parseLong(in.get("partyLogId"));
		Long complementTaskId = NumericUtils.parseLong(in.get("complementTaskId"));
		
		if (partyLogId != null) {
			CRSPartyLogVO partyLogVO = crsPartyLogService.load(partyLogId);

			CRSEntityLogVO companyLogVO = null;
			List<CRSTaxIDNumberLogVO> taxLogList = new ArrayList<CRSTaxIDNumberLogVO>();
			List<CRSEntityControlManListLogVO> ctrlManLogList = new ArrayList<CRSEntityControlManListLogVO>();

			if (partyLogVO != null) {

				List<CRSEntityLogVO> companyLogList = crsEntityLogService.findByPartyLogId(partyLogId, false);
				if (companyLogList.size() > 0) {
					companyLogVO = companyLogList.get(0);
					taxLogList = crsTaxLogService.findByPartyLogIdAndBizId(partyLogId, CRSTinBizSource.BIZ_SOURCE_COMPANY, companyLogVO.getLogId());
					ctrlManLogList = crsEntityControlManListLogService.findByEntityLogId(companyLogVO.getLogId());
					
					if (complementTaskId != null
							&& 0 == nbTaskService.countTaskQueue(complementTaskId, NbTaskCst.TASK_QUEUE_CRS_COMPANY)) {
						companyLogVO = new CRSEntityLogVO();
						ctrlManLogList = new ArrayList<CRSEntityControlManListLogVO>();
						taxLogList = new ArrayList<CRSTaxIDNumberLogVO>();
						
						companyLogVO.setLogId(companyLogVO.getLogId());
						companyLogVO.setChangeId(companyLogVO.getChangeId());
						companyLogVO.setCrsPartyLog(partyLogVO);
					}
				} else {
					companyLogVO = new CRSEntityLogVO();
				}

			} else {
				output.put(KEY_SUCCESS, FAIL);
				output.put(KEY_ERROR_MSG, NBUtils.getTWMsg(MsgCst.MSG_1250566));
				return ;
			}

			output.put("crsIdentity", partyLogVO);
			output.put("crsCompany", companyLogVO);
			output.put("crsTaxList", taxLogList);
			output.put("crsCompanyControlPersonList", ctrlManLogList);

			Map<String, Object> codeTableMap = new HashMap<String, Object>();
			Map<String, Map<String, String>> nationality = super.getNationalityCode(taxLogList);
			codeTableMap.put("nationality", nationality);

			//列表用code table
			output.put("codeTable", codeTableMap);

			output.put("lockPage", super.checkLockpage(partyLogVO, CRSTinBizSource.BIZ_SOURCE_COMPANY));
			output.put("nbInfo", super.getNBInfo(partyLogVO));
			output.put(KEY_SUCCESS, SUCCESS);
			return;

		}

		output.put(KEY_SUCCESS, FAIL);
		output.put(KEY_ERROR_MSG, NBUtils.getTWMsg(MsgCst.MSG_1250566));
		return;
	}

	/**
	 * <p>Description :自我證明-法人 </p>
	 * <p>Created By : Simon.Huang</p>
	 * <p>Create Time : 2018年11月9日</p>
	 * @param in
	 * @param output
	 * @throws Exception
	 */
	public void save(Map<String, Object> in, Map<String, Object> output) throws Exception {

		Long partyLogId = NumericUtils.parseLong(in.get("partyLogId"));
		Long complementTaskId = NumericUtils.parseLong(in.get("complementTaskId"));
		if (partyLogId != null) {

			String type = (String) in.get(KEY_TYPE);
			try {
				Map<String, Object> submitData = (Map<String, Object>) in.get(KEY_DATA);
				Map<String, Object> srcCompany = (Map<String, Object>) submitData.get("company");//第一部份：實體帳戶持有人(法人)身分辨識資料 
				Map<String, Object> srcEntity = (Map<String, Object>) submitData.get("entity");//第二部分：實體類型 
				List<Map<String, Object>> controlPersonList = (List<Map<String, Object>>) submitData.get("controlPersonList");

				if (UPDATE.equals(type)) {
					
					clearComplementTaskBeforeData(in, output);
					
					CRSPartyLogVO partyLogVO = crsPartyLogService.load(partyLogId);
				
					CRSEntityLogVO companyLogVO = null;
					List<CRSEntityLogVO> companyLogList = crsEntityLogService.findByPartyLogId(partyLogId, false);
					List<CRSEntityControlManListLogVO> ctrlManLogList = new ArrayList<CRSEntityControlManListLogVO>();
					if (companyLogList.size() > 0) {
						companyLogVO = companyLogList.get(0);
						companyLogVO.setCrsPartyLog(partyLogVO);
						
						ctrlManLogList = crsEntityControlManListLogService.findByEntityLogId(companyLogVO.getLogId());
						companyLogVO.setCrsEntityControlManListLog(ctrlManLogList);
					} else {
						companyLogVO = new CRSEntityLogVO();
						//需設定為true,才能寫入關連的PartyLogVO的partyLogId
						companyLogVO.setCrsPartyLog(partyLogVO);
						companyLogVO.setChangeId(partyLogVO.getChangeId());
						companyLogVO = crsEntityLogService.save(companyLogVO, true);
					}
					EbaoStringUtils.setProperties(companyLogVO, srcCompany);
					EbaoStringUtils.setProperties(companyLogVO, srcEntity);
					//不設true,只寫入自己的資料(設定true會同步CrsEntityControlManListLog,ctrlManListLog由下方邏輯維護)
					companyLogVO = crsEntityLogService.save(companyLogVO);
					HibernateSession3.currentSession().flush();
					HibernateSession3.detachSession();//斷開主檔的session，處理以下的關連表
					ctrlManLogList = companyLogVO.getCrsEntityControlManListLog();

					//第三部分：具控制權之人 
					for (Map<String, Object> srcCtrlMan : controlPersonList) {
						Long logId = NumericUtils.parseLong(srcCtrlMan.get("logId"));
						CRSEntityControlManListLogVO ctrlManLogVO = null;

						boolean isEmptyData = false;

						isEmptyData = StringUtils.isNullOrEmpty((String) srcCtrlMan.get("personName1"))
										&& StringUtils.isNullOrEmpty((String) srcCtrlMan.get("personName2"))
										&& StringUtils.isNullOrEmpty((String) srcCtrlMan.get("personName3"))
										&& StringUtils.isNullOrEmpty((String) srcCtrlMan.get("personName4"))
										&& StringUtils.isNullOrEmpty((String) srcCtrlMan.get("personName5"))
										&& StringUtils.isNullOrEmpty((String) srcCtrlMan.get("personName6"))
										&& StringUtils.isNullOrEmpty((String) srcCtrlMan.get("personName7"))
										&& StringUtils.isNullOrEmpty((String) srcCtrlMan.get("personName8"))
										&& StringUtils.isNullOrEmpty((String) srcCtrlMan.get("personName9"))
										&& StringUtils.isNullOrEmpty((String) srcCtrlMan.get("personName10"));

						if (isEmptyData) {
							continue;
						}


						boolean isRenewLogId = false; //頁面為javascript控制,可能logId已經不在資料庫中要清除頁面帶上來的logId作新增作業
						if (logId != null) {
							for (CRSEntityControlManListLogVO dbData : ctrlManLogList) {
								if (dbData.getLogId().equals(logId)) {
									ctrlManLogVO = dbData;
									ctrlManLogList.remove(ctrlManLogVO);
									break;
								}
							}
							if (ctrlManLogVO == null) {
								isRenewLogId = true;
							}
						}
						if (ctrlManLogVO == null) {
							ctrlManLogVO = new CRSEntityControlManListLogVO();
						}

						EbaoStringUtils.setProperties(ctrlManLogVO, srcCtrlMan);
						if (isRenewLogId) {
							ctrlManLogVO.setLogId(null);
						}

						ctrlManLogVO.setEntityLogId(companyLogVO.getLogId());
						ctrlManLogVO.setChangeId(partyLogVO.getChangeId());
						ctrlManLogVO.setCrsLogId(partyLogId);
						crsEntityControlManListLogService.save(ctrlManLogVO);
					}

					for (CRSEntityControlManListLogVO dbData : ctrlManLogList) {
						crsEntityControlManListLogService.remove(dbData.getLogId());
					}
					
					
					
					ctrlManLogList = crsEntityControlManListLogService.findByEntityLogId(companyLogVO.getLogId());
					
					
					output.put("crsCompany", companyLogVO);
					output.put("crsCompanyControlPersonList", ctrlManLogList);

					output.put(KEY_SUCCESS, SUCCESS);
					return;
				} else if (DELETE.equals(type)) {

					return;
				}
			} catch (Exception e) {
				throw e;
			} finally {

			}

		}
		output.put(KEY_SUCCESS, FAIL);
		output.put(KEY_ERROR_MSG, NBUtils.getTWMsg(MsgCst.MSG_1250566));
	}


	private void clearComplementTaskBeforeData(Map<String, Object> in, Map<String, Object> output) {
		Long partyLogId = NumericUtils.parseLong(in.get("partyLogId"));
		Long complementTaskId = NumericUtils.parseLong(in.get("complementTaskId"));
		if (complementTaskId != null
				&& 0 == nbTaskService.countTaskQueue(complementTaskId, NbTaskCst.TASK_QUEUE_CRS_COMPANY)) {

			List<CRSTaxIDNumberLogVO> taxLogList = new ArrayList<CRSTaxIDNumberLogVO>();
			List<CRSEntityControlManListLogVO> ctrlManLogList = new ArrayList<CRSEntityControlManListLogVO>();

			List<CRSEntityLogVO> companyLogList = crsEntityLogService.findByPartyLogId(partyLogId, false);
			if (companyLogList.size() > 0){
				for (CRSEntityLogVO companyLogVO  : companyLogList) {
					taxLogList = crsTaxLogService.findByPartyLogIdAndBizId(partyLogId, CRSTinBizSource.BIZ_SOURCE_COMPANY, companyLogVO.getLogId());
					ctrlManLogList = crsEntityControlManListLogService.findByEntityLogId(companyLogVO.getLogId());
					// 清除補全前寫入稅藉編號資料
					for (CRSTaxIDNumberLogVO taxLog : taxLogList) {
						crsTaxLogService.remove(taxLog.getEntityId());
					}
					for(CRSEntityControlManListLogVO ctrlManLogVO : ctrlManLogList) {
						crsEntityControlManListLogService.remove(ctrlManLogVO.getEntityId());
					}
					crsEntityLogService.remove(companyLogVO.getEntityId());
				}
			}
		
			
			nbTaskService.saveTaskQueue(complementTaskId, NbTaskCst.TASK_QUEUE_CRS_COMPANY, "");
		}
	}

}
