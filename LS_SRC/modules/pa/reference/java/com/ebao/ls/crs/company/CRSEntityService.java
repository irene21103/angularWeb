package com.ebao.ls.crs.company;

import com.ebao.ls.crs.vo.CRSEntityVO;
import com.ebao.ls.foundation.service.GenericService;

public interface CRSEntityService extends GenericService<CRSEntityVO>{
	public final static String BEAN_DEFAULT = "crsEntityService";
	
	public abstract CRSEntityVO findByCertiCode(String certiCode);
}
