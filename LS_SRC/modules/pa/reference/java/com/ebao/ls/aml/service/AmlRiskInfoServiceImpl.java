package com.ebao.ls.aml.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;

import com.ebao.ls.aml.bs.AmlRiskInfoService;
import com.ebao.ls.aml.data.AmlRiskInfoDao;
import com.ebao.ls.pub.aml.AmlRiskInfo;

public class AmlRiskInfoServiceImpl implements AmlRiskInfoService {
	@Resource(name = AmlRiskInfoDao.BEAN_DEFAULT)
	private AmlRiskInfoDao amlRiskInfoDao;
	@Override
	public AmlRiskInfo findByCertiCodeAndClientType(String certiCode,String clientType) {
		Map<String, Object> params= new HashMap<String,Object>();
		params.put("certiCode", certiCode);
		params.put("clientType", clientType);
		List<AmlRiskInfo> amlRiskInfos =amlRiskInfoDao.find(params);
		if(CollectionUtils.isNotEmpty(amlRiskInfos)) {
			return amlRiskInfos.get(0);
		}else {
			return null;
		}
	}

}
