package com.ebao.ls.uw.ctrl.pub;

import com.ebao.pub.framework.GenericForm;

/**
 * <p>Title: </p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2004 </p>
 * <p>Company: eBaoTech Corporation </p>
 * <p>Create Time: Thu Sep 09 11:31:20 GMT+08:00 2004 </p>
 * @author Walter Huang
 * @version 1.0
 *
 */

public class UwExtraLoadingForm extends GenericForm {

  private java.lang.Long underwriteId = null;

  private java.lang.Long itemId = null;

  private java.lang.String extraType = "";

  private java.util.Date startDate = null;

  private java.util.Date endDate = null;

  private java.lang.Long policyId = null;

  private java.lang.Long underwriterId = null;

  private java.lang.String extraArith = "";

  private java.math.BigDecimal extraPrem = null;

  private java.math.BigDecimal extraPara = null;

  private java.lang.Long listId = null;

  private java.lang.Integer emValue = null;

  private java.lang.Long insuredId = null;

  private java.lang.Long uwListId = null;

  private String reCalcIndi = null;

  private java.lang.String reason = null;

  private String duration = "0";

  private Integer emCi;
  private Integer emLife;
  private Integer emTpd;

  //add by simon.huang on 2015-07-17 加費年期(長加/短加)
  private String extraPeriodType;
  
  public Integer getEmCi() {
    return emCi;
  }

  public void setEmCi(Integer emCi) {
    this.emCi = emCi;
  }

  public Integer getEmLife() {
    return emLife;
  }

  public void setEmLife(Integer emLife) {
    this.emLife = emLife;
  }

  public Integer getEmTpd() {
    return emTpd;
  }

  public void setEmTpd(Integer emTpd) {
    this.emTpd = emTpd;
  }

  public String getDuration() {
    return duration;
  }

  public void setDuration(String duration) {
    this.duration = duration;
  }

  /**
   * @return Returns the reason.
   */
  public java.lang.String getReason() {
    return reason;
  }

  /**
   * @param reason The reason to set.
   */
  public void setReason(java.lang.String reason) {
    this.reason = reason;
  }

  /**
   * returns the value of the underwriteId
   *
   * @return the underwriteId
   */
  public java.lang.Long getUnderwriteId() {
    return underwriteId;
  }

  /**
   * sets the value of the underwriteId
   *
   * @param underwriteId the underwriteId
   */
  public void setUnderwriteId(java.lang.Long underwriteId) {
    this.underwriteId = underwriteId;
  }

  /**
   * returns the value of the itemId
   *
   * @return the itemId
   */
  public java.lang.Long getItemId() {
    return itemId;
  }

  /**
   * sets the value of the itemId
   *
   * @param itemId the itemId
   */
  public void setItemId(java.lang.Long itemId) {
    this.itemId = itemId;
  }

  /**
   * returns the value of the extraType
   *
   * @return the extraType
   */
  public java.lang.String getExtraType() {
    return extraType;
  }

  /**
   * sets the value of the extraType
   *
   * @param extraType the extraType
   */
  public void setExtraType(java.lang.String extraType) {
    this.extraType = extraType;
  }

  /**
   * returns the value of the startDate
   *
   * @return the startDate
   */
  public java.util.Date getStartDate() {
    return startDate;
  }

  /**
   * sets the value of the startDate
   *
   * @param startDate the startDate
   */
  public void setStartDate(java.util.Date startDate) {
    this.startDate = startDate;
  }

  /**
   * returns the value of the endDate
   *
   * @return the endDate
   */
  public java.util.Date getEndDate() {
    return endDate;
  }

  /**
   * sets the value of the endDate
   *
   * @param endDate the endDate
   */
  public void setEndDate(java.util.Date endDate) {
    this.endDate = endDate;
  }

  /**
   * returns the value of the policyId
   *
   * @return the policyId
   */
  public java.lang.Long getPolicyId() {
    return policyId;
  }

  /**
   * sets the value of the policyId
   *
   * @param policyId the policyId
   */
  public void setPolicyId(java.lang.Long policyId) {
    this.policyId = policyId;
  }

  /**
   * returns the value of the underwriterId
   *
   * @return the underwriterId
   */
  public java.lang.Long getUnderwriterId() {
    return underwriterId;
  }

  /**
   * sets the value of the underwriterId
   *
   * @param underwriterId the underwriterId
   */
  public void setUnderwriterId(java.lang.Long underwriterId) {
    this.underwriterId = underwriterId;
  }

  /**
   * returns the value of the extraArith
   *
   * @return the extraArith
   */
  public java.lang.String getExtraArith() {
    return extraArith;
  }

  /**
   * sets the value of the extraArith
   *
   * @param extraArith the extraArith
   */
  public void setExtraArith(java.lang.String extraArith) {
    this.extraArith = extraArith;
  }

  /**
   * returns the value of the extraPrem
   *
   * @return the extraPrem
   */
  public java.math.BigDecimal getExtraPrem() {
    return extraPrem;
  }

  /**
   * sets the value of the extraPrem
   *
   * @param extraPrem the extraPrem
   */
  public void setExtraPrem(java.math.BigDecimal extraPrem) {
    this.extraPrem = extraPrem;
  }

  /**
   * returns the value of the extraPara
   *
   * @return the extraPara
   */
  public java.math.BigDecimal getExtraPara() {
    return extraPara;
  }

  /**
   * sets the value of the extraPara
   *
   * @param extraPara the extraPara
   */
  public void setExtraPara(java.math.BigDecimal extraPara) {
    this.extraPara = extraPara;
  }

  /**
   * returns the value of the listId
   *
   * @return the listId
   */
  public java.lang.Long getListId() {
    return listId;
  }

  /**
   * sets the value of the listId
   *
   * @param listId the listId
   */
  public void setListId(java.lang.Long listId) {
    this.listId = listId;
  }

  /**
   * returns the value of the emValue
   *
   * @return the emValue
   */
  public java.lang.Integer getEmValue() {
    return emValue;
  }

  /**
   * sets the value of the emValue
   *
   * @param emValue the emValue
   */
  public void setEmValue(java.lang.Integer emValue) {
    this.emValue = emValue;
  }

  /**
   * returns the value of the insuredId
   *
   * @return the insuredId
   */
  public java.lang.Long getInsuredId() {
    return insuredId;
  }

  /**
   * sets the value of the insuredId
   *
   * @param insuredId the insuredId
   */
  public void setInsuredId(java.lang.Long insuredId) {
    this.insuredId = insuredId;
  }

  /**
   * returns the value of the uwListId
   *
   * @return the uwListId
   */
  public java.lang.Long getUwListId() {
    return uwListId;
  }

  /**
   * sets the value of the uwListId
   *
   * @param uwListId the uwListId
   */
  public void setUwListId(java.lang.Long uwListId) {
    this.uwListId = uwListId;
  }
  // Begin Additional Business Methods

  public String callIND;

  public String getCallIND() {
    return callIND;
  }

  public void setCallIND(String callIND) {
    this.callIND = callIND;
  }

  // End Additional Business Methods

  /**
   * @return Returns the reCalcIndi.
   */
  public String getReCalcIndi() {
    return reCalcIndi;
  }

  /**
   * @param reCalcIndi The reCalcIndi to set.
   */
  public void setReCalcIndi(String reCalcIndi) {
    this.reCalcIndi = reCalcIndi;
  }

    /**
     * @return 傳回 extraPeriodType。
     */
    public String getExtraPeriodType() {
        return extraPeriodType;
    }
    
    /**
     * @param extraPeriodType 要設定的 extraPeriodType。
     */
    public void setExtraPeriodType(String extraPeriodType) {
        this.extraPeriodType = extraPeriodType;
    }
}