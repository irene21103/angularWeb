package com.ebao.ls.riskPrevention.ci.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.ebao.ls.riskPrevention.bs.NationalityCodeService;
import com.ebao.ls.riskPrevention.ci.NationalityCodeCI;
import com.ebao.ls.riskPrevention.data.NationalityCodeDao;
import com.ebao.ls.riskPrevention.data.bo.NationalityCode;
import com.ebao.ls.riskPrevention.vo.NationalityCodeQueryVO;
import com.ebao.ls.riskPrevention.vo.NationalityCodeVO;
import com.ebao.pub.util.BeanUtils;

public class NationalityCodeCIImpl implements NationalityCodeCI{
	@Resource(name=NationalityCodeService.BEAN_DEFAULT)
	NationalityCodeService nationalityCodeService;
	
	@Resource(name=NationalityCodeDao.BEAN_DEFAULT)
	NationalityCodeDao dao;
	
	@Override
	public String findNameByNationalityCode(String nationalCode) {
		NationalityCodeVO vo = new NationalityCodeVO();
		vo.setNationalCode(nationalCode);
		List<Map<String,Object>> resultList = nationalityCodeService.query(vo, null);
		if(resultList.size()>0){
			return (String) resultList.get(0).get("NATIONALITY_DESC");
		}
		return null;
	}

	@Override
	public String findCodeByNationalityName(String nationalName) {
		NationalityCodeVO vo = new NationalityCodeVO();
		vo.setNationalName(nationalName);
		List<Map<String,Object>> resultList = nationalityCodeService.query(vo, null);
		if(resultList.size()>0){
			return (String) resultList.get(0).get("NATIONALITY_CODE");
		}
		return null;
	}

	@Override
	public String findRiskByNationalityCode(String nationalCode) {
		return nationalityCodeService.findRiskByNationalityCode(nationalCode);
	}

	@Override
	public String findRiskByNationalityName(String nationalName) {
		return nationalityCodeService.findRiskByNationalityName(nationalName);
	}

	@Override
	public NationalityCodeQueryVO findNationalityInfoByNationalCode(String nationalCode) {
		NationalityCode result = dao.findUniqueByProperty("nationalCode", nationalCode);
		NationalityCodeQueryVO vo = new NationalityCodeQueryVO();
		if(result!=null){
			BeanUtils.copyProperties(vo, result);
		}
		return vo;
	}

	@Override
	public NationalityCodeQueryVO findNationalityInfoByNationalName(
			String nationalName) {
		List<NationalityCode> result = dao.findByProperty("nationalName", nationalName);
		NationalityCodeQueryVO vo = new NationalityCodeQueryVO();
		if(result.size()>0){
			BeanUtils.copyProperties(vo, result.get(0));
		}
		return vo;
	}

}
