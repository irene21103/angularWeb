package com.ebao.ls.uw.ctrl.underwriting;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.ls.pa.pub.ci.CoverageCI;
import com.ebao.ls.pa.pub.ref.api.prd.ProductService;
import com.ebao.ls.pa.pub.ref.vo.prd.ProductVO;
import com.ebao.ls.pa.pub.service.PolicyService;
import com.ebao.ls.pa.pub.vo.CoverageVO;
import com.ebao.ls.uw.ctrl.ActionUtil;
import com.ebao.ls.uw.ctrl.UwGenericAction;
import com.ebao.ls.uw.ds.constant.UwDecisionConstants;
import com.ebao.ls.uw.ds.constant.UwExceptionConstants;
import com.ebao.ls.uw.ds.constant.UwExtraLoadingTypeConstants;
import com.ebao.ls.uw.vo.UwExtraLoadingVO;
import com.ebao.ls.uw.vo.UwLifeInsuredVO;
import com.ebao.ls.uw.vo.UwPolicyVO;
import com.ebao.ls.uw.vo.UwProductVO;
import com.ebao.pub.annotation.PrdAPIUpdate;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.framework.GenericException;
import com.ebao.pub.framework.MultiWarning;

/**
 * <p>
 * Title:Underwriting
 * </p>
 * <p>
 * Description:
 * </p>
 * <p>
 * Copyright: Copyright (c) 2004
 * </p>
 * <p>
 * Company: eBaoTech Corporation
 * </p>
 * 
 * @author jason.luo
 * @since Jun 27, 2005
 * @version 1.0
 */
public class UwValidateAction extends UwGenericAction {
  // Next Action Logical name
  private static final String NEXT_PAGE = "nextAction";

  /**
   * Default Constructor
   */
  public UwValidateAction() {
  }

  /**
   * Check data values and alert warning message.
   * 
   * @param mapping The ActionMapping used to select this instance
   * @param form The optional ActionForm bean for this request (if any)
   * @param request HttpRequest
   * @param response HttpResponse
   * @return ActionForward ActionForward
   * @throws Exception Application Exception
   */
  public MultiWarning processWarning(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {
    MultiWarning warning = new MultiWarning();
    Long underwriteId = Long.valueOf(request.getParameter("underwriteId"));
    UwPolicyVO vo = getUwPolicyDS().findUwPolicy(underwriteId);
    Collection products = getUwPolicyDS().findUwProductEntitis(
        getUnderwriteId(request));
    Collection lifeInsureds = getUwPolicyDS().findUwLifeInsuredEntitis(
        getUnderwriteId(request));
    // check standard life indicator against product definition
    productStandardCheck(lifeInsureds, products, warning);
    if (!warning.isContinuable()) {
      return warning;
    } else {
      // check standard life indicator against LA health status
      checkLAHealthStatus(lifeInsureds, products, warning);
      // check perfer life indicator if necessary
      checkPreferLifeInd(lifeInsureds, products, warning);
      // check commencement date
      postdateCheck(vo.getValidateDate(), warning);
      // check risk commencement date
      if (null != vo.getActualValidate()) {
        postdateCheck(vo.getActualValidate(), warning);
      }
      // check backdating against product definition
      // backdatingCheck(vo.getValidateDate(), products, warning);
      // check generate LCA Ind and update LCA Ind
      checkLCAInd(vo.getGenerateLcaIndi(), vo.getLcaDateIndi(), warning);
      // check product decision based on relationship of product decision
      checkProductDecision(products, warning);
      // check whether product decision violates the rule
      checkProductDecisionRule(products, warning);
      return warning;
    }
  }

  /**
   * Check product decision rules
   * 
   * @param products Products
   * @param warning MultiWarning
   */
  private void checkProductDecisionRule(Collection products,
      MultiWarning warning) throws GenericException {
    Iterator iter = products.iterator();
    List<CoverageVO> nbuVOs = null;
    while (iter.hasNext()) {
      UwProductVO product = (UwProductVO) iter.next();
      if (null == nbuVOs) {
        nbuVOs = coverageCI.findByPolicyId(product.getPolicyId());
      }
      if (isProductDecisionMade(product)) {
        int decision = product.getDecisionId().intValue();
        if (UwDecisionConstants.STANDARD_CASE_UNDERWRITTEN == decision) {
          // lien validation
          checkLienRule(warning, product);
          // loading validation
          extraLoadingRule(warning, product);
          // restrict coverage validation
          checkRCRule(warning, nbuVOs, product);
        }
      }
    }
  }

  /**
   * Check extra loading rule
   * 
   * @param warning
   * @param product
   * @throws GenericException
   */
  private void extraLoadingRule(MultiWarning warning, UwProductVO product)
      throws GenericException {
    Collection extraLoadings = getUwPolicyDS().findUwExtraLoadingEntitis(
        product.getUnderwriteId(), product.getItemId());
    Iterator iters = extraLoadings.iterator();
    while (iters.hasNext()) {
      UwExtraLoadingVO uelvo = (UwExtraLoadingVO) iters.next();
      if (UwExtraLoadingTypeConstants.HEALTH.equals(uelvo.getExtraType())) {
        warning.setContinuable(false);
        warning.addWarning("ERR_"
            + UwExceptionConstants.APP_STAND_DEC_SUB_STAND_TERM_VIOLATION,
            "-EL-A-" + String.valueOf(product.getProductId()));
      }
    }
  }

  /**
   * Check restrict coverage rule
   * 
   * @param warning
   * @param nbuVOs
   * @param product
   */
  private void checkRCRule(MultiWarning warning, List<CoverageVO> nbuVOs,
      UwProductVO product) {
    // restrict coverage validation
    if (null != nbuVOs) {
      for (CoverageVO vo : nbuVOs) {
        if (vo.getItemId().equals(product.getItemId())) {
          if (null != product.getReducedAmount()
              && product.getReducedAmount().compareTo(product.getAmount()) < 0) {
            warning.setContinuable(false);
            warning.addWarning("ERR_"
                + UwExceptionConstants.APP_STAND_DEC_SUB_STAND_TERM_VIOLATION,
                "-RC-RSA-" + String.valueOf(product.getProductId()));
          }
        }
      }
    }
  }

  /**
   * Check lien rule
   * 
   * @param warning
   * @param product
   * @throws GenericException
   */
  private void checkLienRule(MultiWarning warning, UwProductVO product)
      throws GenericException {
    Collection liens = getUwPolicyDS().findUwLienEntitis(
        product.getUnderwriteId(), product.getItemId());
    if (null != liens && 0 < liens.size()) {
      warning.setContinuable(false);
      warning.addWarning("ERR_"
          + UwExceptionConstants.APP_STAND_DEC_SUB_STAND_TERM_VIOLATION, "-L-"
          + String.valueOf(product.getProductId()));
    }
  }

  /**
   * Check product decision
   * 
   * @param products Products
   * @param warning MultiWarning
   */
  private void checkProductDecision(Collection products, MultiWarning warning) {
    HashMap map = new HashMap();
    Iterator iter = products.iterator();
    while (iter.hasNext()) {
      UwProductVO product = (UwProductVO) iter.next();
      if (isProductInvalidate(product)) {
        productValidate(product, products, map, warning);
      }
    }
  }

  private static boolean isDecisionValidate(Integer id) {
    if (null == id) {
      return false;
    } else {
      if (id.intValue() == UwDecisionConstants.CONDITIONAL_UNDERWRITTEN
          || id.intValue() == UwDecisionConstants.STANDARD_CASE_UNDERWRITTEN) {
        return true;
      } else {
        return false;
      }
    }
  }

  private void productValidate(UwProductVO invalidProduct, Collection products,
      HashMap map, MultiWarning warning) {
    Iterator iter = products.iterator();
    while (iter.hasNext()) {
      UwProductVO upvo = (UwProductVO) iter.next();
      if (null != upvo.getMasterId()
          && upvo.getMasterId().longValue() == invalidProduct.getItemId()
              .longValue()) {
        if (isDecisionValidate(upvo.getDecisionId())) {
          if (!map.containsKey(upvo)) {
            map.put(upvo, upvo);
            warning.setContinuable(false);
            warning.addWarning("ERR_"
                + UwExceptionConstants.APP_PLAN_INVALIDATE, String.valueOf(upvo
                .getProductId()));
            UwProductVO newVO = new UwProductVO();
            newVO.setItemId(upvo.getItemId());
            newVO.setDecisionId(upvo.getDecisionId());
            newVO.setMasterId(upvo.getMasterId());
            productValidate(newVO, products, map, warning);
          }
        } else {
          if (isProductDecisionMade(upvo)) {
            productValidate(upvo, products, map, warning);
          }
        }
      }
    }
  }

  private boolean isProductDecisionMade(UwProductVO vo) {
    if (null == vo.getDecisionId()) {
      return false;
    }
    return true;
  }

  private boolean isProductInvalidate(UwProductVO vo) {
    if (!isProductDecisionMade(vo)) {
      return false;
    } else {
      int decisionId = vo.getDecisionId().intValue();
      if (decisionId == UwDecisionConstants.POSTPONE
          || decisionId == UwDecisionConstants.DECLINATION) {
        return true;
      }
    }
    return false;
  }

  /**
   * Check relation ship of Generate LCA IND and Update LCA IND
   * 
   * @param genLcaInd Generate LCA IND
   * @param updateLcaInd Update LCA IND
   * @param warning MultiWarning
   */
  private void checkLCAInd(String genLcaInd, String updateLcaInd,
      MultiWarning warning) {
    if ("Y".equals(genLcaInd)) {
      if (!"Y".equals(updateLcaInd)) {
        warning.setContinuable(false);
        warning.addWarning("ERR_"
            + UwExceptionConstants.APP_LCA_INDI_NOT_CONSISTENT);
      }
    }
  }

  /**
   * Postdate not allowd to commencement date and risk commencement date
   * 
   * @param date Date
   * @param warning MultiWarning
   */
  private void postdateCheck(Date date, MultiWarning warning) {
    if (ActionUtil.compareDate(date, AppContext.getCurrentUserLocalTime())) {
      warning.setContinuable(false);
      warning
          .addWarning("ERR_" + UwExceptionConstants.APP_POSTDATE_NOT_ALLOWED);
    }
  }

  /**
   * Check standard life indicator according to product definition
   * 
   * @param lifeInsureds Life insureds
   * @param products Products
   * @param warning MultiWarning
   * @throws GenericException Application Exception
   */
  @PrdAPIUpdate
  private void productStandardCheck(Collection lifeInsureds,
      Collection products, MultiWarning warning) throws GenericException {
    Iterator productIter = products.iterator();
    while (productIter.hasNext()) {
      UwProductVO product = (UwProductVO) productIter.next();
      ProductVO basicInfo = this.getProductService().getProduct(
          Long.valueOf(product.getProductId().intValue()),policyService.getActiveVersionDate(product));
      if (basicInfo.isStandLifeIndi()) {
        Iterator insureIter = lifeInsureds.iterator();
        while (insureIter.hasNext()) {
          UwLifeInsuredVO ulivo = (UwLifeInsuredVO) insureIter.next();
          if (!"Y".equals(ulivo.getStandLife())) {
            warning.setContinuable(false);
            warning.addWarning("ERR_"
                + UwExceptionConstants.APP_STAND_LIFE_ONLY, String.valueOf(
                product.getProductId()).concat("-").concat(
                String.valueOf(ulivo.getInsuredId())));
          }
        }
      }
    }
  }

  /**
   * Determines whether the product applicable for prefer life ind checking
   * 
   * @param product Product
   * @author jason.luo
   * @since 12.07.2004
   * @version 1.0
   */
  private boolean isApplicableForPerferLifeCheck(UwProductVO product) {
    if (null == product.getDecisionId()) {
      return false;
    } else {
      if (UwDecisionConstants.POSTPONE == product.getDecisionId().intValue()
          || UwDecisionConstants.DECLINATION == product.getDecisionId()
              .intValue()) {
        return false;
      }
    }
    return true;
  }

  /**
   * Check prefer life ind of the life insured
   * 
   * @param insuredList Insured List
   * @param products Products
   * @param warning MultiWarning
   * @throws GenericException Application Exception
   */
  private void checkPreferLifeInd(Collection insuredList, Collection products,
      MultiWarning warning) throws GenericException {
    // Only GELS needs to check prefer life indicator(2 - prefer life ind is Y)
    Iterator uliIter = insuredList.iterator();
    while (uliIter.hasNext()) {
      UwLifeInsuredVO ulivo = (UwLifeInsuredVO) uliIter.next();
      if ("N".equals(ulivo.getStandLife())) {
        Iterator productIter = products.iterator();
        while (productIter.hasNext()) {
          UwProductVO product = (UwProductVO) productIter.next();
          // added by jason.luo at 12.07.2004
          if (!isApplicableForPerferLifeCheck(product)) {
            continue;
          }
          // 2-Y 1-N 0-N/A
          if (ulivo.getInsuredId().equals(product.getInsured1())) {
            if ("2".equals(product.getInsuredStatus())) {
              warning.setContinuable(false);
              warning.addWarning("ERR_"
                  + UwExceptionConstants.APP_STD_LIFE_INDI_PREFER_LIFE, String
                  .valueOf(product.getProductId()).concat("-").concat(
                      String.valueOf(ulivo.getInsuredId())));
            }
          } else if (ulivo.getInsuredId().equals(product.getInsured2())) {
            if ("2".equals(product.getInsuredStatus2())) {
              warning.setContinuable(false);
              warning.addWarning("ERR_"
                  + UwExceptionConstants.APP_STD_LIFE_INDI_PREFER_LIFE, String
                  .valueOf(product.getProductId()).concat("-").concat(
                      String.valueOf(ulivo.getInsuredId())));
            }
          }
        }
      }
    }
  }

  /**
   * Check health status of the life insured If lien or health loading been
   * imposed then the Standard Life Indicator can not be Y
   * 
   * @param insuredList
   * @param products
   * @param warning
   * @throws GenericException
   */
  private void checkLAHealthStatus(Collection insuredList, Collection products,
      MultiWarning warning) throws GenericException {
    Iterator iter = products.iterator();
    while (iter.hasNext()) {
      UwProductVO vo = (UwProductVO) iter.next();
      // Whether lien has been imposed
      Collection liens = getUwPolicyDS().findUwLienEntitis(
          vo.getUnderwriteId(), vo.getItemId());
      if (null != liens && !liens.isEmpty()) {
        Iterator laIter = insuredList.iterator();
        while (laIter.hasNext()) {
          UwLifeInsuredVO lavo = (UwLifeInsuredVO) laIter.next();
          if ("Y".equals(lavo.getStandLife())) {
            warning.setContinuable(false);
            warning.addWarning("ERR_"
                + UwExceptionConstants.APP_STD_LIFE_INDI_LIEN_OR_HEALTH, String
                .valueOf(vo.getProductId()).concat("-L-").concat(
                    String.valueOf(lavo.getInsuredId())));
          }
        }
      }
      // Whether health Loading has been imposed
      Collection extraLoadings = getUwPolicyDS().findUwExtraLoadingEntitis(
          vo.getUnderwriteId(), vo.getItemId());
      Iterator extraLoadingIter = extraLoadings.iterator();
      while (extraLoadingIter.hasNext()) {
        UwExtraLoadingVO uelvo = (UwExtraLoadingVO) extraLoadingIter.next();
        if (UwExtraLoadingTypeConstants.HEALTH.equals(uelvo.getExtraType())) {
          Iterator laIter = insuredList.iterator();
          while (laIter.hasNext()) {
            UwLifeInsuredVO lavo = (UwLifeInsuredVO) laIter.next();
            if ("Y".equals(lavo.getStandLife())) {
              warning.setContinuable(false);
              warning.addWarning("ERR_"
                  + UwExceptionConstants.APP_STD_LIFE_INDI_LIEN_OR_HEALTH,
                  String.valueOf(vo.getProductId()).concat("-H-").concat(
                      String.valueOf(lavo.getInsuredId())));
            }
          }
        }
      }
    }
  }

  /**
   * Forward to another action.
   * 
   * @param mapping The ActionMapping used to select this instance
   * @param form The optional ActionForm bean for this request (if any)
   * @param request HttpRequest
   * @param response HttpResponse
   * @return ActionForward ActionForward
   * @throws GenericException Application Exception
   */
  public ActionForward uwProcess(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws GenericException {
    return mapping.findForward(NEXT_PAGE);
  }

  // private UwPolicyDS uwPolicyDS;
  // public void setUwPolicyDS(UwPolicyDS uwPolicyDS){
  // this.uwPolicyDS = uwPolicyDS;
  // }
  private CoverageCI coverageCI;

  public CoverageCI getCoverageCI() {
    return coverageCI;
  }

  public void setCoverageCI(CoverageCI coverageCI) {
    this.coverageCI = coverageCI;
  }
  private ProductService productService;

  public ProductService getProductService() {
    return productService;
  }

  public void setProductService(ProductService productService) {
    this.productService = productService;
  }
  
  @Resource(name = PolicyService.BEAN_DEFAULT)
  PolicyService policyService;
}
