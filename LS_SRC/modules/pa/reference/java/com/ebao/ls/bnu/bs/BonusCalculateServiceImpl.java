package com.ebao.ls.bnu.bs;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;

import com.ebao.ls.bnu.bs.sp.BonusCalculateDAO;
import com.ebao.pub.framework.GenericDS;
import com.ebao.pub.framework.GenericException;

/**
 *
 * <p>Title: </p>
 * <p>Description:implement bonus domain service </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: eBaoTech Corporation</p>
 * @author :sunrain.han
 * @version 1.0
 */
public class BonusCalculateServiceImpl extends GenericDS
    implements
      BonusCalculateService {

  /**
   * get bonus due date
   * @param validateDate
   * @return
   * @throws GenericException
   */
  public java.util.Date getBonusDueDate(java.util.Date validateDate)
      throws GenericException {

    BonusCalculateDAO dao = new BonusCalculateDAO();
    return dao.getNextBonusDate(validateDate);
  }
  /**
   * clear Rev bonus allocation records
   * @param changeID
   * @param policyChgID
   * @param itemID
   * @return
   * @throws GenericException
   */
  public java.math.BigDecimal clearAccuRB(Long changeID, Long policyChgID,
      Long itemID) throws GenericException {
    java.math.BigDecimal result;
    BonusCalculateDAO dao = new BonusCalculateDAO();
    result = dao.clearAccuRB(changeID, policyChgID, itemID);
    return result;
  }
  public java.math.BigDecimal clearAccuRBSurr(Long changeID, Long policyChgID,
      Long itemID) throws GenericException {
    java.math.BigDecimal result;
    BonusCalculateDAO dao = new BonusCalculateDAO();
    result = dao.clearAccuRBSurr(changeID, policyChgID, itemID);
    return result;
  }
  /**
   * adjust bonus after partial surrender
   * @param itemID
   * @param drawBonus
   * @param moneyID
   * @param changeID
   * @param policyChgID
   * @param optID
   * @return
   * @throws GenericException
   */

  public int getPartialSurrender(Long itemID, java.math.BigDecimal drawBonus,
      Long moneyID, Long changeID, Long policyChgID, Long optID)
      throws GenericException {
    int result;
    BonusCalculateDAO dao = new BonusCalculateDAO();
    result = dao.getPartialSurrender(itemID, drawBonus, moneyID, changeID,
        policyChgID, optID);
    return result;
  }
  /**
   * adjust bonus after increase SA
   * @param itemID
   * @param oldBonus
   * @param newBonus
   * @param bonusSA
   * @param moneyID
   * @param changeID
   * @param policyChgID
   * @param optID
   * @return
   * @throws GenericException
   */
  public int adjustBonusOfIcrSA(Long itemID, BigDecimal oldBonus,
      java.math.BigDecimal newBonus, java.math.BigDecimal bonusSA,
      Long moneyID, Long changeID, Long policyChgID, Long optID)
      throws GenericException {
    int result;
    BonusCalculateDAO dao = new BonusCalculateDAO();
    result = dao.adjustBonusOfIncrSA(itemID, oldBonus, newBonus, bonusSA,
        moneyID, changeID, policyChgID, optID);
    return result;
  }
  /**
   * undo partial surrender
   * @param itemID
   * @param changeID
   * @param policyChgID
   * @throws GenericException
   */
  public void cancelPartialSurr(Long itemID, Long changeID, Long policyChgID)
      throws GenericException {
    BonusCalculateDAO dao = new BonusCalculateDAO();
    dao.cancelPartialSurr(itemID, changeID, policyChgID);
  }
  /**
   * undo increase SA
   * @param itemID
   * @param changeID
   * @param policyChgID
   * @throws GenericException
   */
  public void cancelIncreaseSA(Long itemID, Long changeID, Long policyChgID)
      throws GenericException {
    BonusCalculateDAO dao = new BonusCalculateDAO();
    dao.cancelIncreaseSA(itemID, changeID, policyChgID);
  }

  /**
   * return some bonus info for integration module
   * @param policyID
   * @return
   * @throws GenericException
   */

  public BonusInteVO getIntegrationSumIB(Long policyID) throws GenericException {
    BonusCalculateDAO dao = new BonusCalculateDAO();

    return dao.getIntegrationSumIB(policyID);

  }
  /**
   * return some bonus info for integraton module
   * @param bonusDate
   * @param nricNo
   * @param nricType
   * @return
   * @throws GenericException
   */
  public ArrayList getIntegrationBonusRate(Date bonusDate,
      java.lang.Long policyID) throws GenericException {
    BonusCalculateDAO dao = new BonusCalculateDAO();
    return dao.getIntegrationBonusRate(bonusDate, policyID);
  }
  /**
   * get some bonus info for integration module
   * @param bonusDate
   * @return
   * @throws GenericException
   */
  public ArrayList getIntegrationBonusRatePrd(Date bonusDate)
      throws GenericException {
    BonusCalculateDAO dao = new BonusCalculateDAO();
    return dao.getIntegrationBonusRatePrd(bonusDate);
  }
  /**
   * return bonus projection value
   * @param itemID
   * @param optionSB
   * @param optionCB
   * @param premOption
   * @param projectDate
   * @param projectValue
   * @throws GenericException
   */
  public void getProjectValue(Long itemID, String optionSB, String optionCB,
      String premOption, Date projectDate, String[] projectValue)
      throws GenericException {

    BonusCalculateDAO dao = new BonusCalculateDAO();
    dao.getProjectValue(itemID, optionSB, optionCB, premOption, projectDate,
        projectValue);
  }
  /**
   * return projection maturity value
   * @param itemID
   * @param optionSB
   * @param maturityValue
   * @throws GenericException
   */
  public void getMaturityValue(Long itemID, String optionSB,
      String[] maturityValue) throws GenericException {
    BonusCalculateDAO dao = new BonusCalculateDAO();
    dao.getMaturityValue(itemID, optionSB, maturityValue);
  }
  /**
   * return bonus info for integration module
   * @param policyID
   * @param itemID
   * @param calcDate
   * @return
   * @throws GenericException
   */
  public BonusInteSumIBVO getBonusInfoForInte(Long policyID, Long itemID,
      Date calcDate) throws GenericException {
    BonusCalculateDAO dao = new BonusCalculateDAO();
    return dao.getBonusInfoForInte(policyID, itemID, calcDate);
  }

  /**
   * undo change commencement date
   * @param itemID
   * @param changeID
   * @param policyChgID
   * @throws GenericException
   */
  public void cancelChangeCommenceDate(Long itemID, Long changeID,
      Long policyChgID) throws GenericException {
    BonusCalculateDAO dao = new BonusCalculateDAO();
    dao.cancelIncreaseSA(itemID, changeID, policyChgID);
  }
  /**
   * adjust bonus after change benefit 
   * @param itemID
   * @param changeID
   * @param policyChgID
   * @param oldProductID
   * @throws GenericException
   */
  public void changeBenefit(Long itemID, Long changeID, Long policyChgID,
      Long oldProductID) throws GenericException {
    BonusCalculateDAO dao = new BonusCalculateDAO();
    dao.changeBenefit(itemID, changeID, policyChgID, oldProductID);
  }
  /**
   * undo change benefit 
   * @param itemID
   * @param changeID
   * @param policyChgID
   * @param oldProductID
   * @throws GenericException
   */
  public void cancelChangeBenefit(Long itemID, Long changeID, Long policyChgID,
      Long oldProductID) throws GenericException {
    BonusCalculateDAO dao = new BonusCalculateDAO();
    dao.cancelChangeBenefit(itemID, changeID, policyChgID, oldProductID);
  }
  /**
   * adjust bonus after partial surrender
   * @param itemID
   * @param oldAmount
   * @param newAmount
   * @param changeID
   * @param policyChgID
   * @throws GenericException
   */
  public void calcSaPartSurr(java.lang.Long itemID,
      java.math.BigDecimal oldAmount, java.math.BigDecimal newAmount,
      java.lang.Long changeID, java.lang.Long policyChgID)
      throws GenericException {
    BonusCalculateDAO dao = new BonusCalculateDAO();
    dao.calcSaPartSurr(itemID, oldAmount, newAmount, changeID, policyChgID);
  }
  /**
   * undo partial surrender
   * @param itemID
   * @param changeID
   * @param policyChgID
   * @throws GenericException
   */
  public void cancelSaPartSurr(java.lang.Long changeID,
      java.lang.Long policyChgID, java.lang.Long itemID)
      throws GenericException {
    BonusCalculateDAO dao = new BonusCalculateDAO();
    dao.cancelSaPartSurr(changeID, policyChgID, itemID);
  }
  /**
   * undo clear RB
   * @param itemID
   * @param changeID
   * @param policyChgID
   * @throws GenericException
   */
  public void cancelClearRB(java.lang.Long itemID, java.lang.Long changeID,
      java.lang.Long policyChgID) throws GenericException {
    BonusCalculateDAO dao = new BonusCalculateDAO();
    dao.cancelClearRB(itemID, changeID, policyChgID);
  }
  /**
   * return some bonus info
   * @param itemID
   * @param calcDate
   * @param accuRB1
   * @param interimBonus
   * @param terminalBonus1
   * @param accuRB2
   * @param terminalBonus2
   * @param year1
   * @param year2
   * @throws GenericException
   */
  public BonusQuotationVO quotationCalcBonus(java.lang.Long itemID,
      java.util.Date calcDate, Date validateDate) throws GenericException {

    BonusCalculateDAO dao = new BonusCalculateDAO();
    return dao.quotationCalcBonus(itemID, calcDate, validateDate);
  }
@Override
public BigDecimal calcUnallocBonus4Claim(Long itemID, String payPlanType,
                Date eventDate) throws GenericException {
    BonusCalculateDAO dao = new BonusCalculateDAO();
    return dao.calcUnallocBonus4Claim(itemID, payPlanType, eventDate);
}
@Override
public BonusInfoRepVO getBonusInfo4Rep(Long policyId, Date anniDate)
                throws GenericException {
    BonusCalculateDAO dao = new BonusCalculateDAO();
    return dao.getBonusInfo4Rep(policyId, anniDate);
}


  
  

}