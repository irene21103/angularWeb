package com.ebao.ls.subprem.bs;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import com.ebao.ls.cs.ci.CSCI;
import com.ebao.ls.pa.pub.ci.PolicyCI;
import com.ebao.ls.pa.pub.service.PolicyService;
import com.ebao.ls.pa.pub.vo.PayerAccountVO;
import com.ebao.ls.pa.pub.vo.PolicyHolderVO;
import com.ebao.ls.pa.pub.vo.PolicyVO;
import com.ebao.ls.pty.ci.CustomerCI;
import com.ebao.ls.pty.vo.CompanyCustomerVO;
import com.ebao.ls.pty.vo.PartyVO;
import com.ebao.ls.pty.ds.vo.PersonSearchConditionVO;
import com.ebao.ls.pty.vo.CustomerVO;
import com.ebao.ls.pub.CodeCst;
import com.ebao.pub.annotation.PAPubAPIUpdate;
import com.ebao.pub.framework.ExceptionFactory;
import com.ebao.pub.framework.GenericDS;
import com.ebao.pub.framework.GenericException;
import com.ebao.pub.util.DateUtils;
import com.ebao.pub.util.Log;

/**
 * Title:SearchActionDSImpl.java Description:The implementation of
 * SearchActionDS,process the common search flow,such as
 * 'ByPolicyNumber','ByHolderName' etc. Copyright:copyright(c) 2004
 * Company:eBaoTech corporation
 * 
 * @Author:wei.tian
 * @Version 1.0
 */
public class SearchActionServiceImpl extends GenericDS
    implements
      SearchActionService {
  /**
   * Extract
   * 
   * @param pNumber String
   */
  // get policy by policy id
  @PAPubAPIUpdate("loadPolicyByPolicyId")
  public ArrayList getPolicyByPolicyNumber(String pNumber)
      throws GenericException {
    ArrayList list = new ArrayList();
    try {
      Long policyId;
      PolicyHolderVO policyHolder;
      String policyHolderName = "";
      String dueDate = "";
      String policyStatus = "";
      String payMethod = "";
      policyId = policyService.getPolicyIdByPolicyNumber(pNumber);
      PolicyVO policyVO = policyService.loadPolicyByPolicyId(policyId);
      if (policyVO != null) {
        policyId = policyVO.getPolicyId();
        List<PayerAccountVO> payeraccountVO;
        payeraccountVO = policyVO.getPayerAccounts();
        policyHolder = policyVO.getPolicyHolder();
        policyHolderName = getPolicyholderName(policyHolder);
        policyStatus = policyVO.getRiskStatus().toString();
        if (payeraccountVO != null && payeraccountVO.size() > 0) {
          payMethod = payeraccountVO.get(0).getPaymentMethod().toString();
        }
        Date d = policyCI.findPolicyDueDate(policyId);
        if (d != null) {
          dueDate = DateUtils.date2String(d);
        }
        HashMap hashMap = new HashMap();
        hashMap.put("policyNumberD", pNumber);
        hashMap.put("policyHolderNameD", policyHolderName);
        hashMap.put("dueDate", dueDate);
        hashMap.put("policyStatus", policyStatus);
        hashMap.put("payMethod", payMethod);
        list.add(hashMap);
      }
    } catch (Exception e) {
      throw ExceptionFactory.parse(e);
    }
    return list;
  }

  @PAPubAPIUpdate("loadPOlicyByPolicyId")
  public ArrayList getPolicyByPolicyNumberN0Escape(String pNumber)
      throws GenericException {
    ArrayList list = new ArrayList();
    try {
      Long policyId;
      PolicyHolderVO policyHolder;
      String policyHolderName = "";
      String dueDate = "";
      String policyStatus = "";
      String payMethod = "";
      policyId = policyService.getPolicyIdByPolicyNumber(pNumber);
      PolicyVO policyVO = policyService.loadPolicyByPolicyId(policyId);
      if (policyVO != null) {
        policyId = policyVO.getPolicyId();
        List<PayerAccountVO> payeraccountVO;
        // payeraccountVO = (PayerAccountVO[])
        // HubFactory.newApiInstance(PayerAccountVO.class,
        // SearchActionDSImpl.class);
        payeraccountVO = policyVO.getPayerAccounts();
        policyHolder = policyVO.getPolicyHolder();
        policyHolderName = getPolicyholderName(policyHolder);
        policyStatus = policyVO.getRiskStatus().toString();
        if (payeraccountVO != null && payeraccountVO.size() > 0) {
          payMethod = payeraccountVO.get(0).getPaymentMethod().toString();
        }
        Date d = policyCI.findPolicyDueDate(policyId);
        if (d != null) {
          dueDate = DateUtils.date2String(d);
        }
        HashMap hashMap = new HashMap();
        hashMap.put("policyNumberD", pNumber);
        hashMap.put("policyHolderNameD", policyHolderName);
        hashMap.put("dueDate", dueDate);
        hashMap.put("policyStatus", policyStatus);
        hashMap.put("payMethod", payMethod);
        list.add(hashMap);
      }
    } catch (Exception e) {
      throw ExceptionFactory.parse(e);
    }
    return list;
  }

  /**
   * Extract
   * 
   * @param PolicyHolderName String
   */
  // get policy by policy holder
  public ArrayList getPolicyByHolderName(String PolicyHolderName)
      throws GenericException {
    ArrayList list = new ArrayList();
    PersonSearchConditionVO icconditionVO = new PersonSearchConditionVO();
    List<CustomerVO> indivcustomerVO;
    try {
      Long policyHolderId;
      String policyHolderNameD = "";
      String partyIdTypeD = "";
      String partyIdNumberD = "";
      String dateOfBirthD = "";
      String gender = "";
      String[] partyName = PolicyHolderName.split(";");
      // icconditionVO = (IndivCustomerConditionVO)HubFactory.newApiInstance(
      // IndivCustomerConditionVO.class, SearchActionDSImpl.class);
      // icconditionVO.setFullName(PolicyHolderName);
      if (partyName.length == 1) {
        icconditionVO.setFirstName1(partyName[0]);
      }
      if (partyName.length > 1) {
        icconditionVO.setFirstName1(partyName[0]);
        icconditionVO.setLastName1(partyName[1]);
      }
      indivcustomerVO = customerCI.getPersonList(icconditionVO);
      // get policy
      if (indivcustomerVO != null) {
        for (CustomerVO personVO : indivcustomerVO) {
          policyHolderId = personVO.getCustomerId();
          policyHolderNameD = this.getPolicyholderName(policyHolderId);
          partyIdTypeD = personVO.getCertiType().toString();
          partyIdNumberD = personVO.getCertiCode();
          Date d = personVO.getBirthday();
          if (d != null) {
            dateOfBirthD = DateUtils.date2String(d);
          }
          gender = personVO.getGender();
          HashMap hashMap = new HashMap();
          hashMap.put("policyHolderId", policyHolderId.toString());
          hashMap.put("policyHolderNameD", policyHolderNameD);
          hashMap.put("partyIdTypeD", partyIdTypeD);
          hashMap.put("partyIdNumberD", partyIdNumberD);
          hashMap.put("dateOfBirthD", dateOfBirthD);
          hashMap.put("gender", gender);
          list.add(hashMap);
        }
      }
    } catch (GenericException e) {
      Log.error(this.getClass(), e);
    }
    return list;
  }

  protected String getPolicyholderName(Long id) {
    String spolicyholderName = "";
    try {
      if (id != null) {
        PartyVO partyVO = customerCI.getParty(id);
        if (partyVO.getPartyType().equals(CodeCst.PARTY_TYPE__INDIV_CUSTOMER)) { // individual
          // customer
          /*
           * MODIFICATION START by boyd.wu Oct 10, 2009 CRDB00369679 retrieve
           * correct party name from V_PARTY view
           */
          spolicyholderName = com.ebao.foundation.module.util.i18n.CodeTable
              .getCodeDesc("V_PARTY", String.valueOf(id));
          /*
           * MODIFICATION END by boyd.wu Oct 10, 2009 CRDB00369679
           */
        } else { // company customer
          CompanyCustomerVO organcustomerVO = customerCI.getCompany(id);
          spolicyholderName = organcustomerVO.getCompanyName();
        }
      }
    } catch (GenericException e) {
      Log.error(this.getClass(), e);
    }
    return spolicyholderName;
  }

  /**
   * Extract
   * 
   * @param PartyIdType String
   * @param PartyIdNumber String
   */
  // TODO: get policy by party info (party id and party id type)
  public ArrayList getPolicyByPartyInfo(String PartyIdType, String PartyIdNumber)
      throws GenericException {
    ArrayList list = new ArrayList();
    PersonSearchConditionVO icconditionVO = new PersonSearchConditionVO();
    List<CustomerVO> indivcustomerVO;
    Integer PIdType;
    try {
      Long policyHolderId;
      String policyHolderNameD = "";
      String partyIdTypeD = "";
      String partyIdNumberD = "";
      String dateOfBirthD = "";
      String gender = "";
      // icconditionVO = (IndivCustomerConditionVO)HubFactory.newApiInstance(
      // IndivCustomerConditionVO.class, SearchActionDSImpl.class);
      PIdType = Integer.valueOf(PartyIdType);
      icconditionVO.setCertiType(PIdType);
      icconditionVO.setCertiCode(PartyIdNumber);
      indivcustomerVO = customerCI.getPersonList(icconditionVO);
      // get policy
      if (indivcustomerVO != null) {
        for (CustomerVO personVO : indivcustomerVO) {
          policyHolderId = personVO.getCustomerId();
          policyHolderNameD = personVO.getFirstName() == null ? "" : personVO
              .getFirstName();
          partyIdTypeD = personVO.getCertiType().toString();
          partyIdNumberD = personVO.getCertiCode();
          Date d = personVO.getBirthday();
          if (d != null) {
            dateOfBirthD = DateUtils.date2String(d);
          }
          gender = personVO.getGender();
          HashMap hashMap = new HashMap();
          hashMap.put("policyHolderId", policyHolderId.toString());
          hashMap.put("policyHolderNameD", policyHolderNameD);
          hashMap.put("partyIdTypeD", partyIdTypeD);
          hashMap.put("partyIdNumberD", partyIdNumberD);
          hashMap.put("dateOfBirthD", dateOfBirthD);
          hashMap.put("gender", gender);
          list.add(hashMap);
        }
      }
    } catch (GenericException e) {
      Log.error(this.getClass(), e);
    }
    return list;
  }

  /**
   * Extract
   * 
   * @param PartyIdType String
   * @param PartyIdNumber String
   */
  // get policy by policy id
  public ArrayList getPolicyByHolderId(String PolicyHId)
      throws GenericException {
    ArrayList list = new ArrayList();
    try {
      Long policyId;
      Long policyHolderId;
      String policyNumber = "";
      String policyHolderName = "";
      String dueDate = "";
      String policyStatus = "";
      String payMethod = "";
      policyHolderId = Long.valueOf(PolicyHId);
      List<PolicyVO> policyVOs = policyCI.findByPolicyHolder(policyHolderId);
      // get policy
      if (policyVOs != null && policyVOs.size() > 0) {
        for (PolicyVO policyVO : policyVOs) {
          policyId = policyVO.getPolicyId();
          List<PayerAccountVO> payeraccountVO = policyVO.getPayerAccounts();
          policyNumber = policyVO.getPolicyNumber();
          PolicyHolderVO policyHolder = policyVO.getPolicyHolder();
          policyHolderName = getPolicyholderName(policyHolder);
          policyStatus = policyVO.getRiskStatus().toString();
          if (payeraccountVO != null && payeraccountVO.size() > 0) {
            payMethod = payeraccountVO.get(0).getPaymentMethod().toString();
          }
          Date d = policyCI.findPolicyDueDate(policyId);
          if (d != null) {
            dueDate = DateUtils.date2String(d);
          }
          HashMap hashMap = new HashMap();
          hashMap.put("policyNumberD", policyNumber);
          hashMap.put("policyHolderName", policyHolderName);
          hashMap.put("policyHolderNameD", policyHolderName);
          hashMap.put("dueDate", dueDate);
          hashMap.put("policyStatus", policyStatus);
          hashMap.put("payMethod", payMethod);
          hashMap.put("policyHolderId", policyHolderId);
          list.add(hashMap);
        }
      }
    } catch (Exception e) {
      Log.error(this.getClass(), e);
    }
    return list;
  }

  /**
   * Extract
   * 
   * @param id Long
   */
  protected String getPolicyholderName(PolicyHolderVO policyHolder) {
    String spolicyholderName = "";
    try {
      if (policyHolder != null) {
        PartyVO partyVO = policyHolder.getParty();
        if (partyVO.getPartyType().equals(CodeCst.PARTY_TYPE__INDIV_CUSTOMER)) { // individual
          // customer
          /*
           * MODIFICATION START by boyd.wu Oct 10, 2009 CRDB00369679 retrieve
           * correct party name from V_PARTY view
           */
          spolicyholderName = com.ebao.foundation.module.util.i18n.CodeTable
              .getCodeDesc("V_PARTY", String.valueOf(partyVO.getPartyId()));
          /*
           * MODIFICATION END by boyd.wu Oct 10, 2009 CRDB00369679
           */
        } else { // company customer
          CompanyCustomerVO organcustomerVO = policyHolder.getCompany();
          spolicyholderName = organcustomerVO.getCompanyName();
        }
      }
    } catch (GenericException e) {
      Log.error(this.getClass(), e);
    }
    return spolicyholderName;
  }

  @Resource(name = PolicyCI.BEAN_DEFAULT)
  private PolicyCI policyCI;

  @Resource(name = CSCI.BEAN_DEFAULT)
  private CSCI csCI;

  @Resource(name = CustomerCI.BEAN_DEFAULT)
  private CustomerCI customerCI;


  @Resource(name = PolicyService.BEAN_DEFAULT)
  private PolicyService policyService;
}
