package com.ebao.ls.uw.ctrl.decision;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.ebao.ls.uw.ctrl.pub.UwProductForm;

/**
 * <p>Title: GEL-UW</p>
 * <p>Description: Underwriting Product Decision View Form</p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: eBaoTech</p>
 * @author jason.luo
 * @version 1.0
 * @since 08.11.2004
 */

/**
 * Underwriting Product Decision View Form
 */
public class UwProductDecisionViewForm extends UwProductForm {
	
	
	
	/**
	 * benefit name
	 */
	// benefit id is itemId
	private String benefitName = "";

	/**
	 * benefit status
	 */
	private String benefitStatusName = "";

	/**
	 * Name of the life assured
	 */
	private String lifeAssuredName = "";

	/**
	 * Underwriting decision name
	 */
	private String uwDecisionName = "";

	/**
	 * Underwriting status name
	 */
	private String uwStatusName = "";

	/**
	 * Underwriting date String
	 */
	private String uwDateString = "";

	/** 高保額折扣 */
	private String bigAmountDiscnt = "";
	
	
	/**
	 * Default Constructor
	 */
	public UwProductDecisionViewForm() {

	}

	/**
	 * Returns benefit name
	 * 
	 * @return benefit name
	 */
	public String getBenefitName() {
		return benefitName;
	}

	/**
	 * Returns benefit status name
	 * 
	 * @return benefit status name
	 */
	public String getBenefitStatusName() {
		return benefitStatusName;
	}

	/**
	 * Returns name of LA
	 * 
	 * @return name of LA
	 */
	public String getLifeAssuredName() {
		return lifeAssuredName;
	}

	/**
	 * Sets benefit name
	 * 
	 * @param benefitName
	 *            benefit name
	 */
	public void setBenefitName(String benefitName) {
		this.benefitName = benefitName;
	}

	/**
	 * Sets benefit status name
	 * 
	 * @param benefitStatusName
	 *            benefit status name
	 */
	public void setBenefitStatusName(String benefitStatusName) {
		this.benefitStatusName = benefitStatusName;
	}

	/**
	 * Sets name of LA
	 * 
	 * @param lifeAssuredName
	 *            name of LA
	 */
	public void setLifeAssuredName(String lifeAssuredName) {
		this.lifeAssuredName = lifeAssuredName;
	}

	/**
	 * Returns decision name
	 * 
	 * @return decision name
	 */
	public String getUwDecisionName() {
		return uwDecisionName;
	}

	/**
	 * Sets decision name
	 * 
	 * @param uwDecisionName
	 *            decision name
	 */
	public void setUwDecisionName(String uwDecisionName) {
		this.uwDecisionName = uwDecisionName;
	}

	/**
	 * Sets status name
	 * 
	 * @param uwStatusName
	 *            status name
	 */
	public void setUwStatusName(String uwStatusName) {
		this.uwStatusName = uwStatusName;
	}

	/**
	 * Returns status name
	 * 
	 * @return uwStatusName status name
	 */
	public String getUwStatusName() {
		return uwStatusName;
	}

	/**
	 * Returns uw date(String representation)
	 * 
	 * @return uw date(String representation)
	 */
	public String getUwDateString() {
		return uwDateString;
	}

	
	/**
	 * Sets uw date(String representation)
	 * 
	 * @param uwDateString
	 *            uw date(String representation)
	 */
	public void setUwDateString(String uwDateString) {
		this.uwDateString = uwDateString;
	}

	/**
	 * @return 傳回 bigAmountDiscnt。
	 */
	public String getBigAmountDiscnt() {
		return bigAmountDiscnt;
	}

	/**
	 * @param bigAmountDiscnt 要設定的 bigAmountDiscnt。
	 */
	public void setBigAmountDiscnt(String bigAmountDiscnt) {
		this.bigAmountDiscnt = bigAmountDiscnt;
	}

	

	

	
	
}
