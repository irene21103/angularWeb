package com.ebao.ls.callout.ctrl;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.foundation.module.para.Para;
import com.ebao.ls.callout.batch.CalloutConstants;
import com.ebao.ls.pa.pub.service.PolicyService;
import com.ebao.ls.pty.ds.DeptService;
import com.ebao.ls.pty.service.PartyService;
import com.ebao.ls.pty.vo.DeptVO;
import com.ebao.ls.pub.CodeCst;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.framework.GenericAction;

public class CalloutConfigAction extends GenericAction {

    private static Logger logger = Logger.getLogger(CalloutConfigAction.class);

    @Resource(name = DeptService.BEAN_DEFAULT)
    private DeptService deptDS;

    @Resource(name = PolicyService.BEAN_DEFAULT)
    private PolicyService policyService;

    @Resource(name = PartyService.BEAN_DEFAULT)
    private PartyService partyService;

    @Resource(name = DeptService.BEAN_DEFAULT)
    private DeptService DService;

    @Override
    public ActionForward process(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                    HttpServletResponse response) throws Exception {
        Long userCategory = partyService.getBizCategory(AppContext.getCurrentUser().getUserId());
        if (userCategory == CodeCst.BIZ_CATEGORY_UNB) { // UNB
            return mapping.findForward("unb");
        } else if (userCategory == CodeCst.BIZ_CATEGORY_POS) { // POS

            String deptother = Para.getParaValue(CalloutConstants.DEPT080);
            String[] dept080 = deptother.split(",");
            for (int i = 0; i < dept080.length; i++) {
                DeptVO vo = DService.getDeptById(AppContext.getCurrentUser()
                                .getDeptId());
                if (vo.getDeptCode().equals(dept080[i])) {
                    return mapping.findForward("080");
                }
            }
            return mapping.findForward("pos");
        } else {
            return mapping.findForward("080");
            // return mapping.findForward("unb");
            // return mapping.findForward("pos");
        }

    }

}
