package com.ebao.ls.uw.ctrl.info;

import java.util.Date;
import java.util.List;

public class InsuredImageRecord {
	private Integer cardId;

	private Date scanTime;

	private List<Long> imageIds;

	private String imageIdStr;

	public Integer getCardId() {
		return cardId;
	}

	public void setCardId(Integer cardId) {
		this.cardId = cardId;
	}

	public Date getScanTime() {
		return scanTime;
	}

	public void setScanTime(Date scanTime) {
		this.scanTime = scanTime;
	}

	public List<Long> getImageIds() {
		return imageIds;
	}

	public void setImageIds(List<Long> imageIds) {
		this.imageIds = imageIds;
	}

	public String getImageIdStr() {
		return imageIdStr;
	}

	public void setImageIdStr(String imageIdStr) {
		this.imageIdStr = imageIdStr;
	}

}
