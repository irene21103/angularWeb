package com.ebao.ls.uw.ctrl.decision;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.UserTransaction;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.foundation.module.db.hibernate.HibernateSession3;
import com.ebao.foundation.module.web.action.ExceptionInfoUtils;
import com.ebao.ls.cs.commonflow.ctrl.letter.CsUWIssuesActionHelper;
import com.ebao.ls.escape.helper.EscapeHelper;
import com.ebao.ls.letter.common.CommonLetterService;
import com.ebao.ls.notification.message.WSLetterUnit;
import com.ebao.ls.pa.nb.ctrl.letter.NbLetterHelper;
import com.ebao.ls.pa.nb.ctrl.letter.NbNotificationHelper;
import com.ebao.ls.pa.nb.ctrl.pub.ajax.JSONUtils;
import com.ebao.ls.pa.nb.util.NBUtils;
import com.ebao.ls.pa.pub.bs.ExtraPremService;
import com.ebao.ls.pa.pub.bs.PolicyHolderService;
import com.ebao.ls.pa.pub.bs.PolicyService;
import com.ebao.ls.pa.pub.bs.coverage.CoverageService;
import com.ebao.ls.pa.pub.data.bo.Insured;
import com.ebao.ls.pa.pub.data.org.InsuredDao;
import com.ebao.ls.pub.cst.Template;
import com.ebao.ls.uw.ctrl.UwGenericAction;
import com.ebao.ls.uw.ctrl.ajax.UwExtraPremAjaxService;
import com.ebao.ls.uw.ctrl.extraloading.ExtraLoadingActionHelper;
import com.ebao.ls.uw.ctrl.extraloading.UwProductExtraPremForm;
import com.ebao.ls.uw.ctrl.pub.UwActionHelper;
import com.ebao.ls.uw.ds.UwPolicyService;
import com.ebao.ls.uw.vo.UwExtraLoadingVO;
import com.ebao.pub.annotation.PrdAPIUpdate;
import com.ebao.pub.framework.ExceptionFactory;
import com.ebao.pub.framework.GenericException;
import com.ebao.pub.util.Trans;
import com.ebao.pub.util.TransUtils;

/**
 * <p>
 * Title: 核保作業
 * </p>
 * <p>
 * Description:保項加費
 * </p>
 * <p>
 * Copyright: Copyright (c) 2015
 * </p>
 * <p>
 * Company: TGL Co., Ltd.
 * </p>
 * <p>
 * Create Time: Jul 15, 2015
 * </p>
 * 
 * @author <p>
 *         Update Time: Jul 15, 2015
 *         </p>
 *         <p>
 *         Updater: simon.huang
 *         </p>
 *         <p>
 *         Update Comments:
 *         </p>
 */
public class UwExtraPremShowAction extends UwGenericAction {

    public static final String BEAN_DEFAULT = "/uwExtraPremShow";

    @Resource(name = UwPolicyService.BEAN_DEFAULT)
    protected UwPolicyService uwPolicyDS;

    @Resource(name = PolicyService.BEAN_DEFAULT)
    private PolicyService policyService;

    @Resource(name = PolicyHolderService.BEAN_DEFAULT)
    protected PolicyHolderService policyHolderService;

    @Resource(name = UwActionHelper.BEAN_DEFAULT)
    protected UwActionHelper uwActionHelper;

    @Resource(name = ExtraLoadingActionHelper.BEAN_DEFAULT)
    protected ExtraLoadingActionHelper extraLoadingActionHelper;

    @Resource(name = NbNotificationHelper.BEAN_DEFAULT)
    private NbNotificationHelper nbNotificationHelper;

    @Resource(name = CommonLetterService.BEAN_DEFAULT)
    protected CommonLetterService commonLetterService;

    @Resource(name = NbLetterHelper.BEAN_DEFAULT)
    private NbLetterHelper nbLetterHelper;

    @Resource(name = CoverageService.BEAN_DEFAULT)
    private CoverageService coverageService;

    @Resource(name = ExtraPremService.BEAN_DEFAULT)
    private ExtraPremService extraPremService;

    @Resource(name = InsuredDao.BEAN_DEFAULT)
    private InsuredDao<Insured> insuredDao;

    @Resource(name = CsUWIssuesActionHelper.BEAN_DEFAULT)
    private CsUWIssuesActionHelper csUWIssuesActionHelper;

    @Resource(name = UwExtraPremAjaxService.BEAN_DEFAULT)
    private UwExtraPremAjaxService uwExtraPremAjaxService;

    public static String previewNewLetter = "previewNew";

    Logger log4j = Logger.getLogger(getClass());

    /**
     * @param mapping
     * @param form
     * @param request
     * @param res
     * @return
     * @throws GenericException
     */
    @Override
    @PrdAPIUpdate
    public ActionForward uwProcess(ActionMapping mapping, ActionForm form, HttpServletRequest req, HttpServletResponse res) throws GenericException {
	try {
	    UwProductExtraPremForm actionForm = (UwProductExtraPremForm) form;

	    String isIframe = com.ebao.ls.encode.encoder.Encode.encodeForHTML(req).getParameter("isIframe");	    actionForm.setIsIframe(isIframe);
	    actionForm.setCsUw(Boolean.parseBoolean(com.ebao.ls.encode.encoder.Encode.encodeForHTML(req).getParameter("isCsUw")));
	    String subAction = actionForm.getSubAction();
	    if (previewNewLetter.equals(subAction)) {

		String json = com.ebao.ls.encode.encoder.Encode.encodeForHTML(req).getParameter("jsonData");
		Map<String, Object> in = JSONUtils.parseJSON2Map(json);
		Map<String, Object> out = new HashMap<String, Object>();

		String reqFrom = com.ebao.ls.encode.encoder.Encode.encodeForHTML(req).getParameter("reqFrom");
		Long policyId = Long.valueOf(com.ebao.ls.encode.encoder.Encode.encodeForHTML(req).getParameter("policyId"));
		Long underwriteId = Long.valueOf(com.ebao.ls.encode.encoder.Encode.encodeForHTML(req).getParameter("underwriteId"));		WSLetterUnit unit = null;
		Long templateId = Template.TEMPLATE_20024.getTemplateId();

		// 2017/08/30 simon-RTC 188203-調整加費單預覽不執行存檔，使用頁面資料產出PDF
		UserTransaction ut = null;
		try {
		    ut = Trans.getUserTransaction();
		    ut.begin();

		    in.put("preview", true);
		    uwExtraPremAjaxService.saveUwExtraPrem(in, out);
		    HibernateSession3.currentSession().flush();

		    // 斷開session重新查詢policy,不然會得到hibernate cache中的policy
		    HibernateSession3.detachSession();
		    // 保全核保 或 保全次標加費
		    if (actionForm.isCsUw() || StringUtils.equals(reqFrom, "pos")) {
			Long changeId = Long.valueOf(com.ebao.ls.encode.encoder.Encode.encodeForHTML(req).getParameter("changeId"));

			unit = csUWIssuesActionHelper.prepareExtensionVOPos0040Preview(policyId, changeId, underwriteId);
			templateId = csUWIssuesActionHelper.TEMPLATE_ID__EXTRA_PREM;
		    } else {
			unit = uwActionHelper.getFmtUnb0441Letter(policyId, underwriteId);
		    }

		    // 2017/08/30 simon-RTC 188203-調整加費單預覽不執行存檔，rollback寫入資料
		    TransUtils.rollback(ut);
		} catch (Exception e) {
		    log4j.warn(ExceptionInfoUtils.getExceptionMsg(e));
		    TransUtils.rollback(ut);
		}

		try {
		    /** 信函預覽 **/
		    commonLetterService.previewLetterSetupPDFHeader(req, res, templateId, unit);
		} catch (Exception e) {
		    com.ebao.pub.util.Log.error(this.getClass(), e.getMessage());
		    req.setAttribute("error", NBUtils.getTWMsg("MSG_1262038"));
		    return mapping.findForward("previewError");
		}

		return null;
	    } else {
		// add by tgl151 pos-頁面進入
		String reqFrom = com.ebao.ls.encode.encoder.Encode.encodeForHTML(req.getParameter("reqFrom"));
		String changeId = com.ebao.ls.encode.encoder.Encode.encodeForHTML(req.getParameter("changeId"));
		String policyChgId = com.ebao.ls.encode.encoder.Encode.encodeForHTML(req.getParameter("policyChgId"));
		String validDate = com.ebao.ls.encode.encoder.Encode.encodeForHTML(req.getParameter("validDate"));

		Boolean csUw = actionForm.isCsUw();
		if ((reqFrom != null && reqFrom.equals("pos")) || csUw) {
		    String policyId = com.ebao.ls.encode.encoder.Encode.encodeForHTML(req.getParameter("policyId"));
		    String underwriteId = com.ebao.ls.encode.encoder.Encode.encodeForHTML(req.getParameter("underwriteId"));
		    String itemId = com.ebao.ls.encode.encoder.Encode.encodeForHTML(req.getParameter("itemId"));

		    actionForm.setPolicyId(Long.parseLong(policyId));
		    actionForm.setUnderwriteId(Long.parseLong(underwriteId));
		    com.ebao.ls.encode.encoder.Encode.encodeForHTML(req).setAttribute("reqFrom", reqFrom);
			com.ebao.ls.encode.encoder.Encode.encodeForHTML(req).setAttribute("changeId", changeId);
			com.ebao.ls.encode.encoder.Encode.encodeForHTML(req).setAttribute("policyChgId", policyChgId);
			com.ebao.ls.encode.encoder.Encode.encodeForHTML(req).setAttribute("itemId", itemId);
			com.ebao.ls.encode.encoder.Encode.encodeForHTML(req).setAttribute("validDate", validDate);
		}
	    }
	    return mapping.findForward("display");
	} catch (Exception ex) {
	    throw ExceptionFactory.parse(ex);
	}
    }

    private boolean checkUwExtraPrem(UwProductExtraPremForm form) {
	boolean flag = true;
	Collection<UwExtraLoadingVO> allNewExtra = uwPolicyDS.findUwExtraLoadingEntitis(form.getUnderwriteId());
	if (allNewExtra == null || allNewExtra.isEmpty()) {
	    flag = false;
	}
	return flag;
    }
}