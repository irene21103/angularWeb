package com.ebao.ls.liaRoc.batch;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.transaction.UserTransaction;

import org.apache.commons.collections.CollectionUtils;

import com.ebao.foundation.module.web.action.ExceptionInfoUtils;
import com.ebao.ls.cs.logger.ApplicationLogger;
import com.ebao.ls.cs.logger.helper.AppLogDataHelper;
import com.ebao.ls.foundation.vo.GenericEntityVO;
import com.ebao.ls.liaRoc.LiaRocCst;
import com.ebao.ls.liaRoc.bo.LiaRocNbBatchVO;
import com.ebao.ls.liaRoc.bo.LiaRocUpload;
import com.ebao.ls.liaRoc.bo.LiaRocUploadCoverage;
import com.ebao.ls.liaRoc.bo.LiaRocUploadDetail;
import com.ebao.ls.liaRoc.bo.LiaRocUploadDetailVO;
import com.ebao.ls.liaRoc.ci.LiaRocNbBatchService;
import com.ebao.ls.liaRoc.ci.LiaRocUpload2019CI;
import com.ebao.ls.liaRoc.data.LiaRocNbBatchDao;
import com.ebao.ls.liaRoc.data.LiaRocUploadCoverageDao;
import com.ebao.ls.liaRoc.data.LiaRocUploadDao;
import com.ebao.ls.liaRoc.data.LiaRocUploadDetailDao;
import com.ebao.ls.liaRoc.vo.LiaRocUploadDetailBaseVO;
import com.ebao.ls.liaRoc.vo.LiaRocUploadDetailWrapperVO;
import com.ebao.ls.liaRoc.vo.LiaRocUploadSendVO;
import com.ebao.ls.pa.nb.util.NBUtils;
import com.ebao.ls.pa.pub.bs.coverage.CoverageService;
import com.ebao.ls.pa.pub.vo.CoverageVO;
import com.ebao.ls.pa.pub.vo.PolicyVO;
import com.ebao.ls.pub.CodeCst;
import com.ebao.pub.batch.exe.BatchDB;
import com.ebao.pub.batch.help.BatchHelp;
import com.ebao.pub.batch.type.JobStatus;
import com.ebao.pub.fileresolver.writer.FixedCharLengthWriter;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.util.BeanUtils;
import com.ebao.pub.util.DateUtils;
import com.ebao.pub.util.StringUtils;
import com.ebao.pub.util.Trans;

/**
 * <h1>新契約預收輸入公會通報資料上傳 batch - 收件</h1><p>
 * @since 2019/10/18<p>
 * @author Kathy Yeh
 * <p>Update Time: </p>
 * <p>Updater: </p>
 * <p>Update Comments: </p>
*/
public class LiaRocNbTransferBatch extends LiaRocUploadBatch {
	
    @Resource(name = LiaRocNbBatchService.BEAN_DEFAULT)
    private LiaRocNbBatchService liaRocNbBatchService;
    
    @Resource(name = LiaRocUpload2019CI.BEAN_DEFAULT)
    private LiaRocUpload2019CI liaRocUpload2019CI;
    
    @Resource(name = LiaRocNbBatchDao.BEAN_DEFAULT)
    private LiaRocNbBatchDao liaRocNbBatchDao;
    
	@Resource(name = LiaRocUploadDetailDao.BEAN_DEFAULT)
	private LiaRocUploadDetailDao liaRocUploadDetailDao;
	
	@Resource(name = CoverageService.BEAN_DEFAULT)
	protected CoverageService coverageService;
	
	@Resource(name = LiaRocUploadReceiveBatch.BEAN_DEFAULT)
	protected LiaRocUploadReceiveBatch liaRocUploadReceiveBatch;

	@Resource(name = com.ebao.ls.pa.pub.bs.PolicyService.BEAN_DEFAULT)
	private com.ebao.ls.pa.pub.bs.PolicyService policyDS;
 
	@Resource(name = LiaRocUploadDao.BEAN_DEFAULT)
	private LiaRocUploadDao liaRocUploadDao;
	
	@Resource(name = LiaRocUploadCoverageDao.BEAN_DEFAULT)
	private LiaRocUploadCoverageDao liaRocUploadCoverageDao;
	
	public static final String BEAN_DEFAULT = "liaRocNbTransferBatch";

    @Override
    protected String getUploadType() {
        return LiaRocCst.LIAROC_UL_TYPE_RECEIVE;
    }

    @Override
    protected String getUploadModule() {
        return LiaRocCst.LIAROC_UL_UPLOAD_TYPE_NB;
    }

    @Override
    public int mainProcess() throws Exception {
    	
    	ApplicationLogger.clear();
		ApplicationLogger.setSystemCode(AppLogDataHelper.SYSTEM_CODE_UNB);
		ApplicationLogger.setJobName("新契約預收輸入公會收件通報排程");
		ApplicationLogger.setPolicyCode("LiarocNbBatch");
		ApplicationLogger.setOptionId(DateUtils.date2String(new Date(), "yyyyMMdd HHmmss"));

		if (BatchDB.getSubId() != null) {
			ApplicationLogger.addLoggerData("[INFO]Batch Job Starting.. RunId=" + BatchHelp.getRunId());
		}

		//先處理ID/生日變更通報12/15公會收件通報的資料
		int result02 = this.changeType02Process();
		
		//處理預收輸入尚未做公會收件通報的資料(1 key)
		int result01 = this.changeType01Process();

		//刪除險種通報06
		int result04 = this.changeType04Process();
		
		//新增險種通報01
		int result03 = this.changeType03Process();

		//變更險種，舊險種通報06，新險種通報01
		int result05 = this.changeType05Process();
		
		//13-承保UNDO後通報承保06，當天又承保
		int result13 = this.changeType13Process();
		
		//14-撤件UNDO後通報收件01，當天又撤件
		int result14 = this.changeType14Process();
		
        ApplicationLogger.flush();

		if ((result01 == JobStatus.EXECUTE_FAILED) || 
			(result02 == JobStatus.EXECUTE_FAILED) ||
			(result03 == JobStatus.EXECUTE_FAILED) ||
			(result04 == JobStatus.EXECUTE_FAILED) ||
			(result05 == JobStatus.EXECUTE_FAILED) ||
			(result13 == JobStatus.EXECUTE_FAILED) || 
			(result14 == JobStatus.EXECUTE_FAILED) ) {
			return com.ebao.pub.batch.type.JobStatus.EXECUTE_PARTIAL_SUCESS;
		}

		return com.ebao.pub.batch.type.JobStatus.EXECUTE_SUCCESS;
    }

    /**
     * 處理預收輸入尚未做公會收件通報的資料(1 key)，通報01
     */
    protected int changeType01Process() throws Exception {
        // 批處理狀況預設為成功
        int executeResult = JobStatus.EXECUTE_SUCCESS;
        
    	// 依序處理控制檔
		int errorCount = 0;
		int succCount = 0;
		String changeType = LiaRocCst.LIAROC_NB_BATCH_CHANGE_TYPE_01;
        UserTransaction trans = null;

		
		//取得預收輸入尚未做公會收件通報的資料
		List<LiaRocNbBatchVO> nbBatchVOs = liaRocNbBatchService.getLiaRocNbBatchData(changeType);

    	for(LiaRocNbBatchVO nbBatchVO : nbBatchVOs) {
    		try {
	    		trans = Trans.getUserTransaction();
				trans.begin();

	    		if ((CodeCst.SUBMIT_CHANNEL__MPOS.equals(nbBatchVO.getSubmitChannel())) 
	    		    && (NBUtils.isBRBD(nbBatchVO.getChannelType().toString()))) {
	    			// mpos外部通路(BR,BD)進件時不申報死亡給付險種(99)，由Afins進件做申報
	    			try{
		    			Long uploadId = liaRocUpload2019CI.sendmPosNB(nbBatchVO.getPolicyId(),LiaRocCst.LIAROC_UL_TYPE_RECEIVE);
		    			if (uploadId != null) {
		    				nbBatchVO.setCheckStatus(LiaRocCst.LIAROC_NB_BATCH_CHECK_STATUS_YES);
		    				nbBatchVO.setUploadListId(uploadId);
		    				liaRocNbBatchService.save(nbBatchVO);
		    				succCount++;
		    			}
	    			}catch (Throwable e) {
	    				nbBatchVO.setCheckStatus(LiaRocCst.LIAROC_NB_BATCH_CHECK_STATUS_ERROR);
	    				liaRocNbBatchService.save(nbBatchVO);
	    				errorCount++;
	    				String message = ExceptionInfoUtils.getExceptionMsg(e);
	    	        	ApplicationLogger.addLoggerData("[ERROR]call liaRocUploadCI.sendmPosNB 發生不可預期的錯誤, "
	    	        			+ "PolicyId:" + nbBatchVO.getPolicyId() 
	    	        			+ "Error Message:" + message);
	    			}
	    		}else{
	                // 一般NB收件通報
	    			try {
		                Long uploadId = liaRocUpload2019CI.sendNB(nbBatchVO.getPolicyId(),LiaRocCst.LIAROC_UL_TYPE_RECEIVE);
		    			if (uploadId != null) {
		    				nbBatchVO.setCheckStatus(LiaRocCst.LIAROC_NB_BATCH_CHECK_STATUS_YES);
		    				nbBatchVO.setUploadListId(uploadId);
		    				liaRocNbBatchService.save(nbBatchVO);
		    				succCount++;
		    			}
	    			}catch (Throwable e) {
	    				nbBatchVO.setCheckStatus(LiaRocCst.LIAROC_NB_BATCH_CHECK_STATUS_ERROR);
	    				liaRocNbBatchService.save(nbBatchVO);
	    				errorCount++;
	    	        	String message = ExceptionInfoUtils.getExceptionMsg(e);
	    	        	ApplicationLogger.addLoggerData("[ERROR]call liaRocUploadCI.sendNB 發生不可預期的錯誤, "
	    	        			+ "PolicyId:" + nbBatchVO.getPolicyId() 
	    	        			+ "Error Message:" + message);
	    			}
	    		}
	    		trans.commit();
			} catch (Throwable e) {
	        	trans.rollback();
	        	String message = ExceptionInfoUtils.getExceptionMsg(e);
	        	ApplicationLogger.addLoggerData("[ERROR]changeType01Process發生不可預期的錯誤, Error Message:" + message);
	            executeResult = JobStatus.EXECUTE_FAILED;
	            ApplicationLogger.setReturnCode(ApplicationLogger.RETURN_CODE_FAIL);
			}
        }

		ApplicationLogger.addLoggerData("[INFO] 預收輸入批次寫入公會收件上傳檔(1Key)" 
				+ ";寫檔處理,成功=" + succCount + ",失敗=" + errorCount);

		return executeResult;
    }

    /**
     * 處理被保人ID/生日變更產生通報12/15公會收件通報的資料
     */
    protected int changeType02Process() throws Exception {
		
        // 批處理狀況預設為成功
        int executeResult = JobStatus.EXECUTE_SUCCESS;
    	
    	// 依序處理控制檔
		int errorCount = 0;
		int succCount = 0;
		String changeType = LiaRocCst.LIAROC_NB_BATCH_CHANGE_TYPE_02;
        UserTransaction trans = null;

		//取得被保人ID/生日變更資料
		List<LiaRocNbBatchVO> nbBatchVOs = liaRocNbBatchService.getLiaRocNbBatchData(changeType);

    	for(LiaRocNbBatchVO nbBatchVO : nbBatchVOs) {

    		try {
	    		trans = Trans.getUserTransaction();
				trans.begin();
				
				PolicyVO policyVO = policyDS.retrieveById(nbBatchVO.getPolicyId(), false);
				
				//比對ID不同或是生日不同才要做12/15更正通報
				if (!nbBatchVO.getCertiCodeBf().equals(nbBatchVO.getCertiCodeAf()) ||
						!nbBatchVO.getBirthDateBf().equals(nbBatchVO.getBirthDateAf())) {
					// make 公會上傳主檔VO
					LiaRocUploadSendVO uploadVO = liaRocUpload2019CI.createLiarocUploadMaster(policyVO,
							LiaRocCst.LIAROC_UL_TYPE_RECEIVE, LiaRocCst.LIAROC_UL_BIZ_SOURCE_UNB);

					List<LiaRocUploadDetailVO> fixnewDetails = new ArrayList<LiaRocUploadDetailVO>();

					boolean isStatus06 = false;
					// 以變更前被保人ID 取出最新一筆公會收件通報資料執行通報12-鍵值欄位通報錯誤終止
					List<LiaRocUploadDetail> lastUploadDetails = liaRocUploadDetailDao
							.queryLastReceiveUploadDetailByCertiCode(String.valueOf(nbBatchVO.getChannelType()),
									policyVO.getPolicyNumber(), nbBatchVO.getCertiCodeBf(), nbBatchVO.getApplyDate(),
									policyVO.getSerialNum(), null, isStatus06);

					// 確認有曾做過公會收件通報，執行通報15-通報更正
					if (CollectionUtils.isNotEmpty(lastUploadDetails)) {

						for (LiaRocUploadDetail liaRocUploadDetail : lastUploadDetails) {

							LiaRocUploadDetail newDetailBO = new LiaRocUploadDetail();
							BeanUtils.copyProperties(newDetailBO, liaRocUploadDetail);

							LiaRocUploadDetail detailBo12 = this.createFixUploadDetail(liaRocUploadDetail, null,
									LiaRocCst.LIAROC_POLICY_STATUS__ERROR_TERMINATION);
							LiaRocUploadDetail detailBo15 = this.createFixUploadDetail(newDetailBO, null,
									LiaRocCst.LIAROC_POLICY_STATUS__FIX_ERROR);
							LiaRocUploadDetailVO detailVO12 = new LiaRocUploadDetailVO();
							LiaRocUploadDetailVO detailVO15 = new LiaRocUploadDetailVO();
							detailBo12.copyToVO(detailVO12, Boolean.TRUE);
							detailBo15.copyToVO(detailVO15, Boolean.TRUE);
							if (!StringUtils.isNullOrEmpty(nbBatchVO.getCertiCodeAf())) {
								detailVO15.setCertiCode(nbBatchVO.getCertiCodeAf());
							}
							if (nbBatchVO.getBirthDateAf() != null) {
								detailVO15.setBirthday(nbBatchVO.getBirthDateAf());
							}
							detailVO12.setStatusEffectDate(DateUtils.truncateDay(AppContext.getCurrentUserLocalTime()));
							detailVO15.setStatusEffectDate(DateUtils.truncateDay(AppContext.getCurrentUserLocalTime()));
							//IR-376932 取出上筆公會通報資料後把通報狀態check_status = 1-可上傳，清空RefUploadListId
							detailVO12.setCheckStatus(LiaRocCst.CHECK_STATUS_UPLOAD);
							detailVO12.setRefUploadListId(null);
							detailVO15.setCheckStatus(LiaRocCst.CHECK_STATUS_UPLOAD);			
							detailVO15.setRefUploadListId(null);
							fixnewDetails.add(detailVO12);
							fixnewDetails.add(detailVO15);
						}
					}
					uploadVO.setDataList(fixnewDetails);
					// 寫入公會收件通報上傳主檔和明細檔
					liaRocUpload2019CI.saveUpload(uploadVO);

					if (uploadVO.getListId() != null) {
						nbBatchVO.setCheckStatus(LiaRocCst.LIAROC_NB_BATCH_CHECK_STATUS_YES);
						nbBatchVO.setUploadListId(uploadVO.getListId());
						liaRocNbBatchService.save(nbBatchVO);
						succCount++;
					} else {
						errorCount++;
					}
				}else{
					//變更前、變更後ID若一樣就不做收件通報處理
					nbBatchVO.setCheckStatus(LiaRocCst.LIAROC_NB_BATCH_CHECK_STATUS_Z);
					liaRocNbBatchService.save(nbBatchVO);
					succCount++;
				}
				trans.commit();
    		} catch (Throwable e) {
	        	trans.rollback();
	        	String message = ExceptionInfoUtils.getExceptionMsg(e);
	        	ApplicationLogger.addLoggerData("[ERROR]changeType02Process發生不可預期的錯誤, Error Message:" + message);
	            executeResult = JobStatus.EXECUTE_FAILED;
	            ApplicationLogger.setReturnCode(ApplicationLogger.RETURN_CODE_FAIL);
			} 
    	}

		ApplicationLogger.addLoggerData("[INFO] 預收輸入批次寫入公會收件上傳檔(ID/生日變更)" 
				+ ";寫檔處理,成功=" + succCount + ",失敗=" + errorCount);

		return executeResult;
    }

    /**
     * 處理新增險種需產生通報01公會收件通報的資料
     */
    protected int changeType03Process() throws Exception {

        // 批處理狀況預設為成功
        int executeResult = JobStatus.EXECUTE_SUCCESS;

    	// 依序處理控制檔
		int errorCount = 0;
		int succCount = 0;
		String changeType = LiaRocCst.LIAROC_NB_BATCH_CHANGE_TYPE_03;
        UserTransaction trans = null;
        
		//取得新增險種未處理資料
		List<LiaRocNbBatchVO> nbBatchVOs = liaRocNbBatchService.getLiaRocNbBatchData(changeType);
		//資料重整為 Map<ruleName,List<policyId>>
		Map<String, List<LiaRocNbBatchVO>> nbBatchMap = NBUtils.toMapList(nbBatchVOs, "policyId");
    	
		 //by policyId group
		 for(String policyId : nbBatchMap.keySet() ) {
			 try{
	    		trans = Trans.getUserTransaction();
				trans.begin();
	
				PolicyVO policyVO = policyDS.retrieveById(Long.parseLong(policyId), false);
				//make 公會上傳主檔VO
				LiaRocUploadSendVO uploadVO = liaRocUpload2019CI.createLiarocUploadMaster
						(policyVO, LiaRocCst.LIAROC_UL_TYPE_RECEIVE, LiaRocCst.LIAROC_UL_BIZ_SOURCE_UNB);
	
				List<LiaRocUploadDetailVO> newDetails = new ArrayList<LiaRocUploadDetailVO>();
				
		    	for(LiaRocNbBatchVO nbBatchVO : nbBatchMap.get(policyId)) {
					List<CoverageVO>coverageList  = new ArrayList<CoverageVO>();
		    		CoverageVO coverageVO = coverageService.load(nbBatchVO.getChangeItemId());
					coverageList.add(coverageVO);
					
					LiaRocUploadDetailVO detailVO01 = new LiaRocUploadDetailVO();
					//產出公會上傳明細檔
					List<LiaRocUploadDetailWrapperVO> genUploadDetails = liaRocUpload2019CI.generateNBBatchUploadDetail(
									policyVO, coverageList, LiaRocCst.LIAROC_UL_TYPE_RECEIVE);

					//執行通報01-有效
					if (CollectionUtils.isNotEmpty(genUploadDetails)) {
						detailVO01 = genUploadDetails.get(0);

						//IR-359765 新增被保險人附約(含眷屬)如跳出視窗點選確認時應以變更的日期通報要保書填寫日
						if (nbBatchVO.getSystemDate() != null) {
							detailVO01.setValidateDate(nbBatchVO.getSystemDate());
							detailVO01.setStatusEffectDate(nbBatchVO.getSystemDate());
						}
					}					
					
					boolean isUnb = true;
					boolean is1Key = true;
					//PCR-337603 比對保單狀態是否需要含06-撤件 2020/11/20 Add by Kathy
					boolean isLiaRocPolicyStatus06 = true;
					
					//以新增險種取出最近一筆有無公會收件通報資料
					List<LiaRocUploadDetail> lastUploadDetails = liaRocUploadDetailDao.queryLastReceiveUploadDetail(
									isUnb, 
									String.valueOf(nbBatchVO.getChannelType()),
									policyVO.getPolicyNumber(), 
									nbBatchVO.getCertiCodeBf(), 
									nbBatchVO.getInternalIdBf(), 
									nbBatchVO.getApplyDate(), 
									policyVO.getSerialNum(),
									is1Key,
									isLiaRocPolicyStatus06);
					
					//已通報此處不作通報
					if (lastUploadDetails.size() != 0) {
						LiaRocUploadDetail lastUploadDetail = lastUploadDetails.get(0);
						//上一筆通報狀態不為06-撤件
						if (!LiaRocCst.LIAROC_POLICY_STATUS__FREELOOK.equals(lastUploadDetail.getLiaRocPolicyStatus())){
							//更新狀態為2-不通報，它件已通報
							detailVO01.setCheckStatus(LiaRocCst.CHECK_STATUS_CANCEL_BY_AFINS);
							detailVO01.setRefUploadListId(lastUploadDetail.getListId());
							detailVO01.setPolicyId(lastUploadDetail.getPolicyId());
						}
					}
					
					//更新公會通報保單取號檔(T_LIAROC_UPLOAD_COVERAGE)
					LiaRocUploadCoverage uploadCoverage = null;
					if (nbBatchVO.getPolicyId() != null && nbBatchVO.getPolicyId().longValue() > 0L) {
						uploadCoverage = liaRocUploadCoverageDao.findByPolicyIdLiaRocPolicyCode(nbBatchVO.getPolicyId(), detailVO01.getPolicyId());
					}
					if (uploadCoverage != null) {
						//將本次保項編號(item_id)回寫T_LIAROC_UPLOAD_COVERAGE.ITEM_ID
						uploadCoverage.setItemId(nbBatchVO.getChangeItemId());
						liaRocUploadCoverageDao.saveorUpdate(uploadCoverage);
					}
					
					if (detailVO01.getPolicyId() != null){
						newDetails.add(detailVO01);
						succCount++;
					}else{
						errorCount++;
					}
			
					uploadVO.setDataList(newDetails);
		    	}
				//寫入公會收件通報上傳主檔和明細檔
		    	liaRocUpload2019CI.saveUpload(uploadVO);
				
				for(LiaRocNbBatchVO nbBatchVO : nbBatchMap.get(policyId)) {
					nbBatchVO.setCheckStatus(LiaRocCst.LIAROC_NB_BATCH_CHECK_STATUS_YES);
					if (uploadVO.getListId() != null) {
						nbBatchVO.setUploadListId(uploadVO.getListId());
					}
					liaRocNbBatchService.save(nbBatchVO);
				}
				trans.commit();
			} catch (Throwable e) {
	        	trans.rollback();
	        	String message = ExceptionInfoUtils.getExceptionMsg(e);
	        	ApplicationLogger.addLoggerData("[ERROR]changeType03Process發生不可預期的錯誤, Error Message:" + message);
	            executeResult = JobStatus.EXECUTE_FAILED;
	            ApplicationLogger.setReturnCode(ApplicationLogger.RETURN_CODE_FAIL);
			} 
    	}

		ApplicationLogger.addLoggerData("[INFO] 預收輸入批次寫入公會收件上傳檔(新增險種)" 
				+ ";寫檔處理,成功=" + succCount + ",無資料筆數=" + errorCount);
		
		return executeResult;
    }
    
    
    /**
     * 處理刪除險種需產生通報06公會收件通報的資料
     */
    protected int changeType04Process() throws Exception {

        // 批處理狀況預設為成功
        int executeResult = JobStatus.EXECUTE_SUCCESS;

    	// 依序處理控制檔
		int errorCount = 0;
		int succCount = 0;
		String changeType = LiaRocCst.LIAROC_NB_BATCH_CHANGE_TYPE_04;
        UserTransaction trans = null;
        
		//取得刪除險種未處理資料
		List<LiaRocNbBatchVO> nbBatchVOs = liaRocNbBatchService.getLiaRocNbBatchData(changeType);
		//資料重整為 Map<ruleName,List<policyId>>
		Map<String, List<LiaRocNbBatchVO>> nbBatchMap = NBUtils.toMapList(nbBatchVOs, "policyId");
    	
		 for(String policyId : nbBatchMap.keySet() ) {
			 try{
	    		trans = Trans.getUserTransaction();
				trans.begin();
				
				boolean isUnb = true;
				boolean is1Key = true;
				//PCR-337603 比對保單狀態是否需要含06-撤件 2020/11/20 Add by Kathy
				boolean isLiaRocPolicyStatus06 = false;
	
				PolicyVO policyVO = policyDS.retrieveById(Long.parseLong(policyId), false);
				//make 公會上傳主檔VO
				LiaRocUploadSendVO uploadVO = liaRocUpload2019CI.createLiarocUploadMaster
						(policyVO, LiaRocCst.LIAROC_UL_TYPE_RECEIVE, LiaRocCst.LIAROC_UL_BIZ_SOURCE_UNB);
	
				List<LiaRocUploadDetailVO> fixnewDetails = new ArrayList<LiaRocUploadDetailVO>();
				
		    	for(LiaRocNbBatchVO nbBatchVO : nbBatchMap.get(policyId)) {
	
					//以刪除險種取出最近一筆公會收件通報資料
					List<LiaRocUploadDetail> lastUploadDetails = liaRocUploadDetailDao.queryLastReceiveUploadDetail(
									isUnb, 
									String.valueOf(nbBatchVO.getChannelType()),
									policyVO.getPolicyNumber(), 
									nbBatchVO.getCertiCodeBf(), 
									nbBatchVO.getInternalIdBf(), 
									nbBatchVO.getApplyDate(), 
									policyVO.getSerialNum(),
									is1Key,
									isLiaRocPolicyStatus06);
	
					boolean isStatus06 = true;
					//以刪除險種的被保人ID取出有無做過15-更正通報
					List<LiaRocUploadDetail> lastUpload15Details = liaRocUploadDetailDao.queryLastReceiveUploadDetailByCertiCode(
									String.valueOf(nbBatchVO.getChannelType()),
									policyVO.getPolicyNumber(), 
									null,
									nbBatchVO.getApplyDate(), 
									policyVO.getSerialNum(),
									nbBatchVO.getInternalIdBf(),
									isStatus06	);			
					
					//執行通報06-終止通報
					if (CollectionUtils.isNotEmpty(lastUploadDetails)) {
						LiaRocUploadDetail lastUploadDetail = lastUploadDetails.get(0);
						LiaRocUploadDetail detailBo06 = this.createFixUploadDetail(lastUploadDetail, null, LiaRocCst.LIAROC_POLICY_STATUS__FREELOOK);
						LiaRocUploadDetailVO detailVO06 = new LiaRocUploadDetailVO();
						detailBo06.copyToVO(detailVO06, Boolean.TRUE);
						if (CollectionUtils.isNotEmpty(lastUpload15Details)) {
							LiaRocUploadDetail lastUpload15Detail = lastUpload15Details.get(0);
							//在做06通報前再去確認是否有做過ID/生日變更通報15，若有以新ID/新生日做06-終止通報
							if (!StringUtils.isNullOrEmpty(detailVO06.getCertiCode())  &&
									!StringUtils.isNullOrEmpty(lastUpload15Detail.getCertiCode())) {
								if (detailVO06.getCertiCode() != lastUpload15Detail.getCertiCode()){
									detailVO06.setCertiCode(lastUpload15Detail.getCertiCode());
								}								
							}
							if (detailVO06.getBirthday() != null && lastUpload15Detail.getBirthday()  != null){
								if (detailVO06.getBirthday().compareTo(lastUpload15Detail.getBirthday()) != 0){
									detailVO06.setBirthday(lastUpload15Detail.getBirthday());
								}								
							}
						}
						//刪除險種用item_id找出對應的t_liaroc_upload_coverage
						LiaRocUploadCoverage uploadCoverage = null;
						if (nbBatchVO.getChangeItemId()!= null && nbBatchVO.getChangeItemId().longValue() > 0L) {
							uploadCoverage = liaRocUploadCoverageDao.findByPolicyIdItemId(nbBatchVO.getPolicyId(), nbBatchVO.getChangeItemId());
						}
						if (uploadCoverage != null) {
							//將刪除的保項item_id = -1 做註銷
							uploadCoverage.setItemId(-1L);
							liaRocUploadCoverageDao.saveorUpdate(uploadCoverage);
						}

						detailVO06.setStatusEffectDate(DateUtils.truncateDay(AppContext.getCurrentUserLocalTime()));
						fixnewDetails.add(detailVO06);
						succCount++;
					}else{
						errorCount++;
					}
		
					uploadVO.setDataList(fixnewDetails);
				}
				//寫入公會收件通報上傳主檔和明細檔
		    	liaRocUpload2019CI.saveUpload(uploadVO);
				
				for(LiaRocNbBatchVO nbBatchVO : nbBatchMap.get(policyId)) {
					nbBatchVO.setCheckStatus(LiaRocCst.LIAROC_NB_BATCH_CHECK_STATUS_YES);
					if (uploadVO.getListId() != null) {
						nbBatchVO.setUploadListId(uploadVO.getListId());
					}
					liaRocNbBatchService.save(nbBatchVO);
				}
				trans.commit();
			} catch (Throwable e) {
	        	trans.rollback();
	        	String message = ExceptionInfoUtils.getExceptionMsg(e);
	        	ApplicationLogger.addLoggerData("[ERROR]changeType04Process發生不可預期的錯誤, Error Message:" + message);
	            executeResult = JobStatus.EXECUTE_FAILED;
	            ApplicationLogger.setReturnCode(ApplicationLogger.RETURN_CODE_FAIL);
			} 
    	}

		ApplicationLogger.addLoggerData("[INFO] 預收輸入批次寫入公會收件上傳檔(刪除險種)" 
				+ ";寫檔處理,成功=" + succCount + ",無資料筆數=" + errorCount);
		
		return executeResult;
    }

    /**
     * 處理變更險種需產生通報01有效/06終止公會收件通報的資料
     */
    protected int changeType05Process() throws Exception {

        // 批處理狀況預設為成功
        int executeResult = JobStatus.EXECUTE_SUCCESS;

    	// 依序處理控制檔
		int errorCount = 0;
		int succCount = 0;
		String changeType = LiaRocCst.LIAROC_NB_BATCH_CHANGE_TYPE_05;
        UserTransaction trans = null;
        
		//取得變更險種未處理資料
		List<LiaRocNbBatchVO> nbBatchVOs = liaRocNbBatchService.getLiaRocNbBatchData(changeType);
		//資料重整為 Map<ruleName,List<policyId>>
		Map<String, List<LiaRocNbBatchVO>> nbBatchMap = NBUtils.toMapList(nbBatchVOs, "policyId");
    	
		 //by policyId group
		 for(String policyId : nbBatchMap.keySet() ) {
			 try{
	    		trans = Trans.getUserTransaction();
				trans.begin();
	
				PolicyVO policyVO = policyDS.retrieveById(Long.parseLong(policyId), false);
				//make 公會上傳主檔VO
				LiaRocUploadSendVO uploadVO = liaRocUpload2019CI.createLiarocUploadMaster
						(policyVO, LiaRocCst.LIAROC_UL_TYPE_RECEIVE, LiaRocCst.LIAROC_UL_BIZ_SOURCE_UNB);

				boolean isUnb = true;
				boolean is1Key = true;
				//PCR-337603 比對保單狀態是否需要含06-撤件 2020/11/20 Add by Kathy
				boolean isLiaRocPolicyStatus06 = false;
	
				List<LiaRocUploadDetailVO> newDetails = new ArrayList<LiaRocUploadDetailVO>();
	
		    	for(LiaRocNbBatchVO nbBatchVO : nbBatchMap.get(policyId)) {
					String oldUploadKey = null;
					String newUploadKey = null;
					LiaRocUploadDetailWrapperVO detailVO06 = new LiaRocUploadDetailWrapperVO();		
					LiaRocUploadDetailVO detailVO01 = new LiaRocUploadDetailVO();
					
		    		//以變更前險種取出最近一筆公會收件通報資料
					List<LiaRocUploadDetail> lastUploadDetails = liaRocUploadDetailDao.queryLastReceiveUploadDetail(
									isUnb, 
									String.valueOf(nbBatchVO.getChannelType()),
									policyVO.getPolicyNumber(), 
									nbBatchVO.getCertiCodeBf(), 
									nbBatchVO.getInternalIdBf(), 
									nbBatchVO.getApplyDate(), 
									policyVO.getSerialNum(),
									is1Key,
									isLiaRocPolicyStatus06);

					boolean isStatus06 = true;
					//以變更前險種代碼+ID取出有無做過15-更正通報
					List<LiaRocUploadDetail> lastUpload15Details = liaRocUploadDetailDao.queryLastReceiveUploadDetailByCertiCode(
									String.valueOf(nbBatchVO.getChannelType()),
									policyVO.getPolicyNumber(), 
									null,
									nbBatchVO.getApplyDate(), 
									policyVO.getSerialNum(),
									nbBatchVO.getInternalIdBf(),
									isStatus06	);		

					if (CollectionUtils.isNotEmpty(lastUploadDetails)) {
						LiaRocUploadDetail lastUploadDetail = lastUploadDetails.get(0);
						LiaRocUploadDetail detailBo06 = this.createFixUploadDetail(lastUploadDetail, null, LiaRocCst.LIAROC_POLICY_STATUS__FREELOOK);
						detailBo06.copyToVO(detailVO06, Boolean.TRUE);
						if (CollectionUtils.isNotEmpty(lastUpload15Details)) {
							LiaRocUploadDetail lastUpload15Detail = lastUpload15Details.get(0);
							//在做06通報前再去確認是否有做過ID/生日變更通報15，若有以新ID/新生日做06-終止通報
							if (!StringUtils.isNullOrEmpty(detailVO06.getCertiCode())  &&
									!StringUtils.isNullOrEmpty(lastUpload15Detail.getCertiCode())) {
								if (detailVO06.getCertiCode().equals(lastUpload15Detail.getCertiCode())== false){
									detailVO06.setCertiCode(lastUpload15Detail.getCertiCode());
								}								
							}
							if (detailVO06.getBirthday() != null && lastUpload15Detail.getBirthday()  != null){
								if (detailVO06.getBirthday().compareTo(lastUpload15Detail.getBirthday()) != 0){
									detailVO06.setBirthday(lastUpload15Detail.getBirthday());
								}								
							}
						}
						oldUploadKey = liaRocUploadReceiveBatch.getUploadKey(detailVO06);
						//變更前險種的通報公會保單號碼找出對應的t_liaroc_upload_coverage
						LiaRocUploadCoverage uploadCoverage = null;
						if (nbBatchVO.getPolicyId()!= null && nbBatchVO.getPolicyId().longValue() > 0L) {
							uploadCoverage = liaRocUploadCoverageDao.findByPolicyIdLiaRocPolicyCode(
											nbBatchVO.getPolicyId(),detailVO06.getPolicyId());
						}
						if (uploadCoverage != null) {
							//將變更前的保項item_id = -1 做註銷
							uploadCoverage.setItemId(-1L);
							liaRocUploadCoverageDao.saveorUpdate(uploadCoverage);
						}
					}		    		
					
					//以變更後的險種取得保項明細檔
		    		List<CoverageVO>coverageList  = new ArrayList<CoverageVO>();
		    		CoverageVO coverageVO = coverageService.load(nbBatchVO.getChangeItemId());
					coverageList.add(coverageVO);
					
					//取得公會上傳明細檔
					List<LiaRocUploadDetailWrapperVO> genUploadDetails = liaRocUpload2019CI.generateNBBatchUploadDetail(
							policyVO, coverageList, LiaRocCst.LIAROC_UL_TYPE_RECEIVE);

					if (CollectionUtils.isNotEmpty(genUploadDetails)) {
						newUploadKey = liaRocUploadReceiveBatch.getUploadKey(genUploadDetails.get(0));
						detailVO01 = genUploadDetails.get(0);
					}	
					
					//PCR-337603 比對保單狀態是否需要含06-撤件 2020/11/20 Add by Kathy
					isLiaRocPolicyStatus06 = true; 
		    		//以變更後險種取出最近一筆公會收件通報資料
					LiaRocUploadDetail lastUploadDetail = this.QueryLastReceiveUploadDetail(
									isUnb, 
									String.valueOf(nbBatchVO.getChannelType()),
									policyVO.getPolicyNumber(), 
									nbBatchVO.getCertiCodeBf(), 
									nbBatchVO.getInternalIdAf(), 
									nbBatchVO.getApplyDate(), 
									policyVO.getSerialNum(),
									is1Key,
									isLiaRocPolicyStatus06);

					//已通報此處不作通報
					if (lastUploadDetail != null) {
						//上一筆通報狀態不為06-撤件
						if (!LiaRocCst.LIAROC_POLICY_STATUS__FREELOOK.equals(lastUploadDetail.getLiaRocPolicyStatus())){
							//更新狀態為2-不通報，它件已通報
							detailVO01.setCheckStatus(LiaRocCst.CHECK_STATUS_CANCEL_BY_AFINS);
							detailVO01.setRefUploadListId(lastUploadDetail.getListId());
							detailVO01.setPolicyId(lastUploadDetail.getPolicyId());							
						}
					}
					//更新公會通報保單取號檔(T_LIAROC_UPLOAD_COVERAGE)
					LiaRocUploadCoverage uploadCoverage = null;
					if (nbBatchVO.getPolicyId() != null && nbBatchVO.getPolicyId().longValue() > 0L) {
						uploadCoverage = liaRocUploadCoverageDao.findByPolicyIdLiaRocPolicyCode(
										nbBatchVO.getPolicyId(), detailVO01.getPolicyId());
					}
					if (uploadCoverage != null) {
						//將這次新增item_id 回壓對應的t_liaroc_upload_coverage.item_id
						uploadCoverage.setItemId(nbBatchVO.getChangeItemId());
						liaRocUploadCoverageDao.saveorUpdate(uploadCoverage);
					}					
	 
					//若新舊險種若通報公會的key相同
					if (NBUtils.in(oldUploadKey, newUploadKey)) {
						//更新舊險種通報06-終止明細檔狀態為3-不通報,上傳重覆件(key值重覆)
						detailVO06.setCheckStatus(LiaRocCst.CHECK_STATUS_CANCEL_BY_SAMEKEY);
					}
					
					//執行新險種通報01-有效，舊險種通報06-終止
					if (detailVO01.getPolicyId() != null) {
						// IR-359765 新增被保險人附約(含眷屬)如跳出視窗點選確認時應以變更的日期通報要保書填寫日
						if (nbBatchVO.getSystemDate() != null) {
							detailVO01.setValidateDate(nbBatchVO.getSystemDate());
							detailVO01.setStatusEffectDate(nbBatchVO.getSystemDate());
						}
						//變更前險種和變更後險種一樣時，註記通報明細檔狀態為3-不通報,上傳重覆件(key值重覆)
						if (NBUtils.in(nbBatchVO.getInternalIdBf(), nbBatchVO.getInternalIdAf())) {
							detailVO06.setCheckStatus(LiaRocCst.CHECK_STATUS_CANCEL_BY_SAMEKEY);
							detailVO01.setCheckStatus(LiaRocCst.CHECK_STATUS_CANCEL_BY_SAMEKEY);
						}							
						
						if (detailVO06.getPolicyId() != null) {
							newDetails.add(detailVO06);
						} 
						newDetails.add(detailVO01);
						succCount++;
					} else {
						errorCount++;
					}
				}
		    	uploadVO.setDataList(newDetails);
				//寫入公會收件通報上傳主檔和明細檔
		    	liaRocUpload2019CI.saveUpload(uploadVO);
				
				for(LiaRocNbBatchVO nbBatchVO : nbBatchMap.get(policyId)) {
					nbBatchVO.setCheckStatus(LiaRocCst.LIAROC_NB_BATCH_CHECK_STATUS_YES);
					if (uploadVO.getListId() != null) {
						nbBatchVO.setUploadListId(uploadVO.getListId());
					}
					liaRocNbBatchService.save(nbBatchVO);
				}
				trans.commit();
			} catch (Throwable e) {
	        	trans.rollback();
	        	String message = ExceptionInfoUtils.getExceptionMsg(e);
	        	ApplicationLogger.addLoggerData("[ERROR]changeType03Process發生不可預期的錯誤, Error Message:" + message);
	            executeResult = JobStatus.EXECUTE_FAILED;
	            ApplicationLogger.setReturnCode(ApplicationLogger.RETURN_CODE_FAIL);
			} 
    	}

		ApplicationLogger.addLoggerData("[INFO] 預收輸入批次寫入公會收件上傳檔(變更險種)" 
				+ ";寫檔處理,成功=" + succCount + ",無資料筆數=" + errorCount);
		
		return executeResult;
    }
    
	public int changeType13Process() throws Exception {

		// 批處理狀況預設為成功
		int executeResult = JobStatus.EXECUTE_SUCCESS;

		// 依序處理控制檔
		int errorCount = 0;
		int succCount = 0;
		String changeType = LiaRocCst.LIAROC_NB_BATCH_CHANGE_TYPE_13;
		UserTransaction trans = null;

		//13-承保UNDO後通報承保06，當天又承保
		List<LiaRocNbBatchVO> nbBatchVOs = liaRocNbBatchService.getLiaRocNbBatchData(changeType);

		Date today = DateUtils.truncateDay(new Date());//去除時分秒

		for (LiaRocNbBatchVO nbBatchVO : nbBatchVOs) {

			//僅處理今天0時前的資料
			if (today.getTime() > nbBatchVO.getInsertTimestamp().getTime()) {

				try {
					trans = Trans.getUserTransaction();
					trans.begin();

					//生效狀態判斷
					int liabilityState = policyDS.getLiabilityStatusByPolicyId(nbBatchVO.getPolicyId());
					if (CodeCst.LIABILITY_STATUS__IN_FORCE == liabilityState) {
						//執行全量承保通報
						Long uploadId = liaRocUpload2019CI.sendNB(nbBatchVO.getPolicyId(),
								LiaRocCst.LIAROC_UL_TYPE_INFORCE);

						if (uploadId != null) {
							succCount++;
							nbBatchVO.setCheckStatus(LiaRocCst.LIAROC_NB_BATCH_CHECK_STATUS_YES);
						} else {
							errorCount++;
							nbBatchVO.setCheckStatus(LiaRocCst.LIAROC_NB_BATCH_CHECK_STATUS_ERROR);
						}

						nbBatchVO.setUploadListId(uploadId);
					} else {
						succCount++;
						nbBatchVO.setCheckStatus(LiaRocCst.LIAROC_NB_BATCH_CHECK_STATUS_Z);
					}

					liaRocNbBatchService.save(nbBatchVO);

					trans.commit();
				} catch (Throwable e) {
					trans.rollback();
					String message = ExceptionInfoUtils.getExceptionMsg(e);
					ApplicationLogger.addLoggerData("[ERROR]changeType13Process發生不可預期的錯誤, Error Message:" + message);
					executeResult = JobStatus.EXECUTE_FAILED;
					ApplicationLogger.setReturnCode(ApplicationLogger.RETURN_CODE_FAIL);
				}
			}
		}

		ApplicationLogger
				.addLoggerData("[INFO] 承保UNDO後通報承保06，當天又承保，隔天執行承保通報" + ";寫檔處理,成功=" + succCount + ",失敗=" + errorCount);

		return executeResult;
	}

	public int changeType14Process() throws Exception {

		// 批處理狀況預設為成功
		int executeResult = JobStatus.EXECUTE_SUCCESS;

		// 依序處理控制檔
		int errorCount = 0;
		int succCount = 0;
		String changeType = LiaRocCst.LIAROC_NB_BATCH_CHANGE_TYPE_14;
		UserTransaction trans = null;

		//14-撤件UNDO後通報收件01，當天又撤件
		List<LiaRocNbBatchVO> nbBatchVOs = liaRocNbBatchService.getLiaRocNbBatchData(changeType);

		Date today = DateUtils.truncateDay(new Date());//去除時分秒

		for (LiaRocNbBatchVO nbBatchVO : nbBatchVOs) {

			//僅處理今天0時前的資料
			if (today.getTime() > nbBatchVO.getInsertTimestamp().getTime()) {

				try {
					
					String policyCode = policyDS.getPolicyCodeByPolicyId(nbBatchVO.getPolicyId());
					int proposalStatus = policyDS.getProposalStatusByPolicyCode(policyCode);

					trans = Trans.getUserTransaction();
					trans.begin();
			
					if (proposalStatus == CodeCst.PROPOSAL_STATUS__DECLINED
							|| proposalStatus == CodeCst.PROPOSAL_STATUS__POSTPONED
							|| proposalStatus == CodeCst.PROPOSAL_STATUS__WITHDRAWN) {

						//執行全量收件通報06
						LiaRocUploadSendVO sendVO = liaRocUpload2019CI.getUploadVOByPolicyId(nbBatchVO.getPolicyId(),
								LiaRocCst.LIAROC_UL_TYPE_RECEIVE, LiaRocCst.LIAROC_UL_BIZ_SOURCE_UNB);
						
						List<? extends GenericEntityVO> details = sendVO.getDataList();
						for (GenericEntityVO entityVO : details) {
							LiaRocUploadDetailVO detailVO = (LiaRocUploadDetailVO) entityVO;
							detailVO.setLiaRocPolicyStatus(LiaRocCst.LIAROC_POLICY_STATUS__FREELOOK);
						}
						Long uploadId = liaRocUpload2019CI.saveUpload(sendVO);
						if (uploadId != null) {
							succCount++;
							nbBatchVO.setCheckStatus(LiaRocCst.LIAROC_NB_BATCH_CHECK_STATUS_YES);
						} else {
							errorCount++;
							nbBatchVO.setCheckStatus(LiaRocCst.LIAROC_NB_BATCH_CHECK_STATUS_ERROR);
						}

						nbBatchVO.setUploadListId(uploadId);
					} else {
						succCount++;
						nbBatchVO.setCheckStatus(LiaRocCst.LIAROC_NB_BATCH_CHECK_STATUS_Z);
					}

					liaRocNbBatchService.save(nbBatchVO);

					trans.commit();
				} catch (Throwable e) {
					trans.rollback();
					String message = ExceptionInfoUtils.getExceptionMsg(e);
					ApplicationLogger.addLoggerData("[ERROR]changeType14Process發生不可預期的錯誤, Error Message:" + message);
					executeResult = JobStatus.EXECUTE_FAILED;
					ApplicationLogger.setReturnCode(ApplicationLogger.RETURN_CODE_FAIL);
				}
			}
		}

		ApplicationLogger
				.addLoggerData("[INFO] 撤件UNDO後通報收件01，當天又撤件，隔天執行收件06通報" + ";寫檔處理,成功=" + succCount + ",失敗=" + errorCount);

		return executeResult;
	}

	@Override
	protected FixedCharLengthWriter getFixedLengthWriter() {
		// TODO Auto-generated method stub
		return null;
	}
	
	private LiaRocUploadDetail createFixUploadDetail(LiaRocUploadDetail input, Long listId, String liaRocStatus) {
		LiaRocUploadDetail detailBO = new LiaRocUploadDetail();
		BeanUtils.copyProperties(detailBO, input);
		detailBO.setLiaRocPolicyStatus(liaRocStatus);
		detailBO.setUploadListId(listId);
		detailBO.setListId(null);
		return detailBO;
	}

	private LiaRocUploadDetail  QueryLastReceiveUploadDetail(boolean isUnb, String channelType, String policyCode,
			String certiCode, String internalId,Date applyDate, String serialNum, boolean is1Key,boolean isLiaRocPolicyStatus06 ) {
		
		List<LiaRocUploadDetail>  lastDetailVO01s = new ArrayList<LiaRocUploadDetail>();
		LiaRocUploadDetail  lastDetailVO01               = null;
		//以傳入險種取出最近一筆公會收件通報資料
		lastDetailVO01s = liaRocUploadDetailDao.queryLastReceiveUploadDetail(
						isUnb, 
						channelType,
						policyCode, 
						certiCode, 
						internalId, 
						applyDate, 
						serialNum,
						is1Key,
						isLiaRocPolicyStatus06);
		LiaRocUpload oldLastUpload = null;					
		//有重覆送件的記錄
		if (CollectionUtils.isNotEmpty(lastDetailVO01s)) {
			lastDetailVO01 = lastDetailVO01s.get(0);
			//查明細的主檔，ID不同才查
			oldLastUpload = liaRocUploadDao.load(lastDetailVO01.getUploadListId());
			String status = oldLastUpload.getLiaRocUploadStatus();

			//若原本有為收件通報系統(AFINS),依通路比對通報檔, 若為FOA受理(UNB)通報用保單號碼+要保書填寫日,險種代碼比對, 已放送就不再發送;
			//判斷最後一筆通報01-有效才是重覆送件
			if (LiaRocCst.LIAROC_UL_STATUS_SEND.equals(status)
							|| LiaRocCst.LIAROC_UL_STATUS_RETURN.equals(status)
							|| LiaRocCst.LIAROC_UL_STATUS_RETURN_ERROR.equals(status)
											&& (lastDetailVO01.getLiaRocPolicyStatus().equals(LiaRocCst.LIAROC_POLICY_STATUS__INFORCE) == true)) {
				//取得曾通報過的detail
				return lastDetailVO01;
			} else {
				 lastDetailVO01 = null;
			}
		}
		return lastDetailVO01;
	}

	@Override
	protected List<? extends LiaRocUploadDetailBaseVO> getUpload2020Details() {
		return new ArrayList<LiaRocUploadDetailBaseVO>();
	}
}
