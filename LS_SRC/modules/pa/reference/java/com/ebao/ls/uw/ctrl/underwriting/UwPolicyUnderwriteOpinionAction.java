package com.ebao.ls.uw.ctrl.underwriting;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.ls.escape.helper.EscapeHelper;
import com.ebao.ls.uw.ctrl.UwGenericAction;
import com.ebao.pub.framework.GenericException;

/**
 * <p>
 * Title:Underwriting
 * </p>
 * <p>
 * Description:
 * </p>
 * <p>
 * Copyright: Copyright (c) 2016
 * </p>
 * <p>
 * Company: eBaoTech Corporation
 * </p>
 * 
 * @author ChiaHui Su
 * @since 2016-02-19
 * @version 1.0
 */
public class UwPolicyUnderwriteOpinionAction extends UwGenericAction {
    public static final String BEAN_DEFAULT = "/uw/policyUnderwriteOpinion";
    
    /**
     * 核保檢核綜合意見欄
     */
    public static final String COMMENT_OPTION_LIST = "comment_option_list";

    @Override
    public ActionForward uwProcess(ActionMapping mapping, ActionForm form,
                    HttpServletRequest request, HttpServletResponse response)
                    throws GenericException {
    	String policyId = com.ebao.ls.encode.encoder.Encode.encodeForHTML(request).getParameter("policyId");
    	String underwriteId = com.ebao.ls.encode.encoder.Encode.encodeForHTML(request).getParameter("underwriteId");
    	String commentVersionId = com.ebao.ls.encode.encoder.Encode.encodeForHTML(request).getParameter("commentVersionId");
    	String isIframe = com.ebao.ls.encode.encoder.Encode.encodeForHTML(request).getParameter("isIframe");
    	
        EscapeHelper.escapeHtml(request).setAttribute("policyId", policyId);
        EscapeHelper.escapeHtml(request).setAttribute("underwriteId", underwriteId);
        EscapeHelper.escapeHtml(request).setAttribute("commentVersionId", commentVersionId);
        EscapeHelper.escapeHtml(request).setAttribute(UwPolicyUnderwriteOpinionAction.COMMENT_OPTION_LIST,
                        helper.getCommentOptionList(Long.valueOf(underwriteId), Long.valueOf(commentVersionId)));
        EscapeHelper.escapeHtml(request).setAttribute("isIframe", isIframe);
        return mapping.findForward("display");
    }
    
    @Resource(name = UwPolicySubmitActionHelper.BEAN_DEFAULT)
    protected UwPolicySubmitActionHelper helper;

    public UwPolicySubmitActionHelper getHelper() {
        return helper;
    }

    public void setHelper(UwPolicySubmitActionHelper helper) {
        this.helper = helper;
    }

}