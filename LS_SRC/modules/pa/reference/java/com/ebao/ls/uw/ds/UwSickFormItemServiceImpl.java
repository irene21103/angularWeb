package com.ebao.ls.uw.ds;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.ebao.ls.foundation.service.GenericServiceImpl;
import com.ebao.ls.pa.pub.data.bo.UwSickFormItem;
import com.ebao.ls.pa.pub.vo.UwSickFormItemVO;
import com.ebao.ls.uw.data.UwSickFormItemDelegate;
import com.ebao.pub.framework.GenericException;

/**
 * <p>Title: 核保頁面_疾病問卷照會選項列表</p>
 * <p>Description: 核保頁面_疾病問卷照會選項列表</p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company: TGL Co., Ltd.</p>
 * <p>Create Time: Apr 20, 2016</p> 
 * @author 
 * <p>Update Time: Apr 20, 2016</p>
 * <p>Updater: ChiaHui Su</p>
 * <p>Update Comments: </p>
 */
public class UwSickFormItemServiceImpl extends GenericServiceImpl<UwSickFormItemVO, UwSickFormItem, UwSickFormItemDelegate> implements UwSickFormItemService {

	@Resource(name = UwSickFormItemDelegate.BEAN_DEFAULT)
	  private UwSickFormItemDelegate uwUwSickFormItemDao;
	
	@Override
	  protected UwSickFormItemVO newEntityVO() {
	    return new UwSickFormItemVO();
	  }

	@Override
    public List<Map<String, String>> findSickFormItemList() throws GenericException {
		return uwUwSickFormItemDao.findSickFormItemList();
	}
	
	@Override
    public String findSickFormNameByCode(String code) throws GenericException {
		return uwUwSickFormItemDao.findSickFormNameByCode(code);
	}

	@Override
	public Map<String, UwSickFormItemVO> findByLetterId(Long letterId) {
		List<UwSickFormItem> formItems = uwUwSickFormItemDao.findByLetterId(letterId);
		Map<String, UwSickFormItemVO> itemVOMap = new HashMap<String,UwSickFormItemVO>();
		for(UwSickFormItem item : formItems) {
			UwSickFormItemVO vo = new UwSickFormItemVO();
			item.copyToVO(vo, false);
			itemVOMap.put(vo.getSickFormItem(), vo);
		}
		return itemVOMap;
	}
	
	@Override
	public List<String> findItemsByLetterId(Long letterId) {
		List<String> items = new ArrayList<String>();
		List<UwSickFormItem> formItems = uwUwSickFormItemDao.findByLetterId(letterId);
		for(UwSickFormItem item: formItems) {
			items.add(item.getSickFormItem());
		}
		return items;
	}

	@Override
	public void deleteByLetterId(Long listId) {
		List<UwSickFormItem> itemList = uwUwSickFormItemDao.findByLetterId(listId);
		for( UwSickFormItem item : itemList ){
			uwUwSickFormItemDao.remove(item);
		}
	}
	
	
    
}
