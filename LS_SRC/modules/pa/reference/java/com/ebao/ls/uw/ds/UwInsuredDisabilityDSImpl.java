package com.ebao.ls.uw.ds;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.ebao.ls.pa.pub.bs.InsuredDisabilityIcfService;
import com.ebao.ls.pa.pub.vo.InsuredDisabilityDisplayVO;
import com.ebao.ls.uw.data.TUwInsuredDisabilityDelegate;
import com.ebao.ls.uw.data.bo.UwInsuredDisability;
import com.ebao.ls.uw.data.bo.UwInsuredDisabilityIcf;
import com.ebao.ls.uw.vo.UwLifeInsuredVO;
import com.ebao.pub.framework.GenericDS;
import com.ebao.pub.framework.GenericException;
import com.ebao.pub.util.BeanUtils;

public class UwInsuredDisabilityDSImpl extends GenericDS implements UwInsuredDisabilityService {

	@Resource(name = TUwInsuredDisabilityDelegate.BEAN_DEFAULT)
	private TUwInsuredDisabilityDelegate uwInsuredDisabilityDao;
	
    @Resource(name = UwPolicyService.BEAN_DEFAULT)
    private UwPolicyService uwPolicyDS;
    
	@Resource(name = InsuredDisabilityIcfService.BEAN_DEFAULT)
	protected InsuredDisabilityIcfService insuredDisabilityIcfService;
	
	@Resource(name = UwInsuredDisabilityIcfService.BEAN_DEFAULT)
	protected UwInsuredDisabilityIcfService uwInsuredDisabilityIcfService;

	@Override
	public List<String> getDisabilityTypes(Long uwInsuredId) throws GenericException {
		List<UwInsuredDisability> list = uwInsuredDisabilityDao.findByUwInsuredId(uwInsuredId);
		List<String> disabilityTypes = new ArrayList<String>();
		for (UwInsuredDisability disability : list) {
			disabilityTypes.add(disability.getDisabilityType());
		}
		return disabilityTypes;
	}

	@Override
	public void saveResults(Long uwInsuredId, List<String> disabilityTypes) throws GenericException {
		List<UwInsuredDisability> olds = uwInsuredDisabilityDao.findByUwInsuredId(uwInsuredId);
		Map<String, UwInsuredDisability> map = new HashMap<String, UwInsuredDisability>();
		if (olds != null) {
			for (UwInsuredDisability uwInsuredDisability : olds) {
				map.put(uwInsuredDisability.getDisabilityType(), uwInsuredDisability);
			}
		}
		for (String disabilityType : disabilityTypes) {
			if (map.containsKey(disabilityType)) {
				/* 如果原本身心障礙類別已經存在,移除map中的物件*/
				map.remove(disabilityType);
			} else {
				/* 如果原本身心障礙類別不存在, 用新增*/
				UwInsuredDisability vo = new UwInsuredDisability();
				vo.setDisabilityType(disabilityType);
				vo.setUwInsuredId(uwInsuredId);
				uwInsuredDisabilityDao.create((UwInsuredDisability) BeanUtils.getBean(UwInsuredDisability.class, vo));
			}
		}
		/* map中如果還有object 要刪除*/
		for (String disabilityType : map.keySet()) {
			uwInsuredDisabilityDao.remove(map.get(disabilityType).getListId());
		}

	}
	
	@Override
	public void saveResultsForUndo(Long uwInsuredId, Map<String,List<String>> disablityTypeIcfLevelMap ) throws GenericException {
	
		for (String disabilityType : disablityTypeIcfLevelMap.keySet() ) {
			List<String> disabilityIcfLevelList = disablityTypeIcfLevelMap.get(disabilityType);
			
			/* 新增身心障礙類別及icf編碼*/
			UwInsuredDisability uwDisability = new UwInsuredDisability();
			uwDisability.setDisabilityType(disabilityType);
			uwDisability.setUwInsuredId(uwInsuredId);
			Long listId = uwInsuredDisabilityDao.create((UwInsuredDisability) BeanUtils.getBean(UwInsuredDisability.class, uwDisability));
			uwDisability = uwInsuredDisabilityDao.load(listId);
			
			for(String disabilityLevel : disabilityIcfLevelList) {
				uwInsuredDisabilityIcfService.create(uwDisability, disabilityLevel);
			}

		}

	}

	@Override
	public void removeByUwInsuredId(Long uwInsuredId) throws GenericException {
		List<UwInsuredDisability> uwInsuredDisabilities = uwInsuredDisabilityDao.findByUwInsuredId(uwInsuredId);
		for (UwInsuredDisability uw : uwInsuredDisabilities) {
			uwInsuredDisabilityDao.remove(uw.getListId());
		}
	}
	
	@Override
	public UwInsuredDisability create(long uwInsuredId, String disabilityCode) {
		UwInsuredDisability uwDisability = new UwInsuredDisability();
		uwDisability.setUwInsuredId(uwInsuredId);
		uwDisability.setDisabilityType(disabilityCode);
		uwInsuredDisabilityDao.create(uwDisability);
		return uwDisability;
	}

	@Override
	public List<UwInsuredDisability> findByUwInsuredId(long uwListId) {
		return uwInsuredDisabilityDao.findByUwInsuredId(uwListId);
	}
	
	@Override
	public UwInsuredDisability findByUwInsuredIdDisabilityCode(long underwriteId, long listId, String disabilityCode) {
		UwLifeInsuredVO uwLife = uwPolicyDS.findUwLifeInsuredByListId(underwriteId, listId);
		return uwInsuredDisabilityDao.findByUwInsuredIdDisabilityCode(uwLife.getUwListId(), disabilityCode);
	}
	
	@Override
	public void removeByUwInsuredIdDisabilityCode(long uwListId, String disabilityCode) {
		UwInsuredDisability uwDis = uwInsuredDisabilityDao.findByUwInsuredIdDisabilityCode(uwListId, disabilityCode);
		uwDis.getUwInsuredDisabilityIcfs().clear();
		uwInsuredDisabilityDao.remove(uwDis);
	}

	@Override
	public List<InsuredDisabilityDisplayVO> queryDisability(long underwriteId, long listId) {
		UwLifeInsuredVO uwLife = uwPolicyDS.findUwLifeInsuredByListId(underwriteId, listId);
		List<UwInsuredDisability> uwDisList = findByUwInsuredId(uwLife.getUwListId());
		List<InsuredDisabilityDisplayVO> result = convertToDisplayVOByUw(uwDisList);
		result.stream().sorted();
		return result;
	}
	
	@Override
	public List<InsuredDisabilityDisplayVO> convertToDisplayVOByUw(List<UwInsuredDisability> uwDisList) {
		List<InsuredDisabilityDisplayVO> result = new ArrayList<InsuredDisabilityDisplayVO>();
		for (UwInsuredDisability uwDis : uwDisList) {
			List<UwInsuredDisabilityIcf> icfs = uwDis.getUwInsuredDisabilityIcfs();
			for (UwInsuredDisabilityIcf uwIcf : icfs) {
				InsuredDisabilityDisplayVO vo = new InsuredDisabilityDisplayVO();
				vo.setDisabilityPk(uwDis.getListId());
				vo.setDisabilityType(uwDis.getDisabilityType());
				vo.setIcfPk(uwIcf.getUwIcfListId());
				vo.setDisabilityLevel(uwIcf.getDisabilityLevel());
				boolean existsFinMsrIcf = insuredDisabilityIcfService.existsFinMsrIcf(uwDis.getDisabilityType(), uwIcf.getDisabilityLevel());
				vo.setExistsIcf(existsFinMsrIcf?"Y":"N");
				result.add(vo);
			}
		}
		return result;
	}

}