package com.ebao.ls.uw.ctrl.decision;

import java.util.Date;

import com.ebao.ls.uw.ctrl.pub.UwProductGenericForm;

/**
 * <p>Title: 核保批註</p>
 * <p>Description: 核保批註資料(for UwExclusionAjaxService)</p>
 * <p>Copyright: Copyright (c) 2015</p>
 * <p>Company: TGL Co., Ltd.</p>
 * <p>Create Time: Jul 3, 2015</p> 
 * @author 
 * <p>Update Time: Jul 3, 2015</p>
 * <p>Updater: simon.huang</p>
 * <p>Update Comments: </p>
 */
public class UwProductExclusionForm extends UwProductGenericForm {

	/** 承保被保人 id*/
	private String uwInsuredId;

	/** r版本、s版本是否被勾選*/
	private boolean[] exclusionCodeChecked;

	/** r版本、s版本 代碼*/
	private String[] exclusionCode;

	/** 批註內容*/
	private String[] descLang1;

	/** 批註內容template*/
	private String[] descLangTemplate;

	/** 是否2~6級*/
	private String[] disabilityIndi;

	/** 核保員*/
	private Long[] underwriterId;
	
	/** 按鈕功能*/
	private String subAction;

	/** 批註日期*/
	private Date[] exclusionDate;

	/** 來源為保全核保 */
	private boolean isCsUw;
	
	/* 保全變更申請ID */
	private Long changeId;
	
	/**
	 * @return 傳回 uwInsuredId。
	 */
	public String getUwInsuredId() {
		return uwInsuredId;
	}

	/**
	 * @param uwInsuredId 要設定的 uwInsuredId。
	 */
	public void setUwInsuredId(String uwInsuredId) {
		this.uwInsuredId = uwInsuredId;
	}

	/**
	 * @return 傳回 exclusionCodeChecked。
	 */
	public boolean[] getExclusionCodeChecked() {
		return exclusionCodeChecked;
	}

	/**
	 * @param exclusionCodeChecked 要設定的 exclusionCodeChecked。
	 */
	public void setExclusionCodeChecked(boolean[] exclusionCodeChecked) {
		this.exclusionCodeChecked = exclusionCodeChecked;
	}

	/**
	 * @return 傳回 exclusionCode。
	 */
	public String[] getExclusionCode() {
		return exclusionCode;
	}

	/**
	 * @param exclusionCode 要設定的 exclusionCode。
	 */
	public void setExclusionCode(String[] exclusionCode) {
		this.exclusionCode = exclusionCode;
	}

	/**
	 * @return 傳回 descLang1。
	 */
	public String[] getDescLang1() {
		return descLang1;
	}

	/**
	 * @param descLang1 要設定的 descLang1。
	 */
	public void setDescLang1(String[] descLang1) {
		this.descLang1 = descLang1;
	}

	/**
	 * @return 傳回 descLangTemplate。
	 */
	public String[] getDescLangTemplate() {
		return descLangTemplate;
	}

	/**
	 * @param descLangTemplate 要設定的 descLangTemplate。
	 */
	public void setDescLangTemplate(String[] descLangTemplate) {
		this.descLangTemplate = descLangTemplate;
	}

	/**
	 * @return 傳回 disabilityIndi。
	 */
	public String[] getDisabilityIndi() {
		return disabilityIndi;
	}

	/**
	 * @param disabilityIndi 要設定的 disabilityIndi。
	 */
	public void setDisabilityIndi(String[] disabilityIndi) {
		this.disabilityIndi = disabilityIndi;
	}

	/**
	 * @return 傳回 underwriterId。
	 */
	public Long[] getUnderwriterId() {
		return underwriterId;
	}

	/**
	 * @param underwriterId 要設定的 underwriterId。
	 */
	public void setUnderwriterId(Long[] underwriterId) {
		this.underwriterId = underwriterId;
	}

	/**
	 * @return 傳回 exclusionDate。
	 */
	public Date[] getExclusionDate() {
		return exclusionDate;
	}

	/**
	 * @param exclusionDate 要設定的 exclusionDate。
	 */
	public void setExclusionDate(Date[] exclusionDate) {
		this.exclusionDate = exclusionDate;
	}

	public String getSubAction() {
		return subAction;
	}

	public void setSubAction(String subAction) {
		this.subAction = subAction;
	}
	/**
	 * <p>Description :來源為保全核保 </p>
	 * <p>Created By : TGL144</p>
	 * <p>Create Time : Jan 9, 2017</p>
	 * @return
	 */
	public boolean isCsUw() {
		return isCsUw;
	}
	/**
	 * <p>Description :來源為保全核保 </p>
	 * <p>Created By : TGL144</p>
	 * <p>Create Time : Jan 9, 2017</p>
	 * @param isCsUw
	 */
	public void setCsUw(boolean isCsUw) {
		this.isCsUw = isCsUw;
	}
	/**
	 * <p>Description :保全變更申請ID </p>
	 * <p>Created By : TGL144</p>
	 * <p>Create Time : Sep 10, 2016</p>
	 * @return
	 */
	public Long getChangeId() {
		return changeId;
	}
	/**
	 * <p>Description :保全變更申請ID </p>
	 * <p>Created By : TGL144</p>
	 * <p>Create Time : Sep 10, 2016</p>
	 * @param changeId
	 */
	public void setChangeId(Long changeId) {
		this.changeId = changeId;
	}
}
