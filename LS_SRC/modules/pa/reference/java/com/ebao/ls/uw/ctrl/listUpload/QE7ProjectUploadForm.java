package com.ebao.ls.uw.ctrl.listUpload;

import java.util.List;

import org.apache.struts.upload.FormFile;

import com.ebao.ls.uw.ctrl.listUpload.vo.QE7ProjectExtendVO;
import com.ebao.pub.web.pager.PagerFormImpl;

public class QE7ProjectUploadForm extends PagerFormImpl {

	/**
	 * <code>serialVersionUID</code> 的註解
	 */
	private static final long serialVersionUID = 8812251132141351879L;
	
	private List<QE7ProjectExtendVO> voList;
	
	private List<QE7ProjectExtendVO> errList;
	
	private FormFile uploadFile;
	
	private Long batchNo;

	public List<QE7ProjectExtendVO> getVoList() {
		return voList;
	}

	public void setVoList(List<QE7ProjectExtendVO> voList) {
		this.voList = voList;
	}

	public List<QE7ProjectExtendVO> getErrList() {
		return errList;
	}

	public void setErrList(List<QE7ProjectExtendVO> errList) {
		this.errList = errList;
	}

	public Long getBatchNo() {
		return batchNo;
	}

	public void setBatchNo(Long batchNo) {
		this.batchNo = batchNo;
	}

	public FormFile getUploadFile() {
		return uploadFile;
	}

	public void setUploadFile(FormFile uploadFile) {
		this.uploadFile = uploadFile;
	}

}
