package com.ebao.ls.crs.company.impl;

import java.util.List;

import javax.annotation.Resource;

import com.ebao.ls.crs.company.CRSControlManService;
import com.ebao.ls.crs.data.bo.CRSControlMan;
import com.ebao.ls.crs.data.company.CRSControlManDAO;
import com.ebao.ls.crs.data.company.CRSManAccountDAO;
import com.ebao.ls.crs.vo.CRSControlManVO;
import com.ebao.ls.foundation.service.GenericServiceImpl;

public class CRSControlManServiceImpl extends GenericServiceImpl<CRSControlManVO, CRSControlMan, CRSControlManDAO>
		implements CRSControlManService {

	@Resource(name = CRSManAccountDAO.BEAN_DEFAULT)
	private CRSManAccountDAO accountDAO;

	@Override
	protected CRSControlManVO newEntityVO() {
		return new CRSControlManVO();
	}

	@Override
	public List<CRSControlManVO> findByCertiCode(String certiCode) {
		java.util.List<CRSControlMan> controlList = dao.findByCertiCode(certiCode);
		return this.convertToVOList(controlList);
	}

}
