package com.ebao.ls.crs.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.ebao.ls.crs.constant.CRSTinBizSource;
import com.ebao.ls.crs.vo.CRSIndividualLogVO;
import com.ebao.ls.crs.vo.CRSPartyLogVO;
import com.ebao.ls.crs.vo.CRSTaxIDNumberLogVO;
import com.ebao.ls.pa.nb.util.NBUtils;
import com.ebao.ls.pa.nb.util.NumericUtils;
import com.ebao.ls.pa.pub.MsgCst;
import com.ebao.ls.pa.pub.NbTaskCst;
import com.ebao.pub.util.json.EbaoStringUtils;

/**
 * <p>Title: CRS 聲明書-PCR_264035&263803_CRS專案</p>
 * <p>Description: 自我證明-個人</p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: TGL Co., Ltd.</p>
 * <p>Create Time: 2018/11/08</p> 
 * @author 
 * <p>Update Time: 2018/11/08</p>
 * <p>Updater: simon.huang</p>
 * <p>Update Comments: </p>
 */
public class CrsPersonAjaxService extends CrsAjaxService {

	public static Logger logger = Logger.getLogger(CrsPersonAjaxService.class);

	/**
	 * <p>Description :自我證明-個人 </p>
	 * <p>Created By : Simon.Huang</p>
	 * <p>Create Time : 2018年11月9日</p>
	 * @param in
	 * @param output
	 * @throws Exception
	 */
	public void init(Map<String, Object> in, Map<String, Object> output) throws Exception {

		Long partyLogId = NumericUtils.parseLong(in.get("partyLogId"));
		Long complementTaskId = NumericUtils.parseLong(in.get("complementTaskId"));
		
		if (partyLogId != null) {
			CRSPartyLogVO partyLogVO = crsPartyLogService.load(partyLogId);

			CRSIndividualLogVO personLogVO = null;
			List<CRSTaxIDNumberLogVO> taxLogList = new ArrayList<CRSTaxIDNumberLogVO>();
			if (partyLogVO != null) {
				List<CRSIndividualLogVO> personVOList = crsIndividualLogService.findByPartyLogId(partyLogId, false);
				if (personVOList.size() > 0) {
					personLogVO = personVOList.get(0);
					taxLogList = crsTaxLogService.findByPartyLogIdAndBizId(partyLogId, CRSTinBizSource.BIZ_SOURCE_PERSON, personLogVO.getLogId());

					if (complementTaskId != null
							&& 0 == nbTaskService.countTaskQueue(complementTaskId, NbTaskCst.TASK_QUEUE_CRS_PERSON)) {
						personLogVO = new CRSIndividualLogVO();
						taxLogList = new ArrayList<CRSTaxIDNumberLogVO>();
						personLogVO.setLogId(personLogVO.getLogId());
						personLogVO.setChangeId(personLogVO.getChangeId());
						personLogVO.setCrsPartyLog(partyLogVO);
					}
				} else {
					personLogVO = new CRSIndividualLogVO();
				}
			} else {
				output.put(KEY_SUCCESS, FAIL);
				output.put(KEY_ERROR_MSG, NBUtils.getTWMsg(MsgCst.MSG_1250566));
				return ;
			}

			output.put("crsIdentity", partyLogVO);
			output.put("crsPerson", personLogVO);
			output.put("crsTaxList", taxLogList);

			Map<String, Object> codeTableMap = new HashMap<String, Object>();
			Map<String, Map<String, String>> nationality = getNationalityCode(taxLogList);
			codeTableMap.put("nationality", nationality);

			//列表用code table
			output.put("codeTable", codeTableMap);

			output.put("lockPage", super.checkLockpage(partyLogVO, CRSTinBizSource.BIZ_SOURCE_PERSON));
			output.put("nbInfo", super.getNBInfo(partyLogVO));
			output.put(KEY_SUCCESS, SUCCESS);
			return;

		}

		output.put(KEY_SUCCESS, FAIL);
		output.put(KEY_ERROR_MSG, NBUtils.getTWMsg(MsgCst.MSG_1250566));
		return;
	}

	/**
	 * <p>Description :自我證明-個人 </p>
	 * <p>Created By : Simon.Huang</p>
	 * <p>Create Time : 2018年11月9日</p>
	 * @param in
	 * @param output
	 * @throws Exception
	 */
	public void save(Map<String, Object> in, Map<String, Object> output) throws Exception {

		Long partyLogId = NumericUtils.parseLong(in.get("partyLogId"));

		if (partyLogId != null) {

			String type = (String) in.get(KEY_TYPE);
			try {
				Map<String, Object> submitData = (Map<String, Object>) in.get(KEY_DATA);
				//頁面輸入資料
				Map<String, Object> srcPerson = (Map<String, Object>) submitData.get("person");
				//日期格式轉換

				if (UPDATE.equals(type)) {
					
				
					this.clearComplementTaskBeforeData(in, output);
					
					CRSPartyLogVO partyLogVO = crsPartyLogService.load(partyLogId);
					CRSIndividualLogVO personLogVO = null;
					List<CRSIndividualLogVO> personLogList = crsIndividualLogService.findByPartyLogId(partyLogId, false);

					if (personLogList.size() > 0) {
						personLogVO = personLogList.get(0);
					} else {
						personLogVO = new CRSIndividualLogVO();
						//需設定為true,才能寫入關連的PartyLogVO的partyLogId
						personLogVO.setCrsPartyLog(partyLogVO);
						personLogVO.setChangeId(partyLogVO.getChangeId());
						personLogVO = crsIndividualLogService.save(personLogVO, true);
					}
					EbaoStringUtils.setProperties(personLogVO, srcPerson);

					//不設true,只寫入自己的資料
					personLogVO = crsIndividualLogService.save(personLogVO);

					output.put("crsPerson", personLogVO);
					output.put(KEY_SUCCESS, SUCCESS);
					return;
				}
			} catch (Exception e) {
				throw e;
			} finally {

			}

		}
		output.put(KEY_SUCCESS, FAIL);
		output.put(KEY_ERROR_MSG, NBUtils.getTWMsg(MsgCst.MSG_1250566));
	}

	private void clearComplementTaskBeforeData(Map<String, Object> in, Map<String, Object> output) {
		Long partyLogId = NumericUtils.parseLong(in.get("partyLogId"));
		Long complementTaskId = NumericUtils.parseLong(in.get("complementTaskId"));
		if (complementTaskId != null
				&& 0 == nbTaskService.countTaskQueue(complementTaskId, NbTaskCst.TASK_QUEUE_CRS_PERSON)) {

			List<CRSIndividualLogVO> personVOList = crsIndividualLogService.findByPartyLogId(partyLogId, false);
			if (personVOList.size() > 0) {
				for(CRSIndividualLogVO personLogVO : personVOList) {
					// 清除補全前寫入稅藉編號資料
					List<CRSTaxIDNumberLogVO> taxLogList = crsTaxLogService.findByPartyLogIdAndBizId(partyLogId,
							CRSTinBizSource.BIZ_SOURCE_PERSON, personLogVO.getLogId());
					for (CRSTaxIDNumberLogVO taxLog : taxLogList) {
						crsTaxLogService.remove(taxLog.getEntityId());
					}
					
					crsIndividualLogService.remove(personLogVO.getEntityId());
				}
			}
			nbTaskService.saveTaskQueue(complementTaskId, NbTaskCst.TASK_QUEUE_CRS_PERSON, "");
		}
	}

}
