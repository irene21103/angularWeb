package com.ebao.ls.uw.ctrl.search;

import java.util.Collection;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.ls.uw.ds.UwPolicyService;
import com.ebao.ls.uw.ds.constant.UwExceptionConstants;
import com.ebao.ls.uw.ds.vo.QueryHelperVO;
import com.ebao.ls.uw.vo.UwPolicyVO;
import com.ebao.pub.annotation.DataModeChgModifyPoint;
import com.ebao.pub.framework.AppException;
import com.ebao.pub.framework.GenericAction;
import com.ebao.pub.framework.GenericException;
import com.ebao.pub.framework.MultiWarning;

/**
 * search pool
 * Title:Underwriting
 * Description:
 * Copyright: Copyright (c) 2004
 * Company: eBaoTech Corporation
 * @author Johnson.sun
 * @since  Jun 27, 2005
 * @version 1.0
 */
public class SearchPoolAction extends GenericAction {

  public static final String BEAN_DEFAULT = "/uw/uwPool";

@Override
  @DataModeChgModifyPoint("change aQueryValue type form old UwPolicyVO to QueryHelperVO")
  public MultiWarning processWarning(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

    MultiWarning warning = new MultiWarning();

    UwPoolForm formBean = (UwPoolForm) form;

    QueryHelperVO aQueryValue = new QueryHelperVO();

    // TODO UC023 for NB user,the default application type is NB,for CS user
    // is "CS"
    // when escalation,clear criteria
    if (request.getAttribute("isEscalated") != null) {
      formBean.setPolicyCodePool(null);
    } else {
      // else set these values
      formBean.populateUwPolicyValue(aQueryValue);
    }
    // process query
    Collection c = null;
    try {
      c = uwPolicyDS.processQuery(aQueryValue);
      request.setAttribute("poolForm", formBean);
    } catch (AppException e) {
      //For Defect CRDB00253722
      if (UwExceptionConstants.APP_RETRIEVE_LOCKED_POLICY == e.getErrorCode()) {
        warning.addWarning("ERR_" + e.getErrorCode(), e.getAddInfo());
      } else {
        throw e;
      }
    }
    if (c != null && c.size() > 0) {
      formBean.setppVOs((UwPolicyVO[]) c.toArray(new UwPolicyVO[0]));
      // set total count,for tag use
      request.setAttribute("total_count",
          String.valueOf(uwPolicyDS.getSearchTotalCount(aQueryValue)));
    } else {
      formBean.setppVOs(null);
      request.setAttribute("total_count", String.valueOf(0));

    }
    warning.setContinuable(false);
    return warning;
  }

  /**
   * search pool 
   * @param mapping
   * @param form
   * @param req
   * @param res
   * @throws com.ebao.pub.framework.GenericException
   * @return
   */
  @Override
  public ActionForward process(ActionMapping mapping, ActionForm form,
      HttpServletRequest req, HttpServletResponse res) throws GenericException {

    return mapping.findForward("pool");
  }

  @Resource(name = UwPolicyService.BEAN_DEFAULT)
  private UwPolicyService uwPolicyDS;

  public void setUwPolicyDS(UwPolicyService uwPolicyDS) {
    this.uwPolicyDS = uwPolicyDS;
  }

}