package com.ebao.ls.crs.batch.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;

import com.ebao.foundation.commons.exceptions.ExceptionUtil;
import com.ebao.ls.chl.service.AgentService;
import com.ebao.ls.chl.service.ChannelOrgService;
import com.ebao.ls.chl.vo.ChannelOrgVO;
import com.ebao.ls.chl.vo.ScAgentDataVO;
import com.ebao.ls.chl.vo.ScServiceAgentDataVO;
import com.ebao.ls.chl.vo.ScServiceRepresentDataVO;
import com.ebao.ls.crs.batch.service.CrsMotherService;
import com.ebao.ls.fatca.vo.FatcaDueDiligenceListVO;
import com.ebao.ls.foundation.service.GenericServiceImpl;
import com.ebao.ls.pty.data.bo.FatcaSurveyPopulation;
import com.ebao.ls.pty.data.query.FatcaSurveyPopulationDao;
import com.ebao.ls.pty.vo.FatcaSurveyPopulationVO;
import com.ebao.ls.pub.cst.YesNo;
import com.ebao.ls.sc.vo.AgentChlVO;
import com.ebao.pub.batch.type.LogLevel;
import com.ebao.pub.batch.util.BatchLogUtils;
import com.ebao.pub.framework.internal.ExceptionUtils;

public class CrsMotherServiceImpl  extends GenericServiceImpl<FatcaSurveyPopulationVO, FatcaSurveyPopulation, FatcaSurveyPopulationDao> implements CrsMotherService{
	
	@Resource(name=AgentService.BEAN_DEFAULT)
    private AgentService agentService;
    
    @Resource(name=ChannelOrgService.BEAN_DEFAULT)
    private ChannelOrgService channelServ;
    
	@Override
	public FatcaDueDiligenceListVO findLastestAgent(Long partyId, String certiCode, Long policyId, String policyCode) {
		try {
			/* 本張保單是否為孤兒保單#277983  */
			//找出保單目前服務業務員，若為孤兒保單，回傳空vo
			FatcaDueDiligenceListVO vo = this.getServiceAgent(policyId, Boolean.FALSE);
			if(vo != null)
				return vo;
			
			/* 若為孤兒保單，再找同一人的其他保單的服務業務員(從FATCA母體資料找)  */
			java.util.List<Long> alivePolicyIdList = dao.findAlivePolicyId(partyId, certiCode, null, null);
			if(CollectionUtils.isNotEmpty(alivePolicyIdList)) {
				/* 服務業務員 */
				vo = this.getServiceAgent(alivePolicyIdList.get(0), Boolean.FALSE);
				if(vo != null)
					return vo;
			}
			/* 服務代表 */
			vo = getRepresentAgent(policyId);
			if(vo == null) {
				//exception : 可能因為客戶郵遞區號錯誤(含空白)或新增郵遞區號，造成分派作業查無對應服務代表，造成部分孤兒保單無服務代表案例。
				vo = this.getServiceAgent(policyId, Boolean.TRUE);
				vo.setAgentBizCate(2L);
			}
			return vo;
		}catch(Exception ex) {
			BatchLogUtils.addLog(LogLevel.ERROR, null, 
					String.format("[party_Id :=%d, certi_Code := %s, policy_Id := %d, policy_Code := %s] error :=\n%s", 
							partyId, certiCode, policyId, policyCode, ExceptionUtils.getFullStackTrace(ex)));
			throw ExceptionUtil.parse(ex);
		}
	}
	
	private FatcaDueDiligenceListVO getServiceAgent(Long policyId, Boolean skipDummy) {
		/* 服務業務員 */
		ScAgentDataVO agentDataVO = agentService.getAgentDataByPolicyId(policyId);
		List<ScServiceAgentDataVO> currentServiceAgentList = agentDataVO.getServiceAgentList();
		Boolean isNotAlive = CollectionUtils.isEmpty(currentServiceAgentList);
		FatcaDueDiligenceListVO vo = new FatcaDueDiligenceListVO();
		if(isNotAlive) {
			return null;
		}
		ScServiceAgentDataVO currentServiceAgentVO = currentServiceAgentList.get(0);
		AgentChlVO currentAgentChlVO = agentService.findAgentByAgentId(currentServiceAgentVO.getAgentId());
		if (currentAgentChlVO == null)
			return null;
		ChannelOrgVO channelVO = channelServ.findByChannelCode(currentServiceAgentVO.getAgentChannelCode());
		vo.setAgentBizCate(1L);//非孤兒保單
		vo.setChannelOrg(channelVO.getChannelId());
        vo.setChannelCode(currentServiceAgentVO.getAgentChannelCode());
        vo.setChannelName(currentServiceAgentVO.getAgentChannelName());
        vo.setChannelType(currentServiceAgentVO.getAgentChannelType());
        vo.setAgentName(currentServiceAgentVO.getAgentName());
        vo.setAgentRegisterCode(currentServiceAgentVO.getAgentRegisterCode());
        vo.setServiceAgent(currentServiceAgentVO.getAgentId());
        if(Boolean.FALSE.equals(skipDummy) && YesNo.YES_NO__YES.equals(channelVO.getIsDummy())){//孤兒保單
        	return null;
        }
        return vo;
	}
	
	private FatcaDueDiligenceListVO getRepresentAgent(Long policyId) {
		FatcaDueDiligenceListVO vo = new FatcaDueDiligenceListVO();
		ScAgentDataVO agentDataVO = agentService.getAgentDataByPolicyId(policyId);
		List<ScServiceRepresentDataVO> representAgentList = agentDataVO.getServiceRepresentList();
		if(CollectionUtils.isEmpty(representAgentList))
			return null;
		ScServiceRepresentDataVO representVO = representAgentList.get(0);
		
		AgentChlVO currentAgentChlVO = agentService.findAgentByAgentId(representVO.getAgentId());
		if (currentAgentChlVO == null)
			return null;
		ChannelOrgVO channelVO = channelServ.findByChannelCode(representVO.getRepresentChannelCode());
		vo.setServiceAgent(representVO.getAgentId());
		vo.setAgentName(representVO.getRepresentName());
		vo.setAgentRegisterCode(representVO.getRepresentRegisterCode());
		vo.setChannelOrg(channelVO.getChannelId());
		vo.setChannelCode(representVO.getRepresentChannelCode());
		vo.setChannelName(representVO.getRepresentChannelName());
		vo.setChannelType(channelVO.getChannelType());
		vo.setAgentBizCate(2L);
		return vo;
	}
	@Override
	protected FatcaSurveyPopulationVO newEntityVO() {
		
		return new FatcaSurveyPopulationVO();
	}

}
