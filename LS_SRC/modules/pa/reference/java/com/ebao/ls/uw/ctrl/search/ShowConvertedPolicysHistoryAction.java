package com.ebao.ls.uw.ctrl.search;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.ls.uw.ctrl.pub.UwConditionForm;
import com.ebao.ls.uw.ctrl.pub.UwEndorsementForm;
import com.ebao.ls.uw.ctrl.pub.UwExclusionForm;
import com.ebao.ls.uw.ctrl.pub.UwProductForm;
import com.ebao.ls.uw.ds.UwPolicyService;
import com.ebao.ls.uw.ds.constant.UwExceptionConstants;
import com.ebao.ls.uw.vo.UwConditionVO;
import com.ebao.ls.uw.vo.UwEndorsementVO;
import com.ebao.ls.uw.vo.UwExclusionVO;
import com.ebao.ls.uw.vo.UwPolicyVO;
import com.ebao.ls.uw.vo.UwProductVO;
import com.ebao.pub.framework.AppException;
import com.ebao.pub.framework.ExceptionFactory;
import com.ebao.pub.framework.GenericAction;
import com.ebao.pub.framework.GenericException;
import com.ebao.pub.util.BeanUtils;

/**
 * show converted uw history
 * <p>Title:Underwriting</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: eBaoTech Corporation</p>
 * @author yixing.lu
 * @since  23.03.2005
 * @version 1.0 
 */
public class ShowConvertedPolicysHistoryAction extends GenericAction {

  public static final String UW_HISTORY_LIST = "uw_history_list";

  /**
   * @param mapping
   * @param form
   * @param req
   * @param res
   * @return
   * @throws GenericException 
   */
  @Override
  public ActionForward process(ActionMapping mapping, ActionForm form,
      HttpServletRequest req, HttpServletResponse res) throws GenericException {
    try {
      Long policyId = Long.valueOf(req.getParameter("policyId"));
      Collection result = new ArrayList();
      Collection histories = uwPolicyDS.queryConvertedUwHistory(policyId);
      if (!histories.isEmpty()) {
        Iterator historyIter = histories.iterator();
        //changes UwHistory into UwHistoryForm

        while (historyIter.hasNext()) {
          UwPolicyVO uwPolicyVO = (UwPolicyVO) historyIter.next();
          UwHistoryForm uwHistoryForm = new UwHistoryForm();
          BeanUtils.copyProperties(uwHistoryForm, uwPolicyVO);
          //get UwProductForms and attaches the UwProductForms into UwHistoryForm
          Long underwriteId = uwHistoryForm.getUnderwriteId();
          Collection products = uwPolicyDS.findUwProductEntitis(underwriteId);
          UwProductForm[] uwProductForm = null;
          if (null != products) {
            UwProductVO[] productVOs = (UwProductVO[]) products
                .toArray(new UwProductVO[0]);
            uwProductForm = (UwProductForm[]) BeanUtils.copyArray(
                UwProductForm.class, productVOs);
          }
          uwHistoryForm.setUwproducts(uwProductForm);

          //get UwConditionForms and attaches the UwConditionForms into UwHistoryForm
          Collection conditions = uwPolicyDS
              .findUwConditionEntitis(underwriteId);
          UwConditionForm[] uwConditionForm = null;
          if (null != conditions) {
            UwConditionVO[] productVOs = (UwConditionVO[]) conditions
                .toArray(new UwConditionVO[0]);
            uwConditionForm = (UwConditionForm[]) BeanUtils.copyArray(
                UwConditionForm.class, productVOs);
          }
          uwHistoryForm.setUwconditions(uwConditionForm);

          //get UwExclusionForms and attaches the UwUwExclusionForms into UwHistoryForm
          Collection exclusions = uwPolicyDS
              .findUwExclusionEntitis(underwriteId);
          UwExclusionForm[] uwExclusionForm = null;
          if (null != exclusions) {
            UwExclusionVO[] productVOs = (UwExclusionVO[]) exclusions
                .toArray(new UwExclusionVO[0]);
            uwExclusionForm = (UwExclusionForm[]) BeanUtils.copyArray(
                UwExclusionForm.class, productVOs);
          }
          uwHistoryForm.setUwexclusions(uwExclusionForm);

          //get UwEndorsementForm and attaches the UwEndorsementForm into UwHistoryForm
          Collection endowsments = uwPolicyDS
              .findUwEndorsementEntitis(underwriteId);
          UwEndorsementForm[] uwEndorsementForm = null;
          if (null != endowsments) {
            UwEndorsementVO[] productVOs = (UwEndorsementVO[]) endowsments
                .toArray(new UwEndorsementVO[0]);
            uwEndorsementForm = (UwEndorsementForm[]) BeanUtils.copyArray(
                UwEndorsementForm.class, productVOs);
          }
          uwHistoryForm.setUwendorsements(uwEndorsementForm);
          result.add(uwHistoryForm);
        }//end if 
      }
      //stores the result into the request
      if (result.isEmpty()) {
        throw new AppException(UwExceptionConstants.APP_POLICY_NOT_FOUND);
      }
      req.setAttribute(UW_HISTORY_LIST, result);
      return mapping.findForward("convertedHistory");
    } catch (Exception e) {
      throw ExceptionFactory.parse(e);
    }
  }

  @Resource(name = UwPolicyService.BEAN_DEFAULT)
  private UwPolicyService uwPolicyDS;

  public void setUwPolicyDS(UwPolicyService uwPolicyDS) {
    this.uwPolicyDS = uwPolicyDS;
  }

}