package com.ebao.ls.uw.ctrl.helper;

import com.ebao.pub.framework.GenericException;
import com.ebao.pub.framework.MultiWarning;

public interface LetterValidationHelper {
  static final String BEAN_DEFAULT = "uwLetterValidationHelper";

  public void checkPendingLetter4NbUw(MultiWarning multiWarning, Long policyId)
      throws GenericException;
  
  public void checkPendingLetter4CsUw(MultiWarning multiWarning, Long changeId)
      throws GenericException;
}
