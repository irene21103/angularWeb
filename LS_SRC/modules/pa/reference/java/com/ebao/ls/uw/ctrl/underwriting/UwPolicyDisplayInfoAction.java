package com.ebao.ls.uw.ctrl.underwriting;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.foundation.module.db.hibernate.HibernateSession3;
import com.ebao.ls.escape.helper.EscapeHelper;
import com.ebao.ls.pa.nb.ctrl.detailreg.DetailRegAllInOneForm;
import com.ebao.ls.pa.nb.ctrl.detailreg.DetailRegHelper;
import com.ebao.ls.pa.pub.bs.PolicyService;
import com.ebao.ls.pub.CodeCst;
import com.ebao.ls.pub.cache.ActionDataCache;
import com.ebao.ls.uw.ctrl.UwGenericAction;
import com.ebao.pub.framework.AppException;
import com.ebao.pub.framework.ExceptionFactory;
import com.ebao.pub.framework.GenericException;
import com.ebao.pub.util.StringUtils;

/**
 * <p>
 * Title:Underwriting
 * </p>
 * <p>
 * Description:
 * </p>
 * <p>
 * Copyright: Copyright (c) 2004
 * </p>
 * <p>
 * Company: eBaoTech Corporation
 * </p>
 * 
 * @author sunny
 * @since 2015-05-22
 * @version 1.0
 */
public class UwPolicyDisplayInfoAction extends UwGenericAction {
	public static final String BEAN_DEFAULT = "/uw/uwPolicyDisplayInfo";

	public ActionForward uwProcess(ActionMapping mapping, ActionForm form,
					HttpServletRequest request, HttpServletResponse response)
					throws GenericException {
		String forword = null;
		try {
			javax.servlet.http.HttpSession session = request.getSession();

			//IR-290576-核保中修改頁面清除AML cache
			ActionDataCache.clear();

			EscapeHelper.escapeHtml(request).setAttribute("uwId", EscapeHelper.escapeHtml(request.getParameter("underwriteId")));

			DetailRegAllInOneForm detailRegForm = (DetailRegAllInOneForm) form;

			String strPolicyId = detailRegForm.getPolicyId();
			//清空action form(session scope)
			BeanUtils.copyProperties(form, new DetailRegAllInOneForm());

			Long policyChgId = null;
			String workFlow = "uw";
			if (!StringUtils.isNullOrEmpty(strPolicyId)
							&& !"null".equalsIgnoreCase(strPolicyId)) {

				Long policyId = Long.valueOf(strPolicyId);
				
				detailRegHelper.fillDetailRegAllInOneForm(request, detailRegForm, policyId, workFlow, policyChgId);
				
				// 2016-06-04 ChiaHui 增加核保流程控管機制
				policyService.updatePolicyOPQCheckIndi(policyId, CodeCst.OPQ_WAITING_CHECK);
			} else {
				throw new AppException(20411020007L);
			}
			if (detailRegForm.getProductId() != null) {
				/*
				 * NBRelatedInfoCIVO nbRelatedInfoCIVO = prdDefCI.getNBRelatedInfo(Long
				 * .valueOf(detailRegForm.getProductId()));
				 * request.setAttribute("forwardPermit",
				 * String.valueOf(nbRelatedInfoCIVO .isForwardAllowed()));
				 */
				request.setAttribute("forwardPermit", "false");
			}
			
            if( CodeCst.YES_NO__YES.equals(EscapeHelper.escapeHtml(request.getParameter("lockPage")))) {
           	 EscapeHelper.escapeHtml(request).setAttribute("lockPage", EscapeHelper.escapeHtml(request.getParameter("lockPage")));
           }
          
           if( CodeCst.YES_NO__YES.equals(EscapeHelper.escapeHtml(request.getParameter("displayPolicyInfo")))) {
               EscapeHelper.escapeHtml(request).setAttribute("displayPolicyInfo", EscapeHelper.escapeHtml(request.getParameter("displayPolicyInfo")));
           }
			forword = "display";
			return mapping.findForward(forword);
		} catch (Exception e) {
			throw ExceptionFactory.parse(e);
		}
	}

	@Resource(name = DetailRegHelper.BEAN_DEFAULT)
	private DetailRegHelper detailRegHelper;

	@Resource(name = PolicyService.BEAN_DEFAULT)
	private PolicyService policyService;

}