package com.ebao.ls.uw.ctrl.elderAudio;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ebao.ls.encode.encoder.Encode;
import org.apache.commons.lang.ClassUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.foundation.core.logging.Log;
import com.ebao.ls.chl.service.ChannelOrgService;
import com.ebao.ls.chl.vo.ChannelOrgVO;
import com.ebao.ls.cmu.pub.document.DocumentFormFile;
import com.ebao.ls.cs.logger.ApplicationLogger;
import com.ebao.ls.cs.logger.helper.AppLogDataHelper;
import com.ebao.ls.escape.helper.EscapeHelper;
import com.ebao.ls.image.vo.BillCardVersionVO;
import com.ebao.ls.notification.extension.letter.nb.NbLetterExtensionVO;
import com.ebao.ls.pa.nb.bs.BillCardMdyService;
import com.ebao.ls.pa.nb.util.NBUtils;
import com.ebao.ls.pa.nb.util.UnbCst;
import com.ebao.ls.pa.pub.bs.PolicyService;
import com.ebao.ls.pa.pub.vo.PolicyVO;
import com.ebao.ls.uw.ctrl.helper.ElderAudioHelper;
import com.ebao.ls.uw.ctrl.letter.UNBLetterAction;
import com.ebao.ls.uw.ds.UwElderAudioService;
import com.ebao.ls.uw.vo.UwElderAudioVO;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.i18n.lang.StringResource;
import com.ebao.pub.util.DateUtils;

public class ElderAudioReviewAction extends UNBLetterAction {

	public static final String BEAN_DEFAULT = "/uw/elderAudioReview";

	public final static String ACTION__SAVE_TASK = "saveTask";

	public final static String ACTION__SEND_CRM = "send2CRM";

	public final static String ACTION__DEL_TASK = "delTask";

	private final Log logger = Log.getLogger(ElderAudioReviewAction.class);

	private static final String _FORWARD_DISPLAY = "display";
	private static final String _MESSAGE_ATTR = "message";
	private static final String _DATE_FORMAT_YYMMDD = "yyMMdd";

	@Resource(name = PolicyService.BEAN_DEFAULT)
	private PolicyService policyService;

	@Resource(name = ElderAudioHelper.BEAN_DEFAULT)
	private ElderAudioHelper elderAudioHelper;

	@Resource(name = UwElderAudioService.BEAN_DEFAULT)
	private UwElderAudioService uwElderAudioService;

	@Resource(name = BillCardMdyService.BEAN_DEFAULT)
	private BillCardMdyService billCardMdyService;

	@Resource(name = ChannelOrgService.BEAN_DEFAULT)
	private ChannelOrgService channelOrgService;

	private SimpleDateFormat yyDDmm = new SimpleDateFormat(_DATE_FORMAT_YYMMDD);

	@Override
	public ActionForward process(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		ElderAudioForm aForm = (ElderAudioForm) form;
		logger.debug("process().ElderAudioForm >> \n" + aForm.toString());

		String actionType = aForm.getActionType();
		Long policyId = aForm.getPolicyId();
		PolicyVO policyVO = policyService.load(policyId);

		Date applyDate = policyVO.getApplyDate();
		String elderAudiochannel = policyService.getElderAudioChannel(policyVO);

		HttpSession session = request.getSession();
		if (StringUtils.isBlank(actionType)) {
			session.removeAttribute(ElderAudioHelper.AUDIO_OBJECTS_ATTR);
			session.removeAttribute(ElderAudioHelper.REVIEW_TASK_LIST_ATTR);

			// 取得本保單會辦對象清單
			List<ElderAudioObject> elderAudioObjects = elderAudioHelper.getElderAudioObjects(policyId, applyDate);
			EscapeHelper.escapeHtml(session).setAttribute(ElderAudioHelper.AUDIO_OBJECTS_ATTR, elderAudioObjects); /** Session Fixation */
		} else if (ACTION__SAVE_TASK.equals(actionType)) {
			UwElderAudioVO audioReviewTask = new UwElderAudioVO();
			audioReviewTask.setPolicyId(policyId);
			audioReviewTask.setUnderwriteId(aForm.getUnderwriteId());
			audioReviewTask.setConsultDate(AppContext.getCurrentUserLocalTime());

			// 處理會辦對象資料
			String certiCode = aForm.getSelectedCertiCode();
			String roleType = null;
			if (StringUtils.isBlank(certiCode)) {
				ElderAudioObject otherObj = aForm.getOtherObj();
				if (otherObj.isEmpty()) {
					request.setAttribute(_MESSAGE_ATTR, "會辦對象未點選或輸入，請確認");
					return mapping.findForward(_FORWARD_DISPLAY);
				} else {
					certiCode = otherObj.getCertiCode();
					roleType = otherObj.getFirstUnbRole();

					audioReviewTask.setRoleType(roleType);
					audioReviewTask.setName(otherObj.getName());
					audioReviewTask.setCertiCode(certiCode);
					audioReviewTask.setBirthday(otherObj.getBirthdate());
				}
			} else {
				List<ElderAudioObject> elderAudioObjects = (List<ElderAudioObject>) session.getAttribute(ElderAudioHelper.AUDIO_OBJECTS_ATTR);
				for (ElderAudioObject audioObject : elderAudioObjects) {
					if (certiCode.equals(audioObject.getCertiCode())) {
						roleType = audioObject.getFirstUnbRole();

						audioReviewTask.setRoleType(roleType);
						audioReviewTask.setName(audioObject.getName());
						audioReviewTask.setCertiCode(certiCode);
						audioReviewTask.setBirthday(audioObject.getBirthdate());

						break;
					}
				}
			}
			// logger.error("certiCode >> " + certiCode);
			// logger.error("roleType >> " + roleType);

			// 檢查錄音檔流水編號：錄音流水編號長度必需是10碼數字，可以多筆編號一同輸入，中間以「;」隔開
			String audioNo = aForm.getAudioNo();
			if (StringUtils.isNotBlank(audioNo)) {
				audioNo = audioNo.trim();
				String[] audioNoArray = audioNo.split(";");

				for (String tmpAudioNo : audioNoArray) {
					if(!NBUtils.isLegalAudioNoStr(tmpAudioNo, elderAudiochannel)) {
			        	Map<String, String> strParam = new HashMap<String, String>();
						if(UnbCst.NB_ELDER_AUDIO_CHANNEL_ESUN.equals(elderAudiochannel)) {
			            	//玉山銀要顯示單位簡稱
			            	//以UnbCst.NB_CHANNEL_CODE_ESUN取得單位簡稱
							ChannelOrgVO channelOrgVO = channelOrgService.findByChannelCode(UnbCst.NB_CHANNEL_CODE_ESUN);
	                    	strParam.put("0", channelOrgVO.getOrgAbbr());
		        			//玉山銀-21碼
			            	strParam.put("1", NBUtils.getAudioSpec(UnbCst.NB_ELDER_AUDIO_CHANNEL_ESUN));
			            } else {
			            	strParam.put("0", "");
			            	strParam.put("1", NBUtils.getAudioSpec(UnbCst.NB_ELDER_AUDIO_CHANNEL_DEFAULT));
						}
						
						 //組成錯誤訊息 <0: 單位簡稱>錄音流水編號有誤(非<1:碼數>碼)，請確認
			        	String errorMsg =  StringResource.getStringData("MSG_1267474", AppContext.getCurrentUser().getLangId(), strParam);
						request.setAttribute(_MESSAGE_ATTR, errorMsg);
						return mapping.findForward(_FORWARD_DISPLAY);
					} else {
						if(NBUtils.fetchAudioDateEarlyOf(tmpAudioNo, applyDate, elderAudiochannel)) {
							request.setAttribute(_MESSAGE_ATTR, "錄音日期早於要保書填寫日期，請確認");
							return mapping.findForward(_FORWARD_DISPLAY);
						}
					}
				} // for audioNoArray

				// 「高齡銷售錄音任務列表」區塊己存在相同會辦資料(相同身分證號及相同錄音流水編號)，顯示錯誤訊息「同一會辦對象不可重覆會辦，請修改」。
				Object sessionObj = session.getAttribute(ElderAudioHelper.REVIEW_TASK_LIST_ATTR);
				if (null != sessionObj) {
					List<UwElderAudioVO> elderAudioReviewTasks = (List<UwElderAudioVO>) sessionObj;
					Map<String, UwElderAudioVO> taskMap = new HashMap<String, UwElderAudioVO>();
					for (UwElderAudioVO uwElderAudioVO : elderAudioReviewTasks) {
						taskMap.put(uwElderAudioVO.getCertiCode() + uwElderAudioVO.getAudioNoList(), uwElderAudioVO);
					} // for
					UwElderAudioVO tmpObj = taskMap.get(certiCode + audioNo);
					if (null != tmpObj) {
						request.setAttribute(_MESSAGE_ATTR, "同一會辦對象、同一組錄音流水編號不可重覆會辦，請修改");
						return mapping.findForward(_FORWARD_DISPLAY);
					}
				}
			} else {
				request.setAttribute(_MESSAGE_ATTR, "錄音流水編號不可為空白，請確認");
				return mapping.findForward(_FORWARD_DISPLAY);
			}

			// 處理檔案名稱
			List<String> fileNames = new ArrayList<String>();

			// 取得保單影像檔名
			String imageNames = aForm.getPolicyImageNames();
			if (StringUtils.isNotBlank(imageNames)) {
				String[] tempArray = imageNames.split(",");
				for (int i = 0; i < tempArray.length; i++) {
					fileNames.add(tempArray[i]);
				} // for
			}

			// User 上傳的檔案，從 request 中取出來
			List<DocumentFormFile> uploadFileList = nbLetterHelper.getUploadFileList(request, aForm);
			// logger.error("uploadFileList size >> " + uploadFileList.size());
			if (false == uploadFileList.isEmpty()) {
				for (DocumentFormFile documentFormFile : uploadFileList) {
					fileNames.add(documentFormFile.getFileName());
				}
			}
			if (false == fileNames.isEmpty()) {
				audioReviewTask.setFileNameList(StringUtils.join(fileNames, ";"));
			}

			// 處理錄音檔流水編號
			audioReviewTask.setAudioNoList(aForm.getAudioNo());

			ApplicationLogger.setSystemCode(AppLogDataHelper.SYSTEM_CODE_UNB);
			ApplicationLogger.setJobName(ClassUtils.getShortClassName(this.getClass()));
	 		ApplicationLogger.setPolicyCode(policyVO.getPolicyNumber());
			ApplicationLogger.setOptionId(DateUtils.date2String(new Date(), "yyyyMMdd HHmmss"));
			ApplicationLogger.addLoggerData(com.ebao.pub.util.EnvUtils.getEnvLabel()+", elderAudioHelper.saveTempFile");
			// 儲存實體檔案
			List<String> returnMsgs = elderAudioHelper.saveTempFile(policyVO.getPolicyNumber(), audioReviewTask, uploadFileList, aForm.getApplyImageList());
			audioReviewTask.setTaskNum(returnMsgs.get(0));
			if (returnMsgs.size() > 1) {
				returnMsgs.remove(0);
				String errDtl = StringUtils.join(returnMsgs, "；");
				request.setAttribute(_MESSAGE_ATTR, NBUtils.getTWMsg("MSG_1267179")+"("+errDtl+")");//高齡銷售錄音會辦作業失敗，請洽開發人員
				ApplicationLogger.addLoggerData(errDtl);
				try {
					ApplicationLogger.flush();
				} catch (Exception e1) {
					logger.info("ApplicationLogger.flush() : " + e1.getMessage(), e1);
				}
				return mapping.findForward(_FORWARD_DISPLAY);
			}

			// 加入任務清單
			List<UwElderAudioVO> elderAudioReviewTasks = (List<UwElderAudioVO>) session.getAttribute(ElderAudioHelper.REVIEW_TASK_LIST_ATTR);
			if (null == elderAudioReviewTasks) {
				elderAudioReviewTasks = new ArrayList<UwElderAudioVO>();
			}
			elderAudioReviewTasks.add(audioReviewTask);
			Encode.encodeForHTML(session).setAttribute(ElderAudioHelper.REVIEW_TASK_LIST_ATTR, elderAudioReviewTasks);

			// 回傳之前要清空資料
			aForm.setSelectedCertiCode(null);
			aForm.setActionType(null);
			aForm.setOtherObj(new ElderAudioObject());
			aForm.setAudioNo(null);
			aForm.setApplyImageList(null);
		} else if (ACTION__DEL_TASK.equals(actionType)) {
			String[] delCertiCodes = aForm.getSelectedTaskList();
			List<UwElderAudioVO> elderAudioReviewTasks = (List<UwElderAudioVO>) session.getAttribute(ElderAudioHelper.REVIEW_TASK_LIST_ATTR);
			for (int i = 0; i < delCertiCodes.length; i++) {
				for (UwElderAudioVO uwElderAudioVO : elderAudioReviewTasks) {
					if (delCertiCodes[i].equals(uwElderAudioVO.getCertiCode())) {
						elderAudioReviewTasks.remove(uwElderAudioVO);
						break;
					}
				} // for elderAudioReviewTasks
			} // for delCertiCodes

			Encode.encodeForHTML(session).setAttribute(ElderAudioHelper.REVIEW_TASK_LIST_ATTR, elderAudioReviewTasks);
		} else if (ACTION__SEND_CRM.equals(actionType)) {
			Object sessionObj = session.getAttribute(ElderAudioHelper.REVIEW_TASK_LIST_ATTR);
			if (null != sessionObj) {
				ApplicationLogger.setSystemCode(AppLogDataHelper.SYSTEM_CODE_UNB);
				ApplicationLogger.setJobName(ClassUtils.getShortClassName(this.getClass()));
		 		ApplicationLogger.setPolicyCode(policyVO.getPolicyNumber());
				ApplicationLogger.setOptionId(DateUtils.date2String(new Date(), "yyyyMMdd HHmmss"));
				ApplicationLogger.addLoggerData(com.ebao.pub.util.EnvUtils.getEnvLabel() + ", elderAudioHelper.sendReviewTaskToCRM");

				List<UwElderAudioVO> elderAudioReviewTasks = (List<UwElderAudioVO>) sessionObj;
				String resultMsg = elderAudioHelper.sendReviewTaskToCRM(elderAudioReviewTasks, policyVO.getPolicyNumber());

				try {
					ApplicationLogger.flush();
				} catch (Exception e1) {
					logger.info("ApplicationLogger.flush() : " + e1.getMessage(), e1);
				}

				if (StringUtils.isNotBlank(resultMsg)) {
					request.setAttribute(_MESSAGE_ATTR, NBUtils.getTWMsg("MSG_1267178") + ": " + resultMsg);//CRM系統連線失敗，請洽開發人員: 顯示失敗資料的姓名+”-“+音流水編號, 若有多筆”/”隔開
				} else {
					session.removeAttribute(ElderAudioHelper.REVIEW_TASK_LIST_ATTR);
				}
			} else {
				request.setAttribute(_MESSAGE_ATTR, "沒有需要高齡銷售錄音會辦的資料，請確認");
			}
		}

		return mapping.findForward(_FORWARD_DISPLAY);
	}

	@Override
	protected NbLetterExtensionVO getExtension(ActionForm unbLetterForm) {
		// TODO Auto-generated method stub
		return null;
	}

}
