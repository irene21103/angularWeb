package com.ebao.ls.riskPrevention.ctrl;

import com.ebao.ls.riskPrevention.vo.NationalityCodeLogVO;
import com.ebao.ls.riskPrevention.vo.NationalityCodeVO;
import com.ebao.pub.web.pager.PagerFormImpl;

public class NationalityCodeForm extends PagerFormImpl{
	
	private static final long serialVersionUID = 2991864442137279291L;
	private NationalityCodeLogVO searchVO = new NationalityCodeLogVO();
	private NationalityCodeVO vo = new NationalityCodeVO();
	private boolean newMode = false; // addMode
	private boolean saveMode = false;
	/**	historySearch 
	*	true	某國家風險查詢
	*	false	高風險查詢 > 全部查詢 
	**/
	public NationalityCodeForm(){
		super.getPager().setPageSize(8);
	}

	public NationalityCodeLogVO getSearchVO() {
		return searchVO;
	}

	public void setSearchVO(NationalityCodeLogVO searchVO) {
		this.searchVO = searchVO;
	}

	public NationalityCodeVO getVo() {
		return vo;
	}

	public void setVo(NationalityCodeVO vo) {
		this.vo = vo;
	}

	public boolean isNewMode() {
		return newMode;
	}

	public void setNewMode(boolean newMode) {
		this.newMode = newMode;
	}

	public boolean isSaveMode() {
		return saveMode;
	}

	public void setSaveMode(boolean saveMode) {
		this.saveMode = saveMode;
	}
}
