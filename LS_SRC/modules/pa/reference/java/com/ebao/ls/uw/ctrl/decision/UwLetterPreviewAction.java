package com.ebao.ls.uw.ctrl.decision;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.ls.letter.common.CommonLetterService;
import com.ebao.ls.notification.message.WSLetterUnit;
import com.ebao.ls.pa.nb.util.NumericUtils;
import com.ebao.ls.pub.cst.Template;
import com.ebao.ls.uw.ctrl.pub.UwActionHelper;
import com.ebao.pub.annotation.PrdAPIUpdate;
import com.ebao.pub.framework.ExceptionFactory;
import com.ebao.pub.framework.GenericAction;
import com.ebao.pub.framework.GenericException;

/**
 * <p>Title: 核保作業</p>
 * <p>Description:核保作業 相關letter PDF預覽 共用action</p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company: TGL Co., Ltd.</p>
 * <p>Create Time: Aug 21, 2016</p> 
 * @author 
 * <p>Update Time: Aug 21, 2016</p>
 * <p>Updater: simon.huang</p>
 * <p>Update Comments: </p>
 */
public class UwLetterPreviewAction extends GenericAction {

	public static final String BEAN_DEFAULT = "/uwLetterPreview";

	@Resource(name = UwActionHelper.BEAN_DEFAULT)
	protected UwActionHelper uwActionHelper;

	@Resource(name = CommonLetterService.BEAN_DEFAULT)
	protected CommonLetterService commonLetterService;

	/**
	 * @param mapping
	 * @param form
	 * @param request
	 * @param res
	 * @return
	 * @throws GenericException
	 */
	@Override
	@PrdAPIUpdate
	public ActionForward process(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws GenericException {

		Long policyId = NumericUtils.parseLong(com.ebao.ls.encode.encoder.Encode.encodeForHTML(request).getParameter("policyId"));
		Long underwriteId = NumericUtils.parseLong(com.ebao.ls.encode.encoder.Encode.encodeForHTML(request).getParameter("underwriteId"));
		String letter = com.ebao.ls.encode.encoder.Encode.encodeForHTML(request).getParameter("letter");
		try {
			if (policyId != null && underwriteId != null) {
				WSLetterUnit unit = null;
				if ("uwExclusion".equals(letter)) {
					Long insuredId = NumericUtils.parseLong(request.getParameter("insuredId"));
					unit = uwActionHelper.getFmtUnb0381Letter(policyId, underwriteId, insuredId);
					commonLetterService.previewLetterSetupPDFHeader(request, response, Template.TEMPLATE_20023.getTemplateId(), unit );
				} else if ("uwExtraPrem".equals(letter)) {
					unit = uwActionHelper.getFmtUnb0441Letter(policyId, underwriteId);
					commonLetterService.previewLetterSetupPDFHeader(request, response, Template.TEMPLATE_20024.getTemplateId() , unit);
				}
			}
		} catch (Exception ex) {
			throw ExceptionFactory.parse(ex);
		}
		return null;
	}

}