package com.ebao.ls.riskPrevention.bs.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.ebao.ls.foundation.service.GenericServiceImpl;
import com.ebao.ls.riskPrevention.bs.NationalityCodeService;
import com.ebao.ls.riskPrevention.data.NationalityCodeDao;
import com.ebao.ls.riskPrevention.data.bo.NationalityCode;
import com.ebao.ls.riskPrevention.vo.NationalityCodeVO;
import com.ebao.pub.web.pager.Pager;

public class NationalityCodeServiceImpl extends GenericServiceImpl<NationalityCodeVO, NationalityCode, NationalityCodeDao> implements NationalityCodeService{
	
	@Resource(name = NationalityCodeDao.BEAN_DEFAULT)
	public void setDao(NationalityCodeDao dao){
		super.setDao(dao);
	}
	
	@Override
	protected NationalityCodeVO newEntityVO() {
		return new NationalityCodeVO();
	}
	
	public NationalityCodeVO newNationalityCodeVO(){
		NationalityCodeVO vo = new NationalityCodeVO();
		List<Map<String,Object>> result =super.getDao().nextCode();
		String value = (String)result.get(0).get("NATIONALITY_CODE");
		int valueInt = Integer.parseInt(value);
		int nextValue = valueInt+1;
		vo.setNationalCode(String.valueOf(nextValue));
		return vo;
	}
	
	@Override
	public String getCurrentNatationlCode() {
		return (String) super.getDao().nextCode().get(0).get("NATIONALITY_CODE");
	}
	
	@Override
	public List<Map<String, Object>> query(NationalityCodeVO vo, Pager pager) {
		NationalityCode bo = new NationalityCode();
		bo.copyFromVO(vo, true, false);
		return super.getDao().query(bo, pager);
	}

	@Override
	public boolean update(NationalityCodeVO vo, Pager pager) {
		if(vo.getNationalCode()==null || vo.getNationalRisk()==null)
			return false;
		else{
			NationalityCode bo = new NationalityCode();
			bo.copyFromVO(vo, true, false);
			super.getDao().saveorUpdate(bo);
			return true;
		}
	}

	
	public boolean insert(NationalityCodeVO vo, Pager pager) {
		if(vo.getNationalCode()==null || vo.getNationalRisk()==null || vo.getNationalName() == null || vo.getEnNationalShortName()==null)
			return false;
		else{
			NationalityCode bo = new NationalityCode();
			bo.copyFromVO(vo, true, false);
			super.getDao().save(bo);
			return true;
		}
	}

	@Override
	public String findRiskByNationalityCode(String nationalCode) {
		NationalityCode bo = new NationalityCode();
		bo.setNationalCode(nationalCode);
		List<Map<String,Object>>result = super.getDao().query(bo, null);
		if(result!=null){
			if(result.size()>0){
				return (String)result.get(0).get("RISK_NATIONALITY");
			}
			return null;
		}
		return null;
	}

	@Override
	public String findRiskByNationalityName(String nationalName) {
		NationalityCode bo = new NationalityCode();
		bo.setNationalName(nationalName);
		List<Map<String,Object>>result = super.getDao().query(bo, null);
		if(result!=null){
			if(result.size()>0){
				return (String)result.get(0).get("RISK_NATIONALITY");
			}
			return null;
		}
		return null;
	}

    @Override
    public List<Map<String, Object>> multiSearch(NationalityCodeVO vo, boolean isRiskSearch) {
        NationalityCode bo = new NationalityCode();
        bo.copyFromVO(vo, true, false);
        if(!isRiskSearch){
            bo.setNationalRisk(null);
        }
        return super.getDao().multiSearch(bo);
    }
}
