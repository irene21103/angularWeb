package com.ebao.ls.callout.ctrl;

import com.ebao.ls.callout.vo.CalloutConfigVO;
import com.ebao.pub.web.pager.PagerFormImpl;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.commons.collections.ListUtils;

public class CalloutConfigForm extends PagerFormImpl {

    private static final long serialVersionUID = -5104714951253472209L;

    private CalloutConfigVO calloutConfigVO = new CalloutConfigVO();

    private CalloutConfigVO calloutConfigVO1 = new CalloutConfigVO();

    private CalloutConfigVO calloutConfigVO2 = new CalloutConfigVO();

    private List<CalloutConfigVO> gridList = ListUtils.lazyList(new ArrayList<CalloutConfigVO>(), CalloutConfigVO::new);

    private List<CalloutConfigVO> gridList1 = ListUtils.lazyList(new ArrayList<CalloutConfigVO>(), CalloutConfigVO::new);

    private List<CalloutConfigVO> gridList2 = ListUtils.lazyList(new ArrayList<CalloutConfigVO>(), CalloutConfigVO::new);

    private Date samplingDate;

    /**
     * @return 重新計算抽樣日期(體檢、電訪)
     */
    public Date getSamplingDate() {
        return samplingDate;
    }

    /**
     * @param samplingDate 重新計算抽樣日期(體檢、電訪)
     */
    public void setSamplingDate(Date samplingDate) {
        this.samplingDate = samplingDate;
    }

    public List<CalloutConfigVO> getGridList() {
        return gridList;
    }

    public void setGridList(List<CalloutConfigVO> gridList) {
        this.gridList = gridList;
    }

    public List<CalloutConfigVO> getGridList1() {
        return gridList1;
    }

    public void setGridList1(List<CalloutConfigVO> gridList1) {
        this.gridList1 = gridList1;
    }

    public List<CalloutConfigVO> getGridList2() {
        return gridList2;
    }

    public void setGridList2(List<CalloutConfigVO> gridList2) {
        this.gridList2 = gridList2;
    }

    public CalloutConfigVO getCalloutConfigVO() {
        if (calloutConfigVO == null) {
            calloutConfigVO = new CalloutConfigVO();
        }
        return calloutConfigVO;
    }

    public void setCalloutConfigVO(CalloutConfigVO calloutConfigVO) {
        this.calloutConfigVO = calloutConfigVO;
    }

    public CalloutConfigVO getCalloutConfigVO1() {
        if (calloutConfigVO1 == null) {
            calloutConfigVO1 = new CalloutConfigVO();
        }
        return calloutConfigVO1;
    }

    public void setCalloutConfigVO1(CalloutConfigVO calloutConfigVO1) {
        this.calloutConfigVO1 = calloutConfigVO1;
    }

    public CalloutConfigVO getCalloutConfigVO2() {
        if (calloutConfigVO2 == null) {
            calloutConfigVO2 = new CalloutConfigVO();
        }
        return calloutConfigVO2;
    }

    public void setCalloutConfigVO2(CalloutConfigVO calloutConfigVO2) {
        this.calloutConfigVO2 = calloutConfigVO2;
    }

}
