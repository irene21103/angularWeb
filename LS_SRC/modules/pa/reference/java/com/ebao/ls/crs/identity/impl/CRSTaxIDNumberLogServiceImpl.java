package com.ebao.ls.crs.identity.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.ebao.foundation.module.db.DBean;
import com.ebao.ls.crs.data.bo.CRSTaxIDNumberLog;
import com.ebao.ls.crs.data.identity.CRSTaxIDNumberLogDAO;
import com.ebao.ls.crs.identity.CRSTaxIDNumberLogService;
import com.ebao.ls.crs.vo.CRSTaxIDNumberLogVO;
import com.ebao.ls.foundation.service.GenericServiceImpl;
import com.ebao.pub.framework.RTException;

public class CRSTaxIDNumberLogServiceImpl extends GenericServiceImpl<CRSTaxIDNumberLogVO, CRSTaxIDNumberLog, CRSTaxIDNumberLogDAO> 
				implements CRSTaxIDNumberLogService {
	
	static List<Order> orders = new ArrayList<Order>();
	static {
		orders.add(Order.asc("copy"));
		orders.add(Order.asc("createOrder"));
	}
	
	@Override
	protected CRSTaxIDNumberLogVO newEntityVO() {
		// TODO Auto-generated method stub
		return new CRSTaxIDNumberLogVO();
	}

	@Override
	public List<CRSTaxIDNumberLogVO> findByPartyLogIdAndBizId(Long partyLogId, String bizSource, Long bizId){
		List<CRSTaxIDNumberLog> taxLogList = this.dao.findByCriteriaWithOrder(orders,
						Restrictions.eq("crsPartyLog.logId", partyLogId),
						Restrictions.eq("bizId", bizId),
						Restrictions.eq("bizSource", bizSource));
		return convertToVOList(taxLogList);
	}
	
	@Override
	public List<CRSTaxIDNumberLogVO> findByPartyLogIdAndChangeId(Long partyLogId, String bizSource, Long changeId){
		List<CRSTaxIDNumberLog> taxLogList = this.dao.findByCriteriaWithOrder(orders,
						Restrictions.eq("crsPartyLog.logId", partyLogId),
						Restrictions.eq("changeId", changeId),
						Restrictions.eq("bizSource", bizSource));
		return convertToVOList(taxLogList);
	}

	@Override
	public List<CRSTaxIDNumberLogVO> findByPartyLogIdAndBizIdCopy(Long partyLogId, String bizSource, Long bizId, 
						Integer copy) {
		if(copy != null) {
		
			List<CRSTaxIDNumberLog> taxLogList = this.dao.findByCriteriaWithOrder(orders,
							Restrictions.eq("crsPartyLog.logId", partyLogId),
							Restrictions.eq("bizId", bizId),
							Restrictions.eq("bizSource", bizSource),
							Restrictions.eq("copy", copy.intValue()));
			return convertToVOList(taxLogList);	
		} 
		return new ArrayList<CRSTaxIDNumberLogVO>();
	}

	@Override
	public int getTaxMaxCreateOrder(Long partyLogId, Integer copy) {
		DBean db = null;
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer sql = new StringBuffer();
		try {
			if (1 == 0 || partyLogId == null) {
				throw new NullPointerException();
			}
			sql.append("select nvl(max(create_order),0) maxval from T_CRS_RES_TIN_LOG where CRS_LOG_ID = ? and copy = ? ");
			db = new DBean();
			db.connect();
			con = db.getConnection();
			ps = con.prepareStatement(sql.toString());
			int index = 1;
			ps.setLong(index++, partyLogId);
			ps.setInt(index++, copy);
			rs = ps.executeQuery();
			while (rs.next()) {
				int max = rs.getInt("maxval");
				return max + 1;
			}
		} catch (SQLException e) {
			throw new RTException(e);
		} catch (ClassNotFoundException e) {
			throw new RTException(e);
		} finally {
			DBean.closeAll(rs, ps, db);
		}
		return 1;
	}

	@Override
	public List<CRSTaxIDNumberLogVO> findByLogIdAndBizSource(Long partyLogId, String bizSource1, String bizSource2) {

		List<CRSTaxIDNumberLog> taxLogList = this.dao.findByCriteriaWithOrder(orders,
				Restrictions.eq("crsPartyLog.logId", partyLogId),
				Restrictions.or(Restrictions.eq("bizSource", bizSource1), Restrictions.eq("bizSource", bizSource2)));
		return convertToVOList(taxLogList);
	}
    
}
