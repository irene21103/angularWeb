package com.ebao.ls.uw.ds;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.collections4.CollectionUtils;
import org.hibernate.criterion.Restrictions;

import com.ebao.ls.foundation.service.GenericServiceImpl;
import com.ebao.ls.pa.pub.model.CoverageAgentInfo;
import com.ebao.ls.pa.pub.model.PolicyInfo;
import com.ebao.ls.pa.pub.service.PolicyService;
import com.ebao.ls.pty.ci.DeptCI;
import com.ebao.ls.pty.ds.HospitalService;
import com.ebao.ls.pty.vo.HospitalVO;
import com.ebao.ls.pub.CodeCst;
import com.ebao.ls.sc.service.agent.AgentScService;
import com.ebao.ls.uw.ctrl.info.SalesmanQualityRecordAction;
import com.ebao.ls.uw.data.TCsQmQueryDelegate;
import com.ebao.ls.uw.data.TUwQmQueryDelegate;
import com.ebao.ls.uw.data.bo.CSQualityManageList;
import com.ebao.ls.uw.data.bo.QualityManageList;
import com.ebao.ls.uw.ds.constant.Cst;
import com.ebao.ls.uw.vo.QualityManageListVO;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.util.BeanUtils;
import com.ebao.pub.web.pager.Pager;


public class UwQmQueryDSImpl extends GenericServiceImpl<QualityManageListVO, QualityManageList, TUwQmQueryDelegate> 
		implements UwQmQueryService{
    
	@Resource(name = TCsQmQueryDelegate.BEAN_DEFAULT)
	private TCsQmQueryDelegate tCsQmQueryDelegate;
	
	@Resource(name = DeptCI.BEAN_DEFAULT)
	private DeptCI deptCI;
	
    @Resource(name = TUwQmQueryDelegate.BEAN_DEFAULT)
    public void setDao(TUwQmQueryDelegate dao) {
        super.setDao(dao);
    }
    
    @Resource(name = HospitalService.BEAN_DEFAULT)
    private HospitalService hospitalService;
    
	@Resource(name = PolicyService.BEAN_DEFAULT)
	protected PolicyService policyService;
	
    @Override
    protected QualityManageListVO newEntityVO() {
        return new QualityManageListVO();
    }

    @Override
    public List<QualityManageListVO> pagerQuery(String qualityManageType, 
    											Pager pager,
    											String name,
    											String certiCode,
    											String registerCode) {
    	List<QualityManageList> qualityManageList = super.getDao().queryByCriteria(qualityManageType, pager, name, certiCode, registerCode);
    	List<CSQualityManageList> cSQualityManageList = tCsQmQueryDelegate.queryByCriteria(qualityManageType, pager, name, certiCode, registerCode);
    	
    	this.filterQualityManageListByModule(qualityManageList, cSQualityManageList);
    	
		return convertToVOList(qualityManageList);
    }
    

    @Override
    public boolean delete(Long listId, String deleteReason) {
        return super.getDao().deleteByCriteria(listId, deleteReason);
    }
    
    @Override
    public QualityManageListVO load(Long listId) {
    	QualityManageList bo = super.getDao().load(listId);
    	if(bo == null) {
    		CSQualityManageList csBo = tCsQmQueryDelegate.load(listId);
    		if(csBo != null) {
    			QualityManageListVO vo = new QualityManageListVO();
    			csBo.copyToVO(vo, false);
    			return vo;
    		}
    	} 
        return convertToVO(bo);
    }

    @Override
    public List<QualityManageListVO> checkByCertiCode(Set<String> certiCode) {
        List<QualityManageList> qualityManageList = super.getDao().queryByCertiCodeMainly(certiCode);
        List<CSQualityManageList> cSQualityManageList = tCsQmQueryDelegate.queryByCertiCodeMainly(certiCode);
        
        this.filterQualityManageListByModule(qualityManageList, cSQualityManageList);
        
        return convertToVOList(qualityManageList);
    }

    @Override
	public List<QualityManageListVO> checkByCertiCode(Set<String> certiCode, String qualityManageType, String source) {
        List<CSQualityManageList> boList = tCsQmQueryDelegate.queryByCertiCodeMainly(certiCode, qualityManageType, source);
        
        List<QualityManageListVO> result = new ArrayList<QualityManageListVO>();
		result = (List<QualityManageListVO>) BeanUtils.copyCollection(QualityManageListVO.class,
						boList);
        return result;
	}

    @Override
    @SuppressWarnings("unchecked")
	public List<QualityManageListVO> checkPosByCertiCode(Set<String> certiCode) {
        List<CSQualityManageList> boList = tCsQmQueryDelegate.queryByCertiCodeMainly(certiCode);
        
        List<QualityManageListVO> result = new ArrayList<QualityManageListVO>();
		result = (List<QualityManageListVO>) BeanUtils.copyCollection(QualityManageListVO.class,
						boList);
        return result;
	}
	
	@Override
	public List<QualityManageListVO> findByCertiCodeAndDate(Set<String> certiCode, Date date, String qmType) {

		if (CollectionUtils.isEmpty(certiCode)) return convertToVOList(null);    
		
		List<QualityManageList> qualityManageList = super.getDao().findByCertiCodeAndDate(certiCode, null, qmType);
		List<QualityManageList> qualityManageListApplyDate = super.getDao().findByCertiCodeAndDate(certiCode, date, qmType);

		List<CSQualityManageList> cSQualityManageList = tCsQmQueryDelegate.findByCertiCodeAndDate(certiCode, null, qmType);
		List<CSQualityManageList> cSQualityManageListApplyDate = tCsQmQueryDelegate.findByCertiCodeAndDate(certiCode, date, qmType);

		List<Long> tempListId = new ArrayList<Long>();
		List<Long> tempCSListId = new ArrayList<Long>();

		if( !qualityManageList.isEmpty() ) { 
			for(int i=0; i<qualityManageList.size(); i++ ) {
				tempListId.add(qualityManageList.get(i).getListId());
			}
		}		
		if( !qualityManageListApplyDate.isEmpty() ) {  
			for(int j=0; j<qualityManageListApplyDate.size(); j++ ) {
				Long listId = qualityManageListApplyDate.get(j).getListId();
				if(!tempListId.contains(listId)) {
					tempListId.add(listId);
					this.filterQualityManageListByModuleApplyDate(qualityManageList, qualityManageListApplyDate, listId);										
				}
			}
		}

		if( !cSQualityManageList.isEmpty() ) {
			for(int j=0; j<cSQualityManageList.size(); j++ ) {
				Long listId = cSQualityManageList.get(j).getListId();
				if(!tempCSListId.contains(listId)) {
					tempCSListId.add(listId);
					this.filterCSQualityManageListByModuleApplyDate(qualityManageList, cSQualityManageList, listId);										
				}
			}
		}
		
		if( !cSQualityManageListApplyDate.isEmpty() ) { 	
			for(int j=0; j<cSQualityManageListApplyDate.size(); j++ ) {
				Long listId = cSQualityManageListApplyDate.get(j).getListId();
				if(!tempCSListId.contains(listId)) {
					tempCSListId.add(listId);
					this.filterCSQualityManageListByModuleApplyDate(qualityManageList, cSQualityManageListApplyDate, listId);										
				}
			}
		}
		
		return convertToVOList(qualityManageList);
	}
	
	@Override
	public List<QualityManageListVO> findByRegisterCodeAndDate(Set<String> registerCode, Date date) {

		List<QualityManageList> qualityManageList = super.getDao().findByRegisterCodeAndDate(registerCode, null);
		List<QualityManageList> qualityManageListApplyDate = super.getDao().findByRegisterCodeAndDate(registerCode, date);

		List<CSQualityManageList> cSQualityManageList = tCsQmQueryDelegate.findByRegisterCodeAndDate(registerCode, null);
		List<CSQualityManageList> cSQualityManageListApplyDate = tCsQmQueryDelegate.findByRegisterCodeAndDate(registerCode, date);

		List<Long> tempListId = new ArrayList<Long>();
		List<Long> tempCSListId = new ArrayList<Long>();

		if( !qualityManageList.isEmpty() ) { 
			for(int i=0; i<qualityManageList.size(); i++ ) {
				tempListId.add(qualityManageList.get(i).getListId());
			}
		}		

		if( !qualityManageListApplyDate.isEmpty() ) {
			for(int j=0; j<qualityManageListApplyDate.size(); j++ ) {
				Long listId = qualityManageListApplyDate.get(j).getListId();
				if(!tempListId.contains(listId)) {
					tempListId.add(listId);
					this.filterQualityManageListByModuleApplyDate(qualityManageList, qualityManageListApplyDate, listId);										
				}
			}
		}

		if( !cSQualityManageList.isEmpty() ) {
			for(int j=0; j<cSQualityManageList.size(); j++ ) {
				Long listId = cSQualityManageList.get(j).getListId();
				if(!tempCSListId.contains(listId)) {
					tempCSListId.add(listId);
					this.filterCSQualityManageListByModuleApplyDate(qualityManageList, cSQualityManageList, listId);										
				}
			}
		}
		
		if( !cSQualityManageListApplyDate.isEmpty() ) { 	
			for(int j=0; j<cSQualityManageListApplyDate.size(); j++ ) {
				Long listId = cSQualityManageListApplyDate.get(j).getListId();
				if(!tempCSListId.contains(listId)) {
					tempCSListId.add(listId);
					this.filterCSQualityManageListByModuleApplyDate(qualityManageList, cSQualityManageListApplyDate, listId);										
				}
			}
		}
	
		return convertToVOList(qualityManageList);
	}
	
	@Override
	public List<QualityManageListVO> checkByRegisterCode(Set<String> registerCode) {
		List<QualityManageList> qualityManageList = super.getDao().queryByRegisterCodeMainly(registerCode);
		List<CSQualityManageList> cSQualityManageList = tCsQmQueryDelegate.queryByRegisterCodeMainly(registerCode);
		
		this.filterQualityManageListByModule(qualityManageList, cSQualityManageList);
		
		return convertToVOList(qualityManageList);
	}

	@Override
    public List<QualityManageListVO> checkByRegisterCode(Set<String> registerCode, String qualityManageType, String source) {
        List<CSQualityManageList> boList = tCsQmQueryDelegate.queryByRegisterCodeMainly(registerCode, qualityManageType, source);
        
        List<QualityManageListVO> result = new ArrayList<QualityManageListVO>();
		result = (List<QualityManageListVO>) BeanUtils.copyCollection(QualityManageListVO.class,
						boList);
        return result;
    }

	@Override
	@SuppressWarnings("unchecked")
    public List<QualityManageListVO> checkPosByRegisterCode(Set<String> registerCode) {
        List<CSQualityManageList> boList = tCsQmQueryDelegate.queryByRegisterCodeMainly(registerCode);
        
        List<QualityManageListVO> result = new ArrayList<QualityManageListVO>();
		result = (List<QualityManageListVO>) BeanUtils.copyCollection(QualityManageListVO.class,
						boList);
        return result;
    }

    @Override
	public QualityManageListVO checkByRegisterCodeAndDate(String registerCode, Date date) {
		QualityManageList bo = super.getDao().queryByRegisterCodeAndDate(registerCode, date);
		if ( bo != null) {
			return convertToVO(bo, false);
		}
		return null;
	}
    public List<QualityManageListVO> queryByHospitalId(Long hospitalId) {
        Date date = AppContext.getCurrentUserLocalTime();
        HospitalVO hospital= hospitalService.getHospitalById(hospitalId);
        return super.find(Restrictions.eq("certiCode",  hospital.getHospitalCode())
                        , Restrictions.eq("qualityManageType", Cst.QUALITY_MANAGER_TYPE_HOSPITAL)
                        , Restrictions.eq("certiType", CodeCst.CERTI_TYPE__OTHERS)
                        , Restrictions.le("startDate", date)
                        , Restrictions.ge("endDate", date)
                        , Restrictions.eq("isDelete", CodeCst.YES_NO__NO));
    }
    
    public List<QualityManageListVO> queryByDoctorName(Long hospitalId,String ...names) {
        Date date = AppContext.getCurrentUserLocalTime();
        HospitalVO hospital= hospitalService.getHospitalById(hospitalId);
        return super.find(
                        Restrictions.eq("devCode",  hospital.getHospitalCode())
                        , Restrictions.in("name",  names)
                        , Restrictions.eq("qualityManageType", Cst.QUALITY_MANAGER_TYPE_DOCTOR)
                        , Restrictions.le("startDate", date)
                        , Restrictions.ge("endDate", date)
                        , Restrictions.eq("isDelete", CodeCst.YES_NO__NO));
    }

    private QualityManageList copyList(CSQualityManageList b){
    	QualityManageList a = new QualityManageList();
		a.setBirthDate(b.getBirthDate());
		a.setCause(b.getCause());
		a.setCauseDesc(b.getCauseDesc());
		a.setCertiCode(b.getCertiCode());
		a.setCertiType(b.getCertiType());
		a.setDeleteReason(b.getDeleteReason());
		a.setDevCode(b.getDevCode());
		a.setDevName(b.getDevName());
		a.setEndDate(b.getEndDate());
		a.setInsertedBy(b.getInsertedBy());
		a.setInsertTime(b.getInsertTime());
		a.setInsertTimestamp(b.getInsertTimestamp());
		a.setIsDelete(b.getIsDelete());
		a.setListId(b.getListId());
		a.setMeno(b.getMeno());
		a.setName(b.getName());
		a.setQualityManageType(b.getQualityManageType());
		a.setRegisterCode(b.getRegisterCode());
		a.setSource(b.getSource());
		a.setSourceDescs(b.getSourceDescs());
		a.setSourceListId(b.getSourceListId());
		a.setStartDate(b.getStartDate());
		a.setUpdatedBy(b.getUpdatedBy());
		a.setUpdateTime(b.getUpdateTime());
		a.setUpdateTimestamp(b.getUpdateTimestamp());
		a.setVersion(b.getVersion());
    	return a;
    }

    @Override
    public List<QualityManageListVO> queryByCertiCodeUseTUwQm(Set<String> certiCode, String qualityManageType, String source) {
        return convertToVOList(super.getDao().queryByCertiCodeMainly(certiCode, qualityManageType, source));
    }

    @Override
    public List<QualityManageListVO> queryByCertiCodeUseTCsQm(Set<String> certiCode, String qualityManageType, String source) {
        List<CSQualityManageList> boList = tCsQmQueryDelegate.queryByCertiCodeMainly(certiCode, qualityManageType, source);
        return (List<QualityManageListVO>) BeanUtils.copyCollection(QualityManageListVO.class, boList);
    }
    
    /**
     * <p>Description : IR_328538、PCR_433803、IR_452221，新契約排除 SOURCE_LIST_ID != NULL</p>
     * @param qualityManageList
     * @param cSQualityManageList
     */
    private void filterQualityManageListByModule(List<QualityManageList> qualityManageList, List<CSQualityManageList> cSQualityManageList) {
    	boolean isUnb = this.isUnb();
    	
    	for (CSQualityManageList cSQualityManage : cSQualityManageList) {
			if (isUnb && cSQualityManage.getSourceListId()!=null) {//新契約人員進入查詢畫面不顯示POS複製的業務員品質控管名單；sourceListId不為空，表示POS複製的業務員品質控管名單
				continue;
			}
			
			qualityManageList.add(this.copyList(cSQualityManage));
		}
    }

    private void filterQualityManageListByModuleApplyDate(List<QualityManageList> qualityManageList, List<QualityManageList> qualityManageListApplyDate, Long listId) {
    	boolean isUnb = this.isUnb();
    	
    	for (QualityManageList qualityManage : qualityManageListApplyDate) {
			if (isUnb && qualityManage.getSourceListId()!=null ) {//新契約人員進入查詢畫面不顯示POS複製的業務員品質控管名單；sourceListId不為空，表示POS複製的業務員品質控管名單
				continue;
			}
			if(qualityManage.getListId().equals(listId)) {
				qualityManageList.add(this.copyList(qualityManage));
			}
		}
    }
    
    private void filterCSQualityManageListByModuleApplyDate(List<QualityManageList> qualityManageList, List<CSQualityManageList> cSQualityManageListApplyDate, Long listId) {
    	boolean isUnb = this.isUnb();
    	
    	for (CSQualityManageList qualityManage : cSQualityManageListApplyDate) {
			if (isUnb && qualityManage.getSourceListId()!=null ) {//新契約人員進入查詢畫面不顯示POS複製的業務員品質控管名單；sourceListId不為空，表示POS複製的業務員品質控管名單
				continue;
			}
			if(qualityManage.getListId().equals(listId)) {
				qualityManageList.add(this.copyList(qualityManage));
			}
		}
    }

    private QualityManageList copyList(QualityManageList b) {
    	QualityManageList a = new QualityManageList();
		a.setBirthDate(b.getBirthDate());
		a.setCause(b.getCause());
		a.setCauseDesc(b.getCauseDesc());
		a.setCertiCode(b.getCertiCode());
		a.setCertiType(b.getCertiType());
		a.setDeleteReason(b.getDeleteReason());
		a.setDevCode(b.getDevCode());
		a.setDevName(b.getDevName());
		a.setEndDate(b.getEndDate());
		a.setInsertedBy(b.getInsertedBy());
		a.setInsertTime(b.getInsertTime());
		a.setInsertTimestamp(b.getInsertTimestamp());
		a.setIsDelete(b.getIsDelete());
		a.setListId(b.getListId());
		a.setMeno(b.getMeno());
		a.setName(b.getName());
		a.setQualityManageType(b.getQualityManageType());
		a.setRegisterCode(b.getRegisterCode());
		a.setSource(b.getSource());
		a.setSourceDescs(b.getSourceDescs());
		a.setSourceListId(b.getSourceListId());
		a.setStartDate(b.getStartDate());
		a.setUpdatedBy(b.getUpdatedBy());
		a.setUpdateTime(b.getUpdateTime());
		a.setUpdateTimestamp(b.getUpdateTimestamp());
		a.setVersion(b.getVersion());
    	return a;
    }

	/**
     * <p>Description : IR_328538、PCR_433803、IR_452221，新契約排除 SOURCE_LIST_ID != NULL</p>
     */
    public boolean isUnb() {//TODO 待T_USER_BIZ_CATEGORY表上線後，評估將T_DEPT_BIZ_CATEGORY來源判斷替換成T_USER_BIZ_CATEGORY
    	boolean isUnb = false;
    	Long userId = AppContext.getCurrentUser().getUserId();
    	Integer bizCategory = deptCI.getBizCategoryByUserId(userId);
    	
    	if (bizCategory != null) {
    		isUnb = bizCategory.compareTo(CodeCst.BIZ_CATEGORY_UNB)==0;
    	} else {
    		List<Long> whiteList = new ArrayList<Long>();
    		whiteList.add(100629L);//ECUSER
    		whiteList.add(100636L);//MPOSUSER
    		
    		isUnb = whiteList.contains(userId);
    	}
    	
    	return isUnb;
    }
    
	/**
	 * <pre>
	 * 2022.12.02 IrisLiu, PCR 506547, 檢查指定日期內，保單的任一位招攬業務員是否在品質控管名單
	 * </pre>
	 */
	@Override
	public boolean checkAgentQualityByPolicyIdAndDate(Long policyId, Set<Date> dateSet) {
		//取招攬業務員的ID、登錄證號
		PolicyInfo policyInfo = policyService.load(policyId);
		List<CoverageAgentInfo> coverageAgentInfoList = policyInfo.getMasterCoverage().getCoverageAgents();
		Set<String> agentCertiCodeSet = new HashSet<String>();
		Set<String> registerCodeSet = new HashSet<String>();
		for (CoverageAgentInfo coverageAgentInfo : coverageAgentInfoList) {
			String agentCertiCode = SalesmanQualityRecordAction.findAgentCertiCodeById(coverageAgentInfo.getRegisterCode());
			agentCertiCodeSet.add(agentCertiCode);
			registerCodeSet.add(coverageAgentInfo.getRegisterCode());
		}
		
		//by業務員ID的查詢結果
		List<QualityManageList> qualityManageListByCertiCode = null; //非POS
		List<CSQualityManageList> csQualityManageByCertiCode = null; //POS
		//by業務員登錄證號的查詢結果
		List<QualityManageList> qualityManageListByRegisterCode = null; //非POS
		List<CSQualityManageList> csQualityManageListByRegisterCode = null; //POS
		
		//by日期逐一檢查，是否落在任一招攬業務員的品質控管期間
		for (Date baseDate : dateSet) {
			
			if (baseDate == null) {
				continue;
			}
			
			//by業務員ID檢查
			if (agentCertiCodeSet != null && !agentCertiCodeSet.isEmpty()) {
				qualityManageListByCertiCode = super.getDao().findByCertiCodeAndDate(agentCertiCodeSet, baseDate, Cst.QUALITY_MANAGER_TYPE_AGENT);
				csQualityManageByCertiCode = tCsQmQueryDelegate.findByCertiCodeAndDate(agentCertiCodeSet, baseDate, Cst.QUALITY_MANAGER_TYPE_AGENT);
				for (CSQualityManageList cSQualityManage : csQualityManageByCertiCode) {
					this.filterCSQualityManageListByModuleApplyDate(qualityManageListByCertiCode, csQualityManageByCertiCode, cSQualityManage.getListId());
				}
				
				if (qualityManageListByCertiCode != null && !qualityManageListByCertiCode.isEmpty()) {
					return true;
				}
			}
			
			//by業務員登錄證字號檢查
			if (registerCodeSet != null && !registerCodeSet.isEmpty()) {
				qualityManageListByRegisterCode = super.getDao().findByRegisterCodeAndDate(registerCodeSet, baseDate);
				csQualityManageListByRegisterCode = tCsQmQueryDelegate.findByRegisterCodeAndDate(registerCodeSet, baseDate);
				for (CSQualityManageList cSQualityManage : csQualityManageListByRegisterCode) {
					this.filterCSQualityManageListByModuleApplyDate(qualityManageListByRegisterCode, csQualityManageListByRegisterCode, cSQualityManage.getListId());
				}
				
				if (qualityManageListByRegisterCode != null && !qualityManageListByRegisterCode.isEmpty()) {
					return true;
				}
			}
		}
		
		return false;
	}
	
	@Override
	public List<QualityManageListVO> getEffectiveQualityManages(String qualityManageType, String certiCode,
			String registerCode, String source, Date checkDate) {
		List<QualityManageList> lists = getDao().getEffectiveQualityManages(qualityManageType, certiCode, registerCode, source, checkDate);
		
		// BO to VO
		List<QualityManageListVO> result = lists.stream()
				.map(bo -> 
					{
						QualityManageListVO vo = new QualityManageListVO();
						bo.copyToVO(vo, false);
						return vo;
					})
				.collect(Collectors.toList());
		
		return result;
	}

}
