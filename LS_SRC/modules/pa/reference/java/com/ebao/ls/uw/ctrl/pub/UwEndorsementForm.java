package com.ebao.ls.uw.ctrl.pub;

import com.ebao.pub.framework.GenericForm;

/**
 * 
 * <p>Title:Underwriting</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: eBaoTech Corporation</p>
 * @author mingchun.shi
 * @since  Jun 27, 2005
 * @version 1.0
 */
public class UwEndorsementForm extends GenericForm {

  private java.lang.Long underwriteId = null;

  private java.lang.String endoCode = "";

  private java.lang.Long policyId = null;

  private java.lang.Long endorsementId = null;

  private java.lang.Long underwriterId = null;

  private java.lang.String descLang1 = "";

  private java.lang.String descLang2 = "";

  private java.lang.Long uwListId = null;

  private String effectDate;

  private String expiryDate;

  private String duplicateInd;

  private String autoInd;

  public String getAutoInd() {
    return autoInd;
  }

  public void setAutoInd(String autoInd) {
    this.autoInd = autoInd;
  }

  public String getDuplicateInd() {
    return duplicateInd;
  }

  public void setDuplicateInd(String duplicateInd) {
    this.duplicateInd = duplicateInd;
  }

  /**
   * returns the value of the underwriteId  
   * @return the underwriteId
   */
  public java.lang.Long getUnderwriteId() {
    return underwriteId;
  }

  /**
   * sets the value of the underwriteId  
   * @param underwriteId the underwriteId
   */
  public void setUnderwriteId(java.lang.Long underwriteId) {
    this.underwriteId = underwriteId;
  }

  /**
   * returns the value of the endoCode  
   * @return the endoCode
   */
  public java.lang.String getEndoCode() {
    return endoCode;
  }

  /**
   * sets the value of the endoCode  
   * @param endoCode the endoCode
   */
  public void setEndoCode(java.lang.String endoCode) {
    this.endoCode = endoCode;
  }

  /**
   * returns the value of the policyId  
   * @return the policyId
   */
  public java.lang.Long getPolicyId() {
    return policyId;
  }

  /**
   * sets the value of the policyId   
   * @param policyId the policyId
   */
  public void setPolicyId(java.lang.Long policyId) {
    this.policyId = policyId;
  }

  /**
   * returns the value of the endorsementId  
   * @return the endorsementId
   */
  public java.lang.Long getEndorsementId() {
    return endorsementId;
  }

  /**
   * sets the value of the endorsementId  
   * @param endorsementId the endorsementId
   */
  public void setEndorsementId(java.lang.Long endorsementId) {
    this.endorsementId = endorsementId;
  }

  /**
   * returns the value of the underwriterId  
   * @return the underwriterId
   */
  public java.lang.Long getUnderwriterId() {
    return underwriterId;
  }

  /**
   * sets the value of the underwriterId  
   * @param underwriterId the underwriterId
   */
  public void setUnderwriterId(java.lang.Long underwriterId) {
    this.underwriterId = underwriterId;
  }

  /**
   * returns the value of the descLang1  
   * @return the descLang1
   */
  public java.lang.String getDescLang1() {
    return descLang1;
  }

  /**
   * sets the value of the descLang1  
   * @param descLang1 the descLang1
   */
  public void setDescLang1(java.lang.String descLang1) {
    this.descLang1 = descLang1;
  }

  /**
   * returns the value of the descLang2   
   * @return the descLang2
   */
  public java.lang.String getDescLang2() {
    return descLang2;
  }

  /**
   * sets the value of the descLang2 
   * @param descLang2 the descLang2
   */
  public void setDescLang2(java.lang.String descLang2) {
    this.descLang2 = descLang2;
  }

  /**
   * returns the value of the uwListId 
   * @return the uwListId
   */
  public java.lang.Long getUwListId() {
    return uwListId;
  }

  /**
   * sets the value of the uwListId  
   * @param uwListId the uwListId
   */
  public void setUwListId(java.lang.Long uwListId) {
    this.uwListId = uwListId;
  }

  /**
   * @return Returns the effectDate.
   */
  public String getEffectDate() {
    return effectDate;
  }

  /**
   * @param effectDate The effectDate to set.
   */
  public void setEffectDate(String effectDate) {
    this.effectDate = effectDate;
  }

  /**
   * @return Returns the expiryDate.
   */
  public String getExpiryDate() {
    return expiryDate;
  }

  /**
   * @param expiryDate The expiryDate to set.
   */
  public void setExpiryDate(String expiryDate) {
    this.expiryDate = expiryDate;
  }
}
