package com.ebao.ls.uw.ctrl.info;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.ls.pa.nb.ctrl.detailreg.DetailRegHelper;
import com.ebao.ls.pa.pub.bs.coverage.CoverageService;
import com.ebao.ls.pa.pub.vo.CoverageVO;
import com.ebao.ls.product.service.LifeProductService;
import com.ebao.pub.framework.GenericAction;

public class PolicyHolderInfoSearchAction extends GenericAction {

    private String forward = "display";
    
    @Resource(name = QualityRecordActionHelper.BEAN_DEFAULT)
    private QualityRecordActionHelper qualityRecordActionHelper;
    
    @Resource(name = DetailRegHelper.BEAN_DEFAULT)
    private DetailRegHelper detailRegHelper;
    
    @Resource(name = CoverageService.BEAN_DEFAULT)
    private CoverageService coverageService;
    
    @Resource(name = LifeProductService.BEAN_DEFAULT)
    private LifeProductService lifeProductService;
    
    @Override
    public ActionForward process(ActionMapping mapping, ActionForm form,
                    HttpServletRequest request, HttpServletResponse response)
                    throws Exception {
        
        String policyId = request.getParameter("policyId").toString(); 
        
        CoverageVO coverageVO = coverageService.findMasterCoverageByPolicyId(Long.parseLong(policyId));
        boolean isCalcByAmount = lifeProductService.isCustomizedPremCalcByAmount(Long.valueOf(coverageVO.getProductId().longValue()));
        
        //Develop 331706，coverageList裡含有「CUSTOMIZED_PREM:約定/自訂保費資料」
        List<Map<String, Object>> coverageList = detailRegHelper.getCoveragesWithoutWaivers(policyId, request);
        
        request.setAttribute("coverageList", coverageList);
        //用來給前端判斷是否為顯示「定期超額保險費」
        request.setAttribute("isCalcByAmount", isCalcByAmount);
        return mapping.findForward(forward);
    }

}
