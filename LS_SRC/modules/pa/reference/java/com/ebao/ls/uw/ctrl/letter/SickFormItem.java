package com.ebao.ls.uw.ctrl.letter;

public class SickFormItem {

	private String code;

	private String codeValue;
	
	private String remarkIndi;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getCodeValue() {
		return codeValue;
	}

	public void setCodeValue(String codeValue) {
		this.codeValue = codeValue;
	}

	public String getRemarkIndi() {
		return remarkIndi;
	}

	public void setRemarkIndi(String remarkIndi) {
		this.remarkIndi = remarkIndi;
	}
	
}
