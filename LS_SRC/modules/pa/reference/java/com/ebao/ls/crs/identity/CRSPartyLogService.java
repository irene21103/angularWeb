package com.ebao.ls.crs.identity;

import java.util.List;

import com.ebao.foundation.commons.exceptions.GenericException;
import com.ebao.ls.crs.data.bo.CRSPartyLog;
import com.ebao.ls.crs.vo.CRSPartyLogVO;
import com.ebao.ls.foundation.service.GenericService;
import com.ebao.pub.framework.MultiWarning;
import java.util.Date;

public interface CRSPartyLogService extends GenericService<CRSPartyLogVO> {
	public final static String BEAN_DEFAULT = "crsPartyLogService";

	public abstract java.util.List<CRSPartyLog> queryPartyLog(String policyType, String certiCode, String name, String policyCode, String crsType);

	public abstract CRSPartyLogVO findByChangeIdAndCertiCode(Long changeId,String certiCode);

	public abstract CRSPartyLogVO findByCrsIdAndPolicyChangeId(Long crsId, Long policyChangeId);
	
	public abstract List<CRSPartyLogVO> findByChangeIdAndPolicyChangeId(Long changeId, Long policyChangeId);
	// end
	public abstract CRSPartyLogVO findCRSPartyLog(Long changeId, Long policyId, Long partyId, String roleType) throws GenericException;

	public abstract List<CRSPartyLogVO> findListByCertiCode(String certiCode, Long changeId);
	
	public abstract List<CRSPartyLogVO> findListByCertiCodeAndChangeIdAndPolicyChangeId(String certiCode, Long changeId, Long policyChangeId);

	/**
	 * 直接將該筆Log檔Record，下變更狀態。
	 * @param logId	[crsPartyLogId]
	 */
	public abstract void markRemove(Long logId);

	/**
	 * 直接將該筆Log檔Record，下變更狀態。
	 * @param logId	[crsPartyLogId]
	 */
	public abstract void markRevoke(Long logId);
	
	public abstract java.util.List<CRSPartyLogVO> parseUIRows(List<CRSPartyLog> list, Long userId) throws GenericException;

	public abstract String getSYSCode(Long userId);

	public abstract CRSPartyLogVO initCRSPartyLog(Long policyId, Long partyId, String roleType) throws GenericException;

	/**
	 * 
	 * @param handerId 維護人員
	 * @param processDate 維護時間
	 * @param logVO 聲明書
	 * @param changeId 變更
	 * @param inputSource 來源類型 LINK, BAT, CMN
	 * @throws GenericException
	 */
	public abstract void saveOrUpdate(Long handerId, java.util.Date processDate, CRSPartyLogVO logVO, Long changeId, String inputSource) throws GenericException;

	/**
	 * <p>Description : 新契約覆核記錄LOG，複製一筆完整</p>
	 * <p>Created By : Simon.Huang</p>
	 * <p>Create Time : 2018年12月10日</p>
	 * @param changeId
	 * @param newChangeId
	 * @return
	 * @throws Exception
	 */
	public CRSPartyLogVO copyUnbLog(Long changeId, Long newChangeId) throws Exception;

	/**
	 * 複製指定來源的身分證明到目的(For CRS/Fatca複製身份證明用)
	 * @param crsPartyLogVO_from
	 * @param crsPartyLogVO_to
	 */
	public void copyCRSPartyLog(CRSPartyLogVO crsPartyLogVO_from, CRSPartyLogVO crsPartyLogVO_to);
	
	/**
	 * <p>Description : 新契約頁面刪除</p>
	 * <p>Created By : Simon.Huang</p>
	 * <p>Create Time : 2018年12月11日</p>
	 * @param logId
	 */
	public void removeUnbAll(Long partyLogId);
	
	/**
	 * <p>Description : 新契約頁面法人時刪除個人資料,個人時刪除法人資料</p>
	 * <p>Created By : Simon.Huang</p>
	 * <p>Create Time : 2018年12月11日</p>
	 * @param logId
	 */
	public void removeUnbOther(Long partyLogId);
	
	public abstract List<CRSPartyLogVO> findCRSPartyLog(String certiCode, String policyCode, String sysCode);
	
	public abstract Boolean isExistsCRSDocument(Long crsLogId);

	public abstract Boolean isExistsCRSPartyLog(Long changeId);
	
	public void removePosLog(Long changeId, Long policyChgId);
	
	public void removePosLog(Long changeId, Long policyChgId, String certiCode);
	/**
	 * PCR 257607 330030 e-Approval ITR-1801051_CRS共同申報及盡職審查專案- 保全變更及CRS確認申報國作業
	 * @param crsId
	 * @param changeId
	 * @return
	 */
	public List<CRSPartyLogVO> findByCrsIdAndChangeId(Long crsId, Long changeId);
	/**
	 * PCR 257607 330030 e-Approval ITR-1801051_CRS共同申報及盡職審查專案- 保全變更及CRS確認申報國作業
	 * 保全受理多保全項，取得排序最大的 log 
	 * @param crsId
	 * @param changeId
	 * @return
	 */
	public abstract CRSPartyLogVO findLastLogByCrsIdAndChangeId(Long crsId, Long changeId);
	/**
	 * <p>Description : POS 保全變更，複製一筆完整log by new policyChangeId</p>
	 * <p>Created By : Lyn Kuo</p>
	 * <p>Create Time : 2019年10月10日</p>
	 * @param changeId
	 * @param newChangeId
	 * @param crsId
	 * @param certiCode //IR422658 更換要保人新增的CRS資料尚未產生CrsId/以certiCode取得Log資料
	 * @return CRSPartyLogVO
	 * @throws Exception
	 */	
	public CRSPartyLogVO copyPosLog(Long newChangeId, Long newPolicyChgId,  Long crsId, String certiCode) throws Exception;
	
	/**
	 * CRS PHASE II - [330030][Lyn]e-Approval ITR-1801051_CRS共同申報及盡職審查專案- 保全變更及CRS確認申報國作業 
	 * CRS提交資料檢核 crs共用頁面/pos變更crs身份共用 
	 * 檢核訊息: 提示身分聲明對應的未齊全資料，檢核CRS身份類別和檢附文件是否檢附齊全，未齊全要顯示[警告]訊息
	 * @param logVO
	 * @return
	 */
	public String validateDocsByCrsType(CRSPartyLogVO logVO) ;

	/**
	 * PCR[350318] DEV[376275][Ian] e-Approval ITR-1903876_CRS 作業調整與通報需求(phase II) -保全變更CRS身份及保全輸入檢核</BR>
	 * 檢核當次建立CRS記錄的聲明人姓名與ID未與保單任一關係人(包含要保人、被保人、受益人、法定代理人)相同時 ，須顯示錯誤訊息
	 * */
	public MultiWarning validateCrsDeclarant(CRSPartyLogVO logVO) ;
        
    /**
     * 依CHANGE_ID分組並取得日期區間內同一個證號的最新T_CRS_PARTY_LOG<br>
     * 集合ORDER BY DECLARATION_DATE DESC
     *
     * @param date_s 起始日期
     * @param date_e 結束日期
     * @param certiCode 證號
     * @return 符合條件的CRSPartyLogVO集合
     */
    public List<CRSPartyLogVO> findListByDeclarationDateRangeAndCertiCode(Date date_s, Date date_e, String certiCode);

    /**
     * 依CHANGE_ID分組並取得同一個證號的最新T_CRS_PARTY_LOG<br>
     * 集合ORDER BY DECLARATION_DATE DESC
     *
     * @param certiCode 證號
     * @return 符合條件的CRSPartyLogVO集合
     */
    public List<CRSPartyLogVO> findListByCertiCode(String certiCode);

    /**
     * 用PolicyCode取得T_CRS_PARTY_LOG.LAST_CMT_FLG = 'Y'的集合<br>
     *
     * @param policyCode 保單號碼
     * @return 符合條件的CRSPartyLogVO集合
     */
    public List<CRSPartyLogVO> findListByPolicyCodeOnlyLast(String policyCode);

}
