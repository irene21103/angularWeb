package com.ebao.ls.uw.ctrl.lifeSurvey;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.ebao.ls.pa.pub.vo.InsuredVO;
import com.ebao.ls.pub.CodeCst;
import com.ebao.ls.sc.vo.AgentChlVO;
import com.ebao.ls.uw.ctrl.letter.UNBLetterForm;
import com.ebao.ls.uw.vo.UwLifeSurveyItemVO;

public class LifeSurveyForm extends UNBLetterForm {

	/**
	 * <code>serialVersionUID</code> 的註解
	 */
	private static final long serialVersionUID = 1202986722981718885L;

	// 檢核 3.5 業務規則:「高齡投保件(核保高齡關懷提問)」(Y / N)
	private String isMeetElderCare = CodeCst.YES_NO__NO;

	private Long listId;

	public Long getListId() {
		return listId;
	}

	public void setListId(Long listId) {
		this.listId = listId;
	}

	private Long underwriteId;
	
	private String subAction;

	private List<InsuredVO> insuredVOs;

	private List<UwLifeSurveyItemVO> lifeSurveyItems;
	
	private List<AgentChlVO> agents;
	
	private String retMsg;
	
	private Boolean submitSuccess = new Boolean(false);

	private Integer[] lifeSurveyitem;
	
	private String agent1Name;
	
	private String agent2Name;
	
	private String agent3Name;
	
	private String agent1Number;
	
	private String agent2Number;
	
	private String agent3Number;
	
	private String policyNo;
	
	private long templateId;

	/** 來源為保全核保 */
	private boolean isCsUw;
	/* 保全變更申請ID */
	private Long changeId;
	
	private List<Map<String, Object>> insuredMaps;
	private String phAddress;
	private String phZipCode;
	// 生調類別
	private String surveyType;
	
	private Long surveyAddressId;
	
	private String surveyZipCode;
	
	// 交查事項補充說明
	private String surveyComment;
	private Long policyId;
	
	// 內勤生調交查員
	private String[] applyStaffList;

	// 生調地址
	private String realSurveyAddress;

	// 生調郵遞區號
	private String realSurveyZipCode;

	// 交查對象
	private String selectedObjectCertiCodes;

	public String getIsMeetElderCare() {
		return isMeetElderCare;
	}

	public void setIsMeetElderCare(String isMeetElderCare) {
		this.isMeetElderCare = isMeetElderCare;
	}

	public String getRealSurveyAddress() {
		return realSurveyAddress;
	}

	public void setRealSurveyAddress(String realSurveyAddress) {
		this.realSurveyAddress = realSurveyAddress;
	}
	
	public String getRealSurveyZipCode() {
		return realSurveyZipCode;
	}

	public void setRealSurveyZipCode(String realSurveyZipCode) {
		this.realSurveyZipCode = realSurveyZipCode;
	}
	
	public List<Map<String, Object>> getInsuredMaps() {
		return insuredMaps;
	}

	public void setInsuredMaps(List<Map<String, Object>> insuredMaps) {
		this.insuredMaps = insuredMaps;
	}
	
	public String getPhAddress() {
		return phAddress;
	}
	public void setPhAddress(String phAddress) {
		this.phAddress = phAddress;
	}

	public String getPhZipCode() {
		return phZipCode;
	}
	public void setPhZipCode(String phZipCode) {
		this.phZipCode = phZipCode;
	}
	
	public Long getSurveyAddressId() {
		return surveyAddressId;
	}

	public void setSurveyAddressId(Long surveyAddressId) {
		this.surveyAddressId = surveyAddressId;
	}
	
	public String getSurveyZipCode() {
		return surveyZipCode;
	}

	public void setSurveyZipCode(String surveyZipCode) {
		this.surveyZipCode = surveyZipCode;
	}
	
	public Long getPolicyId() {
		return policyId;
	}

	public void setPolicyId(Long policyId) {
		this.policyId = policyId;
	}

	public String[] getApplyStaffList() {
		return applyStaffList;
	}

	public void setApplyStaffList(String[] applyStaffList) {
		this.applyStaffList = applyStaffList;
	}
	
	public String getSurveyComment() {
		return surveyComment;
	}

	public void setSurveyComment(String surveyComment) {
		this.surveyComment = surveyComment;
	}

	public String getSurveyType() {
		return surveyType;
	}

	public void setSurveyType(String surveyType) {
		this.surveyType = surveyType;
	}

	public Long getUnderwriteId() {
		return underwriteId;
	}

	public void setUnderwriteId(Long underwriteId) {
		this.underwriteId = underwriteId;
	}

	public String getRetMsg() {
		return retMsg;
	}

	public void setRetMsg(String retMsg) {
		this.retMsg = retMsg;
	}

	public List<InsuredVO> getInsuredVOs() {
		return insuredVOs;
	}

	public void setInsuredVOs(List<InsuredVO> insuredVOs) {
		this.insuredVOs = insuredVOs;
	}

	public List<UwLifeSurveyItemVO> getLifeSurveyItems() {
		return lifeSurveyItems;
	}

	public void setLifeSurveyItems(List<UwLifeSurveyItemVO> lifeSurveyItems) {
		this.lifeSurveyItems = lifeSurveyItems;
	}

	public List<AgentChlVO> getAgents() {
		return agents;
	}

	public void setAgents(List<AgentChlVO> agents) {
		this.agents = agents;
	}

	public String getSubAction() {
		return subAction;
	}

	public void setSubAction(String subAction) {
		this.subAction = subAction;
	}

	public Integer[] getLifeSurveyitem() {
		return lifeSurveyitem;
	}

	public void setLifeSurveyitem(Integer[] lifeSurveyitem) {
		this.lifeSurveyitem = lifeSurveyitem;
	}

	public String getAgent1Name() {
		return agent1Name;
	}

	public void setAgent1Name(String agent1Name) {
		this.agent1Name = agent1Name;
	}

	public String getAgent2Name() {
		return agent2Name;
	}

	public void setAgent2Name(String agent2Name) {
		this.agent2Name = agent2Name;
	}

	public String getAgent3Name() {
		return agent3Name;
	}

	public void setAgent3Name(String agent3Name) {
		this.agent3Name = agent3Name;
	}

	public String getAgent1Number() {
		return agent1Number;
	}

	public void setAgent1Number(String agent1Number) {
		this.agent1Number = agent1Number;
	}

	public String getAgent2Number() {
		return agent2Number;
	}

	public void setAgent2Number(String agent2Number) {
		this.agent2Number = agent2Number;
	}

	public String getAgent3Number() {
		return agent3Number;
	}

	public void setAgent3Number(String agent3Number) {
		this.agent3Number = agent3Number;
	}

	public String getSelectedObjectCertiCodes() {
		return selectedObjectCertiCodes;
	}

	public void setSelectedObjectCertiCodes(String selectedObjectCertiCodes) {
		this.selectedObjectCertiCodes = selectedObjectCertiCodes;
	}

	public Boolean getSubmitSuccess() {
		return submitSuccess;
	}

	public void setSubmitSuccess(Boolean submitSuccess) {
		this.submitSuccess = submitSuccess;
	}

	public String getPolicyNo() {
		return policyNo;
	}

	public void setPolicyNo(String policyNo) {
		this.policyNo = policyNo;
	}

	public long getTemplateId() {
		return templateId;
	}

	public void setTemplateId(long templateId) {
		this.templateId = templateId;
	}
	
	/**
	 * <p>Description :來源為保全核保 </p>
	 * <p>Created By : TGL144</p>
	 * <p>Create Time : Nov 29, 2016</p>
	 * @return
	 */
	public boolean isCsUw() {
		return isCsUw;
	}
	/**
	 * <p>Description :來源為保全核保 </p>
	 * <p>Created By : TGL144</p>
	 * <p>Create Time : Nov 29, 2016</p>
	 * @param isCsUw
	 */
	public void setCsUw(boolean isCsUw) {
		this.isCsUw = isCsUw;
	}
	/**
	 * <p>Description :保全變更申請ID </p>
	 * <p>Created By : TGL144</p>
	 * <p>Create Time : Sep 10, 2016</p>
	 * @return
	 */
	public Long getChangeId() {
		return changeId;
	}
	/**
	 * <p>Description :保全變更申請ID </p>
	 * <p>Created By : TGL144</p>
	 * <p>Create Time : Sep 10, 2016</p>
	 * @param changeId
	 */
	public void setChangeId(Long changeId) {
		this.changeId = changeId;
	}

	public String toString() {
		String properties = ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
		properties += ToStringBuilder.reflectionToString(this.getInsuredMaps(), ToStringStyle.MULTI_LINE_STYLE);
		properties += ToStringBuilder.reflectionToString(this.getAgents(), ToStringStyle.MULTI_LINE_STYLE);
		return properties;
	}	
}
