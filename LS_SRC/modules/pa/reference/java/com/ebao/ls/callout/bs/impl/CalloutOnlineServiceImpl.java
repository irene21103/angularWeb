package com.ebao.ls.callout.bs.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;

import com.ebao.foundation.commons.exceptions.GenericException;
import com.ebao.foundation.core.logging.Log;
import com.ebao.foundation.module.util.i18n.CodeTable;
import com.ebao.ls.callout.batch.CalloutConstants;
import com.ebao.ls.callout.bs.CalloutOnlineService;
import com.ebao.ls.callout.vo.CalloutTransVO;
import com.ebao.ls.chl.service.AgentService;
import com.ebao.ls.chl.service.ChannelOrgService;
import com.ebao.ls.chl.vo.ScAgentDataVO;
import com.ebao.ls.chl.vo.ScServiceAgentDataVO;
import com.ebao.ls.pa.nb.bs.rule.PolicyServiceHelper;
import com.ebao.ls.pa.pub.bs.AgentNotifyPremSourceService;
import com.ebao.ls.pa.pub.bs.AgentNotifyPurposeService;
import com.ebao.ls.pa.pub.bs.FinanceNotifyPremSourceService;
import com.ebao.ls.pa.pub.bs.NbAutoWithdrawService;
import com.ebao.ls.pa.pub.bs.PolicyService;
import com.ebao.ls.pa.pub.data.ProductEndorsementDao;
import com.ebao.ls.pa.pub.endorsement.vo.ClauseInfoVO;
import com.ebao.ls.pa.pub.ref.api.prd.ProductService;
import com.ebao.ls.pa.pub.ref.vo.prd.ProductVO;
import com.ebao.ls.pa.pub.vo.AgentNotifyPremSourceVO;
import com.ebao.ls.pa.pub.vo.AgentNotifyPurposeVO;
import com.ebao.ls.pa.pub.vo.CoverageVO;
import com.ebao.ls.pa.pub.vo.FinanceNotifyPremSrcVO;
import com.ebao.ls.pa.pub.vo.NbAutoWithdrawVO;
import com.ebao.ls.pa.pub.vo.PolicyProposalInfoVO;
import com.ebao.ls.pa.rule.RuleRateTableCST;
import com.ebao.ls.pa.rule.data.RuleRateTableDao;
import com.ebao.ls.prd.product.vo.ProductVersionVO;
import com.ebao.ls.product.service.LifeProductService;
import com.ebao.ls.product.service.ProductVersionService;
import com.ebao.ls.pty.ds.EmployeeService;
import com.ebao.ls.pub.CodeCst;
import com.ebao.ls.pub.batch.impl.DataSourceWrapper;
import com.ebao.ls.pub.cst.CalloutTargetType;
import com.ebao.ls.pub.cst.YesNo;
import com.ebao.ls.ws.vo.ecp.WsEbaoCalloutOnlineRqVO;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.security.AppUser;
import com.ebao.pub.util.json.DateUtils;
import com.ebao.pub.wincony.ui.CodeTableUtils;
import com.hazelcast.util.StringUtil;

public class CalloutOnlineServiceImpl implements CalloutOnlineService {

    private final Log log = Log.getLogger(CalloutOnlineServiceImpl.class);

    @Resource(name = PolicyService.BEAN_DEFAULT)
    private PolicyService policyService;

    @Resource(name = ProductService.BEAN_DEFAULT)
    private ProductService prdServ;
    
    @Resource(name = ChannelOrgService.BEAN_DEFAULT)
    private ChannelOrgService channelService;

    @Resource(name=AgentService.BEAN_DEFAULT)
    private AgentService agentService;
    
	@Resource(name = LifeProductService.BEAN_DEFAULT)
	private LifeProductService lifeProductService;
	
	@Resource(name = RuleRateTableDao.BEAN_DEFAULT)
	private RuleRateTableDao ruleRateTableDao;
	
	@Resource(name = PolicyServiceHelper.BEAN_DEFAULT)
	private PolicyServiceHelper rmsPolicyServiceHelper;
	
	@Resource(name = ProductVersionService.BEAN_DEFAULT)
	private ProductVersionService productVersionService;
	
	@Resource(name = ProductEndorsementDao.BEAN_DEFAULT)
	private ProductEndorsementDao productEndorsementDao;
	
	@Resource(name = NbAutoWithdrawService.BEAN_DEFAULT)
	private NbAutoWithdrawService nbAutoWithdrawService;
	
    private void syso(Object msg) {
        log.info("[SYSOUT][BR-CMN-50353-1]" + msg);
    }
    
    private static final String GENDER = "GENDER";
    private static final String BIRTH_DATE = "BIRTH_DATE";
    
    public WsEbaoCalloutOnlineRqVO prepareVO(CalloutTransVO vo) throws GenericException {
        com.ebao.ls.pa.pub.vo.PolicyVO policyVO = policyService.load(new Long(vo.getPolicyId()));
         
        Long masterItemId = policyVO.gitMasterCoverage().getItemId();

        WsEbaoCalloutOnlineRqVO rq = new WsEbaoCalloutOnlineRqVO();
        java.util.Map<String, Object> map = new java.util.TreeMap<String, Object>();
        // PCR355648 整合要保人資訊
        Map<String, Object> holderInfo = getPolicyHolderInfo(vo.getPolicyId());
        Map<String, Object> insuredInfo = getMainInsuredInfo(vo.getPolicyId());
        
        Map<String, Object> masterInfo = getMainInfo(vo.getPolicyId());
        Map<String, Object> prodInfo = getProdInfo(vo.getPolicyId());
        Map<String, Object> channelInfo = getChannelInfo(vo.getPolicyId());
        List<Map<String, Object>> agentInfo = getAgentInfo(vo.getPolicyId(), masterItemId);
        List<Map<String, Object>> benefitInfo = getBenefitInfo(vo.getPolicyId());
        String langId = CodeCst.LANGUAGE__CHINESE_TRAD;
        rq.setCalloutNum(vo.getCalloutNum());
        map.put("CalloutNum", rq.getCalloutNum());
        rq.setHolderName(vo.getHolderName());
        map.put("HolderName", vo.getHolderName());
        rq.setHolderGender(MapUtils.getString(holderInfo, GENDER,""));
        map.put("HolderGender", rq.getHolderGender());
        // PCR355468 傳到ECP時，電訪對象只有要、被保人、法代，需特別轉換。
        rq.setCalloutTarget(CodeTable.getCodeDesc("T_CALLOUT_TARGET_TYPE_ECP", vo.getCalloutTargetType(), langId));
        map.put("CalloutTarget", rq.getCalloutTarget());
        rq.setCalleeCertiCode(vo.getCalleeCertiCode());
        map.put("CalleeCertiCode", rq.getCalleeCertiCode());
        rq.setCalleeName(vo.getCalleeName());
        map.put("CalleeName", rq.getCalleeName());

        if (CalloutConstants.CalloutTargetTypeholder.equals(vo.getCalloutTargetType())) {
            rq.setCalleeGender(rq.getHolderGender());
        // PCR355468 電訪對象被保人拆多項，性別從頁面選擇撈取資料時帶過來
        //} else if (CalloutConstants.CalloutTargetTypeInsuer.equals(vo.getCalloutTargetType())) {
        //    rq.setCalleeGender(MapUtils.getString(insuredInfo, "GENDER", ""));
        } else if (CalloutTargetType.LEGAL_REPRESENTATIVE.equals(vo.getCalloutTargetType())) {
            String certiCode = rq.getCalleeCertiCode();
            if (StringUtils.isNotEmpty(certiCode)) {
                String genderType = certiCode.substring(1, 2);
                if ("1".equals(genderType)) {
                    rq.setCalleeGender("M");
                } else if ("2".equals(genderType)) {
                    rq.setCalleeGender("F");
                }
            }
        } else {
        	rq.setCalleeGender(MapUtils.getString(getCalloutInsuredInfo(vo.getPolicyId(),vo.getCalleeCertiCode()), GENDER, ""));
        }
        	
        map.put("CalleeGender", rq.getCalleeGender());
        rq.setCalleeBirth(com.ebao.pub.util.json.DateUtils.convertToMinguoDate((vo.getCalleeBirthday())));
        map.put("CalleeBirth", rq.getCalleeBirth());
        rq.setContactNum1(getContactNumber(vo.getOfficeTelReg(), vo.getOfficeTel(), vo.getOfficeTelExt()));
        rq.setContactNum2(getContactNumber(vo.getHomeTelReg(), vo.getHomeTel(), vo.getHomeTelExt()));
        rq.setContactNum3(StringUtils.defaultString(vo.getContactNum1(), ""));// 要被保人手機電話
        rq.setContactNum4(StringUtils.defaultString(vo.getContactNum2(), ""));

        map.put("ContactNum1", rq.getContactNum1());
        map.put("ContactNum2", rq.getContactNum2());
        map.put("ContactNum3", rq.getContactNum3());
        map.put("ContactNum4", rq.getContactNum4());

        rq.setHolderTel(StringUtils.defaultString(vo.getContactNum3()));
        map.put("HolderTel", rq.getHolderTel());
        rq.setInsuredTel(StringUtils.defaultString(vo.getContactNum4()));
        map.put("InsuredTel", rq.getInsuredTel());
        rq.setLegalAgentTel(StringUtils.defaultString(vo.getContactNum5()));
        map.put("LegalAgentTel", rq.getLegalAgentTel());
        Integer relationToPh = MapUtils.getIntValue(insuredInfo, "RELATION_TO_PH", -1);
        rq.setHolderRSinsured("2");
        if (CodeCst.RELATION_TO_PH_SELF == relationToPh) {
            rq.setHolderRSinsured("1");
        }
        map.put("HolderRSinsured", rq.getHolderRSinsured());
        if (CalloutConstants.CalloutTargetTypelower.equals(vo.getCalloutTargetType())) {
            rq.setLegalAgentCertiCode(vo.getCalleeCertiCode());
            rq.setLegalAgentName(vo.getCalleeName());
            rq.setLegalAgentBirth(DateUtils.convertTWDate(vo.getCalleeBirthday()));
            rq.setLegalAgentRSinsured(vo.getRelation2());
            rq.setLegalAgentRSholder(vo.getRelation1());
        }
        map.put("LegalAgentCertiCode", rq.getLegalAgentCertiCode());
        map.put("LegalAgentName", rq.getLegalAgentName());
        map.put("LegalAgentBirth", rq.getLegalAgentBirth());
        map.put("LegalAgentRSinsured", rq.getLegalAgentRSinsured());
        map.put("LegalAgentRSholder", rq.getLegalAgentRSholder());
        AppUser user = AppContext.getCurrentUser();

        // 契約主表資料取得
        rq.setPolicyCode(MapUtils.getString(masterInfo, "POLICY_CODE", ""));
        map.put("PolicyCode", rq.getPolicyCode());
        String liabilityState = MapUtils.getString(masterInfo, "LIABILITY_STATE", "");
        rq.setLiabilityStatus(CalloutConstants.LIABILITY_STATUS__INFORCE);
        if (String.valueOf(CodeCst.LIABILITY_STATUS__NON_VALIDATE).equals(liabilityState)) {
            rq.setLiabilityStatus(CalloutConstants.LIABILITY_STATUS__NON_VALIDATE);
        }
        map.put("LiabilityStatus", rq.getLiabilityStatus());
        rq.setIssueDate(DateUtils.convertTWDate(policyVO.getApplyDate()));
        map.put("IssueDate", rq.getIssueDate());
        if (vo.getAcknowledgeDate() != null) {
            rq.setAcknowledgeDate(DateUtils.convertTWDate(vo.getAcknowledgeDate()));
        }
        map.put("AcknowledgeDate", rq.getAcknowledgeDate());
        String moneyId = MapUtils.getString(masterInfo, "MONEY_ID", "");
        if (StringUtils.isNotEmpty(moneyId)) {
            rq.setCurrency(CodeTable.getCodeDesc("T_MONEY", moneyId, langId));
        }
        map.put("Currency", rq.getCurrency());
        // 商品主表資料取得
        rq.setRenewalType(MapUtils.getString(prodInfo, "CHARGE_NAME", ""));
        // #222731 每期保費->首期保費
        PolicyProposalInfoVO proposalVO = policyVO.getProposalInfo();
        String installPrem = proposalVO.getInstallPrem() == null ? "0" : String.format("%,d", proposalVO.getInstallPrem().intValue());
        rq.setEachPrem(installPrem);
        // #222,567 自訂保費
        BigDecimal customizedPrem = new BigDecimal(MapUtils.getLongValue(prodInfo, "CUSTOMIZED_PREM", 0L));
        String customizedPremText = customizedPrem == null ? "0" : String.format("%,d", customizedPrem.intValue());
        rq.setCustomizePrem(customizedPremText);
        rq.setBizParent(MapUtils.getString(prodInfo, "BIG_PRODCATE", ""));
        rq.setBizCate(MapUtils.getString(prodInfo, "SMALL_PRODCATE", ""));
        String amount = MapUtils.getString(prodInfo, "AMOUNT", "");
        String unitFlag = MapUtils.getString(prodInfo, "UNIT_FLAG", "");
        List<String> amountFlag = Arrays.asList(new String[] { "0", "6", "7", "9" });
        if (StringUtils.isNotEmpty(unitFlag) && CollectionUtils.isNotEmpty(policyVO.getCoverages())) {
            List<CoverageVO> coverageList = policyVO.getCoverages();
            CoverageVO coverageVO = null; 
            for(CoverageVO itemVO : coverageList){
                if(itemVO.getMasterId() == null){
                    coverageVO = itemVO;
                    break;
                }
            }
            if ("1".equals(unitFlag)) {
                // 按單位計算
                Integer unit = coverageVO.getCurrentPremium().getUnit().intValue();
                try {
                    amount = String.format("%,d %s", unit, CodeTableUtils.getDesc("V_CALLOUT_UNIT_FLAG", "1"));
                } catch (Exception e) {
                    log.info("[ERROR][GET_UNIT]" + ExceptionUtils.getFullStackTrace(e));
                    amount = String.valueOf(unit);
                }
            } else if ("4".equals(unitFlag)) {
                // 按計劃
                String benefitLevel = coverageVO.getCurrentPremium().getBenefitLevel();
                try {
                    amount = String.format("%s %s", CodeTableUtils.getDesc("V_CALLOUT_UNIT_FLAG", "4"), benefitLevel);
                } catch (Exception e) {
                    log.info("[ERROR][GET_BENEFIT]" + ExceptionUtils.getFullStackTrace(e));
                    amount = benefitLevel;
                }
            } else if (amountFlag.contains(unitFlag)) {
                // 按保額計算sumAssured
                Integer amountVal = 0;
                if(coverageVO.getCurrentPremium() != null && coverageVO.getCurrentPremium().getSumAssured() != null){
                    amountVal = coverageVO.getCurrentPremium().getSumAssured().intValue();
                }
                try {
                    amount = String.format("%,d %s", amountVal, CodeTableUtils.getDesc("V_CALLOUT_UNIT_FLAG", "6"));
                } catch (Exception e) {
                    log.info("[ERROR][GET_AMOUNT]" + ExceptionUtils.getFullStackTrace(e));
                    amount = String.valueOf(amountVal);
                }
            }
        }
        rq.setUnitAmount(amount);
        rq.setCategoryCode(MapUtils.getString(prodInfo, "INTERNAL_ID", ""));
        rq.setCategoryDesc(MapUtils.getString(prodInfo, "PRODUCT_NAME", ""));
        rq.setAnnualCharge(new BigDecimal(MapUtils.getLongValue(prodInfo, "CHARGE_YEAR", 0L)));
        map.put("RenewalType", rq.getRenewalType());
        map.put("EachPrem", rq.getEachPrem());
        map.put("CustomizePrem", rq.getCustomizePrem());
        map.put("BizParent", rq.getBizParent());
        map.put("UnitAmount", rq.getUnitAmount());
        map.put("CategoryCode", rq.getCategoryCode());
        map.put("CategoryDesc", rq.getCategoryDesc());
        map.put("AnnualCharge", rq.getAnnualCharge());

        // 通路資料取得
        rq.setChannelType(MapUtils.getString(channelInfo, "SALES_CHANNEL_NAME", ""));
        map.put("ChannelType", rq.getChannelType());
        rq.setSalesChannelName(MapUtils.getString(channelInfo, "CHANNEL_NAME", ""));
        map.put("SalesChannelName", rq.getSalesChannelName());
        if (agentInfo != null && agentInfo.size() > 0) {
            rq.setAgent1Name(MapUtils.getString(agentInfo.get(0), "AGENT_NAME", ""));
            rq.setAgent1Gender(MapUtils.getString(agentInfo.get(0), GENDER, ""));
            map.put("Agent1Name", rq.getAgent1Name());
            map.put("Agent1Gender", rq.getAgent1Gender());
        }
        if (agentInfo != null && agentInfo.size() > 1) {
            rq.setAgent2Name(MapUtils.getString(agentInfo.get(1), "AGENT_NAME", ""));
            rq.setAgent2Gender(MapUtils.getString(agentInfo.get(1), GENDER, ""));
            map.put("Agent2Name", rq.getAgent2Name());
            map.put("Agent2Gender", rq.getAgent2Gender());
        }
        if (agentInfo != null && agentInfo.size() > 2) {
            rq.setAgent3Name(MapUtils.getString(agentInfo.get(2), "AGENT_NAME", ""));
            rq.setAgent3Gender(MapUtils.getString(agentInfo.get(2), GENDER, ""));
            map.put("Agent3Name", rq.getAgent3Name());
            map.put("Agent3Gender", rq.getAgent3Gender());
        }
        // 受益人資料取得 : 身故受益人或年金身故受益人
        //ObjectFactory of = new ObjectFactory();
        for (int i = 0; i < 5; i++) {
            boolean hasData = i < benefitInfo.size();
            String name = "";
            String relation = "";
            Integer order = 0;
            //BigDecimal shareRate = null;  PCR355468 均分給0
            BigDecimal shareRate = BigDecimal.ZERO;
            if (!hasData) {
                break;
            }
            Map<String, Object> benefitMap = benefitInfo.get(i);
            name = StringUtils.trimToEmpty(MapUtils.getString(benefitMap, "NAME", ""));
            // #222696
            String designation = MapUtils.getString(benefitMap, "DESIGNATION", "");
            relation = CodeTable.getCodeDesc("T_BENE_DESIGNATION", designation, langId);
            order = MapUtils.getInteger(benefitMap, "SHARE_ORDER", 0);
            String avgIndi = MapUtils.getString(benefitMap, "AVG_INDI");
            if (!YesNo.YES_NO__YES.equals(avgIndi)) {
                shareRate = new BigDecimal(MapUtils.getFloat(benefitMap, "SHARE_RATE"));
            }
            if (i == 0) {
                rq.setBenefit1Name(name);
                rq.setCalleeRSbenefit1(relation);
                //rq.setBenefit1Order(of.createWsEbaoCalloutOnlineRqVoBenefit1Order(order));
                rq.setBenefit1Order(order);
                rq.setBenefit1ShareRate(shareRate);
                map.put("Benefit1Name", rq.getBenefit1Name());
                map.put("CalleeRSbenefit1", rq.getCalleeRSbenefit1());
                map.put("Benefit1Order", order);
                map.put("Benefit1ShareRate", rq.getBenefit1ShareRate());
            } else if (i == 1) {
                rq.setBenefit2Name(name);
                rq.setCalleeRSbenefit2(relation);
                //rq.setBenefit2Order(of.createWsEbaoCalloutOnlineRqVoBenefit2Order(order));
                rq.setBenefit2Order(order);
                rq.setBenefit2ShareRate(shareRate);
                map.put("Benefit2Name", rq.getBenefit2Name());
                map.put("CalleeRSbenefit2", rq.getCalleeRSbenefit2());
                map.put("Benefit2Order", order);
                map.put("Benefit2ShareRate", rq.getBenefit2ShareRate());
            } else if (i == 2) {
                rq.setBenefit3Name(name);
                rq.setCalleeRSbenefit3(relation);
                //rq.setBenefit3Order(of.createWsEbaoCalloutOnlineRqVoBenefit3Order(order));
                rq.setBenefit3Order(order);
                rq.setBenefit3ShareRate(shareRate);
                map.put("Benefit3Name", rq.getBenefit3Name());
                map.put("CalleeRSbenefit3", rq.getCalleeRSbenefit3());
                map.put("Benefit3Order", order);
                map.put("Benefit3ShareRate", rq.getBenefit3ShareRate());
            } else if (i == 3) {
                rq.setBenefit4Name(name);
                rq.setCalleeRSbenefit4(relation);
                //rq.setBenefit4Order(of.createWsEbaoCalloutOnlineRqVoBenefit4Order(order));
                rq.setBenefit4Order(order);
                rq.setBenefit4ShareRate(shareRate);
                map.put("Benefit4Name", rq.getBenefit4Name());
                map.put("CalleeRSbenefit4", rq.getCalleeRSbenefit4());
                map.put("Benefit4Order", order);
                map.put("Benefit4ShareRate", rq.getBenefit4ShareRate());
            } else if (i == 4) {
                rq.setBenefit5Name(name);
                rq.setCalleeRSbenefit5(relation);
                //rq.setBenefit5Order(of.createWsEbaoCalloutOnlineRqVoBenefit5Order(order));
                rq.setBenefit5Order(order);
                rq.setBenefit5ShareRate(shareRate);
                map.put("Benefit5Name", rq.getBenefit5Name());
                map.put("CalleeRSbenefit5", rq.getCalleeRSbenefit5());
                map.put("Benefit5Order", order);
                map.put("Benefit5ShareRate", rq.getBenefit5ShareRate());
            }
        }

        Integer benefitCount = getBenefitCount(vo.getPolicyId());
        //rq.setBenefitNum(of.createWsEbaoCalloutOnlineRqVoBenefitNum(benefitCount));
        rq.setBenefitNum(benefitCount);
        map.put("BenefitNum", benefitCount);
        rq.setInitiatorAgent(user.getRealName());
        map.put("InitiatorAgent", rq.getInitiatorAgent());
        rq.setInitiatorUnit("UNB");
        map.put("InitiatorUnit", rq.getInitiatorUnit());
        rq.setEmergencyIndi(vo.getEmergencyIndi());
        map.put("EmergencyIndi", rq.getEmergencyIndi());
        if (vo.getExpectedCalloutDate() != null) {
        	//rq.setExpectedCalloutDateStart(DateUtils.getRocDate(vo.getExpectedCalloutDate(), "yyy/MM/dd HH:mm"));
        	rq.setExpectedCalloutDateStart(convertToMinguoDate(vo.getExpectedCalloutDate(), "yyyy/MM/dd HH:mm"));
        }
        // PCR355468 改成 ExpectedCalloutDateStart
        map.put("ExpectedCalloutDateStart", rq.getExpectedCalloutDateStart());
        // 綁件保單 : T_Callout_Trans.TiedPolicy
        rq.setParentPolicyCode(vo.getTiedPolicy());
        map.put("ParentPolicyCode", rq.getParentPolicyCode());
        rq.setDocketNum(vo.getCalloutDocketNum());
        map.put("DocketNum", rq.getDocketNum());
        rq.setMemo(StringUtils.defaultString(vo.getMemo()));
        map.put("Memo", rq.getMemo());
        //rq.setTransCount(of.createWsEbaoCalloutOnlineRqVoTransCount(vo.getTransCount()));
        rq.setTransCount(vo.getTransCount());
        map.put("TransCount", vo.getTransCount());
        
        // PCR355468 新客服系統新增欄位，ExpectedCalloutTime改成多筆
        if(StringUtils.isNotEmpty(vo.getExpectedCalloutTime())) {
        	//EC與MPOS進件是用","分隔，功能進來是";"，統一將,轉成;
        	rq.setExpectedCalloutTime(getCodeTableString("T_NB_TEL_TIME",vo.getExpectedCalloutTime().replace(',', ';').split(";"),";"));
        }
        map.put("ExpectedCalloutTime", rq.getExpectedCalloutTime());
        rq.setApplyPurpose(getApplySource(vo.getPolicyId()));
        map.put("ApplyPurpose", rq.getApplyPurpose());
        rq.setPremSource(getPremSource(vo.getPolicyId()));
        map.put("PremSource", rq.getPremSource());
        // PCR355468 EC件目前還是單選，綜合查詢顯示與舊資料一致
        rq.setCalloutReason(com.ebao.pub.util.StringUtils.isNullOrEmpty(vo.getCalloutReason())?vo.getCalloutQuestionnaireStr():vo.getCalloutReason());
        map.put("CalloutReason", rq.getCalloutReason());
        //判斷若預約電訪時間(訖)=空白且預約電訪時間(起)≠空白, 此欄位=預約電訪時間(起)
        if(vo.getExpectedCalloutDateEnd() == null) {
        	rq.setExpectedCalloutDateEnd(rq.getExpectedCalloutDateStart());
        }else {
        	//rq.setExpectedCalloutDateEnd(DateUtils.getRocDate(vo.getExpectedCalloutDateEnd(), "yyy/MM/dd HH:mm"));
        	rq.setExpectedCalloutDateEnd(convertToMinguoDate(vo.getExpectedCalloutDateEnd(), "yyyy/MM/dd HH:mm"));
        }
        map.put("ExpectedCalloutDateEnd", rq.getExpectedCalloutDateEnd());
        rq.setExpectedCalloutDateOther(vo.getExpectedCalloutTimeOther());
        map.put("ExpectedCalloutDateOther", rq.getExpectedCalloutDateOther());
        rq.setHolderCertiCode(vo.getHolderCertiCode());
        // 要保人資料
        map.put("HolderCertiCode", rq.getHolderCertiCode());
        if(MapUtils.getObject(holderInfo, BIRTH_DATE) != null) {
        	rq.setHolderBirthDate(DateUtils.convertTWDate((Date) MapUtils.getObject(holderInfo, BIRTH_DATE)));
        	map.put("HolderBirthDate", rq.getHolderBirthDate());
        }
        //rq.setContactNum1(getContactNumber(vo.getOfficeTelReg(), vo.getOfficeTel(), vo.getOfficeTelExt()));
		rq.setHolderHomeTel(getContactNumber(MapUtils.getString(holderInfo, "HOME_TEL_REG", ""),
				MapUtils.getString(holderInfo, "HOME_TEL", ""), MapUtils.getString(holderInfo, "HOME_TEL_EXT", "")));
		map.put("HolderHomeTel", rq.getHolderHomeTel());
		rq.setHolderOfficeTel(getContactNumber(MapUtils.getString(holderInfo, "OFFICE_TEL_REG", ""),
				MapUtils.getString(holderInfo, "OFFICE_TEL", ""), MapUtils.getString(holderInfo, "OFFICE_TEL_EXT", "")));
		map.put("HolderOfficeTel", rq.getHolderOfficeTel());
		rq.setHolderMobileTel(MapUtils.getString(holderInfo, "MOBILE_TEL",""));
		map.put("HolderMobileTel", rq.getHolderMobileTel());
		// 主被保人資料
		rq.setInsuredName(vo.getMainInsuredName());
		map.put("InsuredName", rq.getInsuredName());
		rq.setInsuredCertiCode(vo.getMainInsuredCertiCode());
		map.put("InsuredCertiCode", rq.getInsuredCertiCode());
		rq.setInsuredGender(MapUtils.getString(insuredInfo, GENDER,""));
		map.put("InsuredGender", rq.getInsuredGender());
		if (MapUtils.getObject(insuredInfo, BIRTH_DATE) != null) {
			rq.setInsuredBirthDate(DateUtils.convertTWDate((Date) MapUtils.getObject(insuredInfo, BIRTH_DATE)));
			map.put("InsuredBirthDate", rq.getInsuredBirthDate());
		}
		rq.setInsuredHomeTel(getContactNumber(MapUtils.getString(insuredInfo, "HOME_TEL_REG", ""),
				MapUtils.getString(insuredInfo, "HOME_TEL", ""), MapUtils.getString(insuredInfo, "HOME_TEL_EXT", "")));
		map.put("InsuredHomeTel", rq.getInsuredHomeTel());
		rq.setInsuredOfficeTel(getContactNumber(MapUtils.getString(insuredInfo, "OFFICE_TEL_REG", ""),
				MapUtils.getString(insuredInfo, "OFFICE_TEL", ""), MapUtils.getString(insuredInfo, "OFFICE_TEL_EXT", "")));
		map.put("InsuredOfficeTel", rq.getInsuredOfficeTel());
		rq.setInsuredMobileTel(MapUtils.getString(insuredInfo, "MOBILE_TEL",""));
		map.put("insuredMobileTel", rq.getInsuredMobileTel());
		//超額保費(VLT為定期超額保費)，主約險種=投資型商品, 值=自訂保險費-目標保險費; 其餘商品=0
		if ("投資型".equals(MapUtils.getString(prodInfo, "BIG_PRODCATE", ""))) {
			Long premVLT = (Math.max(MapUtils.getLongValue(prodInfo, "CUSTOMIZED_PREM", 0L) - MapUtils.getLongValue(prodInfo, "STD_PREM_AF", 0L),0L));
			rq.setPremVLT(new BigDecimal(premVLT));
		}else {
			rq.setPremVLT(BigDecimal.ZERO);
		}
		map.put("PremVLT", rq.getPremVLT());
		rq.setCalleeRomanName(vo.getCalleeRomanName());
		map.put("CalleeRomanName", rq.getCalleeName());
		List<CoverageVO> coverageVOList = policyVO.getCoverages();
        //商品不利因素
		List<String> elderCategoryCodelst = new ArrayList<String>();
		if(!CollectionUtils.isEmpty(coverageVOList)) {
			Map<String,String> rmsChannelInfo = rmsPolicyServiceHelper.getRMSChannelInfo(policyVO.getChannelType(), policyVO.getChannelOrgId());
			String saleChannelName = MapUtils.getString(rmsChannelInfo, RuleRateTableCST.FACTOR__SALE_CHANNEL_NAME);			
			for (CoverageVO coverageVO : coverageVOList) {
	        	long productVersionId = coverageVO.getProductVersionId().longValue();
		    	
	        	//PCR509114 修改須看批註設定，不看商品不利因素了
		    	ProductVersionVO productVersionVO = productVersionService.getProductVersion(coverageVO.getProductId().longValue(), productVersionId);
		    	//讀取RT後將結果回傳
		    	List<ClauseInfoVO> endorsementVOList = IterableUtils.toList(productEndorsementDao.queryClauseInfoList(coverageVO.getItemId()));
			    endorsementVOList.stream().forEach(v -> {
		    	String calloutElderCategoryCode = ruleRateTableDao.getRateTableElderCalloutCategoryCode(
		    			v.getPlanCode() , productVersionVO.getProductVersion(), saleChannelName);
		    	if(!StringUtils.isEmpty(calloutElderCategoryCode)) {
		    		elderCategoryCodelst.addAll(Arrays.asList(calloutElderCategoryCode.split(";")));
		    	}
			    });	
			    //PCR509114 因為自動提領批註承保後才抓的到，需特別處理
			    if (!String.valueOf(CodeCst.RI_LIAB_STATUS__IN_FORCE).equals(liabilityState)) {
			    	NbAutoWithdrawVO autoWithdrawVO = nbAutoWithdrawService.findByPolicyId(vo.getPolicyId());
					if (null != autoWithdrawVO && !StringUtil.isNullOrEmpty(autoWithdrawVO.getWithdrawVersion())
							&& !StringUtil.isNullOrEmpty(productVersionVO.getEndorsements())
							&& ( productVersionVO.getEndorsements().contains("0RD") || productVersionVO.getEndorsements().contains("0RE"))) {
						Arrays.asList(productVersionVO.getEndorsements().split(",")).stream().forEach(v -> {
						if(v.contains("0RD") || v.contains("0RE")){
							String calloutElderCategoryCode = ruleRateTableDao.getRateTableElderCalloutCategoryCode(
					    			v , productVersionVO.getProductVersion(), saleChannelName);
							if(!StringUtils.isEmpty(calloutElderCategoryCode)) {
					    		elderCategoryCodelst.addAll(Arrays.asList(calloutElderCategoryCode.split(";")));
					    	}
						}
						});
			    	}
			    }
	        }
		}
		if(!CollectionUtils.isEmpty(elderCategoryCodelst)) {
			rq.setElderlyProductKinds(elderCategoryCodelst.stream().distinct().collect(Collectors.joining(";")));
		}
		rq.setFinancialSource(getFinPremSource(vo.getPolicyId()));
		rq.setHolderAddr(MapUtils.getString(holderInfo, "address",""));
		rq.setInsuredAddr(MapUtils.getString(insuredInfo, "address",""));
		
        return rq;
    }
    // PCR477771 財告書保費來源
    protected String getFinPremSource(Long policyId) {
    	if(policyId == null ) {
    		return "";
    	}  	
    	List<FinanceNotifyPremSrcVO> finNotifyPremSrcList = financeNotifyPremSourceService.findByPolicyId(policyId);
    	if(!CollectionUtils.isEmpty(finNotifyPremSrcList)) {
    		String[] premSource = new String[finNotifyPremSrcList.size()];
    		for (int i = 0; i < finNotifyPremSrcList.size(); i++) {
    			premSource[i] = finNotifyPremSrcList.get(i).getPremSource();
    		}
    		return getCodeTableString("T_NB_PREM_SOURCE",premSource,";");
    	}
        return "";
    }
    
    protected String getApplySource(Long policyId) {
    	if(policyId == null ) {
    		return "";
    	}  	
    	List<AgentNotifyPurposeVO> applyPurposeList = agentNotifyPurposeService.findByPolicyId(policyId);
    	if(!CollectionUtils.isEmpty(applyPurposeList)) {
    		String[] applyPurpose = new String[applyPurposeList.size()];
    		for (int i = 0; i < applyPurposeList.size(); i++) {
    			applyPurpose[i] = applyPurposeList.get(i).getApplyPrupose();
    		}
    		return getCodeTableString("T_NB_APPLY_PURPOSE",applyPurpose,";");
    	}
        return "";
    }
    
    protected String getPremSource(Long policyId) {
    	if(policyId == null ) {
    		return "";
    	}  	
    	List<AgentNotifyPremSourceVO> premSourceList = agentNotifyPremSourceService.findByPolicyId(policyId);
    	if(!CollectionUtils.isEmpty(premSourceList)) {
    		String[] premSource = new String[premSourceList.size()];
    		for (int i = 0; i < premSourceList.size(); i++) {
    			premSource[i] = premSourceList.get(i).getPremSource();
    		}
    		return getCodeTableString("T_NB_PREM_SOURCE_AGENT_NOTIFY",premSource,";");
    	}
        return "";
    }
    
    protected String getCodeTableString(String codeTableName,String[] codeList,String joinString) {
    	if( codeList == null || codeList.length <= 0 ) {
    		return "";
    	}
    	String[] stringList = new String[codeList.length];
    	String codeDesc = "";
    	for (int i = 0; i < codeList.length; i++) {
    		codeDesc = CodeTable.getCodeDesc(codeTableName, codeList[i], CodeCst.LANGUAGE__CHINESE_TRAD);
    		stringList[i] = codeList[i]+"-"+codeDesc;
		}
    	return StringUtils.join(stringList,joinString);
    }

    protected String getHolderGender(Long policyId) {
        StringBuffer sb = new StringBuffer();
        sb.append("select GENDER from t_Policy_Holder where policy_id = ? ");
        List<Map<String, Object>> result = query(sb.toString(), policyId);
        if (result.size() > 0 && !result.get(0).isEmpty()) {
            if (result.get(0).containsKey(GENDER)) {
                return MapUtils.getString(result.get(0), GENDER, "");
            }
        }
        return "";
    }

    protected String getPolicyCode(Long policyId) {
        StringBuffer sb = new StringBuffer();
        sb.append("select POLICY_CODE from t_Contract_Master where policy_id = ? ");
        List<Map<String, Object>> result = query(sb.toString(), policyId);
        if (result.size() > 0 && !result.get(0).isEmpty()) {
            return result.get(0).get("POLICY_CODE").toString();
        } else {
            return "";
        }
    }
    // PCR355648 整合主被保人資訊
    protected Map<String, Object> getMainInsuredInfo(Long policyId) {
        StringBuffer sb = new StringBuffer();
        sb.append("select GENDER, RELATION_TO_PH, gender, birth_date, home_tel_reg, home_tel, ");
        sb.append(" home_tel_ext, office_tel_reg, office_tel, office_tel_ext, mobile_tel, td.address_1 address ");
        sb.append(" from t_Insured_List vil left join t_address td on td.address_id = vil.address_id "); 
        sb.append(" where vil.INSURED_CATEGORY = 1 and vil.POLICY_ID = ? ");
        List<Map<String, Object>> result = query(sb.toString(), policyId);
        if (result.size() > 0) {
            return result.get(0);
        }
        return new HashMap<String, Object>();
    }
    
    // PCR355648 被保人性別
    protected Map<String, Object> getCalloutInsuredInfo(Long policyId, String certiCode) {
        StringBuffer sb = new StringBuffer();
        sb.append("select GENDER ");
        sb.append(" from t_Insured_List vil where vil.POLICY_ID = ? and vil.certi_code = ? ");
        List<Map<String, Object>> result = query(sb.toString(), policyId, certiCode);
        if (result.size() > 0) {
            return result.get(0);
        }
        return new HashMap<String, Object>();
    }
    // PCR355648 整合要保人資訊
    protected Map<String, Object> getPolicyHolderInfo(Long policyId) {
        StringBuffer sb = new StringBuffer();
        sb.append("select tph.certi_code,tph.birth_date,tph.GENDER,tph.office_tel_reg,tph.office_tel, ");
        sb.append(" tph.office_tel_ext,tph.home_tel_reg,tph.home_tel,tph.home_tel_ext,tph.mobile_tel,ta.address_1 address ");
        sb.append(" from t_policy_holder tph left join t_address ta on ta.address_id = tph.address_id where tph.policy_id = ? ");
        List<Map<String, Object>> result = query(sb.toString(), policyId);
        if (result.size() > 0) {
            return result.get(0);
        }
        return new HashMap<String, Object>();
    }

    protected Map<String, Object> getMainInfo(Long policyId) {
        StringBuffer sb = new StringBuffer();
        sb.append(" select POLICY_CODE, LIABILITY_STATE, MONEY_ID from t_Contract_Master vcm ");
        // sb.append("  inner join t_Money tm on tcm.MONEY_ID = tm.MONEY_ID ");
        sb.append(" where vcm.POLICY_ID = ? ");
        List<Map<String, Object>> result = query(sb.toString(), policyId);
        if (result.size() > 0) {
            return result.get(0);
        }
        return new HashMap<String, Object>();
    }

    protected Map<String, Object> getProdInfo(Long policyId) {
        StringBuffer sql = new StringBuffer();
        /* 保項資訊 #221576*/
        sql.append("select pkg_ls_pub_util.f_get_code_mutilang_desc('V_NBU_CALLOUT_CHARGE_MODE', cmprd.RENEWAL_TYPE, '311') charge_name, ");
        sql.append("cmPrd.std_prem_an each_prem, ");
        sql.append("cmPrd.customized_prem, ");
        sql.append("cmPrd.amount, ");
        sql.append("cmPrd.std_prem_an, ");
        sql.append("cmPrd.product_id,  ");
        sql.append("cmPrd.charge_period, ");
        sql.append("cmPrd.charge_year, ");
        // PCR355468 目標保險費
        sql.append("cmPrd.std_prem_af, ");
        sql.append("prd.unit_flag ");
        sql.append("from v_contract_master cm join v_contract_product cmprd on cm.policy_id= ? and cm.policy_id = cmprd.POLICY_ID and cmprd.master_id is null ");
        sql.append("join t_product_life prd on prd.product_id=cmprd.product_id ");
        List<Map<String, Object>> result = query(sql.toString(), policyId);
        if (CollectionUtils.isEmpty(result))
            return new HashMap<String, Object>();
        Map<String, Object> map = result.get(0);
        /* 商品資訊 */
        sql.setLength(0);
        sql.append("select v.* from V_CALLOUT_PROD_CATEGORY v where v.product_id = ? ");
        Long productId = MapUtils.getLong(map, "PRODUCT_ID");
        ProductVO productVO = prdServ.getProduct(productId);
        java.util.List<java.util.Map<String, Object>> mapList = query(sql.toString(), productId);
        map.put("UNIT_FLAG", productVO.getUnitFlag());
        // PCR355468 主約是否投資型  20201030 改成判斷商品大類
        //map.put("IS_ILP", productVO.isIlp());
        if (CollectionUtils.isEmpty(mapList))
            return map;
        for (java.util.Map<String, Object> prodMap : mapList) {
            Long parentCateId = MapUtils.getLong(prodMap, "PARENT_CATE_ID");
            if (new Long(40019).equals(parentCateId)) {// 商品大類
                map.put("BIG_PRODCATE", MapUtils.getString(prodMap, "CATEGORY_NAME"));
            } else if (new Long(40020).equals(parentCateId)) {// 商品小類
                map.put("SMALL_PRODCATE", MapUtils.getString(prodMap, "CATEGORY_NAME"));
            }
            map.put("INTERNAL_ID", MapUtils.getString(prodMap, "INTERNAL_ID"));
            map.put("PRODUCT_NAME", MapUtils.getString(prodMap, "PRODUCT_NAME"));
        }
        return map;
    }

    protected Map<String, Object> getChannelInfo(Long policyId) {
        
        Map<String, Object> result = new HashMap<String, Object>();
        
        ScAgentDataVO agentDataVO = agentService.getAgentDataByPolicyId(policyId);
        if(agentDataVO == null) {
            return result;
        }
        List<ScServiceAgentDataVO> serviceAgentList = agentDataVO.getServiceAgentList();
        if(!CollectionUtils.isEmpty(serviceAgentList)) {
            ScServiceAgentDataVO vo = serviceAgentList.get(0);
            result.put("SALES_CHANNEL_NAME", vo.getAgentChannelTypeName());
            result.put("CHANNEL_NAME", vo.getAgentChannelName());
        }
        return result;
        
//        StringBuffer sb = new StringBuffer();
//        sb.append(" select tsc.sales_channel_name, tco.Channel_Name from t_Contract_Master tcm ");
//        sb.append("  inner join t_Channel_Org tco  on tcm.Channel_Org_Id = tco.Channel_Id ");
//        sb.append("  inner join t_Sales_Channel tsc on tco.Channel_Type = tsc.sales_channel_id ");
//        sb.append("  where tcm.Policy_Id = ? ");
//        List<Map<String, Object>> result = query(sb.toString(), policyId);
//        if (result.size() > 0) {
//            return result.get(0);
//        }
//        return new HashMap<String, Object>();
    }

    protected List<Map<String, Object>> getAgentInfo(Long policyId, Long itemId) {
        StringBuffer sql = new StringBuffer();
        
        sql.append("select benefitagt.*, agt.gender ");
        sql.append("  from t_benefit_agent benefitagt ");
        sql.append("  left join t_agent agt ");
        sql.append("   on  agt.agent_id = benefitAgt.Agent_Id ");
        sql.append(" where benefitAgt.policy_id = ? and benefitAgt.item_id = ? ");
        sql.append(" order by  benefitAgt.Order_Id, benefitAgt.list_id ");
        return query(sql.toString(), policyId, itemId);
    }
    
    
    /**
     * <p>Description: 受益人資訊</p> <p>Create Time: Apr 1, 2017 </p>
     * 
     * @author tgl15
     * @param policyId
     * @return
     */
    protected List<Map<String, Object>> getBenefitInfo(Long policyId) {
        StringBuffer sb = new StringBuffer();
        sb.append(" select distinct tcb.name, tcb.share_order, tcb.share_rate, tcb.designation, tcb.avg_indi ");
        sb.append("  from t_Contract_Bene tcb ");
        sb.append("  where  tcb.bene_type in (2, 14) ");
        sb.append("  and tcb.policy_id = ? ");
        sb.append("  order by tcb.SHARE_ORDER, tcb.share_rate ");
        return query(sb.toString(), policyId);
    }

    private Integer getBenefitCount(Long policyId) throws GenericException {
        Integer result = 0;
        result = getBenefitTypeCount(policyId, "2");
        if(result == 0 )
            result = getBenefitTypeCount(policyId, "14");
        return result;
    }
    private Integer getBenefitTypeCount(Long policyId, String beneType){
      //#by2018.01.25 tgl.unb: joey, ->身故>年金
        StringBuffer sql = new StringBuffer();
        
        // SQL Optimize : 2018/10/16 
        sql.append("select count(distinct cmbe.list_id) DATA_SIZE ");
        sql.append("  from t_contract_product cmprd ");
        sql.append("  join t_benefit_insured be ");
        sql.append("    on be.item_id = cmprd.item_id ");
        sql.append("   and be.order_id = 1 ");
        sql.append("   and be.policy_id = cmprd.policy_id ");
        sql.append("  join t_insured_list belst ");
        sql.append("    on belst.list_id = be.insured_id ");
        sql.append("  join t_contract_bene cmbe ");
        sql.append("    on cmbe.insurant = belst.list_id ");
        sql.append("   and cmbe.policy_id = cmprd.policy_id ");
        sql.append(" where cmprd.policy_id = ? ");
        sql.append("   and cmprd.master_id is null ");
        sql.append("   and cmbe.bene_type = ? ");
        
//        sql.append(" select count(distinct cmbe.list_id) DATA_SIZE from t_contract_master cm join t_contract_product cmprd on cmprd.policy_id = cm.policy_id ");
//        sql.append(" and cmprd.master_id is null and cm.policy_id = ?  ");
//        sql.append(" join t_benefit_insured be on be.item_id = cmprd.item_id  and be.order_id=1  ");
//        sql.append(" join t_insured_list belst on belst.list_id = be.insured_id ");
//        sql.append(" join t_contract_bene cmbe on cmbe.insurant = belst.list_id and cmbe.bene_type = ?  ");
        
        java.util.List<java.util.Map<String, Object>> mapList = query(sql.toString(), policyId, beneType);
        if (CollectionUtils.isEmpty(mapList))
            return 0;
        java.util.Map<String, Object> map = mapList.get(0);
        return MapUtils.getInteger(map, "DATA_SIZE");
    }

    private JdbcTemplate jdbcTemplate;

    private List<Map<String, Object>> query(String sql, Object... queryCondition) throws DataAccessException {
        if (jdbcTemplate == null) {
            jdbcTemplate = new JdbcTemplate(new DataSourceWrapper());
        }
        return jdbcTemplate.queryForList(sql, queryCondition);
    }

    private String getContactNumber(String prefix, String phoneNumber, String ext) {
        StringBuffer number = new StringBuffer("");
        number.append(StringUtils.defaultString(prefix, ""));
        number.append(StringUtils.defaultString(phoneNumber, ""));
        if (StringUtils.isNotEmpty(ext)) {
            number.append(String.format("#%s", ext));
        }
        return number.toString();
    }

    public static String convertToMinguoDate(Date inputDate,String dateFormat) {
    	String minStr = "";
    	if(inputDate != null) {
    		if(com.ebao.pub.util.StringUtils.isNullOrEmpty(dateFormat)) {
    			dateFormat = "yyyy/MM/dd";
    		}
    		minStr = com.ebao.foundation.module.util.format.DateFormat.format(inputDate, dateFormat);
    		minStr = org.apache.commons.lang3.StringUtils.leftPad(String.valueOf(new Integer(minStr.substring(0, 4)) - 1911), 3, "0") + minStr.substring(4);
    		return minStr;
		}
		return minStr;
	}

	private static final String _Q_NEEDED_CALLOUT_REASON_CODE_BY_POLICY_ID = "Select Distinct cr.CALLOUT_REASON, cr.ITEM"
			+ "  From T_NB_CALLOUT_RULE_MSG cr"
			+ "  Join T_PROPOSAL_RULE_RESULT rr On cr.RULE_NAME = rr.RULE_NAME"
			+ " Where rr.POLICY_ID = ?"
			+ "   And rr.RULE_STATUS = 1"
			+ "   And Not Exists (Select z.*"
			+ "                     From (Select b.CALLOUT_REASON_CODE||nvl2(b.EXCEPTION_ITEM_CODE,'-'||b.EXCEPTION_ITEM_CODE,'') callout_reason_code"
			+ "                             From T_CALLOUT_TRANS a"
			+ "                             Join T_CALLOUT_QUESTIONNAIRE b ON a.CALLOUT_NUM = b.CALLOUT_NUM"
			+ "                            Where a.policy_id = rr.POLICY_ID"
			+ "                           UNION"
			+ "                           Select tct.callout_reason callout_reason_code"
			+ "                             From t_callout_trans tct"
			+ "                            Where tct.policy_id = rr.POLICY_ID"
			+ "                              And tct.callout_reason IS NOT NULL"
			+ "                          ) z"
			+ "                    Where callout_reason_code = cr.CALLOUT_REASON"
			+ "                   )";

	public Map<String, String> findNeededCalloutReasonCodeByPolicyId(Long policyId) {
		List<Map<String, Object>> resultMapLst = query(_Q_NEEDED_CALLOUT_REASON_CODE_BY_POLICY_ID, policyId);
		Map<String, String> result = new HashMap<String, String>();
		if (!CollectionUtils.isEmpty(resultMapLst)) {
			for (Map<String, Object> map : resultMapLst) {
				if (!map.isEmpty()) {
					if (StringUtils.isNotEmpty(MapUtils.getString(map, "callout_reason"))) {
						result.put(MapUtils.getString(map, "callout_reason"), MapUtils.getString(map, "item"));
					}
				}
			}
		}
		return result;
	}

    @Resource(name = EmployeeService.BEAN_DEFAULT)
    private EmployeeService empServ;
    
	@Resource(name = AgentNotifyPremSourceService.BEAN_DEFAULT)
	private AgentNotifyPremSourceService agentNotifyPremSourceService;

	@Resource(name = FinanceNotifyPremSourceService.BEAN_DEFAULT)
	private FinanceNotifyPremSourceService financeNotifyPremSourceService;
	
	@Resource(name = AgentNotifyPurposeService.BEAN_DEFAULT)
	private AgentNotifyPurposeService agentNotifyPurposeService;
}
