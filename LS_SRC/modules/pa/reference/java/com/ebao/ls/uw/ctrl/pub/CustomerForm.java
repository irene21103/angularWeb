package com.ebao.ls.uw.ctrl.pub;

import java.math.BigDecimal;

import com.ebao.pub.framework.GenericForm;

/**
 * The ActionForm object is used to hold and validate values for the Customer form.
 * @author Walter Huang
 * @version 1.1
 *
 */
public class CustomerForm extends GenericForm {

  // attributes

  private String updateMode = "";

  private java.lang.Long customerId = null;
  private java.lang.String gender = "";
  private java.util.Date birthday = null;
  private java.lang.Integer certiType = null;
  private java.lang.String certiCode = "";
  private java.lang.String marriageId = "";
  private java.lang.String educationId = "";
  private java.lang.String jobCom = "";
  private BigDecimal height = null;
  private java.math.BigDecimal weight = null;
  private java.lang.String email = "";
  private java.lang.Long income = null;
  private java.lang.String job1 = "";
  private java.lang.String job2 = "";
  private java.lang.Long jobCateId = null;
  private java.lang.String nationality = "";
  private java.lang.Integer jobKind = null;
  private java.lang.String title = "";
  private java.lang.String smoking = "";
  private java.util.Date insertTime = null;
  private java.lang.String status = "";
  private java.util.Date deathTime = null;
  private java.lang.Long jobCateId2 = null;
  private java.lang.String bp = "";
  private java.lang.String drivingLicence = "";
  private java.lang.String ssCode = "";
  private java.lang.String retired = "";
  private java.lang.String langId = "";
  private java.lang.String homeplace = "";
  private java.lang.String nationCode = "";
  private java.lang.String accidentStatus = "";
  private java.util.Date updateTime = null;
  private java.lang.String householder = "";
  private java.lang.String honorTitle = "";
  private java.lang.String custGrade = "";
  private java.lang.String oriCertiCode = "";
  private java.lang.String focusType = "";
  private java.lang.String employed = "";
  private java.lang.Long empId = null;
  private java.lang.String countryCode = "";
  private java.lang.Integer blacklistCause = null;
  private java.lang.String email2 = "";
  private java.lang.Long addressId = null;
  private java.lang.String mobile = "";
  private java.lang.String officeTel = "";
  private java.lang.String homeTel = "";
  private java.lang.String firstName = "";
  private java.lang.String midName = "";
  private java.lang.String lastName = "";
  private java.lang.String aliasName = "";
  private java.lang.Integer industryId = null;
  private java.lang.String religionCode = "";
  private java.lang.Integer dataLevel = null;
  private java.lang.String proofAge = "";
  private java.lang.String suspend = "";
  private java.lang.Long suspendChgId = null;
  private String maFactor;
  private String msFactor;
  private String isMaFactor;
  private String isMsFactor;
  private String ma_max;
  private String ma_min;
  private String ms_max;
  private String ms_min;

  public String getMa_max() {
    return ma_max;
  }

  public void setMa_max(String ma_max) {
    this.ma_max = ma_max;
  }

  public String getMa_min() {
    return ma_min;
  }

  public void setMa_min(String ma_min) {
    this.ma_min = ma_min;
  }

  public String getMs_max() {
    return ms_max;
  }

  public void setMs_max(String ms_max) {
    this.ms_max = ms_max;
  }

  public String getMs_min() {
    return ms_min;
  }

  public void setMs_min(String ms_min) {
    this.ms_min = ms_min;
  }

  public String getIsMaFactor() {
    return isMaFactor;
  }

  public void setIsMaFactor(String isMaFactor) {
    this.isMaFactor = isMaFactor;
  }

  public String getIsMsFactor() {
    return isMsFactor;
  }

  public void setIsMsFactor(String isMsFactor) {
    this.isMsFactor = isMsFactor;
  }

  public String getMaFactor() {
    return maFactor;
  }

  public void setMaFactor(String maFactor) {
    this.maFactor = maFactor;
  }

  public String getMsFactor() {
    return msFactor;
  }

  public void setMsFactor(String msFactor) {
    this.msFactor = msFactor;
  }

  /**
   * returns the value of updateMode
   *
   * @return the updateMode, one of "TRUE" or "FALSE"
   */
  public String getUpdateMode() {
    return updateMode;
  }

  /**
   * sets the value of updateMode
   *
   * @param updateMode the value of updateMode, usually "TRUE" or "FALSE"
   */
  public void setUpdateMode(String updateMode) {
    this.updateMode = updateMode;
  }

  /**
   * returns the value of the customerId
   *
   * @return the customerId
   */
  public java.lang.Long getCustomerId() {
    return customerId;
  }

  /**
   * sets the value of the customerId
   *
   * @param customerId the customerId
   */
  public void setCustomerId(java.lang.Long customerId) {
    this.customerId = customerId;
  }

  /**
   * returns the value of the gender
   *
   * @return the gender
   */
  public java.lang.String getGender() {
    return gender;
  }

  /**
   * sets the value of the gender
   *
   * @param gender the gender
   */
  public void setGender(java.lang.String gender) {
    this.gender = gender;
  }

  /**
   * returns the value of the birthday
   *
   * @return the birthday
   */
  public java.util.Date getBirthday() {
    return birthday;
  }

  /**
   * sets the value of the birthday
   *
   * @param birthday the birthday
   */
  public void setBirthday(java.util.Date birthday) {
    this.birthday = birthday;
  }

  /**
   * returns the value of the certiType
   *
   * @return the certiType
   */
  public java.lang.Integer getCertiType() {
    return certiType;
  }

  /**
   * sets the value of the certiType
   *
   * @param certiType the certiType
   */
  public void setCertiType(java.lang.Integer certiType) {
    this.certiType = certiType;
  }

  /**
   * returns the value of the certiCode
   *
   * @return the certiCode
   */
  public java.lang.String getCertiCode() {
    return certiCode;
  }

  /**
   * sets the value of the certiCode
   *
   * @param certiCode the certiCode
   */
  public void setCertiCode(java.lang.String certiCode) {
    this.certiCode = certiCode;
  }

  /**
   * returns the value of the marriageId
   *
   * @return the marriageId
   */
  public java.lang.String getMarriageId() {
    return marriageId;
  }

  /**
   * sets the value of the marriageId
   *
   * @param marriageId the marriageId
   */
  public void setMarriageId(java.lang.String marriageId) {
    this.marriageId = marriageId;
  }

  /**
   * returns the value of the educationId
   *
   * @return the educationId
   */
  public java.lang.String getEducationId() {
    return educationId;
  }

  /**
   * sets the value of the educationId
   *
   * @param educationId the educationId
   */
  public void setEducationId(java.lang.String educationId) {
    this.educationId = educationId;
  }

  /**
   * returns the value of the jobCom
   *
   * @return the jobCom
   */
  public java.lang.String getJobCom() {
    return jobCom;
  }

  /**
   * sets the value of the jobCom
   *
   * @param jobCom the jobCom
   */
  public void setJobCom(java.lang.String jobCom) {
    this.jobCom = jobCom;
  }

  /**
   * returns the value of the height
   *
   * @return the height
   */
  public BigDecimal getHeight() {
    return height;
  }

  /**
   * sets the value of the height
   *
   * @param height the height
   */
  public void setHeight(BigDecimal height) {
    this.height = height;
  }

  /**
   * returns the value of the weight
   *
   * @return the weight
   */
  public java.math.BigDecimal getWeight() {
    return weight;
  }

  /**
   * sets the value of the weight
   *
   * @param weight the weight
   */
  public void setWeight(java.math.BigDecimal weight) {
    this.weight = weight;
  }

  /**
   * returns the value of the email
   *
   * @return the email
   */
  public java.lang.String getEmail() {
    return email;
  }

  /**
   * sets the value of the email
   *
   * @param email the email
   */
  public void setEmail(java.lang.String email) {
    this.email = email;
  }

  /**
   * returns the value of the income
   *
   * @return the income
   */
  public java.lang.Long getIncome() {
    return income;
  }

  /**
   * sets the value of the income
   *
   * @param income the income
   */
  public void setIncome(java.lang.Long income) {
    this.income = income;
  }

  /**
   * returns the value of the job1
   *
   * @return the job1
   */
  public java.lang.String getJob1() {
    return job1;
  }

  /**
   * sets the value of the job1
   *
   * @param job1 the job1
   */
  public void setJob1(java.lang.String job1) {
    this.job1 = job1;
  }

  /**
   * returns the value of the job2
   *
   * @return the job2
   */
  public java.lang.String getJob2() {
    return job2;
  }

  /**
   * sets the value of the job2
   *
   * @param job2 the job2
   */
  public void setJob2(java.lang.String job2) {
    this.job2 = job2;
  }

  /**
   * returns the value of the jobCateId
   *
   * @return the jobCateId
   */
  public java.lang.Long getJobCateId() {
    return jobCateId;
  }

  /**
   * sets the value of the jobCateId
   *
   * @param jobCateId the jobCateId
   */
  public void setJobCateId(java.lang.Long jobCateId) {
    this.jobCateId = jobCateId;
  }

  /**
   * returns the value of the nationality
   *
   * @return the nationality
   */
  public java.lang.String getNationality() {
    return nationality;
  }

  /**
   * sets the value of the nationality
   *
   * @param nationality the nationality
   */
  public void setNationality(java.lang.String nationality) {
    this.nationality = nationality;
  }

  /**
   * returns the value of the jobKind
   *
   * @return the jobKind
   */
  public java.lang.Integer getJobKind() {
    return jobKind;
  }

  /**
   * sets the value of the jobKind
   *
   * @param jobKind the jobKind
   */
  public void setJobKind(java.lang.Integer jobKind) {
    this.jobKind = jobKind;
  }

  /**
   * returns the value of the title
   *
   * @return the title
   */
  public java.lang.String getTitle() {
    return title;
  }

  /**
   * sets the value of the title
   *
   * @param title the title
   */
  public void setTitle(java.lang.String title) {
    this.title = title;
  }

  /**
   * returns the value of the smoking
   *
   * @return the smoking
   */
  public java.lang.String getSmoking() {
    return smoking;
  }

  /**
   * sets the value of the smoking
   *
   * @param smoking the smoking
   */
  public void setSmoking(java.lang.String smoking) {
    this.smoking = smoking;
  }

  /**
   * returns the value of the insertTime
   *
   * @return the insertTime
   */
  public java.util.Date getInsertTime() {
    return insertTime;
  }

  /**
   * sets the value of the insertTime
   *
   * @param insertTime the insertTime
   */
  public void setInsertTime(java.util.Date insertTime) {
    this.insertTime = insertTime;
  }

  /**
   * returns the value of the status
   *
   * @return the status
   */
  public java.lang.String getStatus() {
    return status;
  }

  /**
   * sets the value of the status
   *
   * @param status the status
   */
  public void setStatus(java.lang.String status) {
    this.status = status;
  }

  /**
   * returns the value of the deathTime
   *
   * @return the deathTime
   */
  public java.util.Date getDeathTime() {
    return deathTime;
  }

  /**
   * sets the value of the deathTime
   *
   * @param deathTime the deathTime
   */
  public void setDeathTime(java.util.Date deathTime) {
    this.deathTime = deathTime;
  }

  /**
   * returns the value of the jobCateId2
   *
   * @return the jobCateId2
   */
  public java.lang.Long getJobCateId2() {
    return jobCateId2;
  }

  /**
   * sets the value of the jobCateId2
   *
   * @param jobCateId2 the jobCateId2
   */
  public void setJobCateId2(java.lang.Long jobCateId2) {
    this.jobCateId2 = jobCateId2;
  }

  /**
   * returns the value of the bp
   *
   * @return the bp
   */
  public java.lang.String getBp() {
    return bp;
  }

  /**
   * sets the value of the bp
   *
   * @param bp the bp
   */
  public void setBp(java.lang.String bp) {
    this.bp = bp;
  }

  /**
   * returns the value of the drivingLicence
   *
   * @return the drivingLicence
   */
  public java.lang.String getDrivingLicence() {
    return drivingLicence;
  }

  /**
   * sets the value of the drivingLicence
   *
   * @param drivingLicence the drivingLicence
   */
  public void setDrivingLicence(java.lang.String drivingLicence) {
    this.drivingLicence = drivingLicence;
  }

  /**
   * returns the value of the ssCode
   *
   * @return the ssCode
   */
  public java.lang.String getSsCode() {
    return ssCode;
  }

  /**
   * sets the value of the ssCode
   *
   * @param ssCode the ssCode
   */
  public void setSsCode(java.lang.String ssCode) {
    this.ssCode = ssCode;
  }

  /**
   * returns the value of the retired
   *
   * @return the retired
   */
  public java.lang.String getRetired() {
    return retired;
  }

  /**
   * sets the value of the retired
   *
   * @param retired the retired
   */
  public void setRetired(java.lang.String retired) {
    this.retired = retired;
  }

  /**
   * returns the value of the langId
   *
   * @return the langId
   */
  public java.lang.String getLangId() {
    return langId;
  }

  /**
   * sets the value of the langId
   *
   * @param langId the langId
   */
  public void setLangId(java.lang.String langId) {
    this.langId = langId;
  }

  /**
   * returns the value of the homeplace
   *
   * @return the homeplace
   */
  public java.lang.String getHomeplace() {
    return homeplace;
  }

  /**
   * sets the value of the homeplace
   *
   * @param homeplace the homeplace
   */
  public void setHomeplace(java.lang.String homeplace) {
    this.homeplace = homeplace;
  }

  /**
   * returns the value of the nationCode
   *
   * @return the nationCode
   */
  public java.lang.String getNationCode() {
    return nationCode;
  }

  /**
   * sets the value of the nationCode
   *
   * @param nationCode the nationCode
   */
  public void setNationCode(java.lang.String nationCode) {
    this.nationCode = nationCode;
  }

  /**
   * returns the value of the accidentStatus
   *
   * @return the accidentStatus
   */
  public java.lang.String getAccidentStatus() {
    return accidentStatus;
  }

  /**
   * sets the value of the accidentStatus
   *
   * @param accidentStatus the accidentStatus
   */
  public void setAccidentStatus(java.lang.String accidentStatus) {
    this.accidentStatus = accidentStatus;
  }

  /**
   * returns the value of the updateTime
   *
   * @return the updateTime
   */
  public java.util.Date getUpdateTime() {
    return updateTime;
  }

  /**
   * sets the value of the updateTime
   *
   * @param updateTime the updateTime
   */
  public void setUpdateTime(java.util.Date updateTime) {
    this.updateTime = updateTime;
  }

  /**
   * returns the value of the householder
   *
   * @return the householder
   */
  public java.lang.String getHouseholder() {
    return householder;
  }

  /**
   * sets the value of the householder
   *
   * @param householder the householder
   */
  public void setHouseholder(java.lang.String householder) {
    this.householder = householder;
  }

  /**
   * returns the value of the honorTitle
   *
   * @return the honorTitle
   */
  public java.lang.String getHonorTitle() {
    return honorTitle;
  }

  /**
   * sets the value of the honorTitle
   *
   * @param honorTitle the honorTitle
   */
  public void setHonorTitle(java.lang.String honorTitle) {
    this.honorTitle = honorTitle;
  }

  /**
   * returns the value of the custGrade
   *
   * @return the custGrade
   */
  public java.lang.String getCustGrade() {
    return custGrade;
  }

  /**
   * sets the value of the custGrade
   *
   * @param custGrade the custGrade
   */
  public void setCustGrade(java.lang.String custGrade) {
    this.custGrade = custGrade;
  }

  /**
   * returns the value of the oriCertiCode
   *
   * @return the oriCertiCode
   */
  public java.lang.String getOriCertiCode() {
    return oriCertiCode;
  }

  /**
   * sets the value of the oriCertiCode
   *
   * @param oriCertiCode the oriCertiCode
   */
  public void setOriCertiCode(java.lang.String oriCertiCode) {
    this.oriCertiCode = oriCertiCode;
  }

  /**
   * returns the value of the focusType
   *
   * @return the focusType
   */
  public java.lang.String getFocusType() {
    return focusType;
  }

  /**
   * sets the value of the focusType
   *
   * @param focusType the focusType
   */
  public void setFocusType(java.lang.String focusType) {
    this.focusType = focusType;
  }

  /**
   * returns the value of the employed
   *
   * @return the employed
   */
  public java.lang.String getEmployed() {
    return employed;
  }

  /**
   * sets the value of the employed
   *
   * @param employed the employed
   */
  public void setEmployed(java.lang.String employed) {
    this.employed = employed;
  }

  /**
   * returns the value of the empId
   *
   * @return the empId
   */
  public java.lang.Long getEmpId() {
    return empId;
  }

  /**
   * sets the value of the empId
   *
   * @param empId the empId
   */
  public void setEmpId(java.lang.Long empId) {
    this.empId = empId;
  }

  /**
   * returns the value of the countryCode
   *
   * @return the countryCode
   */
  public java.lang.String getCountryCode() {
    return countryCode;
  }

  /**
   * sets the value of the countryCode
   *
   * @param countryCode the countryCode
   */
  public void setCountryCode(java.lang.String countryCode) {
    this.countryCode = countryCode;
  }

  /**
   * returns the value of the blacklistCause
   *
   * @return the blacklistCause
   */
  public java.lang.Integer getBlacklistCause() {
    return blacklistCause;
  }

  /**
   * sets the value of the blacklistCause
   *
   * @param blacklistCause the blacklistCause
   */
  public void setBlacklistCause(java.lang.Integer blacklistCause) {
    this.blacklistCause = blacklistCause;
  }

  /**
   * returns the value of the email2
   *
   * @return the email2
   */
  public java.lang.String getEmail2() {
    return email2;
  }

  /**
   * sets the value of the email2
   *
   * @param email2 the email2
   */
  public void setEmail2(java.lang.String email2) {
    this.email2 = email2;
  }

  /**
   * returns the value of the addressId
   *
   * @return the addressId
   */
  public java.lang.Long getAddressId() {
    return addressId;
  }

  /**
   * sets the value of the addressId
   *
   * @param addressId the addressId
   */
  public void setAddressId(java.lang.Long addressId) {
    this.addressId = addressId;
  }

  /**
   * returns the value of the mobile
   *
   * @return the mobile
   */
  public java.lang.String getMobile() {
    return mobile;
  }

  /**
   * sets the value of the mobile
   *
   * @param mobile the mobile
   */
  public void setMobile(java.lang.String mobile) {
    this.mobile = mobile;
  }

  /**
   * returns the value of the officeTel
   *
   * @return the officeTel
   */
  public java.lang.String getOfficeTel() {
    return officeTel;
  }

  /**
   * sets the value of the officeTel
   *
   * @param officeTel the officeTel
   */
  public void setOfficeTel(java.lang.String officeTel) {
    this.officeTel = officeTel;
  }

  /**
   * returns the value of the homeTel
   *
   * @return the homeTel
   */
  public java.lang.String getHomeTel() {
    return homeTel;
  }

  /**
   * sets the value of the homeTel
   *
   * @param homeTel the homeTel
   */
  public void setHomeTel(java.lang.String homeTel) {
    this.homeTel = homeTel;
  }

  /**
   * returns the value of the firstName
   *
   * @return the firstName
   */
  public java.lang.String getFirstName() {
    return firstName;
  }

  /**
   * sets the value of the firstName
   *
   * @param firstName the firstName
   */
  public void setFirstName(java.lang.String firstName) {
    this.firstName = firstName;
  }

  /**
   * returns the value of the midName
   *
   * @return the midName
   */
  public java.lang.String getMidName() {
    return midName;
  }

  /**
   * sets the value of the midName
   *
   * @param midName the midName
   */
  public void setMidName(java.lang.String midName) {
    this.midName = midName;
  }

  /**
   * returns the value of the lastName
   *
   * @return the lastName
   */
  public java.lang.String getLastName() {
    return lastName;
  }

  /**
   * sets the value of the lastName
   *
   * @param lastName the lastName
   */
  public void setLastName(java.lang.String lastName) {
    this.lastName = lastName;
  }

  /**
   * returns the value of the aliasName
   *
   * @return the aliasName
   */
  public java.lang.String getAliasName() {
    return aliasName;
  }

  /**
   * sets the value of the aliasName
   *
   * @param aliasName the aliasName
   */
  public void setAliasName(java.lang.String aliasName) {
    this.aliasName = aliasName;
  }

  /**
   * returns the value of the industryId
   *
   * @return the industryId
   */
  public java.lang.Integer getIndustryId() {
    return industryId;
  }

  /**
   * sets the value of the industryId
   *
   * @param industryId the industryId
   */
  public void setIndustryId(java.lang.Integer industryId) {
    this.industryId = industryId;
  }

  /**
   * returns the value of the religionCode
   *
   * @return the religionCode
   */
  public java.lang.String getReligionCode() {
    return religionCode;
  }

  /**
   * sets the value of the religionCode
   *
   * @param religionCode the religionCode
   */
  public void setReligionCode(java.lang.String religionCode) {
    this.religionCode = religionCode;
  }

  /**
   * returns the value of the dataLevel
   *
   * @return the dataLevel
   */
  public java.lang.Integer getDataLevel() {
    return dataLevel;
  }

  /**
   * sets the value of the dataLevel
   *
   * @param dataLevel the dataLevel
   */
  public void setDataLevel(java.lang.Integer dataLevel) {
    this.dataLevel = dataLevel;
  }

  /**
   * returns the value of the proofAge
   *
   * @return the proofAge
   */
  public java.lang.String getProofAge() {
    return proofAge;
  }

  /**
   * sets the value of the proofAge
   *
   * @param proofAge the proofAge
   */
  public void setProofAge(java.lang.String proofAge) {
    this.proofAge = proofAge;
  }

  /**
   * returns the value of the suspend
   *
   * @return the suspend
   */
  public java.lang.String getSuspend() {
    return suspend;
  }

  /**
   * sets the value of the suspend
   *
   * @param suspend the suspend
   */
  public void setSuspend(java.lang.String suspend) {
    this.suspend = suspend;
  }

  /**
   * returns the value of the suspendChgId
   *
   * @return the suspendChgId
   */
  public java.lang.Long getSuspendChgId() {
    return suspendChgId;
  }

  /**
   * sets the value of the suspendChgId
   *
   * @param suspendChgId the suspendChgId
   */
  public void setSuspendChgId(java.lang.Long suspendChgId) {
    this.suspendChgId = suspendChgId;
  }

  // Begin Additional Business Methods
  private int ageEntry;

  public int getAgeEntry() {
    return ageEntry;
  }
  public void setAgeEntry(int ageEntry) {
    this.ageEntry = ageEntry;
  }
  public void setAgeEntry(Integer ageEntry) {
    this.ageEntry = ageEntry.intValue();
  }
  // End Additional Business Methods
}