package com.ebao.ls.uw.ctrl.info;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.foundation.module.db.DBean;
import com.ebao.ls.pa.nb.util.NBUtils;
import com.ebao.ls.pa.pub.bs.PolicyService;
import com.ebao.ls.pa.pub.vo.CoverageAgentVO;
import com.ebao.ls.pa.pub.vo.CoverageVO;
import com.ebao.ls.pa.pub.vo.PolicyVO;
import com.ebao.ls.sc.data.bo.AgentChl;
import com.ebao.ls.sc.service.agent.AgentScService;
import com.ebao.ls.uw.ds.UwQmQueryService;
import com.ebao.ls.uw.ds.constant.Cst;
import com.ebao.ls.uw.vo.QualityManageListVO;
import com.ebao.pub.framework.GenericAction;
import com.ebao.pub.framework.RTException;
import com.hazelcast.util.StringUtil;

public class SalesmanQualityRecordAction extends GenericAction {

    @Resource(name = QualityRecordActionHelper.BEAN_DEFAULT)
    private QualityRecordActionHelper qualityRecordActionHelper;
    
    @Resource(name = UwQmQueryService.BEAN_DEFAULT)
    private UwQmQueryService uwqqService;
    
	@Resource(name = PolicyService.BEAN_DEFAULT)
    protected PolicyService policyService;
	   
    @Override
    public ActionForward process(ActionMapping mapping, ActionForm form,
                    HttpServletRequest request, HttpServletResponse response)
                    throws Exception {
        
        String policyId = request.getParameter("policyId").toString();
        Set<String> registerCodeSet = new HashSet<String>();
        String forward = "display";
        
        /* PCR-502991:「業務員品質控管資訊」
         * 1.1	系統作業日或要保書填寫日任一落在業務員控管期間須顯示資料
         * 1.2	業務員基本資料存在「身分證號 / 統一證號」資料時，須判斷該「身分證號 / 統一證號」是否存在「業務員品質控管資訊」資料(規則同登錄字號)，若存在須顯示資料
         * 1.3	以「登錄證字號」、「身分證號 / 統一證號」查詢「業務員品質控管資訊」，相同「業務員品質控管資訊」資料不重覆顯示
         */
        Set<String> certiCodeSet = new HashSet<String>();
        String certiCode = new String();

        for(String o : qualityRecordActionHelper.findAgentById(policyId)){
            registerCodeSet.add(o);
            certiCode = findAgentCertiCodeById(o);
            if( !StringUtil.isNullOrEmpty(certiCode)) {
            	certiCodeSet.add(certiCode);
            }
        }
        
	   	PolicyVO policyVO = policyService.load(Long.valueOf(policyId));
	   	Date date = policyVO.getApplyDate();
	   	if( !registerCodeSet.isEmpty() ) {
	        List<QualityManageListVO> listRegisterCode = uwqqService.findByRegisterCodeAndDate(registerCodeSet, date);
	        request.setAttribute("qualityRecordList", listRegisterCode);
	    }
	   	if( !certiCodeSet.isEmpty() ) {	   	
	   		String qmType = Cst.QUALITY_MANAGER_TYPE_AGENT;
	        List<QualityManageListVO> listCertiCodeSet = uwqqService.findByCertiCodeAndDate(certiCodeSet, date, qmType);
	        request.setAttribute("qualityRecordList", listCertiCodeSet);
	   	}
        return mapping.findForward(forward);
    }

    public static final String SELECT_AGENTINFO = 
    		"select distinct certi_code from t_agent  " +
			" where register_code =? " + 
			"   and agent_status='0' " ;   //0-在職

    public static String findAgentCertiCodeById(String registerCode){
    	String agentCertiCode = null;

        if(StringUtils.isEmpty(registerCode)) {
			return agentCertiCode;
        }
        
		DBean db = new DBean();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {		
			db.connect();
			Connection con = db.getConnection();

			ps = con.prepareStatement(SELECT_AGENTINFO);
			ps.setString( 1, registerCode);
			rs= ps.executeQuery();
			
			while(rs.next()) {
				agentCertiCode = rs.getString("certi_code");
			}
			
		} catch (SQLException e) {
			throw new RTException(e);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DBean.closeAll(rs, ps, db);
		}
			return agentCertiCode;
    }
    
}
