package com.ebao.ls.crs.identity.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

import com.ebao.foundation.commons.exceptions.GenericException;
import com.ebao.foundation.core.RTException;
import com.ebao.ls.crs.company.CRSControlManLogService;
import com.ebao.ls.crs.company.CRSEntityControlManListLogService;
import com.ebao.ls.crs.company.CRSEntityLogService;
import com.ebao.ls.crs.constant.CRSCodeCst;
import com.ebao.ls.crs.constant.CRSCodeType;
import com.ebao.ls.crs.constant.CRSTinBizSource;
import com.ebao.ls.crs.customer.CRSIndividualLogService;
import com.ebao.ls.crs.data.bo.CRSPartyLog;
import com.ebao.ls.crs.data.identity.CRSPartyLogDAO;
import com.ebao.ls.crs.identity.CRSPartyLogService;
import com.ebao.ls.crs.identity.CRSPartyService;
import com.ebao.ls.crs.identity.CRSTaxIDNumberLogService;
import com.ebao.ls.crs.vo.CRSControlManLogVO;
import com.ebao.ls.crs.vo.CRSEntityControlManListLogVO;
import com.ebao.ls.crs.vo.CRSEntityLogVO;
import com.ebao.ls.crs.vo.CRSIndividualLogVO;
import com.ebao.ls.crs.vo.CRSManAccountLogVO;
import com.ebao.ls.crs.vo.CRSPartyLogVO;
import com.ebao.ls.crs.vo.CRSPartyVO;
import com.ebao.ls.crs.vo.CRSTaxIDNumberLogVO;
import com.ebao.ls.cs.commonflow.data.TChangeDelegate;
import com.ebao.ls.cs.commonflow.data.TPolicyChangeDelegate;
import com.ebao.ls.cs.commonflow.data.bo.AlterationItem;
import com.ebao.ls.cs.commonflow.data.bo.Change;
import com.ebao.ls.foundation.service.GenericServiceImpl;
import com.ebao.ls.pa.nb.util.NBUtils;
import com.ebao.ls.pa.pub.bs.BeneficiaryService;
import com.ebao.ls.pa.pub.bs.LegalRepresentativeService;
import com.ebao.ls.pa.pub.bs.PolicyHolderService;
import com.ebao.ls.pa.pub.ci.impl.PolicyCIImpl;
import com.ebao.ls.pa.pub.vo.BeneficiaryVO;
import com.ebao.ls.pa.pub.vo.InsuredVO;
import com.ebao.ls.pa.pub.vo.LegalRepresentativeVO;
import com.ebao.ls.pa.pub.vo.PolicyVO;
import com.ebao.ls.pty.ci.CustomerCI;
import com.ebao.ls.pty.ci.DeptCI;
import com.ebao.ls.pty.ci.EmployeeCI;
import com.ebao.ls.pty.vo.EmployeeVO;
import com.ebao.ls.pub.CodeCst;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.framework.MultiWarning;
import com.ebao.pub.i18n.lang.CodeTable;
import com.ebao.pub.i18n.lang.StringResource;
import java.lang.reflect.InvocationTargetException;
import java.util.Date;
import org.apache.commons.lang3.exception.ExceptionUtils;

public class CRSPartyLogServiceImpl extends GenericServiceImpl<CRSPartyLogVO, CRSPartyLog, CRSPartyLogDAO>
				implements CRSPartyLogService {
	//[330030][Lyn]e-Approval ITR-1801051_CRS共同申報及盡職審查專案- 保全變更及CRS確認申報國作業-CRS提交檢核
	private static final int NUMBER_OF_ONE = 1;
	private static final int NUMBER_OF_TWO = 2;
	private static final int NUMBER_OF_THREE = 3;
	private static final int NUMBER_OF_FOUR = 4;
	
	@Resource(name = CRSPartyLogDAO.BEAN_DEFAULT)
	private CRSPartyLogDAO partyLogDAO;

	@Resource(name = EmployeeCI.BEAN_DEFAULT)
	private EmployeeCI empServ;

	@Resource(name = DeptCI.BEAN_DEFAULT)
	private DeptCI deptServ;

	@Resource(name = PolicyHolderService.BEAN_DEFAULT)
	private PolicyHolderService policyHolderServ;

	@Resource(name = com.ebao.ls.pa.pub.bs.InsuredService.BEAN_DEFAULT)
	private com.ebao.ls.pa.pub.bs.InsuredService insuredServ;

	@Resource(name = BeneficiaryService.BEAN_DEFAULT)
	private BeneficiaryService benefitServ;

	@Resource(name = com.ebao.ls.pa.pub.bs.PolicyService.BEAN_DEFAULT)
	private com.ebao.ls.pa.pub.bs.PolicyService policyServ;

	@Resource(name = CustomerCI.BEAN_DEFAULT)
	private CustomerCI customerServ;

	@Resource(name = CRSControlManLogService.BEAN_DEFAULT)
	protected CRSControlManLogService crsControlManLogService;

	@Resource(name = CRSTaxIDNumberLogService.BEAN_DEFAULT)
	protected CRSTaxIDNumberLogService crsTaxLogService;

	@Resource(name = CRSIndividualLogService.BEAN_DEFAULT)
	protected CRSIndividualLogService crsCustomerLogService;

	@Resource(name = CRSEntityLogService.BEAN_DEFAULT)
	protected CRSEntityLogService crsCompanyService;

	@Resource(name = CRSControlManLogService.BEAN_DEFAULT)
	protected CRSControlManLogService crsControllerService;

	@Resource(name = CRSEntityControlManListLogService.BEAN_DEFAULT)
	protected CRSEntityControlManListLogService crsEntityControlManListLogService;
	
	@Resource(name = CRSPartyService.BEAN_DEFAULT)
	protected CRSPartyService crsPartyService;
	
	@Resource(name = PolicyCIImpl.BEAN_DEFAULT)
	protected PolicyCIImpl policyCIImpl;
    
    @Resource(name = LegalRepresentativeService.BEAN_DEFAULT)
	private LegalRepresentativeService legalRepresentativeService;

	@Resource(name = com.ebao.ls.pa.pub.bs.PolicyService.BEAN_DEFAULT)
	private com.ebao.ls.pa.pub.bs.PolicyService paPolicyService;

	@Override
	protected CRSPartyLogVO newEntityVO() {
		return new CRSPartyLogVO();
	}

	@Override
	public List<CRSPartyLog> queryPartyLog(String policyType, String certiCode, String name, String policyCode,
					String crsType) {
		return partyLogDAO.queryPartyLog(policyType, certiCode, name, policyCode, crsType);
	}

	@Override
	public void markRemove(Long logId) {
		CRSPartyLogVO party = this.load(logId);
		if (party == null)
			throw new RTException("[DataNotFound] t_crs_party_log.log_id := " + logId);
		party.setBuildType(CRSCodeCst.BUILD_TYPE__REMOVE);
		this.save(party);
	}
	
	@Override
	public void markRevoke(Long logId) {
		CRSPartyLogVO party = this.load(logId);
		if (party == null)
			throw new RTException("[DataNotFound] t_crs_party_log.log_id := " + logId);
		party.setBuildType(CRSCodeCst.BUILD_TYPE__CANCEL);
		this.save(party);
	}

	@Override
	public List<CRSPartyLogVO> parseUIRows(List<CRSPartyLog> list, Long userId) throws GenericException {
		if (CollectionUtils.isEmpty(list))
			return null;

		List<CRSPartyLogVO> resultVOList = new java.util.LinkedList<CRSPartyLogVO>();
		EmployeeVO currentEmpVO = empServ.getEmployeeByEmpId(userId);
		Integer currentDeptBizId = deptServ.getBizCategoryByDeptId(currentEmpVO.getDeptId());

		for (CRSPartyLog party : list) {
			CRSPartyLogVO vo = new CRSPartyLogVO();
			party.copyToVO(vo, Boolean.FALSE);
			/* 是否准許使用者編輯資料 */
			EmployeeVO empVO = empServ.getEmployeeByEmpId(party.getHandlerId());
			if (empVO != null) {
				Long deptId = empVO.getDeptId();
				Boolean approve = Boolean.FALSE;
				//1. 同一個部門
				approve = deptId.equals(currentEmpVO.getDeptId());
				//2. 同一類型部門，空值未設部門，視同團險GRP
				Integer bizId = deptServ.getBizCategoryByDeptId(deptId);
				if (!approve) {
					if (bizId == null && currentDeptBizId == null) {
						approve = Boolean.TRUE;
					}
					if (bizId != null && currentDeptBizId != null) {
						approve = currentDeptBizId.equals(bizId);
					}
					//批次作業部門為空,改以sys_code判斷是否為同一部門
					if (bizId == null && currentDeptBizId != null) {
						approve = party.getSysCode().equals(getSYSCode(userId));
					}
				}
				vo.setEqualsDept(approve);
			}

			resultVOList.add(vo);
		}
		return resultVOList;
	}

	@Override
	public String getSYSCode(Long userId) {
		EmployeeVO empVO = empServ.getEmployeeByEmpId(userId);
		Integer bizId = deptServ.getBizCategoryByDeptId(empVO.getDeptId());
		if (bizId == null)//空值未設部門，視同團險GRP
			return "GRP";
		if (Integer.valueOf(2).equals(bizId))
			return "UNB";
		if (Integer.valueOf(3).equals(bizId))
			return "CLM";
		if (Integer.valueOf(4).equals(bizId))
			return "POS";
		return "GRP";
	}

	@Override
	public CRSPartyLogVO findByChangeIdAndCertiCode(Long changeId, String certiCode) {
		List<CRSPartyLog> resultList = dao.findListByCertiCode(certiCode, changeId);
		if (CollectionUtils.isEmpty(resultList))
			return null;
		return convertToVO(resultList.get(0));
	}

	@Override
	public CRSPartyLogVO initCRSPartyLog(Long policyId, Long partyId, String roleType) throws GenericException {
		if (StringUtils.isEmpty(roleType))
			return null;//應該出警告訊息
		CRSPartyLogVO vo = new CRSPartyLogVO();
		com.ebao.ls.pa.pub.vo.PolicyVO policyVO = policyServ.load(policyId);
		vo.setPolicyCode(policyVO.getPolicyNumber());
		vo.setBuildType(CRSCodeCst.BUILD_TYPE__BUILDING);
		vo.setRoleType(roleType);
		if (CRSCodeCst.ROLE_TYPE__POLICY_HOLDER.equals(roleType)) {
			com.ebao.ls.pa.pub.vo.PolicyHolderVO policyHolderVO = this.policyHolderServ.getByPolicyIdAndPartyId(policyId, partyId);
			vo.setCertiCode(policyHolderVO.getCertiCode());
			//PCR486833 調整要保人ID類型為空白的判斷條件
			//vo.setCertiType(NumberUtils.toInt(policyHolderVO.getCertiType()));
			vo.setCertiType(policyHolderVO.getCertiType()==null? null:Integer.valueOf(policyHolderVO.getCertiType()));
			vo.setName(policyHolderVO.getName());
			vo.setCountry(policyHolderVO.getNationality());
		} else if (CRSCodeCst.ROLE_TYPE__INSURE_LIST.equals(roleType)) {
			com.ebao.ls.pa.pub.vo.InsuredVO insuredVO = this.insuredServ.getByPolicyIdAndPartyId(policyId, partyId);
			vo.setCertiCode(insuredVO.getCertiCode());
			//vo.setCertiType(NumberUtils.toInt(insuredVO.getCertiType()));
			vo.setCertiType(insuredVO.getCertiType()==null? null:Integer.valueOf(insuredVO.getCertiType()));
			vo.setName(insuredVO.getName());
			vo.setCountry(insuredVO.getNationality());
		} else if (CRSCodeCst.ROLE_TYPE__BENEFICIARY.equals(roleType)) {
			com.ebao.ls.pa.pub.vo.BeneficiaryVO beneficiaryVO = this.benefitServ.getByPolicyIdAndPartyId(policyId, partyId);
			vo.setCertiCode(beneficiaryVO.getCertiCode());
			vo.setCertiType(beneficiaryVO.getCertiType());
			vo.setName(beneficiaryVO.getName());
			vo.setCountry(beneficiaryVO.getNationality());
		} else if (CRSCodeCst.ROLE_TYPE__CUSTOMER.equals(roleType)) {
			com.ebao.ls.pty.vo.CustomerVO customerVO = customerServ.getPerson(partyId);
			vo.setCertiCode(customerVO.getCertiCode());
			vo.setCertiType(customerVO.getCertiType());
			vo.setName(customerVO.getName());
			vo.setCountry(customerVO.getNationality());
		} else if (CRSCodeCst.ROLE_TYPE__COMPANY_CUSTOMER.equals(roleType)) {
			com.ebao.ls.pty.vo.CompanyCustomerVO companyCustomerVO = customerServ.getCompany(partyId);
			vo.setCertiCode(companyCustomerVO.getRegisterCode());
			vo.setCertiType(CodeCst.CERTI_TYPE__COMPANY_ID);
			vo.setName(companyCustomerVO.getName());
			vo.setCountry(companyCustomerVO.getNationality());
		}
		return vo;
	}

	@Override
	public void saveOrUpdate(Long handlerId, java.util.Date processDate, CRSPartyLogVO logVO, Long changeId, String inputSource) throws GenericException {
		Long insertChangeId = changeId;
		if (logVO.getLogId() == null
						&& changeId == null
						&& NBUtils.in(inputSource,CRSCodeCst.INPUT_SOURCE__CMN,CRSCodeCst.INPUT_SOURCE__WS)) {
			Change tChange = new Change();
			tChange.setChangeSource(CodeCst.CHANGE_SOURCE__PARTY);
			tChange.setChangeStatus(CodeCst.CHANGE_STATUS__WAITING_ISSUE);
			// TODO: 暫時先用台北總公司
			tChange.setOrgId(101L);
			insertChangeId = TChangeDelegate.create(tChange);
		}
		logVO.setChangeId(insertChangeId);
		if (StringUtils.isNotEmpty(logVO.getCrsType())) {
			String crsCode = CodeTable.getIdByCode("T_CRS_IDENTITY_TYPE", logVO.getCrsType());
			logVO.setCrsCode((StringUtils.isEmpty(crsCode)) ? CRSCodeCst.CRS_IDENTITY_TYPE__NONE : crsCode);
		}
		if (handlerId != null) {
			EmployeeVO empVO = empServ.getEmployeeByEmpId(handlerId);
			logVO.setHandlerId(handlerId);
			logVO.setHandlerDept(empVO.getDeptId());
		}
		logVO.setProcessDate(processDate);
		this.save(logVO);
	}

	@Override
	public CRSPartyLogVO findCRSPartyLog(Long changeId, Long policyId, Long partyId, String roleType)
					throws GenericException {
		/*查詢客戶 ID*/
		CRSPartyLogVO logVO = this.initCRSPartyLog(policyId, partyId, roleType);
		return this.findByChangeIdAndCertiCode(changeId, logVO.getCertiCode());
	}

	@Override
	public List<CRSPartyLogVO> findListByCertiCode(String certiCode, Long changeId) {
		return this.convertToVOList(partyLogDAO.findListByCertiCode(certiCode, changeId));
	}
	
	@Override
	public List<CRSPartyLogVO> findListByCertiCodeAndChangeIdAndPolicyChangeId(String certiCode, Long changeId, Long policyChangeId) {
		return this.convertToVOList(partyLogDAO.findListByCertiCodeAndChangeIdAndPolicyChangeId(certiCode, changeId,policyChangeId));
	}
	
//	@Override
//	public List<CRSPartyLogVO> findListByChangeIdAndName(String name, Long changeId) {
//		return this.convertToVOList(partyLogDAO.findListByChangeIdAndName(name, changeId));
//	}

	/**
	 * <p>Description : 新契約覆核記錄LOG，複製一筆完整</p>
	 * <p>Created By : Simon.Huang</p>
	 * <p>Create Time : 2018年12月10日</p>
	 * @param partyLogId
	 * @param newChangeId
	 * @return
	 * @throws Exception
	 */
	public CRSPartyLogVO copyUnbLog(Long changeId, Long newChangeId) throws Exception {

		CRSPartyLogVO partyLogVO = this.findByChangeIdAndCertiCode(changeId, null);

		if(partyLogVO == null) {
			return null;
		}
		CRSPartyLogVO clonePartyLogVO = new CRSPartyLogVO();
		BeanUtils.copyProperties(clonePartyLogVO, partyLogVO);
		clonePartyLogVO.setLogId(null);
		clonePartyLogVO.setChangeId(newChangeId);
		clonePartyLogVO.setBuildType(CRSCodeCst.BUILD_TYPE__UNB_LOG);
		clonePartyLogVO = this.save(clonePartyLogVO, false);

		CRSIndividualLogVO personLogVO = null;
		if (partyLogVO != null) {
			Long partyLogId = partyLogVO .getLogId();
			List<CRSIndividualLogVO> personVOList = crsCustomerLogService.findByPartyLogId(partyLogId, false);
			if (personVOList.size() > 0) {
				personLogVO = personVOList.get(0);

				CRSIndividualLogVO clonePersonVO = new CRSIndividualLogVO();
				BeanUtils.copyProperties(clonePersonVO, personLogVO);
				clonePersonVO.setLogId(null);
				clonePersonVO.setChangeId(newChangeId);
				clonePersonVO.setCrsPartyLog(clonePartyLogVO);
				clonePersonVO = crsCustomerLogService.save(clonePersonVO, true);

				List<CRSTaxIDNumberLogVO> taxLogList = crsTaxLogService.findByPartyLogIdAndBizId(partyLogId, CRSTinBizSource.BIZ_SOURCE_PERSON, personLogVO.getLogId());
				for (CRSTaxIDNumberLogVO destVO : taxLogList) {
					CRSTaxIDNumberLogVO cloneTaxVO = new CRSTaxIDNumberLogVO();
					BeanUtils.copyProperties(cloneTaxVO, destVO);
					cloneTaxVO.setLogId(null);
					cloneTaxVO.setChangeId(newChangeId);
					cloneTaxVO.setCrsPartyLog(clonePartyLogVO);
					cloneTaxVO.setBizId(clonePersonVO.getLogId());
					crsTaxLogService.save(cloneTaxVO, true);
				}
			}

			CRSEntityLogVO companyLogVO = null;
			List<CRSEntityLogVO> companyLogList = crsCompanyService.findByPartyLogId(partyLogId, true);
			if (companyLogList.size() > 0) {
				companyLogVO = companyLogList.get(0);

				CRSEntityLogVO cloneCompanyVO = new CRSEntityLogVO();
				BeanUtils.copyProperties(cloneCompanyVO, companyLogVO);
				cloneCompanyVO.setLogId(null);
				cloneCompanyVO.setChangeId(newChangeId);
				cloneCompanyVO.setCrsPartyLog(clonePartyLogVO);
				cloneCompanyVO.setCrsEntityControlManListLog(new ArrayList<CRSEntityControlManListLogVO>());
				cloneCompanyVO = crsCompanyService.save(cloneCompanyVO, true);

				List<CRSTaxIDNumberLogVO> taxLogList = crsTaxLogService.findByPartyLogIdAndBizId(partyLogId, CRSTinBizSource.BIZ_SOURCE_COMPANY, companyLogVO.getLogId());
				for (CRSTaxIDNumberLogVO destVO : taxLogList) {
					CRSTaxIDNumberLogVO cloneTaxVO = new CRSTaxIDNumberLogVO();
					BeanUtils.copyProperties(cloneTaxVO, destVO);
					cloneTaxVO.setLogId(null);
					cloneTaxVO.setChangeId(newChangeId);
					cloneTaxVO.setCrsPartyLog(clonePartyLogVO);
					cloneTaxVO.setBizId(cloneCompanyVO.getLogId());
					cloneTaxVO = crsTaxLogService.save(cloneTaxVO, true);
				}

				List<CRSEntityControlManListLogVO> ctrlManLogList = companyLogVO.getCrsEntityControlManListLog();

				for (CRSEntityControlManListLogVO destVO : ctrlManLogList) {
					CRSEntityControlManListLogVO cloneCtrlManVO = new CRSEntityControlManListLogVO();
					BeanUtils.copyProperties(cloneCtrlManVO, destVO);
					cloneCtrlManVO.setLogId(null);
					cloneCtrlManVO.setChangeId(newChangeId);
					cloneCtrlManVO.setEntityLogId(cloneCompanyVO.getLogId());
					cloneCtrlManVO.setCrsLogId(clonePartyLogVO.getLogId());
					cloneCtrlManVO = crsEntityControlManListLogService.save(cloneCtrlManVO, false);
				}
			}

			List<CRSControlManLogVO> controlVOList = crsControlManLogService.findByPartyLogId(partyLogId, true);

			for (CRSControlManLogVO destCtrlManVO : controlVOList) {

				CRSControlManLogVO cloneCtrlManVO = new CRSControlManLogVO();
				BeanUtils.copyProperties(cloneCtrlManVO, destCtrlManVO);
				cloneCtrlManVO.setLogId(null);
				cloneCtrlManVO.setChangeId(newChangeId);
				cloneCtrlManVO.setCrsPartyLog(clonePartyLogVO);
				cloneCtrlManVO.setCrsManAccountLog(new ArrayList<CRSManAccountLogVO>());
				cloneCtrlManVO = crsControlManLogService.save(cloneCtrlManVO, true);

				List<CRSTaxIDNumberLogVO> taxLogList = crsTaxLogService.findByPartyLogIdAndBizId(partyLogId, CRSTinBizSource.BIZ_SOURCE_CTRL_MAN,
								destCtrlManVO.getLogId());

				for (CRSTaxIDNumberLogVO destVO : taxLogList) {
					CRSTaxIDNumberLogVO cloneTaxVO = new CRSTaxIDNumberLogVO();
					BeanUtils.copyProperties(cloneTaxVO, destVO);
					cloneTaxVO.setLogId(null);
					cloneTaxVO.setChangeId(newChangeId);
					cloneTaxVO.setCrsPartyLog(clonePartyLogVO);
					cloneTaxVO.setBizId(cloneCtrlManVO.getLogId());
					cloneTaxVO = crsTaxLogService.save(cloneTaxVO, true);
				}

				List<CRSManAccountLogVO> accountLogList = destCtrlManVO.getCrsManAccountLog();

				for (CRSManAccountLogVO destVO : accountLogList) {
					CRSManAccountLogVO cloneAccountVO = new CRSManAccountLogVO();
					BeanUtils.copyProperties(cloneAccountVO, destVO);
					cloneAccountVO.setLogId(null);
					cloneAccountVO.setChangeId(newChangeId);
					cloneAccountVO.setCrsLogId(clonePartyLogVO.getLogId());
					cloneAccountVO.setControlLogId(cloneCtrlManVO.getLogId());
					cloneAccountVO = crsControlManLogService.saveAccountLog(cloneAccountVO);
				}
			}
		}

		return clonePartyLogVO;
	}

	@Override
	public void removeUnbAll(Long partyLogId) {

		CRSPartyLogVO partyLogVO = this.load(partyLogId);

		CRSIndividualLogVO personLogVO = null;
		if (partyLogVO != null) {
			List<CRSIndividualLogVO> personVOList = crsCustomerLogService.findByPartyLogId(partyLogId, false);
			if (personVOList.size() > 0) {
				personLogVO = personVOList.get(0);
				List<CRSTaxIDNumberLogVO> taxLogList = crsTaxLogService.findByPartyLogIdAndBizId(partyLogId, CRSTinBizSource.BIZ_SOURCE_PERSON, personLogVO.getLogId());
				for (CRSTaxIDNumberLogVO destVO : taxLogList) {
					crsTaxLogService.remove(destVO.getLogId());
				}
				crsCustomerLogService.remove(personLogVO.getLogId());
			}

			CRSEntityLogVO companyLogVO = null;
			List<CRSEntityLogVO> companyLogList = crsCompanyService.findByPartyLogId(partyLogId, true);
			if (companyLogList.size() > 0) {
				companyLogVO = companyLogList.get(0);
				List<CRSTaxIDNumberLogVO> taxLogList = crsTaxLogService.findByPartyLogIdAndBizId(partyLogId, CRSTinBizSource.BIZ_SOURCE_COMPANY, companyLogVO.getLogId());
				for (CRSTaxIDNumberLogVO destVO : taxLogList) {
					crsTaxLogService.remove(destVO.getLogId());
				}
				crsCompanyService.remove(companyLogVO.getLogId());
			}

			List<CRSControlManLogVO> controlVOList = crsControlManLogService.findByPartyLogId(partyLogId, true);
			for (CRSControlManLogVO destCtrlManVO : controlVOList) {
				List<CRSTaxIDNumberLogVO> taxLogList = crsTaxLogService.findByPartyLogIdAndBizId(partyLogId, CRSTinBizSource.BIZ_SOURCE_CTRL_MAN,
								destCtrlManVO.getLogId());
				for (CRSTaxIDNumberLogVO destVO : taxLogList) {
					crsTaxLogService.remove(destVO.getLogId());
				}
				crsControlManLogService.remove(destCtrlManVO.getLogId());
			}
			
			this.remove(partyLogVO.getLogId());
		}
	}

	
	@Override
	public void removeUnbOther(Long partyLogId) {

		CRSPartyLogVO partyLogVO = this.load(partyLogId);

		CRSIndividualLogVO personLogVO = null;
		if (partyLogVO != null) {
			
			//不為個人，需刪除個人資料
			if (NBUtils.in(partyLogVO.getCrsType(), CRSCodeType.PERSON) == false) {
		
				List<CRSIndividualLogVO> personVOList = crsCustomerLogService.findByPartyLogId(partyLogId, false);
				if (personVOList.size() > 0) {
					personLogVO = personVOList.get(0);
					List<CRSTaxIDNumberLogVO> taxLogList = crsTaxLogService.findByPartyLogIdAndBizId(partyLogId, CRSTinBizSource.BIZ_SOURCE_PERSON, personLogVO.getLogId());
					for (CRSTaxIDNumberLogVO destVO : taxLogList) {
						crsTaxLogService.remove(destVO.getLogId());
					}
					crsCustomerLogService.remove(personLogVO.getLogId());
				}
			}

			//不為法人，需刪除法人資料
			if (NBUtils.in(partyLogVO.getCrsType(), CRSCodeType.COMPANY) == false) {
			
				CRSEntityLogVO companyLogVO = null;
				List<CRSEntityLogVO> companyLogList = crsCompanyService.findByPartyLogId(partyLogId, true);
				if (companyLogList.size() > 0) {
					companyLogVO = companyLogList.get(0);
					List<CRSTaxIDNumberLogVO> taxLogList = crsTaxLogService.findByPartyLogIdAndBizId(partyLogId, CRSTinBizSource.BIZ_SOURCE_COMPANY, companyLogVO.getLogId());
					for (CRSTaxIDNumberLogVO destVO : taxLogList) {
						crsTaxLogService.remove(destVO.getLogId());
					}
					crsCompanyService.remove(companyLogVO.getLogId());
				}
	
				List<CRSControlManLogVO> controlVOList = crsControlManLogService.findByPartyLogId(partyLogId, true);
				for (CRSControlManLogVO destCtrlManVO : controlVOList) {
					List<CRSTaxIDNumberLogVO> taxLogList = crsTaxLogService.findByPartyLogIdAndBizId(partyLogId, CRSTinBizSource.BIZ_SOURCE_CTRL_MAN,
									destCtrlManVO.getLogId());
					for (CRSTaxIDNumberLogVO destVO : taxLogList) {
						crsTaxLogService.remove(destVO.getLogId());
					}
					crsControlManLogService.remove(destCtrlManVO.getLogId());
				}

			}
		}
	}
	@Override
	public List<CRSPartyLogVO> findCRSPartyLog(String certiCode, String policyCode, String sysCode) {
		List<CRSPartyLog> partyLogList = dao.findCRSPartyLog(certiCode, policyCode, sysCode);
		return this.convertToVOList(partyLogList);
	}

	@Override
	public Boolean isExistsCRSDocument(Long crsLogId) {
		Boolean exists = Boolean.FALSE;
		List<CRSIndividualLogVO> customerVOList = this.crsCustomerLogService.findByPartyLogId(crsLogId, Boolean.FALSE);
		List<CRSEntityLogVO> companyVOList = this.crsCompanyService.findByPartyLogId(crsLogId, Boolean.FALSE);
		List<CRSControlManLogVO> controllerVOList = this.crsControllerService.findByPartyLogId(crsLogId, Boolean.FALSE);
		if(CollectionUtils.isNotEmpty(customerVOList)) {
			exists=Boolean.TRUE;
		}else if(CollectionUtils.isNotEmpty(companyVOList)) {
			exists = Boolean.TRUE;
		}else if(CollectionUtils.isNotEmpty(controllerVOList)) {
			exists = Boolean.TRUE;
		}
		return exists;
	}

	@Override
	public Boolean isExistsCRSPartyLog(Long changeId) {
		Boolean exists = Boolean.FALSE;
		List<CRSPartyLogVO> partyLogList = this.convertToVOList(partyLogDAO.findListByCertiCode(null, changeId));
		if (CollectionUtils.isNotEmpty(partyLogList)) {
			for (CRSPartyLogVO crsLogVo : partyLogList) {
				if (!crsLogVo.getBuildType().equals(CRSCodeCst.BUILD_TYPE__REMOVE)) {
					exists = Boolean.TRUE;
				}
			}
		}
		return exists;
	}

	/**
	 * PCR 257607 330030 e-Approval ITR-1801051_CRS共同申報及盡職審查專案- 保全變更及CRS確認申報國作業
	 */
	@Override
	public CRSPartyLogVO findByCrsIdAndPolicyChangeId(Long crsId, Long policyChangeId) {
		CRSPartyLog result = new CRSPartyLog();
		List<CRSPartyLog> resultList = dao.findByCrsIdAndPolicyChangeId(crsId, policyChangeId);
		
		if (CollectionUtils.isEmpty(resultList)) {
			return null;
		}else{
			result = resultList.get(0);
		}
		return convertToVO(result);
	}
	/**
	 * PCR 257607 330030 e-Approval ITR-1801051_CRS共同申報及盡職審查專案- 保全變更及CRS確認申報國作業
	 */
	@Override
	public List<CRSPartyLogVO> findByCrsIdAndChangeId(Long crsId, Long changeId) {
		List<CRSPartyLog> resultList = dao.findByCrsIdAndChangeId(crsId, changeId);
		if (CollectionUtils.isEmpty(resultList)) {
			return null;
		}
		return convertToVOList(resultList);
	}

	@Override
	public List<CRSPartyLogVO> findByChangeIdAndPolicyChangeId(Long changeId, Long policyChangeId) {
		List<CRSPartyLog> resultList = dao.findByChangeIdAndPolicyChangeId(changeId, policyChangeId);
		if (CollectionUtils.isEmpty(resultList)) {
			return null;
		}
		return convertToVOList(resultList);
	}
	/**
	 * PCR 257607 330030 e-Approval ITR-1801051_CRS共同申報及盡職審查專案- 保全變更及CRS確認申報國作業
	 */
	@Override
	public CRSPartyLogVO findLastLogByCrsIdAndChangeId(Long crsId, Long changeId){
		
		List<CRSPartyLogVO> partyLogList = this.findByCrsIdAndChangeId(crsId, changeId);
		List<AlterationItem> alterationItemList= TPolicyChangeDelegate.findByMasterChgIdOrderByOrderIdNotNull(changeId);
		
		if (partyLogList!=null){
			for(AlterationItem item :alterationItemList){
				for(CRSPartyLogVO partyLog :partyLogList){
					//取最近一一筆 
					if (partyLog.getPolicyChangeId().equals(item.getPolicyChgId())){
						
						return partyLog;
					}
				}
			}
			return partyLogList.get(0);
		}else{
		
			return null;
		}
		
	}
	
	/**
	 * PCR 257607 330030 e-Approval ITR-1801051_CRS共同申報及盡職審查專案- 保全變更及CRS確認申報國作業
	 */
	@Override
	public void removePosLog(Long changeId, Long policyChgId) {
		
		List<CRSPartyLog> resultList = dao.findListByCertiCode(null, changeId);
		for (CRSPartyLog partyLog : resultList){
			if (policyChgId!=null){
				//以保全項角度(by policyChangeId) clear all Log
				if ( partyLog.getPolicyChangeId()!=null 
						&&  partyLog.getPolicyChangeId().equals(policyChgId)){
					//pos借用unb method
					this.removeUnbOther(partyLog.getLogId());
					this.removeUnbAll(partyLog.getLogId());
				}
			}else{
				//以保全受理角度(by ChangeId) clear all Log
				//pos借用unb method
				this.removeUnbOther(partyLog.getLogId());
				this.removeUnbAll(partyLog.getLogId());
			}
			
		}
		
	}
	
	public void removePosLog(Long changeId, Long policyChgId, String certiCode) {
		
		List<CRSPartyLog> resultList = dao.findListByCertiCode(certiCode, changeId);
		for (CRSPartyLog partyLog : resultList){
			if (policyChgId!=null){
				//以保全項角度(by policyChangeId) clear all Log
				if ( partyLog.getPolicyChangeId()!=null 
						&&  partyLog.getPolicyChangeId().equals(policyChgId)){
					//pos借用unb method
					this.removeUnbOther(partyLog.getLogId());
					this.removeUnbAll(partyLog.getLogId());
				}
			}else{
				//以保全受理角度(by ChangeId) clear all Log
				//pos借用unb method
				this.removeUnbOther(partyLog.getLogId());
				this.removeUnbAll(partyLog.getLogId());
			}
			
		}
		
	}	
	
	/**
	 * <p>Description : POS 保全變更　已暫存紀錄，複製一筆完整</p>
	 * 	原 BeanUtils.copyProperties  在batch job 叫用此method時，會發生 Interger...等型態自動將 null 轉為0 
	 *  因此改成 PropertyUtils.copyProperties 單純複製即可
	 * <p>Created By : Lyn.Kuo</p>
	 * <p>Create Time : 2019年10月10日</p>
	 * @param partyLogId
	 * @param newChangeId
	 * @return
	 * @throws Exception
	 */
	public CRSPartyLogVO copyPosLog(Long newChangeId, Long newPolicyChgId, Long crsId, String certiCode ) throws Exception {
		//copy生效的log 暫存 (By CrsId)
		
		CRSPartyLogVO partyVO = null;											//Copy Log 來源
		CRSPartyVO crsParty = crsPartyService.findByCrsId(crsId); 								//CRS PARTY 主檔
		//List<CRSPartyLogVO> crsPartyLogList = new ArrayList<CRSPartyLogVO>();
		//CRSPartyLogVO crsPartyLogByPolicyChg = new CRSPartyLogVO();			//以保全項角度查找
		CRSPartyLogVO crsPartyLogByChg = null; 				//以受理角度查找
		
		//crsParty = crsPartyService.findByCrsId(crsId);
		CRSPartyLogVO clonePartyLogVO = new CRSPartyLogVO();
		
		if (crsParty!=null ){
			//crsId = crsParty.getCrsId();
			//2.先查 保全項 已儲存的 T_CRS_PARTY_LOG 資料 (By crsId + PolicyChgId) -- 在copy 前已經清除了 
			//crsPartyLogByPolicyChg = this.findByCrsIdAndPolicyChangeId(crsId,newPolicyChgId );
			//if (crsPartyLogByPolicyChg != null  ) {
			//	partyVO = new CRSPartyLogVO();
			//	BeanUtils.copyProperties(partyVO, crsPartyLogByPolicyChg);
			//}else{
			//若是由保全項變更call 此method ，需考量到多保全項的問題
			//取同一受理案件中，最近一次的log資料 
			if (newPolicyChgId!=null ){
				//3.查同受理的最近一次的 T_CRS_PARTY_LOG 資料 (By crsId + ChangeId)
				crsPartyLogByChg = this.findLastLogByCrsIdAndChangeId(crsId, newChangeId);
			}

			if (crsPartyLogByChg != null) {
				partyVO = new CRSPartyLogVO();
				PropertyUtils.copyProperties(partyVO, crsPartyLogByChg);
			} else {
				// 4.以上都沒有的狀態才改查 T_CRS_PARTY 生效對應的LOG資料 (By CrsId CertiCode)
				
				partyVO = this.load(crsParty.getLogId());
			}
			//}
		// 4.以上都沒有的狀態才改查 T_CRS_PARTY 生效的資料 (By CrsId CertiCode) 
//		if (partyVO ==null ) {
//			//crsPartyLogList = new ArrayList<CRSPartyLogVO>();
//			if (crsParty!=null){
//				partyVO = this.load(crsParty.getLogId());
//			}
//		}
		
	    //IR422658 更換要保人新增的CRS資料尚未產生CrsId/以certiCode取得Log資料
		} else {
			crsPartyLogByChg = this.findByChangeIdAndCertiCode(newChangeId, certiCode);
			if (crsPartyLogByChg != null) {
				partyVO = new CRSPartyLogVO();
				PropertyUtils.copyProperties(partyVO, crsPartyLogByChg);
			}
		}
			
		if(partyVO == null) {
			return null;
		}
		PropertyUtils.copyProperties(clonePartyLogVO, partyVO);
		
		clonePartyLogVO.setLogId(null);
		clonePartyLogVO.setChangeId(newChangeId);
		clonePartyLogVO.setPolicyChangeId(newPolicyChgId);					//批次call 這個 method，這個值塞null 
		/*POS COPY ALL LOG 預設值*/
		clonePartyLogVO.setBuildType(CRSCodeCst.BUILD_TYPE__BUILDING);		//已暫存
		clonePartyLogVO.setSysCode(CRSCodeCst.SYS_CODE__POS);
		clonePartyLogVO.setLastCmtFlg(CodeCst.YES_NO__NO);
		clonePartyLogVO.setInputSource(CRSCodeCst.INPUT_SOURCE__LINK);			//保全 input source 固定為 link
		clonePartyLogVO.setCrsId(crsId);									
		clonePartyLogVO = this.save(clonePartyLogVO, false);

		CRSIndividualLogVO personLogVO = null;
		if (partyVO != null) {
			Long partyLogId = partyVO .getLogId();
			List<CRSIndividualLogVO> personVOList = crsCustomerLogService.findByPartyLogId(partyLogId, false);
			if (personVOList.size() > 0) {
				personLogVO = personVOList.get(0);

				CRSIndividualLogVO clonePersonVO = new CRSIndividualLogVO();
				PropertyUtils.copyProperties(clonePersonVO, personLogVO);
				clonePersonVO.setLogId(null);
				clonePersonVO.setChangeId(newChangeId);
				clonePersonVO.setCrsPartyLog(clonePartyLogVO);
				clonePersonVO.setLastCmtFlg(CodeCst.YES_NO__NO);
				clonePersonVO = crsCustomerLogService.save(clonePersonVO, true);

				List<CRSTaxIDNumberLogVO> taxLogList = crsTaxLogService.findByPartyLogIdAndBizId(partyLogId, CRSTinBizSource.BIZ_SOURCE_PERSON, personLogVO.getLogId());
				for (CRSTaxIDNumberLogVO destVO : taxLogList) {
					CRSTaxIDNumberLogVO cloneTaxVO = new CRSTaxIDNumberLogVO();
					PropertyUtils.copyProperties(cloneTaxVO, destVO);
					cloneTaxVO.setLogId(null);
					cloneTaxVO.setChangeId(newChangeId);
					cloneTaxVO.setCrsPartyLog(clonePartyLogVO);
					cloneTaxVO.setBizId(clonePersonVO.getLogId());
					cloneTaxVO.setLastCmtFlg(CodeCst.YES_NO__NO);
					crsTaxLogService.save(cloneTaxVO, true);
				}
			}

			CRSEntityLogVO companyLogVO = null;
			List<CRSEntityLogVO> companyLogList = crsCompanyService.findByPartyLogId(partyLogId, true);
			if (companyLogList.size() > 0) {
				companyLogVO = companyLogList.get(0);

				CRSEntityLogVO cloneCompanyVO = new CRSEntityLogVO();
				PropertyUtils.copyProperties(cloneCompanyVO, companyLogVO);
				cloneCompanyVO.setLogId(null);
				cloneCompanyVO.setChangeId(newChangeId);
				cloneCompanyVO.setCrsPartyLog(clonePartyLogVO);
				cloneCompanyVO.setCrsEntityControlManListLog(new ArrayList<CRSEntityControlManListLogVO>());
				cloneCompanyVO.setLastCmtFlg(CodeCst.YES_NO__NO);
				cloneCompanyVO = crsCompanyService.save(cloneCompanyVO, true);

				List<CRSTaxIDNumberLogVO> taxLogList = crsTaxLogService.findByPartyLogIdAndBizId(partyLogId, CRSTinBizSource.BIZ_SOURCE_COMPANY, companyLogVO.getLogId());
				for (CRSTaxIDNumberLogVO destVO : taxLogList) {
					CRSTaxIDNumberLogVO cloneTaxVO = new CRSTaxIDNumberLogVO();
					PropertyUtils.copyProperties(cloneTaxVO, destVO);
					cloneTaxVO.setLogId(null);
					cloneTaxVO.setChangeId(newChangeId);
					cloneTaxVO.setCrsPartyLog(clonePartyLogVO);
					cloneTaxVO.setBizId(cloneCompanyVO.getLogId());
					cloneTaxVO.setLastCmtFlg(CodeCst.YES_NO__NO);
					cloneTaxVO = crsTaxLogService.save(cloneTaxVO, true);
				}

				List<CRSEntityControlManListLogVO> ctrlManLogList = companyLogVO.getCrsEntityControlManListLog();

				for (CRSEntityControlManListLogVO destVO : ctrlManLogList) {
					CRSEntityControlManListLogVO cloneCtrlManVO = new CRSEntityControlManListLogVO();
					PropertyUtils.copyProperties(cloneCtrlManVO, destVO);
					cloneCtrlManVO.setLogId(null);
					cloneCtrlManVO.setChangeId(newChangeId);
					cloneCtrlManVO.setEntityLogId(cloneCompanyVO.getLogId());
					cloneCtrlManVO.setCrsLogId(clonePartyLogVO.getLogId());
					cloneCtrlManVO.setLastCmtFlg(CodeCst.YES_NO__NO);
					cloneCtrlManVO = crsEntityControlManListLogService.save(cloneCtrlManVO, false);
				}
			}

			List<CRSControlManLogVO> controlVOList = crsControlManLogService.findByPartyLogId(partyLogId, true);

			for (CRSControlManLogVO destCtrlManVO : controlVOList) {

				CRSControlManLogVO cloneCtrlManVO = new CRSControlManLogVO();
				PropertyUtils.copyProperties(cloneCtrlManVO, destCtrlManVO);
				cloneCtrlManVO.setLogId(null);
				cloneCtrlManVO.setChangeId(newChangeId);
				cloneCtrlManVO.setCrsPartyLog(clonePartyLogVO);
				cloneCtrlManVO.setCrsManAccountLog(new ArrayList<CRSManAccountLogVO>());
				cloneCtrlManVO.setLastCmtFlg(CodeCst.YES_NO__NO);
				cloneCtrlManVO = crsControlManLogService.save(cloneCtrlManVO, true);

				List<CRSTaxIDNumberLogVO> taxLogList = crsTaxLogService.findByPartyLogIdAndBizId(partyLogId, CRSTinBizSource.BIZ_SOURCE_CTRL_MAN,
								destCtrlManVO.getLogId());

				for (CRSTaxIDNumberLogVO destVO : taxLogList) {
					CRSTaxIDNumberLogVO cloneTaxVO = new CRSTaxIDNumberLogVO();
					PropertyUtils.copyProperties(cloneTaxVO, destVO);
					cloneTaxVO.setLogId(null);
					cloneTaxVO.setChangeId(newChangeId);
					cloneTaxVO.setCrsPartyLog(clonePartyLogVO);
					cloneTaxVO.setBizId(cloneCtrlManVO.getLogId());
					cloneTaxVO.setLastCmtFlg(CodeCst.YES_NO__NO);
					cloneTaxVO = crsTaxLogService.save(cloneTaxVO, true);
					
				}

				List<CRSManAccountLogVO> accountLogList = destCtrlManVO.getCrsManAccountLog();

				for (CRSManAccountLogVO destVO : accountLogList) {
					CRSManAccountLogVO cloneAccountVO = new CRSManAccountLogVO();
					PropertyUtils.copyProperties(cloneAccountVO, destVO);
					cloneAccountVO.setLogId(null);
					cloneAccountVO.setChangeId(newChangeId);
					cloneAccountVO.setCrsLogId(clonePartyLogVO.getLogId());
					cloneAccountVO.setControlLogId(cloneCtrlManVO.getLogId());
					cloneAccountVO.setLastCmtFlg(CodeCst.YES_NO__NO);
					cloneAccountVO = crsControlManLogService.saveAccountLog(cloneAccountVO);
				}
			}
		}

		return clonePartyLogVO;
	}
	
	
	
	/**
	 * [330030][Lyn]e-Approval ITR-1801051_CRS共同申報及盡職審查專案- 保全變更及CRS確認申報國作業 -CRS提交檢核
	 * 檢核訊息: 檢核CRS身份類別和檢附文件是否檢附齊全，未齊全要顯示警告訊息</br>
	 * Ex : </br>
	 * 請注意：未齊全資料為自我證明表 - 法人，自我證明表 - 具控制權之人
	 * 
	 * @param logVO 		[CRSPartyLogVO]
	 * @return resultString [String]
	 */
	public String validateDocsByCrsType(CRSPartyLogVO logVO) {
		
		/* 請注意：未齊全資料為XXX（XXX由系統自動帶出，如果多個，以“，”分隔） XXX: (身份聲明書, 自我證明-個人, 自我證明-法人,自我證明-具控制權人) */
		String resultString = "";
		String crsType = NBUtils.trim(logVO.getCrsType()); // CRS 身分類別
//		String docSignStatus = NBUtils.trim(logVO.getDocSignStatus()); // 201908與User確認因為CRS聲明書一定會有，故不用再加判斷。 
		String isIndividual = NBUtils.trim(logVO.getIsIndividualReady()); // 個人-自我證明
		String isEntityReady = NBUtils.trim(logVO.getIsEntityReady()); // 法人-自我證明
		String isCtrlManReady = NBUtils.trim(logVO.getIsCtrlManReady()); // 具控制權人-自我證明

		List<String> errMsgList = new ArrayList<String>();
		String errMsgString = "請注意：未齊全資料為";
		// CRS100 || CRS 200
		if (CRSCodeType.CODE_TYPE_PERSON__A.equals(crsType) || CRSCodeType.CODE_TYPE_COMPANY__A.equals(crsType)) {
//			this.getErrorMsgByDocType(errMsgList, isIndividual, isEntityReady, isCtrlManReady, NUMBER_OF_ONE);
		}
		// CRS110
		if (CRSCodeType.CODE_TYPE_PERSON__B.equals(crsType)) {
//			this.getErrorMsgByDocType(errMsgList, isIndividual, isEntityReady, isCtrlManReady, NUMBER_OF_ONE);
			this.getErrorMsgByDocType(errMsgList, isIndividual, isEntityReady, isCtrlManReady, NUMBER_OF_TWO);
		}
		// CRS210 || CRS 230
		if (CRSCodeType.CODE_TYPE_COMPANY__B.equals(crsType) || CRSCodeType.CODE_TYPE_COMPANY__D.equals(crsType)) {
//			this.getErrorMsgByDocType(errMsgList, isIndividual, isEntityReady, isCtrlManReady, NUMBER_OF_ONE);
			this.getErrorMsgByDocType(errMsgList, isIndividual, isEntityReady, isCtrlManReady, NUMBER_OF_THREE);
			this.getErrorMsgByDocType(errMsgList, isIndividual, isEntityReady, isCtrlManReady, NUMBER_OF_FOUR);
		}
		// CRS220
		if (CRSCodeType.CODE_TYPE_COMPANY__C.equals(crsType)) {
//			this.getErrorMsgByDocType(errMsgList, isIndividual, isEntityReady, isCtrlManReady, NUMBER_OF_ONE);
			this.getErrorMsgByDocType(errMsgList, isIndividual, isEntityReady, isCtrlManReady, NUMBER_OF_THREE);
		}

		StringBuilder bld = new StringBuilder();
		if (!errMsgList.isEmpty()) {
			bld.append(errMsgString);
			String commaStr = StringResource.getStringData("MSG_103560", AppContext.getCurrentUser().getLangId()); 
			for (int i = 0; i < errMsgList.size(); i++) {
				//bld.append(this.getEbaoI18NStr(errMsgList.get(i)));
				bld.append(StringResource.getStringData(errMsgList.get(i), AppContext.getCurrentUser().getLangId()) );
				bld.append((i != (errMsgList.size() - 1)) ? commaStr : ""); 
			}
			return resultString = bld.toString();
		}
		return resultString;
	}
	
	/**
	 * PCR[350318] DEV[376275][Ian] e-Approval ITR-1903876_CRS 作業調整與通報需求(phase II) -保全變更CRS身份及保全輸入檢核</BR>
	 * 檢核當次建立CRS記錄的聲明人姓名與ID未與保單任一關係人(包含要保人、被保人、受益人、法定代理人)是否有相同
	 * @return true:有  false:無
	 * */
	public boolean checkCrsDeclarantIdAndName(CRSPartyLogVO logVO){
		//要保人
		Long policyId = policyCIImpl.getPolicyIdByPolicyCode(logVO.getPolicyCode());
		PolicyVO policyVO = paPolicyService.load(policyId);
		if(policyVO.getPolicyHolder() != null 
				&& StringUtils.equals(logVO.getName(), policyVO.getPolicyHolder().getName()) 
				&& StringUtils.equals(logVO.getCertiCode(), policyVO.getPolicyHolder().getCertiCode())){
			return true;
		}
		
		//被保人
		List<InsuredVO> insuredVOList = policyVO.getInsureds();
		if(insuredVOList != null){
			for(InsuredVO vo : insuredVOList){
				if(StringUtils.equals(logVO.getName(), vo.getName()) && StringUtils.equals(logVO.getCertiCode(), vo.getCertiCode())){
					return true;
				}
			}
		}
		
		//受益人
		List<BeneficiaryVO> beneficiaryVOList = policyVO.getBeneficiaries();
		if(beneficiaryVOList != null){
			for(BeneficiaryVO vo : beneficiaryVOList){
				if(StringUtils.equals(logVO.getName(), vo.getName()) && StringUtils.equals(logVO.getCertiCode(), vo.getCertiCode())){
					return true;
				}
			}
		}
		
		//法定代理人
		List<LegalRepresentativeVO> legalRepresentativeVOList= legalRepresentativeService.findByPolicyId(policyVO.getPolicyId());
		if(legalRepresentativeVOList != null){
			for(LegalRepresentativeVO vo : legalRepresentativeVOList){
				if(StringUtils.equals(logVO.getName(), vo.getName()) && StringUtils.equals(logVO.getCertiCode(), vo.getCertiCode())){
					return true;
				}
			}
		}
		return false;
	}
	
	/**
	 * PCR[350318] DEV[376275][Ian] e-Approval ITR-1903876_CRS 作業調整與通報需求(phase II) -保全變更CRS身份及保全輸入檢核</BR>
	 * 檢核當次建立CRS記錄的聲明人姓名與ID未與保單任一關係人(包含要保人、被保人、受益人、法定代理人)相同時 ，須顯示錯誤訊息
	 * */
	public MultiWarning validateCrsDeclarant(CRSPartyLogVO logVO) {
    	MultiWarning warning = null;
		
		if(!checkCrsDeclarantIdAndName(logVO)){
			warning = new MultiWarning();
    		warning.addWarning(StringResource.getStringData("ERR_20610021008", AppContext.getCurrentUser().getLangId()));
    		warning.setContinuable(Boolean.FALSE);
		}
		
		return warning;
	}
	
	/**
	 * 依照Type回傳對應之errMsg<br>
	 * 1.身份聲明書<br>
	 * 2.自我證明-個人<br>
	 * 3. 自我證明-法人<br>
	 * 4.自我證明-具控制權人<br>
	 * 
	 * @param errMsgList     [List<String>]
	 * @param isIndividual   [String]
	 * @param isEntityReady  [String]
	 * @param isCtrlManReady [String]
	 * @param type           [String]
	 */
	private void getErrorMsgByDocType(List<String> errMsgList, String isIndividual,
			String isEntityReady, String isCtrlManReady, int type) {

		switch (Integer.valueOf(type)) {
		case NUMBER_OF_ONE: // CRS 聲明書 ,User中期表明一定會有聲明書故不作檢核<留作註記>
//			if (!(CRSCodeCst.CRS_DOC_SIGN_STATUS__SIGNOFF.equals(docSignStatus))) {
//				errMsgList.add("身份聲明書");
//			}
//			break;
		case NUMBER_OF_TWO: // 個人-自我證明
			if (!(CodeCst.YES_NO__YES.equals(isIndividual))) {
				errMsgList.add("MSG_1263165");
			}
			break;
		case NUMBER_OF_THREE: // 法人-自我證明
			if (!(CodeCst.YES_NO__YES.equals(isEntityReady))) {
				errMsgList.add("MSG_1263184");
			}
			break;
		case NUMBER_OF_FOUR: // 具控制權人-自我證明
			if (!(CodeCst.YES_NO__YES.equals(isCtrlManReady))) {
				errMsgList.add("MSG_1263199");
			}
			break;

		default:

		}
	}

    @Override
    public List<CRSPartyLogVO> findListByDeclarationDateRangeAndCertiCode(Date date_s, Date date_e, String certiCode) {
        return convertToVOList(dao.findListByDeclarationDateRangeAndCertiCode(date_s, date_e, certiCode));
    }

    @Override
    public List<CRSPartyLogVO> findListByCertiCode(String certiCode) {
        return convertToVOList(dao.findListByDeclarationDateRangeAndCertiCode(null, null, certiCode));
    }

    @Override
    public List<CRSPartyLogVO> findListByPolicyCodeOnlyLast(String policyCode) {
        return convertToVOList(dao.findListByPolicyCode(policyCode, true));
    }

    @Override
    public void copyCRSPartyLog(CRSPartyLogVO crsPartyLogVO_from, CRSPartyLogVO crsPartyLogVO_to) {
        if (crsPartyLogVO_from != null && crsPartyLogVO_to != null) {
            Long partyLogId = crsPartyLogVO_to.getLogId();
            // 移除目的身分資料-個人
            List<CRSIndividualLogVO> personVOList = crsCustomerLogService.findByPartyLogId(partyLogId, false);
            if (CollectionUtils.isNotEmpty(personVOList)) {
                for (CRSIndividualLogVO crsIndividualLogVO : personVOList) {
                    List<CRSTaxIDNumberLogVO> taxLogList = crsTaxLogService.findByPartyLogIdAndBizId(partyLogId, CRSTinBizSource.BIZ_SOURCE_PERSON, crsIndividualLogVO.getLogId());
                    if (CollectionUtils.isNotEmpty(taxLogList)) {
                        for (CRSTaxIDNumberLogVO destVO : taxLogList) {
                            crsTaxLogService.remove(destVO.getLogId());
                        }
                    }
                    crsCustomerLogService.remove(crsIndividualLogVO.getLogId());
                }
            }
            // 移除目的身分資料-法人
            List<CRSEntityLogVO> companyLogList = crsCompanyService.findByPartyLogId(partyLogId, true);
            if (CollectionUtils.isNotEmpty(companyLogList)) {
                for (CRSEntityLogVO companyLogVO : companyLogList) {
                    List<CRSTaxIDNumberLogVO> taxLogList = crsTaxLogService.findByPartyLogIdAndBizId(partyLogId, CRSTinBizSource.BIZ_SOURCE_COMPANY, companyLogVO.getLogId());
                    if (CollectionUtils.isNotEmpty(taxLogList)) {
                        for (CRSTaxIDNumberLogVO destVO : taxLogList) {
                            crsTaxLogService.remove(destVO.getLogId());
                        }
                    }
                    crsCompanyService.remove(companyLogVO.getLogId());
                }
            }
            // 移除目的身分資料-具控制權人
            List<CRSControlManLogVO> controlVOList = crsControlManLogService.findByPartyLogId(partyLogId, true);
            if (CollectionUtils.isNotEmpty(controlVOList)) {
                for (CRSControlManLogVO destCtrlManVO : controlVOList) {
                    List<CRSTaxIDNumberLogVO> taxLogList = crsTaxLogService.findByPartyLogIdAndBizId(partyLogId, CRSTinBizSource.BIZ_SOURCE_CTRL_MAN, destCtrlManVO.getLogId());
                    if (CollectionUtils.isNotEmpty(taxLogList)) {
                        for (CRSTaxIDNumberLogVO destVO : taxLogList) {
                            crsTaxLogService.remove(destVO.getLogId());
                        }
                    }
                    crsControlManLogService.remove(destCtrlManVO.getLogId());
                }
            }
            // 複製身分證明資料-個人
            partyLogId = crsPartyLogVO_from.getLogId();
            personVOList = crsCustomerLogService.findByPartyLogId(partyLogId, false);
            if (CollectionUtils.isNotEmpty(personVOList)) {
                for (CRSIndividualLogVO crsIndividualLogVO : personVOList) {
                    CRSIndividualLogVO clonePersonVO = new CRSIndividualLogVO();
                    try {
                        BeanUtils.copyProperties(clonePersonVO, crsIndividualLogVO);
                        clonePersonVO.setLogId(null);
                        clonePersonVO.setChangeId(crsPartyLogVO_to.getChangeId());
                        clonePersonVO.setCrsPartyLog(crsPartyLogVO_to);
                        clonePersonVO = crsCustomerLogService.save(clonePersonVO, true);

                        List<CRSTaxIDNumberLogVO> taxLogList = crsTaxLogService.findByPartyLogIdAndBizId(partyLogId, CRSTinBizSource.BIZ_SOURCE_PERSON, crsIndividualLogVO.getLogId());
                        if (CollectionUtils.isNotEmpty(taxLogList)) {
                            for (CRSTaxIDNumberLogVO destVO : taxLogList) {
                                CRSTaxIDNumberLogVO cloneTaxVO = new CRSTaxIDNumberLogVO();
                                BeanUtils.copyProperties(cloneTaxVO, destVO);
                                cloneTaxVO.setLogId(null);
                                cloneTaxVO.setChangeId(crsPartyLogVO_to.getChangeId());
                                cloneTaxVO.setCrsPartyLog(crsPartyLogVO_to);
                                cloneTaxVO.setBizId(clonePersonVO.getLogId());
                                crsTaxLogService.save(cloneTaxVO, true);
                            }
                        }
                    } catch (IllegalAccessException ex) {
                        java.util.logging.Logger.getLogger(this.getClass().getName()).severe(ExceptionUtils.getStackTrace(ex));
                    } catch (InvocationTargetException ex) {
                        java.util.logging.Logger.getLogger(this.getClass().getName()).severe(ExceptionUtils.getStackTrace(ex));
                    } catch (com.ebao.pub.framework.GenericException ex) {
                        java.util.logging.Logger.getLogger(this.getClass().getName()).severe(ExceptionUtils.getStackTrace(ex));
                    }
                }
            }
            // 複製身分證明資料-法人
            companyLogList = crsCompanyService.findByPartyLogId(partyLogId, true);
            if (CollectionUtils.isNotEmpty(companyLogList)) {
                for (CRSEntityLogVO crsEntityLogVO : companyLogList) {
                    CRSEntityLogVO cloneCompanyVO = new CRSEntityLogVO();
                    try {
                        BeanUtils.copyProperties(cloneCompanyVO, crsEntityLogVO);
                        cloneCompanyVO.setLogId(null);
                        cloneCompanyVO.setChangeId(crsPartyLogVO_to.getChangeId());
                        cloneCompanyVO.setCrsPartyLog(crsPartyLogVO_to);
                        cloneCompanyVO.setCrsEntityControlManListLog(new ArrayList<CRSEntityControlManListLogVO>());
                        cloneCompanyVO = crsCompanyService.save(cloneCompanyVO, true);

                        List<CRSTaxIDNumberLogVO> taxLogList = crsTaxLogService.findByPartyLogIdAndBizId(partyLogId, CRSTinBizSource.BIZ_SOURCE_COMPANY, crsEntityLogVO.getLogId());
                        if (CollectionUtils.isNotEmpty(taxLogList)) {
                            for (CRSTaxIDNumberLogVO destVO : taxLogList) {
                                CRSTaxIDNumberLogVO cloneTaxVO = new CRSTaxIDNumberLogVO();
                                BeanUtils.copyProperties(cloneTaxVO, destVO);
                                cloneTaxVO.setLogId(null);
                                cloneTaxVO.setChangeId(crsPartyLogVO_to.getChangeId());
                                cloneTaxVO.setCrsPartyLog(crsPartyLogVO_to);
                                cloneTaxVO.setBizId(cloneCompanyVO.getLogId());
                                crsTaxLogService.save(cloneTaxVO, true);
                            }
                        }

                        List<CRSEntityControlManListLogVO> ctrlManLogList = crsEntityLogVO.getCrsEntityControlManListLog();
                        if (CollectionUtils.isNotEmpty(ctrlManLogList)) {
                            for (CRSEntityControlManListLogVO destVO : ctrlManLogList) {
                                CRSEntityControlManListLogVO cloneCtrlManVO = new CRSEntityControlManListLogVO();
                                BeanUtils.copyProperties(cloneCtrlManVO, destVO);
                                cloneCtrlManVO.setLogId(null);
                                cloneCtrlManVO.setChangeId(crsPartyLogVO_to.getChangeId());
                                cloneCtrlManVO.setEntityLogId(cloneCompanyVO.getLogId());
                                cloneCtrlManVO.setCrsLogId(crsPartyLogVO_to.getLogId());
                                crsEntityControlManListLogService.save(cloneCtrlManVO, false);
                            }
                        }
                    } catch (IllegalAccessException ex) {
                        java.util.logging.Logger.getLogger(this.getClass().getName()).severe(ExceptionUtils.getStackTrace(ex));
                    } catch (InvocationTargetException ex) {
                        java.util.logging.Logger.getLogger(this.getClass().getName()).severe(ExceptionUtils.getStackTrace(ex));
                    } catch (com.ebao.pub.framework.GenericException ex) {
                        java.util.logging.Logger.getLogger(this.getClass().getName()).severe(ExceptionUtils.getStackTrace(ex));
                    }
                }
            }
            // 複製身分證明資料-具控制權人
            controlVOList = crsControlManLogService.findByPartyLogId(partyLogId, true);
            if (CollectionUtils.isNotEmpty(controlVOList)) {
                for (CRSControlManLogVO destCtrlManVO : controlVOList) {
                    CRSControlManLogVO cloneCtrlManVO = new CRSControlManLogVO();
                    try {
                        BeanUtils.copyProperties(cloneCtrlManVO, destCtrlManVO);
                        cloneCtrlManVO.setLogId(null);
                        cloneCtrlManVO.setChangeId(crsPartyLogVO_to.getChangeId());
                        cloneCtrlManVO.setCrsPartyLog(crsPartyLogVO_to);
                        cloneCtrlManVO.setCrsManAccountLog(new ArrayList<CRSManAccountLogVO>());
                        cloneCtrlManVO = crsControlManLogService.save(cloneCtrlManVO, true);

                        List<CRSTaxIDNumberLogVO> taxLogList = crsTaxLogService.findByPartyLogIdAndBizId(partyLogId, CRSTinBizSource.BIZ_SOURCE_CTRL_MAN, destCtrlManVO.getLogId());
                        if (CollectionUtils.isNotEmpty(taxLogList)) {
                            for (CRSTaxIDNumberLogVO destVO : taxLogList) {
                                CRSTaxIDNumberLogVO cloneTaxVO = new CRSTaxIDNumberLogVO();
                                BeanUtils.copyProperties(cloneTaxVO, destVO);
                                cloneTaxVO.setLogId(null);
                                cloneTaxVO.setChangeId(crsPartyLogVO_to.getChangeId());
                                cloneTaxVO.setCrsPartyLog(crsPartyLogVO_to);
                                cloneTaxVO.setBizId(cloneCtrlManVO.getLogId());
                                crsTaxLogService.save(cloneTaxVO, true);
                            }
                        }

                        List<CRSManAccountLogVO> accountLogList = destCtrlManVO.getCrsManAccountLog();
                        if (CollectionUtils.isNotEmpty(accountLogList)) {
                            for (CRSManAccountLogVO destVO : accountLogList) {
                                CRSManAccountLogVO cloneAccountVO = new CRSManAccountLogVO();
                                BeanUtils.copyProperties(cloneAccountVO, destVO);
                                cloneAccountVO.setLogId(null);
                                cloneAccountVO.setChangeId(crsPartyLogVO_to.getChangeId());
                                cloneAccountVO.setCrsLogId(crsPartyLogVO_to.getLogId());
                                cloneAccountVO.setControlLogId(cloneCtrlManVO.getLogId());
                                crsControlManLogService.saveAccountLog(cloneAccountVO);
                            }
                        }
                    } catch (IllegalAccessException ex) {
                        java.util.logging.Logger.getLogger(this.getClass().getName()).severe(ExceptionUtils.getStackTrace(ex));
                    } catch (InvocationTargetException ex) {
                        java.util.logging.Logger.getLogger(this.getClass().getName()).severe(ExceptionUtils.getStackTrace(ex));
                    } catch (com.ebao.pub.framework.GenericException ex) {
                        java.util.logging.Logger.getLogger(this.getClass().getName()).severe(ExceptionUtils.getStackTrace(ex));
                    }
                }
            }
        }
    }

}
