package com.ebao.ls.uw.ctrl.search;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionMapping;

import com.ebao.ls.uw.ds.vo.QueryHelperVO;
import com.ebao.ls.uw.vo.UwPolicyVO;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.framework.GenericException;
import com.ebao.pub.framework.GenericForm;

/**
 * 
 * <p>Title:Underwriting</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: eBaoTech Corporation</p>
 * @author mingchun.shi
 * @since  Jun 27, 2005
 * @version 1.0
 */
public class UwPoolForm extends GenericForm {

  //  private UwPolicyPoolVO[] ppVOs;
  private UwPolicyVO[] ppVOs;

  // default page number
  protected int pageNo = 1;

  private String organIdPool;

  private String policyCodePool;

  private String submitChannelPool;

  private String applyCodePool;

  private String benefitTypePool;

  private String iacGroupPool;

  private String startTimePool;

  private String endTimePool;

  private String caseNoPool;

  private String applyStartTimePool;

  private String applyEndTimePool;

  private String uwPendingPool;

  private String uwSourceTypePool;

  private String csTransactionPool;

  private String uwStatusPool = "99";

  private int pageSize = 20;

  private String countryCode;

  private String operatorId;

  //private String queryReceivedStartTimePool;
  private String qRStartTimePool;
  //private String queryReceivedEndTimePool;
  private String qREndTimePool;
  private String scanStartDatePool;
  private String scanEndDatePool;
  private String expertDecisionPool;
  private String customerCompanyNamePool;
  private String appealIndiPool = "";

  //added by robert.xu 0n 2005.12.21
  private String serviceBranchPool;

  private String letterWaitingReply;
  /**
   * @return Returns the expertDecisionPool.
   */
  public String getExpertDecisionPool() {
    return expertDecisionPool;
  }
  /**
   * @param expertDecisionPool The expertDecisionPool to set.
   */
  public void setExpertDecisionPool(String expertDecisionPool) {
    this.expertDecisionPool = expertDecisionPool;
  }
  /**
   * @return Returns the queryReceivedEndTimePool.
   */
  public String getQueryReceivedEndTimePool() {
    return qREndTimePool;
  }
  /**
   * @param queryReceivedEndTimePool The queryReceivedEndTimePool to set.
   */
  public void setQueryReceivedEndTimePool(String queryReceivedEndTimePool) {
    this.qREndTimePool = queryReceivedEndTimePool;
  }
  /**
   * @return Returns the queryReceivedStartTimePool.
   */
  public String getQueryReceivedStartTimePool() {
    return qRStartTimePool;
  }
  /**
   * @param queryReceivedStartTimePool The queryReceivedStartTimePool to set.
   */
  public void setQueryReceivedStartTimePool(String queryReceivedStartTimePool) {
    this.qRStartTimePool = queryReceivedStartTimePool;
  }
  /**
   * @return Returns the scanEndDatePool.
   */
  public String getScanEndDatePool() {
    return scanEndDatePool;
  }
  /**
   * @param scanEndDatePool The scanEndDatePool to set.
   */
  public void setScanEndDatePool(String scanEndDatePool) {
    this.scanEndDatePool = scanEndDatePool;
  }
  /**
   * @return Returns the scanStartDatePool.
   */
  public String getScanStartDatePool() {
    return scanStartDatePool;
  }
  /**
   * @param scanStartDatePool The scanStartDatePool to set.
   */
  public void setScanStartDatePool(String scanStartDatePool) {
    this.scanStartDatePool = scanStartDatePool;
  }

  public String getOrganIdPool() {
    return organIdPool;
  }

  public void setOrganIdPool(String organId) {
    this.organIdPool = organId;
  }

  public String getUwStatusPool() {
    return uwStatusPool;
  }

  public void setUwStatusPool(String uwStatusPool) {
    this.uwStatusPool = uwStatusPool;
  }

  public String getApplyCodePool() {
    return applyCodePool;
  }

  public void setApplyCodePool(String applyCodePool) {
    this.applyCodePool = applyCodePool;
  }

  public UwPolicyVO[] getppVOs() {
    return ppVOs;
  }

  public void setppVOs(UwPolicyVO[] ppVOs) {
    this.ppVOs = ppVOs;
  }

  public int getPageNo() {
    return pageNo;
  }

  public void setPageNo(int pageNo) {
    this.pageNo = pageNo;
  }

  public String getUwSourceTypePool() {
    return uwSourceTypePool;
  }

  public void setUwSourceTypePool(String uwSourceTypePool) {
    this.uwSourceTypePool = uwSourceTypePool;
  }

  public String getCaseNoPool() {
    return caseNoPool;
  }

  public void setCaseNoPool(String caseNo) {
    this.caseNoPool = caseNo;
  }

  public String getStartTimePool() {
    return startTimePool;
  }

  public void setStartTimePool(String startTime) {
    this.startTimePool = startTime;
  }

  public String getEndTimePool() {
    return endTimePool;
  }

  public void setEndTimePool(String endTime) {
    this.endTimePool = endTime;
  }

  public String getApplyStartTimePool() {
    return applyStartTimePool;
  }

  public void setApplyStartTimePool(String applyStartTime) {
    this.applyStartTimePool = applyStartTime;
  }

  public String getApplyEndTimePool() {
    return applyEndTimePool;
  }

  public void setApplyEndTimePool(String applyEndTime) {
    this.applyEndTimePool = applyEndTime;
  }

  public String getCsTransactionPool() {
    return csTransactionPool;
  }

  public void setCsTransactionPool(String csTransaction) {
    this.csTransactionPool = csTransaction;
  }

  public String getPolicyCodePool() {
    return policyCodePool;
  }

  public void setPolicyCodePool(String policyCodePool) {
    this.policyCodePool = policyCodePool;
  }

  public int getPageSize() {
    return pageSize;
  }

  public void setPageSize(int pageSize) {
    this.pageSize = pageSize;
  }

  public String getUwPendingPool() {
    return uwPendingPool;
  }

  public void setUwPendingPool(String uwPending) {
    this.uwPendingPool = uwPending;
  }

  public String getBenefitTypePool() {
    return benefitTypePool;
  }

  public void setBenefitTypePool(String benefitType) {
    this.benefitTypePool = benefitType;
  }

  public String getSubmitChannelPool() {
    return submitChannelPool;
  }

  public void setSubmitChannelPool(String submitChannel) {
    this.submitChannelPool = submitChannel;
  }

  public String getIacGroupPool() {
    return iacGroupPool;
  }

  public void setIacGroupPool(String iacGroup) {
    this.iacGroupPool = iacGroup;
  }

  public String getOperatorId() {
    return operatorId;
  }

  public void setOperatorId(String operatorId) {
    this.operatorId = operatorId;
  }

  public String getCountryCode() {
    return countryCode;
  }

  public void setCountryCode(String countryCode) {
    this.countryCode = countryCode;
  }

  private boolean isValueValidate(String value) {
    return !(value == null || "".equals(value.trim()));
  }

  /**
   * returns the value of the UwPolicy
   * @param value
   * @throws GenericException
   */
  public void populateUwPolicyValue(QueryHelperVO value)
      throws GenericException {

    value.setCustomerCompanyName(AppContext.getCurrentCountry());
    if (isValueValidate(appealIndiPool)) {
      value.setAppealIndi(appealIndiPool);
    }
    if (isValueValidate(qRStartTimePool)) {
      value.setQueryReceivedStartTime(qRStartTimePool);
    }
    if (isValueValidate(qREndTimePool)) {
      value.setQueryReceivedEndTime(qREndTimePool);
    }
    if (isValueValidate(scanStartDatePool)) {
      value.setScanStartDate(scanStartDatePool);
    }
    if (isValueValidate(scanEndDatePool)) {
      value.setScanEndDate(scanEndDatePool);
    }
    if (isValueValidate(expertDecisionPool)) {
      value.setExpertDecision(expertDecisionPool);
    }

    if (isValueValidate(uwSourceTypePool)) {
      value.setUwSourceType(uwSourceTypePool);
    }

    if (isValueValidate(uwStatusPool)) {
      value.setUwStatus(uwStatusPool);
    }

    if (isValueValidate(policyCodePool)) {
      value.setPolicyCode(policyCodePool);
    }

    if (isValueValidate(applyCodePool)) {
      value.setApplyCode(applyCodePool);
    }

    if (isValueValidate(submitChannelPool)) {
      value.setSubmitChannel(submitChannelPool);
    }

    if (isValueValidate(caseNoPool)) {
      value.setCaseNo(caseNoPool);
    }

    if (isValueValidate(iacGroupPool)) {
      value.setIacGroup(iacGroupPool);
    }

    if (isValueValidate(benefitTypePool)) {
      value.setBenefitType(benefitTypePool);
    }

    if (isValueValidate(startTimePool)) {
      value.setStartTime(startTimePool);
    }

    if (isValueValidate(endTimePool)) {
      value.setEndTime(endTimePool);
    }

    if (isValueValidate(applyStartTimePool)) {
      value.setApplyStartTime(applyStartTimePool);
    }

    if (isValueValidate(applyEndTimePool)) {
      value.setApplyEndTime(applyEndTimePool);
    }

    if (isValueValidate(uwPendingPool)) {
      value.setUwPending(Long.valueOf(uwPendingPool));
    }

    value.setPageNo(pageNo == 0 ? 1 : pageNo);
    value.setPageSize(20);

    if (isValueValidate(organIdPool)) {
      value.setOrganId(Long.valueOf(organIdPool));
    }

    value.setOperatorId(Long.valueOf(AppContext.getCurrentUser().getUserId()));

    if (isValueValidate(csTransactionPool)) {
      value.setCsTransaction(Integer.valueOf(csTransactionPool));
    }

    //added for service branch by robert.xu on 2005.12.21
    if (isValueValidate(serviceBranchPool)) {
      value.setServiceBranch(serviceBranchPool);
    }

    if (isValueValidate(letterWaitingReply)) {
      value.setLetterWaitingReply(letterWaitingReply);
    }
  }

  /**
   * @return Returns the appealIndiPool.
   */
  public String getAppealIndiPool() {
    return appealIndiPool;
  }
  /**
   * @param appealIndiPool The appealIndiPool to set.
   */
  public void setAppealIndiPool(String appealIndiPool) {
    this.appealIndiPool = appealIndiPool;
  }
  /**
   * @return Returns the customerCompanyNamePool.
   */
  public String getCustomerCompanyNamePool() {
    return customerCompanyNamePool;
  }
  /**
   * @param customerCompanyNamePool The customerCompanyNamePool to set.
   */
  public void setCustomerCompanyNamePool(String customerCompanyNamePool) {
    this.customerCompanyNamePool = customerCompanyNamePool;
  }
  //added by jason.luo 10.27.2004
  public void reset(ActionMapping mapping, HttpServletRequest request) {
    organIdPool = "";

    policyCodePool = "";
    submitChannelPool = "";
    applyCodePool = "";
    benefitTypePool = "";
    iacGroupPool = "";

    //willis, to initialize all the feilds. No one exclude. 
    //Defect:[GEL00040147]
    //2008-03-04
    //Start........................
    qRStartTimePool = "";
    qREndTimePool = "";
    serviceBranchPool = "";
    scanStartDatePool = "";
    scanEndDatePool = "";
    appealIndiPool = "";
    customerCompanyNamePool = "";
    //End..........................
    startTimePool = "";
    endTimePool = "";
    caseNoPool = "";
    applyStartTimePool = "";
    applyEndTimePool = "";
    uwPendingPool = "";
    uwSourceTypePool = "";
    csTransactionPool = "";
    uwStatusPool = "99";
    countryCode = "";
    operatorId = "";
    expertDecisionPool = "";
    letterWaitingReply = "";
    this.pageNo = 1;

  }
  /**
   * @param serviceBranchPool The serviceBranchPool to set.
   */
  public void setServiceBranchPool(String serviceBranchPool) {
    this.serviceBranchPool = serviceBranchPool;
  }
  /**
   * @return Returns the serviceBranchPool.
   */
  public String getServiceBranchPool() {
    return serviceBranchPool;
  }
  public void setLetterWaitingReply(String letterWaitingReply) {
    this.letterWaitingReply = letterWaitingReply;
  }
  public String getLetterWaitingReply() {
    return letterWaitingReply;
  }
}
