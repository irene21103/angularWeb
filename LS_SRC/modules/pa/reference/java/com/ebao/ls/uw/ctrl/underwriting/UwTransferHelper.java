package com.ebao.ls.uw.ctrl.underwriting;

import com.ebao.foundation.app.sys.sysmgmt.ds.SysMgmtDSLocator;
import com.ebao.foundation.app.sys.sysmgmt.ds.UserDS;
import com.ebao.foundation.app.sys.sysmgmt.ds.UserVO;
import com.ebao.foundation.common.context.AppUserContext;
import com.ebao.foundation.module.db.hibernate.HibernateSession3;
import com.ebao.foundation.module.web.action.ExceptionInfoUtils;
import com.ebao.ls.cs.commonflow.ds.ApplicationService;
import com.ebao.ls.cs.commonflow.ds.task.CsTaskTransService;
import com.ebao.ls.cs.logger.ApplicationLogger;
import com.ebao.ls.leave.service.UserLeaveManagerService;
import com.ebao.ls.pa.nb.bs.ProposalProcessService;
import com.ebao.ls.pa.nb.bs.rule.ProposalRuleResultService;
import com.ebao.ls.pa.nb.ci.ValidatorService;
import com.ebao.ls.pa.nb.rule.ValidatorUtils;
import com.ebao.ls.pa.nb.util.NBUtils;
import com.ebao.ls.pa.nb.vo.ProposalRuleResultVO;
import com.ebao.ls.pa.nb.vo.UnbRoleVO;
import com.ebao.ls.pa.pub.bs.AddressService;
import com.ebao.ls.pa.pub.bs.BeneficiaryService;
import com.ebao.ls.pa.pub.bs.NbSQLOnlyService;
import com.ebao.ls.pa.pub.bs.PolicyService;
import com.ebao.ls.pa.pub.bs.UnbAmlCompanyService;
import com.ebao.ls.pa.pub.data.bo.Policy;
import com.ebao.ls.pa.pub.data.org.PolicyDao;
import com.ebao.ls.pa.pub.vo.BeneficiaryVO;
import com.ebao.ls.pa.pub.vo.CoverageVO;
import com.ebao.ls.pa.pub.vo.PolicyHolderVO;
import com.ebao.ls.pa.pub.vo.PolicyVO;
import com.ebao.ls.pa.pub.vo.UnbAmlCompanyVO;
import com.ebao.ls.product.model.query.output.ProdBizCategory;
import com.ebao.ls.product.model.query.output.ProdBizCategorySub;
import com.ebao.ls.product.service.LifeProductCategoryService;
import com.ebao.ls.product.service.LifeProductService;
import com.ebao.ls.pty.ds.DeptService;
import com.ebao.ls.pub.CodeCst;
import com.ebao.ls.pub.cache.ActionDataCache;
import com.ebao.ls.pub.cache.ActionDataCacheKey;
import com.ebao.ls.pub.cst.ProposalStatus;
import com.ebao.ls.pub.cst.UwDocumentApprovalStatus;
import com.ebao.ls.pub.cst.UwTransferFlowType;
import com.ebao.ls.pub.cst.YesNo;
import com.ebao.ls.pub.task.bs.TaskAssignTransService.TaskName;
import com.ebao.ls.riskAggregation.bs.RiskAggregationService;
import com.ebao.ls.riskPrevention.vo.RiskCustomerQueryVO;
import com.ebao.ls.uw.ctrl.pub.UwPolicyForm;
import com.ebao.ls.uw.ds.UwPolicyService;
import com.ebao.ls.uw.ds.UwProcessService;
import com.ebao.ls.uw.ds.UwTransferService;
import com.ebao.ls.uw.ds.constant.ProposalStatusConstants;
import com.ebao.ls.uw.ds.constant.UwStatusConstants;
import com.ebao.ls.uw.ds.vo.LeaveAuthEdit;
import com.ebao.ls.uw.esc.role.TransferLevelTypeValidate;
import com.ebao.ls.uw.esc.role.UwTransferValidateInput;
import com.ebao.ls.uw.util.Utils;
import com.ebao.ls.uw.vo.UwExtraLoadingVO;
import com.ebao.ls.uw.vo.UwLifeInsuredVO;
import com.ebao.ls.uw.vo.UwPolicyVO;
import com.ebao.ls.uw.vo.UwTransferLevelLimitVO;
import com.ebao.ls.uw.vo.UwTransferRoleLimitVO;
import com.ebao.ls.uw.vo.UwTransferRoleVO;
import com.ebao.ls.uw.vo.UwTransferVO;
import com.ebao.ls.ws.ci.aml.AmlQueryCI;
import com.ebao.ls.ws.ci.aml.civo.FircoRsDtaCIVO;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.framework.AppException;
import com.ebao.pub.framework.GenericException;
import com.ebao.pub.i18n.lang.StringResource;
import com.ebao.pub.util.StringUtils;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.collections.PredicateUtils;
import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.hibernate.Session;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static com.ebao.ls.cs.logger.ApplicationLogger.addLoggerData;
import static com.ebao.ls.product.model.query.output.ProdBizCategory.MAIN_02;
import static com.ebao.ls.pub.cst.UwSourceType.UW_SOURCE_TYPE__NB;
import static com.ebao.ls.pub.cst.UwTransferFlowType.UW_TRANS_FLOW_EXTRA_PREM_MANAGER;
import static com.ebao.ls.pub.cst.UwTransferFlowType.UW_TRANS_FLOW_FIRSTER;
import static com.ebao.ls.pub.cst.UwTransferFlowType.UW_TRANS_FLOW_MANAGER;
import static com.ebao.ls.pub.cst.UwTransferLimitType.UW_LIMIT_TYPE_1;
import static com.ebao.ls.pub.cst.UwTransferLimitType.UW_LIMIT_TYPE_2;
import static com.ebao.ls.pub.cst.UwTransferLimitType.UW_LIMIT_TYPE_3;
import static com.ebao.ls.pub.cst.UwTransferLimitType.UW_LIMIT_TYPE_4;
import static com.ebao.ls.pub.cst.UwTransferLimitType.UW_LIMIT_TYPE_7;
import static com.ebao.ls.pub.cst.UwTransferLimitType.UW_LIMIT_TYPE_N1;
import static com.ebao.ls.pub.cst.VerifactionManagerType.VERIFACTION_MANAGER_TYPE_1;
import static com.ebao.ls.pub.cst.VerifactionManagerType.VERIFACTION_MANAGER_TYPE_10;
import static com.ebao.ls.pub.cst.VerifactionManagerType.VERIFACTION_MANAGER_TYPE_11;
import static com.ebao.ls.pub.cst.VerifactionManagerType.VERIFACTION_MANAGER_TYPE_12;
import static com.ebao.ls.pub.cst.VerifactionManagerType.VERIFACTION_MANAGER_TYPE_8;
import static com.ebao.ls.pub.cst.YesNo.YES_NO__NO;
import static com.ebao.ls.uw.ds.constant.ProposalStatusConstants.DECLINED;
import static com.ebao.ls.uw.ds.constant.ProposalStatusConstants.POSTPONED;
import static java.lang.String.format;
import static java.util.stream.Collectors.joining;

public class UwTransferHelper {
    public static final String BEAN_DEFAULT = "uwTransferHelper";

    private static final Logger logger = LogManager.getFormatterLogger();
    
    @Resource(name = UnbAmlCompanyService.BEAN_DEFAULT)
    protected UnbAmlCompanyService paUnbAmlCompanyService;

    @Resource(name = AddressService.BEAN_DEFAULT)
    private AddressService paAddressService;

    @Resource(name = UwTransferService.BEAN_DEFAULT)
    private UwTransferService uwTransferService;

    @Resource(name = UwPolicyService.BEAN_DEFAULT)
    private UwPolicyService uwPolicyDS;

    @Resource(name = ProposalRuleResultService.BEAN_DEFAULT)
    private ProposalRuleResultService proposalRuleResultService;

    @Resource(name = UwProcessService.BEAN_DEFAULT)
    private UwProcessService uwProcessDS;

    @Resource(name = ProposalProcessService.BEAN_DEFAULT)
    private ProposalProcessService proposalProcessService;

    @Resource(name = CsTaskTransService.BEAN_DEFAULT)
    private CsTaskTransService csTaskTransService;

    @Resource(name = RiskAggregationService.BEAN_DEFAULT)
    private RiskAggregationService raService;

    @Resource(name = PolicyDao.BEAN_DEFAULT)
    private PolicyDao<Policy> policyDao;

    @Resource(name = TransferLevelTypeValidate.BEAN_DEFAULT)
    private TransferLevelTypeValidate transferLevelTypeValidate;

    @Resource(name = UserLeaveManagerService.BEAN_DEFAULT)
    private UserLeaveManagerService leaveManagerService;

    @Resource(name = DeptService.BEAN_DEFAULT)
    private DeptService deptDS;

    @Resource(name = AmlQueryCI.BEAN_DEFAULT)
    private AmlQueryCI amlQueryCI;

    @Resource(name = PolicyService.BEAN_DEFAULT)
    private PolicyService paPolicyService;

    @Resource(name = BeneficiaryService.BEAN_DEFAULT)
    private BeneficiaryService beneficiaryService;

    @Resource(name = LifeProductCategoryService.BEAN_DEFAULT)
    protected LifeProductCategoryService lifeProductCategoryService;

    @Resource(name = LifeProductService.BEAN_DEFAULT)
    protected LifeProductService lifeProductService;
    
    @Resource(name = ApplicationService.BEAN_DEFAULT)
    private ApplicationService applicationService;
    
    @Resource(name = com.ebao.ls.pa.pub.bs.PolicyService.BEAN_DEFAULT)
    private com.ebao.ls.pa.pub.bs.PolicyService policyDS;
    
    @Resource(name = ValidatorService.BEAN_DEFAULT)
    ValidatorService paValidatorService;

    @Resource(name = NbSQLOnlyService.BEAN_DEFAULT)
    private NbSQLOnlyService paNbSQLOnlyService;

    /**
     * <p>Description : 依陳核記錄表，決定執行核保作業人員角色：
     * <p>Created By : simon.huang</p>
     * <p>Create Time : Dec 22, 2016</p>
     * @param request
     * @param uwPolicyForm
     * @param uwPolicyVO
     * @param isIframe
     */
    public void defineUwTransferRole(UwPolicyForm uwPolicyForm, UwPolicyVO uwPolicyVO, String isIframe) {

        Session s = HibernateSession3.currentSession();

        String sourceType = uwPolicyVO.getUwSourceType();
        Long underwriteId = uwPolicyVO.getUnderwriteId();
        Long policyId = uwPolicyVO.getPolicyId();
        Long userId = AppContext.getCurrentUser().getUserId();
        boolean checkIsIframe = "true".equals(isIframe);

        UwTransferVO firstlyVO = uwTransferService.findFirstlyUwTransfer(underwriteId);
        if(checkIsIframe == false){ //綜合查詢不執行
        	if (firstlyVO == null) {
        		firstlyVO = uwTransferService.createFirstlyUwTransfer(policyId, underwriteId);

        		/* 需執行flush,findLastestUwTransfer() 才能抓到第一次進入時 firstlyVO 寫入的資料 */
        		s.flush();
        	}

        	if (uwPolicyVO.getUnderwriterId() == null
        			|| firstlyVO.getTransUnderwriterId().equals(uwPolicyVO.getUnderwriterId()) == false) {
        		/* 更新初始核保人員ID */
        		uwPolicyVO.setUnderwriterId(firstlyVO.getTransUnderwriterId());
        		UserVO userVO = SysMgmtDSLocator.getUserDS().getUserByUserID(firstlyVO.getTransUnderwriterId());
        		//PCR-463250 綜合查詢-新契約資訊要新增顯示核保單位 2023/01/19 Add by Kathy
        		uwPolicyVO.setUwDeptId(Long.parseLong(userVO == null ? null : userVO.getDeptId()));
        		uwPolicyDS.updateUwPolicy(uwPolicyVO, false);
        	}

        	/* 舊資料未設定，需使用UwTransfer 來決定1或4*/
        	if (!checkIsIframe && uwPolicyVO.getUwTransferFlow() == null) {
        		//
        		if (firstlyVO.getInitUnderwriterId().equals(userId)) {
        			uwPolicyVO.setUwTransferFlow(CodeCst.UW_TRANS_FLOW_FIRSTER);
        		} else {
        			// 取得最後一筆陳核記錄
        			UwTransferVO lastestVO = uwTransferService.findLastestWorkUwTransfer(underwriteId);
        			if (CodeCst.UW_TRANS_FLOW_ESCALATE_RESPONSE == lastestVO.getFlowId()
        					&& lastestVO.getTransUnderwriterId().equals(firstlyVO.getInitUnderwriterId())) {
        				uwPolicyVO.setUwTransferFlow(CodeCst.UW_TRANS_FLOW_FIRSTER);
        			} else {
        				uwPolicyVO.setUwTransferFlow(CodeCst.UW_TRANS_FLOW_MANAGER);
        			}
        		}
        		uwPolicyDS.updateUwPolicy(uwPolicyVO, false);
        	}
        }

        boolean isUnderwriter = false; // 核保員
        boolean isManualEscaUser = false; // 人工陳核回覆人員
        boolean isUwManager = false; // 核保簽核主管
        boolean isExtraPremManager = false; // 加費簽核主管

        if (!Utils.isCsUw(sourceType)) {
            if (!checkIsIframe) { // 不為綜合查詢查看核保作業唯讀的情況

                // 取得最後一筆陳核記錄(lastestVO正常一定會有一筆初始核保員的記錄)
            	UwTransferVO lastestVO = uwTransferService.findLastestWorkUwTransfer(underwriteId);

            	//RTC 155704 FIX 流程至人工陳核回覆，多寫了人工派工記錄，並將初始核保員更新為人工陳核回覆人員
            	//所以要判斷為flow_first也要判斷不為人工陳核回覆
                if (!CodeCst.YES_NO__YES.equals(uwPolicyVO.getManuEscIndi())) { // 不為人工陳核
                		
                    if(CodeCst.UW_TRANS_FLOW_ROLE_FINISH == uwPolicyVO.getUwTransferFlow()) {
                		//若是執行核保決定因inforce PL/SQL拋錯會造成寫入核保完成記錄,增加相容錯誤邏輯處理
                		if(lastestVO.getFlowId() == CodeCst.UW_TRANS_FLOW_FIRSTER) {
                			uwPolicyVO.setUwTransferFlow(CodeCst.UW_TRANS_FLOW_FIRSTER);
                		} else if(lastestVO.getFlowId() == CodeCst.UW_TRANS_FLOW_MANAGER) {
                			uwPolicyVO.setUwTransferFlow(CodeCst.UW_TRANS_FLOW_MANAGER);
                		} else {
                			uwPolicyVO.setUwTransferFlow(CodeCst.UW_TRANS_FLOW_FIRSTER);
                		}
                	}
                }

                // 依uwPolicyVO.uwTransferFlow決定頁面角色,有會以下的情況
                // 1, 核保員
                // 4, 核保簽核主管
                // 5, 加費簽核主管
                Integer uwTransferflowType = uwPolicyVO.getUwTransferFlow();
                if (lastestVO.isResponseOpen()) {
                    isManualEscaUser = true; // 人工陳核回覆人員
                } else if (uwTransferflowType == CodeCst.UW_TRANS_FLOW_FIRSTER) {
                    isUnderwriter = true; // 核保員

                    UwTransferVO completeVO = uwTransferService.findLastestCompleteUwTransfer(underwriteId);

                    /* 判斷是否為 加費/批註同意，需調整頁面主管同意下拉選單的文字  */
                    if (completeVO.getFlowId() == CodeCst.UW_TRANS_FLOW_EXTRA_ALL_APPROVAL) {
                        uwPolicyForm.setIsExtraPremApproval(true);
                        uwPolicyForm.setIsExculsionApproval(true);
                    } else if (completeVO.getFlowId() == CodeCst.UW_TRANS_FLOW_EXCLUSION_APPROVAL) {
                        uwPolicyForm.setIsExtraPremApproval(false);
                        uwPolicyForm.setIsExculsionApproval(true);
                    } else if (completeVO.getFlowId() == CodeCst.UW_TRANS_FLOW_EXTRA_PREM_APPROVAL) {
                        uwPolicyForm.setIsExtraPremApproval(true);
                        uwPolicyForm.setIsExculsionApproval(false);
                    } else {
                        uwPolicyForm.setIsExtraPremApproval(false);
                        uwPolicyForm.setIsExculsionApproval(false);
                    }

                } else if (uwTransferflowType == CodeCst.UW_TRANS_FLOW_MANAGER) {
                    isUwManager = true; // 核保簽核主管
                } else if (uwTransferflowType == CodeCst.UW_TRANS_FLOW_EXTRA_PREM_MANAGER) {
                    isExtraPremManager = true; // 加費簽核主管
                    // 決定簽核加費or批註
                    List<ProposalRuleResultVO> needAgreeList = proposalRuleResultService.findNeedApprovalRuleResults(policyId,
                                    UwDocumentApprovalStatus.APPROVAL_TRANSFER);
                    Map<String, List<ProposalRuleResultVO>> ruleNameBindList = NBUtils.toMapList(needAgreeList, "ruleName");
                    boolean isExtraPremApproval = false;
                    boolean isExculsionApproval = false;
                    if (ruleNameBindList.containsKey(CodeCst.PROPOSAL_RULE_MSG_EXTRA_PREM_LETTER)) {
                        isExtraPremApproval = true;
                    }
                    if (ruleNameBindList.containsKey(CodeCst.PROPOSAL_RULE_MSG_EXCLUSION_LETTER)) {
                        isExculsionApproval = true;
                    }
                    uwPolicyForm.setIsExtraPremApproval(isExtraPremApproval);
                    uwPolicyForm.setIsExculsionApproval(isExculsionApproval);
                }

                /* 顯示上次核保人員*/
                if (isManualEscaUser) {
                    // 人工陳核流程，上次核保員顯示人工陳核發起者
                    uwPolicyForm.setPrevUnderwriterId(String.valueOf(lastestVO.getTransUnderwriterId()));
                } else if (isUnderwriter) {
                    // 初始核保員，上次核保員顯示自己
                    uwPolicyForm.setPrevUnderwriterId(String.valueOf(userId));
                } else {
                    // 覆核/主管，上次核保員顯示陳核記錄發起者
                    uwPolicyForm.setPrevUnderwriterId(String.valueOf(lastestVO.getInitUnderwriterId()));
                }
            }

        } else {
            // 保全來源均當作是核保員
            isUnderwriter = true;
            // 上次核保員顯示初始核保員
            if(firstlyVO != null){
            	uwPolicyForm.setPrevUnderwriterId(String.valueOf(firstlyVO.getTransUnderwriterId()));	
            } else {
            	uwPolicyForm.setUnderwriterId(uwPolicyVO.getUnderwriterId());
            }
        }

        uwPolicyForm.setIsUnderwriter(isUnderwriter);
        uwPolicyForm.setIsEscaUser(isManualEscaUser);
        uwPolicyForm.setIsManager(isUwManager);
        uwPolicyForm.setIsExtraPremManager(isExtraPremManager);

        // 初始核保員
        if(firstlyVO != null){
        	uwPolicyForm.setUnderwriterId(firstlyVO.getTransUnderwriterId());	
        } else {
        	uwPolicyForm.setUnderwriterId(uwPolicyVO.getUnderwriterId());
        }
        
    }

    /**<pre>
     * Description : 設定陳核相關資料
     * 角色分為:
     * 1.初始核保員  - 僅 不可編輯 陳核說明+覆核/主管核保意見(文字)+主管簽核意見(Y/N)
     * 2.核保員陳核對象 - 僅可編輯陳核說明
     * 3.核保員覆核主管 - 僅可編輯覆核/主管核保意見(文字)+主管簽核意見(Y/N)
     * 4.核保員授權額度上級主管 - 僅可編輯覆核/主管核保意見(文字)+主管簽核意見(Y/N)
     * </pre>
     * <p>Created By : simon.huang</p>
     * <p>Create Time : Mar 27, 2016</p>
     * @param request
     * @param uwPolicyForm
     * @param underwriteId
     */
    public void getDisplayUwComment(UwPolicyForm uwPolicyForm, Long underwriteId) {

        //List<UwTransferVO> transHisotryList = uwTransferService.findCompleteHistory(underwriteId);
		//陳核備註說明,flow_id=2
        String historyEscaComment = uwPolicyDS.getUwEscaComments(underwriteId); 
        //覆核主管核保意見,flow_id=3,4,5
        String historyUwComment = uwPolicyDS.getUwManagerComments(underwriteId); 

        uwPolicyForm.setHistoryEscaComment(historyEscaComment);
        uwPolicyForm.setHistoryUwComment(historyUwComment);
    }

    /**
     * 取得最後覆核人員資訊(邏輯同契審表)
     *
     * @param uwPolicyForm
     * @param policyId 保單ID
     */
    public void getLastUnderwriterInfo(UwPolicyForm uwPolicyForm, Long policyId) {
        uwPolicyForm.setLastUnderwriterId(null);
        uwPolicyForm.setLastUnderwriterTime(null);
        // 核保主頁面的最後覆核人員、日期要顯示契審表上的最後一筆
        List<UwTransferVO> uwTransferVOs = uwTransferService.getUwTransferByUNB0631Condition(policyId);
        for (UwTransferVO uwTransferVO : uwTransferVOs) {
            uwPolicyForm.setLastUnderwriterTime(uwTransferVO.getSubmitTime());
            if (UwTransferFlowType.UW_TRANS_FLOW_ESCALATE_RESPONSE == uwTransferVO.getFlowId()) {
                uwPolicyForm.setLastUnderwriterId(String.valueOf(uwTransferVO.getInitUnderwriterId()));
            } else {
                uwPolicyForm.setLastUnderwriterId(String.valueOf(uwTransferVO.getTransUnderwriterId()));
            }
        }
    }

    /**
     * <p>Description : 是否執行核保完成,若需向上轉呈則自動reassign task，並回傳false</p>
     * <p>Created By : simon.huang</p>
     * <p>Create Time : Feb 23, 2017</p>
     * @param request
     * @param uwPolicyVO
     * @param uwSourceType
     * @return
     */
    public boolean completeCurrentUwTransfer(HttpServletRequest request, UwPolicyVO uwPolicyVO, String uwSourceType) {
        ApplicationLogger.addLoggerData("UwTransferHelper, completeCurrentUwTransfer : underwriteId=" + uwPolicyVO.getUnderwriteId() + ", uwSourceType=" + uwSourceType + ", manuEscIndi=" + uwPolicyVO.getManuEscIndi());
        Long currUser = AppContext.getCurrentUser().getUserId();

        Long underwriteId = uwPolicyVO.getUnderwriteId();
        UwTransferVO lastestVO = uwTransferService.findLastestWorkUwTransfer(underwriteId);
        UwTransferVO firstlyVO = uwTransferService.findFirstlyUwTransfer(underwriteId);

        /* 依陳核角色保存頁面說明 */
        logger.info("uwPolicyVO.getManuEscIndi():" + uwPolicyVO.getManuEscIndi());
        // 不為人工陳核,依職級額度設定判斷是否需向上陳核
        if (!CodeCst.YES_NO__YES.equals(uwPolicyVO.getManuEscIndi())) {

            // 區分核保簽核或加費/批註簽核
            Integer uwPolicyTransferFlow = uwPolicyVO.getUwTransferFlow();
            // 表示為主管簽核流程，判斷主管簽核狀態是否同意/不同意
            String approvalIndi = uwPolicyVO.getUwCommentIndi();

            boolean isExtraPremManager = (CodeCst.UW_TRANS_FLOW_EXTRA_PREM_MANAGER == uwPolicyTransferFlow);
            boolean isUWManager = (CodeCst.UW_TRANS_FLOW_MANAGER == uwPolicyTransferFlow);
            // boolean isUnderwriter = (CodeCst.UW_TRANS_FLOW_FIRSTER ==
            // uwPolicyTransferFlow);
            logger.info("isExtraPremManager:" + isExtraPremManager + ", isUWManager:" + isUWManager);
            ApplicationLogger.addLoggerData("uwPolicyTransferFlow=" + uwPolicyTransferFlow + ", approvalIndi=" + approvalIndi + ", isExtraPremManager=" + isExtraPremManager + ", isUWManager=" + isUWManager);
            if (isExtraPremManager || isUWManager) {

                //記錄覆核/主管核保意見
                if (StringUtils.isNullOrEmpty(uwPolicyVO.getUwNotes()) == false
                        || org.apache.commons.lang3.StringUtils.isNotBlank(approvalIndi)) {
                    lastestVO.setTransComment(uwPolicyVO.getUwNotes());
                    lastestVO.setApproveStatus(approvalIndi);
                    uwTransferService.save(lastestVO);
                }

                /* 簽核不同意 流程 */
                if (CodeCst.YES_NO__NO.equals(approvalIndi)) {
                    /* 返回核保員 */
                    doUwTransferProposeDesc(uwPolicyVO, firstlyVO.getInitUnderwriterId()); //需記錄陳核備註
                    doUwTransferReject(request, uwPolicyVO, firstlyVO);
                    closeLastestUwTransfer(request, uwPolicyVO, lastestVO);

                    return false;
                }

            }

            Long transerUserId = null;
            UwTransferRoleVO roleVO = uwTransferService.findUwTransferRoleByUser(currUser);
            ApplicationLogger.addLoggerData("findUwTransferRoleByUser ? " + (roleVO != null));

            boolean checkTransfer = true;// PCR-480318 修改程序，預設都要執行(true)，若已完成核保紙本簽核作業(直接核決)=false
            if (roleVO != null && CodeCst.UW_SOURCE_TYPE__NB.equals(uwSourceType)
                    && uwTransferService.getUwTransferRoleBO()
                            .isGreateEquals(roleVO.getRoleId(), CodeCst.UW_ESC_ROLE_G11, false)) {
                // PCR-480318 新契約經理、專案經理G11(含)以上分兩種 轉陳覆核主管、已完成核保紙本簽核作業(直接核決)
                if (Boolean.parseBoolean(request.getParameter("uw_warning_transfer"))) {
                    transerUserId = getTransferUser(currUser, uwSourceType);// 找出要轉陳覆核主管
                    if (null == transerUserId) {
                        return false;
                    }
                }
                //已完成核保紙本簽核作業(直接核決)
                if (Boolean.parseBoolean(request.getParameter("uw_warning_approve"))) {
                    checkTransfer = false;
                }
                logger.info(String.format("wTransferRole = %s no need Transfer", roleVO.getRoleId()));
                ApplicationLogger.addLoggerData(String.format("wTransferRole = %s no need Transfer", roleVO.getRoleId()));
            }
            if (checkTransfer) {
                Map<String, Object> result = isPolicyNeedsTransfer(uwPolicyVO, currUser, uwSourceType);
                boolean isPolicyNeedsTransfer = (Boolean) result.get("result");
                List<Integer> allLimitType = (List<Integer>) MapUtils.getObject(result, "allLimitType", Collections.EMPTY_LIST);
                List<Integer> t_allLimitType = new ArrayList<>();
                CollectionUtils.selectRejected(allLimitType, PredicateUtils.equalPredicate(CodeCst.UW_LIMIT_TYPE_7), t_allLimitType);
                // 新契約-實質受益人辨識結果交叉覆核
                boolean isLegalBeneEvalPolicy = isLegalBeneEvalPolicy(uwPolicyVO);
                // 新契約-身心障礙投保件判斷
                boolean isDisabilityCoverage = isDisabilityCoverage(uwPolicyVO);
                // 新契約-高齡投保件(高齡投保評估量表&關懷)
                PolicyVO policyVO = policyDS.load(uwPolicyVO.getPolicyId());
                boolean isElderScaleCoverage = isElderScaleCoverage(uwSourceType, policyVO);
                // 新契約-洗錢高風險件判斷
                boolean isHighRisk = isHighRisk(uwPolicyVO, allLimitType);
                // 新契約-是否進行交叉覆核
                boolean isNeedLegalBeneEvalPolicy = isNeedLegalBeneEvalPolicy(uwPolicyVO, isLegalBeneEvalPolicy, isDisabilityCoverage, isElderScaleCoverage, isHighRisk);

                ApplicationLogger.addLoggerData(String.format("isHighRisk=%s ", isHighRisk));
                ApplicationLogger.addLoggerData(String.format("UwTransferHelper.isPolicyNeedsTransfer=%s", result));
                ApplicationLogger.addLoggerData(String.format("isPolicyNeedsTransfer=%s", isPolicyNeedsTransfer));
                ApplicationLogger.addLoggerData(String.format("isLegalBeneEvalPolicy=%s", isLegalBeneEvalPolicy));
                ApplicationLogger.addLoggerData(String.format("isDisabilityCoverage=%s", isDisabilityCoverage));
                ApplicationLogger.addLoggerData(String.format("isElderScaleCoverage=%s", isElderScaleCoverage));
                ApplicationLogger.addLoggerData(String.format("isExtraPremManager=%s", isExtraPremManager));
                ApplicationLogger.addLoggerData(String.format("isNeedLegalBeneEvalPolicy=%s", isNeedLegalBeneEvalPolicy));
                ApplicationLogger.addLoggerData(String.format("allLimitType=%s, t_allLimitType=%s", allLimitType, t_allLimitType));

                if (isPolicyNeedsTransfer || isNeedLegalBeneEvalPolicy) {
                    if (CodeCst.UW_SOURCE_TYPE__NB.equals(uwSourceType)) {
                        boolean isExceededAuthLimit = !t_allLimitType.isEmpty();
                        logger.info(String.format("isExceededAuthLimit=%s", isExceededAuthLimit));
                        ApplicationLogger.addLoggerData(String.format("checkTransferUserId start transerUserId=%s, currUser=%s", transerUserId, currUser));

                        Set<String> leakMessage = getTransferUserLeakMessage(currUser, uwSourceType, isPolicyNeedsTransfer, isNeedLegalBeneEvalPolicy, isHighRisk, isLegalBeneEvalPolicy, isDisabilityCoverage, isElderScaleCoverage);
                        if (!leakMessage.isEmpty()) {
                            // 尚有覆核主管未設定
                            return false;
                        }
                        //判斷覆核、交叉覆核人員ID
                        transerUserId = this.checkTransferUserId(currUser, uwSourceType, isPolicyNeedsTransfer, isNeedLegalBeneEvalPolicy,
                                isHighRisk, isLegalBeneEvalPolicy, isDisabilityCoverage, isElderScaleCoverage);

                        ApplicationLogger.addLoggerData(String.format("checkTransferUserId end transerUserId=%s, currUser=%s", transerUserId, currUser));
                        Long transUser = (transerUserId != null) ? transerUserId : currUser;
                        logger.info(String.format("final transUser = %s", transUser));
                        ApplicationLogger.addLoggerData(String.format("final transUser = %s", transUser));
                        List<String> descs = new ArrayList<>();
                        if (isHighRisk) {
                            // 洗錢中高風險
                            // 陳核備註說明=「洗錢高風險件」
                            descs.add(NBUtils.getTWMsg("MSG_1263318"));
                        }
                        if (isExceededAuthLimit) {
                            // 逾核保授權
                            // 陳核備註說明=「逾核保授權」
                            descs.add(StringResource.getStringData("MSG_1266581", AppContext.getCurrentUser().getLangId()));
                        }
                        if (isLegalBeneEvalPolicy) {
                            // 保單為法人實質受益人須辨識保單
                            // 陳核備註說明=「實質受益人辨識結果交叉覆核」
                            descs.add(StringResource.getStringData("MSG_1266567", AppContext.getCurrentUser().getLangId()));
                        }

                        if (isDisabilityCoverage) {
                            // 保單為身心障礙保單
                            // 陳核備註說明=「身心障礙保件交叉覆核」
                            descs.add(StringResource.getStringData("MSG_1267254", AppContext.getCurrentUser().getLangId()));
                        }

                        if (isElderScaleCoverage) {
                            // 保單高齡投保件(高齡投保評估量表 ) 
                            // 陳核備註說明=「高齡保件交叉覆核」
                            descs.add(StringResource.getStringData("MSG_1267255", AppContext.getCurrentUser().getLangId()));
                        }

                        if (!descs.isEmpty()) {
                            String proposeDescTemp = uwPolicyVO.getProposeDesc();
                            // 陳核備註說明
                            uwPolicyVO.setProposeDesc(org.apache.commons.lang3.StringUtils.join(descs, "/"));
                            ApplicationLogger.addLoggerData(String.format("TransComment ProposeDesc = %s", uwPolicyVO.getProposeDesc()));
                            doUwTransferProposeDesc(uwPolicyVO, transUser); // 有陳核備註需記錄
                            // 處理完，再將原本備註寫回
                            uwPolicyVO.setProposeDesc(proposeDescTemp);
                        }
                    } else {
                        transerUserId = getTransferUser(currUser, uwSourceType);
                        ApplicationLogger.addLoggerData("getTransferUser(" + currUser + ", " + uwSourceType + "), transerUserId=" + transerUserId);
                    }
                }
            }

            logger.info("firstlyVO.getTransUnderwriterId():" + firstlyVO.getTransUnderwriterId() + ", currUser:" + currUser);
            ApplicationLogger.addLoggerData("firstlyVO.getTransUnderwriterId():" + firstlyVO.getTransUnderwriterId() + ", currUser:" + currUser);

            // 同初始核保員，更新初始核保員下達決定日期時間
            if (firstlyVO.getTransUnderwriterId().equals(currUser)) {
                firstlyVO.setSubmitTime(AppContext.getCurrentUserLocalTime());
                firstlyVO = uwTransferService.save(firstlyVO);

                // 清除資料, 清空主管簽核意見，
                uwPolicyVO.setUwCommentIndi("");
                uwPolicyVO.setUwNotes("");
                uwPolicyVO.setUnderwriteTime(firstlyVO.getSubmitTime());
                uwPolicyDS.updateUwPolicy(uwPolicyVO, false);
            }
            logger.info("isExtraPremManager:" + isExtraPremManager + ", transerUserId:" + transerUserId);
            ApplicationLogger.addLoggerData("isExtraPremManager:" + isExtraPremManager + ", transerUserId:" + transerUserId);

            if (isExtraPremManager) {
                /* 加費批註簽核同意流程 */
                if (transerUserId != null) {
                    // 轉呈至上級主管, re-assign task
                    doUwTransferProposeDesc(uwPolicyVO, transerUserId); //有陳核備註需記錄
                    doUwTransferExtraPremManager(transerUserId, uwPolicyVO, uwSourceType);
                    closeLastestUwTransfer(request, uwPolicyVO, lastestVO);
                    return false;
                } else {
                    // 主管審核完成, re-assign task回原核保員
                    Long assignTo = firstlyVO.getTransUnderwriterId();
                    doUwTransferProposeDesc(uwPolicyVO, assignTo); //有陳核備註需記錄
                    doUwTransferExtraPremFinish(request, uwPolicyVO, lastestVO, assignTo);
                    closeLastestUwTransfer(request, uwPolicyVO, lastestVO);
                    return false;
                }
            }

            /* 初始核員或核核簽核主管, 簽核流程 */
            if (transerUserId != null) {
                // 轉呈至上級主管, re-assign task
                doUwTransferProposeDesc(uwPolicyVO, transerUserId); //有陳核備註需記錄
                doUwTransferManager(transerUserId, uwPolicyVO, uwSourceType);
                closeLastestUwTransfer(request, uwPolicyVO, lastestVO);
                return false;
            } else {
                /* 因有契審表產生失敗的問題，以下流程需在產生成功後才執行
				// 更新初始核保人員ID及核保時間 
				uwPolicyVO.setUnderwriterId(firstlyVO.getTransUnderwriterId());
				uwPolicyVO.setUnderwriteTime(firstlyVO.getSubmitTime());
				// 更新最後核保覆核人員ID及核保覆核時間 
				uwPolicyVO.setLastUnderwriterId(currUser);
				uwPolicyVO.setLastUnderwriterTime(AppContext.getCurrentUserLocalTime());
				uwPolicyVO.setLastReportNotes(uwPolicyVO.getUwNotes());// 最後覆核/主管核保意見

				doUwTransferFinish(request, uwPolicyVO);
				closeLastestUwTransfer(request, uwPolicyVO, lastestVO);
                 */
                return true; // 執行 compleUnderwriting
            }

        } else {
            // 需區分陳核的request或response
            boolean isEscaUser = (lastestVO != null && lastestVO.isResponseOpen());
            logger.info("isEscaUser:" + isEscaUser);
            ApplicationLogger.addLoggerData("isEscaUser:" + isEscaUser + ", lastestVO.getSubmitTime:" + lastestVO.getSubmitTime());
            if (isEscaUser && lastestVO.getSubmitTime() == null) {
                // 執行陳核的回覆
                doUwTransferResponse(request, uwPolicyVO, lastestVO, uwSourceType);
            } else {
                // 執行人工陳核
                doUwTransferRequest(request, uwPolicyVO, uwSourceType);
            }
        }

        return false;
    }

    public Set<String> getTransferUserLeakMessage(Long currUser, String uwSourceType, boolean isPolicyNeedsTransfer, boolean isNeedLegalBeneEvalPolicy, boolean isHighRisk, boolean isLegalBeneEvalPolicy, boolean isDisabilityCoverage, boolean isElderScaleCoverage) {
        Set<String> vals = new HashSet<>();

        if (UW_SOURCE_TYPE__NB.equals(uwSourceType)) {
            Map<Integer, Long> allUserIds = getAllTransferUserIds(currUser, uwSourceType, isPolicyNeedsTransfer, isNeedLegalBeneEvalPolicy, isHighRisk, isLegalBeneEvalPolicy, isDisabilityCoverage, isElderScaleCoverage);
            Set<Integer> types = allUserIds.keySet();
            for (Integer type : types) {
                Long userid = MapUtils.getLong(allUserIds, type);
                if (userid == null) {
                    switch (type) {
                        case VERIFACTION_MANAGER_TYPE_1:
                            // 逾核保授權覆核主管
                            vals.add("MSG_1267032");

                            break;
                        case VERIFACTION_MANAGER_TYPE_8:
                            // 洗錢高風險覆核主管
                            vals.add("MSG_160");

                            break;
                        case VERIFACTION_MANAGER_TYPE_10:
                            // 實質受益人辨識結果交叉覆核
                            vals.add("MSG_161");

                            break;
                        case VERIFACTION_MANAGER_TYPE_11:
                            // 身心障礙保件交叉覆核
                            vals.add("MSG_162");

                            break;
                        case VERIFACTION_MANAGER_TYPE_12:
                            // 高齡保件交叉覆核
                            vals.add("MSG_163");

                            break;
                    }
                }
            }
        }
        addLoggerData(format("TransferUserLeakMessage=%s", vals));

        return vals;
    }

    public Map<Integer, Long> getAllTransferUserIds(Long currUser, String uwSourceType, boolean isPolicyNeedsTransfer, boolean isNeedLegalBeneEvalPolicy, boolean isHighRisk, boolean isLegalBeneEvalPolicy, boolean isDisabilityCoverage, boolean isElderScaleCoverage) {
        Map<Integer, Long> vals = new HashMap<>();

        if (UW_SOURCE_TYPE__NB.equals(uwSourceType)) {
            if (isHighRisk) {
                // 洗錢高風險覆核主管
                vals.put(VERIFACTION_MANAGER_TYPE_8, getVerifactionManagerTransferUserByType(currUser, VERIFACTION_MANAGER_TYPE_8));
            } else if (isPolicyNeedsTransfer) {
                // 逾核保授權覆核主管
                vals.put(VERIFACTION_MANAGER_TYPE_1, getVerifactionManagerTransferUserByType(currUser, VERIFACTION_MANAGER_TYPE_1));
            }
            if (isNeedLegalBeneEvalPolicy) {
                // 需交叉覆核
                if (isLegalBeneEvalPolicy) {
                    // 實質受益人辨識結果交叉覆核
                    vals.put(VERIFACTION_MANAGER_TYPE_10, getVerifactionManagerTransferUserByType(currUser, VERIFACTION_MANAGER_TYPE_10));
                }
                if (isDisabilityCoverage) {
                    // 身心障礙保件交叉覆核
                    vals.put(VERIFACTION_MANAGER_TYPE_11, getVerifactionManagerTransferUserByType(currUser, VERIFACTION_MANAGER_TYPE_11));
                }
                if (isElderScaleCoverage) {
                    // 高齡保件交叉覆核
                    vals.put(VERIFACTION_MANAGER_TYPE_12, getVerifactionManagerTransferUserByType(currUser, VERIFACTION_MANAGER_TYPE_12));
                }
            }
        }
        addLoggerData(format("AllTransferUserIds=%s", vals));

        return vals;
    }

    public Long checkTransferUserId(Long currUser, String uwSourceType, boolean isPolicyNeedsTransfer, boolean isNeedLegalBeneEvalPolicy, boolean isHighRisk, boolean isLegalBeneEvalPolicy, boolean isDisabilityCoverage, boolean isElderScaleCoverage) {
        Long newTransferUserId = null;

        if (UW_SOURCE_TYPE__NB.equals(uwSourceType)) {
            //權限順序	覆核/主管類型
            //1		洗錢高風險覆核主管
            //2		實質受益人辨識結果交叉覆核
            //3		身心障礙保件交叉覆核
            //4		高齡保件交叉覆核
            Map<Integer, Long> allUserIds = getAllTransferUserIds(currUser, uwSourceType, isPolicyNeedsTransfer, isNeedLegalBeneEvalPolicy, isHighRisk, isLegalBeneEvalPolicy, isDisabilityCoverage, isElderScaleCoverage);
            String sql = " WITH ";
            sql += " D_ROLE AS ( SELECT utr.ROLE_ID, limt.LIMITATION_NAME AS ROLE_NAME, LEVEL AS ROLE_ORDER FROM T_UW_TRANSFER_ROLE utr LEFT OUTER JOIN T_LIMITATION limt ON (utr.ROLE_ID = limt.LIMITATION_ID) INNER JOIN T_LIMITATION_TYPE limtype ON (limt.LIMITATION_TYPE = limtype.SUB_TYPE_ID) WHERE 1 = 1 START WITH utr.ROLE_ID = 600 CONNECT BY utr.UPLINE_ROLE_ID = PRIOR ROLE_ID ), ";
            sql += " D_USER_ROLE AS ( SELECT tr.REAL_NAME, tr.USER_NAME, tr.USER_ID, tr.ROLE_ID FROM (SELECT ur.USER_ID, ur.REAL_NAME, ur.USER_NAME, ur.DEPT_ID, r.* FROM T_USER ur INNER JOIN T_USER_ROLE urr ON (ur.USER_ID = urr.USER_ID AND urr.VALID_INDI = 'Y') INNER JOIN T_ROLE r1 ON (r1.ROLE_ID = urr.ROLE_ID AND r1.ROLE_TYPE = 1) INNER JOIN T_GROUPROLE_ROLE gr ON (gr.GROUPROLE_ID = r1.ROLE_ID) INNER JOIN T_ROLE r ON (r.ROLE_ID = gr.ROLE_ID) UNION SELECT ur.USER_ID, ur.REAL_NAME, ur.USER_NAME, ur.DEPT_ID, r.* FROM T_ROLE r INNER JOIN T_USER_ROLE urr ON (r.ROLE_ID = urr.ROLE_ID AND urr.VALID_INDI = 'Y') INNER JOIN T_USER ur ON (ur.USER_ID = urr.USER_ID)) tr LEFT OUTER JOIN T_DEPT td ON (td.DEPT_ID = tr.DEPT_ID) WHERE tr.ROLE_TYPE = 2 AND tr.SUB_ROLE_TYPE = 36 ), ";
            sql += " D_MANAGER_CONF AS ( SELECT ur_emp.USER_ID, vmc.TYPE_ID, CASE vmc.TYPE_ID WHEN 1 THEN 1 WHEN 8 THEN 1 WHEN 10 THEN 2 WHEN 11 THEN 3 WHEN 12 THEN 4 END AS CONF_ORDER, vmt.TYPE_DESC, vmc.VERIFACTION_MANAGER FROM T_VERIFACTION_MANAGER_CONF vmc LEFT OUTER JOIN T_USER ur_emp ON (vmc.EMP_ID = ur_emp.USER_ID) LEFT OUTER JOIN T_VERIFACTION_MANAGER_TYPE vmt ON (vmc.TYPE_ID = vmt.TYPE_ID) WHERE vmc.TYPE_ID IN (1, 8, 10, 11, 12) ) ";
            sql += " SELECT mc.*, usr.REAL_NAME, usr.USER_NAME, ur.ROLE_ID, r.ROLE_NAME, r.ROLE_ORDER ";
            sql += " FROM D_MANAGER_CONF mc ";
            sql += "   LEFT OUTER JOIN D_USER_ROLE ur ON (ur.USER_ID = mc.VERIFACTION_MANAGER) ";
            sql += "   LEFT OUTER JOIN D_ROLE r ON (ur.ROLE_ID = r.ROLE_ID) ";
            sql += "   LEFT OUTER JOIN T_USER usr ON (usr.USER_ID = mc.VERIFACTION_MANAGER) ";
            sql += " WHERE 1 = 1 ";
            sql += format(" AND   mc.USER_ID = %d ", currUser);
            if (!allUserIds.isEmpty()) {
                sql += format(" AND   mc.TYPE_ID IN (%s) ", allUserIds.keySet().stream().map(String::valueOf).collect(joining(",")));
            } else {
                sql += " AND 1 = 2 ";
            }
            sql += " ORDER BY mc.USER_ID, r.ROLE_ORDER ASC NULLS LAST, mc.CONF_ORDER ASC NULLS LAST ";

            List<Map<String, Object>> datas = paNbSQLOnlyService.queryList(sql);

            for (Map<String, Object> data : datas) {
                // 第一個就是真正的主管
                newTransferUserId = MapUtils.getLong(data, "VERIFACTION_MANAGER");

                break;
            }
            addLoggerData(format("TransferUserId sql[%d]=%s", datas.size(), sql));
        }

        addLoggerData(format("final TransferUserId=%s", newTransferUserId));

        return newTransferUserId;
    }

    public boolean isNeedLegalBeneEvalPolicy(UwPolicyVO uwPolicyVO, boolean isLegalBeneEvalPolicy, boolean isDisabilityCoverage, boolean isElderScaleCoverage, boolean isHighRisk) {
        // 判斷保單是否進行交叉覆核
        boolean isNeedLegalBeneEvalPolicy = false;

        if (UW_SOURCE_TYPE__NB.equals(uwPolicyVO.getUwSourceType())) {
            if (UW_TRANS_FLOW_FIRSTER == uwPolicyVO.getUwTransferFlow()
                    && (isLegalBeneEvalPolicy || isDisabilityCoverage || isElderScaleCoverage || isHighRisk)
                    && !checkExistsLegalBeneEvalPolicy(uwPolicyVO)) {
                // 初始核保員  且  為法人實質受益人須辨識保單或身心障礙件或高齡投保件或洗錢高風險   且  不存在覆核同意 才需要進行交叉覆核
                isNeedLegalBeneEvalPolicy = true;
            }
        }

        return isNeedLegalBeneEvalPolicy;
    }

    public boolean isElderScaleCoverage(String uwSourceType, PolicyVO policyVO) {
        // 判斷保單=高齡投保件(高齡投保評估量表&關懷)
        boolean isElderScaleCoverage = false;

        if (UW_SOURCE_TYPE__NB.equals(uwSourceType)) {
            boolean hasElderQuestionCoverage = paValidatorService.hasElderQuestionCoverage(policyVO);
            boolean hasElderScaleCoverage = paValidatorService.hasElderScaleCoverage(policyVO);
            List<UnbRoleVO> elderCareQuestionRoles = paValidatorService.getElderCareQuestionRoles(policyVO);
            List<UnbRoleVO> elderCareScaleRoles = paValidatorService.getElderCareScaleRoles(policyVO);
            if ((hasElderQuestionCoverage && !elderCareQuestionRoles.isEmpty())
                    || (hasElderScaleCoverage && !elderCareScaleRoles.isEmpty())) {
                isElderScaleCoverage = true;
            }
        }

        return isElderScaleCoverage;
    }

    public boolean isDisabilityCoverage(UwPolicyVO uwPolicyVO) {
        // 判斷保單=身心障礙投保件
        boolean isDisabilityCoverage = false;

        if (UW_SOURCE_TYPE__NB.equals(uwPolicyVO.getUwSourceType())) {
            List<UwLifeInsuredVO> insuredList = (List) uwPolicyDS.findUwLifeInsuredEntitis(uwPolicyVO.getUnderwriteId());
            for (UwLifeInsuredVO ulivo : insuredList) {
                if (!StringUtils.isNullOrEmpty(ulivo.getDisabilityType())
                        && !"000".equals(ulivo.getDisabilityType())) {
                    isDisabilityCoverage = true;
                }
            }
        }

        return isDisabilityCoverage;
    }

    public boolean isHighRisk(UwPolicyVO uwPolicyVO, List<Integer> allLimitType) {
        // 判斷保單=洗錢高風險件
        boolean isHighRisk = false;

        if (UW_SOURCE_TYPE__NB.equals(uwPolicyVO.getUwSourceType())) {
            isHighRisk = IterableUtils.toList(allLimitType).contains(UW_LIMIT_TYPE_7);
        }

        return isHighRisk;
    }

    public boolean isLegalBeneEvalPolicy(UwPolicyVO uwPolicyVO) {
        boolean tof = false;

        if (UW_SOURCE_TYPE__NB.equals(uwPolicyVO.getUwSourceType())) {
            // 判斷保單=法人實質受益人須辨識保單
            PolicyVO policyVO = paPolicyService.load(uwPolicyVO.getPolicyId());
            PolicyHolderVO policyHolderVO = policyVO.getPolicyHolder();
            List<BeneficiaryVO> beneficiaryVOs = beneficiaryService.findByPolicyId(uwPolicyVO.getPolicyId());
            List<UnbAmlCompanyVO> amlCompanyVOs = paUnbAmlCompanyService.findByPolicyId(policyVO.getPolicyId());

            // 1.要保人或受益人任一為法人 且
            boolean tof1 = ValidatorUtils.isAnyOneOrganCustomer(policyHolderVO, beneficiaryVOs);
            // 2.法人來自高風險國家或地區 或 保單投保壽險、投資型保險、年金保險或具保單價值準備金商品
            boolean tof2 = ValidatorUtils.isOrganCustomerHighRiskNationality(policyVO.getApplyDate(), policyHolderVO, beneficiaryVOs, amlCompanyVOs, amlQueryCI, paAddressService);
            boolean tof3 = ValidatorUtils.isNfoProduct(policyVO, lifeProductService, lifeProductCategoryService);
            logger.info(format("tof1=%s, tof2=%s, tof3=%s", tof1, tof2, tof3));
            addLoggerData(format("要保人或受益人任一為法人：%s", tof1));
            addLoggerData(format("法人來自高風險國家或地區：%s", tof2));
            addLoggerData(format("保單投保壽險、投資型保險、年金保險或具保單價值準備金商品：%s", tof3));
            tof = tof1 & (tof2 || tof3);
        }

        return tof;
    }

    public void doUwTransferProposeDesc(UwPolicyVO uwPolicyVO, Long transferId ) {
    	
    	if(!StringUtils.isNullOrEmpty(uwPolicyVO.getProposeDesc())) {

            Long currUser = AppUserContext.getCurrentUser().getUserId();
            // 記錄陳核備註
            UwTransferVO requestVO = new UwTransferVO();
            requestVO.setUnderwriteId(uwPolicyVO.getUnderwriteId());
            requestVO.setInitUnderwriterId(currUser);
            requestVO.setTransComment(uwPolicyVO.getProposeDesc());
            requestVO.setTransUnderwriterId(transferId);
            requestVO.setCreateTime(AppContext.getCurrentUserLocalTime());
            requestVO.setSubmitTime(AppContext.getCurrentUserLocalTime());
            requestVO.setPolicyId(uwPolicyVO.getPolicyId());
            requestVO.setFlowId(CodeCst.UW_TRANS_FLOW_ESCALATE_REQUEST);
            requestVO = uwTransferService.save(requestVO);
            
            uwPolicyVO.setProposeDesc("");
            uwPolicyDS.updateUwPolicy(uwPolicyVO, false);
    	}

        
	}

    private void closeLastestUwTransfer(HttpServletRequest request, UwPolicyVO uwPolicyVO, UwTransferVO lastestRecord) {
        /* 初始核保員的判斷已經在外面一層處理 ，此處不處理 */
        if (CodeCst.UW_TRANS_FLOW_FIRSTER != lastestRecord.getFlowId()) {
            if (lastestRecord.getSubmitTime() == null) {
                lastestRecord.setSubmitTime(AppContext.getCurrentUserLocalTime());
                uwTransferService.save(lastestRecord);
            }
        }
    }

    private void doUwTransferReject(HttpServletRequest request, UwPolicyVO uwPolicyVO, UwTransferVO firstlyVO) {

        Integer oldUwTransferFlow = uwPolicyVO.getUwTransferFlow();
        Long policyId = uwPolicyVO.getPolicyId();

        // 更新核保UwTransferFlow
        uwPolicyVO.setUwStatus(UwStatusConstants.IN_PROGRESS);
        uwPolicyVO.setUwTransferFlow(CodeCst.UW_TRANS_FLOW_FIRSTER);
        uwPolicyDS.updateUwPolicy(uwPolicyVO, false);

        // 清空核保員的下核保決定日期
        firstlyVO.setSubmitTime(null);
        uwTransferService.save(firstlyVO);

        // 更新加費批註照會訊息為wait approval狀態
        if (CodeCst.UW_TRANS_FLOW_EXTRA_PREM_MANAGER == oldUwTransferFlow) {
            List<ProposalRuleResultVO> ruleResults = proposalRuleResultService.findNeedApprovalRuleResults(policyId,
                            UwDocumentApprovalStatus.APPROVAL_TRANSFER);
            for (ProposalRuleResultVO ruleResult : ruleResults) {
                ruleResult.setUwDocumentApprovalStatus(UwDocumentApprovalStatus.APPROVAL_WAITING);
                proposalRuleResultService.save(ruleResult);
            }
        }

        // 返回原核保員
        proposalProcessService.processReassign(uwPolicyVO.getPolicyId(),
                        CodeCst.UW_STATUS__UW_IN_PROGRESS, firstlyVO.getInitUnderwriterId().toString());
    }

    public void doUwTransferExtraPremManager(Long transerUserId, UwPolicyVO uwPolicyVO, String uwSourceType) {

        Long currUser = AppContext.getCurrentUser().getUserId();

        UwTransferVO uwTransferVO = new UwTransferVO();
        uwTransferVO.setUnderwriteId(uwPolicyVO.getUnderwriteId());
        uwTransferVO.setInitUnderwriterId(currUser);
        uwTransferVO.setTransComment(null);
        uwTransferVO.setApproveStatus(null);
        uwTransferVO.setCreateTime(AppContext.getCurrentUserLocalTime());
        uwTransferVO.setPolicyId(uwPolicyVO.getPolicyId());

        // 新增轉呈加費/批註簽核主管
        uwTransferVO.setFlowId(CodeCst.UW_TRANS_FLOW_EXTRA_PREM_MANAGER);
        uwTransferVO.setTransUnderwriterId(transerUserId);
        uwTransferVO = uwTransferService.save(uwTransferVO);

        
        // 清除資料
        uwPolicyVO.setUwCommentIndi(""); // 清空主管簽核意見
        uwPolicyVO.setUwNotes(""); // 清空覆核/主管核保意見
        uwPolicyVO.setUwStatus(UwStatusConstants.ESCALATED);
        uwPolicyVO.setUwTransferFlow(CodeCst.UW_TRANS_FLOW_EXTRA_PREM_MANAGER);
        uwPolicyDS.updateUwPolicy(uwPolicyVO, false);

        // 新契約
        if (CodeCst.UW_SOURCE_TYPE__NB.equals(uwSourceType)) {
            proposalProcessService.processReassign(uwPolicyVO.getPolicyId(), CodeCst.UW_STATUS__UW_IN_PROGRESS, uwTransferVO.getTransUnderwriterId().toString());
        }
        // 保全
        else if (Utils.isCsUw(uwSourceType)) {
            csTaskTransService.completeTask(uwPolicyVO.getChangeId());
            csTaskTransService.assignTaskToSpecficUser(TaskName.CS_UNDERWRITING, uwPolicyVO.getChangeId(), transerUserId, TaskName.CS_UNDERWRITING);
        }
    }

    private void doUwTransferManager(Long transerUserId, UwPolicyVO uwPolicyVO, String uwSourceType) {

        Long currUser = AppContext.getCurrentUser().getUserId();

        UwTransferVO uwTransferVO = new UwTransferVO();
        uwTransferVO.setUnderwriteId(uwPolicyVO.getUnderwriteId());
        uwTransferVO.setInitUnderwriterId(currUser);
        uwTransferVO.setTransComment(null);
        uwTransferVO.setApproveStatus(null);
        uwTransferVO.setCreateTime(AppContext.getCurrentUserLocalTime());
        uwTransferVO.setPolicyId(uwPolicyVO.getPolicyId());

        // 新增轉呈至覆核主管(額度授權主管)
        uwTransferVO.setFlowId(CodeCst.UW_TRANS_FLOW_MANAGER);
        uwTransferVO.setTransUnderwriterId(transerUserId);

        // 清除資料
        uwPolicyVO.setUwCommentIndi(""); // 清空主管簽核意見
        uwPolicyVO.setUwNotes(""); // 清空覆核/主管核保意見
        uwPolicyVO.setUwStatus(UwStatusConstants.ESCALATED);
        uwPolicyVO.setUwTransferFlow(CodeCst.UW_TRANS_FLOW_MANAGER);
        uwPolicyDS.updateUwPolicy(uwPolicyVO, false);
        uwTransferVO = uwTransferService.save(uwTransferVO);

        // 新契約
        if (CodeCst.UW_SOURCE_TYPE__NB.equals(uwSourceType)) {
            proposalProcessService.processReassign(uwPolicyVO.getPolicyId(), CodeCst.UW_STATUS__UW_IN_PROGRESS, uwTransferVO.getTransUnderwriterId().toString());
        }
        // 保全
        else if (Utils.isCsUw(uwSourceType)) {
            csTaskTransService.completeTask(uwPolicyVO.getChangeId());
            csTaskTransService.assignTaskToSpecficUser(TaskName.CS_UNDERWRITING, uwPolicyVO.getChangeId(), transerUserId, TaskName.CS_UNDERWRITING);
        }
    }

    private void doUwTransferRequest(HttpServletRequest request, UwPolicyVO uwPolicyVO, String uwSourceType) {

        Long currUser = AppUserContext.getCurrentUser().getUserId();
        Long reAssingTo = uwPolicyVO.getUwEscaUser();
        // 記錄人工陳核發問－結束
        UwTransferVO requestVO = new UwTransferVO();
        requestVO.setUnderwriteId(uwPolicyVO.getUnderwriteId());
        requestVO.setInitUnderwriterId(currUser);
        // uwTransferVO.setTransComment(uwPolicyVO.getCheckNote());//核保特別說明記錄至UwPolicyVO.CheckNote
        requestVO.setTransComment(uwPolicyVO.getProposeDesc());
        requestVO.setTransUnderwriterId(uwPolicyVO.getUwEscaUser());
        requestVO.setCreateTime(AppContext.getCurrentUserLocalTime());
        requestVO.setSubmitTime(AppContext.getCurrentUserLocalTime());
        requestVO.setPolicyId(uwPolicyVO.getPolicyId());
        requestVO.setFlowId(CodeCst.UW_TRANS_FLOW_ESCALATE_REQUEST);
        requestVO = uwTransferService.save(requestVO);

        // 新增陳核待回覆
        UwTransferVO responseVO = new UwTransferVO();
        responseVO.setUnderwriteId(uwPolicyVO.getUnderwriteId());
        responseVO.setInitUnderwriterId(uwPolicyVO.getUwEscaUser());
        responseVO.setTransComment(null);
        responseVO.setTransUnderwriterId(currUser);
        responseVO.setCreateTime(AppContext.getCurrentUserLocalTime());
        responseVO.setSubmitTime(null); // 等待回覆
        responseVO.setPolicyId(uwPolicyVO.getPolicyId());
        responseVO.setFlowId(CodeCst.UW_TRANS_FLOW_ESCALATE_RESPONSE);
        responseVO = uwTransferService.save(responseVO);

        // 清除資料
        uwPolicyVO.setUwCommentIndi(""); // 清空主管簽核意見
        uwPolicyVO.setUwNotes(""); // 清空覆核/主管核保意見
        uwPolicyVO.setUwStatus(UwStatusConstants.IN_PROGRESS);
        uwPolicyDS.updateUwPolicy(uwPolicyVO, false);

        // 新契約
        if (CodeCst.UW_SOURCE_TYPE__NB.equals(uwSourceType)) {
            proposalProcessService.processReassign(uwPolicyVO.getPolicyId(), CodeCst.UW_STATUS__UW_IN_PROGRESS, reAssingTo.toString());
        }
        // 保全
        else if (Utils.isCsUw(uwSourceType)) {
            logger.info("doUwTransferRequest(): POS assignTaskToSpecficUser, reAssingTo:" + reAssingTo);
            ApplicationLogger.addLoggerData("doUwTransferRequest(): POS assignTaskToSpecficUser, reAssingTo:" + reAssingTo);
            csTaskTransService.completeTask(uwPolicyVO.getChangeId());
            csTaskTransService.assignTaskToSpecficUser(TaskName.CS_UNDERWRITING, uwPolicyVO.getChangeId(), reAssingTo, TaskName.CS_UNDERWRITING);
        }

    }

    private void doUwTransferResponse(HttpServletRequest request, UwPolicyVO uwPolicyVO,
                    UwTransferVO responseVO, String uwSourceType) {

        Long reAssingTo = responseVO.getTransUnderwriterId();

        // 記錄陳核回覆
        responseVO.setTransComment(uwPolicyVO.getUwNotes());
        responseVO.setSubmitTime(AppContext.getCurrentUserLocalTime());
        responseVO = uwTransferService.save(responseVO);

        // 清除資料
        // uwPolicyVO.setCheckNote("");
        // mark by simon.huang 2016/10/21 不作清除一直append
        uwPolicyVO.setManuEscIndi(CodeCst.YES_NO__NO);
        uwPolicyVO.setUwEscaUser(null);
        uwPolicyVO.setProposeDesc("");// 清除request的陳核說明
        uwPolicyVO.setUwNotes("");// 清除response的陳核說明
        uwPolicyVO.setUwStatus(UwStatusConstants.IN_PROGRESS);
        uwPolicyDS.updateUwPolicy(uwPolicyVO, false);

        // 新契約
        if (CodeCst.UW_SOURCE_TYPE__NB.equals(uwSourceType)) {
            proposalProcessService.processReassign(uwPolicyVO.getPolicyId(),
                            CodeCst.UW_STATUS__UW_IN_PROGRESS, reAssingTo.toString());
        }
        // 保全
        else if (Utils.isCsUw(uwSourceType)) {
            logger.info("doUwTransferResponse(): POS assignTaskToSpecficUser, reAssingTo:" + reAssingTo);
            ApplicationLogger.addLoggerData("doUwTransferResponse(): POS assignTaskToSpecficUser, reAssingTo:" + reAssingTo);
            csTaskTransService.completeTask(uwPolicyVO.getChangeId());
            csTaskTransService.assignTaskToSpecficUser(TaskName.CS_UNDERWRITING, uwPolicyVO.getChangeId(), reAssingTo, TaskName.CS_UNDERWRITING);
        }

    }

    private void doUwTransferExtraPremFinish(HttpServletRequest request, UwPolicyVO uwPolicyVO, 
    		UwTransferVO lastestRecord,Long firstUnderwriterId) {

        Integer oldUwTransferFlow = uwPolicyVO.getUwTransferFlow();
        Long policyId = uwPolicyVO.getPolicyId();

        Long currUser = AppContext.getCurrentUser().getUserId();

        UwTransferVO uwTransferVO = new UwTransferVO();

        // 決定簽核加費or批註
        List<ProposalRuleResultVO> needAgreeList = proposalRuleResultService.findNeedApprovalRuleResults(policyId,
                        UwDocumentApprovalStatus.APPROVAL_TRANSFER);
        Map<String, List<ProposalRuleResultVO>> ruleNameBindList = NBUtils.toMapList(needAgreeList, "ruleName");
        boolean isExtraPremApproval = false;
        boolean isExculsionApproval = false;
        if (ruleNameBindList.containsKey(CodeCst.PROPOSAL_RULE_MSG_EXTRA_PREM_LETTER)) {
            isExtraPremApproval = true;
        }
        if (ruleNameBindList.containsKey(CodeCst.PROPOSAL_RULE_MSG_EXCLUSION_LETTER)) {
            isExculsionApproval = true;
        }
        if (isExtraPremApproval && isExculsionApproval) {
            // 加費批註簽核完成
            uwTransferVO.setFlowId(CodeCst.UW_TRANS_FLOW_EXTRA_ALL_APPROVAL);
            if(CodeCst.YES_NO__YES.equals(uwPolicyVO.getUwCommentIndi())){
                uwTransferVO.setExtApprovalType(CodeCst.TYPE_EXT_ALL);
                lastestRecord.setExtApprovalType(CodeCst.TYPE_EXT_ALL);            	
            }
        } else if (isExtraPremApproval) {
            // 加費簽核完成
            uwTransferVO.setFlowId(CodeCst.UW_TRANS_FLOW_EXTRA_PREM_APPROVAL);
            if(CodeCst.YES_NO__YES.equals(uwPolicyVO.getUwCommentIndi())){
            	uwTransferVO.setExtApprovalType(CodeCst.TYPE_EXTRAPREM);
            	lastestRecord.setExtApprovalType(CodeCst.TYPE_EXTRAPREM);
            }
        } else {
            // 批註簽核完成
            uwTransferVO.setFlowId(CodeCst.UW_TRANS_FLOW_EXCLUSION_APPROVAL);
            if(CodeCst.YES_NO__YES.equals(uwPolicyVO.getUwCommentIndi())){
            	uwTransferVO.setExtApprovalType(CodeCst.TYPE_EXCLUSION);
            	lastestRecord.setExtApprovalType(CodeCst.TYPE_EXCLUSION);
            }
        }

        uwTransferVO.setUnderwriteId(uwPolicyVO.getUnderwriteId());
        uwTransferVO.setPolicyId(uwPolicyVO.getPolicyId());
        uwTransferVO.setInitUnderwriterId(currUser);
        uwTransferVO.setTransComment(uwPolicyVO.getUwNotes());
        uwTransferVO.setApproveStatus(uwPolicyVO.getUwCommentIndi());
        uwTransferVO.setTransUnderwriterId(currUser);
        uwTransferVO.setCreateTime(AppContext.getCurrentUserLocalTime());
        uwTransferVO.setSubmitTime(AppContext.getCurrentUserLocalTime());

        uwTransferVO = uwTransferService.save(uwTransferVO);
      
        // 更新核保UwTransferFlow
        uwPolicyVO.setUwStatus(UwStatusConstants.IN_PROGRESS);
        uwPolicyVO.setUwTransferFlow(CodeCst.UW_TRANS_FLOW_FIRSTER);
        uwPolicyDS.updateUwPolicy(uwPolicyVO, false);

        // 更新加費批註照會訊息為agree狀態
        if (CodeCst.UW_TRANS_FLOW_EXTRA_PREM_MANAGER == oldUwTransferFlow) {
            for (ProposalRuleResultVO ruleResult : needAgreeList) {
                ruleResult.setUwDocumentApprovalStatus(UwDocumentApprovalStatus.APPROVAL_AGREE);
                proposalRuleResultService.save(ruleResult);
            }
        }

        // 返回原核保員
        proposalProcessService.processReassign(uwPolicyVO.getPolicyId(),
                        CodeCst.UW_STATUS__UW_IN_PROGRESS, firstUnderwriterId.toString());

    }

    public void doUwTransferFinish(HttpServletRequest request, UwPolicyVO uwPolicyVO) {
        Long currUser = AppContext.getCurrentUser().getUserId();
        {	// 更新最後核保覆核人員ID及核保覆核時間 
        	
        	UwTransferVO lastestVO = uwTransferService.findLastestWorkUwTransfer(uwPolicyVO.getUnderwriteId());
    		UwTransferVO firstlyVO = uwTransferService.findFirstlyUwTransfer(uwPolicyVO.getUnderwriteId());

    		uwPolicyVO.setUnderwriterId(firstlyVO.getTransUnderwriterId());
    		uwPolicyVO.setUnderwriteTime(firstlyVO.getSubmitTime());
    		//PCR-463250 綜合查詢-新契約資訊要新增顯示核保單位 2023/01/19 Add by Kathy
    		UserVO userVO = SysMgmtDSLocator.getUserDS().getUserByUserID(firstlyVO.getTransUnderwriterId());
    		uwPolicyVO.setUwDeptId(Long.parseLong(userVO == null ? null : userVO.getDeptId()));
    	
    		uwPolicyVO.setLastUnderwriterId(currUser);
    		uwPolicyVO.setLastUnderwriterTime(AppContext.getCurrentUserLocalTime());
    		uwPolicyVO.setLastReportNotes(uwPolicyVO.getUwNotes());// 最後覆核/主管核保意見
    		
    		this.closeLastestUwTransfer(request, uwPolicyVO, lastestVO);
        }
	
		//產生結束的記錄
        UwTransferVO uwTransferVO = new UwTransferVO();

        uwTransferVO.setUnderwriteId(uwPolicyVO.getUnderwriteId());
        uwTransferVO.setPolicyId(uwPolicyVO.getPolicyId());
        uwTransferVO.setInitUnderwriterId(currUser);
        uwTransferVO.setTransComment(uwPolicyVO.getReportNotes());
        uwTransferVO.setApproveStatus(uwPolicyVO.getUwCommentIndi());
        uwTransferVO.setTransUnderwriterId(currUser);
        uwTransferVO.setCreateTime(AppContext.getCurrentUserLocalTime());
        uwTransferVO.setSubmitTime(AppContext.getCurrentUserLocalTime());
        uwTransferVO.setFlowId(CodeCst.UW_TRANS_FLOW_ROLE_FINISH);
        uwTransferVO = uwTransferService.save(uwTransferVO);

        uwPolicyVO.setUwStatus(UwStatusConstants.IN_PROGRESS);
        uwPolicyVO.setUwTransferFlow(CodeCst.UW_TRANS_FLOW_ROLE_FINISH);
        
        //PCR_378267 保留核保決定當下之風險等級，後續其他單位或AML系統名單異動時不可覆蓋。
        Integer riskLevel = this.getRiskLevel(uwPolicyVO.getPolicyId());
		String uwRiskLevel = NBUtils.convertRiskLevelInt2Str(riskLevel);
		uwPolicyVO.setUwRiskLevel(uwRiskLevel);
        
        uwPolicyDS.updateUwPolicy(uwPolicyVO, false);
    }

    /**
     * <p>Description : 核保決定-需確認是否有覆核主管或額度授權主管</p>
     * <p>Created By : simon.huang</p>
     * <p>Create Time : May 4, 2016</p>
     * @param uwPolicyVO
     * @param empId
     * @return
     * @throws GenericException
     */
    public Map<String, Object> isPolicyNeedsTransfer(UwPolicyVO uwPolicyVO, Long empId, String uwSourceType) throws GenericException {
        // NB
        if (CodeCst.UW_SOURCE_TYPE__NB.equals(uwSourceType)) {
            // 區分核保簽核或加費/批註簽核
            return isPolicyNeedsTransferForNB(uwPolicyVO, empId, uwPolicyVO.getUwTransferFlow());

            // CS
        } else if (Utils.isCsUw(uwSourceType)) {
            return isPolicyNeedsTransferForCS(uwPolicyVO, empId, uwSourceType);

            // 程式不會執行到這裡
        } else {
            Map<String, Object> result = new HashMap<String, Object>();
            result.put("result", new Boolean(false));
            return result;
        }
    }

    /**
     * <p>Description : 加費/批註-需確認是否有覆核主管或額度授權主管</p>
     * <p>Created By : simon.huang</p>
     * <p>Create Time : May 4, 2016</p>
     * @param uwPolicyVO
     * @param userId
     * @return
     * @throws GenericException
     */
    public Map<String, Object> isExtraPremNeedsTransfer(UwPolicyVO uwPolicyVO, Long userId, String uwSourceType)
                    throws GenericException {
        // NB
        if (CodeCst.UW_SOURCE_TYPE__NB.equals(uwSourceType)) {
            return isPolicyNeedsTransferForNB(uwPolicyVO, userId, CodeCst.UW_TRANS_FLOW_EXTRA_PREM_MANAGER);

            // CS
        } else if (Utils.isCsUw(uwSourceType)) {
            return isPolicyNeedsTransferForCS(uwPolicyVO, userId, uwSourceType);

            // 程式不會執行到這裡
        } else {
            Map<String, Object> result = new HashMap<String, Object>();
            result.put("result", new Boolean(false));
            return result;
        }
    }

    /**
     * <p>Description : 執行新契約簽核作業處理，需判斷檢核類型(1:核保簽核,8:加費/批註簽核)</p>
     * <p>Created By : simon.huang</p>
     * <p>Create Time : Feb 17, 2017</p>
     * @param uwPolicyVO 核保作業檔
     * @param userId 當前核保作業人員(核保員或簽核主管)
     * @param uwTransferFlow 檢核類型(4:核保簽核,5:加費/批註簽核)
     * @return 
     * @throws GenericException
     */
    protected Map<String, Object> isPolicyNeedsTransferForNB(UwPolicyVO uwPolicyVO, Long userId, int uwTransferFlow) throws GenericException {
        Map<String, Object> result = new HashMap<String, Object>();
        result.put("result", false);

        // 主管已簽核完成-同意，核保員再次執行,直接執行核保完成
        if (uwPolicyVO.getLastUnderwriterId() != null
                && CodeCst.YES_NO__YES.equals(uwPolicyVO.getUwCommentIndi())) {
            result.put("result", false); // 執行 compleUnderwriting
            result.put("resultMsg", "主管已簽核完成-同意，核保員再次執行,直接執行核保完成"); // 執行 compleUnderwriting
            return result;
        }

        String policyDecision = uwPolicyVO.getPolicyDecision();// 頁面「保單核保決定」
        Integer proposalStatus = uwProcessDS.getProposalStatus_TGL(uwPolicyVO.getUwProducts(), policyDecision); // 「保單核保決定」對應的要保書狀態

        if (proposalStatus != null
                && ProposalStatus.PROPOSAL_STATUS__WITHDRAWN == proposalStatus) {
            //核保取消，不執行主管授權判斷
            result.put("resultMsg", "核保取消，不執行主管授權判斷");
            return result;
        }

        UwTransferRoleVO roleVO = uwTransferService.findUwTransferRoleByUser(userId);
        UwTransferRoleVO g13RoleVO = uwTransferService.findUwTransferRole(CodeCst.UW_ESC_ROLE_G13);
        
        //PCR-531509
        //檢查核保員授權等級是否G12
        if(null != roleVO) {
            if(roleVO.getRoleId().longValue()==CodeCst.UW_ESC_ROLE_G12.longValue()) {
                //檢核是否超過G13核保額度
                getPolicyNeedsTransferResultForNB(result, g13RoleVO, uwPolicyVO, uwTransferFlow);
            }
        }
        
        //G12未超過G13額度或未觸發G12特殊檢核則重做一次正常額度檢核
        if(!(Boolean)result.get("result")) {
        	//result初始化
        	result = new HashMap<String, Object>();
        	result.put("result", false);
        	
            getPolicyNeedsTransferResultForNB(result, roleVO, uwPolicyVO, uwTransferFlow);
        }

        return result;
    }

    protected void getPolicyNeedsTransferResultForNB(Map<String, Object> result, UwTransferRoleVO roleVO, UwPolicyVO uwPolicyVO, int uwTransferFlow) {
        ApplicationLogger.addLoggerData("getPolicyNeedsTransferResultForNB begin");
        boolean hasresult = false;
        boolean isNeedEscalate = false;

        if (roleVO != null) {
            //把用來檢核的roleid保留下來給後續做分支處理的條件
            result.put("roleId", roleVO.getRoleId());
            ApplicationLogger.addLoggerData(String.format("getPolicyNeedsTransferResultForNB 檢核角色(UwTransferRole.UW_ESC_ROLE)=%s", roleVO.getRoleId()));

            Long underwriteId = uwPolicyVO.getUnderwriteId();
            Long policyId = uwPolicyVO.getPolicyId();
            boolean isMedicalCheck = policyDao.isMedicalCheck(policyId);
            String policyDecision = uwPolicyVO.getPolicyDecision();// 頁面「保單核保決定」
            Integer proposalStatus = uwProcessDS.getProposalStatus_TGL(uwPolicyVO.getUwProducts(), policyDecision); // 「保單核保決定」對應的要保書狀態

            // 依風險累計額度
            List<Integer> allLimitType = new ArrayList<>();
            result.put("allLimitType", allLimitType);

            // 額度設定
            Map<Integer, UwTransferRoleLimitVO> roleLimitMapping = uwTransferService.findRoleLimitInfo(roleVO.getRoleId());
            Map<Long, UwTransferLevelLimitVO> allLevelLimitMap = uwTransferService.findAllLevelLimit(); // limit_item
            Map<Long, UwTransferValidateInput> insuredBindValidateInput = this.initValidateInput(policyId, underwriteId, true);

            boolean isDeclined = DECLINED.equals(proposalStatus);
            boolean isPostponed = POSTPONED.equals(proposalStatus);
            ApplicationLogger.addLoggerData(String.format("getPolicyNeedsTransferResultForNB 是否拒保=%s", isDeclined));
            ApplicationLogger.addLoggerData(String.format("getPolicyNeedsTransferResultForNB 是否延期=%s", isPostponed));

            /* limitType : 壽險/傷害險/健康險/次標等級/拒保(延期)/旅行險  */
            List<Integer> validLimitTypeList = new ArrayList<>();
            /* 核保決定授權判斷，需處理拒保/延期判斷 */
            if (uwTransferFlow == UW_TRANS_FLOW_MANAGER
                    || uwTransferFlow == UW_TRANS_FLOW_FIRSTER) {
                if (isDeclined || isPostponed) {//拒保/延期 
                    validLimitTypeList.clear();//僅作拒保/延期的判斷 RTC 152338
                    validLimitTypeList.add(CodeCst.UW_LIMIT_TYPE_5);
                } else {
                    validLimitTypeList.clear();
                    validLimitTypeList.add(CodeCst.UW_LIMIT_TYPE_7); // 風險等級

                    //商品設定商品小類
                    //根據保單包含的商品類型決定要檢核哪些風險累額
                    PolicyVO policyVO = paPolicyService.load(uwPolicyVO.getPolicyId());
                    for (CoverageVO coverageVO : policyVO.getCoverages()) {
                        Map<ProdBizCategory, ProdBizCategorySub> prodBizCategory = lifeProductCategoryService.findProdBizCategories(coverageVO.getProductId().longValue());
                        ProdBizCategorySub prodBizCategorySub = prodBizCategory.get(MAIN_02);
                        ApplicationLogger.addLoggerData(String.format("getPolicyNeedsTransferResultForNB 取得%s商品小類=%s", coverageVO.getProductId(), prodBizCategorySub.getCategoryCode()));

                        switch (prodBizCategorySub) {
                            case SUB_02_TLF:
                            case SUB_02_SLF:
                            case SUB_02_UL:
                            case SUB_02_VUL:
                            case SUB_02_SNL:
                                //商品小類 - 傳統壽險(TLF)、利變壽險(SLF)、萬能壽險(UL)、變額壽險(VUL)、投資連結型壽險(SNL)
                                validLimitTypeList.add(UW_LIMIT_TYPE_1); // 壽險
                                break;
                            case SUB_02_ACC:
                                //商品小類 - 傷害險
                                validLimitTypeList.add(UW_LIMIT_TYPE_2); // 傷害險
                                break;
                            case SUB_02_HLT:
                                //商品小類 - 健康險
                                validLimitTypeList.add(UW_LIMIT_TYPE_3); // 健康險
                                break;
                            default:
                                break;
                        }
                    }
                }
            } else if (uwTransferFlow == UW_TRANS_FLOW_EXTRA_PREM_MANAGER) {
                //加費簽核只做次標等級檢核
                validLimitTypeList.clear();
                validLimitTypeList.add(UW_LIMIT_TYPE_4); // 次標等級
            }

            //預防重複資料
            validLimitTypeList = validLimitTypeList.stream().distinct().collect(Collectors.toList());
            ApplicationLogger.addLoggerData(String.format("getPolicyNeedsTransferResultForNB 核保陳核驗證類型(UwTransferLimitType.UW_LIMIT_TYPE)=%s", validLimitTypeList));

            for (UwTransferValidateInput validateInput : insuredBindValidateInput.values()) {
                for (Integer limitType : validLimitTypeList) {

                    // limitRoleVO ex:M1角色(limitType可授權額度的設定)
                    UwTransferRoleLimitVO limitRoleVO = roleLimitMapping.get(limitType);

                    if (limitRoleVO != null) {
                        // levelLimitVO : 核保人員的授權角色可核保額度上限(真正的數值)
                        UwTransferLevelLimitVO levelLimitVO = allLevelLimitMap.get(limitRoleVO.getLimitId());

                        // 每個分類均檢核是否有權限執行體檢件
                        if (YES_NO__NO.equals(levelLimitVO.getMedicalCheckIndi())) {// 僅可作無體檢件;
                            if (isMedicalCheck && !hasresult) {
                                // 保單為體檢件,需向上轉呈 ;
                                isNeedEscalate = true;

                                result.put("result", isNeedEscalate);
                                result.put("resultMsg", "核保陳核(" + underwriteId + ")- 保單為體檢件且核保員僅可作無體檢件 ");

                                hasresult = true;
                            }
                            // 體檢件逾核保授權
                            if (!allLimitType.contains(UW_LIMIT_TYPE_N1)) {
                                allLimitType.add(UW_LIMIT_TYPE_N1);
                            }
                        }

                        // 依levelType 規則執行檢核(直接核定/轉呈覆核/授權by EM值/授權by 累積有效保額);
                        boolean resultBoolean = transferLevelTypeValidate.validate(limitType, levelLimitVO, validateInput);
                        if (!resultBoolean) {
                            if (!hasresult) {
                                isNeedEscalate = true;

                                BigDecimal value = validateInput.getSumAssured(limitType);
                                int riskLevel = validateInput.getRiskLevel();
                                result.put("result", isNeedEscalate);
                                result.put("resultMsg", "核保陳核("
                                        + "LIMIT ID=" + limitRoleVO.getListId()
                                        + ",可授權=" + levelLimitVO.getLimitValue()
                                        + ",風險額度=" + value
                                        + ",風險等級=" + riskLevel
                                        + ")- 向上轉呈 by" + limitType);

                                result.put("limitType", limitType);
                                hasresult = true;
                            }
                            if (!allLimitType.contains(limitType)) {
                                allLimitType.add(limitType);
                            }
                        }
                    }
                }
            }
        } else {
            isNeedEscalate = true;
            result.put("result", isNeedEscalate);
            result.put("resultMsg", "核保陳核(未設定授權額度)");
        }

        ApplicationLogger.addLoggerData("getPolicyNeedsTransferResultForNB end");
    }

    /**
     * 依保單ID檢核當前核保ID-核保陳核歷史記錄中是否存在有「實質受益人辨識結果交叉覆核」的同意陳核回覆
     *
     * @param uwPolicyVO
     * @return true：存在, false：不存在
     */
    public boolean checkExistsLegalBeneEvalPolicy(UwPolicyVO uwPolicyVO) {
        boolean tof = false;

        if (uwPolicyVO != null) {
            List<UwTransferVO> uwTransferVOs = uwTransferService.getUwTransferByPolicyID(uwPolicyVO.getPolicyId());
            boolean judgeFlowManager = false;
            for (UwTransferVO uwTransferVO : uwTransferVOs) {
                if (tof) {
                    // 已經找到就停止loop
                    break;
                }
                if (ObjectUtils.notEqual(uwPolicyVO.getUnderwriteId(), uwTransferVO.getUnderwriteId())) {
                    // 不同核保紀錄不比對
                    continue;
                }
                // 判斷本次核保紀錄中是否含有「實質受益人辨識結果交叉覆核」同意結果
                if (judgeFlowManager) {
                    if (UwTransferFlowType.UW_TRANS_FLOW_MANAGER == uwTransferVO.getFlowId()
                            && YesNo.YES_NO__YES.equals(uwTransferVO.getApproveStatus())) {
                        tof = true;
                    }

                    judgeFlowManager = false;
                }
                // 判斷本次核保紀錄中是否含有「實質受益人辨識結果交叉覆核」覆核事項
                if (UwTransferFlowType.UW_TRANS_FLOW_ESCALATE_REQUEST == uwTransferVO.getFlowId()
                        && org.apache.commons.lang3.StringUtils.trimToEmpty(uwTransferVO.getTransComment()).contains(StringResource.getStringData("MSG_1266567", AppContext.getCurrentUser().getLangId()))) {
                    judgeFlowManager = true;
                }
            }
        }

        return tof;
    }

    protected Map<String, Object> isPolicyNeedsTransferForCS(UwPolicyVO uwPolicyVO, Long empId, String uwSourceType) throws GenericException {
        ApplicationLogger.addLoggerData("isPolicyNeedsTransferForCS(" + uwPolicyVO.getUnderwriteId() + " , empId=" + empId + ", uwSourceType=" + uwSourceType + ")");
        Map<String, Object> result = new HashMap<String, Object>();
        result.put("result", false);

        Long underwriteId = uwPolicyVO.getUnderwriteId();
        boolean isNeedEscalate = false;

        // 依風險累計額度
        Long policyId = uwPolicyVO.getPolicyId();

        boolean isMedicalCheck = policyDao.isMedicalCheck(policyId);
        UwTransferRoleVO roleVO = uwTransferService.findUwTransferRoleByUser(empId);
        ApplicationLogger.addLoggerData("isMedicalCheck=" + isMedicalCheck);
        if (roleVO != null) {
            ApplicationLogger.addLoggerData("roleId=" + roleVO.getRoleId());
            Map<Integer, UwTransferRoleLimitVO> roleLimitMapping = uwTransferService.findRoleLimitInfo(roleVO.getRoleId());
            Map<Long, UwTransferLevelLimitVO> allLevelLimitMap = uwTransferService.findAllLevelLimit(); // limit_item
                                                                                                        // 額度設定

            String policyDecision = uwPolicyVO.getPolicyDecision();// 頁面「保單核保決定」
            Integer proposalStatus = uwProcessDS.getProposalStatus_TGL(uwPolicyVO.getUwProducts(), policyDecision); // 「保單核保決定」對應的要保書狀態

            //若為保全件，應提供policyChgId才能正確處理
            Map<Long, UwTransferValidateInput> insuredBindValidateInput = this.initValidateInput(policyId, underwriteId, false, applicationService.getPolicyChgIdForCalcRA(uwPolicyVO.getChangeId()));

            boolean isDeclined = ProposalStatusConstants.DECLINED.equals(proposalStatus);
            boolean isPostponed = ProposalStatusConstants.POSTPONED.equals(proposalStatus);
            
            ApplicationLogger.addLoggerData("policyDecision=" + policyDecision + ", proposalStatus=" + proposalStatus + ", isDeclined=" + isDeclined + ", isPostponed=" + isPostponed);

            for (UwTransferValidateInput validateInput : insuredBindValidateInput.values()) {

                // boolean isSubStandard =
                // validateInput.getMaxEmValue().compareTo(BigDecimal.ZERO) > 0;
                // // 是否次標準體

                List<Integer> validLimitTypeList = new ArrayList<Integer>();
                /* limitType : 壽險/傷害險/健康險/次標等級/拒保(延期)/旅行險  */
                if (isDeclined || isPostponed) {
                    validLimitTypeList.add(CodeCst.UW_LIMIT_TYPE_5);
                } else {
                    validLimitTypeList.add(CodeCst.UW_LIMIT_TYPE_1);
                    validLimitTypeList.add(CodeCst.UW_LIMIT_TYPE_2);
                    validLimitTypeList.add(CodeCst.UW_LIMIT_TYPE_3);
                    validLimitTypeList.add(CodeCst.UW_LIMIT_TYPE_4);
                    // validLimitTypeList.add(CodeCst.UW_LIMIT_TYPE_7); // 風險等級

                }

                for (Integer limitType : validLimitTypeList) {
                    // limitRoleVO ex:M1角色(limitType可授權額度的設定)
                    UwTransferRoleLimitVO limitRoleVO = roleLimitMapping.get(limitType);
                    ApplicationLogger.addLoggerData("[loop] limitType=" + limitType);
                    if (limitRoleVO != null) {
                        // levelLimitVO : 核保人員的授權角色可核保額度上限(真正的數值)
                        UwTransferLevelLimitVO levelLimitVO = allLevelLimitMap.get(limitRoleVO.getLimitId());
                        ApplicationLogger.addLoggerData("limitType=" + limitType + ", limitRole.LimitId=" + limitRoleVO.getLimitId() + ", levelLimit.MedicalCheckIndi=" + levelLimitVO.getMedicalCheckIndi());
                        if (CodeCst.YES_NO__NO.equals(levelLimitVO.getMedicalCheckIndi())) {// 僅可作無體檢件;
                            if (isMedicalCheck) {
                                // 保單為體檢件,需向上轉呈 ;
                                isNeedEscalate = true;
                                result.put("result", isNeedEscalate);
                                result.put("resultMsg", "核保陳核(" + underwriteId + ")- 保單為體檢件  ");
                                break;
                            }
                        }
                        // 依levelType 規則執行檢核(直接核定/轉呈覆核/授權by EM值/授權by 累積有效保額);
                        boolean resultBoolean = transferLevelTypeValidate.validate(limitType, levelLimitVO, validateInput);
                        ApplicationLogger.addLoggerData("limitType=" + limitType + ", resultBoolean=" + resultBoolean);
                        if (!resultBoolean) {
                            isNeedEscalate = true;
                            result.put("result", isNeedEscalate);
                            result.put("resultMsg", "核保陳核(" + underwriteId + ")- 向上轉呈 by" + limitType);
                            result.put("limitType", limitType);
                            break;
                        }
                    }
                }
            }
        } else {
            // 無核保權限, 需向上轉呈
            ApplicationLogger.addLoggerData("No UwTransferRole for " + empId);
            isNeedEscalate = true;
            result.put("result", isNeedEscalate);
            result.put("resultMsg", "核保陳核(" + underwriteId + ")- 無核保權限 ");
        }
        
        ApplicationLogger.addLoggerData("isNeedEscalate=" + isNeedEscalate);

        // 需要向上陳核
        if (isNeedEscalate) {
            // 覆核主管
            LeaveAuthEdit leaveAuthVO = leaveManagerService.findCSOrUNBAuthLeave(null, empId, uwSourceType);

            // 需要向上陳核，但無覆核主管時，拋Exception
            if (leaveAuthVO == null) {
                ApplicationLogger.addLoggerData("leaveAuthVO is null.");
                throw new AppException(20610020109L);
            }
            else {
                ApplicationLogger.addLoggerData("leaveAuthVO.MasterEmpId=" + leaveAuthVO.getMasterEmpId());
                /* 判斷覆核主管不可同本人 */
                if (leaveAuthVO != null && leaveAuthVO.getMasterEmpId() != null
                                && leaveAuthVO.getMasterEmpId().equals(empId) == false) {
                    isNeedEscalate = true;
                    result.put("result", isNeedEscalate);
                    result.put("resultMsg", "核保陳核(" + underwriteId + ")- 師徒制- A(" + empId + ") -> B (" + leaveAuthVO.getMasterEmpId() + ") ");
                    return result;
                }

                ApplicationLogger.addLoggerData("LastUnderwriterId=" + uwPolicyVO.getLastUnderwriterId() + ", UwCommentIndi="+uwPolicyVO.getUwCommentIndi());
                // 主管已簽核完成-同意，核保員再次執行,直接執行核保完成
                if (uwPolicyVO.getLastUnderwriterId() != null
                                && CodeCst.YES_NO__YES.equals(uwPolicyVO.getUwCommentIndi())) {
                    result.put("result", false); // 執行 compleUnderwriting
                    return result;
                }
            }
        }
        return result;
    }

    private Map<Long, UwTransferValidateInput> initValidateInput(Long policyId, Long underwriteId, boolean isNB) {
    	return this.initValidateInput(policyId, underwriteId, isNB, null);
    }
    
    @SuppressWarnings("unchecked")
    private Map<Long, UwTransferValidateInput> initValidateInput(Long policyId, Long underwriteId, boolean isNB, Long policyChgId) {

        Map<Long, UwTransferValidateInput> insuredBindValidatInput = new HashMap<Long, UwTransferValidateInput>();

        Collection<UwExtraLoadingVO> allExtra = uwPolicyDS.findUwExtraLoadingEntitis(underwriteId);
        Collection<UwLifeInsuredVO> uwInsuredVOList = uwPolicyDS.findUwLifeInsuredEntitis(underwriteId);

        // 取得覆核時要保人風險等級記錄
        int riskLevel = 1;
        if(isNB){
        	riskLevel = this.getRiskLevel(policyId);
        	/* 經由RMS計算出來的風險累積資料   */
            raService.calcRA(policyId);
        } else {
        	//若為保全件，應提供policyChgId才能正確處理
        	/* 經由RMS計算出來的風險累積資料   */
            raService.calcRA(policyId, policyChgId);	
        }
		 
        
        // 依被保險人初始核格授權表資料
        for (UwLifeInsuredVO uwLifeInsuredVO : uwInsuredVOList) {
            UwTransferValidateInput validateInput = new UwTransferValidateInput();
            Long partyId = uwLifeInsuredVO.getInsuredId();

            // 加費檔累加被保險人em值及加費金額
            BigDecimal maxEmValue = BigDecimal.ZERO;
            BigDecimal sumExtraPrem = BigDecimal.ZERO;
            for (UwExtraLoadingVO uwExtraPremVO : allExtra) {
                if (uwExtraPremVO.getInsuredId().longValue() == partyId) {
                    if (CodeCst.EXTRA_TYPE__HEALTH_EXTRA.equals(uwExtraPremVO.getExtraType())) {
                        /* 弱體加費，em最大值 */
                        if (uwExtraPremVO.getEmValue() != null) {
                            if (uwExtraPremVO.getEmValue() > maxEmValue.intValue()) {
                                maxEmValue = new BigDecimal(uwExtraPremVO.getEmValue());
                            }
                        }
                    }
                    if (uwExtraPremVO.getExtraPrem() != null) {
                        sumExtraPrem = sumExtraPrem.add(uwExtraPremVO.getExtraPrem());
                    }
                }
            }
           
            Map<Integer, BigDecimal> typeBindRiskSumAssured = raService.getUwTransferRisKSumAssuredByInsuredPartyId(policyId, partyId);

            validateInput.setTypeBindRiskSumAssured(typeBindRiskSumAssured);
            validateInput.setMaxEmValue(maxEmValue);
            validateInput.setSumExtraPrem(sumExtraPrem);

            // 要保人風險等級
            validateInput.setRiskLevel(riskLevel);

            insuredBindValidatInput.put(partyId, validateInput);
        }

        return insuredBindValidatInput;
    }
    
	/**
	 * <p>Description : BR-UNB-VER-006B 詢問萊斯系統風險評級。</p>
	 * <p>Created By : Vince.Cheng</p>
	 * <p>Create Time : Aug 31, 2020</p>
	 * @param policyId 
	 * @return
	 */
	public Integer getRiskLevel(Long policyId) {
		Integer riskLevel = 1;

		try {
			FircoRsDtaCIVO fircoRsData = (FircoRsDtaCIVO) ActionDataCache.getCacheValue(ActionDataCacheKey.NB_AML, policyId);
			if (fircoRsData == null) {
				fircoRsData = amlQueryCI.queryAmlUnb(policyId);
				NBUtils.logger(this.getClass(), "get aml from ws aml !");
			} else {
				NBUtils.logger(this.getClass(), "get aml from thread cache !");
			}

			if (fircoRsData != null) {
				if (fircoRsData.getHeadRsData() != null 
								&& !"0".equals(fircoRsData.getHeadRsData().getRetCode())) {
					logger.error("[ERROR] 洗錢防制風險查詢接口 :" + fircoRsData.getHeadRsData().getRetCode() + ":" + fircoRsData.getHeadRsData().getErrMsg());
					NBUtils.logger(this.getClass(), "[ERROR] 洗錢防制風險查詢接口 :" + fircoRsData.getHeadRsData().getRetCode() + ":" + fircoRsData.getHeadRsData().getErrMsg());
					riskLevel = -1;
				} else {
					//風險計算結果
					RiskCustomerQueryVO riskVO = NBUtils.getAmlRiskLevel(fircoRsData);
					String level = riskVO.getRiskLevel();
					riskLevel = NBUtils.convertRiskLevelStr2Int(level);
					logger.info("UwTransfer AML Level:" + level + ":" + riskLevel);
					NBUtils.logger(this.getClass(), "UwTransfer AML Level:" + level + ":" + riskLevel);
				}
			} else {
				logger.error("[ERROR] 洗錢防制風險查詢接口 : empty response !! ");
				NBUtils.logger(this.getClass(), "[ERROR] 洗錢防制風險查詢接口 : empty response !! ");
				riskLevel = -1;
			}
		} catch (Throwable e) {
			logger.error("[ERROR] 洗錢防制風險查詢接口 :" + ExceptionInfoUtils.getExceptionMsg(e));
			NBUtils.logger(this.getClass(), "[ERROR] 洗錢防制風險查詢接口 :" + ExceptionInfoUtils.getExceptionMsg(e));
			riskLevel = -1;
		}
		NBUtils.logger(this.getClass(), "riskLevel: " + riskLevel);
		return riskLevel;
	}

    /**
     * <p>Description : 核保決定-同機構可人工陳核UserID 清單</p>
     * <p>Created By : simon.huang</p>
     * <p>Create Time : Nov 22, 2016</p>
     * @param userId 
     * @param uwSourceType (新契約或保全)
     * @return 同機構可人工陳核UserID List
     * @throws GenericException
     */
    public List<Long> getManualTransferUser(Long userId, String uwSourceType) throws GenericException {

        List<Long> userList = new ArrayList<Long>();
        UserDS userDS = SysMgmtDSLocator.getUserDS();
        UserVO userVO = userDS.getUserByUserID(userId);
        Integer userBizCategory = deptDS.getBizCategoryByDeptId(new Long(userVO.getDeptId()));
        
        int srcBizCategory = CodeCst.BIZ_CATEGORY_UNB;
        if (Utils.isCsUw(uwSourceType)) {
            srcBizCategory = CodeCst.BIZ_CATEGORY_POS;
            
            //IR-245286-(PROD) 核保決定->簽核主管，無保費主管 (增加登入人員部門別的判斷因為PA共用POS核保作業)
            if(CodeCst.BIZ_CATEGORY_CLM == userBizCategory.intValue()) {
            	srcBizCategory = CodeCst.BIZ_CATEGORY_CLM;
            }
            //IR-245286-(PROD) 核保決定->簽核主管，無保費主管 (增加登入人員部門別的判斷因為PA共用POS核保作業)
            if(CodeCst.BIZ_CATEGORY_PA == userBizCategory.intValue()) {
            	srcBizCategory = CodeCst.BIZ_CATEGORY_PA;
            }
        }

        /* 底層為直接查t_dept對應的biz_category,作一個小cache避免相同dept_id多次查找  */
        Map<String, Integer> cacheDeptIdBindBizCategory = new HashMap<String, Integer>();

        /* 有人工陳核陳核權限的人員清單 */
        for(long roleId :uwTransferService.getUwTransferRoleBO().getRule()) {
            @SuppressWarnings("unchecked")
            Collection<UserVO> list = userDS.getUsersByRoleID(roleId);
            for (UserVO tempUserVo : list) {
                /* 需判斷同一機構 */
                if (!tempUserVo.getDisable() && NBUtils.in(userVO.getOrganId(), tempUserVo.getOrganId())) {
                    String deptId = tempUserVo.getDeptId();
                    Integer deptBizCategory = null;

                    /* 判斷部門別的業務屬性是新契約或保全 */
                    if (StringUtils.isNullOrEmpty(deptId) == false) {
                        if (cacheDeptIdBindBizCategory.containsKey(deptId)) {
                            deptBizCategory = cacheDeptIdBindBizCategory.get(deptId);
                        } else {
                            deptBizCategory = deptDS.getBizCategoryByDeptId(new Long(deptId));
                            cacheDeptIdBindBizCategory.put(deptId, deptBizCategory);
                        }
                    }

                    if (deptBizCategory != null && deptBizCategory.intValue() == srcBizCategory) {
                    	if (userList.contains(tempUserVo.getUserId()) == false) {
                            userList.add(tempUserVo.getUserId());
                        }
                    }
                }
            }
        }
        
        userList.remove(userId);
        return userList;
    }

    /**
     * <p>Description : 核保決定-取得覆核主管/額度授權主管的UserID</p>
     * <p>Created By : simon.huang</p>
     * <p>Create Time : May 4, 2016</p>
     * @param userId
     * @param uwSourceType
     * @return 覆核主管/額度授權主管 UserID
     * @throws GenericException
     */
    public Long getTransferUser(Long userId, String uwSourceType) throws GenericException {

        Long transferUserId = null;

        /* 是否有覆核主管 */
        LeaveAuthEdit leaveAuthVO = leaveManagerService.findCSOrUNBAuthLeave(null, userId, uwSourceType);

        /* 判斷覆核主管不可同本人 */
        if (leaveAuthVO != null && leaveAuthVO.getMasterEmpId() != null
                        && leaveAuthVO.getMasterEmpId().equals(userId) == false) {
            if (leaveAuthVO.getMasterEmpId().longValue() > 0) {
                transferUserId = leaveAuthVO.getMasterEmpId();
                return transferUserId;
            }
        }

        return transferUserId;
    }

    /**
     * <p>Description : 核保決定-取得高風險主管的UserID</p>
     * <p>Created By : simon.huang</p>
     * <p>Create Time : May 4, 2016</p>
     * @param userId
     * @param uwSourceType
     * @return 覆核主管/額度授權主管 UserID
     * @throws GenericException
     */
    public Long getHighRiskTransferUser(Long userId) throws GenericException {

        Long transferUserId = null;

        /* 是否有高風險主管 */
        LeaveAuthEdit leaveAuthVO = leaveManagerService.findAuthLeave(null, userId,
                        CodeCst.VERIFACTION_MANAGER_TYPE_8);

        /* 判斷高風險主管不可同本人 */
        if (leaveAuthVO != null && leaveAuthVO.getMasterEmpId() != null
                        && leaveAuthVO.getMasterEmpId().equals(userId) == false) {
            if (leaveAuthVO.getMasterEmpId().longValue() > 0) {
                transferUserId = leaveAuthVO.getMasterEmpId();
                return transferUserId;
            }
        }

        return transferUserId;
    }

    /**
     * 核保決定-取得實質受益人辨識結果交叉覆核人員的UserID
     *
     * @param userId 使用者ID
     * @return 實質受益人辨識結果交叉覆核人員使用者ID
     * @throws GenericException
     */
    public Long getLegalBeneEvalTransferUser(Long userId) throws GenericException {

        Long transferUserId = null;

        /* 是否有實質受益人辨識結果交叉覆核人員使用者 */
        LeaveAuthEdit leaveAuthVO = leaveManagerService.findAuthLeave(null, userId, CodeCst.VERIFACTION_MANAGER_TYPE_10);

        /* 判斷覆核人員不可同本人 */
        if (leaveAuthVO != null && leaveAuthVO.getMasterEmpId() != null && !leaveAuthVO.getMasterEmpId().equals(userId)) {
            if (leaveAuthVO.getMasterEmpId() > 0) {
                transferUserId = leaveAuthVO.getMasterEmpId();
                return transferUserId;
            }
        }

        return transferUserId;
    }
    
    /**
     * 核保決定-依覆核管理類別找覆核人員的UserID
     *
     * @param userId 使用者ID
     * @return 叉覆核人員使用者ID
     * @throws GenericException
     */
    public Long getVerifactionManagerTransferUserByType(Long userId, int verifactionManagerType) throws GenericException {

        Long transferUserId = null;

        /* 是否有覆核人員使用者 */
        LeaveAuthEdit leaveAuthVO = leaveManagerService.findAuthLeave(null, userId, verifactionManagerType );

        /* 判斷覆核人員不可同本人 */
        if (leaveAuthVO != null && leaveAuthVO.getMasterEmpId() != null && !leaveAuthVO.getMasterEmpId().equals(userId)) {
            if (leaveAuthVO.getMasterEmpId() > 0) {
                transferUserId = leaveAuthVO.getMasterEmpId();
                return transferUserId;
            }
        }

        return transferUserId;
    }

}
