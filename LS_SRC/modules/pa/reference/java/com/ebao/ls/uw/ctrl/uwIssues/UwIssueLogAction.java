package com.ebao.ls.uw.ctrl.uwIssues;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.ls.pub.action.ElapsedTimeLoggerAction;

/**
 * <p>Title: 核保問題頁面Action</p>
 * <p>Description: 核保問題頁面Action</p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company: TGL Co., Ltd.</p>
 * <p>Create Time: Feb 19, 2016</p> 
 * @author 
 * <p>Update Time: Feb 19, 2016</p>
 * <p>Updater: Alex Cheng</p>
 * <p>Update Comments: 2016-11-29 Kate Hsiao 增加保全規則校驗邏輯 </p>
 */
public class UwIssueLogAction extends ElapsedTimeLoggerAction {
	public static final String BEAN_DEFAULT = "/uw/uwIssues";

	@Resource(name = UWIssueAction.BEAN_DEFAULT)
	private UWIssueAction uwIssueAction;

	@Override
	protected ActionForward doProcess(ActionMapping mapping, ActionForm form,
					HttpServletRequest request, HttpServletResponse response) throws Exception {
		return uwIssueAction.process(mapping, form, request, response);
	}

	@Override
	protected String getModule() {
		return ElapsedTimeLoggerAction.MODULE_UNB;
	}

	@Override
	protected Class<?> getApplicationId() {
		return UWIssueAction.class;
	}

	@Override
	protected String getApplicationDesc() {
		return "UW Issue Action(核保照會存檔)";
	}

}
