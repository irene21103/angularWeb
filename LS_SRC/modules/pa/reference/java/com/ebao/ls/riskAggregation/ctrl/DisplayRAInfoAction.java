package com.ebao.ls.riskAggregation.ctrl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.ls.liaRoc.LiaRocCst;
import com.ebao.ls.liaRoc.LiarocDownloadLogCst;
import com.ebao.ls.liaRoc.ci.LiaRocDownload2020CI;
import com.ebao.ls.liaRoc.ci.LiaRocDownloadCI;
import com.ebao.ls.liaRoc.ds.LiarocDownloadLogHelper;
import com.ebao.ls.pa.nb.util.NBUtils;
import com.ebao.ls.pa.pub.bs.InsuredService;
import com.ebao.ls.pa.pub.bs.PolicyService;
import com.ebao.ls.pa.pub.bs.coverage.CoverageService;
import com.ebao.ls.pa.pub.vo.CoverageVO;
import com.ebao.ls.pa.pub.vo.InsuredVO;
import com.ebao.ls.pa.pub.vo.PolicyVO;
import com.ebao.ls.pub.CodeCst;
import com.ebao.ls.pub.data.InsuredVOComparator;
import com.ebao.ls.pub.util.BizUtils;
import com.ebao.ls.riskAggregation.bs.AggrSaPremResultService;
import com.ebao.ls.riskAggregation.bs.RiskAggregationService;
import com.ebao.ls.riskAggregation.data.bo.AggregationResult;
import com.ebao.ls.riskAggregation.vo.AggrSaPremResultVO;
import com.ebao.ls.riskAggregation.vo.PolicyHistoryVO;
import com.ebao.ls.riskAggregation.vo.RAInfoVO;
import com.ebao.ls.riskAggregation.vo.RiskInfoVO;
import com.ebao.ls.uw.ci.UwPolicyCI;
import com.ebao.ls.uw.ds.UwWorkSheetService;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.framework.GenericAction;
import com.ebao.pub.util.BeanUtils;

/**
 * to display risk aggregation information
 * 
 * @version 1.0
 * @created 19-JAN-2009 10:40:54
 */
public class DisplayRAInfoAction extends GenericAction {
	public static final String BEAN_DEFAULT = "/ra/displayRaInfo";

	@Resource(name = RiskAggregationService.BEAN_DEFAULT)
  public RiskAggregationService riskAggregationService;
	
	@Resource(name = com.ebao.ls.pa.nb.bs.rule.CoverageInsuredService.BEAN_DEFAULT)
	public com.ebao.ls.pa.nb.bs.rule.CoverageInsuredService coverageInsuredService;
	
    @Resource(name = LiaRocDownloadCI.BEAN_DEFAULT)
    private LiaRocDownloadCI liaRocDownloadCI;

	@Resource(name = LiaRocDownload2020CI.BEAN_DEFAULT)
	private LiaRocDownload2020CI liaRocDownload2020CI;

	Logger log4j = Logger.getLogger(getClass());

  /**
   * @param mapping
   * @param form
   * @param request
   * @param response
   * @exception Exception
   */
  public ActionForward process(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {
    RaInfoForm raForm = (RaInfoForm) form;
    Long policyId = Long.parseLong(request.getParameter("policyId"));
    String isIframe = request.getParameter("isIframe");
    
    LiarocDownloadLogHelper.initNB(LiarocDownloadLogCst.NB_UW_RA_INFO, policyId);
    
    //取得保單基本資料
    getBasicInfo(raForm, policyId);
    //取得保單被保險人資料
    initRaInfo(raForm);
    //取得被保險人風險累計資料
    initRiskInfo(raForm, policyId);
    raForm.setBaseCurrency(Integer.parseInt(BizUtils.getLocalBaseCurrency()));
    raForm.setOtherTotal(liaRocDownloadCI.getOtherTotalAnnPremByPolicyId(policyId));
    raForm.setCompanyTotal(liaRocDownloadCI.getCompTotalAnnPremByPolicyId(policyId));
    raForm.setIsIframe(isIframe);
    
    if(liaRocDownload2020CI.hasLiaroc20(policyId) == false){
	    for(RAInfoVO vo :raForm.getRaInfos()) {
	    	liaRocDownloadCI.saveLiaRocDoanloadLog(vo.getInsureCetiCode(), 
					vo.getInsureName(),
					policyService.getPolicyCodeByPolicyId(policyId), 
					LiaRocCst.DL_LOG_MSG_RA_INFO
					);
	    }
    }
    
    try {
		LiarocDownloadLogHelper.flush();
	} catch (Exception e) {
		log4j.error(e);
	}
    
    
    return mapping.findForward("display");
  }

  /**
   * initialize RA info list
   * 
   * @param raForm
   */
  public void initRaInfo(RaInfoForm raForm) {
    List<RAInfoVO> raInfos = new ArrayList<RAInfoVO>();
    List<InsuredVO> insureds = insuredService.findByPolicyId(raForm
        .getPolicyId());
    java.util.Collections.sort(insureds, new InsuredVOComparator());    
    for (InsuredVO insuredVO : insureds) {
      RAInfoVO raInfo = new RAInfoVO();
      raInfo.setInsureCetiCode(insuredVO.getCertiCode());
      raInfo.setPartyId(insuredVO.getPartyId());
      raInfo.setInsureName(insuredVO.getName());
      raInfos.add(raInfo);
    }
    raForm.setRaInfos(raInfos);
  }

  /**
   * initialize risk info
   * 
   * @param policyId
   */
  public void initRiskInfo(RaInfoForm raForm,Long policyId) {
    List<RAInfoVO> raInfos = raForm.getRaInfos();
    //Map<String, Long> otherTotal = new HashMap<String, Long>();
    Map<String, Long> companyTotal = new HashMap<String, Long>();    
    for (RAInfoVO infoVO : raInfos) {
    	String certiCode =  infoVO.getInsureCetiCode();
    	/** 本次累積體檢保額 **/
    	Map<Integer, BigDecimal> riskInfo = insuredService.getPolicyRiskInfo(raForm.getPolicyId(), certiCode);
    	Iterator<Map.Entry<Integer, BigDecimal>> infoEntry = riskInfo.entrySet().iterator();
    	List<RiskInfoVO> riskVOs = new ArrayList<RiskInfoVO>();
    	while (infoEntry.hasNext()) {
			Map.Entry<Integer, BigDecimal> info = infoEntry.next();
			RiskInfoVO vo = new RiskInfoVO();
			vo.setRiskType(info.getKey());/* 風險類型 */
			vo.setRiskAmount0(info.getValue()); /* 風險保額 */
			riskVOs.add(vo);
		}
    	infoVO.setRiskInfoList(riskVOs);
    	/**各類別累積保額**/
    	Map<Integer, Map<Integer, BigDecimal>> historyRiskInfo = insuredService.getHistoryPolicyRiskInfo(raForm.getPolicyId(),certiCode);
    	Iterator<Map.Entry<Integer, Map<Integer, BigDecimal>>> historyInfoEntry = historyRiskInfo.entrySet().iterator();
    	List<RiskInfoVO> historyRiskVOs = new ArrayList<RiskInfoVO>();
    	while (historyInfoEntry.hasNext()) {
			Map.Entry<Integer, Map<Integer, BigDecimal>> info = historyInfoEntry.next();
			Map<Integer, BigDecimal> categoryInfo = info.getValue();
			RiskInfoVO vo = new RiskInfoVO();
			vo.setRiskType(info.getKey());
			vo.setRiskAmount0(categoryInfo.get(CodeCst.AGGREGATION_CATEGORY_APPLYING));
			vo.setRiskAmount1(categoryInfo.get(CodeCst.AGGREGATION_CATEGORY_EFFECTIVE));
			vo.setRiskAmount2(categoryInfo.get(CodeCst.AGGREGATION_CATEGORY_LAPSED));
			vo.setRiskAmount3(categoryInfo.get(CodeCst.AGGREGATION_CATEGORY_TOTAL));
			historyRiskVOs.add(vo);
		}
    	infoVO.setHistoryRiskInfoList(historyRiskVOs);

		List<AggrSaPremResultVO> aggrPremList = aggrSaPremResultService.findByRuleNameAndPolicyId(AggrSaPremResultService.RULE_NAME_334, policyId);//BC479：新增334結果儲存至T_RMS_AGGR_SA_PREM_RESULT
		infoVO.setAggrPremList(aggrPremList);

    	String partyId = infoVO.getPartyId().toString();
    	//本公司年繳保費
    	Long policyChgId = null;
    	
    	BigDecimal annualPremAllCompanies = coverageInsuredService.getAnnualPremAllCompanies(certiCode,policyId,policyChgId);
    	
    	//RTC-217681-以小數點後2位4捨5入至整數
    	annualPremAllCompanies = NBUtils.specialScale(annualPremAllCompanies, 2, 0, RoundingMode.HALF_UP);
	
    	companyTotal.put(partyId, annualPremAllCompanies.longValue());
    	
    }
    
    
//    List<AggregationResult> riskList = riskAggregationService
//        .getRAResult(raForm.getPolicyId());
//    for (AggregationResult risk : riskList) {
//      for (RAInfoVO infoVO : raInfos) {
//        // if exits record for this party
//        if (risk.getPartyId().compareTo(infoVO.getPartyId()) == 0) {
//          // to check whether it has the risk info list
//          if (infoVO.getRiskInfoList() != null) {
//            int riskIndex = 0;
//            for (RiskInfoVO riskInfo : infoVO.getRiskInfoList()) {
//              if (risk.getRiskType().compareTo(riskInfo.getRiskType()) == 0) {
//                initRiskAmount(risk, riskInfo);
//                break;
//              }
//              riskIndex++;
//            }
//            // if not this record,then add it to list
//            if (riskIndex == infoVO.getRiskInfoList().size()) {
//              RiskInfoVO vo = new RiskInfoVO();
//              vo.setRiskType(risk.getRiskType());
//              initRiskAmount(risk, vo);
//              infoVO.getRiskInfoList().add(vo);
//            }
//            break;
//          } else {
//            RiskInfoVO vo = new RiskInfoVO();
//            vo.setRiskType(risk.getRiskType());
//            initRiskAmount(risk, vo);
//            List<RiskInfoVO> riskVOs = new ArrayList<RiskInfoVO>();
//            riskVOs.add(vo);
//            infoVO.setRiskInfoList(riskVOs);
//          }
//        }
//      }
//    }
    raForm.setRaInfos(raInfos);
  }

  /**
   * initialize policy history VO
   * 
   * @param raForm
   */
  public void initPolicyHistoryInfo(RaInfoForm raForm) {
    List<PolicyHistoryVO> historyVOList;
    for (RAInfoVO raInfoVO : raForm.getRaInfos()) {
      historyVOList = new ArrayList<PolicyHistoryVO>();
      List<PolicyVO> policies = policyService.findByInsuredId(raInfoVO
          .getPartyId());
      for (PolicyVO policy : policies) {
        PolicyHistoryVO historyVO = new PolicyHistoryVO();
        BeanUtils.copyProperties(historyVO, policy);
        historyVO.setHasUW(uwPolicyCI.hasManualUW(policy.getPolicyId()));
        CoverageVO coverageVO = coverageService
            .findMasterCoverageByPolicyId(policy.getPolicyId());
        if (coverageVO != null) {
          historyVO.setMasterSA(coverageVO.getCurrentPremium().getSumAssured());
        }
        BigDecimal totalAnnualPremium = policyService
            .getTotalAnnualPremiumByPolicyId(policy.getPolicyId());
        historyVO.setTotalAnnualPremium(totalAnnualPremium);
        if (!BizUtils.isAccessible(AppContext.getCurrentUser().getUserId(),
            policy.getOrganId())) {
          historyVO.setAccessible(CodeCst.YES_NO__NO);
        }
        historyVOList.add(historyVO);
      }
      raInfoVO.setHistoryList(historyVOList);
    }
  }

  /**
   * set value to suitable field
   * 
   * @param risk
   * @param riskInfo
   */
  public void initRiskAmount(AggregationResult risk, RiskInfoVO riskInfo) {
    switch (risk.getAggregationCategory()) {
      case CodeCst.AGGREGATION_CATEGORY_APPLYING :
        riskInfo.setRiskAmount0(risk.getRiskAmount());
        break;
      case CodeCst.AGGREGATION_CATEGORY_EFFECTIVE :
        riskInfo.setRiskAmount1(risk.getRiskAmount());
        break;
      case CodeCst.AGGREGATION_CATEGORY_LAPSED :
        riskInfo.setRiskAmount2(risk.getRiskAmount());
        break;
      default :
        riskInfo.setRiskAmount3(risk.getRiskAmount());
    }
  }

  /**
   * get basic info
   * 
   * @param raForm
   * @param policyId
   */
  public void getBasicInfo(RaInfoForm raForm, Long policyId) {
//    Date now = AppContext.getCurrentUserLocalTime();
    PolicyVO policy = policyService.load(policyId);
//    PolicyVO policy = policyService.retrievePolicyAtAnyTime(policyId, now);
    BeanUtils.copyProperties(raForm, policy);
    raForm.setMasterProduct(coverageService.findMasterCoverageByPolicyId(
        policyId).getProductId());
  }

	@Resource(name = UwWorkSheetService.BEAN_DEFAULT)
	private UwWorkSheetService uwWorkSheetService;

	@Resource(name = PolicyService.BEAN_DEFAULT)
	private PolicyService policyService;

	@Resource(name = CoverageService.BEAN_DEFAULT)
	private CoverageService coverageService;

	@Resource(name = UwPolicyCI.BEAN_DEFAULT)
	private UwPolicyCI uwPolicyCI;

	@Resource(name = InsuredService.BEAN_DEFAULT)
	private InsuredService insuredService;

	@Resource(name = AggrSaPremResultService.BEAN_DEFAULT)
	private AggrSaPremResultService aggrSaPremResultService;

}