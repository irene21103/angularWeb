package com.ebao.ls.uw.ds;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.ebao.ls.foundation.service.GenericServiceImpl;
import com.ebao.ls.pa.nb.ci.UnbProcessCI;
import com.ebao.ls.pa.pub.data.bo.UwCheckListStatus;
import com.ebao.ls.pa.pub.data.bo.UwPatchImage;
import com.ebao.ls.pa.pub.vo.UwCheckListStatusVO;
import com.ebao.ls.pa.pub.vo.UwPatchImageVO;
import com.ebao.ls.pub.CodeCst;
import com.ebao.ls.uw.data.TUwCheckListStatusDelegate;
import com.ebao.ls.uw.data.TUwPatchImageDelegate;

public class UwCheckListStatusServiceImpl extends GenericServiceImpl<UwCheckListStatusVO, UwCheckListStatus, TUwCheckListStatusDelegate<UwCheckListStatus>> implements UwCheckListStatusService{
    @Resource(name = TUwCheckListStatusDelegate.BEAN_DEFAULT)
    private TUwCheckListStatusDelegate tUwCheckListStatusDelegate;

	

	@Override
	public List<UwCheckListStatusVO> getCheckListStatusByPolicyId(String policyId) {
		return tUwCheckListStatusDelegate.getCheckListStatusByPolicyId(policyId);
	}

	@Override
	public String getCheckListStatusTypeName(Long typeId) {
		String typeName = "";
		if(typeId != null){
			
		typeName =dao.getCheckListTypeName(typeId);
		}
		return typeName;
	}

	@Override
	protected UwCheckListStatusVO newEntityVO() {
		return new UwCheckListStatusVO();
	}
	
	@Override
	public UwCheckListStatusVO getCheckListStatusByListId(String listId) {
		UwCheckListStatus result = dao.load(Long.valueOf(listId));
		return convertToVO(result);
	}
	
	
	@Override
	public UwCheckListStatusVO save(UwCheckListStatusVO uwCheckListStatusVO) {
		Boolean isInsert = uwCheckListStatusVO.getListId() == null;
		super.save(uwCheckListStatusVO);
		if (isInsert) {
			unbProcessCI.checkListInsert(uwCheckListStatusVO.getPolicyId());
		} 
		return uwCheckListStatusVO;
		
	}
	
	@Override
	public List<UwCheckListStatusVO> getCheckListStatusByUnderwriteId(Long underwriteId) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("underwriteId", underwriteId);
		List<UwCheckListStatus> result = dao.find(map);
		return convertToVOList(result);
	}
	
	@Resource(name = UnbProcessCI.BEAN_DEFAULT)
	private UnbProcessCI unbProcessCI;
	
}
