package com.ebao.ls.uw.ctrl.letter;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.UserTransaction;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.jfree.util.Log;

import com.ebao.ls.letter.common.CommonLetterService;
import com.ebao.ls.pa.nb.bs.rule.ProposalRuleMsgService;
import com.ebao.ls.pa.nb.bs.rule.ProposalRuleResultService;
import com.ebao.ls.pa.nb.ci.ValidatorService;
import com.ebao.ls.pa.nb.ctrl.letter.NbLetterHelper;
import com.ebao.ls.pa.nb.util.NBUtils;
import com.ebao.ls.pa.nb.vo.ProposalRuleMsgVO;
import com.ebao.ls.pa.nb.vo.ProposalRuleResultVO;
import com.ebao.ls.pa.pub.bs.PolicyService;
import com.ebao.ls.pa.pub.vo.InsuredVO;
import com.ebao.ls.pa.pub.vo.PolicyVO;
import com.ebao.ls.pa.pub.vo.UwAddInformLetterVO;
import com.ebao.ls.pub.CodeCst;
import com.ebao.ls.uw.ctrl.pub.UwActionHelper;
import com.ebao.ls.uw.ds.UwAddInformLetterService;
import com.ebao.ls.uw.ds.UwIssuesLetterService;
import com.ebao.ls.uw.ds.UwPolicyService;
import com.ebao.ls.uw.vo.UwPolicyVO;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.framework.ExceptionFactory;
import com.ebao.pub.framework.GenericAction;
import com.ebao.pub.framework.GenericException;
import com.ebao.pub.util.StringUtils;
import com.ebao.pub.util.Trans;

/**
 * <p>Title: 補充高知是項letter</p>
 * <p>Description: 補充高知是項letter</p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company: TGL Co., Ltd.</p>
 * <p>Create Time: Jun 07, 2016</p> 
 * @author 
 * <p>Update Time: Jun 07, 2016</p>
 * <p>Updater: Sunny Wu</p>
 * <p>Update Comments: </p>
 */
public class DisplayAddInformLetterAction extends GenericAction{
	/**
	 * <code>serialVersionUID</code> 的註解
	 */
	private static final long serialVersionUID = 1L;

	public static final String BEAN_DEFAULT = "/uw/displayAddInformLetter";

	protected final static String SUBACTION__INIT = "queryInit";
	protected final static String SUBACTION__SAVE = "save";

	@Resource(name = UwIssuesLetterService.BEAN_DEFAULT)
	private UwIssuesLetterService uwIssuesLetterService;

	/**
	 * Action進入點
	 */
	public ActionForward process(ActionMapping mapping, ActionForm form,
					HttpServletRequest request, HttpServletResponse response)
					throws GenericException {
		try {
			AddInformLetterForm aForm = (AddInformLetterForm) form;
			String subAction = aForm.getSubAction();
			String forward = "display";
			
			if(StringUtils.isNullOrEmpty(subAction) || SUBACTION__INIT.equals(subAction)){
				this.queryInit(aForm, request);
				forward = "display";
				aForm.setRetMsg(null);
			}else if(SUBACTION__SAVE.equals(subAction)){
				UserTransaction trans = Trans.getUserTransaction();
				try {
					trans.begin();
					Long policyId = aForm.getPolicyId();
					Long underwriteId = aForm.getUnderwriteId();
					Long listId = aForm.getListId();
					String name = aForm.getName();

					
					UwAddInformLetterVO letter = null;
					ProposalRuleResultVO resultVO = null;
					Long msgId = null;
					Long docId = null;
					if (listId != null) {
						letter = uwAddInformLetterService.load(listId);
						msgId = letter.getMsgId();
						docId = letter.getDocumentId();
					}
					
					
					if(docId != null){
						//檢核照會是否已發放
						String documentStatus = uwIssuesLetterService.getMasterDocStatus(msgId);
						if (CodeCst.DOCUMENT_STATUS__TO_BE_PRINTED.equals(documentStatus)) {
							//未列印照會更新內容
							//WSLetterUnit unit = uwActionHelper.getFmtUnb0381Letter(policyId, underwriteId, insuredVO.getListId());
							//commonLetterService.updateLetterContent(docId, unit);
							msgId = letter.getMsgId();
							docId = letter.getDocumentId();
						} else  if(CodeCst.DOCUMENT_STATUS__ABORTED.equals(documentStatus)
									|| CodeCst.DOCUMENT_STATUS__DELETED.equals(documentStatus)){
						    //信函已註銷，使用原核保訊息
							msgId = letter.getMsgId();
							docId = null;
						}  else {
							//其它則新增新的補充告知事項核保訊息,清空原documentId
							resultVO = this.saveProposalRuleResult(name, policyId);
							msgId = resultVO.getListId();
							docId = null;
						}
					} else {
						if (msgId != null) {
							resultVO = proposalRuleResultService.load(msgId);
							if (resultVO != null &&
									CodeCst.PROPOSAL_RULE_STATUS__PENDING != resultVO.getRuleStatus())  {
								msgId = null;
							}
						}
						if(msgId == null){
							//規則已被關閉虛要重新產生
							resultVO = this.saveProposalRuleResult(name, policyId);
							msgId = resultVO.getListId();
						}
					}

					if(letter == null){
						letter = new UwAddInformLetterVO();
						if(underwriteId != null){
							letter.setUnderwriteId(underwriteId);
						}
						letter.setSourceType(aForm.getSourceType());
						letter.setSourceId(aForm.getSourceId());
						letter.setStatus("0");
					}
					letter.setContent(aForm.getDocumentContent());
					letter.setMsgId(msgId);
					letter.setDocumentId(docId);
					letter = uwAddInformLetterService.save(letter);

					trans.commit();
					
					
				} catch (Exception e) {
					Log.error(DisplayAddInformLetterAction.class, e);
					trans.rollback();
		            aForm.setRetMsg("MSG_1258279");
		            aForm.setSubmitSuccess(false);
		        }
				this.queryInit(aForm, request);
				forward = "display";
				
			}
			
			return mapping.findForward(forward);
		} catch (Exception e) {
			throw ExceptionFactory.parse(e);
		}
	}

	/**
	 * <p>Description : 新增補充告知Letter核保問題</p>
	 * @param insuredName
	 * @param sickFormName
	 * @param policyId
	 * @return
	 */
	private ProposalRuleResultVO saveProposalRuleResult(String name, Long policyId){
		ProposalRuleMsgVO msgVO = null;
		ProposalRuleResultVO vo = null;
		
		vo = new ProposalRuleResultVO();
		msgVO = proposalRuleMsgService.findProposalRuleMsg(CodeCst.PROPOSAL_RULE_MSG_UW_ADDINFORM_LETTER
				, AppContext.getCurrentUserLocalTime());

		if (msgVO != null) {
			boolean isOpqV2 = validatorService.isOpqV2(policyId);
			vo.setPolicyId(policyId);
			vo.setRuleType(msgVO.getRuleType());
			vo.setRuleLetterCode(msgVO.getRuleCode());
			vo.setRuleStatus(CodeCst.PROPOSAL_RULE_STATUS__PENDING);
			vo.setRuleName(msgVO.getRuleName());
			vo.setPrintable(msgVO.getPrintable());
			
			String violatedDesc = NBUtils.getViolatedDescByProposalRuleMsgVO(msgVO, isOpqV2);
			String letterContent = NBUtils.getLetterContentByProposalRuleMsgVO(msgVO, isOpqV2);
			
			violatedDesc = violatedDesc.replace("{0}", StringUtils.nullToEmpty(name));
			letterContent = letterContent.replace("{0}", StringUtils.nullToEmpty(name));
						
			vo.setViolatedDesc(violatedDesc);
			vo.setLetterContent(letterContent);
			vo.setUpdateTime(new Date());
			
		    vo = proposalRuleResultService.save(vo);
		    vo = proposalRuleResultService.load(vo.getListId());
		}
		return vo;
	}
	
	/**
	 * <p>Description : 起始畫面所需資料</p>
	 * @param aForm
	 * @param request
	 */
	private void queryInit(AddInformLetterForm aForm, HttpServletRequest request){
		Long policyId = aForm.getPolicyId();
		Long underwriteId = aForm.getUnderwriteId();
		if(policyId == null && underwriteId == null){
			throw new RuntimeException("NO policyId & underwriteId");
		}
		if(policyId == null){
			UwPolicyVO uwPolicyVO = uwPolicyService.findUwPolicy(underwriteId);
			policyId = uwPolicyVO.getPolicyId();
		}
		
		PolicyVO policyVO = policyService.load(policyId);
		
		if(policyVO != null){
			List<InsuredVO> insuredVOs = policyVO.gitSortInsuredList();
			/*改用 policyVO.gitSortInsuredList();
			List<InsuredVO> insuredVOs = policyVO.getInsureds();
			Collections.sort(insuredVOs, new Comparator<InsuredVO>() {
	            @Override
	            public int compare(InsuredVO m1, InsuredVO m2) {
	            	return new CompareToBuilder()
					.append("0".equals(m1.getInsuredCategory())?"9":m1.getInsuredCategory(),
							"0".equals(m2.getInsuredCategory())?"9":m2.getInsuredCategory())
				    .append(m1.getNbInsuredCategory(),m2.getNbInsuredCategory())
					.toComparison();
	            }
	        });
	        */
			aForm.setInsuredVOs(insuredVOs);
			
			/* 核保作業中有被保險人列表的功能增加判斷是否有險種，無險種disabled被保險人checkbox不可勾選 */
			UwActionHelper.countInsuredCoverage(request, policyVO);
			
		}
	}
	
	
	@Resource(name = UwAddInformLetterService.BEAN_DEFAULT)
	private UwAddInformLetterService uwAddInformLetterService;
	
	@Resource(name = PolicyService.BEAN_DEFAULT)
	private PolicyService policyService;
		
	@Resource(name = UwPolicyService.BEAN_DEFAULT)
	private UwPolicyService uwPolicyService;
	
	@Resource(name = ProposalRuleMsgService.BEAN_DEFAULT)
	protected ProposalRuleMsgService proposalRuleMsgService;
	
	@Resource(name = ProposalRuleResultService.BEAN_DEFAULT)
	protected ProposalRuleResultService proposalRuleResultService;

	@Resource(name = ValidatorService.BEAN_DEFAULT)
	protected  ValidatorService validatorService;

	@Resource(name = NbLetterHelper.BEAN_DEFAULT)
    private NbLetterHelper nbLetterHelper;
	
	@Resource(name=CommonLetterService.BEAN_DEFAULT)
	protected CommonLetterService commonLetterService;

	
}
