package com.ebao.ls.uw.ctrl.helper;

import static com.ebao.ls.pa.nb.bs.NbValidateRoleConfService.RULE_TYPE_1;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ebao.foundation.module.para.Para;
import com.ebao.ls.cmu.pub.document.DocumentFormFile;
import com.ebao.ls.cs.commonflow.ds.cmnquery.PolicyQueryService;
import com.ebao.ls.file.bs.FileService;
import com.ebao.ls.image.bs.ImageService;
import com.ebao.ls.image.data.bo.Image;
import com.ebao.ls.image.vo.ImageVO;
import com.ebao.ls.pa.nb.bs.NbValidateRoleConfService;
import com.ebao.ls.pa.nb.ci.ValidatorService;
import com.ebao.ls.pa.nb.util.NBUtils;
import com.ebao.ls.pa.nb.util.UnbRoleType;
import com.ebao.ls.pa.nb.vo.ElderAudioExtInfoVO;
import com.ebao.ls.pa.nb.vo.UnbRoleVO;
import com.ebao.ls.pa.pub.bs.AgentNotifyService;
import com.ebao.ls.pa.pub.bs.ElderScaleRoleService;
import com.ebao.ls.pa.pub.bs.LegalRepresentativeService;
import com.ebao.ls.pa.pub.bs.NbPolicySpecialRuleService;
import com.ebao.ls.pa.pub.bs.PolicyService;
import com.ebao.ls.pa.pub.vo.AgentNotifyVO;
import com.ebao.ls.pa.pub.vo.ElderScaleRoleVO;
import com.ebao.ls.pa.pub.vo.PolicyVO;
import com.ebao.ls.product.model.query.output.LifeProduct;
import com.ebao.ls.product.service.LifeProductService;
import com.ebao.ls.pub.CodeCst;
import com.ebao.ls.pub.CodeCstTgl;
import com.ebao.ls.pub.cst.Billcard;
import com.ebao.ls.pub.util.BizUtils;
import com.ebao.ls.uw.ctrl.elderAudio.ElderAudioObject;
import com.ebao.ls.uw.ds.UwElderAudioService;
import com.ebao.ls.uw.vo.UwElderAudioVO;
import com.ebao.pub.util.DateUtils;
import com.ebao.pub.util.EnvUtils;

import edu.emory.mathcs.backport.java.util.Arrays;

/**
 * Title: 新契約<BR>
 * Description: 高齡銷售錄音審核任務工具類<BR>
 * Copyright: Copyright (c) 2022<BR>
 * Company: TGL Co., Ltd.<BR>
 * Create Time: Jul 22, 2022<BR>
 * 
 * @author Update Time: <BR>
 *         Updater: <BR>
 *         Update Comments: <BR>
 */
public class ElderAudioHelper {

	public static final String BEAN_DEFAULT = "paElderAudioHelper";

	public static final String AUDIO_OBJECTS_ATTR = "elderAudioObjects";
	public static final String REVIEW_TASK_LIST_ATTR = "reviewTaskList";
	public static final String UPLOAD_FOLDER = "upload";
	public static final SimpleDateFormat dateDateFormatyyyyMMddHHmmss = new SimpleDateFormat("yyyyMMddHHmmss");

	private static final Logger log = LoggerFactory.getLogger(ElderAudioHelper.class);

	@Resource(name = FileService.BEAN_DEFAULT)
	private FileService fileService;

	@Resource(name = PolicyService.BEAN_DEFAULT)
	private PolicyService paPolicyService;

	@Resource(name = AgentNotifyService.BEAN_DEFAULT)
	private AgentNotifyService agentNotifyService;

	@Resource(name = NbPolicySpecialRuleService.BEAN_DEFAULT)
	private NbPolicySpecialRuleService nbSpecialRuleService;

	@Resource(name = ValidatorService.BEAN_DEFAULT)
	private ValidatorService validatorService;

	@Resource(name = UwElderAudioService.BEAN_DEFAULT)
	private UwElderAudioService uwElderAudioService;

	@Resource(name = NbValidateRoleConfService.BEAN_DEFAULT)
	private NbValidateRoleConfService validateRoleConfService;

	@Resource(name = LifeProductService.BEAN_DEFAULT)
	private LifeProductService lifeProductService;

	@Resource(name = LegalRepresentativeService.BEAN_DEFAULT)
	private LegalRepresentativeService legalRepresentativeService;

	@Resource(name = PolicyQueryService.BEAN_DEFAULT)
	private PolicyQueryService policyQueryService;

	@Resource(name = ElderScaleRoleService.BEAN_DEFAULT)
	private ElderScaleRoleService elderScaleRoleService;

	@Resource(name = ImageService.BEAN_DEFAULT)
	ImageService imageService;

	/**
	 * @return 錄音會辦對象清單
	 * @throws InvocationTargetException
	 * @throws IllegalAccessException
	 */
	public List<ElderAudioObject> getElderAudioObjects(Long policyId, Date applyDate)
			throws IllegalAccessException, InvocationTargetException {
		// 人員順序：被保險人 要保人 首期授權人 續期授權人 實際繳交保費人 法定代理人 授權人之法定代理人
		List<UnbRoleVO> validateRoles = validateRoleConfService.getNbValidateRole(policyId, RULE_TYPE_1);
		// 取參數: 年齡計算方式
		String paraElderAgeMethod = Para.getParaValue(CodeCstTgl.UNB_ELDER_AGE_METHOD);

		List<ElderAudioObject> elderAudioObjects = new ArrayList<ElderAudioObject>();
		if (null != applyDate && CollectionUtils.isNotEmpty(validateRoles)) {
			for (UnbRoleVO unbRoleVO : validateRoles) {
				ElderAudioObject tempObj = new ElderAudioObject();
				BeanUtils.copyProperties(tempObj, unbRoleVO);

				if (null != tempObj.getBirthdate()) { // 排除生日空白
					// 畫面上顯示「投保年齡」，實際上看設定是要用「足歲」還是「投保年齡」
					tempObj.setAgeInsurance(BizUtils.getAge(paraElderAgeMethod, tempObj.getBirthdate(), applyDate));
				}

				elderAudioObjects.add(tempObj);
			} // for
		}

		return elderAudioObjects;
	}

	/**
	 * 取得符合條件的ImageId
	 * @param policyId
	 * @param cardId
	 * @return
	 */
	private Set<String> fillImageId(Long policyId, Long cardId) {
		Set<String> imgset = new HashSet<>();
		List<Image> imageList = imageService.queryByImageTypeId(policyId, cardId, false, null, null);
		if (CollectionUtils.isNotEmpty(imageList)) {
			for (Image img : imageList) {
				imgset.add(img.getImageId().toString());
			}
		}
		return imgset;
	}

	/**
	 * 利用業務員招攬高齡客戶投保評估量表 - 身分(ElderScaleRoleVO)資料充填高齡銷售錄音審核任務(UwElderAudioVO)<BR>
	 * 包含產製資料夾與置放錄音問卷影像於共享資料夾中，未充填非屬於高齡客戶投保評估量表的高齡銷售錄音審核任務資料<BR>
	 * 允許統號、姓名是空白
	 * @param esrUNBNi 高齡客戶投保評估量表
	 * @param certiBD <certCode,Birthday>
	 * @param consultDate 會辦日期時間
	 * @param audioNoStr 同一張保單的所有高齡客戶投保評估量表所有角色的錄音編號(不含空白且不重覆)，若有多個中間「；」隔開
	 * @param policyPath 暫存資料夾路徑，到policyCode/
	 * @param imgIds 錄音問卷影像(imageId)清單
	 * @return 高齡銷售錄音審核任務
	 */
	private List<UwElderAudioVO> fillUwElderAudio(List<ElderScaleRoleVO> esrUNBNi,
			Date consultDate, String audioNoStr, String policyPath, Set<String> imgIds) {
		List<UwElderAudioVO> elderAudioTasks = new ArrayList<>();
		if(CollectionUtils.isEmpty(esrUNBNi))return elderAudioTasks;
		for(ElderScaleRoleVO esri:esrUNBNi) {
			UwElderAudioVO taski = new UwElderAudioVO();
			taski.setPolicyId(esri.getPolicyId());
			taski.setRoleType(esri.getRoleType());
			taski.setName(StringUtils.isBlank(esri.getName())? "" : esri.getName());
			taski.setCertiCode(StringUtils.isBlank(esri.getCertiCode())? "" : esri.getCertiCode());
			taski.setConsultDate(consultDate);
			taski.setAudioNoList(audioNoStr);
			//gen temp folder and put image file
			String fixCertiCode = fixCertiCode(esri.getCertiCode());
			String tempFolder = fetchTaskFolder(policyPath,
							(StringUtils.isNotBlank(fixCertiCode)? fixCertiCode : esri.getCertiCode()) + "_000");
			checkFolder(policyPath, new String[] { tempFolder });
			//fileNameList
			List<String> fileNames = new ArrayList<>();
			List<String> missFiles = cloneImagesToTempFolder(
					genPath(policyPath, new String[] { tempFolder }), imgIds.stream().toArray(String[]::new), fileNames);
			Set<String> uniFileNames = new HashSet<>(fileNames);
			taski.setFileNameList(String.join(";", uniFileNames));
			if(missFiles.isEmpty()) {
				taski.setTaskNum(tempFolder);//未取得任務編號前要保留交換資料夾名稱
				elderAudioTasks.add(taski);
			} else {
				NBUtils.logger(this.getClass(),
						"cloneImagesToTempFolder fail [policyId:"+esri.getPolicyId()+"] "+String.join(",", missFiles));
			}
			
		}
		return elderAudioTasks;
	}

	/**
	 * <pre>
	 * 保單要保書狀態=待核保(覆核完成時)，依「高齡客戶投保評估量表暨業務員報告書」、「保險業務員核保報告書」啟動高齡銷售錄音覆審會辦任務 
	 * 提供覆核完成時自動判斷是否新增高齡銷售錄音任務(透過CRM rest-api)
	 * from T_NB_ELDER_SCALE_ROLE
	 * PCR-500628 錄音任務基本資料，允許「姓名/ID/出生日期」為空白
	 * </pre>
	 * 
	 * @param policyId
	 * @return
	 */
	public void checkElderAudioTask(long policyId) {
		
		NBUtils.logger(this.getClass(), "checkElderAudioTask start, policyId=" + Long.toString(policyId));
		
		//「依業務員招攬高齡客戶投保評估量表-身分」存
		List<ElderScaleRoleVO> esrUNBN074= new ArrayList<>();
		List<ElderScaleRoleVO> esrUNBN076= new ArrayList<>();
		StringBuffer sb = new StringBuffer();// 處理錄音流水編號

		PolicyVO policyInfo = null;
		try {
			
			policyInfo = paPolicyService.load(policyId);
			
			//檢查保單是否存在，要產taskNo與資料夾名稱，若無則須放棄
			if (null == policyInfo || StringUtils.isBlank(policyInfo.getPolicyNumber())) {
				NBUtils.logger(this.getClass(), "NO policyCode, policyId=" + Long.toString(policyId));
				return;
			}
			
			// 檢查保單填寫日是否在高齡關懷生效日(含)以後
			if (!nbSpecialRuleService.isMatchElderCareStartDate(policyInfo.getPolicyId())) {
				NBUtils.logger(this.getClass(), "is NOT MatchElderCareStartDate, policyId=" + Long.toString(policyId));
				return;
			}
			
			// 檢查是否投保需要(高齡錄音)的商品=false則不進行任務
			if (!validatorService.hasElderRecordCoverage(policyInfo)) {
				NBUtils.logger(this.getClass(), "hasElderRecordCoverage() is false");
				return;
			}
			
		}catch(Exception ep) {
			NBUtils.logger(this.getClass(), "取保單資訊與是否投保需要(高齡錄音)的商品 錯誤." + ExceptionUtils.getStackTrace(ep));
			return;// 若錯誤則須放棄
		}
		
		//該保單相關角色
		List<UnbRoleVO> validateRoles = validateRoleConfService.getNbValidateRole(policyId, RULE_TYPE_1);
		
		//取本次可啟動錄音覆審會辦的資料
		List<ElderScaleRoleVO> elderScaleRoleVO = getElderScaleRoleList(policyInfo, validateRoles);
		
		String elderAudiochannel = paPolicyService.getElderAudioChannel(policyInfo);
		for (ElderScaleRoleVO esRoleVO : elderScaleRoleVO) {
			boolean isLegalAudioNoStr=NBUtils.isLegalAudioNoStr(esRoleVO.getAudioNo(), elderAudiochannel);
			if(isLegalAudioNoStr) {
				@SuppressWarnings("unchecked")
				List<String> lsAudioNo = Arrays.asList(esRoleVO.getAudioNo().split(";"));
				isLegalAudioNoStr = CollectionUtils.isNotEmpty(NBUtils.checkAudioNo(lsAudioNo, elderAudiochannel).get("Y"));
			}
			
			if(!isLegalAudioNoStr) {
				// 錄音編號若有不正確，被剃除該角色之任務要寫LOG{policyId,role,certCode,name,error-audioNo}
				NBUtils.logger(this.getClass(), "錄音編號不正確，被剃除, policyId=" + Long.toString(policyId) 
						+ ", role=" + esRoleVO.getRoleType() + ", certCode=" + esRoleVO.getCertiCode()
						+ ", name=" + esRoleVO.getName() + ",audioNo=" + esRoleVO.getAudioNo());
				continue;// 錄音編號都要正確否則剃除該角色之任務
			}
			
			if(UnbRoleType.POLICY_HOLDER.getRoleType().equals(esRoleVO.getRoleType())
					|| UnbRoleType.INSURED.getRoleType().equals(esRoleVO.getRoleType())) {
				//被保險人或要保人
				esrUNBN074.add(esRoleVO);
			}else if(UnbRoleType.PAYER_2.getRoleType().equals(esRoleVO.getRoleType())) {
				//繳款人
				esrUNBN076.add(esRoleVO);
			}else {
				// 無角色可以符合之任務要寫LOG{policyId,role,certCode,name}
				NBUtils.logger(this.getClass(), "角色不符合，被剃除, policyId=" + Long.toString(policyId)
						+ ", role=" + esRoleVO.getRoleType() + ", certCode=" + esRoleVO.getCertiCode()
						+ ", name=" + esRoleVO.getName());
				continue;// 不屬於這些角色就不加載錄音編號
			}
			
			sb.append(esRoleVO.getAudioNo()+";");// 收錄所有未被剃除的錄音編號
			
		}
		
		// 高齡客戶投保評估量表都沒有指定的角色 或沒有錄音編號 就離開
		if ((CollectionUtils.isEmpty(esrUNBN074) && CollectionUtils.isEmpty(esrUNBN076)) || sb.length() < 10) {
			return;
		}
		
		//PCR-500628 要保人／被保險人同一人且要保人和被保險人輸入相同之錄音編號，系統只需會辦一筆任務(以被保險人為主)
		if (esrUNBN074.size() > 1) {
			if(esrUNBN074.get(0).getCertiCode().equals(esrUNBN074.get(1).getCertiCode())
					&& esrUNBN074.get(0).getAudioNo().equals(esrUNBN074.get(1).getAudioNo())) {
				esrUNBN074.removeIf(vi -> (UnbRoleType.POLICY_HOLDER.getRoleType().equals(vi.getRoleType())));
			}
		}

		// 找此保單有{UNBN074,UNBN076}的cardID對到同保單Image.typeId，分取放到2集合中
		Set<String> imgUNBN074 = new HashSet<>(); //高齡銷售過程錄音問卷
		Set<String> imgUNBN076 = new HashSet<>(); //高齡銷售過程錄音問卷＿繳款人
		
		//imageService.getCardId return Long.MIN_VALUE if null
		Long cardIdUNBN074 = imageService.getCardId(Billcard.CARD_CODE_UNBN074);
		if (cardIdUNBN074 > -1L) {
			imgUNBN074.addAll(fillImageId(policyId, cardIdUNBN074));	
		}			
		
		Long cardIdUNBN076 = imageService.getCardId(Billcard.CARD_CODE_UNBN076);
		if (cardIdUNBN076 > -1L) {
			imgUNBN076.addAll(fillImageId(policyId, cardIdUNBN076));
		}
		
		// Image無資料要寫LOG{policyId,cardIdUNBN074,cardIdUNBN076}
		if (CollectionUtils.isNotEmpty(esrUNBN074) && CollectionUtils.isEmpty(imgUNBN074)) {
			NBUtils.logger(this.getClass(),
					"UNBN074沒有文件, policyId=" + Long.toString(policyId) + ", cardIdUNBN074=" + cardIdUNBN074.toString());
		}
		
		if (CollectionUtils.isNotEmpty(esrUNBN076) && CollectionUtils.isEmpty(imgUNBN076)) {
			NBUtils.logger(this.getClass(),
					"UNBN076沒有文件, policyId=" + Long.toString(policyId) + ", cardIdUNBN076=" + cardIdUNBN076.toString());
		}
		
		// 若UNBN074,UNBN076都沒有文件就離開
		if (CollectionUtils.isEmpty(imgUNBN074) && CollectionUtils.isEmpty(imgUNBN076)) {
			return;
		}

		ElderAudioExtInfoVO extInfo = new ElderAudioExtInfoVO();
		Date consultDate = new Date();// 會辦日期時間
		String noticeYYYYMMDD = DateUtils.date2String(consultDate, "yyyyMMdd");
		extInfo.setNoticeYYYYMMDD(noticeYYYYMMDD);
		
		try {
			
			Long productVersionId = policyInfo.gitMasterCoverage().getProductVersionId();
			LifeProduct lifeProduct = lifeProductService.getProductByVersionId(productVersionId);
			extInfo.setMasterProductCode(lifeProduct.getProduct().getInternalId());
			extInfo.setMasterProductName(policyQueryService.getProductName(productVersionId));
			extInfo.setPolicyCode(policyInfo.getPolicyNumber());

			if(CollectionUtils.isNotEmpty(validateRoles)) {
				// 擷取要保人資料
				for (UnbRoleVO unbRoleVO : validateRoles) {
					if (unbRoleVO.getPolicyRole().contains(UnbRoleType.POLICY_HOLDER.getRoleType())) {
						extInfo.setOwnerId(unbRoleVO.getCertiCode());
						extInfo.setOwnerName(unbRoleVO.getName());
						break;
					}
				}
			}
			
		} catch (Exception ex) {
			NBUtils.logger(this.getClass(), "getProduct info fail." + ExceptionUtils.getStackTrace(ex));
			// 這邊的是有錯誤仍要可以接續送任務
		}

		List<UwElderAudioVO> elderAudioTasks = new ArrayList<>();
		String[] audioNos = sb.toString().split(";");
		@SuppressWarnings("unchecked")
		Map<String,List<String>> audioNo =NBUtils.checkAudioNo(Arrays.asList(audioNos), elderAudiochannel);
		String audioNoStr =String.join(";",audioNo.get("Y"));
		// fetch tempt folder
		String CRM_SHARE_ROOT=EnvUtils.getSendToCRMSharePath();//FT_CRM_PATH=/ebao_crm
		if (null == CRM_SHARE_ROOT || CRM_SHARE_ROOT.contains("null")) {
			NBUtils.logger(this.getClass(), "NO CRM_SHARE_ROOT, FT_CRM_PATH cannot be null, please check out env.properties");
			return;
		}
		
		String basePath = genPath(CRM_SHARE_ROOT + File.separator, new String[] { "elder_audio" });
		String[] workPath = { noticeYYYYMMDD.substring(0, 4), noticeYYYYMMDD.substring(4, 6),
				noticeYYYYMMDD.substring(6), extInfo.getPolicyCode() };
		checkFolder(basePath, workPath);
		String tempPath = genPath(basePath, workPath);

		// 有((1-要保人,2-被保險人)角色要找:esrUNBN074,imgUNBN074
		if (!esrUNBN074.isEmpty() && imgUNBN074.size() > 0) {
			elderAudioTasks.addAll(fillUwElderAudio(esrUNBN074, consultDate, audioNoStr, tempPath, imgUNBN074));
		}
		
		// 有(14-繳款人)角色要找:esrUNBN076,imgUNBN076
		if (!esrUNBN076.isEmpty() && imgUNBN076.size() > 0) {
			elderAudioTasks.addAll(fillUwElderAudio(esrUNBN076, consultDate, audioNoStr, tempPath, imgUNBN076));
		}
		
		if(elderAudioTasks.isEmpty()) {
			return;
		}
		
		try {
			uwElderAudioService.sendReviewTaskToCRM(elderAudioTasks, extInfo, true);
		} catch (Exception ex) {
			NBUtils.logger(this.getClass(), "sendReviewTaskToCRM fail." + ExceptionUtils.getStackTrace(ex));
			// 這邊寫的是有關送到CRM前發生的錯誤
		}
		
		NBUtils.logger(this.getClass(), "checkElderAudioTask end, policyId=" + Long.toString(policyId));
	}

	/**
	 * 提供核保員新增高齡銷售錄音任務(透過CRM rest-api)
	 * @param tasks
	 * @param policyCode
	 * @param isAddOPQWhileCRMFail
	 * @return
	 */
	public String sendReviewTaskToCRM(List<UwElderAudioVO> tasks, String policyCode) {
		ElderAudioExtInfoVO extInfo = new ElderAudioExtInfoVO();
		extInfo.setPolicyCode(policyCode);
		extInfo.setFolderName(tasks.get(0).getTaskNum());//核保員起的任務分割為兩段，第一段上傳檔案成功後會將資料夾名稱置於TaskNum中
		String noticeYYYYMMDD = DateUtils.date2String(tasks.get(0).getConsultDate(), "yyyyMMdd");
		if (StringUtils.isNotBlank(noticeYYYYMMDD) && !noticeYYYYMMDD.matches("[0-9]{8}")) {
			return NBUtils.getTWMsg("MSG_1267157");//照會日期格式錯誤(YYYYMMDD)
		}
		extInfo.setNoticeYYYYMMDD(noticeYYYYMMDD);
		List<UnbRoleVO> validateRoles = validateRoleConfService.getNbValidateRole(
				tasks.get(0).getPolicyId(), RULE_TYPE_1);
		for (UnbRoleVO unbRoleVO : validateRoles) {
			if (unbRoleVO.getPolicyRole().contains(UnbRoleType.POLICY_HOLDER.getRoleType())) {
				extInfo.setOwnerId(unbRoleVO.getCertiCode());
				extInfo.setOwnerName(unbRoleVO.getName());
				break;
			}
		}
		try {
			PolicyVO policyInfo = paPolicyService.load(tasks.get(0).getPolicyId());
			Long productVersionId = policyInfo.gitMasterCoverage().getProductVersionId();
			LifeProduct lifeProduct = lifeProductService.getProductByVersionId(productVersionId);
			extInfo.setMasterProductCode(lifeProduct.getProduct().getInternalId());
			extInfo.setMasterProductName(policyQueryService.getProductName(productVersionId));
		} catch (Exception ex) {
			NBUtils.logger(this.getClass(),
					"setRateTableCommonInput.lifeProductService.getProductByVersionId fail."
					+ ExceptionUtils.getStackTrace(ex));
		}
		String sentResult = uwElderAudioService.sendReviewTaskToCRM(tasks, extInfo, false);
		return sentResult;
	}

	/**
	 * 將錄音會辦中使用者選擇的保單影像檔和上傳的檔案置放於CRM共享資料夾<BR>
	 * 檔案放置位置為 elder_audio\照會日期西元年YYYY\照會日期月MM\照會日期日DD\policyCode\certiCode_Num3碼\
	 * 
	 * @param policyCode
	 * @param audioReviewTask
	 * @param uploadFileList  核保員上傳的檔案
	 * @param selImgIds       核保員選擇的要保書影像檔 IDs
	 * @return 回應資料夾名稱於List第一筆 ex: CertiCode_001<BR>
	 * 成功只有第一筆；若有錯誤，會置於List第一筆之後
	 */
	public List<String> saveTempFile(String policyCode, UwElderAudioVO audioReviewTask,
			List<DocumentFormFile> uploadFileList, String[] selImgIds) {
		List<String> returnMsg = new ArrayList<>();
		//處理統號空白
		boolean isEmptyCertiCode = StringUtils.isBlank(audioReviewTask.getCertiCode());
		String fixCertiCode = fixCertiCode(audioReviewTask.getCertiCode());
		String emptyCertiCode = "EmptyCertiCode";
		// 確認路徑與生成 /efb_uat/ebao_crm/
		// 檔案放置位置為 elder_audio/照會日期西元年YYYY/照會日期月MM/照會日期日DD/policyCode/certiCode_Number/
		String noticeYYYYMMDD=DateUtils.date2String(audioReviewTask.getConsultDate(), "yyyyMMdd");
		if (StringUtils.isNotBlank(noticeYYYYMMDD) && !noticeYYYYMMDD.matches("[0-9]{8}")) {
			returnMsg.add((isEmptyCertiCode? emptyCertiCode : audioReviewTask.getCertiCode()) + "_000");
			returnMsg.add(NBUtils.getTWMsg("MSG_1267157"));//照會日期格式錯誤(YYYYMMDD)
			return returnMsg;
		}
		String CRM_SHARE_ROOT=EnvUtils.getSendToCRMSharePath();//FT_CRM_PATH=/ebao_crm
		if (null == CRM_SHARE_ROOT || CRM_SHARE_ROOT.contains("null")) {
			returnMsg.add((isEmptyCertiCode? emptyCertiCode : audioReviewTask.getCertiCode()) + "_000");
			returnMsg.add(NBUtils.getTWMsg("MSG_1253632")+"(FT_CRM_PATH is null)");//請確認檔案路徑是否存在
			return returnMsg;
		}
		String basePath = genPath(CRM_SHARE_ROOT + File.separator, new String[] { "elder_audio" });
		String[] subPath = { noticeYYYYMMDD.substring(0, 4), noticeYYYYMMDD.substring(4, 6),
				noticeYYYYMMDD.substring(6), policyCode, "", UPLOAD_FOLDER };
		String[] workPath = (String[]) Arrays.copyOf(subPath, 4);
		String tempFolder = fetchTaskFolder(genPath(basePath, workPath),
						(StringUtils.isNotBlank(fixCertiCode)? fixCertiCode : audioReviewTask.getCertiCode()) + "_000");
		subPath[4] = tempFolder;
		returnMsg.add(tempFolder);
		// new temp folder basePath+subath
		String uploadPath = genPath(basePath, subPath);
		if (checkFolder(basePath, subPath)) {
			// 核保員上傳的檔案
			for (DocumentFormFile dffi : uploadFileList) {
				String extnd="";
				try {
					extnd = fetchFileExtnd(dffi.getFormFile().getFileName());
					if (extnd.length() > 3 && ".pdf,.tiff,.png".contains(extnd.toLowerCase())) {//pdf, tif, tiff, png
						List<String> returnMsgUpld = uploadToTempFile(uploadPath,
								dffi.getFormFile().getFileData(), dffi.getFileName()+extnd);
						if (!CollectionUtils.isEmpty(returnMsgUpld)) {
							NBUtils.logger(this.getClass(), "uploadToTempFile fail, "
									+ DateUtils.date2String(audioReviewTask.getConsultDate(), "YYYYMMDD") + "_" + policyCode
									+ "_" + (isEmptyCertiCode? emptyCertiCode : audioReviewTask.getCertiCode())
									+ "_" + audioReviewTask.getAudioNoList() + "_"
									+ dffi.getFileName()+extnd);
							returnMsg.addAll(returnMsgUpld);
						}
					}else {
						returnMsg.add(NBUtils.getTWMsg("MSG_1267158")+":"+dffi.getFileName()+extnd);//檔案名稱錯誤，副檔名只允許.pdf,.tif,.png
					}
				} catch (Exception ex) {
					NBUtils.logger(this.getClass(), "uploadToTempFile exception["+ex.getMessage()+"],  "
							+ DateUtils.date2String(audioReviewTask.getConsultDate(), "YYYYMMDD") + "_" + policyCode
							+ "_" + (isEmptyCertiCode? emptyCertiCode : audioReviewTask.getCertiCode())
							+ "_" + audioReviewTask.getAudioNoList() + "_"
							+ dffi.getFileName()+extnd);
					returnMsg.add(NBUtils.getTWMsg("MSG_1267160") + ":" + dffi.getFileName()+extnd);
				}
			}

		}
		//核保員選擇的要保書影像檔
		if (null != selImgIds && selImgIds.length > 0) {
			List<String> returnMsgImgs = cloneImagesToTempFolder(
					genPath(basePath, (String[]) Arrays.copyOf(subPath, 5)), selImgIds, null);
			if (!CollectionUtils.isEmpty(returnMsgImgs)) {
				returnMsg.add(DateUtils.date2String(audioReviewTask.getConsultDate(), "YYYYMMDD") + "_" + policyCode
						+ "_" + (isEmptyCertiCode? emptyCertiCode : audioReviewTask.getCertiCode())
						+ "_" + audioReviewTask.getAudioNoList() + "_Images");
				returnMsg.addAll(returnMsgImgs);
			}
		}
		tryExecuteChmod(uploadPath);
		return returnMsg;
	}

	/**
	 * 將使用者上傳的檔案置放於CRM共享資料夾<BR>
	 * 檔案放置位置為 elder_audio\照會日期西元年YYYY\照會日期月MM\照會日期日DD\policyCode\certiCode_Num3碼\
	 * 
	 * @param tempFolder 共享資料夾
	 * @param buf
	 * @param fileName       檔名
	 * @return
	 */
	public List<String> uploadToTempFile(String tempFolder, byte[] buf, String fileName) {
		List<String> returnMsg = new ArrayList<>();
		if (null == buf || buf.length < 1) {
			returnMsg.add(NBUtils.getTWMsg("MSG_1267159")+ ":" + fileName);//無檔案
			return returnMsg;
		}
		try {
			fileName=fileName.replaceAll("[/\\\\]", "_");
			FileUtils.writeByteArrayToFile(new File(tempFolder + fileName), buf);
		} catch (Exception ex) {
			NBUtils.logger(this.getClass(), "uploadToTempFile fail, "
					+ tempFolder + fileName + "\n" + ExceptionUtils.getStackTrace(ex));
			returnMsg.add(NBUtils.getTWMsg("MSG_1267160") + ":" + tempFolder + fileName);//檔案儲存錯誤
		}
		return returnMsg;
	}

	/**
	 * 將使用者指定的要保書影像檔置放於CRM共享資料夾<BR>
	 * 資料夾名稱為certiCode_Num3碼<BR>
	 * 檔案放置位置為 elder_audio\照會日期西元年YYYY\照會日期月MM\照會日期日DD\資料夾名稱\
	 * 
	 * @param tempFolderPath 共享資料夾路徑
	 * @param selImgIds  保單影像陣列
	 * @param fileNames  處理成功時填註文件名稱的清單參考，不需要填註時為null
	 * @return empty list if success, else fail file
	 */
	public List<String> cloneImagesToTempFolder(String tempFolderPath, String[] selImgIds, List<String> fileNames) {
		List<String> returnMsg = new ArrayList<>();
		boolean isFill = (null != fileNames);
		for (String imageId : selImgIds) {
			if (StringUtils.isBlank(imageId)) {
				continue;
			}
			//key:vo,cardName value:vo,CodeDesc
			String[] splits=imageId.split(",");
			for(String imageIdi : splits) {
				if(!imageIdi.matches("\\d+")) {
					continue;
				}
				Map<String, Object> imgMap = paPolicyService.findImageByImageId(imageIdi);
				if (imgMap == null) {
					continue;
				}
				String fileName = (String) imgMap.get("cardName");
				log.info("fileName=" + fileName);
				ImageVO img = (ImageVO) imgMap.get("vo");
				String seqNumber = "00";
				if(img.getSeqNumber() != null) {
					seqNumber = (img.getSeqNumber().intValue()<10?"0":"") + img.getSeqNumber().toString() ;
				}
				//imageTypeId_scanTime_seqNumber_imageId
				String imgName = img.getImageTypeId().toString() + "_" + dateDateFormatyyyyMMddHHmmss.format(img.getScanTime()) 
						+ "_" + seqNumber + "_" + imageIdi + fetchFileExtnd(img.getImageFileName());
				try {
					byte[] buf = fileService.getDocument(new Long(imageIdi), CodeCst.DIVA_MAPPING_TYPE__IMAGE);
					FileUtils.writeByteArrayToFile(new File(tempFolderPath + imgName), buf);
					if(isFill) fileNames.add(fileName);
				} catch (Exception ex) {
					NBUtils.logger(this.getClass(), "cloneImagesToTempFolder fail, "
							+ tempFolderPath + imgName + "\n" + ex.getMessage());
					returnMsg.add(NBUtils.getTWMsg("MSG_1267160") + ":" 
							+ tempFolderPath + imgName + "[" + fileName + "]");//檔案儲存錯誤
				}
			}
		}
		tryExecuteChmod(tempFolderPath);
		return returnMsg;
	}

	private void tryExecuteChmod(String rootDir) {
		try {
			// 設定rootDir目錄下的所有權限777
			Process p = Runtime.getRuntime().exec(String.format("chmod 777 -R %s", rootDir));
			p.waitFor();
		} catch (IOException ex) {
		} catch (InterruptedException ex) {
		}
	}

	/** 擷取副檔名<BR>
	 * 從最後找第一個「.」到最後，若無則回傳空字串
	 * @param path 要往下建置的資料夾(節點)
	 * @return
	 */
	private String fetchFileExtnd(String fileName) {
		String extnd = "";
		int point = fileName.lastIndexOf(".");
		if (point > 0 && point < fileName.length() - 1) {
			extnd = fileName.substring(point);
		}
		return extnd;
	}

	/**
	 * 確認與建置要走過的資料夾
	 * @param base 基本路徑
	 * @param path 要往下建置的資料夾(節點)
	 * @return
	 */
	private boolean checkFolder(String base, String[] path) {
		StringBuilder sb = new StringBuilder(base);
		for (String folder : path) {
			sb.append(folder).append(File.separator);
			File pthFile = new File(sb.toString());
			if (pthFile.exists() && pthFile.isDirectory()) {
				continue;
			}
			pthFile.mkdirs();
			boolean r = pthFile.setReadable(true, false);
			boolean w = pthFile.setWritable(true, false);
			boolean x = pthFile.setExecutable(true, false);
			if (!r || !w || !x) {
				NBUtils.logger(this.getClass(), "folder[" + folder + "]checkFolder mod fail, Readable="
								+ r +", Writable="+ w + ", Executable=" + x);
			}
		}
		return true;
	}

	/**
	 * 測試與取得可以使用的資料夾名稱<BR>
	 * 只處理taskFolder末三碼
	 * @param path 路徑(末尾有分隔符)
	 * @param taskFolder 暫存資料夾名稱 格式須為「ASCII英數字_數字3碼」
	 * @return 指定為特定任務的暫存資料夾名稱，格式錯誤回傳原taskFolder
	 */
	public String fetchTaskFolder(String path, String taskFolder) {
		String head = taskFolder.substring(0, taskFolder.length() - 3);
		String s3 = taskFolder.substring(taskFolder.length() - 3);
		int num = Integer.parseInt(s3);
		for (int i = num; i < 1000; i++) {
			File workFolder = new File(path + head + s3);
			if (workFolder.exists() && workFolder.isDirectory()) {
				s3 = String.format("%03d", i);
				continue;
			}
			return head + s3;
		}
		return taskFolder;
	}

	/**
	 * 生成路徑字串
	 * @param base 基本路徑
	 * @param path 要往下建置的資料夾(節點)
	 * @return
	 */
	public String genPath(String base, String[] path) {
		StringBuilder sb = new StringBuilder(base);
		for (String folder : path) {
			sb.append(folder).append(File.separator);
		}
		return sb.toString();
	}

	/**
	 * 確認與生成統號字串<BR>
	 * 足10碼不修正，回空字串
	 * 空白預設A000000000，以此補缺
	 * @param certiCode 統號字串
	 * @return 有修改過的10碼 或 空字串
	 */
	public String fixCertiCode(String certiCode) {
		if(StringUtils.isBlank(certiCode)) {
			return "A000000000";
		}else if(certiCode.length()==10) {
			return "";
		}else if(certiCode.length()>10){
			return certiCode.substring(0, 10);
		}
		//生成統號字串
		return certiCode + "A000000000".substring(certiCode.length());
	}

	/**
	 * 透過角色資訊充填業務員招攬高齡客戶投保評估量表ElderScaleRoleVO
	 * @param unbRoleVO
	 * @param policyId
	 * @return
	 */
	private ElderScaleRoleVO fillByUnbRoleVO(UnbRoleVO unbRoleVO, long policyId, String roleType, String audioNo) {
		ElderScaleRoleVO newElderScaleRoleVO = new ElderScaleRoleVO();
		newElderScaleRoleVO.setPolicyId(policyId);
		newElderScaleRoleVO.setRoleType(roleType);
		newElderScaleRoleVO.setCertiCode(StringUtils.defaultString(unbRoleVO.getCertiCode()));
		newElderScaleRoleVO.setName(StringUtils.defaultString(unbRoleVO.getName()));
		newElderScaleRoleVO.setBirthday(unbRoleVO.getBirthdate());
		newElderScaleRoleVO.setAudioNo(audioNo);
		return newElderScaleRoleVO;
	}
	
	/**
	 *取本次可啟動錄音覆審會辦的資料
	 * @param policyInfo
	 * @param validateRoles
	 * @return
	 */
	private List<ElderScaleRoleVO> getElderScaleRoleList(PolicyVO policyInfo, List<UnbRoleVO> validateRoles){
		
		// 「高齡評估量表」內填寫的高齡錄音資料
		Map<String, ElderScaleRoleVO> ElderScaleRoleInES = this.getElderScaleRoleListFromElderScale(policyInfo.getPolicyId());
		
		// 「業務員報告書」內填寫的高齡錄音資料
		Map<String, ElderScaleRoleVO> ElderScaleRoleInAN = getElderScaleRoleListFromAgentNotify(policyInfo.getPolicyId(), policyInfo.getApplyDate(), validateRoles);
		
		// 比對「高齡評估量表」、「業務員報告書」填寫內容，若符合規則，則加入本次可啟動錄音覆審會辦的清單內	
		List<ElderScaleRoleVO> elderScaleRoleList = new ArrayList<ElderScaleRoleVO>();
		addElderScaleRole(elderScaleRoleList, UnbRoleType.POLICY_HOLDER.getRoleType(),
				ElderScaleRoleInES.get(UnbRoleType.POLICY_HOLDER.getRoleType()),
				ElderScaleRoleInAN.get(UnbRoleType.POLICY_HOLDER.getRoleType()));
		
		addElderScaleRole(elderScaleRoleList, UnbRoleType.INSURED.getRoleType(),
				ElderScaleRoleInES.get(UnbRoleType.INSURED.getRoleType()),
				ElderScaleRoleInAN.get(UnbRoleType.INSURED.getRoleType()));
		
		addElderScaleRole(elderScaleRoleList, UnbRoleType.PAYER_2.getRoleType(),
				ElderScaleRoleInES.get(UnbRoleType.PAYER_2.getRoleType()),
				ElderScaleRoleInAN.get(UnbRoleType.PAYER_2.getRoleType()));
		
		return elderScaleRoleList;
	}
	
	/**
	 * 比對高齡評估量表、業務員報告書內所填寫的資料，判斷以何者起高齡錄音任務，並將結果加入任務List內
	 * @param elderScaleRoleList
	 * @param roleType
	 * @param infoInES　高齡評估量表內所填寫的資料
	 * @param infoInAN　業務員報告書內所填寫的資料
	 */
	private void addElderScaleRole(List<ElderScaleRoleVO> elderScaleRoleList, String roleType,
			ElderScaleRoleVO infoInES, ElderScaleRoleVO infoInAN) {
		
		boolean isInfoInESNull = infoInES == null;
		boolean isinfoInANnNUll = infoInAN == null;
		
		StringBuilder sb = new StringBuilder();
		sb.append("roleType=").append(roleType).append("(").append(UnbRoleType.getByRoleType(roleType).getRoleName()).append(")");
		
		if(isInfoInESNull) {
			sb.append("；高齡評估量表：未填寫");
		}else {
			sb.append("；高齡評估量表內：CertiCode=").append(infoInES.getCertiCode());
			sb.append("、name=").append(infoInES.getName());
			sb.append("、audioNo=").append(infoInES.getAudioNo());			
		}
		
		if(isinfoInANnNUll) {
			sb.append("；業務員報告書：未填寫");
		}else {
			sb.append("；業報書內：CertiCode=").append(infoInAN.getCertiCode());
			sb.append("、name=").append(infoInAN.getName());
			sb.append("、audioNo=").append(infoInAN.getAudioNo());
		}	
		
		// 下列條件判斷是否啟動高齡銷售錄音覆審會辦任務：
		// 1.1「保險業務員核保報告書」區塊無值，「高齡客戶投保評估量表暨業務員報告書」區塊有值，以「高齡客戶投保評估量表暨業務員報告書」區塊的資料啟動高齡銷售錄音覆審會辦任務。
		// 1.2「高齡客戶投保評估量表暨業務員報告書」區塊無值，「保險業務員核保報告書」區塊錄音檔流水編號有值，以「保險業務員核保報告書」區塊輸入的錄音檔流水編號，啟動高齡銷售錄音覆審會辦任務。
		// 1.3「高齡客戶投保評估量表暨業務員報告書」及「保險業務員核保報告書」區塊錄音檔流水編號皆為空白時，不須啟動高齡銷售錄音覆審會辦任務。
		// 1.4「高齡客戶投保評估量表暨業務員報告書」及「保險業務員核保報告書」區塊同一身分錄音檔流水編號皆有值且相同時只需啟動一筆高齡銷售錄音覆審會辦任務；同一身分錄音檔流水編號皆有值且不同時，不啟動錄音覆審會辦，但需出檢核規則提醒核保員。
		String processNote = "";
		boolean isAudioNoInESEmpty = isInfoInESNull || StringUtils.isBlank(infoInES.getAudioNo());
		boolean isAudioNoInANEmpty = isinfoInANnNUll || StringUtils.isBlank(infoInAN.getAudioNo());
		if(isAudioNoInESEmpty && isAudioNoInANEmpty) {
			processNote = "高齡評估量表、業報書的錄音檔流水編號皆為空白，不可起任務";
			
		}else if(Boolean.FALSE == isAudioNoInESEmpty && Boolean.FALSE == isAudioNoInANEmpty) {
			
				// 將錄音流水編號排序後再做比較(避免兩邊都填，但編號填寫順序不同，導致判定為不相符，ex.1;2;3 vs 1;3;2)
				String[] adudioNoInES = infoInES.getAudioNo().split(";");
				String[]  adudioNoInAN = infoInAN.getAudioNo().split(";");
				Arrays.sort(adudioNoInES);
				Arrays.sort(adudioNoInAN);
				
				if(Arrays.equals(adudioNoInES, adudioNoInAN)) {					
					processNote = "高齡評估量表、業報書為同錄音檔流水編號，可起任務";
					 //同角色+同錄音流水編號時，以高齡評估量表或業報書起任務皆可；但高齡評估量表內會有繳款人ID、姓名，故以高齡評估量表內容為主
					elderScaleRoleList.add(infoInES);
				}else {
					processNote = "高齡評估量表、業報書為不同錄音檔流水編號，不可起任務";
				}
				
			
		}else if(Boolean.FALSE == isAudioNoInESEmpty) {
			processNote = "僅高齡評估量表有填錄音檔流水編號，以高齡評估量表起任務";
			elderScaleRoleList.add(infoInES);
			
		}else if(Boolean.FALSE == isAudioNoInANEmpty) {
			processNote = "僅業報書有填錄音檔流水編號，以業報書起任務";
			elderScaleRoleList.add(infoInAN);
		}
		
		sb.append("；判斷結果=").append(processNote);
		NBUtils.logger(this.getClass(), sb.toString());
	}
	
	/**
	 * 自高齡評估量表取得高齡錄音資料
	 * @param policyId
	 * @return
	 */
	private Map<String, ElderScaleRoleVO> getElderScaleRoleListFromElderScale(Long policyId){
		
		Map<String, ElderScaleRoleVO> resultMap = new HashMap<String, ElderScaleRoleVO>();
		
		//高齡評估量表
		List<ElderScaleRoleVO> elderScaleRoleVO = elderScaleRoleService.findByPolicyId(policyId);
		boolean isElderScaleRoleEmpty = CollectionUtils.isEmpty(elderScaleRoleVO); //是否有【高齡客戶投保評估量表暨業務員報告書】
		
		//處理高齡評估量表資料
		NBUtils.logger(this.getClass(), "policyId=" + Long.toString(policyId) + "，高齡評估量表是否為空=" + isElderScaleRoleEmpty);
		if (Boolean.FALSE == isElderScaleRoleEmpty) {
			for (ElderScaleRoleVO esRoleVO : elderScaleRoleVO) {
				
				if (UnbRoleType.INSURED.getRoleType().equals(esRoleVO.getRoleType())) {
					resultMap.put(UnbRoleType.INSURED.getRoleType(), esRoleVO);
					
				} else if (UnbRoleType.POLICY_HOLDER.getRoleType().equals(esRoleVO.getRoleType())) {
					resultMap.put(UnbRoleType.POLICY_HOLDER.getRoleType(), esRoleVO);
					
				} else if (UnbRoleType.PAYER_2.getRoleType().equals(esRoleVO.getRoleType())) {
					resultMap.put(UnbRoleType.PAYER_2.getRoleType(), esRoleVO);
					
				} else {
					NBUtils.logger(this.getClass(),
							"角色不符合，被剔除, policyId=" + Long.toString(policyId) + ", role=" + esRoleVO.getRoleType()
									+ ", certCode=" + esRoleVO.getCertiCode() + ", name=" + esRoleVO.getName());
					continue;// 不屬於這些角色就不加載錄音編號
				}
			}
		}
		
		return resultMap;
	}
	
	/**
	 *  自業務員報告書取得高齡錄音資料
	 * @param policyId
	 * @param applyDate
	 * @param validateRoles
	 * @return
	 */
	private Map<String, ElderScaleRoleVO> getElderScaleRoleListFromAgentNotify(Long policyId, Date applyDate, List<UnbRoleVO> validateRoles){
		
		Map<String, ElderScaleRoleVO> resultMap = new HashMap<String, ElderScaleRoleVO>();
		
		//業務員報告書(部分通路的高齡錄音資料會填在業報書，ex.台新銀(版次T結尾)、玉山銀(版次E結尾))
		AgentNotifyVO anVO = agentNotifyService.load(policyId);
		boolean isAgentNotifyEmpty = (anVO == null || anVO.isEmpty());
		NBUtils.logger(this.getClass(), "policyId=" + Long.toString(policyId) + "，業務員報告書是否為空=" + isAgentNotifyEmpty);
		
		//處理業務員報告書資料
		ElderScaleRoleVO insuredRoleVOInAN = null;//被保險人
		ElderScaleRoleVO holderRoleVOInAN = null;//要保人
		ElderScaleRoleVO traderRoleVOInAN = null;//繳款人
		
		if(Boolean.FALSE == isAgentNotifyEmpty) {
			
			UnbRoleVO legalRoleVOInAN = null;//法代
			for (UnbRoleVO unbRoleVO : validateRoles) {
				
				String[] unbPolicyRole = unbRoleVO.getPolicyRole().split(",");
				
				//法定代理人
				if (ArrayUtils.contains(unbPolicyRole, UnbRoleType.LEGAL_REPRESENTATIVE.getRoleType())) {
					legalRoleVOInAN = new UnbRoleVO();
					try {
						BeanUtils.copyProperties(legalRoleVOInAN, unbRoleVO);
					} catch (Exception e) {
						NBUtils.logger(this.getClass(), "處理法代複製出錯誤:" + e.getMessage() + ", policyId=" + Long.toString(policyId));
						legalRoleVOInAN = null;
					}
				}
				
				//要保人
				if (holderRoleVOInAN == null && StringUtils.isNotEmpty(anVO.getAudioNoPh())
						&& ArrayUtils.contains(unbPolicyRole, UnbRoleType.POLICY_HOLDER.getRoleType())) {
					holderRoleVOInAN = fillByUnbRoleVO(unbRoleVO, policyId, UnbRoleType.POLICY_HOLDER.getRoleType(), anVO.getAudioNoPh());
				}
				
				//被保人
				if (insuredRoleVOInAN == null && StringUtils.isNotEmpty(anVO.getAudioNoInsured())
						&& ArrayUtils.contains(unbPolicyRole, UnbRoleType.INSURED.getRoleType())) {
					insuredRoleVOInAN = fillByUnbRoleVO(unbRoleVO, policyId, UnbRoleType.INSURED.getRoleType(), anVO.getAudioNoInsured());
				}
				
				//繳款人
				if (traderRoleVOInAN == null && StringUtils.isNotEmpty(anVO.getAudioNoTrader())
						&& ArrayUtils.contains(unbPolicyRole, UnbRoleType.PAYER_2.getRoleType())) {
					traderRoleVOInAN = fillByUnbRoleVO(unbRoleVO, policyId, UnbRoleType.PAYER_2.getRoleType(), anVO.getAudioNoTrader());
				}
				
			}
			
			//若資料填寫不完整，但有輸入正確的錄音編號，且存在錄音問卷影像(UNBN076)，仍需起會辦任務
			//要保人只有錄音編號
			if(StringUtils.isNotEmpty(anVO.getAudioNoPh()) && holderRoleVOInAN == null) {
				holderRoleVOInAN = fillByUnbRoleVO(new UnbRoleVO(), policyId, UnbRoleType.POLICY_HOLDER.getRoleType(), anVO.getAudioNoPh());
			}
			
			//被保險人只有錄音編號
			if(StringUtils.isNotEmpty(anVO.getAudioNoInsured()) && insuredRoleVOInAN == null) {
				insuredRoleVOInAN = fillByUnbRoleVO(new UnbRoleVO(), policyId, UnbRoleType.INSURED.getRoleType(), anVO.getAudioNoInsured());
			}
			
			//繳款人只有錄音編號
			if(StringUtils.isNotEmpty(anVO.getAudioNoTrader()) && traderRoleVOInAN == null) {
				traderRoleVOInAN = fillByUnbRoleVO(new UnbRoleVO(), policyId, UnbRoleType.PAYER_2.getRoleType(), anVO.getAudioNoTrader());
			}
			
			//要保人或被保險人為未成年者，且保單有輸入法定代理人資料，系統帶法定代理人的角色及姓名(因業報書上的要/被保人相關欄位會填法代資料)
			int grownUpAge = NBUtils.getGrownUpAge(applyDate); // 未成年年齡
			if(legalRoleVOInAN != null && insuredRoleVOInAN != null) {
				long insuredAge = BizUtils.getAge(Para.getParaValue(CodeCstTgl.UNB_ELDER_AGE_METHOD), insuredRoleVOInAN.getBirthday(), applyDate);
				if(grownUpAge > insuredAge) {
					insuredRoleVOInAN = fillByUnbRoleVO(legalRoleVOInAN, policyId, UnbRoleType.INSURED.getRoleType(), anVO.getAudioNoInsured());
				}
			}
			
			if(legalRoleVOInAN != null  && holderRoleVOInAN != null) {
				long holderAge = BizUtils.getAge(Para.getParaValue(CodeCstTgl.UNB_ELDER_AGE_METHOD), holderRoleVOInAN.getBirthday(), applyDate);
				if(grownUpAge > holderAge) {
					holderRoleVOInAN = fillByUnbRoleVO(legalRoleVOInAN, policyId, UnbRoleType.POLICY_HOLDER.getRoleType(), anVO.getAudioNoPh());
				}
			}
			
			if(insuredRoleVOInAN != null) {
				resultMap.put(UnbRoleType.INSURED.getRoleType(), insuredRoleVOInAN);
			}
			
			if(holderRoleVOInAN != null) {
				resultMap.put(UnbRoleType.POLICY_HOLDER.getRoleType(), holderRoleVOInAN);
			}
			
			if(traderRoleVOInAN != null) {
				resultMap.put(UnbRoleType.PAYER_2.getRoleType(), traderRoleVOInAN);
			}
			
		}
		
		return resultMap;
		
	}

}
