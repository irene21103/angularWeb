package com.ebao.ls.liaRoc.ctrl;

import java.util.ArrayList;
import java.util.List;

import com.ebao.pub.framework.GenericForm;

/**
 * <h1>公會資料下載 form bean</h1><p>
 * @since 2016/03/19<p>
 * @author Amy Hung
 * <p>Update Time: </p>
 * <p>Updater: </p>
 * <p>Update Comments: </p>
*/
@SuppressWarnings("serial")
public class DownloadLiaRocForm extends GenericForm {

    String actionType;

    List<String> downloadIds = new ArrayList<String>();

    List<String> errorIds = new ArrayList<String>();

    public String getActionType() {
        return actionType;
    }

    public void setActionType(String actionType) {
        this.actionType = actionType;
    }

    public List<String> getDownloadIds() {
        return downloadIds;
    }

    public String getDownloadIds(int index) {
        return downloadIds.get(index);
    }

    public void setDownloadIds(List<String> downloadIds) {
        this.downloadIds = downloadIds;
    }

    public void setDownloadIds(int index, String value) {
        while (index >= downloadIds.size()) {
            downloadIds.add(value);
        }
        this.downloadIds.set(index, value);
    }

    public List<String> getErrorIds() {
        return errorIds;
    }

    public void setErrorIds(List<String> errorIds) {
        this.errorIds = errorIds;
    }

}
