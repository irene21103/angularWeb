package com.ebao.ls.callout.batch.task;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.transaction.UserTransaction;

import org.springframework.context.ApplicationContextAware;

import com.ebao.foundation.commons.exceptions.ExceptionUtil;
import com.ebao.foundation.core.logging.Log;
import com.ebao.foundation.module.para.Para;
import com.ebao.ls.callout.batch.AbstractCalloutTask;
import com.ebao.ls.callout.batch.CalloutConstants;
import com.ebao.ls.callout.batch.CalloutDataExportService;
import com.ebao.ls.callout.data.bo.CalloutConfig;
import com.ebao.ls.cs.ci.TeleInterviewCI;
import com.ebao.ls.pub.cst.CalloutConfigType;
import com.ebao.pub.batch.help.BatchHelp;
import com.ebao.pub.batch.type.JobStatus;
import com.ebao.pub.batch.type.LogLevel;
import com.ebao.pub.batch.util.BatchLogUtils;
import com.ebao.pub.util.DateUtils;
import com.ebao.pub.util.TransUtils;

public class POS04ChangeBeneficiaryForCohabitTask extends AbstractCalloutTask implements ApplicationContextAware {
    
    public static final String BEAN_DEFAULT = "posFourTask";
    
    private Log log = Log.getLogger(POS04ChangeBeneficiaryForCohabitTask.class);
    
    @Resource(name = TeleInterviewCI.BEAN_DEFAULT)
    private TeleInterviewCI pos;

    private final Long DISPLAY_ORDER = 4L;

    private String CONFIG_TYPE = CalloutConfigType.CALLOUT_CONFIG_TYPE_POS;
    
    @Resource(name=CalloutDataExportService.BEAN_DEFAULT)
    private CalloutDataExportService exportServ;
    
    private void syso(Object msg){
        log.info("[BR-CMN-TFI-005][檔案POS-4 變更身故受益人為同居人]" + msg);
        BatchLogUtils.addLog(LogLevel.INFO, null, "[BR-CMN-TFI-005][檔案POS-4 變更身故受益人為同居人]" +msg);
    }
    
    private void syso(String layout, Object... msg){
        syso(String.format(layout, msg));
    }
    
    @Override
    public int mainProcess() throws Exception {
        String fileName = Para.getParaValue(CalloutConstants.POSFour);
        /* 是否重覆跑批 */
        java.util.Date processDate = BatchHelp.getProcessDate();
        if(isProcess(fileName, BatchHelp.getProcessDate())){
            syso("業務日期 = %s, 已處理完畢", com.ebao.pub.util.json.DateUtils.convertToMinguoDate(BatchHelp.getProcessDate()));
            return JobStatus.EXECUTE_SUCCESS;
        }
        
        /* 抽取母體資料 */
        CalloutConfig cfg = getConfig(CONFIG_TYPE, DISPLAY_ORDER);
        Long sampleType = cfg.getListId();
        if(sampleType == null)
            return JobStatus.EXECUTE_SUCCESS;
        
        List<Map<String, Object>> mapList = getData(processDate, null);
        mapList = cleanDuplicateBatch(mapList, sampleType, processDate);
        
        int totalSize = mapList != null  ? mapList.size()  : 0;
        syso("母體資料 List.size = " + totalSize);
        if(totalSize == 0)
            return JobStatus.EXECUTE_SUCCESS;
        
        /* 抽取樣本資料 */
        Integer sampleSize = readSample(mapList, CONFIG_TYPE, DISPLAY_ORDER);
        syso("資料抽樣筆數 = " + sampleSize);
        /* 儲存電訪紀錄 */
        UserTransaction ut = null;
        try{
            ut = TransUtils.getUserTransaction();
            ut.begin();
            save(mapList, BatchHelp.getProcessDate(), fileName, "POS","POS-4", CONFIG_TYPE, DISPLAY_ORDER);
            /* 匯出抽樣保單 */
            exportServ.feedMap(mapList, CalloutConstants.POSFour, BatchHelp.getProcessDate());
            ut.commit();
        }catch(Exception ex){
            TransUtils.rollback(ut);
            BatchLogUtils.addLog(LogLevel.ERROR, null, "[BR-CMN-TFI-005][檔案POS-4 變更身故受益人為同居人]\n" + ExceptionUtil.parse(ex));
            return JobStatus.EXECUTE_FAILED;
        }
        
        return JobStatus.EXECUTE_SUCCESS;
    }
    
  public java.util.List<Map<String, Object>> getData(Date inputDate, String policyCode){
      syso("業務日期= '%s'", com.ebao.pub.util.json.DateUtils.format( inputDate , "yyyy/MM/dd"));
      Date processDate = DateUtils.getNextWorkday(inputDate, -1);
      syso("前一工作日= '%s'", com.ebao.pub.util.json.DateUtils.format(  processDate, "yyyy/MM/dd"));
      List<Map<String, Object>> mapList = pos.getChangeBeneficiaryForCohabit(processDate, processDate);
      return mapList;
  }

    @Override
    public String saveToTrans(Map<String, Object> map, Date processDate,
                    String fileName, String dept) {
        String calloutNum = saveInsured(map, processDate, fileName, dept);
        return calloutNum;
    }

}
