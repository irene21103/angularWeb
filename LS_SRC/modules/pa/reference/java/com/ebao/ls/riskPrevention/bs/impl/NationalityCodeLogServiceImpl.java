package com.ebao.ls.riskPrevention.bs.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.ebao.ls.foundation.service.GenericServiceImpl;
import com.ebao.ls.riskPrevention.bs.NationalityCodeLogService;
import com.ebao.ls.riskPrevention.data.NationalityCodeLogDao;
import com.ebao.ls.riskPrevention.data.bo.NationalityCodeLog;
import com.ebao.ls.riskPrevention.vo.NationalityCodeLogVO;
import com.ebao.pub.web.pager.Pager;

public class NationalityCodeLogServiceImpl extends GenericServiceImpl<NationalityCodeLogVO, NationalityCodeLog, NationalityCodeLogDao> implements NationalityCodeLogService {
	
	@Resource(name= NationalityCodeLogDao.BEAN_DEFAULT)
	public void setDao(NationalityCodeLogDao dao){
		super.setDao(dao);
	}
	
	@Override
	protected NationalityCodeLogVO newEntityVO() {
		return new NationalityCodeLogVO();
	}
	
	@Override
	public List<Map<String, Object>> nationalQuery(NationalityCodeLogVO vo, Pager pager, boolean history) {
		NationalityCodeLog bo = new NationalityCodeLog();
		bo.copyFromVO(vo, true, false);
		if(history)
			return super.getDao().nationalQuery(bo, pager);
		else
			return super.getDao().latestQuery(bo, pager);
	}

	@Override
	public List<Map<String, Object>> riskQuery(NationalityCodeLogVO vo, Pager pager) {
		NationalityCodeLog bo = new NationalityCodeLog();
		bo.copyFromVO(vo, true, false);
		return super.getDao().riskQuery(bo, pager);
	}

	@Override
	public boolean newLog(NationalityCodeLogVO vo) {
		if(vo.getNationalCode()==null || vo.getNationalName()==null || vo.getNationalRisk()==null ||vo.getUpdatedDeptID()==null || vo.getUpdatedDeptName()==null){
			return false;
		}
		NationalityCodeLog bo = new NationalityCodeLog();
		bo.copyFromVO(vo, true, false);
		super.getDao().save(bo);
		return true;
	}
}
