package com.ebao.ls.uw.cmu.event;

import com.ebao.foundation.commons.annotation.TriggerPoint;
import com.ebao.ls.cmu.event.NbUwEscalated;
import com.ebao.pub.event.AbstractIntegrationListener;
import com.ebao.pub.integration.IntegrationMessage;

public class NbUwEscalatedIntegrationListener extends
    AbstractIntegrationListener<NbUwEscalated> {
  final static String code = "uw.nbuwEscalated";

  @Override
  @TriggerPoint(component = "Underwriting", description = "There are two ways to escalate an underwriting case. Underwriters can manually escalate a proposal to senior staff. Upon underwriting submission, the system will check underwriter's authority against decision, the over-authority decision will be escalated automatically.<br/>When an underwriting case has been escalated successfully, the event is triggered.<br/>Menu navigation: New business > Worklist > Underwriting.<br/>", event = "direct:event."
      + code, id = "com.ebao.tp.ls." + code, howToUse = "Can trigger some actions which is unrelated with main process to this event such as letters, SMS etc. It is commonly seen that system will generate proposal status change reminder to producers or customers.<br/>")
  protected String getEventCode(NbUwEscalated event,
      IntegrationMessage<Object> in) {
    return "event." + code;
  }
}
