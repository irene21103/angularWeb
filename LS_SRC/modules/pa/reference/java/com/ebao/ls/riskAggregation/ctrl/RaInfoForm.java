package com.ebao.ls.riskAggregation.ctrl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.ebao.ls.riskAggregation.vo.RAInfoVO;
import com.ebao.pub.framework.GenericForm;

public class RaInfoForm extends GenericForm {

	private static final long serialVersionUID = 1L;

	private Long policyId;

	private String policyNumber;

	private String proposalNumber;

	private Integer riskStatus = java.lang.Integer.valueOf(0);

	private BigDecimal initialPrem = new java.math.BigDecimal(0);

	private Integer currency;

	private String initialType;

	private Long serviceAgent;

	private Integer masterProduct;

	private Date applyDate;

	private Integer baseCurrency;

	private List<RAInfoVO> raInfos;

	private Integer proposalStatus;

	private Map<String, Long> otherTotal;

	private Map<String, Long> companyTotal;
	
	private String isIframe;

	public String getPolicyNumber() {
		return policyNumber;
	}

	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}

	public String getProposalNumber() {
		return proposalNumber;
	}

	public void setProposalNumber(String proposalNumber) {
		this.proposalNumber = proposalNumber;
	}

	public Integer getRiskStatus() {
		return riskStatus;
	}

	public void setRiskStatus(Integer riskStatus) {
		this.riskStatus = riskStatus;
	}

	public BigDecimal getInitialPrem() {
		return initialPrem;
	}

	public void setInitialPrem(BigDecimal initialPrem) {
		this.initialPrem = initialPrem;
	}

	public Integer getCurrency() {
		return currency;
	}

	public void setCurrency(Integer currency) {
		this.currency = currency;
	}

	public String getInitialType() {
		return initialType;
	}

	public void setInitialType(String initialType) {
		this.initialType = initialType;
	}

	public Long getServiceAgent() {
		return serviceAgent;
	}

	public void setServiceAgent(Long serviceAgent) {
		this.serviceAgent = serviceAgent;
	}

	public Integer getMasterProduct() {
		return masterProduct;
	}

	public void setMasterProduct(Integer masterProduct) {
		this.masterProduct = masterProduct;
	}

	public Date getApplyDate() {
		return applyDate;
	}

	public void setApplyDate(Date applyDate) {
		this.applyDate = applyDate;
	}

	public Integer getBaseCurrency() {
		return baseCurrency;
	}

	public void setBaseCurrency(Integer baseCurrency) {
		this.baseCurrency = baseCurrency;
	}

	public List<RAInfoVO> getRaInfos() {
		return raInfos;
	}

	public void setRaInfos(List<RAInfoVO> raInfos) {
		this.raInfos = raInfos;
	}

	public Long getPolicyId() {
		return policyId;
	}

	public void setPolicyId(Long policyId) {
		this.policyId = policyId;
	}

	public Integer getProposalStatus() {
		return proposalStatus;
	}

	public void setProposalStatus(Integer proposalStatus) {
		this.proposalStatus = proposalStatus;
	}

	public Map<String, Long> getOtherTotal() {
		return otherTotal;
	}

	public void setOtherTotal(Map<String, Long> otherTotal) {
		this.otherTotal = otherTotal;
	}

	public Map<String, Long> getCompanyTotal() {
		return companyTotal;
	}

	public void setCompanyTotal(Map<String, Long> companyTotal) {
		this.companyTotal = companyTotal;
	}

	public String getIsIframe() {
		return isIframe;
	}

	public void setIsIframe(String isIframe) {
		this.isIframe = isIframe;
	}

}
