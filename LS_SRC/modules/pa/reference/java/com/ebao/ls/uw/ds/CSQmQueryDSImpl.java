package com.ebao.ls.uw.ds;

import com.ebao.ls.foundation.service.GenericServiceImpl;
import com.ebao.ls.uw.data.TCsQmQueryDelegate;
import com.ebao.ls.uw.data.bo.CSQualityManageList;
import com.ebao.ls.uw.vo.CSQualityManageListVO;

public class CSQmQueryDSImpl extends GenericServiceImpl<CSQualityManageListVO, CSQualityManageList, TCsQmQueryDelegate> implements CSQmQueryService {

	@Override
	protected CSQualityManageListVO newEntityVO() {
		return new CSQualityManageListVO();
	}

    @Override
    public boolean delete(Long listId, String deleteReason) {
    	return super.getDao().deleteByCriteria(listId, deleteReason);
    }

}
