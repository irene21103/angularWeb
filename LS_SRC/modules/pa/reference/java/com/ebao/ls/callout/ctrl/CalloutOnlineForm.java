package com.ebao.ls.callout.ctrl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.Factory;
import org.apache.commons.collections.ListUtils;

import com.ebao.ls.callout.vo.CalloutTransVO;
import com.ebao.pub.web.pager.PagerFormImpl;

public class CalloutOnlineForm extends PagerFormImpl {

    private static final long serialVersionUID = 1L;

    private String policyCode;

    private Long changeId;

    private Long policyId;

    private String calloutReason = "";
    
    private String calloutTypeStr;

    private CalloutTransVO calloutTransVO = new CalloutTransVO();

    private String oCalloutIds;

    private String parentPolicyId = "";

    private String parentPolicyListId = "";

    private String modifyParentPolicyId = "";
    
    private String returnMsg;
    
    private String calloutNum;
    
    private String senderFlag;
    
    private String noCalloutReason;
    
    private String noCalloutReasonDesc;
        
    @SuppressWarnings("unchecked")
    private List<CalloutTransVO> calloutTransVOList = ListUtils.lazyList(
                    new ArrayList<CalloutTransVO>(), new Factory() {
                        @Override
                        public CalloutTransVO create() {
                            return new CalloutTransVO();
                        }
                    });

    public String getPolicyCode() {
        return policyCode;
    }

    public void setPolicyCode(String policyCode) {
        this.policyCode = policyCode;
    }

    public Long getChangeId() {
        return changeId;
    }

    public void setChangeId(Long changeId) {
        this.changeId = changeId;
    }

    public Long getPolicyId() {
        return policyId;
    }

    public void setPolicyId(Long policyId) {
        this.policyId = policyId;
    }

    public String getCalloutReason() {
        return calloutReason;
    }

    public void setCalloutReason(String calloutReason) {
        this.calloutReason = calloutReason;
    }

    public CalloutTransVO getCalloutTransVO() {
        return calloutTransVO;
    }

    public void setCalloutTransVO(CalloutTransVO calloutTransVO) {
        this.calloutTransVO = calloutTransVO;
    }

    public String getoCalloutIds() {
        return oCalloutIds;
    }

    public void setoCalloutIds(String oCalloutIds) {
        this.oCalloutIds = oCalloutIds;
    }

    public String getParentPolicyId() {
        return parentPolicyId;
    }

    public void setParentPolicyId(String parentPolicyId) {
        this.parentPolicyId = parentPolicyId;
    }

    public String getParentPolicyListId() {
        return parentPolicyListId;
    }

    public void setParentPolicyListId(String parentPolicyListId) {
        this.parentPolicyListId = parentPolicyListId;
    }

    public String getModifyParentPolicyId() {
        return modifyParentPolicyId;
    }

    public void setModifyParentPolicyId(String modifyParentPolicyId) {
        this.modifyParentPolicyId = modifyParentPolicyId;
    }

    public List<CalloutTransVO> getCalloutTransVOList() {
        return calloutTransVOList;
    }

    public void setCalloutTransVOList(List<CalloutTransVO> calloutTransVOList) {
        this.calloutTransVOList = calloutTransVOList;
    }

    public String getCalloutTypeStr() {
        return calloutTypeStr;
    }

    public void setCalloutTypeStr(String calloutTypeStr) {
        this.calloutTypeStr = calloutTypeStr;
    }

    public String getReturnMsg() {
        return returnMsg;
    }

    public void setReturnMsg(String returnMsg) {
        this.returnMsg = returnMsg;
    }

	public String getCalloutNum() {
		return calloutNum;
	}

	public void setCalloutNum(String calloutNum) {
		this.calloutNum = calloutNum;
	}

	public String getSenderFlag() {
		return senderFlag;
	}

	public void setSenderFlag(String senderFlag) {
		this.senderFlag = senderFlag;
	}

	public String getNoCalloutReason() {
		return noCalloutReason;
	}

	public void setNoCalloutReason(String noCalloutReason) {
		this.noCalloutReason = noCalloutReason;
	}

	public String getNoCalloutReasonDesc() {
		return noCalloutReasonDesc;
	}

	public void setNoCalloutReasonDesc(String noCalloutReasonDesc) {
		this.noCalloutReasonDesc = noCalloutReasonDesc;
	}

}
