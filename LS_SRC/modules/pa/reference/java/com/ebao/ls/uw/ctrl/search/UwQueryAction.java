package com.ebao.ls.uw.ctrl.search;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.ls.pty.ci.CustomerCI;
import com.ebao.ls.pty.vo.CustomerVO;
import com.ebao.ls.uw.ctrl.decision.UwProductDecisionViewForm;
import com.ebao.ls.uw.ctrl.pub.UwConditionForm;
import com.ebao.ls.uw.ctrl.pub.UwEndorsementForm;
import com.ebao.ls.uw.ctrl.pub.UwExclusionForm;
import com.ebao.ls.uw.ctrl.pub.UwPolicyForm;
import com.ebao.ls.uw.ctrl.underwriting.UwInsuredListDecisionViewForm;
import com.ebao.ls.uw.ds.UwPolicyService;
import com.ebao.ls.uw.vo.UwLifeInsuredVO;
import com.ebao.ls.uw.vo.UwPolicyVO;
import com.ebao.ls.uw.vo.UwProductVO;
import com.ebao.pub.framework.GenericAction;
import com.ebao.pub.framework.GenericException;
import com.ebao.pub.util.BeanUtils;
import com.ebao.pub.util.Log;

/**
 * <p>
 * Title: GEL-UW
 * </p>
 * <p>
 * Description: Underwriting query list page
 * </p>
 * <p>
 * Copyright: Copyright (c) 2004
 * </p>
 * <p>
 * Company: eBaoTech
 * </p>
 *
 * @author yixing.lu
 * @version 1.0
 * @since 08.23.2004
 */
public class UwQueryAction extends GenericAction {
  public UwPolicyService getUwPolicyDS() {
    return uwPolicyDS;
  }

  public static final String MAIN_FORM = "main_form";
  public static final String DISCOUNT_TYPE = "discount_type";
  public static final String PRODUCT_LIST = "product_list";
  public static final String LIFE_ASSURED_LIST = "life_assured_list";
  public static final String ENDORSEMENTCODE_LIST = "endorsementCode_list";
  public static final String CONDITIONCODE_LIST = "conditionCode_list";
  public static final String EXCLUSIONCODE_LIST = "exclusionCode_list";

  /**
   * getting policy information for customer service
   *
   * @param mapping The ActionMapping used to select this instance
   * @param form The optional ActionForm bean for this request (if any)
   * @param request Http Request
   * @param response Http Response
   * @return ActionForward ActionForward
   * @throws java.lang.Exception ApplicationException
   */
  @Override
  public ActionForward process(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {
    // get the underwriteId value
    Long underwriteId = null;
    underwriteId = (Long) request.getAttribute("underwriteId");
    if (underwriteId == null) {
      underwriteId = Long.valueOf(com.ebao.ls.encode.encoder.Encode.encodeForHTML(request).getParameter("underwriteId"));
    }
    // get policy vo
    UwPolicyVO vo = uwPolicyDS.findUwPolicy(underwriteId);
    if (vo == null) {
      throw new IllegalArgumentException("uwpolicy with underwrite id "
          + underwriteId + " not found.");
    }
    UwPolicyForm upForm = new UwPolicyForm();
    BeanUtils.copyProperties(upForm, vo);
    upForm.setChangeId(Long.valueOf(com.ebao.ls.encode.encoder.Encode.encodeForHTML(request).getParameter("changId")));
    // set uwpolicy information
    request.setAttribute(UwQueryAction.MAIN_FORM, upForm);
    /*
     * Retrieve discount type, currently the same discount type has been
     * recorded on all benefits. The discount type has been imported after NBU
     * with the same discount type on all benefits.
     */
    Collection benefits = uwPolicyDS.findUwProductEntitis(underwriteId);
    Iterator iter = benefits.iterator();
    while (iter.hasNext()) {
      UwProductVO upvo = (UwProductVO) iter.next();
      // set discount type
      upForm.setDiscountType(upvo.getDiscountType());
      // all benefits has the same discount type so we break
      break;
    }
    request.setAttribute(UwQueryAction.PRODUCT_LIST,
        getUwProductDecisionViewForms(benefits));
    // set life assured list
    // integration with party module
    // if interface(data transfer object) changed later we need to change the
    // corresponding jsp script in the jsp file
    Collection insuredLists = uwPolicyDS.findUwLifeInsuredEntitis(underwriteId);
    request.setAttribute(UwQueryAction.LIFE_ASSURED_LIST,
        getInsuredListDecisionViewForms(insuredLists));
    /*
     * request.setAttribute(UwQueryAction.LIFE_ASSURED_LIST,
     * BeanUtils.copyCollection(UwInsuredListForm.class,
     * uwPolicyDS.findUwLifeInsuredEntitis(
     * Long.valueOf(request.getParameter("underwriteId")))));
     */
    // set condition code list
    request.setAttribute(
        UwQueryAction.CONDITIONCODE_LIST,
        BeanUtils.copyCollection(UwConditionForm.class,
            uwPolicyDS.findUwConditionEntitis(underwriteId)));
    // set endorsement code list
    request.setAttribute(
        UwQueryAction.ENDORSEMENTCODE_LIST,
        BeanUtils.copyCollection(UwEndorsementForm.class,
            uwPolicyDS.findUwEndorsementEntitis(underwriteId)));
    // set exclusion code list
    request.setAttribute(
        UwQueryAction.EXCLUSIONCODE_LIST,
        BeanUtils.copyCollection(UwExclusionForm.class,
            uwPolicyDS.findUwExclusionEntitis(underwriteId)));
    return mapping.findForward("uwQuery");
  }

  private Collection getInsuredListDecisionViewForms(Collection lifeInsuredVOs) {
    // currently the presentation layer knows too much about our business layer
    // I prefer to encapsulates these coupling in our business layer and not
    // exposed it into
    // presentation layer.
    // According to my opinion this method will be migrated into our bo object
    // someday later
    // to eliminates this coupling and reusability will be increased
    // consequently.
    // As a result if we migrated this method into our business layer we must
    // solve
    // the interface mismatch problem and corresponding transfer data object
    // will be introduced.
    Collection result = new ArrayList();
    Iterator iter = lifeInsuredVOs.iterator();
    while (iter.hasNext()) {
      UwLifeInsuredVO insured = (UwLifeInsuredVO) iter.next();
      UwInsuredListDecisionViewForm form = new UwInsuredListDecisionViewForm();
      try {
        BeanUtils.copyProperties(form, insured);
        CustomerVO customer = customerCI.getPerson(insured.getInsuredId());
        // set life insured's gender
        form.setGenderName(customer.getGender());
        // set party id type
        form.setPartyIdType(customer.getPartyType());
        // set party id no
        form.setPartyIdNO(String.valueOf(customer.getPartyId()));
        // set life insured's name
        form.setLifeAssuredName(getCustomerFullName(customer));
        form.setDobValue(customer.getBirthday());
        /**
         * @todo below logic will be eliminated by some other method current the
         *       solution will be very slow. And the db schema will be altered
         *       for a more effective solution the solution wll be offered by
         *       walter.
         */
        /*
         * Currently standar life indicator stored at benefit level and they
         * related using underwritingid and insuredid criteria
         */
        Collection benefits = uwPolicyDS.findUwProductEntitis(insured
            .getUnderwriteId());
        Iterator benefitsIter = benefits.iterator();
        while (benefitsIter.hasNext()) {
          UwProductVO upvo = (UwProductVO) benefitsIter.next();
          if (upvo.getInsured1().equals(insured.getInsuredId())) {
            form.setStdLifeIndi(upvo.getStandLife1());
            break;
          }
        }
      } catch (GenericException ex) {
        Log.error(UwQueryAction.class, "Can't finding customer"
            + insured.getInsuredId().toString()
            + " through party integration interface", ex);
      } catch (Exception ex) {
        Log.error(this.getClass(), ex);
      }
      result.add(form);
    }
    return result;
  } // end of method getInsuredListDecisionViewForms

  // this method should be migrated into party interface for conveniency
  private String getCustomerFullName(CustomerVO customer) {
    StringBuffer sbf = new StringBuffer();
    if (null != customer.getFirstName()) {
      sbf.append(customer.getFirstName());
    }
    sbf.append(" ");
    if (null != customer.getLastName()) {
      sbf.append(customer.getLastName());
    }
    return sbf.toString();
  }

  /**
   * Integration with party and other modules, translates UwProductVO into
   * UwProductDecisionViewForm with the supporting utility( CustomerVO) of party
   * module
   *
   * @param productVOs Collection UwProductVO collection
   * @return Collection UwProductDecisionViewForm collection
   */
  private Collection getUwProductDecisionViewForms(Collection productVOs) {
    // similar decision lies in method getInsuredListDecisionViewForms
    Collection result = new ArrayList();
    Iterator iter = productVOs.iterator();
    while (iter.hasNext()) {
      UwProductDecisionViewForm form = new UwProductDecisionViewForm();
      UwProductVO upvo = (UwProductVO) iter.next();
      try {
        BeanUtils.copyProperties(form, upvo);
        CustomerVO customer = customerCI.getPerson(upvo.getInsured1());
        form.setLifeAssuredName(getCustomerFullName(customer));
      } catch (GenericException ex) {
        if (Log.isErrorEnabled(UwQueryAction.class)) {
          Log.error(UwQueryAction.class, "Can't finding customer"
              + upvo.getInsured1().toString()
              + " through party integration interface", ex);
        }
      } catch (Exception ex) {
        Log.error(this.getClass(), ex);
      }
      result.add(form);
    }
    return result;
  }

  @Resource(name = UwPolicyService.BEAN_DEFAULT)
  private UwPolicyService uwPolicyDS;

  public void setUwPolicyDS(UwPolicyService uwPolicyDS) {
    this.uwPolicyDS = uwPolicyDS;
  }

  @Resource(name = CustomerCI.BEAN_DEFAULT)
  private CustomerCI customerCI;

  /**
   * @return the customerCI
   */
  public CustomerCI getCustomerCI() {
    return customerCI;
  }

  /**
   * @param customerCI the customerCI to set
   */
  public void setCustomerCI(CustomerCI customerCI) {
    this.customerCI = customerCI;
  }
}