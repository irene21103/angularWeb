package com.ebao.ls.uw.ctrl.info;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.ls.pa.nb.util.NBUtils;
import com.ebao.ls.pa.pub.MsgCst;
import com.ebao.ls.uw.ds.UwInsureHistoryService;
import com.ebao.ls.uw.ds.vo.UwInsureClientVO;
import com.ebao.pub.framework.GenericAction;
import com.ebao.pub.util.StringUtils;

/**
 * <p>Title: 查詢財務資訊ACTION</p>
 * <p>Description: Function Description Only vegetable</p>
 * <p>Create Time: Nov 29, 2019</p> 
 * @author 
 * <p>Update Time: Nov 29, 2019</p>
 * <p>Updater: Jeremy Zhu</p>
 * <p>Update Comments: </p>
 */
public class FinanceNotifyRecordAction extends GenericAction {

    @Resource(name = UwInsureHistoryService.BEAN_DEFAULT)
    private UwInsureHistoryService uwInsureHistoryService;

    @Override
    public ActionForward process(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                    HttpServletResponse response) throws Exception {

        UwHistoryInsureForm uwHistoryInsureForm = (UwHistoryInsureForm) form;
        String policyId = request.getParameter("policyId").toString();
        String forward = "display";

        if (!StringUtils.isNullOrEmpty(policyId)) {
        	uwHistoryInsureForm.setPolicyId(new Long(policyId));
        	
        	List<UwInsureClientVO> insureClientList = new ArrayList<UwInsureClientVO>();
        	//獲得財務資訊
        	uwInsureHistoryService.findUwFinanceNotiFy(insureClientList, policyId);
        	
        	uwHistoryInsureForm.setInsureClientList(insureClientList);

        }

        return mapping.findForward(forward);

    }

}
