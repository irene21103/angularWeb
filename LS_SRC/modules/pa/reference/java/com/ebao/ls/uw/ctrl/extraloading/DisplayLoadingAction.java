package com.ebao.ls.uw.ctrl.extraloading;

import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.ls.pa.pub.ci.CoverageCI;
import com.ebao.ls.pa.pub.ref.api.prd.ProductService;
import com.ebao.ls.pa.pub.service.PolicyService;
import com.ebao.ls.pa.pub.vo.CoverageVO;
import com.ebao.ls.pub.CodeCst;
import com.ebao.ls.uw.ctrl.UwGenericAction;
import com.ebao.ls.uw.ctrl.pub.CustomerForm;
import com.ebao.ls.uw.ctrl.pub.UwExtraLoadingForm;
import com.ebao.ls.uw.ctrl.pub.UwProductForm;
import com.ebao.ls.uw.ds.sp.QueryUwDataSp;
import com.ebao.ls.uw.util.Utils;
import com.ebao.ls.uw.vo.UwExtraLoadingVO;
import com.ebao.ls.uw.vo.UwProductVO;
import com.ebao.pub.framework.ExceptionFactory;
import com.ebao.pub.framework.GenericException;
import com.ebao.pub.util.BeanUtils;
import com.ebao.pub.util.DateUtils;

/**
 * <p>
 * Title:Underwriting
 * </p>
 * <p>
 * Description:
 * </p>
 * <p>
 * Copyright: Copyright (c) 2004
 * </p>
 * <p>
 * Company: eBaoTech Corporation
 * </p>
 * 
 * @author mingchun.shi
 * @since Jun 27, 2005
 * @version 1.0
 */
public class DisplayLoadingAction extends UwGenericAction {
  public static final String BEAN_DEFAULT = "/uw/displayLoading";

/**
   * reloading action
   * 
   * @param mapping
   * @param form
   * @param req
   * @param res
   * @return
   * @throws GenericException
   */
  @Override
  public ActionForward uwProcess(ActionMapping mapping, ActionForm form,
      HttpServletRequest req, HttpServletResponse res) throws GenericException {
    try {
      // get the value from page
      Long underwriteId = Long.valueOf(req.getParameter("underwriteId"));
      Long itemId = Long.valueOf(req.getParameter("itemIds"));
      Long insuredId = Long.valueOf(req.getParameter("insured"));
      // calculate the extra prem
      // ActionUtil.calAndSaveExtraLoading(req);
      UwProductVO productValue = initProductVO(req, underwriteId, itemId);
      Collection insureds = this.getUwPolicyDS().findUwLifeInsuredEntitis(
          Long.valueOf(req.getParameter("underwriteId")),
          Long.valueOf(req.getParameter("itemIds")));
      req.setAttribute("insured", insuredId);
      insureds = BeanUtils.copyCollection(CustomerForm.class, insureds);
      CustomerForm[] insured = (CustomerForm[]) insureds
          .toArray(new CustomerForm[0]);
      Long policyId = productValue.getPolicyId();
      String uwSourceType = req.getParameter("uwSourceType");
      if (!CodeCst.UW_SOURCE_TYPE__NB.equals(uwSourceType)) {
        setDueDateList(policyId, req);
      }
      req.setAttribute("insureds", insured);
      req.setAttribute("action_form",
          decompose(productValue, req, underwriteId, itemId, insuredId));
    } catch (Exception e) {
      throw ExceptionFactory.parse(e);
    }
    return mapping.findForward("extra");
  }

  /**
   * get due date list
   * 
   * @param policyId
   * @param request
   * @throws Exception
   */
  public void setDueDateList(Long policyId, HttpServletRequest request)
      throws Exception {
    CoverageVO policyProductVO = coverageCI.getMainBenefitByPolicyId(policyId);
    Integer mainProductId = policyProductVO.getProductId();
    Date validateDate = policyProductVO.getInceptionDate();
    String chargeType = policyProductVO.getCurrentPremium().getPaymentFreq();
    Date paidUpDate = policyProductVO.getPaidupDate();
    boolean isIlp = this.getProductService()
        .getProduct(Long.valueOf(mainProductId.intValue()),policyService.getActiveVersionDate(policyProductVO)).isIlp();
    List dueDateList = Utils.getAllDueDate(validateDate, chargeType, isIlp,
        paidUpDate);
    String dueDates = "";
    for (Iterator iter = dueDateList.iterator(); iter.hasNext();) {
      String element = (String) iter.next();
      dueDates = "'" + element + "'" + "," + dueDates;
    }
    // add by hanzhong.yan for cq:GEL00029962 2007/9/7
    if ("1".equals(policyProductVO.getCoveragePeriod()) && isIlp) {// if meet
      // ilp &&
      // cover
      // whole
      // life,then
      // allow
      // 09/09/9999
      dueDates = "'09/09/9999'," + dueDates;
    }
    // end add
    if (dueDates.endsWith(",")) {
      dueDates = dueDates.substring(0, dueDates.length() - 1);
    }
    String dateArray = "[" + dueDates + "]";
    request.setAttribute("dateArray", dateArray);
    // added to fix defect GEL00023057
    String paidUpDateStr = DateUtils.date2String(paidUpDate);
    paidUpDateStr = "'" + paidUpDateStr + "'";
    request.setAttribute("paidUpDate", paidUpDateStr);
    //    
  }

  /**
   * init the product vo
   * 
   * @param req
   * @return
   * @throws Exception
   */
  public UwProductVO initProductVO(HttpServletRequest req, Long underwriteId,
      Long itemId) throws Exception {
    UwProductVO uwProductVO = getUwPolicyDS().findUwProduct(underwriteId,
        itemId);
    // set duration limit
    req.setAttribute("durationScope", QueryUwDataSp.getPremTerm(uwProductVO,policyService.getActiveVersionDate(uwProductVO)));
    // get the special product extraLoading ariths
    String[] ariths = getExtraAriths(Long.valueOf(uwProductVO.getProductId()
        .intValue()),policyService.getActiveVersionDate(uwProductVO));
    String arithsFlag = "";
    if (ariths.length == 0) {
      arithsFlag = "true";
    }
    String sqlAriths = "ADD_ARITH in (";
    for (int i = 0; i < ariths.length - 1; i++) {
      sqlAriths = sqlAriths + "'" + ariths[i] + "'" + ",";
    }
    if (!"true".equals(arithsFlag)) {
      sqlAriths = sqlAriths + "'" + ariths[ariths.length - 1] + "'" + ")";
    } else {
      sqlAriths = sqlAriths + "99)";
    }
    req.setAttribute("sqlAriths", sqlAriths);
    req.setAttribute("arithsFlag", arithsFlag);
    return uwProductVO;
  }

  /**
   * get the UwExtraLoadingInfoForm value
   * 
   * @param productValue
   * @param req
   * @param insuredId
   * @param itemId
   * @param underwriteId
   * @return
   * @throws Exception
   */
  public UwExtraLoadingInfoForm decompose(UwProductVO productValue,
      HttpServletRequest req, Long underwriteId, Long itemId, Long insuredId)
      throws Exception {
    UwExtraLoadingInfoForm extraForm = new UwExtraLoadingInfoForm();
    extraForm.setApplyCode(req.getParameter("applyCode"));
    extraForm.setPolicyCode(req.getParameter("policyCode"));
    extraForm.setBenefityType(req.getParameter("benefitType"));
    extraForm.setUwStatus(req.getParameter("uwStatus"));
    try {
      UwProductForm productForm = getUwProductForm(req);
      if ("true".equals(req.getParameter("isLoading"))) {
        extraForm.setUwProductForm(productForm);
      } else {
        BeanUtils.copyProperties(extraForm.getUwProductForm(), productValue);
      }
    } catch (Exception e) {
      throw ExceptionFactory.parse(e);
    }
    String decisionId = req.getParameter("decisionId");
    if (decisionId != null && decisionId.trim().length() > 0) {
      extraForm.getUwProductForm().setDecisionId(
          Integer.valueOf(decisionId.trim()));
    } else {
      extraForm.getUwProductForm().setDecisionId(Integer.valueOf(2));
    }
    // get the default charge period
    int chargeYear = QueryUwDataSp.getChargeTerm(productValue.getItemId());
    extraForm.getUwProductForm().setChargeYear(Integer.valueOf(chargeYear));

    extraForm.setDuration(chargeYear + "");
    Collection allExtra = getUwPolicyDS().findUwExtraLoadingEntitis(
        underwriteId, itemId, insuredId);
    UwExtraLoadingVO[] extraLoadingVOs = (UwExtraLoadingVO[]) allExtra
        .toArray(new UwExtraLoadingVO[0]);
    if (extraLoadingVOs != null) {
      for (int i = 0; i < extraLoadingVOs.length; i++) {
        UwExtraLoadingVO extraLoading = extraLoadingVOs[i];
        UwExtraLoadingForm extraLoadingForm = new UwExtraLoadingForm();
        if (extraLoading != null) {
          BeanUtils.copyProperties(extraLoadingForm, extraLoading);
          if (!"".equals(extraLoading.getExtraType())) {
            if (extraLoading.getExtraType() != null) {
              switch (extraLoading.getExtraType().charAt(0)) {
              case 'A':
                extraForm.getHealth().add(extraLoadingForm);
                extraForm.setHealthFlag("Y");
                break;
              case 'B':
                extraForm.getOccupation().add(extraLoadingForm);
                extraForm.setOccupationFlag("Y");
                break;
              case 'C':
                extraForm.getAvocation().add(extraLoadingForm);
                extraForm.setAvocationFlag("Y");
                break;
              case 'D':
                extraForm.getResidential().add(extraLoadingForm);
                extraForm.setResidentialFlag("Y");
                break;
              case 'E':
                extraForm.getAviation().add(extraLoadingForm);
                extraForm.setAviationFlag("Y");
                break;
              case 'F':
                extraForm.getOther().add(extraLoadingForm);
                extraForm.setOtherFlag("Y");
                break;
              }
            }
          }
        }
      }
    }
    if (extraForm.getHealth().size() == 0) {
      extraForm.getHealth().add(new UwExtraLoadingForm());
    }
    if (extraForm.getOccupation().size() == 0) {
      extraForm.getOccupation().add(new UwExtraLoadingForm());
    }
    if (extraForm.getAvocation().size() == 0) {
      extraForm.getAvocation().add(new UwExtraLoadingForm());
    }
    if (extraForm.getResidential().size() == 0) {
      extraForm.getResidential().add(new UwExtraLoadingForm());
    }
    if (extraForm.getAviation().size() == 0) {
      extraForm.getAviation().add(new UwExtraLoadingForm());
    }
    if (extraForm.getOther().size() == 0) {
      extraForm.getOther().add(new UwExtraLoadingForm());
    }
    return extraForm;
  }

  /**
   * get the UwProductForm value
   * 
   * @param req HttpServletRequest
   * @throws Exception
   * @return UwProductForm
   */
  public UwProductForm getUwProductForm(HttpServletRequest req)
      throws Exception {
    UwProductForm productForm = new UwProductForm();
    Long underwriteId = Long.valueOf(req.getParameter("underwriteId"));
    Long itemId = Long.valueOf(req.getParameter("itemIds"));
    UwProductVO pvo = getUwPolicyDS().findUwProduct(underwriteId, itemId);
    productForm.setInitialType(pvo.getInitialType());
    productForm.setCountWay(pvo.getCountWay());
    productForm.setUnderwriteId(Long.valueOf(req.getParameter("underwriteId")));
    productForm.setAge1(pvo.getAge1() != null ? pvo.getAge1() : null);
    productForm.setAge2(pvo.getAge2() != null ? pvo.getAge2() : null);
    productForm.setItemId(Long.valueOf(req.getParameter("itemIds")));
    Long insurdedId = Long.valueOf(0);
    if ((req.getParameter("insured") != null)
        && (req.getParameter("insured").length() > 0)) {
      insurdedId = Long.valueOf(req.getParameter("insured"));
    }
    boolean firstInsured = insurdedId.longValue() == pvo.getInsured1()
        .longValue() ? true : false;
    if ((req.getParameter("policyId") != null)
        && (req.getParameter("policyId").length() > 0)) {
      productForm.setPolicyId(Long.valueOf(req.getParameter("policyId")));
    }
    if (firstInsured) {
      productForm.setInsured1(Long.valueOf(req.getParameter("insured")));
    } else {
      productForm.setInsured2(Long.valueOf(req.getParameter("insured")));
    }
    if ((req.getParameter("coverAgeYear") != null)
        && (req.getParameter("coverAgeYear").length() > 0)) {
      productForm.setCoverageYear(Integer.valueOf(req
          .getParameter("coverAgeYear")));
    }
    if ((req.getParameter("chargeYear") != null)
        && (req.getParameter("chargeYear").length() > 0)) {
      productForm
          .setChargeYear(Integer.valueOf(req.getParameter("chargeYear")));
    }
    if ((req.getParameter("productId") != null)
        && (req.getParameter("productId").length() > 0)) {
      productForm.setProductId(Integer.valueOf(req.getParameter("productId")));
    }
    if ((req.getParameter("underwriterId") != null)
        && (req.getParameter("underwriterId").length() > 0)) {
      productForm.setUnderwriterId(Long.valueOf(req
          .getParameter("underwriterId")));
    }
    if ((req.getParameter("commenceDate") != null)
        && (req.getParameter("commenceDate").length() > 0)) {
      productForm.setValidateDate(DateUtils.toDate(req
          .getParameter("commenceDate")));
    }
    String advantageIND = req.getParameter("advantageIND");
    if ((advantageIND != null) && (advantageIND.length() > 0)) {
      if (firstInsured) {
        productForm.setAdvantageIndi1(advantageIND);
      } else {
        productForm.setAdvantageIndi2(advantageIND);
      }
    }
    return productForm;
  }

  /**
   * get the extraLoading arichs for specisal product
   * 
   * @param productId
   * @return
   * @throws GenericException
   */
  private String[] getExtraAriths(Long productId, Date versionDate) throws GenericException {
    List<String> extraTypes = this.getProductService().getProduct(productId,versionDate)
        .getExtraTypes();
    return extraTypes.toArray(new String[extraTypes.size()]);
  }

  @Resource(name = CoverageCI.BEAN_DEFAULT)
  private CoverageCI coverageCI;

  public CoverageCI getCoverageCI() {
    return coverageCI;
  }

  public void setCoverageCI(CoverageCI coverageCI) {
    this.coverageCI = coverageCI;
  }

  @Resource(name = ProductService.BEAN_DEFAULT)
  private ProductService productService;

  public ProductService getProductService() {
    return productService;
  }

  public void setProductService(ProductService productService) {
    this.productService = productService;
  }
  
  @Resource(name = PolicyService.BEAN_DEFAULT)
  PolicyService policyService;
}
