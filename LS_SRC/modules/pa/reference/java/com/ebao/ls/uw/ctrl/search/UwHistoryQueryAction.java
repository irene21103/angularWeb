package com.ebao.ls.uw.ctrl.search;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ebao.ls.escape.helper.EscapeHelper;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.ls.uw.ctrl.ReqParamNameConstants;
import com.ebao.ls.uw.ctrl.pub.UwConditionForm;
import com.ebao.ls.uw.ctrl.pub.UwExclusionForm;
import com.ebao.ls.uw.ctrl.pub.UwProductForm;
import com.ebao.ls.uw.ds.UwPolicyService;
import com.ebao.ls.uw.vo.UwPolicyVO;
import com.ebao.pub.framework.GenericAction;
import com.ebao.pub.util.BeanUtils;
import com.ebao.pub.util.Env;

/**
 * UwHistory Query Action 
 * <p>Title:Underwriting</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: eBaoTech Corporation</p>
 * @author mingchun.shi
 * @since  Jun 27, 2005
 * @version 1.0
 */
public class UwHistoryQueryAction extends GenericAction {

  /**
   * Underwriting history url logic name
   */
  public static final String UW_HISTORY_URL = "uwHistoryDetail";

  /**
   * Underwriting history collection request key
   */
  public static final String UW_HISTORY_LIST = "uw_history_list";

public static final String BEAN_DEFAULT = "/uw/uwHistory";

  /**
   * Default Constructor
   */
  public UwHistoryQueryAction() {

  }

  /**
   * UwHistory Query Action main entry.
   * @param mapping
   * @param form
   * @param request
   * @param response
   * @return
   * @throws Exception
   */
  @Override
  public ActionForward process(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {
    Collection result = new ArrayList();
    //get UwHistorys
    Collection historys = uwPolicyDS.queryUwHistory(
        request.getParameter(ReqParamNameConstants.PROPOSALNO),
        request.getParameter(ReqParamNameConstants.UW_APP_TYPE_CODE),
        request.getParameter(ReqParamNameConstants.HISTORY_DISPLAY_STATUS));
    java.util.Iterator historyIter = historys.iterator();
    //changes UwHistory into UwHistoryForm
    while (historyIter.hasNext()) {
      UwPolicyVO vo = (UwPolicyVO) historyIter.next();
      Date dueDate = uwPolicyDS.getPolicyNextDueDate(vo.getPolicyId()
          .longValue());
      UwHistoryForm uhForm = new UwHistoryForm();
      BeanUtils.copyProperties(uhForm, vo);
      uhForm.setDueDate(dueDate);
      result.add(uhForm);
      //get UwProductForms and attaches the UwProductForms into UwHistoryForm
      Collection products = uwPolicyDS.findUwProductEntitis(uhForm
          .getUnderwriteId());
      if (null != products) {
        UwProductForm[] forms = new UwProductForm[products.size()];
        products = BeanUtils.copyCollection(UwProductForm.class, products);
        System.arraycopy(products.toArray(), 0, forms, 0, forms.length);
        uhForm.setUwproducts(forms);
      }
      //get UwConditionForms and attaches the UwConditionForms into UwHistoryForm
      Collection conditions = uwPolicyDS.findUwConditionEntitis(uhForm
          .getUnderwriteId());
      if (null != conditions) {
        UwConditionForm[] forms = new UwConditionForm[conditions.size()];
        conditions = BeanUtils
            .copyCollection(UwConditionForm.class, conditions);
        System.arraycopy(conditions.toArray(), 0, forms, 0, forms.length);
        if (0 < forms.length) {
          uhForm.setUwconditions(forms);
        }
      }
      //get UwExclusionForms and attaches the UwUwExclusionForms into UwHistoryForm
      Collection exclusions = uwPolicyDS.findUwExclusionEntitis(uhForm
          .getUnderwriteId());
      if (null != exclusions) {
        UwExclusionForm[] forms = new UwExclusionForm[exclusions.size()];
        exclusions = BeanUtils
            .copyCollection(UwExclusionForm.class, exclusions);
        System.arraycopy(exclusions.toArray(), 0, forms, 0, forms.length);
        if (0 < forms.length) {
          uhForm.setUwexclusions(forms);
        }
      }
    }//end of historyIter.hasNext()
     //add by hanzhong.yan for cq:GEL00035333 2007/11/1
    List resultList = new ArrayList(result);
    if (resultList != null) {
      Collections.sort(resultList, new Comparator() {

        @Override
        public int compare(Object o1, Object o2) {
          UwHistoryForm form1 = (UwHistoryForm) o1;
          UwHistoryForm form2 = (UwHistoryForm) o2;
          if (form1.getInsertTime().getTime() > form2.getInsertTime().getTime())
            return -1;
          else
            return 1;
        }

      });
    }
    //stores the result into the request
    request.setAttribute(UW_HISTORY_LIST, resultList);
    String underwriteId = request.getParameter("underwriteId");
    String roleType = request.getParameter("roleType");
    String originSource = request.getParameter("originSource");
    String exitUrl = null;
    if ("commonQuery".equalsIgnoreCase(originSource)) {
      Long insuredId = Long.valueOf(request.getParameter("insuredId"));
      exitUrl = Env.URL_PREFIX + "/uw/showLastestRA.do?partyId=" + insuredId;
    } else {
      exitUrl = Env.URL_PREFIX + "/uw/showRiskAggregation.do?underwriteId="
          + underwriteId + "&roleType=" + roleType;

    }
    if (roleType != null && roleType.trim().length() > 0) {
      EscapeHelper.escapeHtml(request).setAttribute("exitUrl", exitUrl);
    }
    //send to history page for rending the client result 
    return mapping.findForward(UW_HISTORY_URL);
  }

  @Resource(name = UwPolicyService.BEAN_DEFAULT)
  private UwPolicyService uwPolicyDS;

  public void setUwPolicyDS(UwPolicyService uwPolicyDS) {
    this.uwPolicyDS = uwPolicyDS;
  }

}
