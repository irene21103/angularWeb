package com.ebao.ls.crs.ci.impl;

import java.sql.CallableStatement;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;

import com.ebao.foundation.common.lang.exception.ExceptionUtils;
import com.ebao.foundation.commons.exceptions.ExceptionUtil;
import com.ebao.foundation.commons.exceptions.GenericException;
import com.ebao.ls.clm.bs.dataKeyIn.DataKeyInFatcaCrsService;
import com.ebao.ls.crs.ci.CRSPartyCI;
import com.ebao.ls.crs.ci.CRSPartyReportVO;
import com.ebao.ls.crs.company.CRSControlManService;
import com.ebao.ls.crs.company.CRSEntityService;
import com.ebao.ls.crs.constant.CRSCodeCst;
import com.ebao.ls.crs.customer.CRSIndividualService;
import com.ebao.ls.crs.data.batch.CrsRightNotifyDAO;
import com.ebao.ls.crs.data.bo.CrsRightNotify;
import com.ebao.ls.crs.exceptions.ValidateException;
import com.ebao.ls.crs.identity.CRSPartyLogService;
import com.ebao.ls.crs.identity.CRSPartyService;
import com.ebao.ls.crs.identity.CRSTaxIDNumberLogService;
import com.ebao.ls.crs.identity.CRSTaxIDNumberService;
import com.ebao.ls.crs.vo.CRSControlManVO;
import com.ebao.ls.crs.vo.CRSEntityVO;
import com.ebao.ls.crs.vo.CRSIndividualVO;
import com.ebao.ls.crs.vo.CRSPartyLogVO;
import com.ebao.ls.crs.vo.CRSPartyVO;
import com.ebao.ls.crs.vo.CRSTaxIDNumberLogVO;
import com.ebao.ls.crs.web.WebWarningMessage;
import com.ebao.ls.cs.logger.ApplicationLogger;
import com.ebao.ls.cs.logger.helper.AppLogDataHelper;
import com.ebao.ls.pub.CodeCst;

public class CRSPartyCIImpl implements CRSPartyCI{
	
	@Resource(name=CRSPartyService.BEAN_DEFAULT)
	private CRSPartyService partyService;
	
	@Resource(name=CRSPartyLogService.BEAN_DEFAULT)
	private CRSPartyLogService partyLogService;
	
	@Resource(name=CRSIndividualService.BEAN_DEFAULT)
	private CRSIndividualService customerService;
	
	@Resource(name=CRSEntityService.BEAN_DEFAULT)
	private CRSEntityService companyService;
	
	@Resource(name=CRSControlManService.BEAN_DEFAULT)
	private CRSControlManService controllerService;
	
	@Resource(name=CRSTaxIDNumberService.BEAN_DEFAULT)
	private CRSTaxIDNumberService taxIdNumberService;
	
	@Resource(name=CRSTaxIDNumberLogService.BEAN_DEFAULT)
	private CRSTaxIDNumberLogService taxIdNumberLogService;
	
	@Resource(name = CrsRightNotifyDAO.BEAN_DEFAULT)
    private CrsRightNotifyDAO notifyDao;

	@Resource(name = DataKeyInFatcaCrsService.BEAN_DEFAULT)
	private DataKeyInFatcaCrsService dataKeyInFatcaCrsService;
	
	@Override
	public List<CRSPartyLogVO> findCRSPartyLog(String certiCode, Long changeId) {
		return partyLogService.findListByCertiCode(certiCode, changeId);
	}

	@Override
	public List<CRSPartyLogVO> findCRSPartyLog(String certiCode) {
		return this.findCRSPartyLog(certiCode, null);
	}

	@Override
	public List<CRSPartyLogVO> findCRSPartyLog(Long changeId) {
		return this.findCRSPartyLog(null, changeId);
	}

	@Override
	public CRSPartyVO findParty(String certiCode) {
		if(StringUtils.isEmpty(certiCode))
			return null;
		java.util.Map<String,Object> params = new HashMap<String, Object>();
		params.put("certiCode", certiCode);
		Map<String,Object> order = new HashMap<String,Object>();
		//PCR-257607 add order by condition, if result records is duplicate   
		order.put("docSignDate","desc");
		order.put("crsId","asc");
		java.util.List<CRSPartyVO> partyList = this.partyService.find(params, order);
		if(CollectionUtils.isEmpty(partyList))
			return null;
		return partyList.get(0);
	}
	
	@Override
	public CRSPartyVO findPartyWithoutCertiCode(String policyCode, String name) {
		if(StringUtils.isEmpty(policyCode)|| StringUtils.isEmpty(name))
			return null;
		Map<String,Object> params = new HashMap<String, Object>();
		params.put("policyCode", policyCode);
		params.put("name", name);
		Map<String,Object> order = new HashMap<String,Object>();
		order.put("crsId","desc");
		java.util.List<CRSPartyVO> partyList = this.partyService.find(params, order);
		if(CollectionUtils.isEmpty(partyList))
			return null;
		return partyList.get(0);
	}
	
	@Override
	public CRSPartyVO findPartybyPolicyName(String policyCode, String name) {
		if(StringUtils.isEmpty(policyCode) || StringUtils.isEmpty(name))
			return null;
		java.util.Map<String,Object> params = new HashMap<String, Object>();
		java.util.Map<String,Object> orders = new HashMap<String, Object>();		
		params.put("policyCode", policyCode);
		params.put("name", name);
		orders.put("docSignDate", "desc");
		
		java.util.List<CRSPartyVO> partyList = this.partyService.find(params,orders);
		if(CollectionUtils.isEmpty(partyList)){
			return null;
		}else {
			return partyList.get(0);
		}
					
	}
	
	private Boolean validateSubmit(CRSPartyLogVO partyLogVO) throws ValidateException{
		WebWarningMessage msg = new WebWarningMessage();
		if(!CRSCodeCst.BUILD_TYPE__BUILDING.equals(partyLogVO.getBuildType())) {
			msg.add("BUILD_TYPE", "建檔狀態不為[建檔中]");
		}
		if(!CRSCodeCst.INPUT_SOURCE__BAT.equals(partyLogVO.getInputSource()) && StringUtils.isEmpty(partyLogVO.getAgentAnnounce())) {
			msg.add("[AGENT_ANNOUNCE]", "[業務員聲明] 欄位尚未填寫");
		}
		if(StringUtils.isEmpty(partyLogVO.getDocSignStatus())) {
			msg.add("[DOC_SIGN_STATUS]", "[聲明書簽署狀態] 欄位尚未填寫");
		}
		if(StringUtils.isNotEmpty(partyLogVO.getDocSignStatus()) && partyLogVO.getDocSignDate() == null) {
			msg.add("[DOC_SIGN_DATE]", "[狀態日期] 欄位尚未填寫");
		}
		if(CRSCodeCst.CRS_DOC_SIGN_STATUS__SIGNOFF.equals(partyLogVO.getDocSignStatus()) && partyLogVO.getDeclarationDate() == null) {
			msg.add("[DECLARATION_DATE]", "[聲明書簽署狀態]=已簽署,但 [聲明日期 西元年/月/日] 欄位尚未填寫");
		}
		if(CollectionUtils.isNotEmpty(msg.toMsgInfoList())) {
			throw new ValidateException(msg);
		}
		return Boolean.TRUE;
	}
	@Override
	public CRSPartyReportVO submitByLogId(Long logId) throws GenericException {
		CRSPartyLogVO partyLogVO = this.partyLogService.load(logId);
		/* 提交作業  */
		CallableStatement proc = null;
		java.sql.Connection conn = null;
        com.ebao.foundation.module.db.DBean db = new com.ebao.foundation.module.db.DBean();
        try {
			db.connect();
			conn = db.getConnection();
			
			// PCR 458639 [理賠集中建檔]集中登打頁面~FATCA&CRS
			String clmDocumentId = partyLogVO.getClmDocumnetId();
			
			// 有理賠集中建檔寫入clmDocumentId 回壓 tempVO.IsClmSubmint = 'Y'
			if (StringUtils.isNotEmpty(clmDocumentId)) {
				
				partyLogVO.setIsClmSubmint(CodeCst.YES_NO__YES);
								
				//呼叫理賠提供接口回壓理賠資訊
				dataKeyInFatcaCrsService.doUpdateCrsIndi(clmDocumentId);		
			}	
			
			this.partyLogService.save(partyLogVO);
			String proceduleName = "CALL PKG_LS_CRS_CI.P_LOG_2_CRS(?) ";
			proc = conn.prepareCall(proceduleName);
			proc.setLong(1, logId);
			proc.execute();
			
		} catch (SQLException e) {
			String errMsg = String.format("[T_CRS_PARTY_LOG] log_id := %d \n%s " ,logId, ExceptionUtils.getFullStackTrace(e));
			this.writeAppLog(partyLogVO.getPolicyCode(), ApplicationLogger.RETURN_CODE_FAIL, errMsg);
			throw ExceptionUtil.parse(e);
		} catch (ClassNotFoundException e) {
			String errMsg = String.format("[T_CRS_PARTY_LOG] log_id := %d \n%s " ,logId, ExceptionUtils.getFullStackTrace(e));
			this.writeAppLog(partyLogVO.getPolicyCode(), ApplicationLogger.RETURN_CODE_FAIL, errMsg);
			throw ExceptionUtil.parse(e);
		} catch(Exception e){
			String errMsg = String.format("[T_CRS_PARTY_LOG] log_id := %d \n%s " ,logId, ExceptionUtils.getFullStackTrace(e));
			this.writeAppLog(partyLogVO.getPolicyCode(), ApplicationLogger.RETURN_CODE_FAIL, errMsg);
			throw ExceptionUtil.parse(e);
		} finally {
			com.ebao.foundation.module.db.DBean.closeAll(null, proc, db);
		}
        return generateCRSPartyReport(partyLogVO.getCertiCode());
	}
	
	private void writeAppLog(String policyCode, String returnCode, String loggerData) {
		ApplicationLogger.setPolicyCode(policyCode);
		ApplicationLogger.setOptionId(com.ebao.pub.util.DateUtils.date2String(new Date(), "yyyyMMdd HHmmss"));
		ApplicationLogger.setSystemCode(AppLogDataHelper.SYSTEM_CODE_CMN);
		ApplicationLogger.setJobName("CRSPartyCI.submitByLogId");
		ApplicationLogger.setReturnCode(returnCode);
		ApplicationLogger.addLoggerData(loggerData);
	}
	@Override
	public void markRemove(Long logId) {
		this.partyLogService.markRemove(logId);
	}

	@Override
	public CRSPartyReportVO generateCRSPartyReport(String certiCode) throws GenericException {
		CRSPartyVO partyVO = this.partyService.findByCertiCode(certiCode);
		if(partyVO == null)
			return null;
		CRSPartyReportVO reportVO = new CRSPartyReportVO();
		reportVO.setParty(partyVO);
		if(CRSCodeCst.CRS_IDENTITY_TYPE__MAN.equals(partyVO.getCrsCode())) {
			//個人
			CRSIndividualVO customerVO = customerService.findByCertiCode(certiCode);
			reportVO.setIndividual(customerVO);
			//稅藉資料
		}else if(CRSCodeCst.CRS_IDENTITY_TYPE__COMPANY.equals(partyVO.getCrsCode())){
			//法人
			CRSEntityVO entityVO = companyService.findByCertiCode(certiCode);
			reportVO.setEntity(entityVO);
			//法人-稅藉資料
			//控制權人
			java.util.List<CRSControlManVO> controlVOList = controllerService.findByCertiCode(certiCode);
			reportVO.setControlManList(controlVOList);
		}
		return reportVO;
	}

	@Override
	public List<CRSPartyLogVO> findCRSPartyLog(String certiCode, String policyCode, String sysCode) {
		return partyLogService.findCRSPartyLog(certiCode, policyCode, sysCode);
	}

	
	@Override
    public CrsRightNotify markRightNotify(Long crsId, String certiCode, Date processDate) throws GenericException {		
		Long compareZero =0L;
		if (crsId.compareTo(compareZero) > 0){
			//一般案例 crsID > 0 。 取updateTime最新一筆
			CrsRightNotify identity = notifyDao.quryByCrsId(crsId);       
		    if (identity == null || identity.getCrsId() == null) {
		    	   notifyDao.create(crsId, certiCode, processDate);
		    	   identity = notifyDao.quryByCrsId(crsId); 
		     }else {
		    	   identity = notifyDao.updNotifyDate(crsId, certiCode, processDate) ;
		    }
		       
		    return identity;           	
		}else {
			//要保人融通件，沒有CRS主檔資料(crsid=-1)
			CrsRightNotify identity = notifyDao.quryByCrsIdCertiCode(crsId, certiCode);
			if (identity == null || identity.getCrsId() == null) {
		    	   notifyDao.create(crsId, certiCode, processDate);
		    	   identity = notifyDao.quryByCrsIdCertiCode(crsId, certiCode);
		       }else {
		    	   identity = notifyDao.updNotifyDate(crsId, certiCode, processDate) ;
		       }		       
		    return identity;
		}
		
       
   }
	
	@Override
	public List<CRSTaxIDNumberLogVO> findByLogIdAndBizSource(Long partyLogId, String bizSource1, String bizSource2) {
		return taxIdNumberLogService.findByLogIdAndBizSource(partyLogId, bizSource1, bizSource2);
	}

}
