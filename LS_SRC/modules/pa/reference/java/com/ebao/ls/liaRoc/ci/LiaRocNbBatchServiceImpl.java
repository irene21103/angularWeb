package com.ebao.ls.liaRoc.ci;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.ebao.foundation.module.db.hibernate.HibernateSession3;
import com.ebao.ls.foundation.service.GenericServiceImpl;
import com.ebao.ls.liaRoc.LiaRocCst;
import com.ebao.ls.liaRoc.bo.LiaRocNbBatch;
import com.ebao.ls.liaRoc.bo.LiaRocNbBatchVO;
import com.ebao.ls.liaRoc.bo.LiaRocUploadDetail2020;
import com.ebao.ls.liaRoc.data.LiaRocNbBatchDao;
import com.ebao.ls.liaRoc.data.LiaRocUploadDetailDao;
import com.ebao.ls.pa.pub.bs.coverage.CoverageService;
import com.ebao.ls.pa.pub.service.PolicyService;
import com.ebao.ls.pa.pub.vo.CoverageInsuredVO;
import com.ebao.ls.pa.pub.vo.CoverageVO;
import com.ebao.ls.pa.pub.vo.PolicyVO;
import com.ebao.ls.pub.CodeCst;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.framework.ExceptionFactory;
import com.ebao.pub.util.DateUtils;
import com.ebao.pub.util.StringUtils;

public class LiaRocNbBatchServiceImpl extends GenericServiceImpl<LiaRocNbBatchVO, LiaRocNbBatch, LiaRocNbBatchDao> 
				implements LiaRocNbBatchService{
	
	@Resource(name = PolicyService.BEAN_DEFAULT)
	private PolicyService policyService;
	
	@Resource(name = com.ebao.ls.pa.pub.bs.PolicyService.BEAN_DEFAULT)
	private com.ebao.ls.pa.pub.bs.PolicyService policyDS;
	
    @Resource(name = LiaRocUploadDetailDao.BEAN_DEFAULT)
	public LiaRocUploadDetailDao liaRocUploadDetailDao;
    
    @Resource(name = LiaRocNbBatchDao.BEAN_DEFAULT)
	public LiaRocNbBatchDao liaRocNbBatchDao;

	@Resource(name = LiaRocUploadCommonService.BEAN_DEFAULT)
	private LiaRocUploadCommonService liaRocUploadCommonService;
	
	@Resource(name = CoverageService.BEAN_DEFAULT)
	protected CoverageService coverageService;
	
    @Override
    public void save1KeyData(Long policyId, String changeType) {
    	
		Long defaultItemId = 1L; //預收輸入儲存是整張保單資料，故itemId 預設 = 1

    	LiaRocNbBatchVO liaRocNbBatchVO = new LiaRocNbBatchVO();
		liaRocNbBatchVO.setPolicyId(policyId);
		liaRocNbBatchVO.setChangeType(LiaRocCst.LIAROC_NB_BATCH_CHANGE_TYPE_01);
		liaRocNbBatchVO.setChangeItemId(defaultItemId);
		
		this.save(saveCommonData(liaRocNbBatchVO,true,null));
    }

    @Override
	public List<LiaRocNbBatchVO> getLiaRocNbBatchData(String changeType) {

        try {
            if (changeType == null) {
                throw new NullPointerException();
            }
            
    		Session session = HibernateSession3.currentSession();
    		Criteria criteria = session.createCriteria(LiaRocNbBatch.class);
    		criteria.add(Restrictions.eq("changeType", changeType))
    					.add(Restrictions.eq("checkStatus",LiaRocCst.LIAROC_NB_BATCH_CHECK_STATUS_NO));
    		criteria.addOrder(Order.asc("listId"));
    		java.util.List<LiaRocNbBatch> liaRocNbBatchList = (java.util.List<LiaRocNbBatch>) criteria.list();
    		return convertToVOList(liaRocNbBatchList);
        } catch (Exception e) {
            throw ExceptionFactory.parse(e);
        }

    }
    /**
     * 取得新式公會批次未通報資料(V2020)
     */   
    @Override
	public List<LiaRocNbBatchVO> getLiaRocNbBatchData2020(String changeType) {

        try {
            if (changeType == null) {
                throw new NullPointerException();
            }
            
    		Session session = HibernateSession3.currentSession();
    		Criteria criteria = session.createCriteria(LiaRocNbBatch.class);
    		criteria.add(Restrictions.eq("changeType", changeType))
    					.add(Restrictions.eq("checkStatus2020",LiaRocCst.LIAROC_NB_BATCH_CHECK_STATUS_NO));
    		criteria.addOrder(Order.asc("listId"));
    		java.util.List<LiaRocNbBatch> liaRocNbBatchList = (java.util.List<LiaRocNbBatch>) criteria.list();
    		return convertToVOList(liaRocNbBatchList);
        } catch (Exception e) {
            throw ExceptionFactory.parse(e);
        }
    } 
    
    /**
     * 依保單號碼、異動類別查詢尚未通報新式公會之資料
     */   
    @Override
	public List<LiaRocNbBatchVO> getLiaRocNbBatchData2020ByChangeType(Long policyId, String changeType) {

        try {
            if (changeType == null) {
                throw new NullPointerException();
            }
            
    		Session session = HibernateSession3.currentSession();
    		Criteria criteria = session.createCriteria(LiaRocNbBatch.class);
    		criteria.add(Restrictions.eq("policyId", policyId))
    					.add(Restrictions.eq("changeType", changeType))
    					.add(Restrictions.eq("checkStatus2020",LiaRocCst.LIAROC_NB_BATCH_CHECK_STATUS_NO));
    		criteria.addOrder(Order.asc("listId"));
    		java.util.List<LiaRocNbBatch> liaRocNbBatchList = (java.util.List<LiaRocNbBatch>) criteria.list();
    		return convertToVOList(liaRocNbBatchList);
        } catch (Exception e) {
            throw ExceptionFactory.parse(e);
        }
    } 

    /**
     * 險種新/刪:取得有同樣的policyId + certieCode + internalId +changeItemId+changeType=03,04(險種新/刪)+ checkStatus = 'N'(未處理) 公會批次未通報資料
     * 險種變更  :取得有同樣的policyId + certieCode + internalId +changeItemId+ changeType=05(險種修改)+ checkStatus = 'N'(未處理) 公會批次未通報資料
     * 
     */
	private List<LiaRocNbBatchVO> getLastLiaRocNbBatchData(Long policyId, String certiCode, String internalId,Long changeItemId) {

        try {
            if (policyId == null || StringUtils.isNullOrEmpty(certiCode) || StringUtils.isNullOrEmpty(internalId)) {
                throw new NullPointerException();
            }
            Map parameters = new HashMap();
            parameters.put("policyId", policyId);
            parameters.put("certiCodeBf", certiCode);
            parameters.put("checkStatus",LiaRocCst.LIAROC_NB_BATCH_CHECK_STATUS_NO);
            
            //比對舊資料若為03-新增/04-刪除險種，要以變更前險種做判斷
            List<LiaRocNbBatch> liaRocNbBatchList = (List<LiaRocNbBatch>) this.dao.findByCriteria(
            		Restrictions.allEq(parameters),
            		Restrictions.eq("internalIdBf", internalId),
            		Restrictions.eq("changeItemId", changeItemId),
                    Restrictions.between("changeType", 
                    		               LiaRocCst.LIAROC_NB_BATCH_CHANGE_TYPE_03,
                                	       LiaRocCst.LIAROC_NB_BATCH_CHANGE_TYPE_04 ));
            //比對舊資料若為05-變更險種，要以變更後險種做判斷 
           liaRocNbBatchList.addAll((List<LiaRocNbBatch>) this.dao.findByCriteria(
            		Restrictions.allEq(parameters),
            		Restrictions.eq("changeItemId", changeItemId),
            		Restrictions.eq("internalIdAf", internalId),
                    Restrictions.eq("changeType", LiaRocCst.LIAROC_NB_BATCH_CHANGE_TYPE_05)));
            
            return convertToVOList(liaRocNbBatchList);
        } catch (Exception e) {
            throw ExceptionFactory.parse(e);
        }
    }
	
	   /**
     * 取得有同樣的policyId + certieCode+changeType=02(ID/生日變更)+changeItemId + checkStatus2020 = 'N'(未處理) 公會批次未通報資料
     * 
     */
	private List<LiaRocNbBatchVO> getLastLiaRocNbBatchData02(Long policyId, Long changeItemId,String oldCertiCode,String newCertiCode) {

        try {
            if (policyId == null || StringUtils.isNullOrEmpty(oldCertiCode) || StringUtils.isNullOrEmpty(newCertiCode)) {
                throw new NullPointerException();
            }
            Map parameters = new HashMap();
            parameters.put("policyId", policyId);
            parameters.put("changeType", LiaRocCst.LIAROC_NB_BATCH_CHANGE_TYPE_02);
            parameters.put("changeItemId",changeItemId);
            parameters.put("certiCodeBf", newCertiCode);
            parameters.put("certiCodeAf", oldCertiCode);
            parameters.put("checkStatus",LiaRocCst.LIAROC_NB_BATCH_CHECK_STATUS_NO);
            
            //判斷未處理的ID變更是否有A ->B, B -> A
            List<LiaRocNbBatch> liaRocNbBatchList = (List<LiaRocNbBatch>) this.dao.findByCriteria(
            		Restrictions.allEq(parameters));
            
            return convertToVOList(liaRocNbBatchList);
        } catch (Exception e) {
            throw ExceptionFactory.parse(e);
        }
    }

    /**
     * 新式公會通報(V2020)
     * 險種新/刪:取得有同樣的policyId + certieCode + internalId + changeType=03,04(險種新/刪)+ checkStatus2020 = 'N'(未處理) 新式公會批次未通報資料
     * 險種變更  :取得有同樣的policyId + certieCode + internalId +changeItemId+ changeType=05(險種修改)+ checkStatus2020 = 'N'(未處理) 新式公會批次未通報資料
     */
	private List<LiaRocNbBatchVO> getLastLiaRoc2020NbBatchData(Long policyId, String certiCode, String internalId,Long changeItemId) {

        try {
            if (policyId == null || StringUtils.isNullOrEmpty(certiCode) || StringUtils.isNullOrEmpty(internalId)) {
                throw new NullPointerException();
            }
            Map parameters = new HashMap();
            parameters.put("policyId", policyId);
            parameters.put("certiCodeBf", certiCode);
            parameters.put("checkStatus2020",LiaRocCst.LIAROC_NB_BATCH_CHECK_STATUS_NO);
            
            //比對舊資料若為03-新增/04-刪除險種，要以變更前險種做判斷
            List<LiaRocNbBatch> liaRocNbBatchList = (List<LiaRocNbBatch>) this.dao.findByCriteria(
            		Restrictions.allEq(parameters),
            		Restrictions.eq("internalIdBf", internalId),
                    Restrictions.between("changeType", 
                    		               LiaRocCst.LIAROC_NB_BATCH_CHANGE_TYPE_03,
                                	       LiaRocCst.LIAROC_NB_BATCH_CHANGE_TYPE_04 ));
            //比對舊資料若為05-變更險種，要以變更後險種做判斷 
           liaRocNbBatchList.addAll((List<LiaRocNbBatch>) this.dao.findByCriteria(
            		Restrictions.allEq(parameters),
            		Restrictions.eq("changeItemId", changeItemId),
            		Restrictions.eq("internalIdAf", internalId),
                    Restrictions.eq("changeType", LiaRocCst.LIAROC_NB_BATCH_CHANGE_TYPE_05)));
            
            return convertToVOList(liaRocNbBatchList);
        } catch (Exception e) {
            throw ExceptionFactory.parse(e);
        }
    }
	
	   /**
     * 新式公會通報(V2020)
     * 取得有同樣的policyId + certieCode+changeType=02(ID/生日變更)+changeItemId + checkStatus2020 = 'N'(未處理) 新式公會批次未通報資料
     */
	private List<LiaRocNbBatchVO> getLastLiaRoc2020NbBatchData02(Long policyId, Long changeItemId,String oldCertiCode,String newCertiCode) {

        try {
            if (policyId == null || StringUtils.isNullOrEmpty(oldCertiCode) || StringUtils.isNullOrEmpty(newCertiCode)) {
                throw new NullPointerException();
            }
            Map parameters = new HashMap();
            parameters.put("policyId", policyId);
            parameters.put("changeType", LiaRocCst.LIAROC_NB_BATCH_CHANGE_TYPE_02);
            parameters.put("changeItemId",changeItemId);
            parameters.put("certiCodeBf", newCertiCode);
            parameters.put("certiCodeAf", oldCertiCode);
            parameters.put("checkStatus2020",LiaRocCst.LIAROC_NB_BATCH_CHECK_STATUS_NO);
            
            //判斷未處理的ID變更是否有A ->B, B -> A
            List<LiaRocNbBatch> liaRocNbBatchList = (List<LiaRocNbBatch>) this.dao.findByCriteria(
            		Restrictions.allEq(parameters));
            
            return convertToVOList(liaRocNbBatchList);
        } catch (Exception e) {
            throw ExceptionFactory.parse(e);
        }
    }

	
	@Override
	public void saveInsuredChange(Long policyId, String changeType, Long changeItemId,String newCertiCode, String oldCertiCode,
			Date newBirthDate, Date oldBirthDate) {

		CoverageVO coverageVO = coverageService.load(changeItemId);
		//先取得liaRocNbBatchVO裡有無相同未處理過的同一保單有做ID變更的記錄
		List<LiaRocNbBatchVO> lastNbBatchList  = null;
		//執行日大於新式公會通報生效日期(2021/07/01)，要執行新式公會更正通報
		if (liaRocUploadCommonService.isLiaRocV2020Process()){		
			 lastNbBatchList  = this.getLastLiaRoc2020NbBatchData02(policyId,changeItemId, oldCertiCode, newCertiCode);
		}else{	
			 lastNbBatchList  = this.getLastLiaRocNbBatchData02(policyId,changeItemId, oldCertiCode, newCertiCode);
		}
		if (lastNbBatchList.size() == 0  ||  lastNbBatchList == null) {
			//當日未有ID變更記錄，新增一筆
			LiaRocNbBatchVO liaRocNbBatchVO = new LiaRocNbBatchVO();
			liaRocNbBatchVO.setPolicyId(policyId);
			liaRocNbBatchVO.setChangeType(LiaRocCst.LIAROC_NB_BATCH_CHANGE_TYPE_02);
			liaRocNbBatchVO.setChangeItemId(changeItemId);
			liaRocNbBatchVO.setCertiCodeBf(oldCertiCode);
			liaRocNbBatchVO.setCertiCodeAf(newCertiCode);
			liaRocNbBatchVO.setBirthDateBf(oldBirthDate);
			liaRocNbBatchVO.setBirthDateAf(newBirthDate);
			this.save(saveCommonData(liaRocNbBatchVO,false,coverageVO));
		}else{
			//同一保單在同一批未處理裡已做過ID變更，要將已存在的資料更新成最新的
			LiaRocNbBatchVO liaRocNbBatchVO = lastNbBatchList.get(0);
			//ID變更 A ->B, B ->A，最後記錄A ->A，等定時起批做公會通報再排除這種重覆通報　2020/03/22 Add by Kathy
			liaRocNbBatchVO.setCertiCodeAf(newCertiCode);
			this.save(saveCommonData(liaRocNbBatchVO,false,coverageVO));
		}
	}

	@Override                     
	public void saveCoverageChange(Long policyId, Long itemId, Integer newProductId, Integer oldProductId,
									String changeType, boolean isApplyDate){
		//先取得liaRocNbBatchVO裡有無相同未處理過的policyId + certiCode + internalId 的異動資料(排除change_type = 01(收件)和02(ID/生日異動))
		//沒有就新增一筆，有讀到就更新成最新異動狀態
		String oldInternalId = liaRocUploadDetailDao.findInternalIdByProductId(oldProductId);
		String newInternalId = liaRocUploadDetailDao.findInternalIdByProductId(newProductId);
		String certiCode     = liaRocNbBatchDao.queryCertiCodeByCoverageItemId(policyId, itemId);
		CoverageVO coverageVO = coverageService.load(itemId);
		List<LiaRocNbBatchVO> lastNbBatchList  = null;
		//執行日大於新式公會通報生效日期(2021/07/01)，要執行新式公會更正通報
		if (liaRocUploadCommonService.isLiaRocV2020Process()) {
			lastNbBatchList = this.getLastLiaRoc2020NbBatchData(policyId, certiCode, oldInternalId, itemId);
		} else {
			lastNbBatchList = this.getLastLiaRocNbBatchData(policyId, certiCode, oldInternalId, itemId);
		}
		if (lastNbBatchList.size() == 0  ||  lastNbBatchList == null) {
			//當日未有保項異動記錄，新增一筆
			LiaRocNbBatchVO liaRocNbBatchVO = new LiaRocNbBatchVO();
			liaRocNbBatchVO.setPolicyId(policyId);
			liaRocNbBatchVO.setChangeType(changeType);
			liaRocNbBatchVO.setChangeItemId(itemId);
			if (!StringUtils.isNullOrEmpty(certiCode)) {
				liaRocNbBatchVO.setCertiCodeBf(certiCode);
			}
			liaRocNbBatchVO.setInternalIdBf(oldInternalId);
			liaRocNbBatchVO.setInternalIdAf(newInternalId);

			this.save(saveCommonData(liaRocNbBatchVO,isApplyDate,coverageVO));
		}else{
			//當日已存在保項異動記錄時更新變更類別
			LiaRocNbBatchVO liaRocNbBatchVO = lastNbBatchList.get(0);
			liaRocNbBatchVO.setChangeItemId(itemId);
			liaRocNbBatchVO.setChangeType(changeType);
			if (LiaRocCst.LIAROC_NB_BATCH_CHANGE_TYPE_05.equals(changeType)) {
				// 險種變更 A ->B, B ->A，最後記錄A ->A，等定時起批做公會通報再排除這種重覆通報 2020/03/22
				// Add by Kathy
				if (oldInternalId.equals(liaRocNbBatchVO.getInternalIdAf())) {
					liaRocNbBatchVO.setInternalIdAf(newInternalId);
				}
			} else {
				liaRocNbBatchVO.setInternalIdAf(null);
			}

			this.save(saveCommonData(liaRocNbBatchVO, isApplyDate,coverageVO));
		}
	}

	@Override
	public void saveCoverageInsDel(Long policyId, Long itemId, String changeType, boolean isApplyDate) {
		//先取得liaRocNbBatchVO裡有無相同未處理過的policyId + certiCode + internalId 的異動資料(排除change_type = 01(收件)和02(ID/生日異動))
		//沒有就新增一筆，有讀到就更新成最新異動狀態
		String internalId = liaRocUploadDetailDao.findInternalIdByItemId(itemId); 
		String certiCode  = liaRocNbBatchDao.queryCertiCodeByCoverageItemId(policyId, itemId);
		CoverageVO coverageVO = coverageService.load(itemId);
		List<LiaRocNbBatchVO> lastNbBatchList  = this.getLastLiaRocNbBatchData(policyId, certiCode, internalId,itemId);
		if (lastNbBatchList.size() == 0  ||  lastNbBatchList == null) {
			//當日未有保項異動記錄，新增一筆
			LiaRocNbBatchVO liaRocNbBatchVO = new LiaRocNbBatchVO();
			liaRocNbBatchVO.setPolicyId(policyId);
			liaRocNbBatchVO.setChangeType(changeType);
			liaRocNbBatchVO.setChangeItemId(itemId);
			if (!StringUtils.isNullOrEmpty(certiCode)) {
				liaRocNbBatchVO.setCertiCodeBf(certiCode);
			}
			liaRocNbBatchVO.setInternalIdBf(internalId);
			liaRocNbBatchVO.setInternalIdAf(null);
			
			this.save(saveCommonData(liaRocNbBatchVO,isApplyDate,coverageVO));
		}else{
			//當日已存在保項異動記錄時更新變更類別
			LiaRocNbBatchVO liaRocNbBatchVO = lastNbBatchList.get(0);
			liaRocNbBatchVO.setChangeItemId(itemId);

			if (LiaRocCst.LIAROC_NB_BATCH_CHANGE_TYPE_04.equals(changeType)){
				if (LiaRocCst.LIAROC_NB_BATCH_CHANGE_TYPE_05.equals(liaRocNbBatchVO.getChangeType())) {
					liaRocNbBatchVO.setInternalIdBf(liaRocNbBatchVO.getInternalIdBf());
					liaRocNbBatchVO.setInternalIdAf(null);
				}
			}else{
				liaRocNbBatchVO.setInternalIdAf(null);
			}
			liaRocNbBatchVO.setChangeType(changeType);

			this.save(saveCommonData(liaRocNbBatchVO,isApplyDate,coverageVO));

		}
	}

	@Override
	public void saveAmountChange(Long policyId, Long itemId, String changeType, boolean isApplyDate, String AmountBf, String AmountAf) {
		//先取得liaRocNbBatchVO裡有無相同未處理過的policyId + certiCode + itemId 的異動資料(排除change_type = 01(收件)和02(ID/生日異動))
		//沒有就新增一筆，有讀到就更新成最新異動狀態
		String certiCode  = liaRocNbBatchDao.queryCertiCodeByCoverageItemId(policyId, itemId);
		CoverageVO coverageVO = coverageService.load(itemId);
		
		//當日有保額異動記錄，新增一筆
		LiaRocNbBatchVO liaRocNbBatchVO = new LiaRocNbBatchVO();
		liaRocNbBatchVO.setPolicyId(policyId);
		liaRocNbBatchVO.setChangeType(changeType);
		liaRocNbBatchVO.setChangeItemId(itemId);
		if (!StringUtils.isNullOrEmpty(certiCode)) {
			liaRocNbBatchVO.setCertiCodeBf(certiCode);
		}
		//存保額/單位/計劃別
		liaRocNbBatchVO.setInternalIdBf(AmountBf); 
		liaRocNbBatchVO.setInternalIdAf(AmountAf);
		
		this.save(saveCommonData(liaRocNbBatchVO,isApplyDate, coverageVO));

	}

	@Override
	protected LiaRocNbBatchVO newEntityVO() {
		// TODO Auto-generated method stub
		return new LiaRocNbBatchVO();
	}

	private LiaRocNbBatchVO saveCommonData(LiaRocNbBatchVO vo, boolean isApplyDate, CoverageVO coverageVO){
    	PolicyVO policyVO = policyDS.retrieveById(vo.getPolicyId(), false);
    	vo.setCheckStatus(LiaRocCst.LIAROC_NB_BATCH_CHECK_STATUS_NO);
    	vo.setCheckStatus2020(LiaRocCst.LIAROC_NB_BATCH_CHECK_STATUS_NO);
    	vo.setSubmitChannel(policyVO.getSubmitChannel());
    	vo.setChannelType(policyVO.getChannelType());
    	vo.setApplyDate(policyVO.getApplyDate());
		//實際T_CONTRACT_PRODUCT.CHARGE_YEAR(繳費年期) for 比對規則使用
		if (coverageVO != null) {
			if (coverageVO.getChargeYear() != null) {
				vo.setChargeYear(coverageVO.getChargeYear());
			}
			if (coverageVO.getProposalTerm() != null) {
				vo.setProposalTerm(coverageVO.getProposalTerm());
			}
		}

    	/* 保單狀況生效日(通報公會)規則
    	 * isApplyDate = true  then 以原通報規則填入，SystemDate = null，待夜間批次呼叫liaRocUploadCI.generateUploadDetail下面規則處理
    	 *   (1)內部通路：以「要保書填寫日」通報。;
    	 *   (2)外部通路：保經代傳真件(97)及健康/醫療險(98)以「受理/收件確認日期」通報；含死亡給付險種(99)以「要保書填寫日」;
    	 * 	  (3)微型保單 收件通報以通路受理日通報;
    	 * isApplyDate = false then liaRocNbBatchVO.SystemDate = LocalTime(作業日)
    	 */
    	if (isApplyDate) {
    		vo.setSystemDate(null);
    	}else{
    		vo.setSystemDate(DateUtils.truncateDay(AppContext.getCurrentUserLocalTime()));
    	}

		return vo;
	}
}


