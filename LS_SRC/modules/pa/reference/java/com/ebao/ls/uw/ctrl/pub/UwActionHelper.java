package com.ebao.ls.uw.ctrl.pub;

import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.transaction.UserTransaction;

import org.apache.commons.beanutils.BeanPredicate;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.PredicateUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.log4j.Logger;

import com.ebao.foundation.common.context.AppUserContext;
import com.ebao.foundation.module.db.DBean;
import com.ebao.foundation.module.util.i18n.CodeTable;
import com.ebao.foundation.module.web.action.ExceptionInfoUtils;
import com.ebao.ls.cs.commonflow.data.query.JobCategoryDao;
import com.ebao.ls.cs.commonflow.ds.AlterationItemService;
import com.ebao.ls.cs.commonflow.ds.product.ProductConfigService;
import com.ebao.ls.cs.commonflow.vo.AlterationItemVO;
import com.ebao.ls.cs.commonflow.vo.JobCategoryVO;
import com.ebao.ls.notification.extension.letter.nb.FmtUnb0381CoverageVO;
import com.ebao.ls.notification.extension.letter.nb.FmtUnb0381ExtensionVO;
import com.ebao.ls.notification.extension.letter.nb.FmtUnb0441ExtensionVO;
import com.ebao.ls.notification.extension.letter.nb.FmtUnb0441InvBasedPremium;
import com.ebao.ls.notification.extension.letter.nb.FmtUnb0441InvCustomerPremMain;
import com.ebao.ls.notification.extension.letter.nb.FmtUnb0441InvExtra;
import com.ebao.ls.notification.extension.letter.nb.FmtUnb0441InvMain;
import com.ebao.ls.notification.extension.letter.nb.FmtUnb0441InvRider;
import com.ebao.ls.notification.extension.letter.nb.FmtUnb0441InvTargetDueMain;
import com.ebao.ls.notification.extension.letter.nb.FmtUnb0441PremTotal;
import com.ebao.ls.notification.extension.letter.nb.FmtUnb0441TradItem;
import com.ebao.ls.notification.extension.letter.nb.index.FmtUnb0381IndexVO;
import com.ebao.ls.notification.extension.letter.nb.index.FmtUnb0441IndexVO;
import com.ebao.ls.notification.message.WSItemString;
import com.ebao.ls.notification.message.WSLetterUnit;
import com.ebao.ls.pa.nb.ctrl.letter.NbLetterHelper;
import com.ebao.ls.pa.nb.ctrl.letter.NbNotificationHelper;
import com.ebao.ls.pa.nb.rule.impl.InsuredJobCateIdValidator;
import com.ebao.ls.pa.nb.util.NBUtils;
import com.ebao.ls.pa.nb.util.NumericUtils;
import com.ebao.ls.pa.pub.MsgCst;
import com.ebao.ls.pa.pub.TableCst;
import com.ebao.ls.pa.pub.bs.ExtraPremService;
import com.ebao.ls.pa.pub.bs.InsuredService;
import com.ebao.ls.pa.pub.bs.PolicyHolderService;
import com.ebao.ls.pa.pub.bs.PolicyService;
import com.ebao.ls.pa.pub.bs.coverage.CoverageService;
import com.ebao.ls.pa.pub.bs.impl.sp.CoverageSP;
import com.ebao.ls.pa.pub.ci.CalculatorCI;
import com.ebao.ls.pa.pub.data.bo.ExtraPrem;
import com.ebao.ls.pa.pub.data.org.ExtraPremDao;
import com.ebao.ls.pa.pub.ref.api.prd.ProductService;
import com.ebao.ls.pa.pub.vo.CoverageInsuredVO;
import com.ebao.ls.pa.pub.vo.CoveragePremium;
import com.ebao.ls.pa.pub.vo.CoverageVO;
import com.ebao.ls.pa.pub.vo.InsuredVO;
import com.ebao.ls.pa.pub.vo.PolicyVO;
import com.ebao.ls.pa.pub.vo.UwMedicalLetterVO;
import com.ebao.ls.pa.pub.vo.UwSickFormLetterVO;
import com.ebao.ls.prd.product.pub.data.TProdVerEndorsementDelegate;
import com.ebao.ls.prd.product.vo.ProdVerEndorsementVO;
import com.ebao.ls.prd.product.vo.ProductVO;
import com.ebao.ls.product.model.query.output.LifeProduct;
import com.ebao.ls.product.service.LifeProductCategoryService;
import com.ebao.ls.product.service.LifeProductService;
import com.ebao.ls.pub.CodeCst;
import com.ebao.ls.pub.RatetableCst;
import com.ebao.ls.pub.cst.Language;
import com.ebao.ls.pub.cst.Template;
import com.ebao.ls.pub.ratetable.RatetableService;
import com.ebao.ls.uw.ctrl.decision.UwDecisionSaveAction;
import com.ebao.ls.uw.ds.UwIssuesLetterService;
import com.ebao.ls.uw.ds.UwMedicalLetterService;
import com.ebao.ls.uw.ds.UwPolicyService;
import com.ebao.ls.uw.ds.UwRiApplyService;
import com.ebao.ls.uw.ds.UwSickFormLetterService;
import com.ebao.ls.uw.ds.sp.QueryUwDataSp;
import com.ebao.ls.uw.vo.UwEndorsementVO;
import com.ebao.ls.uw.vo.UwExclusionVO;
import com.ebao.ls.uw.vo.UwExtraLoadingVO;
import com.ebao.ls.uw.vo.UwProductVO;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.framework.ExceptionFactory;
import com.ebao.pub.framework.GenericException;
import com.ebao.pub.i18n.lang.StringResource;
import com.ebao.pub.util.Log;
import com.ebao.pub.util.StringUtils;
import com.ebao.pub.util.Trans;
import com.ebao.pub.util.TransUtils;

/**
 * <p>
 * Title: 核保作業批註、加費共用方法
 * </p>
 * <p>
 * Description: 核保作業批註、加費共用方法
 * </p>
 * <p>
 * Copyright: Copyright (c) 2015
 * </p>
 * <p>
 * Company: TGL Co., Ltd.
 * </p>
 * <p>
 * Create Time: Jul 15, 2015
 * </p>
 * 
 * @author <p>
 *         Update Time: Jul 15, 2015
 *         </p>
 *         <p>
 *         Updater: simon.huang
 *         </p>
 *         <p>
 *         Update Comments:
 *         </p>
 */
public class UwActionHelper {
	private Logger logger = Logger.getLogger(UwActionHelper.class);

	public static final String BEAN_DEFAULT = "uwActionHelper";
	
	//BC353 PCR-263273 新增目標到期債指標 2018/11/06 Add by Kathy

	public boolean isFound17 = false;

	@Resource(name = UwPolicyService.BEAN_DEFAULT)
	protected UwPolicyService uwPolicyService;

	@Resource(name = PolicyHolderService.BEAN_DEFAULT)
	protected PolicyHolderService policyHolderService;

	@Resource(name = PolicyService.BEAN_DEFAULT)
	protected PolicyService policyService;

	@Resource(name = com.ebao.ls.pa.pub.service.PolicyService.BEAN_DEFAULT)
	protected com.ebao.ls.pa.pub.service.PolicyService policyDS;

	@Resource(name = JobCategoryDao.BEAN_DEFAULT)
	private JobCategoryDao jobCategoryDao;

	@Resource(name = ExtraPremService.BEAN_DEFAULT)
	private ExtraPremService extraPremService;

	@Resource(name = LifeProductService.BEAN_DEFAULT)
	private LifeProductService lifeProductService;

	@Resource(name = NbLetterHelper.BEAN_DEFAULT)
	private NbLetterHelper nbLetterHelper;

	@Resource(name = CoverageService.BEAN_DEFAULT)
	private CoverageService coverageService;

	@Resource(name = NbNotificationHelper.BEAN_DEFAULT)
	private NbNotificationHelper nbNotificationHelper;

	@Resource(name = CalculatorCI.BEAN_DEFAULT)
	private CalculatorCI calculatorCI;

	@Resource(name = UwMedicalLetterService.BEAN_DEFAULT)
	private UwMedicalLetterService uwMedicalLetterService;

	@Resource(name = InsuredService.BEAN_DEFAULT)
	protected InsuredService insuredService;

	@Resource(name = UwSickFormLetterService.BEAN_DEFAULT)
	private UwSickFormLetterService uwSickFormLetterService;

	@Resource(name = UwRiApplyService.BEAN_DEFAULT)
	protected UwRiApplyService uwRiApplyService;

	@Resource(name = ProductService.BEAN_DEFAULT)
	protected ProductService productService;

	@Resource(name = UwIssuesLetterService.BEAN_DEFAULT)
	private UwIssuesLetterService uwIssuesLetterService;

	@Resource(name = AlterationItemService.BEAN_DEFAULT)
	private AlterationItemService alterationItemService;

	@Resource(name = RatetableService.BEAN_DEFAULT)
	private RatetableService ratetableService;
	
	@Resource(name = ExtraPremDao.BEAN_DEFAULT)
	private ExtraPremDao<ExtraPrem> extraPremDao;
	
	@Resource(name = LifeProductCategoryService.BEAN_DEFAULT)
	private LifeProductCategoryService lifeProductCategoryService;
	//
	@Resource(name = ProductConfigService.BEAN_DEFAULT)
	private ProductConfigService productConfigService;

	/**
     * <p>
     * Description : 初始「除外責任批註書」WSLetterUnit
     * </p>
     * <p>
     * Created By : simon.huang
     * </p>
     * <p>
     * Create Time : Aug 21, 2016
     * </p>
     * <p>
     * Modified By : Vince.Cheng
     * </p>
     * <p>
     * Modified Time : Mar 19, 2021
     * </p>
     * 
	 * @param policyId
	 * @return WSLetterUnit <除外責任批註書>
	 * @throws Exception
	 */
    public WSLetterUnit getFmtUnb0381Letter(Long policyId, Long underwriteId, Long insuredId) throws Exception {

		Long templateId = Template.TEMPLATE_20023.getTemplateId();

		FmtUnb0381ExtensionVO extensionVO = getFmtUnb0381ExtensionVO(policyId, insuredId);

		PolicyVO policyVO = policyService.load(policyId);
		List<InsuredVO> listInsured = policyVO.getInsureds();
		Long partyId = 0L;
		for (InsuredVO insuredVO : listInsured) {
			if (insuredId.equals(insuredVO.getListId())) {
				partyId = insuredVO.getPartyId();

			}
		}
		//String policyCode = extensionVO.getPolicyCode();
		//String[] exclusionCode = uwProductExclusionForm.getExclusionCode();
		List<WSItemString> documentContents = new ArrayList<WSItemString>();
		Map<String, Object> in = new HashMap<String, Object>();
		in.put("policyId", policyId);
		in.put("underwriteId", underwriteId);

		List<UwExclusionVO> uwExclusionList = new ArrayList<UwExclusionVO>();//保單險種批註設定

		List<UwEndorsementVO> uwEndorsementList = new ArrayList<UwEndorsementVO>();//保單險種條款設定
		uwExclusionList.addAll(uwPolicyService.findUwExclusionEntitis(underwriteId));

		List<Map<String, Object>> coverageList = getDisplayCoverageList(policyVO, in);
		uwEndorsementList.addAll(uwPolicyService.findUwEndorsementEntitis(underwriteId));

		Map<String, List<UwExclusionVO>> uwExclusionMapList = NBUtils.toMapList(uwExclusionList, "uwExclusionGroupId");
		Map<String, List<UwEndorsementVO>> uwEndorMapList = NBUtils.toMapList(uwEndorsementList, "uwExclusionGroupId");

		//unicode->中文 
		String dot = String.valueOf('\u3001'); //、
		String s1 = String.valueOf('\u4e00'); //一
		String s2 = String.valueOf('\u4e8c'); //二
		String s3 = String.valueOf('\u4e09'); //三
		String s4 = String.valueOf('\u56db'); //四
		String s5 = String.valueOf('\u4e94'); //五
		String s6 = String.valueOf('\u516d'); //六
		String s7 = String.valueOf('\u4e03'); //七
		String s8 = String.valueOf('\u516b'); //八
		String s9 = String.valueOf('\u4e5d'); //九
		String s10 = String.valueOf('\u5341'); //十

		String[] rowIndex = { s1, s2, s3, s4, s5, s6, s7, s8, s9, s10, s10 + s1, s10 + s2, s10 + s3, s10 + s4, s10 + s5, s10 + s6, s10 + s7, s10 + s8, s10 + s9 };
		int indexNum = 0;
		for (String uwExclusionGroupId : uwExclusionMapList.keySet()) {
			List<UwExclusionVO> exclusionList = uwExclusionMapList.get(uwExclusionGroupId);
			List<UwEndorsementVO> endorList = uwEndorMapList.get(uwExclusionGroupId);

			UwExclusionVO exclusionVO = exclusionList.get(0);

			if (partyId.equals(exclusionVO.getInsuredId())) {

				String exclusionCode = exclusionVO.getExclusionCode();

				if ("S".equals(exclusionCode.substring(0, 1))) {
					WSItemString wsString = new WSItemString(null, exclusionVO.getDescLang1());
					documentContents.add(wsString);
				} else {
					WSItemString wsString = new WSItemString(rowIndex[indexNum] + dot, exclusionVO.getDescLang1());
					documentContents.add(wsString);
					indexNum++;
				}

				List<WSItemString> subContent = new ArrayList<WSItemString>();
				List<FmtUnb0381CoverageVO> coverageList0381 = new ArrayList<FmtUnb0381CoverageVO>();
				for (Map<String, Object> coverage : coverageList) {
					for (UwExclusionVO tmp : exclusionList) {
						String itemId = String.valueOf(tmp.getItemId());
						if (itemId.equals(String.valueOf(coverage.get("itemId")))) {//險種
							String plan = MapUtils.getString(coverage, "productName") + " (" + MapUtils.getString(coverage, "planCode") + ") ";
							String waiver = MapUtils.getString(coverage, "waiver");
							FmtUnb0381CoverageVO coverage0381 = new FmtUnb0381CoverageVO();
							coverage0381.setPlan(plan);
							
							if (CodeCst.YES_NO__YES.equals(waiver)) {
								//豁免險不重覆帶入
								if (!this.coverageList0381ContainsPlan(coverageList0381, plan)) {
									coverageList0381.add(coverage0381);
								}
							} else {
								//PCR_426401 BC416 3.6.1 FMT_UNB_0381 除外責任批註書  新增 繳費年期、給付期間(年)；非豁免險才顯示
								String chargePeriod = MapUtils.getString(coverage, "chargePeriod");
								coverage0381.setChargePeriod(StringUtils.emptyToNull(chargePeriod));
								
								String proposalTerm = MapUtils.getString(coverage, "proposalTerm");
								coverage0381.setProposalTerm(StringUtils.emptyToNull(proposalTerm));
								
								coverageList0381.add(coverage0381);
							}

							if (endorList != null) {//批註
								for (UwEndorsementVO endorsement : endorList) {
									if (itemId.equals(String.valueOf(endorsement.getItemId()))) {
										plan = endorsement.getDescLang1() + " (" + endorsement.getEndoCode() + ") ";
										
										if (!this.coverageList0381ContainsPlan(coverageList0381, plan)) {//批註不重覆帶入
											coverage0381 = new FmtUnb0381CoverageVO();
											coverage0381.setPlan(plan);
											
											coverageList0381.add(coverage0381);
										}
									}
								}
							}
						}
					}
				}
				
				if (org.apache.commons.collections4.CollectionUtils.isNotEmpty(coverageList0381)) {
					WSItemString contentVo = new WSItemString();
					contentVo.setCoverageList(coverageList0381);
					subContent.add(contentVo);
				}

				documentContents.addAll(subContent);
				//documentContents.add(new WSItemString(""));//IR_437456 調整行距

			}
		}
		extensionVO.setContents(documentContents);

		FmtUnb0381IndexVO index = new FmtUnb0381IndexVO();
		nbNotificationHelper.initIndexVO(index, extensionVO, templateId);

		WSLetterUnit unit = new WSLetterUnit(extensionVO, index);
		//通路寄送規則,採Email寄送
		NbLetterHelper.unbSendingMethod(unit);
		
		return unit;
	}
    
	private boolean coverageList0381ContainsPlan(List<FmtUnb0381CoverageVO> coverageList0381, String plan) {
		if (CollectionUtils.isNotEmpty(coverageList0381) && org.apache.commons.lang3.StringUtils.isNotBlank(plan)) {
			for (FmtUnb0381CoverageVO coverage0381 : coverageList0381) {
				if (plan.equals(coverage0381.getPlan())) {
					return true;
				}
			}
		}
		
		return false;
	}

	public String getAmountUnitBenefitLevel(CoverageVO coverageVO) {

		String value = "";

		CoveragePremium premium = coverageVO.getCurrentPremium();

		//String yun = NBUtils.getTWMsg("MSG_216294"); // 元
		String danway = NBUtils.getTWMsg("MSG_113300"); // 單位
		String project = NBUtils.getTWMsg("MSG_NB_000132"); // 計劃
		
		if (coverageVO.isWaiver() == false) {
			BigDecimal amount = premium.getSumAssured();
			BigDecimal unit = premium.getUnit();
			String benefitLevel = premium.getBenefitLevel();
			if (amount != null && amount.compareTo(BigDecimal.ZERO) > 0) {
				value = amount.toString() ; //+ yun;
			} else if (unit != null && unit.compareTo(BigDecimal.ZERO) > 0) {
				value = unit.toString() + danway;
			} else if (benefitLevel != null && benefitLevel.equals("0") == false) {
				if("B".equals(benefitLevel)){
					//基本計劃
		    value = CodeTable.getCodeDesc("T_BENEFIT_LEVEL", benefitLevel, Language.LANGUAGE__CHINESE_TRAD);
				} else {
					value = project + benefitLevel;
				}
			}
			
		} else {
			//PCR 156268調整豁免險保額呈現空白，F18需顯示保額
			LifeProduct product = lifeProductService.getProductByVersionId(coverageVO.getProductVersionId());
			if ("F18".equals(product.getProduct().getInternalId())) {
				BigDecimal amount = premium.getSumAssured();
				if (amount != null && amount.compareTo(BigDecimal.ZERO) > 0) {
					value = amount.toString();
				}
			}
		}

		return value;
	}

	/**
     * <p>
     * Description : 初始「新契約內容變更同意書」WSLetterUnit
     * </p>
     * <p>
     * Created By : simon.huang
     * </p>
     * <p>
     * Create Time : Aug 21, 2016
     * </p>
     * 
	 * @param policyId
	 * @return WSLetterUnit <新契約內容變更同意書>
	 */
	public WSLetterUnit getFmtUnb0441Letter(Long policyId, Long underwriteId) {

		Long templateId = Template.TEMPLATE_20024.getTemplateId();

		PolicyVO policyVO = policyService.load(policyId);
		CoverageVO masterCoverageVO = policyVO.gitMasterCoverage();//主約
		Long itemId = masterCoverageVO.getItemId();
		Long productId = Long.valueOf(masterCoverageVO.getProductId());
		Long productVersionId = masterCoverageVO.getProductVersionId();

		Collection<UwExtraLoadingVO> allExtra = uwPolicyService.findUwExtraLoadingEntitis(underwriteId);

		//備註1.7/1.11/1.27/1.28
		FmtUnb0441ExtensionVO extensionVO = getFmtUnb0441ExtensionVO(policyVO, allExtra);

		/* 判斷是否有COI的收費規則，決定是否為投資型 */
		if (coverageService.isCOIProduct(productId.longValue(), productVersionId)) {
			//初始投資型主約(弱加/職加) 1.8~1.13
			this.initInvMainProductInfo(extensionVO, policyVO, allExtra);
			//初始投資型附約(弱加/職加) 1.16
			this.initInvRiderProductInfo(extensionVO, policyVO, allExtra);
			//初始投資型保險費合計 1.18
			//BC353 PCR-263273 投資型目標到期債不顯示應繳保險費合計欄位 2018/11/06 Add by Kathy
			//BC405 投資型商品且彈性繳不顯示應繳保險費合計欄位欄位
			Boolean checkPaymentFreq = true;
			String paymentFreq = masterCoverageVO.getNextPremium().getPaymentFreq();
			
			// 繳別為躉繳 且 可單次追加投資 代表彈性繳
			if (CodeCst.CHARGE_MODE__SINGLE.equals(paymentFreq)) {
				LifeProduct lifeProduct = lifeProductService.getProductByVersionId(productVersionId);
				
				if(CodeCst.YES_NO__YES.equals(lifeProduct.getProduct().getTopupPermit())){
					checkPaymentFreq=false;
				}
			}
			
			if (isFound17 == false && checkPaymentFreq) {
				this.initInvTotalPremInfo(extensionVO, policyVO, allExtra);
			}
			//初始投資型備註 1.19/1.20/1.21/1.26/1.32
			this.initInvNoticeInfo(extensionVO, policyVO, allExtra);
	    // BC374 加費單中 signatureStyle=2(投資型) 才顯示特定的簽名欄區塊 2019/07/05 added by
	    // Jyun-yu
			extensionVO.setSignatureStyle("2");

		} else {
			//初始傳統型主約(弱加/職加) 1.14
			//PCR 156288-整合主約與附約一同顯示,註解主約的處理
			//this.initTradMainProductInfo(extensionVO, policyVO, allExtra);
			//初始傳統型附約(弱加/職加) 1.15
			this.initTradRiderProductInfo(extensionVO, policyVO, allExtra);
			//初始傳統型保險費合計 1.17
			this.initTradTotalPremInfo(extensionVO, policyVO, allExtra);
			//初始傳統型備註 1.22/1.25
			this.initTradNoticeInfo(extensionVO, policyVO, allExtra);
	    // BC374 加費單中 signatureStyle=1(傳統型) 才顯示特定的簽名欄區塊 2019/07/05 added by
	    // Jyun-yu
			extensionVO.setSignatureStyle("1");
		}

		List<String> discntDesc = new ArrayList<String>();
		//PCR 156288-新增折讓保險費備註，調整先執行1.24, 再執行1.23的判斷
		
		//1.24    繳費折讓折讓率 payDiscnt   
		if ((findDiscntRate(itemId, "3")).compareTo(BigDecimal.ZERO) > 0) {
			//mark by RTC-225798(2018/03/06 simon)
	    // extensionVO.setPayDiscnt(this.decimalToString(findDiscntRate(itemId,
	    // "3")));
			discntDesc.add(NBUtils.getTWMsg("MSG_1262612")); //繳費方式之保費折讓
		}
		//1.23    集體彙繳保費折讓率   togetherPayDiscnt
		if ((findDiscntRate(itemId, "2")).compareTo(BigDecimal.ZERO) > 0) {
			//mark by RTC-225798(2018/03/06 simon)
	    // extensionVO.setTogetherPayDiscnt(this.decimalToString(findDiscntRate(itemId,
	    // "2")));
			discntDesc.add(NBUtils.getTWMsg("MSG_1262634")); //集體彙繳之保費折讓
		}
		
		//PCR 156288-新增折讓保險費備註
		//1.33/1.34
		if(discntDesc.size() > 0){
			extensionVO.setNoticeDiscntPrem(CodeCst.YES_NO__YES);
	    extensionVO.setNoticeDiscntItem(org.apache.commons.lang3.StringUtils.join(discntDesc, "/"));
		}

		FmtUnb0441IndexVO index = new FmtUnb0441IndexVO();
		nbNotificationHelper.initIndexVO(index, extensionVO, templateId);

		WSLetterUnit unit = new WSLetterUnit(extensionVO, index);
		//通路寄送規則,採Email寄送
		NbLetterHelper.unbSendingMethod(unit);
		
		return unit;
	}

    private void initInvMainProductInfo(FmtUnb0441ExtensionVO extensionVO, PolicyVO policyVO, Collection<UwExtraLoadingVO> allExtra) {

		CoverageVO masterCoverageVO = policyVO.gitMasterCoverage();//主約
		Long itemId = masterCoverageVO.getItemId();
		Long productId = Long.valueOf(masterCoverageVO.getProductId());
		Long productVersionId = masterCoverageVO.getProductVersionId();

		LifeProduct lifeProduct = lifeProductService.getProductByVersionId(productVersionId);
		ProductVO productVO = lifeProduct.getProduct();
		String isCustomizedPrem = productVO.getCustomizedPremiumIndi(); //自訂保險費
		//判斷 1.8(投資型主約弱體加費) or 1.9( 投資型主約職業加費) or 1.10(投資型主約弱體加費及職業加費)
		/* 1.8 投資型主約弱體加費*/
		FmtUnb0441InvExtra invHealthExtra;
		/* 1.9 投資型主約職業加費*/
		FmtUnb0441InvExtra invOccupExtra;
		/* 1.10 投資型主約弱加+職加*/
		FmtUnb0441InvExtra invExtra;
		/* 1.12 投資型主約*/  
		FmtUnb0441InvMain invMain;
		/* 1.13 投資型自訂保險費主約*/
		FmtUnb0441InvCustomerPremMain invCustomerPremMain;
		/* 1.38 投資型目標到期債主約 */
		//BC353 PCR-263273 2018/10/11 add By Kathy
		FmtUnb0441InvTargetDueMain invTargetDueMain;
		
	/*
	 * <註:標準保費是指高保額折讓後的保費，應收保險費是指其他折讓後的保費> coiMap.put("stdPremCOI",
	 * stdPremCOI); //標準保費/保險成本
		*/
		Map<String, BigDecimal> coiMap = CoverageSP.getExtraPremCOI(itemId);
		BigDecimal stdPremCOI = (BigDecimal) coiMap.get("stdPremCOI");
		if (stdPremCOI == null) {
			stdPremCOI = BigDecimal.ZERO;
		}
		//判斷 1.8(投資型主約弱體加費) or 1.9( 投資型主約職業加費) or 1.10(投資型主約弱體加費及職業加費)
		//過濾主約的加費資料
		@SuppressWarnings("unchecked")
	Collection<UwExtraLoadingVO> coverageExtraList = CollectionUtils.select(allExtra, new BeanPredicate("itemId", PredicateUtils.equalPredicate(itemId)));

		UwExtraLoadingVO healthExtra = null;
		UwExtraLoadingVO jobExtra = null;

		for (UwExtraLoadingVO extraLoadingVO : coverageExtraList) {
			if (CodeCst.EXTRA_TYPE__HEALTH_EXTRA.equals(extraLoadingVO.getExtraType())) {
				healthExtra = extraLoadingVO;
			} else if (CodeCst.EXTRA_TYPE__OCCUPATION_EXTRA.equals(extraLoadingVO.getExtraType())) {
				jobExtra = extraLoadingVO;
			}
		}
		//END-判斷 1.8(投資型主約弱體加費) or 1.9( 投資型主約職業加費) or 1.10(投資型主約弱體加費及職業加費)

		if (healthExtra != null && jobExtra == null) {
			//1.8 投資型主約弱體加費;
			invHealthExtra = new FmtUnb0441InvExtra();
			invHealthExtra.setRate(this.decimalToString(healthExtra.getExtraPara()));
			invHealthExtra.setPremBf(this.decimalToString(stdPremCOI));

			BigDecimal premAf = stdPremCOI;
			if (healthExtra.getExtraPrem() != null) {
				premAf = premAf.add(healthExtra.getExtraPrem());
			}

			if (premAf.compareTo(BigDecimal.ZERO) >= 0) {
				invHealthExtra.setPremAf(this.decimalToString(premAf));
			}
			extensionVO.setInvHealthExtra(invHealthExtra);

		} else if (healthExtra == null && jobExtra != null) {
			//1.9  投資型主約職業加費;
			invOccupExtra = new FmtUnb0441InvExtra();
			invOccupExtra.setExtraPrem(this.decimalToString(jobExtra.getExtraPrem()));
			invOccupExtra.setPremBf(this.decimalToString(stdPremCOI));

			BigDecimal premAf = stdPremCOI;
			if (jobExtra.getExtraPrem() != null) {
				premAf = premAf.add(jobExtra.getExtraPrem());
			}
			if (premAf.compareTo(BigDecimal.ZERO) >= 0) {
				invOccupExtra.setPremAf(this.decimalToString(premAf));
			}
			extensionVO.setInvOccupExtra(invOccupExtra);

		} else if (healthExtra != null && jobExtra != null) {
			// 1.10  投資型主約弱體加費及職業加費 ;   
			invExtra = new FmtUnb0441InvExtra();
			invExtra.setRate(this.decimalToString(healthExtra.getExtraPara()));

			BigDecimal extraPrem = BigDecimal.ZERO;
			BigDecimal premAf = stdPremCOI;
			if (jobExtra.getExtraPrem() != null) {
				premAf = premAf.add(jobExtra.getExtraPrem());
				extraPrem = extraPrem.add(jobExtra.getExtraPrem());
			}
			if (healthExtra.getExtraPrem() != null) {
				premAf = premAf.add(healthExtra.getExtraPrem());
				//RTC-224191-同時有職加及弱加，只顯示職加金額
				//extraPrem = extraPrem.add(healthExtra.getExtraPrem());
			}

			invExtra.setExtraPrem(this.decimalToString(extraPrem));
			invExtra.setPremBf(this.decimalToString(stdPremCOI));
			if (premAf.compareTo(BigDecimal.ZERO) >= 0) {
				invExtra.setPremAf(this.decimalToString(premAf));
			}
			extensionVO.setInvExtra(invExtra);
		}

		isFound17 = false;
		if ((CodeCst.YES_NO__NO).equalsIgnoreCase(isCustomizedPrem)) {
			//1.38 投資型目標到期債主約
			//BC353 PCR-263273 2018/10/09 add by Kathy

			/* 查詢此保單的基金代碼和基金類別 */
	        Map<String, String> tFundDataMap =  new HashMap<String, String>();
	        tFundDataMap = this.getTFundData(masterCoverageVO.getPolicyId());
	        String  tFundCode	 = null;
	        String  tFundType	 = null;

			if (tFundDataMap != null) {
				tFundCode  = tFundDataMap.get("fundCode");    
				tFundType  = tFundDataMap.get("fundType");
				if ("17".equals(tFundType)) {
					isFound17   = true;
				}
			}

			// tFundType = 17 代表保單連結投資標的分類=(目標到期基金)投資標的，則保險費欄位顯示自訂保費
	        if (isFound17) {
	        	invTargetDueMain = new FmtUnb0441InvTargetDueMain();
	        	invTargetDueMain.setPlanCode(productVO.getInternalId());
	        	invTargetDueMain.setAmount(this.getAmountUnitBenefitLevel(masterCoverageVO));
	        	invTargetDueMain.setInsuredName(extensionVO.getMainInsuredName());
				invTargetDueMain.setInsuredRomanName(extensionVO.getMainInsuredRomanName());
	        	BigDecimal targetDueCustomizedPrem = masterCoverageVO.getCustomizedPrem();
	        	invTargetDueMain.setCustomerPrem(this.decimalToString(targetDueCustomizedPrem));

	        	extensionVO.setInvTargetDueMain(invTargetDueMain);
	        }else{
	        	String specialTypeM = lifeProductCategoryService.getTypeProductSpecial(productId.intValue(),
						CodeCst.NB_PRODUCT_SPECIAL_TYPE__M);
	        	String unitFlag = productVO.getUnitFlag();
	        	
	        	invMain = new FmtUnb0441InvMain();
	        	invMain.setPlanCode(productVO.getInternalId());
			
	        	if(masterCoverageVO.getProductType() != null) {
		    String type = CodeTable.getCodeDesc(TableCst.T_PD_VERSION_TYPE_OPT, masterCoverageVO.getProductType(), CodeCst.LANGUAGE__CHINESE_TRAD);
	        		invMain.setType(type);
	        	}
	        
	        	invMain.setAmount(this.getAmountUnitBenefitLevel(masterCoverageVO));
	        	invMain.setInsuredName(extensionVO.getMainInsuredName());
				invMain.setInsuredRomanName(extensionVO.getMainInsuredRomanName());
	        	//參考保險費
	        	BigDecimal referencePrem = masterCoverageVO.getCurrentPremium().getStdPremAf();

	        	//標準保險費
	        	//<註:加費頁面上所稱「標準保費」是指未扣除高保額折讓之保費(期繳)> 
		        BigDecimal stdPrem = coverageService.getStdPremWithoutAnyDiscnt(itemId, productId);

	        	invMain.setReferencePrem(this.decimalToString(referencePrem));
	        	invMain.setStdPrem(this.decimalToString(stdPrem));
	        	// BC405 新增投資型模板為nb product special為M且unit_Flag為0
	        	if(!StringUtils.isNullOrEmpty(specialTypeM) && CodeCst.UNIT_FLAG__NON_SCHEDULED.equals(unitFlag)) {
	        		
	        		FmtUnb0441InvBasedPremium invBasedPremium = new FmtUnb0441InvBasedPremium();
		        	com.ebao.pub.util.BeanUtils.copyProperties(invBasedPremium, invMain);
		        	
	        		extensionVO.setInvBasedPremium(invBasedPremium);
	        	}
	        	else {
	        		extensionVO.setInvMain(invMain);
	        	}
	        	
	        	}
	        } else {
			
        	//1.13 投資型自訂保險費主約;
        	invCustomerPremMain = new FmtUnb0441InvCustomerPremMain();
        	invCustomerPremMain.setPlanCode(productVO.getInternalId());
        	
        	//[Develop 330044] 是否為BC374 新商品VLT&VLU added by Jyun-yu
        	boolean isCalcByAmount =  lifeProductService.isCustomizedPremCalcByAmount(Long.valueOf(masterCoverageVO.getProductId().longValue()));
        	if(isCalcByAmount){
        		invCustomerPremMain.setRecurringTopupIndi(CodeCst.YES_NO__YES);
        		invCustomerPremMain.setChargeYear(String.valueOf(masterCoverageVO.getChargeYear()));
	    } else {
        		invCustomerPremMain.setRecurringTopupIndi(CodeCst.YES_NO__NO);
        	}
        	 
        	if(masterCoverageVO.getProductType() != null) {
		String type = CodeTable.getCodeDesc(TableCst.T_PD_VERSION_TYPE_OPT, masterCoverageVO.getProductType(), CodeCst.LANGUAGE__CHINESE_TRAD);
        		invCustomerPremMain.setType(type);
        	}
			
        	invCustomerPremMain.setAmount(this.getAmountUnitBenefitLevel(masterCoverageVO));
        	invCustomerPremMain.setInsuredName(extensionVO.getMainInsuredName());
			invCustomerPremMain.setInsuredRomanName(extensionVO.getMainInsuredRomanName());

        	BigDecimal targetPrem = masterCoverageVO.getCurrentPremium().getStdPremAf();
        	BigDecimal overPrem = BigDecimal.ZERO;
        	BigDecimal customizedPrem = masterCoverageVO.getCustomizedPrem();

        	if (customizedPrem == null) {
        		if (CodeCst.YES_NO__YES.equals(masterCoverageVO.getSameStdPremIndi())) {
	        			customizedPrem = targetPrem;
        		}
        	}

        	if (customizedPrem != null && targetPrem != null) {
        		overPrem = customizedPrem.subtract(targetPrem);
        		if (overPrem.compareTo(BigDecimal.ZERO) < 0) {
        			overPrem = BigDecimal.ZERO;
        		}
        	}
	    /*
	     * 因前端邏輯,不為user自行輸入,改由自訂-目標 算出超額保險費 if
	     * (masterCoverageVO.getSingleTopup() != null &&
	     * masterCoverageVO.getSingleTopup().getTopupAmount() != null) {
	     * overPrem = masterCoverageVO.getSingleTopup().getTopupAmount(); }
        	 */
        	invCustomerPremMain.setTargetPrem(this.decimalToString(targetPrem));
        	invCustomerPremMain.setOverPrem(this.decimalToString(overPrem));
        	invCustomerPremMain.setCustomerPrem(this.decimalToString(customizedPrem));

        	extensionVO.setInvCustomerPremMain(invCustomerPremMain);
        	}
	}

	private void initInvRiderProductInfo(FmtUnb0441ExtensionVO extensionVO, PolicyVO policyVO, Collection<UwExtraLoadingVO> allExtra) {

		//1.16 投資型附約明細 ;                 
		List<FmtUnb0441InvRider> invRiderDetail = new ArrayList<FmtUnb0441InvRider>();

		FmtUnb0441InvRider invWaiverRider = null;
		BigDecimal waiverStdPrem = BigDecimal.ZERO; //豁免險標準保險費
		BigDecimal waiverExtraPrem = BigDecimal.ZERO; //豁免險加費
		BigDecimal waiverTotalPrem = BigDecimal.ZERO; //豁免險應繳保險費

		for (CoverageVO riderCoverage : policyVO.gitSortCoverageList()) {
			Long itemId = riderCoverage.getItemId();

			if (riderCoverage.getMasterId() != null) {

				CoveragePremium riderPremium = riderCoverage.getCurrentPremium();

				LifeProduct product = lifeProductService.getProductByVersionId(riderCoverage.getProductVersionId());
				ProductVO productVO = product.getProduct();

				if (riderCoverage.isWaiver() == false) {
					FmtUnb0441InvRider invRider = new FmtUnb0441InvRider();
					invRider.setPlanCode(productVO.getInternalId());
					invRider.setAmount(this.getAmountUnitBenefitLevel(riderCoverage));
					invRider.setInsuredName(riderCoverage.getInsureds().get(0).getInsured().getName());
					invRider.setInsuredRomanName(riderCoverage.getInsureds().get(0).getInsured().getRomanName());

					//標準保費
					invRider.setStdPrem(this.decimalToString(riderPremium.getStdPremAf()));
					//加費保險費
					BigDecimal extraPrem = BigDecimal.ZERO;

					@SuppressWarnings("unchecked")
		    Collection<UwExtraLoadingVO> riderExtraList = CollectionUtils.select(allExtra, new BeanPredicate("itemId", PredicateUtils.equalPredicate(itemId)));

					for (UwExtraLoadingVO riderExtra : riderExtraList) {
						if (riderExtra.getExtraPrem() != null) {
							extraPrem = extraPrem.add(riderExtra.getExtraPrem());
						}
					}
					if (extraPrem.compareTo(BigDecimal.ZERO) >= 0) {
						invRider.setExtraPrem(this.decimalToString(extraPrem));
					}

					//應繳保險費=標準保費+加費保險費
					BigDecimal stdPremAf = riderPremium.getStdPremAf().add(extraPrem);
					invRider.setPremAf(this.decimalToString(stdPremAf));

					//PCR_426401 BC416 3.6.2 FMT_UNB_0441新契約內容變更同意書  新增 折讓保險費、繳費年期、給付期間(年)；非豁免險才顯示
					BigDecimal discntPrem = riderPremium.getDiscntPremAf(); //折讓保險費					
					invRider.setDiscntPrem(this.decimalToString(discntPrem));
					
					String chargePeriod = NBUtils.generateChargePeriodDesc(riderCoverage, productVO, product);//繳費年期
					invRider.setChargePeriod(StringUtils.emptyToNull(chargePeriod));
					
					String proposalTerm = NBUtils.generateProposalTermText(riderCoverage.getProposalTerm(), productVO);//給付期間(年)
					invRider.setProposalTerm(StringUtils.emptyToNull(proposalTerm));
					
					invRiderDetail.add(invRider);
				} else {

					//豁免險累加為1條
					if (invWaiverRider == null) {
						invWaiverRider = new FmtUnb0441InvRider();
						invWaiverRider.setPlanCode(productVO.getInternalId());
						//invWaiverRider.setAmount(this.getAmountUnitBenefitLevel(riderCoverage));
						//豁免險不顯示保額
						invWaiverRider.setAmount(null);
						invWaiverRider.setInsuredName(riderCoverage.getInsureds().get(0).getInsured().getName());
					} else {
						//多筆清空保額
						invWaiverRider.setAmount(null);
					}

					//豁免險標準保險費
					if (riderPremium.getStdPremAf() != null) {
						waiverStdPrem = waiverStdPrem.add(riderPremium.getStdPremAf());
					}
					//豁免險加費
					if (riderPremium.getExtraPremAf() != null) {
						waiverExtraPrem = waiverExtraPrem.add(riderPremium.getExtraPremAf());
					}
					//豁免險應繳保險費
					if (riderPremium.getTotalPremAf() != null) {
						waiverTotalPrem = waiverTotalPrem.add(riderPremium.getTotalPremAf());
					}
				}
			}
		}

		if (invWaiverRider != null) {
			invWaiverRider.setStdPrem(this.decimalToString(waiverStdPrem));
			invWaiverRider.setExtraPrem(this.decimalToString(waiverExtraPrem));
			invWaiverRider.setPremAf(this.decimalToString(waiverTotalPrem));
			invRiderDetail.add(invWaiverRider);
		}

		if (invRiderDetail.size() > 0) {
			extensionVO.setInvRiderDetail(invRiderDetail);
			
			//PCR 156288-應繳保險費備註
			//RTC-224174-C-->投資型主約有附約時動態顯示
			extensionVO.setNoticeTotalPrem("I");//I-投資型
		}
	}

	private void initInvTotalPremInfo(FmtUnb0441ExtensionVO extensionVO, PolicyVO policyVO, Collection<UwExtraLoadingVO> allExtra) {

		CoverageVO masterCoverageVO = policyVO.gitMasterCoverage();
		String payMode = "COD_T_CHARGE_MODE_" + masterCoverageVO.getCurrentPremium().getPaymentFreq();

		//  1.18  投資型保險費合計    tradPremTotal
		FmtUnb0441PremTotal invPremTotal = new FmtUnb0441PremTotal();

		//  1.18.1  應繳保險費合計    premTotalAf
		BigDecimal totalPremAf = BigDecimal.ZERO;
		for (CoverageVO vo : policyVO.gitSortCoverageList()) {
			if(vo.getCustomizedPrem() != null && vo.getCustomizedPrem().compareTo(BigDecimal.ZERO) > 0) {
				totalPremAf = totalPremAf.add(vo.getCustomizedPrem());
			} else if (vo.getCurrentPremium().getTotalPremAf() != null) {
				totalPremAf = totalPremAf.add(vo.getCurrentPremium().getTotalPremAf());
			}
		}

		if (totalPremAf.compareTo(BigDecimal.ZERO) > 0) {
			invPremTotal.setPremTotalAf(this.decimalToString(totalPremAf));
		}
		//  1.18.2  應繳保險費   installPrem
		invPremTotal.setInstallPrem(this.decimalToString(policyVO.getProposalInfo().getInstallPrem()));

		//  1.18.3  繳別  payMode
		invPremTotal.setPayMode(StringResource.getStringData(payMode, AppUserContext.getCurrentUser().getLangId()));
		//  1.18.4  幣別  currency
		String moneyCode = "";
		String moneyId = String.valueOf(policyVO.getCurrency());
		if("8".equals(moneyId)){
			moneyCode = StringResource.getStringData("COD_T_MONEY_SQL_8", CodeCst.LANGUAGE__CHINESE_TRAD);			
		}else{
			moneyCode = StringResource.getStringData("COD_T_MONEY_" + moneyId, CodeCst.LANGUAGE__CHINESE_TRAD);
		}
		invPremTotal.setCurrency(moneyCode);

		extensionVO.setInvdPremTotal(invPremTotal);
	}

	private void initInvNoticeInfo(FmtUnb0441ExtensionVO extensionVO, PolicyVO policyVO, Collection<UwExtraLoadingVO> allExtra) {

		CoverageVO masterCoverageVO = policyVO.gitMasterCoverage();
		//1.19  投資型主約弱體加費備註  noticeInvHealthExtra
	if (extensionVO.getInvHealthExtra() != null || extensionVO.getInvExtra() != null || extensionVO.getInvOccupExtra() != null) {
			//RTC-224174-B-->投資型主約有【弱體】/【膱業】加費才須顯示
			extensionVO.setNoticeInvHealthExtra(CodeCst.YES_NO__YES);
		}

		//1.20    投資型自訂保費備註   noticeCustomerPrem
		if (extensionVO.getInvCustomerPremMain() != null) {
			boolean isCalcByAmount =  lifeProductService.isCustomizedPremCalcByAmount(Long.valueOf(masterCoverageVO.getProductId().longValue()));
			if(isCalcByAmount){
				extensionVO.setNoticeRecurringTopup(CodeCst.YES_NO__YES);
	    } else {
				extensionVO.setNoticeCustomerPrem(CodeCst.YES_NO__YES);
			}
		}
		//1.21  投資型保單顯示備註   noticeInv , RTC-224174-A-->已刪除此警語，不須顯示
		//extensionVO.setNoticeInv(CodeCst.YES_NO__YES);

		//1.26    投資型主被保險人小於(等於)15歲備註 noticeNotOver15;
		//RTC-224174 comment 64
		// BC405 移除此備註
		//long age = policyVO.gitMasterCoverage().getLifeInsured1().getEntryAge();
		//if (age <= 15) {
		//	extensionVO.setNoticeNoteOver15(CodeCst.YES_NO__YES);
		//}

	}
	
	/**
     * <p>
     * Description :
     * </p>
     * <p>
     * Created By : simon.huang
     * </p>
     * <p>
     * Create Time : Aug 27, 2017
     * </p>
     * 
	 * @param extensionVO
	 * @param policyVO
	 * @param allExtra
	 * @deprecated (PCR 156288-整合主約與副約一同顯示，因此增加deprecated說明)
	 */
	@SuppressWarnings("unused")
	private void initTradMainProductInfo(FmtUnb0441ExtensionVO extensionVO, PolicyVO policyVO, Collection<UwExtraLoadingVO> allExtra) {

		CoverageVO masterCoverageVO = policyVO.gitMasterCoverage();//主約
		Long itemId = masterCoverageVO.getItemId();
		Long productId = Long.valueOf(masterCoverageVO.getProductId());
		Long productVersionId = masterCoverageVO.getProductVersionId();

		LifeProduct lifeProduct = lifeProductService.getProductByVersionId(productVersionId);
		ProductVO productVO = lifeProduct.getProduct();

		CoveragePremium premium = masterCoverageVO.getCurrentPremium();
		//1.14 傳統型主約  ;  
		FmtUnb0441TradItem tradMain = new FmtUnb0441TradItem();

		tradMain.setPlanCode(productVO.getInternalId());
		tradMain.setInsuredName(extensionVO.getMainInsuredName());
		tradMain.setInsuredRomanName(extensionVO.getMainInsuredRomanName());
		tradMain.setAmount(this.getAmountUnitBenefitLevel(masterCoverageVO));

		BigDecimal stdPremWithoutAnyDiscnt = premium.getStdPremAf(); //標準保險費(期繳/無高保額折讓)
		Long stdPremDiscnt = new Long(0); //高保額折讓(期繳)
		BigDecimal extraPrem = BigDecimal.ZERO; //加費保險費
		BigDecimal discntPrem = BigDecimal.ZERO; //折讓保險費
		BigDecimal totalPrem = BigDecimal.ZERO; //應繳保險費
	stdPremWithoutAnyDiscnt = coverageService.getStdPremWithoutAnyDiscnt(itemId, productId);
		stdPremDiscnt = CoverageSP.getNbDisnctPrem(masterCoverageVO.getItemId());

		extraPrem = premium.getExtraPremAf();
		discntPrem = premium.getDiscntPremAf();
		totalPrem = premium.getTotalPremAf();

		//標準保險費
		tradMain.setStdPrem(this.decimalToString(stdPremWithoutAnyDiscnt));
		//高保額保費折讓
		tradMain.setHighPremDiscnt(this.decimalToString(stdPremDiscnt));
		tradMain.setExtraPrem(this.decimalToString(extraPrem));
		tradMain.setExtraPrem(this.decimalToString(extraPrem));
		tradMain.setPremAf(this.decimalToString(totalPrem));
		//PCR 156288-增加折讓保險費顯示
		tradMain.setDiscntPrem(this.decimalToString(discntPrem));

		extensionVO.setTradMain(tradMain);

	}

	private void initTradRiderProductInfo(FmtUnb0441ExtensionVO extensionVO, PolicyVO policyVO, Collection<UwExtraLoadingVO> allExtra) {

		//1.15 傳統型附約明細      ;            
		List<FmtUnb0441TradItem> tradRiderDetail = new ArrayList<FmtUnb0441TradItem>();

		FmtUnb0441TradItem tradWaiverRider = null;
		BigDecimal waiverStdPrem = BigDecimal.ZERO; //豁免險標準保險費
		BigDecimal waiverHiPremDiscnt = BigDecimal.ZERO;//豁免險高保額折讓	 
		BigDecimal waiverExtraPrem = BigDecimal.ZERO; //豁免險加費
		BigDecimal waiverDiscntPrem = BigDecimal.ZERO; //豁免險折讓保險費
		BigDecimal waiverTotalPrem = BigDecimal.ZERO; //豁免險應繳保險費

		boolean isHasRider = false;
		for (CoverageVO riderCoverage : policyVO.gitSortCoverageList()) {

			Long itemId = riderCoverage.getItemId();
			Long productId = Long.valueOf(riderCoverage.getProductId());
			Long productVersionId = riderCoverage.getProductVersionId();

			LifeProduct lifeProduct = lifeProductService.getProductByVersionId(productVersionId);
			ProductVO productVO = lifeProduct.getProduct();

			//PCR 156288-整合主約與副約一同顯示
			//if (riderCoverage.getMasterId() != null) {

			CoveragePremium riderPremium = riderCoverage.getCurrentPremium();

			if(riderCoverage.getMasterId() != null) {
				isHasRider = true;
			}
			
			if (riderCoverage.isWaiver() == false) {

				FmtUnb0441TradItem tradRider = new FmtUnb0441TradItem();

				tradRider.setPlanCode(productVO.getInternalId());
				tradRider.setAmount(this.getAmountUnitBenefitLevel(riderCoverage));
				tradRider.setInsuredName(riderCoverage.getInsureds().get(0).getInsured().getName());
				tradRider.setInsuredRomanName(riderCoverage.getInsureds().get(0).getInsured().getRomanName());

				BigDecimal stdPremWithoutAnyDiscnt = riderPremium.getStdPremAf(); //標準保險費(期繳/無高保額折讓)
				Long stdPremDiscnt = new Long(0); //高保額折讓(期繳)
				BigDecimal extraPrem = BigDecimal.ZERO; //加費保險費
				BigDecimal discntPrem = BigDecimal.ZERO; //折讓保險費
				BigDecimal totalPrem = BigDecimal.ZERO; //應繳保險費
		stdPremWithoutAnyDiscnt = coverageService.getStdPremWithoutAnyDiscnt(itemId, productId);
				stdPremDiscnt = CoverageSP.getNbDisnctPrem(riderCoverage.getItemId());

				extraPrem = riderPremium.getExtraPremAf();
				discntPrem = riderPremium.getDiscntPremAf();
				totalPrem = riderPremium.getTotalPremAf();

				//標準保險費
				tradRider.setStdPrem(this.decimalToString(stdPremWithoutAnyDiscnt));
				//高保額保費折讓
				tradRider.setHighPremDiscnt(this.decimalToString(stdPremDiscnt));
				tradRider.setExtraPrem(this.decimalToString(extraPrem));
				tradRider.setPremAf(this.decimalToString(totalPrem));
				//PCR 156288-增加折讓保險費顯示
				tradRider.setDiscntPrem(this.decimalToString(discntPrem));

				//PCR_426401 BC416 3.6.2 FMT_UNB_0441新契約內容變更同意書  新增 繳費年期、給付期間(年)；非豁免險才顯示
				String chargePeriod = NBUtils.generateChargePeriodDesc(riderCoverage, productVO, lifeProduct);//繳費年期
				tradRider.setChargePeriod(StringUtils.emptyToNull(chargePeriod));
				
				String proposalTerm = NBUtils.generateProposalTermText(riderCoverage.getProposalTerm(), productVO);//給付期間(年)
				tradRider.setProposalTerm(StringUtils.emptyToNull(proposalTerm));
				
				tradRiderDetail.add(tradRider);
			} else {

				if (tradWaiverRider == null) {
					//豁免險累加為1條
					tradWaiverRider = new FmtUnb0441TradItem();
					tradWaiverRider.setPlanCode(productVO.getInternalId());
					//tradWaiverRider.setAmount(this.getAmountUnitBenefitLevel(riderCoverage));
					//豁免險不顯示保額
					tradWaiverRider.setAmount(null);
					tradWaiverRider.setInsuredName(riderCoverage.getInsureds().get(0).getInsured().getName());
				} else {
					//多筆清空保額
					tradWaiverRider.setAmount(null);
				}
				
				//豁免險標準保險費
				if (riderPremium.getStdPremAf() != null) {
					waiverStdPrem = waiverStdPrem.add(riderPremium.getStdPremAf());
				}
				//豁免險加費
				if (riderPremium.getExtraPremAf() != null) {
					waiverExtraPrem = waiverExtraPrem.add(riderPremium.getExtraPremAf());
				}
				//豁免險折讓保險費
				if (riderPremium.getDiscntPremAf() != null) {
					waiverDiscntPrem = waiverDiscntPrem.add(riderPremium.getDiscntPremAf());
				}
				//豁免險應繳保險費
				if (riderPremium.getTotalPremAf() != null) {
					waiverTotalPrem = waiverTotalPrem.add(riderPremium.getTotalPremAf());
				}
			}
			//}
		}

		if (tradWaiverRider != null) {
			//標準保險費
			tradWaiverRider.setStdPrem(this.decimalToString(waiverStdPrem));
			//高保額保費折讓
			tradWaiverRider.setHighPremDiscnt(this.decimalToString(waiverHiPremDiscnt));
			tradWaiverRider.setExtraPrem(this.decimalToString(waiverExtraPrem));
			tradWaiverRider.setPremAf(this.decimalToString(waiverTotalPrem));
			//PCR 156288-增加折讓保險費顯示
			tradWaiverRider.setDiscntPrem(this.decimalToString(waiverDiscntPrem));

			tradRiderDetail.add(tradWaiverRider);
		}

		if (tradRiderDetail.size() > 0) {
			extensionVO.setTradRiderDetail(tradRiderDetail);
		}
		//if(isHasRider) {
		//PCR 156288-應繳保險費備註
		//RTC-224174-C-->傳統型主約有附約時動態顯示
		//更正RTC-224174-C-->傳統型主約(一律)時動態顯示
		extensionVO.setNoticeTotalPrem("T");//T-傳統型	
		//}
	}

	private void initTradTotalPremInfo(FmtUnb0441ExtensionVO extensionVO, PolicyVO policyVO, Collection<UwExtraLoadingVO> allExtra) {

		CoverageVO masterCoverageVO = policyVO.gitMasterCoverage();
		String payMode = "COD_T_CHARGE_MODE_" + masterCoverageVO.getCurrentPremium().getPaymentFreq();

		//  1.17  傳統型保險費合計    tradPremTotal
		FmtUnb0441PremTotal tradPremTotal = new FmtUnb0441PremTotal();

		//  1.17.1  調整後保險費合計    premTotalAf 
		BigDecimal totalPremAf = BigDecimal.ZERO;
		for (CoverageVO vo : policyVO.gitSortCoverageList()) {
			if (vo.getCurrentPremium().getTotalPremAf() != null) {
				totalPremAf = totalPremAf.add(vo.getCurrentPremium().getTotalPremAf());
			}
		}

		if (totalPremAf.compareTo(BigDecimal.ZERO) > 0) {
			tradPremTotal.setPremTotalAf(this.decimalToString(totalPremAf));
		}

		//  1.17.2  應繳保險費   installPrem      
		tradPremTotal.setInstallPrem(this.decimalToString(policyVO.getProposalInfo().getInstallPrem()));
		//  1.17.3  繳別  payMode
		tradPremTotal.setPayMode(StringResource.getStringData(payMode, AppUserContext.getCurrentUser().getLangId()));
		//  1.17.4  幣別  currency
		String moneyCode = "";
		String moneyId = String.valueOf(policyVO.getCurrency());
		if("8".equals(moneyId)){
			moneyCode = StringResource.getStringData("COD_T_MONEY_SQL_8", CodeCst.LANGUAGE__CHINESE_TRAD);			
		}else{
			moneyCode = StringResource.getStringData("COD_T_MONEY_" + moneyId, CodeCst.LANGUAGE__CHINESE_TRAD);
		}
		tradPremTotal.setCurrency(moneyCode);

		extensionVO.setTradPremTotal(tradPremTotal);

	}

	private void initTradNoticeInfo(FmtUnb0441ExtensionVO extensionVO, PolicyVO policyVO, Collection<UwExtraLoadingVO> allExtra) {

		//1.22   傳統型主約備註1  mark by RTC-225798(2018/03/06 simon)
		//倘若本保險單享有高保額折讓或須額外加費，調整後保險費合計為標準保險費扣除高保額保費折讓加上加費保險費。;
		//extensionVO.setNoticeTrad1(CodeCst.YES_NO__YES);
		// 1.25  傳統型主約備註2 mark by RTC-225798(2018/03/06 simon)
		//各項保險費折讓，日後若有異動，將依本公司之相關規定辦理。 ;
		//extensionVO.setNoticeTrad2(CodeCst.YES_NO__YES);
	}

	private String decimalToString(Object targetPrem) {
		if (targetPrem != null) {
			return targetPrem.toString();
		}
		return null;
	}

	/**
     * <p>
     * Description : 批註/加費 共用前端頁面-初始保項列表
     * </p>
     * <p>
     * Created By : simon.huang
     * </p>
     * <p>
     * Create Time : Aug 5, 2016
     * </p>
     * 
	 * @param policyVO
	 * @param in
	 * @return
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 * @throws NoSuchMethodException
	 */
	@SuppressWarnings("unchecked")
    public List<Map<String, Object>> getDisplayCoverageList(PolicyVO policyVO, Map<String, Object> in) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {

		List<Map<String, Object>> coverageList = new ArrayList<Map<String, Object>>(); //被保險人險種

		//CoverageVO masterCoverageVO = policyVO.gitMasterCoverage();

		String reqFrom = (String) in.get("reqFrom");
		String itemId = (String) in.get("itemId");
		
		//險種列表
		
		Map<String,CoverageVO> itemIdBindCoverage = NBUtils.toMap(policyVO.gitSortCoverageList(), "itemId");
		for (CoverageVO coverageVO : policyVO.gitSortCoverageList()) {
			Integer productId = coverageVO.getProductId();
			Long productVersionId = coverageVO.getProductVersionId();
			LifeProduct lifeProduct = lifeProductService.getProductByVersionId(productVersionId);
			ProductVO productVO = lifeProduct.getProduct();
			
			/*
	     * if (coverageVO.isWaiver() &&
	     * !masterCoverageVO.getItemId().equals(coverageVO.getMasterId())) {
	     * //豁免險且非主約的豁免險不作顯示 continue; }
			*/
			if (isPOS(reqFrom) && !itemId.contains(coverageVO.getItemId().toString())) {
				continue;
			}

	    if (isPOS(reqFrom) == false && policyVO.getRiskStatus() == CodeCst.LIABILITY_STATUS__NON_VALIDATE && coverageVO.getRiskStatus() != CodeCst.LIABILITY_STATUS__NON_VALIDATE) {
				//新契約件僅處理待生效保項(豁免險不可附加的不作處理)
				continue;
			}
			

			
			
			

			//t_add_arith 加費類型區分傳統/投資
			List<String> addArithTypes = lifeProduct.getNbRules().getExtraTypes();

			Map<String, Object> row = new HashMap<String, Object>();

			String isCOIProduct = CodeCst.YES_NO__NO;
			/* 判斷是否有coi的收費規則 */
			if (coverageService.isCOIProduct(productId.longValue(), productVersionId)) {
				isCOIProduct = CodeCst.YES_NO__YES;
			}

			//被保險人id
			if (coverageVO.getInsureds() != null && coverageVO.getInsureds().size() > 0) {
				
				if(coverageVO.isWaiver() == false){
					CoverageInsuredVO insured1 = coverageVO.getLifeInsured1();
					row.put("insuredId", insured1.getInsuredId());	
				} else {
					if("pos".equals(reqFrom)){
			// CoverageVO mainCoverage =
			// itemIdBindCoverage.get(String.valueOf(coverageVO.getMasterId()));
			// CoverageInsuredVO insured1 =
			// mainCoverage.getLifeInsured1();
						CoverageInsuredVO insured1 = coverageVO.getLifeInsured1();
						row.put("insuredId", insured1.getInsuredId());
					} else {
						CoverageInsuredVO insured1 = coverageVO.getLifeInsured1();
						row.put("insuredId", insured1.getInsuredId());	
					}
				}
			}
			//險種名稱

			row.put("itemId", coverageVO.getItemId());
			row.put("itemOrder", coverageVO.getItemOrder());
			row.put("productId", productId);
			row.put("masterId", coverageVO.getMasterId());
			row.put("waiver", coverageVO.getWaiverExt() != null ? coverageVO.getWaiverExt().getWaiver() : "");
			row.put("productVersionId", productVersionId);
			if (isPOS(reqFrom)) {
				row.put("productName", productConfigService.getProductNameWithProposalTermCategory(coverageVO)); //dev428931-除外責任XTC(投保期間分類設定為3)險種名稱後方加「長期照護分期保險金給付年度XX年」，其中XX顯示長照年度		
			}else{
				row.put("productName", policyDS.getCoverageProudctName(coverageVO));
			}
			row.put("productVersionTypeId", coverageVO.getVersionTypeId());
			row.put("planCode", productVO.getInternalId());
			row.put("itemOrder", coverageVO.getItemOrder());
			row.put("isCOIProduct", isCOIProduct);
			row.put("addArithTypes", addArithTypes);
			//險種費用相關
			CoveragePremium coveragePremium = coverageVO.getCurrentPremium();

			Map<String, Object> premiumMap = BeanUtils.describe(coveragePremium);
			row.putAll(premiumMap);
			// logger.warn("coveragePremium=" + ToStringBuilder.reflectionToString(coveragePremium, ToStringStyle.MULTI_LINE_STYLE));
	    logger.warn("coveragePremium.coverageStdPremAf=" + coveragePremium.getStdPremAf() + ",isWaiver=" + coverageVO.isWaiver());
	    logger.warn(getClass() + ":itemId=" + coverageVO.getItemId() + ", coveragePremium.coverageStdPremAf=" + coveragePremium.getStdPremAf() + ",isWaiver=" + coverageVO.isWaiver());
			BigDecimal stdPremWithoutAnyDiscnt = coveragePremium.getStdPremAf(); //期繳保費(無高保額折讓)
			BigDecimal nextStdPremAf = coverageVO.getNextPremium().getStdPremAf(); // IR#441717 調整顯示為下期標準保費
			Long stdPremDiscnt = new Long(0); //高保額折讓(期繳)
			Date dueDate = null;
			logger.warn("170519 >> nextStdPremAf =" + nextStdPremAf + ", CustomizedPrem::" + coverageVO.getCustomizedPrem());
			// 非豁免
			if (!coverageVO.isWaiver()) {
				Long policyChgId = null;
				int bizCategory = CodeCst.BIZ_CATEGORY_UNB;
				Date validateDate = null;
				// 2017-07-06 Kate 更新保全標準保費(無高保額折讓)的算法 
				if (this.isPOS(reqFrom)) {
					policyChgId = Long.parseLong((String) in.get("policyChgId"));
					bizCategory = CodeCst.BIZ_CATEGORY_POS;
					
					CoverageVO policyProduct = coverageService.load(coverageVO.getItemId());
					logger.warn("UwActionHelper dueDate =" + policyProduct.getCoverageExtend().getDueDate());
					dueDate = policyProduct.getCoverageExtend().getDueDate();
					validateDate = alterationItemService.getAlterationItem(policyChgId).getValidateTime();
				}
				
				boolean ilp = this.isILP(coverageVO.getItemId());
				// FIXME -- 等了解更多後應整理此段程式
				// IR #322328 POS 投資型商品 畫面上標準保費 為T_CONTRACT_PRODUCT.STD_PREM_AF
				logger.warn(">> reqFrom::" + reqFrom + "this.isPOS(reqFrom)::" + this.isPOS(reqFrom) + "Rollback 170519 >> ilp::" +  ilp);
				//--------------------------------------------------------------------------------
				// 以下調整拆分UNB與POS對於 標準保費顯示數值 各自定義
				// POS畫面且為投資型路線
				if (this.isPOS(reqFrom)) {
					if(ilp){
						stdPremWithoutAnyDiscnt = coveragePremium.getStdPremAf();
						logger.warn(">> 1.1. isPOS & isILP then stdPremWithoutAnyDiscnt =" + stdPremWithoutAnyDiscnt);
					}else{
						// IR#447046 保全的部分 標準保費維持 #441417 的下期應繳保費
						stdPremWithoutAnyDiscnt = nextStdPremAf;
						logger.warn(">> 1.2. isPOS & is Trad then stdPremWithoutAnyDiscnt =" + stdPremWithoutAnyDiscnt);
					}
				} else {
					// 顯示UNB畫面時, 投資或傳統之 標準保費 數據
					stdPremWithoutAnyDiscnt = coverageService.getStdPremWithoutAnyDiscnt(coverageVO.getItemId(), coverageVO.getProductId().longValue(), bizCategory, dueDate, validateDate, policyChgId);
					logger.warn(">> 1.3. stdPremWithoutAnyDiscnt =" + stdPremWithoutAnyDiscnt);
				}
				logger.warn("stdPremWithoutAnyDiscnt =" + stdPremWithoutAnyDiscnt);
			
				// 192414 POS stdPremWithoutAnyDiscnt為年期的，所以在乘上係數率
//				if (this.isPOS(reqFrom)) {
//					logger.warn("Rollback 170519 >> 1.3. into isPOS block");
//					String chargeType = coverageVO.getCurrentPremium().getPaymentFreq();
//					Float modeltype = (float) 1;
//					if(ilp == true) {
//						chargeType = "4";
//						modeltype = (float) 3;
//					}
//					float calcRate = this.pmodelFactor(coverageVO.getProductId(), chargeType, modeltype, coverageVO.getCurrentPremium().getPaymentMethod());
//					logger.warn("Rollback 170519 >> 1.4. calcRate::" + calcRate);
//					if(ilp != true) {						
//						stdPremWithoutAnyDiscnt = stdPremWithoutAnyDiscnt.multiply(BigDecimal.valueOf(calcRate));
//						logger.warn("Rollback 170519 >> 1.5. stdPremWithoutAnyDiscnt * calcRate::" + stdPremWithoutAnyDiscnt);
//					}else{
//						BigDecimal trialCalcValue = stdPremWithoutAnyDiscnt.multiply(BigDecimal.valueOf(calcRate));
//						logger.warn("Rollback 170519 >> 1.6. trialCalcValue ::" + trialCalcValue); 
//					}
//
//					logger.warn("modeltype = " + modeltype + ", chargeType = " + chargeType + ", calcRate = " + calcRate);
//				}
				stdPremDiscnt = CoverageSP.getNbDisnctPrem(coverageVO.getItemId());
				logger.warn("stdPremWithoutAnyDiscnt = " + stdPremWithoutAnyDiscnt + ", stdPremDiscnt = " + stdPremDiscnt);
			}
			// 2017-07-14 Kate 在投資型商品算保險成本COI時使用 
			row.put("dueDate", dueDate);
			row.put("jobExtraPrem", 0); //default 0 
			if (coverageVO.getMasterId() == null) {
				InsuredVO insuredVO = coverageVO.getLifeInsured1().getInsured();
				//被保險人職業加費率
				Long jobCate = insuredVO.getOccupCate();

				List<JobCategoryVO> list = jobCategoryDao.findByJobCateId(jobCate);
				if (list != null && list.size() > 0) {
					JobCategoryVO jobVO = list.get(0);

					//RTC 155815-OIU商品不作職加預設金額設定
					//RTC 156119-費率999以下才出
		    if (policyVO.isOIU() == false && jobVO.getFeeRate() != null && jobVO.getFeeRate().compareTo(InsuredJobCateIdValidator.ignoreRate) < 0) {

						//BC374 IR-370700 VLT 改以每元之保額輸入職加金額，改讀EmUnitAmount 做職加費率計算 2020/03/13 Add by Kathy
						//Long saUnitAmount = getProductSaUnitAmount(productId);
						//if (saUnitAmount != null) {
						Long emUnitAmount = getProductEmUnitAmount(productId);
						if (emUnitAmount != null) {
							//職業加費比例為萬元計,需依商品設定千元/萬元設定為計價比例。
							BigDecimal jobExtraPrem = jobVO.getFeeRate();
							jobExtraPrem = jobExtraPrem.divide(new BigDecimal(10000)).multiply(new BigDecimal(emUnitAmount));
							row.put("jobExtraPrem", jobExtraPrem);
						}
					}
				}
			}

			row.put("stdPremWithoutAnyDiscnt", stdPremWithoutAnyDiscnt);
			// 高保額保費折讓
			row.put("stdPremDiscnt", coverageVO.isWaiver() ? 0L : CoverageSP.getNbDisnctPrem(coverageVO.getItemId()));

			//PCR_426401 BC416 3.2	批註 新增 繳費年期、給付期間(年)
			row.put("chargePeriod", NBUtils.generateChargePeriodDesc( coverageVO, productVO, lifeProduct));
			row.put("proposalTerm", NBUtils.generateProposalTermText( coverageVO.getProposalTerm(), productVO));
			
			coverageList.add(row);
		}

		return coverageList;
	}

	private boolean isPOS(String reqFrom) {
		return reqFrom != null && reqFrom.equals("pos");
	}

	/**
     * <p>
     * Description : 前端頁面初始-批註及商品條款所需資料
     * </p>
     * <p>
     * Created By : simon.huang
     * </p>
     * <p>
     * Create Time : Aug 5, 2016
     * </p>
     * 
	 * @param policyInfo
	 * @param in
	 * @return
	 */
	public List<Map<String, Object>> getDisplayCoverageEndorsement(PolicyVO policyInfo, Map<String, Object> in) {
		List<Map<String, Object>> endorsementList = new ArrayList<Map<String, Object>>();//險種保單附加條款

		String reqFrom = (String) in.get("reqFrom");
		String itemId = (String) in.get("itemId");

		//險種列表
		for (CoverageVO coverageInfo : policyInfo.getCoverages()) {

			if (coverageInfo.isWaiver()) {
				continue;
			}
			// 當是保全模組的保全項:次標加費進入時，只取傳入的保項 
			if (isPOS(reqFrom) && !itemId.contains(coverageInfo.getItemId().toString())) {
				continue;
			}

			Integer productId = coverageInfo.getProductId();
			Long productVersionId = coverageInfo.getProductVersionId();

			//險種保單附加條款
	    List<ProdVerEndorsementVO> endorsementVOList = TProdVerEndorsementDelegate.findByProductId(productId, productVersionId, coverageInfo.getApplyDate());

			for (ProdVerEndorsementVO endorseVO : endorsementVOList) {
				String endorId = String.valueOf(endorseVO.getEndorId());
				String endorName = CodeTable.getCodeDesc(TableCst.T_ENDORSEMENT_CODE_BY_ID, endorId);
				String endorCode = CodeTable.getCodeById(TableCst.T_ENDORSEMENT_CODE_BY_ID, endorId);
				Map<String, Object> endorsement = new HashMap<String, Object>();
				endorsement.put("itemId", coverageInfo.getItemId());
				endorsement.put("endorId", endorseVO.getEndorId());
				endorsement.put("endorName", endorName);
				endorsement.put("endorCode", endorCode);
				endorsementList.add(endorsement);
			}
		}

		return endorsementList;
	}

	/**
     * <p>
     * Description : 批註/加費 共用前端頁面-初始被保險人列表
     * </p>
     * <p>
     * Created By : simon.huang
     * </p>
     * <p>
     * Create Time : Aug 5, 2016
     * </p>
     * 
	 * @param policyVO
	 * @param in
	 * @return
	 */
	public List<Map<String, Object>> getDisplayInusredList(PolicyVO policyVO, Map<String, Object> in) {

		List<Map<String, Object>> insuredList = new ArrayList<Map<String, Object>>();

		String reqFrom = (String) in.get("reqFrom");
		String itemIdsStr = (String) in.get("itemId");
		List<InsuredVO> insuredVOs = new ArrayList<InsuredVO>();
		//pos次標加費進入加費頁面，只顯示選中保項的被保險人
		if (isPOS(reqFrom)) {
			String[] itemIds = itemIdsStr.split(",");
			Map<Long,Object> insuredIdMap = new HashMap<Long, Object>();
			for (String itemId : itemIds) {
				CoverageVO coverageVO = coverageService.load(Long.parseLong(itemId));
				List<CoverageInsuredVO> coverageInsuredVOs = coverageVO.getInsureds();
				if (coverageInsuredVOs != null && coverageInsuredVOs.size() > 0) {
					for (CoverageInsuredVO coverageInsuredVO : coverageInsuredVOs) {
						if(!insuredIdMap.containsKey(coverageInsuredVO.getInsured().getListId())) {
							insuredIdMap.put(coverageInsuredVO.getInsured().getListId(), coverageInsuredVO.getInsured());
							insuredVOs.add(coverageInsuredVO.getInsured());
						}
					}
				}
			}
		} else {
			insuredVOs = policyVO.gitSortInsuredList();
		}
	
	String phCertiCode = policyVO.getPolicyHolder().getCertiCode();
		//被保險人列表 > 被保險人姓名|出生年月|保險年齡|性別 
		for (InsuredVO insuredVO : insuredVOs) {

			Long insuredId = insuredVO.getListId();

			Map<String, Object> row = new HashMap<String, Object>();
			row.put("insuredId", insuredId);
			row.put("certiCode", insuredVO.getCertiCode());
			row.put("insuredCategory", insuredVO.getInsuredCategory());
			row.put("partyId", insuredVO.getPartyId());
			row.put("name", insuredVO.getName());
			row.put("birthDate", insuredVO.getBirthDate());
			row.put("gender", insuredVO.getGender());
			row.put("relationToPH", insuredVO.getRelationToPH());
			row.put("nbInsuredCategory", insuredVO.getNbInsuredCategory());
			row.put("isWaiver", CodeCst.YES_NO__NO);
			row.put("age", null);
	    row.put("isHolder", insuredVO.isSamePolicyHolder(phCertiCode)?CodeCst.YES_NO__YES:CodeCst.YES_NO__NO);

			//保險年齡
			for (CoverageVO coverageInfo : policyVO.gitCoveragesByInsured(insuredId)) {
				if (coverageInfo.getInsureds() != null && coverageInfo.getInsureds().size() > 0) {
					CoverageInsuredVO insured1 = coverageInfo.getLifeInsured1();
					if (insuredVO.getListId().longValue() == insured1.getInsuredId().longValue()) {
						row.put("age", insured1.getEntryAge());
						break;
					}
				}
			}

			/* 2016/07/26 Alex Cheng 增加傷殘加費金額取得 */
	    BigDecimal sumDisabilityExtraPrem = extraPremService.sumExtraPremByPolicyIdInsureId(policyVO.getPolicyId(), insuredId, CodeCst.EXTRA_TYPE__DISABILITY_EXTRA);
			if (sumDisabilityExtraPrem == null) {
				row.put("sumDisabilityExtraPrem", 0);
			} else {
				row.put("sumDisabilityExtraPrem", sumDisabilityExtraPrem);
			}

			if (insuredVO.isWaiverInsured()) {
				row.put("isWaiver", CodeCst.YES_NO__YES);
			}
			insuredList.add(row);
		}

		return insuredList;
	}

	/**
     * <p>
     * Description : 依保單險種設定保險成本(投資型險種才需計算)
     * </p>
     * <p>
     * Created By : simon.huang
     * </p>
     * <p>
     * Create Time : Nov 1, 2016
     * </p>
     * 
	 * @param policyInfo
	 * @param in
	 * @param coverageList
	 */
	public void setupCOI(Map<String, Object> in, List<Map<String, Object>> coverageList) {

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		for (Map<String, Object> row : coverageList) {
			Long itemId = (Long) row.get("itemId");
			Long underwriteId = NumericUtils.parseLong(in.get("underwriteId"));
			String isCOIProduct = (String) row.get("isCOIProduct");
			if (CodeCst.YES_NO__YES.equals(isCOIProduct)) {
				//投資型險種，需設定保單成本
				Map<String, BigDecimal> coiMap = new HashMap<String, BigDecimal>();
				try {
					String reqFrom = (String) in.get("reqFrom");
					if (isPOS(reqFrom) || reqFrom.equals("csuw")) {

						Long policyChgId = (long) 0;
						if(reqFrom.equals("csuw")) {
							String a = CsUw(itemId,underwriteId);
							String policyChgnDate[] = a.split("\\|");
							policyChgId =  NumericUtils.parseLong(policyChgnDate[0]);
						} else {
							policyChgId = NumericUtils.parseLong(in.get("policyChgId"));
						}
						AlterationItemVO alterationItemVO = alterationItemService.getAlterationItem(policyChgId);
						Integer serviceId = alterationItemVO.getServiceId();
						
			// 投資型 在計算死亡率因位due date 在未來會無法取到，因此改用 min charge due
			// date
						// IR #322328  dueDate、validateDate 皆改用 下次周月日
						Date dueDate = extraPremDao.getMinChargeDueDate(itemId);
						coiMap = CoverageSP.getExtraPremCOI(itemId, dueDate, dueDate, policyChgId, serviceId);
					} else {
						// SRC 加費查詢
						coiMap = CoverageSP.getExtraPremCOI(itemId);
					}
				} catch (Exception e) {
					coiMap.put("stdPremCOI", BigDecimal.ZERO); //標準保費/保險成本
					coiMap.put("discntedPremCOI", BigDecimal.ZERO); //應收保險費/保險成本 
					row.put("errorMsg", ExceptionInfoUtils.getExceptionMsg(e));
				}
		/*
		 * <註:標準保費是指高保額折讓後的保費，應收保險費是指其他折讓後的保費> coiMap.put("stdPremCOI",
		 * stdPremCOI); //標準保費/保險成本 coiMap.put("discntedPremCOI",
		 * discntedPremCOI); //應收保險費/保險成本
				*/
				row.putAll(coiMap);

			}
		}
	}

	/**
     * <p>
     * Description : 依被豁免險險種 標準保費(年)+弱加+職加 ，重新設定豁免險保額，重新計算豁免險保額異動後的保費
     * </p>
     * <p>
     * Created By : simon.huang
     * </p>
     * <p>
     * Create Time : Jan 15, 2017
     * </p>
     * 
	 * @param uwCoverageVO
	 * @param waiverUwCoverageVO
	 * @param uwCoverageExtraPrems
	 * @return
	 * @throws GenericException
	 *  @see {@link UwDecisionSaveAction#reCalExtraLoading()}
	 */
    public Map<String, BigDecimal> reCalWaiverPrem(UwProductVO uwCoverageVO, UwProductVO waiverUwCoverageVO, Map<String, UwExtraLoadingVO> uwCoverageExtraPrems) throws GenericException {

		Map<String, BigDecimal> result = new HashMap<String, BigDecimal>();

		BigDecimal totalPremAn = uwCoverageVO.getDiscntedPremAn();
		for (UwExtraLoadingVO uwExtraPrem : uwCoverageExtraPrems.values()) {
			if (uwExtraPrem.getExtraPrem() != null) {
				totalPremAn = totalPremAn.add(totalPremAn);
			}
		}

		waiverUwCoverageVO.setReducedAmount(totalPremAn);
		UserTransaction trans = null;
		try {
			trans = Trans.getUserTransaction();
			trans.begin();

	    QueryUwDataSp.CalcMain(waiverUwCoverageVO.getItemId(), waiverUwCoverageVO.getValidateDate(), waiverUwCoverageVO.getDerivation(), false, waiverUwCoverageVO.getReducedAmount());

			waiverUwCoverageVO = uwPolicyService.findUwProduct(waiverUwCoverageVO.getUwListId());

			result.put("stdPremAf", waiverUwCoverageVO.getStdPremAf());
			result.put("siscntedPremAf", waiverUwCoverageVO.getDiscntedPremAf());

			//TransUtils.rollback(trans);
			trans.commit();
		} catch (Exception e) {
			TransUtils.rollback(trans);
			throw ExceptionFactory.parse(e);
		}

		return result;
	}

    private FmtUnb0441ExtensionVO getFmtUnb0441ExtensionVO(PolicyVO policyVO, Collection<UwExtraLoadingVO> allExtra) {

		Long templateId = Template.TEMPLATE_20024.getTemplateId();
		Long policyId = policyVO.getPolicyId();

		FmtUnb0441ExtensionVO extensionVO = new FmtUnb0441ExtensionVO();

		nbNotificationHelper.initNBLetterExtensionVO(policyId, extensionVO);
		nbNotificationHelper.setLetterContractorAtUW(extensionVO, policyId);

		extensionVO.setNoticeDate(AppContext.getCurrentUserLocalTime());
		extensionVO.setTemplateId(templateId);

		extensionVO.setBarcode1(CodeCst.BARCODE1_FMT_UNB_0441);
		extensionVO.setBarcode2(policyVO.getPolicyNumber());

		extensionVO.setHotline(nbNotificationHelper.getHotline());

		// 2020/04/28 Charlotte Wang
		// PCR 304073 原住民姓名羅馬拼音調整為：
		//1.7	疾病原因	sickReasonStr <被保險人姓名一>因<疾病名稱>、<被保險人姓名二>因<疾病名稱>
		List<String> insuredReason = new ArrayList<String>();
		for(InsuredVO insuredVO : policyVO.gitSortInsuredList()){
			//RTC-222998-豁免險被保險人需顯示在加費單中
			//if(insuredVO.isWaiverInsured() == false){
				Long partyId = insuredVO.getPartyId();
				
				@SuppressWarnings("unchecked")
	    Collection<UwExtraLoadingVO> insuredExtraList = CollectionUtils.select(allExtra, new BeanPredicate("insuredId", PredicateUtils.equalPredicate(partyId)));

				List<String> reason = new ArrayList<String>();

				//弱體加費原因
				for (UwExtraLoadingVO vo : insuredExtraList) {
					if (CodeCst.EXTRA_TYPE__HEALTH_EXTRA.equals(vo.getExtraType())) {
						reason.add(vo.getReason());
						break;
					}
				}
				//職業業加 費原因
				for (UwExtraLoadingVO vo : insuredExtraList) {
					if (CodeCst.EXTRA_TYPE__OCCUPATION_EXTRA.equals(vo.getExtraType())) {
						reason.add(vo.getReason());
						break;
					}
				}
				
			if (reason.size() > 0) {
				if (org.apache.commons.lang3.StringUtils.isNotBlank(insuredVO.getRomanName())) {
					insuredReason.add(insuredVO.getName() + insuredVO.getRomanName() + '\u56e0' // 因
									+ org.apache.commons.lang3.StringUtils.join(reason, '\u53ca') // 及
					);
				} else {
					insuredReason.add(insuredVO.getName() + '\u56e0' // 因
									+ org.apache.commons.lang3.StringUtils.join(reason, '\u53ca') //及
					);
				}
			}
		}
	
		if(insuredReason.size() > 0){
	    extensionVO.setSickReasonStr(org.apache.commons.lang3.StringUtils.join(insuredReason, '\u3001') // 、
			);
		}
		
		//1.11	幣別單位	currency 幣別單位
		//RTC-190056-幣別  臺幣需帶新臺幣
		String moneyCode = "";
		String moneyId = String.valueOf(policyVO.getCurrency());
		if("8".equals(moneyId)){
			moneyCode = StringResource.getStringData("COD_T_MONEY_SQL_8", CodeCst.LANGUAGE__CHINESE_TRAD);			
		}else{
			moneyCode = StringResource.getStringData("COD_T_MONEY_" + moneyId, CodeCst.LANGUAGE__CHINESE_TRAD);
		}
		extensionVO.setCurrency(moneyCode);

		//加費資料
		Integer shortExtraPolicyYear = null;
		Integer longExtraPolicyYear = null;
		for(UwExtraLoadingVO healthExtra :  allExtra) {
	
			if (CodeCst.EXTRA_TYPE__HEALTH_EXTRA.equals(healthExtra.getExtraType())) {
				String extraPeriodType = healthExtra.getExtraPeriodType();
				//1.27    短期加費保單年度    shortExtraPolicyYear;
				if ("2".equals(extraPeriodType)) {
					if (healthExtra.getDuration() != null) {
			if (shortExtraPolicyYear == null || shortExtraPolicyYear.intValue() < healthExtra.getDuration().intValue()) {
							shortExtraPolicyYear = 	healthExtra.getDuration();
						}
					}
				}
				//1.28    長期加費備註  noticeLongExtra
				if ("1".equals(extraPeriodType)) {
					extensionVO.setNoticeLongExtra(CodeCst.YES_NO__YES);

					if (healthExtra.getDuration() != null) {
						//RCR 156268-1.32 長期加費備註(加費年期)
			if (longExtraPolicyYear == null || longExtraPolicyYear.intValue() < healthExtra.getDuration().intValue()) {
							longExtraPolicyYear = healthExtra.getDuration();
						}
					}
				}
			}
		}

		if(shortExtraPolicyYear != null) {
			extensionVO.setShortExtraPolicyYear(String.valueOf(shortExtraPolicyYear));
		}
			
		if(longExtraPolicyYear != null) {
			//(第N保單週年N動態顯示，加費頁面欄位「可調整加費年度」=99時N顯示為5;加費頁面橡位「可調整加費年度」，≠99顯顯示輸入的值)
			if(longExtraPolicyYear.intValue() >= 99){
				longExtraPolicyYear = 5 ;
			}
			extensionVO.setLongExtraPolicyYear(String.valueOf(longExtraPolicyYear));
		}

		/* Develop 246653  主約 - 實物給付計劃欄位 (PCR 243931)*/ 
	// String servicesBenefitLevel =
	// policyVO.gitMasterCoverage().getServicesBenefitLevel();
		// PCR320621  主附約都要判斷
		for (CoverageVO coverage : policyVO.getCoverages()) {
			Long productVersionId = coverage.getProductVersionId();
			String servicesBenefitLevel = coverage.getServicesBenefitLevel();
			if(productVersionId != null && !StringUtils.isNullOrEmpty(servicesBenefitLevel)) {
				LifeProduct product = lifeProductService.getProductByVersionId(productVersionId);
		if (product != null && !StringUtils.isNullOrEmpty(product.getProduct().getTermsSaShow()) && "1".equals(product.getProduct().getTermsSaShow())) {
					String strId = "COD_T_BENEFIT_LEVEL_SQL_" + servicesBenefitLevel;
					String servicesBenefitLevelDesc = StringResource.getStringData(strId, CodeCst.LANGUAGE__CHINESE_TRAD);
					extensionVO.setServicesBenefitLevel(servicesBenefitLevel);
					extensionVO.setServicesBenefitLevelDesc(servicesBenefitLevelDesc);
				}
			}
		}
		
		return extensionVO;

	}
	
	private BigDecimal findDiscntRate(Long item_id, String discountType) {

		StringBuffer sql = new StringBuffer();
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		DBean db = null;
		BigDecimal discntRate = BigDecimal.ZERO;
		try {
			db = new DBean();
			db.connect();
			conn = db.getConnection();

			sql.append("select t.discnt_rate from t_Prem_Discnt t where t.item_id=" + item_id + " and t.discount_type='" + discountType + "' ");

			pst = conn.prepareStatement(sql.toString());

			rs = pst.executeQuery();
			if (rs.next()) {
				discntRate = rs.getBigDecimal("DISCNT_RATE");
			}
		} catch (Exception e) {
			throw ExceptionFactory.parse(e);
		} finally {
			DBean.closeAll(rs, pst, db);
		}

		return discntRate;
	}

	private FmtUnb0381ExtensionVO getFmtUnb0381ExtensionVO(Long policyId, Long insuredId) {

		Long templateId = Template.TEMPLATE_20023.getTemplateId();

		PolicyVO policyVO = policyService.load(policyId);
		FmtUnb0381ExtensionVO extensionVO = new FmtUnb0381ExtensionVO();
		nbNotificationHelper.initNBLetterExtensionVO(policyId, extensionVO);

		extensionVO.setNoticeDate(AppContext.getCurrentUserLocalTime());
		extensionVO.setTemplateId(templateId);

		nbNotificationHelper.setLetterContractorAtUW(extensionVO, policyId);

		extensionVO.setBarcode1(CodeCst.BARCODE1_FMT_UNB_0381);
		extensionVO.setBarcode2(policyVO.getPolicyNumber());
		extensionVO.setHotline(nbNotificationHelper.getHotline());
		extensionVO.setInsuredId(String.valueOf(insuredId));
		InsuredVO insuredVO = policyVO.gitInsured(insuredId);
		if (insuredVO != null) {
			extensionVO.setCertiCode(insuredVO.getCertiCode());
			extensionVO.setInsuredName(insuredVO.getName());
			extensionVO.setInsuredRomanName(insuredVO.getRomanName());
		}

		return extensionVO;

	}

	/**
     * <p>
     * Description : 取得批註
     * </p>
     * <p>
     * Created By : Victor Chang
     * </p>
     * <p>
     * Create Time : Oct 23, 2016
     * </p>
     * 
	* @param list
	* @return
	*/
	private List<WSItemString> getWSItemStringAll(List<String> list) {

		List<WSItemString> item = new ArrayList<WSItemString>();
		if (list != null) {
			for (String content : list) {
				item.add(new WSItemString(null, content));
			}
		}
		return item;

	}

	/**
     * <p>
     * Description : 執行加費金額計算
     * </p>
     * <p>
     * Created By : simon.huang
     * </p>
     * <p>
     * Create Time : Aug 5, 2016
     * </p>
     * 
	 * @param uwExtraLoadingVO
	 * @param uwProductVO
	 */
	public void calcExtraPrem(UwExtraLoadingVO uwExtraLoadingVO, UwProductVO uwProductVO, Map<String, Object> in) {
		
		BigDecimal extraPrem = BigDecimal.ZERO;
		BigDecimal extraPremAn = BigDecimal.ZERO;
		// PCR 156226
		String reqFrom = (String) in.get("reqFrom");
		boolean isPOS = isPOS(reqFrom);
		boolean ilp = this.isILP(uwProductVO.getItemId());
		String nextOrThis = "N";
		
		Date dueDate = uwExtraLoadingVO.getStartDate();
		
		if(isPOS) {
			CoverageVO coverageVO = coverageService.load(uwProductVO.getItemId());
			
			dueDate = coverageVO.getCoverageExtend().getDueDate();
			nextOrThis = "Y";
			String extraArith = CodeCst.ADD_ARITH__EXTRA_TYPE_A; //加費選項(計算方式)
			String extraTypeHealth = CodeCst.EXTRA_TYPE__HEALTH_EXTRA;
			String extraTypeJob = CodeCst.EXTRA_TYPE__OCCUPATION_EXTRA;
			Integer type = 0;
			Integer jobPara = 0;
			Map<String, Object> submitData = (Map<String, Object>) in.get("submitData");
			Map<String, Object> typeAMap = (Map<String, Object>) submitData.get(extraTypeHealth); //弱加相關資訊
			Map<String, Object> typeBMap = (Map<String, Object>) submitData.get(extraTypeJob); //職加相關資訊
			List<Map<String, Object>> extraPremList = (List<Map<String, Object>>) typeAMap.get("extraPremList");
			if(!typeBMap.isEmpty()) {
				String test = (String) typeBMap.get("jobextra");
				if(!test.isEmpty()) {
					jobPara = Integer.valueOf(test);
				}				
			}	
			if(!typeAMap.isEmpty()) {
				String test = (String) extraPremList.get(0).get("test");
				if(test != null && !test.isEmpty()) {
					type = Integer.valueOf((String) extraPremList.get(0).get("test"));
				}
			}		
			if (CodeCst.EXTRA_TYPE__HEALTH_EXTRA.equals(uwExtraLoadingVO.getExtraType()) && type >0) {
				if(ilp) {
					uwExtraLoadingVO.setExtraArith(CodeCst.ADD_ARITH__EXTRA_TYPE_B_ILP_PD);
					uwExtraLoadingVO.setExtraPara(BigDecimal.valueOf(type));
				}else {
					uwExtraLoadingVO.setExtraArith(CodeCst.ADD_ARITH__EXTRA_TYPE_B);
					uwExtraLoadingVO.setExtraPara(BigDecimal.valueOf(type));
				}
			}
			if (CodeCst.EXTRA_TYPE__OCCUPATION_EXTRA.equals(uwExtraLoadingVO.getExtraType()) && jobPara >0) {
				if(ilp) {
					uwExtraLoadingVO.setExtraPara(BigDecimal.valueOf(jobPara));
				}else {
					uwExtraLoadingVO.setExtraPara(BigDecimal.valueOf(jobPara));	
				}
			}
		}

		if (CodeCst.ADD_ARITH__EXTRA_TYPE_MANUAL.equals(uwExtraLoadingVO.getExtraArith())) {
			//重新計算金額
	    List<BigDecimal> calcResult = calculatorCI.getTryExtraPremAfAn(uwExtraLoadingVO.getItemId(), uwProductVO.getAmount(), // 險種保額
		    uwExtraLoadingVO.getInsuredId(), uwExtraLoadingVO.getStartDate(), // 生效日
					"N", //nextOrThis
		    uwExtraLoadingVO.getExtraType(), uwExtraLoadingVO.getExtraArith(), uwExtraLoadingVO.getExtraPrem(), // 傳入加費金額，由繳別系數計算後回傳年化加費金額
		    new BigDecimal(uwExtraLoadingVO.getEmValue())); // 弱體加費的em
								    // value
			extraPrem = (BigDecimal) calcResult.get(0);
			extraPremAn = (BigDecimal) calcResult.get(1);
		} else {
			// 加費比率大於0才作計算
			if (uwExtraLoadingVO.getExtraPara() != null && uwExtraLoadingVO.getExtraPara().floatValue() > 0) {				
				
				if (ilp && org.apache.commons.lang3.StringUtils.equals(reqFrom, "pos")) {
					// 當來源是保全且是投資型商品
					Date minChargeDueDate = extraPremDao.getMinChargeDueDate(uwProductVO.getItemId());
		    List<BigDecimal> calcResult = calculatorCI.getTryExtraPremAfAn(uwExtraLoadingVO.getItemId(), uwProductVO.getAmount(), // 險種保額
							uwExtraLoadingVO.getInsuredId(),
							// #322328 保全投資型商品dueDate、validatedate 一率使用 下次周月日
							minChargeDueDate,//dueDate
							minChargeDueDate,//validatedate
							"N", //nextOrThis
			    uwExtraLoadingVO.getExtraType(), uwExtraLoadingVO.getExtraArith(), uwExtraLoadingVO.getExtraPara(), // 加費比例
																// 或每萬元加費金額
			    new BigDecimal(uwExtraLoadingVO.getEmValue())); // 弱體加費的em
									    // value
					extraPrem = (BigDecimal) calcResult.get(0);
					extraPremAn = (BigDecimal) calcResult.get(1);
				} else {

					//重新計算金額
		    List<BigDecimal> calcResult = calculatorCI.getTryExtraPremAfAn(uwExtraLoadingVO.getItemId(), uwProductVO.getAmount(), // 險種保額
		    			    uwExtraLoadingVO.getInsuredId(), dueDate, // 生效日
		    			    nextOrThis, //nextOrThis IR#441717 用下期去算
			    uwExtraLoadingVO.getExtraType(), uwExtraLoadingVO.getExtraArith(), uwExtraLoadingVO.getExtraPara(), // 加費比例
																// 或每萬元加費金額
			    new BigDecimal(uwExtraLoadingVO.getEmValue())); // 弱體加費的em
									    // value
					extraPrem = (BigDecimal) calcResult.get(0);
					extraPremAn = (BigDecimal) calcResult.get(1);
				}
			}
		}

		logger.warn(getClass()+": extraPrem=" +extraPrem + ",extraPremAn=" + extraPremAn);
		uwExtraLoadingVO.setExtraPrem(extraPrem);
		uwExtraLoadingVO.setExtraPremAn(extraPremAn);

	}

	/**
     * <p>
     * Description : 核保作業中刪除保項，判斷核保作業中與保項有關連的資料並提示錯誤訊息
     * </p>
     * <p>
     * Created By : simon.huang
     * </p>
     * <p>
     * Create Time : Jun 28, 2017
     * </p>
     * 
	 * @param policyId
	 * @param underwriteId
	 * @param itemId
	 * @return
	 */
	public String checkUwDeleteProduct(Long policyId, Long underwriteId, Long itemId) {

		List<String> msgData = new ArrayList<String>();
		int count = uwPolicyService.getUWExclusionCountByItemId(itemId,underwriteId);
		if (count > 0) {
			msgData.add(NBUtils.getTWMsg(MsgCst.MSG_209066));
		}
		count = uwPolicyService.getUWExtraPremCountByItemId(itemId,underwriteId);
		if (count > 0) {
			msgData.add(NBUtils.getTWMsg(MsgCst.MSG_114604));
		}

		//核保作業中有關連險種的資料，不可刪除，並提示訊息
		if (msgData.isEmpty() == false) {

			Map<String, String> msgParams = new HashMap<String, String>();
	    msgParams.put("0", org.apache.commons.lang.StringUtils.join(msgData.toArray(), "、 "));

			String msg = StringResource.getStringData(MsgCst.MSG_1261274,

					CodeCst.LANGUAGE__CHINESE_TRAD, msgParams);

			return msg;
		}
		
		//2018/02/04 by Simon,RTC-222977-取消再保會辦保項的檢核，刪除時一併刪除
//
	// List<NbRiResultVO> riResultList =
	// uwRiApplyService.findNBRiResultByItem(itemId);
//		if (riResultList.size() > 0) {
//			CoverageVO coverageVO = coverageService.load(itemId);
	// com.ebao.ls.pa.pub.ref.vo.prd.ProductVO productVO =
	// productService.getProductByVersionId(
//					coverageVO.getProductId().longValue(),
//					coverageVO.getProductVersionId());
//			Map<String, String> msgParams = new HashMap<String, String>();
//			msgParams.put("0", productVO.getInternalId());
//
//			//{0}已再保會辦，無法異動。
//			String msg = StringResource.getStringData(MsgCst.MSG_1262470,
//					CodeCst.LANGUAGE__CHINESE_TRAD, msgParams);
//			return msg;
//		}
//	

		return null;

	}

	/**
     * <p>
     * Description : 核保作業中新增險種需,判斷是加費單照會是否已發放,需提示訊息
     * </p>
     * <p>
     * Created By : simon.huang
     * </p>
     * <p>
     * Create Time : Jul 2, 2017
     * </p>
     * 
	 * @param policyId
	 * @param underwriteId
	 * @return
	 */
	public String checkUwExtraPremDocument(Long policyId, Long underwriteId) {

		Collection<UwExtraLoadingVO> uwExtraLoadingList = uwPolicyService.findUwExtraLoadingEntitis(underwriteId);
		if (uwExtraLoadingList.size() > 0) {
			for (UwExtraLoadingVO uwExtraPremVO : uwExtraLoadingList) {
				Long msgId = uwExtraPremVO.getMsgId();
				if (msgId != null) {
					String documentStatus = uwIssuesLetterService.getMasterDocStatus(msgId);
					if (StringUtils.isNullOrEmpty(documentStatus) == false) {
			if (!CodeCst.DOCUMENT_STATUS__ABORTED.equals(documentStatus) && !CodeCst.DOCUMENT_STATUS__DELETED.equals(documentStatus)) {

							return NBUtils.getTWMsg(MsgCst.MSG_1262523);
						}
					}
				}

				//只處理第一筆
				break;
			}

		}

		return null;
	}

	/**
     * <p>
     * Description : 核保作業中有關連被保險人的資料，不可刪除
	 * </p>
     * <p>
     * Created By : simon.huang
     * </p>
     * <p>
     * Create Time : Apr 14, 2017
     * </p>
     * 
	 * @param underwriteId
	 * @param orgPartyId
	 * @return
	 */
    public String checkUwDeleteInsured(Long policyId, Long underwriteId, Long orgPartyId, Long insuredId) {

		//被保險人若有發放體檢照會事項則不可刪除
		List<UwMedicalLetterVO> letterList = uwMedicalLetterService.queryAllMedicalLetter(underwriteId, insuredId);
		//取出狀態不為0(暫存)的總數
	CollectionUtils.filter(letterList, new BeanPredicate("status", PredicateUtils.notPredicate(PredicateUtils.equalPredicate("0"))));
		for (UwMedicalLetterVO letter : letterList) {
			if (letter.getDocumentId() != null) {
				String documentStatus = uwIssuesLetterService.getDocumentStatus(letter.getDocumentId());
				if (StringUtils.isNullOrEmpty(documentStatus) == false) {
		    if (!CodeCst.DOCUMENT_STATUS__ABORTED.equals(documentStatus) && !CodeCst.DOCUMENT_STATUS__DELETED.equals(documentStatus)) {
						InsuredVO insuredVO = insuredService.load(insuredId);
						Map<String, String> msgParams = new HashMap<String, String>();
						msgParams.put("0", insuredVO.getName());
						//{0}已發放體檢照會，無法異動。
			String msg = StringResource.getStringData(MsgCst.MSG_1262383, CodeCst.LANGUAGE__CHINESE_TRAD, msgParams);
						return msg;
					}
				}
			}
		}

		List<UwSickFormLetterVO> sickLetterList = uwSickFormLetterService.findByPolicyInsured(underwriteId, insuredId, null);

		for (UwSickFormLetterVO letter : sickLetterList) {

			if (letter.getDocumentId() != null) {

				String documentStatus = uwIssuesLetterService.getDocumentStatus(letter.getDocumentId());

				if (StringUtils.isNullOrEmpty(documentStatus) == false) {
		    if (!CodeCst.DOCUMENT_STATUS__ABORTED.equals(documentStatus) && !CodeCst.DOCUMENT_STATUS__DELETED.equals(documentStatus)) {

						InsuredVO insuredVO = insuredService.load(insuredId);
						Map<String, String> msgParams = new HashMap<String, String>();
						msgParams.put("0", insuredVO.getName());
						//{0}已發放疾病問卷照會，無法異動。
			String msg = StringResource.getStringData(MsgCst.MSG_1262469, CodeCst.LANGUAGE__CHINESE_TRAD, msgParams);
						return msg;
					}
				}
			}
		}
		//2018/02/04 by Simon,RTC-222977-取消再保會辦保項的檢核，刪除時一併刪除
	// List<NbRiResultVO> riResultList =
	// uwRiApplyService.findNBRiResultByInsured(insuredId);
//		if (riResultList.size() > 0) {
//			InsuredVO insuredVO = insuredService.load(insuredId);
//			Map<String, String> msgParams = new HashMap<String, String>();
//			msgParams.put("0", insuredVO.getName());
//			//{0}已再保會辦，無法異動。
//
//			String msg = StringResource.getStringData(MsgCst.MSG_1262470,
//					CodeCst.LANGUAGE__CHINESE_TRAD, msgParams);
//			return msg;
//		}

		return null;
	}

	/**
     * <p>
     * Description : 核保作業中有關連被保險人的資料，不可異動，並提示訊息： 「被保險人異動，請先刪除該被保險人核保作業中的{0}資料。 」
     * </p>
     * <p>
     * Created By : simon.huang
     * </p>
     * <p>
     * Create Time : Apr 14, 2017
     * </p>
     * 
	 * @param underwriteId
	 * @param orgPartyId
	 * @return
	 */
    public String checkUwChangeInsured(Long policyId, Long underwriteId, Long orgPartyId, Long insuredId) {

		List<String> msgData = new ArrayList<String>();
		int count = uwPolicyService.getUWExclusionCountByInsuredPartyId(underwriteId, orgPartyId);
		if (count > 0) {
			msgData.add(NBUtils.getTWMsg(MsgCst.MSG_209066));
		}
		count = uwPolicyService.getUWExtraPremCountByInsuredPartyId(underwriteId, orgPartyId);
		if (count > 0) {
			msgData.add(NBUtils.getTWMsg(MsgCst.MSG_114604));
		}
		//核保作業中有關連被保險人的資料，不可異動，並提示訊息
		if (msgData.isEmpty() == false) {
			Map<String, String> msgParams = new HashMap<String, String>();
	    msgParams.put("0", org.apache.commons.lang.StringUtils.join(msgData.toArray(), "、 "));

			//被保險人異動，請先刪除該被保險人核保作業中的{0}資料。
	    String msg = StringResource.getStringData(MsgCst.MSG_1261653, CodeCst.LANGUAGE__CHINESE_TRAD, msgParams);

			return msg;
		}
		//2018/02/04 by Simon,RTC-222977-取消再保會辦保項的檢核，刪除時一併刪除
	// List<NbRiResultVO> riResultList =
	// uwRiApplyService.findNBRiResultByInsured(insuredId);
//		if (riResultList.size() > 0) {
//			InsuredVO insuredVO = insuredService.load(insuredId);
//			Map<String, String> msgParams = new HashMap<String, String>();
//			msgParams.put("0", insuredVO.getName());
//			//{0}已再保會辦，無法異動。
//
//			String msg = StringResource.getStringData(MsgCst.MSG_1262470,
//					CodeCst.LANGUAGE__CHINESE_TRAD, msgParams);
//			return msg;
//		}

	// return this.checkUwDeleteInsured(policyId, underwriteId, orgPartyId,
	// insuredId);
		return null;
	}

	/**
     * <p>
     * Description : 核保作業中有被保險人列表的功能增加判斷是否有險種，無險種disabled被保險人checkbox不可勾選
     * </p>
     * <p>
     * Created By : simon.huang
     * </p>
     * <p>
     * Create Time : Jul 3, 2017
     * </p>
     * 
	 * @param request
	 * @param policyVO
	 */
	public static void countInsuredCoverage(HttpServletRequest request, PolicyVO policyVO) {
		Map<Long, Integer> insuredBindCoverageCount = new HashMap<Long, Integer>();
		for (InsuredVO insuredVO : policyVO.gitSortInsuredList()) {
			Long insuredId = insuredVO.getListId();
			List<CoverageVO> coverages = policyVO.gitCoveragesByInsured(insuredId);
			if (coverages == null) {
				insuredBindCoverageCount.put(insuredId, 0);
			} else {
				insuredBindCoverageCount.put(insuredId, coverages.size());
			}
		}
		request.setAttribute("insuredBindCoverageCount", insuredBindCoverageCount);
	}
	
	/** 
     * <p>
     * Description : 標準保費POS使用 年化費率的費率表
     * 
	 * @param productId
	 * @param chargetype
	 * @param modeltype
	 * @param paymode
	 * @return
	 */
	private float pmodelFactor(float productId, String chargetype, float modeltype, float paymode) {
		float chargerate = 1;
		DBean db = new DBean();
		CallableStatement stmt = null;
		Connection conn = null;
		try {
			db.connect();
			conn = db.getConnection();
			stmt = conn.prepareCall("{ call pkg_ls_prd_rt_ci.p_get_model_factor(?,?,?,?,?)}");
			stmt.setFloat(1, productId);
			stmt.setString(2, chargetype);
			stmt.setInt(3, (int) modeltype);
			stmt.setInt(4, (int) paymode);
			stmt.registerOutParameter(5, Types.FLOAT);
			stmt.execute();

			chargerate = stmt.getFloat(5);
		} catch (Exception e) {
			throw ExceptionFactory.parse(e);
		} finally {
			DBean.closeAll(null, stmt, db);
		}
		return chargerate;
	}
	
	/*
	 * For COI setup Query out
	 * 
	 * @param itemId
	 * 
	 * @param underwriteId
	 */
	private String CsUw(Long itemId, Long underwriteId) {
		DBean dbean = new DBean();
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String chgIdnDate = "";
		try {
			dbean.connect();
			conn = dbean.getConnection();
			StringBuffer sql = new StringBuffer(1024);
			sql.append("select                              ");
			sql.append("pc.policy_chg_id, ce.due_date       ");
			sql.append("from                                ");
			sql.append("t_policy_change pc, t_uw_policy up, ");
			sql.append("t_contract_extend ce  where         ");
			sql.append("pc.service_id = up.cs_transaction   ");
			sql.append("and pc.master_chg_id = up.change_id ");
			sql.append("and up.underwrite_id = ?            ");
			sql.append("and ce.item_id = ?                  ");
			ps = conn.prepareStatement(sql.toString());

			int paraIdx = 0;
			if (underwriteId != null) {
				ps.setString(paraIdx + 1, underwriteId.toString());
				paraIdx++;
			}
			if (itemId != null) {
				ps.setString(paraIdx + 1, itemId.toString());
				paraIdx++;
			}
			rs = ps.executeQuery();
			String strI = "";
			Date due = new Date();
			while (rs.next()) {
				strI = rs.getBigDecimal("policy_chg_id").toString();
				due = rs.getDate("due_date");
			}
			chgIdnDate = strI + "|" + due;
		} catch (Exception e) {
			throw ExceptionFactory.parse(e);
		} finally {
			DBean.closeAll(rs, ps, dbean);
		}
		return chgIdnDate;
	}

	/**
	 * Get product sa unit amount value which defined in rate table.
	 * 
	 * @param productId
	 * @return
	 * @throws GenericException
	 */
    public Long getProductSaUnitAmount(Integer productId) throws GenericException {
		Long saUnitAmount = null;
		Map<String, Object> condition = new HashMap<String, Object>();
		condition.put("product_id", productId);
		try {
			Map<String, Object> result = ratetableService.lookupRatetable(RatetableCst.RT_CODE__PRODUCT_UNIT_RATE, condition);

			saUnitAmount = (Long) result.get("SA_Unit_Amount");
		} catch (Exception e) {
			// do nothing
			Log.debug(getClass(), e);
		}
		return saUnitAmount;
	}
	
	/**
	 * Get product em unit amount value which defined in rate table.
	 * BC374 改以每元之保額輸入職加金額，取EM_UNIT_AMOUNT 計算職加
	 * @param productId
	 * @return
	 * @throws GenericException
	 */
	public Long getProductEmUnitAmount(Integer productId)
			throws GenericException {
		Long emUnitAmount = null;
		Map<String, Object> condition = new HashMap<String, Object>();
		condition.put("product_id", productId);
		try {
			Map<String, Object> result = ratetableService.lookupRatetable(RatetableCst.RT_CODE__PRODUCT_UNIT_RATE, condition);

			emUnitAmount = (Long) result.get("EM_Unit_Amount");
		} catch (Exception e) {
			// do nothing
			Log.debug(getClass(), e);
		}
		return emUnitAmount;
	}
	
    //BC353 PCR-263273 2018/10/15 Add by Kathy
    public Map<String, String> getTFundData(Long policyId) {
        return coverageService.getTFundData(policyId);
    }
    
	/**
	 * Check Coverage is ILP Product?
	 * 
	 * @param itemId
	 * @return
	 */
	public boolean isILP(Long itemId) {
		return coverageService.isILPBenefit(itemId);
	}
    
}
