package com.ebao.ls.uw.ds;

import com.ebao.ls.foundation.service.GenericServiceImpl;
import com.ebao.ls.pa.pub.vo.VOGenerator;
import static com.ebao.ls.pub.cst.YesNo.YES_NO__NO;
import static com.ebao.ls.pub.cst.YesNo.YES_NO__YES;
import com.ebao.ls.uw.ci.UwPolicyCI;
import com.ebao.ls.uw.data.UwElderCareDao;
import com.ebao.ls.uw.data.bo.UwElderCare;
import com.ebao.ls.uw.vo.UwElderCareDtlVO;
import com.ebao.ls.uw.vo.UwElderCareTypeVO;
import com.ebao.ls.uw.vo.UwElderCareVO;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;
import java.util.stream.LongStream;
import javax.annotation.Resource;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.SQLQuery;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

public class UwElderCareServiceImpl extends GenericServiceImpl<UwElderCareVO, UwElderCare, UwElderCareDao<UwElderCare>> implements UwElderCareService {

    protected final Logger logger = LogManager.getFormatterLogger();

    @Resource(name = VOGenerator.BEAN_DEFAULT)
    private VOGenerator voGenerator;

    @Resource(name = UwElderCareDtlService.BEAN_DEFAULT)
    private UwElderCareDtlService uwElderCareDtlService;

    @Resource(name = UwElderCareTypeService.BEAN_DEFAULT)
    private UwElderCareTypeService uwElderCareTypeService;

    @Resource(name = UwPolicyCI.BEAN_DEFAULT)
    private UwPolicyCI uwPolicyCI;

    @Override
    protected UwElderCareVO newEntityVO() {
        return voGenerator.uwElderCareVONewInstance();
    }

    @Override
    public List<UwElderCareVO> findByCriteriaWithOrder(List<Order> orders, Criterion... criterion) {
        return convertToVOList(dao.findByCriteriaWithOrder(orders, criterion));
    }

    @Override
    public List<UwElderCareVO> findByCriteria(Criterion... criterion) {
        return convertToVOList(dao.findByCriteria(criterion));
    }

    /**
     * 新增 高齡投保核保評估資訊<br>
     * 會根據 POLICY_ID, CERTI_CODE, LAST_CMT_FLG = 'Y' 查詢是否已經有過關懷<br>
     * 並查詢保單當下的 UNDERWRITE_ID
     *
     * @param uwElderCareVO 高齡投保核保評估（需先塞好 CARE_TYPE）
     * @return true：成功, false：失敗
     */
    @Override
    public boolean writeUwElderCare(UwElderCareVO uwElderCareVO) {
        boolean tof = false;
        if (uwElderCareVO != null) {
            // 查詢保單當下的 UNDERWRITE_ID
            if (uwElderCareVO.getUnderwriteId() == null) {
                Long underwriteId = uwPolicyCI.getUNBUnderwriteId(uwElderCareVO.getPolicyId());
                uwElderCareVO.setUnderwriteId(underwriteId);
            }

            UwElderCareVO sureVO = findByCriteria(
                    Restrictions.eq("underwriteId", uwElderCareVO.getUnderwriteId()),
                    Restrictions.eq("policyId", uwElderCareVO.getPolicyId()),
                    Restrictions.eq("certiCode", uwElderCareVO.getCertiCode())
            ).stream().findFirst().orElse(uwElderCareVO);

            // 根據 UNDERWRITE_ID, POLICY_ID, CERTI_CODE 更新所有 LAST_CMT_FLG = 'N' 為 LAST_CMT_FLG = 'Y'
            this.checkAndChangeLastUwElderCare(sureVO.getUnderwriteId(), sureVO.getPolicyId(), sureVO.getCertiCode());

            UwElderCareVO new_sureVO = save(sureVO);
            if (new_sureVO != null) {
                tof = true;
                writeDetailUwElderCare(new_sureVO.getListId(), uwElderCareVO.getUwElderCareTypeVOs());
            }
        }
        return tof;
    }

    @Override
    public Optional<UwElderCareTypeVO> getLastElderCareDataByIdAndCertiCode(Long policyId, Long underwriteId, String certiCode) {
        if (policyId != null && underwriteId != null && certiCode != null) {
            return findElderCareDataById(policyId, underwriteId).stream()
                    .filter(careVO -> StringUtils.equals(careVO.getCertiCode(), certiCode))
                    .flatMap(careVO -> careVO.getUwElderCareTypeVOs().stream())
                    .filter(careTypeVO -> StringUtils.equals(careTypeVO.getLastCmtFlg(), YES_NO__YES))
                    .findFirst();
        }

        return Optional.empty();
    }

    @Override
    public List<UwElderCareVO> findElderCareDataById(Long policyId, Long underwriteId) {
        return findByCriteriaWithOrder(Arrays.asList(Order.desc("listId")),
                Restrictions.eq("policyId", policyId),
                Restrictions.eq("underwriteId", underwriteId)).stream()
                .peek(careVO -> findElderCareDetailData(careVO, Collections.emptySet()))
                .collect(Collectors.toList());
    }

    @Override
    public int updateCareDataByListIds(Set<Long> dtlResetNListIds, String certiCodeSel, Map<String, Object> datas, Set<Long> careListIds, Set<Long> typeListIds, Set<Long> dtlListIds) {
        AtomicInteger rowCnt = new AtomicInteger(0);
        if (StringUtils.isNotBlank(certiCodeSel)) {
            // 將其他類型核保員評估清空
            if (!dtlResetNListIds.isEmpty()) {
                uwElderCareDtlService.findByCriteria(Restrictions.in("listId", dtlResetNListIds)).forEach(dtlVO -> {
                    // 核保員評估
                    dtlVO.setResultCode(YES_NO__NO);
                    uwElderCareDtlService.save(dtlVO);
                });
            }
            // 先重設預設值
            careListIds.forEach(careListId -> {
                rowCnt.addAndGet(updateCareDataToDefaultById(careListId));
            });
            typeListIds.forEach(typeListId -> {
                rowCnt.addAndGet(uwElderCareTypeService.updateCareDataToDefaultById(typeListId));
            });
            dtlListIds.forEach(dtlListId -> {
                rowCnt.addAndGet(uwElderCareDtlService.updateCareDataToDefaultById(dtlListId));
            });
            if (!careListIds.isEmpty()) {
                // 儲存表單
                findByCriteria(Restrictions.in("listId", careListIds)).forEach(careVO -> {
                    // 確認高齡投保評估量表問項皆完成(Y/N)
                    careVO.setAttchElderScale(MapUtils.getString(datas, String.format("attchElderScale_%s", certiCodeSel), YES_NO__NO));
                    // 評估說明
                    careVO.setScaleComment(MapUtils.getString(datas, String.format("scaleComment_%s", certiCodeSel), ""));
                    // 是否具有辨識能力評估結果(Y/N/W)
                    careVO.setScaleResult(MapUtils.getString(datas, String.format("scaleResult_%s", certiCodeSel), null));
                    save(careVO);

                });
            }
            typeListIds.forEach(typeListId -> {
                uwElderCareDtlService.findByCriteria(Restrictions.eq("careTypeListId", typeListId), Restrictions.in("listId", dtlListIds)).forEach(dtlVO -> {
                    // 核保員評估
                    dtlVO.setResultCode(MapUtils.getString(datas, String.format("resultCode_%s_%s_%s", certiCodeSel, dtlVO.getCareTypeListId(), dtlVO.getListId()), YES_NO__NO));
                    uwElderCareDtlService.save(dtlVO);
                });
            });
        }
        return rowCnt.get();
    }

    @Override
    public int updateCareDataToDefaultById(Long listId) {
        SQLQuery query = dao.getSession().createSQLQuery("UPDATE T_UW_ELDER_CARE SET ATTCH_ELDER_SCALE = NULL, SCALE_RESULT = NULL, SCALE_COMMENT = NULL WHERE LIST_ID = :LIST_ID");
        query.setParameter("LIST_ID", listId);
        return query.executeUpdate();
    }

    @Override
    public List<UwElderCareVO> copyElderCareDataById(Long old_underwriteId, Long new_underwriteId) {
        if (old_underwriteId != null && new_underwriteId != null) {
            AtomicReference<Long> policyId = new AtomicReference<>();
            Map<Long, Long> mapCareIds = new LinkedHashMap<>();
            Map<Long, Long> mapTypeIds = new LinkedHashMap<>();
            Map<Long, Long> mapDtlIds = new LinkedHashMap<>();
            Optional.ofNullable(uwPolicyCI.getUwPolicyByUwId(old_underwriteId)).ifPresent(old_uwPolicyCIVO -> {
                policyId.set(old_uwPolicyCIVO.getPolicyId());
                findElderCareDataById(policyId.get(), old_underwriteId).forEach(oldCareVO -> {
                    Long oldCareId = oldCareVO.getListId();
                    // 儲存主檔 - T_UW_ELDER_CARE
                    oldCareVO.setListId(null);
                    oldCareVO.setUnderwriteId(new_underwriteId);
                    UwElderCareVO newCareVO = save(oldCareVO);
                    mapCareIds.put(newCareVO.getListId(), oldCareId);
                    // 儲存明細 - T_UW_ELDER_CARE_TYPE
                    uwElderCareTypeService.findByCriteriaWithOrder(Arrays.asList(Order.asc("updateTimestamp")), Restrictions.eq("careListId", mapCareIds.get(newCareVO.getListId())))
                            .forEach(oldTypeVO -> {
                                Long oldTypeId = oldTypeVO.getListId();
                                oldTypeVO.setListId(null);
                                oldTypeVO.setCareListId(newCareVO.getListId());
                                UwElderCareTypeVO newTypeVO = uwElderCareTypeService.save(oldTypeVO);
                                mapTypeIds.put(newTypeVO.getListId(), oldTypeId);
                                // 儲存明細 - T_UW_ELDER_CARE_DTL
                                uwElderCareDtlService.findByCriteriaWithOrder(Arrays.asList(Order.asc("displayOrder")), Restrictions.eq("careTypeListId", mapTypeIds.get(newTypeVO.getListId())))
                                        .forEach(oldDtlVO -> {
                                            Long oldDtlId = oldDtlVO.getListId();
                                            oldDtlVO.setListId(null);
                                            oldDtlVO.setCareTypeListId(newTypeVO.getListId());
                                            UwElderCareDtlVO newDtlVO = uwElderCareDtlService.save(oldDtlVO);
                                            mapDtlIds.put(newDtlVO.getListId(), oldDtlId);
                                        });
                            });
                });
            });
            if (policyId.get() != null) {
                // 依照最新資訊更新時間戳記
                findElderCareDataById(policyId.get(), old_underwriteId).forEach(careVO -> {
                    careVO.getUwElderCareTypeVOs().stream()
                            .sorted(Comparator.comparing(UwElderCareTypeVO::getUpdateTimestamp).reversed())
                            .limit(1)
                            .forEach(careTypeVO -> {
                                // 暫停一下 - 因為判斷最後儲存時間來當作最新一筆資料，所以故意拖一下時間
                                LongStream.rangeClosed(0, 700000000L).average();
                                mapTypeIds.entrySet().stream()
                                        .filter(ey -> Objects.equals(ey.getValue(), careTypeVO.getListId()))
                                        .map(Map.Entry::getKey)
                                        .findFirst()
                                        .ifPresent(uwElderCareTypeService::updateCareDataToDefaultById);
                            });
                });

                return findElderCareDataById(policyId.get(), new_underwriteId);
            }
        }

        return Collections.emptyList();
    }

    private void findElderCareDetailData(UwElderCareVO careVO, Set<Long> careTypeIds) {
        if (careVO != null) {
            List<Criterion> criterias = new ArrayList<>();
            criterias.add(Restrictions.eq("careListId", careVO.getListId()));
            if (CollectionUtils.isNotEmpty(careTypeIds)) {
                criterias.add(Restrictions.in("listId", careTypeIds));
            }
            careVO.setUwElderCareTypeVOs(uwElderCareTypeService.findByCriteriaWithOrder(Arrays.asList(Order.desc("listId")), criterias.toArray(new Criterion[0])));
            IterableUtils.toList(careVO.getUwElderCareTypeVOs()).forEach(careTypeVO -> {
                careTypeVO.setUwElderCareDtlVOs(uwElderCareDtlService.findByCriteriaWithOrder(
                        Arrays.asList(
                                Order.asc("displayOrder"),
                                Order.asc("listId")
                        ),
                        Restrictions.eq("careTypeListId", careTypeVO.getListId())
                ));
            });
        }
    }

    private void checkAndChangeLastUwElderCare(Long underwriteId, Long policyID, String certiCode) {
        List<UwElderCareVO> datas = findElderCareDataById(policyID, underwriteId);

        if (CollectionUtils.isNotEmpty(datas)) {
            datas.stream()
                    .filter(careVO -> StringUtils.contains(careVO.getCertiCode(), certiCode))
                    .forEach(careVO -> uwElderCareTypeService.checkAndChangeLastUwElderCare(careVO.getListId()));
        }
    }

    private void writeDetailUwElderCare(Long listId, List<UwElderCareTypeVO> uwElderCareTypeVOs) {
        IterableUtils.toList(uwElderCareTypeVOs).forEach(typeVO -> {
            if (typeVO != null) {
                typeVO.setCareListId(listId);
                typeVO.setLastCmtFlg(YES_NO__YES);
                Optional.ofNullable(uwElderCareTypeService.save(typeVO)).ifPresent(new_typeVO -> {
                    IterableUtils.toList(new_typeVO.getUwElderCareDtlVOs()).forEach(dtlVO -> {
                        dtlVO.setCareTypeListId(new_typeVO.getListId());
                        uwElderCareDtlService.save(dtlVO);
                    });
                });
            }
        });
    }

}
