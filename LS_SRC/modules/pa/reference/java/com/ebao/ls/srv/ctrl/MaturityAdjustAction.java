package com.ebao.ls.srv.ctrl;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLEncoder;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.UserTransaction;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.ls.srv.ds.SurvivalRepayService;
import com.ebao.ls.srv.ds.vo.MaturityAdjustInfoVO;
import com.ebao.pub.framework.ExceptionFactory;
import com.ebao.pub.framework.GenericAction;
import com.ebao.pub.framework.GenericException;
import com.ebao.pub.util.BeanUtils;
import com.ebao.pub.util.DateUtils;
import com.ebao.pub.util.Trans;
import com.ebao.pub.util.TransUtils;

/**
 * <p>Title: MaturityAdjustAction </p>		
 * <p>Description: This class Maturity adjust's action class, 
 *    it implement change disbursment method,
 *       calc Maturity interest , withHolding tax and generate relate arap record.
 *       forward party ui add bankaccout
 *       popup payement query ui.</p>
 * <p>Copyright: Copyright (c) 2002	</p>
 * <p>Company: 	eBaoTech			</p>
 * <p>Create Time:		2005/04/11	</p> 
 * @author simen.li
 * @version 1.0
 */
public class MaturityAdjustAction extends GenericAction {

  /**
   * <p>Description process some action </p>
   * @param mapping
   * @param form,
   * @param request
   * @param response 
   * @return ActionForward
   * @throws Exception
   * <P>Create time 2005/4/11</P>
   */
  @Override
  public ActionForward process(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {
    MaturityAdjustForm form1 = (MaturityAdjustForm) form;

    form1.setPolicyNumber(request.getParameter("policyNumber"));
    form1.setCaseNumber(request.getParameter("caseNumber"));
    form1.setIdNumber(request.getParameter("idNumber"));
    form1.setBatchNumber(request.getParameter("batchNumber"));
    form1.setLifeAssured(request.getParameter("lifeAssured"));
    form1.setItemId(request.getParameter("itemId"));
    form1.setActionType(request.getParameter("actionType"));

    String actionType = form1.getActionType();
    String forward = "maturityAdjust";

    if (null == actionType || "".equals(actionType)
        || "search".equals(actionType)) {
      //search Maturity
      doSearch(form1, request);
    } else if ("submit".equals(actionType)) {

      UserTransaction trans = null;
      try {
        trans = Trans.getUserTransaction();
        trans.begin();

        if (null == form1.getPayId() || (form1.getPayId().intValue() == 0)) {
          survivalRepayDS.maturityInfoUpdate(form1.getItemId(), form1
              .getCurDisbursementMethod(), form1.getAccountId());
        } else {
          survivalRepayDS.maturityInfoUpdate(form1.getPayId(), form1
              .getCurDisbursementMethod(), form1.getAccountId(), form1
              .getPreviousAdjustMaturityAmount(), form1
              .getAdjustMaturityAmount(), form1.getMaturityInterest(), form1
              .getWithHoldingTax(), DateUtils
              .toDate(form1.getChequePrintDate()));
        }
        trans.commit();
        forward = "succeed";
      } catch (Exception e) {
        TransUtils.rollback(trans);
        throw ExceptionFactory.parse(e);
      } finally {
        doSearch(form1, request);
      }
    } else if ("calc".equals(actionType)) {
      String chequePrintDate = form1.getChequePrintDate();
      BigDecimal adjustMaturityAmount = form1.getAdjustMaturityAmount();
      BigDecimal netMaturityAmount = form1.getNetMaturiryAmount();
      BigDecimal preMaturityAmount = form1.getPreviousAdjustMaturityAmount();
      BigDecimal interest = survivalRepayDS.calcMaturityInterest(Long
          .valueOf(Long.parseLong(form1.getItemId())), form1
          .getNetMaturiryAmount(), DateUtils.toDate(form1.getDueDate()),
          DateUtils.toDate(chequePrintDate));

      BigDecimal withHoldingTax = survivalRepayDS.calcWithHoldingTax(interest,
          Long.valueOf(Long.parseLong(form1.getItemId())), DateUtils
              .toDate(chequePrintDate));

      doSearch(form1, request);

      form1.setMaturityInterest(interest);
      form1.setAdjustMaturityAmount(adjustMaturityAmount);
      form1.setWithHoldingTax(withHoldingTax);
      form1.setChequePrintDate(chequePrintDate);
      form1.setRevisedMaturityAmount(netMaturityAmount.add(interest).add(
          withHoldingTax.negate()).add(preMaturityAmount).add(
          adjustMaturityAmount));

    } else if ("calcWithHoldingTax".equals(actionType)) {
      String chequePrintDate = form1.getChequePrintDate();
      BigDecimal adjustMaturityAmount = form1.getAdjustMaturityAmount();
      BigDecimal interest = form1.getMaturityInterest();
      BigDecimal netMaturityAmount = form1.getNetMaturiryAmount();
      BigDecimal preMaturityAmount = form1.getPreviousAdjustMaturityAmount();
      BigDecimal withHoldingTax = survivalRepayDS.calcWithHoldingTax(interest,
          Long.valueOf(Long.parseLong(form1.getItemId())), DateUtils
              .toDate(chequePrintDate));

      doSearch(form1, request);

      form1.setMaturityInterest(interest);
      form1.setAdjustMaturityAmount(adjustMaturityAmount);
      form1.setWithHoldingTax(withHoldingTax);
      form1.setChequePrintDate(chequePrintDate);
      form1.setRevisedMaturityAmount(netMaturityAmount.add(interest).add(
          withHoldingTax.negate()).add(preMaturityAmount).add(
          adjustMaturityAmount));
    } else if ("save".equals(actionType)) {

      UserTransaction trans = null;
      try {
        trans = Trans.getUserTransaction();
        trans.begin();

        survivalRepayDS.maturityInfoSave(form1.getPayId(), form1
            .getAdjustMaturityAmount(), form1.getMaturityInterest(), form1
            .getWithHoldingTax(), DateUtils.toDate(form1.getChequePrintDate()));

        trans.commit();
      } catch (Exception e) {
        TransUtils.rollback(trans);
        throw ExceptionFactory.parse(e);
      }
    } else if ("addAccount".equals(actionType)) {

      try {

        String exitUrl = getExitURL(form1.getItemId());
        String path = "/pty/toAccount.do?partyId=" + form1.getPayeeId()
            + "&invoker=SRV" + "&exitUrl=" + exitUrl;

        ActionForward actionForward = getActionForward(path);
        return actionForward;

      } catch (Exception e) {

        throw ExceptionFactory.parse(e);
      }
    }
    return mapping.findForward(forward);
  }

  /**
   * search maturity info
   * @param form1
   * @throws Exception
   */
  private void doSearch(MaturityAdjustForm form1, HttpServletRequest request)
      throws Exception {

    MaturityAdjustInfoVO infoVO = survivalRepayDS.findMaturiyInfoByItem(form1
        .getItemId());

    BeanUtils.copyProperties(form1, infoVO);

    if (null != infoVO.getPayDue()) {
      form1.setFeeStatus(String.valueOf(infoVO.getPayDue().getFeeStatus()));
    }

    //netMaturityBreakDownForm.setTPayDue(infoVO.getTPayDue());
    request.setAttribute("accountInfos", infoVO.getAccountInfos());

    request.setAttribute("itemId", infoVO.getItemId());
    request.setAttribute("payeeId", infoVO.getPayeeId());

    if (null != infoVO.getPayDue()) {
      request.setAttribute("feeStatus", String.valueOf(infoVO.getPayDue()
          .getFeeStatus()));

      String isBeRefunded = (survivalRepayDS.isBeRefunded(infoVO.getPayDue())
          ? "1"
          : "0");

      request.setAttribute("isBeRefunded", isBeRefunded);
    }

    request.setAttribute("checkedAccountId", infoVO.getCheckedAccountId());

  }

  /**
   * <p>Description:get url string for call back by party ui</p>
   * @param param1
   * @return String url
   * @throws GenericException
   * <p>Create time: 2005/4/11</p>
   */
  public String getExitURL(String param1) throws GenericException {
    //String exitUrl =Env.URL_PREFIX + "/srv/MaturityAdjust.do?itemId=" + param1;
    String exitUrl = "/srv/MaturityAdjust.do?itemId=" + param1;
    try {
      exitUrl = URLEncoder.encode(exitUrl, "UTF-8");
    } catch (UnsupportedEncodingException ex) {
      throw ExceptionFactory.parse(ex);
    }
    return exitUrl;
  }

  /**
   * <p>Description: get actionForward by spec url</p>
   * @param path
   * @return ActionForward
   * <p>Create time:2005/4/11</p>
   */
  public ActionForward getActionForward(String path) {
    ActionForward actionForward = new ActionForward();
    actionForward.setContextRelative(true);
    actionForward.setRedirect(false); // modified for GEL00040593
    actionForward.setPath(path);
    return actionForward;
  }

  @Resource(name = SurvivalRepayService.BEAN_DEFAULT)
  private SurvivalRepayService survivalRepayDS;

}
