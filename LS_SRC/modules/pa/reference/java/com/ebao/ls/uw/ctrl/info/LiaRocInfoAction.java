package com.ebao.ls.uw.ctrl.info;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.foundation.module.util.i18n.StringResource;
import com.ebao.ls.liaRoc.LiarocDownloadLogCst;
import com.ebao.ls.liaRoc.ci.LiaRocDownload2020CI;
import com.ebao.ls.liaRoc.ci.LiaRocDownloadCI;
import com.ebao.ls.liaRoc.ds.LiarocDownloadLogHelper;
import com.ebao.ls.pa.pub.service.PolicyService;
import com.ebao.ls.pa.pub.vo.PolicyVO;
import com.ebao.ls.pa.pub.vo.UwNotintoRecvVO;
import com.ebao.ls.product.model.query.output.LifeProduct;
import com.ebao.ls.product.service.LifeProductService;
import com.ebao.ls.pub.CodeCst;
import com.ebao.ls.uw.ds.UwNotintoRecvService;
import com.ebao.ls.uw.ds.UwWorkSheetService;
import com.ebao.ls.uw.ds.vo.LiaRocInfoForm;
import com.ebao.ls.uw.ds.vo.LiaRocLiability17DetailVO;
import com.ebao.ls.uw.ds.vo.LiaRocPaidInjuryMedicalDetailVO;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.framework.ExceptionFactory;
import com.ebao.pub.framework.GenericAction;

/**
 * talks to Domain Service Layer or forwards to another page according to
 * predefined business logic or rules.
 */

public class LiaRocInfoAction extends GenericAction {

	public static final String BEAN_DEFAULT = "uwLiaRocInfo";

	public static final String REC_FORM_LIST = "recFormList";

	public static final String ISSUE_FORM_LIST = "issueFormList";

	public static final String NOTINTO_RECV_LIST = "notintoRecvList";

	public static final String TERMINATE_FORM = "terminateForm";
	
	public static final String LIAROC_DETAIL_LIST = "LiarocDetailList";
	
	public static final String LIABILITY17_DETAIL_LIST = "Liability17DetailList";
	
	public static final String  LIAROC20 = "liaroc20";

	@Resource(name = LiaRocDownloadCI.BEAN_DEFAULT)
	private LiaRocDownloadCI liaRocDownloadCI;

	@Resource(name = UwWorkSheetService.BEAN_DEFAULT)
	private UwWorkSheetService uwWorkSheetService;

	@Resource(name = PolicyService.BEAN_DEFAULT)
	private PolicyService policyService;

	@Resource(name = UwNotintoRecvService.BEAN_DEFAULT)
	private UwNotintoRecvService uwNotintoRecvService;

	@Resource(name = LifeProductService.BEAN_DEFAULT)
	private LifeProductService lifeProductService;
	

	@Resource(name = LiaRocDownload2020CI.BEAN_DEFAULT)
	private LiaRocDownload2020CI liaRocDownload2020CI;

	@Override
	@SuppressWarnings("deprecation")
	public ActionForward process(ActionMapping mapping, ActionForm form,
					HttpServletRequest request, HttpServletResponse response)
					throws Exception {

		try {

			if (request.getParameter("policyId") == null) {
				throw new InvalidParameterException("policyId should not be null.");
			}

			Long policyId = Long.valueOf(request.getParameter("policyId"));
			PolicyVO policy = policyService.loadPolicyByPolicyId(policyId);
			
			if (policy != null) {
				
				LiarocDownloadLogHelper.initNB(LiarocDownloadLogCst.NB_UW_LIAROC_DISPLAY, policyId);
				
				List<LiaRocInfoForm> issueFormList = new ArrayList<LiaRocInfoForm>();
				List<LiaRocInfoForm> receiveFormList = new ArrayList<LiaRocInfoForm>();
				LiaRocInfoForm issueForm = new LiaRocInfoForm();
				//LiaRocInfoForm receiveForm = new LiaRocInfoForm();
				
				// 承保資料查詢;
				issueForm.setDownloadType("2");
				issueFormList.addAll(uwWorkSheetService.queryDetailDetail(policy, "2"));
				request.setAttribute(ISSUE_FORM_LIST, issueFormList);

				// 收件資料查詢;
				receiveFormList.addAll(uwWorkSheetService.queryDetailDetail(policy, "1"));
				request.setAttribute(REC_FORM_LIST, receiveFormList);
				
				// 喪葬費用保險金通報資料通報資訊
				List<LiaRocLiability17DetailVO> liaRocLiability17DetailVoList =liaRocDownload2020CI.findLiability17DetailInfo(policyId);
				request.setAttribute(LIABILITY17_DETAIL_LIST , liaRocLiability17DetailVoList);
				
				// 終止保單通報資訊
				LiaRocInfoForm terminateForm = uwWorkSheetService.queryTerminateDetail(policy);
				request.setAttribute(TERMINATE_FORM, terminateForm);

				/* 未進件之收件通報查詢 */
				List<UwNotintoRecvVO> uwNotintoRecvList = uwNotintoRecvService.getNotIntoAFINSList(policyId, policy.getInsureds());
				String langId = AppContext.getCurrentUser().getLangId();
				String yun = StringResource.getStringData("MSG_216294", langId); // 元
				String danway = StringResource.getStringData("MSG_113300", langId); // 單位
				String project = StringResource.getStringData("MSG_NB_000132", langId); // 計畫
			
				for (UwNotintoRecvVO vo : uwNotintoRecvList) {
					
					vo.setPolicyCode(vo.getPolicyCode() + "/" + StringUtils.defaultString(vo.getCompanyNo()));
					if (StringUtils.isNotBlank(vo.getAmount()) && StringUtils.isNotBlank(vo.getInternalId())) {
						LifeProduct prod = lifeProductService.getProductByInternalId(vo.getInternalId());
						if (prod != null) {
							String unitFlag = prod.getProduct().getUnitFlag();
							if (CodeCst.UNIT_FLAG_BY_UNIT.equals(unitFlag)) {
								vo.setAmount(vo.getAmount() + danway);
							} else if (CodeCst.UNIT_FLAG_BY_BENEFIE_LEVEL.equals(unitFlag)) {
								vo.setAmount(project + vo.getAmount());
							} else {
								vo.setAmount(String.format("%,d",
												Integer.parseInt(vo.getAmount()))
												+ yun);
							}
						}
					}
				}
				request.setAttribute(NOTINTO_RECV_LIST, uwNotintoRecvList);
				
				/*公會通報資訊呈現是新公會還是舊公會下載資料判斷 */
				if (liaRocDownload2020CI.hasLiaroc20(policyId)) {
					request.setAttribute(LIAROC20, CodeCst.YES_NO__YES);
				}else{
					request.setAttribute(LIAROC20, CodeCst.YES_NO__NO);
				}
				
				//核保主頁面-取得公會實支實付醫療/傷害明細資訊
				List<LiaRocPaidInjuryMedicalDetailVO> liaRocPaidInjuryMedicalDetailVoList = liaRocDownloadCI.findLiarocPaidInjuryMedicalDetailInfo(policyId);
			
				request.setAttribute(LIAROC_DETAIL_LIST, liaRocPaidInjuryMedicalDetailVoList);
				
				LiarocDownloadLogHelper.flush();
			}

			String forward = "display";
			return mapping.findForward(forward);

		} catch (Exception e) {
			throw ExceptionFactory.parse(e);
		}

	}

}
