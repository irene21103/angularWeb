package com.ebao.ls.beneOwner.batch.service.impl;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.ebao.ls.beneOwner.batch.service.BeneOwnerSurveyService;
import com.ebao.ls.chl.service.AgentService;
import com.ebao.ls.chl.service.ChannelOrgService;
import com.ebao.ls.chl.vo.ChannelOrgVO;
import com.ebao.ls.chl.vo.ScAgentDataVO;
import com.ebao.ls.chl.vo.ScServiceAgentDataVO;
import com.ebao.ls.chl.vo.ScServiceRepresentDataVO;
import com.ebao.ls.cs.boaudit.data.BeneficialOwnerSurveyDao;
import com.ebao.ls.cs.boaudit.data.bo.BeneficialOwnerSurvey;
import com.ebao.ls.cs.boaudit.vo.BeneficialOwnerSurveyVO;
import com.ebao.ls.foundation.service.GenericServiceImpl;
import com.ebao.ls.pty.ds.CustomerService;
import com.ebao.ls.pub.batch.impl.DataSourceWrapper;
import com.ebao.ls.pub.cst.YesNo;
import com.ebao.ls.sc.vo.AgentChlVO;
import com.ebao.pub.batch.help.BatchHelp;
import com.ebao.pub.batch.type.LogLevel;
import com.ebao.pub.batch.util.BatchLogUtils;
import com.ebao.pub.framework.GenericException;
import com.ebao.pub.para.Para;
import com.ebao.pub.util.DateUtils;
import com.ibm.icu.text.SimpleDateFormat;

public class BeneOwnerSurveyServiceImpl
extends
GenericServiceImpl<BeneficialOwnerSurveyVO, BeneficialOwnerSurvey, BeneficialOwnerSurveyDao>
implements BeneOwnerSurveyService {

    @Resource(name=AgentService.BEAN_DEFAULT)
    private AgentService agentService;
    
    @Resource(name=ChannelOrgService.BEAN_DEFAULT)
    private ChannelOrgService channelServ;
    
    @Resource(name=CustomerService.BEAN_DEFAULT)
    private  CustomerService customerService;
    
    @Resource(name = BeneficialOwnerSurveyDao.BEAN_DEFAULT)
    private BeneficialOwnerSurveyDao beneficialOwnerSurveyDao;

    protected static final NamedParameterJdbcTemplate namedParameterjdbcTemplate = new NamedParameterJdbcTemplate(new DataSourceWrapper());
    
    private static Logger logger = Logger.getLogger(BeneOwnerSurveyServiceImpl.class);
    
    // 孤兒保單派發至 PCD = HO
    private final Long DELIVER_TO_HO = 4L;
    
    // 虛擬業務員=孤兒保單
    private final Long ORPHAN = 2L;
    
    private Date processDate;

    private void log(String message) {
    	logger.info("[SYSOUT][BeneOwnerSurveyServiceImpl]" + message);
    	BatchLogUtils.addLog(LogLevel.INFO, null, message);
    }
    
    @Override
	public BeneficialOwnerSurveyVO queryBeneficialOwnerSurvey(Long partyId, Long policyId,
			String targetSurveyType, Date targetsurveyDate)
			throws GenericException {				
    	BeneficialOwnerSurveyVO vo = null;        
    	BeneficialOwnerSurvey beneficialOwnerSurvey = dao.queryBeneficialOwnerSurvey(partyId, policyId, targetSurveyType, targetsurveyDate);
		if (beneficialOwnerSurvey != null) {
			vo = this.convertToVO(beneficialOwnerSurvey) ;
		} 
		return vo;
	}
    
    public List<Map<String,Object>> getBeneOwnerSurveyList(Date processDate) {
    	
        StringBuilder sql = new StringBuilder();	
		String skipTimeConstraint = Para.getParaValue(3071000001L);
		
		/**
		 * 法人實質受益人調查對象抽取條件
		 * 1.當批次FMT_POS_0470保險金到期通知給付項目的受益人
           2.受益人為法人
                                排除  1.三年內eBao系統已有法人實質受益人建檔之法人受益人
                2.法人記錄不需提供實質受益人標示 = Y
                3.二個月內己發送過的法人受益人調查名單
		 */
        sql.append(" select distinct policy_id, ");
        sql.append("                 policy_code,  ");
        sql.append("                 liability_state liability_status,     ");
        sql.append("                 party_id,     ");
        sql.append("                 name,     ");
        sql.append("                 certi_code,     ");
        sql.append("                 to_char(payDueDate,'yyyy-MM-dd') pay_due_date,     ");
        sql.append("                 payPlanType pay_plan_type,     ");
        sql.append("                 agent_id,     ");
        sql.append("                 register_code agent_register_code,     ");
        sql.append("                 agent_name,     ");
        sql.append("                 business_cate agent_biz_cate,     ");
        sql.append("                 channel_id channel_org,     ");
        sql.append("                 channel_code,     ");
        sql.append("                 channel_name,     ");
        sql.append("                 decode(business_cate, 2, 4, channel_type) channel_type ");
        sql.append(" from (     ");
        sql.append(" select fmt.change_id, cm.policy_id, cm.policy_code, cm.liability_state, pp.plan_id, cm.service_agent,  ");
        sql.append("        cmbene.party_id, cmbene.certi_code, cmbene.name, cmbene.certi_type, pp.item_id,cm.validate_date, pp.update_timestamp , ");
        sql.append("        agt.agent_id, agt.register_code, agt.agent_name, payPlanType, payDueDate , ");
        sql.append("        agt.business_cate, org.channel_id, org.channel_code, channel_name, org.channel_type, ");
        sql.append("        rank() over(partition by nvl(cmbene.certi_code,'X'), cmbene.name  order by cm.validate_date desc , pp.update_timestamp desc, pp.plan_id desc, cmBene.bene_type ) seq ");
        sql.append("  from (select p470.planid, p470.itemid, p470.change_id, p470.payPlanType, p470.payDueDate  ");
        sql.append("          from v_fmt_pos_0470_batch p470, t_contract_product cmPrd   "); //各項保險金到期通知書
        sql.append("         where 1 = 1  ");
        sql.append("           and p470.payDueDate is not null  ");
        sql.append("           and p470.policy_id = cmPrd.policy_id    ");
        //sql.append("           and p470.policy_id = 20099251   "); //TEST
        sql.append("           and p470.itemid = cmPrd.item_id  ");                                                                                                                                                                                                                                                                                                           
        sql.append("           and exists   ");
        sql.append("              (select *   ");
        sql.append("               from t_product_category prodCate     ");
            sql.append("               where prodCate.product_id = cmPrd.product_id     ");
            sql.append("               and prodCate.category_id in (40031, 40154)) ");
        if(YesNo.YES_NO__NO.equals(skipTimeConstraint)) {
            sql.append("           and :process_date >= :fifteen_date ");
            sql.append("           and to_number(:current_month) > nvl(to_number((select to_char(max(doc.insert_time),'YYYYMM') ");
            sql.append("                              from t_document doc ");
            sql.append("                              where doc.template_id = 30030 and doc.insert_time <  :process_date )),0) ");
        }
        sql.append("           and p470.payDueDate > :process_date ");
        sql.append("           and to_char(p470.payDueDate,'yyyyMM') =  :after_two_month ");
        sql.append("		   and p470.payPlanType not in (1,9) ");
        sql.append("  ) fmt  ");
        sql.append("  join t_pay_plan pp  ");
        sql.append("    on fmt.planid = pp.plan_id  ");
        sql.append("  join (select cmBene.policy_id,  ");
        sql.append("               cmBene.item_id,  ");
        sql.append("               cmBene.Party_Id,  ");
        sql.append("               cmBene.Certi_Code,  ");
        sql.append("               cmBene.name, ");
        sql.append("               cmBene.Certi_Type,  ");
        sql.append("               decode(cmbene.bene_type, 4, 2, 1, 3, 3, 4, 5, 10, 6, 9) bene_type  ");
        sql.append("          from t_contract_bene cmBene  ");
        sql.append("         where cmBene.Bene_Type in (4, 1, 3, 5, 6)) cmBene  ");
        sql.append("    on cmBene.item_id = pp.item_id  ");
        sql.append("   and cmBene.Bene_Type = pp.pay_plan_type  ");
        sql.append("   and cmBene.party_id > 0  ");
        sql.append("  join t_contract_master cm  ");
        sql.append("    on cm.policy_id = pp.policy_id  ");
        sql.append("   and cm.liability_state > 0  ");
        sql.append("  join t_agent agt ");
        sql.append("    on cm.service_agent = agt.agent_id ");
        sql.append("  join t_channel_org org ");
        sql.append("    on agt.channel_org_id = org.channel_id ");
        sql.append("  join t_company_customer cc  ");
        sql.append("    on cc.company_id = cmBene.party_id  ");
        sql.append("   and nvl(is_gov,'N') = 'N' ");  
        sql.append("   and not exists (select * from t_beneficial_owner bo  ");
        sql.append("                   where cc.company_id=bo.parent_id and update_time > add_months(:process_date,-12) and pp.pay_plan_type = 4 ) ");
        sql.append("   and not exists (select * from t_beneficial_owner bo  ");
        sql.append("                   where cc.company_id=bo.parent_id and update_time > add_months(:process_date,-36) and pp.pay_plan_type != 4 ) ");
        sql.append("   and not exists (select * from t_beneficial_owner_survey bos where cc.company_id=bos.party_id ");
        sql.append("                   and target_survey_type=1 and to_char(target_survey_date,'yyyyMM') >=  :before_two_month) ");
        sql.append(") where seq=1 ");
        
    	Map<String, Object> params = new HashMap<String, Object>();
        java.sql.Date processSqlDate = new java.sql.Date(processDate.getTime());
        java.util.Date fifteenDate = DateUtils.toDate(DateUtils.date2string(processDate, "yyyyMM") + "15", "yyyyMMdd");
        java.sql.Date fifteenSqlDate = new java.sql.Date(fifteenDate.getTime());
        String afterTwoMonth = DateUtils.date2String(DateUtils.addMonth(processDate,2), "yyyyMM");
        String beforeTwoMonth = DateUtils.date2String(DateUtils.addMonth(processDate,-2), "yyyyMM");
        String currentMonth = DateUtils.date2String(processDate, "yyyyMM");
        params.put("after_two_month", afterTwoMonth);
        params.put("before_two_month", beforeTwoMonth);
        params.put("current_month", currentMonth);
        params.put("process_date", processSqlDate);
        params.put("fifteen_date", fifteenSqlDate);
 
        List<Map<String,Object>> results = namedParameterjdbcTemplate.queryForList(sql.toString(), params);
        if (CollectionUtils.isEmpty(results)) {
            return null;
        }
        return results;
    }
    
    public List<Map<String,Object>> getBeneOwnerNoReplyList(Date processDate) {
    	
        StringBuilder sql = new StringBuilder();	
		
		/**
		 * 法人實質受益人調查名單未建檔抽取條件
		 * 產生法人實質受益人調查名單報表數據，給付日前一個月25號前，仍無該法人實質受益人之建檔記錄。
		 */
        sql.append(" select distinct policy_id, ");
        sql.append("                 policy_code,  ");
        sql.append("                 liability_status,     ");
        sql.append("                 party_id,     ");        
        sql.append("                 certi_code,     ");
        sql.append("                 name,     ");
        sql.append("                 pay_due_date,     ");
        sql.append("                 pay_plan_type,     ");
        sql.append("                 agent_id,     ");
        sql.append("                 agent_register_code,     ");
        sql.append("                 agent_name,     ");
        sql.append("                 agent_biz_cate,     ");
        sql.append("                 channel_org,     ");
        sql.append("                 channel_code,     ");
        sql.append("                 channel_name,     ");
        sql.append("                 channel_type, ");
        sql.append("                 target_survey_date, ");
        sql.append("                 target_survey_type ");
        sql.append("   from t_beneficial_owner_survey bos  ");
        sql.append("   join t_company_customer cc  ");
        sql.append("     on bos.party_id = cc.company_id  ");
        sql.append("    and nvl(cc.is_gov,'N') = 'N' "); 
        sql.append("  where target_survey_type=1 ") ;
        sql.append("    and :process_date >= :twentyFifth_Date ");
        sql.append("    and to_char(target_survey_date,'yyyyMM')= :before_one_month");
        sql.append("    and not exists (select * from t_beneficial_owner bo where cc.company_id=bo.parent_id and update_time > add_months(target_survey_date,-36)) ");
        sql.append("    and not exists (select * from t_beneficial_owner_survey bos2 where cc.company_id=bos2.party_id ");
        sql.append("                       and bos2.target_survey_type=2 and to_char(bos2.target_survey_date,'yyyyMM') =  :current_month) ");
        
    	Map<String, Object> params = new HashMap<String, Object>();
        java.sql.Date processSqlDate = new java.sql.Date(processDate.getTime());
        String beforeOneMonth = DateUtils.date2String(DateUtils.addMonth(processDate,-1), "yyyyMM");
        String currentMonth = DateUtils.date2String(processDate, "yyyyMM");
        java.util.Date twentyFifthDate = DateUtils.toDate(DateUtils.date2string(processDate, "yyyyMM") + "25", "yyyyMMdd");
        java.sql.Date twentyFifthSqlDate = new java.sql.Date(twentyFifthDate.getTime());
        params.put("before_one_month", beforeOneMonth);
        params.put("process_date", processSqlDate);
        params.put("current_month", currentMonth);
        params.put("twentyFifth_Date", twentyFifthSqlDate);
 
        List<Map<String,Object>> results = namedParameterjdbcTemplate.queryForList(sql.toString(), params);
        if (CollectionUtils.isEmpty(results)) {
            return null;
        }
        return results;
    }
    
    @Override
	public BeneficialOwnerSurveyVO createBeneficialOwnerSurvey(Map<String, Object> column, Date processDate, String targetSurveyType)
			throws GenericException {	

    	BeneficialOwnerSurveyVO beneficialOwnerSurveyVO = new BeneficialOwnerSurveyVO();
    	Long partyId =  MapUtils.getLong(column, "party_id");;
    	Long policyId = MapUtils.getLong(column, "policy_id");
    	String name = StringUtils.trim(MapUtils.getString(column, "name",""));
    	String certiCode = StringUtils.trim(MapUtils.getString(column, "certi_code",""));
    	String payPlanType = StringUtils.trim(MapUtils.getString(column, "pay_plan_type",""));
    	String stringDate = MapUtils.getString(column, "pay_due_date","");
    	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date payDueDate = null;
		try {
			payDueDate = new Date(sdf.parse(stringDate).getTime());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
    	String policyCode = StringUtils.trim(MapUtils.getString(column, "policy_code",""));
    
    	// 調查名單存入t_beneficial_owner_survey
         beneficialOwnerSurveyVO.setPolicyId(policyId);
    	 beneficialOwnerSurveyVO.setPolicyCode(policyCode);
    	 beneficialOwnerSurveyVO.setLiabilityStatus(MapUtils.getLong(column, "liability_status"));
    	 beneficialOwnerSurveyVO.setPartyId(partyId);
    	 beneficialOwnerSurveyVO.setName(name);
    	 beneficialOwnerSurveyVO.setCertiCode(certiCode);
    	 beneficialOwnerSurveyVO.setPayPlanType(payPlanType);
    	 beneficialOwnerSurveyVO.setPayDueDate(payDueDate);
    	 beneficialOwnerSurveyVO.setAgentId(MapUtils.getLong(column, "agent_id"));
    	 beneficialOwnerSurveyVO.setAgentRegisterCode(StringUtils.trim(MapUtils.getString(column, "agent_register_code", "")));
    	 beneficialOwnerSurveyVO.setAgentName(StringUtils.trim(MapUtils.getString(column, "agent_name", "")));
    	 beneficialOwnerSurveyVO.setAgentBizCate(MapUtils.getLong(column, "agent_biz_cate"));
     	 BigDecimal channelOrg = (BigDecimal) column.get("channel_org");
    	 if(channelOrg != null) {
    		 beneficialOwnerSurveyVO.setChannelOrg(channelOrg.longValue());
    	 }
    	 beneficialOwnerSurveyVO.setChannelCode(StringUtils.trim(MapUtils.getString(column, "channel_code", "")));
    	 beneficialOwnerSurveyVO.setChannelName(StringUtils.trim(MapUtils.getString(column, "channel_name", "")));
    	 beneficialOwnerSurveyVO.setTargetSurveyDate(processDate);
    	 beneficialOwnerSurveyVO.setTargetSurveyType(targetSurveyType);
    	 BigDecimal channelType = (BigDecimal)column.get("channel_type");
    	 if(channelType != null) {
    		 beneficialOwnerSurveyVO.setChannelType(channelType.longValue());
    	 }
    	 
    	 //調查名單產生的日期
    	 if (targetSurveyType.equals("2")) {
    		 stringDate = MapUtils.getString(column, "target_survey_date","");
    	 	 Date notifyDate = null;
    	 	 try {
     			 notifyDate = new Date(sdf.parse(stringDate).getTime());
     		 } catch (ParseException e) {
     			 // TODO Auto-generated catch block
     		 	e.printStackTrace();
     		 }
    	 	 beneficialOwnerSurveyVO.setNotifyDate(notifyDate);
    		 
    	 }
 		
    	 return beneficialOwnerSurveyVO;
    	 
		} 
 
    
    /**
     * 派發孤兒保單
     */
    @Override
    public void deliverOrphan(BeneficialOwnerSurveyVO vo){
        // 孤兒保單派發至 PCD = HO
    	if(new Long(ORPHAN).equals(vo.getAgentBizCate())){
            vo.setChannelType(DELIVER_TO_HO);
        }
    }
    
    /**
     * 更新業務員
     */
    @Override
    public void updateServiceAgent(BeneficialOwnerSurveyVO vo) throws Exception {
        Long policyId = vo.getPolicyId();
        ScAgentDataVO agentDataVO = agentService.getAgentDataByPolicyId(policyId);
        List<ScServiceAgentDataVO> serviceAgentList = agentDataVO.getServiceAgentList();
        List<ScServiceRepresentDataVO> representAgentList = agentDataVO.getServiceRepresentList();
        if(CollectionUtils.isEmpty(serviceAgentList)){
            BatchLogUtils.addLog(LogLevel.WARN, null, String.format("[BR-CMN-FAT-033][同步FATCA盡職調查清單的服務員與綜合查詢一致] [查無服務員]policy_code = %s, certi_code =%s, policy_id = %d, party_id =%d" , vo.getPolicyCode(), vo.getCertiCode(), vo.getPolicyId(), vo.getPartyId()));
            return;
        }
        ScServiceAgentDataVO serviceAgentVO = serviceAgentList.get(0);
        AgentChlVO agentchlVO = agentService.findAgentByAgentId(serviceAgentVO.getAgentId());
        if(agentchlVO == null)
            return;
        
        BatchLogUtils.addLog(LogLevel.WARN, null, "-1-serviceAgentList.size()="+serviceAgentList.size());
        Boolean IsAllDummy = Boolean.TRUE;
        for(ScServiceAgentDataVO svo:serviceAgentList){
        	BatchLogUtils.addLog(LogLevel.WARN, null, "0-findAgentByAgentId.getAgentId="+svo.getAgentId());
        	AgentChlVO agentChlVO =agentService.findAgentByAgentId(svo.getAgentId());
        	if(agentChlVO!=null){
        		BatchLogUtils.addLog(LogLevel.WARN, null, "agentChlVO!=nullagentChlVO.getAgentId()="+agentChlVO.getAgentId()+";agentChlVO.getIsDummy()="+agentChlVO.getIsDummy()+";");
        		if(YesNo.YES_NO__NO.equals(agentChlVO.getIsDummy())){
        			IsAllDummy = Boolean.FALSE;;
        		}
        	}
        }
        BatchLogUtils.addLog(LogLevel.WARN, null, "1-IsAllDummy="+IsAllDummy);
        
        vo.setAgentId(serviceAgentVO.getAgentId());
        if(agentchlVO.getBusinessCate() != null)
            vo.setAgentBizCate(Long.valueOf(agentchlVO.getBusinessCate()));
        vo.setAgentName(serviceAgentVO.getAgentName());
        vo.setAgentRegisterCode(serviceAgentVO.getAgentRegisterCode());
        
        Boolean toPCD = Boolean.FALSE;
        if(agentchlVO.getChannelOrgId() != null){
        	BatchLogUtils.addLog(LogLevel.WARN, null, "2-serviceAgentVO.getAgentChannelCode()="+serviceAgentVO.getAgentChannelCode());
        	ChannelOrgVO channelVO = channelServ.findByChannelCode(serviceAgentVO.getAgentChannelCode());
            if(channelVO != null){
            	vo.setChannelOrg(channelVO.getChannelId());
                vo.setChannelCode(serviceAgentVO.getAgentChannelCode());
                vo.setChannelName(serviceAgentVO.getAgentChannelName());
                vo.setChannelType(serviceAgentVO.getAgentChannelType());
                //PCR-519543 排除BRBD
                BatchLogUtils.addLog(LogLevel.WARN, null, "3-channelVO.getChannelType()="+channelVO.getChannelType());
                if(5l!=channelVO.getChannelType()&&6l!=channelVO.getChannelType()){
                	toPCD = YesNo.YES_NO__YES.equals(channelVO.getIsDummy()) && IsAllDummy;
                	vo.setAgentBizCate(YesNo.YES_NO__YES.equals(channelVO.getIsDummy())&& IsAllDummy ? 2L : 1L);
                }else{
                	//外部通路(BR/FID)-一律給服務單位處理，不歸入直營通路的虛擬單位PCD。
                	vo.setAgentBizCate(1L);
                }
                
            }
        }
        BatchLogUtils.addLog(LogLevel.WARN, null, "4-toPCD="+toPCD);
        //孤兒保單服務業務員
        if(toPCD && CollectionUtils.isNotEmpty(representAgentList)) {
			ScServiceRepresentDataVO representVO = representAgentList.get(0);
			vo.setAgentId(representVO.getAgentId());
			vo.setAgentName(representVO.getRepresentName());
			vo.setAgentRegisterCode(representVO.getRepresentRegisterCode());
			vo.setAgentBizCate(2L);
        }
    }

	@Override
	protected BeneficialOwnerSurveyVO newEntityVO() {
		// TODO Auto-generated method stub
		return null;
	}
	
	/**
     * 儲存紀錄
     */
	@Override
	public Long saveBeneOwnerSurvey(BeneficialOwnerSurveyVO beneOwnerSurveyVO) throws GenericException {		
		if(beneOwnerSurveyVO!= null ){
			BeneficialOwnerSurvey entity = new BeneficialOwnerSurvey();
		    entity.copyFromVO(beneOwnerSurveyVO, Boolean.TRUE, Boolean.FALSE);
			beneficialOwnerSurveyDao.save(entity);
			
			return  beneOwnerSurveyVO.getListID() ;
		}
		return null ; 
	}
	
	
}
