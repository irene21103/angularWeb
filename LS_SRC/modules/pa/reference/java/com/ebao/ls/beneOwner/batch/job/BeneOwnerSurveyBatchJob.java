package com.ebao.ls.beneOwner.batch.job;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.transaction.UserTransaction;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Session;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.ebao.foundation.common.lang.exception.ExceptionUtils;
import com.ebao.foundation.commons.exceptions.ExceptionUtil;
import com.ebao.foundation.core.logging.Log;
import com.ebao.foundation.module.util.format.DateFormat;
import com.ebao.ls.batch.TGLBaseUnpieceableJob;
import com.ebao.ls.beneOwner.batch.service.BeneOwnerSurveyService;
import com.ebao.ls.cs.boaudit.data.BeneficialOwnerSurveyDao;
import com.ebao.ls.cs.boaudit.data.bo.BeneficialOwnerSurvey;
import com.ebao.ls.cs.boaudit.vo.BeneficialOwnerSurveyVO;
import com.ebao.ls.cs.commonflow.data.bo.Change;
import com.ebao.ls.cs.pub.helper.DateHelper;
import com.ebao.ls.pub.CodeCst;
import com.ebao.ls.pub.batch.impl.BatchJdbcTemplate;
import com.ebao.ls.pub.batch.impl.DataSourceWrapper;
import com.ebao.ls.pub.cst.YesNo;
import com.ebao.pub.batch.HibernateSession;
import com.ebao.pub.batch.help.BatchHelp;
import com.ebao.pub.batch.type.JobStatus;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.para.Para;
import com.ebao.pub.util.DateUtils;
import com.ebao.pub.util.Trans;
import com.ebao.pub.util.TransUtils;

/**
 * 保險金法人實質受益人調查名單
 */
public class BeneOwnerSurveyBatchJob extends TGLBaseUnpieceableJob {
	
	public static final String BEAN_DEFAULT = "beneOwnerSurveyBatchJob";
	
	private final Log logger = Log.getLogger(BeneOwnerSurveyBatchJob.class);
	
	private static final boolean IS_DEBUG= false;
	
	private String targetSurveyType = "1";
	
	@Resource(name = BeneOwnerSurveyService.BEAN_DEFAULT)
	private BeneOwnerSurveyService beneOwnerSurveyService;
	
	/**
     * 版本號
     */
    private static final String VERSION = "0.24";
	
	/**
     * 業務日期
     */
    private Date processDate = null;
    
    /**
     * 寫info log
     * @param layout
     * @param msg
     */
    private void log(String msg) {
    	logger.info("[SYSOUT][BeneOwnerSurveyBatchJob] " + msg);
    	batchLogger.info(msg);
    }
    
    /**
     * 批次進入點
     */
    @Override
    public int processJob() throws Exception {
    	log("processJob - start");
    	log("VERSION = " + VERSION);
        
    	if (!isMock()) {
    		processDate = BatchHelp.getProcessDate();
    	} else {
    		log("測試模式");
    		if (processDate == null) {
    			throw new IllegalArgumentException("processDate is null");
    		}
    	}
        
        log("業務日期 = " + processDate);
        
        try {
            process(processDate, targetSurveyType);
        } catch(Exception ex){
            log(ExceptionUtils.getFullStackTrace(ex));
            throw ExceptionUtil.parse(ex);
        }
        log("processJob - end");
        if (!isMock()) {
        	return getExecuteStatus();
        }
        else {
        	log("成功筆數 = " + getBatchContext().getSuccessCount());
        	log("失敗筆數 = " + getBatchContext().getErrorCount());
        	return getExecuteStatus(getBatchContext().getSuccessCount(), getBatchContext().getErrorCount());
        }
    }
    
    private void process(Date processDate, String targetSurveyType) throws Exception {
        // 取得法人實質受益人應調查名單
    	List<Map<String,Object>> mapList=  beneOwnerSurveyService.getBeneOwnerSurveyList(processDate);
        UserTransaction trans = null;
        if(CollectionUtils.isNotEmpty(mapList)) {
        	for (Map<String,Object> map : mapList) {
            	try {
            		trans = TransUtils.getUserTransaction();
            		trans.begin();
            		
            		// 新增調查名單
            		BeneficialOwnerSurveyVO beneficialOwnerSurveyVO = beneOwnerSurveyService.createBeneficialOwnerSurvey(map,processDate,targetSurveyType);
            		// 更新業務員名單
               	    beneOwnerSurveyService.updateServiceAgent(beneficialOwnerSurveyVO);     
               	    // 儲存調查名單
               	    beneOwnerSurveyService.saveBeneOwnerSurvey(beneficialOwnerSurveyVO);
         
                    incSuccessCount();
                    trans.commit();                
            	} catch (Exception ex) {
            		trans.rollback();
            		log("發生異常");
                	log(ExceptionUtils.getFullStackTrace(ex));
                	incErrorCount();
                } finally {
                	trans = null;
                }
            }
        }
        
    }
    
    public void setProcessDate(Date date) {
    	this.processDate = date;
    }

	@Override
	protected String getJobName() {
		// TODO Auto-generated method stub
		return null;
	}
}