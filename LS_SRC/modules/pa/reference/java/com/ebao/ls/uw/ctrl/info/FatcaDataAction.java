package com.ebao.ls.uw.ctrl.info;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.ls.uw.ds.UwInsureHistoryService;
import com.ebao.ls.uw.ds.vo.UwFatcaDataVO;
import com.ebao.pub.framework.GenericAction;
import com.ebao.pub.util.StringUtils;

/**
 * <p>Title: 查詢FATCA資料ACTION</p>
 * <p>Description: 查詢FATCA資料ACTION</p>
 * <p>Copyright: Copyright (c) 2016</p>
 * <p>Company: TGL Co., Ltd.</p>
 * <p>Create Time: Jun 13, 2016</p> 
 * @author 
 * <p>Update Time: Jun 13, 2016</p>
 * <p>Updater: Alex Cheng</p>
 * <p>Update Comments: </p>
 */
public class FatcaDataAction extends GenericAction {

	@Resource(name = UwInsureHistoryService.BEAN_DEFAULT)
	private UwInsureHistoryService uwInsureHistoryService;
	
	@Override
	public ActionForward process(ActionMapping mapping, ActionForm form,
					HttpServletRequest request, HttpServletResponse response)
					throws Exception {

		UwFatcaDataForm uwFatcaDataForm = (UwFatcaDataForm) form;
		String policyId = request.getParameter("policyId").toString();
		String forward = "display";

		if (!StringUtils.isNullOrEmpty(policyId)) {
			uwFatcaDataForm.setPolicyId(new Long(policyId));
			List<UwFatcaDataVO> fatcaDatas = uwInsureHistoryService.findFatcaDatas(new Long(policyId));
			uwFatcaDataForm.setFatcaDatas(fatcaDatas);
		}
		return mapping.findForward(forward);
	}

}
