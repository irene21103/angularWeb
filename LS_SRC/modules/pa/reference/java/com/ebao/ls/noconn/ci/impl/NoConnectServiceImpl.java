package com.ebao.ls.noconn.ci.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;

import com.ebao.ls.cmu.pub.data.DocumentENoticeDao;
import com.ebao.ls.noconn.ci.NoConnectService;
import com.ebao.ls.noconn.vo.NoConnectVO;

public class NoConnectServiceImpl implements NoConnectService{
		
	@Resource(name = "eNoticeMainDao")
    private DocumentENoticeDao documentENoticeDao;
	@Override
	public NoConnectVO findNoConnectDataByPolicyCode(String policyCode) {
		List<Map<String, Object>> list=documentENoticeDao.queryPolicy(policyCode, null );
		NoConnectVO noConnectVO=null;
		if(CollectionUtils.isNotEmpty(list)) {
			Map<String, Object> map= list.get(0);
			
			String noConnect = map.get("NO_CONNECT")!=null?map.get("NO_CONNECT").toString():"";
			String noConnectAddr = map.get("NO_CONNECT_ADDR")!=null?map.get("NO_CONNECT_ADDR").toString():"";
			String noConnectEmail=map.get("NO_CONNECT_EMAIL")!=null?map.get("NO_CONNECT_EMAIL").toString():"";
			String noConnectSMS=map.get("NO_CONNECT_SMS")!=null?map.get("NO_CONNECT_SMS").toString():"";
			noConnectVO = new NoConnectVO(policyCode,noConnect,noConnectAddr,noConnectEmail, noConnectSMS);
		}
		
		return noConnectVO;
	}

	@Override
	public List<NoConnectVO> findNoConnectDataByCertiCode(String certiCode) {
		List<Map<String, Object>> list=documentENoticeDao.queryPolicy(null, certiCode );
		List<NoConnectVO> noConnectVOs= null;
		if(CollectionUtils.isNotEmpty(list)) {
			noConnectVOs= new ArrayList<NoConnectVO>();
			for(Map<String, Object> map : list) {
				String policyCode = map.get("POLICY_CODE")!=null?map.get("POLICY_CODE").toString():"";
				String noConnect = map.get("NO_CONNECT")!=null?map.get("NO_CONNECT").toString():"";
				String noConnectAddr = map.get("NO_CONNECT_ADDR")!=null?map.get("NO_CONNECT_ADDR").toString():"";
				String noConnectEmail=map.get("NO_CONNECT_EMAIL")!=null?map.get("NO_CONNECT_EMAIL").toString():"";
				String noConnectSMS=map.get("NO_CONNECT_SMS")!=null?map.get("NO_CONNECT_SMS").toString():"";
				NoConnectVO noConnectVO = new NoConnectVO(policyCode,noConnect,noConnectAddr,noConnectEmail, noConnectSMS);
				noConnectVOs.add(noConnectVO);
			}
		}
		
		return noConnectVOs;
	}

	

}
