package com.ebao.ls.uw.ds;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ebao.ls.foundation.service.GenericServiceImpl;
import com.ebao.ls.pa.pub.data.bo.UwPatchImage;
import com.ebao.ls.pa.pub.vo.UwPatchImageVO;
import com.ebao.ls.pub.CodeCst;
import com.ebao.ls.uw.data.TUwPatchImageDelegate;

public class UwPatchImageServiceImpl extends GenericServiceImpl<UwPatchImageVO, UwPatchImage, TUwPatchImageDelegate<UwPatchImage>> implements UwPatchImageService {

	@Override
	protected UwPatchImageVO newEntityVO() {
		return new UwPatchImageVO();
	}

	public List<UwPatchImageVO> getPatchImageByPolicyId(Long policyId) {
		return dao.getPatchImageByPolicyId(policyId);
	}

	@Override
	public UwPatchImageVO getPatchImageByListId(String listId) {
		UwPatchImage result = dao.load(Long.valueOf(listId));
		return convertToVO(result);
	}
	@Override
	public java.util.List<UwPatchImageVO> getPatchImage(Long underwriteId, Long policyId, java.util.Date uploadDate , String imageName){
		return dao.getPatchImage(underwriteId, policyId, uploadDate, imageName);
	}
	@Override
	public boolean existPatchImageNotConfirmByListId(Long policyId) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("policyId", policyId);
		map.put("confirmComplete", CodeCst.YES_NO__NO);
		List<UwPatchImage> list = dao.find(map);
		if( list == null || list.isEmpty()) {
			return false;
		}
		return true;
	}

	@Override
	public boolean existByFileNameAndConfirmComplete(Long policyId, String fileName) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("imageName", fileName);
		map.put("policyId", policyId);
		map.put("confirmComplete", CodeCst.YES_NO__NO);
		List<UwPatchImage> list = dao.find(map);
		if( list == null || list.isEmpty()) {
			return false;
		}
		return true;
	}
	
	public List<UwPatchImageVO> getPatchImageByUnderwriteId(Long underwriteId) {
		return dao.getPatchImageByUnderwriteId(underwriteId);
	}
}
