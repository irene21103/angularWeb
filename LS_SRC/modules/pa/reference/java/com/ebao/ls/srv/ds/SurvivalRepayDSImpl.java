package com.ebao.ls.srv.ds;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.hibernate.FlushMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ebao.foundation.module.db.hibernate.HibernateSession3;
import com.ebao.ls.arap.pub.ci.ARAPCI;
import com.ebao.ls.arap.vo.PremArapVO;
import com.ebao.ls.clm.ci.ClaimCI;
import com.ebao.ls.cs.ci.CSCI;
import com.ebao.ls.cs.ci.vo.CSCIVO;
import com.ebao.ls.cs.ci.vo.CSPayerPayeeRuleCIVO;
import com.ebao.ls.cs.pub.bo.CommonDumper;
import com.ebao.ls.pa.pub.bs.PayPlanService;
import com.ebao.ls.pa.pub.ci.CalculatorCI;
import com.ebao.ls.pa.pub.ci.PolicyCI;
import com.ebao.ls.pa.pub.ref.api.prd.ProductService;
import com.ebao.ls.pa.pub.service.PolicyService;
import com.ebao.ls.pa.pub.vo.CoverageVO;
import com.ebao.ls.pa.pub.vo.PolicyHolderVO;
import com.ebao.ls.pa.pub.vo.PolicyVO;
import com.ebao.ls.pty.vo.AddressVO;
import com.ebao.ls.pty.vo.BankAccountVO;
import com.ebao.ls.pty.vo.CompanyCustomerVO;
import com.ebao.ls.pty.vo.CustomerVO;
import com.ebao.ls.pub.CodeCst;
import com.ebao.ls.pub.dump.Dumper;
import com.ebao.ls.srv.bo.PayPlan;
import com.ebao.ls.srv.bo.PayPlanPayee;
import com.ebao.ls.srv.data.TPayDueDelegate;
import com.ebao.ls.srv.data.TPayDueDelegateExt;
import com.ebao.ls.srv.data.bo.PayDue;
import com.ebao.ls.srv.data.query.MaturityInfoQuery;
import com.ebao.ls.srv.ds.sp.SurvivalDSSp;
import com.ebao.ls.srv.ds.vo.AnnuityUpdateInfoVO;
import com.ebao.ls.srv.ds.vo.MaturityAdjustInfoVO;
import com.ebao.ls.srv.ds.vo.PayPlanPayeeVO;
import com.ebao.ls.srv.ds.vo.PayPlanVO;
import com.ebao.ls.srv.ds.vo.PayeeAccountInfoVO;
import com.ebao.ls.srv.vo.PayDueVO;
import com.ebao.pub.annotation.DataModeChgMovePoint;
import com.ebao.pub.annotation.PrdAPIUpdate;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.framework.AppException;
import com.ebao.pub.framework.ExceptionFactory;
import com.ebao.pub.framework.GenericException;
import com.ebao.pub.framework.ObjectNotFoundException;
import com.ebao.pub.i18n.lang.CodeTable;
import com.ebao.pub.security.AppUser;
import com.ebao.pub.util.BeanUtils;
import com.ebao.pub.util.DateUtils;
import com.ebao.pub.util.Log;

/**
 * <p>
 * Title: SurvivalRepayDSImpl
 * </p>
 * <p>
 * Description:Implement domain service for annuity , maturity search , update
 * </p>
 * <p>
 * Copyright: Copyright (c) 2002
 * </p>
 * <p>
 * Company: eBaoTech
 * </p>
 * <p>
 * Create Time: 2005/04/11
 * </p>
 * 
 * @author simen.li
 */
public class SurvivalRepayDSImpl implements SurvivalRepayService {
  @Resource(name = SurvivalRepaymentTool.BEAN_DEFAULT)
  private SurvivalRepaymentTool survivalRepaymentTool;
  private static final Logger logger = LoggerFactory.getLogger(SurvivalRepayDSImpl.class);
  public SurvivalRepayDSImpl() {
  }

  /**
   * <p>
   * Description: Change survival start pay date
   * </p>
   * 
   * @param itemId
   * @param changeId
   * @param policyChgId
   * @param oprId
   * @throws GenericException <p>
   *           Create time:2005/04/11
   *           </p>
   */
  public void changeSurvivalStartPayDate(Long itemId, Long changeId,
      Long policyChgId, Long oprId) throws GenericException {
    SurvivalDSSp.changeSurvivalStartPayDate(itemId, changeId, policyChgId,
        oprId);
  }

  /**
   * <p>
   * Description:calc maturity amount
   * </p>
   * 
   * @param itemId
   * @return BigDecimal maturity amount
   * @throws GenericException <p>
   *           Create time:2005/04/11
   *           </p>
   */
  public BigDecimal getMaturityAmountByItem(Long itemId)
      throws GenericException {
    return SurvivalDSSp.getMaturityAmountByItem(itemId);
  }

  /**
   * <p>
   * Description:calc net maturity amount
   * </p>
   * 
   * @param itemId
   * @return BigDecimal maturity amount
   * @throws GenericException <p>
   *           Create time:2005/04/11
   *           </p>
   */
  public BigDecimal getNetMaturityAmountByItem(Long itemId)
      throws GenericException {
    BigDecimal netMaturity = new BigDecimal(0);
    CoverageVO policyProductVO = survivalRepaymentTool
        .getCoverageByItemId(itemId);
    PayDue payDue = getLastPayDueBoByPolicyIdAndItemIdAndLiabId(policyProductVO
        .getPolicyId(), policyProductVO.getItemId(), Long
        .valueOf(CodeCst.LIABILITY__MATURITY));
    if (null != payDue) {
      if (null != payDue.getPayId()) {
        BigDecimal totalAdjustAmount = getTotalAdjustAmount(payDue.getItemId(),
            payDue.getPayDueDate());
        netMaturity = getNetMaturityAmount(payDue).add(
            totalAdjustAmount.negate());
      }
    }
    return netMaturity;
  }

  /**
   * <p>
   * Description: find Maturity Adjust info
   * </p>
   * 
   * @param itemId
   * @return MaturityAdjustInfoVO
   * @throws GenericException <p>
   *           Create time:2005/04/11
   *           </p>
   */
  public MaturityAdjustInfoVO findMaturiyInfoByItem(String itemId)
      throws GenericException {
    MaturityAdjustInfoVO infoVO = new MaturityAdjustInfoVO();
    CoverageVO policyProductVO = survivalRepaymentTool.getCoverageByItemId(Long
        .valueOf(Long.parseLong(itemId)));
    PolicyVO policyVO = survivalRepaymentTool
        .getPolicyByPolicyId(policyProductVO.getPolicyId());
    // start handling security
    AppUser user = AppContext.getCurrentUser();
    String organIDs = user.getAccessOrganIDs();
    // 
    // 
    // check if the policy is permitted to handle
    String[] split = StringUtils.split(organIDs, ",");
    int j = 0;
    for (int i = 0; i < split.length; i++) {
      // 
      if (policyVO.getOrganId().toString().equals(split[i])) {
        j++;
      }
    }
    if (j == 0) {
      long errCode = 20910020018L;// no right to handle this policy
      throw new AppException(errCode);
    }
    // end of handling security
    infoVO.setItemId(itemId);
    infoVO.setPolicyNumber(policyVO.getPolicyNumber());
    CustomerVO lifeAssured = policyProductVO.getInsureds().get(0).getInsured()
        .getPerson();
    infoVO.setLifeAssured(lifeAssured.getFirstName());
    PayPlanPayeeVO payeeVO = this.getPayPlanPayeeVO(Long.parseLong(itemId),
        CodeCst.PAY_PLAN_TYPE__MATURITY);
    // PayeeCIVO payeeVO = survivalRepaymentTool.getPayeeByPolicyId(policyVO.getPolicyId(),
    // CodeCst.PAYMENT_TYPE__SUR_MAT_PAY);
    // the payee is individual customer
    CustomerVO indivPayee = survivalRepaymentTool.getIndivCustomer(payeeVO
        .getPayeeId());
    if (indivPayee != null) {
      if (indivPayee.getFirstName() != null) {
        infoVO.setPayee(indivPayee.getFirstName());
      }
    }
    CompanyCustomerVO organPayee = survivalRepaymentTool.getOrganCustomer(payeeVO
        .getPayeeId());
    // the payee is company customer
    if (organPayee != null) {
      if (organPayee.getCompanyName() != null) {
        infoVO.setPayee(organPayee.getCompanyName());
      }
    }
    PolicyHolderVO policyHolder = policyVO.getPolicyHolder();
    if (policyHolder.getParty().isPerson()) {
      infoVO.setPolicyHolder(policyHolder.getPerson().getFirstName());
    } else {
      infoVO.setPolicyHolder(policyHolder.getCompany().getCompanyName());
    }
    // the situation that both indivPayee and organPayee are null, or not
    // null
    // should be controlled by business logic
    infoVO.setAccountInfos(this.getAccountInfo(payeeVO.getPayeeId(), payeeVO
        .getPayeeAccountId()));
    String curPayMode = String.valueOf(payeeVO.getPayMode().longValue());
    infoVO.setCurDisbursementMethod(curPayMode);
    // PayeeCIVO csPayee = survivalRepaymentTool.getPayeeByPolicyId(policyVO.getPolicyId(),
    // CodeCst.PAYMENT_TYPE__CASH_BONUS);
    PayPlanPayeeVO csPayee = this.getPayPlanPayeeVO(Long.parseLong(itemId),
        CodeCst.PAY_PLAN_TYPE__CASH_BONUS);
    if (null != csPayee) {
      String csPayMode = String.valueOf(csPayee.getPayMode().longValue());
      infoVO.setDisbursementMethod(CodeTable.getCodeDesc("T_PAY_MODE",
          csPayMode));
      if (csPayee.getPayMode().intValue() == CodeCst.PAY_MODE__GIRO) {
        infoVO.setBankAccount(survivalRepaymentTool.getBankAccountByAccountId(
            csPayee.getPayeeAccountId()).getBankAccount());
      } else {
        infoVO.setBankAccount("");
      }
    } else {
      // csPayee = survivalRepaymentTool.getPayeeByPolicyId(policyVO.getPolicyId(), "3"
      // //CodeCst.
      // PAYMENT_TYPE__SB
      // );
      csPayee = this.getPayPlanPayeeVO(Long.parseLong(itemId), "3");
      if (null != csPayee) {
        String csPayMode = String.valueOf(csPayee.getPayMode().longValue());
        infoVO.setDisbursementMethod(CodeTable.getCodeDesc("T_PAY_MODE",
            csPayMode));
        if (csPayee.getPayMode().intValue() == CodeCst.PAY_MODE__GIRO) {
          infoVO.setBankAccount(survivalRepaymentTool
              .getBankAccountByAccountId(csPayee.getPayeeAccountId())
              .getBankAccount());
        } else {
          infoVO.setBankAccount("");
        }
      }
    }
    if (null != payeeVO.getPayeeAccountId()) {
      infoVO.setAccountId(String.valueOf(payeeVO.getPayeeAccountId()));
      infoVO.setCheckedAccountId(String.valueOf(payeeVO.getPayeeAccountId()));
    } else {
      infoVO.setAccountId("");
      infoVO.setCheckedAccountId("");
    }
    CustomerVO customerVO = survivalRepaymentTool.getIndivCustomer(payeeVO
        .getPayeeId());
    if (customerVO == null) {
      CompanyCustomerVO compayHolder = survivalRepaymentTool.getOrganCustomer(payeeVO
          .getPayeeId());
      infoVO.setIdNumber(compayHolder.getRegisterCode());
    } else {
      infoVO.setIdNumber(customerVO.getCertiCode());
    }
    PayDue payDue = getLastPayDueBoByPolicyIdAndItemIdAndLiabId(policyVO
        .getPolicyId(), policyProductVO.getItemId(), Long
        .valueOf(CodeCst.LIABILITY__MATURITY));
    if (null != payDue) {
      if (null != payDue.getPayId()) {
        infoVO.setCaseNumber(payDue.getCaseNumber());
        infoVO.setBatchNumber(payDue.getBatchNumber());
        infoVO.setAdjustMaturityAmount(new BigDecimal(0));
        BigDecimal totalAdjustAmount = getTotalAdjustAmount(payDue.getItemId(),
            payDue.getPayDueDate());
        infoVO.setPreviousAdjustMaturityAmount(totalAdjustAmount);
        infoVO.setMaturityInterest(payDue.getMbInterest());
        infoVO.setWithHoldingTax(payDue.getWithholdTax());
        // update by shirley.miao for GEL00039784
        BigDecimal netMaturityAmount = getNetMaturityAmount(payDue)
        /* .add(totalAdjustAmount.negate()) */;
        infoVO.setNetMaturiryAmount(netMaturityAmount);
        infoVO.setRevisedMaturityAmount(netMaturityAmount.add(
            payDue.getMbInterest()).add(payDue.getWithholdTax().negate()).add(
            totalAdjustAmount));
        PayDueVO payDueVO = new PayDueVO();
        BeanUtils.copyProperties(payDueVO, payDue);
        infoVO.setPayDue(payDueVO);
        infoVO.setPayeeId(String.valueOf(payeeVO.getPayeeId().longValue()));
        infoVO.setDueDate(DateUtils.date2String(payDue.getPayDueDate()));
        infoVO.setChequePrintDate(DateUtils.date2String(payDue
            .getChkPrintDate()));
        infoVO.setPayId(payDue.getPayId());
      }
    }
    infoVO.setPayeeId(String.valueOf(payeeVO.getPayeeId().longValue()));
    infoVO.setItemId(itemId);
    return infoVO;
  }

  /**
   * <p>
   * Description:find annuity policy info
   * </p>
   * 
   * @param policyNumber
   * @return AnnuityUpdateInfoVO
   * @throws GenericException <p>
   *           Create time:2005/04/11
   *           </p>
   */
  @PrdAPIUpdate
  public AnnuityUpdateInfoVO findAnnuityInfoByPolicyCode(String policyNumber)
      throws GenericException {
    Long policyId = null;
    PolicyVO policyVO = survivalRepaymentTool
        .getPolicyVOByPolicyNumber(policyNumber);
    if (null == policyVO) {
      long invalidPolicyErr = 22210420006L;
      throw new AppException(invalidPolicyErr);
    }
    if (policyVO.getSuspend().equalsIgnoreCase("Y")) {
      throw new AppException(20910020009L);
    }
    // start handling security
    AppUser user = AppContext.getCurrentUser();
    String organIDs = user.getAccessOrganIDs();
    // 
    // 
    // check if the policy is permitted to handle
    String[] split = StringUtils.split(organIDs, ",");
    int j = 0;
    for (int i = 0; i < split.length; i++) {
      // 
      if (policyVO.getOrganId().toString().equals(split[i])) {
        j++;
      }
    }
    if (j == 0) {
      long errCode = 20910020018L;// no right to handle this policy
      throw new AppException(errCode);
    }
    // end of handling security
    if (policyVO.getRiskStatus().intValue() != CodeCst.LIABILITY_STATUS__IN_FORCE) {
      long noInforcePolicyErr = 20910020013L;
      throw new AppException(noInforcePolicyErr);
    }
    CoverageVO policyProductVO = survivalRepaymentTool
        .getPolicyProductByPolicyId(policyVO.getPolicyId());
    Integer liabId = SurvivalDSSp
        .isSurvivalProduct(policyProductVO.getItemId());
    boolean isVaPolicy = policyCI.isVAPolicy(policyVO.getPolicyId());
    if (liabId.intValue() != CodeCst.LIABILITY__ANNUNITY && !isVaPolicy) {
      long noAnnuityPolicyErr = 20910020012L;
      throw new AppException(noAnnuityPolicyErr);
    }
    policyId = policyVO.getPolicyId();
    AnnuityUpdateInfoVO infoVO = new AnnuityUpdateInfoVO();
    infoVO.setPolicyCode(policyNumber);
    PolicyHolderVO policyHolder = policyVO.getPolicyHolder();
    if (policyHolder.getParty().isPerson()) {
      if (policyHolder.getPerson().getFirstName() != null) {
        infoVO.setPolicyHolder(policyHolder.getPerson().getFirstName());
      }
    } else {
      if (policyHolder.getCompany().getCompanyName() != null) {
        infoVO.setPolicyHolder(policyHolder.getCompany().getCompanyName());
      }
    }
    infoVO.setPolicyStatus(CodeTable.getCodeDesc("T_LIABILITY_STATUS", String
        .valueOf(policyVO.getRiskStatus().intValue())));
    infoVO.setValidDate(DateUtils.date2String(policyVO.getInceptionDate()));
    if (isVaPolicy) {
      infoVO.setIsVa("Y");
      PayPlan vaPayPlan = PayPlan.findByItemIdAndPayPlanType(policyProductVO
          .getItemId(), CodeCst.PAY_PLAN_TYPE__VA);
      PayPlanPayeeVO vaPayeeVO = this.getPayPlanPayeeVO(vaPayPlan.getPlanId());
      PayPlan gmwbPlan = PayPlan.findByItemIdAndPayPlanType(policyProductVO
          .getItemId(), CodeCst.PAY_PLAN_TYPE__GMWB);
      if (vaPayPlan.getPayStatus() != null
          && !vaPayPlan.getPayStatus().equals("0")) {
        infoVO.setIsVaStart("Y");
      }
      infoVO.setVaPayPeriod(vaPayPlan.getPayPeriod());
      infoVO.setVaPayYear(vaPayPlan.getPayYear().toString());
      infoVO.setVaEndPeriod(vaPayPlan.getEndPeriod());
      infoVO.setVaEndYear(vaPayPlan.getEndYear().toString());
      infoVO.setVaPayType(vaPayPlan.getPlanFreq());
      if (this.getProductService().getProduct(
          Long.valueOf(policyProductVO.getProductId().intValue()),policyService.getActiveVersionDate(policyProductVO))
          .isPayEnsure()) {
        if (vaPayPlan.getGuaranteePeriod() != null) {
          infoVO.setVaPayEnsure(vaPayPlan.getGuaranteePeriod().toString());
        } else {
          infoVO.setVaPayEnsure("0");
        }
      } else {
        infoVO.setVaPayEnsure(null);
      }
      infoVO.setVaPaymentMethod(CodeTable.getCodeDesc("T_PAY_TYPE", String
          .valueOf(vaPayPlan.getPlanFreq())));
      String curVaPayMode = String.valueOf(vaPayeeVO.getPayMode().longValue());
      infoVO.setVaPayMode(curVaPayMode);
      infoVO.setCurVaPayMode(curVaPayMode);
      if (null != vaPayeeVO.getPayeeAccountId()) {
        infoVO.setVaBankAccount(survivalRepaymentTool
            .getBankAccountByAccountId(vaPayeeVO.getPayeeAccountId())
            .getBankAccount());
        infoVO.setVaAccountId(String.valueOf(vaPayeeVO.getPayeeAccountId()));
        infoVO.setVaCheckedAccountId(String.valueOf(vaPayeeVO
            .getPayeeAccountId()));
      } else {
        infoVO.setVaAccountId("");
        infoVO.setVaCheckedAccountId("");
      }
      infoVO.setVaAccountInfos(getAccountInfo(vaPayeeVO.getPayeeId(), vaPayeeVO
          .getPayeeAccountId()));
      infoVO.setVaPayeeId(String.valueOf(vaPayeeVO.getPayeeId().longValue()));
      // PayDue vaPayDue = PayDue.getLastPayDueByPolicyId(policyId); // the
      // payee is individual customer
      CustomerVO indivPayee = survivalRepaymentTool.getIndivCustomer(vaPayeeVO
          .getPayeeId());
      if (indivPayee != null) {
        if (indivPayee.getFirstName() != null) {
          infoVO.setVaPayeeName(indivPayee.getFirstName());
          infoVO.setVaPayeeIdType(CodeTable.getCodeDesc("T_CERTI_TYPE", String
              .valueOf(indivPayee.getCertiType().longValue())));
          infoVO.setVaPayeeIdNumber(indivPayee.getCertiCode());
          // if t_pay_plan.manual_indi is null follow rules
          String manualIndi = vaPayPlan.getManualIndi();
          if (manualIndi == null) {
            infoVO.setVaManual(isManualIndicator(indivPayee, policyVO));
          } else {
            infoVO.setVaManual(manualIndi);
          }
        }
      }
      CompanyCustomerVO organPayee = survivalRepaymentTool.getOrganCustomer(vaPayeeVO
          .getPayeeId());
      // the payee is company customer,IdType,IdNumber will not be null changed
      // by shirley.miao for GEL00029510
      if (organPayee != null) {
        if (organPayee.getCompanyName() != null) {
          infoVO.setVaPayeeName(organPayee.getCompanyName());
          infoVO.setVaPayeeIdType(CodeTable.getCodeDesc("T_REGISTER_TYPE",
              String.valueOf(organPayee.getRegisterType())));
          infoVO.setVaPayeeIdNumber(organPayee.getRegisterCode());
          // if t_pay_plan.manual_indi is null follow rules
          String manualIndi = vaPayPlan.getManualIndi();
          if (manualIndi == null) {
            infoVO.setVaManual(isManualIndicator(organPayee, policyVO));
          } else {
            infoVO.setVaManual(manualIndi);
          }
        }
      }
      // the situation that both indivPayee and organPayee are null, or
      // not null
      // should be controlled by business logic
      Date curDischargeDate = vaPayPlan.getDischargeDate();
      Date lastChargeDate = this.getAnnuityLastDischargeDate(vaPayPlan);
      infoVO.setVaDischargeVoucher("N"); // default discharge Voucher
      infoVO.setVaCancelLastDischarge("N");
      infoVO.setVaNextDischargeDate(DateUtils.date2String(curDischargeDate));
      infoVO.setVaLastDischargeDate(DateUtils.date2String(lastChargeDate));
      infoVO.setNextVaDate(DateUtils.date2String(vaPayPlan.getPayDueDate()));
      infoVO.setItemId(String.valueOf(vaPayPlan.getItemId().longValue()));
      // infoVO.setPayDue(vaPayDue);
      infoVO.setVaPlanId(vaPayPlan.getPlanId());
      if (gmwbPlan != null && gmwbPlan.getPlanId() != null) {
        infoVO.setIsGmwb("Y");
        if (gmwbPlan.getPayStatus() != null
            && !gmwbPlan.getPayStatus().equals("0")) {
          infoVO.setIsGmwbStart("Y");
        }
        PayPlanPayeeVO gmwbPayeeVO = getPayPlanPayeeVO(gmwbPlan.getPlanId());
        infoVO.setGmwbPayPeriod(gmwbPlan.getPayPeriod());
        infoVO.setGmwbPayYear(gmwbPlan.getPayYear().toString());
        infoVO.setGmwbEndPeriod(gmwbPlan.getEndPeriod());
        infoVO.setGmwbEndYear(gmwbPlan.getEndYear().toString());
        infoVO.setGmwbPayType(gmwbPlan.getPlanFreq());
        infoVO.setGmwbPaymentMethod(CodeTable.getCodeDesc("T_PAY_TYPE", String
            .valueOf(gmwbPlan.getPlanFreq())));
        String curGmwbPayMode = String.valueOf(gmwbPayeeVO.getPayMode()
            .longValue());
        infoVO.setGmwbPayMode(curGmwbPayMode);
        infoVO.setCurGmwbPayMode(curGmwbPayMode);
        if (null != gmwbPayeeVO.getPayeeAccountId()) {
          infoVO.setGmwbBankAccount(survivalRepaymentTool
              .getBankAccountByAccountId(gmwbPayeeVO.getPayeeAccountId())
              .getBankAccount());
          infoVO.setGmwbAccountId(String.valueOf(gmwbPayeeVO
              .getPayeeAccountId()));
          infoVO.setGmwbCheckedAccountId(String.valueOf(gmwbPayeeVO
              .getPayeeAccountId()));
        } else {
          infoVO.setGmwbAccountId("");
          infoVO.setGmwbCheckedAccountId("");
        }
        infoVO.setGmwbAccountInfos(getAccountInfo(gmwbPayeeVO.getPayeeId(),
            gmwbPayeeVO.getPayeeAccountId()));
        infoVO.setGmwbPayeeId(String.valueOf(gmwbPayeeVO.getPayeeId()
            .longValue()));
        // PayDue vaPayDue = PayDue.getLastPayDueByPolicyId(policyId); // the
        // payee is individual customer
        CustomerVO indivGmwbPayee = survivalRepaymentTool
            .getIndivCustomer(gmwbPayeeVO.getPayeeId());
        if (indivGmwbPayee != null) {
          if (indivGmwbPayee.getFirstName() != null) {
            infoVO.setGmwbPayeeName(indivGmwbPayee.getFirstName());
            infoVO.setGmwbPayeeIdType(CodeTable.getCodeDesc("T_CERTI_TYPE",
                String.valueOf(indivGmwbPayee.getCertiType().longValue())));
            infoVO.setGmwbPayeeIdNumber(indivGmwbPayee.getCertiCode());
            // if t_pay_plan.manual_indi is null follow rules
            String manualIndi = gmwbPlan.getManualIndi();
            if (manualIndi == null) {
              infoVO.setGmwbManual(isManualIndicator(indivGmwbPayee, policyVO));
            } else {
              infoVO.setGmwbManual(manualIndi);
            }
          }
        }
        CompanyCustomerVO organGmwbPayee = survivalRepaymentTool
            .getOrganCustomer(gmwbPayeeVO.getPayeeId());
        // the payee is company customer,IdType,IdNumber will not be null
        // changed by shirley.miao for GEL00029510
        if (organGmwbPayee != null) {
          if (organGmwbPayee.getCompanyName() != null) {
            infoVO.setGmwbPayeeName(organGmwbPayee.getCompanyName());
            infoVO.setGmwbPayeeIdType(CodeTable.getCodeDesc("T_REGISTER_TYPE",
                String.valueOf(organGmwbPayee.getRegisterType())));
            infoVO.setGmwbPayeeIdNumber(organGmwbPayee.getRegisterCode());
            // if t_pay_plan.manual_indi is null follow rules
            String manualIndi = gmwbPlan.getManualIndi();
            if (manualIndi == null) {
              infoVO.setGmwbManual(isManualIndicator(organGmwbPayee, policyVO));
            } else {
              infoVO.setGmwbManual(manualIndi);
            }
          }
        }
        infoVO.setNextGmwbDate(DateUtils.date2String(gmwbPlan.getPayDueDate()));
        // infoVO.setPayDue(vaPayDue);
        infoVO.setGmwbPlanId(gmwbPlan.getPlanId());
      }
    } else {
      PayPlan payPlan = new PayPlan();
      PayPlanPayeeVO payeeVO = new PayPlanPayeeVO();
      payPlan.findByItemIdAndLiabId(policyProductVO.getItemId(), Integer
          .valueOf(CodeCst.LIABILITY__ANNUNITY));
      payeeVO = this.getPayPlanPayeeVO(payPlan.getPlanId());
      infoVO.setDischargeVoucher("N"); // default discharge Voucher
      infoVO.setCancelLastDischarge("N");
      // change from Disbursement Method to Annuity Frequency
      if (policyProductVO.getAnnuityPayPlan() != null) {
        infoVO.setPaymentMethod(CodeTable.getCodeDesc("T_PAY_TYPE", String
            .valueOf(policyProductVO.getAnnuityPayPlan().getPlanFreq())));
      }
      String curPayMode = String.valueOf(payeeVO.getPayMode().longValue());
      infoVO.setDisbursementMethod(curPayMode);
      // infoVO.setDisbursementMethod(CodeTable.getCodeDesc("T_PAY_MODE",
      // curPayMode));
      infoVO.setCurDisbursementMethod(curPayMode);
      if (null != payeeVO.getPayeeAccountId()) {
        infoVO.setBankAccount(survivalRepaymentTool.getBankAccountByAccountId(
            payeeVO.getPayeeAccountId()).getBankAccount());
        infoVO.setAccountId(String.valueOf(payeeVO.getPayeeAccountId()));
        infoVO.setCheckedAccountId(String.valueOf(payeeVO.getPayeeAccountId()));
      } else {
        infoVO.setAccountId("");
        infoVO.setCheckedAccountId("");
      }
      infoVO.setAccountInfos(getAccountInfo(payeeVO.getPayeeId(), payeeVO
          .getPayeeAccountId()));
      infoVO.setPayeeId(String.valueOf(payeeVO.getPayeeId().longValue()));
      if (null != payPlan && null != payPlan.getPlanId()) {
        PayDue payDue = TPayDueDelegateExt.findLastPayDueByPolicyId(policyId);
        Log
            .debug(getClass(), "before tpp, plan_id is :!"
                + payPlan.getPlanId());
        // the payee is individual customer
        CustomerVO indivPayee = survivalRepaymentTool.getIndivCustomer(payeeVO
            .getPayeeId());
        if (indivPayee != null) {
          if (indivPayee.getFirstName() != null) {
            infoVO.setPayeeName(indivPayee.getFirstName());
            infoVO.setPayeeIdType(CodeTable.getCodeDesc("T_CERTI_TYPE", String
                .valueOf(indivPayee.getCertiType().longValue())));
            infoVO.setPayeeIdNumber(indivPayee.getCertiCode());
            // if t_pay_plan.manual_indi is null follow rules
            String manualIndi = payPlan.getManualIndi();
            if (manualIndi == null) {
              infoVO.setManual(isManualIndicator(indivPayee, policyVO));
            } else {
              infoVO.setManual(manualIndi);
            }
          }
        }
        CompanyCustomerVO organPayee = survivalRepaymentTool.getOrganCustomer(payeeVO
            .getPayeeId());
        // the payee is company customer,IdType,IdNumber will not be null
        // changed by shirley.miao for GEL00029510
        if (organPayee != null) {
          if (organPayee.getCompanyName() != null) {
            infoVO.setPayeeName(organPayee.getCompanyName());
            infoVO.setPayeeIdType(CodeTable.getCodeDesc("T_REGISTER_TYPE",
                String.valueOf(organPayee.getRegisterType())));
            infoVO.setPayeeIdNumber(organPayee.getRegisterCode());
            // if t_pay_plan.manual_indi is null follow rules
            String manualIndi = payPlan.getManualIndi();
            if (manualIndi == null) {
              infoVO.setManual(isManualIndicator(organPayee, policyVO));
            } else {
              infoVO.setManual(manualIndi);
            }
          }
        }
        // the situation that both indivPayee and organPayee are null, or
        // not null
        // should be controlled by business logic
        Date curDischargeDate = payPlan.getDischargeDate();
        Date lastChargeDate = this.getAnnuityLastDischargeDate(payPlan);
        infoVO.setNextDischargeDate(DateUtils.date2String(curDischargeDate));
        infoVO.setLastDischargeDate(DateUtils.date2String(lastChargeDate));
        infoVO.setNextAnnuityDate(DateUtils
            .date2String(payPlan.getPayDueDate()));
        // for GEL00023086,when exist record in t_pay_plan,not sure there is an
        // according record in t_pay_due,so we should derive vaule from payPlan
        infoVO.setItemId(String.valueOf(payPlan.getItemId().longValue()));
        PayDueVO payDueVO = new PayDueVO();
        BeanUtils.copyProperties(payDueVO, payDue);
        infoVO.setPayDue(payDueVO);
        infoVO.setPlanId(payPlan.getPlanId());
      } else {
        Date nextDueDate = SurvivalDSSp.getAnnStartPayDate(policyProductVO
            .getItemId());
        infoVO.setItemId(String.valueOf(policyProductVO.getItemId()));
        infoVO.setNextAnnuityDate(DateUtils.date2String(nextDueDate));
        // the payee is individual customer
        CustomerVO indivPayee = survivalRepaymentTool.getIndivCustomer(payeeVO
            .getPayeeId());
        if (indivPayee != null) {
          if (indivPayee.getFirstName() != null) {
            infoVO.setPayeeName(indivPayee.getFirstName());
            infoVO.setPayeeIdType(CodeTable.getCodeDesc("T_CERTI_TYPE", String
                .valueOf(indivPayee.getCertiType().longValue())));
            infoVO.setPayeeIdNumber(indivPayee.getCertiCode());
          }
        }
        CompanyCustomerVO organPayee = survivalRepaymentTool.getOrganCustomer(payeeVO
            .getPayeeId());
        // the payee is company customer,IdType,IdNumber will not be null
        // changed by shirley.miao for GEL00029510
        if (organPayee != null) {
          if (organPayee.getCompanyName() != null) {
            infoVO.setPayeeName(organPayee.getCompanyName());
            infoVO.setPayeeIdType(CodeTable.getCodeDesc("T_REGISTER_TYPE",
                String.valueOf(organPayee.getRegisterType())));
            infoVO.setPayeeIdNumber(organPayee.getRegisterCode());
          }
        }
        Date nextChargeDate = SurvivalDSSp.getFirstChargeDate(policyProductVO
            .getItemId());
        infoVO.setNextDischargeDate(DateUtils.date2String(nextChargeDate));
      }
    }
    infoVO.setAssignee(survivalRepaymentTool
        .getAbsoluteAssigneeNameByPolicyId(policyVO.getPolicyId()));
    return infoVO;
  }

  /**
   * <p>
   * Description get account info
   * </p>
   * 
   * @param payeeId
   * @param accountId
   * @return
   * @throws GenericException <p>
   *           Create time:2005/04/11
   *           </p>
   */
  private List<PayeeAccountInfoVO> getAccountInfo(Long payeeId, Long accountId)
      throws GenericException {
    BankAccountVO[] bankAccountVOs = survivalRepaymentTool
        .getAllBankAccountByCustomerId(payeeId);
    List<PayeeAccountInfoVO> allAccountInfoLst = new ArrayList<PayeeAccountInfoVO>();
    PayeeAccountInfoVO accountInfo = null;
    for (int i = 0; i < bankAccountVOs.length; i++) {
      accountInfo = new PayeeAccountInfoVO();
      accountInfo.setAccountId(String.valueOf(bankAccountVOs[i].getAccountId()
          .longValue()));
      accountInfo.setAccountNo(bankAccountVOs[i].getBankAccount());
      accountInfo.setAccountHolder(bankAccountVOs[i].getAccoName());
      accountInfo.setBankName(survivalRepaymentTool.getBankVOByBandCode(
          bankAccountVOs[i].getBankCode()).getBankName());
      allAccountInfoLst.add(accountInfo);
      if (bankAccountVOs[i].getAccountId().equals(accountId)) {
        accountInfo.setChecked(true);
      } else {
        accountInfo.setChecked(false);
      }
    }
    return allAccountInfoLst;
  }

  /**
   * <p>
   * Description: judge whether or not need set manual Indicator
   * </p>
   * 
   * @param indivCustomerVO
   * @return true is need set manual indicator
   * @throws GenericException <p>
   *           Create time:2005/04/11
   *           </p>
   */
  public String isManualIndicator(CustomerVO indivCustomerVO, PolicyVO policyVO)
      throws GenericException {
    String retValue = "N";
    AddressVO addressVO = survivalRepaymentTool.getAddress(policyVO
        .getPolicyHolder().getAddressId());
    // PartyAddressVO
    // addressVO=survivalRepaymentTool.getPartyAddressbyPartyId(indivCustomerVO.getCustomerId()
    // );
    if (indivCustomerVO.getAccidentStatus().equals(
        CodeCst.ACCIDENT_STATUS__DEATH)) {
      retValue = "Y";
    } else if (null != indivCustomerVO.getBankruptcyStatus()
        && indivCustomerVO.getBankruptcyStatus().equalsIgnoreCase("Y")) {
      retValue = "Y";
    } else if (!policyVO.getAssignmentType().equals(
        CodeCst.ASSIGNMENT_TYPE__NON)) {
      retValue = "Y";
    } else if (null != addressVO.getForeignIndi()
        && !addressVO.getForeignIndi().equalsIgnoreCase("N")) {
      retValue = "Y";
    } else if (null != addressVO.getAddressStatus()
        && !addressVO.getAddressStatus().equals("1")) {
      retValue = "Y";
    }
    return retValue;
  }

  /**
   * <p>
   * Description: judge whether or not need set manual Indicator
   * </p>
   * 
   * @param CompanyCustomerVO
   * @return true is need set manual indicator
   * @throws GenericException <p>
   *           Create time:2005/09/21
   *           </p>
   */
  public String isManualIndicator(CompanyCustomerVO organCustomerVO, PolicyVO policyVO)
      throws GenericException {
    String retValue = "N";
    AddressVO addressVO = survivalRepaymentTool.getAddress(policyVO
        .getPolicyHolder().getAddressId());
    // PartyAddressVO
    // addressVO=survivalRepaymentTool.getPartyAddressbyPartyId(indivCustomerVO.getCustomerId()
    // );
    /*    */
    if (null != organCustomerVO.getBankruptcyStatus()
        && organCustomerVO.getBankruptcyStatus().equalsIgnoreCase("Y")) {
      retValue = "Y";
    } else if (!policyVO.getAssignmentType().equals(
        CodeCst.ASSIGNMENT_TYPE__NON)) {
      retValue = "Y";
    } else if (null != addressVO.getForeignIndi()
        && !addressVO.getForeignIndi().equalsIgnoreCase("N")) {
      retValue = "Y";
    } else if (null != addressVO.getAddressStatus()
        && !addressVO.getAddressStatus().equals("1")) {
      retValue = "Y";
    } /*
     * else if (survivalRepaymentTool.isForeigner(organCustomerVO.getCompanyId())) {
     * retValue = "Y"; }
     */
    return retValue;
  }

  /**
   * <p>
   * Description update Annuity Info--before draw
   * </p>
   * 
   * @param tPayDue
   * @param payMode
   * @param accountId
   * @param dischargeVoucher
   * @param cancelLastDischarge
   * @throws ObjectNotFoundException
   * @throws GenericException <p>
   *           Create time:2005/04/11
   *           </p>
   */
  public void updateAnnuityInfo(String itemId, String payMode, String accountId)
      throws ObjectNotFoundException, GenericException {
    CoverageVO policyProductVO = survivalRepaymentTool.getCoverageByItemId(Long
        .valueOf(Long.parseLong(itemId)));
    PayPlanPayeeVO payeeVO = this.getPayPlanPayeeVO(Long.parseLong(itemId),
        CodeCst.PAY_PLAN_TYPE__ANNUITY);
    // PayeeCIVO payeeVO = survivalRepaymentTool.getPayeeByPolicyId(policyProductVO
    // .getPolicyId(), CodeCst.PAYMENT_TYPE__SUR_ANNU_PAY);
    // register new service
    CSCIVO csVO = survivalRepaymentTool.registerChangeAndLockPolicy(
        policyProductVO.getPolicyId(), "302");
    if (null == payMode || "".equals(payMode)) {
      long errCode = 20910020015L;
      throw new AppException(errCode);
    }
    payeeVO.setPayMode(Integer.valueOf(payMode));
    if (payMode.equals(CodeCst.PAYMODE_TYPE__GIRO)) {
      if (null == accountId || "".equals(accountId)) {
        long noBankAccount = 20910020010L;
        throw new AppException(noBankAccount);
      } else {
        payeeVO.setPayeeAccountId(Long.valueOf(accountId));
      }
    }
    modifyPayee(csVO.getMasterChgId(), csVO.getPolicyChgId(), payeeVO);
    // effective
    survivalRepaymentTool.effectiveChangeAndUnlockPolicy(csVO.getPolicyChgId());
  }

  /**
   * <p>
   * Description update Variable Annuity Infomation
   * </p>
   * 
   * @param annuityUpdateVO
   * @throws ObjectNotFoundException
   * @throws GenericException <p>
   *           Create time:2008/09/22
   *           </p>
   */
  public void updateVaInfo(AnnuityUpdateInfoVO annuityUpdateVO)
      throws Exception {
    if (null == annuityUpdateVO.getCurVaPayMode()
        || "".equals(annuityUpdateVO.getCurVaPayMode())) {
      long errCode = 20910020015L;
      throw new AppException(errCode);
    }
    PayPlan vaPayPlan = new PayPlan();
    vaPayPlan.retrieve(annuityUpdateVO.getVaPlanId());
    // generate policy change to dump table
    CSCIVO csVO = survivalRepaymentTool.registerChangeAndLockPolicy(vaPayPlan
        .getPolicyId(), "302");
    PayPlanPayeeVO planPayeeVO = getPayPlanPayeeVO(vaPayPlan.getPlanId());
    CommonDumper.srcToLogByRecord(csVO.getMasterChgId(), csVO.getPolicyChgId(),
        "T_PAY_PLAN", vaPayPlan.getPlanId(), CommonDumper.OPER_TYPE_UPDATE,
        Long.valueOf(AppContext.getCurrentUser().getUserId()));
    if (annuityUpdateVO.getIsVaStart().equals("N")) {
      vaPayPlan.setPayPeriod(annuityUpdateVO.getVaPayPeriod());
      vaPayPlan.setPayYear(Integer.valueOf(annuityUpdateVO.getVaPayYear()));
      vaPayPlan.setEndPeriod(annuityUpdateVO.getVaEndPeriod());
      vaPayPlan.setEndYear(Integer.valueOf(annuityUpdateVO.getVaEndYear()));
      vaPayPlan.setPlanFreq(annuityUpdateVO.getVaPayType());
      if (annuityUpdateVO.getVaPayEnsure() != null
          && !annuityUpdateVO.getVaPayEnsure().equals("")) {
        vaPayPlan.setGuaranteePeriod(Integer.valueOf(annuityUpdateVO
            .getVaPayEnsure()));
      } else {
        vaPayPlan.setGuaranteePeriod(null);
      }
      vaPayPlan.update();
      // Recalculate for the VA plan start date and end date after update
      SurvivalDSSp.updatePlanInfo(vaPayPlan.getItemId(), csVO.getMasterChgId(),
          csVO.getPolicyChgId(), vaPayPlan.getPayPlanType());
    } else {
      if (annuityUpdateVO.getVaDischargeVoucher().equalsIgnoreCase("Y")
          && annuityUpdateVO.getVaCancelLastDischarge().equalsIgnoreCase("Y")) {
        throw new AppException(20910020007L);
      } else if (annuityUpdateVO.getVaDischargeVoucher().equalsIgnoreCase("Y")) {
        Date nextChargeDate = this.getAnnuityNextDischargeDate(vaPayPlan);
        vaPayPlan.setDischargeDate(nextChargeDate);
        vaPayPlan.setDischargeIndi("Y");
      } else if (annuityUpdateVO.getVaCancelLastDischarge().equalsIgnoreCase(
          "Y")) {
        Date lastChargeDate = this.getAnnuityLastDischargeDate(vaPayPlan);
        vaPayPlan.setDischargeIndi("N");
        if (null != lastChargeDate) {
          vaPayPlan.setDischargeDate(lastChargeDate);
        }
      }
      PolicyVO policyVO = survivalRepaymentTool.getPolicyByPolicyId(vaPayPlan
          .getPolicyId());
      String orgin_manual = vaPayPlan.getManualIndi();
      // if user changed
      if (!annuityUpdateVO.getVaManual().equals(orgin_manual)) {
        vaPayPlan.setManualIndi(annuityUpdateVO.getVaManual());
      } else {// if not change
        // and orgin is NOT "Y"
        if (!annuityUpdateVO.getVaManual().equals("Y")) {
          // the payee is individual customer
          CustomerVO indivPayee = survivalRepaymentTool
              .getIndivCustomer(planPayeeVO.getPayeeId());
          if (indivPayee != null) {
            if (indivPayee.getFirstName() != null) {
              vaPayPlan.setManualIndi(isManualIndicator(indivPayee, policyVO));
            }
          }
          CompanyCustomerVO organPayee = survivalRepaymentTool
              .getOrganCustomer(planPayeeVO.getPayeeId());
          // the payee is company customer,IdType,IdNumber will be null
          if (organPayee != null) {
            if (organPayee.getCompanyName() != null) {
              vaPayPlan.setManualIndi(isManualIndicator(organPayee, policyVO));
            }
          }
        }
      }
      vaPayPlan.update();
    }
    planPayeeVO.setItemId(Long.valueOf(annuityUpdateVO.getItemId()));
    if (Integer.valueOf(annuityUpdateVO.getCurVaPayMode()).equals(
        CodeCst.PAY_MODE__CHEQUE)
        && annuityUpdateVO.getVaManual().equals("Y")) {
      planPayeeVO.setPayMode(CodeCst.PAY_MODE__MAN_CHEQUE);
    } else {
      planPayeeVO
          .setPayMode(Integer.valueOf(annuityUpdateVO.getCurVaPayMode()));
    }
    if (annuityUpdateVO.getCurVaPayMode().equals(CodeCst.PAYMODE_TYPE__GIRO)) {
      if (null == annuityUpdateVO.getVaAccountId()
          || "".equals(annuityUpdateVO.getVaAccountId())) {
        long noBankAccount = 20910020010L;
        throw new AppException(noBankAccount);
      } else {
        planPayeeVO.setPayeeAccountId(Long.valueOf(annuityUpdateVO
            .getVaAccountId()));
      }
    }
    Dumper.pSrcToLogByRecord(csVO.getMasterChgId(), csVO.getPolicyChgId(),
        "T_PAY_PLAN_PAYEE", planPayeeVO.getListId().longValue(),
        CodeCst.LOG_TYPE__LOG_TYPE_COMMON,
        CodeCst.DATA_OPER_TYPE__DUMP_OPER_UPD, AppContext.getCurrentUser()
            .getUserId());
    PayPlanPayee payPlanPayee = new PayPlanPayee();
    BeanUtils.copyProperties(payPlanPayee, planPayeeVO);
    payPlanPayee.store();
    if (annuityUpdateVO.getIsGmwb().equals("Y")) {
      PayPlan gmwbPlan = new PayPlan();
      gmwbPlan.retrieve(annuityUpdateVO.getGmwbPlanId());
      PayPlanPayeeVO gmwbPayeeVO = getPayPlanPayeeVO(gmwbPlan.getPlanId());
      if (annuityUpdateVO.getIsGmwbStart().equals("N")) {
        CommonDumper.srcToLogByRecord(csVO.getMasterChgId(), csVO
            .getPolicyChgId(), "T_PAY_PLAN", gmwbPlan.getPlanId(),
            CommonDumper.OPER_TYPE_UPDATE, Long.valueOf(AppContext
                .getCurrentUser().getUserId()));
        gmwbPlan.setPayPeriod(annuityUpdateVO.getGmwbPayPeriod());
        gmwbPlan.setPayYear(Integer.valueOf(annuityUpdateVO.getGmwbPayYear()));
        gmwbPlan.setEndPeriod(annuityUpdateVO.getGmwbEndPeriod());
        gmwbPlan.setEndYear(Integer.valueOf(annuityUpdateVO.getGmwbEndYear()));
        gmwbPlan.setPlanFreq(annuityUpdateVO.getGmwbPayType());
        gmwbPlan.update();
        // Recalculate for the GMWB plan start date and end date
        SurvivalDSSp.updatePlanInfo(gmwbPlan.getItemId(),
            csVO.getMasterChgId(), csVO.getPolicyChgId(), gmwbPlan
                .getPayPlanType());
      }
      gmwbPayeeVO.setPayMode(Integer.valueOf(annuityUpdateVO
          .getCurGmwbPayMode()));
      if (annuityUpdateVO.getCurGmwbPayMode()
          .equals(CodeCst.PAYMODE_TYPE__GIRO)) {
        if (null == annuityUpdateVO.getGmwbAccountId()
            || "".equals(annuityUpdateVO.getGmwbAccountId())) {
          long noBankAccount = 20910020010L;
          throw new AppException(noBankAccount);
        } else {
          gmwbPayeeVO.setPayeeAccountId(Long.valueOf(annuityUpdateVO
              .getGmwbAccountId()));
        }
      }
      Dumper.pSrcToLogByRecord(csVO.getMasterChgId(), csVO.getPolicyChgId(),
          "T_PAY_PLAN_PAYEE", gmwbPayeeVO.getListId().longValue(),
          CodeCst.LOG_TYPE__LOG_TYPE_COMMON,
          CodeCst.DATA_OPER_TYPE__DUMP_OPER_UPD, AppContext.getCurrentUser()
              .getUserId());
      PayPlanPayee gmwbPayee = new PayPlanPayee();
      BeanUtils.copyProperties(gmwbPayee, gmwbPayeeVO);
      gmwbPayee.store();
    }
    survivalRepaymentTool.effectiveChangeAndUnlockPolicy(csVO.getPolicyChgId());
  }

  /**
   * <p>
   * Description update Annuity Info
   * </p>
   * 
   * @param tPayDue
   * @param payMode
   * @param accountId
   * @param dischargeVoucher
   * @param cancelLastDischarge
   * @throws ObjectNotFoundException
   * @throws GenericException <p>
   *           Create time:2005/04/11
   *           </p>
   */
  public void updateAnnuityInfo(Long planId, String payMode, String accountId,
      String manual, String dischargeVoucher, String cancelLastDischarge)
      throws Exception {
    PayPlan payPlan = new PayPlan();
    payPlan.retrieve(planId);
    // generate policy change to dump table
    CSCIVO csVO = survivalRepaymentTool.registerChangeAndLockPolicy(payPlan
        .getPolicyId(), "302");
    // PayeeCIVO payeeVO = getPayeeVO(payPlan);
    PayPlanPayeeVO payeeVO = this.getPayPlanPayeeVO(planId);
    // survivalRepaymentTool.getPayeeByPolicyId(payPlan.getPolicyId(),CodeCst.
    // PAYMENT_TYPE__SUR_ANNU_PAY);
    /*
     * CustomerVO customerVO=survivalRepaymentTool.getIndivCustomer(payeeVO.getPayeeId()); Date
     * birthDay=customerVO.getBirthday();
     */
    if (dischargeVoucher.equalsIgnoreCase("Y")
        && cancelLastDischarge.equalsIgnoreCase("Y")) {
      throw new AppException(20910020007L);
    } else if (dischargeVoucher.equalsIgnoreCase("Y")) {
      Date nextChargeDate = this.getAnnuityNextDischargeDate(payPlan);
      payPlan.setDischargeDate(nextChargeDate);
      payPlan.setDischargeIndi("Y");
    } else if (cancelLastDischarge.equalsIgnoreCase("Y")) {
      Date lastChargeDate = this.getAnnuityLastDischargeDate(payPlan);
      payPlan.setDischargeIndi("N");
      if (null != lastChargeDate) {
        payPlan.setDischargeDate(lastChargeDate);
      }
    }
    PolicyVO policyVO = survivalRepaymentTool.getPolicyByPolicyId(payPlan
        .getPolicyId());
    String orgin_manual = payPlan.getManualIndi();
    // if user changed
    if (!manual.equals(orgin_manual)) {
      payPlan.setManualIndi(manual);
    } else {// if not change
      // and orgin is NOT "Y"
      if (!manual.equals("Y")) {
        // the payee is individual customer
        CustomerVO indivPayee = survivalRepaymentTool.getIndivCustomer(payeeVO
            .getPayeeId());
        if (indivPayee != null) {
          if (indivPayee.getFirstName() != null) {
            payPlan.setManualIndi(isManualIndicator(indivPayee, policyVO));
          }
        }
        CompanyCustomerVO organPayee = survivalRepaymentTool.getOrganCustomer(payeeVO
            .getPayeeId());
        // the payee is company customer,IdType,IdNumber will be null
        if (organPayee != null) {
          if (organPayee.getCompanyName() != null) {
            payPlan.setManualIndi(isManualIndicator(organPayee, policyVO));
          }
        }
        // the situation that both indivPayee and organPayee are null,
        // or not null
        // should be controlled by business logic
      }
    }
    // dump pay plan table to log
    CommonDumper.srcToLogByRecord(csVO.getMasterChgId(), csVO.getPolicyChgId(),
        "T_PAY_PLAN", planId, CommonDumper.OPER_TYPE_UPDATE, Long
            .valueOf(AppContext.getCurrentUser().getUserId()));
    payPlan.update();
    if (null == payMode || "".equals(payMode)) {
      long errCode = 20910020015L;
      throw new AppException(errCode);
    }
    payeeVO.setPayMode(Integer.valueOf(payMode));
    if (payMode.equals(CodeCst.PAYMODE_TYPE__GIRO)) {
      if (null == accountId || "".equals(accountId)) {
        long noBankAccount = 20910020010L;
        throw new AppException(noBankAccount);
      } else {
        payeeVO.setPayeeAccountId(Long.valueOf(accountId));
      }
    }
    modifyPayee(csVO.getMasterChgId(), csVO.getPolicyChgId(), payeeVO);
    survivalRepaymentTool.effectiveChangeAndUnlockPolicy(csVO.getPolicyChgId());
  }

  /**
   * <p>
   * Description: change payee info
   * </p>
   * 
   * @param policyId
   * @param payeeVO
   * @throws GenericException <p>
   *           Create time:2005/04/11
   *           </p>
   */
  private void modifyPayee(Long changeId, Long policyChgId,
      PayPlanPayeeVO payeeVO) throws GenericException {
    // if (policyCI.isVAPolicy(payeeVO.getPolicyId())) {
    try {
      Dumper.pSrcToLogByRecord(changeId.longValue(), policyChgId.longValue(),
          "T_PAY_PLAN_PAYEE", payeeVO.getListId().longValue(),
          CodeCst.LOG_TYPE__LOG_TYPE_COMMON,
          CodeCst.DATA_OPER_TYPE__DUMP_OPER_UPD, AppContext.getCurrentUser()
              .getUserId());
      PayPlanPayee payPlanPayee = new PayPlanPayee();
      BeanUtils.copyProperties(payPlanPayee, payeeVO);
      // payPlanPayee.setPlanId(planId);
      payPlanPayee.store(); // payPlanPayee.update();
    } catch (Exception e) {
      throw ExceptionFactory.parse(e);
    }
    // } else {
    // CSCIVO csVO = survivalRepaymentTool.registerChangeAndLockPolicy(policyId);
    // policyCI.updatePayeeInfo(changeId, policyChgId, payeeVO);
    // survivalRepaymentTool.effectiveChangeAndUnlockPolicy(csVO.getPolicyChgId());
    // }
  }

  /**
   * <p>
   * Description: calc Maturity Interest
   * </p>
   * 
   * @param amount
   * @param startDate
   * @param endDate
   * @return
   * @throws GenericException <p>
   *           Create time:2005/04/11
   *           </p>
   */
  public BigDecimal calcMaturityInterest(Long itemId, BigDecimal amount,
      Date startDate, Date endDate) throws GenericException {
    CoverageVO policyProductVO = survivalRepaymentTool
        .getCoverageByItemId(itemId);
    PolicyVO policyVO = survivalRepaymentTool
        .getPolicyByPolicyId(policyProductVO.getPolicyId());
    BigDecimal interestAmount = payPlanService.calcuInterestByOrganId(amount,
        startDate, endDate, policyVO.getOrganId(),policyProductVO.getProductId().longValue());
    return interestAmount;
  }

  /**
   * <p>
   * Descrption:calc Maturity Interest WithHoding Tax
   * </p>
   * 
   * @param interest
   * @param itemId
   * @param chequePrintDate
   * @return
   * @throws GenericException <p>
   *           Create time:2005/04/11
   *           </p>
   */
  public BigDecimal calcWithHoldingTax(BigDecimal interest, Long itemId,
      Date chequePrintDate) throws GenericException {
    CoverageVO policyProductVO = survivalRepaymentTool
        .getCoverageByItemId(itemId);
    PolicyVO policyVO = survivalRepaymentTool
        .getPolicyByPolicyId(policyProductVO.getPolicyId());
    // PayeeCIVO payeeVO = survivalRepaymentTool.getPayeeByPolicyId(policyProductVO
    // .getPolicyId(), CodeCst.PAYMENT_TYPE__SUR_MAT_PAY);
    PayPlanPayeeVO payeeVO = this.getPayPlanPayeeVO(itemId,
        CodeCst.PAY_PLAN_TYPE__MATURITY);
    BigDecimal taxAmount = payPlanService.calcuWithholdingTax(interest,
        chequePrintDate, payeeVO.getPayeeId(), policyVO.getOrganId());
    return taxAmount;
  }

  /**
   * <p>
   * Description:check if the policy is cpf or not
   * <p>
   * 
   * @param itemId
   * @return boolean
   * @throws GenericException <p>
   *           Create time: 2005/09/08
   *           </p>
   */
  private boolean isCpf(Long itemId) throws GenericException {
    return SurvivalDSSp.isCpf(itemId);
  }

  /**
   * <p>
   * Description:get total maturity AdjustAmount
   * <p>
   * 
   * @param itemId
   * @return BigDecimal
   * @throws GenericException <p>
   *           Create time: 2005/09/23
   *           </p>
   */
  private BigDecimal getTotalAdjustAmount(Long itemId, Date dueDate)
      throws GenericException {
    return SurvivalDSSp.getTotalAdjustAmount(itemId, dueDate);
  }

  /**
   * <p>
   * Description:update Maturity Info--before draw
   * </p>
   * 
   * @param tPayDue
   * @param payMode
   * @param accountId
   * @param interest
   * @param withHodingTax
   * @throws ObjectNotFoundException
   * @throws GenericException <p>
   *           Create time:2005/04/11
   *           </p>
   */
  public void maturityInfoUpdate(String itemId, String payMode, String accountId)
      throws ObjectNotFoundException, GenericException {
    String payeeName = null;
    CoverageVO policyProductVO = survivalRepaymentTool.getCoverageByItemId(Long
        .valueOf(Long.parseLong(itemId)));
    CSCIVO csVO = survivalRepaymentTool.registerChangeAndLockPolicy(
        policyProductVO.getPolicyId(), "303");
    // PayeeCIVO payeeVO = survivalRepaymentTool.getPayeeByPolicyId(policyProductVO
    // .getPolicyId(), CodeCst.PAYMENT_TYPE__SUR_MAT_PAY);
    PayPlanPayeeVO payeeVO = this.getPayPlanPayeeVO(Long.parseLong(itemId),
        CodeCst.PAY_PLAN_TYPE__MATURITY);
    // modify by sunrain.han 2006.7.4
    // check if the payee name is same with account holder name,if not throw
    // UC0080_ERR_003 20910020020L
    if (payMode.equals(CodeCst.PAYMODE_TYPE__GIRO)) {
      CustomerVO indivCustomerVO = survivalRepaymentTool.getIndivCustomer(payeeVO
          .getPayeeId());
      if (indivCustomerVO != null && indivCustomerVO.getFirstName() != null) {
        payeeName = indivCustomerVO.getFirstName();
      } else {
        CompanyCustomerVO organPayee = survivalRepaymentTool.getOrganCustomer(payeeVO
            .getPayeeId());
        payeeName = organPayee.getCompanyName();
      }
      BankAccountVO bankAccountVO = survivalRepaymentTool
          .getBankAccountByAccountId(Long.valueOf(accountId));
      if (!bankAccountVO.getAccoName().equalsIgnoreCase(payeeName)) {
        long noequalname = 20910020020L;
        throw new AppException(noequalname);
      }
    }
    // modify end
    // code reviewer throw exception , pay mode can not be null
    // if (null == payMode || "".equals(payMode)) {
    // payMode = String.valueOf(payeeVO.getPayMode().longValue());
    //      
    // } else {
    payeeVO.setPayMode(Integer.valueOf(payMode));
    if (payMode.equals(CodeCst.PAYMODE_TYPE__GIRO)) {
      if (null == accountId || "".equals(accountId)) {
        long noBankAccount = 20910020010L;
        throw new AppException(noBankAccount);
      } else {
        payeeVO.setPayeeAccountId(Long.valueOf(accountId));
      }
    }
    // }
    modifyPayee(csVO.getMasterChgId(), csVO.getPolicyChgId(), payeeVO);
    survivalRepaymentTool.effectiveChangeAndUnlockPolicy(csVO.getPolicyChgId());
  }

  /**
   * <p>
   * Description:update Maturity Info
   * </p>
   * 
   * @param tPayDue
   * @param payMode
   * @param accountId
   * @param interest
   * @param withHodingTax
   * @throws ObjectNotFoundException
   * @throws GenericException <p>
   *           Create time:2005/04/11
   *           </p>
   */
  public void maturityInfoUpdate(Long payId, String payMode, String accountId,
      BigDecimal preAdjustAmount, BigDecimal adjustAmount, BigDecimal interest,
      BigDecimal withHoldingTax, Date chkPrintDate) throws Exception {
    String payeeName = null;
    PayDue payDue = TPayDueDelegate.load(payId);
    PayDueVO payDueVO = new PayDueVO();
    BeanUtils.copyProperties(payDueVO, payDue);
    // TPayPlan tPayPlan = TPayPlanDelegate.load(tPayDue.getPlanId());
    CSCIVO csVO = survivalRepaymentTool.registerChangeAndLockPolicy(payDueVO
        .getPolicyId(), "303");
    // PayeeCIVO payeeVO = survivalRepaymentTool.getPayeeByPolicyId(payDueVO.getPolicyId(),
    // CodeCst.PAYMENT_TYPE__SUR_MAT_PAY);
    PayPlanPayeeVO payeeVO = this.getPayPlanPayeeVO(payDueVO.getItemId(),
        CodeCst.PAY_PLAN_TYPE__MATURITY);
    // modify by sunrain.han 2006.7.4
    // check if the payee name is same with account holder name,if not throw
    // UC0080_ERR_003 20910020020L
    if (payMode.equals(CodeCst.PAYMODE_TYPE__GIRO)) {
      CustomerVO indivCustomerVO = survivalRepaymentTool.getIndivCustomer(payeeVO
          .getPayeeId());
      if (indivCustomerVO != null && indivCustomerVO.getFirstName() != null) {
        payeeName = indivCustomerVO.getFirstName();
      } else {
        CompanyCustomerVO organPayee = survivalRepaymentTool.getOrganCustomer(payeeVO
            .getPayeeId());
        payeeName = organPayee.getCompanyName();
      }
      BankAccountVO bankAccountVO = survivalRepaymentTool
          .getBankAccountByAccountId(Long.valueOf(accountId));
      if (!bankAccountVO.getAccoName().equalsIgnoreCase(payeeName)) {
        long noequalname = 20910020020L;
        throw new AppException(noequalname);
      }
    }
    // modify end
    if (0 == payDueVO.getFeeStatus().intValue()) {
      payeeVO.setPayMode(Integer.valueOf(payMode));
      if (payMode.equals(CodeCst.PAYMODE_TYPE__GIRO)) {
        if (null == accountId || "".equals(accountId)) {
          long noBankAccount = 20910020010L;
          throw new AppException(noBankAccount);
        } else {
          payeeVO.setPayeeAccountId(Long.valueOf(accountId));
        }
      }
      modifyPayee(csVO.getMasterChgId(), csVO.getPolicyChgId(), payeeVO);
    } else {
    }
    if (isPaidMaturity(payDueVO) || isPaidMaturityInterest(payDueVO)) {
      long cancelErr = 20910020008L;
      throw new AppException(cancelErr);
    } else {
      CSPayerPayeeRuleCIVO payerVO = csCI.getDerivedInsuranceRoles(payDueVO
          .getPolicyId(), CodeCst.BIZ_FEE_TYPE__PREM_COLLECT);
      if (payDueVO.getFeeStatus().intValue() == 1) {
        // get previous change id, because all chenge id should be
        // equal, so
        // choose the first one's
        PremArapVO[] premARAPVO = survivalRepaymentTool
            .getARAPByPolicyIdAndFeeType(payDueVO.getPolicyId(), Integer
                .valueOf(85));
        for (int i = 0; i < premARAPVO.length; i++) {
          Long isRejected = arAPCI.isBeRejected(premARAPVO[i].getListId(), Long
              .valueOf(AppContext.getCurrentUser().getUserId()));
          if ((isRejected.longValue() > 0)
              && adjustAmount.compareTo(BigDecimal.ZERO) != 0) {
            long rejectErr = 20910020016L;
            // "Please reject the payment record in Payment
            // Authorization
            // before adjusting maturity amount";
            throw new AppException(rejectErr);
          }
        }
        Long oprId = Long.valueOf(AppContext.getCurrentUser().getUserId());
        // cancel interest amount.
        cancelMaturityInterestByChange(payDue, oprId);
        // cancel all 85 and 190 181 189
        cancelMaturityInterest(payDue);
        // if the policy is cpf and net_amount >0
        if (interest.compareTo(BigDecimal.ZERO) == 1) {
          if (interest.subtract(withHoldingTax).compareTo(BigDecimal.ZERO) == 1) {
            BigDecimal cpfPartion = new BigDecimal("0");
            BigDecimal noCpfPartion;
            if (isCpf(payDue.getItemId())) {
              BigDecimal[] amounts = csCI.calcCpfApportionment(payDueVO
                  .getPolicyId(), interest.subtract(withHoldingTax), Integer
                  .valueOf(payMode), Integer
                  .valueOf(CodeCst.FEE_TYPE__MATURITY), Integer.valueOf(2));
              cpfPartion = amounts[0];
              noCpfPartion = amounts[1];
            } else {
              noCpfPartion = interest.subtract(withHoldingTax);
            }
            if (cpfPartion.compareTo(BigDecimal.ZERO) == 1) {
              // cpf partion to payer
              generatePremARAP(payDue, payerVO.getPartyId(), payerVO
                  .getAccountId(), Long.valueOf(payMode),
                  csVO.getMasterChgId(), csVO.getPolicyChgId(), Integer
                      .valueOf(190), cpfPartion, chkPrintDate);
              generatePremARAP(payDue, payerVO.getPartyId(), payerVO
                  .getAccountId(), Long.valueOf(payMode),
                  csVO.getMasterChgId(), csVO.getPolicyChgId(), Integer
                      .valueOf(181), cpfPartion, chkPrintDate);
              generatePremARAP(payDue, payerVO.getPartyId(), payerVO
                  .getAccountId(), Long.valueOf(payMode),
                  csVO.getMasterChgId(), csVO.getPolicyChgId(), Integer
                      .valueOf(CodeCst.FEE_TYPE__MATURITY), cpfPartion,
                  chkPrintDate);
            }
            // no cpf partion to payee.
            if (noCpfPartion.compareTo(BigDecimal.ZERO) == 1) {
              generatePremARAP(payDue, payeeVO, csVO.getMasterChgId(), csVO
                  .getPolicyChgId(), Integer.valueOf(190), noCpfPartion,
                  chkPrintDate);
              generatePremARAP(payDue, payeeVO, csVO.getMasterChgId(), csVO
                  .getPolicyChgId(), Integer.valueOf(181), noCpfPartion,
                  chkPrintDate);
              generatePremARAP(payDue, payeeVO, csVO.getMasterChgId(), csVO
                  .getPolicyChgId(), Integer
                  .valueOf(CodeCst.FEE_TYPE__MATURITY), noCpfPartion,
                  chkPrintDate);
            }
          }
          if (withHoldingTax.compareTo(BigDecimal.ZERO) == 1) {
            generatePremARAP(payDue, payerVO.getPartyId(), payerVO
                .getAccountId(), Long.valueOf(payMode), csVO.getMasterChgId(),
                csVO.getPolicyChgId(), Integer.valueOf(189), withHoldingTax,
                chkPrintDate);
          }
          survivalRepaymentTool.arapRefund(csVO.getMasterChgId(), payDue
              .getPolicyId(), Long.valueOf(CodeCst.FEE_TYPE__MATURITY), oprId);
        }
        if (adjustAmount.compareTo(BigDecimal.ZERO) != 0) {
          // check if negative adjustAmount is too large to be greater
          // than netAmount
          BigDecimal netMaturityAmount = getNetMaturityAmount(payDue);
          // start total adjamount
          BigDecimal totalAdjustAmount = getTotalAdjustAmount(payDue
              .getItemId(), payDue.getPayDueDate());
          if (netMaturityAmount.add(totalAdjustAmount).add(adjustAmount)
              .compareTo(BigDecimal.ZERO) == -1) {
            long negativeErr = 20910020017L;// "Adjusted Maturity
            // Amount
            // cannot be negative";
            throw new AppException(negativeErr);
          }
          // end of total adjamount
          // create three types at same time (F194,F177,F85)
          // For Fully Retained, pay mode should be t_contract_payee.pay_mode
          if (premARAPVO[0].getPayMode().intValue() == 5) {
            premARAPVO[0].setPayMode(Integer.valueOf(payMode));
          }
          if (premARAPVO.length > 0) {
            generatePremARAP(payDue, premARAPVO[0].getPayeeId(), premARAPVO[0]
                .getAccountId(), Long.valueOf(premARAPVO[0].getPayMode()
                .toString()), premARAPVO[0].getChangeId(), premARAPVO[0]
                .getPolicyChgId(), Integer.valueOf(194), adjustAmount, payDue
                .getPayDueDate());
            generatePremARAP(payDue, premARAPVO[0].getPayeeId(), premARAPVO[0]
                .getAccountId(), Long.valueOf(premARAPVO[0].getPayMode()
                .toString()), premARAPVO[0].getChangeId(), premARAPVO[0]
                .getPolicyChgId(), Integer.valueOf(177), adjustAmount, payDue
                .getPayDueDate());
            generatePremARAP(payDue, premARAPVO[0].getPayeeId(), premARAPVO[0]
                .getAccountId(), Long.valueOf(premARAPVO[0].getPayMode()
                .toString()), premARAPVO[0].getChangeId(), premARAPVO[0]
                .getPolicyChgId(), Integer.valueOf(85), adjustAmount, payDue
                .getPayDueDate());
            survivalRepaymentTool.arapRefund(premARAPVO[0].getChangeId(),
                payDue.getPolicyId(), Long.valueOf(CodeCst.FEE_TYPE__MATURITY),
                oprId);
          }
          payDue.setAdjustAmount(preAdjustAmount.add(adjustAmount));
        } else {
          survivalRepaymentTool.arapRefund(premARAPVO[0].getChangeId(), payDue
              .getPolicyId(), Long.valueOf(CodeCst.FEE_TYPE__MATURITY), oprId);
        }
      }
    }
    payDue.setMbInterest(interest);
    payDue.setWithholdTax(withHoldingTax);
    payDue.setChkPrintDate(chkPrintDate);
    CommonDumper.srcToLogByRecord(csVO.getMasterChgId(), csVO.getPolicyChgId(),
        "t_pay_due", payDue.getPayId(), CommonDumper.OPER_TYPE_UPDATE, Long
            .valueOf(AppContext.getCurrentUser().getUserId()));

    TPayDueDelegate.update(payDue);
    survivalRepaymentTool.effectiveChangeAndUnlockPolicy(csVO.getPolicyChgId());
  }

  /**
   * <p>
   * Description:judge this Maturity amount whether or not paid
   * </p>
   * 
   * @param PayDue payDue
   * @return true :maturity amount be paid
   * @throws GenericException <p>
   *           Create time:2005/04/11
   *           </p>
   */
  private boolean isPaidMaturity(PayDueVO payDue) throws GenericException {
    PremArapVO[] araps = survivalRepaymentTool
        .getARAPByItemIdAndFeeTypeAndDueTime(Long.valueOf(0), payDue
            .getItemId(), Integer.valueOf(CodeCst.FEE_TYPE__MATURITY), payDue
            .getPayDueDate());
    if (null == araps) {
      return false;
    }
    for (int i = 0; i < araps.length; i++) {
      // for normal maturity pay job,the arap due time is equal with
      // t_pay_due.pay_due_date
      // so this if statement is spilth.
      if (araps[i].getDueTime().equals(payDue.getPayDueDate())) {
        // decide whether those fee has been refunded or printed check.
        // if Y ,don't do adjustment before cancel repayment.
        return arAPCI.isBeRefunded(araps[i].getListId(),
            Long.valueOf(AppContext.getCurrentUser().getUserId()))
            .booleanValue();
      }
    }
    return false;
  }

  /**
   * <p>
   * Descroption:judge this Maturity Interest whether or not paid if
   * checkPrintDate is null or mb interest is 0 , consider don't paid .
   * </p>
   * 
   * @param PayDue payDue
   * @return ture : maturity interest be paid
   * @throws GenericException <p>
   *           Create time:2005/04/11
   *           </p>
   */
  private boolean isPaidMaturityInterest(PayDueVO payDue)
      throws GenericException {
    if (null == payDue.getChkPrintDate()
        || 0 == payDue.getMbInterest().compareTo(BigDecimal.ZERO)) {
      return false;
    }
    PremArapVO[] araps = survivalRepaymentTool
        .getARAPByItemIdAndFeeTypeAndDueTime(Long.valueOf(0), payDue
            .getItemId(), Integer.valueOf(CodeCst.FEE_TYPE__MATURITY), payDue
            .getChkPrintDate());
    if (null != araps) {
      for (int i = 0; i < araps.length; i++) {
        if (araps[i].getDueTime().equals(payDue.getChkPrintDate())) {
          // decide whether interest has been refunded
          return arAPCI.isBeRefunded(araps[i].getListId(),
              Long.valueOf(AppContext.getCurrentUser().getUserId()))
              .booleanValue();
        }
      }
    }
    return false;
  }

  /**
   * <p>
   * Description:check this payemnt record whether or not be refunded
   * </p>
   * 
   * @param PayDue payDue
   * @return true is : be refunded aleady.
   * @throws GenericException <p>
   *           Create time:2005/04/11
   *           </p>
   */
  public boolean isBeRefunded(PayDueVO payDue) throws GenericException {
    return (isPaidMaturity(payDue) || isPaidMaturityInterest(payDue)
        ? true
        : false);
  }

  /**
   * <p>
   * Description:cancel Maturity Interest and WithHolding tax cacel action is
   * :insert negative fee amount's prem arap
   * </p>
   * 
   * @param PayDue payDue
   * @return boolean
   * @throws GenericException <p>
   *           Create time:2005/04/11
   *           </p>
   */
  private boolean cancelMaturityInterest(PayDue payDue) throws GenericException {
    PremArapVO[] arapsAll = survivalRepaymentTool
        .getARAPByItemIdAndFeeTypeAndDueTime(Long.valueOf(0), payDue
            .getItemId(), Integer.valueOf(CodeCst.FEE_TYPE__MATURITY), payDue
            .getChkPrintDate());
    // Long maxChangeId=payDue.getChangeId();
    // sunrain.han at 06.08.11
    Long maxChangeId = Long.valueOf(0);
    Long minChangeId = Long.valueOf(0);
    if (null != arapsAll) {
      for (int i = 0; i < arapsAll.length; i++) {
        if (minChangeId.longValue() == 0) {
          minChangeId = arapsAll[i].getChangeId();
        }
        if (arapsAll[i].getChangeId().compareTo(maxChangeId) > 0) {
          maxChangeId = arapsAll[i].getChangeId();
        }
        if (arapsAll[i].getChangeId().compareTo(minChangeId) < 0) {
          minChangeId = arapsAll[i].getChangeId();
        }
      }
      if (arapsAll.length == 1) {
        minChangeId = Long.valueOf(0);
      }
    }
    // 1. maturity interest
    if (maxChangeId.longValue() > minChangeId.longValue()) {
      offsetArapByItemIdAndFeeTypeAndDate(maxChangeId, payDue.getItemId(),
          Integer.valueOf(190), payDue.getChkPrintDate());
      offsetArapByItemIdAndFeeTypeAndDate(maxChangeId, payDue.getItemId(),
          Integer.valueOf(CodeCst.FEE_TYPE__MATURITY), payDue.getChkPrintDate());
      offsetArapByItemIdAndFeeTypeAndDate(maxChangeId, payDue.getItemId(),
          Integer.valueOf(181), payDue.getChkPrintDate());
      offsetArapByItemIdAndFeeTypeAndDate(maxChangeId, payDue.getItemId(),
          Integer.valueOf(189), payDue.getChkPrintDate());
    }
    /*
     * issue log offsetArapByItemIdAndFeeTypeAndDate(payDue.getItemId(),
     * Integer.valueOf( 769), payDue.getChkPrintDate());
     */
    return true;
  }

  /**
   * <p>
   * Description:cancel Maturity Interest and WithHolding tax according
   * tpayDue's changeId Cancel F32 t_cash record
   * </p>
   * 
   * @param PayDue payDue
   * @return boolean
   * @throws GenericException <p>
   *           Create time:2005/04/11
   *           </p>
   */
  private boolean cancelMaturityInterestByChange(PayDue payDue, Long oprId)
      throws GenericException {
    PremArapVO[] araps = survivalRepaymentTool
        .getARAPByItemIdAndFeeTypeAndDueTime(Long.valueOf(0), payDue
            .getItemId(), Integer.valueOf(CodeCst.FEE_TYPE__MATURITY), payDue
            .getChkPrintDate());
    if (null == araps || araps.length == 0) {
      return true;
    }
    int MaturityCount = 0;
    Long minChangeId = Long.valueOf(0);
    Long maxChangeId = Long.valueOf(0);
    for (int i = 0; i < araps.length; i++) {
      if (araps[i].getFeeStatus().intValue() == 0) {
        MaturityCount++;
        if (minChangeId.longValue() == 0) {
          minChangeId = araps[i].getChangeId();
        }
        if (araps[i].getChangeId().compareTo(maxChangeId) > 0) {
          maxChangeId = araps[i].getChangeId();
        }
        if (araps[i].getChangeId().compareTo(minChangeId) < 0) {
          minChangeId = araps[i].getChangeId();
        }
        // return true;
      }
    }
    if (MaturityCount == 1
        && !payDue.getChkPrintDate().equals(payDue.getPayDueDate())) {
      minChangeId = Long.valueOf(0);
    }
    if (maxChangeId.longValue() > minChangeId.longValue()) {
      survivalRepaymentTool.cancelArapByChangeId(maxChangeId, oprId);
      return true;
    }
    return true;
  }

  /**
   * <p>
   * Description:Cancel generated prem arap record
   * </p>
   * 
   * @param itemId
   * @param feeType
   * @param chkPrintDate
   * @return
   * @throws GenericException <p>
   *           Create time:2005/04/11
   *           </p>
   */
  /*
   * private boolean cancelArapByItemIdAndFeeTypeAndDate(Long changeId, Long
   * itemId, Integer feeType, Date chkPrintDate) throws GenericException {
   * PremARAPVO[] araps = survivalRepaymentTool.getARAPByItemIdAndFeeTypeAndDueTime(changeId,
   * itemId, feeType, chkPrintDate); if (null != araps) { for (int i = 0; i <
   * araps.length; i++) { if (araps[i].getLiabId().intValue() ==
   * CodeCst.LIABILITY__MATURITY) {
   * survivalRepaymentTool.cancelArapByListId(araps[i].getListId()); } } } return true; }
   */
  /**
   * <p>
   * Description:cancel arap , by insert negative arap record
   * </p>
   * 
   * @param itemId
   * @param feeType
   * @param chkPrintDate
   * @return
   * @throws GenericException <p>
   *           Create time:2005/08/08
   *           </p>
   */
  private Long offsetArapByItemIdAndFeeTypeAndDate(Long changeId, Long itemId,
      Integer feeType, Date chkPrintDate) throws GenericException {
    PremArapVO[] araps = survivalRepaymentTool
        .getARAPByItemIdAndFeeTypeAndDueTime(changeId, itemId, feeType,
            chkPrintDate);
    Long listId = null;
    if (null != araps) {
      for (int i = 0; i < araps.length; i++) {
        try {
          PremArapVO arapVO = new PremArapVO();
          BeanUtils.copyProperties(arapVO, araps[i]);
          // for GEL00036397, confirmed by BCP module, don't need to insert
          // offset feeAmt for records been cancelled.
          if ((!Integer.valueOf(CodeCst.FEE_STATUS__CANCELLED).equals(
              arapVO.getFeeStatus()))
              && (!Integer.valueOf(CodeCst.FEE_STATUS__POSTED_CANCELLED)
                  .equals(arapVO.getFeeStatus()))) {
            arapVO.setLiabId(null);
            arapVO.setFeeAmount(new BigDecimal(araps[i].getFeeAmount()
                .doubleValue()
                * (-1)));
            arapVO.setAccountingDate(AppContext.getCurrentUserLocalTime());

            arapVO.setPosted("N");
            arapVO.setDueTime(AppContext.getCurrentUserLocalTime());
            arapVO.setRelatedId(araps[i].getListId());
            arapVO.setFeeStatus(Integer.valueOf(1));
            arapVO.setOffsetStatus("1");
            arapVO.setOffsetTime(AppContext.getCurrentUserLocalTime());
            arapVO.setFinishTime(AppContext.getCurrentUserLocalTime());
            araps[i].setOffsetStatus("1");
            araps[i].setOffsetTime(AppContext.getCurrentUserLocalTime());
            araps[i].setFeeStatus(Integer.valueOf(1));
            araps[i].setFinishTime(AppContext.getCurrentUserLocalTime());
            araps[i].setAccountingDate(AppContext.getCurrentUserLocalTime());
            araps[i].setCheckEnterTime(AppContext.getCurrentUserLocalTime());
            arAPCI.updatePremARAP(araps[i]);
            listId = survivalRepaymentTool.generateArap(arapVO);
          }
        } catch (Exception e) {
          throw ExceptionFactory.parse(e);
        }
      }
    }
    return listId;
  }

  /**
   * <p>
   * Description:build premarap vo
   * </p>
   * 
   * @param policyId
   * @param ItemId
   * @param changeId
   * @param policyChgId
   * @param feeType
   * @param feeAmount
   * @return PremARAPVO
   * @throws GenericException <p>
   *           Create time:2005/04/11
   *           </p>
   */
  private Long buildPremARAP(Long policyId, Long itemId, Integer liabId,
      Long payeeId, Long payeeAccountId, Long payeeMode, Long changeId,
      Long policyChgId, Integer feeType, BigDecimal feeAmount, Date dueDate,
      String batchNo) throws GenericException {
    PolicyVO policyVO = survivalRepaymentTool.getPolicyByPolicyId(policyId);
    Long listId = generatePremArapViaPlSql(itemId, liabId, payeeId, policyVO
        .getPolicyHolder().getAddressId(), payeeAccountId, payeeMode,
        feeAmount, dueDate, changeId, policyChgId, Long.valueOf(AppContext
            .getCurrentUser().getUserId()), Long.valueOf(feeType.intValue()),
        batchNo, null);
    return listId;
  }

  /**
   * <p>
   * Description:build premarap vo
   * </p>
   * 
   * @param policyId
   * @param ItemId
   * @param changeId
   * @param policyChgId
   * @param feeType
   * @param feeAmount
   * @return PremARAPVO
   * @throws GenericException <p>
   *           Create time:2005/04/11
   *           </p>
   */
  private Long buildPremARAP(Long policyId, Long itemId, Integer liabId,
      PayPlanPayeeVO payeeVO, Long changeId, Long policyChgId, Integer feeType,
      BigDecimal feeAmount, Date dueDate, String batchNo)
      throws GenericException {
    // String accountType = null;
    PolicyVO policyVO = survivalRepaymentTool.getPolicyByPolicyId(policyId);
    Long listId = generatePremArapViaPlSql(itemId, liabId,
        payeeVO.getPayeeId(), policyVO.getPolicyHolder().getAddressId(),
        payeeVO.getPayeeAccountId(), Long.valueOf(payeeVO.getPayMode()
            .intValue()), feeAmount, dueDate, changeId, policyChgId, Long
            .valueOf(AppContext.getCurrentUser().getUserId()), Long
            .valueOf(feeType.intValue()), batchNo, null);
    /*
     * PolicyProductCIVO
     * policyProductVO=survivalRepaymentTool.getPolicyProductByItemId(itemId);
     * arapVO.setHeadId(policyVO.getHeadId());
     * arapVO.setBranchId(policyVO.getBranchId());
     * arapVO.setOrganId(policyVO.getOrganId());
     * arapVO.setDeptId(policyVO.getDeptId());
     * arapVO.setPolicyType(policyVO.getPolicyType());
     * arapVO.setAgentId(policyVO.getServiceAgent());
     * arapVO.setPolicyId(policyId); arapVO.setItemId(itemId);
     * arapVO.setProductId(policyProductVO.getProductId());
     * arapVO.setFeeAmount(feeAmount);
     * arapVO.setChargeType(policyProductVO.getInitialType());
     * arapVO.setPolicyYear(survivalRepaymentTool.getPolicyYearByItemid(itemId));
     * arapVO.setPolicyPeriod(survivalRepaymentTool.getPolicyPeriodByItemid(itemId));
     * arapVO.setBenefitType(survivalRepaymentTool.getBenefitType(
     * policyProductVO.getProductId()));
     * arapVO.setPeriodType(survivalRepaymentTool.getBasicProductLifeByProductId(
     * policyProductVO.getProductId()).getPeriodType());
     * arapVO.setFeeType(feeType); arapVO.setPayMode(payeeVO.getPayMode());
     * arapVO.setMoneyId(policyVO.getMoneyId()); arapVO.setChangeId(changeId);
     * arapVO.setDueTime(dueDate); arapVO.setCashierId(new
     * Long(AppContext.getCurrentUser().getUserId()));
     * arapVO.setDerivation(CodeCst.DERIVATION_TYPE__CONVERTED);
     * arapVO.setLiabId(liabId);
     * arapVO.setPayeeAddressId(policyVO.getAddressId());
     * arapVO.setStdPremAf(policyProductVO.getStdPremAf());
     * arapVO.setDiscntPremAf(policyProductVO.getDiscntPremAf());
     * arapVO.setDiscntedPremAf(policyProductVO.getDiscntedPremAf());
     * arapVO.setPolicyFeeAf(policyProductVO.getPolicyFeeAf());
     * arapVO.setGrossPremAf(policyProductVO.getGrossPremAf());
     * arapVO.setPayeeId(payeeVO.getPayeeId()); arapVO.setBillDate(dueDate);
     * arapVO.setAccountId(payeeVO.getAccountId());
     * arapVO.setPolicyChgId(policyChgId);
     * arapVO.setServiceId(Integer.valueOf(6)); arapVO.setBatchNo(batchNo);
     * arapVO.setFeeStatus(Integer.valueOf(1));
     * arapVO.setWithdrawType(CodeCst.WITHDRAW_TYPE__MATURITY); if
     * (feeType.intValue() == CodeCst.FEE_TYPE__ANNUNITY || feeType.intValue()
     * == CodeCst.FEE_TYPE__MATURITY) { arapVO.setFeeStatus(Integer.valueOf(0));
     * }else{ arapVO.setFeeStatus(Integer.valueOf(1));
     * arapVO.setAccountingDate(dueDate); }
     * arapVO.setFinishTime(AppContext.getCurrentUserLocalTime());
     * arapVO.setCheckEnterTime(AppContext.getCurrentUserLocalTime()); return
     * arapVO;
     */
    return listId;
  }

  /**
   * create pay plan for VA NB data entry by feng.shen need: payPlanVO
   * 
   * @param PayPlanVO payPlanVO
   * @return Long
   * @throws GenericException
   * @throws Exception
   * @roseuid 4222ED8C0028
   */
  public Long createPayPlanNB(PayPlanVO payPlanVO) throws GenericException {
    if (payPlanVO.getItemId() != null && payPlanVO.getPayPlanType() != null) {
      PayPlan payPlan = PayPlan.findByItemIdAndPayPlanType(payPlanVO
          .getItemId(), payPlanVO.getPayPlanType());
      if (payPlan != null && payPlan.getPlanId() != null) {
        payPlanVO.setPlanId(payPlan.getPlanId());
        BeanUtils.copyProperties(payPlan, payPlanVO);
        payPlan.update();
        return payPlan.getPlanId();
      } else {
        BeanUtils.copyProperties(payPlan, payPlanVO);
        return payPlan.create();
      }
    }
    return null;
  }

  /**
   * remove the pay plan record during NB by feng.shen need: planId
   * 
   * @param Long planId
   * @return void
   * @throws GenericException
   * @throws Exception
   * @roseuid 4222ED8C0028
   */
  public void removePayPlan(Long planId) throws GenericException {
    PayPlan.remove(planId);
  }

  /**
   * @todo javadoc
   * @param itemId Long
   * @param payPlanType String
   * @throws GenericException
   * @return PolicyAccountVO[]
   */
  public PayPlanVO findByItemIdAndPayPlanType(java.lang.Long itemId,
      java.lang.String payPlanType) throws GenericException {
    try {
      PayPlanVO payPlanVO = new PayPlanVO();
      PayPlan payPlan = PayPlan.findByItemIdAndPayPlanType(itemId, payPlanType);
      BeanUtils.copyProperties(payPlanVO, payPlan);
      return payPlanVO;
    } catch (Exception ex) {
      throw ExceptionFactory.parse(ex);
    }
  }

  /**
   * create a null account for VA NB data entry by feng.shen need:
   * policyId,policyAccountType
   * 
   * @param policyAccountParamVO policyAccountParamVO
   * @return Long
   * @throws GenericException
   * @throws Exception
   * @roseuid 4222ED8C0028
   */
  public Long createPayPlanPayeeNB(PayPlanPayeeVO payPlanPayeeVO)
      throws GenericException {
    if (payPlanPayeeVO.getPlanId() != null
        && payPlanPayeeVO.getPayeeId() != null) {
      PayPlanPayee payPlanPayee = PayPlanPayee.findByPlanIdAndPayeeId(
          payPlanPayeeVO.getPlanId(), payPlanPayeeVO.getPayeeId());
      if (payPlanPayee != null && payPlanPayee.getListId() != null) {
        payPlanPayeeVO.setListId(payPlanPayee.getListId());
        BeanUtils.copyProperties(payPlanPayee, payPlanPayeeVO);
        payPlanPayee.store(); // payPlanPayee.update();
        return payPlanPayee.getListId();
      } else {
        BeanUtils.copyProperties(payPlanPayee, payPlanPayeeVO);
        return payPlanPayee.create();
      }
    }
    return null;
  }

  /**
   * create a null account for data entry need: policyId,policyAccountType
   * 
   * @param policyAccountParamVO policyAccountParamVO
   * @return Long
   * @throws GenericException
   * @throws Exception
   * @roseuid 4222ED8C0028
   */
  public Long createPayPlanPayeeCS(PayPlanPayeeVO payPlanPayeeVO)
      throws GenericException {
    if (payPlanPayeeVO.getPlanId() != null) {
      PayPlanPayee[] payPlanPayee = PayPlanPayee.findByPlanId(payPlanPayeeVO
          .getPlanId());
      PayPlanPayee payee = new PayPlanPayee();
      if (payPlanPayee != null && payPlanPayee.length > 0) {
        BeanUtils.copyProperties(payee, payPlanPayee[0]);
      }
      if (payee != null && payee.getListId() != null) {
        payPlanPayeeVO.setListId(payee.getListId());
        BeanUtils.copyProperties(payee, payPlanPayeeVO);
        payee.store(); // payPlanPayee.update();
        return payee.getListId();
      } else {
        BeanUtils.copyProperties(payee, payPlanPayeeVO);
        return payee.create();
      }
    }
    return null;
  }

  /**
   * remove the pay plan payee record during NB by plan_id by feng.shen need:
   * planId
   * 
   * @param Long planId
   * @return void
   * @throws GenericException
   * @throws Exception
   * @roseuid 4222ED8C0028
   */
  public void removePayPlanPayee(Long planId) throws GenericException {
    PayPlanPayee[] planPayee = PayPlanPayee.findByPlanId(planId);
    if (planPayee != null && planPayee.length > 0) {
      for (int i = 0; i < planPayee.length; i++) {
        PayPlanPayee.remove(planPayee[i].getListId());
      }
    }
  }

  /**
   * @todo javadoc
   * @param policyId Long
   * @throws GenericException
   * @return PolicyAccountVO[]
   */
  public PayPlanPayeeVO[] findByPlanId(java.lang.Long planId)
      throws GenericException {
    try {
      PayPlanPayee payPlanPayee[] = PayPlanPayee.findByPlanId(planId);
      if (payPlanPayee != null && payPlanPayee.length > 0) {
        PayPlanPayeeVO[] payPlanPayeeVO = new PayPlanPayeeVO[payPlanPayee.length];
        for (int i = 0; i < payPlanPayee.length; i++) {
          payPlanPayeeVO[i] = new PayPlanPayeeVO();
          BeanUtils.copyProperties(payPlanPayeeVO[i], payPlanPayee[i]);
        }
        return payPlanPayeeVO;
      }
      return new PayPlanPayeeVO[0];
    } catch (Exception ex) {
      throw ExceptionFactory.parse(ex);
    }
  }

  /**
   * <p>
   * Description:generate prem arap
   * </p>
   * 
   * @param payDue
   * @param payeeVO
   * @param changeId
   * @param policyChgId
   * @param feeType
   * @param feeAmount
   * @param dueDate
   * @return Long listId
   * @throws GenericException <p>
   *           Create time:2005/04/11
   *           </p>
   */
  private Long generatePremARAP(PayDue payDue, Long payeeId,
      Long payeeAccountId, Long payeeMode, Long changeId, Long policyChgId,
      Integer feeType, BigDecimal feeAmount, Date dueDate)
      throws GenericException {
    Long listId = buildPremARAP(payDue.getPolicyId(), payDue.getItemId(),
        payDue.getLiabId(), payeeId, payeeAccountId, payeeMode, changeId,
        policyChgId, feeType, feeAmount, dueDate, null);
    return listId;
  }

  /**
   * <p>
   * Description:generate prem arap
   * </p>
   * 
   * @param payDue
   * @param payeeVO
   * @param changeId
   * @param policyChgId
   * @param feeType
   * @param feeAmount
   * @param dueDate
   * @return Long listId
   * @throws GenericException <p>
   *           Create time:2005/04/11
   *           </p>
   */
  private Long generatePremARAP(PayDue payDue, PayPlanPayeeVO payeeVO,
      Long changeId, Long policyChgId, Integer feeType, BigDecimal feeAmount,
      Date dueDate) throws GenericException {
    Long listId = buildPremARAP(payDue.getPolicyId(), payDue.getItemId(),
        payDue.getLiabId(), payeeVO, changeId, policyChgId, feeType, feeAmount,
        dueDate, null);
    return listId;
  }

  /**
   * <p>
   * Description:find maturity info for ui
   * </p>
   * 
   * @param policyNumber
   * @param caseNumber
   * @param lifeAsured
   * @param idNumber
   * @param batchNumber
   * @param first
   * @return
   * @throws GenericException <p>
   *           Create time:2005/04/11
   *           </p>
   */
  public List findMaturityPoolInfo(String policyNumber, String caseNumber,
      String lifeAsured, String idNumber, String batchNumber, Integer first,
      Integer pageSize) throws GenericException {
    return MaturityInfoQuery.findMaturityPoolInfo(policyNumber, caseNumber,
        lifeAsured, idNumber, batchNumber, first, pageSize, Long
            .valueOf(CodeCst.LIABILITY__MATURITY));
  }

  /**
   * <p>
   * Description:get Maturity Pool 's size.
   * </p>
   * 
   * @param liabId
   * @return
   * @throws GenericException <p>
   *           Create time:2005/04/11
   *           </p>
   */
  public Integer findMaturityPoolInfoCount(String policyNumber,
      String caseNumber, String lifeAsured, String idNumber,
      String batchNumber, Long liabId) throws GenericException {
    return MaturityInfoQuery.findMaturityPoolInfoCount(policyNumber,
        caseNumber, lifeAsured, idNumber, batchNumber, liabId);
  }

  /**
   * <p>
   * Description:get annuity next discharge date.
   * </p>
   * 
   * @param PayPlan PayPlan
   * @return
   * @throws ObjectNotFoundException
   * @throws GenericException <p>
   *           Create time:2005/04/11
   *           </p>
   */
  public Date getAnnuityNextDischargeDate(PayPlan payPlan)
      throws ObjectNotFoundException, GenericException {
    // Not use when calculate the discharge date
    java.util.Date birthDay = com.ebao.pub.framework.AppContext
        .getCurrentUserLocalTime();
    Date nextChargeDate = null;
    if (DateUtils.getYear(payPlan.getDischargeDate()) == 9999) {
      nextChargeDate = payPlan.getDischargeDate();
    } else {
      nextChargeDate = SurvivalDSSp.getNextChargeDate(payPlan.getItemId(),
          birthDay, payPlan.getDischargeDate());
    }
    return nextChargeDate;
  }

  /**
   * <p>
   * Description:get annuity pre discharge date
   * </p>
   * 
   * @param PayPlan payPlan
   * @return
   * @throws ObjectNotFoundException
   * @throws GenericException <p>
   *           Create time:2005/04/11
   *           </p>
   */
  public Date getAnnuityLastDischargeDate(PayPlan payPlan)
      throws ObjectNotFoundException, GenericException {
    java.util.Date birthDay = com.ebao.pub.framework.AppContext
        .getCurrentUserLocalTime();
    Date lastChargeDate = null;
    if (payPlan.getDischargeDate() != null) {
      if (DateUtils.getYear(payPlan.getDischargeDate()) == 9999) {
        lastChargeDate = payPlan.getDischargeDate();
      } else {
        lastChargeDate = SurvivalDSSp.getLastChargeDate(payPlan.getItemId(),
            birthDay, payPlan.getDischargeDate());
      }
    }
    return lastChargeDate;
  }

  /**
   * <p>
   * Description:save Maturity Info
   * </p>
   * don't generate bcp record
   * 
   * @param tPayDue
   * @param interest
   * @param withHodingTax
   * @throws ObjectNotFoundException
   * @throws GenericException <p>
   *           Create time:2005/04/11
   *           </p>
   */
  public void maturityInfoSave(Long payId, BigDecimal adjustAmount,
      BigDecimal interest, BigDecimal withHoldingTax, Date chkPrintDate)
      throws ObjectNotFoundException, GenericException {
    PayDue tPayDue = TPayDueDelegate.load(payId);
    tPayDue.setAdjustAmount(adjustAmount);
    tPayDue.setMbInterest(interest);
    tPayDue.setWithholdTax(withHoldingTax);
    tPayDue.setChkPrintDate(chkPrintDate);
    TPayDueDelegate.update(tPayDue);
  }

  /**
   * <p>
   * Description:get last pay date by spec item id
   * <p>
   * 
   * @param itemId
   * @param birthDay
   * @param curChargeDate
   * @return Date
   * @throws GenericException <p>
   *           Create time: 2005/07/26
   *           </p>
   */
  public Date getSurvivalLastPayDate(Long itemId) throws GenericException {
    return SurvivalDSSp.getSurvivalLastPayDate(itemId);
  }

  /**
   * <p>
   * Description:get the template type and the xml string of Maturity & Annuity
   * Remittance Advice
   * <p>
   * 
   * @param itemId
   * @param feetype
   * @param dueTime
   * @param chequeAmount
   * @return String [2]
   * @throws GenericException <p>
   *           Create time: 2005/08/07
   *           </p>
   */
  public String[] getSurvivalAdvice(Long itemId, Long feeType, Date dueTime,
      BigDecimal chequeAmount) throws GenericException {
    return SurvivalDSSp.getSurvivalAdvice(itemId, feeType, dueTime,
        chequeAmount);
  }

  /**
   * <p>
   * Description:Create prem arap
   * </p>
   * 
   * @param itemId
   * @param liabId
   * @param payeeId
   * @param payeeAddressId
   * @param payeeAccountId
   * @param payeeMode
   * @param feeAmount
   * @param dueDate
   * @param changeId
   * @param policyChgId
   * @param oprId
   * @param feeType
   * @param batchNo
   * @param accountType
   * @param listId
   * @throws GenericException <p>
   *           Create time:2005/08/22
   *           </p>
   */
  private Long generatePremArapViaPlSql(Long itemId, Integer liabId,
      Long payeeId, Long payeeAddressId, Long payeeAccountId, Long payeeMode,
      BigDecimal feeAmount, Date dueDate, Long changeId, Long policyChgId,
      Long oprId, Long feeType, String batchNo, String accountType)
      throws GenericException {
    Long listId = SurvivalDSSp.generatePremArapViaPlSql(itemId, Long
        .valueOf(liabId), payeeId, payeeAddressId, payeeAccountId, payeeMode,
        feeAmount, dueDate, changeId, policyChgId, oprId, feeType, batchNo,
        accountType);
    return listId;
  }

  public Date getLiabStartPayDate(Long itemId, Long productId, String liabId,
      String sourceIndi) throws GenericException {
    // TODO Auto-generated method stub
    return SurvivalDSSp.getLiabStartDate(itemId, productId, liabId, sourceIndi);
  }
  
  @Override
  public Date getLiabEndPayDate(Long itemId, int liabId) throws GenericException {
    // TODO Auto-generated method stub
    return SurvivalDSSp.getLiabEndPayDate(itemId, liabId);
  }
  
  private PayPlanPayeeVO getPayPlanPayeeVO(Long planId) throws GenericException {
    PayPlanPayee payPlanPayee[] = PayPlanPayee.findByPlanId(planId);
    PayPlanPayeeVO planPayeeVO = new PayPlanPayeeVO();
    if (payPlanPayee != null && payPlanPayee.length > 0) {
      BeanUtils.copyProperties(planPayeeVO, payPlanPayee[0]);
    }
    return planPayeeVO;
  }

  private PayPlanPayeeVO getPayPlanPayeeVO(Long itemId, String payPlanType)
      throws GenericException {
    Long planId = PayPlan.findByItemIdAndPayPlanType(itemId, payPlanType)
        .getPlanId();
    PayPlanPayee payPlanPayee[] = PayPlanPayee.findByPlanId(planId);
    PayPlanPayeeVO planPayeeVO = null;
    if (payPlanPayee != null && payPlanPayee.length > 0) {
      planPayeeVO = new PayPlanPayeeVO();
      BeanUtils.copyProperties(planPayeeVO, payPlanPayee[0]);
    }
    return planPayeeVO;
  }

  /*
   * private PayeeCIVO getPayeeVO(PayPlan payPlan) throws GenericException {
   * return survivalRepaymentTool.getPayeeByPolicyId(payPlan.getPolicyId(),
   * CodeCst.PAYMENT_TYPE__SUR_ANNU_PAY); }
   */
  public BigDecimal getUnpaidAccountAmount(Long itemId, String accountType)
      throws GenericException {
    try {
      PayPlan p = PayPlan.findByItemIdAndPayPlanType(itemId, accountType);
      double paidPeriods = 0;
      double unpaidPeriods = 0;
      if (p != null && p.getPlanId() != null) {
        // annuity certain
        if (DateUtils.getYear(p.getEndDate()) < 9999) {
          unpaidPeriods = (DateUtils.getMonthAmount(p.getPayDueDate(),
              DateUtils.addDay(p.getEndDate(), 1)))
              / calculatorCI.getMonthsBychargeType(p.getPlanFreq());
        } else {
          paidPeriods = DateUtils.getMonthAmount(p.getBeginDate(), p
              .getPayDueDate())
              / calculatorCI.getMonthsBychargeType(p.getPlanFreq());
          // whole life within guarantee period
          if (p.getGuaranteePeriod() != null
              && p.getGuaranteePeriod().doubleValue()
                  * getPeriodNumByPayFreq(p.getPlanFreq()) > paidPeriods) {
            unpaidPeriods = p.getGuaranteePeriod().doubleValue()
                * getPeriodNumByPayFreq(p.getPlanFreq()) - paidPeriods;
          } else {
            unpaidPeriods = 0;
          }
        }
        if (p.getInstalmentAmount() != null) {
          return p.getInstalmentAmount()
              .multiply(new BigDecimal(unpaidPeriods));
        } else {
          return BigDecimal.ZERO;
        }
      } else {
        return BigDecimal.ZERO;
      }
    } catch (ObjectNotFoundException e) {
      return BigDecimal.ZERO;
    }
  }

  private int getPeriodNumByPayFreq(String PayFrequence) {
    int periodNum = 1;
    if ("1".equals(PayFrequence)) {// Yearly
      periodNum = 1;
    } else if ("2".equals(PayFrequence)) {// Half-Yearly
      periodNum = 2;
    } else if ("3".equals(PayFrequence)) {// Quarterly
      periodNum = 4;
    } else if ("4".equals(PayFrequence)) {// Monthly
      periodNum = 12;
    } else if ("5".equals(PayFrequence)) {// Single
      periodNum = 1;
    }
    return periodNum;
  }

  public BigDecimal getUnpaidAnnuityAmount(Long itemId) throws GenericException {
    return getUnpaidAccountAmount(itemId, "6");
  }

  public BigDecimal getUnpaidWithdrawAmount(Long itemId)
      throws GenericException {
    return getUnpaidAccountAmount(itemId, "5");
  }

  // 1. Accumulated Period
  // 2. Withdraw Period
  // 3. Payout Phrase
  public String getPeriodType(Long itemId, Date eventDate)
      throws GenericException {
    PayPlan withdrawAccount;
    PayPlan annuityAccount;
    FlushMode original = HibernateSession3.currentSession().getFlushMode();
    HibernateSession3.currentSession().setFlushMode(FlushMode.COMMIT);
    logger.info("itemId="+itemId);
    withdrawAccount = PayPlan.findByItemIdAndPayPlanType(itemId, "5");
    if (withdrawAccount != null && withdrawAccount.getPlanId() == null) {
      withdrawAccount = null;
    }
    logger.info("withdrawAccount="+ToStringBuilder.reflectionToString(withdrawAccount));
    annuityAccount = PayPlan.findByItemIdAndPayPlanType(itemId, "6");
    if (annuityAccount != null && annuityAccount.getPlanId() == null) {
      annuityAccount = null;
    }
    HibernateSession3.currentSession().setFlushMode(original);
    if ((withdrawAccount != null && eventDate.before(withdrawAccount
        .getBeginDate()))
        || (withdrawAccount == null && annuityAccount != null && eventDate
            .before(annuityAccount.getBeginDate()))
        || (withdrawAccount == null && annuityAccount == null)) {
      return "1";
    } else if ((withdrawAccount != null && eventDate.before(withdrawAccount
        .getEndDate()))) {
      return "2";
    } else {
      return "3";
    }
  }

  /**
   * find pay plan by itemId and pay plan type by feng.shen
   * 
   * @param planId
   * @throws GenericException
   */
  public PayPlanVO findPayPlanByPlanId(java.lang.Long planId)
      throws GenericException {
    try {
      PayPlan payPlan = PayPlan.findPayPlanByPlanId(planId);
      PayPlanVO payPlanVO = new PayPlanVO();
      BeanUtils.copyProperties(payPlanVO, payPlan);
      return payPlanVO;
    } catch (Exception e) {
      throw ExceptionFactory.parse(e);
    }
  }

  // create payPlan record
  public Long createPayPlanCS(PayPlanVO payPlanVO) throws GenericException {
    // TODO Auto-generated method stub
    if (payPlanVO.getItemId() != null && payPlanVO.getPayPlanType() != null) {
      PayPlan payPlan = PayPlan.findByItemIdAndPayPlanType(payPlanVO
          .getItemId(), payPlanVO.getPayPlanType());
      if (payPlan != null && payPlan.getPlanId() != null) {
        payPlanVO.setPlanId(payPlan.getPlanId());
        BeanUtils.copyProperties(payPlan, payPlanVO);
        payPlan.update();
        return payPlan.getPlanId();
      } else {
        BeanUtils.copyProperties(payPlan, payPlanVO);
        return payPlan.create();
      }
    }
    return null;
  }

  /**
   * update payPlan and payPlanPayee Info--before draw
   * 
   * @param regularWithdrawVO
   * @throws ObjectNotFoundException
   * @throws GenericException
   */
  public void payPlanAndPayPlanPayeeInfoUpdate(PayPlanVO payPlanVO,
      PayPlanPayeeVO payPlanPayeeVO) throws ObjectNotFoundException,
      GenericException {
    PayPlan payPlan = new PayPlan();
    PayPlanPayee payPlanPayee = new PayPlanPayee();
    if (payPlanVO != null && payPlanVO.getPlanId() != null) {
      BeanUtils.copyProperties(payPlan, payPlanVO);
    }
    if (payPlanPayeeVO != null && payPlanPayeeVO.getListId() != null) {
      BeanUtils.copyProperties(payPlanPayee, payPlanPayeeVO);
    }
    PolicyVO policyVO = survivalRepaymentTool.getPolicyByPolicyId(payPlanVO
        .getPolicyId());
    // register Change And LockPolicy
    CSCIVO csVO = csCI.registerChangeAndLockPolicy(policyVO.getPolicyId(),
        Integer.valueOf(CodeCst.SERVICE__REGULAR_WITHDRAW_PLAN),
        CodeCst.CHANGE_SOURCE__CS,// CodeCst.CHANGE_SOURCE__BATCH,
        Integer.valueOf(99));
    // generate policy change to dump table
    // dump pay plan table to log
    try {
      CommonDumper.srcToLogByRecord(csVO.getMasterChgId(), csVO
          .getPolicyChgId(), "T_PAY_PLAN", payPlan.getPlanId(),
          CommonDumper.OPER_TYPE_UPDATE, Long.valueOf(AppContext
              .getCurrentUser().getUserId()));
      Dumper.pSrcToLogByRecord(csVO.getMasterChgId(), csVO.getPolicyChgId(),
          "T_PAY_PLAN_PAYEE", payPlanPayee.getListId(),
          CodeCst.LOG_TYPE__LOG_TYPE_COMMON,
          CodeCst.DATA_OPER_TYPE__DUMP_OPER_UPD, AppContext.getCurrentUser()
              .getUserId());
    } catch (Exception e) {
      // TODO Auto-generated catch block
      throw ExceptionFactory.parse(e);
    }
    Log
        .debug(this.getClass(), "--------test----plan_id:"
            + payPlan.getPlanId());
    payPlan.update();
    payPlanPayee.store();
    survivalRepaymentTool.effectiveChangeAndUnlockPolicy(csVO.getPolicyChgId());
  }

  /**
   * <p>Description:Get last PayDue by policyId, itemId, liabId<p>
   * @param Long policyId
   * @param Long itemId
   * @param Long liabId
   * @return payDue
   * @throws GenericException
   * <p>Create time:2005/07/26<p>
   */
  @DataModeChgMovePoint
  private PayDue getLastPayDueBoByPolicyIdAndItemIdAndLiabId(Long policyId,
      Long itemId, Long liabId) throws GenericException {
    PayDue tPayDue = TPayDueDelegateExt
        .findLastPayDueByPolicyIdAndItemIdAndLiabId(policyId, itemId, liabId);
    PayDue payDue = new PayDue();
    try {
      BeanUtils.copyProperties(payDue, tPayDue);
    } catch (Exception e) {
      throw ExceptionFactory.parse(e);
    }
    return payDue;
  }

  @DataModeChgMovePoint
  public PayDueVO getLastPayDueByPolicyIdAndItemIdAndLiabId(Long policyId,
      Long itemId, Long liabId) throws GenericException {
    PayDue payDue = getLastPayDueBoByPolicyIdAndItemIdAndLiabId(policyId,
        itemId, liabId);
    PayDueVO vo = new PayDueVO();
    BeanUtils.copyProperties(vo, payDue);
    return vo;
  }

  /**
   * <p>Description:Get last PayDue by policyId, itemId, liabId<p>
   * @param Long itemId
   * @param Long liabId
   * @return payDue
   * @throws GenericException
   * <p>Create time:2005/07/26<p>
   */
  @DataModeChgMovePoint
  public PayDueVO getLastPayDueByItemIdAndLiabId(Long itemId, Long liabId)
      throws GenericException {
    PayDue tPayDue = TPayDueDelegateExt.findLastPayDueByItemIdAndLiabId(itemId,
        liabId);
    PayDueVO payDue = new PayDueVO();
    try {
      BeanUtils.copyProperties(payDue, tPayDue);
    } catch (Exception e) {
      throw ExceptionFactory.parse(e);
    }
    return payDue;
  }

  @DataModeChgMovePoint
  private BigDecimal getNetMaturityAmount(PayDue payDue) {
    BigDecimal retAmount = new BigDecimal("0");
    if (null == payDue.getItemId() || payDue.getItemId().intValue() == 0
        || null == payDue.getFeeAmount()) {
      return retAmount;
    }
    retAmount = payDue.getFeeAmount();

    retAmount = retAmount.add(payDue.getTotalBonus());
    //
    //
    retAmount = retAmount.add(payDue.getInterimBonus());
    //
    retAmount = retAmount.add(payDue.getSpecialBonus());

    retAmount = retAmount.add(payDue.getSbAmount());

    retAmount = retAmount.add(payDue.getCashBonus());

    retAmount = retAmount.add(payDue.getRenewalSuspense());

    retAmount = retAmount.add(payDue.getCollectSuspense());

    retAmount = retAmount.add(payDue.getGeneralSuspense());

    retAmount = retAmount.add(payDue.getApaAmount());

    retAmount = retAmount.add((null == payDue.getPaidUpAddition()
        ? new BigDecimal(0)
        : payDue.getPaidUpAddition()));

    //removed by shirley.miao for GEL00039784
    //retAmount=retAmount.add((null==getAdjustAmount()?new BigDecimal(0):getAdjustAmount()));

    retAmount = retAmount.subtract(payDue.getAplAmount());

    retAmount = retAmount.subtract(payDue.getRetainedAmount());

    retAmount = retAmount.subtract((null == payDue.getSbPaidAmount()
        ? new BigDecimal(0)
        : payDue.getSbPaidAmount()));

    retAmount = retAmount.subtract((null == payDue.getRepayPrem()
        ? new BigDecimal(0)
        : payDue.getRepayPrem()));
    retAmount = retAmount.subtract(payDue.getLoanAmount());

    retAmount = retAmount.subtract(payDue.getStudyLoan());

    return retAmount;
  }

  @DataModeChgMovePoint
  public BigDecimal getNetMaturityAmount(PayDueVO payDueVO) {
    PayDue payDue = new PayDue();
    BeanUtils.copyProperties(payDue, payDueVO);
    return this.getNetMaturityAmount(payDue);
  }
  
  public BigDecimal getPaidMaturityAmount(Long policyId,Long itemId)
      throws GenericException {
    BigDecimal netMaturity = new BigDecimal(0);
 
    PayDue payDue = getLastPayDueBoByPolicyIdAndItemIdAndLiabId(policyId, itemId, Long
        .valueOf(CodeCst.LIABILITY__MATURITY));
    if (null != payDue) {
      if (null != payDue.getPayId()
          &&payDue.getFeeStatus()!=null
          &&payDue.getFeeStatus()==CodeCst.FEE_STATUS__PAY_CONFIRMED) {
        BigDecimal totalAdjustAmount = getTotalAdjustAmount(payDue.getItemId(),
            payDue.getPayDueDate());
        netMaturity = getNetMaturityAmount(payDue).add(
            totalAdjustAmount.negate());
      }
    }
    return netMaturity;
  }

    @Override
    public BigDecimal getPaidSurvivalAmount(Long itemId, Date eventDate)
                    throws GenericException {
        BigDecimal rtnVal = new BigDecimal(0);
        PayPlan payPlan = new PayPlan();
        payPlan.findByItemIdAndLiabId(itemId, Integer.valueOf(CodeCst.LIABILITY__SURVIVAL));
        if(payPlan.getBeginDate() != null && payPlan.getEndDate() != null 
                        && !payPlan.getBeginDate().after(eventDate) && !payPlan.getEndDate().before(eventDate)){
            List<PayDue> list = TPayDueDelegateExt.findPayDueForPaidSurvivalAmount(itemId, eventDate, CodeCst.LIABILITY__SURVIVAL);
            if (list != null) {
                for (PayDue data : list) {
                    rtnVal = rtnVal.add(data.getFeeAmount());
                }
            }
        }
        return rtnVal;
    }

    @Override
    public BigDecimal getNoPaidSurvivalAmount(Long itemId, Date eventDate)
                    throws GenericException {
        BigDecimal rtnVal = new BigDecimal(0);
        List<PayDue> list = TPayDueDelegateExt.findPayDueForNoPaidSurvivalAmount(itemId, eventDate, CodeCst.LIABILITY__SURVIVAL);
        if (list != null) {
            for (PayDue data : list) {
                rtnVal = rtnVal.add(data.getFeeAmount());
            }
        }
        return rtnVal;
    }
    
    @Override
    public BigDecimal getPaidAnnuityAmount(Long itemId, Date eventDate)
                    throws GenericException {
        BigDecimal rtnVal = new BigDecimal(0);
        List<PayDue> list = TPayDueDelegateExt.findPayDueForPaidAnnuityAmount(itemId, eventDate, CodeCst.LIABILITY__ANNUNITY);
        if (list != null) {
            for (PayDue data : list) {
                rtnVal = rtnVal.add(data.getFeeAmount());
            }
        }
        return rtnVal;
    }
    

  @Override
    public BigDecimal calcMaturityAmount4Rep(Long policyId)
                    throws GenericException {
        return SurvivalDSSp.calcMaturityAmount4Rep(policyId);
    }
  
  


@Override
public Map<String, String> getAnnuityPaidInfo(Map<String, String> params)
                throws GenericException {
    return SurvivalDSSp.getAnnuityPaidInfo(params);
}

    /**
     * <p>
     * Description: Delete survival pay plan
     * </p>
     * 
     * @param itemId
     * @param changeId
     * @param policyChgId
     * @param oprId
     * @throws GenericException <p>
     *           Create time:2017/09/11
     *           </p>
     */
    public void delPayPlan(Long itemId, Long changeId,
                    Long policyChgId, Long oprId) throws GenericException {
        SurvivalDSSp.delPayPlan(itemId, changeId, policyChgId,
                        oprId);
    }


@Resource(name = ProductService.BEAN_DEFAULT)
  private ProductService productService;

  public ProductService getProductService() {
    return productService;
  }

  @Resource(name = PolicyCI.BEAN_DEFAULT)
  private PolicyCI policyCI;

  @Resource(name = ClaimCI.BEAN_DEFAULT)
  private ClaimCI claimCI;

  @Resource(name = PayPlanService.BEAN_DEFAULT)
  private PayPlanService payPlanService;

  @Resource(name = CSCI.BEAN_DEFAULT)
  private CSCI csCI;

  @Resource(name = ARAPCI.BEAN_DEFAULT)
  private ARAPCI arAPCI;

  @Resource(name = CalculatorCI.BEAN_DEFAULT)
  private CalculatorCI calculatorCI;
  
  @Resource(name = PolicyService.BEAN_DEFAULT)
  PolicyService policyService;
}