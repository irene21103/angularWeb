	package com.ebao.ls.liaRoc.batch;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.transaction.UserTransaction;

import com.ebao.foundation.module.db.hibernate.HibernateSession3;
import com.ebao.foundation.module.web.action.ExceptionInfoUtils;
import com.ebao.ls.cs.logger.ApplicationLogger;
import com.ebao.ls.cs.logger.helper.AppLogDataHelper;
import com.ebao.ls.liaRoc.LiaRocCst;
import com.ebao.ls.liaRoc.vo.LiaRocUploadDetail2020WrapperVO;
import com.ebao.ls.liaRoc.vo.LiaRocUploadDetailBaseVO;
import com.ebao.ls.liaRoc.vo.LiaRocUploadDetailWrapperVO;
import com.ebao.ls.pa.nb.util.NBUtils;
import com.ebao.pub.batch.exe.BatchDB;
import com.ebao.pub.batch.help.BatchHelp;
import com.ebao.pub.batch.type.JobStatus;
import com.ebao.pub.fileresolver.writer.FixedCharLengthWriter;
import com.ebao.pub.util.DateUtils;
import com.ebao.pub.util.StringUtils;
import com.ebao.pub.util.Trans;

/**
 * <h1>公會通報資料上傳 batch - 收件</h1><p>
 * @since 2016/03/10<p>
 * @author Amy Hung
 * <p>Update Time: </p>
 * <p>Updater: </p>
 * <p>Update Comments: </p>
*/
public class LiaRocUploadReceiveBatch extends LiaRocUploadBatch {

	public static final String BEAN_DEFAULT = "liaRocUploadReceiveBatch";

	@Override
	protected String getUploadType() {
		return LiaRocCst.LIAROC_UL_TYPE_RECEIVE;
	}

	@Override
	protected String getUploadModule() {
		return LiaRocCst.LIAROC_UL_UPLOAD_TYPE_NB;
	}

	/**
	 * 用來將通報明細資料轉換成 txt 檔的 writer
	*/
	@Resource(name = "liaRocUploadRequestWriter")
	private FixedCharLengthWriter fixedLengthWriter;

	/**
	 * <p>Description : 查詢要通報的明細資料</p>
	 * <p>Created By : Amy Hung</p>
	 * <p>Create Time : Mar 15, 2016</p>
	 * @return
	 */
	protected List<LiaRocUploadDetailWrapperVO> getUploadDetails() {
		//承保跟通報batch取件後先將狀態壓成Ｑ, 避免動作中有資料寫入比對有誤
		List<String> bizSource = new ArrayList<String>();
		bizSource.add(LiaRocCst.LIAROC_UL_BIZ_SOURCE_UNB);
		bizSource.add(LiaRocCst.LIAROC_UL_BIZ_SOURCE_FOA_UNB);
		bizSource.add(LiaRocCst.LIAROC_UL_BIZ_SOURCE_AFINS);
		bizSource.add(LiaRocCst.LIAROC_UL_BIZ_SOURCE_POS);
		bizSource.add(LiaRocCst.LIAROC_UL_BIZ_SOURCE_FOA_POS);
		bizSource.add(LiaRocCst.LIAROC_UL_BIZ_SOURCE_AFINS_POS);

		liaRocUpload2019CI.uploadLiaRocUploadStatusToQ(bizSource, LiaRocCst.LIAROC_UL_TYPE_RECEIVE);

		// 查詢新契約-收件的通報明細資料
		List<LiaRocUploadDetailWrapperVO> uploadList = liaRocUpload2019CI.findUploadDetailDataByCriteria(
						bizSource, LiaRocCst.LIAROC_UL_TYPE_RECEIVE, "insert_time", true);

		ApplicationLogger.addLoggerData("[INFO]未移除重覆 TotalDetail: " + uploadList.size());
		List<LiaRocUploadDetailWrapperVO> finalUploadList = checkUploadKey(uploadList);
		return finalUploadList;
	}

	@Override
	public int mainProcess() throws Exception {
		int result = -1;
		if (!liaRocUploadCommonService.isLiaRocV2020Process()) {
			result = mainProcess2019();
		}
		result = mainProcess2020();
		return result;
	}

	public int mainProcess2019() throws Exception {

		ApplicationLogger.setSystemCode(AppLogDataHelper.SYSTEM_CODE_UNB);
		ApplicationLogger.setJobName("公會收件通報排程");
		ApplicationLogger.setPolicyCode("LiarocUploadReceive");
		ApplicationLogger.setOptionId(DateUtils.date2String(new Date(), "yyyyMMdd HHmmss"));

		if (BatchDB.getSubId() != null) {
			ApplicationLogger.addLoggerData("[INFO]INFO]Batch Job Starting.. RunId=" + BatchHelp.getRunId());
		}

		int result = this.subProcess();

		ApplicationLogger.flush();

		return result;
	}

	/**
	 * <h1>批處理主程式<h1>
	 * <p>通報主檔資料是以保單為單位，通報明細資料是以保項為單位，因此一筆通報主檔資料會有多筆的通報明細資料;
	 * 上傳公會時，若該主檔下的任一筆明細資料解析發生錯誤，則該筆通報主檔狀態會是P(解析有錯)並且不會發送公會<p>
	 */
	protected int subProcess() throws Exception {

		// 批處理狀況預設為成功
		int executeResult = JobStatus.EXECUTE_SUCCESS;

		// 記錄各通報主檔資料更新回 DB 的狀態
		Map<Long, String> uploadStatus = new HashMap<Long, String>();

		UserTransaction trans = null;
		try {

			List<LiaRocUploadDetailWrapperVO> liaRocUploadDetailBaseVOs = this.getUploadDetails();
			do { //IR-321585 PMC_UAT,增加通報查詢筆數限制

				trans = Trans.getUserTransaction();
				trans.begin();

				// 查詢新契約-收件的通報明細資料
				List<LiaRocUploadDetailBaseVO> uploadList = new ArrayList<LiaRocUploadDetailBaseVO>();

				ApplicationLogger.addLoggerData("[INFO]上傳TotalDetail: " + liaRocUploadDetailBaseVOs.size());
				if (liaRocUploadDetailBaseVOs.size() > 0) {
					for (LiaRocUploadDetailWrapperVO vo : liaRocUploadDetailBaseVOs) {
						uploadList.add(vo);

						//每500筆通報
						if (uploadList.size() >= 500) {

							//取下一筆，若ulistid不一樣就先通報出去
							int index = liaRocUploadDetailBaseVOs.indexOf(vo);
							LiaRocUploadDetailWrapperVO nextVO = null;
							if ((index + 1) < liaRocUploadDetailBaseVOs.size()) {
								nextVO = liaRocUploadDetailBaseVOs.get(index + 1);
							}

							if (nextVO != null && !nextVO.getUListId().equals(vo.getUListId())) {
								String fileName = super.getUploadRequestFileName();
								ApplicationLogger.addLoggerData("[INFO]上傳通報檔名: " + fileName);
								// 傳送通報資料
								executeResult = super.processUpload(uploadList, fileName, uploadStatus);

								ApplicationLogger.addLoggerData("[INFO]上傳SubDetail: " + uploadList.size());
								uploadList.clear();
								uploadStatus.clear();
								HibernateSession3.currentSession().flush();
							}
						}
					}

					if (uploadList.size() > 0) {
						String fileName = super.getUploadRequestFileName();
						ApplicationLogger.addLoggerData("[INFO]上傳通報檔名: " + fileName);
						ApplicationLogger.addLoggerData("[INFO]上傳SubDetail: " + uploadList.size());
						// 傳送通報資料
						executeResult = super.processUpload(uploadList, fileName, uploadStatus);
						HibernateSession3.currentSession().flush();
					}
				} else {
					ApplicationLogger.addLoggerData("[INFO]沒有要通報的資料");
				}

				trans.commit();

				uploadList.clear();
				uploadStatus.clear();

				liaRocUploadDetailBaseVOs.clear();
				liaRocUploadDetailBaseVOs = null;

				liaRocUploadDetailBaseVOs = this.getUploadDetails();

				//IR-321585 PMC_UAT,增加通報查詢筆數限制
			} while (liaRocUploadDetailBaseVOs.size() > 0);

			ApplicationLogger.addLoggerData("[INFO]Batch Job finished..");
		} catch (Throwable e) {
			trans.rollback();
			String message = ExceptionInfoUtils.getExceptionMsg(e);
			ApplicationLogger.addLoggerData("[ERROR]發生不可預期的錯誤, Error Message:" + message);
			executeResult = JobStatus.EXECUTE_FAILED;
			ApplicationLogger.setReturnCode(ApplicationLogger.RETURN_CODE_FAIL);
		}

		try {
			trans = Trans.getUserTransaction();
			trans.begin();
			ApplicationLogger.addLoggerData("[INFO]update no_item start..");
			//更新主檔為 NO_ITEM及通報資料狀態為9-無可通報保項(count明細檔check_status='1'為0 ，表示主檔為9-無可通報保項)
			liaRocUpload2019CI.uploadLiaRocUploadStatusTo9();
			ApplicationLogger.addLoggerData("[INFO]update no_item finished..");
			trans.commit();
		} catch (Throwable e) {
			trans.rollback();
			String message = ExceptionInfoUtils.getExceptionMsg(e);
			ApplicationLogger.addLoggerData("[ERROR]update no_item, Error Message:" + message);
		}

		return executeResult;
	}

	@Override
	protected FixedCharLengthWriter getFixedLengthWriter() {
		return fixedLengthWriter;
	}

	/**
	 * <p>Description : 依保險業通報作業資訊系統操作手冊 Page 32
	 * A.	鍵值偵錯規則：
	 * 1.當日異動檔KEY=產壽險別＋公司代號＋被保險人身分證號碼＋被保險人出生日期＋保單號碼＋保單分類＋險種分類＋險種。
	 * 2.各公司請依key值逐單通報，當日同一筆資料更新順序以最新通報時間為準。
	 * </p>
	 * <p>Created By : Simon.Huang</p>
	 * <p>Create Time : 2019年3月19日</p>
	 * @param uploadList
	 * @return
	 */
	protected List<LiaRocUploadDetailWrapperVO> checkUploadKey(List<LiaRocUploadDetailWrapperVO> uploadList) {

		//IR-299907-同一時間的承保通報資料需比對ITEM_ID保留最後的保項執行通報
		List<LiaRocUploadDetailWrapperVO> duplicateList = new ArrayList<LiaRocUploadDetailWrapperVO>();
		Map<String, LiaRocUploadDetailWrapperVO> compareKeys = new HashMap<String, LiaRocUploadDetailWrapperVO>();
		for (LiaRocUploadDetailWrapperVO warpperVO : uploadList) {

			// 因AFINS收件通報不做比對，故12/15不作key值比對，一律送出
			if (!NBUtils.in(warpperVO.getLiaRocPolicyStatus(),
							LiaRocCst.LIAROC_POLICY_STATUS__FIX_ERROR,
							LiaRocCst.LIAROC_POLICY_STATUS__ERROR_TERMINATION)) {

				String checkKey = this.getUploadKey(warpperVO);

				if (compareKeys.containsKey(checkKey)) {
					//有相同key的只取最後上傳的作通報,移除較前的通報明細檔
					LiaRocUploadDetailWrapperVO duplicateVO = compareKeys.remove(checkKey);

					ApplicationLogger.addLoggerData("[INFO]通報移除重覆保項->key=" + checkKey);
					ApplicationLogger.addLoggerData("[" + warpperVO.getInternalId() + "]通報移除重覆保項->保留="
									+ warpperVO.getListId() + "+通報狀態=" + warpperVO.getLiaRocPolicyStatus());
					ApplicationLogger.addLoggerData("[" + duplicateVO.getInternalId() + "]通報移除重覆保項->移除="
									+ duplicateVO.getListId() + "+通報狀態=" + duplicateVO.getLiaRocPolicyStatus());
					duplicateList.add(duplicateVO);
				}
				//設定作重覆比對
				compareKeys.put(checkKey, warpperVO);
			}
		}

		if (duplicateList.size() > 0) {
			//DEV-340097 增加更新邏輯：3-不通報,上傳重覆件,註解更新主檔狀態為刪除的邏輯 
			for (LiaRocUploadDetailWrapperVO removeVO : duplicateList) {
				//取出相同key這次要上傳的明細檔資料
				String checkKey = this.getUploadKey(removeVO);
				LiaRocUploadDetailWrapperVO warpperVO = compareKeys.get(checkKey);

				//更新明細檔狀態為3-不通報,上傳重覆件
				liaRocUploadDetailDao.updateCheckStatus(removeVO.getListId(),
								warpperVO.getListId(),
								LiaRocCst.CHECK_STATUS_CANCEL_BY_SAMEKEY);
			}

			//IR-299907-同一時間的承保通報資料需比對ITEM_ID，移除重覆的
			uploadList.removeAll(duplicateList);

			/*
			//IR-299907-移除後預設為D-刪除，若有保項執行通報，由後續通報流程更新rqUID及上傳狀態
			List<Long> updateDeletList = new ArrayList<Long>();
			for(LiaRocUploadDetailWrapperVO removeVO : duplicateList) {
				if(updateDeletList.contains(removeVO.getUploadListId()) == false) {
					updateDeletList.add(removeVO.getUploadListId());
				}
			}
			for(Long uploadListId : updateDeletList) {
				//IR-299907-更新狀態為刪除
				liaRocUploadCI.delete(uploadListId);
			}*/
		}

		return uploadList;
	}

	public String getUploadKey(LiaRocUploadDetailWrapperVO warpperVO) {

		//當日異動檔KEY=產壽險別＋公司代號＋被保險人身分證號碼＋被保險人出生日期＋保單號碼＋保單分類＋險種分類＋險種。
		List<String> keys = new ArrayList<String>();

		//被保險人身分證號碼
		keys.add(StringUtils.nullToEmpty(warpperVO.getCertiCode()));
		//被保險人出生日期
		if (warpperVO.getBirthday() != null) {
			keys.add(DateUtils.date2String(warpperVO.getBirthday()));
		} else {
			keys.add(" ");
		}
		//保單號碼
		keys.add(warpperVO.getPolicyId());
		//險種分類:1   人壽保險,2   傷害保險,3   健康保險
		keys.add(warpperVO.getLiaRocProductCategory());
		//險種:
		//01  一般,02  特定,03  投資型,04  日額型,05  實支實付型,
		//06  日額或實支實付擇一,07  手術型,08  重大疾病,09  帳戶型,
		//10  長期看護型,11  失能,12  防癌,13  旅行平安,14  微型,
		//15  微型實支實付型
		keys.add(warpperVO.getLiaRocProductType());

		String uploadDetailKey = org.apache.commons.lang.StringUtils.join(keys.iterator(), ",");

		return uploadDetailKey;
	}

	/* 2019 */
	/* ************************************************************************************************** */
	/* 2020 */
	/**
	 * <p>Description : </p>
	 * <p>Created By : Simon Huang</p>
	 * <p>Create Time : 2020年12月9日</p>
	 * @return
	 */
	protected List<? extends LiaRocUploadDetailBaseVO> getUpload2020Details() {
		//承保跟通報batch取件後先將狀態壓成Ｑ, 避免動作中有資料寫入比對有誤
		List<String> bizSource = new ArrayList<String>();
		bizSource.add(LiaRocCst.LIAROC_UL_BIZ_SOURCE_UNB);
		bizSource.add(LiaRocCst.LIAROC_UL_BIZ_SOURCE_FOA_UNB);
		bizSource.add(LiaRocCst.LIAROC_UL_BIZ_SOURCE_AFINS);
		bizSource.add(LiaRocCst.LIAROC_UL_BIZ_SOURCE_POS);
		bizSource.add(LiaRocCst.LIAROC_UL_BIZ_SOURCE_FOA_POS);
		bizSource.add(LiaRocCst.LIAROC_UL_BIZ_SOURCE_AFINS_POS);

		liaRocUpload2020CI.uploadLiaRocUploadStatusToQ(bizSource, LiaRocCst.LIAROC_UL_TYPE_RECEIVE);

		// 查詢新契約-收件的通報明細資料
		List<LiaRocUploadDetail2020WrapperVO> uploadList = liaRocUpload2020CI.findUploadDetailDataByCriteria(
						bizSource, LiaRocCst.LIAROC_UL_TYPE_RECEIVE, "insert_time", true);

		ApplicationLogger.addLoggerData("[INFO]未移除重覆 TotalDetail: " + uploadList.size());
		List<LiaRocUploadDetail2020WrapperVO> finalUploadList = checkUpload2020Key(uploadList);

		return finalUploadList;
	}

	public int mainProcess2020() throws Exception {

		ApplicationLogger.setSystemCode(AppLogDataHelper.SYSTEM_CODE_UNB);
		ApplicationLogger.setJobName("公會收件通報排程2020");
		ApplicationLogger.setPolicyCode("LiarocUploadReceive");
		ApplicationLogger.setOptionId(DateUtils.date2String(new Date(), "yyyyMMdd HHmmss"));

		if (BatchDB.getSubId() != null) {
			ApplicationLogger.addLoggerData("[INFO]INFO]Batch Job Starting.. RunId=" + BatchHelp.getRunId());
		}

		int result = this.subProcess2020(LiaRocCst.LIAROC_UL_TYPE_RECEIVE);

		ApplicationLogger.flush();

		return result;
	}

	/**
	 * <p>Description : 依保險業通報作業資訊系統操作手冊 Page 32
	 * A.	鍵值偵錯規則：
	 * 1.當日異動檔KEY=產壽險別＋公司代號＋被保險人身分證號碼＋被保險人出生日期＋保單號碼＋保單分類＋險種分類＋險種。
	 * 2.各公司請依key值逐單通報，當日同一筆資料更新順序以最新通報時間為準。
	 * </p>
	 * <p>Created By : Simon.Huang</p>
	 * <p>Create Time : 2019年3月19日</p>
	 * @param uploadList
	 * @return
	 */
	protected List<LiaRocUploadDetail2020WrapperVO> checkUpload2020Key(List<LiaRocUploadDetail2020WrapperVO> uploadList) {

		//IR-299907-同一時間的承保通報資料需比對ITEM_ID保留最後的保項執行通報
		List<LiaRocUploadDetail2020WrapperVO> duplicateList = new ArrayList<LiaRocUploadDetail2020WrapperVO>();
		Map<String, LiaRocUploadDetail2020WrapperVO> compareKeys = new HashMap<String, LiaRocUploadDetail2020WrapperVO>();
		for (LiaRocUploadDetail2020WrapperVO warpperVO : uploadList) {

			// 因AFINS收件通報不做比對，故12/15不作key值比對，一律送出
			if (!NBUtils.in(warpperVO.getLiaRocPolicyStatus(),
							LiaRocCst.LIAROC_POLICY_STATUS__FIX_ERROR,
							LiaRocCst.LIAROC_POLICY_STATUS__ERROR_TERMINATION)) {

				String checkKey = this.getUpload2020Key(warpperVO);

				if (compareKeys.containsKey(checkKey)) {
					//有相同key的只取最後上傳的作通報,移除較前的通報明細檔
					LiaRocUploadDetail2020WrapperVO duplicateVO = compareKeys.remove(checkKey);

					ApplicationLogger.addLoggerData("[INFO]通報移除重覆保項->key=" + checkKey);
					ApplicationLogger.addLoggerData("[" + warpperVO.getInternalId() + "]通報移除重覆保項->保留="
									+ warpperVO.getListId() + "+通報狀態=" + warpperVO.getLiaRocPolicyStatus());
					ApplicationLogger.addLoggerData("[" + duplicateVO.getInternalId() + "]通報移除重覆保項->移除="
									+ duplicateVO.getListId() + "+通報狀態=" + duplicateVO.getLiaRocPolicyStatus());
					duplicateList.add(duplicateVO);
				}
				//設定作重覆比對
				compareKeys.put(checkKey, warpperVO);
			}
		}

		if (duplicateList.size() > 0) {
			//DEV-340097 增加更新邏輯：3-不通報,上傳重覆件,註解更新主檔狀態為刪除的邏輯 
			for (LiaRocUploadDetail2020WrapperVO removeVO : duplicateList) {
				//取出相同key這次要上傳的明細檔資料
				String checkKey = this.getUpload2020Key(removeVO);
				LiaRocUploadDetail2020WrapperVO warpperVO = compareKeys.get(checkKey);
				//更新明細檔狀態為3-不通報,上傳重覆件
				liaRocUploadDetail2020Dao.updateCheckStatus(removeVO.getListId(),
								warpperVO.getListId(),
								LiaRocCst.CHECK_STATUS_CANCEL_BY_SAMEKEY);

			}

			//IR-299907-同一時間的承保通報資料需比對ITEM_ID，移除重覆的
			uploadList.removeAll(duplicateList);

			/*
			//IR-299907-移除後預設為D-刪除，若有保項執行通報，由後續通報流程更新rqUID及上傳狀態
			List<Long> updateDeletList = new ArrayList<Long>();
			for(LiaRocUploadDetailWrapperVO removeVO : duplicateList) {
				if(updateDeletList.contains(removeVO.getUploadListId()) == false) {
					updateDeletList.add(removeVO.getUploadListId());
				}
			}
			for(Long uploadListId : updateDeletList) {
				//IR-299907-更新狀態為刪除
				liaRocUploadCI.delete(uploadListId);
			}*/
		}

		return uploadList;
	}

	public String getUpload2020Key(LiaRocUploadDetail2020WrapperVO warpperVO) {

		//當日異動檔KEY=產壽險別＋公司代號＋被保險人身分證號碼＋被保險人出生日期＋保單號碼＋保單分類＋險種分類＋險種。
		List<String> keys = new ArrayList<String>();

		//被保險人身分證號碼
		keys.add(StringUtils.nullToEmpty(warpperVO.getCertiCode()));
		//被保險人出生日期
		if (warpperVO.getBirthday() != null) {
			keys.add(DateUtils.date2String(warpperVO.getBirthday()));
		} else {
			keys.add(" ");
		}
		//保單號碼
		keys.add(warpperVO.getLiaRocPolicyCode());
		//險種分類:1   人壽保險,2   傷害保險,3   健康保險, 4 年金險
		keys.add(warpperVO.getLiaRocProductCategory());
		//險種:
		//01  一般,02  特定,03  投資型,04  日額型,05  實支實付型,
		//06  日額或實支實付擇一,07  手術型,08  重大疾病,09  帳戶型,
		//10  長期看護型,11  失能,12  防癌,13  旅行平安,14  微型,
		//15  微型實支實付型,16  小額終老保險,17  失能扶助保險 
		//18  登山綜合保險, 19  定期人壽保險, 20  海域活動綜合保險
		keys.add(warpperVO.getLiaRocProductType());

		String uploadDetailKey = org.apache.commons.lang.StringUtils.join(keys.iterator(), ",");

		return uploadDetailKey;
	}
}
