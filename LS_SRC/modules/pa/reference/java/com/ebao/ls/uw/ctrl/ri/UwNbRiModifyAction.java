package com.ebao.ls.uw.ctrl.ri;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.UserTransaction;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.ls.pa.pub.vo.NbReinsurerSpecVO;
import com.ebao.ls.uw.ds.UwRiApplyService;
import com.ebao.pub.Cst;
import com.ebao.pub.framework.GenericAction;
import com.ebao.pub.util.BeanUtils;
import com.ebao.pub.util.TransUtils;

public class UwNbRiModifyAction extends GenericAction {

	@Resource(name = UwRiApplyService.BEAN_DEFAULT)
	private UwRiApplyService uwRiApplyService;

	private static final String ACTION_SAVE = "save";
	
	@Override
	public ActionForward process(ActionMapping mapping, ActionForm form, 
				HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		UwNbRiSpecForm actionForm = (UwNbRiSpecForm) form;
		
		if(ACTION_SAVE.equals(actionForm.getSubAction())) {
		  UserTransaction ut = null;
		  try {
		    ut = TransUtils.getUserTransaction();
		    ut.begin();
	      saveNbRiSpec(actionForm);
	      ut.commit();
		  } catch (Exception e) {
		    TransUtils.rollback(ut);
		    throw e;
		  }
		}
    request.setAttribute(Cst.ACTION_FORM, loadNbRiSpec(actionForm.getReinsurerId()));
    request.setAttribute("subAction", null);
		
		return mapping.findForward("display");
	}
	
	public UwNbRiSpecForm loadNbRiSpec(Long reinsurerId) {
		UwNbRiSpecForm actionForm = new UwNbRiSpecForm();
		NbReinsurerSpecVO specVO = uwRiApplyService.loadReinsurerSpec(reinsurerId);
		BeanUtils.copyProperties(actionForm, specVO);
		actionForm.setSubAction(null);
		return actionForm;
	}
	
	public void saveNbRiSpec(UwNbRiSpecForm actionForm) {
		NbReinsurerSpecVO specVO = new NbReinsurerSpecVO();
		BeanUtils.copyProperties(specVO, actionForm);
		uwRiApplyService.reinsurerSpecSaveOrUpdate(specVO);
	}

}
