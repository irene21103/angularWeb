package com.ebao.ls.uw.ds;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.Resource;

import org.apache.commons.collections.MapUtils;

import com.ebao.ls.foundation.service.GenericServiceImpl;
import com.ebao.ls.foundation.util.BeanConvertor;
import com.ebao.ls.pty.vo.DeptVO;
import com.ebao.ls.pty.vo.EmployeeVO;
import com.ebao.ls.uw.bo.LeaveUwAuthEdit;
import com.ebao.ls.uw.data.TUwAreaDelegate;
import com.ebao.ls.uw.data.TUwAuthLeaveDelegate;
import com.ebao.ls.uw.data.bo.LeaveUwAuth;
import com.ebao.ls.uw.data.bo.UwArea;
import com.ebao.ls.uw.vo.LeaveUwAuthVO;
import com.ebao.ls.uw.vo.UwAreaVO;

/**
 * <p>Title: Module Information Andy GOD</p>
 * <p>Description: Function Description Only vegetable</p>
 * <p>Copyright: Copyright (c) 2015</p>
 * <p>Company: TGL Co., Ltd.</p>
 * <p>Create Time: Aug 11, 2015</p> 
 * @author 
 * <p>Update Time: Aug 11, 2015</p>
 * <p>Updater: Alex Cheng</p>
 * <p>Update Comments: </p>
 */
public class UwAuthLeaveDSImpl extends GenericServiceImpl<LeaveUwAuthVO, LeaveUwAuth, TUwAuthLeaveDelegate> implements UwAuthLeaveService{

  @Resource(name = TUwAuthLeaveDelegate.BEAN_DEFAULT)
  private TUwAuthLeaveDelegate uwAuthLeaveDao;
  
  @Resource(name = UwAreaService.BEAN_DEFAULT)
  private UwAreaService uwAreaService;
  
  @Resource(name = TUwAreaDelegate.BEAN_DEFAULT)
  private TUwAreaDelegate uwAreaDao;

  @Override
  protected LeaveUwAuthVO newEntityVO() {
    return new LeaveUwAuthVO();
  }

  @Override
  public LeaveUwAuthEdit findUwAuthLeave(Long leaveUwAuthListId, Long empId){
    LeaveUwAuthEdit retVO = new LeaveUwAuthEdit();
    retVO = uwAuthLeaveDao.findLeaveUwAuthByListIdEmpId(leaveUwAuthListId, empId);
    return retVO;
  }
  
  @Override
  public LeaveUwAuthEdit findUnderwriteManager(Long empId){
    LeaveUwAuthEdit retVO = new LeaveUwAuthEdit();
    retVO = uwAuthLeaveDao.findLeaveUwAuthByListIdEmpId(null, empId);
    return retVO;
  }
  
  @Override
  public List<DeptVO> findDeptByLevel(String deptId, String level){
    return uwAuthLeaveDao.findDeptByLevel(deptId, level);
  }
  
  @Override
  public List<EmployeeVO> findEmpByDeptId(String deptId){
    return uwAuthLeaveDao.findEmpByDeptId(deptId);
  }
  
  @Override
  public LeaveUwAuthEdit findDeptByEmpId(String empId){
    return uwAuthLeaveDao.findDeptByEmpId(empId);
  }
  
  @Override
  public void saveLeaveUwAuthData(LeaveUwAuthVO newVO, List<UwAreaVO> newUwAreaList) throws Exception{
    
    LeaveUwAuth leaveUwAuth = null;
    if(newVO.getListId() != null && newVO.getListId() > 0){
      leaveUwAuth = uwAuthLeaveDao.load(newVO.getListId());
    }else{
      leaveUwAuth = new LeaveUwAuth();
    }
    BeanConvertor.convertBean(leaveUwAuth, newVO, true);

    uwAuthLeaveDao.save(leaveUwAuth);
    
    List<UwArea> uwAreaList = uwAreaDao.findUwAreaByEmpId(leaveUwAuth.getEmpId());
    Map<Long ,UwArea> uwAreaMap = new HashMap<Long ,UwArea>();
    
    if(uwAreaList != null && uwAreaList.size() > 0){
      for(UwArea uwArea : uwAreaList){
        uwAreaMap.put(uwArea.getListId(), uwArea);
      }
    }
    
    if(newUwAreaList != null && newUwAreaList.size() > 0){
      UwArea oldUwArea = null;
      for(UwAreaVO newUwAreaVO : newUwAreaList){
        oldUwArea = null;
        boolean isInsert = true;
        if(newUwAreaVO.getListId() != null && newUwAreaVO.getListId() > 0){
          /* 更新 */
          oldUwArea = (UwArea)MapUtils.getObject(uwAreaMap, newUwAreaVO.getListId(), null);
          if(oldUwArea != null){
            BeanConvertor.convertBean(oldUwArea, newUwAreaVO, true);
            uwAreaDao.save(oldUwArea);
            isInsert = false;
          }
          uwAreaMap.remove(newUwAreaVO.getListId());
        }
        if(isInsert){
          /* 新增 */
          oldUwArea = new UwArea();
          BeanConvertor.convertBean(oldUwArea, newUwAreaVO);
          oldUwArea.setListId(null);
          uwAreaDao.save(oldUwArea);
        }
      }
    }
    /* 刪除剩下的舊資料 */
    if(uwAreaMap.size() > 0){
      for(Entry<Long, UwArea> entry : uwAreaMap.entrySet()){
        uwAreaDao.remove(entry.getValue());
      }
    }
  }
  
  @Override
  public List<LeaveUwAuthEdit> searchLeaveDataByEmp(
          Long deptId, Long sectionDeptId, Long empId, String empCode, 
          boolean isEditRole, Long loginDeptId) throws Exception{
    return uwAuthLeaveDao.searchLeaveDataByEmp(deptId, sectionDeptId, empId, empCode, isEditRole, loginDeptId);
  }

  @Override
  public List<LeaveUwAuthEdit> searchLeaveDataByArea(Long channelId, 
          Long communicateId, boolean isEditRole, Long loginDeptId)
          throws Exception {
    return uwAuthLeaveDao.searchLeaveDataByArea(channelId, communicateId, isEditRole, loginDeptId);
  }
  
  
}
