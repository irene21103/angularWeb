package com.ebao.ls.uw.ctrl.decision;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.UserTransaction;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.ls.escape.helper.EscapeHelper;
import com.ebao.ls.uw.ctrl.ActionUtil;
import com.ebao.ls.uw.ds.UwPolicyService;
import com.ebao.ls.uw.vo.UwRiFacVO;
import com.ebao.pub.framework.AppException;
import com.ebao.pub.framework.ExceptionFactory;
import com.ebao.pub.framework.GenericAction;
import com.ebao.pub.framework.GenericException;
import com.ebao.pub.util.Trans;
import com.ebao.pub.util.TransUtils;

/**
 * <p>
 * Title:
 * </p>
 * <p>
 * Description:
 * </p>
 * <p>
 * Copyright: Copyright (c) 2004
 * </p>
 * <p>
 * Company: eBaoTech Corporation
 * </p>
 * 
 * @author mingchun.shi
 * @version 1.0
 */

public class CancelFacRequestAction extends GenericAction {

	public static final String BEAN_DEFAULT = "/uw/cancelFacRequest";

	/**
	 * cancel the fac request
	 * 
	 * @param mapping
	 * @param form
	 * @param req
	 * @param res
	 * @return
	 * @throws Exception
	 */
	@Override
	public ActionForward process(ActionMapping mapping, ActionForm form,
			HttpServletRequest req, HttpServletResponse res)
			throws GenericException {
		
		ApplyFacRequestForm requestForm = (ApplyFacRequestForm) form;
		
		UserTransaction ut = null;
		
		String underwritingId = EscapeHelper.escapeHtml(req.getParameter("underwriteId"));
		
		EscapeHelper.escapeHtml(req).setAttribute("underwriteId", underwritingId);
		
		try {
			String[] itemIds = requestForm.getItemIds();

			Long listId = Long.valueOf(requestForm.getListId());
			
			// check record with status 'canceled','confirmed'
			checkRecordStatus(listId);

			ut = Trans.getUserTransaction();

			ut.begin();
			
			uwPolicyService.cancleFacRequest(listId);
			
			ut.commit();

			ActionUtil.setFacRequestList(uwPolicyService, req, itemIds);

		} catch (Exception e) {
			TransUtils.rollback(ut);
			throw ExceptionFactory.parse(e);
		}
	
		return mapping.findForward("display");
	}
	
	
	
	private void checkRecordStatus(Long listId) throws AppException {
		List<UwRiFacVO> requestList = uwPolicyService.findByListId(listId);

		if (requestList != null && requestList.size() > 0) {
			for (int j = 0, size = requestList.size(); j < size; j++) {
				UwRiFacVO uwRiFacVO = requestList.get(j);
				
				if (uwRiFacVO.getStatus() == UwPolicyService.FAC_REQ_STATUS_CANCEL) {
					throw new AppException(UwPolicyService.ERR_INVALID_FAC_REQ_STATUS);
				}
				
				if (uwRiFacVO.getStatus() == UwPolicyService.FAC_REQ_STATUS_CONFIRMED) {
					throw new AppException(UwPolicyService.ERR_INVALID_FAC_REQ_STATUS);
				}
			}
		}
	

	}

	@Resource(name = UwPolicyService.BEAN_DEFAULT)
	private UwPolicyService uwPolicyService;

	public UwPolicyService getUwPolicyService() {
		return uwPolicyService;
	}

	public void setUwPolicyService(UwPolicyService uwPolicyService) {
		this.uwPolicyService = uwPolicyService;
	}

}
