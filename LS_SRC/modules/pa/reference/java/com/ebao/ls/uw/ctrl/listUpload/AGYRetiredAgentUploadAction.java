package com.ebao.ls.uw.ctrl.listUpload;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.UserTransaction;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.Row;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.foundation.module.util.beanutils.BeanUtils;
import com.ebao.ls.pa.nb.vo.AGYRetiredAgentVO;
import com.ebao.ls.uw.ctrl.listUpload.helper.ListUploadActionHelper;
import com.ebao.ls.uw.ctrl.listUpload.vo.AGYRetiredAgentExtendVO;
import com.ebao.ls.uw.ds.AGYRetiredAgentService;
import com.ebao.pub.util.TransUtils;

public class AGYRetiredAgentUploadAction extends ListUploadActionHelper<AGYRetiredAgentExtendVO, AGYRetiredAgentUploadForm> {

	@Resource(name = AGYRetiredAgentService.BEAN_DEFAULT)
	protected AGYRetiredAgentService agyRetiredAgentService;
	
	/**
	 * 上傳的檔案內容存在 Session 裡的名字。
	 */
	private static final String UPLOAD_VO_SESSION_KEY = "UPLOAD_VO_SESSION_KEY_1000008398 ";
	private static final String ERR_UPLOAD_VO_SESSION_KEY = "ERROR_UPLOAD_VO_SESSION_KEY_1000008398 ";
	
	// 存放於Request的錯誤資訊KEY
	private static final String ERR_MSG_LABEL = "fileErrMsg";  
	
	// 存放於Request的執行資訊KEY
	private static final String PROC_MSG_LABEL = "processMsg";
	
	// 規範Excel存取欄位數量
	private static final int EXCEL_COLUMN_NUM = 8;

	@Override
	public ActionForward process(ActionMapping mapping, ActionForm actionForm,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		AGYRetiredAgentUploadForm form = (AGYRetiredAgentUploadForm) actionForm;

		if (StringUtils.equals("save", form.getActionType())) {// 存檔。
			
			getVoList(request, form);
			
			if (form.getErrList() == null || form.getErrList().size() == 0){
				if (form.getVoList() != null && form.getVoList().size() > 0) {
					try {
						saveList(form);
						request.setAttribute(getProcessMsgLable(), "已上傳成功");
					} catch (Exception e) {
						// errMsg
						request.setAttribute(getProcessMsgLable(), "上傳失敗");
					}
				}
			} else {
				// 有錯誤，請修正後重新上傳
				request.setAttribute(getProcessMsgLable(), "請修正錯誤後上傳");
			}
			cleanSession(request);

		} else if (StringUtils.equals("upload", form.getActionType())) { // 上傳。

			// 每次重讀都清掉上次的資料內容。
			cleanSession(request);
			form.setVoList(super.readExcelFile(actionForm, request, form.getUploadFile()));
			form.setErrList(super.getErrorList(form.getVoList()));
			setSession(request, form);
			
			if (form.getErrList().size() <= 0 && form.getVoList().size() > 0) {
				request.setAttribute("storable", true);
			}
			
			request.setAttribute("uploadFile", form.getUploadFile());
			request.setAttribute("voLists", form.getVoList());
			request.setAttribute("errSize", form.getErrList().size());
			request.setAttribute("totalSize", form.getVoList().size());
			
		} else {

			// 每次進來都把 session 裡之前上傳的檔案內容清掉。
			cleanSession(request);

		}

		return mapping.findForward("display");
	}

	/**
	 *  Row to Object
	 */
	@Override
	protected AGYRetiredAgentExtendVO readRowData(Row row, Integer countNum, ActionForm actionForm) {
		
		AGYRetiredAgentUploadForm form = (AGYRetiredAgentUploadForm) actionForm;
		
		AGYRetiredAgentExtendVO uploadVO = new AGYRetiredAgentExtendVO();
        uploadVO.setSerialNumber((long) countNum);
        Integer columnPointer = 0;
        try {
            uploadVO.setOrgAbbr(super.getCellValue(row, columnPointer++, String.class));
            uploadVO.setRegisterCode(super.getCellValue(row, columnPointer++, String.class)); // NOT NULL
            uploadVO.setName(super.getCellValue(row, columnPointer++, String.class));
            uploadVO.setAgentCate(super.getCellValue(row, columnPointer++, String.class));
            uploadVO.setDutyDate(super.getCellValue(row, columnPointer++, Date.class)); // NOT NULL
            uploadVO.setAcceptedPeriod(super.getCellValue(row, columnPointer++, Integer.class)); // NOT NULL
            uploadVO.setCaseNum(super.getCellValue(row, columnPointer++, Long.class));
            uploadVO.setFfyc(super.getCellValue(row, columnPointer++, Long.class));
            uploadVO.setBatchNo(form.getBatchNo());
            
            String checkStr = "";
            if(StringUtils.isEmpty(uploadVO.getRegisterCode())){
            	checkStr += "Data Cannot be NULL !! (At line:" + countNum + " 登錄證字號)\n";
            }
            if (uploadVO.getDutyDate() == null) {
            	checkStr += "Data Cannot be NULL !! (At line:" + countNum + " 到職日)";
            }
            if (uploadVO.getAcceptedPeriod() == null) {
            	checkStr += "Data Cannot be NULL !! (At line:" + countNum + " 受理年月)";
            }
            
            if(StringUtils.isNotEmpty(checkStr)){
            	uploadVO.setErrorMsg(checkStr);
            }
            
        } catch (Exception e) {
        	uploadVO.setErrorMsg("Format Data Error !! (At line:" + countNum + " column:" + columnPointer + ")");
        }
		return uploadVO;
	}

	protected void saveList(AGYRetiredAgentUploadForm form) throws Exception {
		List<AGYRetiredAgentVO> voList = new ArrayList<AGYRetiredAgentVO>();
		for(AGYRetiredAgentExtendVO extendVO:form.getVoList()) {
			AGYRetiredAgentVO vo = new AGYRetiredAgentVO();
			BeanUtils.copyProperties(vo, extendVO);
			voList.add(vo);
		}
		
		UserTransaction ut = null;
		try {
			ut = TransUtils.getUserTransaction();
			ut.begin();
			agyRetiredAgentService.removeAll();
			agyRetiredAgentService.uploadList(voList);
			ut.commit();
		} catch (Exception e) {
			TransUtils.rollback(ut);
			throw e;
		} 
		
	}

	@Override
	protected void getVoList(HttpServletRequest request, AGYRetiredAgentUploadForm form) {
		form.setVoList(getVoList(request, UPLOAD_VO_SESSION_KEY));
		form.setErrList(getVoList(request, ERR_UPLOAD_VO_SESSION_KEY));
	}
	
	@Override
	protected void cleanSession(HttpServletRequest request){
        request.getSession().setAttribute(UPLOAD_VO_SESSION_KEY, null);
        request.getSession().setAttribute(ERR_UPLOAD_VO_SESSION_KEY, null);
	}
	
	@Override
	protected void setSession(HttpServletRequest request, AGYRetiredAgentUploadForm form){
        request.getSession().setAttribute(UPLOAD_VO_SESSION_KEY, form.getVoList());
        request.getSession().setAttribute(ERR_UPLOAD_VO_SESSION_KEY, form.getErrList());
	}

	@Override
	protected int getExcelCellSize() {
		return EXCEL_COLUMN_NUM;
	}

	@Override
	protected String getFileErrMsgLable() {
		return ERR_MSG_LABEL;
	}

	@Override
	protected String getProcessMsgLable() {
		return PROC_MSG_LABEL;
	}

	@Override
	protected SimpleDateFormat getDateFormat() {
		return new SimpleDateFormat("yyyy/MM/dd");
	}

	@Override
	protected boolean errorCondition(AGYRetiredAgentExtendVO t) {
		return StringUtils.isNotEmpty(t.getErrorMsg());
	}

}
