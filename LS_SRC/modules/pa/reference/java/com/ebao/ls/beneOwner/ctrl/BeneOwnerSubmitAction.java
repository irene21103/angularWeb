package com.ebao.ls.beneOwner.ctrl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import com.ebao.pub.util.Env;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.UserTransaction;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.foundation.core.logging.Log;
import com.ebao.foundation.module.db.hibernate.HibernateSession3;
import com.ebao.ls.cs.boaudit.ds.BeneficialOwnerDSImpl;
import com.ebao.ls.cs.boaudit.ds.BeneficialOwnerService;
import com.ebao.ls.cs.boaudit.vo.BeneficialOwnerVO;
import com.ebao.ls.pa.pty.service.CertiCodeCI;
import com.ebao.ls.pty.ci.AddressFormatCI;
import com.ebao.ls.pty.ci.CustomerCI;
import com.ebao.ls.pty.ds.ContactService;
import com.ebao.ls.pty.ds.vo.CompanySearchConditionVO;
import com.ebao.ls.pty.vo.AddressFormatVO;
import com.ebao.ls.pty.vo.AddressVO;
import com.ebao.ls.pty.vo.CompanyCustomerVO;
import com.ebao.ls.pty.vo.CustomerVO;
import com.ebao.ls.pub.CodeCst;
import com.ebao.pub.fms.util.BeanUtils;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.framework.AppException;
import com.ebao.pub.framework.ExceptionFactory;
import com.ebao.pub.framework.GenericAction;
import com.ebao.pub.framework.GenericException;
import com.ebao.pub.framework.MultiWarning;
import com.ebao.pub.i18n.lang.CodeTable;
import com.ebao.pub.i18n.lang.StringResource;
import com.ebao.pub.util.Trans;
import com.ebao.pub.util.TransUtils;

public class BeneOwnerSubmitAction extends GenericAction {

	private final Log log = Log.getLogger(BeneOwnerSubmitAction.class);

	@Resource(name = CustomerCI.BEAN_DEFAULT)
	private CustomerCI customerCI;

	@Resource(name = BeneficialOwnerService.BEAN_DEFAULT)
	private BeneficialOwnerService beneficialOwnerService;

	@Resource(name = ContactService.BEAN_DEFAULT)
	private ContactService contactService;

	@Resource(name = AddressFormatCI.BEAN_DEFAULT)
	private AddressFormatCI adddressFormatCI;

	@Resource(name = CertiCodeCI.BEAN_DEFAULT)
	private CertiCodeCI certiCodeCI;

	@Override
	public ActionForward process(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		BeneOwnerAddForm boForm = (BeneOwnerAddForm) form;

		UserTransaction trans = null;
		try {
			trans = Trans.getUserTransaction();
			trans.begin();

			// 維護或新增法人資料
			Long companyId = updateOrCreateCompany(boForm);

			// 維護或新增法人實質受益人資料
			List<BeneficialOwnerVO> beneficialOwnerList = editBeneOwnerHandler(boForm);

			//PCR-415244 同業裁罰保險金辨識實質受益人
			//beneficialOwnerService.resetBOList(companyId, beneficialOwnerList,
			beneficialOwnerService.resetBOListUd(companyId, beneficialOwnerList,
					CodeCst.BENEFICIARY_OWNER_TYPE__DATA_SOURCE_POS);

			trans.commit();
			HibernateSession3.currentSession().flush();
		} catch (Exception ee) {
			TransUtils.rollback(trans);
			throw ExceptionFactory.parse(ee);
		}
		response.sendRedirect(Env.URL_PREFIX + "/beneOwner/BeneOwnerAdd.do?companyId="+boForm.getCompanyId()+"&submitModeFromOpen=UPDATE&saveData=Y");
		return mapping.findForward(null);
	}

	/**
	 * 新增或修改法人實質受益人Party資料
	 * 
	 * @param boForm
	 * @return
	 */
	private List<BeneficialOwnerVO> editBeneOwnerHandler(BeneOwnerAddForm boForm) {
		List<BeneficialOwnerVO> beneficialOwnerList = new ArrayList<BeneficialOwnerVO>();
		List<BeneOwnerForm> beneOwnerFormList = boForm.getBeneOwnerList();

		if (beneOwnerFormList != null) {
			for (BeneOwnerForm beneOwnerForm : beneOwnerFormList) {
				BeneficialOwnerVO beneficialOwnerVO = new BeneficialOwnerVO();

				String isCompany = beneOwnerForm.getIsCompany();
				String partyType = new String();
				if (StringUtils.equals(CodeCst.YES_NO__NO, isCompany)) {
					partyType = CodeCst.BENEFICIARY_OWNER_TYPE__PARTY_TYPE_CUSTOMER;
				} else {
					partyType = CodeCst.BENEFICIARY_OWNER_TYPE__PARTY_TYPE_COMPANY;
				}
				beneficialOwnerVO.setActualController(StringUtils.isBlank(beneOwnerForm.getActualController())?CodeCst.YES_NO__NO:beneOwnerForm.getActualController());
				beneficialOwnerVO.setPosition(beneOwnerForm.getPosition());
				beneficialOwnerVO.setShares(beneOwnerForm.getShares());
				beneficialOwnerVO.setPartyType(partyType);
				beneficialOwnerVO.setType(beneOwnerForm.getBeneOwnerType());
				Long boPartyId = beneOwnerForm.getBoPartyId();
				String beneOwnerCode = beneOwnerForm.getBeneOwnerCode();


				// PCR_499347 本次需求內容變更後t_beneficial_owner將以 partyId + beneOwnerType 為鍵值，若本次新增資料有同 BeneOwnerCode + IsCompany 資料，以舊有資料partyId為主
				if (boPartyId == null) {
					for (BeneOwnerForm form : beneOwnerFormList) {
						if(beneOwnerForm.getBeneOwnerCode().equals(form.getBeneOwnerCode()) && beneOwnerForm.getIsCompany().equals(form.getIsCompany()) && form.getBoPartyId() != null){
							boPartyId = form.getBoPartyId();
						}
					}
				}
				if (boPartyId == null) {
					// 自然人
					if (CodeCst.BENEFICIARY_OWNER_TYPE__PARTY_TYPE_CUSTOMER.equals(partyType)) {
						CustomerVO customerVO = new CustomerVO();

						initCustomerByBeneForm(customerVO, beneOwnerForm);
						if (!StringUtils.isBlank(beneOwnerCode)) {
							customerVO.setCertiType(certiCodeCI.getCertiType(beneOwnerCode));
						}
						// 新增自然人
						boPartyId = customerCI.addPerson(customerVO);
						// 法人
					} else if (CodeCst.BENEFICIARY_OWNER_TYPE__PARTY_TYPE_COMPANY.equals(partyType)) {
						CompanyCustomerVO companyCustomerVO = new CompanyCustomerVO();

						initCompanyByBeneForm(companyCustomerVO, beneOwnerForm);

						boPartyId = customerCI.addCompany(companyCustomerVO);
					}

					// PCR_499347 本次需求內容變更後t_beneficial_owner將以 partyId + beneOwnerType 為鍵值，若有一次新增兩筆同 BeneOwnerCode + IsCompany 資料，應視為同一筆partyId
					for (BeneOwnerForm form : beneOwnerFormList) {
						if(beneOwnerForm.getBeneOwnerCode().equals(form.getBeneOwnerCode()) && beneOwnerForm.getIsCompany().equals(form.getIsCompany())){
							form.setBoPartyId(boPartyId);
						}
					}
				} else {
					if (CodeCst.BENEFICIARY_OWNER_TYPE__PARTY_TYPE_CUSTOMER.equals(partyType)) {
						CustomerVO customerVO = customerCI.getPerson(boPartyId);
						if (customerVO != null) {
							CustomerVO customerTempVO = new CustomerVO();

							initCustomerByBeneForm(customerTempVO, beneOwnerForm);
							// 判斷是否有變更，先不啟用
							// boolean isDiff = beneficialOwnerService.compareDiff(customerVO,
							// customerTempVO,
							// BeneficialOwnerDSImpl.BENEFICIAL_OWNER_CUSTOMER_CHECK_TRANS.keySet());
							initCustomerByBeneForm(customerVO, beneOwnerForm);
							customerCI.updatePerson(customerVO);
						}
					} else if (CodeCst.BENEFICIARY_OWNER_TYPE__PARTY_TYPE_COMPANY.equals(partyType)) {
						CompanyCustomerVO companyVO = customerCI.getCompany(boPartyId);
						if (companyVO != null) {
							CompanyCustomerVO companyTempVO = new CompanyCustomerVO();

							initCompanyByBeneForm(companyTempVO, beneOwnerForm);
							// 判斷是否有變更，先不啟用
							// boolean isDiff = beneficialOwnerService.compareDiff(companyVO, companyTempVO,
							// BeneficialOwnerDSImpl.BENEFICIAL_OWNER_COMPANY_CHECK_TRANS.keySet());
							initCompanyByBeneForm(companyVO, beneOwnerForm);
							customerCI.updateCompany(companyVO);
						}
					}
				}

				beneficialOwnerVO.setBoPartyId(boPartyId);
				// 新增後須回壓form
				beneOwnerForm.setBoPartyId(boPartyId);
				beneficialOwnerList.add(beneficialOwnerVO);
			}
		}
		return beneficialOwnerList;
	}

	private void initCustomerByBeneForm(CustomerVO customerVO, BeneOwnerForm beneOwnerForm) {
		for (Entry<String, String> entry : BeneficialOwnerDSImpl.BENEFICIAL_OWNER_CUSTOMER_CHECK_TRANS.entrySet()) {
			BeanUtils.setProperty(customerVO, entry.getKey(), BeanUtils.getProperty(beneOwnerForm, entry.getValue()));
		}
	}

	private void initCompanyByBeneForm(CompanyCustomerVO companyVO, BeneOwnerForm beneOwnerForm) {
		for (Entry<String, String> entry : BeneficialOwnerDSImpl.BENEFICIAL_OWNER_COMPANY_CHECK_TRANS.entrySet()) {
			BeanUtils.setProperty(companyVO, entry.getKey(), BeanUtils.getProperty(beneOwnerForm, entry.getValue()));
		}
	}

	private Long updateOrCreateCompany(BeneOwnerAddForm boForm) {
		// parent_Party_id
		Long companyId = boForm.getCompanyId();
		// 統一編號
		String registerCode = boForm.getRegisterCode();
		// 免填寫實質受益人資訊
		String isGov = boForm.getIsGov();
		if (StringUtils.isBlank(isGov)) {
			isGov = CodeCst.YES_NO__NO;
		}
		// 機構類型
		String govType = boForm.getGovType();
		// 名稱
		String companyName = boForm.getCompanyName();
		// 註冊地國籍
		String regNationality = boForm.getRegNationality();
		// 註冊地郵遞區號
		String regPostCode = boForm.getRegPostCode();
		// 註冊地地址
		String regAddress = boForm.getRegAddress();
		// 營業處國籍
		String bizNationality = boForm.getBizNationality();
		// 營業處郵遞區號
		String bizPostCode = boForm.getBizPostCode();
		// 營業處地址
		String bizAddress = boForm.getBizAddress();
		// 行業代碼
		String companyCategory = boForm.getCompanyCategory();
		// 法人無記名股票之情況
		Integer bearerIndi = boForm.getBearerIndi();
		// 無記名發行股數占總發行股數之(%)
		BigDecimal bearerPercentageBD = boForm.getBearerPercentage();
		CompanyCustomerVO companyCustomerVO = new CompanyCustomerVO();
		if (companyId != null) {
			companyCustomerVO = customerCI.getCompany(Long.valueOf(companyId));
		}

		companyCustomerVO.setRegisterCode(registerCode);
		companyCustomerVO.setCompanyName(companyName);
		companyCustomerVO.setIsGov(isGov);
		companyCustomerVO.setGovType(govType);
		companyCustomerVO.setCompanyCategory(companyCategory);
		companyCustomerVO.setBearerIndi(bearerIndi);
		companyCustomerVO.setBearerPercentage(bearerPercentageBD);

		Long regAddressId = companyCustomerVO.getRegAddressId();
		Long bizAddressId = companyCustomerVO.getBizAddressId();

		regAddressId = createOrUpateAddress(regAddressId, regAddress, regNationality, regPostCode);
		bizAddressId = createOrUpateAddress(bizAddressId, bizAddress, bizNationality, bizPostCode);
		companyCustomerVO.setRegAddressId(regAddressId);
		companyCustomerVO.setBizAddressId(bizAddressId);

		if (companyId == null) {
			companyId = customerCI.addCompany(companyCustomerVO);
			boForm.setCompanyId(companyId);
		} else {
			customerCI.updateCompany(companyCustomerVO);
		}
		// 新增需要回壓
		boForm.setCompanyId(companyId);
		return companyId;
	}

	private Long createOrUpateAddress(Long addressId, String address1, String nationality, String postCode) {
		boolean needCreate = false;
		if (addressId != null) {
			AddressVO addressVO = contactService.getAddressbyAddressId(addressId);
			if (addressVO == null || addressVO.isEmpty()) {
				// 建立營業處Address檔
				needCreate = true;
			} else {
				AddressFormatVO addressFormatVo = adddressFormatCI.parse(address1);
				addressVO.setAddress1(address1);
				addressVO.setNationality(nationality);
				addressVO.setPostCode(postCode);
				addressVO.setCity(addressFormatVo.getCity());
				addressVO.setState(addressFormatVo.getState());
				contactService.updateAddress(addressVO);
			}
		} else {
			needCreate = true;
		}
		if (needCreate) {
			AddressFormatVO addressFormatVo = adddressFormatCI.parse(address1);
			AddressVO addressVO = new AddressVO();
			addressVO.setAddress1(address1);
			addressVO.setNationality(nationality);
			addressVO.setPostCode(postCode);
			addressVO.setCity(addressFormatVo.getCity());
			addressVO.setState(addressFormatVo.getState());
			addressVO.setAddressFormat("1");
			addressId = contactService.createAddress(addressVO);
		}
		return addressId;
	}

	@Override
	public MultiWarning processWarning(ActionMapping actionMapping, ActionForm actionForm, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		MultiWarning mw = new MultiWarning();
		BeneOwnerAddForm boForm = (BeneOwnerAddForm) actionForm;
		List<BeneOwnerForm> boList = boForm.getBeneOwnerList();
		// 因為前端頁面刪除，流水號可能跳號，會產生null情況
		if (boList != null) {
			Iterator<BeneOwnerForm> iter = boList.iterator();
			while (iter.hasNext()) {
				BeneOwnerForm item = iter.next();
				if (item == null) {
					iter.remove();
				}
			}
		}

		String registerCode = boForm.getRegisterCode();
		Long companyId = boForm.getCompanyId();
		String companyName = boForm.getCompanyName();
		String regNationality = boForm.getRegNationality();
		String regPostCode = boForm.getRegPostCode();
		String regAddress = boForm.getRegAddress();
		String bizNationality = boForm.getBizNationality();
		String bizPostCode = boForm.getBizPostCode();
		String bizAddress = boForm.getBizAddress();
		String companyCategory = boForm.getCompanyCategory();
		Integer bearerIndi = boForm.getBearerIndi();
		String govType = boForm.getGovType();
		String isGov = boForm.getIsGov();
		if (StringUtils.isBlank(isGov)) {
			isGov = CodeCst.YES_NO__NO;
			boForm.setIsGov(isGov);
			boForm.setGovType(null);
		}
		
		// IR-291049-非中華民國可不用輸入郵遞區號。
		boolean isEmptyError = false;
		if(StringUtils.equals(regNationality, CodeCst.NATIONALITY_CODE__TW)) {
			if(StringUtils.isBlank(regPostCode)) {
				isEmptyError = true;
			}
		}

		if(StringUtils.equals(bizNationality, CodeCst.NATIONALITY_CODE__TW)) {
			if(StringUtils.isBlank(bizPostCode)) {
				isEmptyError = true;
			}
		}
		
		if (isEmptyError || StringUtils.isBlank(registerCode) || StringUtils.isBlank(isGov) || StringUtils.isBlank(companyName)
				|| StringUtils.isBlank(regNationality) 
				|| StringUtils.isBlank(regAddress) || StringUtils.isBlank(bizNationality)
				|| StringUtils.isBlank(bizAddress)
				|| StringUtils.isBlank(companyCategory) || bearerIndi == null
				|| (CodeCst.YES_NO__YES.equals(isGov) && StringUtils.isBlank(govType))) {
			// 您有必填項目沒有輸入。
			mw.addWarning("MSG_1253969");
			mw.setContinuable(false);
		}


		
		if (companyId != null) {
			CompanyCustomerVO companyCustomerVO = customerCI.getCompany(Long.valueOf(companyId));
			if (companyCustomerVO == null || companyCustomerVO.getCompanyId() == null) {
				// 無效的companyId!
				mw.addWarning("ERR_20610020895");
				mw.setContinuable(false);
			}
		} else {
			if (StringUtils.isNotBlank(registerCode)) {
				CompanySearchConditionVO conditionVo = new CompanySearchConditionVO();
				conditionVo.setRegisterCode(registerCode);
				List<CompanyCustomerVO> companyCustomerList = customerCI.getCompanyList(conditionVo);
				if (companyCustomerList != null && companyCustomerList.size() > 0) {
					// 法人統一編號已重複不可新增。
					mw.addWarning("ERR_20610020894");
					mw.setContinuable(false);
				}
			}
		}
        
		String langId = AppContext.getCurrentUser().getLangId();
		ArrayList<String> typeList = new ArrayList<String>();

		if (boList != null) {
			for (BeneOwnerForm bo : boList) {
				String isCompany = bo.getIsCompany();
				if (!StringUtils.equals(isCompany, CodeCst.YES_NO__YES)) {
					String beneOwnerCode = bo.getBeneOwnerCode();
					if (StringUtils.isNotBlank(beneOwnerCode)) {
						Integer certiType = certiCodeCI.getCertiType(beneOwnerCode);
						if (CodeCst.CERTI_TYPE__PERSION_ID == certiType) {
							String nationality = bo.getBeneOwnerNationality();
							if (!StringUtils.equals(nationality, CodeCst.NATIONALITY_CODE__TW)) {
								// <{beneOwner}>身分證明編號符合國民生分證編碼，國籍應為中華民國。
								Map<String, String> map = new HashMap<String, String>();
								GenericException ge = new AppException(20610020893L);
								map.put("beneOwner", bo.getBeneOwnerName());
								ge.addMessageParameter(map);
								mw.addWarning(ge);
								mw.setContinuable(false);
							}
						}
					}
				}
				if(!typeList.contains(bo.getBeneOwnerType())) {
					typeList.add(bo.getBeneOwnerType());
				}
			}
		}
		if(typeList.size() != 2) {
			String ownerType = null;
			if(typeList.size() == 0) {
				ownerType = !StringUtils.equals(CodeCst.YES_NO__YES, isGov)?StringResource.getStringData("MSG_1263276", langId):CodeTable.getCodeDesc("T_BENEFICIAL_OWNER_TYPE",CodeCst.BENEFICIARY_OWNER_TYPE__SENIOR_MANAGEMENT);
			} else {
				if((typeList.size() == 1 && typeList.get(0).equals(CodeCst.BENEFICIARY_OWNER_TYPE__BENEFICIAL_OWNER))){
					ownerType =  CodeTable.getCodeDesc("T_BENEFICIAL_OWNER_TYPE",CodeCst.BENEFICIARY_OWNER_TYPE__SENIOR_MANAGEMENT);
				}
				if((typeList.size() == 1 && typeList.get(0).equals(CodeCst.BENEFICIARY_OWNER_TYPE__SENIOR_MANAGEMENT) && !StringUtils.equals(CodeCst.YES_NO__YES, isGov))){
					ownerType =  CodeTable.getCodeDesc("T_BENEFICIAL_OWNER_TYPE",CodeCst.BENEFICIARY_OWNER_TYPE__BENEFICIAL_OWNER);
				}
			}
			if(ownerType != null ) {
				Map<String, String> map = new HashMap<String, String>();
				GenericException ge = new AppException(20610020896L);
				map.put("ownerType", ownerType);
				ge.addMessageParameter(map);
				mw.addWarning(ge);
				mw.setContinuable(false);
			}
 	    }

		if (StringUtils.isNotBlank(regAddress)) {
			AddressFormatVO regAddressFormat = adddressFormatCI.parse(regAddress);
			// 註冊地址與營業地址如符合地址正規劃檢核，國籍需為中華民國，若否，提示錯誤訊息MSG-ERR-UBO-003 國籍應為中華民國。
			if (!regAddressFormat.hasError()) {
				if (!StringUtils.equals(regNationality, CodeCst.NATIONALITY_CODE__TW)) {
					Map<String, String> map = new HashMap<String, String>();
					// <{errorStr}>位於中華民國境內，國籍應為中華民國。
					GenericException ge = new AppException(20610020892L);
					// 註冊地之地址
					map.put("errorStr", StringResource.getStringData("MSG_1263272", "311"));
					ge.addMessageParameter(map);
					mw.addWarning(ge);
					mw.setContinuable(false);
				}
			}
		}

		if (StringUtils.isNotBlank(bizAddress)) {
			AddressFormatVO bizAddressFormat = adddressFormatCI.parse(bizAddress);
			// 註冊地址與營業地址如符合地址正規劃檢核，國籍需為中華民國，若否，提示錯誤訊息MSG-ERR-UBO-003 國籍應為中華民國。
			if (!bizAddressFormat.hasError()) {
				if (!StringUtils.equals(bizNationality, CodeCst.NATIONALITY_CODE__TW)) {
					Map<String, String> map = new HashMap<String, String>();
					// <{errorStr}>位於中華民國境內，國籍應為中華民國。
					GenericException ge = new AppException(20610020892L);
					// 主要營業處所地址(其他)
					map.put("errorStr", StringResource.getStringData("MSG_1263273", "311"));
					ge.addMessageParameter(map);
					mw.addWarning(ge);
					mw.setContinuable(false);
				}
			}
		}

		return mw;
	}

}
