package com.ebao.ls.liaRoc.batch;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import com.ebao.ls.cs.logger.ApplicationLogger;
import com.ebao.ls.cs.logger.helper.AppLogDataHelper;
import com.ebao.ls.liaRoc.LiaRocCst;
import com.ebao.ls.liaRoc.vo.LiaRocUploadDetail2020WrapperVO;
import com.ebao.ls.liaRoc.vo.LiaRocUploadDetailWrapperVO;
import com.ebao.pub.batch.exe.BatchDB;
import com.ebao.pub.batch.help.BatchHelp;
import com.ebao.pub.util.DateUtils;

/**
 * <h1>公會通報資料上傳 batch - 承保</h1>
 * <p>
 * 收件及承保的批處理邏輯目前皆相同，差別是查詢的待傳送資料條件不同，
 * 因此直接繼承 LiaRocUploadReceiveBatch，覆寫查詢通報明細資料的語法；
 * 把收件及承保分開 class 是考慮後續若有需求異動造成處理邏輯不同時，程式可以較容易的被擴充調整
 * </p>
 * @since 2016/03/10<p>
 * @author Amy Hung
 * <p>Update Time: </p>
 * <p>Updater: </p>
 * <p>Update Comments: </p>
*/
public class LiaRocUploadInforceBatch extends LiaRocUploadReceiveBatch {

	public static final String BEAN_DEFAULT = "liaRocUploadInforceBatch";


	@Resource(name = LiaRocUpload902Batch.BEAN_DEFAULT)
	protected LiaRocUpload902Batch liaRocUpload902Batch;

	
	@Override
	protected String getUploadType() {
		return LiaRocCst.LIAROC_UL_TYPE_INFORCE;
	}

	@Override
	protected List<LiaRocUploadDetailWrapperVO> getUploadDetails() {
		//承保跟通報batch取件後先將狀態壓成Ｑ, 避免動作中有資料寫入比對有誤
		List<String> bizSource = new ArrayList<String>();
		bizSource.add(LiaRocCst.LIAROC_UL_BIZ_SOURCE_UNB);
		bizSource.add(LiaRocCst.LIAROC_UL_BIZ_SOURCE_FOA_UNB);
		bizSource.add(LiaRocCst.LIAROC_UL_BIZ_SOURCE_POS);
		bizSource.add(LiaRocCst.LIAROC_UL_BIZ_SOURCE_FOA_POS);
		bizSource.add(LiaRocCst.LIAROC_UL_BIZ_SOURCE_CLM); /**理賠也會有承保通報**/

		liaRocUpload2019CI.uploadLiaRocUploadStatusToQ(bizSource, LiaRocCst.LIAROC_UL_TYPE_INFORCE);

		// 查詢新契約-承保的通報明細資料
		List<LiaRocUploadDetailWrapperVO> uploadList = liaRocUpload2019CI.findUploadDetailDataByCriteria(
						bizSource, LiaRocCst.LIAROC_UL_TYPE_INFORCE, "insert_time", true);

		ApplicationLogger.addLoggerData("[INFO]未移除重覆 TotalDetail: " + uploadList.size());
		List<LiaRocUploadDetailWrapperVO> finalUploadList = checkUploadKey(uploadList);
		return finalUploadList;
	}

	@Override
	public int mainProcess() throws Exception {
		int result = -1;
		if (!liaRocUploadCommonService.isLiaRocV2020Process()) {
			result = mainProcess2019();
		}
		result = mainProcess2020();
		
		result = liaRocUpload902Batch.mainProcess();
		
		return result;
	}

	public int mainProcess2019() throws Exception {

		ApplicationLogger.setSystemCode(AppLogDataHelper.SYSTEM_CODE_UNB);
		ApplicationLogger.setJobName("公會承保通報排程");
		ApplicationLogger.setPolicyCode("LiarocUploadInforce");
		ApplicationLogger.setOptionId(DateUtils.date2String(new Date(), "yyyyMMdd HHmmss"));

		if (BatchDB.getSubId() != null) {
			ApplicationLogger.addLoggerData("[INFO]INFO]Batch Job Starting.. RunId=" + BatchHelp.getRunId());
		}

		int result = this.subProcess();

		ApplicationLogger.flush();

		return result;
	}

	/* 2019 */
	/* ************************************************************************************************** */
	/* 2020 */

	@Override
	protected List<LiaRocUploadDetail2020WrapperVO> getUpload2020Details() {
		//承保跟通報batch取件後先將狀態壓成Ｑ, 避免動作中有資料寫入比對有誤
		List<String> bizSource = new ArrayList<String>();
		bizSource.add(LiaRocCst.LIAROC_UL_BIZ_SOURCE_UNB);
		bizSource.add(LiaRocCst.LIAROC_UL_BIZ_SOURCE_FOA_UNB);
		bizSource.add(LiaRocCst.LIAROC_UL_BIZ_SOURCE_POS);
		bizSource.add(LiaRocCst.LIAROC_UL_BIZ_SOURCE_FOA_POS);
		bizSource.add(LiaRocCst.LIAROC_UL_BIZ_SOURCE_CLM); /**理賠也會有承保通報**/

		liaRocUpload2020CI.uploadLiaRocUploadStatusToQ(bizSource, LiaRocCst.LIAROC_UL_TYPE_INFORCE);

		// 查詢新契約-承保的通報明細資料
		List<LiaRocUploadDetail2020WrapperVO> uploadList = liaRocUpload2020CI.findUploadDetailDataByCriteria(
						bizSource, LiaRocCst.LIAROC_UL_TYPE_INFORCE, "insert_time", true);

		ApplicationLogger.addLoggerData("[INFO]未移除重覆 TotalDetail: " + uploadList.size());
		List<LiaRocUploadDetail2020WrapperVO> finalUploadList = checkUpload2020Key(uploadList);
		return finalUploadList;
	}

	public int mainProcess2020() throws Exception {

		ApplicationLogger.setSystemCode(AppLogDataHelper.SYSTEM_CODE_UNB);
		ApplicationLogger.setJobName("公會承保通報排程2020");
		ApplicationLogger.setPolicyCode("LiarocUploadInforce");
		ApplicationLogger.setOptionId(DateUtils.date2String(new Date(), "yyyyMMdd HHmmss"));

		if (BatchDB.getSubId() != null) {
			ApplicationLogger.addLoggerData("[INFO]INFO]Batch Job Starting.. RunId=" + BatchHelp.getRunId());
		}

		int result = this.subProcess2020(LiaRocCst.LIAROC_UL_TYPE_INFORCE);

		ApplicationLogger.flush();

		return result;
	}
}
