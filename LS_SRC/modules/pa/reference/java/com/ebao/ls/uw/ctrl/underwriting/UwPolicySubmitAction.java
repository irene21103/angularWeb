package com.ebao.ls.uw.ctrl.underwriting;

import com.ebao.foundation.commons.exceptions.ExceptionUtil;
import com.ebao.foundation.module.db.DBean;
import com.ebao.foundation.module.db.hibernate.HibernateSession3;
import com.ebao.foundation.module.web.action.ExceptionInfoUtils;
import com.ebao.ls.arap.pub.ci.CashCI;
import com.ebao.ls.cmu.bs.document.DocumentManagementService;
import com.ebao.ls.cmu.bs.document.IssuesDocVO;
import com.ebao.ls.cmu.event.NbUwCompletedWithAccepted;
import com.ebao.ls.cmu.event.NbUwCompletedWithConditionallyAccepted;
import com.ebao.ls.cmu.event.NbUwCompletedWithDeclined;
import com.ebao.ls.cmu.event.NbUwCompletedWithPostponed;
import com.ebao.ls.crs.constant.CRSCodeCst;
import com.ebao.ls.crs.identity.CRSPartyLogService;
import com.ebao.ls.crs.nb.NBcrsService;
import com.ebao.ls.crs.vo.CRSPartyLogVO;
import com.ebao.ls.crs.vo.NBcrsVO;
import com.ebao.ls.cs.ci.CSCI;
import com.ebao.ls.cs.commonflow.ds.AlterationItemService;
import com.ebao.ls.cs.commonflow.ds.ApplicationService;
import com.ebao.ls.cs.commonflow.ds.rule.PosRuleService;
import com.ebao.ls.cs.commonflow.ds.sp.ChangeSP;
import com.ebao.ls.cs.commonflow.vo.AlterationItemVO;
import com.ebao.ls.cs.commonflow.vo.ApplicationVO;
import com.ebao.ls.cs.logger.ApplicationLogger;
import com.ebao.ls.cs.logger.helper.AppLogDataHelper;
import com.ebao.ls.cs.policyalteration.ds.trad.normalrevival.PolicyRevivalDS;
import com.ebao.ls.cs.rule.CSRuleService;
import com.ebao.ls.escape.helper.EscapeHelper;
import com.ebao.ls.gl.pub.ExtGLCst;
import com.ebao.ls.image.ci.ImageCI;
import com.ebao.ls.leave.service.UserLeaveManagerService;
import com.ebao.ls.liaRoc.LiaRocCst;
import com.ebao.ls.liaRoc.LiarocDownloadLogCst;
import com.ebao.ls.liaRoc.bo.LiaRocDownload2020VO;
import com.ebao.ls.liaRoc.ci.LiaRocDownload2020CI;
import com.ebao.ls.liaRoc.ci.LiaRocUploadCI;
import com.ebao.ls.liaRoc.ci.LiaRocUploadCommonService;
import com.ebao.ls.liaRoc.ds.LiarocDownloadLogHelper;
import com.ebao.ls.pa.nb.bs.DisabilityCancelDetailService;
import com.ebao.ls.pa.nb.bs.NbValidateRoleConfService;
import com.ebao.ls.pa.nb.bs.ProposalProcessService;
import com.ebao.ls.pa.nb.bs.ProposalService;
import com.ebao.ls.pa.nb.bs.rule.ProposalRuleEngineService;
import com.ebao.ls.pa.nb.bs.rule.ProposalRuleResultService;
import com.ebao.ls.pa.nb.bs.rule.ProposalRuleService;
import com.ebao.ls.pa.nb.bs.rule.vo.RuleResultVO;
import com.ebao.ls.pa.nb.ci.ValidatorService;
import com.ebao.ls.pa.nb.ctrl.detailreg.DetailRegHelper;

import static com.ebao.ls.pa.nb.util.Cst.UW_ELDER_CARE_TYPE_CALLOUT;
import static com.ebao.ls.pa.nb.util.Cst.UW_ELDER_CARE_TYPE_LIFE_SURVEY;

import com.ebao.ls.pa.nb.util.NBUtils;
import com.ebao.ls.pa.nb.vo.DisabilityCancelDetailVO;
import com.ebao.ls.pa.nb.vo.UnbRoleVO;
import com.ebao.ls.pa.pub.MsgCst;
import com.ebao.ls.pa.pub.TableCst;
import com.ebao.ls.pa.pub.bs.BeneficiaryService;
import com.ebao.ls.pa.pub.bs.InsuredService;
import com.ebao.ls.pa.pub.bs.LegalRepresentativeService;
import com.ebao.ls.pa.pub.bs.NbInsMicroRoleService;
import com.ebao.ls.pa.pub.bs.NbPolicySpecialRuleService;
import com.ebao.ls.pa.pub.bs.PolicyBankAuthService;
import com.ebao.ls.pa.pub.bs.coverage.CoverageService;
import com.ebao.ls.pa.pub.data.bo.Policy;
import com.ebao.ls.pa.pub.data.org.PolicyDao;
import com.ebao.ls.pa.pub.model.CoverageAgentInfo;
import com.ebao.ls.pa.pub.model.CoverageInfo;
import com.ebao.ls.pa.pub.model.ExtraPremInfo;
import com.ebao.ls.pa.pub.model.PolicyInfo;
import com.ebao.ls.pa.pub.ref.api.prd.ProductService;
import com.ebao.ls.pa.pub.ref.vo.prd.ProductVO;
import com.ebao.ls.pa.pub.service.PolicyService;
import com.ebao.ls.pa.pub.vo.*;
import com.ebao.ls.prd.product.vo.ProductVersionVO;
import com.ebao.ls.product.service.LifeProductCategoryService;
import com.ebao.ls.product.service.LifeProductRTService;
import com.ebao.ls.product.service.LifeProductService;
import com.ebao.ls.product.service.ProductVersionService;
import com.ebao.ls.pty.ci.LIACI;
import com.ebao.ls.pub.CodeCst;
import com.ebao.ls.pub.CodeCstTgl;
import com.ebao.ls.pub.cache.ActionDataCache;
import com.ebao.ls.pub.cache.ActionDataCacheKey;
import com.ebao.ls.pub.cst.Billcard;
import com.ebao.ls.pub.cst.NBPolicySpecialRuleType;

import static com.ebao.ls.pub.cst.YesNo.YES_NO__NA;
import static com.ebao.ls.pub.cst.YesNo.YES_NO__NO;
import static com.ebao.ls.pub.cst.YesNo.YES_NO__YES;

import com.ebao.ls.riskPrevention.bs.RiskCustomerService;
import com.ebao.ls.uw.ctrl.ActionUtil;
import com.ebao.ls.uw.ctrl.helper.LetterValidationHelper;
import com.ebao.ls.uw.ctrl.pub.UwPolicyForm;
import com.ebao.ls.uw.ds.UwChangeMsgService;
import com.ebao.ls.uw.ds.UwCheckListStatusService;
import com.ebao.ls.uw.ds.UwCommentService;
import com.ebao.ls.uw.ds.UwElderCareService;
import com.ebao.ls.uw.ds.UwMedicalLetterService;
import com.ebao.ls.uw.ds.UwPatchImageService;
import com.ebao.ls.uw.ds.UwPolicyService;
import com.ebao.ls.uw.ds.UwProcessService;
import com.ebao.ls.uw.ds.UwRiApplyService;
import com.ebao.ls.uw.ds.UwTransferService;
import com.ebao.ls.uw.ds.UwWorkSheetService;
import com.ebao.ls.uw.ds.UwWorkSheetService.ResultType;
import com.ebao.ls.uw.ds.constant.CompleteUwProcessStatusConstants;
import com.ebao.ls.uw.ds.constant.Cst;
import com.ebao.ls.uw.ds.constant.ProposalStatusConstants;
import com.ebao.ls.uw.ds.constant.UwExceptionConstants;
import com.ebao.ls.uw.ds.constant.UwExtraLoadingTypeConstants;
import com.ebao.ls.uw.ds.constant.UwSourceTypeConstants;
import com.ebao.ls.uw.ds.constant.UwStatusConstants;
import com.ebao.ls.uw.ds.sp.CompleteUWProcessSp;
import com.ebao.ls.uw.ds.vo.LeaveAuthEdit;
import com.ebao.ls.uw.ds.vo.UwIssueMedicalItemVO;
import com.ebao.ls.uw.util.Utils;
import com.ebao.ls.uw.vo.UwCommentOptionVO;
import com.ebao.ls.uw.vo.UwCommentVO;
import com.ebao.ls.uw.vo.UwElderCareTypeVO;
import com.ebao.ls.uw.vo.UwElderCareVO;
import com.ebao.ls.uw.vo.UwExclusionVO;
import com.ebao.ls.uw.vo.UwExtraLoadingVO;
import com.ebao.ls.uw.vo.UwLifeInsuredVO;
import com.ebao.ls.uw.vo.UwPolicyVO;
import com.ebao.ls.uw.vo.UwProductVO;
import com.ebao.ls.uw.vo.UwTransferRoleVO;
import com.ebao.ls.uw.vo.UwTransferVO;
import com.ebao.ls.ws.ci.aml.AmlQueryCI;
import com.ebao.ls.ws.ci.aml.civo.FircoRsDtaCIVO;
import com.ebao.pub.annotation.DataModeChgModifyPoint;
import com.ebao.pub.annotation.PAPubAPIUpdate;
import com.ebao.pub.event.ApplicationEventVO;
import com.ebao.pub.event.EventService;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.framework.AppException;
import com.ebao.pub.framework.ExceptionFactory;
import com.ebao.pub.framework.GenericAction;
import com.ebao.pub.framework.GenericException;
import com.ebao.pub.framework.GenericWarning;
import com.ebao.pub.framework.MultiWarning;
import com.ebao.pub.framework.RTException;
import com.ebao.pub.i18n.lang.CodeTable;
import com.ebao.pub.i18n.lang.StringResource;
import com.ebao.pub.para.Para;
import com.ebao.pub.util.BeanUtils;
import com.ebao.pub.util.DateUtils;
import com.ebao.pub.util.StringUtils;
import com.ebao.pub.util.Trans;
import com.ebao.pub.util.TransUtils;
import com.ebao.pub.workflow.wfmodel.WfTaskInstance;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.Resource;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import org.apache.commons.beanutils.BeanComparator;
import org.apache.commons.beanutils.BeanPredicate;
import org.apache.commons.beanutils.BeanPropertyValueEqualsPredicate;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.collections.PredicateUtils;
import org.apache.commons.collections.TransformerUtils;
import org.apache.commons.lang.ClassUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.CharUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.jfree.util.Log;
import com.ebao.ls.cmu.pub.vo.FmtPolicyVO;
import com.ebao.ls.cmu.service.FmtPolicyService;
import com.ebao.ls.pa.nb.ci.CtrsCI;
import com.ebao.ls.pa.nb.ci.ECProposalCI;
import com.ebao.ls.pa.pub.bs.PolicyRemakeDetailService;
import com.ebao.ls.prt.service.PolicyPrintJobCI;
import com.ebao.ls.prt.vo.PolicyPrintJobVO;
import com.ebao.ls.rstf.ci.ctrs.CtrsCst;
import com.ebao.ls.rstf.ci.ctrs.CtrsUtils;
import com.ebao.ls.rstf.ci.ctrs.vo.CtrsReturnOutVO;
import com.ebao.ls.rstf.ci.ec.vo.EcReturnOutVO;
import java.util.Objects;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static com.ebao.ls.cs.logger.ApplicationLogger.SYSTEM_CODE_POS;
import static com.ebao.ls.cs.logger.ApplicationLogger.SYSTEM_CODE_UNB;
import static com.ebao.ls.liaRoc.LiarocDownloadLogCst.NB_UW_SECOND_WORK_SHEET;
import static com.ebao.ls.pa.pub.MsgCst.MSG_1258901;
import static com.ebao.ls.pa.pub.MsgCst.MSG_1258902;
import static com.ebao.ls.pa.pub.MsgCst.MSG_1267582;
import static com.ebao.ls.pub.CodeCstTgl.OPQ_CHECK_UW_WORK_SHEET;
import static com.ebao.ls.pub.cst.UwSourceType.UW_SOURCE_TYPE__NB;
import static com.ebao.ls.pub.cst.UwTransferFlowType.UW_TRANS_FLOW_EXTRA_PREM_MANAGER;
import static com.ebao.ls.pub.cst.UwTransferFlowType.UW_TRANS_FLOW_FIRSTER;
import static com.ebao.ls.pub.cst.UwTransferFlowType.UW_TRANS_FLOW_MANAGER;
import static com.ebao.ls.pub.cst.UwTransferLimitType.UW_LIMIT_TYPE_7;
import static com.ebao.ls.pub.cst.UwTransferRole.UW_ESC_ROLE_G11;
import static com.ebao.ls.pub.cst.UwTransferRole.UW_ESC_ROLE_G12;
import static com.ebao.ls.pub.cst.UwTransferRole.UW_ESC_ROLE_G13;
import static com.ebao.ls.uw.ds.UwWorkSheetService.ResultType.SUCCESS;
import static com.ebao.ls.uw.ds.constant.Cst.POLICY_DECISION_ACCEPT;
import static com.ebao.ls.uw.ds.constant.UwExceptionConstants.APP_UW_COMPLETE_PRE_PROPOSAL_STATUS;
import static com.ebao.ls.uw.ds.constant.UwSourceTypeConstants.NEW_BIZ;
import static com.ebao.ls.uw.ds.constant.UwStatusConstants.ESCALATED;
import static com.ebao.ls.uw.ds.constant.UwStatusConstants.IN_PROGRESS;
import static com.ebao.ls.uw.ds.constant.UwStatusConstants.WAITING;



/**
 * <p> Title:Underwriting</p>
 * <p>Description:</p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: eBaoTech Corporation</p>
 * 
 * @author mingchun.shi
 * @since 2005-11-1
 * @version 1.0
 */
public class UwPolicySubmitAction extends GenericAction {
	private static final Logger logger = LogManager.getFormatterLogger();

	private static final int MEDICAL_COMPARE_AMOUNT_1 = 500000;

	/* Uw sharing pool's action mapping logic name */
	public static final String UW_SHARINGPOOL_URL = "sharingPoolMainPage";

	public static final String UW_NB_WORKFLOW_PAGE = "nbWorkflowPage";

	/* Uw manual escalating action mapping logic name */
	public static final String UW_MANUAL_ESCALTING_URL = "manualEscaltingPage";

	/* Uw manual escalating action mapping logic name */
	public static final String IN_FORCE_FAILURE_URL = "inForceFailurePage";

	public static final String UW_COMPLETE_STATUS = "uwCompleteStatus";

	public static final String CS_SUCCESS_URL = "csSuccess";

	public static final String CS_UW_DISP_SUBMIT = "csUwDispSubmit";

	public static final String CS_UW_DISP_ENTER = "csUwDispEnter";

	public static final String BEAN_DEFAULT = "/uw/uwSubmit";

    /** 是否法人實質受益人須辨識保單 */
    boolean isLegalBeneEvalPolicy = false;
    /** 是否 實質受益人辨識 或 身心障礙件 或 高齡投侃件 結果需交叉覆核 */
    boolean isNeedLegalBeneEvalPolicy = false;
    /** 是否保單覆核 */
    boolean isPolicyNeedsTransfer = false;

	@Resource(name = ProposalRuleEngineService.BEAN_DEFAULT)
	private ProposalRuleEngineService proposalRule;

	@Resource(name = UwRiApplyService.BEAN_DEFAULT)
	protected UwRiApplyService uwRiApplyService;

	@Resource(name = UwWorkSheetService.BEAN_DEFAULT)
	private UwWorkSheetService uwWorkSheetService;

	@Resource(name = LegalRepresentativeService.BEAN_DEFAULT)
	private LegalRepresentativeService legalRepresentativeService;

	@Resource(name = LiaRocDownload2020CI.BEAN_DEFAULT)
	private LiaRocDownload2020CI liaRocDownload2020CI;
	
	@Resource(name = NbPolicySpecialRuleService.BEAN_DEFAULT)
	private NbPolicySpecialRuleService nbSpecialRuleService;

	public ProposalRuleEngineService getProposalRule() {
		return proposalRule;
	}

	public void setProposalRule(ProposalRuleEngineService proposalRule) {
		this.proposalRule = proposalRule;
	}

	@Resource(name = UwChangeMsgService.BEAN_DEFAULT)
	protected UwChangeMsgService uwChangeMsgService;

	@Resource(name = CSRuleService.BEAN_DEFAULT)
	private CSRuleService csRuleService;

	@Resource(name = PosRuleService.BEAN_DEFAULT)
	private PosRuleService posRuleService;
	
	@Resource(name = UwMedicalLetterService.BEAN_DEFAULT)
	private UwMedicalLetterService uwMedicalLetterService;

    @Resource(name = AmlQueryCI.BEAN_DEFAULT)
    private AmlQueryCI amlQueryCI;
    
    @Resource(name = DisabilityCancelDetailService.BEAN_DEFAULT)
    private DisabilityCancelDetailService disabilityCancelDetailService;
    
	@Resource(name = NbInsMicroRoleService.BEAN_DEFAULT)
	protected NbInsMicroRoleService nbInsMicroRoleService;

	@Resource(name = CSCI.BEAN_DEFAULT)
	protected CSCI csCi;

    /**
     * process action warning
     *
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return @throws Exception
     */
    @Override
    protected MultiWarning processWarning(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        // retrieve underwrite id from request
        ActionDataCache.clear();
        Long underwriteId = Long.valueOf(com.ebao.ls.encode.encoder.Encode.encodeForHTML(request).getParameter("underwriteId"));

        UwPolicyVO uwPolicyVO = uwPolicyDS.findUwPolicy(underwriteId);

        // judge if the underwriting is newbiz underwriting ,cs underwriting or
        // claim underwriting
        String uwSourceType = uwPolicyVO.getUwSourceType();
        boolean isNewbiz = UW_SOURCE_TYPE__NB.equals(uwSourceType);
        boolean isCs = Utils.isCsUw(uwSourceType);
        boolean isClaim = Utils.isClaimUw(uwSourceType);
        // add a more check whether uw record is valid
        String uwStatus = uwPolicyVO.getUwStatus();
        if (!IN_PROGRESS.equals(uwStatus)
                && !WAITING.equals(uwStatus)
                && !ESCALATED.equals(uwStatus)) {
            throw new AppException(
                    APP_UW_COMPLETE_PRE_PROPOSAL_STATUS,
                    "Invalid UnderWriting Status.");
        }
        // very ugly
        String medicalIndi = request.getParameter("medicalExamIndi");
        EscapeHelper.escapeHtml(request).setAttribute("medicalExamIndi", medicalIndi);

        if (NEW_BIZ.equals(uwPolicyVO.getUwSourceType())) {
            ApplicationLogger.setSystemCode(SYSTEM_CODE_UNB);
            ApplicationLogger.setOptionId(DateUtils.date2String(new Date(), "yyyyMMdd HHmmss"));
        } else {
            ApplicationLogger.setSystemCode(SYSTEM_CODE_POS);
            if (uwPolicyVO.getChangeId() != null) {
                ApplicationLogger.setOptionId(uwPolicyVO.getChangeId().toString());
            }
        }

        ApplicationLogger.setJobName(ClassUtils.getShortClassName(UwPolicySubmitAction.class));
        ApplicationLogger.setPolicyCode(uwPolicyVO.getPolicyCode());
        ApplicationLogger.addLoggerData(com.ebao.pub.util.EnvUtils.getEnvLabel());
        ApplicationLogger.addLoggerData("###### processWarning()");
        ApplicationLogger.addLoggerData("PolicyDecision=" + uwPolicyVO.getPolicyDecision());

        MultiWarning multiWarning = null;

        // request.setAttribute("clickByContinueButtonIndicator", "Y");
        /* PCR-480318 目前先採用核保時更新BO */
        if (!uwTransferService.reLoadUwTransferRoleBO()) {
            throw new GenericException(10010020006L, "重新載入核保陳核角色表UwTransferRoleBO失敗");
        }

        Long currUser = AppContext.getCurrentUser().getUserId();

        try {
            if (isNewbiz || isCs) {
                // helper.defineUwTransferRole(request, (UwPolicyForm)form,
                // underwriteId, "false");
                // for NB's & CS's check
                ApplicationLogger.addLoggerData("processSubmitWarning() begin");
                multiWarning = processSubmitWarning(request, underwriteId, uwPolicyVO, uwSourceType);

                ApplicationLogger.addLoggerData("processSubmitWarning() end");
                ApplicationLogger.addLoggerData("AF processSubmitWarning, currUser=" + currUser + ", ManuEscIndi=" + uwPolicyVO.getManuEscIndi() + ", PolicyDecision=" + uwPolicyVO.getPolicyDecision()
                        + ", " + uwPolicyVO.getUwTransferFlow() + ", UwTransferFlow=" + uwPolicyVO.getUwTransferFlow());

                if (!YES_NO__YES.equals(uwPolicyVO.getManuEscIndi())) { // 不為人工陳核
                    //核保通過，執行基本資料檢核
                    if (POLICY_DECISION_ACCEPT.equals(uwPolicyVO.getPolicyDecision())) {
                        if (UW_TRANS_FLOW_FIRSTER == uwPolicyVO.getUwTransferFlow()
                                || UW_TRANS_FLOW_MANAGER == uwPolicyVO.getUwTransferFlow()) {
                            /**
                             * 核保通過的檢核*
                             */
                            ApplicationLogger.addLoggerData("checkInforce() begin");
                            checkInforce(multiWarning, underwriteId, uwPolicyVO);
                            ApplicationLogger.addLoggerData("checkInforce() end");
                            if (multiWarning != null && multiWarning.getWarnings().size() > 0) {
                                return multiWarning;
                            }
                        }
                    }

                    if (isNewbiz || (isCs && POLICY_DECISION_ACCEPT.equals(uwPolicyVO.getPolicyDecision()))) {
                        ApplicationLogger.addLoggerData("UwTransferFlow=" + uwPolicyVO.getUwTransferFlow() + ", UwCommentIndi=" + uwPolicyVO.getUwCommentIndi());
                        // 為初始核保員，或為主管簽核流程，主管簽核同意，才執行權限表檢核
                        if (UW_TRANS_FLOW_FIRSTER == uwPolicyVO.getUwTransferFlow()
                                || YES_NO__YES.equals(uwPolicyVO.getUwCommentIndi())) {
                            //新契約作業執行aml cache作業
                            if (isNewbiz) {
                                FircoRsDtaCIVO fircoRsData = amlQueryCI.queryAmlUnb(uwPolicyVO.getPolicyId());
                                NBUtils.logger(this.getClass(), "load aml first !");
                                ActionDataCache.addCache(ActionDataCacheKey.NB_AML,
                                        uwPolicyVO.getPolicyId(), fircoRsData);
                            }

                            ApplicationLogger.addLoggerData("uwTransferHelper.isPolicyNeedsTransfer() begin");
                            Map<String, Object> result = uwTransferHelper.isPolicyNeedsTransfer(uwPolicyVO, currUser, uwSourceType);
                            ApplicationLogger.addLoggerData("uwTransferHelper.isPolicyNeedsTransfer() end");
                            isPolicyNeedsTransfer = (Boolean) result.get("result");
                            List<Integer> allLimitType = (List<Integer>) MapUtils.getObject(result, "allLimitType", Collections.EMPTY_LIST);
                            List<Integer> t_allLimitType = new ArrayList<>();
                            CollectionUtils.selectRejected(allLimitType, PredicateUtils.equalPredicate(UW_LIMIT_TYPE_7), t_allLimitType);
                            // 新契約-實質受益人辨識結果交叉覆核
                            isLegalBeneEvalPolicy = uwTransferHelper.isLegalBeneEvalPolicy(uwPolicyVO);
                            // 新契約-身心障礙投保件判斷
                            boolean isDisabilityCoverage = uwTransferHelper.isDisabilityCoverage(uwPolicyVO);
                            // 新契約-高齡投保件(高齡投保評估量表&關懷)
                            PolicyVO policyVO = policyDS.load(uwPolicyVO.getPolicyId());
                            boolean isElderScaleCoverage = uwTransferHelper.isElderScaleCoverage(uwSourceType, policyVO);
                            // 新契約-洗錢高風險件判斷
                            boolean isHighRisk = uwTransferHelper.isHighRisk(uwPolicyVO, allLimitType);
                            // 新契約-是否進行交叉覆核
                            isNeedLegalBeneEvalPolicy = uwTransferHelper.isNeedLegalBeneEvalPolicy(uwPolicyVO, isLegalBeneEvalPolicy, isDisabilityCoverage, isElderScaleCoverage, isHighRisk);

                            //是否為加費/批註主管簽核
                            boolean isExtraPremManager = Objects.equals(UW_TRANS_FLOW_EXTRA_PREM_MANAGER, uwPolicyVO.getUwTransferFlow());

                            ApplicationLogger.addLoggerData(String.format("isHighRisk=%s ", isHighRisk));
                            ApplicationLogger.addLoggerData(String.format("UwTransferHelper.isPolicyNeedsTransfer=%s", result));
                            ApplicationLogger.addLoggerData(String.format("isPolicyNeedsTransfer=%s", isPolicyNeedsTransfer));
                            ApplicationLogger.addLoggerData(String.format("isLegalBeneEvalPolicy=%s", isLegalBeneEvalPolicy));
                            ApplicationLogger.addLoggerData(String.format("isDisabilityCoverage=%s", isDisabilityCoverage));
                            ApplicationLogger.addLoggerData(String.format("isElderScaleCoverage=%s", isElderScaleCoverage));
                            ApplicationLogger.addLoggerData(String.format("isExtraPremManager=%s", isExtraPremManager));
                            ApplicationLogger.addLoggerData(String.format("isNeedLegalBeneEvalPolicy=%s", isNeedLegalBeneEvalPolicy));
                            ApplicationLogger.addLoggerData(String.format("allLimitType=%s, t_allLimitType=%s", allLimitType, t_allLimitType));

                            if (isPolicyNeedsTransfer || isNeedLegalBeneEvalPolicy) {
                                ApplicationLogger.addLoggerData("uwTransferService.findUwTransferRoleByUser() begin");
                                UwTransferRoleVO roleVO = uwTransferService.findUwTransferRoleByUser(currUser);
                                ApplicationLogger.addLoggerData("uwTransferService.findUwTransferRoleByUser() end");

                                if (roleVO != null) {
                                    ApplicationLogger.addLoggerData("roleVO.getRoleId()=" + roleVO.getRoleId());
                                } else {
                                    ApplicationLogger.addLoggerData("roleVO is null.");
                                }

                                // 檢查是否有未設定之覆核主管
                                Set<String> leakMessage = uwTransferHelper.getTransferUserLeakMessage(currUser, uwSourceType, isPolicyNeedsTransfer, isNeedLegalBeneEvalPolicy, isHighRisk, isLegalBeneEvalPolicy, isDisabilityCoverage, isElderScaleCoverage);

                                //PCR-480318 new rule >=G11提示需EC主管簽核,確認後繼續執行
                                if (roleVO != null && uwTransferService.getUwTransferRoleBO()
                                        .isGreateEquals(roleVO.getRoleId(), UW_ESC_ROLE_G11, false)) { //判斷是否為經理、專案經理G11
                                    if (checkWarningContainMSG1266560(multiWarning)) {
                                        // 當檢核「法人實質受益人辨識評估結果」未填寫時且核保人員本身為經理不應該讓核保流程繼續(需解決必填欄位問題)
                                        multiWarning.setContinuable(false);
                                        return multiWarning;
                                    } else if (!leakMessage.isEmpty()) {
                                        leakMessage.forEach(multiWarning::addWarning);
                                        multiWarning.setContinuable(false);
                                        return multiWarning;
                                    } else if (!isNeedLegalBeneEvalPolicy && isPolicyNeedsTransfer && !isExtraPremManager) {
                                        //20230627 加費簽核不進此流程
                                        //UAT-Internal Issue 533891 僅只有在這裡設置的multiwarning才要顯示PCR-480318新增的三個按鈕
                                        request.setAttribute("enableWarningChange", true);

                                        //PCR-531509
                                        //G12等級核保員額外檢查是否超過G13授權額度
                                        if (roleVO.getRoleId().longValue() == UW_ESC_ROLE_G12.longValue()) {
                                            if (result.containsKey("roleId")
                                                    && Long.valueOf(result.get("roleId").toString()).longValue() == UW_ESC_ROLE_G13.longValue()) {
                                                multiWarning.setContinuable(true);
                                                multiWarning.addWarning(MSG_1267582);
                                                request.setAttribute("disableDoWarningTransfer", true);
                                                return multiWarning;
                                            }
                                        }
                                        //若核保員核保授權職級>=G11，是保單覆核{           
                                        multiWarning.setContinuable(true);
                                        multiWarning.addWarning(MsgCst.MSG_1262163);
                                        return multiWarning;
                                    }
                                } else {
                                    Long transerUserId = null;

                                    if (UW_SOURCE_TYPE__NB.equals(uwSourceType)) {
                                        ApplicationLogger.addLoggerData(String.format("processWarning checkTransferUserId start transerUserId=%s, currUser=%s", transerUserId, currUser));
                                        // 有未設定之覆核主管
                                        if (!leakMessage.isEmpty()) {
                                            leakMessage.forEach(multiWarning::addWarning);
                                            multiWarning.setContinuable(false);
                                            return multiWarning;
                                        }
                                        transerUserId = uwTransferHelper.checkTransferUserId(currUser, uwSourceType, isPolicyNeedsTransfer, isNeedLegalBeneEvalPolicy,
                                                isHighRisk, isLegalBeneEvalPolicy, isDisabilityCoverage, isElderScaleCoverage);
                                        ApplicationLogger.addLoggerData(String.format("processWarning checkTransferUserId end transerUserId=%s, currUser=%s", transerUserId, currUser));
                                    } else {
                                        transerUserId = uwTransferHelper.getTransferUser(currUser, uwSourceType);
                                    }

                                    ApplicationLogger.addLoggerData("transerUserId=" + transerUserId);
                                    if (transerUserId == null) {
                                        multiWarning.addWarning(MSG_1258902);
                                        multiWarning.setContinuable(false);
                                        return multiWarning;
                                    }
                                }
                            }
                        }
                    }
                } else {
                    // 人工陳核須確認是否有設定覆核/主管
                    multiWarning = new MultiWarning();

                    ApplicationLogger.addLoggerData("UwEscaUser=" + uwPolicyVO.getUwEscaUser());

                    if (uwPolicyVO.getUwEscaUser() == null) {
                        multiWarning.addWarning(MSG_1258901);
                        multiWarning.setContinuable(false);
                        return multiWarning;
                    }
                }
                // when cs-uw,synchronize records
                if (isCs) {
                    CompleteUWProcessSp.synUWInfo4CS(uwPolicyVO.getPolicyId(),
                            uwPolicyVO.getUnderwriteId(), uwPolicyVO.getChangeId(),
                            Integer.valueOf(1));
                }
                //NB warning info
                //IR304338 multiWarning沒有錯誤訊息才檢查warning，否則錯誤會被蓋掉
                if (isNewbiz && multiWarning.getWarnings().size() == 0) {
                    multiWarning = processNBWarningCheck(underwriteId, uwPolicyVO);
                    if (multiWarning != null && multiWarning.getWarnings().size() > 0) {
                        return multiWarning;
                    }
                }
            } else if (isClaim) {
                // for claim's check
                multiWarning = processClaimSubmit(request, underwriteId, uwPolicyVO);
            }

        } catch (Exception e) {
            ApplicationLogger.addLoggerData(ExceptionUtils.getFullStackTrace(e));
            try {
                ApplicationLogger.flush();
            } catch (Exception e1) {
                logger.info("ApplicationLogger.flush() : " + e1.getMessage(), e1);
            }
            throw ExceptionFactory.parse(e);
        }

        UserTransaction trans = Trans.getUserTransaction();
        try {
            trans.begin();
            // modified by hanzhong.yan for cq:GEL00032134 2007/9/7 ,override
            // the
            // method below , add a indicator,but it is always transport false
            // in
            // processwaring method

            boolean isWarnningOnCheckFirstApproval = false;
            if (multiWarning == null || multiWarning.getWarnings().size() == 0) {
                if (isCs) {
                    ApplicationLogger.addLoggerData("No Warnings, PolicyDecision=" + uwPolicyVO.getPolicyDecision() + ", UwTransferFlow=" + uwPolicyVO.getUwTransferFlow());
                }
                boolean isCompleUnderwriting = true;
                if (isNewbiz || (isCs && POLICY_DECISION_ACCEPT.equals(uwPolicyVO.getPolicyDecision()))) {
                    /* 執行首次向上轉呈有警示訊息 */
                    if (isNewbiz && UW_TRANS_FLOW_FIRSTER == uwPolicyVO.getUwTransferFlow()) {
                        if (multiWarning == null || multiWarning.isEmpty()) {
                            //執行OPQ+RMS重新校驗
                            ApplicationLogger.addLoggerData("checkFirstApproval() begin");
                            multiWarning = checkFirstApproval(uwPolicyVO, uwSourceType);
                            ApplicationLogger.addLoggerData("checkFirstApproval() end");
                        }

                        if (multiWarning != null && multiWarning.getWarnings().size() > 0) {
                            isWarnningOnCheckFirstApproval = true;
                        }
                    }

                    if (isCs) { // IR412673 核保頁面，未產生有未處理RMS錯誤訊息，來源為保全均須檢核RMS錯誤訊息
                        ApplicationLogger.addLoggerData("checkFirstApproval() begin");
                        multiWarning = checkFirstApproval(uwPolicyVO, uwSourceType);
                        ApplicationLogger.addLoggerData("checkFirstApproval() end");
                        // PCR434704_新增保單風險屬性及職業代碼不同檢核-保全規則引擎檢核  修案問題。
                        if (multiWarning != null && multiWarning.getWarnings().size() > 0) {
                            isWarnningOnCheckFirstApproval = true;
                        }
                    }

                    if (multiWarning != null && multiWarning.getWarnings().size() > 0) {
                        isCompleUnderwriting = false;
                        if (isCs) {
                            ApplicationLogger.addLoggerData("Got Warnings, size=" + multiWarning.getWarnings().size());
                        }
                    } else {

                        ApplicationLogger.addLoggerData("uwTransferHelper.completeCurrentUwTransfer() begin");
                        isCompleUnderwriting = uwTransferHelper.completeCurrentUwTransfer(request, uwPolicyVO, uwSourceType);
                        ApplicationLogger.addLoggerData("uwTransferHelper.completeCurrentUwTransfer() end");

                        /* 下核保決定(任何決定)產生第二版契審表 ,但需檢核核保通過時再次檢核是否有產生核保訊息   */
                        if (isNewbiz && isCompleUnderwriting) {
                            String param = Para.getParaValue(OPQ_CHECK_UW_WORK_SHEET);
                            if (YES_NO__YES.equals(param)) {
                                ApplicationLogger.addLoggerData("uwWorkSheetService.generateWorkSheet() begin");

                                LiarocDownloadLogHelper.initNB(NB_UW_SECOND_WORK_SHEET, uwPolicyVO.getPolicyId());

                                ResultType uwWorkSheetresult = uwWorkSheetService.generateWorkSheet(
                                        uwPolicyVO.getPolicyId(),
                                        uwPolicyVO.getUnderwriteId());
                                ApplicationLogger.addLoggerData("uwWorkSheetService.generateWorkSheet() end");
                                if (SUCCESS.equals(uwWorkSheetresult) == false) {
                                    /* 契審表產生失敗 */
                                    multiWarning.addWarning(uwWorkSheetresult.getMsgId());
                                    multiWarning.setContinuable(false);

                                    isCompleUnderwriting = false;
                                }
                            } else {
                                ApplicationLogger.addLoggerData(String.format("注意：OPQ_CHECK_UW_WORK_SHEET目前設定為%s", param));
                            }

                            if (isCompleUnderwriting) {
                                ApplicationLogger.addLoggerData("uwTransferHelper.doUwTransferFinish() begin");
                                uwTransferHelper.doUwTransferFinish(request, uwPolicyVO);
                                ApplicationLogger.addLoggerData("uwTransferHelper.doUwTransferFinish() end");
                            }
                        }
                    }

                }
                if (isCompleUnderwriting && (!isCs || POLICY_DECISION_ACCEPT.equals(uwPolicyVO.getPolicyDecision()))) {
                    // PCR 373515 保留核保決定資料記錄
                    if (isNewbiz) {
                        this.processLog(uwPolicyVO.getPolicyId());
                    }
                    // 不為人工陳核，且已簽核完成，執行核保決定
                    ApplicationLogger.addLoggerData("compleUnderwriting begin");
                    compleUnderwriting(request, uwPolicyVO, uwSourceType, multiWarning, false);

                    ApplicationLogger.addLoggerData("compleUnderwriting end");
                }

                ApplicationLogger.addLoggerData("isWarnningOnCheckFirstApproval=" + isWarnningOnCheckFirstApproval);
                ApplicationLogger.addLoggerData("isCompleUnderwriting=" + isCompleUnderwriting);

                request.setAttribute("isCompleUnderwriting", isCompleUnderwriting);
            }
            if (multiWarning != null && multiWarning.getWarnings().size() > 0) {
                if (isCs && isWarnningOnCheckFirstApproval) {
                    // [RTC 241724] 保全核保RMS重新校驗有寫入檢核訊息, 不應將訊息 rollback 
                    trans.commit();
                } else {
                    trans.rollback();
                }
                return multiWarning;
            }

            trans.commit();
        } catch (Exception e) {
            TransUtils.rollback(trans);
            ApplicationLogger.addLoggerData(ExceptionUtils.getFullStackTrace(e));
            throw ExceptionFactory.parse(e);
        } finally {

            try {
                ApplicationLogger.flush();
            } catch (Exception e) {
                logger.info("ApplicationLogger.flush() : " + e.getMessage(), e);
            }

            try {
                LiarocDownloadLogHelper.flush();
            } catch (Exception ex) {
                logger.info("LiarocDownloadLogHelper.flush() : " + ex.getMessage(), ex);
            }
        }
        return multiWarning;
    }

    private boolean checkWarningContainMSG1266560(MultiWarning multiWarning) {
        if (multiWarning != null) {
            List warnings = multiWarning.getWarnings();
            return CollectionUtils.exists(warnings, PredicateUtils.transformedPredicate(
                    TransformerUtils.invokerTransformer("getStrId"),
                    PredicateUtils.equalPredicate("MSG_1266560")));
        }
        return false;
    }

	private MultiWarning checkFirstApproval(UwPolicyVO uwPolicyVO, String uwSourceType) {
		MultiWarning multiWarning = new MultiWarning();
		boolean isNewbiz = CodeCst.UW_SOURCE_TYPE__NB.equals(uwSourceType);
		boolean isCs = Utils.isCsUw(uwSourceType);
		if (isNewbiz) {// 新契約
			return checkFirstApprovalForUNB(uwPolicyVO);

		} else if (isCs) {// 保全
			return checkFirstApprovalForCS(uwPolicyVO);
		}
		return multiWarning;
	}

	/**
	 * <p>Description : 核保決定「通過」，第一次向上轉呈執行OPQ+RMS校驗、及未確認的核保中異動檢核(已在checkInforce處理)、產出第二版契審表</p>
	 * <p>Created By : simon.huang</p>
	 * <p>Create Time : Jan 2, 2017</p>
	 * @param uwPolicyVO
	 * @return
	 */
	private MultiWarning checkFirstApprovalForUNB(UwPolicyVO uwPolicyVO) {
		MultiWarning multiWarning = new MultiWarning();

		/* 初始核保員第一次向上轉呈, 執行OPQ+RMS、核保中異動及契審表產生 */
		Long underwriteId = uwPolicyVO.getUnderwriteId();
		Long policyId = uwPolicyVO.getPolicyId();
		Long currUser = AppContext.getCurrentUser().getUserId();
		UwTransferVO firstlyVO = uwTransferService.findFirstlyUwTransfer(underwriteId);

		/* 不為人工陳核且為初始核保員下核保決定 */
		if (!CodeCst.YES_NO__YES.equals(uwPolicyVO.getManuEscIndi())
						&& firstlyVO.getTransUnderwriterId().equals(currUser)) {

			/* 核保通過執行校驗 */
			if (Cst.POLICY_DECISION_ACCEPT.equals(uwPolicyVO.getPolicyDecision())) {

				//PCR-363527 計算保費與生效日 2020/06/07 Add by Kathy
				proposalService.doSubmitDetailRegistration(uwPolicyVO.getPolicyId());
			
				String capsilStatus = policyService.getCapsilStatus(policyId);
				
				if(liaRocDownload2020CI.isLiaRocV2020DownloadDate()) {
					//新式公會開始下載日期-執行終止保單資料下載
					ApplicationLogger.addLoggerData("downloadByUnbTerminateCheck() begin");
					multiWarning = downloadByUnbTerminateCheck(uwPolicyVO.getPolicyId());
					ApplicationLogger.addLoggerData("downloadByUnbTerminateCheck() end");
				}
				
				//PCR 不為在途件才執行OPQ+RMS校驗
				if (StringUtils.isNullOrEmpty(capsilStatus)) {

					UserTransaction userTransaction2 = null;
					try {
						userTransaction2 = Trans.getUserTransaction();
						userTransaction2.begin();
						/* 核保中修改，重新執行OPQ欄規則校驗 */
						if(multiWarning.isEmpty()) {
							LiarocDownloadLogHelper.initNB(LiarocDownloadLogCst.NB_UW_DECISION_CHECK_RULE, policyId);
							boolean isEcOffline = policyDS.isEcOffline(policyId);
							if (isEcOffline) {
								proposalService.checkProposalRules(policyId, ProposalRuleService.ROLESSET_VERIFICATION_EC_OFFLINE);
							} else {
								proposalService.checkProposalRules(policyId, ProposalRuleService.ROLESSET_UW_MODIFIY);
							}
						}
						
						try {
							LiarocDownloadLogHelper.flush();
						} catch (Exception ex) {
							logger.info("LiarocDownloadLogHelper.flush() : " + ex.getMessage(), ex);
						}
						userTransaction2.commit();
					} catch (Exception e) {
						TransUtils.rollback(userTransaction2);
						throw ExceptionFactory.parse(e);
					} finally {
						LiarocDownloadLogHelper.clear();
					}
					/* 2017/01/02 by simon.huang 檢核是否因為再次檢核產生，新的OPQ未處理訊息 */
					if(multiWarning.isEmpty()) {
						boolean hasPendingIssue = proposalRuleResultService.hasPendingIssue(policyId);
						if (hasPendingIssue) {
							/* MSG_1259179 存在待處理的核保信息，請確認    */
							multiWarning.addWarning("MSG_1259179");
							multiWarning.setContinuable(false);
						}
					}
				}
			}
		}

		return multiWarning;
	}

	/**
	 * 保全規則校驗
	 * @param uwPolicyVO
	 * @return
	 */
	private MultiWarning checkFirstApprovalForCS(UwPolicyVO uwPolicyVO) {
		MultiWarning multiWarning = new MultiWarning();

		/* 不為人工陳核且核保通過 */
		if (!CodeCst.YES_NO__YES.equals(uwPolicyVO.getManuEscIndi()) && Cst.POLICY_DECISION_ACCEPT.equals(uwPolicyVO.getPolicyDecision())) {
			/* 初始核保員第一次向上轉呈, 執行RMS */
			Long underwriteId = uwPolicyVO.getUnderwriteId();
			Long currUser = AppContext.getCurrentUser().getUserId();
			UwTransferVO firstlyVO = uwTransferService.findFirstlyUwTransfer(underwriteId);
			// pcr_457817 記錄公會下載的log
			Long policyId = uwPolicyVO.getPolicyId();
			// IR412673 核保頁面，未產生有未處理RMS錯誤訊息，來源為保全均須檢核RMS錯誤訊息
			//if (firstlyVO.getTransUnderwriterId().equals(currUser)) {
				// 主管已簽核完成-同意，核保員再次執行,非首次核保,直接return;
				if (uwPolicyVO.getLastUnderwriterId() != null && CodeCst.YES_NO__YES.equals(uwPolicyVO.getUwCommentIndi())) {
					// 直接return;
					return multiWarning;
				}

				// 執行RMS校驗
				try {
					//pcr_457817 記錄下載公會的log
					LiarocDownloadLogHelper.initNB(LiarocDownloadLogCst.POS_RULE_004_CHECK, policyId);
					// RMS檢核
					csRuleService.checkCSRules(uwPolicyVO.getChangeId());
					//PCR 415727 新增保全規則校驗作業架構 - 新增保全規則校驗結果, by保全受理檢核
					posRuleService.checkPosRules(uwPolicyVO.getChangeId(),null);
				} catch (Exception e) {
					throw ExceptionFactory.parse(e);
				}
				//pcr_457817
				finally {
					try{
						LiarocDownloadLogHelper.flush();
					}catch(Exception e){
						logger.error("LiarocDownloadLogHelper.flush() : " + e.getMessage(), e);
					}
				}

				boolean hasPendingIssue = proposalRuleResultService.hasPendingIssueForCS(uwPolicyVO.getChangeId());
				ApplicationLogger.addLoggerData("checkFirstApprovalForCS.hasPendingIssue=" + hasPendingIssue);
				if (hasPendingIssue) {
					/* MSG_1259179 存在待處理的核保信息，請確認    */
					multiWarning.addWarning("MSG_1259179");
					multiWarning.setContinuable(false);
				}
			}
	//	}
		return multiWarning;
	}

	/**
	 * @param request
	 * @param underwriteId
	 * @param uwPolicyVO
	 * @param uwSourceType
	 * @return @throws GenericException
	 * @throws NamingException
	 * @throws IllegalStateException
	 * @throws SecurityException
	 * @throws SystemException
	 */
	@PAPubAPIUpdate("loadPolicyByPolicyId")
	protected MultiWarning processSubmitWarning(HttpServletRequest request,
					Long underwriteId, UwPolicyVO uwPolicyVO, String uwSourceType)
					throws GenericException, NamingException, IllegalStateException,
					SecurityException, SystemException {

		boolean isNewbiz = CodeCst.UW_SOURCE_TYPE__NB.equals(uwSourceType);
		Collection products = uwPolicyDS.findUwProductEntitis(underwriteId);
		Collection lifeInsureds = new ArrayList();
		// comment by hanzhong.yan for cq:GEL00029240,and replace the method
		// with
		// "helper.drawValueFromPage(request,uwPolicyVO,products);"
		// //get value from page
		// helper.setUwPolicyValueFromPage(request, uwPolicyVO, products);
		// 2015-07-09 hang by sunny 確認跟存檔要用同一個method存畫面資料

		UserTransaction userTransaction = Trans.getUserTransaction();
		try {
			userTransaction.begin();

			ApplicationLogger.addLoggerData("helper.drawValueFromPage_TGL() begin");
			helper.drawValueFromPage_TGL(request, uwPolicyVO, products);
			ApplicationLogger.addLoggerData("helper.drawValueFromPage_TGL() end");

			// 人工陳核
			if (CodeCst.YES_NO__YES.equals(uwPolicyVO.getManuEscIndi())) {
				if (uwPolicyVO.getUwEscaUser() == null) {
					/* 是否有覆核主管 */
					Long currUser = AppContext.getCurrentUser().getUserId();

					ApplicationLogger.addLoggerData("leaveManagerService.findCSOrUNBAuthLeave() begin");
					LeaveAuthEdit leaveAuthVO = leaveManagerService.findCSOrUNBAuthLeave(null, currUser, uwSourceType);
					ApplicationLogger.addLoggerData("leaveManagerService.findCSOrUNBAuthLeave() end");

					/* 判斷覆核主管不可同本人 */
					if (leaveAuthVO != null && leaveAuthVO.getMasterEmpId() != null
									&& leaveAuthVO.getMasterEmpId().equals(currUser) == false) {
						if (leaveAuthVO.getMasterEmpId().longValue() > 0) {
							uwPolicyVO.setUwEscaUser(leaveAuthVO.getMasterEmpId());
						}
					}
				}
			}

			// 需先執行drawValueFromPage_TGL作是否體檢 是否調閱影像 身心障礙 內勤人員生調 更新,再執行查詢更新
			lifeInsureds.addAll(uwPolicyDS.findUwLifeInsuredEntitis(underwriteId));

			ApplicationLogger.addLoggerData("helper.storePolicyInfo() begin");
			helper.storePolicyInfo(request, uwPolicyVO, products, lifeInsureds);
			// add by hanzhong.yan 2007/7/28 for cq:GEL00029240
			ApplicationLogger.addLoggerData("helper.updatePolicyAndProduct() begin");
			helper.updatePolicyAndProduct(uwPolicyVO, products);
			ApplicationLogger.addLoggerData("helper.updatePolicyAndProduct() end");
			/* store comments 2009-12-4
			PolicyVO policy = policyService.loadPolicyByPolicyId(uwPolicyVO
			                .getPolicyId());
			policy.setComments(request
			                .getParameter(ReqParamNameConstants.POLICY_UNDERWRITING_COMMENT));
			policyCI.updatePolicy(policy);
			*/
			userTransaction.commit();
		} catch (Exception e) {
			TransUtils.rollback(userTransaction);
			throw ExceptionFactory.parse(e);
		}
		// to check
		MultiWarning multiWarning = new MultiWarning();
		if (!CodeCst.YES_NO__YES.equals(uwPolicyVO.getManuEscIndi())) { // 不為人工陳核

			// 需依角色設定欄位
			UwPolicyForm uwPolicyForm = new UwPolicyForm();

			ApplicationLogger.addLoggerData("uwTransferHelper.defineUwTransferRole() begin");
			uwTransferHelper.defineUwTransferRole(uwPolicyForm, uwPolicyVO, "false");
			ApplicationLogger.addLoggerData("uwTransferHelper.defineUwTransferRole() end");
			boolean isUnderwriter = uwPolicyForm.getIsUnderwriter(); // 核保員

			if (isUnderwriter) { // 初始核保員執行以下檢核及更新

				UserTransaction userTransaction2 = Trans.getUserTransaction();
				try {
					userTransaction2.begin();
					logger.info("UwPolicySubmitAction.isNewbiz=" + isNewbiz);
					// add by hanzhong.yan for cq:GEL00029240,check some
					// condition on page

					ApplicationLogger.addLoggerData("helper.checkPageInfo() begin");
					helper.checkPageInfo(request, uwPolicyVO, products);
					ApplicationLogger.addLoggerData("helper.updatePolicyAndProduct() begin");
					// store uw policy & product info
					helper.updatePolicyAndProduct(uwPolicyVO, products);
					// 2017-08-16 Kate RTC#173364 保全核保不須異動投保年齡
					// 20150709 add by sunny add 被保人身心障礙欄位
					ApplicationLogger.addLoggerData("helper.updateInsuredListDisability() begin");
					helper.updateInsuredListDisability(underwriteId, isNewbiz);

					if (isNewbiz) {
						// 20150709 change by sunny 保項核保決定要由保單核保結論判斷

						ApplicationLogger.addLoggerData("helper.changeUnderwritingDecision() begin");
						helper.changeUnderwritingDecision(
										uwPolicyVO.getPolicyDecision(), underwriteId);

						// add by simon.huang 同步核保主頁面的保單生效日回policy及coverage
						if (Cst.POLICY_DECISION_ACCEPT.equals(uwPolicyVO.getPolicyDecision())) {

							// PCR-307485 VLO 下核保決定時先取得現金收益分配帳戶(T_INVEST_CASH_ACCOUNT)ACCOUNT_ID，
							// 待保單承保時再將T_INVEST_CASH_ACCOUNT帳戶資料更新至T_CONTRACT_BENE.BENE_TYPE = '8'
							// 2019/07/30 Add by Kathy
							ApplicationLogger.addLoggerData("helper.saveBeneType8byInvCashAccount() begin");
							helper.saveBeneType8byInvCashAccount(uwPolicyVO.getPolicyId(), uwPolicyVO.getApplyDate());
							ApplicationLogger.addLoggerData("helper.saveBeneType8byInvCashAccount() end");

							//BC-373 PCR-332528 RTC-336493 下核保決定時，投資型定期買回暨投資標的運用期屆滿處理方式，新增預設受益人&T_CONTRACT_BENE_FUND資料 2019/08/26 Add by Yvon Sun
							ApplicationLogger.addLoggerData("helper.saveTargetMaturityFundBeneficiaryRelatedData() begin");
							helper.saveTargetMaturityFundBeneficiaryRelatedData(uwPolicyVO.getPolicyId());
							ApplicationLogger.addLoggerData("helper.saveTargetMaturityFundBeneficiaryRelatedData() end");

							// 核保通過，t_bank_account 寫入
							ApplicationLogger.addLoggerData("helper.saveBankAccountOnAcceptUnderwrite() begin");
							helper.saveBankAccountOnAcceptUnderwrite(uwPolicyVO.getPolicyId());

							/* 2015/11/25 alex cheng 增加受益人 帳戶資料回寫  BankAccount */
							ApplicationLogger.addLoggerData("beneficiaryService.findByPolicyId() begin");
							List<BeneficiaryVO> beneficiarys = beneficiaryService.findByPolicyId(uwPolicyVO.getPolicyId());
							for (BeneficiaryVO beneficiaryVO : beneficiarys) {
								ApplicationLogger.addLoggerData("helper.saveOrUpdateAccount() begin");
								helper.saveOrUpdateAccount(beneficiaryVO, uwPolicyVO.getApplyDate());
								ApplicationLogger.addLoggerData("helper.saveOrUpdateAccount() end");
								beneficiaryService.saveByDataEntry(beneficiaryVO);
								ApplicationLogger.addLoggerData("beneficiaryService.saveByDataEntry() end");
							}

							//2017/05/25 simon.huang 第7年度增值回饋分享金帳戶資料回寫BankAccount

							ApplicationLogger.addLoggerData("helper.saveFeedbackBonus7BankAccount() begin");
						  	helper.saveFeedbackBonus7BankAccount(uwPolicyVO.getPolicyId());
							ApplicationLogger.addLoggerData("helper.saveFeedbackBonus7BankAccount() end");

						}

						//PCR384234	
						Long policyId= uwPolicyVO.getPolicyId();

						//投保投資型商品且保單投資標的任一連結含高收益債券, 且 檢附「投資標的連結高收益債券之風險預告書」有附約定書
						if ( detailRegHelper.chkExist_HighYieldBond(policyId) ) {
							boolean haveInvestRiskForm= detailRegHelper.haveInvestRiskForm(policyId);
							PolicyVO policyVO = policyVoService.load(policyId);
							// PCR487290
							if (nbSpecialRuleService.isApplyDateMatchRuleTypeStartDate(policyVO.getPolicyId(), NBPolicySpecialRuleType.NB_ADJUST_FUND_RISK_VERSION_START_DATE)) {
								policyVO.setIlpRiskDisclosureStatement((haveInvestRiskForm
										? com.ebao.ls.pub.CodeCst.YES_NO__YES: com.ebao.ls.pub.CodeCst.YES_NO__NO));
							} else {
								policyVO.setIlpRiskDisclosureStatement(com.ebao.ls.pub.CodeCst.YES_NO__NO);// 保單申請日在5/4 (含)前，不論有沒有檢附新版的風險預告書，值都是N
							}//===========END of PCR487290
							policyVoService.save(policyVO);
						}
						//===========END of PCR384234	

						ApplicationLogger.addLoggerData("helper.getInsuredList() begin");
						List<UwLifeInsuredVO> insuredList = helper.getInsuredList(request, uwSourceType); //PCR-524169,POS need sourceType
						ApplicationLogger.addLoggerData("processCheck() begin");
						processCheck(multiWarning, underwriteId, uwPolicyVO, products,
										lifeInsureds, insuredList, isNewbiz);
						ApplicationLogger.addLoggerData("processCheck() end");
					}

					// add end
					// modified end
					/* if no warning,transaction commit. */
					if (!multiWarning.isEmpty()) {
						userTransaction2.rollback();
						// add by hanzhong.yan 2007/7/28 for cq:GEL00029240
						refreshSession(request, uwPolicyVO);
						return multiWarning;
					}
					userTransaction2.commit();
					//***************************for test************************************
					HibernateSession3.currentSession().flush();
					HibernateSession3.currentSession().clear();

					ApplicationLogger.addLoggerData("coverageService.findByPolicyId() begin");
					List<CoverageVO> coverages = coverageService.findByPolicyId(uwPolicyVO.getPolicyId());
					ApplicationLogger.addLoggerData("coverageService.findByPolicyId() end");
					logger.info("UwPolicySubmitAction.2 commit.....................");
					for (CoverageVO coverage : coverages) {
						for (CoverageInsuredVO coverageInsured : coverage.getInsureds()) {
							Long itemId = coverageInsured.getItemId();
							Integer entryAge = coverageInsured.getEntryAge();
							logger.info("UwPolicySubmitAction.itemId=" + itemId
											+ ",entryAge=" + entryAge);
						}
					}
					//***************************for test end************************************
				} catch (Exception e) {
					TransUtils.rollback(userTransaction2);
					throw ExceptionFactory.parse(e);
				}
			}

		}

		return multiWarning;
	}

    private PolicyVO loadBfInforcePolicy(Long policyId) {
        Map parmMap = new HashMap();
        parmMap.put("policyId", policyId);
        parmMap.put("serviceId", new Integer(CodeCst.SERVICE__IN_FORCE_POLICY));
        Map orderMap = new HashMap();
        orderMap.put("policyChgId", "desc");
        List<AlterationItemVO> alterationItemVOs = alterationItemService.find(parmMap, orderMap);
        PolicyVO policy = null;

        if (alterationItemVOs != null) {
            Long policyChgId = alterationItemVOs.get(0).getPolicyChgId();
            policy = policyDS.findBfByPolicyIdAndPolicyChgId(policyId, policyChgId);
        }
        return policy;
    }

    /**
     * add by hanzhong.yan 2007/7/28 for cq:GEL00029240 when checked failed , then
     * update the for
     * 
     * @param request
     * @param uwPolicyVO
     * @throws GenericException
     */
    protected void refreshSession(HttpServletRequest request, UwPolicyVO uwPolicyVO) throws GenericException {
        UwPolicyForm uwPolicyForm = new UwPolicyForm();
        helper.constructUwPolicyForm("false", uwPolicyForm, uwPolicyVO);
        // set uwpolicy information
        request.setAttribute(UwPolicyMainAction.MAIN_FORM, uwPolicyForm);

        Long currentUserId = AppContext.getCurrentUser().getUserId();
        List<Long> transferUserList = uwTransferHelper.getManualTransferUser(currentUserId, uwPolicyVO.getUwSourceType());
        request.setAttribute("transferUserList", transferUserList);
    }

	/**
	 * process claim submit
	 * 
	 * @param request
	 * @param underwriteId
	 * @param uwPolicyVO
	 * @return @throws Exception
	 */
	protected MultiWarning processClaimSubmit(HttpServletRequest request,
					Long underwriteId, UwPolicyVO uwPolicyVO) throws GenericException {
		Collection products = uwPolicyDS.findUwProductEntitis(underwriteId);
		Collection lifeInsureds = uwPolicyDS.findUwLifeInsuredEntitis(underwriteId);
		UserTransaction userTransaction = null;
		MultiWarning multiWarning = new MultiWarning();
		try {
			userTransaction = Trans.getUserTransaction();
			userTransaction.begin();
			// get value from page
			helper.setUwPolicyValueFromPage(request, uwPolicyVO, products);
			helper.storeUwPolicyValue(request, uwPolicyVO, products, lifeInsureds);
			/* if no warning,transaction commit. */
			if (!multiWarning.isEmpty()) {
				userTransaction.rollback();
				return multiWarning;
			} else {
				uwPolicyDS.updateUwPolicy(uwPolicyVO, true);
			}
			userTransaction.commit();
		} catch (Exception e) {
			TransUtils.rollback(userTransaction);
			throw ExceptionFactory.parse(e);
		}
		return multiWarning;
	}

	/**
	 * @param request
	 * @param uwPolicyVO
	 * @param uwSourceType
	 * @throws GenericException
	 * @throws RTException
	 */
	@DataModeChgModifyPoint("change code from uwPolicyVO.getProposalStatus() to getProposalDecision(),refer to UwProcessDSImpl.setPolicyValue")
	protected void compleUnderwriting(HttpServletRequest request,
					UwPolicyVO uwPolicyVO, String uwSourceType, MultiWarning multiWarning,
					boolean isContinueTriger) throws GenericException {

		Long userId = AppContext.getCurrentUser().getUserId();

		// 依狀態 決定核保流程
		int completeStatus = uwProcessDS.completeUnderwriting(uwPolicyVO);

		boolean isNewbiz = UwSourceTypeConstants.NEW_BIZ.equals(uwSourceType);
		// implement rule enginee
		if (isNewbiz && !isContinueTriger) {
			/*
			 * ProposalRule rule = new ProposalRule();
			 * rule.checkProposalRule("DetailReg", uwPolicyVO.getPolicyId());
			 */
			RuleResultVO ruleResult = proposalRule.checkRulesDetailReg(uwPolicyVO
							.getPolicyId());
			if (ruleResult.getStatus() < 0) {
				List list = ruleResult.getErrorList();
				for (int i = 0; i < list.size(); i++) {
					String[] results = (String[]) list.get(i);
					if (results[1] == null || "".equals(results[1])) {
						multiWarning.addWarning("ERR_" + results[0]);
					} else {
						multiWarning.addWarning("ERR_" + results[0], results[1]);
					}
				}
				multiWarning.setContinuable(false);
			}

			this.noticePolicyDecision(uwPolicyVO, multiWarning);//499927 BC456_保障型平台微型保險，「EC線下核保新契約通知保單核保結論」

			if (!multiWarning.isContinuable()) {
				return;
			}
		}
		/* complete underwriting */
		Integer status = Integer.valueOf(999);
		Integer proposalStatus = uwPolicyVO.getProposalDecision();
		if (proposalStatus != null) {
			status = Integer.valueOf(proposalStatus.intValue());
		}
		boolean isAccept = ProposalStatusConstants.ACCEPTED.equals(status);
		boolean isConditionalAccept = ProposalStatusConstants.CONDITIONAL_ACCEPTED
						.equals(status);
		if (completeStatus != CompleteUwProcessStatusConstants.PROPOSAL_NEED_MANUAL_ESCALATING
						&& completeStatus != CompleteUwProcessStatusConstants.PROPOSAL_BEEN_ESCALATED
						&& completeStatus != CompleteUwProcessStatusConstants.PROPOSAL_NEED_TRANSFER
						&& isNewbiz && (isAccept || isConditionalAccept)) {
			// add to control NBU process with workflow engine,by robert.xu on
			// 2008.4.17
			
			//因completeTask會執行承保作業處理,需先處理設定法定代理人作業,才不會承保作業依關係人展開了法定代理人,又被saveUNBUnderwriting清除成一筆
			Long policyId = uwPolicyVO.getPolicyId();
			checkPosQEPPaymentFreq(policyId);
					
			//生成法定代理人partyId且維護法定代理人為一筆
			NBUtils.logger(this.getClass(),"生成法定代理人partyId");
			legalRepresentativeService.saveUNBUnderwriting(policyId);
			
			//重要:此處會執行承保作業，有核保通過預設處理作業需在此作業(completeTask)前處理
			proposalProcessService.completeTask(uwPolicyVO.getPolicyId(),
							uwPolicyVO.getUwStatus(), CodeCst.PROPOSAL_STATUS__UW);

			// since UW doesn't re-calc premium, when job class and other info
			// changed, premium may changed too
			// added to synchronize the premium info between uw table and main
			// table.
			// ning.qi GEL00041383
			// comment out to retain original premium info
			// CompleteUWProcessSp.synPremInfoToUW(uwPolicyVO.getPolicyId(),
			// uwPolicyVO.getUnderwriteId());
		}
		// add to control NBU process with workflow engine,by robert.xu on
		// 2008.4.17
		boolean isDeclined = ProposalStatusConstants.DECLINED.equals(status); //82
		boolean isPostponed = ProposalStatusConstants.POSTPONED.equals(status); //84
		boolean isWithdraw = ProposalStatusConstants.WITHDRAWN.equals(status);//86
		ApplicationEventVO eventVO = new ApplicationEventVO();
		eventVO.setTransactionId(uwPolicyVO.getUnderwriteId());
		eventVO.setUserId(userId);
		eventVO.setPolicyId(uwPolicyVO.getPolicyId());
		if (isNewbiz
						&& completeStatus == CompleteUwProcessStatusConstants.PROPOSAL_NEED_TRANSFER) {
			// 轉呈 覆核/主管或額度授權主管
			// doUwTransferManager(request, uwPolicyVO, lastest);

		} // is declined or postponed
		else if (isNewbiz && (isDeclined || isPostponed)) {
			proposalProcessService.completeTask(uwPolicyVO.getPolicyId(),
							uwPolicyVO.getUwStatus(), CodeCst.PROPOSAL_STATUS__UW);
			if (isDeclined) {
				NbUwCompletedWithDeclined uwDecline = new NbUwCompletedWithDeclined(eventVO);
				eventService.publishEvent(uwDecline);
			}
			if (isPostponed) {
				NbUwCompletedWithPostponed uwPostponed = new NbUwCompletedWithPostponed(eventVO);
				eventService.publishEvent(uwPostponed);
			}

			// PCR 472104 外來人口更新式證號進度追蹤-UNB串接接口
			csCi.doUnbCancel(uwPolicyVO.getPolicyId());

			Date lastBatchDate = liaRocUploadCommonService.getLastNBBatchDate(uwPolicyVO.getPolicyId(), 
							LiaRocCst.LIAROC_NB_BATCH_CHANGE_TYPE_12);
			if(lastBatchDate != null 
							&& DateUtils.date2String(lastBatchDate).equals(DateUtils.date2String(new Date()))){
				//撤件UNDO後通報收件01，當天又撤件記錄一筆異動,由SMART QUERY產生報表後人工通報
				liaRocUploadCommonService.recordNBBatchData(uwPolicyVO.getPolicyId(), LiaRocCst.LIAROC_NB_BATCH_CHANGE_TYPE_14);
			} else {
				/**公會通報**/
				liaRocUploadCI.sendNBRecCancel(uwPolicyVO.getPolicyId());
			}
		}
		// for conditional accept
		else if (isNewbiz && isConditionalAccept) {
			if (isConditionalAccept) {
				NbUwCompletedWithConditionallyAccepted uwCondAccepte = new NbUwCompletedWithConditionallyAccepted(
								eventVO);
				eventService.publishEvent(uwCondAccepte);
			}
		}
		// add by poppy for accept
		else if (isNewbiz
						&& isAccept
						&& completeStatus == CompleteUwProcessStatusConstants.COMPLETE_UW_PROCESS_SUCCESS) {

			NbUwCompletedWithAccepted uwCondAccepte = new NbUwCompletedWithAccepted(
							eventVO);
			eventService.publishEvent(uwCondAccepte);

		} else if (isNewbiz && isWithdraw) {

			Long policyId = uwPolicyVO.getPolicyId();
			/* 統一由下方  uwPolicyDS.refundPolicyWithdraw()執行核保撤退費作業,批次撤件才使用withdrawProposalService.submitWithdrawProposal()
			WithdrawProposalVO withdrawProposalVo = new WithdrawProposalVO();
			withdrawProposalVo.setPolicyId(policyId);
			PolicyCancelVO policyCancelVo = new PolicyCancelVO();
			policyCancelVo.setPolicyId(policyId);
			policyCancelVo.setCancelType("1");// TODO cancel type should be
			policyCancelVo.setCauseId("3");// Cancelled by company before
			                               // acceptance
			policyCancelVo.setReason(uwPolicyVO.getCancelDesc());
			withdrawProposalVo.setPolicyCancel(policyCancelVo);
			withdrawProposalService.submitWithdrawProposal(withdrawProposalVo, Long
			                .valueOf(AppContext.getCurrentUser().getUserId()));
			                */
			// changed by matt.zhou 2010-02-27
			// extract method for other project customization
			// add to control NBU process with workflow engine,by robert.xu on
			// 2008.4.17
			// WfHelper.processManagerForWithdraw(policyId);
			proposalProcessService.completeTask(policyId, null, CodeCst.PROPOSAL_STATUS__UW,
							ProposalProcessService.DIRECTTONOTACCEPT);

			// PCR 472104 外來人口更新式證號進度追蹤-UNB串接接口
			csCi.doUnbCancel(uwPolicyVO.getPolicyId());

			/**公會通報**/
			liaRocUploadCI.sendNBRecCancel(uwPolicyVO.getPolicyId());
			// end by poppy
			// add end
		}
		/** 拒保若有懸帳金額，作懸帳退費 ( 如高保額退費流程) */
		//#156436
		if (isNewbiz) {
			String policyDecision = uwPolicyVO.getPolicyDecision();
			if (Cst.POLICY_DECISION_ACCEPT.equals(policyDecision) == false) {
				//RTC188158-不為核保通過一律執行退費
				String uwStatus = uwPolicyVO.getUwStatus();
				//if(org.apache.commons.lang3.StringUtils.isNotEmpty(uwStatus) && Cst.POLICY_DECISION_WITHDRAW.equals(uwStatus)){
				uwPolicyDS.refundPolicyWithdraw(uwPolicyVO.getPolicyId(), CodeCst.WITHDRAW_TYPE__CANCEL_REFUND);
				//        	    }else{
				//        	        uwPolicyDS.refundPolicySuspenses(uwPolicyVO.getPolicyId(), CodeCst.WITHDRAW_TYPE__CANCEL_REFUND);
				//        	    }
				
				//更新CRS主檔為取消
				NBcrsVO nbCRSVO = this.nbCrsService.findFirstByPolicyId(uwPolicyVO.getPolicyId());
				if (nbCRSVO != null) {
				
					CRSPartyLogVO backLogVO = this.crsPartyLogServ.findByChangeIdAndCertiCode(nbCRSVO.getChangeId(), null);
					
					if (backLogVO != null && NBUtils.in(backLogVO.getBuildType(), 
									CRSCodeCst.BUILD_TYPE__COMPLETE,
									CRSCodeCst.BUILD_TYPE__UNB_LOG) == false) {
						NBUtils.logger(this.getClass(),"更新CRS主檔狀態04");
					
						backLogVO.setBuildType(CRSCodeCst.BUILD_TYPE__REMOVE);
						//更新維護人員/維護部門/維護日
						this.crsPartyLogServ.saveOrUpdate(userId, 
										AppContext.getCurrentUserLocalTime(), 
										backLogVO, backLogVO.getChangeId(), 
										CRSCodeCst.INPUT_SOURCE__LINK);
					}
				}
				/* #325504 核保未通過 填寫身心障礙 */
				List<InsuredVO> insuredVOList = this.insuredService.findByPolicyId(uwPolicyVO.getPolicyId());
				if(CollectionUtils.isNotEmpty(insuredVOList)) {
					for(InsuredVO insuredVO : insuredVOList) {
						if(!"000".equals(insuredVO.getDisabilityType())) {
							this.uwPolicyDS.writeInsuredCancelDetail(insuredVO.getListId());
						}
					}
				}

				this.noticeCancelElecPolicy(uwPolicyVO);//457819 電子保單_個險，「保單生效UNDO後撤銷」

			}else {
				/* #325504 核保通過 */
				
			}
			
			//新契約下核保決定，更新核保時間及核保決定回身心障礙未承保原因檔
			List<DisabilityCancelDetailVO> cancelList = disabilityCancelDetailService.findByPolicyId(uwPolicyVO.getPolicyId(), "UNB");
			for(DisabilityCancelDetailVO cancelDetail : cancelList) {
				cancelDetail.setPolicyDecision(uwPolicyVO.getPolicyDecision());
				cancelDetail.setUnderwriteTime(uwPolicyVO.getUnderwriteTime());
				if (Cst.POLICY_DECISION_ACCEPT.equals(policyDecision) == false) {
					//不為核保決定寫入狀態確認,核保決定A需在承保寫入status,confirm_date
					cancelDetail.setStatus("2");//確認
					cancelDetail.setConfirmDate(AppContext.getCurrentUserLocalTime());
					cancelDetail.setProposalStatus(proposalStatus);
				}
				disabilityCancelDetailService.saveOrUpdate(cancelDetail);
			}
		}

		request.setAttribute(UW_COMPLETE_STATUS, Integer.valueOf(completeStatus));
	}

	/**
	 * 457819 電子保單_個險
	 * 「保單生效UNDO後撤銷」：
	 * 1.	保單核保決定≠A-核保通過 且
	 * 2.	保單為生效後修改(生效後UNDO) 
	 * 註：保單若為生效後UNDO且核保決定=A-核保通過，保單是否須重製，同現行由核保人員自行判斷，若須重製由核保員通知行政科人工重製。
	 */
	private void noticeCancelElecPolicy(UwPolicyVO uwPolicyVO) {
		if (paValidatorService.isReversalAfterInForce(uwPolicyVO.getPolicyId())) {
			ApplicationLogger.addLoggerData(NBUtils.getTWMsg("ERR_20410020164"));//本件為UNDO件
			PolicyRemakeDetailVO policyRemakeDetailVO = policyRemakeDetailService.findLastestRemakeDetail(uwPolicyVO.getPolicyId());

			if (policyRemakeDetailVO!=null && policyRemakeDetailVO.getElecType()==CodeCst.ELEC_TYPE__ELECTRONIC) {//最新一份為電子保單
				PolicyPrintJobVO policyPrintJobVO = policyPrintJobCI.getLastPolicyPrintJob(uwPolicyVO.getPolicyId(), CodeCst.PRINT_JOB_TYPE_NB);
				FmtPolicyVO fmtPolicyVO = fmtPolicyService.findLastByJobId(policyPrintJobVO.getJobId());
				String documentNo = CtrsUtils.getCtrs533DocumentNo4Unb(fmtPolicyVO.getRptName(), fmtPolicyVO.getJobId());//CF歸檔鍵值
				ApplicationLogger.addLoggerData(NBUtils.getTWMsg("COD_T_ELEC_TYPE_2") + ", documentNo: " + documentNo);//電子保單

				if (!StringUtils.isNullOrEmpty(documentNo)) {
					CtrsReturnOutVO out = ctrsCI.noticeCancelElecPolicy(documentNo, CtrsCst.CTRS_533_CANCEL_CODE__REVERSAL_AFTER_IN_FORCE);
					ApplicationLogger.addLoggerData("CTRS533發送結果 code: " + out.getCode() + ", msg: " + out.getMsg());
				}
			}
		}
	}

	/**
	 * 499927 BC456_保障型平台微型保險
	 * 「EC線下核保新契約通知保單核保結論」
	 */
	private void noticePolicyDecision(UwPolicyVO uwPolicyVO, MultiWarning multiWarning) {
		boolean isEcOffline = policyDS.isEcOffline(uwPolicyVO.getPolicyId());
		if (isEcOffline) {
			ApplicationLogger.addLoggerData(NBUtils.getTWMsg("EC400發送"));
			EcReturnOutVO out = ecProposalCI.noticePolicyDecision(uwPolicyVO.getPolicyCode(), uwPolicyVO.getPolicyDecision());
			ApplicationLogger.addLoggerData("EC400發送結果 code: " + out.getExeResult() + ", msg: " + out.getErrMsg());
			if (!CodeCst.YES_NO__YES.equals(out.getExeResult())) {
				multiWarning.addWarning(NBUtils.getTWMsg("MSG_1267274"));//與EC連線失敗無法下核決，請通知規劃科同仁，謝謝
				multiWarning.setContinuable(false);
			}
		}
	}

	private void checkPosQEPPaymentFreq(Long policyId) {

		NBUtils.logger(this.getClass(),"保全接口更新生存保險金給付周期");
		try{
			CoverageVO masterCoverage = coverageService.findMasterCoverageByPolicyId(policyId);
			
			if (masterCoverage != null) {
				
				Long productId = masterCoverage.getProductId().longValue();
				ProductVersionVO productVersionVO = productVersionService.getProductVersion(
								masterCoverage.getProductId().longValue(),
								masterCoverage.getProductVersionId());
				
				String versionCode = null;
				if (productVersionVO != null) {
					versionCode = productVersionVO.getProductVersion();
				}
				List<Object> sbMfList = lifeProductRTService.getSbMfByProductVersion(productId, versionCode);
				
				if(sbMfList != null && sbMfList.size() > 0 
								&& sbMfList.contains(CodeCst.PAY_TYPE__YEARLY)) {
					
	        		boolean isUpdate = false;
	        		
	        		NBUtils.logger(this.getClass(),"masterCoverage PaymentFreq=" + masterCoverage.getPaymentFreq());
	        		
	        		if(!CodeCst.PAY_TYPE__YEARLY.equals(masterCoverage.getPaymentFreq())) {
	        			BigDecimal minSA = lifeProductRTService.getPosCheckAmountByProductVersion(productId, versionCode);
	        			BigDecimal totalAmount = masterCoverage.getCurrentPremium().getSumAssured();
	        			NBUtils.logger(this.getClass(),"PosCheckAmount ,"
	        							+ ",minSA=" 
	        							+ (minSA == null ? "NULL" :minSA)  
	        							+ ",totalAmount=" 
	        							+ (totalAmount == null ? "NULL" : totalAmount));
	        			//保額已變更為{amount}元，依條款約定將同步調整生存金給付週期由月給付改為年給付。
	        			if(minSA != null && totalAmount != null) {
	        				if(totalAmount.compareTo(minSA) < 0){
	        					NBUtils.logger(this.getClass(),"totalAmount.compareTo(minSA) < 0");
	        					masterCoverage.setPaymentFreq(CodeCst.PAY_TYPE__YEARLY);
	        					isUpdate = true;
	        				}
	        			}
	        		}
	        		
	        		if(!CodeCst.PAY_TYPE__YEARLY.equals(masterCoverage.getPaymentFreq())) {
	        			Long insuredId = masterCoverage.getLifeInsured1().getInsuredId();
	        			List<BeneficiaryVO> beneList = beneficiaryService.findByPolicyId(policyId);
	        			int lifeBeneSize  = 0 ;
	        			for (BeneficiaryVO beneficiaryVO : beneList) {
							if(beneficiaryVO.getInsuredId() != null) {
								if (beneficiaryVO.getInsuredId().equals(insuredId)) {//主被保險人
									String beneType = beneficiaryVO.getBeneType();
									if (CodeCst.BENEFICIARY_TYPE_01.equals(beneType)) {
										lifeBeneSize++;
										// 針對可選生存金給付週期商品，如保單設定非年給付，且變更後該保項的生存金受益人超過一人
										if (lifeBeneSize > 1) {
											masterCoverage.setPaymentFreq(CodeCst.PAY_TYPE__YEARLY);
					        				isUpdate = true;
										}
									}
								}
							}
						}
	        		}
	        		
	        		NBUtils.logger(this.getClass(),"isUpdate PaymentFreq=" + isUpdate);
	        		if(isUpdate) {
	        			coverageService.saveOrUpdate(masterCoverage);
	        		}
				} else {
					NBUtils.logger(this.getClass(),"no sbMfList productId=" + productId + ",versionCode=" + versionCode);
				}
			}
			
		}catch(Exception e){
			String message = ExceptionInfoUtils.getExceptionMsg(e);
			int len = message.length();
			if (len > 0) {
				NBUtils.logger(this.getClass(),"[ERROR]Exception => " + message.substring(0, (len > 2000 ? 2000 : len)));
			} else {
				NBUtils.logger(this.getClass(),"[ERROR]Exception => " + e);
			}
		}
	}
	
	/**
	 * @param multiWarning
	 * @param underwriteId
	 * @param uwPolicyVO
	 * @param products
	 * @param lifeInsureds
	 * @throws GenericException
	 */
	protected void processCheck(MultiWarning multiWarning, Long underwriteId,
					UwPolicyVO uwPolicyVO, Collection products, Collection lifeInsureds,
					List<UwLifeInsuredVO> insuredList, boolean isNewbiz) throws GenericException {
		// check product exclusion
		// checkProductExclusion(multiWarning, underwriteId, products);
		// check medical billing
		// checkMedicalIndi(multiWarning, underwriteId, lifeInsureds, products);
		// check standard life indicator
		// checkStandLifeIndi(multiWarning, underwriteId, products,
		// lifeInsureds,uwPolicyVO);
		// check perfer life indicator if necessary
		// checkPreferLifeInd(lifeInsureds, products, multiWarning);
		// check commence date
		checkCommenceAndRiskCommenceDate(multiWarning, uwPolicyVO);
		// check back dating
		// mark by amy 2016/03/09 in TGL rule, 目前生效日是以收費日判斷，註解此檢查
		// checkBackDatingIndi(multiWarning, uwPolicyVO, products, isNewbiz);
		// end mark by amy 2016/03/09.
		// check generate LCA Ind and update LCA Ind
		// checkLcaIndi(multiWarning, uwPolicyVO);
		// check product decision
		// MARK by SUNNY 2015-07-09 : 保項核保決定要由保單核保結論決定
		// checkProductDecision(multiWarning, products);
		// rule 2:all generated lca letters must be returned or settled.
		pendingLetterCheck(multiWarning, uwPolicyVO, isNewbiz);
		// check fac request
		// checkFacRequest(multiWarning, products);
		// check whether the ma/ms factor is within the range
		/* 2015/07/21 alex cheng 目前投資型商品不算MA */
		// checkMAMSFactor(multiWarning, products, lifeInsureds);
		// 2016-02-24 檢核被保險人身心障礙選項為「是」者，是否存在身心障礙資料
		checkInsuredDisability(multiWarning, insuredList);
		// 2016-03-23 檢核保單核保決定
		checkPolicyDecision(multiWarning, uwPolicyVO);
		// 2016-03-24 檢核暫不列印保單決定
		checkPendingIssueStatus(multiWarning, uwPolicyVO);
		// 2016-10-19 檢核保單意見是否填寫
		checkUWComments(multiWarning, uwPolicyVO);
		// 2019-10-30 洗錢資恐高風險客戶 PCR337216
		checkRiskReprotNotes(multiWarning, uwPolicyVO);
            checkLegalBeneEvalNotes(multiWarning, uwPolicyVO);
            checkNbRiResults(multiWarning, uwPolicyVO);
            checkUwElderCare(multiWarning, uwPolicyVO);

        //IR_467518核保決定部份資料空白不可生效( 監護宣告、是否體檢、 是否調閱影像 、身心障礙 、內勤人員生調) 20220525 add by Sun
        checkBlankField(multiWarning, insuredList);
        
        //微型保單需檢核
    	CoverageVO masterCoverage = coverageService.findMasterCoverageByPolicyId(uwPolicyVO.getPolicyId());
        String paraDateStr = Para.getParaValue( CodeCstTgl.UNB_MICRO_ROLE_DATE ); //暫定為2022/10/01(111/01/31)
		Date paraDate = com.ebao.pub.util.json.DateUtils.parse(paraDateStr, com.ebao.pub.util.json.DateUtils.datePatternYYMMDD);
		//檢核起日=要保書填寫日2022/10/01
		if( uwPolicyVO.getApplyDate() != null && uwPolicyVO.getApplyDate().compareTo(paraDate) >= 0 ) {
			boolean isMicro = lifeProductCategoryService.isMicro(masterCoverage.getProductId().longValue());
	        if(isMicro && Cst.POLICY_DECISION_ACCEPT.equals(uwPolicyVO.getPolicyDecision()) ) {
	        	//PCR_497838微型保險填報檢校  20220910 add by Sun
	            checkInsuredMicroRole(multiWarning, uwPolicyVO.getPolicyId(), insuredList);
	        }
		}
	}

	/**
	 * @param multiWarning
	 * @param underwriteId
	 * @param uwPolicyVO
	 * @param products
	 * @param lifeInsureds
	 * @throws GenericException
	 */
	protected MultiWarning processNBWarningCheck(Long underwriteId, UwPolicyVO uwPolicyVO)
			throws GenericException{
		MultiWarning multiWarning = new MultiWarning();
		// 2018-11-26 檢核一般懸帳大於等於50萬，出警告訊息。
		checkSuspense(multiWarning, uwPolicyVO);
		
		multiWarning.setContinuable(true);
		return multiWarning;
	}
	
	/**核保通過前先檢核1.AGENT_ID不為空, 2.保費不為0
	 * <p>Description : </p>
	 * <p>Created By : Sunny Wu</p>
	 * <p>Create Time : Feb 29, 2016</p>
	 * @param multiWarning
	 * @param underwriteId
	 * @param uwPolicyVO
	 */
	private void checkInforce(MultiWarning multiWarning, Long underwriteId, UwPolicyVO uwPolicyVO) {
		Long policyId = uwPolicyVO.getPolicyId();
		PolicyInfo policy = policyService.load(policyId);
		String langId = AppContext.getCurrentUser().getLangId();

		String uwSourceType = uwPolicyVO.getUwSourceType();
		boolean isNewbiz = CodeCst.UW_SOURCE_TYPE__NB.equals(uwSourceType);
		// PCR345349 增加POS核保訊息檢核
		boolean isCs = Utils.isCsUw(uwSourceType);
		if (isNewbiz) {
			/* 保單狀態不為待生效，需出錯誤訊息  */
			if (policy.getRiskStatus() != com.ebao.ls.pub.CodeCst.LIABILITY_STATUS__NON_VALIDATE) {
				multiWarning.setContinuable(false);
				/* MSG_910001497 當前保單狀態*/
				multiWarning.addWarning(StringResource.getStringData("MSG_910001497", langId)
								+ ":" + CodeTable.getCodeDesc(TableCst.T_LIABILITY_STATUS, policy.getRiskStatus(), langId));

				return;
			}
			
			String message = getCheckProductVersionStr(policyId);
			if (StringUtils.isNullOrEmpty(message) == false) {
				multiWarning.setContinuable(false);
				multiWarning.addWarning(message);
				return;
			}
		}

		// check agentId需存在系統中
		List<CoverageAgentInfo> agents = policy.getMasterCoverage().getCoverageAgents();
		for (CoverageAgentInfo agent : agents) {
			if (agent.getAgentId() == null) {
				multiWarning.setContinuable(false);
				multiWarning.addWarning(StringResource.getStringData("MSG_1254932", langId)
								+ " " + agent.getRegisterCode());
			}
		}

		if (isNewbiz) {
			/*
			 * PCR_422148 BC410
			 * 新增下列檢核，若符合顯示「保單有加費資料，但不存在新契約內容變更同意書之影像，請確認。」視窗錯誤訊息，不可執行：
			 * 1.	保單核保決定=A-核保通過 且
			 * 2.	保單存在加費資料(加費保險費及加收保險成本>0) 且
			 * 3.	影像不存在「UNBB021-新契約內容變更同意書」文件類別
			 * */
			boolean hasExtraPrem = false;
						
			// 2017-08-01 Kate RTC#170986 當此保項是waiver的商品，在保全核保時Std_Prem_Af有可能為零
			// check 保費需大於0
			List<CoverageInfo> coverages = policy.getCoverages();
			for (CoverageInfo coverage : coverages) {

				//RTC 187356-不檢核豁免險保費為0
				boolean isWaiver = false;
				if (coverage.getWaiverExt() != null) {
					if (com.ebao.ls.pub.CodeCst.YES_NO__YES.equals(coverage.getWaiverExt().getWaiver())) {
						isWaiver = true;
					}
				}

				if (isWaiver == false) {
					if (coverage.getCurrentPremium().getStdPremBf().compareTo(BigDecimal.ZERO) <= 0
									&& coverage.getCurrentPremium().getStdPremAf().compareTo(BigDecimal.ZERO) <= 0) {
						multiWarning.setContinuable(false);
						multiWarning.addWarning(CodeTable.getCodeById("V_PRODUCT_LIFE_1", coverage.getProductId().toString())
										+ StringResource.getStringData("MSG_10751", langId)
										+ "=0 !!!");
						logger.error("t_contract_product.item_id=" + coverage.getCoverageId() + ",Std_Prem_Af=0 !!!");
					}
				}
				
				if (!hasExtraPrem) {
					hasExtraPrem = this.checkExtraPrem(coverage);
				}
			}
			
			if (hasExtraPrem) {
				if (!imageCI.hasImagebyImageTypeId(policyId, Long.parseLong(Billcard.CARD_ID_UNBB021))) {//影像不存在「UNBB021-新契約內容變更同意書」文件類別
					if (!isSkipImageCheck()) {
						multiWarning.setContinuable(false);
						multiWarning.addWarning("MSG_1266000");
					}
				}
			}

			// 2016/05/19 by simon.huang 是否有未處理核保信息
			boolean hasPendingIssue = proposalRuleResultService.hasPendingIssue(policyId);
			if (hasPendingIssue) {
				// MSG_1259179 存在待處理的核保信息，請確認。
				multiWarning.addWarning("MSG_1259179");
				multiWarning.setContinuable(false);
			}

			boolean hasPendingChangeMsg = uwChangeMsgService.hasPendingChangeMsg(underwriteId);
			if (hasPendingChangeMsg) {
				// MSG_1261656 存在未確認的核保中異動信息，請確認。
				multiWarning.addWarning("MSG_1261656");
				multiWarning.setContinuable(false);
			}

			//2017/05/09 by simon.haung,RTC 155028-執行核保決定需檢核體檢照會狀態
			Predicate predicate = new BeanPredicate("status",
							PredicateUtils.equalPredicate(Integer
											.valueOf(CodeCst.PROPOSAL_RULE_STATUS__PENDING)));
			List<UwIssueMedicalItemVO> retList = uwMedicalLetterService.getMedicalIssueListByUnderwriteId(policyId,
							underwriteId);
			Collection<UwIssueMedicalItemVO> pendingList = CollectionUtils.select(retList, predicate);
			if (pendingList.size() > 0) {
				//MSG_1262348 存在待處理的體檢照會，請確認。
				multiWarning.addWarning("MSG_1262348");
				multiWarning.setContinuable(false);
			}

			//2017/09/22 by simon.huang,RTC 190353-需檢核是否存在待確認會辦記錄
			List<UwCheckListStatusVO> checkListStatusList = uwCheckListStatusService.getCheckListStatusByUnderwriteId(
							underwriteId);
			for (UwCheckListStatusVO uwCheckListStatusVO : checkListStatusList) {
				String indi = uwCheckListStatusVO.getConfirmComplete();
				if (indi != null) {
					//DB 為char(2)需作trim()再比對
					indi = indi.trim();
				}
				if (!CodeCst.YES_NO__YES.equals(indi)) {
					// MSG_1262696 存在待確認的會辦事項，請確認。
					multiWarning.addWarning("MSG_1262696");
					multiWarning.setContinuable(false);
					break; //for each;
				}
			}

			List<UwPatchImageVO> patchImageList = uwPatchImageService.getPatchImageByUnderwriteId(underwriteId);
			for (UwPatchImageVO patchImageVO : patchImageList) {
				if (!CodeCst.YES_NO__YES.equals(patchImageVO.getConfirmComplete())) {
					// MSG_1262697 存在待確認補全影像訊息，請確認。
					multiWarning.addWarning("MSG_1262697");
					multiWarning.setContinuable(false);
					break; //for each;
				}

			}
			//END-2017/09/22

			//2017/10/30 by ycsu,RTC 190353-需檢核核保照會通知單信函狀態應為已回覆
			/* issuesLtrList 取得 信函列表 */
			/* use : uw's method*/
			List<IssuesDocVO> uWLetterList = this.getUWLetterList(policyId);

			for (IssuesDocVO issuesDocVO : uWLetterList) {
				//新契約承保前照會單，信函狀態, 有1:待發放,2:己發放, 3:發送完成, 8(不用) 則提示「檢核信函狀態」
				if (CodeCst.LETTER_STATUS_TO_BE_PUBLISHED.equals(issuesDocVO.getStatus())
								|| CodeCst.LETTER_STATUS_PUBLISHED.equals(issuesDocVO.getStatus())
								|| CodeCst.LETTER_STATUS_ISSUED.equals(issuesDocVO.getStatus())) {
					// MSG_1262760 檢核信函狀態。
					multiWarning.addWarning("MSG_1262760");
					multiWarning.setContinuable(false);
					break; //for each;
				}
			}
		}
		// PCR345349 POS增加「未確認的核保中異動信息」檢核
		if (isCs) {
			boolean hasPendingChangeMsg = uwChangeMsgService.hasPendingChangeMsg(underwriteId);
			if (hasPendingChangeMsg) {
				// MSG_1261656 存在未確認的核保中異動信息，請確認。
				multiWarning.addWarning("MSG_1261656");
				multiWarning.setContinuable(false);
			}
		}
	}

	/**
	 * Check prefer life ind of the life insured
	 * 
	 * @param insuredList Insured List
	 * @param products Products
	 * @param warning MultiWarning
	 * @throws GenericException Application Exception
	 */
	public void checkPreferLifeInd(Collection insuredList, Collection products,
					MultiWarning warning) throws GenericException {
		Iterator uliIter = insuredList.iterator();
		while (uliIter.hasNext()) {
			UwLifeInsuredVO ulivo = (UwLifeInsuredVO) uliIter.next();
			if ("N".equals(ulivo.getStandLife())) {
				Iterator productIter = products.iterator();
				while (productIter.hasNext()) {
					UwProductVO product = (UwProductVO) productIter.next();
					// added by jason.luo at 12.07.2004
					if (!helper.isApplicableForPerferLifeCheck(product)) {
						continue;
					}
					// 2-Y 1-N 0-N/A
					if (ulivo.getInsuredId().equals(product.getInsured1())) {
						if (CodeCst.INSURED_STATUS__PREFERRED.equals(product
										.getInsuredStatus())) {
							warning.setContinuable(false);
							warning.addWarning(
											"ERR_" + UwExceptionConstants.APP_STD_LIFE_INDI_PREFER_LIFE,
											String.valueOf(product.getProductId()).concat("-")
															.concat(String.valueOf(ulivo.getInsuredId())));
						}
					} else if (ulivo.getInsuredId().equals(product.getInsured2())) {
						if (CodeCst.INSURED_STATUS__PREFERRED.equals(product
										.getInsuredStatus2())) {
							warning.setContinuable(false);
							warning.addWarning(
											"ERR_" + UwExceptionConstants.APP_STD_LIFE_INDI_PREFER_LIFE,
											String.valueOf(product.getProductId()).concat("-")
															.concat(String.valueOf(ulivo.getInsuredId())));
						}
					}
				}
			}
		}
	}

	/**
	 * check MA/MS Factor
	 * 
	 * @param multiWarning
	 * @param products
	 * @throws GenericException
	 */
	protected void checkMAMSFactor(MultiWarning multiWarning,
					Collection products, Collection insureds) throws GenericException {
		boolean isMsFactor;
		boolean isMaFactor;
		Iterator iter = products.iterator();
		while (iter.hasNext()) {
			UwProductVO uwProductVO = (UwProductVO) iter.next();
			isMsFactor = false;
			isMaFactor = false;
			ProductVO showVO = productService.getProductByVersionId(
							Long.valueOf(uwProductVO.getProductId().intValue()),
							uwProductVO.getProductVersionId());
			if (showVO.isMaFactor()) {
				isMaFactor = true;
			}
			if (showVO.isMsFactor()) {
				isMsFactor = true;
			}
			// check ma factor range
			if (isMaFactor && uwProductVO.getUwsafactor() != null) {
				BigDecimal[] rtn = helper.getFactorScope(uwProductVO,
								Integer.valueOf(CodeCst.ILP_FACTOR_TYPE__MA_FACTOR), insureds);
				if ((rtn[0] != null && uwProductVO.getUwsafactor().compareTo(rtn[0]) < 0)
								|| (rtn[1] != null && uwProductVO.getUwsafactor().compareTo(rtn[1]) > 0)) {
					multiWarning.setContinuable(false);
					AppException MSException = new AppException(20510020019L);
					Hashtable map = new Hashtable();
					map.put("factorType", "MA");
					map.put("range", "[" + rtn[0] + "," + rtn[1] + "]");
					MSException.addMessageParameter(map);
					multiWarning.addWarning(MSException);
				}
			}
			if (isMsFactor && uwProductVO.getUwmsfactor() != null) {
				BigDecimal[] rtn = helper.getFactorScope(uwProductVO,
								Integer.valueOf(CodeCst.ILP_FACTOR_TYPE__MS_FACTOR), insureds);
				if ((rtn[0] != null && uwProductVO.getUwmsfactor().compareTo(rtn[0]) < 0)
								&& (rtn[1] != null && uwProductVO.getUwmsfactor().compareTo(rtn[1]) > 0)) {
					multiWarning.setContinuable(false);
					AppException MSException = new AppException(20510020019L);
					Hashtable map = new Hashtable();
					map.put("factorType", "MS");
					map.put("range", "[" + rtn[0] + "," + rtn[1] + "]");
					MSException.addMessageParameter(map);
					multiWarning.addWarning(MSException);
				}
			}
		}
	}

	protected void checkAllLetterSetted(MultiWarning multiWarning, Long policyId)
					throws GenericException {
	}

	/**
	 * check exclusion
	 * 
	 * @param multiWarning
	 * @param underwriteId
	 * @param products
	 * @throws GenericException
	 */
	protected void checkProductExclusion(MultiWarning multiWarning,
					Long underwriteId, Collection products) throws GenericException {
		Collection uwExclusionList = uwPolicyDS.findUwExclusionEntitis(
						underwriteId);
		Iterator iter1 = uwExclusionList.iterator();
		while (iter1.hasNext()) {
			UwExclusionVO uwExclusionVO = (UwExclusionVO) iter1.next();
			Iterator iter2 = products.iterator();
			while (iter2.hasNext()) {
				UwProductVO uwProductVO = (UwProductVO) iter2.next();
				if (uwProductVO.getPolicyId().equals(uwExclusionVO.getPolicyId())
								&& new Long(uwProductVO.getProductId()).equals(uwExclusionVO
												.getProductId())) {
					if (new Integer(CodeCst.PRODUCT_DECISION__DECLINED)
									.equals(uwProductVO.getDecisionId())
									|| new Integer(CodeCst.PRODUCT_DECISION__POSTPONED)
													.equals(uwProductVO.getDecisionId())) {
						multiWarning.setContinuable(false);
						multiWarning.addWarning("ERR_20520120042");
					}
				}
			}
		}
	}

	/**
	 * check medical indicator
	 * 
	 * @param multiWarning
	 * @param underwriteId
	 * @param lifeInsureds
	 * @param products
	 * @throws GenericException
	 */
	protected void checkMedicalIndi(MultiWarning multiWarning, Long underwriteId,
					Collection lifeInsureds, Collection products) throws GenericException {
		if (products != null && products.size() > 0) {
			UwProductVO uwProductVO = (UwProductVO) products.iterator().next();
			Long policyId = uwProductVO.getPolicyId();
			// get insured1 & insured2's info
			Long insured1 = uwProductVO.getInsured1();
			Long insured2 = uwProductVO.getInsured2();
			Integer age1 = uwProductVO.getAge1();
			Integer age2 = uwProductVO.getAge2();
			boolean isError = false;
			for (Iterator iter = lifeInsureds.iterator(); iter.hasNext();) {
				UwLifeInsuredVO element = (UwLifeInsuredVO) iter.next();
				String medicalIndi = element.getMedicalExamIndi();
				if (!"P".equals(medicalIndi)) {
					continue;
				}
				Long insuredId = element.getInsuredId();
				long totalAmount = uwPolicyDS.getTotalAmountOfInsured(insuredId)
								.longValue();
				int insuredAge = 0;
				if (insuredId.equals(insured1)) {
					insuredAge = age1.intValue();
				} else if (insuredId.equals(insured2)) {
					insuredAge = age2.intValue();
				}
				if (insuredAge >= 16 && insuredAge <= 50) {
					if (totalAmount >= MEDICAL_COMPARE_AMOUNT_1) {
						isError = true;
					}
				} else if (insuredAge >= 51 && insuredAge <= 55) {
					int MEDICAL_COMPARE_AMOUNT_2 = 250000;
					if (totalAmount >= MEDICAL_COMPARE_AMOUNT_2) {
						isError = true;
					}
				}
				boolean isDeclarationExist = uwPolicyDS.getDeclarationExitIndi(
								policyId, insuredId);
				if (isDeclarationExist) {
					isError = true;
				}
				if (isError) {
					multiWarning.setContinuable(true);
					//保單不允許輔助醫療測試
					multiWarning.addWarning("ERR_20510020014");
				}
			}
		}
	}

	/**
	 * @param multiWarning
	 * @param products
	 * @throws GenericException
	 */
	protected void checkProductDecision(MultiWarning multiWarning,
					Collection products) throws GenericException {
		// check product decision based on relationship of product decision
		HashMap map = new HashMap();
		Iterator iter3 = products.iterator();
		while (iter3.hasNext()) {
			UwProductVO product = (UwProductVO) iter3.next();
			if (helper.isProductInvalidate(product)) {
				helper.productValidate(product, products, map, multiWarning);
			}
		}
		// check whether product decision violates the rule
		helper.checkProductDecisionRule(products, multiWarning);
	}

	/**
	 * @param multiWarning
	 * @param uwPolicyVO
	 */
	protected void checkLcaIndi(MultiWarning multiWarning, UwPolicyVO uwPolicyVO) {
		if ("Y".equals(uwPolicyVO.getGenerateLcaIndi())) {
			if (!"Y".equals(uwPolicyVO.getLcaDateIndi())) {
				multiWarning.setContinuable(false);
				multiWarning.addWarning("ERR_"
								+ UwExceptionConstants.APP_LCA_INDI_NOT_CONSISTENT);
			}
		}
	}

	/**
	 * to check whether these are any fac request
	 * 
	 * @param multiWarning
	 * @param products
	 * @throws GenericException
	 */
	protected void checkFacRequest(MultiWarning multiWarning, Collection products)
					throws GenericException {
		Iterator iter3 = products.iterator();
		UwProductVO product = null;
		while (iter3.hasNext()) {
			product = (UwProductVO) iter3.next();
			if (uwPolicyDS.hasUncompleteApplyFacRequest(product.getItemId())) {
				multiWarning.setContinuable(false);
				multiWarning
								.addWarning("ERR_" + UwExceptionConstants.Fac_REQUEST_CHECK);
			}
		}
	}

	/**
	 * @param multiWarning
	 * @param uwPolicyVO
	 * @param products
	 * @throws GenericException
	 */
	protected void checkBackDatingIndi(MultiWarning multiWarning,
					UwPolicyVO uwPolicyVO, Collection products, boolean isNewbiz)
					throws GenericException {
		if (isNewbiz) {
			Date commencementDate = uwPolicyVO.getValidateDate();
			// check backdating against product definition
			if (ActionUtil.compareDate(AppContext.getCurrentUserLocalTime(),
							commencementDate)) {
				Iterator productsIter = products.iterator();
				while (productsIter.hasNext()) {
					UwProductVO upvo = (UwProductVO) productsIter.next();
					// modified by hendry.xu for GEL00023877.
					// if (null == upvo.getMasterId()) {
					if (!productService
									.getProductByVersionId(Long.valueOf(upvo.getProductId().intValue()),
													upvo.getProductVersionId())
									.isBackdateIndi()) {
						multiWarning.setContinuable(false);
						multiWarning.addWarning("ERR_"
										+ UwExceptionConstants.APP_BACKDATING_NOT_ALLOWED,
										String.valueOf(upvo.getProductId()));
					}
					// }
				}
			}
		}
	}

	/**
	 * @param multiWarning
	 * @param uwPolicyVO
	 */
	protected void checkCommenceAndRiskCommenceDate(MultiWarning multiWarning,
					UwPolicyVO uwPolicyVO) {
		// check commencement date
		/*
		 * if (ActionUtil.compareDate(uwPolicyVO.getValidateDate(), AppContext
		 * .getCurrentUserLocalTime())) { multiWarning.setContinuable(false);
		 * multiWarning.addWarning("ERR_" +
		 * UwExceptionConstants.APP_POSTDATE_NOT_ALLOWED); } // check risk
		 * commencement date if (null != uwPolicyVO.getActualValidate()) { if
		 * (ActionUtil.compareDate(uwPolicyVO.getActualValidate(), AppContext
		 * .getCurrentUserLocalTime())) { multiWarning.setContinuable(false);
		 * multiWarning.addWarning("ERR_" +
		 * UwExceptionConstants.APP_POSTDATE_NOT_ALLOWED); } }
		 */
	}

	/**
	 * check stardard life indicator
	 * 
	 * @param multiWarning
	 * @param underwriteId
	 * @param products
	 * @param lifeInsureds
	 * @throws GenericException
	 */
	protected void checkStandLifeIndi(MultiWarning multiWarning,
					Long underwriteId, Collection products, Collection lifeInsureds,
					UwPolicyVO uwPolicyVO) throws GenericException {
		// check standard life indicator against product definition
		Iterator productIter = products.iterator();
		while (productIter.hasNext()) {
			UwProductVO product = (UwProductVO) productIter.next();
			// add by hanzhong.yan for defect GEL00035353 2007/11/2
			Long insured1 = product.getInsured1() == null ? Long.valueOf(0)
							: product
											.getInsured1();
			Long insured2 = product.getInsured2() == null ? Long.valueOf(0)
							: product
											.getInsured2();
			int productId = product.getProductId().intValue();
			ProductVO basicInfoVO = productService.getProductByVersionId(
							Long.valueOf(productId),
							product.getProductVersionId());
			if (basicInfoVO.isStandLifeIndi()) {
				Iterator insureIter = lifeInsureds.iterator();
				while (insureIter.hasNext()) {
					UwLifeInsuredVO ulivo = (UwLifeInsuredVO) insureIter.next();
					if (ulivo.getInsuredId().longValue() != insured1.longValue()
									&& ulivo.getInsuredId().longValue() != insured2.longValue()) {
						continue;
					}
					if (!"Y".equals(ulivo.getStandLife())) {
						multiWarning.setContinuable(false);
						multiWarning.addWarning(
										"ERR_" + UwExceptionConstants.APP_STAND_LIFE_ONLY,
										String.valueOf(product.getProductId()).concat("-")
														.concat(String.valueOf(ulivo.getInsuredId())));
					}
				}
			}
		}
		// check standard life indicator against lia
		Iterator insuredIter = lifeInsureds.iterator();
		while (insuredIter.hasNext()) {
			UwLifeInsuredVO uwLifeInsuredVO = (UwLifeInsuredVO) insuredIter.next();
			if ("N".equals(uwLifeInsuredVO.getStandLife())) {
				Long insuredId = uwLifeInsuredVO.getInsuredId();
				boolean isLiaExist = liaCI.isLIAExistForPartyAndPolicy(insuredId,
								uwPolicyVO.getPolicyCode());
				if (!isLiaExist) {
					multiWarning.setContinuable(false);
					multiWarning.addWarning("ERR_"
									+ UwExceptionConstants.APP_STD_LIFE_INDI_LIEN_OR_LIA);
				}
			}
		}
		// check standard life indicator against LA health status
		Iterator iter2 = products.iterator();
		long insured1 = 0l;
		long insured2 = 0l;
		while (iter2.hasNext()) {
			UwProductVO uwProductVO = (UwProductVO) iter2.next();
			if (uwProductVO.getInsured1() != null) {
				insured1 = uwProductVO.getInsured1().longValue();
			}
			if (uwProductVO.getInsured2() != null) {
				insured2 = uwProductVO.getInsured2().longValue();
			}
			// Whether health Loading has been imposed
			Collection extraLoadings = uwPolicyDS.findUwExtraLoadingEntitis(
							underwriteId, uwProductVO.getItemId());
			Iterator extraLoadingIter = extraLoadings.iterator();
			while (extraLoadingIter.hasNext()) {
				UwExtraLoadingVO uelvo = (UwExtraLoadingVO) extraLoadingIter.next();
				if (UwExtraLoadingTypeConstants.HEALTH.equals(uelvo.getExtraType())) {
					long endTime = 0l;
					long currentDate = 0l;
					try {
						endTime = DateUtils.truncateDay(uelvo.getEndDate()).getTime();
						currentDate = DateUtils.truncateDay(
										AppContext.getCurrentUserLocalTime()).getTime();
					} catch (Exception e) {
						throw ExceptionFactory.parse(e);
					}
					// add by hanzhong.yan for cq:GEL00035314 2007/11/1
					if (endTime <= currentDate) {// if the HE has expired , then
													// continue;
						continue;
					}
					boolean needCheck = true;
					Iterator laIter = lifeInsureds.iterator();
					while (laIter.hasNext()) {
						UwLifeInsuredVO lavo = (UwLifeInsuredVO) laIter.next();
						/*
						 * if the insured is neither insured1 nor insured2 of current
						 * product,then skip the check
						 */
						if (lavo.getInsuredId().longValue() != insured1
										&& lavo.getInsuredId().longValue() != insured2) {
							continue;
						}
						if ("N".equals(lavo.getStandLife())) {
							needCheck = false;
							break;
						}
					}
					if (needCheck) {
						multiWarning.setContinuable(false);
						multiWarning.addWarning("ERR_"
										+ UwExceptionConstants.APP_STD_LIFE_INDI_LIEN_OR_HEALTH);
					}
				}
			}
		}
	}

	/**
	 * @param mapping
	 * @param form
	 * @param request
	 * @param res
	 * @return @throws Exception
	 */
	@Override
	public ActionForward process(ActionMapping mapping, ActionForm form,
					HttpServletRequest request, HttpServletResponse res) throws Exception {
		String continueIndicator = request
						.getParameter("clickByContinueButtonIndicator");

		String isWarningConfirmed = request.getParameter("syskey_warning_confirmed");
		/** PCR-480318 轉陳覆核主管:uw_warning_transfer 已完成核保紙本簽核作業:uw_warning_approve*/
		boolean isWarnTransfer = Boolean.parseBoolean(request.getParameter("uw_warning_transfer"));
		// retrieve underwrite id from request
		Long underwriteId = Long.valueOf(request.getParameter("underwriteId"));
		UwPolicyVO uwPolicyVO = uwPolicyDS.findUwPolicy(underwriteId);

		ApplicationLogger applicationLogger = new ApplicationLogger();
		ApplicationLogger.setPolicyCode(uwPolicyVO.getPolicyCode());

		if (UwSourceTypeConstants.NEW_BIZ.equals(uwPolicyVO.getUwSourceType())) {
			ApplicationLogger.setSystemCode(AppLogDataHelper.SYSTEM_CODE_UNB);
			ApplicationLogger.setOptionId(DateUtils.date2String(new Date(), "yyyyMMdd HHmmss"));
		} else {
			ApplicationLogger.setSystemCode(AppLogDataHelper.SYSTEM_CODE_POS);
			if (uwPolicyVO.getChangeId() != null) {
				ApplicationLogger.setOptionId(uwPolicyVO.getChangeId().toString());
			}
		}
		
		ApplicationLogger.addLoggerData(com.ebao.pub.util.EnvUtils.getEnvLabel());
		ApplicationLogger.setJobName("UwPolicySubmitAction");
		ApplicationLogger.addLoggerData("######uw policy submit action begin");

		ApplicationLogger.addLoggerData("policyRevivalDS.printPolicyAccountInfo() begin");
		policyRevivalDS.printPolicyAccountInfo(uwPolicyVO.getPolicyId());
		ApplicationLogger.addLoggerData("policyRevivalDS.printPolicyAccountInfo() end");
		// judge if the underwriting is newbiz underwriting ,cs underwriting or
		// claim underwriting
		String uwSourceType = uwPolicyVO.getUwSourceType();
		boolean isNewbiz = CodeCst.UW_SOURCE_TYPE__NB.equals(uwSourceType);
		boolean isCs = Utils.isCsUw(uwSourceType);
		boolean isCompleUnderwriting = false;
		if (request.getAttribute("isCompleUnderwriting") != null) {
			isCompleUnderwriting = ((Boolean) request.getAttribute("isCompleUnderwriting")).booleanValue();
		}
		// 因processWarning警示訊息由前端重新確認後執行
		logger.info("continueIndicator:" + continueIndicator);
		logger.info("isWarningConfirmed:" + isWarningConfirmed);
		logger.info("isCs:" + isCs + ", isCompleUnderwriting:" + isCompleUnderwriting);

		if (!StringUtils.isNullOrEmpty(continueIndicator)
						|| "true".equals(isWarningConfirmed)) {
			// if we can get the key from parameter,then
			// show that the triger is by clicking
			// continue button
			UserTransaction trans = Trans.getUserTransaction();

			MultiWarning multiWarning = new MultiWarning();
			try {
				trans.begin();
				// modified by hanzhong.yan for cq:GEL00032134 2007/9/7
				// ,override the
				// method below , add a indicator,but it is always transport
				// false in
				// processwaring method.
				// but in process method , it always be true.

				if (isNewbiz || (isCs && Cst.POLICY_DECISION_ACCEPT.equals(uwPolicyVO.getPolicyDecision()))) {
					logger.info("checkUwTransfer...");

					ApplicationLogger.addLoggerData("uwTransferHelper.completeCurrentUwTransfer() begin");
					isCompleUnderwriting = uwTransferHelper.completeCurrentUwTransfer(request, uwPolicyVO, uwSourceType);
					ApplicationLogger.addLoggerData("uwTransferHelper.completeCurrentUwTransfer() end");
                                    /* 下核保決定(任何決定)產生第二版契審表 ,但需檢核核保通過時再次檢核是否有產生核保訊息   */
                                    if (isNewbiz && isCompleUnderwriting) {
                                        String param = Para.getParaValue(com.ebao.ls.pub.CodeCstTgl.OPQ_CHECK_UW_WORK_SHEET);
                                        if (CodeCst.YES_NO__YES.equals(param)) {

                                            ApplicationLogger.addLoggerData("uwWorkSheetService.generateWorkSheet() begin");

                                            LiarocDownloadLogHelper.initNB(LiarocDownloadLogCst.NB_UW_SECOND_WORK_SHEET, uwPolicyVO.getPolicyId());

                                            ResultType uwWorkSheetresult = uwWorkSheetService.generateWorkSheet(
                                                    uwPolicyVO.getPolicyId(),
                                                    uwPolicyVO.getUnderwriteId());
                                            ApplicationLogger.addLoggerData("uwWorkSheetService.generateWorkSheet() end");

                                            if (ResultType.SUCCESS.equals(uwWorkSheetresult) == false) {
                                                /* 契審表產生失敗 */
                                                multiWarning.addWarning(uwWorkSheetresult.getMsgId());
                                                multiWarning.setStrId(uwWorkSheetresult.getMsgId());
                                                isCompleUnderwriting = false;
                                            }
                                        } else {
                                            ApplicationLogger.addLoggerData(String.format("注意：OPQ_CHECK_UW_WORK_SHEET目前設定為%s", param));
                                        }

                                        if (isCompleUnderwriting) {

                                            ApplicationLogger.addLoggerData("uwTransferHelper.doUwTransferFinish() begin");
                                            uwTransferHelper.doUwTransferFinish(request, uwPolicyVO);
                                            ApplicationLogger.addLoggerData("uwTransferHelper.doUwTransferFinish() end");
                                        }
                                    }else if (isNewbiz && !isCompleUnderwriting && isWarnTransfer) {
                                        /* PCR-480318 轉陳覆核主管 未設定覆核主管 出錯誤訊息：未設定覆核主管*/
                                        if(null==uwTransferHelper.getTransferUser(AppContext.getCurrentUser().getUserId(), uwSourceType)) {
                                            multiWarning.addWarning("MSG_1267032");//未設定覆核主管
                                            multiWarning.setContinuable(false);
                                        }
                                    }
				}

				if (multiWarning.isEmpty() == false) {

					if(isCs) {
					    trans.rollback();
					}

					List<String> msg_title = new ArrayList<String>();
					List<String> msg = new ArrayList<String>();
					/* PCR-480318 轉陳覆核主管 未設定覆核主管 出錯誤訊息：未設定覆核主管 紅色字*/
					if (isNewbiz && !isCompleUnderwriting && isWarnTransfer) {
						@SuppressWarnings("unchecked")
						List<GenericWarning> gwarn = multiWarning.getWarnings();
						for (GenericWarning gi : gwarn) {
							msg.add(StringResource.getStringData(gi.getStrId(), request));
						}
						msg_title.add(StringResource.getStringData("MSG_1267030", request) + ":"
								+ StringResource.getStringData(multiWarning.getStrId(), request));// 轉陳覆核主管
						request.setAttribute("msg_title", msg_title);
						request.setAttribute("msg", msg);
						request.setAttribute("errColor", "red");
					} else {
						msg.add(multiWarning.getMessage(request));
						request.setAttribute("msg", msg);
					}
					ActionForward forward = mapping.findForward(UW_MANUAL_ESCALTING_URL);

					try {
						ApplicationLogger.flush();
					} catch (Exception ex) {
						logger.info("ApplicationLogger.flush() : " + ex.getMessage(), ex);
					}
					try {
						LiarocDownloadLogHelper.flush();
					} catch (Exception ex) {
						logger.info("LiarocDownloadLogHelper.flush() : " + ex.getMessage(), ex);
					}
					
					return forward;
				}

				if (isCompleUnderwriting && (!isCs || Cst.POLICY_DECISION_ACCEPT.equals(uwPolicyVO.getPolicyDecision()))) {
					logger.info("compleUnderwriting...");
					// 不為人工陳核，且已簽核完成，執行核保決定

					ApplicationLogger.addLoggerData("compleUnderwriting() begin");
					compleUnderwriting(request, uwPolicyVO, uwSourceType, null, true);
					ApplicationLogger.addLoggerData("compleUnderwriting() end");
			
				}
				try {
					LiarocDownloadLogHelper.flush();
				} catch (Exception ex) {
					logger.info("LiarocDownloadLogHelper.flush() : " + ex.getMessage(), ex);
				}
				trans.commit();
			} catch (Exception e) {
				TransUtils.rollback(trans);
				ApplicationLogger.addLoggerData(ExceptionUtils.getFullStackTrace(e));

				try {
					ApplicationLogger.flush();
				} catch (Exception ex) {
					logger.info("ApplicationLogger.flush() : " + ex.getMessage(), ex);
				}
				throw ExceptionFactory.parse(e);
			} finally {
				LiarocDownloadLogHelper.clear();
			}
		}
		ActionForward forward = null;
		// due to workflow's limitation,currently no any suitable solution for
		// this
		if (CodeCst.UW_SOURCE_TYPE__NB.equals(uwSourceType)) {
                    boolean isEscalated = UwStatusConstants.ESCALATED.equals(uwPolicyVO.getUwStatus());
                    if (isEscalated) {
                        ApplicationLogger.addLoggerData("proposalProcessService.getCurrentTask() begin");
                        WfTaskInstance taskInstance = proposalProcessService.getCurrentTask(uwPolicyVO.getPolicyId());
                        ApplicationLogger.addLoggerData("proposalProcessService.getCurrentTask() end");
                        List<String> msg_title = new ArrayList<String>();
                        List<String> msg = new ArrayList<String>();

                        if (isNeedLegalBeneEvalPolicy) {
                            // 本件須交叉覆核，系統自動陳核至覆核人員
                            msg_title.add(StringResource.getStringData(MsgCst.MSG_1266647, request));
                        }
                        if (isPolicyNeedsTransfer) {
                            // 本件超過授權額度，系統自動陳核至更高權限者
                            msg_title.add(StringResource.getStringData(MsgCst.MSG_1261466, request));
                        }

                        msg.add(StringResource.getStringData(MsgCst.MSG_232585, request)
                                + CodeTable.getCodeDesc("T_EMPLOYEE", taskInstance.getActor()));

                        request.setAttribute("msg_title", msg_title);
                        request.setAttribute("msg", msg);
                        forward = mapping.findForward(UW_MANUAL_ESCALTING_URL);
                    } else {
                        forward = mapping.findForward(UW_NB_WORKFLOW_PAGE);
                    }
		} else if (Utils.isCsUw(uwSourceType)) {
			logger.info("isCompleUnderwriting:" + isCompleUnderwriting);
			logger.info("PolicyDecision:" + uwPolicyVO.getPolicyDecision());
			ApplicationVO csApplicationVO = csApplicationService.getApplication(uwPolicyVO.getChangeId());
			request.setAttribute("csAppStatus", csApplicationVO.getCsAppStatus());
			logger.info("csAppStatus:" + csApplicationVO.getCsAppStatus());
			if (isCompleUnderwriting) {
				// TODO:chris
				Long changeId = uwPolicyVO.getChangeId();
				request.setAttribute("changeId", changeId);
				request.setAttribute("warningCheck", CodeCst.YES_NO__NO);

				if (Cst.POLICY_DECISION_ACCEPT.equals(uwPolicyVO.getPolicyDecision())) {
					logger.info("go to CS_UW_DISP_SUBMIT!");
					// 通過類的核保意見要進行自動核決,進到下一步核准或結案/生效
					// forward = mapping.findForward(CS_SUCCESS_URL);
					forward = mapping.findForward(CS_UW_DISP_SUBMIT);
				} else {
					logger.info("go to CS_UW_DISP_ENTER!");
					request.setAttribute("csActionType", "Reject");
					forward = mapping.findForward(CS_UW_DISP_ENTER);
					// forward = mapping.findForward(CS_SUCCESS_URL);
					// 拒絕類的核保意見要導到核決畫面
				}
			} else {
				// 權限不足或人工呈核,呈核導入resultPage
				logger.info("go to CS_SUCCESS_URL!");

				logger.info("isCompleUnderwriting is False!");
				forward = mapping.findForward(CS_SUCCESS_URL);
			}
		} else {
			forward = mapping.findForward(UW_SHARINGPOOL_URL);
		}
		ApplicationLogger.addLoggerData("######uw policy submit action end");

		ApplicationLogger.addLoggerData("policyRevivalDS.printPolicyAccountInfo() begin");
		policyRevivalDS.printPolicyAccountInfo(uwPolicyVO.getPolicyId());
		ApplicationLogger.addLoggerData("policyRevivalDS.printPolicyAccountInfo() end");

		try {
			ApplicationLogger.flush();
		} catch (Exception e) {
			logger.info("ApplicationLogger.flush() : " + e.getMessage(), e);
		}
		logger.info("forward:" + forward);
		return forward;
	}

	/**
	 * check CS/NBU's query letters
	 * 
	 * @param caseIdStr
	 * @param response
	 * @throws NumberFormatException
	 * @throws GenericException
	 */
	protected void pendingLetterCheck(MultiWarning multiWarning,
					UwPolicyVO uwPolicyVO, boolean isNewbiz) throws NumberFormatException,
					GenericException {
		if (isNewbiz) {
			letterValidationHelper.checkPendingLetter4NbUw(multiWarning,
							uwPolicyVO.getPolicyId());
		} else {// CS underwriting
			letterValidationHelper.checkPendingLetter4CsUw(multiWarning,
							uwPolicyVO.getChangeId());
		}
	}

	/**
	 * check Insured Disability
	 * 
	 * @param multiWarning
	 * @param lifeInsureds
	 * @throws GenericException
	 */
	protected void checkInsuredDisability(MultiWarning multiWarning, List<UwLifeInsuredVO> insuredList) throws GenericException {
		boolean checkResult = true;
		int disabilityCnt = 0;
		int dataExistCnt = 0;
		for (UwLifeInsuredVO ulivo : insuredList) {
			String disabilityIndi = ulivo.getDisabilityIndi();
			if (disabilityIndi != null && disabilityIndi.equals("Y")) {
				disabilityCnt++;
				String disabilityType = ulivo.getDisabilityType();
				if (disabilityType == null || disabilityType.equals("")) {
					checkResult = false;
				} else {
					dataExistCnt++;
				}
			}
		}
		if (!checkResult) {
			multiWarning.setContinuable(false);
			multiWarning.addWarning(
							"ERR_" + UwExceptionConstants.APP_INSURED_DISABILITY_ERROR);
		}
		if (disabilityCnt != dataExistCnt) {
			multiWarning.setContinuable(false);
			multiWarning.addWarning("MSG_1257717");
		}
	}

	/**
	 * checkPendingIssueStatus
	 * 
	 * @param multiWarning
	 * @param uwPolicyVO
	 * @throws GenericException
	 */
	protected void checkPendingIssueStatus(MultiWarning multiWarning, UwPolicyVO uwPolicyVO) throws GenericException {
		String pendingIssueType = uwPolicyVO.getPendingIssueType();
		if (pendingIssueType != null && pendingIssueType.equals("1")) {
			String pendingIssueCause = uwPolicyVO.getPendingIssueCause();
			if (pendingIssueCause != null && pendingIssueCause.equals("2")) {
				String pendingIssueDesc = uwPolicyVO.getPendingIssueDesc();
				if (pendingIssueDesc == null || pendingIssueDesc.equals("")) {
					multiWarning.setContinuable(false);
					/* MSG_1257769  請填寫保單暫不列印原因 */
					multiWarning.addWarning("MSG_1257769");
				}
			}
		}
	}

	/**
	 * check Policy Decision
	 * 
	 * @param multiWarning
	 * @param uwPolicyVO
	 * @throws GenericException
	 */
	protected void checkPolicyDecision(MultiWarning multiWarning, UwPolicyVO uwPolicyVO) throws GenericException {
		String decision = uwPolicyVO.getPolicyDecision();
		List<UwLifeInsuredVO> insuredList = (List) uwPolicyDS.findUwLifeInsuredEntitis(uwPolicyVO.getUnderwriteId());
		
		// 若保單核保決定!=「核保通過」
		if (decision != null && (decision.equals("2") || decision.equals("3") || decision.equals("0"))) {
			String cancelDesc = uwPolicyVO.getCancelDesc();
			if (cancelDesc == null || cancelDesc.equals("")) {
				multiWarning.setContinuable(false);
				//保單核保未通過，請填寫未承保原因
				multiWarning.addWarning("MSG_1257718");
			}
		}
		List<CoverageVO> coverageVOs = coverageService.findByPolicyId(uwPolicyVO.getPolicyId());
		Collections.sort(coverageVOs,new BeanComparator("itemOrder"));
		
		if (decision != null && !decision.equals("A")) {
			
			for(UwLifeInsuredVO uw: insuredList ) {
				if(! "000".equals(uw.getDisabilityType())) {
					boolean isExist = true;
					for(CoverageVO coverageVO : coverageVOs) {
						//只比對相同保險人的險種
						Long insuredId = coverageVO.getLifeInsured1().getInsuredId();
						if(insuredId.equals(uw.getListId())) {
							if(coverageVO.isWaiver()== false) {
								isExist = disabilityCancelDetailService.checkCancelReason1ByItemID(uw.getPolicyId(), coverageVO.getItemId());
							} else {
								isExist = disabilityCancelDetailService.checkCancelReason1ByProdID(uw.getPolicyId(), coverageVO.getProductId());
							}
							if(!isExist) {
								break;
							}
						}
					}
					if(!isExist) {
						String warnInfo = StringResource.getStringData("MSG_ERR_NB_UW_011", AppContext.getCurrentUser().getLangId());
						InsuredVO insuredVO = insuredService.load(uw.getListId());						
						warnInfo = warnInfo.replace("<insuranceName>", insuredVO.getName());
						multiWarning.addWarning(warnInfo);
						multiWarning.setContinuable(false);
					}
				}	
			}	
		}

		for(UwLifeInsuredVO uw : insuredList) {
			InsuredVO insuredVO = null ;
			if("000".equals(uw.getDisabilityType())) {
				boolean flag = disabilityCancelDetailService.checkCancelDetailByInsuredId(uw.getPolicyId(),uw.getListId());
				if(flag) {
					String warnInfo = StringResource.getStringData("MSG_ERR_NB_UW_012", AppContext.getCurrentUser().getLangId());
					insuredVO = insuredService.load(uw.getListId());						
					warnInfo = warnInfo.replace("<insuranceName>", insuredVO.getName());
					multiWarning.addWarning(warnInfo);
					multiWarning.setContinuable(false); 
				}
			}

			/**
			 * 2019/06/25 email調整：
			 * 當保單核保決定=A核保通過，且保單內被保險人+險種存在身心障礙未承保原因類別項目時，
			 * 產生錯誤訊息：「被保險人＜姓名＞投保＜險種代碼＞/＜險種代碼＞/＜險種代碼＞/＜險種代碼＞/＜險種代碼＞(須可顯示所有存在的險種)核保決定為核保通過，但已填寫身心障礙未承保原因類別項目，請刪除。」
			 */
			if (decision != null && decision.equals("A")) {
				if (insuredVO == null) {
					insuredVO = insuredService.load(uw.getListId());
				}
				List<String> warningProduct = new ArrayList<String>();
				List<DisabilityCancelDetailVO> cancelDetailList = disabilityCancelDetailService.findByCertiCode(
								insuredVO.getPolicyId(), "UNB", insuredVO.getCertiCode());
				Set<Long> itemCheckSet = new HashSet<Long>();
				for (DisabilityCancelDetailVO cancelDetailVO : cancelDetailList) {
					if (cancelDetailVO.getItemID() != null) {//查找此被保險人的未承保原因檔資料
						itemCheckSet.add(cancelDetailVO.getItemID());
					}
				}
				//核決通過但有填寫未承保原因檔需出訊息
				for (CoverageVO coverageVO : coverageVOs) {
					CoverageInsuredVO coverageInsuredVO = coverageVO.getLifeInsured1() ;
					//同一被保險人的險種
					if(coverageInsuredVO.getInsuredId().equals(insuredVO.getListId())) {
						if (itemCheckSet.contains(coverageInsuredVO.getItemId())) {
							//現有保項(非刪除保項)有未承保原因檔,add to warningProduct
							String internalId = CodeTable.getCodeById(TableCst.V_PRODUCT_LIFE, coverageVO.getProductId().toString());
							warningProduct.add(internalId);
						}
					}
				
				}
				if (warningProduct.size() > 0) {
					//被保險人＜姓名＞投保＜險種代碼＞/＜險種代碼＞/＜險種代碼＞/＜險種代碼＞/＜險種代碼＞核保決定為核保通過，但已填寫身心障礙未承保原因類別項目，請刪除。
					String warnInfo = StringResource.getStringData("MSG_1264745", AppContext.getCurrentUser().getLangId());
					warnInfo = warnInfo.replace("{Name}", insuredVO.getName());
					warnInfo = warnInfo.replace("{PlanCode}", org.apache.commons.lang.StringUtils.join(warningProduct.toArray(), "/"));
					multiWarning.addWarning(warnInfo);
					multiWarning.setContinuable(false);
				}
			}
		}

	}

    /**
     * checkUWComments
     *
     * @param multiWarning
     * @param uwPolicyVO
     * @throws GenericException
     */
    protected void checkUWComments(MultiWarning multiWarning, UwPolicyVO uwPolicyVO) throws GenericException {
        String decision = uwPolicyVO.getPolicyDecision();
        // 若保單核保決定=「核保通過」
        if (org.apache.commons.lang3.StringUtils.equals(decision, "A")) {
            Collection<UwCommentOptionVO> commentOptions = uwCommentService.getCommentOptionListByVersionId(uwPolicyVO.getCommentVersionId());
            if (CollectionUtils.isNotEmpty(commentOptions)) {
                Collection<UwCommentVO> uwCommentResult = uwCommentService.getCommentResultByUnderwriteId(uwPolicyVO.getUnderwriteId());
                List<UwCommentVO> t_uwCommentResult = new ArrayList<UwCommentVO>();
                CollectionUtils.selectRejected(uwCommentResult, PredicateUtils.anyPredicate(Arrays.asList(
                        new BeanPropertyValueEqualsPredicate("resultCode", ExtGLCst.VALUE_STRING_YES),
                        new BeanPropertyValueEqualsPredicate("resultCode", ExtGLCst.VALUE_STRING_NO),
                        new BeanPropertyValueEqualsPredicate("resultCode", ExtGLCst.VALUE_STRING_NA)
                )), t_uwCommentResult);
                if (CollectionUtils.isEmpty(uwCommentResult) || CollectionUtils.isNotEmpty(t_uwCommentResult)) {
                    multiWarning.setContinuable(false);
                    multiWarning.addWarning("MSG_1261213");
                }
            }
        }
    }

	/**
	 * checkSuspense
	 * 
	 * @param multiWarning
	 * @param uwPolicyVO
	 * @throws GenericException
	 */
	protected void checkSuspense(MultiWarning multiWarning, UwPolicyVO uwPolicyVO) throws GenericException {
		// 若保單核保決定=要保撤回
		if (Cst.POLICY_DECISION_WITHDRAW.equals(uwPolicyVO.getPolicyDecision())) {
			// IR348536 無法在for loop中對issuesDocVos異動會錯，需new另一個來處理
			//無核保信函
			List<IssuesDocVO> issuesDocVosAll = getUWLetterList(uwPolicyVO.getPolicyId());
			List<IssuesDocVO> issuesDocVos = new ArrayList<IssuesDocVO>();
			//排除已作廢信函
			if (issuesDocVosAll != null) {
				for (IssuesDocVO issuesDocVO : issuesDocVosAll) {
					if (StringUtils.isNullOrEmpty(issuesDocVO.getStatus())
							|| !CodeCst.LETTER_STATUS_ABORTED.equals(issuesDocVO.getStatus())) {
						issuesDocVos.add(issuesDocVO);
					}
				}
			}
			if (issuesDocVos.isEmpty()) {
				//懸帳金額大於50萬
				BigDecimal suspenseAmt = cashCI.getSuspenseByIdPremPurpose(uwPolicyVO.getPolicyId(), CodeCst.PREM_PURPOSE__GENERAL);
				int moneyId = policyDao.findPolicyCurrency(uwPolicyVO.getPolicyId());
				suspenseAmt = CodeCst.MONEY__NTD == moneyId ? suspenseAmt : liaRocUploadCommonService.transferToTwCurrency(moneyId,uwPolicyVO.getApplyDate(), suspenseAmt);
				if (suspenseAmt.compareTo(BigDecimal.valueOf(500000)) >= 0) {
					multiWarning.addWarning("MSG_1263319");
				}
			}
		}
	}

    private void checkUwElderCare(MultiWarning multiWarning, UwPolicyVO uwPolicyVO) {
        Set<String> errors = new LinkedHashSet<>();
        String decision = org.apache.commons.lang3.StringUtils.trimToEmpty(uwPolicyVO.getPolicyDecision());
        boolean matchElderCareStartDate = nbSpecialRuleService.isMatchElderCareStartDate(uwPolicyVO.getPolicyId());
        // 保單核保決定=A_核保通過 且 要保書填寫日大於等於2022.10.01
        if ("A".equals(decision) && matchElderCareStartDate) {
            PolicyVO policyVO = policyDS.load(uwPolicyVO.getPolicyId());
            // 保單=高齡投保件(高齡關懷提問)時或高齡投保件(高齡投保評估量表)
            boolean hasElderQuestionCoverage = paValidatorService.hasElderQuestionCoverage(policyVO);
            boolean hasElderScaleCoverage = paValidatorService.hasElderScaleCoverage(policyVO);
            List<UnbRoleVO> elderCareQuestionRoles = paValidatorService.getElderCareQuestionRoles(policyVO);
            List<UnbRoleVO> elderCareScaleRoles = paValidatorService.getElderCareScaleRoles(policyVO);
            if ((hasElderQuestionCoverage && !elderCareQuestionRoles.isEmpty())
                    || (hasElderScaleCoverage && !elderCareScaleRoles.isEmpty())) {
                List<UwElderCareVO> allUwElderCareResultVOs = uwElderCareService.findElderCareDataById(policyVO.getPolicyId(), uwPolicyVO.getUnderwriteId());
                // 符合評估年齡的角色證號
                List<String> certiCodeQuestionRole = elderCareQuestionRoles.stream().map(UnbRoleVO::getCertiCode).collect(Collectors.toList());
                List<String> certiCodeScaleRole = elderCareScaleRoles.stream().map(UnbRoleVO::getCertiCode).collect(Collectors.toList());
                List<String> certiCodes = org.apache.commons.collections4.CollectionUtils.collate(certiCodeQuestionRole, certiCodeScaleRole, false);

                // 「評估結果」不存在生調或電訪代表未填寫完成
                // PCR533204 排除通路已執行過高齡電訪，不檢核
                certiCodes.forEach(certi -> {
                    if (!allUwElderCareResultVOs.stream()
                            .filter(careVO -> org.apache.commons.lang3.StringUtils.equals(certi, careVO.getCertiCode()))
                            .flatMap(careVO -> careVO.getUwElderCareTypeVOs().stream())
                            .findFirst()
                            .isPresent() && !CodeCst.YES_NO__YES.equals(policyDS.isBrBdElderCalloutFlag(policyVO))) {
                        errors.add("MSG_76");
                    }
                });
                // 「評估結果」任一角色勾選「欠缺辨識不利投保權益之能力或保險商品不適合」時，顯示「高齡投保件之核保評估結果為不適合投保。」錯誤訊息。
                if (allUwElderCareResultVOs.stream()
                        .filter(careVO -> certiCodes.contains(careVO.getCertiCode()))
                        .anyMatch(careVO -> org.apache.commons.lang3.StringUtils.equals(careVO.getScaleResult(), YES_NO__NO))) {
                    errors.add("MSG_73");
                }

                // 「核保員評估」或「評估結果」或「核保評估說明」任一角色未勾選或填寫，顯示「高齡投保件核保評估未完成，請確認。」錯誤訊息。
                if (!allUwElderCareResultVOs.stream()
                        .filter(careVO -> certiCodes.contains(careVO.getCertiCode()))
                        .findFirst()
                        .isPresent()) {
                    errors.add("MSG_76");
                }
                // 「核保員評估」或「評估結果」或「核保評估說明」任一角色未勾選，顯示「高齡投保件核保評估未完成，請確認。」錯誤訊息。
                certiCodes.forEach(certi -> {
                    if (!allUwElderCareResultVOs.stream()
                    		.anyMatch(careVO -> org.apache.commons.lang3.StringUtils.equals(certi, careVO.getCertiCode()))) {
                        errors.add("MSG_76");
                    }
                });
                // 「核保員評估」或「評估結果」或「核保評估說明」任一角色未填寫，顯示「高齡投保件核保評估未完成，請確認。」錯誤訊息。
                if (allUwElderCareResultVOs.stream()
                        .filter(careVO -> certiCodes.contains(careVO.getCertiCode()))
                        .anyMatch(careVO -> {
                            String attchElderScale = org.apache.commons.lang3.StringUtils.trimToEmpty(careVO.getAttchElderScale());
                            String scaleResult = org.apache.commons.lang3.StringUtils.trimToEmpty(careVO.getScaleResult());
                            String scaleComment = org.apache.commons.lang3.StringUtils.trimToEmpty(careVO.getScaleComment());
                            scaleComment = scaleComment.replaceAll(String.valueOf(CharUtils.CR), "").replaceAll(String.valueOf(CharUtils.LF), "");
                            return !org.apache.commons.lang3.StringUtils.equals(attchElderScale, YES_NO__YES)
                                    || !ArrayUtils.contains(new String[]{YES_NO__YES, YES_NO__NO, YES_NO__NA}, scaleResult)
                                    || org.apache.commons.lang3.StringUtils.isBlank(scaleComment);
                        })) {
                    errors.add("MSG_76");
                }

				// 2023.2.6 PCR 500628 高齡投保件核保評估功能需求
				// 高齡關懷提問會辦方式，若是電訪和生調都有，兩種都要評估，因此兩種都要檢核
				allUwElderCareResultVOs.stream()
						.filter(careVO -> certiCodes.contains(careVO.getCertiCode()))
						.forEach(careVO -> {
							careVO.getUwElderCareTypeVOs().stream()
									// 生調檢查最新
									.filter(careTypeVO -> ObjectUtils.equals(careTypeVO.getCareType(), UW_ELDER_CARE_TYPE_LIFE_SURVEY))
									.sorted(Comparator.comparing(UwElderCareTypeVO::getUpdateTimestamp).reversed())
									.limit(1)
									.forEach(careTypeVO -> {
										if (careTypeVO.getUwElderCareDtlVOs().stream()
												.anyMatch(dtlVO -> !org.apache.commons.lang3.StringUtils.equals(dtlVO.getResultCode(), YES_NO__YES))) {
											errors.add("MSG_76");
										}
									});
						});

				allUwElderCareResultVOs.stream()
						.filter(careVO -> certiCodes.contains(careVO.getCertiCode()))
						// 電訪檢查全部
						.forEach(careVO -> {
							if (careVO.getUwElderCareTypeVOs().stream()
									.filter(typeVO -> ObjectUtils.equals(typeVO.getCareType(), UW_ELDER_CARE_TYPE_CALLOUT))
									.flatMap(typeVO -> typeVO.getUwElderCareDtlVOs().stream())
									.anyMatch(dtlVO -> !org.apache.commons.lang3.StringUtils.equals(dtlVO.getResultCode(), YES_NO__YES))) {
								errors.add("MSG_76");
							}
						});
            }
        }

        if (!errors.isEmpty()) {
            multiWarning.setContinuable(false);
            errors.forEach(multiWarning::addWarning);
        }
    }

    private void checkLegalBeneEvalNotes(MultiWarning multiWarning, UwPolicyVO uwPolicyVO) {
        String decision = org.apache.commons.lang3.StringUtils.trimToEmpty(uwPolicyVO.getPolicyDecision());
        String legalBeneEvalNotes = org.apache.commons.lang3.StringUtils.trimToEmpty(uwPolicyVO.getLegalBeneEvalNotes());
        legalBeneEvalNotes = legalBeneEvalNotes.replaceAll(String.valueOf(CharUtils.CR), "").replaceAll(String.valueOf(CharUtils.LF), "");

        if (!decision.isEmpty() && legalBeneEvalNotes.length() < 10) {
            // 若保單核保決定有值且「法人實質受益人辨識評估結果」欄位=空白或少於10個字時，顯示錯誤訊息
            if (uwTransferHelper.isLegalBeneEvalPolicy(uwPolicyVO)) {
                multiWarning.setContinuable(false);
                // 要保人或受益人為法人，「法人實質受益人辨識評估結果」欄位未填寫或填寫未完整。
                multiWarning.addWarning("MSG_1266560");
            }
        }
    }
    
    private void checkBlankField(MultiWarning multiWarning, List<UwLifeInsuredVO> insuredList ) {
    	
    	boolean checkGuardianAncmnt = Boolean.FALSE;
    	boolean checkMedicalExamIndi = Boolean.FALSE;
    	boolean checkImageIndi = Boolean.FALSE;
    	boolean checkDisabilityIndi = Boolean.FALSE;
    	boolean checkHoLifeSurveyIndi = Boolean.FALSE;
    	
    	for (UwLifeInsuredVO ulivo : insuredList) {

    		String guardianAncmnt = ulivo.getGuardianAncmnt();
			String medicalExamIndi = ulivo.getMedicalExamIndi();
			String imageIndi = ulivo.getImageIndi();
			String disabilityIndi = ulivo.getDisabilityIndi();
			String hoLifeSurveyIndi = ulivo.getHoLifeSurveyIndi();
			
			if( StringUtils.isNullOrEmpty(guardianAncmnt) ) {
				checkGuardianAncmnt = Boolean.TRUE;
			}
			
			if( StringUtils.isNullOrEmpty(medicalExamIndi) ) {
				checkMedicalExamIndi = Boolean.TRUE;
			}

			if( StringUtils.isNullOrEmpty(imageIndi) ) {
				checkImageIndi = Boolean.TRUE;
			}

			if( StringUtils.isNullOrEmpty(disabilityIndi) ) {
				checkDisabilityIndi = Boolean.TRUE;
			}

			if( StringUtils.isNullOrEmpty(hoLifeSurveyIndi) ) {
				checkHoLifeSurveyIndi = Boolean.TRUE;
			}
		}
    	
    	if( checkGuardianAncmnt ) {
    		multiWarning.setContinuable(false); //監護宣告未填寫
    	    multiWarning.addWarning("MSG_1265717");
    	}
    	
    	if( checkMedicalExamIndi ) {
    		
    		multiWarning.setContinuable(false);//是否體檢未選擇
    	    multiWarning.addWarning( StringResource.getStringData("MSG_101926", AppContext.getCurrentUser().getLangId()) + 
    				StringResource.getStringData("MSG_1261109", AppContext.getCurrentUser().getLangId())  ); 
    	}
    	
    	if( checkImageIndi ) {
    		
    		multiWarning.setContinuable(false);//是否調閱影像 未選擇
    	    multiWarning.addWarning( StringResource.getStringData("MSG_1257257", AppContext.getCurrentUser().getLangId()) + 
    				StringResource.getStringData("MSG_1261109", AppContext.getCurrentUser().getLangId()) ); 
    	}
    	
    	if( checkDisabilityIndi ) {
    		
    		multiWarning.setContinuable(false);//身心障礙 未選擇
    	    multiWarning.addWarning(  StringResource.getStringData("MSG_1250818", AppContext.getCurrentUser().getLangId()) + 
    				StringResource.getStringData("MSG_1261109", AppContext.getCurrentUser().getLangId())  ); 
    	}
    	
    	if( checkHoLifeSurveyIndi ) {
    		
    		multiWarning.setContinuable(false);// 內勤人員生調 
    	    multiWarning.addWarning( StringResource.getStringData("MSG_1260652", AppContext.getCurrentUser().getLangId()) + 
    				StringResource.getStringData("MSG_1261109", AppContext.getCurrentUser().getLangId()) ); 
    	}

    }
    
private void checkInsuredMicroRole(MultiWarning multiWarning, Long policyId, List<UwLifeInsuredVO> insuredList ) {
    	
    List<NbInsMicroRoleVO> nbInsMicroRoleList = nbInsMicroRoleService.findByPolicyId(policyId);
    
    for( UwLifeInsuredVO insuredVO : insuredList) {
      	//取被保險人舊的主要微型身份設定資料
        List<NbInsMicroRoleVO> nbMainInsMicroRoleVOList = nbInsMicroRoleList.stream().filter( 
        		( NbInsMicroRoleVO vo ) -> CodeCst.YES_NO__YES.equals( vo.getMainRoleflag() ) && insuredVO.getListId().equals(vo.getInsuredId()) ).collect(Collectors.toList());

        //	1.1	投保微型保險且保單核保決定A-核保通過且「被保險人主要微型身分」未勾選，顯示「投保微型保險，請依已檢附證明文件之微型身分擇一勾選「被保險人主要微型身分」」。
        if( nbMainInsMicroRoleVOList.isEmpty() ) {
        	multiWarning.setContinuable(false);
    	    multiWarning.addWarning( StringResource.getStringData("MSG_1267197", AppContext.getCurrentUser().getLangId())  ); 
        }else {
        
        	//1.2投保微型保險且保單核保決定A-核保通過且「身心障礙」≠Y-是且「被保險人主要微型身分」=本人09且微型身分本人之身心障礙類別或等級空白，
        	//   顯示「投保微型保險，主要微型投保身分為身心障礙者本人，請勾選「微型身分本人之身心障礙類別及等級」」。
        	NbInsMicroRoleVO nbInsMicroRoleVO = nbMainInsMicroRoleVOList.get(0);
            if(  CodeCst.RELATION_TO_PHROLE_SELF.equals(nbInsMicroRoleVO.getInsRoleType())
            		&& CodeCst.PH_ROLE_CODE_NINE.equals(nbInsMicroRoleVO.getMicroType()) 
            			&& !CodeCst.YES_NO__YES.equals(insuredVO.getDisabilityIndi())
            					&&  ( StringUtils.isNullOrEmpty(insuredVO.getMicroDisabilityTypeList()) 
            						  || StringUtils.isNullOrEmpty(insuredVO.getMicroDisabilityClass()) ) ) {
            	 
            	multiWarning.setContinuable(false);
        	    multiWarning.addWarning( StringResource.getStringData("MSG_1267198", AppContext.getCurrentUser().getLangId())  ); 
            }
        }
    }

}

    /**
     * 洗錢資恐高風險客戶核保審查意見
     *
     * @param multiWarning
     * @param uwPolicyVO
     * @throws GenericException
     */
    protected void checkRiskReprotNotes(MultiWarning multiWarning, UwPolicyVO uwPolicyVO) throws GenericException {

        String decision = org.apache.commons.lang3.StringUtils.trimToEmpty(uwPolicyVO.getPolicyDecision());
        String riskReportNotes = org.apache.commons.lang3.StringUtils.trimToEmpty(uwPolicyVO.getRiskReportNotes());
        riskReportNotes = riskReportNotes.replaceAll(String.valueOf(CharUtils.CR), "").replaceAll(String.valueOf(CharUtils.LF), "");
        // 若保單核保決定有值
        if (!decision.isEmpty()) {
            PolicyInfo policy = policyService.load(uwPolicyVO.getPolicyId());
            String riskLevel = riskCustomerService.queryAMLRiskLevelByCertiCode(policy.getPolicyHolder().getCertiCode());
            // 保單洗錢防治=高風險
            if ("H".equals(riskLevel)) {
                // 且高風險核保意見=空白或少於10個字
                if (riskReportNotes.length() < 10) {
                    multiWarning.setContinuable(false);
                    /*要保人為洗錢資恐高風險客戶，洗錢資恐高風險客戶核保審查意見欄未填寫或填寫未完整。*/
                    multiWarning.addWarning("MSG_1264955");
                }
            }
        }
    }

    private void checkNbRiResults(MultiWarning multiWarning, UwPolicyVO uwPolicyVO) {
        if (ObjectUtils.notEqual(uwPolicyVO, null)) {
            if (org.apache.commons.lang3.StringUtils.equals("A", uwPolicyVO.getPolicyDecision())) {
                PolicyVO policyVO = policyVoService.load(uwPolicyVO.getPolicyId());
                Date underwriteTime = AppContext.getCurrentUserLocalTime();
                Date applyDate = policyVO.getApplyDate();
                
                List<NbRiResultVO> nbRiResultVOList = uwRiApplyService.queryNbRiResultList(policyVO);
                for (NbRiResultVO nbRiResultVO : nbRiResultVOList) {
                    Date reinsuredDate = nbRiResultVO.getReinsuredDate();

                    try {
                        if (org.apache.commons.lang3.time.DateUtils.truncatedCompareTo(reinsuredDate, applyDate, Calendar.DATE) < 0
                                || org.apache.commons.lang3.time.DateUtils.truncatedCompareTo(reinsuredDate, underwriteTime, Calendar.DATE) > 0) {
                            multiWarning.setContinuable(false);
                            // MSG_37-再保回覆日不可早於要保書填寫日或晚於核保決定日，請確認!
                            multiWarning.addWarning("MSG_37");
                        }
                    } catch (Exception e) {
                        multiWarning.setContinuable(false);
                        // MSG_37-再保回覆日不可早於要保書填寫日或晚於核保決定日，請確認!
                        multiWarning.addWarning("MSG_37");
                    }
                }
            }
        }
    }

	/**
	 * <p>Description : 取得 照會信函列表</p>
	 * <p>Created By : YCsu</p>
	 * <p>Create Time : Nov 01, 2017</p>
	 * @param policyId
	 * @return
	 */
	protected List<IssuesDocVO> getUWLetterList(Long policyId) {
		List<IssuesDocVO> issuesDocVos = documentManagementService
						.getUWLetterListByPolicyId(policyId);
		return issuesDocVos;
	}

	/**
	 * <p>Description :  IR_397347_保單70000236150險種XMR抓錯版本
	 * 	因java cache可能會造成版本錯誤，增加檢核直接比對資料庫版本</p>
	 * <p>Created By : Simon Huang</p>
	 * <p>Create Time : 2020年7月14日</p>
	 * @param policyId
	 * @return
	 * @throws com.ebao.pub.framework.ObjectNotFoundException
	 * @throws GenericException
	 */
	public String getCheckProductVersionStr(Long policyId)
					throws com.ebao.pub.framework.ObjectNotFoundException, GenericException {
		String message = "";
		DBean db = new DBean(true);
		java.sql.PreparedStatement stmt = null;
		Connection conn = null;
		java.sql.ResultSet rows = null;
		try {
			db.connect();
			conn = db.getConnection();

			stmt = conn.prepareStatement("SELECT pkg_ls_pm_nb_ci.f_check_product_version_str(?) AS VAL FROM DUAL");
			stmt.setLong(1, policyId);
			stmt.execute();
			rows = stmt.executeQuery();
			while (rows.next()) {
				message = rows.getString("VAL");
			}
		} catch (Exception ex) {
			throw ExceptionUtil.parse(ex);
		} finally {
			DBean.closeAll(null, stmt, db);
		}
		return message;
	}
	
	private boolean checkExtraPrem(CoverageInfo coverage) {
		boolean hasExtraPrem = false;
		
		List<ExtraPremInfo> extraPremInfoList = coverage.getExtraPrems();
		if (org.apache.commons.collections4.CollectionUtils.isNotEmpty(extraPremInfoList)) {
			for (ExtraPremInfo extraPremInfo : extraPremInfoList) {
				if (BigDecimal.ZERO.compareTo(extraPremInfo.getExtraPrem()) == -1) {//保單存在加費資料(加費保險費及加收保險成本>0)
					hasExtraPrem = true;
					
					break;
				}
			}
		}
		
		return hasExtraPrem;
	}

	/**
	 * <p>Description : 核保前先寫入policy log(service id=1095)</p>
	 * <p>Created By : jeter.chen</p>
	 * <p>Create Time : Dec 31, 2020</p>
	 * @param policyId
	 * @throws GenericException
	 */
	private void processLog(Long policyId) throws Exception {

		//log write
		org.hibernate.Session s = com.ebao.foundation.module.db.hibernate.HibernateSession3
				.currentSession();
		UserTransaction ut = Trans.getUserTransaction();
		ut.begin();
		int serviceId = CodeCst.SERVICE_UNB_UW_LOG;
		AlterationItemVO changeVO = null;
		Long changeId = null;
		Long policyChgId = null;
		//log write
		changeVO = ChangeSP.generatePolicyChange(
				policyId,
				CodeCst.CHANGE_SOURCE__NEWBIZ,
				serviceId,
				AppContext.getCurrentUser().getUserId());
		changeId = changeVO.getMasterChgId();
		policyChgId = changeVO.getPolicyChgId();

		List<PolicyBankAuthVO> bankAuthVoList = policyBankAuthService.findByPolicyId(policyId);

		for(PolicyBankAuthVO bankAuthVO : bankAuthVoList){
			if (bankAuthVO.getChangeId() == null &&
					bankAuthVO.getPolicyChangeId() == null) {
				//各複製一筆出來作核保記錄(刪除註記)
				PolicyBankAuthVO newPolicyBankAuth = new PolicyBankAuthVO();
				BeanUtils.copyProperties(newPolicyBankAuth, bankAuthVO);
				newPolicyBankAuth.setListId(null);
				newPolicyBankAuth.setChangeId(changeId);
				newPolicyBankAuth.setPolicyChangeId(policyChgId);
				newPolicyBankAuth.setApprovalStatus(CodeCst.APPROVAL_STATUS__DELETED);
				policyBankAuthService.saveOrUpdate(newPolicyBankAuth);

				bankAuthVO.setChangeId(null);
				bankAuthVO.setPolicyChangeId(null);
				policyBankAuthService.saveOrUpdate(bankAuthVO);
			}
		}

	    //存FATCALOG資料
//		FatcaIdentityVO fatcaIdentityVO = fatcaCI.queryFatcaIdentityByPolicyId(policyId);
//		if(fatcaIdentityVO != null) {
//			if(fatcaIdentityVO.getTemp() != null) {
//				detailRegHelper.saveFatcaLogData(fatcaIdentityVO, changeId, policyChgId);
//			}
//		}

//		//CRS LOG資料
//		detailRegHelper.saveCrsLogData(policyId, changeId);

		policyDS.writeDumpLogByPolicy("BF", changeId, policyChgId, policyId);

		s.flush();
		ut.commit();
	}

	/**
	 * 新契約執行終止保單下載，需同時下載被保險人身份及要保人身份的公會資料
	 * @param policyId
	 * @return
	 */
	private MultiWarning downloadByUnbTerminateCheck(Long policyId) {

		MultiWarning multiWarning = new MultiWarning();
		
		String errMsg = null;
		try {

			try {
				List<LiaRocDownload2020VO> downloadList = liaRocDownload2020CI.downloadUnbTerminateCheck(policyId);

				if (downloadList == null || downloadList.size() == 0) {
					//公會資料下載失敗，請重新下載
					errMsg = StringResource.getStringData("MSG_1257674", AppContext.getCurrentUser().getLangId());
				} else if (LiaRocCst.LIAROC_DL_STATUS_ERROR.equals(downloadList.get(0).getLiarocDownloadStatus())) {
					//公會資料下載失敗，請重新下載
					errMsg = StringResource.getStringData("MSG_1257674", AppContext.getCurrentUser().getLangId());
				} else {
					//公會資料下載成功
					errMsg = "";
				}
			} catch (IllegalArgumentException e) {
				//請輸入被保險人ID號
				errMsg = e.getMessage();
			}
		} catch (Throwable ex) {
			Log.warn(ex.getMessage());
			//公會資料下載失敗，請重新下載
			errMsg = StringResource.getStringData("MSG_1257674", AppContext.getCurrentUser().getLangId());
		}
		
		if(StringUtils.isNullOrEmpty(errMsg) == false) {
			multiWarning.addWarning("MSG_1257674");
			multiWarning.setContinuable(false);
		}
		
		return multiWarning;
	}

    private boolean isSkipImageCheck() {
        boolean skip = false;
        try {
            // 系統參數UW_SKIP_IMAGE_CHECK_INDI核保時是否略過影像檢查規則
            skip = CodeCst.YES_NO__YES.equals(Para.getParaValue(com.ebao.ls.pa.nb.util.Cst.PARA_NB_UW_SKIP_IMAGE_CHECK_INDI));
        } catch (Exception ex) {
            logger.error("", ex);
        }
        return skip;
    }

    @Resource(name = AlterationItemService.BEAN_DEFAULT)
    private AlterationItemService alterationItemService;

    @Resource(name = UwProcessService.BEAN_DEFAULT)
    private UwProcessService uwProcessDS;

    @Resource(name = UwPolicyService.BEAN_DEFAULT)
    private UwPolicyService uwPolicyDS;

    @Resource(name = ValidatorService.BEAN_DEFAULT)
    ValidatorService paValidatorService;

    @Resource(name = NbValidateRoleConfService.BEAN_DEFAULT)
    private NbValidateRoleConfService paNbValidateRoleConfService;

    @Resource(name = UwElderCareService.BEAN_DEFAULT)
    private UwElderCareService uwElderCareService;

    @Resource(name = ProposalProcessService.BEAN_DEFAULT)
    private ProposalProcessService proposalProcessService;

    @Resource(name = PolicyRemakeDetailService.BEAN_DEFAULT)
	private PolicyRemakeDetailService policyRemakeDetailService;

    @Resource(name = PolicyPrintJobCI.BEAN_DEFAULT)
    private PolicyPrintJobCI policyPrintJobCI;

    @Resource(name = FmtPolicyService.BEAN_DEFAULT)
	private FmtPolicyService fmtPolicyService;

    @Resource(name = CtrsCI.BEAN_DEFAULT)
	private CtrsCI ctrsCI;

    @Resource(name = ECProposalCI.BEAN_DEFAULT)
	private ECProposalCI ecProposalCI;

    @Resource(name = LIACI.BEAN_DEFAULT)
    private LIACI liaCI;

    @Resource(name = LetterValidationHelper.BEAN_DEFAULT)
    private LetterValidationHelper letterValidationHelper;

    @Resource(name = UwPolicySubmitActionHelper.BEAN_DEFAULT)
    private UwPolicySubmitActionHelper helper;

    @Resource(name = ProductService.BEAN_DEFAULT)
    private ProductService productService;

    @Resource(name = PolicyService.BEAN_DEFAULT)
    private PolicyService policyService;

    @Resource(name = EventService.BEAN_DEFAULT)
    private EventService eventService;

    @Resource(name = BeneficiaryService.BEAN_DEFAULT)
    private BeneficiaryService beneficiaryService;

    @Resource(name = UwTransferService.BEAN_DEFAULT)
    private UwTransferService uwTransferService;

    @Resource(name = UserLeaveManagerService.BEAN_DEFAULT)
    private UserLeaveManagerService leaveManagerService;

    @Resource(name = LifeProductCategoryService.BEAN_DEFAULT)
    protected LifeProductCategoryService lifeProductCategoryService;

    @Resource(name = LifeProductService.BEAN_DEFAULT)
    protected LifeProductService lifeProductService;

    @Resource(name = ProposalRuleResultService.BEAN_DEFAULT)
    private ProposalRuleResultService proposalRuleResultService;

    @Resource(name = LiaRocUploadCI.BEAN_DEFAULT)
    private LiaRocUploadCI liaRocUploadCI;

    @Resource(name = LiaRocUploadCommonService.BEAN_DEFAULT)
    private LiaRocUploadCommonService liaRocUploadCommonService;

    @Resource(name = UwCommentService.BEAN_DEFAULT)
    private UwCommentService uwCommentService;

    @Resource(name = "applicationService")
    ApplicationService csApplicationService;

    @Resource(name = ProposalService.BEAN_DEFAULT)
    private ProposalService proposalService;

    @Resource(name = UwTransferHelper.BEAN_DEFAULT)
    private UwTransferHelper uwTransferHelper;

    @Resource(name = CoverageService.BEAN_DEFAULT)
    protected CoverageService coverageService;

    @Resource(name = UwCheckListStatusService.BEAN_DEFAULT)
    protected UwCheckListStatusService uwCheckListStatusService;

    @Resource(name = UwPatchImageService.BEAN_DEFAULT)
    protected UwPatchImageService uwPatchImageService;

    @Resource(name = DocumentManagementService.BEAN_DEFAULT)
    private DocumentManagementService documentManagementService;

    @Resource(name = PolicyRevivalDS.BEAN_DEFAULT)
    private PolicyRevivalDS policyRevivalDS;

    @Resource(name = ProductVersionService.BEAN_DEFAULT)
    protected ProductVersionService productVersionService;

    @Resource(name = LifeProductRTService.BEAN_DEFAULT)
    private LifeProductRTService lifeProductRTService;

    @Resource(name = CashCI.BEAN_DEFAULT)
    private CashCI cashCI;

    @Resource(name = PolicyDao.BEAN_DEFAULT)
    private PolicyDao<Policy> policyDao;

    @Resource(name = NBcrsService.BEAN_DEFAULT)
    private NBcrsService nbCrsService;

    @Resource(name = CRSPartyLogService.BEAN_DEFAULT)
    private CRSPartyLogService crsPartyLogServ;

    @Resource(name = InsuredService.BEAN_DEFAULT)
    private InsuredService insuredService;

    @Resource(name = RiskCustomerService.BEAN_DEFAULT)
    RiskCustomerService riskCustomerService;

    //PCR384234
    @Resource(name = com.ebao.ls.pa.pub.bs.PolicyService.BEAN_DEFAULT)
    private com.ebao.ls.pa.pub.bs.PolicyService policyVoService;

    //PCR384234
    @Resource(name = DetailRegHelper.BEAN_DEFAULT)
    private DetailRegHelper detailRegHelper;

    @Resource(name = com.ebao.ls.pa.pub.bs.PolicyService.BEAN_DEFAULT)
    private com.ebao.ls.pa.pub.bs.PolicyService policyDS;
    @Resource(name = ImageCI.BEAN_DEFAULT)
    private ImageCI imageCI;

    @Resource(name = PolicyBankAuthService.BEAN_DEFAULT)
    private PolicyBankAuthService policyBankAuthService;

}
