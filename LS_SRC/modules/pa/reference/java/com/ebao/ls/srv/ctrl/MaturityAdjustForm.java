package com.ebao.ls.srv.ctrl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.ebao.ls.srv.data.bo.PayDue;
import com.ebao.pub.framework.GenericForm;

/**
 * <p>Title: MaturityAdjustForm </p>		
 * <p>Description: This class Maturity adjust's form class</p>
 * <p>Copyright: Copyright (c) 2002	</p>
 * <p>Company: 	eBaoTech			</p>
 * <p>Create Time:		2005/04/11	</p> 
 * @author simen.li
 * @version 1.0
 */
public class MaturityAdjustForm extends GenericForm {
  private String policyNumber = "";
  private String caseNumber = "";
  private String lifeAssured = "";
  private String policyHolder = "";
  private String payee = "";
  private String idNumber = "";
  private String batchNumber = "";
  private String actionType = "";
  private String disbursementMethod = "";
  private String curDisbursementMethod = "";
  private String bankAccount = "";
  private String dueDate = "";
  private String chequePrintDate = "";
  private BigDecimal netMaturiryAmount = new BigDecimal("0");
  private BigDecimal previousAdjustMaturityAmount = new BigDecimal("0");
  private BigDecimal adjustMaturityAmount = new BigDecimal("0");
  private BigDecimal maturityInterest = new BigDecimal("0");
  private BigDecimal withHoldingTax = new BigDecimal("0");
  private BigDecimal revisedMaturityAmount = new BigDecimal("0");
  private String itemId = "";
  private String accountId = "";
  private String payeeId = "";

  private List accountInfos = new ArrayList();

  private PayDue payDue = null;

  private String checkedAccountId = "";

  private Long payId = Long.valueOf(0);

  private String feeStatus = "";

  /**
   * @return Returns the feeStatus.
   */
  public String getFeeStatus() {
    return feeStatus;
  }
  /**
   * @param feeStatus The feeStatus to set.
   */
  public void setFeeStatus(String feeStatus) {
    this.feeStatus = feeStatus;
  }
  /**
   * @return Returns the payId.
   */
  public Long getPayId() {
    return payId;
  }
  /**
   * @param payId The payId to set.
   */
  public void setPayId(Long payId) {
    this.payId = payId;
  }
  /**
   * @return Returns the checkedAccountId.
   */
  public String getCheckedAccountId() {
    return checkedAccountId;
  }

  /**
   * @param checkedAccountId The checkedAccountId to set.
   */
  public void setCheckedAccountId(String checkedAccountId) {
    this.checkedAccountId = checkedAccountId;
  }

  /**
   * @return Returns the payeeId.
   */
  public String getPayeeId() {
    return payeeId;
  }
  /**
   * @param payeeId The payeeId to set.
   */
  public void setPayeeId(String payeeId) {
    this.payeeId = payeeId;
  }
  /**
   * @return Returns the accountId.
   */
  public String getAccountId() {
    return accountId;
  }
  /**
   * @param accountId The accountId to set.
   */
  public void setAccountId(String accountId) {
    this.accountId = accountId;
  }
  /**
   * @return Returns the tPayDue.
   */
  public PayDue getPayDue() {
    return payDue;
  }
  /**
   * @param payDue The tPayDue to set.
   */
  public void setPayDue(PayDue payDue) {
    this.payDue = payDue;
  }

  /**
   * @return Returns the itemId.
   */
  public String getItemId() {
    return itemId;
  }
  /**
   * @param itemId The itemId to set.
   */
  public void setItemId(String itemId) {
    this.itemId = itemId;
  }
  /**
   * @return Returns the accountInfos.
   */
  public List getAccountInfos() {
    /*
     if (this.accountInfos.size()==0){
     PayeeAccountInfoVO accountInfo=new PayeeAccountInfoVO();
     accountInfo.setAccountHolder("aaa");
     accountInfo.setAccountId("1");
     accountInfo.setBankName("acc1");
     accountInfo.setAccountNo("111");
     accountInfos.add(accountInfo);
     }*/
    return accountInfos;
  }
  /**
   * @param accountInfos The accountInfos to set.
   */
  public void setAccountInfos(List accountInfos) {
    this.accountInfos = accountInfos;
  }

  /**
   * @return Returns the previousAdjustMaturityAmount.
   */
  public BigDecimal getPreviousAdjustMaturityAmount() {
    return previousAdjustMaturityAmount;
  }
  /**
   * @param previousAdjustMaturityAmount The previousAdjustMaturityAmount to set.
   */
  public void setPreviousAdjustMaturityAmount(
      BigDecimal previousAdjustMaturityAmount) {
    this.previousAdjustMaturityAmount = previousAdjustMaturityAmount;
  }

  /**
   * @return Returns the adjustMaturityAmount.
   */
  public BigDecimal getAdjustMaturityAmount() {
    return adjustMaturityAmount;
  }
  /**
   * @param adjustMaturityAmount The adjustMaturityAmount to set.
   */
  public void setAdjustMaturityAmount(BigDecimal adjustMaturityAmount) {
    this.adjustMaturityAmount = adjustMaturityAmount;
  }
  /**
   * @return Returns the bankAccount.
   */
  public String getBankAccount() {
    return bankAccount;
  }
  /**
   * @param bankAccount The bankAccount to set.
   */
  public void setBankAccount(String bankAccount) {
    this.bankAccount = bankAccount;
  }
  /**
   * @return Returns the chequePrintDate.
   */
  public String getChequePrintDate() {
    return chequePrintDate;
  }
  /**
   * @param chequePrintDate The chequePrintDate to set.
   */
  public void setChequePrintDate(String chequePrintDate) {
    this.chequePrintDate = chequePrintDate;
  }
  /**
   * @return Returns the curDisbursementMethod.
   */
  public String getCurDisbursementMethod() {
    return curDisbursementMethod;
  }
  /**
   * @param curDisbursementMethod The curDisbursementMethod to set.
   */
  public void setCurDisbursementMethod(String curDisbursementMethod) {
    this.curDisbursementMethod = curDisbursementMethod;
  }
  /**
   * @return Returns the disbursementMethod.
   */
  public String getDisbursementMethod() {
    return disbursementMethod;
  }
  /**
   * @param disbursementMethod The disbursementMethod to set.
   */
  public void setDisbursementMethod(String disbursementMethod) {
    this.disbursementMethod = disbursementMethod;
  }
  /**
   * @return Returns the dueDate.
   */
  public String getDueDate() {
    /*
     if (null==dueDate || "".equals(dueDate)){
     dueDate="01/05/2005";
     }*/
    return dueDate;
  }
  /**
   * @param dueDate The dueDate to set.
   */
  public void setDueDate(String dueDate) {
    this.dueDate = dueDate;
  }
  /**
   * @return Returns the maturityInterest.
   */
  public BigDecimal getMaturityInterest() {
    return maturityInterest;
  }
  /**
   * @param maturityInterest The maturityInterest to set.
   */
  public void setMaturityInterest(BigDecimal maturityInterest) {
    this.maturityInterest = maturityInterest;
  }
  /**
   * @return Returns the netMaturiryAmount.
   */
  public BigDecimal getNetMaturiryAmount() {
    return netMaturiryAmount;
  }
  /**
   * @param netMaturiryAmount The netMaturiryAmount to set.
   */
  public void setNetMaturiryAmount(BigDecimal netMaturiryAmount) {
    this.netMaturiryAmount = netMaturiryAmount;
  }
  /**
   * @return Returns the revisedMaturityAmount.
   */
  public BigDecimal getRevisedMaturityAmount() {
    return revisedMaturityAmount;
  }
  /**
   * @param revisedMaturityAmount The revisedMaturityAmount to set.
   */
  public void setRevisedMaturityAmount(BigDecimal revisedMaturityAmount) {
    this.revisedMaturityAmount = revisedMaturityAmount;
  }
  /**
   * @return Returns the withHoldingTax.
   */
  public BigDecimal getWithHoldingTax() {
    return withHoldingTax;
  }
  /**
   * @param withHoldingTax The withHoldingTax to set.
   */
  public void setWithHoldingTax(BigDecimal withHoldingTax) {
    this.withHoldingTax = withHoldingTax;
  }

  /**
   * @return Returns the batchNumber.
   */
  public String getBatchNumber() {
    return batchNumber;
  }
  /**
   * @param batchNumber The batchNumber to set.
   */
  public void setBatchNumber(String batchNumber) {
    this.batchNumber = batchNumber;
  }
  /**
   * @return Returns the caseNumber.
   */
  public String getCaseNumber() {
    return caseNumber;
  }
  /**
   * @param caseNumber The caseNumber to set.
   */
  public void setCaseNumber(String caseNumber) {
    this.caseNumber = caseNumber;
  }
  /**
   * @return Returns the idNumber.
   */
  public String getIdNumber() {
    return idNumber;
  }
  /**
   * @param idNumber The idNumber to set.
   */
  public void setIdNumber(String idNumber) {
    this.idNumber = idNumber;
  }
  /**
   * @return Returns the lifeAssured.
   */
  public String getLifeAssured() {
    return lifeAssured;
  }
  /**
   * @param lifeAssured The lifeAssured to set.
   */
  public void setLifeAssured(String lifeAssured) {
    this.lifeAssured = lifeAssured;
  }
  /**
   * @return Returns the policyNumber.
   */
  public String getPolicyNumber() {
    return policyNumber;
  }
  /**
   * @param policyNumber The policyNumber to set.
   */
  public void setPolicyNumber(String policyNumber) {
    this.policyNumber = policyNumber;
  }

  /**
   * @return Returns the actionType.
   */
  public String getActionType() {
    return actionType;
  }
  /**
   * @param actionType The actionType to set.
   */
  public void setActionType(String actionType) {
    this.actionType = actionType;
  }

  /**
   * @return Returns the policyHolder.
   */
  public String getPolicyHolder() {
    return policyHolder;
  }
  /**
   * @param policyHolder The policyHolder to set.
   */
  public void setPolicyHolder(String policyHolder) {
    this.policyHolder = policyHolder;
  }

  /**
   * @return Returns the payee.
   */
  public String getPayee() {
    return payee;
  }
  /**
   * @param payee The payee to set.
   */
  public void setPayee(String payee) {
    this.payee = payee;
  }
}
