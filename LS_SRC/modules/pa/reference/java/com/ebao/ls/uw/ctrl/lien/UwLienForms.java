package com.ebao.ls.uw.ctrl.lien;

import com.ebao.pub.framework.GenericForm;

/**
 * <p>Title: GEL-UW</p>
 * <p>Description: edit lien information Form</p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: eBaoTech</p>
 * @author yixing.lu
 * @version 1.0
 * @since 09.08.2004
 */

public class UwLienForms extends GenericForm {

  private java.lang.Long underwriteId;

  private java.lang.Long itemId;

  private java.lang.String[] lienType;

  private java.lang.Integer[] emValue;

  private java.lang.Integer[] lienYear;

  private java.lang.Long[] uwListId;

  private java.lang.Double[] reduceRate;

  private java.lang.Double[] reducedAmount;

  private String[] reduceEnd;

  private String[] reduceStart;

  private String applyCode;

  private String policyCode = null;

  private java.math.BigDecimal amount = null;

  private java.util.Date commencementDate = null;

  private Integer chargeYear = null;

  private Integer converYear = null;

  private Integer currPolicyYear = null;

  private java.lang.Integer productId = null;

  private String[] listId;
  private String[] isOn;

  /**
   * returns the value of the underwriteId
   *
   * @return the underwriteId
   */
  public java.lang.Long getUnderwriteId() {
    return underwriteId;
  }

  /**
   * sets the value of the underwriteId
   *
   * @param underwriteId the underwriteId
   */
  public void setUnderwriteId(java.lang.Long underwriteId) {
    this.underwriteId = underwriteId;
  }

  /**
   * returns the value of the itemId
   *
   * @return the itemId
   */
  public java.lang.Long getItemId() {
    return itemId;
  }

  /**
   * sets the value of the itemId
   *
   * @param itemId the itemId
   */
  public void setItemId(java.lang.Long itemId) {
    this.itemId = itemId;
  }

  /**
   * returns the value of the reduceStart
   *
   * @return the reduceStart
   */
  public String[] getReduceStart() {
    return reduceStart;
  }

  /**
   * sets the value of the reduceStart
   *
   * @param reduceStart the reduceStart
   */
  public void setReduceStart(String[] reduceStart) {
    this.reduceStart = reduceStart;
  }

  /**
   * returns the value of the reduceEnd
   *
   * @return the reduceEnd
   */
  public String[] getReduceEnd() {
    return reduceEnd;
  }

  /**
   * sets the value of the reduceEnd
   *
   * @param reduceEnd the reduceEnd
   */
  public void setReduceEnd(String[] reduceEnd) {
    this.reduceEnd = reduceEnd;
  }

  /** returns the value of the lienType
   *
   * @return the lienType
   */
  public java.lang.String[] getLienType() {
    return lienType;
  }

  /**
   * sets the value of the lienType
   *
   * @param lienType the lienType
   */
  public void setLienType(java.lang.String[] lienType) {
    this.lienType = lienType;
  }

  /**
   * returns the value of the emValue
   *
   * @return the emValue
   */
  public java.lang.Integer[] getEmValue() {
    return emValue;
  }

  /**
   * sets the value of the emValue
   *
   * @param emValue the emValue
   */
  public void setEmValue(java.lang.Integer[] emValue) {
    this.emValue = emValue;
  }

  /**
   * returns the value of the uwListId
   *
   * @return the uwListId
   */
  public java.lang.Long[] getUwListId() {
    return uwListId;
  }

  /**
   * sets the value of the uwListId
   *
   * @param uwListId the uwListId
   */
  public void setUwListId(java.lang.Long[] uwListId) {
    this.uwListId = uwListId;
  }

  /**
   * returns the value of the applyCode
   *
   * @return the applyCode
   */

  public String getApplyCode() {
    return applyCode;
  }

  public void setApplyCode(String applyCode) {
    this.applyCode = applyCode;
  }

  /**
   * returns the value of the policyCode
   *
   * @return the policyCode
   */

  public String getPolicyCode() {
    return policyCode;
  }

  public void setPolicyCode(String policyCode) {
    this.policyCode = policyCode;
  }

  /**
   * returns the value of the converYear
   *
   * @return the converYear
   */

  public Integer getConverYear() {
    return converYear;
  }

  public void setConverYear(Integer converYear) {
    this.converYear = converYear;
  }

  /**
   * returns the value of the amount
   *
   * @return the amount
   */

  public java.math.BigDecimal getAmount() {
    return amount;
  }

  public void setAmount(java.math.BigDecimal amount) {
    this.amount = amount;
  }

  /**
   * returns the value of the chargeYear
   *
   * @return the chargeYear
   */

  public Integer getChargeYear() {
    return chargeYear;
  }

  public void setChargeYear(Integer chargeYear) {
    this.chargeYear = chargeYear;
  }

  /**
   * returns the value of the commencementDate
   *
   * @return the commencementDate
   */

  public java.util.Date getCommencementDate() {
    return commencementDate;
  }

  public void setCommencementDate(java.util.Date commencementDate) {
    this.commencementDate = commencementDate;
  }

  /**
   * returns the value of the currPolicyYear
   *
   * @return the currPolicyYear
   */

  public Integer getCurrPolicyYear() {
    return currPolicyYear;
  }

  public void setCurrPolicyYear(Integer currPolicyYear) {
    this.currPolicyYear = currPolicyYear;
  }

  /**
   * returns the value of the productId
   *
   * @return the productId
   */
  public java.lang.Integer getProductId() {
    return productId;
  }

  /**
   * sets the value of the productId
   *
   * @param productId the productId
   */
  public void setProductId(java.lang.Integer productId) {
    this.productId = productId;
  }

  /**
   * returns the value of the lienYear
   *
   * @return the lienYear
   */

  public Integer[] getLienYear() {
    return lienYear;
  }

  public void setLienYear(Integer[] lienYear) {
    this.lienYear = lienYear;
  }

  /**
   * returns the value of the reduceRate
   *
   * @return the reduceRate
   */

  public Double[] getReduceRate() {
    return reduceRate;
  }

  public void setReduceRate(Double[] reduceRate) {
    this.reduceRate = reduceRate;
  }

  /**
   * returns the value of the reducedAmount
   *
   * @return the reducedAmount
   */

  public Double[] getReducedAmount() {
    return reducedAmount;
  }

  public void setReducedAmount(Double[] reducedAmount) {
    this.reducedAmount = reducedAmount;
  }

  /**
   * returns the value of the listId
   *
   * @return the listId
   */

  public String[] getListId() {
    return listId;
  }

  public void setListId(String[] listId) {
    this.listId = listId;
  }

  // End Additional Business Methods

  /**
   * @return Returns the isOn.
   */
  public String[] getIsOn() {
    return isOn;
  }
  /**
   * @param isOn The isOn to set.
   */
  public void setIsOn(String[] isOn) {
    this.isOn = isOn;
  }
}