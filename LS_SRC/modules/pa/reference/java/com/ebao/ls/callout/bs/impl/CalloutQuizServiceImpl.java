package com.ebao.ls.callout.bs.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;

import com.ebao.foundation.module.util.beanutils.BeanUtils;
import com.ebao.ls.callout.bs.CalloutQuizService;
import com.ebao.ls.callout.bs.CalloutTransService;
import com.ebao.ls.callout.data.CalloutQuizDao;
import com.ebao.ls.callout.data.bo.CalloutQuiz;
import com.ebao.ls.callout.vo.CalloutQuizVO;
import com.ebao.ls.callout.vo.CalloutTransVO;
import com.ebao.ls.foundation.service.GenericServiceImpl;
import com.ebao.ls.pa.nb.util.Cst;
import com.ebao.ls.pub.cst.CalloutQuizCategory;
import com.ebao.ls.uw.ds.UwElderCareService;
import com.ebao.ls.uw.vo.UwElderCareDtlVO;
import com.ebao.ls.uw.vo.UwElderCareTypeVO;
import com.ebao.ls.uw.vo.UwElderCareVO;
import com.ebao.pub.util.StringUtils;

public class CalloutQuizServiceImpl extends GenericServiceImpl<CalloutQuizVO, CalloutQuiz, CalloutQuizDao>
		implements CalloutQuizService {

	@Resource(name = CalloutTransService.BEAN_DEFAULT)
	CalloutTransService calloutTransService;
	@Resource(name = UwElderCareService.BEAN_DEFAULT)
	private UwElderCareService uwElderCareService;
	
	@Override
	protected CalloutQuizVO newEntityVO() {
		return new CalloutQuizVO();
	}

	@Override
	public List<CalloutQuizVO> findByCalloutNum(String calloutNum) throws Exception {
		return convertToVOList(dao.findByCalloutNum(calloutNum));
	}

	@Override
	public List<CalloutQuizVO> findByCalloutNum(String calloutNum, Integer category) throws Exception {

		return convertToVOList(dao.findByCalloutNumAndCategory(calloutNum, category));
	}

	// 處理電訪高齡關懷提問寫到高齡投保核保評估相關表
	@Override
	public boolean writeUwElderCare(List<com.ebao.ls.ws.vo.cmn.cmn190.CalloutQuiz> volist) throws Exception {
		if(CollectionUtils.isEmpty(volist)) {
			return Boolean.TRUE;
		}
		//判斷是否有電訪高齡關懷提問
		String calloutNum = "";
		List<UwElderCareDtlVO> elderCareDtl = new ArrayList<UwElderCareDtlVO>();
		for (int i = 0; i < volist.size(); ++i) {
			com.ebao.ls.ws.vo.cmn.cmn190.CalloutQuiz newVO = volist.get(i);
			
			if(StringUtils.isNullOrEmpty(newVO.getCategory())) {
				continue;
			}
			Integer newCategory = Integer.valueOf(newVO.getCategory());
			//有電訪高齡關懷提問
			if(null != newCategory && CalloutQuizCategory.ELDER_QUIZ.equals(newCategory)) {
				calloutNum = newVO.getCalloutNum();
				UwElderCareDtlVO uwElderCareDtlVO = new UwElderCareDtlVO();
				uwElderCareDtlVO.setDisplayOrder(Long.valueOf(elderCareDtl.size()+1));
				uwElderCareDtlVO.setQuestionContent(newVO.getQuestionContent());
				uwElderCareDtlVO.setAnswerContent(newVO.getAnswerContent());
				elderCareDtl.add(uwElderCareDtlVO);
			}
		}
		if(!StringUtils.isNullOrEmpty(calloutNum) && !CollectionUtils.isEmpty(elderCareDtl)) {
			CalloutTransVO vo = calloutTransService
					.findCalloutTransVOByCalloutNum(calloutNum);
			if(null != vo) {
				// Set Type
				List<UwElderCareTypeVO> uwElderCareTypeVOs = new ArrayList<UwElderCareTypeVO>(); 
				UwElderCareTypeVO uwElderCareTypeVO = new UwElderCareTypeVO();
				uwElderCareTypeVO.setCareTaskNo(calloutNum);
				uwElderCareTypeVO.setCareTaskDate(vo.getCalloutTime());
				uwElderCareTypeVO.setCareType(Cst.UW_ELDER_CARE_TYPE_CALLOUT);
				uwElderCareTypeVO.setUwElderCareDtlVOs(elderCareDtl);
				uwElderCareTypeVOs.add(uwElderCareTypeVO);
				// Set Main   
			   	UwElderCareVO uwElderCareVO = new UwElderCareVO();
				uwElderCareVO.setCertiCode(vo.getCalleeCertiCode());
				uwElderCareVO.setPolicyId(vo.getPolicyId());
				uwElderCareVO.setUwElderCareTypeVOs(uwElderCareTypeVOs);
				return uwElderCareService.writeUwElderCare(uwElderCareVO);
			}
		}
		return Boolean.TRUE;
	}
	
	@Override
	public void updateOrInsertVOList(List<com.ebao.ls.ws.vo.cmn.cmn190.CalloutQuiz> volist, String uwSourceType) throws Exception {
		String calloutNum = "";
		Integer category = CalloutQuizCategory.ELDER_QUIZ;
		List<CalloutQuizVO> oldQuizVOList = new ArrayList<CalloutQuizVO>();
		//最後更新或新增的calloutQuiz
		//List<CalloutQuizVO> calloutQuizLst = new ArrayList<CalloutQuizVO>();
		Map<String, Integer> seqMap = new HashMap<String, Integer>();
		
		//com.ebao.ls.ws.vo.cmn.cmn190.CalloutQuiz 
		if(!CollectionUtils.isEmpty(volist)) {
			for (int i = 0; i < volist.size(); i++ ) {
				
				com.ebao.ls.ws.vo.cmn.cmn190.CalloutQuiz newVO = volist.get(i);
				if(newVO == null) {
					continue;
				}
				String newCalloutNum = newVO.getCalloutNum();
				if(StringUtils.isNullOrEmpty(newVO.getCategory())) {
					continue;
				}
				Integer newCategory = Integer.valueOf(newVO.getCategory());
				//找是否有舊資料
				if(!calloutNum.equals(newCalloutNum) || !category.equals(newCategory)) {
					oldQuizVOList = this.findByCalloutNum(newCalloutNum, newCategory);
					calloutNum = newCalloutNum;
					category = newCategory;
				}
				//從MAP取號，初始從0開始，方便從LIST取值
				Integer seq = 0;
				if(seqMap.containsKey(calloutNum + "_" + category.toString())){
					seq = seqMap.get(calloutNum + "_" + category.toString());
				}
				//舊資料存在應從舊資料取出更新
				CalloutQuizVO vo = new CalloutQuizVO();
				if(!CollectionUtils.isEmpty(oldQuizVOList) && oldQuizVOList.size() > seq ) {
					vo = oldQuizVOList.get(seq);
				}
				//更新MAP
				seqMap.put(calloutNum + "_" + category.toString(), seq+1);
				//從接口更新資料
				BeanUtils.copyProperties(vo, newVO);
				vo.setDisplayOrder(seq+1);
				String newUwSourceType = uwSourceType ;
				if(StringUtils.isNullOrEmpty(newUwSourceType)) {
					newUwSourceType = newCalloutNum.contains("UNB") ? "1" : "2";
				}
				vo.setUwSourceType(newUwSourceType);
				this.save(vo);
			}
		}
	}

}
