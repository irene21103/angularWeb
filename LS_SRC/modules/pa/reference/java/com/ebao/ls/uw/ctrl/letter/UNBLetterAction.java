package com.ebao.ls.uw.ctrl.letter;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.UserTransaction;

import org.apache.commons.io.IOUtils;
import org.apache.struts.action.ActionForm;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import com.ebao.foundation.module.util.i18n.StringResource;
import com.ebao.ls.arap.cash.service.IssueReplyListService;
import com.ebao.ls.arap.vo.IssueReplyListVO;
import com.ebao.ls.cmu.bs.template.TemplateService;
import com.ebao.ls.cmu.pub.document.DocumentFormFile;
import com.ebao.ls.cmu.pub.model.DocumentInput.DocumentPropsKey;
import com.ebao.ls.cmu.service.DocumentService;
import com.ebao.ls.cs.common.report.CSReportService;
import com.ebao.ls.letter.common.CommonLetterService;
import com.ebao.ls.letter.common.type.ViewType;
import com.ebao.ls.notification.extension.letter.nb.NbLetterExtensionVO;
import com.ebao.ls.notification.extension.letter.pa.fmtpa0740.FmtPa0740ExtensionVO;
import com.ebao.ls.notification.extension.letter.pa.index.FmtPa0740Index;
import com.ebao.ls.notification.message.WSEmailVO;
import com.ebao.ls.notification.message.WSLetterAddressInfo;
import com.ebao.ls.notification.message.WSLetterImageInfo;
import com.ebao.ls.notification.message.WSLetterUnit;
import com.ebao.ls.notification.message.WSLetterVO;
import com.ebao.ls.notification.message.WSSmsVO;
import com.ebao.ls.pa.nb.ctrl.letter.NbLetterHelper;
import com.ebao.ls.pa.nb.data.bo.DocumentImage;
import com.ebao.ls.pa.nb.letter.AbstractFmtUnbLetter;
import com.ebao.ls.pa.nb.letter.impl.FmtUnb0130LetterImpl;
import com.ebao.ls.pa.nb.letter.impl.FmtUnb02001LetterImpl;
import com.ebao.ls.pa.nb.letter.impl.FmtUnb02002LetterImpl;
import com.ebao.ls.pa.nb.letter.impl.FmtUnb0770LetterImpl;
import com.ebao.ls.pa.pa.letter.impl.FmtPa0740Service;
import com.ebao.ls.pa.pa.letter.impl.FmtPa0740ServiceImpl;
import com.ebao.ls.pa.pub.service.PolicyService;
import com.ebao.ls.ws.util.DateUtils;
import com.ebao.pub.event.EventService;
import com.ebao.pub.framework.ExceptionFactory;
import com.ebao.pub.framework.GenericAction;
import com.ebao.pub.framework.GenericException;
import com.ebao.pub.util.Log;
import com.ebao.pub.util.StringUtils;
import com.ebao.pub.util.Trans;
import com.ebao.pub.util.TransUtils;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

public abstract class UNBLetterAction extends GenericAction implements ApplicationContextAware {
	public static final String ADD = "clearAdd";
	public static final String SAVE = "save";
	public static final String PERVIEW = "perview";
	public static final String ONLINE_PRINT = "online";
	public static final String UPLOAD_FILE = "uploadFile";
	public static final String DELETE_FILE = "deleteFile";
	
	public static final String FORWARD_WINDOW_CLOSE = "window_close";

	protected final static String BATCH_PRINT__SUCCESS = "MSG_1257540";

    protected final static String ONLINE_PRINT__SUCCESS = "MSG_1257541";
	
	protected ApplicationContext applicationContext;
	protected java.util.Map<Long,String> letterMap = new java.util.HashMap<Long,String>();
	protected java.util.Map<Long,String> paLetterMap = new java.util.HashMap<Long,String>();
	
	public UNBLetterAction(){
	    letterMap.put(FmtUnb0130LetterImpl.TEMPLATE_ID, FmtUnb0130LetterImpl.BEAN_DEFAULT);
	    letterMap.put(FmtUnb02001LetterImpl.TEMPLATE_ID, FmtUnb02001LetterImpl.BEAN_DEFAULT);
	    letterMap.put(FmtUnb02002LetterImpl.TEMPLATE_ID,  FmtUnb02002LetterImpl.BEAN_DEFAULT);
	    letterMap.put(FmtUnb0770LetterImpl.TEMPLATE_ID, FmtUnb0770LetterImpl.BEAN_DEFAULT);
	    paLetterMap.put(FmtPa0740Service.TEMPLATE_ID, FmtPa0740Service.BEAN_DEFAULT);
	}
	
	@Resource(name = EventService.BEAN_DEFAULT)
	private EventService eventService;
	@Resource(name = CSReportService.BEAN_DEFAULT)
	private CSReportService cSReportService;
	
	

	protected abstract NbLetterExtensionVO getExtension(ActionForm unbLetterForm);
	
	protected void saveNewLetter(HttpServletRequest request, HttpServletResponse response, 
					UNBLetterForm unbLetterForm,
					List<DocumentFormFile> uploadFileList)
	                throws Exception {
		UserTransaction trans = Trans.getUserTransaction();
		Long documentId = null;
		try {
			trans.begin();
			String actionType = unbLetterForm.getActionType();
			
			Long templateId = Long.valueOf(String.valueOf(unbLetterForm.getTemplateId()));
			Long policyId = null;
			String policyNo = unbLetterForm.getPolicyNo();
			FmtPa0740Index replyList = null;
			FmtPa0740ExtensionVO extensionVO = null;
			if (unbLetterForm.getPolicyId() != null) {
			    policyId = unbLetterForm.getPolicyId();
			    if ( policyNo == null ) {
			    	policyNo = policyService.load(policyId).getPolicyNumber();
			    	unbLetterForm.setPolicyNo(policyNo);
			    }
			}
			else {
	            if (StringUtils.isNullOrEmpty(policyNo)) {
	            	throw new Exception("Policy Code is empty");
	            }
	            //get policyId
	            policyId = policyService.getPolicyIdByPolicyNumber(policyNo);
	            unbLetterForm.setPolicyId(policyId);
			}
			// Create WSLetterUnit
			NbLetterExtensionVO extension = getExtension(unbLetterForm);
            WSLetterUnit unit = nbLetterHelper.generalLetterUnit(   response,
            				                                        policyId, 
                                                                    templateId, 
                                                                    uploadFileList,
                                                                    unbLetterForm.getApplyImageList(),
                                                                 extension);
            
            if(letterMap.containsKey(templateId)){
                AbstractFmtUnbLetter letter =  this.applicationContext.getBean(letterMap.get(templateId), AbstractFmtUnbLetter.class);
                letter.customize(templateId, policyId, extension.getFileNames(), unbLetterForm.getDocumentContent());
                unit.setExtension(letter.getExtensionVO());
                unit.setIndex(letter.getIndexVO());
                
                //通路寄送規則,採Email寄送
                NbLetterHelper.unbSendingMethod(unit);
            }
            if(paLetterMap.containsKey(templateId)){
            	FmtPa0740ServiceImpl letter =  this.applicationContext.getBean(paLetterMap.get(templateId), FmtPa0740ServiceImpl.class);
            	List<Map<String, Object>> list = letter.queryPolicyObject(policyId);            	
        		Map<String, Object> policyMap = list.get(0);      
        		FmtPa0740ExtensionVO paExtension = letter.queryFmtPa0740(policyId, unbLetterForm);
        		//PCR 157982 新增照會項目
        		paExtension.setDocumentContent(unbLetterForm.getDocumentContent());
        		//
        		FmtPa0740Index index = letter.getIndexVO(policyId, policyMap, templateId, paExtension, unbLetterForm);
        		cSReportService.initSpecialFirstIndex(index, policyId);
        		unit.setExtension(paExtension);  
        		unit.setIndex(index);
        		replyList = index;
        		extensionVO = paExtension;
            }
            // Send Letter or Preview
            if (PERVIEW.equals(actionType)) {
            	previewLetter(request, response, templateId, unit);
            } else {
            		documentId = doSendLetter(actionType, templateId, unit, policyId, policyNo, unbLetterForm , uploadFileList);
            	if(extensionVO != null) {
            		//PCR 262151 新增照會回銷表
            		this.setIssueReplyList(unit,unbLetterForm,policyId,replyList,extensionVO);
            	}
            }
            trans.commit();
            
            
		} catch (Exception e) {
			com.ebao.pub.util.Log.error(this.getClass(), e.toString());
			TransUtils.rollback(trans);
			Log.error(this.getClass(), e);
			throw ExceptionFactory.parse(e);
		}
		if (documentId != null ) {
            unbLetterForm.setDocumentNo(documentService.load(documentId).getDocumentNo());                    
            unbLetterForm.setDocumentId(documentId);
        }
		
	}
	
	
	protected void previewLetter(HttpServletRequest request, HttpServletResponse response, Long tempId, WSLetterUnit... units) throws IOException, Exception {
		OutputStream outStream = null;
		try {
			commonLetterService.previewLetterSetupPDFHeader(request, response,tempId, units);
		} finally {
			if (outStream != null) {
				try {
					outStream.flush();
					outStream.close();
				} catch (IOException e) {
					throw ExceptionFactory.parse(e);
				}
			}
		}
	}

	/**
	 * 呼叫 commonLetterService
	 * 預覽 (previewLetter) / 儲存 (sendBatchLetter) / 線上列印 (sendOnlineLetter)
	 * 
	 * 回傳 docuemntId  (預覧回傳 null)
	 * @param outputStream 
	 */
	private Long doSendLetter(String actionType, Long templateId, WSLetterUnit unit, Long policyId, String policyCode,  UNBLetterForm unbLetterForm ,List<DocumentFormFile> uploadFileList ) throws Exception {
	    
	    Long documentId = null;
	    if (SAVE.equals(actionType)) {
	    	if ( unbLetterForm.getDocumentId() > 0L) {  //更新
	    		documentId = unbLetterForm.getDocumentId();
	    		commonLetterService.updateLetterContent(documentId, unit);
	    		
	    	} else {//新增
	            
	    		documentId = commonLetterService.sendBatchLetter(   policyId, 
                                                                templateId, 
                                                                unit, 
                                                                policyId, 
                                                                policyCode,
                                                                new HashMap<DocumentPropsKey, Object>());
	    	}
            
	    	if( documentId != null) {
	    		
	    		String documentNo = Long.toString(documentId);
	    		List<DocumentImage> documentImageList = new ArrayList<DocumentImage>();
		    	
		     	for( DocumentFormFile documentFormFile : uploadFileList ) {
		     		
		    		DocumentImage documentImage = new DocumentImage();
		    		
		    		documentImage.setDocumentNo(documentNo);
		    		documentImage.setFileName(documentFormFile.getFileName());
		    		documentImage.setArchivePath(documentFormFile.getArchivePath());
		    		documentImage.setImageName(documentFormFile.getImageName());
		    		documentImage.setOrderNum(documentFormFile.getOrderNum());
		    		documentImage.setImageIds(documentFormFile.getImageIds());
		    		
		    		documentImageList.add(documentImage);

		    	}
		    	
		     	//存進T_DOCUMENT_IMAGE
		    	commonLetterService.saveDocumentImage(documentNo, documentImageList);
		    	
		    	//存進T_DOCUMENT_IMAGE後，再移除保單影像清單資訊
		     	for (Iterator<DocumentFormFile> iterator = uploadFileList.iterator(); iterator.hasNext(); ) {
		     		DocumentFormFile documentFormFile = iterator.next();
		     		if( documentFormFile.getImageIds() != null ) {
		     			iterator.remove();
		     		}
		     	}
	    	}

        } 
        else if (ONLINE_PRINT.equals(actionType)) {
        	if ( unbLetterForm.getDocumentId() > 0L) {
	    		documentId = unbLetterForm.getDocumentId();
	    		commonLetterService.updateLetterContent(documentId, unit);
	    		/**IR208721 儲存後再按online發送會有問題, 因為原本只把B改為O, 並沒有規檔, 修改為A, 讓下游歸檔, 發送方式一致性Online列印也改為Adhot發送**/
	    		commonLetterService.sendGroupLetter(documentId,null);
	    	} else {
	    		documentId = commonLetterService.sendLetter(policyId, 
                                                                Long.valueOf(templateId), 
                                                                unit, 
                                                                policyId, 
                                                                policyCode,
                                                                new HashMap<DocumentPropsKey, Object>());
	    		    
	    		
	    	}
        }
	    
	    return documentId;
	}

	final static XStream xStream = new XStream(new DomDriver("utf-8"));
	
	private static String convertToXML(Object obj) {
        ByteArrayOutputStream buff = new ByteArrayOutputStream();
        try {
            // 讓 xml 的 tag 內容不要出現 package 名稱
            xStream.alias("WSSmsVO", WSSmsVO.class);
            xStream.alias("WSEmailVO", WSEmailVO.class);
            xStream.alias("WSEmailVO", WSEmailVO.class);
            xStream.alias("WSLetterAddressInfo", WSLetterAddressInfo.class);
            xStream.alias("WSLetterImageInfo", WSLetterImageInfo.class);
            xStream.alias("WSLetterUnit", WSLetterUnit.class);
            xStream.alias("WSLetterVO", WSLetterVO.class);
            
            xStream.toXML(obj, buff);
            
            buff.flush();
            return new String(buff.toByteArray(), "utf-8");
        } catch (IOException e) {
            throw ExceptionFactory.parse(e);
        } finally {
            IOUtils.closeQuietly(buff);
        }
    }
	
	// for 預覧測試時用而已
    private WSLetterVO prepareWSLetterVO(ViewType viewType) {

        WSLetterVO vo = new WSLetterVO();
        // P: 預覽  A:歸檔  R:重送
        vo.setViewType(viewType.getCode());
        
        return vo;
    }
    
    //PCR 262151 新增照會回銷表
	private void setIssueReplyList(WSLetterUnit unit, UNBLetterForm unbLetterForm, Long policyId, FmtPa0740Index index, FmtPa0740ExtensionVO extensionVO) throws ParseException {
		IssueReplyListVO issueReplyListVO = new IssueReplyListVO();
		try {
			issueReplyListVO.setDocumentId(unit.getDocumentId());
			issueReplyListVO.setPolicyCode(unbLetterForm.getPolicyNo());
			issueReplyListVO.setPolicyId(policyId);
//			issueReplyListVO.setRcptDept(paExtension.getChannelName());
//			Date date = null;
//			DateFormat fmt =new SimpleDateFormat("yyyy-MM-dd");
//			date = fmt.parse(unbLetterForm.getCreateDate());
			issueReplyListVO.setAgent(extensionVO.getAgent());
			issueReplyListVO.setNoticeDate(DateUtils.parse(unbLetterForm.getCreateDate(), "yyyy-MM-dd"));
			issueReplyListVO.setReplyDate(DateUtils.parse(unbLetterForm.getDonePatchDate(), "yyyy-MM-dd"));
			issueReplyListVO.setCetiReason(checkCetiReason(unbLetterForm));
			issueReplyListVO.setDocumentContent(unbLetterForm.getDocumentContent());
			issueReplyListService.insert(issueReplyListVO);
		} catch (GenericException e) {
			e.printStackTrace();
		}
	}
	
	//PCR 262151 新增照會回銷表
	private String checkCetiReason(UNBLetterForm unbLetterForm) {
		List<String> cetiReasonNumber = new ArrayList<String>();
			try {
//				if (!"".equals(unbLetterForm.getCetiReason1())&&unbLetterForm.getCetiReason1() != null) {
				if (!StringUtils.isNullOrEmpty(unbLetterForm.getCetiReason1())) {
					cetiReasonNumber.add(StringResource.getStringData("MSG_1265541","311"));
				}
				if (!StringUtils.isNullOrEmpty(unbLetterForm.getCetiReason2())) {
					cetiReasonNumber.add(StringResource.getStringData("MSG_1265542","311"));
				}
				if (!StringUtils.isNullOrEmpty(unbLetterForm.getCetiReason3())) {
					cetiReasonNumber.add(StringResource.getStringData("MSG_1265543","311"));
				}
				if (!StringUtils.isNullOrEmpty(unbLetterForm.getCetiReason4())) {
					cetiReasonNumber.add(StringResource.getStringData("MSG_1265544","311"));
				}
				if (!StringUtils.isNullOrEmpty(unbLetterForm.getCetiReason5())) {
					cetiReasonNumber.add(StringResource.getStringData("MSG_1265545","311"));
				}
				if (!StringUtils.isNullOrEmpty(unbLetterForm.getCetiReason7())) {
					cetiReasonNumber.add(StringResource.getStringData("MSG_1265546","311"));
				}
				if (!StringUtils.isNullOrEmpty(unbLetterForm.getCetiReason8())) {
					cetiReasonNumber.add(StringResource.getStringData("MSG_1265565","311"));
				}
				if (!StringUtils.isNullOrEmpty(unbLetterForm.getCetiReason6())) {
					cetiReasonNumber.add(unbLetterForm.getDocumentContent());
				} 
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			String str = "";
			if(!"".equals(cetiReasonNumber)) {
				str = cetiReasonNumber.toString();
			}
			
		return str;
	}
    
    @Override
    public void setApplicationContext(ApplicationContext applicationContext)
                    throws BeansException {
        this.applicationContext = applicationContext;
    }
    
	@Resource(name = PolicyService.BEAN_DEFAULT)
    private PolicyService policyService;
	
	@Resource(name = NbLetterHelper.BEAN_DEFAULT)
    protected NbLetterHelper nbLetterHelper;
	
	@Resource(name=CommonLetterService.BEAN_DEFAULT)
	protected CommonLetterService commonLetterService;
	
	@Resource(name=DocumentService.BEAN_DEFAULT)
	protected DocumentService documentService;
	
	@Resource(name=TemplateService.BEAN_DEFAULT)
	protected TemplateService templateService;
	
	@Resource(name=IssueReplyListService.BEAN_DEFAULT)
	protected IssueReplyListService issueReplyListService;
	
}
