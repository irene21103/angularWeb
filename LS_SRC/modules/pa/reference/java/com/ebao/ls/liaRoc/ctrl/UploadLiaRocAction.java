package com.ebao.ls.liaRoc.ctrl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.UserTransaction;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.ClassUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.jbpm.util.IoUtil;

import com.ebao.foundation.module.db.hibernate.HibernateSession3;
import com.ebao.foundation.module.util.i18n.StringResource;
import com.ebao.foundation.module.web.action.ExceptionInfoUtils;
import com.ebao.ls.batch.file.filehelper.FileGenericHelper;
import com.ebao.ls.cs.commonflow.data.TCsApplicationDelegate;
import com.ebao.ls.cs.commonflow.data.bo.Application;
import com.ebao.ls.cs.commonflow.ds.AlterationItemService;
import com.ebao.ls.cs.commonflow.ds.ApplicationService;
import com.ebao.ls.cs.commonflow.vo.AlterationItemVO;
import com.ebao.ls.cs.logger.ApplicationLogger;
import com.ebao.ls.cs.logger.helper.AppLogDataHelper;
import com.ebao.ls.cs.pub.ds.CSLiaRocUploadService;
import com.ebao.ls.liaRoc.LiaRocCst;
import com.ebao.ls.liaRoc.bo.LiaRocUploadDetail;
import com.ebao.ls.liaRoc.bo.LiaRocUploadDetail2020VO;
import com.ebao.ls.liaRoc.bo.LiaRocUploadDetailVO;
import com.ebao.ls.liaRoc.bo.LiaRocUploadVO;
import com.ebao.ls.liaRoc.ci.LiaRocUpload2019CI;
import com.ebao.ls.liaRoc.ci.LiaRocUpload2020CI;
import com.ebao.ls.liaRoc.ci.LiaRocUploadCommonService;
import com.ebao.ls.liaRoc.data.LiaRocUploadDetailDao;
import com.ebao.ls.liaRoc.vo.LiaRocUploadDetail2020WrapperVO;
import com.ebao.ls.liaRoc.vo.LiaRocUploadDetailWrapperVO;
import com.ebao.ls.liaRoc.vo.LiaRocUploadSendVO;
import com.ebao.ls.pa.nb.util.NBUtils;
import com.ebao.ls.pa.pub.bs.coverage.CoverageService;
import com.ebao.ls.pa.pub.service.PolicyService;
import com.ebao.ls.pa.pub.vo.CoverageVO;
import com.ebao.ls.pa.pub.vo.PolicyVO;
import com.ebao.ls.pty.ci.DeptCI;
import com.ebao.ls.pub.CodeCst;
import com.ebao.ls.pub.cst.CsAppStatus;
import com.ebao.ls.pub.cst.Service;
import com.ebao.ls.pub.util.TGLDateUtil;
import com.ebao.pub.fileresolver.writer.FixedCharLengthWriter;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.framework.GenericAction;
import com.ebao.pub.util.BeanUtils;
import com.ebao.pub.util.DateUtils;
import com.ebao.pub.util.EnvUtils;
import com.ebao.pub.util.Trans;
import com.ebao.pub.util.TransUtils;
import com.tgl.tools.ListTool;

/**
 * <h1>單筆上傳公會通報</h1><p>
 * @since 2016/03/24<p>
 * @author Amy Hung
 * <p>Update Time: </p>
 * <p>Updater: </p>
 * <p>Update Comments: </p>
 * <p>
 * TODO:
 * 本程式可以做收件, 承保, 107通報
 * 目前只完成新契約的收件、承保通報，保全及理賠的部份待其它模組完成
 * 
 * </p>
*/
public class UploadLiaRocAction extends GenericAction {

	@Resource(name = DeptCI.BEAN_DEFAULT)
	private DeptCI deptCI;

	@Resource(name = PolicyService.BEAN_DEFAULT)
	private PolicyService policyService;

	@Resource(name = LiaRocUpload2019CI.BEAN_DEFAULT)
	private LiaRocUpload2019CI liaRocUpload2019CI;

	@Resource(name = LiaRocUpload2020CI.BEAN_DEFAULT)
	private LiaRocUpload2020CI liaRocUpload2020CI;

	@Resource(name = LiaRocUploadCommonService.BEAN_DEFAULT)
	private LiaRocUploadCommonService liaRocUploadCommonService;	
	
	@Resource(name = CSLiaRocUploadService.BEAN_DEFAULT)
	private CSLiaRocUploadService csLiaRocUploadService;

	@Resource(name = ApplicationService.BEAN_DEFAULT)
	private ApplicationService applicationService;

	/**
	 * 用來將通報明細資料轉換成 txt 檔的 writer
	*/
	@Resource(name = "liaRocUploadRequestWriter")
	private FixedCharLengthWriter fixedLengthWriter;

	@Resource(name = LiaRocUploadDetailDao.BEAN_DEFAULT)
	private LiaRocUploadDetailDao liaRocUploadDetailDao;
	
	@Resource(name = CoverageService.BEAN_DEFAULT)
    protected CoverageService coverageService;
	
	@Resource(name = AlterationItemService.BEAN_DEFAULT)
    private AlterationItemService alterationItemDS;
	
	@Override
	public ActionForward process(ActionMapping mapping, ActionForm form, HttpServletRequest request,
					HttpServletResponse response) throws Exception {
		
		if (form instanceof UploadLiaRocForm) {
			UploadLiaRocForm oForm = (UploadLiaRocForm) form;
			if (StringUtils.equals("upload", oForm.getActionType())) {

				try {
					if (this.upload(oForm, request)) {
						if (request.getAttribute("sysMsg") == null) {
							request.setAttribute("sysMsg", StringResource.getStringData("MSG_1257703",
									AppContext.getCurrentUser().getLangId()));
						}
					} else {
						request.setAttribute("list", oForm.getUploadLiaRocVO());
					}
				} catch (Exception e) {
					String error = ExceptionInfoUtils.getExceptionMsg(e);
					int len = error.length();
					if (len > 0) {
						ApplicationLogger
								.addLoggerData("[ERROR]Exception => " + error.substring(0, (len > 2000 ? 2000 : len)));
					} else {
						ApplicationLogger.addLoggerData("[ERROR]Exception => " + e);
					}
					ApplicationLogger.flush();
				}

			} else if (StringUtils.equals("add", oForm.getActionType())) {

				Long policyId = vaildateData(oForm, request);
				if (policyId != null) {
					UploadLiaRocVO uploadLiaRocVO = new UploadLiaRocVO();
					uploadLiaRocVO.setPolicyCode(oForm.getPolicyCode());
					uploadLiaRocVO.setLiarocUploadType(oForm.getLiarocUploadType());
					uploadLiaRocVO.setPolicyId(policyId);
					oForm.getUploadLiaRocVO().add(uploadLiaRocVO);
					oForm.setPolicyCode(null);
					oForm.setLiarocUploadType(null);
				}
				request.setAttribute("list", oForm.getUploadLiaRocVO());

			}
			return mapping.findForward("uploadPolicy");
		} else if (form instanceof UploadLiaRocItemForm) {
			UploadLiaRocItemForm oForm = (UploadLiaRocItemForm) form;
			
			if (StringUtils.equals("queryItem", oForm.getActionType())) {
				List<CoverageVO> coverageVOList = vaildateData(oForm, request);
				if (coverageVOList != null && coverageVOList.size()>0) {
					List<UploadLiaRocItemVO> list = new ArrayList<UploadLiaRocItemVO>();
					for (CoverageVO vo : coverageVOList) {
						if (StringUtils.equals(vo.getWaiverExt().getWaiver(), CodeCst.YES_NO__NO)) { // 只處理非豁免險
							UploadLiaRocItemVO item = new UploadLiaRocItemVO();
							item.setPolicyCode(oForm.getPolicyCode());
							item.setItemId(vo.getItemId());
							item.setItemOrder(vo.getItemOrder());
							item.setRiskStatus(vo.getRiskStatus());
							item.setProductId(vo.getProductId());
							item.setProductVersionId(vo.getProductVersionId());
							item.setInceptionDate(TGLDateUtil.format(vo.getInceptionDate()));
							item.setChargeYear(vo.getChargeYear());
							item.setCoverageYear(vo.getCoverageYear());
							item.setExpiryDate(TGLDateUtil.format(vo.getExpiryDate()));
							item.setCoverageVo(vo);
							item.setName(vo.getLifeInsured1().getInsured().getName());
							item.setCertiCode(vo.getLifeInsured1().getInsured().getCertiCode());
							item.setRelationToPh(vo.getLifeInsured1().getInsured().getRelationToPH());
							item.setInsuredCategory(vo.getLifeInsured1().getInsured().getInsuredCategory());

							String amtUnitLvlNumber = "";
							BigDecimal zero = new BigDecimal("0"); 
							BigDecimal sumAssured = null;
							BigDecimal unit = null;
							String benefitLevel = null;
							if(vo.getCurrentPremium() != null) {
								sumAssured = vo.getCurrentPremium().getSumAssured() ;
								unit = vo.getCurrentPremium().getUnit() ;
								benefitLevel = vo.getCurrentPremium().getBenefitLevel() ;
								if (sumAssured != null && sumAssured.compareTo(zero) > 0) {
									amtUnitLvlNumber = sumAssured + NBUtils.getTWMsg("MSG_6998"); 
								} else if(unit != null && unit.compareTo(zero) > 0) {
									amtUnitLvlNumber = unit + NBUtils.getTWMsg("MSG_209418");
								} else if(StringUtils.isNotBlank(benefitLevel)){
									amtUnitLvlNumber = NBUtils.getTWMsg("MSG_RPT_901117061") + benefitLevel;
								}
							}
							item.setAmtUnitLvlNumber(amtUnitLvlNumber);
							item.setChargeMode(vo.getCurrentPremium().getPaymentFreq());
							list.add(item);
						}
					}
					oForm.setUploadLiaRocItemVO(list);
					request.setAttribute("uploadLiaRocItemVO", oForm.getUploadLiaRocItemVO());
				}
				
			} else if (StringUtils.equals("uploadItem", oForm.getActionType())) {
				try {
					if (this.upload(oForm, request)) {
						if (request.getAttribute("sysMsg") == null) {
							request.setAttribute("sysMsg", StringResource.getStringData("MSG_1257703",
									AppContext.getCurrentUser().getLangId()));
						}
					} else {
						request.setAttribute("uploadLiaRocItemVO", oForm.getUploadLiaRocItemVO());
					}
				} catch (Exception e) {
					String error = ExceptionInfoUtils.getExceptionMsg(e);
					int len = error.length();
					if (len > 0) {
						ApplicationLogger
								.addLoggerData("[ERROR]Exception => " + error.substring(0, (len > 2000 ? 2000 : len)));
					} else {
						ApplicationLogger.addLoggerData("[ERROR]Exception => " + e);
					}
					ApplicationLogger.flush();
				}
			}
			return mapping.findForward("uploadItem");
		}
			

		return mapping.findForward("success");

	}

	/**
	 * <p>Description : 檢核輸入的保單號碼及通報類型</p>
	 * <p>Created By : Amy Hung</p>
	 * <p>Create Time : Mar 23, 2016</p>
	 * @param form
	 * @param request
	 * @return PolicyID if validate successful, null if failed.
	 */
	private Long vaildateData(UploadLiaRocForm form, HttpServletRequest request) {

		Long policyId = null;

		// 檢查保單號碼有沒有存在
		policyId = policyService.getPolicyIdByPolicyNumber(form.getPolicyCode());
		if (policyId == null) {
			// 保單號碼不正確
			request.setAttribute("sysMsg",
							StringResource.getStringData("MSG_1257776", AppContext.getCurrentUser().getLangId()));
			return null;
		}

		// 檢查保單號碼/通報類型是否已經存在列表
		for (UploadLiaRocVO vo : form.getUploadLiaRocVO()) {
			if (vo.getPolicyCode().equals(form.getPolicyCode())
							&& vo.getLiarocUploadType().equals(form.getLiarocUploadType())) {
				// 相同的通報資料已存在，請勿重覆輸入
				request.setAttribute("sysMsg",
								StringResource.getStringData("MSG_1257775", AppContext.getCurrentUser().getLangId()));
				return null;
			}
		}

		// 如果為新契約人員
		if (deptCI.getBizCategoryByUserId(AppContext.getCurrentUser().getUserId()) == CodeCst.BIZ_CATEGORY_UNB) {

			PolicyVO policyVO = policyService.loadPolicyByPolicyId(policyId);
			boolean isPass = true;

			// 收件通報的保單需要存在待覆核狀態日
			if (form.getLiarocUploadType().equals(LiaRocCst.LIAROC_UL_TYPE_RECEIVE)) {
				if (policyVO.getProposalStatus() < 20) {
					isPass = false;
				}
			} else if (form.getLiarocUploadType().equals(LiaRocCst.LIAROC_UL_TYPE_INFORCE)) {
				// 承保通報的保單狀態須為生效
				if (CodeCst.LIABILITY_STATUS__IN_FORCE != policyVO.getRiskStatus()) {
					isPass = false;
				}
			}

			if (!isPass) {
				request.setAttribute("sysMsg",
								StringResource.getStringData("MSG_1257702", AppContext.getCurrentUser().getLangId()));
				return null;
			}

		}

		return policyId;

	}
	
	private List<CoverageVO> vaildateData(UploadLiaRocItemForm form, HttpServletRequest request) {
		
		Long policyId = null;

		// 檢查保單號碼有沒有存在
		policyId = policyService.getPolicyIdByPolicyNumber(form.getPolicyCode());
		if (policyId == null) {
			// 保單號碼不正確
			request.setAttribute("sysMsg", StringResource.getStringData("MSG_1257776", AppContext.getCurrentUser().getLangId()));
			return null;
		}

		// 如果為新契約人員
		if (deptCI.getBizCategoryByUserId(AppContext.getCurrentUser().getUserId()) == CodeCst.BIZ_CATEGORY_UNB) {

			PolicyVO policyVO = policyService.loadPolicyByPolicyId(policyId);
			boolean isPass = true;

			// 收件通報的保單需要存在待覆核狀態日
			if (form.getLiarocUploadType().equals(LiaRocCst.LIAROC_UL_TYPE_RECEIVE)) {
				if (policyVO.getProposalStatus() < 20) {
					isPass = false;
				}
			} else if (form.getLiarocUploadType().equals(LiaRocCst.LIAROC_UL_TYPE_INFORCE)) {
				// 承保通報的保單狀態須為生效
				if (CodeCst.LIABILITY_STATUS__IN_FORCE != policyVO.getRiskStatus()) {
					isPass = false;
				}
			}

			if (!isPass) {
				request.setAttribute("sysMsg", StringResource.getStringData("MSG_1257702", AppContext.getCurrentUser().getLangId()));
				return null;
			}
			
			return policyVO.gitSortCoverageList();
		} else if (deptCI.getBizCategoryByUserId(AppContext.getCurrentUser().getUserId()) == CodeCst.BIZ_CATEGORY_POS) {
			PolicyVO policyVO = policyService.loadPolicyByPolicyId(policyId);
			List<Application> applicationList = TCsApplicationDelegate.findByPolicyIdAndCsAppStatus(policyId, CsAppStatus.CS_APP_STATUS__IN_FORCE);
			Long changeId = null;
			List<CoverageVO> coverageVOList = new ArrayList<CoverageVO>();
			if (applicationList != null && applicationList.size() > 0) {
				changeId = applicationList.get(applicationList.size() - 1).getChangeId();
				List<AlterationItemVO> alterationItemList = applicationService.getAlterationItemList(changeId);
				for (AlterationItemVO alterationItemVO : alterationItemList) {
					for (CoverageVO coverageVO : policyVO.gitSortCoverageList()) {
						if (liaRocUpload2019CI.needUploadItem(coverageVO)) { // 判斷商品要不要通報
							String operType = coverageService.getOperTypeInPolicyChange(coverageVO.getItemId(), alterationItemVO.getPolicyChgId());
							if (CodeCst.DATA_OPER_TYPE__DUMP_OPER_ADD.equals(operType)) {
								coverageVOList.add(coverageVO);
							}
						}
					}
				}
			}
			if (coverageVOList.size() == 0) {
				request.setAttribute("sysMsg", StringResource.getStringData("MSG_1250566", AppContext.getCurrentUser().getLangId()));
			}
			return coverageVOList;
		}

		return null;

	}
	

	/**
	 * <p>Description : 上傳公會通報</p>
	 * <p>Created By : Amy Hung</p>
	 * <p>Create Time : Mar 25, 2016</p>
	 * @param form
	 * @param request
	 * @return
	 */
	private boolean upload(UploadLiaRocForm form, HttpServletRequest request) {

		boolean isAllPass = true;
		List<String> failItems = new ArrayList<String>();

        // 如果為新契約人員
        int bizCategory = deptCI.getBizCategoryByUserId(AppContext.getCurrentUser().getUserId());
        NBUtils.logger(this.getClass(), "userId="+ AppContext.getCurrentUser().getUserId() 
                        + ",bizCategory=" + bizCategory);
        
		String bizSource;
		String appSystemCode;
		if (bizCategory == CodeCst.BIZ_CATEGORY_UNB) {
			bizSource = LiaRocCst.LIAROC_UL_BIZ_SOURCE_UNB;
            appSystemCode = AppLogDataHelper.SYSTEM_CODE_UNB;
		} else if (bizCategory == CodeCst.BIZ_CATEGORY_POS) {
			bizSource = LiaRocCst.LIAROC_UL_BIZ_SOURCE_POS;
            appSystemCode = AppLogDataHelper.SYSTEM_CODE_POS;
		} else if (bizCategory == CodeCst.BIZ_CATEGORY_CLM) {
			bizSource = LiaRocCst.LIAROC_UL_BIZ_SOURCE_CLM;
            appSystemCode = AppLogDataHelper.SYSTEM_CODE_CLM;
		} else {
			request.setAttribute("sysMsg", "ERROR USER BIZ CATEGORY !!!");
			return false;
		}
		
		if (form.getLiarocUploadType().equals(LiaRocCst.LIAROC_UL_TYPE_RECEIVE)
						|| form.getLiarocUploadType().equals(LiaRocCst.LIAROC_UL_TYPE_INFORCE)) {

			// 依保單號碼逐一通報
			for (UploadLiaRocVO formUploadVO : form.getUploadLiaRocVO()) {
			    
			    String policyCode = formUploadVO.getPolicyCode();
	            ApplicationLogger.setPolicyCode(policyCode);
                ApplicationLogger.addLoggerData("begin");
                ApplicationLogger.setJobName(ClassUtils.getShortClassName(this.getClass()));
                ApplicationLogger.setOptionId(DateUtils.date2String(new Date(), "yyyyMMdd HHmmss"));
                ApplicationLogger.setSystemCode(appSystemCode);
                
				UserTransaction trans = null;

				List<Long> uploadIdList = new ArrayList<Long>();
				try {
					//增加單筆上傳公會通報的功能。作業人員可以按照單張保單選擇上傳收件通報或承保通報。
					//保全會依最後的保全變更案件, 根據更新後的資料, 重新產生保全變更項的公會通報資料。
					if (LiaRocCst.LIAROC_UL_BIZ_SOURCE_UNB.equals(bizSource)) {
						trans = Trans.getUserTransaction();
						trans.begin();
						
						try {

							//新契約一律通報不作保項比對
		                    LiaRocUploadSendVO sendVO = liaRocUpload2019CI.getUploadVOByPolicyId(formUploadVO.getPolicyId(),
		                    				formUploadVO.getLiarocUploadType(), bizSource);
		                    Long uploadId = liaRocUpload2019CI.saveUpload(sendVO);

							uploadToESP(uploadId);
							uploadIdList.add(uploadId);
							trans.commit();
						} catch(Exception e) {
							TransUtils.rollback(trans);
						}
					
					} else if (LiaRocCst.LIAROC_UL_BIZ_SOURCE_POS.equals(bizSource)) {
						
						NBUtils.logger(this.getClass(), "bizSource="+ bizSource);
						List<Application> applicationList = null;
						
						//保全會依最後的保全變更案件, 根據更新後的資料, 重新產生保全變更項的公會通報資料。
						if (formUploadVO.getLiarocUploadType().equals(LiaRocCst.LIAROC_UL_TYPE_RECEIVE)) {
							applicationList = TCsApplicationDelegate.findByPolicyId(formUploadVO.getPolicyId());
						} else {
							applicationList = TCsApplicationDelegate.findByPolicyIdAndCsAppStatus(
												formUploadVO.getPolicyId(), CsAppStatus.CS_APP_STATUS__IN_FORCE);
						}
						
						NBUtils.logger(this.getClass(), "applicationList="+ applicationList.size());
						
						Long changeId = null;
						if (applicationList != null && applicationList.size() > 0) {
							changeId = applicationList.get(applicationList.size() - 1).getChangeId();

							NBUtils.logger(this.getClass(), "changeId="+ changeId);
							
							trans = Trans.getUserTransaction();
							trans.begin();
							try {
								List<AlterationItemVO> alterationItemList = applicationService.getAlterationItemList(changeId);

								if (formUploadVO.getLiarocUploadType().equals(LiaRocCst.LIAROC_UL_TYPE_RECEIVE)) {

									for (AlterationItemVO alterationItemVO : alterationItemList) {
										
										NBUtils.logger(this.getClass(), "RECEIVE alterationItemVO getPolicyChgId="
														+ alterationItemVO.getPolicyChgId()
														+",serviceId=" + alterationItemVO.getServiceId());
										if(alterationItemVO.getServiceId() == Service.SERVICE__ADD_RIDER) {
											Long uploadId = csLiaRocUploadService.saveReceiveNotice(alterationItemVO.getPolicyChgId(), true);
											NBUtils.logger(this.getClass(), "uploadToESP("+uploadId+") begin");
											uploadToESP(uploadId);
											NBUtils.logger(this.getClass(), "uploadToESP("+uploadId+") end");
											uploadIdList.add(uploadId);
											
										}
									
									}
								} else if (formUploadVO.getLiarocUploadType().equals(LiaRocCst.LIAROC_UL_TYPE_INFORCE)) {

									for (AlterationItemVO alterationItemVO : alterationItemList) {

										NBUtils.logger(this.getClass(), "INFORCE alterationItemVO getPolicyChgId="
														+ alterationItemVO.getPolicyChgId()
														+",serviceId=" + alterationItemVO.getServiceId()
														+",validateTime=" + DateUtils.date2String(alterationItemVO.getValidateTime()));
										
										Long uploadId = csLiaRocUploadService.saveInforceNotice(changeId,
														alterationItemVO.getPolicyChgId(),
														alterationItemVO.getValidateTime());
										
										NBUtils.logger(this.getClass(), "uploadToESP("+uploadId+") begin");
										uploadToESP(uploadId);
										NBUtils.logger(this.getClass(), "uploadToESP("+uploadId+") end");
										
										uploadIdList.add(uploadId);
									}
								}

								trans.commit();
							} catch(Exception e) {
								TransUtils.rollback(trans);
							}
						}
					}

					if (uploadIdList != null && uploadIdList.size() > 0) {
						HibernateSession3.detachSession();
						for (Long uploadId : uploadIdList) {

							if (uploadId != null) {

								LiaRocUploadVO uploadVO = liaRocUpload2019CI.load(uploadId);
								// 判斷此筆保單號碼的通報是否成功
								String status = uploadVO.getLiaRocUploadStatus();
								
								NBUtils.logger(this.getClass(), "getLiaRocUploadStatus="+status);
								if (LiaRocCst.LIAROC_UL_STATUS_SEND_NO_ITEM.equals(status)) {
									failItems.add(formUploadVO.getPolicyCode() + "(無可通報險種)");
									isAllPass = false; // 有部份失敗的記錄
								} else if (!LiaRocCst.LIAROC_UL_STATUS_SEND.equals(status)) {

									String desc = "unknow";
									if ("0".equals(status)) desc = "待通報";
									if ("1".equals(status)) desc = "通報發送完成";
									if ("2".equals(status)) desc = "通報回傳完成";
									if ("9".equals(status)) desc = "沒有險種需要通報";
									if ("D".equals(status)) desc = "註記刪除不通報";
									if ("E".equals(status)) desc = "通報流程發生未預期的錯誤";
									if ("F".equals(status)) desc = "ESP端回傳接收失敗";
									if ("N".equals(status)) desc = "ESP端回傳接收失敗";
									if ("P".equals(status)) desc = "通報資料parse有錯";
									if ("Q".equals(status)) desc = "通報佇列中";
									if ("X".equals(status)) desc = "保項存在於回傳偵錯報表中";

									failItems.add(formUploadVO.getPolicyCode() + "(" + desc + ")");
									
									NBUtils.logger(this.getClass(), formUploadVO.getPolicyCode() + "(" + desc + ")");
									isAllPass = false; // 有部份失敗的記錄
								}
							} else {
								
								NBUtils.logger(this.getClass(), "uploadId is null(無可通報險種)");
								
								failItems.add(formUploadVO.getPolicyCode() + "(無可通報險種)");
								isAllPass = false; // 有部份失敗的記錄
							}
						}
					} else {
						request.setAttribute("sysMsg", formUploadVO.getPolicyCode() + "(無可通報險種)");
					}
				} catch (Exception e) {
                    NBUtils.logger(this.getClass(), exceptionTrace(e));
					request.setAttribute("sysMsg", e.getMessage());
					failItems.add(formUploadVO.getPolicyCode());
					isAllPass = false; // 有部份失敗的記錄
				} finally {
                    try {
                        ApplicationLogger.flush();
                    } catch (Exception e) {
                        NBUtils.logger(this.getClass(), exceptionTrace(e));
                    }
					//for testing rollback;
					//TransUtils.rollback(trans);
				}
			}

		} else {
			request.setAttribute("sysMsg", "CLM do nothing");
			return false;
		}

		if (failItems.size() > 0) {
			request.setAttribute("sysMsg",
							StringResource.getStringData("MSG_1259767", AppContext.getCurrentUser().getLangId())
											+ StringUtils.join(failItems.toArray(), ", "));
		}

		return isAllPass;

	}
	
	private boolean upload(UploadLiaRocItemForm form, HttpServletRequest request) {

		boolean isAllPass = true;
		List<String> failItems = new ArrayList<String>();

        // 如果為新契約人員
        int bizCategory = deptCI.getBizCategoryByUserId(AppContext.getCurrentUser().getUserId());
        NBUtils.logger(this.getClass(), "userId="+ AppContext.getCurrentUser().getUserId() 
                        + ",bizCategory=" + bizCategory);
        
		String bizSource;
		String appSystemCode;
		if (bizCategory == CodeCst.BIZ_CATEGORY_UNB) {
			bizSource = LiaRocCst.LIAROC_UL_BIZ_SOURCE_UNB;
            appSystemCode = AppLogDataHelper.SYSTEM_CODE_UNB;
		} else if (bizCategory == CodeCst.BIZ_CATEGORY_POS) {
			bizSource = LiaRocCst.LIAROC_UL_BIZ_SOURCE_POS;
            appSystemCode = AppLogDataHelper.SYSTEM_CODE_POS;
		} else if (bizCategory == CodeCst.BIZ_CATEGORY_CLM) {
			bizSource = LiaRocCst.LIAROC_UL_BIZ_SOURCE_CLM;
            appSystemCode = AppLogDataHelper.SYSTEM_CODE_CLM;
		} else {
			request.setAttribute("sysMsg", "ERROR USER BIZ CATEGORY !!!");
			return false;
		}
		
		Long policyId = policyService.getPolicyIdByPolicyNumber(form.getPolicyCode());
		PolicyVO policyVO = policyService.loadPolicyByPolicyId(policyId);
		if (form.getLiarocUploadType().equals(LiaRocCst.LIAROC_UL_TYPE_RECEIVE)
						|| form.getLiarocUploadType().equals(LiaRocCst.LIAROC_UL_TYPE_INFORCE)) {

			// 依保單號碼逐一通報
			List<CoverageVO> coverageVOList = new ArrayList<CoverageVO>();
			List<Integer> itemOrderList = Arrays.asList(form.getItemOrders());
			for (CoverageVO coverageVO : policyVO.getCoverages()) {
				if(itemOrderList.contains(coverageVO.getItemOrder())) {
					coverageVOList.add(coverageVO);
				}
			}
		    String policyCode = form.getPolicyCode();
            ApplicationLogger.setPolicyCode(policyCode);
            ApplicationLogger.addLoggerData("begin");
            ApplicationLogger.setJobName(ClassUtils.getShortClassName(this.getClass()));
            ApplicationLogger.setOptionId(DateUtils.date2String(new Date(), "yyyyMMdd HHmmss"));
            ApplicationLogger.setSystemCode(appSystemCode);
            
			UserTransaction trans = null;

			List<Long> uploadIdList = new ArrayList<Long>();
			try {
				//增加單筆上傳公會通報的功能。作業人員可以按照單張保單選擇上傳收件通報或承保通報。
				//保全會依最後的保全變更案件, 根據更新後的資料, 重新產生保全變更項的公會通報資料。
				if (LiaRocCst.LIAROC_UL_BIZ_SOURCE_UNB.equals(bizSource)) {
					trans = Trans.getUserTransaction();
					trans.begin();
					
					try {

						//新契約一律通報不作保項比對
	                    LiaRocUploadSendVO sendVO = liaRocUpload2019CI.getUploadVOByPolicyId(policyId, form.getLiarocUploadType(), bizSource);
	                    List<LiaRocUploadDetailVO> dataList = this.getDetailList(policyVO, coverageVOList, form.getLiarocUploadType());
	                    sendVO.setDataList(dataList);
	                    
	                    Long uploadId = liaRocUpload2019CI.saveUpload(sendVO);
						uploadToESP(uploadId);
						uploadIdList.add(uploadId);
						trans.commit();
					} catch(Exception e) {
						TransUtils.rollback(trans);
					}
				
				} else if (LiaRocCst.LIAROC_UL_BIZ_SOURCE_POS.equals(bizSource)) {
					
					NBUtils.logger(this.getClass(), "bizSource="+ bizSource);
					List<Application> applicationList = null;
					
					//保全會依最後的保全變更案件, 根據更新後的資料, 重新產生保全變更項的公會通報資料。
					if (form.getLiarocUploadType().equals(LiaRocCst.LIAROC_UL_TYPE_RECEIVE)) {
						applicationList = TCsApplicationDelegate.findByPolicyId(policyId);
					} else {
						applicationList = TCsApplicationDelegate.findByPolicyIdAndCsAppStatus( policyId, CsAppStatus.CS_APP_STATUS__IN_FORCE);
					}
					
					NBUtils.logger(this.getClass(), "applicationList="+ applicationList.size());
					
					Long changeId = null;
					if (applicationList != null && applicationList.size() > 0) {
						changeId = applicationList.get(applicationList.size() - 1).getChangeId();

						NBUtils.logger(this.getClass(), "changeId="+ changeId);
						
						trans = Trans.getUserTransaction();
						trans.begin();
						try {
							List<AlterationItemVO> alterationItemList = applicationService.getAlterationItemList(changeId);
							if (form.getLiarocUploadType().equals(LiaRocCst.LIAROC_UL_TYPE_RECEIVE)) {
								for (AlterationItemVO alterationItemVO : alterationItemList) {
									NBUtils.logger(this.getClass(), "RECEIVE alterationItemVO getPolicyChgId=" + alterationItemVO.getPolicyChgId() +",serviceId=" + alterationItemVO.getServiceId());
									if(alterationItemVO.getServiceId() == Service.SERVICE__ADD_RIDER) {
										Long uploadId = this.saveReceiveNotice(alterationItemVO.getPolicyChgId(), true, coverageVOList);
										NBUtils.logger(this.getClass(), "uploadToESP("+uploadId+") begin");
										uploadToESP(uploadId);
										NBUtils.logger(this.getClass(), "uploadToESP("+uploadId+") end");
										uploadIdList.add(uploadId);
									}
								}
							} else if (form.getLiarocUploadType().equals(LiaRocCst.LIAROC_UL_TYPE_INFORCE)) {
								for (AlterationItemVO alterationItemVO : alterationItemList) {
									NBUtils.logger(this.getClass(), "INFORCE alterationItemVO getPolicyChgId=" + alterationItemVO.getPolicyChgId() +",serviceId=" + alterationItemVO.getServiceId() +",validateTime=" + DateUtils.date2String(alterationItemVO.getValidateTime()));
									Long uploadId = csLiaRocUploadService.saveInforceNotice(changeId,coverageVOList,
													alterationItemVO.getPolicyChgId(),
													alterationItemVO.getValidateTime());
									
									NBUtils.logger(this.getClass(), "uploadToESP("+uploadId+") begin");
									uploadToESP(uploadId);
									NBUtils.logger(this.getClass(), "uploadToESP("+uploadId+") end");
									
									uploadIdList.add(uploadId);
								}
							}

							trans.commit();
						} catch(Exception e) {
							TransUtils.rollback(trans);
						}
					}
				}

				if (uploadIdList != null && uploadIdList.size() > 0) {
					HibernateSession3.detachSession();
					for (Long uploadId : uploadIdList) {

						if (uploadId != null) {

							LiaRocUploadVO uploadVO = liaRocUpload2019CI.load(uploadId);
							// 判斷此筆保單號碼的通報是否成功
							String status = uploadVO.getLiaRocUploadStatus();
							
							NBUtils.logger(this.getClass(), "getLiaRocUploadStatus="+status);
							if (LiaRocCst.LIAROC_UL_STATUS_SEND_NO_ITEM.equals(status)) {
								failItems.add(form.getPolicyCode() + "(無可通報險種)");
								isAllPass = false; // 有部份失敗的記錄
							} else if (!LiaRocCst.LIAROC_UL_STATUS_SEND.equals(status)) {

								String desc = "unknow";
								if ("0".equals(status)) desc = "待通報";
								if ("1".equals(status)) desc = "通報發送完成";
								if ("2".equals(status)) desc = "通報回傳完成";
								if ("9".equals(status)) desc = "沒有險種需要通報";
								if ("D".equals(status)) desc = "註記刪除不通報";
								if ("E".equals(status)) desc = "通報流程發生未預期的錯誤";
								if ("F".equals(status)) desc = "ESP端回傳接收失敗";
								if ("N".equals(status)) desc = "ESP端回傳接收失敗";
								if ("P".equals(status)) desc = "通報資料parse有錯";
								if ("Q".equals(status)) desc = "通報佇列中";
								if ("X".equals(status)) desc = "保項存在於回傳偵錯報表中";

								failItems.add(form.getPolicyCode() + "(" + desc + ")");
								
								NBUtils.logger(this.getClass(), form.getPolicyCode() + "(" + desc + ")");
								isAllPass = false; // 有部份失敗的記錄
							}
						} else {
							NBUtils.logger(this.getClass(), "uploadId is null(無可通報險種)");
							failItems.add(form.getPolicyCode() + "(無可通報險種)");
							isAllPass = false; // 有部份失敗的記錄
						}
					}
				} else {
					request.setAttribute("sysMsg", form.getPolicyCode() + "(無可通報險種)");
				}
			} catch (Exception e) {
                NBUtils.logger(this.getClass(), exceptionTrace(e));
				request.setAttribute("sysMsg", e.getMessage());
				failItems.add(form.getPolicyCode());
				isAllPass = false; // 有部份失敗的記錄
			} finally {
                try {
                    ApplicationLogger.flush();
                } catch (Exception e) {
                    NBUtils.logger(this.getClass(), exceptionTrace(e));
                }
			}

		} else {
			request.setAttribute("sysMsg", "CLM do nothing");
			return false;
		}

		if (failItems.size() > 0) {
			request.setAttribute("sysMsg", StringResource.getStringData("MSG_1259767", AppContext.getCurrentUser().getLangId()) + StringUtils.join(failItems.toArray(), ", "));
		}

		return isAllPass;

	}

	private void uploadToESP(Long uploadId) {

		if (uploadId != null) {
			LiaRocUploadVO uploadVO = liaRocUpload2019CI.load(uploadId);
			List<LiaRocUploadDetail> details = liaRocUploadDetailDao.findByUploadId(uploadId);

			if (details != null && details.size() > 0) {
				
			    // Prepare UploadData
                List<LiaRocUploadDetailWrapperVO> dataList = new ArrayList<LiaRocUploadDetailWrapperVO>();
                dataList = (List<LiaRocUploadDetailWrapperVO>) BeanUtils.copyCollection( LiaRocUploadDetailWrapperVO.class, details);

				// Prepare liaRocUploadSendVO
                LiaRocUploadSendVO liaRocUploadSendVO = new LiaRocUploadSendVO();
                BeanUtils.copyProperties(liaRocUploadSendVO, uploadVO);
				liaRocUploadSendVO.setDataList(dataList);
                liaRocUploadSendVO.setParseBean(fixedLengthWriter);

				try {
				    
				    byte[] bytesToWrite = IoUtil.readBytes(liaRocUploadSendVO.parseToText());
					NBUtils.logger(getClass(), new String(bytesToWrite, fixedLengthWriter.getCharset()));
					
					// To ESP Check For AppLog
                    Integer dataCount = (bytesToWrite.length + 1) / 288;
                    boolean isParsingCorrect = (bytesToWrite.length + 1) % 288 == 0;
                    NBUtils.logger(getClass(), "PerLine : 288(Included: '\\n') bytes ");
                    NBUtils.logger(getClass(), "UploadDtlSize : " + dataList.size());
                    NBUtils.logger(getClass(), "UploadToESP__Size : " + dataCount);
                    NBUtils.logger(getClass(), "UploadToESP__Byte_IsCorrect : " + (isParsingCorrect ? "Y)" : "N)") );
                    NBUtils.logger(getClass(), "UploadToESP__Detail_TextBytes : " + bytesToWrite.length + " bytes ");
					
//					if (false) {
//					    writeOtuFile(uploadVO, bytesToWrite);
//					}
					
				} catch (Exception e) {
					String error = ExceptionInfoUtils.getExceptionMsg(e);
					int len = error.length();
					if (len > 0) {
						ApplicationLogger.addLoggerData("[ERROR]Exception => " + error.substring(0, (len > 2000 ? 2000 : len)));
					} else {
						ApplicationLogger.addLoggerData("[ERROR]Exception => " + e);
					}
				}
				
				if (LiaRocCst.LIAROC_UL_BIZ_SOURCE_UNB.equals(uploadVO.getBizSource())) {
					liaRocUpload2019CI.send(ListTool.toList(liaRocUploadSendVO), liaRocUploadSendVO.getLiaRocUploadType(),
									LiaRocCst.LIAROC_UL_UPLOAD_TYPE_NB);
				} else {
					liaRocUpload2019CI.send(ListTool.toList(liaRocUploadSendVO), liaRocUploadSendVO.getLiaRocUploadType(),
									LiaRocCst.LIAROC_UL_UPLOAD_TYPE_POS);
				}
			}

		}

	}
	
	private void writeOtuFile(LiaRocUploadVO uploadVO, byte[] bytesToWrite) {
        String fileName = "UploadLiaRoc-" + uploadVO.getPolicyId() + "-" + uploadVO.getListId() + ".txt";
        try {
            NBUtils.logger(getClass(), "Write to File Start! FileName: " + fileName);
            writeOutFile(bytesToWrite, fileName);
            NBUtils.logger(getClass(), "Write to File End! FileName: " + fileName);
        } catch (Exception e) {
            NBUtils.logger(getClass(), "Write to File Error! FileName: " + fileName);
            NBUtils.logger(getClass(), "Write to File Error! Exception: " + exceptionTrace(e));
        }
	}
    
    private void writeOutFile(byte[] bytesToWrite, String fileName) throws Exception {
        
        File folder = new File(EnvUtils.getSendToInternalSharePath(), "liaRocUploadLog");
        NBUtils.logger(getClass(), "Write to Folder! FolderName: " + folder.getAbsolutePath());
        
        File uploadFile = new File(folder, fileName);
        
        if (uploadFile.getParentFile().exists() == false) {
            FileUtils.forceMkdir(uploadFile.getParentFile());
            FileGenericHelper.setFolderReadWritable(uploadFile.getParentFile());
        }
        
        OutputStream os = new FileOutputStream(uploadFile);
        os.write(bytesToWrite);
        os.flush();
        os.close();
        
    }
    
    private String exceptionTrace(Exception e) {
        String errorMsg = null;
        try {
            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));
            errorMsg = errors.toString();
        } catch (Exception ex) {
            errorMsg = "Catch Error Failed !";
        }
        return errorMsg;
    }
    
	private List<LiaRocUploadDetailVO> getDetailList(PolicyVO policyVO,List<CoverageVO> coverageVOList, String liaRocUploadType) {
		List<LiaRocUploadDetailVO> detailList = new ArrayList<LiaRocUploadDetailVO>();
		// setup 通報明細資料
		for (CoverageVO coverage : coverageVOList) {
			if (liaRocUpload2019CI.needUploadItem(coverage)) {
				LiaRocUploadDetailWrapperVO detailMapVO = liaRocUpload2019CI.generateUploadDetail(policyVO, coverage, liaRocUploadType);
				detailList.add(detailMapVO);
			}
		}
		return detailList;

	}
	
	private Long saveReceiveNotice(Long policyChgId, boolean isFromManual,List<CoverageVO> coverageVOList) {

		boolean isLiarocV2020 = liaRocUploadCommonService.isLiaRocV2020Process();
    	
		
        String liaRocUploadType = LiaRocCst.LIAROC_UL_TYPE_RECEIVE;
        List<LiaRocUploadDetailVO> details = new ArrayList<LiaRocUploadDetailVO>();
        List<LiaRocUploadDetail2020VO> details2020 = new ArrayList<LiaRocUploadDetail2020VO>();

        // 從 FOA 進的件在通路受理時已經收件通報過因此 by pass 以避免重覆通報
        // Long changeId = alterationItemVO.getMasterChgId();
        // List<CsAcceptanceStatus> csAcceptanceStatusList =
        // csAcceptanceStatusDao.findByChangeId(changeId);

        // 保全收件通報的保單需要符合以下條件
        // 1.保全項目=新增附約(含豁免險)
        // 2.保全狀態=錄入完成
        // 3.保全錄入完成日期=系統日期
        // 4.去除已通報的案件 t_cs_acceptance_status.change_id 一樣的
        // i.保全項目=新增附約(含豁免險)
        AlterationItemVO alterationItemVO = alterationItemDS.getAlterationItem(policyChgId);
        Long policyId = alterationItemVO.getPolicyId();

        Application appliction = TCsApplicationDelegate.load(alterationItemVO.getMasterChgId());
        HibernateSession3.currentSession().evict(appliction);
        
        PolicyVO policyVO = policyService.loadPolicyByPolicyId(policyId);
        if (Service.SERVICE__ADD_RIDER == alterationItemVO.getServiceId()) // ii.保全狀態=錄入完成
         {

        	// setup 通報明細資料
        	for (CoverageVO coverageVO : coverageVOList) {
				if (liaRocUpload2019CI.needUploadItem(coverageVO)) { // 判斷商品要不要通報

					String operType = coverageService.getOperTypeInPolicyChange(coverageVO.getItemId(),
							policyChgId);
					if (CodeCst.DATA_OPER_TYPE__DUMP_OPER_ADD.equals(operType)) {
						
						if(!isLiarocV2020) {
							LiaRocUploadDetailWrapperVO detailMapVO = liaRocUpload2019CI.generateCSUploadDetail(appliction,
											policyVO, coverageVO, liaRocUploadType);
									detailMapVO.setLiaRocPolicyStatus(LiaRocCst.LIAROC_POLICY_STATUS__INFORCE); // 有效
									details.add(detailMapVO);							
						}

						LiaRocUploadDetail2020WrapperVO detail2020VO = liaRocUpload2020CI.generateCSUploadDetail(appliction,
										policyVO, coverageVO, liaRocUploadType);
								detail2020VO.setLiaRocPolicyStatus(LiaRocCst.LIAROC_POLICY_STATUS__INFORCE); // 有效
								details2020.add(detail2020VO);
					}
				}
        	} // if size>0 save data
        }
        Long uploadListId = null;
        if(isFromManual) { 
        	//來自人工上傳頁面一律通報不作重覆比對
    		uploadListId = csLiaRocUploadService.saveUpload2020(liaRocUploadType, policyVO, details2020, 
        					alterationItemVO.getMasterChgId(),
        					alterationItemVO.getPolicyChgId());
    		
        	if(!isLiarocV2020) {
        		uploadListId = csLiaRocUploadService.saveUpload(liaRocUploadType, policyVO, details, 
        					alterationItemVO.getMasterChgId(),
        					alterationItemVO.getPolicyChgId());
        	}
        } else {
        	//需作重覆上傳比對
    		uploadListId = csLiaRocUploadService.saveReceiveUpload2020(liaRocUploadType, policyVO, details2020,
        					alterationItemVO.getMasterChgId(),
        					alterationItemVO.getPolicyChgId());
    		
        	if(!isLiarocV2020) {
        		uploadListId = csLiaRocUploadService.saveReceiveUpload(liaRocUploadType, policyVO, details,
        					alterationItemVO.getMasterChgId(),
        					alterationItemVO.getPolicyChgId());
        	}
        }
        return uploadListId ;
    }
	
}
