package com.ebao.ls.crs.batch.job;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.transaction.UserTransaction;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Session;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.ebao.foundation.common.lang.exception.ExceptionUtils;
import com.ebao.foundation.core.logging.Log;
import com.ebao.ls.batch.TGLBasePieceableJob;
import com.ebao.ls.crs.batch.service.CRSDueDiligenceListService;
import com.ebao.ls.crs.batch.vo.CRSTempVO;
import com.ebao.ls.crs.data.batch.CRSDueDiligenceListDAO;
import com.ebao.ls.crs.data.bo.CRSDueDiligenceList;
import com.ebao.ls.crs.vo.CRSDueDiligenceListVO;
import com.ebao.ls.cs.commonflow.data.TChangeDelegate;
import com.ebao.ls.cs.commonflow.data.bo.Change;
import com.ebao.ls.cs.pub.helper.DateHelper;
import com.ebao.ls.fatca.batch.data.FatcaBenefitSurveyListDAO;
import com.ebao.ls.fatca.batch.data.FatcaDueDiligenceListDAO;
import com.ebao.ls.fatca.constant.FatcaSurveyType;
import com.ebao.ls.fatca.data.bo.FatcaDueDiligenceList;
import com.ebao.ls.fatca.vo.FatcaDueDiligenceListVO;
import com.ebao.ls.pub.CodeCst;
import com.ebao.ls.pub.batch.impl.BatchJdbcTemplate;
import com.ebao.ls.pub.batch.impl.DataSourceWrapper;
import com.ebao.pub.batch.HibernateSession;
import com.ebao.pub.batch.help.BatchHelp;
import com.ebao.pub.batch.type.JobStatus;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.util.Trans;
import com.ebao.pub.util.TransUtils;

/**
 * CRS盡職調查名單
 */
public class CrsDueDiligenceListBatchJob extends TGLBasePieceableJob implements ApplicationContextAware {
	
	public static final String BEAN_DEFAULT = "crsDueDiligenceListBatchJob";
	
	private final Log logger = Log.getLogger(CrsDueDiligenceListBatchJob.class);
	
	private static final boolean IS_DEBUG= false;
	
	@Resource(name = CRSDueDiligenceListService.BEAN_DEFAULT)
	private CRSDueDiligenceListService crsDueDiligenceListService;
	
	@Resource(name = FatcaBenefitSurveyListDAO.BEAN_DEFAULT)
    private FatcaBenefitSurveyListDAO fatcaBenefitSurveyListDAO;
	
	@Resource(name = CRSDueDiligenceListDAO.BEAN_DEFAULT)
    private CRSDueDiligenceListDAO crsDueDiligenceListDAO;
	
    @Resource(name = FatcaDueDiligenceListDAO.BEAN_DEFAULT)
    private FatcaDueDiligenceListDAO fatcaDueDiligenceListDAO;
    
    private ApplicationContext ctx;
    
    private static BatchJdbcTemplate batchJdbcTemplate = new BatchJdbcTemplate(new DataSourceWrapper());
    
    private static NamedParameterJdbcTemplate namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(batchJdbcTemplate);
    
    private Date processDate;
    
    private String exeSql;

	public String getExeSql() {
		return exeSql;
	}

	public void setExeSql(String exeSql) {
		this.exeSql = exeSql;
	}

	private void log(String msg) {
		logger.info("[SYSOUT][CrsBenefitOwnerListBatchJob]" + msg);
		batchLogger.info(msg);
    }
	
	private void logWithId(String id, String msg) {
		
		if(!IS_DEBUG){
			return;
			}
		
		if (StringUtils.isNotBlank(id)) {
			msg = "[" + id + "] " + msg;
		}
		log(msg);
    }
	
    private void logWithLayout(String layout, Object... msg){
    	log(String.format(layout, msg));
    }
    
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		ctx=applicationContext;
	}

	@Override
	protected String getJobName() {
		return BatchHelp.getJobName();
	}
	
	/**
	 * 單筆測試用
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public int executeById(String id) throws Exception {
		preProcess();
		return execute(id);
	}

	@SuppressWarnings("unchecked")
	@Override
	protected Object[] prepareIds() throws Throwable {
		// 由Fatca母體資料抽取CRS盡職調查名單 條件:1.Fatca關注險種 2.保單狀態=有效+停效+當年度變更為無效件
		StringBuffer sql = new StringBuffer();
		processDate = BatchHelp.getProcessDate();	
		
		//匯率取前一年度最後一天
	    Calendar calendar = Calendar.getInstance();
	    calendar.add(Calendar.YEAR, -1);
	    calendar.set(Calendar.MONTH, Calendar.DECEMBER);
	    calendar.set(Calendar.DATE, 31);
	    Date baseDate = calendar.getTime();
	    DateHelper.clearTime(baseDate);

		Map<String, Object> params = new HashMap<String, Object>();
//        Double usExchangeRate =  crsDueDiligenceListDAO.getUSExchangeRate(baseDate);
//        Double cnExchangeRate =  crsDueDiligenceListDAO.getCNExchangeRate(baseDate);
//        params.put("us_exchange_rate", usExchangeRate);
//        params.put("cn_exchange_rate", cnExchangeRate);
        java.sql.Date processSqlDate = new java.sql.Date(baseDate.getTime());
        params.put("process_date", processSqlDate);

    	// 高資產帳戶持有人在CRS聲明書狀態非[已簽署];高資產未執行過AML判定
		// OR[已通知未簽署]且發送單位為POS，建檔狀態 = 已建檔、已註記	
		sql.append(" select distinct tfm1.party_id||'/'||tfm1.certi_code||'/'||party_type||'/'||tfm1.policy_value");
		sql.append(" from ");
		sql.append(" ( ");
		sql.append(" select tfsp.account_owner_party_id party_id,tfsp.account_owner_certi_code certi_code, ");
		sql.append("        party_type, sum(policy_value*pkg_ls_crs_foreign_indi.f_get_us_exchage_rate(tfsp.money_id, :process_date)) policy_value "); 
		sql.append(" from t_fatca_survey_population tfsp, t_contract_master tcm, t_party party");
		sql.append(" where tfsp.policy_id=tcm.policy_id  ");
		sql.append(" and party.party_id=tfsp.account_owner_party_id and party_type=1  ");
		sql.append(" and tfsp.product_id not in (select product_id from t_product_category where category_id=60028) ");
		sql.append(" and (tfsp.policy_status in (1,2) or (tfsp.policy_status=3 and tcm.liability_state_date >= to_date(to_char(tfsp.ad_year||'0101'),'yyyymmdd') )) ");
		sql.append(" group by account_owner_party_id, account_owner_certi_code, party_type");
		sql.append(" having sum(policy_value*pkg_ls_crs_foreign_indi.f_get_us_exchage_rate(tfsp.money_id, :process_date)) >= 1000000");
		sql.append(" ) tfm1   ");	
		sql.append(" where  not exists (select 1 from t_crs_party crs ");
		sql.append("                    where  certi_code is not null ");
		sql.append("                    and    certi_code = tfm1.certi_code ");
		sql.append("                    and    ( crs.doc_sign_status in ('02','03') or ");
		sql.append("                             crs.aml_check_date is not null or ");
		sql.append("                            (crs.doc_sign_status='01' and crs.sys_code='POS' and crs.build_type in ('01','06')");
		sql.append("                              and crs.six_indi_check_date is not null)");
		sql.append("                           ) ");
		sql.append("                    ) ");
		// 低資產帳戶持有人在CRS聲明書狀態非[ 1.已簽署 2.人工判定 3.批次判定 ]
		// OR[已通知未簽署]且發送單位為POS，建檔狀態 = 已建檔、已註記	
		sql.append(" union ");
		sql.append(" select distinct tfm1.party_id||'/'||tfm1.certi_code||'/'||party_type||'/'||tfm1.policy_value");
		sql.append(" from ");
		sql.append(" ( ");
		sql.append(" select tfsp.account_owner_party_id party_id,tfsp.account_owner_certi_code certi_code, ");
		sql.append("        party_type, sum(policy_value*pkg_ls_crs_foreign_indi.f_get_us_exchage_rate(tfsp.money_id, :process_date)) policy_value "); 
		sql.append(" from t_fatca_survey_population tfsp, t_contract_master tcm, t_party party");
		sql.append(" where tfsp.policy_id=tcm.policy_id  ");
		sql.append(" and party.party_id=tfsp.account_owner_party_id and party_type=1  ");
		sql.append(" and (tfsp.policy_status in (1,2) or (tfsp.policy_status=3 and tcm.liability_state_date >= to_date(to_char(tfsp.ad_year||'0101'),'yyyymmdd') )) ");
		sql.append(" group by account_owner_party_id, account_owner_certi_code, party_type");
		sql.append(" ) tfm1   ");
		sql.append(" where not exists (select 1 from t_crs_party crs ");
		sql.append("                   where  certi_code is not null ");
		sql.append("                   and    certi_code = tfm1.certi_code ");
		sql.append("                   and    ( crs.doc_sign_status in ('02','03','05','06') or ");
		sql.append("                            crs.six_indi_check_date is not null or ");
		sql.append("                           (crs.doc_sign_status='01' and crs.sys_code='POS' and crs.build_type in ('01','06')");
		sql.append("                             and crs.six_indi_check_date is not null)");
		sql.append("                          ) ");
		sql.append("                 ) ");
		sql.append(" and not exists (select * from t_crs_due_diligence_list dil ");
		sql.append("                 where  certi_code = tfm1.certi_code ");
		sql.append("                 and    party_id = tfm1.party_id ");
		sql.append("                 and    target_survey_type = 15 ");
		sql.append("                 ) ");
		//PCR375626 法人盡職調查
		sql.append(" union ");
		sql.append(" select distinct tfm1.party_id||'/'||tfm1.certi_code||'/'||party_type||'/'||tfm1.policy_value");
		sql.append(" from ");
		sql.append(" ( ");
		sql.append(" select tfsp.account_owner_party_id party_id,tfsp.account_owner_certi_code certi_code, ");
		sql.append("        party_type, sum(policy_value*pkg_ls_crs_foreign_indi.f_get_us_exchage_rate(tfsp.money_id, :process_date)) policy_value "); 
		sql.append(" from t_fatca_survey_population tfsp, t_contract_master tcm, t_party party");
		sql.append(" where tfsp.policy_id=tcm.policy_id  ");
		sql.append(" and party.party_id=tfsp.account_owner_party_id and party_type=2  ");
		sql.append(" and (tfsp.policy_status in (1,2) or (tfsp.policy_status=3 and tcm.liability_state_date >= to_date(to_char(tfsp.ad_year||'0101'),'yyyymmdd') )) ");
		sql.append(" group by account_owner_party_id, account_owner_certi_code, party_type");
		sql.append(" having sum(policy_value*pkg_ls_crs_foreign_indi.f_get_us_exchage_rate(tfsp.money_id, :process_date)) >= 250000");
		sql.append(" ) tfm1   ");	
		sql.append(" where  not exists (select 1 from t_crs_party crs ");
		sql.append("                    where  certi_code is not null ");
		sql.append("                    and    certi_code = tfm1.certi_code ");
		sql.append("                    and    (doc_sign_status <> '05' or crs_type = 'CRS240') ");
		sql.append("                    ) ");
		
		String sqlString = sql.toString();
	    log("prepareIds, SQL = " + sql);
	    List<String> itemList = namedParameterJdbcTemplate.queryForList(sqlString, params, String.class);
		logWithLayout("CrsDueDiligenceListBatchJob total size = %,d " , itemList.size());
        return itemList.toArray();
	}
	
	@Override
	public int preProcess() throws Exception {

		StringBuffer sql = new StringBuffer();
		
        sql.append(" select distinct tfsp.policy_id, ");
        sql.append("                 tfsp.policy_code, ");
        sql.append("                 tcm.liability_state policy_status,    ");
        sql.append("                 (case when tfsp.account_owner_party_id=tfsp.app_party_id then app_name else insured_name end) target_name,     ");
        sql.append("                 tfsp.account_owner_certi_code certi_code,      ");
        sql.append("                 (case when tfsp.account_owner_party_id=tfsp.app_party_id then '01' else '02' end) role_type,     ");
        sql.append("                 agt.agent_id service_agent,     ");
        sql.append("                 agt.register_code agent_register_code,     ");
        sql.append("                 agt.agent_name,     ");
        sql.append("                 agt.business_cate agent_biz_cate,     ");
        sql.append("                 org.channel_id channel_org,     ");
        sql.append("                 org.channel_code,     ");
        sql.append("                 org.channel_name,     ");
        sql.append("                 decode(agt.business_cate, 2, 4, org.channel_type) channel_type,     ");
        sql.append("                 (case when :target_survey_type <> 03 then pkg_ls_crs_foreign_indi.f_get_six_indi(tfsp.account_owner_party_id,tfsp.account_owner_certi_code,to_date(to_char(tfsp.ad_year||'0101'),'yyyymmdd')) else null end) six_indi, ");
        sql.append("                 (case when (:policy_value >= 1000000 or :target_survey_type = 03) then pkg_ls_crs_foreign_indi.f_get_aml_indi(tfsp.account_owner_certi_code) else null end) aml_indi ");	
		sql.append(" from t_fatca_survey_population tfsp, t_contract_master tcm, ");
		sql.append("      t_agent agt, t_channel_org org,  ");
		sql.append("      (   ");
		sql.append("      select tfsp.account_owner_party_id party_id, tfsp.account_owner_certi_code certi_code,  ");
		sql.append("             tcm.policy_id, tcm.validate_date, liability_state, tfsp.list_id, ");
		sql.append("             rank() over (partition by account_owner_party_id, account_owner_certi_code  ");
		sql.append("                          order by tcm.liability_state, tcm.validate_date desc, tcm.policy_id desc,tfsp.item_id desc, tfsp.list_id desc) rank  ");
		sql.append("      from t_fatca_survey_population tfsp, t_contract_master tcm   ");
		sql.append("      where tfsp.policy_id=tcm.policy_id    ");
		sql.append("      and (tfsp.policy_status in (1,2) or (tfsp.policy_status=3 and tcm.liability_state_date >= to_date(to_char(tfsp.ad_year||'0101'),'yyyymmdd')))    ");
		sql.append("      ) tfm  ");
		sql.append(" where tfm.party_id  =  :party_id ");
		sql.append(" and   tfm.certi_code =  :certi_code ");
		sql.append(" and   rank = 1  ");
		sql.append(" and   tfsp.list_id =  tfm.list_id ");
		sql.append(" and   tfsp.policy_id=tcm.policy_id");
        sql.append(" and   service_agent = agt.agent_id ");
        sql.append(" and   agt.channel_org_id = org.channel_id ");
 
        this.setExeSql(sql.toString());
		return super.preProcess();
	}
	
	@Override
	protected int execute(String id) throws Exception {
		logWithId(id, "execute - start");
		String[] identityId = StringUtils.split(id, "/");
		BigDecimal highValue = new BigDecimal("1000000");

        String partyId     = identityId[0];
        String certiCode   = identityId[1];
        String partyType   = identityId[2];
        String surveyType  = null;
        BigDecimal policyValue = new BigDecimal(identityId[3]);
        
        if (CodeCst.PARTY_TYPE__ORGAN_CUSTOMER.equals(partyType)){
        	surveyType = FatcaSurveyType.NEED_TO_SURVEY;
    	}else{
   		      if(policyValue.compareTo(highValue) > 0){
   			    surveyType = FatcaSurveyType.IS_HIGH_ASSET; 
   		      }
   		      else{
   			    surveyType = FatcaSurveyType.IS_LOW_ASSET; 
   		      }
   	    }
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("party_id", partyId);
        params.put("certi_code", certiCode);
        params.put("policy_value", policyValue);
        params.put("target_survey_type", surveyType);
 
        List<Map<String,Object>> mapList= namedParameterJdbcTemplate.queryForList(this.getExeSql(),  params);
        if(CollectionUtils.isEmpty(mapList)) {
        	logWithId(id, "不符合處理條件，略過");
        	return JobStatus.EXECUTE_SUCCESS;
        }
        List<CRSTempVO> crsTempVOList = new LinkedList<CRSTempVO>();
        
        // 擷取查詢結果
        for (Map<String, Object> column : mapList) {
        	CRSTempVO crsTempVO = new CRSTempVO();
            
        	Long targetId = Long.valueOf(partyId);
        	String targetCertiCode = certiCode;
        	Long surveyId = MapUtils.getLong(column, "policy_id");
        	String targetName = StringUtils.trim(MapUtils.getString(column, "target_name",""));
        	String policyCode = StringUtils.trim(MapUtils.getString(column, "policy_code",""));
        	String roleType = StringUtils.trim(MapUtils.getString(column, "role_type",""));
    		String sixIndi = StringUtils.trim(MapUtils.getString(column, "six_indi",""));
    		String amlIndi = StringUtils.trim(MapUtils.getString(column, "aml_indi",""));
        	
        	logWithId(id, "targetId = " + targetId + ", surveyId = " + surveyId
        			+ ", targetCertiCode = " + targetCertiCode + ",policyCode = " + policyCode + ", targetName = " + targetName );
        	
        	// 所有執行CRS盡職調查件存入t_Crs_Due_Diligence_List
        	 CRSDueDiligenceListVO crsVO = new CRSDueDiligenceListVO ();
        	 
        	 crsVO.setTargetSurveyType(surveyType);
        	 crsVO.setPolicyId(surveyId);
         	 crsVO.setPolicyCode(policyCode);
         	 crsVO.setLiabilityStatus(MapUtils.getInteger(column, "policy_status"));
         	 crsVO.setPartyId(targetId);
        	 crsVO.setName(targetName);
        	 crsVO.setCertiCode(targetCertiCode);
        	 crsVO.setPartyType(partyType);
        	 crsVO.setPolicyValue(policyValue);
        	 crsVO.setAgentId(MapUtils.getLong(column, "service_agent"));
         	 crsVO.setRegisterCode(StringUtils.trim(MapUtils.getString(column, "agent_register_code", "")));
         	 crsVO.setAgentName(StringUtils.trim(MapUtils.getString(column, "agent_name", "")));
         	 BigDecimal channelOrg = (BigDecimal) column.get("channel_org");
        	 if(channelOrg != null) {
        		crsVO.setChannelId(channelOrg.longValue());
        	 }
         	 crsVO.setChannelCode(StringUtils.trim(MapUtils.getString(column, "channel_code", "")));
        	 crsVO.setChannelName(StringUtils.trim(MapUtils.getString(column, "channel_name", "")));
        	 crsVO.setTargetSurveyDate(processDate);
        	 BigDecimal channelType = (BigDecimal)column.get("channel_type");
        	 if(channelType != null) {
        	 	crsVO.setChannelType(channelType.longValue());
        	 }
        	 if(sixIndi != "") {
         		crsVO.setSixIndi(sixIndi);
         	 }
         	 if(amlIndi != "") {
         		crsVO.setAmlIndi(amlIndi);
         	 }
         	 
         	CRSDueDiligenceList resultCRSBO = crsDueDiligenceListDAO.searchCRSDueDiligenceList(targetId, surveyId, crsVO.getTargetSurveyType());
         	if (resultCRSBO == null) {
        		// 新紀錄
        		crsTempVO.setNewCRSRecord(true);
        	} else {
        		// 已有記錄
        		crsVO.setListId(resultCRSBO.getListId());
        		crsTempVO.setNewCRSRecord(false);
        	}
         	
        	// 高資產符合六大指標或AML存入Fatca盡職調查檔
         	// PCR375626 增加低資產及法人盡職調查
        	 FatcaDueDiligenceListVO fatcaVO = new FatcaDueDiligenceListVO();
        	 if ( sixIndi != "" || amlIndi != "" ||CodeCst.PARTY_TYPE__ORGAN_CUSTOMER.equals(partyType)){
            	fatcaVO.setTargetSurveyType(surveyType);
            	fatcaVO.setPolicyId(surveyId);
            	fatcaVO.setPolicyCode(policyCode);
            	fatcaVO.setPolicyStatus(MapUtils.getLong(column, "policy_status"));
            	fatcaVO.setTargetId(targetId);
            	fatcaVO.setTargetName(targetName);
            	fatcaVO.setTargetCertiCode(targetCertiCode);
            	fatcaVO.setTargetPartyType(partyType);
            	fatcaVO.setServiceAgent(MapUtils.getLong(column, "service_agent"));
            	fatcaVO.setAgentRegisterCode(StringUtils.trim(MapUtils.getString(column, "agent_register_code", "")));
            	fatcaVO.setAgentName(StringUtils.trim(MapUtils.getString(column, "agent_name", "")));
            	BigDecimal agentBizCate = (BigDecimal) column.get("agent_biz_cate");
            	if(agentBizCate != null) {
            		fatcaVO.setAgentBizCate(agentBizCate.longValue());
            	}
            	if(channelOrg != null) {
            		fatcaVO.setChannelOrg(channelOrg.longValue());
            	}
            	fatcaVO.setChannelCode(StringUtils.trim(MapUtils.getString(column, "channel_code", "")));
            	fatcaVO.setChannelName(StringUtils.trim(MapUtils.getString(column, "channel_name", "")));

            	if(channelType != null) {
            		fatcaVO.setChannelType(channelType.longValue());
            	}
            	// 設定六大指標及AML註記
            	if(sixIndi != "") {
            		fatcaVO.setSixIndi(sixIndi);
            	}
            	if(amlIndi != "") {
            		fatcaVO.setAmlIndi(amlIndi);;
            	}
            	fatcaVO.setRoleType(roleType);
            	fatcaVO.setTargetSurveyDate(processDate);

            	// 查詢T_FATCA_DUE_DILIGENCE_LIST檢查目前有無紀錄
            	// FatcaDueDiligenceList resultFatcaBO = fatcaBenefitSurveyListDAO.searchFatcaDueDiligenceList(targetId, surveyId, FatcaSurveyType.IS_HIGH_ASSET,processDate);
            	// PCR396290 如聲明人同時符合FATCA / CRS 記錄，[需調查類型]以FATCA為主顯示
            	java.util.List<FatcaDueDiligenceList> resultFatcaBO = fatcaBenefitSurveyListDAO.searchFatcaDueDiligenceList(targetId, surveyId, processDate);
            	if (resultFatcaBO == null) {
            		// 新紀錄
            		crsTempVO.setNewRecord(true);
            		// 調查系統別為CRS
            		fatcaVO.setSurveySystemType(CRSDueDiligenceListService.SURVEY_SYSTEM_TYPE_CRS);
            	} else {
            	  for (FatcaDueDiligenceList resultFatca : resultFatcaBO) {
            		  if(resultFatca.getTargetSurveyType().equals(FatcaSurveyType.IS_HIGH_ASSET) ||
            			 resultFatca.getTargetSurveyType().equals(FatcaSurveyType.FATCA_SURVEY_TYPE__HAS_AMERICAN_SIGN)) {
                  		 // 已有記錄
                  		 crsTempVO.setNewRecord(false);
            			 fatcaVO.setTargetSurveyType(resultFatca.getTargetSurveyType());
            			 fatcaVO.setListID(resultFatca.getListID());
                		 // 調查系統別為FATCA，表示是由FATCA批次建立資料，需更新SURVEY_SYSTEM_TYPE為CRS/FATCA，否則保持目前狀態(CRS or CRS/FATCA)
                		 String surveySystemType = resultFatca.getSurveySystemType();
                		 if (StringUtils.equals(surveySystemType, CRSDueDiligenceListService.SURVEY_SYSTEM_TYPE_FATCA)) {
                		     fatcaVO.setSurveySystemType(CRSDueDiligenceListService.SURVEY_SYSTEM_TYPE_ALL);
                		 } else {
                		     fatcaVO.setSurveySystemType(surveySystemType);
                		 } 
            		  }	
            		  else {
            			// 新紀錄
                  		crsTempVO.setNewRecord(true);
                  		// 調查系統別為CRS
                  		fatcaVO.setSurveySystemType(CRSDueDiligenceListService.SURVEY_SYSTEM_TYPE_CRS);
            		  }
            	  }
            	}
                
         	 }

            crsTempVO.setFatcaDueDiligenceListVO(fatcaVO);
            crsTempVO.setCrsDueDiligenceListVO(crsVO);

            Long insertChangeId;
            Change tChange = new Change();
    		tChange.setChangeSource(CodeCst.CHANGE_SOURCE__PARTY);
    		tChange.setChangeStatus(CodeCst.CHANGE_STATUS__WAITING_ISSUE);
    		tChange.setOrgId(101L);
    		insertChangeId = TChangeDelegate.create(tChange);
    	    crsTempVO.setChangeId(insertChangeId );
	
            crsTempVOList.add(crsTempVO);
        }
        logWithId(id, "crsTempVOList.size() = " + crsTempVOList.size());
        
        UserTransaction trans = null;
        int successCount = 0;
        int errorCount = 0;
        for (CRSTempVO crsTempVO : crsTempVOList) {
        	try {
        		trans = TransUtils.getUserTransaction();
        		trans.begin();
        		FatcaDueDiligenceListVO fatcaVO = crsTempVO.getFatcaDueDiligenceListVO();
        		if((!fatcaVO.isEmpty() || fatcaVO != null) && 
               	   (fatcaVO.getSixIndi()!= null || fatcaVO.getAmlIndi() != null || CodeCst.PARTY_TYPE__ORGAN_CUSTOMER.equals(fatcaVO.getTargetPartyType()))){
        			// 更新業務員名單
        			crsDueDiligenceListService.updateServiceAgent(fatcaVO);        
        			// 產生盡職調查名單報表數據
        			if (crsTempVO.isNewRecord()) {
        				logWithId(id, "Create new record to fatcaBenefitSurveyList.");
        				fatcaBenefitSurveyListDAO.createOrUpdateVO(fatcaVO);
        			} else {
        				logWithId(id, "Update current record in fatcaBenefitSurveyList.");
        				fatcaBenefitSurveyListDAO.updateByVO(fatcaVO);
        			}
        		}
        		
        		CRSDueDiligenceListVO crsVO = crsTempVO.getCrsDueDiligenceListVO();
        		if(!crsVO.isEmpty() || crsVO != null){
        			// 更新業務員名單
        			crsDueDiligenceListService.updateCrsServiceAgent(crsVO);  
        			// 產生CRS報表數據
        			if (crsTempVO.isNewCRSRecord()) {
        				crsDueDiligenceListDAO.createOrUpdateVO(crsVO);
        			} else {
        				crsDueDiligenceListDAO.updateByVO(crsVO);
        			}
        		}
                // 自動執行盡職調查 CRS建檔 => 高資產符合六大指標或AML及低資產不符六大指標及法人
                Long userId = AppContext.getCurrentUser().getUserId();
                if ((!crsVO.isEmpty() || crsVO != null))	 {
                	crsDueDiligenceListService.createCrsRecord(id, crsTempVO, userId, processDate);
                }
            
                successCount++;
                trans.commit();
                logWithId(id, "CRS建檔完成");
        	} catch (Exception ex) {
        		logWithId(id, "發生異常! 錯誤訊息 = " + ExceptionUtils.getFullStackTrace(ex));
        		errorCount++;
        		trans.rollback();
        	} finally {
        		trans = null;
        	}
        }
        logWithId(id, "successCount = " + successCount + ", errorCount = " + errorCount);
        logWithId(id, "execute - end");
        
        if (errorCount == 0) {
			return JobStatus.EXECUTE_SUCCESS;
		} else if (successCount > 0) {
			return JobStatus.EXECUTE_PARTIAL_SUCESS;
		} else {
			return JobStatus.EXECUTE_FAILED;
		}
	}
	
	/**
	 * 父類別的transation是以一片為單位，為了避免同片內一筆rollback時，把上一筆的變更也一同rollback，覆寫父類別的方法
	 */
	@Override
    public int exeSingleRecord(String id) throws Exception {
        long start = System.currentTimeMillis();
        
        UserTransaction trans = null;
        int ret = 0;
        try {
            trans = Trans.getUserTransaction();
            trans.begin();
        
            ret = this.execute(id);
            this.updateStatus(id, ret);
            if (ret != JobStatus.EXECUTE_SUCCESS) {
                batchLogger.error("執行不成功! ID:{0} 狀態:{1}", id, ret);
            }
            long stop = System.currentTimeMillis();
            if(enableStopwatch()){
                batchLogger.debug("ID:{0} 執行花費時間:{1} ms", id, stop - start);
            }
            trans.commit();
        }catch(Exception e){
            trans.rollback();
            batchLogger.error("執行不成功! ID:{0} errMsg:{1}", id, e.toString());
        }
        return ret;
    }
	
	/**
	 * 更新t_batch_job_record的分片紀錄執行結果
	 * @param id
	 * @param status
	 */
	private void updateStatus(String id, int status) {
        Long runId = BatchHelp.getRunId();

        Session s = HibernateSession.currentSession();
        String sql = "update t_batch_job_record set status=?, finish_time=sysdate where run_id = ? and identity=?";
        Connection con = s.connection();
        PreparedStatement ps = null;
        try {
            ps = con.prepareStatement(sql);
            ps.setInt(1, status);
            ps.setLong(2, runId);
            ps.setString(3, id);
            ps.execute();
        } catch (SQLException e) {
            //e.printStackTrace();
        	batchLogger.error("TGLBasePieceableJob.updateStatus error", e);
        } finally {
        	try {
        		if(ps!=null) {
        			ps.close();
        		} 		
        	} catch(Exception e) {
        		
        	}
        }
    }	
	
	public Date getProcessDate() {
		return processDate;
	}

	public void setProcessDate(Date processDate) {
		this.processDate = processDate;
	}
}