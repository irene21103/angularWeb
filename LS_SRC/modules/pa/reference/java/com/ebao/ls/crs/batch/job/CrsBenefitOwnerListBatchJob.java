package com.ebao.ls.crs.batch.job;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.transaction.UserTransaction;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Session;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.ebao.foundation.common.lang.exception.ExceptionUtils;
import com.ebao.foundation.core.logging.Log;
import com.ebao.ls.batch.TGLBasePieceableJob;
import com.ebao.ls.crs.batch.service.CrsBenefitSurveyListService;
import com.ebao.ls.crs.batch.vo.CRSTempVO;
import com.ebao.ls.fatca.batch.data.FatcaBenefitSurveyListDAO;
import com.ebao.ls.fatca.constant.FatcaSurveyType;
import com.ebao.ls.fatca.data.bo.FatcaDueDiligenceList;
import com.ebao.ls.fatca.vo.FatcaDueDiligenceListVO;
import com.ebao.ls.pub.batch.impl.BatchJdbcTemplate;
import com.ebao.ls.pub.batch.impl.DataSourceWrapper;
import com.ebao.ls.pub.cst.YesNo;
import com.ebao.pub.batch.HibernateSession;
import com.ebao.pub.batch.help.BatchHelp;
import com.ebao.pub.batch.type.JobStatus;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.para.Para;
import com.ebao.pub.util.DateUtils;
import com.ebao.pub.util.Trans;
import com.ebao.pub.util.TransUtils;

/**
 * CRS保險金受益人調查名單
 */
public class CrsBenefitOwnerListBatchJob extends TGLBasePieceableJob implements ApplicationContextAware {
	
	public static final String BEAN_DEFAULT = "crsBenefitOwnerListBatchJob";
	
	private final Log logger = Log.getLogger(CrsBenefitOwnerListBatchJob.class);
	
	@Resource(name = CrsBenefitSurveyListService.BEAN_DEFAULT)
	private CrsBenefitSurveyListService crsBenefitSurveyListService;
	
	@Resource(name = FatcaBenefitSurveyListDAO.BEAN_DEFAULT)
    private FatcaBenefitSurveyListDAO fatcaBenefitSurveyListDAO;
	
    private ApplicationContext ctx;
    
    private static BatchJdbcTemplate batchJdbcTemplate = new BatchJdbcTemplate(new DataSourceWrapper());
    
    private static NamedParameterJdbcTemplate namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(batchJdbcTemplate);
    
    private Date processDate;
    
    private String exeSql;

	public String getExeSql() {
		return exeSql;
	}

	public void setExeSql(String exeSql) {
		this.exeSql = exeSql;
	}

	private void log(String msg) {
		logger.info("[SYSOUT][CrsBenefitOwnerListBatchJob]" + msg);
		batchLogger.info(msg);
    }
	
	private void logWithId(String id, String msg) {
		if (StringUtils.isNotBlank(id)) {
			msg = "[" + id + "] " + msg;
		}
		log(msg);
    }
	
    private void logWithLayout(String layout, Object... msg){
    	log(String.format(layout, msg));
    }
    
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		ctx=applicationContext;
	}

	@Override
	protected String getJobName() {
		return BatchHelp.getJobName();
	}
	
	/**
	 * 單筆測試用
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public int executeById(String id) throws Exception {
		preProcess();
		return execute(id);
	}

	@Override
	protected Object[] prepareIds() throws Throwable {
		// 由v_fmt_pos_0470各項保險金通知書中找出兩個月後要發放保險金的CRS受益人對象
		StringBuffer sql = new StringBuffer();
		String skipTimeConstraint = Para.getParaValue(3071000001L);
		
		sql.append(" select distinct policy_id || '/' || party_id  || '/' ||  item_id  || '/' || change_id from ( ");
		sql.append(" select fmt.change_id, cm.policy_id, cm.policy_code, cm.liability_state,cm.service_agent, cmbene.party_id, cmbene.certi_code, cmbene.certi_type, pp.item_id ");
		sql.append("  from (select p470.planid, p470.itemid, p470.change_id  ");
		sql.append("          from v_fmt_pos_0470_batch p470, t_contract_product cmPrd   "); //各項保險金到期通知書
		sql.append("         where 1 = 1  ");
		sql.append("           and p470.payDueDate is not null  ");
		sql.append("           and p470.policy_id = cmPrd.policy_id    ");
		sql.append("           and p470.itemid = cmPrd.item_id  ");
		// 需先判斷給付險種具現金價值,否則seq=1可能為健康活力金 ,PCR396290一併修正
		sql.append("           and exists   ");
		sql.append("              (select *   ");
		sql.append("               from t_product_category prodCate     ");
        sql.append("               where prodCate.product_id = cmPrd.product_id     ");
        sql.append("               and prodCate.category_id in (40031, 40154)) ");
		if(YesNo.YES_NO__NO.equals(skipTimeConstraint)) {
				sql.append("           and :process_date >= :fifteen_date ");
				sql.append("           and to_number(:current_month) > nvl(to_number((select to_char(max(doc.insert_time),'YYYYMM') ");
				sql.append("															from t_document doc ");
				sql.append("															where doc.template_id = 30030 and doc.insert_time <  :process_date )),0) ");
		}
		sql.append("           and p470.payDueDate > :process_date ");
		sql.append("           and to_char(p470.payDueDate,'yyyyMM') =  :after_two_month ");
		sql.append("  ) fmt  ");
		sql.append("  join t_pay_plan pp  ");
		sql.append("    on fmt.planid = pp.plan_id  ");
		sql.append("  join (select cmBene.policy_id,  ");
		sql.append("               cmBene.item_id,  ");
		sql.append("               cmBene.Party_Id,  ");
		sql.append("               cmBene.Certi_Code,  ");
		sql.append("				cmBene.name, ");
		sql.append("               cmBene.Certi_Type,  ");
		sql.append("               cmBene.Share_Order,  ");
		sql.append("               decode(cmbene.bene_type, 4, 2, 1, 3, 3, 4, 5, 10, 6, 9) bene_type  ");
		sql.append("          from t_contract_bene cmBene  ");
		sql.append("         where cmBene.Bene_Type in (4, 1, 3, 5, 6)) cmBene  ");
		sql.append("    on cmBene.item_id = pp.item_id  ");
		sql.append("   and cmBene.Bene_Type = pp.pay_plan_type  ");
		sql.append("   and cmBene.Share_Order = 1 ");
		//補IR316960產生受益人調查名單時請排除受益人與被保險人關係為法定繼承人
		sql.append("   and cmBene.party_id > 0  ");
		sql.append("  join t_contract_master cm  ");
		sql.append("    on cm.policy_id = pp.policy_id  ");
		sql.append("   and cm.liability_state > 0  ");
		sql.append(")");

		Map<String, Object> params = new HashMap<String, Object>();
        java.sql.Date processSqlDate = new java.sql.Date(processDate.getTime());
        java.util.Date fifteenDate = DateUtils.toDate(DateUtils.date2string(processDate, "yyyyMM") + "15", "yyyyMMdd");
        java.sql.Date fifteenSqlDate = new java.sql.Date(fifteenDate.getTime());
        String afterTwoMonth = DateUtils.date2String(DateUtils.addMonth(processDate,2), "yyyyMM");
        String currentMonth = DateUtils.date2String(processDate, "yyyyMM");
        params.put("after_two_month", afterTwoMonth);
        params.put("current_month", currentMonth);
        params.put("process_date", processSqlDate);
        params.put("fifteen_date", fifteenSqlDate);
        
        String sqlString = sql.toString();
        log("prepareIds, SQL = " + sql);
        
		List<String> itemList = namedParameterJdbcTemplate.queryForList(sqlString, params, String.class);
		logWithLayout("[BR-CMN-FAT-026][CrsBenefitOwnerListBatchJob] current_month := %s, after_two_month : = %s, process_date := %s, fifteen_date := %s ", currentMonth, afterTwoMonth , DateUtils.date2String(processDate, "yyyyMMdd"), DateUtils.date2String(fifteenDate, "yyyyMMdd"));
		logWithLayout("[BR-CMN-FAT-026][CrsBenefitOwnerListBatchJob] v_fmt_pos_0470 total size = %,d " , itemList.size());
        return itemList.toArray();
	}
	
	@Override
	public int preProcess() throws Exception {
		processDate = BatchHelp.getProcessDate();
		
		StringBuffer sql = new StringBuffer();
        sql.append(" select distinct bene.policy_id survey_id, ");
        sql.append("                 bene.policy_code,  ");
        sql.append("                 bene.liability_state policy_status,     ");
        sql.append("                 bene.party_id target_id,     ");
        sql.append("                 bene.name target_name,     ");
        sql.append("                 trim(bene.certi_code) target_certi_code,     ");
        sql.append("                 party.party_type target_party_type,     ");
        sql.append("                 agt.agent_id service_agent,     ");
        sql.append("                 agt.register_code agent_register_code,     ");
        sql.append("                 agt.agent_name,     ");
        sql.append("                 agt.business_cate agent_biz_cate,     ");
        sql.append("                 org.channel_id channel_org,     ");
        sql.append("                 org.channel_code,     ");
        sql.append("                 org.channel_name,     ");
        sql.append("                 decode(agt.business_cate, 2, 4, org.channel_type) channel_type,     ");
        sql.append("                 '03' role_type     ");
        sql.append(" from (   ");
        sql.append("   select cm.policy_code, cm.policy_id, cm.liability_state, cm.service_agent, cmbene.party_id, trim(cmbene.certi_code) certi_code ");
        sql.append("   , cmbene.name, cmprd.item_id, cmprd.product_version_id, cmprd.product_id from t_contract_master cm ");
        sql.append("   join t_contract_product cmprd ");
        sql.append("     on cm.policy_id = :policy_id and cm.policy_id = cmprd.policy_id and cmprd.item_id = :item_id ");        
        sql.append("   join t_contract_bene_log cmbene ");
        sql.append("     on cmbene.item_id = cmprd.item_id and cmbene.last_cmt_flg='Y' and cmbene.party_id = :party_id and cmbene.Bene_Type in (4, 1, 3, 5, 6) ");
        sql.append(" ) bene   ");
        sql.append(" join t_agent agt ");
        sql.append("   on bene.service_agent = agt.agent_id ");
        sql.append(" join t_channel_org org ");
        sql.append("   on agt.channel_org_id = org.channel_id ");
        sql.append(" join t_party party ");
        sql.append("   on party.party_id = bene.Party_Id ");
        sql.append(" and exists ");
        sql.append(" (select 1 from t_liability_config cfg     ");
        sql.append("   where cfg.product_id = bene.Product_Id     ");
        sql.append("   and cfg.product_version_id = bene.product_version_id     ");
        sql.append("   and cfg.fatca_ind = 'Y')     ");
        sql.append(" and (exists     ");
        // category_id = 40031(投資型), 40154(具有現金價值之保險合約(屬於FATCA保險帳戶))
        sql.append("   (select *     ");
        sql.append("     from t_product_category prodCate     ");
        sql.append("     where prodCate.product_id = bene.product_id     ");
        sql.append("     and prodCate.category_id in (40031, 40154)) ");
        sql.append("   or exists ");
        // 或者pay_plan_type= 2(年金) 6(變額年金)
        sql.append("   (select *     ");
        sql.append("     from t_pay_plan payPlan     ");
        sql.append("     where payPlan.policy_id = bene.policy_id     ");
        sql.append("     and payPlan.item_id = bene.item_id     ");
        sql.append("     and payPlan.pay_plan_type in ('2', '6')))  ");
        // 該對象在Fatca盡職調查名單中沒有CRS相關的建檔紀錄(SURVEY_SYSTEM_TYPE = CRS 或 CRS/FATCA)
        // PCR257607 mark for 受益人CRS未回覆或批次判定需重覆調查
        // sql.append(" and not exists "); 
        // sql.append("   (select 1 from T_FATCA_DUE_DILIGENCE_LIST fddl "); 
        // sql.append("     where 1=1 ");
        // sql.append("	 and fddl.target_certi_code = bene.certi_code ");
        // sql.append("	 and fddl.target_survey_type = '06' ");
        // sql.append("	 and fddl.SURVEY_SYSTEM_TYPE in ('CRS', 'CRS/FATCA')) ");
        // 該對象未有CRS 完成建檔記錄，意指未符合下述
        // 聲明書簽署狀態=已簽署" + 
        // or 聲明書簽署狀態=不願簽署 且 [稅務居住者之國家/地區]非空值
        // or 聲明書簽署狀態=未回覆 且 [稅務居住者之國家/地區]非空值 
        // PCR-415240 改Call PKG判斷受益人是否需調查
        sql.append(" and  pkg_ls_crs_foreign_indi.f_chk_bene_survey(bene.policy_code, bene.certi_code) = 'Y' ");   
        sql.append(" left join t_customer cust on cust.customer_id = bene.party_id     ");
        sql.append(" left join t_company_customer comp on comp.company_id = bene.party_id  ");
       
        this.setExeSql(sql.toString());
		return super.preProcess();
	}
	
	@Override
	protected int execute(String id) throws Exception {
		logWithId(id, "execute - start");
		String[] identityId = StringUtils.split(id, "/");
        String policyId = identityId[0];
        String partyId = identityId[1];
        String itemId = identityId[2];
        String changeId = identityId[3];
        
        logWithId(id, "policyId = " + policyId + ", partyId = " + partyId + ", itemId = " + itemId + ", changeId = " + changeId);
        
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("policy_id", policyId);
        params.put("party_id", partyId);
        params.put("item_id", itemId);
        
        List<Map<String,Object>> mapList= namedParameterJdbcTemplate.queryForList(this.getExeSql(),  params);
        if(CollectionUtils.isEmpty(mapList)) {
        	logWithId(id, "不符合處理條件，略過");
        	return JobStatus.EXECUTE_SUCCESS;
        }
        List<CRSTempVO> crsTempVOList = new LinkedList<CRSTempVO>();
        
        // 擷取查詢結果
        for (Map<String, Object> column : mapList) {
        	CRSTempVO crsTempVO = new CRSTempVO();
        	crsTempVO.setChangeId(Long.valueOf(changeId));
        	
        	Long targetId = MapUtils.getLong(column, "target_id");
        	Long surveyId = MapUtils.getLong(column, "survey_id");
        	String targetCertiCode = StringUtils.trim(MapUtils.getString(column, "target_certi_code", ""));
        	String targetName = StringUtils.trim(MapUtils.getString(column, "target_name"));
        	String policyCode = StringUtils.trim(MapUtils.getString(column, "policy_code",""));
        	
        	logWithId(id, "targetId = " + targetId + ", surveyId = " + surveyId
        			+ ", targetCertiCode = " + targetCertiCode + ",policyCode = " + policyCode + ", targetName = " + targetName );
        	
        	// IR-305650 當certi_code為空，以[changeId+保單號碼+受益人姓名]作為條件，檢查是否存在建檔紀錄，避免重複建檔
        	if (StringUtils.isBlank(targetCertiCode)) {
        		logWithId(id, "證號為空...");
        		if (crsBenefitSurveyListService.isCrsPartyLogExist(Long.valueOf(changeId), policyCode, targetName)) {
        			logWithId(id, "CrsPartyLog已有建檔紀錄，略過此筆");
        			continue;
        		}
        	}
        	
            FatcaDueDiligenceListVO fatcaVO = new FatcaDueDiligenceListVO();
            fatcaVO.setTargetSurveyType(FatcaSurveyType.THE_BENEFITS_OWNER);
            fatcaVO.setPolicyId(surveyId);
            fatcaVO.setPolicyCode(policyCode);
            fatcaVO.setPolicyStatus(MapUtils.getLong(column, "policy_status"));
            fatcaVO.setTargetId(targetId);
            fatcaVO.setTargetName(targetName);
            fatcaVO.setTargetCertiCode(targetCertiCode);
            fatcaVO.setTargetPartyType(MapUtils.getString(column,"target_party_type"));
            fatcaVO.setServiceAgent(MapUtils.getLong(column, "service_agent"));
            fatcaVO.setAgentRegisterCode(StringUtils.trim(MapUtils.getString(column, "agent_register_code", "")));
            fatcaVO.setAgentName(StringUtils.trim(MapUtils.getString(column, "agent_name", "")));
            BigDecimal agentBizCate = (BigDecimal) column.get("agent_biz_cate");
            if(agentBizCate != null) {
            	fatcaVO.setAgentBizCate(agentBizCate.longValue());
            }
            BigDecimal channelOrg = (BigDecimal) column.get("channel_org");
            if(channelOrg != null) {
            	fatcaVO.setChannelOrg(channelOrg.longValue());
            }
            fatcaVO.setChannelCode(StringUtils.trim(MapUtils.getString(column, "channel_code", "")));
            fatcaVO.setChannelName(StringUtils.trim(MapUtils.getString(column, "channel_name", "")));
            BigDecimal channelType = (BigDecimal)column.get("channel_type");
            if(channelType != null) {
            	fatcaVO.setChannelType(channelType.longValue());
            }
            fatcaVO.setRoleType(MapUtils.getString(column, "role_type"));
            fatcaVO.setTargetSurveyDate(processDate);
            
            // PCR257607 補10801及10802受益人調查產生CRS聲明書
            if ("00".equals(MapUtils.getString(column, "role_type"))){
        	    fatcaVO.setRoleType("03");
        	    crsTempVO.setTempBatch("Y");
            }
            
            // 查詢T_FATCA_DUE_DILIGENCE_LIST檢查目前有無紀錄
            FatcaDueDiligenceList resultFatcaBO = null;
            if (StringUtils.isBlank(targetCertiCode)) {
            	resultFatcaBO = fatcaBenefitSurveyListDAO.searchFatcaDueDiligenceList(targetId, surveyId, FatcaSurveyType.THE_BENEFITS_OWNER, processDate);
            } else {
            	resultFatcaBO = fatcaBenefitSurveyListDAO.searchFatcaDDListByCertiCode(targetCertiCode, surveyId, FatcaSurveyType.THE_BENEFITS_OWNER, processDate);
            }
            if (resultFatcaBO == null) {
            	// 新紀錄
            	crsTempVO.setNewRecord(true);
            	// 調查系統別為CRS
            	fatcaVO.setSurveySystemType(CrsBenefitSurveyListService.SURVEY_SYSTEM_TYPE_CRS);
            } else {
            	// 已有記錄
            	crsTempVO.setNewRecord(false);
            	fatcaVO.setListID(resultFatcaBO.getListID());
            	// 調查系統別為FATCA，表示是由FATCA批次建立資料，需更新SURVEY_SYSTEM_TYPE為CRS/FATCA，否則保持目前狀態(CRS or CRS/FATCA)
            	String surveySystemType = resultFatcaBO.getSurveySystemType();
            	if (StringUtils.equals(surveySystemType, CrsBenefitSurveyListService.SURVEY_SYSTEM_TYPE_FATCA)) {
            		fatcaVO.setSurveySystemType(CrsBenefitSurveyListService.SURVEY_SYSTEM_TYPE_ALL);
            	} else {
            		fatcaVO.setSurveySystemType(surveySystemType);
            	}
            }
            crsTempVO.setFatcaDueDiligenceListVO(fatcaVO);
            crsTempVOList.add(crsTempVO);
        }
        logWithId(id, "crsTempVOList.size() = " + crsTempVOList.size());
        
        UserTransaction trans = null;
        int successCount = 0;
        int errorCount = 0;
        for (CRSTempVO crsTempVO : crsTempVOList) {
        	try {
        		trans = TransUtils.getUserTransaction();
        		trans.begin();
        		FatcaDueDiligenceListVO fatcaVO = crsTempVO.getFatcaDueDiligenceListVO();
            	// 更新業務員名單
                crsBenefitSurveyListService.updateServiceAgent(fatcaVO);        
                // BR-CMN-FAT-027 產生受益人調查名單報表數據
                if (crsTempVO.isNewRecord()) {
                	logWithId(id, "Create new record to fatcaBenefitSurveyList.");
                	fatcaBenefitSurveyListDAO.createOrUpdateVO(fatcaVO);
                } else {
                	logWithId(id, "Update current record in fatcaBenefitSurveyList.");
                	fatcaBenefitSurveyListDAO.updateByVO(fatcaVO);
                }
                // BR-CMN-FAT-033 自動執行受益人 CRS建檔
                Long userId = AppContext.getCurrentUser().getUserId();
                crsBenefitSurveyListService.createCrsRecord(id, crsTempVO, userId, processDate);
                successCount++;
                trans.commit();
                logWithId(id, "CRS建檔完成");
        	} catch (Exception ex) {
        		logWithId(id, "發生異常! 錯誤訊息 = " + ExceptionUtils.getFullStackTrace(ex));
        		errorCount++;
        		trans.rollback();
        	} finally {
        		trans = null;
        	}
        }
        logWithId(id, "successCount = " + successCount + ", errorCount = " + errorCount);
        logWithId(id, "execute - end");
        
        if (errorCount == 0) {
			return JobStatus.EXECUTE_SUCCESS;
		} else if (successCount > 0) {
			return JobStatus.EXECUTE_PARTIAL_SUCESS;
		} else {
			return JobStatus.EXECUTE_FAILED;
		}
	}
	
	/**
	 * 父類別的transation是以一片為單位，為了避免同片內一筆rollback時，把上一筆的變更也一同rollback，覆寫父類別的方法
	 */
	@Override
    public int exeSingleRecord(String id) throws Exception {
        long start = System.currentTimeMillis();
        
        UserTransaction trans = null;
        int ret = 0;
        try {
            trans = Trans.getUserTransaction();
            trans.begin();
        
            ret = this.execute(id);
            this.updateStatus(id, ret);
            if (ret != JobStatus.EXECUTE_SUCCESS) {
                batchLogger.error("執行不成功! ID:{0} 狀態:{1}", id, ret);
            }
            long stop = System.currentTimeMillis();
            if(enableStopwatch()){
                batchLogger.debug("ID:{0} 執行花費時間:{1} ms", id, stop - start);
            }
            trans.commit();
        }catch(Exception e){
            trans.rollback();
            batchLogger.error("執行不成功! ID:{0} errMsg:{1}", id, e.toString());
        }
        return ret;
    }
	
	/**
	 * 更新t_batch_job_record的分片紀錄執行結果
	 * @param id
	 * @param status
	 */
	private void updateStatus(String id, int status) {
        Long runId = BatchHelp.getRunId();

        Session s = HibernateSession.currentSession();
        String sql = "update t_batch_job_record t set t.status=?, t.finish_time=sysdate where t.run_id = ? and t.identity=?";
        Connection con = s.connection();
        PreparedStatement ps = null;
        try {
            ps = con.prepareStatement(sql);
            ps.setInt(1, status);
            ps.setLong(2, runId);
            ps.setString(3, id);
            ps.execute();
        } catch (SQLException e) {
            //e.printStackTrace();
        	batchLogger.error("TGLBasePieceableJob.updateStatus error", e);
        } finally {
        	try {
        		if(ps!=null) {
        			ps.close();
        		} 		
        	} catch(Exception e) {
        		
        	}
        }
    }	
	
	public Date getProcessDate() {
		return processDate;
	}

	public void setProcessDate(Date processDate) {
		this.processDate = processDate;
	}
}