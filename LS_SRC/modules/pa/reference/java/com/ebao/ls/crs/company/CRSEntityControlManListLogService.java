package com.ebao.ls.crs.company;

import java.util.List;

import com.ebao.ls.crs.vo.CRSEntityControlManListLogVO;
import com.ebao.ls.foundation.service.GenericService;

public interface CRSEntityControlManListLogService extends GenericService<CRSEntityControlManListLogVO> {
	public final static String BEAN_DEFAULT = "crsEntityControlManListLogService";

	public List<CRSEntityControlManListLogVO> findByEntityLogId(Long entityLogId);
}
