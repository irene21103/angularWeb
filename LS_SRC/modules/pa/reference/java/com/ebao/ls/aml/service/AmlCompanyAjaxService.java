package com.ebao.ls.aml.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.beanutils.BeanPredicate;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.PredicateUtils;
import org.apache.log4j.Logger;
import org.hibernate.criterion.Restrictions;

import com.ebao.foundation.module.db.hibernate.HibernateSession3;
import com.ebao.ls.pa.nb.ctrl.pub.ajax.AjaxBaseService;
import com.ebao.ls.pa.nb.util.NBUtils;
import com.ebao.ls.pa.nb.util.NumericUtils;
import com.ebao.ls.pa.nb.util.UnbCst;
import com.ebao.ls.pa.pub.MsgCst;
import com.ebao.ls.pa.pub.TableCst;
import com.ebao.ls.pa.pub.bs.AddressService;
import com.ebao.ls.pa.pub.bs.NbTaskService;
import com.ebao.ls.pa.pub.bs.PolicyService;
import com.ebao.ls.pa.pub.bs.UnbAmlBeneficialOwnerService;
import com.ebao.ls.pa.pub.bs.UnbAmlCompanyService;
import com.ebao.ls.pa.pub.vo.UnbAmlBeneficialOwnerVO;
import com.ebao.ls.pa.pub.vo.UnbAmlCompanyVO;
import com.ebao.ls.pty.ci.CustomerCI;
import com.ebao.ls.pty.vo.AddressVO;
import com.ebao.ls.pty.vo.CompanyCustomerVO;
import com.ebao.ls.pty.vo.CustomerVO;
import com.ebao.ls.pub.CodeCst;
import com.ebao.ls.pub.cst.AddressFormat;
import com.ebao.ls.pub.cst.NBDummy;
import com.ebao.pub.util.StringUtils;
import com.ebao.pub.util.json.EbaoStringUtils;

/**
 * <p>Title: PCR_247911_e-Approval ITR-1801060_AML&CFT專案Q2需求</p>
 * <p>Description: 防制洗錢及打擊資恐法人聲明書</p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: TGL Co., Ltd.</p>
 * <p>Create Time: 2018年12月18日</p> 
 * @author 
 * <p>Update Time: 2018年12月18日</p>
 * <p>Updater: Simon.Huang</p>
 * <p>Update Comments: </p>
 */
public class AmlCompanyAjaxService extends AjaxBaseService {

	public static Logger logger = Logger.getLogger(AmlCompanyAjaxService.class);

	@Resource(name = UnbAmlCompanyService.BEAN_DEFAULT)
	protected UnbAmlCompanyService amlCompanyService;

	@Resource(name = UnbAmlBeneficialOwnerService.BEAN_DEFAULT)
	protected UnbAmlBeneficialOwnerService amlBeneficialOwnerService;

	@Resource(name = PolicyService.BEAN_DEFAULT)
	protected PolicyService policyService;

	@Resource(name = AddressService.BEAN_DEFAULT)
	private AddressService addressService;

	@Resource(name = CustomerCI.BEAN_DEFAULT)
	private CustomerCI customerCI;
	
	//PCR 364900-UNB補全件建檔任務
	@Resource(name = NbTaskService.BEAN_DEFAULT)
	protected NbTaskService nbTaskService;
	
	/**
	 * <p>Description : 防制洗錢及打擊資恐法人聲明書</p>
	 * <p>Created By : Simon.Huang</p>
	 * <p>Create Time : 2018年12月18日</p>
	 * @param in
	 * @param output
	 * @throws Exception
	 */
	public void init(Map<String, Object> in, Map<String, Object> output) throws Exception {

		Long policyId = NumericUtils.parseLong(in.get("policyId"));
		Long companyId = NumericUtils.parseLong(in.get("companyId"));
		Integer copies = NumericUtils.parseInteger(in.get("copies"));
		Long policyChgId = NumericUtils.parseLong(in.get("policyChgId"));
		
		if (policyId != null) {
			String policyCode = policyService.getPolicyCodeByPolicyId(policyId);
			UnbAmlCompanyVO amlCompanyVO = null;
			List<UnbAmlBeneficialOwnerVO> amlBeneOwnerList = new ArrayList<UnbAmlBeneficialOwnerVO>();

			if (policyChgId != null) {
				List<UnbAmlCompanyVO> companyList = amlCompanyService.findByPolicyIdAndPolicyChgId(policyId, policyChgId);
				if (companyId != null || copies != null) {
					for (UnbAmlCompanyVO companyVo : companyList) {
						if (companyId != null) {
							if (companyId.equals(companyVo.getListId())) {
								amlCompanyVO = companyVo;
								break;
							}
						} else if (copies != null) {
							if (copies.equals(companyVo.getCopy())) {
								amlCompanyVO = companyVo;
								break;
							}
						}
					}
				}

				if (amlCompanyVO == null) {
					amlCompanyVO = new UnbAmlCompanyVO();
				} else {
					amlBeneOwnerList = amlBeneficialOwnerService.findByPolicyIdAndPolicyChgId(
									policyId, policyChgId);

					CollectionUtils.filter(amlBeneOwnerList, new BeanPredicate("parentId", PredicateUtils.equalPredicate(amlCompanyVO.getListId())));
				}
			} else {

				if (companyId != null) {
					amlCompanyVO = amlCompanyService.load(companyId);

					amlBeneOwnerList = amlBeneficialOwnerService.findByPolicyIdAndCompanyId(
									policyId, companyId);
				} else {

					if (copies != null) {
						List<UnbAmlCompanyVO> companyList = amlCompanyService.find(
										Restrictions.eq("policyId", policyId),
										Restrictions.eq("copy", copies));
						if (companyList.size() > 0) {
							amlCompanyVO = companyList.get(0);
							amlBeneOwnerList = amlBeneficialOwnerService.findByPolicyIdAndCompanyId(
											policyId, amlCompanyVO.getListId());
						}
					}

					if (amlCompanyVO == null) {
						amlCompanyVO = new UnbAmlCompanyVO();
					}
				}
		
				Integer tmpCopies = copies ;
				if(tmpCopies == null) {
					tmpCopies = amlCompanyVO.getCopy();
					if(tmpCopies == null) {
						tmpCopies = amlCompanyService.getMaxCopy(policyId);
					}
				}
	
			}
			
			//將amlBeneOwnerList拆分成法人之高階管理人員amlSeniorManagementList/法人之實質受益人unbAmlBeneficialOwnerVOList
			List<UnbAmlBeneficialOwnerVO> amlSeniorManagementList = NBUtils.filterUnbAmlBeneficialOwnerVOListByType(amlBeneOwnerList, UnbCst.NB_AML_BENEFICIAL_OWNER_TYPE__SENIOR_MANAGEMENT);
			List<UnbAmlBeneficialOwnerVO> unbAmlBeneficialOwnerVOList = NBUtils.filterUnbAmlBeneficialOwnerVOListByType(amlBeneOwnerList, UnbCst.NB_AML_BENEFICIAL_OWNER_TYPE__BENEFICIAL_OWNER);

			output.put("policyCode", policyCode);
			output.put("amlCompany", amlCompanyVO);
			output.put("amlSeniorManagementList", amlSeniorManagementList);
			output.put("amlBeneOwnerList", unbAmlBeneficialOwnerVOList);

			Map<String, Object> addressInfo = new HashMap<String, Object>();
			AddressVO regAddress = null;
			AddressVO bizAddress = null;

			if (amlCompanyVO.getRegAddressId() != null) {
				regAddress = addressService.find(amlCompanyVO.getRegAddressId());
			} else {
				regAddress = new AddressVO();
			}
			if (amlCompanyVO.getBizAddressId() != null) {
				bizAddress = addressService.find(amlCompanyVO.getBizAddressId());
			} else {
				bizAddress = new AddressVO();
			}

			addressInfo.put("regAddress", regAddress.getAddress1());
			addressInfo.put("regAddressPostCode", regAddress.getPostCode());
			addressInfo.put("regAddressNationality", regAddress.getNationality());
			addressInfo.put("bizAddress", bizAddress.getAddress1());
			addressInfo.put("bizAddressPostCode", bizAddress.getPostCode());
			addressInfo.put("bizAddressNationality", bizAddress.getNationality());
			output.put("addressInfo", addressInfo);

			//列表用code table
			Map<String, Object> codeTableMap = new HashMap<String, Object>();
			Map<String, Map<String, String>> nationality = this.getNationalityCode(amlBeneOwnerList);
			codeTableMap.put("nationality", nationality);
			codeTableMap.put("nbYesNo", NBUtils.getTCodeData(TableCst.V_NB_YES_NO));
			output.put("codeTable", codeTableMap);

			if (policyChgId != null) {
				output.put("lockPae", CodeCst.YES_NO__YES);
			}
			output.put(KEY_SUCCESS, SUCCESS);
			return;
		}

		output.put(KEY_SUCCESS, FAIL);
		output.put(KEY_ERROR_MSG, NBUtils.getTWMsg(MsgCst.MSG_1250566));
		return;
	}

    /**
     * 
     * <p>Description :防制洗錢及打擊資恐法人聲明書- 法人資料   </p>
     * <p>Created By : Simon.Huang</p>
     * <p>Create Time : 2018年12月29日</p>
     * @param in
     * @param output
     * @throws Exception
     */
    public void saveCompany(Map<String, Object> in, Map<String, Object> output) throws Exception {

        Long policyId = NumericUtils.parseLong(in.get("policyId"));

        if (policyId != null) {

            String type = (String) in.get(KEY_TYPE);
            try {
                Map<String, Object> submitData = (Map<String, Object>) in.get(KEY_DATA);
                //頁面輸入資料
                Map<String, Object> amlCompany = (Map<String, Object>) submitData.get("amlCompany");
                Long companyId = NumericUtils.parseLong(submitData.get("companyId"));//法人存先存檔前會是空值
                if (UPDATE.equals(type)) {

                    UnbAmlCompanyVO amlCompanyVO = storeAmlCompany(policyId, companyId, amlCompany);
                    assignLogDataListId(in, amlCompanyVO.getListId());
                    output.put("amlCompany", amlCompanyVO);
                    output.put(KEY_SUCCESS, SUCCESS);
                    return;
                }
            } catch (Exception e) {
                throw e;
            } finally {

            }
        }
        output.put(KEY_SUCCESS, FAIL);
        output.put(KEY_ERROR_MSG, NBUtils.getTWMsg(MsgCst.MSG_1250566));
    }

    /**
     * <p>Description : 防制洗錢及打擊資恐法人聲明書-高階管理人員  </p>
     * <p>Created By : Vince.Cheng</p>
     * <p>Create Time : 2020年08月24日</p>
     * @param in
     * @param output
     * @throws Exception
     */
    public void saveSeniorManagement(Map<String, Object> in, Map<String, Object> output) throws Exception {

        Long policyId = NumericUtils.parseLong(in.get("policyId"));

        if (policyId != null) {
            String type = (String) in.get(KEY_TYPE);
            try {

                if (UPDATE.equals(type)) {

                    Map<String, Object> submitData = (Map<String, Object>) in.get(KEY_DATA);

                    //先處理法人資料存檔
                    Map<String, Object> amlCompany = (Map<String, Object>) submitData.get("amlCompany");
                    Long companyId = NumericUtils.parseLong(submitData.get("companyId"));//法人存先存檔前會是空值
                    UnbAmlCompanyVO amlCompanyVO = storeAmlCompany(policyId, companyId, amlCompany);

                    //在處理實質受益人存檔
                    Long entityId = NumericUtils.parseLong(submitData.get(KEY_ENTITY_ID));
                    Map<String, Object> amlSeniorManagement = (Map<String, Object>) submitData.get("amlSeniorManagement");
                    Map<String, Object> seniorManagement = (Map<String, Object>) amlSeniorManagement.get("seniorManagement");
                    UnbAmlBeneficialOwnerVO amlSeniorManagementVO = null;
                    if (entityId != null) {
                        amlSeniorManagementVO = amlBeneficialOwnerService.load(entityId);
                    } else {
                        amlSeniorManagementVO = new UnbAmlBeneficialOwnerVO();
                    }

                    EbaoStringUtils.setProperties(amlSeniorManagementVO, seniorManagement);
                    amlSeniorManagementVO.setParentId(amlCompanyVO.getListId());
                    amlSeniorManagementVO.setPolicyId(policyId);
                    amlSeniorManagementVO.setIsCompany((String) seniorManagement.get("isCompany"));
                    amlSeniorManagementVO.setType(UnbCst.NB_AML_BENEFICIAL_OWNER_TYPE__SENIOR_MANAGEMENT);
                    //generate party;
                    generateAmlBeneOwnerParty(amlSeniorManagementVO);
                    amlBeneficialOwnerService.saveOrUpdate(amlSeniorManagementVO);
                    assignLogDataListId(in, amlSeniorManagementVO.getListId());

                    output.put("amlCompany", amlCompanyVO);
                    output.put("amlSeniorManagement", amlSeniorManagementVO);

                    if (amlSeniorManagementVO.getNationality() != null) {
                        Map<String, Object> codeTableMap = new HashMap<String, Object>();
                        List<UnbAmlBeneficialOwnerVO> beneOwnerList = new ArrayList<UnbAmlBeneficialOwnerVO>();
                        beneOwnerList.add(amlSeniorManagementVO);
                        Map<String, Map<String, String>> nationality = this.getNationalityCode(beneOwnerList);
                        codeTableMap.put("nationality", nationality);
                        //列表用code table
                        output.put("codeTable", codeTableMap);
                    }

                    output.put(KEY_SUCCESS, SUCCESS);

                    return;
                } else if (DELETE.equals(type)) {

                    String deleteId = (String) in.get(KEY_DELETE_ID);
                    String listId[] = deleteId.split(",");
                    for (int i = 0; i < listId.length; i++) {
                        Long entityId = NumericUtils.parseLong(listId[i]);
                        if (entityId != null) {
                            amlBeneficialOwnerService.remove(entityId);
                            assignLogDataListId(in, entityId);
                        }
                    }
                    output.put(KEY_SUCCESS, SUCCESS);
                    return;
                }
            } catch (Exception e) {
                throw e;
            } finally {

            }
        }

        output.put(KEY_SUCCESS, FAIL);
        output.put(KEY_ERROR_MSG, NBUtils.getTWMsg(MsgCst.MSG_1250566));
    }

    /**
     * <p>Description : 防制洗錢及打擊資恐法人聲明書-法人之實質受益人  </p>
     * <p>Created By : Simon.Huang</p>
     * <p>Create Time : 2018年12月29日</p>
     * @param in
     * @param output
     * @throws Exception
     */
    public void saveBeneficialOwner(Map<String, Object> in, Map<String, Object> output) throws Exception {

        Long policyId = NumericUtils.parseLong(in.get("policyId"));

        if (policyId != null) {
            String type = (String) in.get(KEY_TYPE);
            try {

                if (UPDATE.equals(type)) {

                    Map<String, Object> submitData = (Map<String, Object>) in.get(KEY_DATA);

                    //先處理法人資料存檔
                    Map<String, Object> amlCompany = (Map<String, Object>) submitData.get("amlCompany");
                    Long companyId = NumericUtils.parseLong(submitData.get("companyId"));//法人存先存檔前會是空值
                    UnbAmlCompanyVO amlCompanyVO = storeAmlCompany(policyId, companyId, amlCompany);

                    //在處理實質受益人存檔
                    Long entityId = NumericUtils.parseLong(submitData.get(KEY_ENTITY_ID));
                    Map<String, Object> amlBeneOwner = (Map<String, Object>) submitData.get("amlBeneOwner");
                    Map<String, Object> beneficialOwner = (Map<String, Object>) amlBeneOwner.get("beneficialOwner");
                    UnbAmlBeneficialOwnerVO amlBeneOwnerVO = null;
                    if (entityId != null) {
                        amlBeneOwnerVO = amlBeneficialOwnerService.load(entityId);
                    } else {
                        amlBeneOwnerVO = new UnbAmlBeneficialOwnerVO();
                    }

                    EbaoStringUtils.setProperties(amlBeneOwnerVO, beneficialOwner);
                    amlBeneOwnerVO.setParentId(amlCompanyVO.getListId());
                    amlBeneOwnerVO.setPolicyId(policyId);
                    amlBeneOwnerVO.setIsCompany((String) beneficialOwner.get("isCompany"));
                    amlBeneOwnerVO.setType(UnbCst.NB_AML_BENEFICIAL_OWNER_TYPE__BENEFICIAL_OWNER);
                    //generate party;
                    generateAmlBeneOwnerParty(amlBeneOwnerVO);
                    amlBeneficialOwnerService.saveOrUpdate(amlBeneOwnerVO);
                    assignLogDataListId(in, amlBeneOwnerVO.getListId());

                    output.put("amlCompany", amlCompanyVO);
                    output.put("amlBeneOwner", amlBeneOwnerVO);

                    if (amlBeneOwnerVO.getNationality() != null) {
                        Map<String, Object> codeTableMap = new HashMap<String, Object>();
                        List<UnbAmlBeneficialOwnerVO> beneOwnerList = new ArrayList<UnbAmlBeneficialOwnerVO>();
                        beneOwnerList.add(amlBeneOwnerVO);
                        Map<String, Map<String, String>> nationality = this.getNationalityCode(beneOwnerList);
                        codeTableMap.put("nationality", nationality);
                        //列表用code table
                        output.put("codeTable", codeTableMap);
                    }

                    output.put(KEY_SUCCESS, SUCCESS);

                    return;
                } else if (DELETE.equals(type)) {

                    String deleteId = (String) in.get(KEY_DELETE_ID);
                    String listId[] = deleteId.split(",");
                    for (int i = 0; i < listId.length; i++) {
                        Long entityId = NumericUtils.parseLong(listId[i]);
                        if (entityId != null) {
                            amlBeneficialOwnerService.remove(entityId);
                            assignLogDataListId(in, entityId);
                        }
                    }
                    output.put(KEY_SUCCESS, SUCCESS);
                    return;
                }
            } catch (Exception e) {
                throw e;
            } finally {

            }
        }

        output.put(KEY_SUCCESS, FAIL);
        output.put(KEY_ERROR_MSG, NBUtils.getTWMsg(MsgCst.MSG_1250566));
    }

	/**
	 * <p>Description : 寫入法人資料 </p>
	 * <p>Created By : Simon.Huang</p>
	 * <p>Create Time : 2018年12月29日</p>
	 * @param policyId
	 * @param companyId
	 * @param amlCompany
	 * @return
	 * @throws Exception
	 */
	private UnbAmlCompanyVO storeAmlCompany(Long policyId, Long companyId, Map<String, Object> amlCompany)
					throws Exception {

		UnbAmlCompanyVO amlCompanyVO = null;
		if (companyId != null) {
			amlCompanyVO = amlCompanyService.load(companyId);
		} else {
			amlCompanyVO = new UnbAmlCompanyVO();
			amlCompanyVO.setPolicyId(policyId);
			amlCompanyVO.setBoPartyId(NBDummy.DUMMY_PARTYID_FOR_COMPANY);
			amlCompanyVO.setCopy(amlCompanyService.getMaxCopy(policyId));
		}
		EbaoStringUtils.setProperties(amlCompanyVO, amlCompany);
		//地址處理
		amlCompanyVO.setRegAddressId(getAddressIdByNationality(
						(String) amlCompany.get("regAddress"),
						(String) amlCompany.get("regAddressPostCode"),
						(String) amlCompany.get("regAddressNationality")));
		amlCompanyVO.setBizAddressId(getAddressIdByNationality(
						(String) amlCompany.get("bizAddress"),
						(String) amlCompany.get("bizAddressPostCode"),
						(String) amlCompany.get("bizAddressNationality")));
		//generate party;
		generateAmlCompanyParty(amlCompanyVO);
		amlCompanyVO = amlCompanyService.saveOrUpdate(amlCompanyVO);

		HibernateSession3.currentSession().flush();

		return amlCompanyVO;
	}

	/**
	 * <p>Description : 產生法人party</p>
	 * <p>Created By : Simon.Huang</p>
	 * <p>Create Time : 2019年1月9日</p>
	 * @param amlCompanyVO
	 */
	private void generateAmlCompanyParty(UnbAmlCompanyVO amlCompanyVO) {

		Long partyId = NBDummy.DUMMY_PARTYID_FOR_COMPANY;
		amlCompanyVO.setBoPartyId(partyId);
		if (StringUtils.isNullOrEmpty(amlCompanyVO.getCompanyNo())) {
			return;
		}

		//法人
		partyId = policyRoleHelper.findCompanyPartyIdByCondition(amlCompanyVO.getCompanyNo());
		if (NumericUtils.isDummyPartyId(partyId)) {
			CompanyCustomerVO companyCustomerVO = new CompanyCustomerVO();

			companyCustomerVO.setRegisterCode(amlCompanyVO.getCompanyNo());
			companyCustomerVO.setCompanyName(amlCompanyVO.getCompanyName());
			companyCustomerVO.setIsGov(null);
			companyCustomerVO.setCompanyCategory(amlCompanyVO.getCompanyCategory());
			companyCustomerVO.setBearerIndi(null);
			companyCustomerVO.setBearerPercentage(null);

			Long regAddressId = amlCompanyVO.getRegAddressId();
			Long bizAddressId = amlCompanyVO.getBizAddressId();
			companyCustomerVO.setRegAddressId(regAddressId);
			companyCustomerVO.setBizAddressId(bizAddressId);
			partyId = customerCI.addCompany(companyCustomerVO);
			
		}
		
		amlCompanyVO.setBoPartyId(partyId);
	}

	/**
	 * <p>Description : 產生實質受益人party</p>
	 * <p>Created By : Simon.Huang</p>
	 * <p>Create Time : 2019年1月9日</p>
	 * @param amlBeneOwnerVO
	 */
	private void generateAmlBeneOwnerParty(UnbAmlBeneficialOwnerVO amlBeneOwnerVO) {

		String isCompany = amlBeneOwnerVO.getIsCompany();
		String identityNo = amlBeneOwnerVO.getIdentityNo();
		Long partyId = null;
		if (NBUtils.in(CodeCst.YES_NO__NO, isCompany)) {
			partyId = NBDummy.DUMMY_PARTYID_FOR_PERSON;

			if (!StringUtils.isNullOrEmpty(identityNo)) {
				partyId = policyRoleHelper.findCustomerPartyIdByCondition(identityNo,
								amlBeneOwnerVO.getName(), amlBeneOwnerVO.getBirthDate());

				if (NumericUtils.isDummyPartyId(partyId)) {
					CustomerVO customerVO = new CustomerVO();
					customerVO.setCertiCode(identityNo);
					customerVO.setFirstName(amlBeneOwnerVO.getName());
					customerVO.setRomanName(amlBeneOwnerVO.getRomanName());
					customerVO.setBirthday(amlBeneOwnerVO.getBirthDate());
					customerVO.setNationality(amlBeneOwnerVO.getNationality());
					if (!StringUtils.isNullOrEmpty(identityNo)) {
						customerVO.setCertiType(certiCodeCI.getCertiType(identityNo));
					}
					// 新增自然人
					partyId = customerCI.addPerson(customerVO);
				}
			}

		} else {
			partyId = NBDummy.DUMMY_PARTYID_FOR_COMPANY;
			if (!StringUtils.isNullOrEmpty(identityNo)) {
				partyId = policyRoleHelper.findCompanyPartyIdByCondition(identityNo);

				if (NumericUtils.isDummyPartyId(partyId)) {
					CompanyCustomerVO companyCustomerVO = new CompanyCustomerVO();
					companyCustomerVO.setRegisterCode(identityNo);
					companyCustomerVO.setCompanyName(amlBeneOwnerVO.getName());
					companyCustomerVO.setIsGov(null);
					companyCustomerVO.setCompanyCategory(null);
					companyCustomerVO.setBearerIndi(null);
					companyCustomerVO.setBearerPercentage(null);
					companyCustomerVO.setNationality(amlBeneOwnerVO.getNationality());
					partyId = customerCI.addCompany(companyCustomerVO);
				}
			}
		}

		amlBeneOwnerVO.setPartyId(partyId);

	}

	private Long getAddressIdByNationality(String address1, String postCode, String nationality) {
		AddressVO addressVO = new AddressVO();
		addressVO.setAddress1(address1);
		addressVO.setPostCode(postCode);
		addressVO.setNationality(nationality);
		addressVO.setAddressFormat(AddressFormat.ADDRESSFORMAT_FREE);
		addressVO = addressService.saveOrUpdateAddress(addressVO);
		return addressVO.getAddressId();
	}

	protected Map<String, Map<String, String>> getNationalityCode(List<UnbAmlBeneficialOwnerVO> beneOwnerList)
					throws Exception {
		Map<String, Map<String, String>> nationality = new HashMap<String, Map<String, String>>();
		String whereClause = "";
		for (UnbAmlBeneficialOwnerVO beneOwnerVO : beneOwnerList) {
			if (beneOwnerVO.getNationality() != null) {
				if (whereClause.length() > 0) {
					whereClause += ",";
				}
				whereClause += "'" + beneOwnerVO.getNationality() + "'";
			}
		}
		if (whereClause.length() > 0) {
			whereClause = "NATIONALITY_CODE IN (" + whereClause + ")";
			nationality.putAll(NBUtils.getTCodeData(TableCst.V_NB_NATIONALITY_CODE, whereClause));
		}
		return nationality;
	}

}
