package com.ebao.ls.uw.ctrl.listUpload;

import java.util.List;

import org.apache.struts.upload.FormFile;

import com.ebao.ls.uw.ctrl.listUpload.vo.AGYRetiredAgentExtendVO;
import com.ebao.pub.web.pager.PagerFormImpl;

public class AGYRetiredAgentUploadForm extends PagerFormImpl {

	/**
	 * <code>serialVersionUID</code> 的註解
	 */
	private static final long serialVersionUID = 1422280701803950434L;

	private List<AGYRetiredAgentExtendVO> voList;
	
	private List<AGYRetiredAgentExtendVO> errList;
	
	private FormFile uploadFile;
	
	private Long batchNo;

	public List<AGYRetiredAgentExtendVO> getVoList() {
		return voList;
	}

	public void setVoList(List<AGYRetiredAgentExtendVO> voList) {
		this.voList = voList;
	}

	public FormFile getUploadFile() {
		return uploadFile;
	}

	public void setUploadFile(FormFile uploadFile) {
		this.uploadFile = uploadFile;
	}

	public List<AGYRetiredAgentExtendVO> getErrList() {
		return errList;
	}

	public void setErrList(List<AGYRetiredAgentExtendVO> errList) {
		this.errList = errList;
	}

	public Long getBatchNo() {
		return batchNo;
	}

	public void setBatchNo(Long batchNo) {
		this.batchNo = batchNo;
	}

}
