package com.ebao.ls.beneOwner.ctrl;

import java.util.ArrayList;
import java.util.List;

import com.ebao.ls.pty.vo.CompanyCustomerVO;
import com.ebao.pub.web.pager.PagerFormImpl;

public class BeneOwnerOpenForm extends PagerFormImpl {

	public BeneOwnerOpenForm() {
		this.companyCustomerVOList = new ArrayList<CompanyCustomerVO>();
	}

	private String registerCodeSearch;

	/**
	 * companyCustomerVO list. for search.
	 */
	private List<CompanyCustomerVO> companyCustomerVOList;

	public List<CompanyCustomerVO> getCompanyCustomerVOList() {
		return companyCustomerVOList;
	}

	public void setCompanyCustomerVOList(List<CompanyCustomerVO> companyCustomerVOList) {
		this.companyCustomerVOList = companyCustomerVOList;
	}

	public String getRegisterCodeSearch() {
		return registerCodeSearch;
	}

	public void setRegisterCodeSearch(String registerCodeSearch) {
		this.registerCodeSearch = registerCodeSearch;
	}

}
