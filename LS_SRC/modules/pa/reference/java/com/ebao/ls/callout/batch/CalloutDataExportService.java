package com.ebao.ls.callout.batch;

import java.util.Map;

import com.ebao.pub.framework.GenericException;


public interface CalloutDataExportService {
    
    public final static String BEAN_DEFAULT = "calloutDataExportService";
    
    public abstract void feedMap(java.util.List<Map<String, Object>> map, Long typeId, java.util.Date processDate)  throws GenericException;
    
}
