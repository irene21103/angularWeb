package com.ebao.ls.uw.ctrl.listUpload;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.UserTransaction;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.Row;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.foundation.module.util.beanutils.BeanUtils;
import com.ebao.ls.pa.nb.vo.Qe7ProjectVO;
import com.ebao.ls.pub.CodeCst;
import com.ebao.ls.uw.ctrl.listUpload.helper.ListUploadActionHelper;
import com.ebao.ls.uw.ctrl.listUpload.vo.QE7ProjectExtendVO;
import com.ebao.ls.uw.ds.Qe7ProjectService;
import com.ebao.pub.util.TransUtils;

public class QE7ProjectUploadAction extends
		ListUploadActionHelper<QE7ProjectExtendVO, QE7ProjectUploadForm> {

	@Resource(name = Qe7ProjectService.BEAN_DEFAULT)
	Qe7ProjectService qe7ProjectService;
	/**
	 * 上傳的檔案內容存在 Session 裡的名字。
	 */
	private static final String UPLOAD_VO_SESSION_KEY = "UPLOAD_VO_SESSION_KEY_1000008397 ";

	private static final String ERR_UPLOAD_VO_SESSION_KEY = "ERROR_UPLOAD_VO_SESSION_KEY_1000008397 ";

	// 存放於Request的錯誤資訊KEY
	private static final String ERR_MSG_LABEL = "fileErrMsg";

	// 存放於Request的執行資訊KEY
	private static final String PROC_MSG_LABEL = "processMsg";

	// 規範Excel存取欄位數量
	private static final int EXCEL_COLUMN_NUM = 4;

	@Override
	public ActionForward process(ActionMapping mapping, ActionForm actionForm,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		QE7ProjectUploadForm form = (QE7ProjectUploadForm) actionForm;

	    if (StringUtils.equals("upload", form.getActionType())) { // 上傳。

			// 每次重讀都清掉上次的資料內容。
			cleanSession(request);
			
			List<QE7ProjectExtendVO> voList = new ArrayList<QE7ProjectExtendVO>();
			
			UserTransaction ut = null;
			try {

				ut = TransUtils.getUserTransaction();
				ut.begin();
				qe7ProjectService.removeAll();
				voList = super.readExcelFile(actionForm, request, form.getUploadFile());
				ut.commit();
				
			} catch (Exception e) {
				TransUtils.rollback(ut);
				throw e;
			}
			
			form.setErrList(super.getErrorList(voList));
			setSession(request, form);

			request.setAttribute("errLists", form.getErrList());
			request.setAttribute("errSize", form.getErrList().size());
			request.setAttribute("totalSize", voList.size());

		} else {

			// 每次進來都把 session 裡之前上傳的檔案內容清掉。
			cleanSession(request);

		}

		return mapping.findForward("display");
	}

	@Override
	protected QE7ProjectExtendVO readRowData(Row row, Integer countNum, ActionForm actionForm) {
		
		QE7ProjectUploadForm form = (QE7ProjectUploadForm) actionForm;
		QE7ProjectExtendVO uploadVO = new QE7ProjectExtendVO();
        uploadVO.setSerialNumber((long) countNum);
        Integer columnPointer = 0;
        
        try {
        	uploadVO.setCertiCode(super.getCellValue(row, columnPointer++, String.class));
        	uploadVO.setName(super.getCellValue(row, columnPointer++, String.class));
        	uploadVO.setPolicyCode(super.getCellValue(row, columnPointer++, String.class));
        	uploadVO.setPayDueDate(super.getCellValue(row, columnPointer++, Date.class));
        	uploadVO.setBatchNo(form.getBatchNo());
        	
        	String checkStr = "";
        	
        	if(StringUtils.isEmpty(uploadVO.getPolicyCode())) {
        		checkStr += "資料不可為空 !! (欄數:" + countNum + " 保單號碼)\n";
        	} 
        	
        	if(StringUtils.isEmpty(uploadVO.getCertiCode())) {
        		checkStr += "資料不可為空 !! (欄數:" + countNum + " 身份證字號)\n";
        	} 
        	
        	if(StringUtils.isNotEmpty(checkStr)){
            	uploadVO.setErrorMsg(checkStr);
            	uploadVO.setIsQualify(CodeCst.YES_NO__NO);
            }  else {
            	uploadVO.setIsQualify(CodeCst.YES_NO__YES);
            }
        	
        } catch (Exception e) {
        	uploadVO.setErrorMsg("資料格式錯誤 !! (行數:" + countNum + " 欄數:" + columnPointer + ")");
        	uploadVO.setIsQualify(CodeCst.YES_NO__NO);
        }
        
        try {
        	
        	if(CodeCst.YES_NO__YES.equals(uploadVO.getIsQualify())) {
            	
            	Qe7ProjectVO vo = new Qe7ProjectVO();
    			BeanUtils.copyProperties(vo, uploadVO);
    			qe7ProjectService.save(vo, true);
    			
            }
        	
        } catch (Exception e) {
        	uploadVO.setErrorMsg("資料儲存錯誤! 請檢核保單號碼是否有誤! (行數:" + countNum + ")");
        	uploadVO.setIsQualify(CodeCst.YES_NO__NO);
        }
        
		return uploadVO;
	}

	@Override
	protected void getVoList(HttpServletRequest request, QE7ProjectUploadForm form) {
		form.setVoList(getVoList(request, UPLOAD_VO_SESSION_KEY));
		form.setErrList(getVoList(request, ERR_UPLOAD_VO_SESSION_KEY));
	}

	@Override
	protected void cleanSession(HttpServletRequest request) {
		request.getSession().setAttribute(UPLOAD_VO_SESSION_KEY, null);
		request.getSession().setAttribute(ERR_UPLOAD_VO_SESSION_KEY, null);
	}

	@Override
	protected void setSession(HttpServletRequest request,
			QE7ProjectUploadForm form) {
		request.getSession().setAttribute(UPLOAD_VO_SESSION_KEY, form.getVoList());
		request.getSession().setAttribute(ERR_UPLOAD_VO_SESSION_KEY, form.getErrList());
	}

	@Override
	protected int getExcelCellSize() {
		return EXCEL_COLUMN_NUM;
	}

	@Override
	protected String getFileErrMsgLable() {
		return ERR_MSG_LABEL;
	}

	@Override
	protected String getProcessMsgLable() {
		return PROC_MSG_LABEL;
	}

	@Override
	protected SimpleDateFormat getDateFormat() {
		return new SimpleDateFormat("yyyyMMdd");
	}

	@Override
	protected boolean errorCondition(QE7ProjectExtendVO t) {
		return StringUtils.isNotEmpty(t.getErrorMsg());
	}

}
