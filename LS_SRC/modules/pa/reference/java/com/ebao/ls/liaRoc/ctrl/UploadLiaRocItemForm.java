package com.ebao.ls.liaRoc.ctrl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.Factory;
import org.apache.commons.collections.ListUtils;

import com.ebao.pub.framework.GenericForm;

/**
 * <h1>單筆上傳公會通報</h1><p>
 * @since getDate()<p>
 * @author Caby Chen
 * <p>Update Time: </p>
 * <p>Updater: </p>
 * <p>Update Comments: </p>
*/
@SuppressWarnings("serial")
public class UploadLiaRocItemForm extends GenericForm {

	String actionType;

	String policyCode;

	String liarocUploadType;

	String tabType;

	String retMsg;
	
	Integer[] itemOrders;

	List<UploadLiaRocItemVO> uploadLiaRocItemVO;

	@SuppressWarnings("unchecked")
	public List<UploadLiaRocItemVO> getUploadLiaRocItemVO() {
		if (uploadLiaRocItemVO == null) {
			uploadLiaRocItemVO = ListUtils.lazyList(new ArrayList<UploadLiaRocItemVO>(), new Factory() {
				@Override
				public Object create() {
					return new UploadLiaRocItemVO();
				}
			});
		}
		return uploadLiaRocItemVO;
	}

	public void setUploadLiaRocItemVO(List<UploadLiaRocItemVO> uploadLiaRocVO) {
		this.uploadLiaRocItemVO = uploadLiaRocVO;
	}

	public String getActionType() {
		return actionType;
	}

	public void setActionType(String actionType) {
		this.actionType = actionType;
	}

	public String getPolicyCode() {
		return policyCode;
	}

	public void setPolicyCode(String policyCode) {
		this.policyCode = policyCode;
	}

	public String getLiarocUploadType() {
		return liarocUploadType;
	}

	public void setLiarocUploadType(String liarocUploadType) {
		this.liarocUploadType = liarocUploadType;
	}

	public String getTabType() {
		return tabType;
	}

	public void setTabType(String tabType) {
		this.tabType = tabType;
	}

	public String getRetMsg() {
		return retMsg;
	}

	public void setRetMsg(String retMsg) {
		this.retMsg = retMsg;
	}

	public Integer[] getItemOrders() {
		return itemOrders;
	}

	public void setItemOrders(Integer[] itemOrders) {
		this.itemOrders = itemOrders;
	}

}
