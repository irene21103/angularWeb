package com.ebao.ls.uw.ctrl.extraloading;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ebao.ls.escape.helper.EscapeHelper;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.ls.pa.pub.service.PolicyService;
import com.ebao.ls.pub.CodeCst;
import com.ebao.ls.uw.ctrl.ActionUtil;
import com.ebao.ls.uw.ctrl.pub.CustomerForm;
import com.ebao.ls.uw.ctrl.pub.UwExtraLoadingForm;
import com.ebao.ls.uw.ds.constant.UwExceptionConstants;
import com.ebao.ls.uw.ds.sp.CompleteUWProcessSp;
import com.ebao.ls.uw.ds.sp.QueryUwDataSp;
import com.ebao.ls.uw.util.Utils;
import com.ebao.ls.uw.vo.UwExtraLoadingVO;
import com.ebao.ls.uw.vo.UwPolicyVO;
import com.ebao.ls.uw.vo.UwProductVO;
import com.ebao.pub.framework.AppException;
import com.ebao.pub.framework.ExceptionFactory;
import com.ebao.pub.framework.GenericException;
import com.ebao.pub.util.BeanUtils;

/**
 * 
 * <p>
 * Title:Underwriting
 * </p>
 * <p>
 * Description:
 * </p>
 * <p>
 * Copyright: Copyright (c) 2004
 * </p>
 * <p>
 * Company: eBaoTech Corporation
 * </p>
 * 
 * @author luik lu
 * @since Jun 27, 2005
 * @version 1.0
 */

public class RefreshExtraAction extends ReloadLoadingAction {

  /**
   * refresh extra loading action
   * 
   * @param mapping
   * @param form
   * @param req
   * @param res
   * @throws com.ebao.pub.framework.GenericException
   * @return
   */
  public ActionForward uwProcess(ActionMapping mapping, ActionForm form,
      HttpServletRequest req, HttpServletResponse res) throws GenericException {
    try {
      Long underwriteId = Long.valueOf(com.ebao.ls.encode.encoder.Encode.encodeForHTML(req).getParameter("underwriteId"));
      Long itemId = Long.valueOf(com.ebao.ls.encode.encoder.Encode.encodeForHTML(req).getParameter("itemIds"));
      Long insuredId = Long.valueOf(com.ebao.ls.encode.encoder.Encode.encodeForHTML(req).getParameter("insured"));
      UwProductVO productValue = initProductVO(req);
      Collection insureds = getUwPolicyDS().findUwLifeInsuredEntitis(
          underwriteId, itemId);
      insureds = BeanUtils.copyCollection(CustomerForm.class, insureds);
      CustomerForm[] insured = (CustomerForm[]) insureds
          .toArray(new CustomerForm[insureds.size()]);
      req.setAttribute("insureds", insured);
      req.setAttribute("insured", insuredId);

      UwExtraLoadingInfoForm extraForm;
      // to do calculate the extraRate
      //			String[] emValue = req.getParameterValues("emValue");
      //			String[] extraArith = req.getParameterValues("extraArith");
      String gender = req.getParameter("gender");
      if ("Y".equals(req.getParameter("calRate"))) {
          //計算EM率
        List<UwExtraLoadingVO> extraLoadVOs = productValue.getUwExtraLoadings();
        for (int i = 0; i < extraLoadVOs.size(); i++) {
          if ((extraLoadVOs.get(i).getEmValue() != null)
              && (extraLoadVOs.get(i).getEmValue().intValue() >= 0)
              && ("1".equals(extraLoadVOs.get(i).getExtraArith()))
              && (CodeCst.EXTRA_TYPE__HEALTH_EXTRA.equals(extraLoadVOs.get(i).getExtraType()))) {
            extraLoadingActionHelper.doCalExRate(productValue, gender, i,
                extraLoadVOs.get(i).getExtraArith(), insuredId, true);
          }
        }
      } else {
          //計算總額外保費
        // calculate the extra prem
        UwExtraLoadingVO[] loadingVOs = ActionUtil.calAndSaveExtraLoading(req,
            insuredId);
        productValue.setUwExtraLoadings(Arrays.asList(loadingVOs));
      }

      //when cs-uw,synchronize records
      UwPolicyVO uwPolicyVO = getUwPolicyDS().findUwPolicy(underwriteId);
      if (Utils.isCsUw(uwPolicyVO.getUwSourceType())) {
        CompleteUWProcessSp.synUWInfo4CS(uwPolicyVO.getPolicyId(), uwPolicyVO
            .getUnderwriteId(), uwPolicyVO.getChangeId(), Integer.valueOf(2));
      }

      // get the extra loading form
      extraForm = decompose(productValue, req);

      // do add extra layer if need
      if ((req.getParameter("addExtraType") != null)
          && (req.getParameter("addExtraType").length() > 0)) {
        EscapeHelper.escapeHtml(req).setAttribute("addExtraType", req.getParameter("addExtraType"));
        doAddExtraLayer(req.getParameter("addExtraType"), extraForm, req);
      }
      EscapeHelper.escapeHtml(req).setAttribute("action_form", extraForm);

      String uwSourceType = req.getParameter("uwSourceType");
      if (!CodeCst.UW_SOURCE_TYPE__NB.equals(uwSourceType)) {
        setDueDateList(productValue.getPolicyId(), req);
      }

    } catch (Exception e) {
      throw ExceptionFactory.parse(e);
    }

    return mapping.findForward("extra");
  }

  /**
   * init the product vo
   * 
   * @param req
   * @return
   * @throws Exception
   */
  public UwProductVO initProductVO(HttpServletRequest req) throws Exception {
    Long underwriteId = Long.valueOf(com.ebao.ls.encode.encoder.Encode.encodeForHTML(req).getParameter("underwriteId"));
    Long itemId = Long.valueOf(com.ebao.ls.encode.encoder.Encode.encodeForHTML(req).getParameter("itemIds"));
    Long insuredId = Long.valueOf(com.ebao.ls.encode.encoder.Encode.encodeForHTML(req).getParameter("insured"));
    req.setAttribute("insured", insuredId);
    UwProductVO productValue = getUwPolicyDS().findUwProduct(underwriteId,
        itemId);
    // set duration limit
    req.setAttribute("durationScope", QueryUwDataSp.getPremTerm(productValue,policyService.getActiveVersionDate(productValue)));
    // get the special product extraLoading ariths
    String[] ariths = extraLoadingActionHelper.getExtraAriths(Long
        .valueOf(productValue.getProductId().intValue()),policyService.getActiveVersionDate(productValue));
    String arithsFlag = "";
    if (ariths.length == 0) {
      arithsFlag = "true";
    }
    String sqlAriths = "ADD_ARITH in (";
    for (int i = 0; i < ariths.length - 1; i++) {
      sqlAriths = sqlAriths + "'" + ariths[i] + "'" + ",";
    }
    if (!"true".equals(arithsFlag)) {
      sqlAriths = sqlAriths + "'" + ariths[ariths.length - 1] + "'" + ")";
    } else {
      sqlAriths = sqlAriths + "99)";
    }

    req.setAttribute("sqlAriths", sqlAriths);
    req.setAttribute("arithsFlag", arithsFlag);

    UwExtraLoadingVO[] extraLoadingVOs;

    if ("true".equals(req.getParameter("isLoading"))) {
      extraLoadingVOs = ActionUtil.calAndSaveExtraLoading(req, insuredId);
    } else {
      Collection allExtra = getUwPolicyDS().findUwExtraLoadingEntitis(
          underwriteId, itemId, insuredId);
      extraLoadingVOs = (UwExtraLoadingVO[]) allExtra
          .toArray(new UwExtraLoadingVO[allExtra.size()]);
    }
    if (extraLoadingVOs == null) {
      extraLoadingVOs = new UwExtraLoadingVO[0];
    }
    productValue.setUwExtraLoadings(Arrays.asList(extraLoadingVOs));
    return productValue;
  }

  /**
   * add the layer
   * 
   * @param extraType
   *            String
   * @param extraLoadingInfoForm
   *            UwExtraLoadingInfoForm
   */
  public void doAddExtraLayer(String extraType,
      UwExtraLoadingInfoForm extraLoadingInfoForm, HttpServletRequest req)
      throws GenericException {
    // if uwSourceType is NB ,the healthy layer's number must be more than 2
    if ("1".equals(req.getParameter("uwSourceType"))) {
      if (extraLoadingInfoForm.getHealth().size() > 1) {
        req = null;
        extraLoadingInfoForm = null;
        throw new AppException(UwExceptionConstants.APP_ONLY_LAYER,
            "Layer 1 and Layer 2 are applicable to health loading only!");
      }
    }

    switch (extraType.toCharArray()[0]) {
      case 'A' :
        extraLoadingInfoForm.getHealth().add(new UwExtraLoadingForm());
        extraLoadingInfoForm.setHealthFlag("Y");
        break;
      case 'B' :
        extraLoadingInfoForm.getOccupation().add(new UwExtraLoadingForm());
        extraLoadingInfoForm.setOccupationFlag("Y");
        break;
      case 'C' :
        extraLoadingInfoForm.getAvocation().add(new UwExtraLoadingForm());
        extraLoadingInfoForm.setAvocationFlag("Y");
        break;
      case 'D' :
        extraLoadingInfoForm.getResidential().add(new UwExtraLoadingForm());
        extraLoadingInfoForm.setResidentialFlag("Y");
        break;
      case 'E' :
        extraLoadingInfoForm.getAviation().add(new UwExtraLoadingForm());
        extraLoadingInfoForm.setAviationFlag("Y");
        break;
      case 'F' :
        extraLoadingInfoForm.getOther().add(new UwExtraLoadingForm());
        extraLoadingInfoForm.setOtherFlag("Y");
        break;
      default :
        break;
    }
  }

  //  	private UwPolicyDS uwPolicyDS;
  //	public void setUwPolicyDS(UwPolicyDS uwPolicyDS){
  //		this.uwPolicyDS = uwPolicyDS;
  //	}

  @Resource(name = PolicyService.BEAN_DEFAULT)
  PolicyService policyService;
  
  @Resource(name = ExtraLoadingActionHelper.BEAN_DEFAULT)
  ExtraLoadingActionHelper extraLoadingActionHelper;
}