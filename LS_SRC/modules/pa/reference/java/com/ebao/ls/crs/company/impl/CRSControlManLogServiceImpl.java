package com.ebao.ls.crs.company.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.collections4.CollectionUtils;
import org.hibernate.criterion.Restrictions;

import com.ebao.foundation.commons.exceptions.GenericException;
import com.ebao.foundation.module.db.DBean;
import com.ebao.ls.crs.company.CRSControlManLogService;
import com.ebao.ls.crs.data.bo.CRSControlManLog;
import com.ebao.ls.crs.data.bo.CRSManAccountLog;
import com.ebao.ls.crs.data.bo.CRSPartyLog;
import com.ebao.ls.crs.data.company.CRSControlManLogDAO;
import com.ebao.ls.crs.data.company.CRSManAccountLogDAO;
import com.ebao.ls.crs.data.identity.CRSPartyLogDAO;
import com.ebao.ls.crs.vo.CRSControlManLogVO;
import com.ebao.ls.crs.vo.CRSManAccountLogVO;
import com.ebao.ls.crs.vo.CRSPartyLogVO;
import com.ebao.ls.foundation.service.GenericServiceImpl;
import com.ebao.ls.foundation.util.BeanConvertor;
import com.ebao.ls.pub.cst.YesNo;
import com.ebao.ls.pub.util.HashMapBuilder;
import com.ebao.pub.framework.ExceptionFactory;
import com.ebao.pub.framework.RTException;
import com.ebao.pub.util.StringUtils;

public class CRSControlManLogServiceImpl
        extends GenericServiceImpl<CRSControlManLogVO, CRSControlManLog, CRSControlManLogDAO>
        implements CRSControlManLogService {

    @Resource(name = CRSManAccountLogDAO.BEAN_DEFAULT)
    private CRSManAccountLogDAO accountLogDAO;

    @Resource(name = CRSPartyLogDAO.BEAN_DEFAULT)
    private CRSPartyLogDAO partyLogDAO;

    @Override
    protected CRSControlManLogVO newEntityVO() {
        return new CRSControlManLogVO();
    }

    @Override
    public List<CRSControlManLogVO> findByPartyLogId(Long partyLogId, boolean loadOtherTable) {
        List<CRSControlManLog> ctrlManLogList = this.dao.findByPartyLogId(partyLogId);
        return convertToVOList(ctrlManLogList, loadOtherTable);
    }

    @Override
    public CRSControlManLogVO findByPartyLogIdAndCopy(Long partyLogId, Integer copy, boolean loadOtherTable) {
        if (copy != null) {
            List<CRSControlManLog> controlLogList = this.dao.findByCriteria(
                    Restrictions.eq("crsPartyLog.logId", partyLogId),
                    Restrictions.eq("copy", copy.intValue()));
            if (controlLogList.size() > 0) {
                return convertToVO(controlLogList.get(0), loadOtherTable);
            }
        }
        return null;
    }

    @Override
    public void removeAccountLog(Long logId) {

        if (logId != null) {
            CRSManAccountLog crsAccountLog = accountLogDAO.load(logId);
            if (crsAccountLog != null) {
                accountLogDAO.remove(crsAccountLog);
            }
        }
    }

    @Override
    public CRSManAccountLogVO saveAccountLog(CRSManAccountLogVO accountLogVO) {

        try {
            CRSManAccountLog bo = null;
            if (accountLogVO.getLogId() != null) {
                bo = accountLogDAO.load(accountLogVO.getLogId());
                if (bo == null) {
                    throw new NullPointerException("Object " + CRSManAccountLog.class.getSimpleName()
                            + " (" + accountLogVO.getEntityId() + ")");
                }
            } else {
                bo = new CRSManAccountLog();
            }
            BeanConvertor.convertVOtoBO(accountLogVO, bo, true, true);
            accountLogDAO.save(bo);
            BeanConvertor.convertBOtoVO(bo, accountLogVO, true);
            return accountLogVO;
        } catch (Exception e) {
            throw ExceptionFactory.parse(e);
        }
    }

    @Override
    public Integer getMaxCopy(Long partyLogId) {

        DBean db = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        StringBuffer sql = new StringBuffer();
        try {
            if (1 == 0 || partyLogId == null) {
                throw new NullPointerException();
            }
            sql.append("select nvl(max(copy),0) maxval from T_CRS_CTRL_MAN_LOG where CRS_LOG_ID = ? ");
            db = new DBean();
            db.connect();
            con = db.getConnection();
            ps = con.prepareStatement(sql.toString());
            int index = 1;
            ps.setLong(index++, partyLogId);
            rs = ps.executeQuery();
            while (rs.next()) {
                int max = rs.getInt("maxval");
                return max + 1;
            }
        } catch (SQLException e) {
            throw new RTException(e);
        } catch (ClassNotFoundException e) {
            throw new RTException(e);
        } finally {
            DBean.closeAll(rs, ps, db);
        }
        return 1;
    }

    @Override
    public Integer getMinCopy(Long partyLogId) {
        DBean db = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        StringBuffer sql = new StringBuffer();
        try {
            if (1 == 0 || partyLogId == null) {
                throw new NullPointerException();
            }
            sql.append("select nvl(min(copy),1) minval from T_CRS_CTRL_MAN_LOG where CRS_LOG_ID = ? ");
            db = new DBean();
            db.connect();
            con = db.getConnection();
            ps = con.prepareStatement(sql.toString());
            int index = 1;
            ps.setLong(index++, partyLogId);
            rs = ps.executeQuery();
            while (rs.next()) {
                int min = rs.getInt("minval");
                return min;
            }
        } catch (SQLException e) {
            throw new RTException(e);
        } catch (ClassNotFoundException e) {
            throw new RTException(e);
        } finally {
            DBean.closeAll(rs, ps, db);
        }
        return 1;
    }

    @Override
    public CRSControlManLogVO save(CRSControlManLogVO logVO) throws GenericException {
        CRSControlManLogVO ctrlVO = super.save(logVO);

        String isReady = YesNo.YES_NO__NO;
        CRSControlManLogVO reloadVO = this.load(ctrlVO.getLogId());
        CRSPartyLogVO partyLogVO = reloadVO.getCrsPartyLog();
        if (partyLogVO != null) {
            CRSPartyLog partyLog = partyLogDAO.load(partyLogVO.getLogId());
            java.util.List<CRSControlManLogVO> resultVOList = this.findByPartyLogId(partyLog.getLogId(), Boolean.FALSE);
            if (CollectionUtils.isNotEmpty(resultVOList)) {
                for (CRSControlManLogVO vo : resultVOList) {
                    if (vo.getSignatureDate() != null) {
                        isReady = YesNo.YES_NO__YES;
                    }
                }
            }
            partyLog.setIsCtrlManReady(isReady);
            partyLogDAO.saveorUpdate(partyLog);
            CRSPartyLogVO newPartyLogVO = new CRSPartyLogVO();
            partyLog.copyToVO(newPartyLogVO, Boolean.TRUE);
            ctrlVO.setCrsPartyLog(newPartyLogVO);
        }
        return ctrlVO;
    }

    /**
     * Description : 因新契約輸入具控制權人無parentId，於核保決定後重新編排新契約parentId<br>
     * Created By : Simon.Huang<br>
     * Create Time : 2018年12月18日<br>
     *
     * @param partyLogId
     */
    @Override
    public void syncUNBCtrlManByName(Long partyLogId) {

        List<CRSControlManLogVO> ctrlManList = this.findByPartyLogId(partyLogId, false);

        String parentName = null;
        CRSControlManLogVO parentVO = null;

        for (CRSControlManLogVO ctrlManVO : ctrlManList) {

            ctrlManVO.setParentId(null);

            String fullName = org.apache.commons.lang.StringUtils.trimToEmpty(ctrlManVO.getlName())
                    + org.apache.commons.lang.StringUtils.trimToEmpty(ctrlManVO.getfName());
            //第一份的第一頁
            if (parentName == null) {
                parentName = fullName;
                parentVO = ctrlManVO;

                this.save(ctrlManVO, false);
                continue;
            }

            if (StringUtils.isNullOrEmpty(fullName)) {
                //未填姓名與上一頁控制權人作同一份綁定
                ctrlManVO.setParentId(parentVO.getLogId());
            } else if (fullName.equals(parentName)) {
                //相同姓名與上一頁控制權人作同一份綁定
                ctrlManVO.setParentId(parentVO.getLogId());
            } else {
                //姓名不同切換至下一份
                parentName = fullName;
                parentVO = ctrlManVO;
            }

            this.save(ctrlManVO, false);
        }
    }

    @Override
    public List<CRSControlManLogVO> findByChangeId(Long changeId) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("changeId", HashMapBuilder.build("eq", changeId));
        return convertToVOList(dao.find(params));
    }
}
