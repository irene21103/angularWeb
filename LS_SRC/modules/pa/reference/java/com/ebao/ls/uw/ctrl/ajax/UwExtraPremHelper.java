package com.ebao.ls.uw.ctrl.ajax;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.ebao.ls.pa.nb.util.NBUtils;
import com.ebao.ls.pa.nb.util.NumericUtils;
import com.ebao.ls.pa.nb.util.UnbCst;
import com.ebao.ls.prd.product.vo.ProductVersionVO;
import com.ebao.ls.product.model.query.output.LifeProduct;
import com.ebao.ls.product.service.LifeProductService;
import com.ebao.ls.product.service.ProductVersionService;
import com.ebao.ls.pub.CodeCst;
import com.ebao.ls.uw.vo.UwExtraLoadingVO;
import com.ebao.ls.uw.vo.UwProductVO;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.framework.internal.spring.DSProxy;
import com.ebao.pub.util.DateUtils;
import com.ebao.pub.util.StringUtils;

/**
 * <p>
 * Title: 新契約-核保作業
 * </p>
 * <p>
 * Description: 加費作業共用方法
 * </p>
 * <p>
 * Copyright: Copyright (c) 2016
 * </p>
 * <p>
 * Company: TGL Co., Ltd.
 * </p>
 * <p>
 * Create Time: Jun 22, 2016
 * </p>
 * 
 * @author <p>
 *         Update Time: Jun 22, 2016
 *         </p>
 *         <p>
 *         Updater: simon.huang
 *         </p>
 *         <p>
 *         Update Comments:
 *         </p>
 */
public class UwExtraPremHelper {

    @SuppressWarnings("unchecked")
    public static List<UwExtraLoadingVO> getHealthExtraPremList(Map<String, Object> in, Collection<UwProductVO> uwProducts) {

	LifeProductService lifeProductService = (LifeProductService) DSProxy.newInstanceByBeanName(LifeProductService.BEAN_DEFAULT);
	ProductVersionService productVersionService = (ProductVersionService) DSProxy.newInstanceByBeanName(ProductVersionService.BEAN_DEFAULT);

	List<UwExtraLoadingVO> healthExtraPremList = new ArrayList<UwExtraLoadingVO>();
	String extraArith = CodeCst.ADD_ARITH__EXTRA_TYPE_A; // 加費選項(計算方式)
	String extraType = CodeCst.EXTRA_TYPE__HEALTH_EXTRA;

	Long policyId = NumericUtils.parseLong(in.get("policyId"));
	Long underwriteId = NumericUtils.parseLong(in.get("underwriteId"));

	Map<String, Object> submitData = (Map<String, Object>) in.get("submitData");
	Long partyId = NumericUtils.parseLong(submitData.get("partyId")); // 被保險人
									  // partyId

	Map<String, Object> typeAMap = (Map<String, Object>) submitData.get(extraType); // 職加相關資訊
	if (typeAMap == null || typeAMap.isEmpty()) {
	    return healthExtraPremList;
	}

	// PCR 156226
	String reqFrom = (String) in.get("reqFrom");
	boolean isPOS = (reqFrom != null && reqFrom.equals("pos"));
	String type = "";

	String extraPeriodType = (String) typeAMap.get("extraPeriodType");
	String duration = (String) typeAMap.get("duration");

	String reason = (String) typeAMap.get("reason");
	// 險種及emValue,extraPara,extraPrem的集合
	List<Map<String, Object>> extraPremList = (List<Map<String, Object>>) typeAMap.get("extraPremList");

	Map<String, UwProductVO> itemIdBindUwProduct = NBUtils.toMap(uwProducts, "itemId");

	for (Map<String, Object> extraPremMap : extraPremList) {

	    // 設定頁面資料
	    String itemId = (String) extraPremMap.get("itemId");

	    if (UnbCst.WAIVER.equals(itemId)) {
		// 新契約邏輯//新增前端頁面豁免險整合為一筆,判斷為WAIVER,則取傳入uwProudcts第一筆
		// itemId = uwProducts.iterator().next().getItemId().toString();
		Iterator<UwProductVO> itor = uwProducts.iterator();
		while(itor.hasNext()) {
		    UwProductVO vo  = itor.next();
		    if(CodeCst.YES_NO__YES.equals(vo.getWaiver())){
			itemId = vo.getItemId().toString();
			break;
		    }
		}
	    }

	    String emValue = (String) extraPremMap.get("emValue");
	    String extraPara = (String) extraPremMap.get("extraPara");
	    String extraPrem = (String) extraPremMap.get("extraPrem");
	    String manualIndi = (String) extraPremMap.get("manualIndi");

	    // PCR 156226
	    if (isPOS) {
		type = (String) extraPremMap.get("test");
	    }

	    UwProductVO uwProductVO = itemIdBindUwProduct.get(itemId); // 正常不為空

	    if (uwProductVO == null) {// 新契約調整為單筆uwProducts傳入，所以會有對應不到的情況
		continue;
	    }
	    LifeProduct lifeProduct = lifeProductService.getProductByVersionId(uwProductVO.getProductVersionId());

	    if (CodeCst.YES_NO__YES.equals(manualIndi)) {
		// RTC-114916加費選項(計算方式採人工加費不經保費公式重算) by simon.huang 2017/02/28
		extraArith = CodeCst.ADD_ARITH__EXTRA_TYPE_MANUAL;
	    } else if (lifeProduct.isInvestLink()) {
		extraArith = CodeCst.ADD_ARITH__EXTRA_TYPE_A_ILP_PD; // 投資型
								     // 加費選項(計算方式)
		// PCR 156226
		if (isPOS && !type.isEmpty()) {
		    extraArith = CodeCst.ADD_ARITH__EXTRA_TYPE_B_ILP_PD;
		}
	    } else {
		extraArith = CodeCst.ADD_ARITH__EXTRA_TYPE_A;
		// PCR 156226
		if (isPOS && !type.isEmpty()) {
		    extraArith = CodeCst.ADD_ARITH__EXTRA_TYPE_B;
		}
	    }

	    if (!StringUtils.isNullOrEmpty(emValue)) {
		UwExtraLoadingVO uwExtraLoadingVO = new UwExtraLoadingVO();

		uwExtraLoadingVO.setExtraArith(extraArith);

		uwExtraLoadingVO.setEmValue(Integer.valueOf(emValue));

		uwExtraLoadingVO.setExtraType(extraType);
		uwExtraLoadingVO.setReason(reason);
		uwExtraLoadingVO.setReCalcIndi("N");
		uwExtraLoadingVO.setExtraPeriodType(extraPeriodType);

		uwExtraLoadingVO.setUnderwriteId(underwriteId);
		uwExtraLoadingVO.setItemId(NumericUtils.parseLong(itemId));
		uwExtraLoadingVO.setPolicyId(policyId);
		uwExtraLoadingVO.setUnderwriterId(Long.valueOf(AppContext.getCurrentUser().getUserId()));
		uwExtraLoadingVO.setInsuredId(partyId);

		if (StringUtils.isNullOrEmpty(extraPara)) {
		    uwExtraLoadingVO.setExtraPara(null);
		} else {
		    uwExtraLoadingVO.setExtraPara(new BigDecimal(extraPara));
		}
		if (!StringUtils.isNullOrEmpty(extraPrem)) {
		    uwExtraLoadingVO.setExtraPrem(new BigDecimal(extraPrem));
		    uwExtraLoadingVO.setExtraPremAn(BigDecimal.ZERO);
		} else {
		    uwExtraLoadingVO.setExtraPrem(BigDecimal.ZERO);
		    uwExtraLoadingVO.setExtraPremAn(BigDecimal.ZERO);
		}

		if (!StringUtils.isNullOrEmpty(duration)) {
		    uwExtraLoadingVO.setDuration(Integer.valueOf(duration));
		} else {
		    uwExtraLoadingVO.setDuration(null);
		}
		// 設定startDate/endDate
		syncExtraPremStartDate(uwProductVO, uwExtraLoadingVO);
		// 2017-07-11 Kate RTC#156006 startDate保全應取變更生效日
		if (reqFrom != null && reqFrom.equals("pos")) {
		    Date validDate = DateUtils.toDate((String) in.get("validDate"));
		    uwExtraLoadingVO.setStartDate(validDate);
		}
		healthExtraPremList.add(uwExtraLoadingVO);
	    }
	}

	return healthExtraPremList;
    }

    @SuppressWarnings("unchecked")
    public static List<UwExtraLoadingVO> getJobExtraPremList(Map<String, Object> in, Collection<UwProductVO> uwProducts) {

	LifeProductService lifeProductService = (LifeProductService) DSProxy.newInstanceByBeanName(LifeProductService.BEAN_DEFAULT);
	ProductVersionService productVersionService = (ProductVersionService) DSProxy.newInstanceByBeanName(ProductVersionService.BEAN_DEFAULT);

	List<UwExtraLoadingVO> jobExtraPremList = new ArrayList<UwExtraLoadingVO>();
	String extraArith = CodeCst.ADD_ARITH__EXTRA_TYPE_B; // 加費選項(計算方式)
	String extraType = CodeCst.EXTRA_TYPE__OCCUPATION_EXTRA;

	Long policyId = NumericUtils.parseLong(in.get("policyId"));
	Long underwriteId = NumericUtils.parseLong(in.get("underwriteId"));

	Map<String, Object> submitData = (Map<String, Object>) in.get("submitData");
	Long partyId = NumericUtils.parseLong(submitData.get("partyId")); // 被保險人
									  // partyId

	Map<String, Object> typeBMap = (Map<String, Object>) submitData.get(extraType); // 職加相關資訊
	if (typeBMap == null || typeBMap.isEmpty()) {
	    return jobExtraPremList;
	}
	String extraPara = (String) typeBMap.get("extraPara"); // 頁面:每千元/每萬元保額加費金額
	String reason = (String) typeBMap.get("reason"); // 頁面:加費原因
	// 險種及職業加費金額的集合
	List<Map<String, Object>> extraPremList = (List<Map<String, Object>>) typeBMap.get("extraPremList");

	// PCR 156226
	String reqFrom = (String) in.get("reqFrom");
	boolean isPOS = (reqFrom != null && reqFrom.equals("pos"));
	String jobextra = ""; // 頁面:加費比率

	Map<String, UwProductVO> itemIdBindUwProduct = NBUtils.toMap(uwProducts, "itemId");
	for (Map<String, Object> extraPremMap : extraPremList) {// {itemId,extraPrem}

	    String itemId = (String) extraPremMap.get("itemId"); // 頁面hidden
								 // 保項id
	    String extraPrem = (String) extraPremMap.get("extraPrem"); // 頁面hidden
								       // 加費金額
	    UwProductVO uwProductVO = itemIdBindUwProduct.get(itemId); // 正常不為空

	    if (uwProductVO == null) {// 新契約調整為單筆uwProducts傳入，所以會有對應不到的情況
		continue;
	    }
	    LifeProduct lifeProduct = lifeProductService.getProductByVersionId(uwProductVO.getProductVersionId());
	    ProductVersionVO lifeProdSys = productVersionService.load(uwProductVO.getProductVersionId());
	    String originalSystem = null;
	    originalSystem = lifeProdSys.getOriginalSystem();

	    if (isPOS) {
		jobextra = (String) typeBMap.get("jobextra"); // 頁面:加費比率
	    }
	    if (lifeProduct.isInvestLink()) {
		extraArith = CodeCst.ADD_ARITH__EXTRA_TYPE_B_ILP_PD; // 投資型
								     // 加費選項(計算方式)
		if (isPOS && !jobextra.isEmpty()) {
		    extraArith = CodeCst.ADD_ARITH__EXTRA_TYPE_A_ILP_PD; // 投資型
									 // 加費選項(計算方式)
		}
	    } else {
		extraArith = CodeCst.ADD_ARITH__EXTRA_TYPE_B;
		if (isPOS && !jobextra.isEmpty()) {
		    extraArith = CodeCst.ADD_ARITH__EXTRA_TYPE_A;
		}
	    }

	    // K 商品要職加要轉換
	    if (isPOS && !jobextra.isEmpty() && "2".equals(originalSystem)) {
		if (lifeProduct.isInvestLink()) {
		    extraArith = CodeCst.ADD_ARITH__EXTRA_TYPE_B_ILP_PD; // 投資型
									 // 加費選項(計算方式)
		} else {
		    extraArith = CodeCst.ADD_ARITH__EXTRA_TYPE_B;
		}
	    } else if (isPOS && jobextra.isEmpty() && "2".equals(originalSystem)) {
		if (lifeProduct.isInvestLink()) {
		    extraArith = CodeCst.ADD_ARITH__EXTRA_TYPE_A_ILP_PD; // 投資型
									 // 加費選項(計算方式)
		} else {
		    extraArith = CodeCst.ADD_ARITH__EXTRA_TYPE_A;
		}
	    }

	    UwExtraLoadingVO uwExtraLoadingVO = new UwExtraLoadingVO();

	    uwExtraLoadingVO.setExtraArith(extraArith);
	    uwExtraLoadingVO.setEmValue(0);
	    uwExtraLoadingVO.setExtraType(extraType);

	    uwExtraLoadingVO.setReason(reason);
	    uwExtraLoadingVO.setReCalcIndi("N");
	    uwExtraLoadingVO.setExtraPeriodType("2"); // RI要求新契約職加一律放短加

	    uwExtraLoadingVO.setUnderwriteId(underwriteId);
	    uwExtraLoadingVO.setItemId(uwProductVO.getItemId());
	    uwExtraLoadingVO.setPolicyId(policyId);
	    uwExtraLoadingVO.setUnderwriterId(AppContext.getCurrentUser().getUserId());
	    uwExtraLoadingVO.setInsuredId(partyId);

	    if (StringUtils.isNullOrEmpty(extraPara)) {
		uwExtraLoadingVO.setExtraPara(null);
	    } else {
		uwExtraLoadingVO.setExtraPara(new BigDecimal(extraPara));

	    }
	    if (!StringUtils.isNullOrEmpty(extraPrem)) {
		uwExtraLoadingVO.setExtraPrem(new BigDecimal(extraPrem));
	    } else {
		uwExtraLoadingVO.setExtraPrem(BigDecimal.ZERO);
	    }
	    uwExtraLoadingVO.setDuration(null);
	    // 設定startDate/endDate
	    syncExtraPremStartDate(uwProductVO, uwExtraLoadingVO);
	    // 2017-07-11 Kate RTC#156006 startDate保全應取變更生效日
	    if (reqFrom != null && reqFrom.equals("pos")) {
		Date validDate = DateUtils.toDate((String) in.get("validDate"));
		uwExtraLoadingVO.setStartDate(validDate);
	    }

	    jobExtraPremList.add(uwExtraLoadingVO);
	}

	return jobExtraPremList;
    }

    /**
     * <p>
     * Description : 同步uwProductVO,startDate至uwExtraPrem 及依duration設定endDate
     * </p>
     * <p>
     * Created By : simon.huang
     * </p>
     * <p>
     * Create Time : Jan 16, 2017
     * </p>
     * 
     * @param uwProductVO
     * @param uwExtraPrem
     * @param duration
     */
    public static void syncExtraPremStartDate(UwProductVO uwProductVO, UwExtraLoadingVO uwExtraPrem) {

	// 設定邏輯參考 ActionUtil.getExtraLoadings();
	Date commenceDate = uwProductVO.getValidateDate();// 險種生效日
	Date startDay = commenceDate;
	Date endDay = null;

	Integer duration = uwExtraPrem.getDuration();
	if (NBUtils.in(uwExtraPrem.getExtraArith(), CodeCst.ADD_ARITH__EXTRA_TYPE_A_ILP_PD, CodeCst.ADD_ARITH__EXTRA_TYPE_A, CodeCst.ADD_ARITH__EXTRA_TYPE_MANUAL)) {

	    /* 弱體加費需依加費年期計算出endDate */

	    if ("1".equals(uwExtraPrem.getExtraPeriodType())) {
		// 長加直接使用 endDate
		endDay = uwProductVO.getEndDate();
	    } else {

		// RTC-187584-躉繳時短加也用保項的保障終止日
		if (CodeCst.CHARGE_MODE__SINGLE.equals(uwProductVO.getInitialType())) {
		    endDay = uwProductVO.getEndDate();
		} else {
		    if (duration != null) {
			if (Integer.valueOf(duration).intValue() >= 99) {
			    endDay = uwProductVO.getEndDate();
			} else {
			    endDay = DateUtils.addDay(DateUtils.addYear(startDay, Integer.valueOf(duration).intValue()), -1);
			}
		    } else {
			endDay = null;
		    }
		}
	    }

	}
	if (CodeCst.EXTRA_TYPE__OCCUPATION_EXTRA.equals(uwExtraPrem.getExtraType())) {
	    /* 職業加費直接使用 endDate */
	    endDay = uwProductVO.getEndDate();
	}

	if (endDay == null) {
	    endDay = startDay;
	}

	uwExtraPrem.setEndDate(endDay);
	uwExtraPrem.setStartDate(startDay);
    }

}
