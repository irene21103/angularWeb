package com.ebao.ls.callout.ctrl;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.ls.callout.bs.CalloutQuizService;
import com.ebao.ls.callout.vo.CalloutQuizVO;
import com.ebao.pub.framework.GenericAction;
import com.ebao.pub.framework.GenericException;

public class CalloutQuizAction extends GenericAction {
	private static Logger logger = LogManager.getLogger(CalloutQuizAction.class);

	@Resource(name = CalloutQuizService.BEAN_DEFAULT)
	private CalloutQuizService calloutQuizService;

	@Override
	public ActionForward process(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws GenericException {
		String calloutNum = request.getParameter("calloutNum");
		List<CalloutQuizVO> list;
		try {
			list = calloutQuizService.findByCalloutNum(calloutNum);
			request.setAttribute("calloutList", list);
		} catch (Exception e) {
			logger.catching(e);
			//e.printStackTrace();
		}
		return mapping.findForward("display");
	}

}
