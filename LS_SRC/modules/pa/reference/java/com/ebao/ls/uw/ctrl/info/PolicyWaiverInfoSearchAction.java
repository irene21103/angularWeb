package com.ebao.ls.uw.ctrl.info;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.ls.pa.nb.ctrl.detailreg.DetailRegHelper;
import com.ebao.pub.framework.GenericAction;

public class PolicyWaiverInfoSearchAction extends GenericAction {

    private String forward = "display";
    
    @Resource(name = QualityRecordActionHelper.BEAN_DEFAULT)
    private QualityRecordActionHelper qualityRecordActionHelper;
    
    @Resource(name = DetailRegHelper.BEAN_DEFAULT)
    private DetailRegHelper detailRegHelper;
    
    @Override
    public ActionForward process(ActionMapping mapping, ActionForm form,
                    HttpServletRequest request, HttpServletResponse response)
                    throws Exception {
        
        String policyId = request.getParameter("policyId").toString(); 
        List<Map<String, Object>> coverageList = detailRegHelper.getCoveragesWithWaivers(policyId, request);
        request.setAttribute("coverageList", coverageList);
        return mapping.findForward(forward);
    }

}
