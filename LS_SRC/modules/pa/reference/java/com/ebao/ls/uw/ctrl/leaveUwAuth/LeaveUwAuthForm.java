package com.ebao.ls.uw.ctrl.leaveUwAuth;

import com.ebao.ls.uw.bo.LeaveUwAuthEdit;
import com.ebao.pub.framework.GenericForm;

/**
 * <p>Title: Module Information Andy GOD</p>
 * <p>Description: Function Description Only vegetable</p>
 * <p>Copyright: Copyright (c) 2015</p>
 * <p>Company: TGL Co., Ltd.</p>
 * <p>Create Time: Aug 6, 2015</p> 
 * @author 
 * <p>Update Time: Aug 6, 2015</p>
 * <p>Updater: Alex Cheng</p>
 * <p>Update Comments: </p>
 */
public class LeaveUwAuthForm extends GenericForm {

  private static final long serialVersionUID = 7560275134473822767L;

  //mode type
  private String actionType;
  
  private String leaveUwAuthListId;
  
  private String editEmpId;
  
  private String masterEmpId;
  
  private LeaveUwAuthEdit leaveUwAuthEdit;
  
  private String retMsg;
  
  private String searchType;
  
  private String deptId;
  
  private String sectionDeptId;
  
  private String empName;
  
  private String empId;
  
  private String channelType;
  
  private String channelId;
  
  private Boolean isEditRole;

  public LeaveUwAuthEdit getLeaveUwAuthEdit() {
    if(leaveUwAuthEdit == null){
      this.setLeaveUwAuthEdit(new LeaveUwAuthEdit());
    }
    return leaveUwAuthEdit;
  }

  public void setLeaveUwAuthEdit(LeaveUwAuthEdit leaveUwAuthEdit) {
    this.leaveUwAuthEdit = leaveUwAuthEdit;
  }

  public String getActionType() {
    return actionType;
  }

  public void setActionType(String actionType) {
    this.actionType = actionType;
  }

  public String getLeaveUwAuthListId() {
    return leaveUwAuthListId;
  }

  public void setLeaveUwAuthListId(String leaveUwAuthListId) {
    this.leaveUwAuthListId = leaveUwAuthListId;
  }

  public String getEmpId() {
    return empId;
  }

  public void setEmpId(String empId) {
    this.empId = empId;
  }

  public String getRetMsg() {
    return retMsg;
  }

  public void setRetMsg(String retMsg) {
    this.retMsg = retMsg;
  }

  public String getMasterEmpId() {
    return masterEmpId;
  }

  public void setMasterEmpId(String masterEmpId) {
    this.masterEmpId = masterEmpId;
  }

  public String getEditEmpId() {
    return editEmpId;
  }

  public void setEditEmpId(String editEmpId) {
    this.editEmpId = editEmpId;
  }

  public String getSearchType() {
    return searchType;
  }

  public void setSearchType(String searchType) {
    this.searchType = searchType;
  }

  public String getDeptId() {
    return deptId;
  }

  public void setDeptId(String deptId) {
    this.deptId = deptId;
  }

  public String getSectionDeptId() {
    return sectionDeptId;
  }

  public void setSectionDeptId(String sectionDeptId) {
    this.sectionDeptId = sectionDeptId;
  }

  public String getEmpName() {
    return empName;
  }

  public void setEmpName(String empName) {
    this.empName = empName;
  }

  public String getChannelType() {
    return channelType;
  }

  public void setChannelType(String channelType) {
    this.channelType = channelType;
  }

  public String getChannelId() {
    return channelId;
  }

  public void setChannelId(String channelId) {
    this.channelId = channelId;
  }

  public Boolean getIsEditRole() {
    return isEditRole;
  }

  public void setIsEditRole(Boolean isEditRole) {
    this.isEditRole = isEditRole;
  }
  
}
