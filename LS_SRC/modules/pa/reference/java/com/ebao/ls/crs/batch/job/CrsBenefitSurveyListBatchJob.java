package com.ebao.ls.crs.batch.job;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.transaction.UserTransaction;
import org.apache.commons.lang3.StringUtils;
import com.ebao.foundation.common.lang.exception.ExceptionUtils;
import com.ebao.foundation.commons.exceptions.ExceptionUtil;
import com.ebao.ls.batch.TGLBaseUnpieceableJob;
import com.ebao.ls.crs.batch.service.CrsBenefitSurveyListService;
import com.ebao.ls.crs.batch.vo.CRSTempVO;
import com.ebao.ls.fatca.batch.data.FatcaBenefitSurveyListDAO;
import com.ebao.ls.fatca.constant.FatcaSurveyType;
import com.ebao.ls.fatca.vo.FatcaDueDiligenceListVO;
import com.ebao.pub.batch.help.BatchHelp;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.util.TransUtils;

/**
 * CRS提取受益人調查對象，產出報表用數據，並執行建檔
 */
public class CrsBenefitSurveyListBatchJob extends TGLBaseUnpieceableJob {
	
    public static final String BEAN_DEFAULT = "crsBenefitSurveyListBatchJob";
    
    @Override
    protected String getJobName() {
        return BatchHelp.getJobName();
    }

    @Resource(name = CrsBenefitSurveyListService.BEAN_DEFAULT)
    private CrsBenefitSurveyListService crsBenefitSurveyListService;
    
    @Resource(name = FatcaBenefitSurveyListDAO.BEAN_DEFAULT)
    private FatcaBenefitSurveyListDAO fatcaBenefitSurveyListDAO;
    
    /**
     * 版本號
     */
    private static final String VERSION = "0.24";
    
    /**
     * 業務日期
     */
    private Date processDate = null;
    
    /**
     * 寫info log
     * @param layout
     * @param msg
     */
    private void log(String msg) {
    	logger.info("[SYSOUT][CrsBenefitSurveyListTask] " + msg);
    	batchLogger.info(msg);
    }
    
    /**
     * 批次進入點
     */
    @Override
    public int processJob() throws Exception {
    	log("processJob - start");
    	log("VERSION = " + VERSION);
        
    	if (!isMock()) {
    		processDate = BatchHelp.getProcessDate();
    	} else {
    		log("測試模式");
    		if (processDate == null) {
    			throw new IllegalArgumentException("processDate is null");
    		}
    	}
        
        log("業務日期 = " + processDate);
        
        try {
            // 受益人變更的新受益人
            process(processDate, FatcaSurveyType.RENEW_BENEFICIARY);
            // 即期年金受益人(被保險人)
            process(processDate, FatcaSurveyType.IMMEDIATE_ANNUITY_BENEFICIARY);
            // 保險金受益人
            process(processDate, FatcaSurveyType.THE_BENEFITS_OWNER);
            // PCR257607 理賠給付受益人
            process(processDate, FatcaSurveyType.CLAIM_PAYMENT_BENEFICIARY);
        } catch(Exception ex){
            log(ExceptionUtils.getFullStackTrace(ex));
            throw ExceptionUtil.parse(ex);
        }
        log("processJob - end");
        if (!isMock()) {
        	return getExecuteStatus();
        }
        else {
        	log("成功筆數 = " + getBatchContext().getSuccessCount());
        	log("失敗筆數 = " + getBatchContext().getErrorCount());
        	return getExecuteStatus(getBatchContext().getSuccessCount(), getBatchContext().getErrorCount());
        }
    }
    
    private void process(Date processDate, String fatcaSurveyType) throws Exception {
    	// BR-CMN-FAT-026 從盡職調查母體，抽取符合條件資料
    	log("[BR-CMN-FAT-026][提取受益人調查對象] fatcaSurveyType = " + fatcaSurveyType);
    	List<CRSTempVO> crsTempVOList = crsBenefitSurveyListService.
    			queryBeneficiaryList(processDate, fatcaSurveyType);
        log("[BR-CMN-FAT-026][提取受益人調查對象] crsTempVOList.size() = " + crsTempVOList.size());
        UserTransaction trans = null;
        for (CRSTempVO crsTempVO : crsTempVOList) {
        	try {
        		trans = TransUtils.getUserTransaction();
        		trans.begin();
        		FatcaDueDiligenceListVO fatcaVO = crsTempVO.getFatcaDueDiligenceListVO();        	
            	// 更新業務員名單 (理賠給付受益人不更新)
        		if(!StringUtils.equals(fatcaSurveyType, FatcaSurveyType.CLAIM_PAYMENT_BENEFICIARY)){
            	   crsBenefitSurveyListService.updateServiceAgent(fatcaVO);
            	   // BR-CMN-FAT-028 確定受益人調查名單的派發對象_孤兒保單
                   crsBenefitSurveyListService.deliverOrphan(fatcaVO);
        		}
                // BR-CMN-FAT-027 產生受益人調查名單ODS報表數據
                if (crsTempVO.isNewRecord()) {
                	log("Create new record to fatcaBenefitSurveyList.");
                	fatcaBenefitSurveyListDAO.createOrUpdateVO(fatcaVO);
                } else {
                	log("Update current record in fatcaBenefitSurveyList.");
                	fatcaBenefitSurveyListDAO.updateByVO(fatcaVO);
                }
                // BR-CMN-FAT-033 自動執行受益人 CRS建檔
                // PCR257607 理賠給付受益人僅產生調查名單
                if(!StringUtils.equals(fatcaSurveyType, FatcaSurveyType.CLAIM_PAYMENT_BENEFICIARY)){
                	Long userId = AppContext.getCurrentUser().getUserId();
                    crsBenefitSurveyListService.createCrsRecord(null, crsTempVO, userId, processDate);
                }

                incSuccessCount();
                trans.commit();                
        	} catch (Exception ex) {
        		log("發生異常");
            	log(ExceptionUtils.getFullStackTrace(ex));
            	incErrorCount();
            	trans.rollback();
            } finally {
            	trans = null;
            }
        }
    }
    
    public void setProcessDate(Date date) {
    	this.processDate = date;
    }
}
