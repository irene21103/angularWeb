package com.ebao.ls.riskPrevention.ci.impl;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;
import javax.transaction.UserTransaction;

import com.ebao.foundation.core.logging.Log;
import com.ebao.foundation.module.db.Trans;
import com.ebao.ls.pa.nb.util.NBUtils;
import com.ebao.ls.pty.ds.DeptService;
import com.ebao.ls.pub.CodeCst;
import com.ebao.ls.riskPrevention.bs.NationalityCodeService;
import com.ebao.ls.riskPrevention.bs.RiskCustomerLogService;
import com.ebao.ls.riskPrevention.bs.RiskCustomerService;
import com.ebao.ls.riskPrevention.bs.impl.RiskCustomerAuthServiceImpl;
import com.ebao.ls.riskPrevention.ci.RiskCustomerCI;
import com.ebao.ls.riskPrevention.vo.RiskCustomerLogVO;
import com.ebao.ls.riskPrevention.vo.RiskCustomerQueryVO;
import com.ebao.ls.riskPrevention.vo.RiskCustomerVO;
import com.ebao.ls.uw.ci.UwChangeMsgCI;
import com.ebao.ls.ws.ci.aml.AmlQueryCI;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.util.BeanUtils;
import com.ebao.pub.util.StringUtils;

public class RiskCustomerCIImpl implements RiskCustomerCI {

    private final Log log = Log.getLogger(RiskCustomerCIImpl.class);
    
    @Resource(name = RiskCustomerService.BEAN_DEFAULT)
    RiskCustomerService riskCustomerService;

    @Resource(name = RiskCustomerLogService.BEAN_DEFAULT)
    RiskCustomerLogService riskCustomerLogService;

    @Resource(name = NationalityCodeService.BEAN_DEFAULT)
    NationalityCodeService nationalityCodeService;

    @Resource(name = DeptService.BEAN_DEFAULT)
    DeptService deptService;

    @Resource(name = RiskCustomerAuthServiceImpl.BEAN_DEFAULT)
    RiskCustomerAuthServiceImpl pageAuthService;

    @Resource(name = UwChangeMsgCI.BEAN_DEFAULT)
    UwChangeMsgCI uwChangeMsgCI;
    
    private static String MODE_POS = "posMode";
    
    private static String MODE_UNB = "unbMode";
    
    private static String MODE_NEW_PAGE = "riskNewMode";
    
    private static String MODE_MODIFY_PAGE = "riskModifyMode";

    
    @Resource(name = AmlQueryCI.BEAN_DEFAULT)
    private AmlQueryCI amlQueryCI;

    
    
    /**
     * <p>Description: 使用  客戶 身分證字號/護照號碼/統一編(證)號  查詢客戶風險</p>
     * @return null:查無相關資料 / RiskCustomerQueryVO:客戶風險資訊
     */
    @Override
    public RiskCustomerQueryVO queryCustomerDetail(String certiCode) {
        RiskCustomerQueryVO vo = null;
        RiskCustomerVO data = riskCustomerService.queryByCertiCode(certiCode);
        if (data != null) {
            vo = new RiskCustomerQueryVO();
            BeanUtils.copyProperties(vo, data);
        }
        return vo;
    }

    /**
     * RiskPrevention Page Use Only
     */
    @Override
    public RiskCustomerVO newCustomerDetail(String certiCode) throws Exception {
        log.info("[RiskCustomerCIImpl][newCustomerDetail][newPage] Start Calculate. ");
        return newCustomerDetail(certiCode, AppContext.getCurrentUserLocalTime(), null, MODE_NEW_PAGE);
    }

    /**
     * RiskPrevention Page Use Only
     */
    @Override
    public RiskCustomerVO newCustomerDetail(String certiCode, String riskFamous, Integer riskManual) throws Exception {
        log.info("[RiskCustomerCIImpl][newCustomerDetail][modifyPage] Start Calculate. ");
        return newCustomerDetail(certiCode, AppContext.getCurrentUserLocalTime(), null, MODE_MODIFY_PAGE, riskFamous, riskManual);
    }


    /**
     * POS Use Only
     */
    @Override
    public RiskCustomerQueryVO updateCustomerDetail(String certiCode) throws Exception {
        log.info("[RiskCustomerCIImpl][newCustomerDetail][POS_Mode] Start Calculate. ");
        RiskCustomerVO newVO = newCustomerDetail(certiCode, AppContext.getCurrentUserLocalTime(), null, MODE_POS);
        log.info("[RiskCustomerCIImpl][newCustomerDetail][POS_Mode] End of Calculate. ");

        // 設定回傳 VO
        RiskCustomerQueryVO out = new RiskCustomerQueryVO();
        BeanUtils.copyProperties(out, newVO);
        return out;
    }
    
    protected RiskCustomerVO newCustomerDetail(String certiCode, 
                    Date validateDate, Long policyId, String mode) throws Exception {
        return newCustomerDetail(certiCode, validateDate, policyId, mode, null, null);
    }

    protected RiskCustomerVO newCustomerDetail(String certiCode, Date validateDate, Long policyId, 
                    String mode, String riskFamous, Integer riskManaul) throws Exception {
        log.info("[RiskCustomerCIImpl][newCustomerDetail] Main Process Start. ");
        if (NBUtils.isEmptyOrDummyCertiCode(certiCode)) {
            log.warn("[RiskCustomerCIImpl][newCustomerDetail] Certi_Code is inValid . ");
            return null;
        }
        // 客戶風險預設值
        RiskCustomerVO newVO = new RiskCustomerVO();
        newVO.setCertiCode(certiCode);
        newVO.setRiskOIU(0);
        newVO.setRiskProduct(0);
        newVO.setRiskScore(0);

        // 取得客戶風險相關資料 : 已根據時間排序
        List<Map<String, Object>> policies = riskCustomerService
                        .prevQueryFromPolicyHolderByCertiCodeWithDate(certiCode, validateDate);
        log.info("[RiskCustomerCIImpl][newCustomerDetail] Query Result: " + policies.size() + ". ");

        // 是否存在於 T_Policy_Holder
        if (policies.size() < 1) {
            log.warn("[RiskCustomerCIImpl][newCustomerDetail] The Customer is not Holder of Policies. ");
            return null;
        }

        boolean firstActive = false;
        for (Map<String, Object> policy : policies) {
            // 是否為有效件 ( 詳情見:isValidatePolicy() )
            boolean validatePolicy = isValidatePolicy(
                            policy.get("LIABILITY_STATE"), (Date) policy.get("P_LAPSE_DATE"), validateDate);
            
            log.info("[RiskCustomerCIImpl][newCustomerDetail] The Policy: " + policy.get("POLICY_ID") 
                                                                + " is " + (validatePolicy ? "Valid" : "InValid") + ".");
            
            if (validatePolicy) {
                // 取得客戶最新相關資料
                if (!firstActive) {
                    
                    newVO.setName((String) policy.get("NAME"));
                    newVO.setNationalCode((String) policy.get("NATIONALITY"));
                    // 設定國家風險
                    if (StringUtils.isNullOrEmpty(newVO.getNationalCode()) == false) {
                        newVO.setRiskNational(nationalityCodeService.findRiskByNationalityCode(newVO.getNationalCode()));
                    }
                    firstActive = true;
                    log.info("[RiskCustomerCIImpl][newCustomerDetail] Set Name and Nationality as " + newVO.getName() + "/" + newVO.getNationalCode());
                    log.info("[RiskCustomerCIImpl][newCustomerDetail] Set Risk of Nationality: " + newVO.getRiskNational());
                    
                } else if(NBUtils.isEmptyOrDummyName(newVO.getName())) { // 若為無效名字 
                    newVO.setName((String)policy.get("NAME"));
                    log.info("[RiskCustomerCIImpl][newCustomerDetail] Set Name: " + newVO.getName());
                }
                // 若產品為風險管道
                if (CodeCst.YES_NO__YES.equals(policy.get("IS_OIU"))) {
                    log.info("[RiskCustomerCIImpl][newCustomerDetail] Product: " + policy.get("PRODUCT_ID") + " is OIU Product!");
                    newVO.setRiskOIU(newVO.getRiskOIU() + CodeCst.RISK__RISK_OIU);
                }
                // 若產品為風險產品
                if (CodeCst.PRODUCT_RISK_EVALUATE_YES.equals(policy.get("PRODUCT_RISK_EVALUATE"))) {
                    log.info("[RiskCustomerCIImpl][newCustomerDetail] Product: " + policy.get("PRODUCT_ID") + " is Risk Product!");
                    newVO.setRiskProduct(newVO.getRiskProduct() + CodeCst.RISK__RISK_PRODUCT);
                }
            }
        }

        // 原人工設定風險分數
        int originalManual = 0;
        
        // 帶出原風險調整資料
        RiskCustomerVO oldVO = riskCustomerService.queryByCertiCode(newVO.getCertiCode());
        
        // 非洗錢修改頁面  不會傳值
        if (!mode.equals(MODE_MODIFY_PAGE)) {
            // 預設值
            newVO.setRiskFamous(CodeCst.YES_NO__NO);
            newVO.setRiskManual(0);
            
            // 若有舊風險資料，則寫入
            if(oldVO != null){
                log.info("[RiskCustomerCIImpl][newCustomerDetail] Enter RiskFamous and RiskManaul by OLD Record:"
                                + " RiskFamous-" + oldVO.getRiskFamous() + " RiskManual-" + oldVO.getRiskManual());
                
                newVO.setRiskFamous(oldVO.getRiskFamous() != null ? oldVO.getRiskFamous() : CodeCst.YES_NO__NO);
                newVO.setRiskManual(oldVO.getRiskManual() != null ? oldVO.getRiskManual() : 0);
            }
        } else {
            log.info("[RiskCustomerCIImpl][newCustomerDetail] Enter RiskFamous and RiskManaul by Default or User Enter!"
                                + " RiskFamous-" + riskFamous + " RiskManual-" + riskManaul);
            
            newVO.setRiskFamous(riskFamous);
            newVO.setRiskManual(riskManaul);
        }
        originalManual += CodeCst.YES_NO__YES.equals(newVO.getRiskFamous()) ? CodeCst.RISK__RISK_FAMOUS : 0;
        originalManual += newVO.getRiskManual();
        

        
        if(mode.equals(MODE_UNB)){
            log.info("[RiskCustomerCIImpl][newCustomerDetail][UNB_Mode] ReMark SYSTEM_AUTO_ADJUSTMENT.");
            newVO.setRiskRemark(CodeCst.RISK__SYSTEM_AUTO_ADJUSTMENT);
        }
        
        // 風險地區分數
        int nationalScore = (CodeCst.YES_NO__YES.equals(newVO.getRiskNational()) ? CodeCst.RISK__RISK_NATIONALITY : 0);
        
        // 風險分數結算 ( 總分 / 不大於 100 )
        newVO.setRiskOIU(newVO.getRiskOIU() > CodeCst.RISK__HIGHTEST_SCORE 
                        ? CodeCst.RISK__HIGHTEST_SCORE : newVO.getRiskOIU());
        
        newVO.setRiskProduct(newVO.getRiskProduct() > CodeCst.RISK__HIGHTEST_SCORE
                        ? CodeCst.RISK__HIGHTEST_SCORE : newVO.getRiskProduct());
        
        newVO.setRiskScore((newVO.getRiskOIU() + newVO.getRiskProduct() + nationalScore + originalManual) > CodeCst.RISK__HIGHTEST_SCORE
                        ? CodeCst.RISK__HIGHTEST_SCORE : (newVO.getRiskOIU() + newVO.getRiskProduct() + nationalScore + originalManual));

        // 設定風險等級
        if (newVO.getRiskScore() >= CodeCst.RISK__LOWTEST_HIGH_LEVEL) {
            newVO.setRiskLevel(CodeCst.RISK_LEVEL_HIGH);
        } else if (newVO.getRiskScore() >= CodeCst.RISK__LOWTEST_MEDIUM_LEVEL) {
            newVO.setRiskLevel(CodeCst.RISK_LEVEL_MEDIUM);
        } else {
            newVO.setRiskLevel(CodeCst.RISK_LEVEL_LOW);
        }


        RiskCustomerLogVO newLog = new RiskCustomerLogVO();
        newLog = (RiskCustomerLogVO) BeanUtils.getBean(RiskCustomerLogVO.class, newVO);
        newLog.setInsertBy(AppContext.getCurrentUser().getUserId());
        newLog.setUpdateBy(newLog.getInsertBy());
        newLog.setInsertTime(AppContext.getCurrentUserLocalTime());
        newLog.setUpdateTime(newLog.getInsertTime());
        newLog.setInsertTimestamp(newLog.getInsertTime());
        newLog.setUpdateTimestamp(newLog.getInsertTime());
        newLog.setUpdatedDeptID(AppContext.getCurrentUser().getDeptId());
        newLog.setUpdatedDeptName(deptService.getDeptById(newLog.getUpdatedDeptID()).getDeptName());

        if (MODE_UNB.equals(mode) && !isSameObject(newVO, oldVO)) {
            
            log.info("[RiskCustomerCIImpl][newCustomerDetail][UNB_Mode] Prepare Save Data.");
            // 寫入新紀錄
            UserTransaction ut = Trans.getUserTransaction();
            try {
                ut.begin();
                riskCustomerService.saveOrUpdate(newVO);
                riskCustomerLogService.save(newLog);
                ut.commit();
            } catch (Exception e) {
                ut.rollback();
                throw e;
            }

            log.info("[RiskCustomerCIImpl][newCustomerDetail][UNB_Mode] Prepare Send Event.");
            if (policyId != null) {
                // 核保訊息通知有人工修改紀錄
                uwChangeMsgCI.prcUwChangeRiskMsg(policyId, newLog.getCertiCode(), newLog.getInsertTime());
            }
            log.info("[RiskCustomerCIImpl][newCustomerDetail][UNB_Mode] End Of Saving Data.");
            
        } else if (MODE_POS.equals(mode) && !isSameObject(newVO, oldVO)) {
            log.info("[RiskCustomerCIImpl][newCustomerDetail][POS_Mode] Prepare Save Data.");
            riskCustomerService.saveOrUpdate(newVO);
            riskCustomerLogService.save(newLog);
            log.info("[RiskCustomerCIImpl][newCustomerDetail][POS_Mode] End Of Saving Data.");
        }
        log.info("[RiskCustomerCIImpl][newCustomerDetail] End Of Main Process.");
        return newVO;
    }

    @Override
    public boolean isManualAdjustHistory(String certiCode) throws Exception {

        // 歷史記錄是否有人工調整記錄
        boolean isHasManulAdjust = riskCustomerLogService.isManualAdjustHistory(certiCode);

        return isHasManulAdjust;
    }

    /**
     * <p>Description : 判斷是否為有效件 </p>
     * <p>Created By : Calvin Chen</p>
     * <p>Create Time : Jan 10, 2017</p>
     * @param liabilityState
     * @param pLapseDate
     * @param validateDate
     * @return
     */
    private boolean isValidatePolicy(Object liabilityState, Date pLapseDate, Date validateDate) {

        Integer state;
        if (liabilityState == null) {
            return false;
        } else if (liabilityState instanceof BigDecimal) {
            state =  ((BigDecimal) liabilityState).intValue();
        } else {
            state = Integer.parseInt(liabilityState.toString());
        }
        /**  產品風險保單有效件: 未生效件、有效、豁免、ETI、RPU、停效兩年內
         *   T_Contract_Master: LIABILITY_STATE
         *   未生效件: LIABILITY_STATE = 0
         *   有效: LIABILITY_STATE = 1
         *   豁免 (T_CONTRACT_PRODUCT:WAIVER:'Y'): LIABILITY_STATE = 1
         *   ETI (T_CONTRACT_EXTEND:PREM_STATIS:8): LIABILITY_STATE = 1
         *   RPU (T_CONTRACT_EXTEND:PREM_STATIS:3): LIABILITY_STATE = 1
         *   停效: LIABILITY_STATE = 2
         */
        if (CodeCst.LIABILITY_STATUS__NON_VALIDATE == state || CodeCst.LIABILITY_STATUS__IN_FORCE == state) { // 未生效，有效
            return true;
        } else if (CodeCst.LIABILITY_STATUS__LAPSED == state) { // 停效
            if (pLapseDate == null || validateDate == null) {
                return false;
            }
            Calendar lapseDate = Calendar.getInstance();
            Calendar currentDate = Calendar.getInstance();
            lapseDate.setTime(pLapseDate);
            lapseDate.add(Calendar.YEAR, 2); // 有效件 到期日
            currentDate.setTime(validateDate);
            if (currentDate.after(pLapseDate)) { // 現在時間在有效件到期日之後(失效)
                return false;
            }
        }
        return false;
    }

    /**
     * <p>Description : Check Two Risk Record </p>
     * <p>Created By : Calvin Chen</p>
     * <p>Create Time : Jan 10, 2017</p>
     * @param object1
     * @param object2
     * @return
     */
    @SuppressWarnings("unchecked")
    private boolean isSameObject(Object object1, Object object2) {
        if(object1 == null || object2== null){
            return false;
        }
        Map<String, Object> object1Map = BeanUtils.getMap(object1);
        Map<String, Object> object2Map = BeanUtils.getMap(object2);
        Set<Map.Entry<String, Object>> objectEntrySet = new HashSet<Map.Entry<String, Object>>();
        objectEntrySet.addAll(object1Map.entrySet());
        objectEntrySet.addAll(object2Map.entrySet());
        for (Map.Entry<String, Object> entry : objectEntrySet) {
            if (object1Map.get(entry.getKey()) != null && object2Map.get(entry.getKey()) != null) {
                if (!object1Map.get(entry.getKey()).equals(object2Map.get(entry.getKey()))) {
                    return false;
                }
            } else if (object1Map.get(entry.getKey()) == null && object2Map.get(entry.getKey()) == null) {
            } else {
                return false;
            }
        }
        return true;
    }

}
