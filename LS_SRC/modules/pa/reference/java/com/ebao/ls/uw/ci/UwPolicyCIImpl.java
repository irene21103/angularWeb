package com.ebao.ls.uw.ci;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;

import com.ebao.foundation.module.db.hibernate.HibernateSession3;
import com.ebao.ls.arap.data.bo.CashRefund;
import com.ebao.ls.arap.data.dao.CashRefundDao;
import com.ebao.ls.arap.pub.ci.CashCI;
import com.ebao.ls.arap.pub.ci.CashCIVO;
import com.ebao.ls.cs.logger.ApplicationLogger;
import com.ebao.ls.pa.nb.ci.UnbLetterCI;
import com.ebao.ls.pa.pub.ci.PolicyCI;
import com.ebao.ls.pty.ci.EmployeeCI;
import com.ebao.ls.pty.vo.EmployeeVO;
import com.ebao.ls.pub.CodeCst;
import com.ebao.ls.uw.ci.vo.UwPolicyCIVO;
import com.ebao.ls.uw.data.TUwInsuredListDelegate;
import com.ebao.ls.uw.data.bo.UwLifeInsured;
import com.ebao.ls.uw.data.bo.UwPolicy;
import com.ebao.ls.uw.ds.UwInsuredDisabilityService;
import com.ebao.ls.uw.ds.UwMedicalLetterService;
import com.ebao.ls.uw.ds.UwPolicyService;
import com.ebao.ls.uw.ds.UwRiApplyService;
import com.ebao.ls.uw.ds.UwSickFormLetterService;
import com.ebao.ls.uw.vo.UwExtraLoadingVO;
import com.ebao.ls.uw.vo.UwLifeInsuredVO;
import com.ebao.ls.uw.vo.UwPolicyVO;
import com.ebao.ls.uw.vo.UwProductVO;
import com.ebao.pub.framework.ExceptionFactory;
import com.ebao.pub.framework.GenericException;
import com.ebao.pub.util.BeanUtils;
import com.ebao.pub.util.DBean;

/**
 * Title:Underwriting <br>
 * Description: <br>
 * Copyright: Copyright (c) 2004 <br>
 * Company: eBaoTech Corporation <br>
 *
 * @author yixing.lu
 * @since Time:Jun 23, 2005
 * @version 1.0
 */
public class UwPolicyCIImpl implements UwPolicyCI {
	

	@Resource(name = UwMedicalLetterService.BEAN_DEFAULT)
	private UwMedicalLetterService uwMedicalLetterService;

	@Resource(name = UwSickFormLetterService.BEAN_DEFAULT)
	private UwSickFormLetterService uwSickFormLetterService;

	@Resource(name = PolicyCI.BEAN_DEFAULT)
	private PolicyCI policyCI;

	@Resource(name = UwRiApplyService.BEAN_DEFAULT)
	protected UwRiApplyService uwRiApplyService;
	
	@Resource(name = TUwInsuredListDelegate.BEAN_DEFAULT)
	private TUwInsuredListDelegate uwInsuredListDao;

	/**
	 * return uw policy vo
	 *
	 * @param policyId
	 * @return
	 * @throws GenericException
	 */
	public UwPolicyCIVO getUwPolicy(Long policyId) throws GenericException {
		Long underwriteId = getUwPolicyDS().retrieveUwIdbyPolicyId(policyId);
		if (underwriteId == null) {
			return null;
		}
		UwPolicyVO policyVO = getUwPolicyDS().findUwPolicy(underwriteId);
		Collection productCo = getUwPolicyDS().findUwProductEntitis(underwriteId);
		Iterator productIt = productCo.iterator();
		while (productIt.hasNext()) {
			UwProductVO productVO = (UwProductVO) productIt.next();
			if (productVO.getMasterId() == null) {
				policyVO.setUwDecision(productVO.getDecisionId());
			}
		}
		UwPolicyCIVO plicyCIVO = (UwPolicyCIVO) BeanUtils.getBean(
				UwPolicyCIVO.class, policyVO);
		return plicyCIVO;
	}

	/**
	 * return uw policy vo
	 *
	 * @param policyId
	 * @param uwSourceType
	 * @return
	 * @throws GenericException
	 */
	public UwPolicy getUwPolicy(Long policyId, String uwSourceType) throws GenericException {
		Long underwriteId = getUwPolicyDS().retrieveUwIdbyPolicyId(policyId);
		if (underwriteId == null) {
			return null;
		}
		UwPolicy policyVO = getUwPolicyDS().findLastestUWPolicy(policyId, uwSourceType);
		Collection productCo = getUwPolicyDS().findUwProductEntitis(underwriteId);
		Iterator productIt = productCo.iterator();
		while (productIt.hasNext()) {
			UwProductVO productVO = (UwProductVO) productIt.next();
			if (productVO.getMasterId() == null) {
				policyVO.setUwDecision(productVO.getDecisionId());
			}
		}
		return policyVO;
	}

	@Override
	public Long getUnderwriterId(Long policyId) throws GenericException {
		Long underwriteId = getUwPolicyDS().retrieveUwIdbyPolicyId(policyId);
		if (underwriteId == null) {
			return null;
		}
		UwPolicyVO policyVO = getUwPolicyDS().findUwPolicy(underwriteId);
		Long underwriterId = policyVO.getUnderwriterId();
		return underwriterId;
	}

	public void updatePendingReason(Long policyId, Long pendingCode)
			throws GenericException {
		if (null == policyId) {
			throw new IllegalArgumentException("Policy Id can't be null.");
		}
		Long underwriteId = getUwPolicyDS().retrieveUwIdbyPolicyId(policyId);
		if (underwriteId == null) {
			return;
		}
		UwPolicyVO policyVO = getUwPolicyDS().findUwPolicy(underwriteId);
		policyVO.setUwPending(pendingCode);
		getUwPolicyDS().updateUwPolicy(policyVO, false);
	}

	/*
	 * (non-Javadoc)
	 * @see com.ebao.ls.uw.ci.UwPolicyCI#removeUWExtraPrem(java.lang.Long,
	 * java.lang.Long)
	 */
	public void removeUWExtraPrem(Long policyId, Long partyId)
			throws GenericException {
		Long underwriteId = uwPolicyDS.findLastUnderwriteIdbyPolicyId(policyId);
		if (underwriteId != null) {
			Collection<UwExtraLoadingVO> uwExtras = getUwPolicyDS()
					.findUwExtraLoadingEntitis(underwriteId);
			if (uwExtras != null && uwExtras.size() > 0) {
				for (Iterator itera = uwExtras.iterator(); itera.hasNext();) {
					UwExtraLoadingVO uwLoading = (UwExtraLoadingVO) itera.next();
					if (partyId.equals(uwLoading.getInsuredId())
							&& uwLoading.getUwListId() != null) {
						getUwPolicyDS().removeUwExtraLoading(uwLoading);
					}
				}
			}
		}
	}

	/**
	 * update uw policy
	 *
	 * @param value
	 * @throws GenericException
	 */
	public void updateUwPolicy(Long uwId, Long level, Long category)
			throws GenericException {
		UwPolicyVO uwPolicyVO = getUwPolicyDS().findUwPolicy(uwId);
		uwPolicyVO.setUwLevel(level);
		uwPolicyVO.setUwCategory(category);
		getUwPolicyDS().updateUwPolicy(uwPolicyVO, false);
	}

	/**
	 * get uw life insured info
	 *
	 * @param policyId
	 * @return
	 */
	public Collection<UwLifeInsuredVO> findUwLifeInsured(Long policyId) {
		Long underwriteId = uwPolicyDS.findLastUnderwriteIdbyPolicyId(policyId);
		return uwPolicyDS.findUwLifeInsuredEntitis(underwriteId);
	}

	public boolean hasManualUW(Long policyId) throws GenericException {
		Collection<UwPolicyVO> uwPolicyVOs = uwPolicyDS
				.findUwPolicyByPolicyId(policyId);
		for (UwPolicyVO vo : uwPolicyVOs) {
			if (CodeCst.UW_STATUS__WAITING_UW.equals(vo.getUwStatus())
					|| CodeCst.UW_STATUS__UW_IN_PROGRESS.equals(vo.getUwStatus())
					|| CodeCst.UW_STATUS__ESCALATED.equals(vo.getUwStatus())
					|| CodeCst.UW_STATUS__FINISHED.equals(vo.getUwStatus())
					|| CodeCst.UW_STATUS__INVALID.equals(vo.getUwStatus())) {
				return true;
			}
		}
		return false;
	}

	@Resource(name = UwPolicyService.BEAN_DEFAULT)
	private UwPolicyService uwPolicyDS;

	public UwPolicyService getUwPolicyDS() {
		return this.uwPolicyDS;
	}

	/**
	 * get uw policy bu uw ID
	 */
	public UwPolicyCIVO getUwPolicyByUwId(Long uwId) throws GenericException {
		try {
			UwPolicyVO policyVO = uwPolicyDS.findUwPolicy(uwId);
			Collection productCo = uwPolicyDS.findUwProductEntitis(uwId);
			Iterator productIt = productCo.iterator();
			while (productIt.hasNext()) {
				UwProductVO productVO = (UwProductVO) productIt.next();
				if (productVO.getMasterId() == null) {
					policyVO.setUwDecision(productVO.getDecisionId());
				}
			}
			UwPolicyCIVO plicyCIVO = (UwPolicyCIVO) BeanUtils.getBean(
					UwPolicyCIVO.class, policyVO);
			return plicyCIVO;
		} catch (Exception e) {
			throw ExceptionFactory.parse(e);
		}
	}

	@Override
	public void removeUWInfoRelatedToInsuredRemoved(Long policyId, Long insuredId) {
		uwMedicalLetterService.removeByInsured(insuredId);
		uwRiApplyService.removeNBRiResultByInsuredId(insuredId);
		uwSickFormLetterService.removeByInsured(policyId, insuredId);

		//2016/05/30 add by simon.haung,核保中修改，刪除被保險人，同步刪除uw被保險人
		//刪除核保作業中的被保險人資料
		//uwInsured的insuredId是PartyID,listId才是insuredId,uwListId是PK
		List<UwLifeInsured> uwInsuredList = uwInsuredListDao.findByPolicyId(policyId);
		for (UwLifeInsured uwLifeInsured : uwInsuredList) {
			if (uwLifeInsured.getListId().equals(insuredId)) {
				//IR:383234 刪除要保人豁免時,連帶清除身心障礙相關資料
				uwInsuredDisbilityDS.removeByUwInsuredId(uwLifeInsured.getUwListId());
				//移除t_uw_insured_list
				ApplicationLogger.addLoggerData("uwInsuredListDao.delete() begin");
				uwInsuredListDao.remove(uwLifeInsured.getUwListId());
				ApplicationLogger.addLoggerData("uwInsuredListDao.delete() end");
			}
		}
		HibernateSession3.currentSession().flush();
	}


	/**
	 * get channel org name
	 *
	 * @param channelOrgId
	 * @return String
	 */
	@Override
	public String getChannelOrgName(Long channelOrgId) throws GenericException {
		return uwPolicyDS.getChannelOrgName(channelOrgId);
	}

	/**
	 * <p>Description : 取得新契約保單核保員ID(含在途件邏輯)</p>
	 * <p>Created By : simon.huang</p>
	 * <p>Create Time : Aug 4, 2017</p>
	 * @param policyId
	 * @return
	 * @throws GenericException
	 */
	@Override
	public Long getUNBUnderwriterId(Long policyId) throws GenericException {
		
		Long underwriterId = null;
    	/* (重要)DBean 設定false,與流程共用同一個session,且不作 flush,clear */
        DBean db = new DBean(false);
        CallableStatement stmt = null;
        Connection conn = null;
        try {
        	
            db.connect();
            conn = db.getConnection();
            
            stmt = conn.prepareCall("{ ? = call  pkg_ls_uw_nb.f_get_unb_underwriter_id(?) }");
            stmt.registerOutParameter(1, java.sql.Types.NUMERIC);
            stmt.setLong(2, policyId);
            stmt.execute();
			BigDecimal result = stmt.getBigDecimal(1) ;
            stmt.close();

            if(result != null){
            	underwriterId = result.longValue();
            }
        } catch (Exception e) {
            throw ExceptionFactory.parse(e);
        } finally {
            DBean.closeAll(null, stmt, db);
        }		

		return underwriterId ;
	}
	
	/**
	 * <p>Description : 取得新契約保單核保ID</p>
	 * <p>Created By : simon.huang</p>
	 * <p>Create Time : Aug 4, 2017</p>
	 * @param policyId
	 * @return
	 * @throws GenericException
	 */
	@Override
	public Long getUNBUnderwriteId(Long policyId) throws GenericException {

		Long underwriteId = null;
    	/* (重要)DBean 設定false,與流程共用同一個session,且不作 flush,clear */
        DBean db = new DBean(false);
        CallableStatement stmt = null;
        Connection conn = null;
        try {
        	
            db.connect();
            conn = db.getConnection();
            
            stmt = conn.prepareCall("{ ? = call  pkg_ls_uw_nb.f_get_unb_underwrite_id(?) }");
            stmt.registerOutParameter(1, java.sql.Types.NUMERIC);
            stmt.setLong(2, policyId);
            stmt.execute();
			BigDecimal result = stmt.getBigDecimal(1) ;
            stmt.close();

            if(result != null){
            	underwriteId = result.longValue();
            }
        } catch (Exception e) {
            throw ExceptionFactory.parse(e);
        } finally {
            DBean.closeAll(null, stmt, db);
        }		

		return underwriteId ;
	}

	/**
	 * <p>Description : 提供退費方式匯款改為支票，更新cash相關欄位執行支票列印</p>
	 * <p>Created By : chinho</p>
	 * <p>Create Time : 2017年11月22日</p>
	 * @param feeId
	 * @return
	 * @throws GenericException
	 */
	@Override
	public Long refundCashChangeToCheck(Long feeId, Long oldFeeId) throws GenericException {
		
		CashCIVO cashCIVO = cashCI.findTCashByFeeId(feeId);
		
		//需更新cashier為核保員,
		//@see:UwPolicyDSImpl.refundPolicySuspenses();
		Long underwriterId = this.getUNBUnderwriterId(cashCIVO.getPolicyId());
		EmployeeVO underwriterVO = employeeCI.getEmployeeByEmpId(underwriterId);
        cashCIVO.setCashierId(underwriterId);
        cashCIVO.setCashierOrganId(underwriterVO.getOrganId());
        cashCIVO.setCashierDeptId(underwriterVO.getDeptId());
        cashCI.updateTCash(cashCIVO);
        //end
		
		Long docId = refundCashByCheck(feeId);
		return docId;
	}

	/**
	 * <p>Description : 新契約退費簽核完成執行支票列印</p>
	 * <p>Created By : chinho</p>
	 * <p>Create Time : 2017年11月22日</p>
	 * @param feeId
	 * @return
	 * @throws GenericException
	 */
	@Override
	public Long refundCashByCheck(Long feeId) throws GenericException {
		 // 新契約使用者要FMT_UNB_0364
		
        Long docId = null;
		try {
			docId = unbLetterCI.doCheckPrinting(feeId);
		} catch (com.ebao.foundation.commons.exceptions.GenericException e) {
			docId = null;
		} 
        
		if (docId != null){
        List<Criterion> clist = new ArrayList<Criterion>();
        clist.add(Restrictions.eq("feeId", feeId));
        List<CashRefund> findList = cashRefundDao.find(clist
                        .toArray(new Criterion[0]));
        if (!findList.isEmpty()) {
            CashRefund refund = findList.get(0);
            refund.setDocNo(docId);
            refund.setAttachmentIndi("2");
            refund.setFormNo("FMT_UNB_0364");
            cashRefundDao.saveorUpdate(refund);
           }
		}else{ 
			////IR275048 沒policy_id也可產出支票
	        List<Criterion> clist = new ArrayList<Criterion>();
	        clist.add(Restrictions.eq("feeId", feeId));
	        List<CashRefund> findList = cashRefundDao.find(clist
	                        .toArray(new Criterion[0]));
	        if (!findList.isEmpty()) {
	            CashRefund refund = findList.get(0);
//	            refund.setDocNo(docId);
	            refund.setAttachmentIndi("2");
	            refund.setFormNo("FMT_UNB_0364");
	            cashRefundDao.saveorUpdate(refund);
		        }
		 }
        return docId;
	}

	/**
	 * <p>Description : 提供退費方式匯款改為支票判斷此退費資料是否為新契約case</p>
	 * <p>Created By : chinho</p>
	 * <p>Create Time : 2017年11月22日</p>
	 * @param feeId
	 * @return
	 * @throws GenericException
	 */
	@Override
	public boolean isUNBReufnd(Long feeId) throws GenericException {

		boolean isUnbRefund = false;
    	/* (重要)DBean 設定false,與流程共用同一個session,且不作 flush,clear */
        DBean db = new DBean(false);
        CallableStatement stmt = null;
        Connection conn = null;
        try {
        	
            db.connect();
            conn = db.getConnection();
            
            stmt = conn.prepareCall("{ ? = call PKG_LS_PM_NB_CI.f_is_unb_refund(?) }");
            stmt.registerOutParameter(1, java.sql.Types.VARCHAR);
            stmt.setLong(2, feeId);
            stmt.execute();
			String indi = stmt.getString(1) ;
			
            if(CodeCst.YES_NO__YES.equals(indi)){
            	isUnbRefund = true;
            }
        } catch (Exception e) {
            throw ExceptionFactory.parse(e);
        } finally {
            DBean.closeAll(null, stmt, db);
        }		

		return isUnbRefund ;
	}
	
	// IR 264793 - start
	@Override
	public Long getUnderwriterId(Long policyId, Long userId) throws GenericException {
		Long underwriteId = getUwPolicyDS().retrieveUwIdbyPolicyId(policyId, userId);
		if (underwriteId == null) {
			return null;
		}
//		UwPolicyVO policyVO = getUwPolicyDS().findUwPolicy(underwriteId);
//		Long underwriterId = policyVO.getUnderwriterId();
		return underwriteId;
	}
	// IR 264793 -- end

    @Resource(name = UnbLetterCI.BEAN_DEFAULT)
    UnbLetterCI unbLetterCI;

    @Resource(name = CashRefundDao.BEAN_DEFAULT)
    CashRefundDao cashRefundDao;

    @Resource(name = CashCI.BEAN_DEFAULT)
    private CashCI cashCI;

	@Resource(name = EmployeeCI.BEAN_DEFAULT)
	private EmployeeCI employeeCI;

    @Resource(name = UwInsuredDisabilityService.BEAN_DEFAULT)
    private UwInsuredDisabilityService uwInsuredDisbilityDS;
    
}
