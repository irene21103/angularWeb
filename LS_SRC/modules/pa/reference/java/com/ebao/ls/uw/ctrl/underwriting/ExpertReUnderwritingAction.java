package com.ebao.ls.uw.ctrl.underwriting;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.RollbackException;
import javax.transaction.UserTransaction;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.ls.uw.ds.UwProcessService;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.framework.ExceptionFactory;
import com.ebao.pub.framework.GenericAction;
import com.ebao.pub.framework.GenericException;
import com.ebao.pub.framework.MultiWarning;
import com.ebao.pub.util.Trans;
import com.ebao.pub.util.TransUtils;

/**
 * Back to NBU data extry and synchronized data with NBU module
 * <p>Title:Underwriting</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: eBaoTech Corporation</p>
 * @author jason.luo
 *
 */
public class ExpertReUnderwritingAction extends GenericAction {
  /**
   * Next logic page after back to data entry process
   */
  public static final String NEXT_PAGE = "nextStep";

  @Override
  public MultiWarning processWarning(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {
    MultiWarning warning = new MultiWarning();
    Long policyId = Long.valueOf(com.ebao.ls.encode.encoder.Encode.encodeForHTML(request).getParameter("policyId"));
    Long userId = Long.valueOf(AppContext.getCurrentUser().getUserId());

    UserTransaction trans = null;
    try {
      trans = Trans.getUserTransaction();
      //trans.setTransactionTimeout(180);
      trans.begin();
      /*comments for workflow engine not maintian t_proposal_process,by robert.xu on 2008.6.19
       proposalAPI.changeProposalStatus4Reuw(policyId);
       */
      uwProcessDS.processNBTransaction(policyId, userId, "N");

      //add to control NBU process with workflow engine,by robert.xu on 2008.4.17
      //WfHelper.processManagerForExpertReUW(policyId, result);
      //add end

      trans.commit();
    } catch (Exception e) {
      TransUtils.rollback(trans);
      /*
       * if the exception is caused by transaction time out, we need
       * display a friendly error message to user instead of throwing out
       * a java error
       */
      if (e instanceof RollbackException) {
        warning.addWarning("ERR_20412020042");
        warning.setContinuable(false);
        return warning;

      } else {
        throw ExceptionFactory.parse(e);
      }
      //throw ExceptionFactory.parse(e);
    }
    return warning;
  }

  /**
   * Cancel UwProposal underwriting
   * 
   * @param mapping The ActionMapping used to select this instance
   * @param form The optional ActionForm bean for this request (if any)
   * @param request                   HttpRequest
   * @param response                  HttpResponse
   * @return ActionForward            ActionForward
   * @throws GenericException         Application Exception
   */
  @Override
  public ActionForward process(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws GenericException {

    return mapping.findForward(NEXT_PAGE);
  }

  @Resource(name = UwProcessService.BEAN_DEFAULT)
  private UwProcessService uwProcessDS;

  public void setUwProcessDS(UwProcessService uwProcessDS) {
    this.uwProcessDS = uwProcessDS;
  }

}