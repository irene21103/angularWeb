package com.ebao.ls.beneOwner.ctrl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ebao.foundation.core.logging.Log;
import com.ebao.ls.escape.helper.EscapeHelper;
import com.ebao.ls.pty.ci.CustomerCI;
import com.ebao.ls.pty.ds.vo.CompanySearchConditionVO;
import com.ebao.ls.pty.ds.vo.PersonSearchConditionVO;
import com.ebao.ls.pty.vo.CompanyCustomerVO;
import com.ebao.ls.pty.vo.CustomerVO;
import com.ebao.ls.pub.CodeCst;

@Controller("beneOwnerCtrl")
public class BeneOwnerCtrl {

	private final Log log = Log.getLogger(BeneOwnerCtrl.class);

	@Resource(name = CustomerCI.BEAN_DEFAULT)
	private CustomerCI customerCI;

	@RequestMapping(value = "/ls/beneOwner/ctrl/checkPartySize.d")
	@ResponseBody
	public Object checkPartySize(Model model, @RequestBody Map<String, Object> map, HttpServletRequest request)
			throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();
		String beneOwnerCodeTemp = (String) map.get("beneOwnerCodeTemp");
		String isCompany = (String) map.get("isCompany");
		if (StringUtils.isBlank(beneOwnerCodeTemp) || StringUtils.isBlank(isCompany)) {
			result.put("size", "0");
			return EscapeHelper.escapeHtml(result);
		}
		if (CodeCst.YES_NO__YES.equals(isCompany)) {
			CompanySearchConditionVO conditionVo = new CompanySearchConditionVO();
			conditionVo.setRegisterCode(beneOwnerCodeTemp);
			List<CompanyCustomerVO> list = customerCI.getCompanyList(conditionVo);
			if (list == null || list.size() == 0) {
				result.put("size", "0");
			} else {
				result.put("size", "1");
			}
		} else {
			PersonSearchConditionVO conditions = new PersonSearchConditionVO();
			conditions.setCertiCode(beneOwnerCodeTemp);
			List<CustomerVO> list = customerCI.getPersonList(conditions);
			if (list == null || list.size() == 0) {
				result.put("size", "0");
			} else {
				result.put("size", "1");
			}
		}

		return EscapeHelper.escapeHtml(result);
	}
}
