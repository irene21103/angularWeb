package com.ebao.ls.uw.ctrl.leaveUwAuth;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.UserTransaction;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ebao.ls.pa.pub.auth.UwAreaLeaveAuthServiceImpl;
import com.ebao.ls.pub.auth.AuthItemCst;
import com.ebao.ls.uw.bo.LeaveUwAuthEdit;
import com.ebao.ls.uw.ds.UwAreaService;
import com.ebao.ls.uw.ds.UwAuthLeaveService;
import com.ebao.ls.uw.vo.LeaveUwAuthVO;
import com.ebao.ls.uw.vo.UwAreaVO;
import com.ebao.pub.framework.AppContext;
import com.ebao.pub.framework.ExceptionFactory;
import com.ebao.pub.framework.GenericAction;
import com.ebao.pub.framework.MultiWarning;
import com.ebao.pub.util.StringUtils;
import com.ebao.pub.util.Trans;
import com.ebao.pub.util.TransUtils;

/**
 * <p>Title: 休假管理及核保轄區設置-維護頁面</p>
 * <p>Description: Function Description Only vegetable</p>
 * <p>Copyright: Copyright (c) 2015</p>
 * <p>Company: TGL Co., Ltd.</p>
 * <p>Create Time: Aug 6, 2015</p> 
 * @author 
 * <p>Update Time: Aug 6, 2015</p>
 * <p>Updater: Alex Cheng</p>
 * <p>Update Comments: </p>
 */
public class LeaveUwAuthEditAction extends GenericAction {
  
  protected static final String ACTION_UPDATE = "update";
  protected static final String ACTION_SAVE = "save";
  
  @Resource(name = UwAreaService.BEAN_DEFAULT)
  private UwAreaService uwAreaService;
  
  @Resource(name = UwAuthLeaveService.BEAN_DEFAULT)
  private UwAuthLeaveService uwAuthLeaveService;
  
  @Resource(name = UwAreaLeaveAuthServiceImpl.BEAN_DEFAULT)
  private UwAreaLeaveAuthServiceImpl uwAreaLeaveAuthService;
  
  @Override
  protected MultiWarning processWarning(ActionMapping mapping,
                  ActionForm form,
                  HttpServletRequest request, HttpServletResponse response)
                  throws Exception {
//      LeaveUwAuthForm leaveUwAuthForm = (LeaveUwAuthForm) form;
      MultiWarning mw = new MultiWarning();
      mw.setContinuable(false);
      return mw;
  }
  
  @Override
  public ActionForward process(ActionMapping mapping, ActionForm form, HttpServletRequest request,
          HttpServletResponse response) throws Exception {
    LeaveUwAuthForm leaveUwAuthForm = (LeaveUwAuthForm) form;
    String findForward = "success";
    
    String saction = leaveUwAuthForm.getActionType();
    LeaveUwAuthEdit leaveUwAuthEdit = null;
    
    boolean isEditRole = uwAreaLeaveAuthService.hasAccessRight(AuthItemCst.AUTH_ITEM_LEAVE_UW_AREA_PAGE);
    leaveUwAuthForm.setIsEditRole(isEditRole);
    LeaveUwAuthEdit loginEmpData = uwAuthLeaveService.findDeptByEmpId(Long.toString(AppContext.getCurrentUser().getUserId()));
    Long loginDeptId = loginEmpData.getSectionDeptId() != null ? loginEmpData.getSectionDeptId(): loginEmpData.getMasterDeptId();
    if(!StringUtils.isNullOrEmpty( saction)){
      if(LeaveUwAuthEditAction.ACTION_UPDATE.equals(saction)){
        Long leaveUwAuthListId = StringUtils.isNullOrEmpty(leaveUwAuthForm.getLeaveUwAuthListId()) ? 
                null: Long.valueOf(leaveUwAuthForm.getLeaveUwAuthListId());
        Long empId = StringUtils.isNullOrEmpty(leaveUwAuthForm.getEditEmpId()) ? 
                null: Long.valueOf(leaveUwAuthForm.getEditEmpId());
        leaveUwAuthEdit = uwAuthLeaveService.findUwAuthLeave(leaveUwAuthListId, empId);
        if(leaveUwAuthEdit != null && StringUtils.isNullOrEmpty(leaveUwAuthEdit.getSamePersonFlag())){
          leaveUwAuthEdit.setSamePersonFlag("N");
        }
        if(leaveUwAuthEdit.getMasterEmpId() != null){
          leaveUwAuthForm.setMasterEmpId(leaveUwAuthEdit.getMasterEmpId().toString());
        }
        this.setDefDate(leaveUwAuthEdit);
        List<UwAreaVO> uwAreas = uwAreaService.findUwAreaByEmpId(empId);
        leaveUwAuthEdit.setUwAreas(uwAreas);
        leaveUwAuthForm.setLeaveUwAuthEdit(leaveUwAuthEdit);
        findForward = "success";
      }else if(LeaveUwAuthEditAction.ACTION_SAVE.equals(saction)){
        Long empId = StringUtils.isNullOrEmpty(leaveUwAuthForm.getEditEmpId()) ? 
                null: Long.valueOf(leaveUwAuthForm.getEditEmpId());
        leaveUwAuthEdit = leaveUwAuthForm.getLeaveUwAuthEdit();
        leaveUwAuthEdit.setMasterEmpId(
                StringUtils.isNullOrEmpty(leaveUwAuthForm.getMasterEmpId())?null:new Long(leaveUwAuthForm.getMasterEmpId()));
//        this.setDefDate(leaveUwAuthEdit);
        LeaveUwAuthVO newVO = this.getLeaveUwAuthVO(leaveUwAuthEdit);
        List<UwAreaVO> uwAreaList = this.getUwAreas(leaveUwAuthEdit);
        UserTransaction ut = null;

        try {
            ut = Trans.getUserTransaction();
            ut.begin();
            
            uwAuthLeaveService.saveLeaveUwAuthData(newVO, uwAreaList);
            
            ut.commit();

            List<UwAreaVO> uwAreas = uwAreaService.findUwAreaByEmpId(empId);
            leaveUwAuthEdit.setUwAreas(uwAreas);
            leaveUwAuthForm.setLeaveUwAuthEdit(leaveUwAuthEdit);
            leaveUwAuthForm.setRetMsg("MSG_FMS_497");
            List<LeaveUwAuthEdit> leaveUwAuthList = null;
            if("1".equals(leaveUwAuthForm.getSearchType())){
              Long deptId = StringUtils.isNullOrEmpty(leaveUwAuthForm.getDeptId())? 
                      null : new Long(leaveUwAuthForm.getDeptId());
              Long sectionDeptId = StringUtils.isNullOrEmpty(leaveUwAuthForm.getSectionDeptId())? 
                      null : new Long(leaveUwAuthForm.getSectionDeptId());
              Long empName = StringUtils.isNullOrEmpty(leaveUwAuthForm.getEmpName())? 
                      null : new Long(leaveUwAuthForm.getEmpName());
              String empCode = StringUtils.isNullOrEmpty(leaveUwAuthForm.getEmpId())? 
                      null : leaveUwAuthForm.getEmpId();
              
              leaveUwAuthList = uwAuthLeaveService.searchLeaveDataByEmp(deptId, sectionDeptId, empName, empCode, isEditRole, loginDeptId);
            }else if("2".equals(leaveUwAuthForm.getSearchType())){
              Long channelType = StringUtils.isNullOrEmpty(leaveUwAuthForm.getChannelType())? 
                      null : new Long(leaveUwAuthForm.getChannelType());
              Long channelId = StringUtils.isNullOrEmpty(leaveUwAuthForm.getChannelId())? 
                      null : new Long(leaveUwAuthForm.getChannelId());
              leaveUwAuthList = uwAuthLeaveService.searchLeaveDataByArea(channelType, channelId, isEditRole, loginDeptId);
            }
            request.setAttribute("leaveUwAuthList", leaveUwAuthList);
        } catch (Exception e) {
            TransUtils.rollback(ut);
            throw ExceptionFactory.parse(e);
        }
        findForward = "saveSuccess";
      }
    }
    
    return mapping.findForward(findForward);
  }

  /**
   * <p>Description : 將頁面休假覆核資料組出LeaveUwAuthVO</p>
   * <p>Created By : Alex Cheng</p>
   * <p>Create Time : Aug 12, 2015</p>
   * @param leaveUwAuthEdit
   * @return LeaveUwAuthVO
   */
  private LeaveUwAuthVO getLeaveUwAuthVO(LeaveUwAuthEdit leaveUwAuthEdit){
    LeaveUwAuthVO leaveUwAuthVO = new LeaveUwAuthVO();
    leaveUwAuthVO.setListId(leaveUwAuthEdit.getListId());
    leaveUwAuthVO.setEmpId(leaveUwAuthEdit.getEmpId());
    leaveUwAuthVO.setStartDate(leaveUwAuthEdit.getStartDate());
    leaveUwAuthVO.setEndDate(leaveUwAuthEdit.getEndDate());
    leaveUwAuthVO.setStartDateType(leaveUwAuthEdit.getStartDateType());
    leaveUwAuthVO.setEndDateType(leaveUwAuthEdit.getEndDateType());
    leaveUwAuthVO.setMasterEmpId(leaveUwAuthEdit.getMasterEmpId());
    leaveUwAuthVO.setSamePersonFlag(leaveUwAuthEdit.getSamePersonFlag());
    return leaveUwAuthVO;
  }
  
  /**
   * <p>Description : 將頁面資料組出UwAreaVO集合</p>
   * <p>Created By : Alex Cheng</p>
   * <p>Create Time : Aug 12, 2015</p>
   * @param leaveUwAuthEdit
   * @return List<UwAreaVO>
   */
  private List<UwAreaVO> getUwAreas(LeaveUwAuthEdit leaveUwAuthEdit){
    List<UwAreaVO> uwAreas = new ArrayList<UwAreaVO>();
    if(leaveUwAuthEdit != null && 
            leaveUwAuthEdit.getChannelIds() != null && leaveUwAuthEdit.getChannelIds().length > 0){
      String uwAreaListId = "";
      String channelId = "";
      UwAreaVO uwAreaVO =new UwAreaVO();
      for(int i = 0 ; i < leaveUwAuthEdit.getChannelIds().length ; i++){
        uwAreaVO =new UwAreaVO();
        uwAreaListId = leaveUwAuthEdit.getUwAreaListId()[i];
        channelId = leaveUwAuthEdit.getChannelIds()[i];
        if(!StringUtils.isNullOrEmpty(channelId)){
          uwAreaVO.setListId(StringUtils.isNullOrEmpty(uwAreaListId)?null:new Long(uwAreaListId));
          uwAreaVO.setEmpId(leaveUwAuthEdit.getEmpId());
          uwAreaVO.setChannelId(StringUtils.isNullOrEmpty(channelId)?null:new Long(channelId));
          uwAreas.add(uwAreaVO);
        }
      }
    }
    return uwAreas;
  }
  
  /**
   * <p>Description : 濾掉過期請假日期資料</p>
   * <p>Created By : Alex Cheng</p>
   * <p>Create Time : Aug 13, 2015</p>
   * @param leaveUwAuthEdit
   */
  private void setDefDate(LeaveUwAuthEdit leaveUwAuthEdit){
    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
    SimpleDateFormat sdf2 = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
    if(leaveUwAuthEdit.getEndDate() != null){
      try {
        Date endDate = leaveUwAuthEdit.getEndDate();
        String endDateStr = sdf.format(leaveUwAuthEdit.getEndDate());
        if(leaveUwAuthEdit.getEndDateType().equals("1")){
          endDateStr += " 12:00:00";
        }else{
          endDateStr += " 18:00:00";
        }
         Calendar nowCal = Calendar.getInstance();
         endDate = sdf2.parse(endDateStr);
         Calendar endCal = Calendar.getInstance();
         endCal.setTime(endDate);
         if(endCal.before(nowCal)){
           leaveUwAuthEdit.setStartDate(null);
           leaveUwAuthEdit.setStartDateType("1");
           leaveUwAuthEdit.setEndDate(null);
           leaveUwAuthEdit.setEndDateType("1");
         }
      } catch (ParseException e) {
//        e.printStackTrace();
      }
    }
  }
}
