package com.ebao.ls.riskAggregation.vo;

import java.math.BigDecimal;
import java.util.Date;

import com.ebao.ls.pub.CodeCst;
import com.ebao.pub.framework.GenericVO;

public class PolicyHistoryVO extends GenericVO {
  /**	 */
  private static final long serialVersionUID = 1L;
  private String policyNumber;
  private Long policyId;
  private Date inceptionDate;
  private Integer riskStatus = java.lang.Integer.valueOf(0);
  private BigDecimal masterSA = new java.math.BigDecimal(0);
  private Integer currency;
  private boolean hasUW;
  private BigDecimal totalAnnualPremium = new java.math.BigDecimal(0);
  private Integer proposalStatus;
  private String accessible = CodeCst.YES_NO__YES;

  public BigDecimal getTotalAnnualPremium() {
    return totalAnnualPremium;
  }

  public void setTotalAnnualPremium(BigDecimal totalAnnualPremium) {
    this.totalAnnualPremium = totalAnnualPremium;
  }

  public String getPolicyNumber() {
    return policyNumber;
  }

  public void setPolicyNumber(String policyNumber) {
    this.policyNumber = policyNumber;
  }

  public Date getInceptionDate() {
    return inceptionDate;
  }

  public void setInceptionDate(Date inceptionDate) {
    this.inceptionDate = inceptionDate;
  }

  public Integer getRiskStatus() {
    return riskStatus;
  }

  public void setRiskStatus(Integer riskStatus) {
    this.riskStatus = riskStatus;
  }

  public BigDecimal getMasterSA() {
    return masterSA;
  }

  public void setMasterSA(BigDecimal masterSA) {
    this.masterSA = masterSA;
  }

  public Integer getCurrency() {
    return currency;
  }

  public void setCurrency(Integer currency) {
    this.currency = currency;
  }

  public boolean isHasUW() {
    return hasUW;
  }

  public void setHasUW(boolean hasUW) {
    this.hasUW = hasUW;
  }

  public Integer getProposalStatus() {
    return proposalStatus;
  }

  public void setProposalStatus(Integer proposalStatus) {
    this.proposalStatus = proposalStatus;
  }

  public void setPolicyId(Long policyId) {
    this.policyId = policyId;
  }

  public Long getPolicyId() {
    return policyId;
  }

  /**
   * @return the accessible
   */
  public String getAccessible() {
    return accessible;
  }

  /**
   * @param accessible the accessible to set
   */
  public void setAccessible(String accessible) {
    this.accessible = accessible;
  }
}
