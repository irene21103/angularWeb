#!/bin/sh
#
# Source some variables
#
BASE_NAME=`dirname $0`
. ${BASE_NAME}/shell.properties

AGENT_ID=1

#
# Set in shell.properties (above)
#


LOG_OUTPUT="${BATCH_LOG_PATH}/`date +'%Y-%m-%d_%H-%M-%S'`.log"

# batch processes running
batchprocs() {
    BATCH_PROCS=`ps -ef|grep java|grep -v "grep"|grep  "\-D$ENVFLAG"|awk '{print $2}'`
}


# Start Batch
start() {
    echo  "Starting Batch: \n"
        
    batchprocs
    if [ "${BATCH_PROCS}" != "" ]; then
        echo "batch start failure!"
        echo
        echo "Batch is already running..."
        return 1
    fi


    # All clear
    cd ${BATCH_HOME}/bin
    umask 007
#    /bin/rm -f $LOG_OUTPUT
    sh controller.sh ${ENVFLAG} >>${LOG_OUTPUT} 2>>${LOG_OUTPUT}& 
    sh agent.sh ${AGENT_ID} ${ENVFLAG} >>${LOG_OUTPUT} 2>>${LOG_OUTPUT}&      
    echo "Batch start success">>${LOG_OUTPUT}
    return 0
}

# Stop BATCH

StopBatch() 

{
    echo  "Stopping BATCH: "
   # checkuser
    batchprocs
      echo "BATCH_PROCS=" ${BATCH_PROCS}
    if [ "${BATCH_PROCS}" == "" ]; then
        echo "failure" >>${LOG_OUTPUT}
        echo
        echo "Batch is not running..." >>${LOG_OUTPUT}
        return 1
    fi

    # All clear
    cd ${BATCH_HOME}/bin
    umask 007    
    batchprocs
    if [ "${BATCH_PROCS}" != "" ]; then
        # Let's try to -TERM
        /usr/bin/kill -TERM ${BATCH_PROCS}
    fi
    batchprocs
    if [ "${BATCH_PROCS}" != "" ]; then
        # Let's try it the hard way!
        /usr/bin/kill -9 ${BATCH_PROCS}
    fi

    batchprocs
    if [ "${BATCH_PROCS}" != "" ]; then
        echo "failure!"
        echo
        echo "Some processes could not be stopped:"
        echo ${BATCH_PROCS}
        echo "A possible solution is to try this command once more!"
        return 1
    else
        echo "success!"
        return 0
    fi
}


case "$1" in
    'start')
        start
    ;;
    'stop')
        StopBatch
    ;;
    'restart')
        StopBatch
        start
    ;;  
    'status')
        batchprocs
        if [ "${BATCH_PROCS}" == "" ]; then
            echo "Batch is stopped"  
           else
            echo "Batch is running"
	      echo "PID: ${BATCH_PROCS}"
        fi         
    ;;
    *)
        echo "Usage: $0 {start|stop|restart|status}"
        exit 1
    ;;
esac
echo
exit $?
