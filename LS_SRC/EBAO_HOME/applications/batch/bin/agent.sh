#!/bin/sh

# batch agaent startup script
ENVFLAG=$2

#
# Source some variables
#
BASE_NAME=`dirname $0`
. ${BASE_NAME}/shell.properties

#
# Set in shell.properties
#  


INSTANCE_NAME=batch
############Please don't change the following item#####

LOG_OUTPUT_PATH=${BATCH_LOG_PATH}
APP_LIB_PATH=${BATCH_HOME}/lib
CONF_FILE_PATH=${BATCH_HOME}/conf
EBAO_CLASSPATH=${TMP_PATH}/rms_temp/classes
APPCLASSPATH=${JDBC_DRIVER_PATH}/${JDBC_DRIVER_NAME}:${APP_LIB_PATH}/jta.jar:${APP_LIB_PATH}/servlet.jar:${APP_LIB_PATH}/mail.jar:${APP_LIB_PATH}/activation.jar:${APP_LIB_PATH}/jms.jar:${APP_LIB_PATH}/ejb.jar

CLASSPATH=${JAVA_HOME}/lib/tools.jar:$CLASSPATH:${APPCLASSPATH}:${EBAO_CLASSPATH}:${CONF_FILE_PATH}:${LS_LIB}/lib/'*'
export CLASSPATH
echo CLASSPATH==${CLASSPATH}


outfile=agent$1_`date +'%Y-%m-%d_%H-%M-%S'`
ln -sf ${LOG_OUTPUT_PATH}/$outfile ${WORK_PATH}/agent$1.out
ln -sf ${LOG_OUTPUT_PATH}/$outfile agent$1.out
gcfile=agent_gc_`date +'%Y-%m-%d_%H-%M-%S'`

if [ "${JDK_VENDOR}" = "SUN" ];
then
export PERMSIZE="-XX:MaxPermSize=@BATCH_PERMSIZE@"
export HAEPDUMP="-XX:+HeapDumpOnOutOfMemoryError"
export GCCOMMOND="-Xloggc"
else
GCCOMMOND="-Xverbosegclog"
fi

${JAVA_HOME}/bin/java @Agent_JVMOPTIONS@ ${PERMSIZE} ${HAEPDUMP} -verbose:gc -XX:+PrintGCDetails -XX:+PrintGCTimeStamps ${GCCOMMOND}:${LOG_OUTPUT_PATH}/${gcfile} -D${ENVFLAG}=agent -cp ${CLASSPATH} -Dconfig.file.path.batch=${CONF_FILE_PATH} -Dconfig.appserver.instance.name=${INSTANCE_NAME} com.ebao.pub.batch.dispatch.BatchServer n $1  >> ${LOG_OUTPUT_PATH}/${outfile} 2>&1 &
echo $! > process_a$1.pid
